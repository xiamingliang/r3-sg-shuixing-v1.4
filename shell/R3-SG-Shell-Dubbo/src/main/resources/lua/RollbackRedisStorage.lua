--
-- Created by IntelliJ IDEA.
-- User: chenbin
-- Date: 2019/9/22
-- Time: 10:02 AM
-- To change this template use File | Settings | File Templates.
--

function string.split(str, delimiter)
    if str == nil or str == '' or delimiter == nil then
        return nil
    end
    local results = {}
    for match in (str .. delimiter):gmatch('(.-)' .. delimiter) do
        table.insert(results, match)
    end
    return results
end

local result = {0}

for i, j in pairs(KEYS) do

    local rollback = redis.call('HVALS', j)

    for k, v in pairs(rollback) do

        local rollbackItems = string.split(rollback[k], ',')

        redis.call('HINCRBYFLOAT', rollbackItems[1], 'preout', tonumber(rollbackItems[2]) * -1)
        redis.call('HINCRBYFLOAT', rollbackItems[1], 'prein', tonumber(rollbackItems[3]) * -1)
        redis.call('HINCRBYFLOAT', rollbackItems[1], 'storage', tonumber(rollbackItems[4]) * -1)
        redis.call('HINCRBYFLOAT', rollbackItems[1], 'available', 0 - tonumber(rollbackItems[4])  + tonumber(rollbackItems[2]))

    end

    redis.call('DEL', j)

end

return result

