function string.split(str, delimiter)
    if str == nil or str == '' or delimiter == nil then
        return nil
    end
    local results = {}
    for match in (str .. delimiter):gmatch('(.-)' .. delimiter) do
        table.insert(results, match)
    end
    return results
end

local result = {0}

for i, j in pairs(KEYS) do

    local rollback = redis.call('HVALS', j)

    for k, v in pairs(rollback) do

        local rollbackItems = string.split(rollback[k], ',')

        redis.call('INCRBYFLOAT', rollbackItems[1], tonumber(rollbackItems[2]) * -1)

    end

    redis.call('DEL', j)

end

return result