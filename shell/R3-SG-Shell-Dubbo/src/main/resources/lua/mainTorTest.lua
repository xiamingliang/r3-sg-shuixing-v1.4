--
-- Created by IntelliJ IDEA.
-- User: chose
-- Date: 2019/7/14
-- Time: 10:27
-- To change this template use File | Settings | File Templates.
--
local redis = require 'redis'
local host = '127.0.0.1'
local port = 6379
client = redis.connect(host, port)

redis.call = function(cmd, ...)
    return assert(load('return client:' .. string.lower(cmd) .. '(...)'))(...)
end

function print_r ( t )
    local print_r_cache={}
    local function sub_print_r(t,indent)
        if (print_r_cache[tostring(t)]) then
            print(indent.."*"..tostring(t))
        else
            print_r_cache[tostring(t)]=true
            if (type(t)=="table") then
                for pos,val in pairs(t) do
                    if (type(val)=="table") then
                        print(indent.."["..pos.."] => "..tostring(t).." {")
                        sub_print_r(val,indent..string.rep(" ",string.len(pos)+8))
                        print(indent..string.rep(" ",string.len(pos)+6).."}")
                    elseif (type(val)=="string") then
                        print(indent.."["..pos..'] => "'..val..'"')
                    else
                        print(indent.."["..pos.."] => "..tostring(val))
                    end
                end
            else
                print(indent..tostring(t))
            end
        end
    end
    if (type(t)=="table") then
        print(tostring(t).." {")
        sub_print_r(t,"  ")
        print("}")
    else
        sub_print_r(t,"  ")
    end
    print()
end

redis.call('AUTH', '123456')

redis.call('HMSET', 'sg:storage:999:9999', 'preout', 0, 'prein', 0, 'storage', 100)
redis.call('HMSET', 'sg:storage:888:8888', 'preout', 0, 'prein', 0, 'storage', 5)

-- local redisData = redis.call('HMGET', 'sg:storage:888:8888','prein','preout','storage')

-- for k, v in pairs(redisData) do
--     print(k .. " - " .. v)
-- end

local KEYS = {'sg:storage:999:9999', 'sg:storage:888:8888'}
local ARGV = {'5,10,15,222', '7,14,-6,333','0#0#0#1#1'}