local queryResultList = {}
local result = {}
result[1] = "0"

for k, v in pairs(KEYS) do

    local index = #queryResultList + 1

    queryResultList[index] = redis.call('HMGET', v, 'preout', 'prein', 'storage', 'available')

    if (queryResultList[index][1] == false or #queryResultList[index] == 0) then
        queryResultList[index] = {'0', '0', '0', '0'}
    end

    table.insert(queryResultList[index], 5, v)

end

result[2] = queryResultList

return result