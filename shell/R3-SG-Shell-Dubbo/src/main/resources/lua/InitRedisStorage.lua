function string.split(str, delimiter)
    if str == nil or str == '' or delimiter == nil then
        return nil
    end
    local results = {}
    for match in (str .. delimiter):gmatch('(.-)' .. delimiter) do
        table.insert(results, match)
    end
    return results
end

local result = {0, 0}
local successNum = 0

for k, v in pairs(KEYS) do
    local arg = string.split(ARGV[k], ',')

    redis.call(
        'HMSET',
        v,
        'preout',
        tonumber(arg[1]),
        'prein',
        tonumber(arg[2]),
        'storage',
        tonumber(arg[3]),
        'available',
        tonumber(arg[3]) - tonumber(arg[1])
    )

    successNum = successNum + 1
end

if (#KEYS ~= successNum) then
    result[1] = -1
end

result[2] = successNum

return result