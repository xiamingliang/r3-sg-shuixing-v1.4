local queryResultList = {}
local result = {}
result[1] = "0"

for k, v in pairs(KEYS) do

    local index = #queryResultList + 1

    local storageData = redis.call('GET', v)

    if (storageData == nil or (type(storageData) == 'boolean' and not storageData)) then
        storageData = '0'
    end

    queryResultList[index] = {storageData, v}

end

result[2] = queryResultList

return result