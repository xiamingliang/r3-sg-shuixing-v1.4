function string.split(str, delimiter)
    if str == nil or str == '' or delimiter == nil then
        return nil
    end
    local results = {}
    for match in (str .. delimiter):gmatch('(.-)' .. delimiter) do
        table.insert(results, match)
    end
    return results
end

local negativeStockItem
local rollbackFlg = false
local setDataFlg = false
local result = {}
result[1] = "0"

local iscancel = ARGV[#ARGV]
local messageKey = ARGV[#ARGV-1]

for k, v in pairs(KEYS) do

    local arg = string.split(ARGV[k], ',')

    local storageData = redis.call('GET', v)

    if (storageData == nil or (type(storageData) == 'boolean' and not storageData)) then
        storageData = '0'
    end

    local storageNumChange = tonumber(arg[1])
    local itemId = arg[2]
    local isNegativeStorage = arg[3]

    if (iscancel == '1') then
        storageNumChange = storageNumChange * -1
        ARGV[k] = storageNumChange .. ',' .. arg[2] .. ',' .. arg[3]
    end

    local storageNumSet = tonumber(storageData) + storageNumChange

    --负在库判断，如果当前库存改变量小于0、更新后小于0、参数设置不允许负库存,则进行异常处理
    if (isNegativeStorage == '0' and tonumber(storageNumChange) < 0 and storageNumSet < 0) then
        result[1] = "-1"
        negativeStockItem = itemId
        rollbackFlg = true
        break
    else
        redis.call('SET', v, storageNumSet)
        redis.call('HMSET', messageKey, itemId, v .. ',' .. ARGV[k])
        setDataFlg = true
    end

end

if (setDataFlg) then
    redis.call('EXPIRE', messageKey, 3 * 24 * 60 * 60)
end

if (rollbackFlg) then

    local rollback = redis.call('HVALS', messageKey)

    for k, v in pairs(rollback) do

        local rollbackItems = string.split(rollback[k], ',')

        redis.call('INCRBYFLOAT', rollbackItems[1], tonumber(rollbackItems[2]) * -1)

    end

    redis.call('DEL', messageKey)

end

result[2] = negativeStockItem

return result