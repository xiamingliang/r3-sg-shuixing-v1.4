function string.split(str, delimiter)
    if str == nil or str == '' or delimiter == nil then
        return nil
    end
    local results = {}
    for match in (str .. delimiter):gmatch('(.-)' .. delimiter) do
        table.insert(results, match)
    end
    return results
end

local outStockItemList = {}
local negativeStockItem
local rollbackFlg = false
local setDataFlg = false
local result = {}
result[1] = "0"

local preoutOperateType = ARGV[#ARGV-1]
local iscancel = ARGV[#ARGV]
local messageKey = ARGV[#ARGV-2]

for k, v in pairs(KEYS) do

    local arg = string.split(ARGV[k], ',')

    local storageData = redis.call('HMGET', v, 'preout', 'prein', 'storage', 'available')

    if (storageData[1] == false or #storageData == 0) then
        storageData = {'0', '0', '0', '0'}
    end

    local itemId = arg[4]

    local controlInfo = string.split(arg[5], '#')
    local isNegativePreout = controlInfo[1]
    local isNegativePrein = controlInfo[2]
    local isNegativeStorage = controlInfo[3]
    local isNegativeAvailable = controlInfo[4]

    local preoutNumChange = tonumber(arg[1])
    local preinNumChange = tonumber(arg[2])
    local storageNumChange = tonumber(arg[3])

    if (iscancel == '1') then
        preoutNumChange = preoutNumChange * -1
        preinNumChange = preinNumChange * -1
        storageNumChange = storageNumChange * -1
        ARGV[k] = preoutNumChange .. ',' .. preinNumChange .. ',' .. storageNumChange .. ',' .. arg[4] .. ',' .. arg[5]
    end

    local preoutNumSet = tonumber(storageData[1]) + preoutNumChange
    local preinNumSet = tonumber(storageData[2]) + preinNumChange
    local storageNumSet = tonumber(storageData[3]) + storageNumChange
    local availableNumSet = storageNumSet - preoutNumSet

    --负在单判断，如果当前在单改变量小于0、更新后在单量小于0、参数设置不允许负在单,则进行异常处理
    --负在途判断，如果当前在途改变量小于0、更新后小于0、参数设置不允许负在途,则进行异常处理
    --负在库判断，如果当前库存改变量小于0、更新后小于0、参数设置不允许负库存,则进行异常处理
    --负可用判断，如果当前在单改变量大于0或者在库改变量<0、更新后可用库存小于0、参数设置不允许负可用,则进行异常处理
    if
    ((isNegativePreout == '0' and tonumber(preoutNumChange) < 0 and preoutNumSet < 0) or
            (isNegativePrein == '0' and tonumber(preinNumChange) < 0 and preinNumSet < 0) or
            (isNegativeStorage == '0' and tonumber(storageNumChange) < 0 and storageNumSet < 0) or
            (isNegativeAvailable == '0' and (tonumber(preoutNumChange) > 0 or tonumber(storageNumChange) < 0) and availableNumSet < 0))
    then

        result[1] = preoutOperateType

        if (preoutOperateType == '1' or preoutOperateType == '2') then
            negativeStockItem = itemId
            rollbackFlg = true
            break
        else
            outStockItemList[#outStockItemList + 1] = itemId
        end

    else

        redis.call('HMSET', v, 'preout', preoutNumSet, 'prein', preinNumSet, 'storage', storageNumSet, 'available', availableNumSet)
        redis.call('HMSET', messageKey, itemId, v .. ',' .. ARGV[k])
        setDataFlg = true

    end

end

if (setDataFlg) then
    redis.call('EXPIRE', messageKey, 3 * 24 * 60 * 60)
end

if (rollbackFlg) then

    local rollback = redis.call('HVALS', messageKey)

    for k, v in pairs(rollback) do

        local rollbackItems = string.split(rollback[k], ',')

        redis.call('HINCRBYFLOAT', rollbackItems[1], 'preout', tonumber(rollbackItems[2]) * -1)
        redis.call('HINCRBYFLOAT', rollbackItems[1], 'prein', tonumber(rollbackItems[3]) * -1)
        redis.call('HINCRBYFLOAT', rollbackItems[1], 'storage', tonumber(rollbackItems[4]) * -1)
        redis.call('HINCRBYFLOAT', rollbackItems[1], 'available', 0 - tonumber(rollbackItems[4])  + tonumber(rollbackItems[2]))

    end

    redis.call('DEL', messageKey)

end

result[2] = outStockItemList
result[3] = negativeStockItem

return result