package com.jackrain.nea;

//import com.jackrain.nea.sentinel.R3SentinelConsumer;
//import com.jackrain.nea.util.ApplicationContextHandle;

import com.burgeon.r3.JarResourceUtil;
import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.annotation.EnableAsync;

import java.io.IOException;

/**
 * Main注册程序
 *
 * @author: 易邵峰
 * @since: 2019-01-09
 * create at : 2019-01-09 21:30
 */
@EnableDiscoveryClient
@SpringBootApplication
@MapperScan("com.jackrain.nea")
@EnableAsync
@Slf4j
public class MainApplication extends SpringBootServletInitializer {
    static {
        System.setProperty("dubbo.application.logger", "slf4j");
    }

    public static void main(String[] args) throws IOException {
        // 若将devtools.enabled设置为true，会导致无法加载Dubbo
        System.setProperty("spring.devtools.restart.enabled", "false");

        //KafKa 加载问题，临时解决
        if (null == System.getProperty("java.security.auth.login.config")) {
            String securityLoginConfigFile = JarResourceUtil.exportJarResourceToTempFile("kafka_client_jaas.conf",
                    "r3.kafka_client_jaas.conf", null);
            System.setProperty("java.security.auth.login.config", securityLoginConfigFile);
        }

        ApplicationContext context = SpringApplication.run(applicationClass, args);

//        R3SentinelConsumer sentinelConsumer = ApplicationContextHandle.getBean(R3SentinelConsumer.class);
//        sentinelConsumer.initialSentinel();

        System.out.println("Start SprintBoot Success ContextId=" + context.getId());
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(applicationClass);
    }

    private static Class<MainApplication> applicationClass = MainApplication.class;

}
