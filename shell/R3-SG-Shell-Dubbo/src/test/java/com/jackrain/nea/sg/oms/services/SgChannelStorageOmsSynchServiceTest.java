package com.jackrain.nea.sg.oms.services;

import com.jackrain.nea.MainApplication;
import com.jackrain.nea.sg.oms.model.request.SgChannelStorageOmsInitRequest;
import com.jackrain.nea.sg.oms.model.request.SgChannelStorgeOmsCallbackRequest;
import com.jackrain.nea.sg.oms.model.request.SgStorageOmsSynchronousRequest;
import com.jackrain.nea.sg.oms.model.result.SgStorageOmsSynchronousResult;
import com.jackrain.nea.sg.oms.model.table.SgBChannelStorage;
import com.jackrain.nea.sg.oms.model.tableExtend.SgBChannelSynstockQExtend;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

@RunWith(SpringRunner.class)
@EnableAutoConfiguration
@Transactional
@SpringBootTest(classes = {
        MainApplication.class
})
public class SgChannelStorageOmsSynchServiceTest {

    @Autowired
    SgChannelStorageOmsSynchService sgChannelStorageOmsSynchService;
    @Autowired
    SgChannelStorageOmsCallbackService sgChannelStorageOmsCallbackService;
    @Autowired
    SgChannelStorageOmsInitService sgChannelStorageOmsInitService;
    @Autowired
    SyncChannelStorageAllService syncChannelStorageAllService;


    /**
     * 库存同步服务测试
     */
    @Test
    @Transactional
    public void test1() {
        SgStorageOmsSynchronousRequest request = new SgStorageOmsSynchronousRequest();
        System.out.println("================校验是否需要同步==============");
        request.setSyncType(1);
        System.out.println("================增量同步==============");
        SgBChannelStorage sg = new SgBChannelStorage();
        sg.setId(878L);
        sg.setQty(BigDecimal.valueOf(0));
        sg.setNumiid("563693926014");
        sg.setCpCShopId(18L);
        sg.setCpCShopId(20190424L);
        sg.setCpCShopTitle("淘宝百尚pasonz官方店");
        sg.setCpCShopSellerNick("百尚pasonz");
        sg.setSkuId("3719223223823");
        sg.setPsCSkuId(373170L);
        sg.setPsCSkuEcode("318137072600000");
        sg.setPsCProId(12783L);
        sg.setPsCProEcode("PWH1381903");
        sg.setPsCProEname("船袜");
        sg.setPsCSpec1Ecode("6000");
        sg.setPsCSpec1Id(13000L);
        sg.setPsCSpec1Ename("浅花灰");
        sg.setPsCSpec2Ename("均码");
        sg.setPsCSpec2Ecode("00");
        sg.setPsCSpec2Id(180L);
        sg.setCpCPlatformId(2);
        request.setSgBChannelStorage(sg);

        request.setSourceNo("123456789");
        ValueHolderV14<SgStorageOmsSynchronousResult> holderV14 = sgChannelStorageOmsSynchService.synchronousStock(request);
        System.out.println(holderV14);

    }

    /**
     * 全量同步
     */
    @Test
    @Transactional
    @Rollback(false)
    public void test2() {
        SgStorageOmsSynchronousRequest request = new SgStorageOmsSynchronousRequest();
        System.out.println("================校验是否需要同步==============");
        request.setSyncType(1);
        System.out.println("================全量同步==============");
        SgBChannelStorage sg = new SgBChannelStorage();
        sg.setQty(BigDecimal.valueOf(1000));
        sg.setCpCShopId(20190424L);
        request.setSgBChannelStorage(sg);
        ValueHolderV14<SgStorageOmsSynchronousResult> holderV14 = syncChannelStorageAllService.synchronousStock2(request,false);
        System.out.println(holderV14);

    }

    /**
     * 增量回调Rpc
     */
    @Test
    @Transactional
    @Rollback(false)
    public void test3() {
        System.out.println("================回调Rpc==============");

        SgChannelStorgeOmsCallbackRequest sgChannelStorgeOmsCallbackRequest = new SgChannelStorgeOmsCallbackRequest();
        sgChannelStorgeOmsCallbackRequest.setId(40L);
        sgChannelStorgeOmsCallbackRequest.setErrorMsg("已超时!");
        sgChannelStorgeOmsCallbackRequest.setSynStatus(SgBChannelSynstockQExtend.SynStatusEnum.TIMEOUT);
        ValueHolderV14<Integer> valueHolderV14 = sgChannelStorageOmsCallbackService.callbackRPC(sgChannelStorgeOmsCallbackRequest);
        System.out.println(valueHolderV14);
    }

    /**
     * 全量补偿
     */
    @Test
    @Transactional
    @Rollback(false)
    public void test4() {
        System.out.println("================全量补偿==============");
        ValueHolderV14<SgStorageOmsSynchronousResult> holderV14 = sgChannelStorageOmsSynchService.compensationAll();
        System.out.println(holderV14);
    }

    /**
     * 是否同步
     */
    @Test
    public void test5() {
        System.out.println("================是否同步==============");
        int b = sgChannelStorageOmsSynchService.whetherSynchronization(100L, "3376469692870");
        System.out.println(b);
    }

    /**
     * 初始化渠道库存
     */
    @Test
    public void test6() {
        System.out.println("================初始化渠道库存==============");
        ValueHolderV14<Integer> valueHolderV14 = sgChannelStorageOmsInitService.initChannelStorage(new SgChannelStorageOmsInitRequest());
        System.out.println(valueHolderV14);
    }

    /**
     * ES 查询
     */
    @Test
    public void test7() {
        int b = sgChannelStorageOmsSynchService.searchES(100L, "3376469692870");
        System.out.println(b);
    }


    /**
     * 是否需要同步
     */
    @Test
    public void test8() {
        int b = sgChannelStorageOmsSynchService.whetherSynchronization(100L, "3376469692870");
        System.out.println(b);
    }


    @Test
    @Rollback(false)
    public void test9() {
        ValueHolderV14 valueHolderV14 = sgChannelStorageOmsSynchService.initChannelStorage(null);
        System.out.println(valueHolderV14);
    }
    @Test
    @Rollback(false)
    public void test10() {
        ValueHolderV14 valueHolderV14 = sgChannelStorageOmsSynchService.tbSyncChannelStorageAll();
        System.out.println(valueHolderV14);
    }
    @Test
    @Rollback(false)
    public void test11() {
        ValueHolderV14 valueHolderV14 = sgChannelStorageOmsSynchService.jdSyncChannelStorageAll();
        System.out.println(valueHolderV14);
    }

    @Test
    @Rollback(false)
    public void test12() {
        ValueHolderV14 valueHolderV14 = sgChannelStorageOmsSynchService.compensationAll();
        System.out.println(valueHolderV14);
    }
}
