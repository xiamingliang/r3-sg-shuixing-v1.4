package com.jackrain.nea.sg.oms.services;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.MainApplication;
import com.jackrain.nea.sg.itface.consumer.WMSBackDeliveryOrderConsumer;
import com.jackrain.nea.sg.itface.consumer.WMSBackEntryOrderConsumer;
import com.jackrain.nea.sg.itface.consumer.WMSBackInventoryConsumer;
import com.jackrain.nea.sg.itface.consumer.WMSBackStockOutConsumer;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.IOException;

import static junit.framework.TestCase.assertTrue;

/**
 * @author caopengflying
 * @time 2019/5/7 16:46
 */
@Slf4j
@RunWith(SpringRunner.class)
@EnableAutoConfiguration
@Transactional
@MapperScan({
        "com.jackrain.nea.core.webaction.mapper",
        "com.jackrain.nea.sg.in.mapper",
        "com.jackrain.nea.sg.phyadjust.mapper",
        "com.jackrain.nea.sg.out.mapper"
})

@SpringBootTest(classes = {
        MainApplication.class
       /* // 以下是通用需要加载的
        ApplicationContextHandle.class,
        TmCacheConf.class,
        TableManager.class,
        Dictionary.class,
        DubboConfig.class,
        // 以下是你需要用到什么就加载什么
        WMSBackStockOutConsumer.class,
        SgPhyOutNoticesSaveService.class,
        SgPhyOutResultSaveAndAuditService.class,
        WMSBackEntryOrderConsumer.class,
        SgPhyInResultSaveCmdImpl.class,
        SgPhyInResultSaveService.class,
        SgPhyInNoticesSelectService.class,
        WMSBackInventoryConsumer.class,
        WMSBackDeliveryOrderConsumer.class,
        SgPhyInResultSaveCmdImpl.class,
        SgBPhyInNoticesMapper.class,
        SgBPhyInNoticesItemMapper.class,
        SgPhyAdjSaveAndAuditService.class,
        SgPhyAdjustSaveService.class,
        WebActionMapper.class,
        SgBPhyAdjustMapper.class,
        R3DefaultMqProducerInitial.class,
        R3DefaultOrderMqProducerInitial.class,
        SgBPhyOutNoticesItemMapper.class,
        SgBPhyOutNoticesMapper.class,
        R3MqSendHelper.class,
        SgPhyInResultSaveAndAuditService.class,
        SgPhyInResultAuditService.class,
        SgPhyInNoticesPassService.class*/
})
public class WMSBackConsumerTest {
    @Autowired
    private WMSBackStockOutConsumer wmsBackStockOutConsumer;
    @Autowired
    private WMSBackEntryOrderConsumer wmsBackEntryOrderConsumer;
    @Autowired
    private WMSBackInventoryConsumer wmsBackInventoryConsumer;
    @Autowired
    private WMSBackDeliveryOrderConsumer wmsBackDeliveryOrderConsumer;

    //测试大货出库WMS回传服务 - 回传
    @Test
    public void testStockOutConsumer() throws IOException {
        //已完成
        ClassPathResource resource = new ClassPathResource("mockData/stockOutJson.json");
        File filePath = resource.getFile();
        //读取文件
        String input = FileUtils.readFileToString(filePath, "UTF-8");
        //将读取的数据转换为JSONObject
        JSONObject jsonObject = JSONObject.parseObject(input);
        Boolean flag = wmsBackStockOutConsumer.consume(jsonObject.toJSONString());
        assertTrue(flag);
    }

    //测试大货入库WMS回传服务 - 回传
    @Test
    public void testEntryOrderConsumer() throws IOException {
        ClassPathResource resource = new ClassPathResource("mockData/entryOrderJson.json");
        File filePath = resource.getFile();
        //读取文件
        String input = FileUtils.readFileToString(filePath, "UTF-8");
        //将读取的数据转换为JSONObject
        JSONObject jsonObject = JSONObject.parseObject(input);
        Boolean flag = wmsBackEntryOrderConsumer.consume(jsonObject.toJSONString());
        assertTrue(flag);
    }

    //库存盘点结果WMS回传服务
    @Test
    public void testInventoryConsumer() throws IOException {
        //todo 保存服务报空指针 思远
        ClassPathResource resource = new ClassPathResource("mockData/inventoryJson.json");
        File filePath = resource.getFile();
        //读取文件
        String input = FileUtils.readFileToString(filePath, "UTF-8");
        //将读取的数据转换为JSONObject
        JSONObject jsonObject = JSONObject.parseObject(input);
        Boolean flag = wmsBackInventoryConsumer.consume(jsonObject.toJSONString());
        assertTrue(flag);
    }

    //(云枢纽回传库存中心)零售发货单 - 回传
    @Test
    public void testDeliveryOrder() throws IOException {
        ClassPathResource resource = new ClassPathResource("mockData/deliveryOrderJson.json");
        File filePath = resource.getFile();
        //读取文件
        String input = FileUtils.readFileToString(filePath, "UTF-8");
        //将读取的数据转换为JSONObject
        JSONObject jsonObject = JSONObject.parseObject(input);
        Boolean flag = wmsBackDeliveryOrderConsumer.consume(jsonObject.toJSONString());
        assertTrue(flag);
    }
}


