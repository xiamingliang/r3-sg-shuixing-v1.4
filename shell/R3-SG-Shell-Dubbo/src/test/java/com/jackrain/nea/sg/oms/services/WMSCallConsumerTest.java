package com.jackrain.nea.sg.oms.services;

import com.jackrain.nea.config.TmCacheConf;
import com.jackrain.nea.core.schema.Dictionary;
import com.jackrain.nea.core.schema.TableManager;
import com.jackrain.nea.core.webaction.mapper.WebActionMapper;
import com.jackrain.nea.r3.mq.R3DefaultMqProducerInitial;
import com.jackrain.nea.r3.mq.R3DefaultOrderMqProducerInitial;
import com.jackrain.nea.r3.mq.util.R3MqSendHelper;
import com.jackrain.nea.sg.in.mapper.SgBPhyInNoticesMapper;
import com.jackrain.nea.sg.in.services.SgPhyInNoticesCallBackService;
import com.jackrain.nea.sg.itface.consumer.WMSCallDeliveryOrderConsumer;
import com.jackrain.nea.sg.itface.consumer.WMSCallEntryOrderConsumer;
import com.jackrain.nea.sg.itface.consumer.WMSCallStockOutConsumer;
import com.jackrain.nea.sg.itface.mapper.SgBigPhyOutNoticesToWmsBackMapper;
import com.jackrain.nea.sg.itface.services.SgBigPhyOutNoticesToWmsBackService;
import com.jackrain.nea.sg.out.services.SgPhyOutNoticesSaveService;
import com.jackrain.nea.sg.out.services.SgPhyOutNoticesSaveWMSBackService;
import com.jackrain.nea.sg.out.services.SgPhyOutResultSaveAndAuditService;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static junit.framework.TestCase.assertTrue;

/**
 * @author caopengflying
 * @time 2019/5/7 16:46
 */
@Slf4j
@RunWith(SpringRunner.class)
@EnableAutoConfiguration
@Transactional
@MapperScan({
        "com.jackrain.nea.core.webaction.mapper",
        "com.jackrain.nea.sg.phyadjust.mapper",
        "com.jackrain.nea.sg.itface.mapper",
        "com.jackrain.nea.sg.in.mapper"
})
@SpringBootTest(classes = {
        // 以下是通用需要加载的
        ApplicationContextHandle.class,
        TmCacheConf.class,
        TableManager.class,
        Dictionary.class,

        // 以下是你需要用到什么就加载什么
        WMSCallDeliveryOrderConsumer.class,
        SgPhyOutNoticesSaveService.class,
        SgPhyOutResultSaveAndAuditService.class,
        WebActionMapper.class,
        SgPhyOutNoticesSaveWMSBackService.class,
        R3DefaultMqProducerInitial.class,
        R3DefaultOrderMqProducerInitial.class,
        SgBPhyInNoticesMapper.class,
        R3MqSendHelper.class,
        WMSCallStockOutConsumer.class,
        SgPhyInNoticesCallBackService.class,
        WMSCallEntryOrderConsumer.class,
        SgBigPhyOutNoticesToWmsBackService.class,
        SgBigPhyOutNoticesToWmsBackMapper.class
})
public class WMSCallConsumerTest {

    @Autowired
    private WMSCallDeliveryOrderConsumer wmsCallDeliveryOrderConsumer;
    //测试新增出库通知单并传WMS服务 - 回执
    @Test
    public void testDeliveryOrderConsumer(){
        String message = "{\n" +
                "  \"method\": \"taobao.qimen.deliveryorder.create\",\n" +
                "  \"data\": [{\n" +
                "    \"code\": 0,\n" +
                "    \"message\": \"\",\n" +
                "    \"orderNo\": \"test001\"\n" +
                "  }]\n" +
                "}";
        Boolean flag = wmsCallDeliveryOrderConsumer.consume(message);
        assertTrue(flag);
    }

    @Autowired
    private WMSCallEntryOrderConsumer wmsCallEntryOrderConsumer;
    @Test
    public void testEntryOrderConsumer(){
        String message = "{\n" +
                "  \"method\": \"taobao.qimen.deliveryorder.create\",\n" +
                "  \"data\": [{\n" +
                "    \"code\": 0,\n" +
                "    \"message\": \"\",\n" +
                "    \"orderNo\": \"1557370128376\"\n" +
                "  }]\n" +
                "}";
        Boolean flag = wmsCallEntryOrderConsumer.consume(message);
        assertTrue(flag);
    }

    @Autowired
    private WMSCallStockOutConsumer wmsCallStockOutConsumer;
    //测试大货出库通知单传WMS - 回执
    @Test
    public void testStockOutConsumer(){
        String message = "{\n" +
                "  \"method\": \"taobao.qimen.deliveryorder.create\",\n" +
                "  \"data\": [{\n" +
                "    \"code\": 0,\n" +
                "    \"message\": \"\",\n" +
                "    \"orderNo\": \"test001\"\n" +
                "  }]\n" +
                "}";
        Boolean flag = wmsCallStockOutConsumer.consume(message);
        assertTrue(flag);
    }

}
