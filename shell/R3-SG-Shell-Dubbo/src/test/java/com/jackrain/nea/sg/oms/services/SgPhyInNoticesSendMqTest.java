package com.jackrain.nea.sg.oms.services;

import com.google.common.collect.Lists;
import com.jackrain.nea.MainApplication;
import com.jackrain.nea.sg.in.services.SgPhyInNoticesSendMQService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;


/**
 * 入库通知单上传WMS定时任务
 * @author jiang.cj
 * @since 2019-05-09
 * create at : 2019-05-09 10:03:59
 */
@RunWith(SpringRunner.class)
@EnableAutoConfiguration
@SpringBootTest(classes = {
        MainApplication.class
})
public class SgPhyInNoticesSendMqTest {

    @Autowired
    private SgPhyInNoticesSendMQService sgPhyInNoticesSendMQService;


    @Test
    public void sgPhyInNoticesSendMq() {
        List<Long> list= Lists.newArrayList();
        list.add(231L);
        sgPhyInNoticesSendMQService.saveSgBPhyInResult(list);

    }

}
