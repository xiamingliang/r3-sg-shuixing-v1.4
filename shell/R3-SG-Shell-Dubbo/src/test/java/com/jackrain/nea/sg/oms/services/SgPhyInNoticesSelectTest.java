package com.jackrain.nea.sg.oms.services;


import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.MainApplication;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.in.mapper.SgBPhyInNoticesItemMapper;
import com.jackrain.nea.sg.in.model.request.*;
import com.jackrain.nea.sg.in.model.table.SgBPhyInNoticesItem;
import com.jackrain.nea.sg.in.services.*;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.face.impl.UserImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * 入库通知单上传WMS定时任务
 * @author jiang.cj
 * @since 2019-05-09
 * create at : 2019-05-09 10:03:59
 */
@RunWith(SpringRunner.class)
@EnableAutoConfiguration
@SpringBootTest(classes = {
        MainApplication.class
})
public class SgPhyInNoticesSelectTest {


    @Autowired
    private SgPhyInNoticesSelectByPchService sgPhyInNoticesSelectByPchService;
    @Autowired
    private SgPhyInNoticesSaveService inserver;
    @Autowired
    private SgPhyInNoticesPassService passService;
    @Autowired
    private SgPhyInNoticesCallBackService callBackService;
    @Autowired
    private SgPhyInNoticesVoidService voidService;

    @Test
    public void sgPhyInNoticesSelectTest(){
        HashMap map=new HashMap();
        JSONObject obj=new JSONObject();
        obj.put("value","201906131109270001Z");
        map.put("param",obj);
        ValueHolder vh=sgPhyInNoticesSelectByPchService.getSgPhyInNoticesByPch(map);
        String billNo= (String) vh.get("data");
        System.out.println(billNo);

    }

    @Test
    public void saveSgPhyInNoticesTest(){
        SgPhyInNoticesBillSaveRequest noticesBillSaveRequest = new SgPhyInNoticesBillSaveRequest();
        SgPhyInNoticesRequest noticesRequest = new SgPhyInNoticesRequest();
        noticesRequest.setId((long) -1);
        noticesRequest.setSourceBillId((long) 12341241);
        noticesRequest.setSourceBillType(SgConstantsIF.BILL_TYPE_RETAIL);
        noticesRequest.setSourceBillNo("1234556778");
        noticesRequest.setCpCPhyWarehouseId(16L);
        noticesRequest.setCpCPhyWarehouseEcode("130");
        noticesRequest.setCpCPhyWarehouseEname("测试实体发货仓");
        noticesRequest.setCpCCsEcode("01");
        noticesRequest.setCpCCsEname("乔丹1");
        noticesRequest.setInType(1);

        noticesBillSaveRequest.setNotices(noticesRequest);

        List<SgPhyInNoticesItemRequest> itemList = new ArrayList<>();
        SgPhyInNoticesItemRequest noticesItemRequest = new SgPhyInNoticesItemRequest();
        noticesItemRequest.setId((long) -1);

        noticesItemRequest.setSourceBillItemId(123456L);
        noticesItemRequest.setPsCSkuId(524L);
        noticesItemRequest.setPsCSkuEcode("218237663II0002");
        noticesItemRequest.setPsCProId(448L);
        noticesItemRequest.setPsCProEcode("QIAODAN2019");
        noticesItemRequest.setPsCProEname("乔丹运动男士短袖");
        noticesItemRequest.setGbcode("123456000");
        //noticesItemRequest.setPsCBrandId(1L);

        noticesItemRequest.setPsCSpec1Id(3207L);
        noticesItemRequest.setPsCSpec1Ecode("藕色");
        noticesItemRequest.setPsCSpec1Ename("藕色");

        noticesItemRequest.setPsCSpec2Id(3204L);
        noticesItemRequest.setPsCSpec2Ecode("003");
        noticesItemRequest.setPsCSpec2Ename("170/72A");

        noticesItemRequest.setQty(new BigDecimal(5));
        noticesItemRequest.setQtyIn(new BigDecimal(2));

        noticesItemRequest.setPriceCost(new BigDecimal("18.01"));
        noticesItemRequest.setPriceList(new BigDecimal("20.01"));
        noticesItemRequest.setQty(new BigDecimal("3"));
        itemList.add(noticesItemRequest);


        noticesBillSaveRequest.setItemList(itemList);
        noticesBillSaveRequest.setLoginUser(getUser());


        List<SgPhyInNoticesBillSaveRequest> list = new ArrayList<>();
        list.add(noticesBillSaveRequest);
        inserver.saveSgBPhyInNotices(list);
    }

    @Test
    public void passSgPhyInNoticesTest(){

        SgPhyInNoticesBillPassRequest request =new SgPhyInNoticesBillPassRequest();
        request.setId(1599L);

        List<SgPhyInNoticesItemRequest> list = new ArrayList<>();
        SgPhyInNoticesItemRequest noticesItemRequest =new SgPhyInNoticesItemRequest();

        SgBPhyInNoticesItemMapper noticesItemMapper = ApplicationContextHandle.getBean(SgBPhyInNoticesItemMapper.class);
        SgBPhyInNoticesItem selectItem = noticesItemMapper.selectById(2815L);
        BeanUtils.copyProperties(selectItem, noticesItemRequest);
        noticesItemRequest.setQtyIn(new BigDecimal(1));
        list.add(noticesItemRequest);
        request.setItemList(list);
        request.setLoginUser(getUser());

        passService.updateInPass(request);
    }

    @Test
    public void callBackSgPhyInNoticesTest(){

        List<NoticesCallBackRequest> list =new ArrayList<>();
        NoticesCallBackRequest request=new NoticesCallBackRequest();
        request.setCode(0);
        request.setMessage("1");
        request.setOrderNo("1562152049877");
        list.add(request);
        User u = getUser();
        callBackService.callBackSgPhyInNotices(list,u);
    }

    @Test
    public void voidSgPhyInNoticesTest(){

        SgPhyInNoticesVoidReqeust voidReqeust= new SgPhyInNoticesVoidReqeust();
        voidReqeust.setId(1599L);
        voidService.voidSgBPhyInNotices(voidReqeust);
    }

    private UserImpl getUser() {
        UserImpl user = new UserImpl();
        user.setClientId(27);
        user.setOrgId(37);
        user.setId(100);
        user.setName("root");
        user.setEname("测试用户");
        return user;
    }

}
