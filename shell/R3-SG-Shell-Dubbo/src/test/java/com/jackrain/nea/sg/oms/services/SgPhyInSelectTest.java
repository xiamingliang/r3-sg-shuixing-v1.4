package com.jackrain.nea.sg.oms.services;


import com.jackrain.nea.MainApplication;
import com.jackrain.nea.sg.basic.mapper.RptTStorageSkuMapper;
import com.jackrain.nea.sg.in.services.SgPhyInResultSelectService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;


/**
 * 入库通知单上传WMS定时任务
 * @author jiang.cj
 * @since 2019-05-09
 * create at : 2019-05-09 10:03:59
 */
@RunWith(SpringRunner.class)
@EnableAutoConfiguration
@SpringBootTest(classes = {
        MainApplication.class
})
public class SgPhyInSelectTest {

    @Autowired
    private RptTStorageSkuMapper rptTStorageSkuMapper;
    @Autowired
    private SgPhyInResultSelectService sgPhyInResultSelectService;

    @Test
    public void sgPhyInSelectTest(){
//        SgBPhyInResultSaveRequest request=new SgBPhyInResultSaveRequest();
//        request.setSourceBillId(30121L);
//        request.setSourceBillType(2);
//        sgPhyInResultSelectService.selectSgPhyInResult(request);
        //RptTStorageSku rptTStorageSku = rptTStorageSkuMapper.selectById(1L);


//        HashMap<String, Object> param = new HashMap<>();
//        //param.put("v_version", "1");
//        param.put("v_rpt_date", "20190630");
//        param.put("v_store_ecode", "6008-01");
//        param.put("v_brand_id", 4L);
//        param.put("v_proyear", 876L);
//        param.put("v_prosea", 914L);
//        param.put("v_materieltype", "ZF01");
//        param.put("v_largeseries", 2420L);
//        param.put("v_jacket_bottom", "2001");
//        param.put("v_smallseries", 2479L);
//        param.put("v_smallcate", 362L);
//        param.put("v_pro_ecode", "PHS3381589");
//        param.put("v_sex", 2692L);
//        // param.put("v_clrs",null);
//        param.put("v_sku_ecode", "218339037LN0003");
//        param.put("v_gbcode", "6902349069158");
//        HashMap hashMap = rptTStorageSkuMapper.generateBatchNo(param);



        HashMap<String, Object> param = new HashMap<>();
        //param.put("v_version", "1");
        param.put("start_day", "20190613");
        param.put("end_day", "20190615");
        param.put("v_store_ecode", "6008-01");
        param.put("v_brand_id", 876L);
        param.put("v_pro_ecode", 914L);
        param.put("v_sku_ecode", "ZF01");
        param.put("v_gbcode", "6902349069158");
        //HashMap hashMap = rptTStorageSkuMapper.generateBatchNo(param);
        System.out.println("11");
    }

}
