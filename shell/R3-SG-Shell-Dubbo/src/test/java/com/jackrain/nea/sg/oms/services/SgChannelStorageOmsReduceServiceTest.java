package com.jackrain.nea.sg.oms.services;


import com.jackrain.nea.MainApplication;
import com.jackrain.nea.sg.oms.common.OmsResultCode;
import com.jackrain.nea.sg.oms.model.request.SgChannelStorageOmsReduceItemRequest;
import com.jackrain.nea.sg.oms.model.request.SgChannelStorageOmsReduceRequest;
import com.jackrain.nea.sg.oms.model.result.SgChannelStorageOmsReduceResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;


@RunWith(SpringRunner.class)
@EnableAutoConfiguration
@Transactional
@Rollback(false)
@SpringBootTest(classes = {
        MainApplication.class
})
public class SgChannelStorageOmsReduceServiceTest {

    @Autowired
    SgChannelStorageOmsReduceService sgChannelStorageOmsReduceService;

    private String SUCCESS = "【检测通过】";

    private String FAIL = "【检测失败】";

    /**
     * 基础方法排查
     */
    @Test
    public void prepare() {
        systemPrint(">>>>>>>>>>>>>>>>>start>>>>>>>>>>>>");
        systemPrint(">>>>>>>>>>>>>>>>>开始检测渠道库存计算池>>>>>>>>>>>>");
        SgChannelStorageOmsReduceRequest request = new SgChannelStorageOmsReduceRequest();
        SgChannelStorageOmsReduceItemRequest itemRequest = new SgChannelStorageOmsReduceItemRequest();
        itemRequest.setCpCStoreId(40l);
        itemRequest.setPsCSkuId(15l);
        itemRequest.setQtyStorage(new BigDecimal( -10));
        itemRequest.setSourceCpCShopId(1l);
        request.setItemList(Lists.newArrayList(itemRequest));
        String messageTag = null;
        ValueHolderV14<SgChannelStorageOmsReduceResult> result = sgChannelStorageOmsReduceService.reductChannelStorageOms(request,messageTag);
        if (OmsResultCode.isSuccess(result)) {
            systemPrint("库存扣减服务执行成功");
        } else {
            systemPrint("库存扣减服务执行失败");
        }
        systemPrint("具体信息:" + result.getMessage());
        systemPrint(">>>>>>>>>>>>>>>>>end>>>>>>>>>>>>");
    }



    private void systemPrint(String message) {
        System.out.println(message);
    }
}
