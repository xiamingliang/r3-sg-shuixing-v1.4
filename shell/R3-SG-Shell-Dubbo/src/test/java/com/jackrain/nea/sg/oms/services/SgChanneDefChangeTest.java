package com.jackrain.nea.sg.oms.services;

import com.jackrain.nea.MainApplication;
import com.jackrain.nea.sg.oms.api.SgChannelStoreChangeCmd;
import com.jackrain.nea.sg.oms.model.request.SgChannelStorageDefChangeRequest;
import com.jackrain.nea.sg.oms.model.request.SgChannelStoreChangeRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

@RunWith(SpringRunner.class)
@EnableAutoConfiguration
@Transactional
@SpringBootTest(classes = {
        MainApplication.class
})

public class SgChanneDefChangeTest {

    @Autowired
    SgChannelStorageDefChangeService sgChannelStorageDefChangeService;

    @Autowired
    SgChannelStoreChangeService sgChannelStoreChangeService;

    @Autowired
    SgChannelStoreChangeCmd sgChannelStoreChangeCmd;

    @Test
    @Rollback(false)
    public void test() {
        SgChannelStorageDefChangeRequest request = new SgChannelStorageDefChangeRequest();
        //request.setLowStock(BigDecimal.valueOf(100));//安全库存数
        //request.setStockScale(BigDecimal.valueOf(0.25));//库存比例
        request.setPlatForm(6L);//平台类型
        request.setCpCCustomerId(6L);//经销商ID
        request.setCpCCustomerEcode("经销商编码_修改");//经销商编码
        request.setCpCCustomerEname("经销商名称-京东刘强东修改抹茶妹");//经销商名称
        request.setId(1L);//平台店铺ID
        request.setShopType("10");//店铺类型
        request.setSellerNick("京东刘强东->抹茶妹");//卖家昵称
        request.setIsSyncStock(1);//是否同步
        request.setIsAdd(true);
        ValueHolderV14 valueHolderV14 = sgChannelStorageDefChangeService.changeSbGChannelDef(request);
        System.out.println(valueHolderV14);
    }

    @Test
    @Rollback(false)
    public void test2(){
        SgChannelStoreChangeRequest request = new SgChannelStoreChangeRequest();
        request.setCpCShopId(20L);
        request.setCpCShopTitle("京东刘强东4");
        request.setCpCStoreEcode("StoreEcode");
        request.setIsSend(1);
        request.setCpCStoreId(176L);
        request.setLowStock(BigDecimal.valueOf(3));
        request.setRate(BigDecimal.valueOf(2));
        request.setPriority(1);
        ValueHolderV14 valueHolderV14 = sgChannelStoreChangeService.saveSbGChannelStore(request);
        sgChannelStoreChangeCmd.saveSbGChannelStore(request);
        System.out.println(valueHolderV14);
    }

    @Test
    @Rollback(false)
    public void test3(){
        SgChannelStoreChangeRequest request = new SgChannelStoreChangeRequest();
        request.setCpCShopId(123L);
        ValueHolderV14 valueHolderV14 = sgChannelStoreChangeService.deleteSbGChannelStore(request);
        System.out.println(valueHolderV14);
    }

    /**
     * 根据渠道店铺ID修改渠道店铺标题
     */
    @Test
    @Rollback(false)
    public void test4(){
        SgChannelStoreChangeRequest request = new SgChannelStoreChangeRequest();
        request.setCpCShopId(123L);
        request.setCpCShopTitle("洛师吴彦祖");
        ValueHolderV14 valueHolderV14 = sgChannelStoreChangeService.changeCpCStoreTitleById(request);
        System.out.println(valueHolderV14);
    }



}
