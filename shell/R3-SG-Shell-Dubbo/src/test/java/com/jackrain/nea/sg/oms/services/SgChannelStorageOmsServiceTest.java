package com.jackrain.nea.sg.oms.services;


import com.jackrain.nea.MainApplication;
import com.jackrain.nea.sg.oms.mapper.SgBChannelDefMapper;
import com.jackrain.nea.sg.oms.mapper.SgBChannelProductMapper;
import com.jackrain.nea.sg.oms.mapper.SgBChannelStorageChangeFtpMapper;
import com.jackrain.nea.sg.oms.mapper.SgBChannelStorageMapper;
import com.jackrain.nea.sg.oms.model.request.SgChannelStorageOmsChangeRequest;
import com.jackrain.nea.sg.oms.model.result.SgChannelStorageOmsChangeResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;


@RunWith(SpringRunner.class)
@EnableAutoConfiguration
@Transactional
@SpringBootTest(classes = {
        MainApplication.class
})
public class SgChannelStorageOmsServiceTest {

    @Autowired
    private SgChannelStorageOmsService sgChannelStorageOmsService;

    @Autowired
    private SgBChannelStorageMapper sgBChannelStorageMapper;

    @Autowired
    private SgBChannelDefMapper sgBChannelDefMapper;

    @Autowired
    private SgBChannelProductMapper sgBChannelProductMapper;

    @Autowired
    private SgBChannelStorageChangeFtpMapper sgBChannelStorageChangeFtpMapper;

    private String SUCCESS = "【检测通过】";

    private String FAIL = "【检测失败】";

    /**
     *  测试更新/新增渠道库存服务
     * */
    @Test
    @Rollback(false)
    public void testChangeStorage(){
        systemPrint(">>>>>>>>>>>>>>>>>start>>>>>>>>>>>>");
        systemPrint(">>>>>>>>>>>>>>>>>测试参数>>>>>>>>>>>>");
        SgChannelStorageOmsChangeRequest request = new SgChannelStorageOmsChangeRequest();
        request.setCpCShopId(18L);
        request.setQtyStorage(new BigDecimal(0));
        request.setSkuId("3541186806571");
        request.setSourceCpCShopId(1L);
        systemPrint("[request:" + request.toString() + "]");
        ValueHolderV14<SgChannelStorageOmsChangeResult> holderV14 = sgChannelStorageOmsService.changeStorage(request);
        systemPrint(holderV14.getMessage());
        systemPrint(">>>>>>>>>>>>>>>>>end>>>>>>>>>>>>");
    }

    private void systemPrint(String message) {
        System.out.println(message);
    }
}
