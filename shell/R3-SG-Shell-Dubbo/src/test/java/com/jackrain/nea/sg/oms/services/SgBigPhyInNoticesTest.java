package com.jackrain.nea.sg.oms.services;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.MainApplication;
import com.jackrain.nea.config.DubboConfig;
import com.jackrain.nea.config.PropertiesConf;
import com.jackrain.nea.r3.mq.exception.SendMqException;
import com.jackrain.nea.r3.mq.util.R3MqSendHelper;
import com.jackrain.nea.sg.basic.config.SgStorageMqConfig;
import com.jackrain.nea.sg.in.common.SgInConstants;
import com.jackrain.nea.sg.in.model.request.SgPhyInNoticesSelectBySourceRequest;
import com.jackrain.nea.sg.in.model.request.SgPhyInNoticesVoidReqeust;
import com.jackrain.nea.sg.in.model.result.SgBPhyInNoticesResult;
import com.jackrain.nea.sg.in.services.SgPhyInNoticesSelectService;
import com.jackrain.nea.sg.in.services.SgPhyInNoticesVoidService;
import com.jackrain.nea.sg.out.common.SgOutConstants;
import com.jackrain.nea.sg.out.model.request.SgOutNoticesJdCancelMsgRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.DateUtil;
import com.jackrain.nea.web.face.impl.UserImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: ChenChen
 * @Description:
 * @Date: Create in 20:31 2019/5/7
 */
@RunWith(SpringRunner.class)
@EnableAutoConfiguration
@Transactional
@SpringBootTest(classes = {
        MainApplication.class,
        DubboConfig.class
})
public class SgBigPhyInNoticesTest {
    @Autowired
    private SgPhyInNoticesSelectService sgPhyInNoticesSelectService;

    @Test
    public void selectSgBPhyInNoticesByBillNo() {
        ValueHolderV14<SgBPhyInNoticesResult> result =
                sgPhyInNoticesSelectService.selectSgBPhyInNoticesByBillNo("test");
        result.getData();
    }

    @Test
    public void selectByBigCrgo() {
        ValueHolderV14<List<SgBPhyInNoticesResult>> result =
                sgPhyInNoticesSelectService.selectByBigCrgo();
        result.getData();
    }

    @Autowired
    private SgPhyInNoticesVoidService sgPhyInNoticesVoidService;

    @Test
    public void voidSgBPhyInNotices() {
        SgPhyInNoticesVoidReqeust sgPhyInNoticesVoidReqeust = new SgPhyInNoticesVoidReqeust();
        sgPhyInNoticesVoidReqeust.setSourceBillId((long) 3333248);
        sgPhyInNoticesVoidReqeust.setSourceBillType(1);
        sgPhyInNoticesVoidReqeust.setLoginUser(getUser());
        ValueHolderV14 holderV14 = sgPhyInNoticesVoidService.voidSgBPhyInNotices(sgPhyInNoticesVoidReqeust);
        holderV14.toString();
    }

    @Test
    public void selectBySource() {
        SgPhyInNoticesSelectBySourceRequest sgPhyInNoticesVoidReqeust = new SgPhyInNoticesSelectBySourceRequest();
        sgPhyInNoticesVoidReqeust.setSourceBillId((long) 30053);
        sgPhyInNoticesVoidReqeust.setSourceBillType(2);
        sgPhyInNoticesVoidReqeust.setLoginUser(getUser());
        ValueHolderV14 holderV14 = sgPhyInNoticesSelectService.selectBySource(sgPhyInNoticesVoidReqeust);
        System.out.printf(holderV14.toString());
    }

    @Autowired
    private R3MqSendHelper r3MqSendHelper;
    @Autowired
    private SgStorageMqConfig mqConfig;
    @Autowired
    private PropertiesConf pconf;

    private SgOutNoticesJdCancelMsgRequest sgOutNoticesJdCancelMsgRequest = new SgOutNoticesJdCancelMsgRequest();

    @Test
    public void testMq() {
        List<Long> ids = new ArrayList<>();
        ids.add(11628L);
        ids.add(11878L);
        ids.add(111L);
        ids.add(10482L);
        ids.add(12664L);
        sgOutNoticesJdCancelMsgRequest.setResultList(ids);

        String configName = SgInConstants.SG_PHY_RESULT_IN_MQ_CONFINGNAME;
        String topic = pconf.getProperty(SgInConstants.R3_OMS_SG_OUT_MQ_TOPIC_KEY);
        String tag = "";
        String body = JSONObject.toJSONString(sgOutNoticesJdCancelMsgRequest);
        String msgKey = ids.get(0) + DateUtil.getDateTime(SgOutConstants.MQ_FORMAT);
        try {
            r3MqSendHelper.sendMessage(configName, body, topic, tag, msgKey);
        } catch (SendMqException e) {
            e.printStackTrace();
        }
//        try {
//            r3MqSendHelper.sendMessage(mqConfig.getChannelSynchConfigName(),
//                    JSONObject.toJSONString(new int[]{
//                            57}),
//                    mqConfig.getChannelSynchTopic(),
//                    SgInConstants.SG_PHY_IN_NOTICES_TO_RESULT_MQ_TAG,
//                    "in_notices_to_in_result_" + 1 + "_" + new Date().getTime());
//            System.out.printf("success mq ");
//
//        } catch (SendMqException e) {
//
//
//        }
    }

    private UserImpl getUser() {
        UserImpl user = new UserImpl();
        user.setClientId(27);
        user.setOrgId(37);
        user.setId(100);
        user.setName("root");
        user.setEname("测试用户");
        return user;
    }
}
