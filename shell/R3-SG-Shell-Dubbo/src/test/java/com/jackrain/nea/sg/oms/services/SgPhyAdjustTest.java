package com.jackrain.nea.sg.oms.services;

import com.jackrain.nea.MainApplication;
import com.jackrain.nea.sg.phyadjust.model.request.SgPhyAdjustBillAuditRequest;
import com.jackrain.nea.sg.phyadjust.model.request.SgPhyAdjustBillSaveRequest;
import com.jackrain.nea.sg.phyadjust.model.request.SgPhyAdjustItemSaveRequest;
import com.jackrain.nea.sg.phyadjust.model.request.SgPhyAdjustSaveRequest;
import com.jackrain.nea.sg.phyadjust.services.SgPhyAdjustAuditService;
import com.jackrain.nea.sg.phyadjust.services.SgPhyAdjustSaveService;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.face.impl.UserImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author jiang.cj
 * @since 2019-05-09
 * create at : 2019-05-09 10:03:59
 */
@RunWith(SpringRunner.class)
@EnableAutoConfiguration
@SpringBootTest(classes = {
        MainApplication.class
})
public class SgPhyAdjustTest {

    @Autowired
    private SgPhyAdjustSaveService service;

    @Autowired
    private SgPhyAdjustAuditService auditService;


    @Test
    public void saveSgPhyInResult() {
        //库存调整单 保存
        SgPhyAdjustBillSaveRequest request =new SgPhyAdjustBillSaveRequest();
        SgPhyAdjustSaveRequest adjust = new SgPhyAdjustSaveRequest();
        adjust.setBillType(1);//单据类型
        adjust.setSourceBillType(2);//来源单据类型
        adjust.setCpCPhyWarehouseId(68L);//实体仓id
        adjust.setSgBAdjustPropId(1L);//调整单性质id

        List<SgPhyAdjustItemSaveRequest> listAjustItem =new ArrayList<>();
        SgPhyAdjustItemSaveRequest adjustItem = new SgPhyAdjustItemSaveRequest();

        adjustItem.setQty(new BigDecimal(20));
        adjustItem.setPsCProEcode("218126564");
        adjustItem.setPsCSkuEcode("218126564IM0003");
        adjustItem.setPsCSkuId(331117L);
        adjustItem.setPsCProId(10929L);
        adjustItem.setGbcode("6902349049877-1");
        listAjustItem.add(adjustItem);

        User user = getRootUser();
        request.setObjId(-1L);
        request.setLoginUser(user);
        request.setPhyAdjust(adjust);
        request.setItems(listAjustItem);

        service.save(request, false);
        //auditSgPhyInResult();
    }


    @Test
    public void auditSgPhyInResult() {
        SgPhyAdjustBillAuditRequest request =new SgPhyAdjustBillAuditRequest();
        request.setSourceBillId(4L);
        request.setSourceBillType(2);
        User user = getRootUser();
        request.setUser(user);
        auditService.audit(request);
    }


    private User getRootUser() {
        UserImpl user = new UserImpl();
        user.setId(893);
        user.setName("Fire BALL-V1");
        user.setEname("Pokemon-mapper");
        user.setActive(true);
        user.setClientId(37);
        user.setOrgId(27);
        user.setIsAdmin(2);
        user.setIsDev(2);
        return user;
    }
}
