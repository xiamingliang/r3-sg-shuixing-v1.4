package com.jackrain.nea.sg.oms.services;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.MainApplication;
import com.jackrain.nea.sg.itface.services.SgPhyInNoticeSendToWmsTaskService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


/**
 * 入库通知单上传WMS定时任务
 * @author jiang.cj
 * @since 2019-05-09
 * create at : 2019-05-09 10:03:59
 */
@RunWith(SpringRunner.class)
@EnableAutoConfiguration
@SpringBootTest(classes = {
        MainApplication.class
})
public class SgPhyInNoticesToWscTaskTest {

    @Autowired
    private SgPhyInNoticeSendToWmsTaskService sgPhyInNoticeSendToWmsTaskService;


    @Test
    public void sgPhyInNoticesToWscTask() {
        JSONObject params=new JSONObject();
        sgPhyInNoticeSendToWmsTaskService.sgPhyInNoticeSendToWmsTask(params);

    }

}
