package com.jackrain.nea.sg.oms.services;

import com.google.common.collect.Lists;
import com.jackrain.nea.MainApplication;
import com.jackrain.nea.sg.in.model.request.NoticesCallBackRequest;
import com.jackrain.nea.sg.in.services.SgPhyInNoticesCallBackService;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.face.impl.UserImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * 入库通知单上传WMS回调测试
 * @author jiang.cj
 * @since 2019-05-09
 * create at : 2019-05-09 10:03:59
 */
@RunWith(SpringRunner.class)
@EnableAutoConfiguration
@SpringBootTest(classes = {
        MainApplication.class
})
public class SgPhyInNoticesToWscCallBackTest {
    @Autowired
  private   SgPhyInNoticesCallBackService sgPhyInNoticesCallBackService;

    @Test
    public void sgPhyInNoticesToWscCallBack() {
        List<NoticesCallBackRequest> list= Lists.newArrayList();
        NoticesCallBackRequest request1=new NoticesCallBackRequest();
        request1.setCode(0);
        request1.setMessage("ok");
        request1.setOrderNo("test");
        NoticesCallBackRequest request2=new NoticesCallBackRequest();
        request2.setCode(-1);
        request2.setMessage("fail");
        request2.setOrderNo("test1");
        list.add(request1);
        list.add(request2);
        User user=getUser();
        ValueHolderV14 result=sgPhyInNoticesCallBackService.callBackSgPhyInNotices(list,user);
        System.out.println(result);
    }

    private UserImpl getUser() {
        UserImpl user = new UserImpl();
        user.setClientId(27);
        user.setOrgId(37);
        user.setId(100);
        user.setName("root");
        user.setEname("测试用户");
        return user;
    }
}
