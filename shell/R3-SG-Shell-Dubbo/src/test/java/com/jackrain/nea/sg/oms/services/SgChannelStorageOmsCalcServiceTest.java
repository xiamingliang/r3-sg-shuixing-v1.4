package com.jackrain.nea.sg.oms.services;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.MainApplication;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.mapper.SgBStorageMapper;
import com.jackrain.nea.sg.basic.model.table.SgBStorage;
import com.jackrain.nea.sg.oms.common.OmsResultCode;
import com.jackrain.nea.sg.oms.mapper.SgBChannelDefMapper;
import com.jackrain.nea.sg.oms.mapper.SgBChannelProductMapper;
import com.jackrain.nea.sg.oms.mapper.SgBChannelStorageMapper;
import com.jackrain.nea.sg.oms.mapper.SgBChannelStoreMapper;
import com.jackrain.nea.sg.oms.model.request.SgChannelStorageOmsCalcRequest;
import com.jackrain.nea.sg.oms.model.result.SgChannelStorageOmsCalcResult;
import com.jackrain.nea.sg.oms.model.table.*;
import com.jackrain.nea.sg.oms.model.tableExtend.*;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertTrue;


@RunWith(SpringRunner.class)
@EnableAutoConfiguration
@Transactional

@SpringBootTest(classes = {
        MainApplication.class
})
public class SgChannelStorageOmsCalcServiceTest {

    @Value("${r3.sg.oms.channelStorageCalcBatchNum}")
    private Integer channelStorageCalcBatchNum;

    @Autowired
    SgChannelStorageOmsCalcService sgStorageOmsBillUpdateService;

    @Autowired
    private SgChannelStorageOmsBufferService bufferService;

    @Autowired
    private SgBChannelDefMapper sgBChannelDefMapper;

    @Autowired
    private SgBChannelStoreMapper sgBChannelStoreMapper;

    @Autowired
    private SgBChannelProductMapper sgBChannelProductMapper;

    @Autowired
    private SgBChannelStorageMapper sgBChannelStorageMapper;
    @Autowired
    private SgChannelStorageOmsCalcService sgChannelStorageOmsCalcService;

    @Autowired
    private SgBStorageMapper sgBStorageMapper;

    private String SUCCESS = "【检测通过】";

    private String FAIL = "【检测失败】";

    /**
     * 基础方法排查
     */
    @Test
    public void prepare() {
        systemPrint(">>>>>>>>>>>>>>>>>start>>>>>>>>>>>>");
        systemPrint(">>>>>>>>>>>>>>>>>开始检测渠道库存计算池>>>>>>>>>>>>");
        List<SgBChannelStorageBuffer> sgBChannelStorageBufferList = testgetChannelStorageOmsBuffer("【获取渠道库存计算池】");
        if (CollectionUtils.isNotEmpty(sgBChannelStorageBufferList)) {
            SgBChannelStorageBufferExtend sgBChannelStorageBufferExtend = new SgBChannelStorageBufferExtend();
            BaseExtend baseExtend = new BaseExtend();
            baseExtend.setIdList(sgBChannelStorageBufferList.stream().map(SgBChannelStorageBuffer::getId).collect(Collectors.toList()));
            sgBChannelStorageBufferExtend.setBaseExtend(baseExtend);
            sgBChannelStorageBufferExtend.setUpdateDealStatus(SgBChannelStorageBufferExtend.DealStatusEnum.DEALING.getCode());
//            updateChannelStorageOmsBufferStatus(sgBChannelStorageBufferExtend, "【更新渠道库存计算池】");
            if (updateChannelStorageOmsBufferStatus(sgBChannelStorageBufferExtend, "【更新渠道库存计算池】") > 0) {
                sgBChannelStorageBufferExtend.setUpdateDealStatus(SgBChannelStorageBufferExtend.DealStatusEnum.UN_DEAL.getCode());
                updateChannelStorageOmsBufferStatus(sgBChannelStorageBufferExtend, "【还原渠道库存计算池】");
            }
        }
        systemPrint(">>>>>>>>>>>>>>>>>测试基础数据>>>>>>>>>>>>");
        Long cpCShopId = 20190424l;
        Long errorCpCShopId = 666666l;
        String skuId = "222";
        String reSkuId = "AA";
        String errorSkuId = "test";
        Integer psCSkuId = 15;
        Integer errorPsCSkuId = 666666;
        List<Integer> cpCStoreIdList = Lists.newArrayList(40);
        List<Integer> errorCpCStoreIdList = Lists.newArrayList(1);
        systemPrint("[cpCShopId:" + cpCShopId + "]");
        systemPrint("[errorCpCShopId:" + errorCpCShopId + "]");
        systemPrint("[skuId:" + skuId + "]");
        systemPrint("[reSkuId:" + reSkuId + "]");
        systemPrint("[errorSkuId:" + errorSkuId + "]");
        systemPrint("[psCSkuId:" + psCSkuId + "]");
        systemPrint("[errorPsCSkuId:" + errorPsCSkuId + "]");
        systemPrint("[cpCStoreIdList:" + StringUtils.join(cpCStoreIdList, ",") + "]");
        systemPrint("[errorCpCStoreIdList:" + StringUtils.join(errorCpCStoreIdList, ",") + "]");
        systemPrint(">>>>>>>>>>>>>>>>>开始检测渠道库存计算基础方法>>>>>>>>>>>>");
        systemPrint("<<<<<<<<<<<<<<<<<检测异常数据情况<<<<<<<<<<<<<<<<<");
        SgChannelStorageOmsCalcRequest request = new SgChannelStorageOmsCalcRequest();
        request.setCpCShopId(errorCpCShopId);
        request.setSkuId(errorSkuId);
        testDef(request, "【无效平台店铺】");
        testStore(request, "【无效平台店铺】");
        testProduct(request, "【无匹配的SKU渠道商品信息】");
        request.setSkuId(reSkuId);
        testProduct(request, "【存在多条匹配的SKU渠道商品信息】");
        testStorage(request, errorPsCSkuId, errorCpCStoreIdList, "【无逻辑库存数据】");
        testChannelStorage(request, "【无匹配的渠道商品信息】");
        systemPrint("<<<<<<<<<<<<<<<<<检测正常数据情况<<<<<<<<<<<<<<<<<");
        request.setCpCShopId(cpCShopId);
        request.setSkuId(skuId);
        Boolean defFlag = testDef(request, "【有效平台店铺】") != null;
        Boolean storeFlag = CollectionUtils.isNotEmpty(testStore(request, "【有效平台店铺】"));
        Boolean storageFlag = CollectionUtils.isNotEmpty(testStorage(request, psCSkuId, cpCStoreIdList, "【存在逻辑库存数据】"));
        Boolean channelStorageFlag = testChannelStorage(request, "【存在的渠道商品信息】") != null;
        if (defFlag && storeFlag && storageFlag
                && CollectionUtils.isNotEmpty(sgBChannelStorageBufferList)) {
            // 基础数据都校验通过且计算缓存池有数据,触发计算方法
            BeanUtils.copyProperties(sgBChannelStorageBufferList.get(0), request);
            request.setMessageKey("[bufferId:" + sgBChannelStorageBufferList.get(0).getId() + "]");
            calcChannelStorageOms(request);
        }
        systemPrint(">>>>>>>>>>>>>>>>>end>>>>>>>>>>>>");
    }

    @Test
    public void testCaluChannelStorageOms(){
        SgChannelStorageOmsCalcRequest request = new SgChannelStorageOmsCalcRequest();
        request.setCpCShopId((long) 18);
        request.setSkuId("3376469692870");
        request.setCpCShopTitle("星痕翼");
        request.setSourceCpCShopId(17L);
        request.setMessageKey("bufferId:315");
        request.setSourceNo("bufferId:315");
        calcChannelStorageOms(request);
    }

    @Rollback(false)
    @Test
    public void testExecuteTask(){
        ValueHolderV14<Boolean> holder = sgChannelStorageOmsCalcService.executeTask();
        assertTrue(OmsResultCode.isSuccess(holder));

    }

    /**
     * 测试单个取单库存计算方法
     */
    private void calcChannelStorageOms(SgChannelStorageOmsCalcRequest request) {
        if (request == null) {
            request = new SgChannelStorageOmsCalcRequest();
        }
        ValueHolderV14<SgChannelStorageOmsCalcResult> result = sgStorageOmsBillUpdateService.calcChannelStorageOms(request);
        if (OmsResultCode.isSuccess(result)) {
            systemPrint("库存计算服务执行成功");
        } else {
            systemPrint("库存计算服务执行失败");
        }
        systemPrint("具体信息:" + result.getMessage());
    }


    /**
     * 测试渠道信息表
     *
     * @param
     */
    private List<SgBChannelStorageBuffer> testgetChannelStorageOmsBuffer(String extendMessage) {
        String desc = "测试渠道信息表:";
        try {
            SgBChannelStorageBufferExtend sgBChannelStorageBufferExtend = new SgBChannelStorageBufferExtend();
            sgBChannelStorageBufferExtend.setListSize(channelStorageCalcBatchNum);
            sgBChannelStorageBufferExtend.setDealStatus(SgBChannelStorageBufferExtend.DealStatusEnum.UN_DEAL.getCode());
            ValueHolderV14<List<SgBChannelStorageBuffer>> bufferResult = bufferService.getChannelStorageOmsBuffer(sgBChannelStorageBufferExtend);
            if (!OmsResultCode.isSuccess(bufferResult)) {
                systemPrint(desc + extendMessage + "【获取失败】" + FAIL);
            } else if (CollectionUtils.isEmpty(bufferResult.getData())) {
                systemPrint(desc + extendMessage + "【数据库不存在对应数据】" + SUCCESS);
            } else {
                systemPrint(desc + extendMessage + "【数据库存在对应数据】" + SUCCESS);
            }
            return bufferResult.getData();
        } catch (Exception e) {
            systemPrint(desc + extendMessage + FAIL);
            return Lists.newArrayList();
        }
    }

    /**
     * 测试渠道信息表
     *
     * @param
     */
    private int updateChannelStorageOmsBufferStatus(SgBChannelStorageBufferExtend sgBChannelStorageBufferExtend, String extendMessage) {
        String desc = "测试渠道信息表:";
        ValueHolderV14<List<SgBChannelStorageBuffer>> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        holder.setData(Lists.newArrayList());
        try {
            ValueHolderV14<Integer> result = bufferService.updateChannelStorageOmsBufferStatus(sgBChannelStorageBufferExtend);
            if (!OmsResultCode.isSuccess(result)) {
                systemPrint(desc + extendMessage + "【更新失败】" + FAIL);
            } else if (result.getData() == 0) {
                systemPrint(desc + extendMessage + "【无可更新的数据】" + SUCCESS);
            } else {
                systemPrint(desc + extendMessage + "【更新成功】" + SUCCESS);
            }
            return result.getData();
        } catch (Exception e) {
            systemPrint(desc + extendMessage + FAIL);
            return 0;
        }
    }

    /**
     * 测试渠道信息表
     *
     * @param request
     */
    private SgBChannelDef testDef(SgChannelStorageOmsCalcRequest request, String extendMessage) {
        String desc = "测试渠道信息表:";
        try {
            SgBChannelStorageDefExtend sgBChannelStorageDefExtend = new SgBChannelStorageDefExtend();
            sgBChannelStorageDefExtend.setCpCShopId(request.getCpCShopId());
            QueryWrapper<SgBChannelDef> defQueryWrapper = sgBChannelStorageDefExtend.createQueryWrapper();
            SgBChannelDef sgBChannelDef = sgBChannelDefMapper.selectOne(defQueryWrapper);
            if (sgBChannelDef == null) {
                systemPrint(desc + extendMessage + "【数据库无对应数据】" + SUCCESS);
            } else {
                systemPrint(desc + extendMessage + "【数据库存在对应数据】" + SUCCESS);
            }
            return sgBChannelDef;
        } catch (Exception e) {
            systemPrint(desc + extendMessage + FAIL);
            return null;
        }
    }

    /**
     * 测试渠道逻辑仓关系表
     *
     * @param request
     */
    private List<SgBChannelStore> testStore(SgChannelStorageOmsCalcRequest request, String extendMessage) {
        String desc = "测试获取渠道逻辑仓关系:";
        try {
            SgBChannelStoreExtend sgBChannelStoreExtend = new SgBChannelStoreExtend();
            sgBChannelStoreExtend.setCpCShopId(request.getCpCShopId());
            QueryWrapper<SgBChannelStore> storeQueryWrapper = sgBChannelStoreExtend.createQueryWrapper();
            List<SgBChannelStore> sgBChannelStoreList = sgBChannelStoreMapper.selectList(storeQueryWrapper);
            if (CollectionUtils.isEmpty(sgBChannelStoreList)) {
                systemPrint(desc + extendMessage + "【数据库无对应数据】" + SUCCESS);
            } else {
                systemPrint(desc + extendMessage + "【数据库存在对应数据】" + SUCCESS);
            }
            return sgBChannelStoreList;
        } catch (Exception e) {
            systemPrint(desc + extendMessage + FAIL);
            return Lists.newArrayList();
        }
    }

    /**
     * 测试获取渠道商品表
     *
     * @param request
     */
    private SgBChannelProduct testProduct(SgChannelStorageOmsCalcRequest request, String extendMessage) {
        String desc = "测试获取渠道商品表:";
        try {
            SgBChannelProductExtend sgBChannelProductExtend = new SgBChannelProductExtend();
            sgBChannelProductExtend.setCpCShopId(request.getCpCShopId());
            sgBChannelProductExtend.setSkuId(request.getSkuId());
            QueryWrapper<SgBChannelProduct> productQueryWrapper = sgBChannelProductExtend.createQueryWrapper();
            SgBChannelProduct sgBChannelProduct = sgBChannelProductMapper.selectOne(productQueryWrapper);
            if (sgBChannelProduct == null) {
                systemPrint(desc + extendMessage + "【数据库无对应数据】" + SUCCESS);
            } else {
                systemPrint(desc + extendMessage + "【数据库存在对应数据】" + SUCCESS);
            }
            return sgBChannelProduct;
        } catch (Exception e) {
            systemPrint(desc + extendMessage + FAIL);
            return null;
        }
    }

    /**
     * 测试获取逻辑库数据
     *
     * @param request
     * @param psCSkuId
     * @param cpCStoreIdList
     * @param extendMessage
     */
    private List<SgBStorage> testStorage(SgChannelStorageOmsCalcRequest request, Integer psCSkuId, List<Integer> cpCStoreIdList, String extendMessage) {
        String desc = "测试获取逻辑库数据:";
        try {
            List<SgBStorage> sgBStorageList = sgBStorageMapper.selectList(new QueryWrapper<SgBStorage>()
                    .eq("PS_C_SKU_ID", psCSkuId)
                    .in("CP_C_STORE_ID", cpCStoreIdList));
            if (CollectionUtils.isEmpty(sgBStorageList)) {
                systemPrint(desc + extendMessage + "【数据库无对应数据】" + SUCCESS);
            } else {
                systemPrint(desc + extendMessage + "【数据库存在对应数据】" + SUCCESS);
            }
            return sgBStorageList;
        } catch (Exception e) {
            systemPrint(desc + extendMessage + FAIL);
            return Lists.newArrayList();
        }
    }

    /**
     * 测试获取渠道库存表
     *
     * @param request
     */
    private SgBChannelStorage testChannelStorage(SgChannelStorageOmsCalcRequest request, String extendMessage) {
        String desc = "测试获取渠道库存表:";
        try {
            SgBChannelStorageExtend sgBChannelStorageStorageExtend = new SgBChannelStorageExtend();
            sgBChannelStorageStorageExtend.setCpCShopId(request.getCpCShopId());
            sgBChannelStorageStorageExtend.setSkuId(request.getSkuId());
            QueryWrapper<SgBChannelStorage> storageQueryWrapper = sgBChannelStorageStorageExtend.createQueryWrapper();
            SgBChannelStorage sgBChannelStorage = sgBChannelStorageMapper.selectOne(storageQueryWrapper);
            if (sgBChannelStorage == null) {
                systemPrint(desc + extendMessage + "【数据库无对应数据】" + SUCCESS);
            } else {
                systemPrint(desc + extendMessage + "【数据库存在对应数据】" + SUCCESS);
            }
            return sgBChannelStorage;
        } catch (Exception e) {
            systemPrint(desc + extendMessage + FAIL);
            return null;
        }
    }


    private void systemPrint(String message) {
        System.out.println(message);
    }
}
