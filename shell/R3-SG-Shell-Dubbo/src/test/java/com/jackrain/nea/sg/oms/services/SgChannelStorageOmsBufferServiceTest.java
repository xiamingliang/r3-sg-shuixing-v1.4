package com.jackrain.nea.sg.oms.services;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.MainApplication;
import com.jackrain.nea.model.util.ModelUtil;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.oms.common.OmsResultCode;
import com.jackrain.nea.sg.oms.mapper.SgBChannelStorageBufferMapper;
import com.jackrain.nea.sg.oms.model.request.SgChannelStorageOmsBufferRequest;
import com.jackrain.nea.sg.oms.model.table.SgBChannelStorageBuffer;
import com.jackrain.nea.sg.oms.model.tableExtend.BaseExtend;
import com.jackrain.nea.sg.oms.model.tableExtend.SgBChannelStorageBufferExtend;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import org.apache.commons.io.FileUtils;
import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import static junit.framework.TestCase.*;


/**
 * @author caopengflying
 * @time 2019/4/25 11:23
 */
@RunWith(SpringRunner.class)
@EnableAutoConfiguration
@Transactional
@SpringBootTest(classes = {
        MainApplication.class
})
public class SgChannelStorageOmsBufferServiceTest {
    @Autowired
    private SgBChannelStorageBufferMapper sgBChannelStorageBufferMapper;

    @Test
    @Rollback(false)
    public void testUnique(){
        List<SgBChannelStorageBuffer>sgBChannelStorageBufferList = Lists.newArrayList();
        SgBChannelStorageBuffer sgBChannelStorageBuffer = new SgBChannelStorageBuffer();
        sgBChannelStorageBuffer.setId(2L);
        sgBChannelStorageBuffer.setCpCShopId(18L);
        sgBChannelStorageBuffer.setDealStatus(1);
        sgBChannelStorageBuffer.setSkuId("3541186806572");
        sgBChannelStorageBuffer.setSourceCpCShopId(18L);
        sgBChannelStorageBufferList.add(sgBChannelStorageBuffer);

        SgBChannelStorageBuffer sgBChannelStorageBuffer1 = new SgBChannelStorageBuffer();
        sgBChannelStorageBuffer1.setId(3L);
        sgBChannelStorageBuffer1.setCpCShopId(19L);
        sgBChannelStorageBuffer1.setDealStatus(1);
        sgBChannelStorageBuffer1.setSkuId("3541186806572");
        sgBChannelStorageBuffer1.setSourceCpCShopId(18L);
        sgBChannelStorageBufferList.add(sgBChannelStorageBuffer);
        int i = sgBChannelStorageBufferMapper.insertSgBChannelStorageBufferList(sgBChannelStorageBufferList);
        assertEquals(i, 2);
    }

    /**
     * 测试插入库存计算缓存池
     */
    @Test
    public void testInsertSgBCSBufferList(){
        List<SgBChannelStorageBuffer> sgBChannelStorageBufferList = Lists.newArrayList();
        SgBChannelStorageBuffer sgBChannelStorageBuffer = new SgBChannelStorageBuffer();
        sgBChannelStorageBuffer.setId(ModelUtil.getSequence(SgConstants.SG_B_CHANNEL_STORAGE_BUFFER));
        sgBChannelStorageBuffer.setSourceCpCShopId((long) -1);
        sgBChannelStorageBuffer.setDealStatus(1);
        sgBChannelStorageBuffer.setSkuId("123");
        sgBChannelStorageBuffer.setAdOrgId(SgBChannelStorageBufferExtend.DEFAULT_ORG_ID);
        sgBChannelStorageBuffer.setAdClientId(SgBChannelStorageBufferExtend.DEFAULT_CLIENT_ID);
        sgBChannelStorageBuffer.setCpCShopId((long) 1);
        sgBChannelStorageBuffer.setCpCShopTitle("test");
        sgBChannelStorageBuffer.setCreationdate(new Date());
        sgBChannelStorageBuffer.setSourceNo("无");
        sgBChannelStorageBufferList.add(sgBChannelStorageBuffer);
        Integer integer = sgBChannelStorageBufferMapper.insertSgBChannelStorageBufferList(sgBChannelStorageBufferList);
        assertEquals(java.util.Optional.of(1).get(),integer);
    }

    /**
     * 测试消息转化
     */
    @Test
    public void testParseMessageToSgChannelStorageOmsBufferRequest() throws IOException {
        ClassPathResource resource = new ClassPathResource("mockData/channelStorageBufferJson.json");
        File filePath = resource.getFile();
        //读取文件
        String input = FileUtils.readFileToString(filePath, "UTF-8");
        //将读取的数据转换为JSONObject
        JSONObject jsonObject = JSONObject.parseObject(input);
        List<SgChannelStorageOmsBufferRequest> sgChannelStorageOmsBufferRequestList =
                SgBChannelStorageBufferExtend.parseMessageToSgChannelStorageOmsBufferRequest(jsonObject.toJSONString());
        assertNotNull(sgChannelStorageOmsBufferRequestList);

    }
    @Autowired
    private SgChannelStorageOmsBufferService sgChannelStorageOmsBufferService;
    /**
     * 测试通过逻辑仓ID构造库存缓存池模型，并插入数据库
     */
    @Test
    public void testCreateChannelStorageOmsBuffer(){
        List<SgChannelStorageOmsBufferRequest>sgChannelStorageOmsBufferRequestList = Lists.newArrayList();
        SgChannelStorageOmsBufferRequest sgChannelStorageOmsBufferRequest= new SgChannelStorageOmsBufferRequest();
        sgChannelStorageOmsBufferRequest.setChangeDate(new Date());
        sgChannelStorageOmsBufferRequest.setPsCSkuId(20L);
        sgChannelStorageOmsBufferRequest.setChangeNum(new BigDecimal(-1));
        sgChannelStorageOmsBufferRequest.setCpCStoreId(55L);
        sgChannelStorageOmsBufferRequest.setSourceCpCShopId(10L);
        sgChannelStorageOmsBufferRequest.setSourceNo("SN19051000000014");
        sgChannelStorageOmsBufferRequestList.add(sgChannelStorageOmsBufferRequest);
        String messageTag = null;
        ValueHolderV14<Integer> channelStorageOmsBuffer = sgChannelStorageOmsBufferService.createChannelStorageOmsBuffer(sgChannelStorageOmsBufferRequestList,messageTag);
        assertTrue(OmsResultCode.isSuccess(channelStorageOmsBuffer));
    }

    @Autowired
    private SgChannelStorageOmsBufferService bufferService;
    /**
     * 测试按照ID顺序获取库存计算缓存池数据
     */
    @Test
    public void testGetChannelStorageOmsBuffer(){
        SgBChannelStorageBufferExtend sgBChannelStorageBufferExtend = new SgBChannelStorageBufferExtend();
        sgBChannelStorageBufferExtend.setListSize(20);
        sgBChannelStorageBufferExtend.setDealStatus(SgBChannelStorageBufferExtend.DealStatusEnum.UN_DEAL.getCode());
        ValueHolderV14<List<SgBChannelStorageBuffer>> bufferResult = bufferService.getChannelStorageOmsBuffer(sgBChannelStorageBufferExtend);
        assertTrue(OmsResultCode.isSuccess(bufferResult));
    }

    /**
     * 测试更新计算缓冲池状态
     */
    @Test
    public void testUpdateChannelStorageOmsBuffer(){
        SgBChannelStorageBufferExtend sgBChannelStorageBufferExtend = new SgBChannelStorageBufferExtend();
        BaseExtend baseExtend = new BaseExtend();
        baseExtend.setIdList(Lists.newArrayList((long)1));
        sgBChannelStorageBufferExtend.setBaseExtend(baseExtend);
        sgBChannelStorageBufferExtend.setUpdateDealStatus(SgBChannelStorageBufferExtend.DealStatusEnum.DEALING.getCode());
        ValueHolderV14<Integer> integerValueHolderV14 = bufferService.updateChannelStorageOmsBufferStatus(sgBChannelStorageBufferExtend);
        assertTrue(OmsResultCode.isSuccess(integerValueHolderV14));
    }


}
