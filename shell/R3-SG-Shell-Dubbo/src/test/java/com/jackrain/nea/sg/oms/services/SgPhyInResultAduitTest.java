package com.jackrain.nea.sg.oms.services;

import com.alibaba.fastjson.JSONArray;
import com.jackrain.nea.MainApplication;
import com.jackrain.nea.sg.basic.common.SystemUserResource;
import com.jackrain.nea.sg.in.model.request.SgPhyInResultBillAuditRequest;
import com.jackrain.nea.sg.in.services.SgPhyInResultAuditService;
import com.jackrain.nea.web.face.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;



/**
 *
 * @author jiang.cj
 * @since 2019-05-09
 * create at : 2019-05-09 10:03:59
 */
@RunWith(SpringRunner.class)
@EnableAutoConfiguration
@SpringBootTest(classes = {
        MainApplication.class
})
public class SgPhyInResultAduitTest {

    @Autowired
    private SgPhyInResultAuditService sgPhyInResultAuditService;


    @Test
    public void auditSgPhyInResult() {
        JSONArray ids=new JSONArray();
        ids.add(434);
        User user=SystemUserResource.getRootUser();
        SgPhyInResultBillAuditRequest request=new SgPhyInResultBillAuditRequest();
        request.setIds(ids);
        request.setLoginUser(user);
        sgPhyInResultAuditService.auditSgPhyInResult(request);

    }

}
