package com.jackrain.nea.sg.oms.services;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.MainApplication;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.out.mapper.SgBPhyOutNoticesItemMapper;
import com.jackrain.nea.sg.out.mapper.SgBPhyOutNoticesMapper;
import com.jackrain.nea.sg.out.model.request.SgPhyOutNoticesBillSaveRequest;
import com.jackrain.nea.sg.out.model.request.SgPhyOutNoticesItemSaveRequest;
import com.jackrain.nea.sg.out.model.request.SgPhyOutNoticesSaveRequest;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutNotices;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutNoticesItem;
import com.jackrain.nea.sg.out.services.SgPhyOutNoticesBatchSaveAndWMSService;
import com.jackrain.nea.sg.out.services.SgPhyOutNoticesSaveFaceService;
import com.jackrain.nea.web.face.impl.UserImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * 入库通知单上传WMS定时任务
 * @author jiang.cj
 * @since 2019-05-09
 * create at : 2019-05-09 10:03:59
 */
@RunWith(SpringRunner.class)
@EnableAutoConfiguration
@SpringBootTest(classes = {
        MainApplication.class
})
public class SgPhyInSaveTest {

    @Autowired
    private SgPhyOutNoticesSaveFaceService outService;
    @Autowired
    private SgPhyOutNoticesBatchSaveAndWMSService wmsService;


    @Autowired
    private SgBPhyOutNoticesMapper noticesMapper;
    @Autowired
    private SgBPhyOutNoticesItemMapper itemMapper;


//
//    @Autowired
//    private SgPhyInResultSaveService sgPhyInResultSaveService;
//    @Autowired
//    private SgPhyInResultSaveCmd sgPhyInResultSaveCmd;
//    @Autowired
//    private SgPhyInResultSaveAndAuditService sgPhyInResultSaveAndAuditService;
    @Test
    public void sgPhyInSave() throws IOException {
//        //已完成
//        ClassPathResource resource = new ClassPathResource("mockData/inNoticeJson.json");
//        File filePath = resource.getFile();
//        //读取文件
//        String input = FileUtils.readFileToString(filePath, "UTF-8");
//        //将读取的数据转换为JSONObject
//        SgPhyInResultBillSaveRequest billSaveRequest = JSON.parseObject(input, SgPhyInResultBillSaveRequest.class);
//
//        User user=new UserImpl();
//        ((UserImpl) user).setId(666);
//        ((UserImpl) user).setName("WMS");
//        ((UserImpl) user).setEname("WMS");
//        ((UserImpl) user).setLocale(new Locale("zh_CN"));
//        billSaveRequest.setLoginUser(user);
//        sgPhyInResultSaveAndAuditService.saveAndAuditBill(Lists.newArrayList(billSaveRequest),2);
    }


    @Test
    public void saveSgPhyInNoticesTest(){

//        ValueHolderV14 valueHolder = new ValueHolderV14();
//        HashMap<String, Object> map = new HashMap<>();
//        map.put("resultCode","11000");
//        map.put("resultMessage","始发地址信息及发货仓无效");
//        valueHolder.setCode(0);
//        valueHolder.setMessage("success");
//        valueHolder.setData("{\"deliveryId\":\"JDVC00736339243\",\"preSortResult\":{\"aging\":0,\"agingName\":\"无时效\",\"isHideContractNumbers\":1,\"isHideName\":0,\"road\":\"11\",\"siteId\":707458,\"siteName\":\"◆绵阳园艺山营业部\",\"siteType\":4,\"slideNo\":\"816\",\"sourceCrossCode\":\"99\",\"sourceSortCenterId\":706634,\"sourceSortCenterName\":\"泉州外单分拣中心\",\"sourceTabletrolleyCode\":\"450-3A\",\"targetSortCenterId\":419358,\"targetSortCenterName\":\"绵阳中转场\",\"targetTabletrolleyCode\":\"◆绵1-A03◆\"},\"promiseTimeType\":1,\"resultCode\":100,\"resultMessage\":\"成功\"}");
//        valueHolder.setData(JSON.toJSONString(map));
//        outService.updateOutNotices(valueHolder,getUser(),125L);
//
//
//        valueHolder.setData("{\"deliveryId\":\"JDVC00743245367\",\"resultCode\":100,\"resultMessage\":\"成功\"}");
//        outService.updateOutNotices(valueHolder,getUser(),125L);
//
//        valueHolder.setCode(-1);
//        valueHolder.setData("{\"deliveryId\":\"JDVC00743245367\",\"resultCode\":100,\"resultMessage\":\"成功\"}");
//        outService.updateOutNotices(valueHolder,getUser(),125L);


    }


    @Test
    public void savesTest(){

        SgBPhyOutNotices outNotices =noticesMapper.selectById(11635L);
        QueryWrapper<SgBPhyOutNoticesItem> queryWrapper = new QueryWrapper<>();
        queryWrapper.in("sg_b_phy_out_notices_id",outNotices.getId());
        List<SgBPhyOutNoticesItem> itemList = itemMapper.selectList(queryWrapper);

        outNotices.setSourceBillType(SgConstantsIF.BILL_TYPE_RETAIL);
        outNotices.setSourceBillId(357852963L);
        outNotices.setSourceBillNo("001-002-003");
        outNotices.setId(-1L);
        outNotices.setBillNo("001-002-003-004");



        List<SgPhyOutNoticesBillSaveRequest> requests =new ArrayList<>();

        SgPhyOutNoticesBillSaveRequest saveRequest = new SgPhyOutNoticesBillSaveRequest();
        //主表
        SgPhyOutNoticesSaveRequest outNoticesRequest = new SgPhyOutNoticesSaveRequest();
        BeanUtils.copyProperties(outNotices, outNoticesRequest);
        //明细
        List<SgPhyOutNoticesItemSaveRequest> outNoticesItemRequests =new ArrayList<>();
        SgPhyOutNoticesItemSaveRequest itemSaveRequest = new SgPhyOutNoticesItemSaveRequest();
        BeanUtils.copyProperties(itemList.get(0), itemSaveRequest);
        outNoticesItemRequests.add(itemSaveRequest);

        saveRequest.setOutNoticesItemRequests(outNoticesItemRequests);
        saveRequest.setOutNoticesRequest(outNoticesRequest);
        saveRequest.setLoginUser(getUser());
        requests.add(saveRequest);


        wmsService.batchSaveOutNoticesAndWMS(requests, null, getUser());

    }

    private UserImpl getUser() {
        UserImpl user = new UserImpl();
        user.setClientId(27);
        user.setOrgId(37);
        user.setId(100);
        user.setName("root");
        user.setEname("测试用户");
        return user;
    }

}
