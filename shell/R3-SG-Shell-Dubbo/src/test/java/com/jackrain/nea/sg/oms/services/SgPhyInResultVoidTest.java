package com.jackrain.nea.sg.oms.services;

import com.alibaba.fastjson.JSONArray;
import com.jackrain.nea.MainApplication;
import com.jackrain.nea.sg.basic.common.SystemUserResource;
import com.jackrain.nea.sg.in.services.SgPhyInResultVoidService;
import com.jackrain.nea.web.face.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


/**
 *
 * @author jiang.cj
 * @since 2019-05-09
 * create at : 2019-05-09 10:03:59
 */
@RunWith(SpringRunner.class)
@EnableAutoConfiguration
@SpringBootTest(classes = {
        MainApplication.class
})
public class SgPhyInResultVoidTest {

    @Autowired
    private SgPhyInResultVoidService sgPhyInResultVoidService;


    @Test
    public void auditSgPhyInResult() {
        JSONArray ids=new JSONArray();
        ids.add(31);
        ids.add(33);
        User user=SystemUserResource.getRootUser();
        sgPhyInResultVoidService.voidSgPhyInResult(ids,user);

    }

}
