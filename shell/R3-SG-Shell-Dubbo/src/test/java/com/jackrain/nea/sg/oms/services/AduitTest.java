package com.jackrain.nea.sg.oms.services;


import com.jackrain.nea.MainApplication;
import com.jackrain.nea.r3.mq.exception.SendMqException;
import com.jackrain.nea.sg.basic.common.SystemUserResource;
import com.jackrain.nea.sg.in.services.SgPhyInResultAuditService;
import com.jackrain.nea.web.face.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


/**
 * @author jiang.cj
 * @since 2019-05-09
 * create at : 2019-05-09 10:03:59
 */
@RunWith(SpringRunner.class)
@EnableAutoConfiguration
@SpringBootTest(classes = {
        MainApplication.class
})
public class AduitTest {

    @Autowired
    private SgPhyInResultAuditService sgPhyInResultAuditService;


    @Test
    public void auditSgPhyInResult() throws SendMqException {
        //48
        //3
        User user = SystemUserResource.getRootUser();
        sgPhyInResultAuditService.aduitSgResultBill(48L, user,  false,null);

    }

}
