package com.jackrain.nea.sg.sx.checkdiffe.filter;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Lists;
import com.jackrain.nea.ac.api.AcBOperateCloseCtrlCmd;
import com.jackrain.nea.ac.model.request.AcBOperateCloseCtrlRequest;
import com.jackrain.nea.ac.model.table.AcBOperateClose;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.cpext.model.request.CpStoreCustomerRequest;
import com.jackrain.nea.cpext.model.table.CpCStore;
import com.jackrain.nea.cpext.model.table.CpCustomer;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.oc.basic.cache.CommonCacheValUtils;
import com.jackrain.nea.sg.basic.api.SgStorageQueryCmd;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.model.request.SgStorageQueryRequest;
import com.jackrain.nea.sg.basic.model.table.SgBStorage;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.sx.checkdiffe.common.SgChkDiffeConstants;
import com.jackrain.nea.sg.sx.checkdiffe.mapper.SgBChkDiffeApplyItemMapper;
import com.jackrain.nea.sg.sx.checkdiffe.mapper.SgBChkDiffeApplyMapper;
import com.jackrain.nea.sg.sx.checkdiffe.model.table.SgBChkDiffeApply;
import com.jackrain.nea.sg.sx.checkdiffe.model.table.SgBChkDiffeApplyItem;
import com.jackrain.nea.sg.sx.checkdiffe.utils.MapUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.tableService.DbRowAction;
import com.jackrain.nea.tableService.MainTableRecord;
import com.jackrain.nea.tableService.SubTableRecord;
import com.jackrain.nea.tableService.TableServiceContext;
import com.jackrain.nea.tableService.filter.BaseFilter;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author chenxinxing
 * @description 盘差申请单新增保存
 * @since 2020/2/13 22:36
 */


@Slf4j
@Component
public class SgChkDiffeApplySaveFilter extends BaseFilter {

    @Override
    public void before(TableServiceContext context, MainTableRecord row) {
        Locale locale = context.getLocale();
        User user = context.getUser();
        Long objId = row.getId();

        JSONObject commitData = row.getCommitData();
        Date billDate = commitData.getDate("BILL_DATE");
        if (billDate == null) {
            billDate=new Date();
            commitData.put("BILL_DATE", billDate);
        }

        if (row.getAction().equals(DbRowAction.INSERT)) {
            commitData.put("TRANS_STATUS", SgChkDiffeConstants.SGCHKDIFFE_TRANS_STATUS_WAIT);

            // 补充门店经销商信息
            Long storeId = commitData.getLong("CP_C_STORE_ID");
            AssertUtils.notNull(storeId, "所属组织不能为空！", locale);
            checkCustomerEqual(storeId, commitData, locale);
        } else if (row.getAction().equals(DbRowAction.UPDATE)) {
            Long origStoreId = commitData.getLong("CP_C_STORE_ID");
            if(origStoreId==null){
                SgBChkDiffeApplyMapper sgBChkDiffeApplyMapper = ApplicationContextHandle.getBean(SgBChkDiffeApplyMapper.class);
                SgBChkDiffeApply sgBChkDiffeApply = sgBChkDiffeApplyMapper.selectById(objId);
                origStoreId = sgBChkDiffeApply.getCpCStoreId();
                commitData.put("CP_C_STORE_ID",origStoreId);
            }
            checkCustomerEqual(origStoreId, commitData, locale);
        }


        //关账控制
        String cpCStoreEcode = commitData.getString("CP_C_STORE_ECODE");
        AcBOperateCloseCtrlCmd acBOperateCloseCtrlCmd = (AcBOperateCloseCtrlCmd)
                ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                        AcBOperateCloseCtrlCmd.class.getName(), "ac", "1.0");

        AcBOperateCloseCtrlRequest acBOperateCloseCtrlRequest = new AcBOperateCloseCtrlRequest();
        acBOperateCloseCtrlRequest.setEcode(cpCStoreEcode);
        acBOperateCloseCtrlRequest.setCloseDate(billDate);
        ValueHolderV14<AcBOperateClose> acBOperateCloseValueHolderV14 = null;
        try {
            acBOperateCloseValueHolderV14 = acBOperateCloseCtrlCmd.queryAcBOperateCloseCtrl(acBOperateCloseCtrlRequest);
        } catch (NDSException e) {
            log.debug("acBOperateCloseCtrlCmd.queryAcBOperateCloseCtrl. ReceiveParams:params:{};"
                    + e.getMessage());
            AssertUtils.logAndThrow("调用查询关账服务异常");
        }
        if (!acBOperateCloseValueHolderV14.isOK()) {
            Date closeDate = acBOperateCloseValueHolderV14.getData().getCloseDate();
            if(closeDate==null){
                AssertUtils.logAndThrow("未设置关账日期!");
            }
            SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd");
            String closeDateStr = format.format(closeDate);
            AssertUtils.logAndThrow("此营运中心关账日期为" + closeDateStr + "，不允许再操作单据！");
        }

        log.debug(this.getClass().getName() + ",commitData:" + JSON.toJSONString(commitData, SerializerFeature.WriteMapNullValue));


        SgBChkDiffeApplyItemMapper sgBChkDiffeApplyItemMapper = ApplicationContextHandle.getBean(SgBChkDiffeApplyItemMapper.class);
        List<SgBChkDiffeApplyItem> sgBChkDiffeApplyItems = sgBChkDiffeApplyItemMapper.selectList(new QueryWrapper<SgBChkDiffeApplyItem>().lambda().eq(SgBChkDiffeApplyItem::getSgBChkDiffeApplyId, objId));
        List<String> existSkuEcodes = Lists.transform(sgBChkDiffeApplyItems, o -> o.getPsCSkuEcode());

        List<String> psCProEcodeList = Lists.newArrayList();
        SubTableRecord subTableRecord = row.getSubTables().get(SgChkDiffeConstants.SG_B_CHK_DIFFE_APPLY_ITEM.toUpperCase());
        if (subTableRecord != null) {
            subTableRecord.getRows().forEach(rowRecord -> {
                JSONObject itemCommitData = rowRecord.getCommitData();

                String psCSkuEcode = itemCommitData.getString("PS_C_SKU_ECODE");
                if(existSkuEcodes.contains(psCSkuEcode)){
                    AssertUtils.logAndThrow("明细中存在已添加的商品条码,请删除该条码,重新添加!");
                }

                log.debug(this.getClass().getName() + ",itemCommitData:" + JSON.toJSONString(itemCommitData, SerializerFeature.WriteMapNullValue));
                BigDecimal qtyActual = itemCommitData.getBigDecimal("QTY_ACTUAL");
                AssertUtils.notNull(qtyActual, "实盘数量不能为空！", locale);
                psCProEcodeList.add(itemCommitData.getString("PS_C_PRO_ECODE"));
            });
        }

        if (subTableRecord != null) {
            Long storeId = commitData.getLong("CP_C_STORE_ID");
            List<SgBStorage> sgBStorages = queryPhysicalStorage(user, storeId, psCProEcodeList);
            Map<String, SgBStorage> psCProEcode2SgBStorage = MapUtils.convert2Map(sgBStorages, o -> o.getPsCProEcode());

            subTableRecord.getRows().forEach(rowRecord -> {
                JSONObject itemCommitData = rowRecord.getCommitData();

                String psCProEcode = itemCommitData.getString("PS_C_PRO_ECODE");
                SgBStorage sgBStorage = psCProEcode2SgBStorage.get(psCProEcode);
                BigDecimal qtyStorage = null;

                //账面数量
                if (sgBStorage != null) {
                    qtyStorage = sgBStorage.getQtyStorage();
                } else {
                    qtyStorage =BigDecimal.ZERO;
                }
                itemCommitData.put("QTY_BOOK", qtyStorage);

                //审核数量
                itemCommitData.put("QTY_STATUS", BigDecimal.ZERO);

                //实盘数量
                BigDecimal qtyActual = itemCommitData.getBigDecimal("QTY_ACTUAL");

                //申请调整数量
                BigDecimal num1 = new BigDecimal(qtyStorage.toString());
                BigDecimal num2 = new BigDecimal(qtyActual.toString());
                BigDecimal qtyApply = num1.subtract(num2);
                itemCommitData.put("QTY_APPLY", qtyApply);

                //差异数量
                BigDecimal num3 = new BigDecimal(qtyApply.toString());
                BigDecimal num4 = new BigDecimal(BigDecimal.ZERO.toString());
                BigDecimal qtyDiffeRence = num3.subtract(num4);
                itemCommitData.put("QTY_DIFFERENCE", qtyDiffeRence);

            });
        }

    }


    /**
     * 补充门店和经销商信息
     *
     * @param storeId    店仓
     * @param commitData 入参
     * @param locale     国际化
     */
    private void checkCustomerEqual(Long storeId, JSONObject commitData, Locale locale) {
        CommonCacheValUtils commonUtils = ApplicationContextHandle.getBean(CommonCacheValUtils.class);
        CpStoreCustomerRequest request = commonUtils.getStoreAndCustomer(storeId);

        AssertUtils.cannot((request == null || request.getCpCStore() == null), "店仓不存在!", locale);
        AssertUtils.cannot((request == null || request.getCpCustomer() == null), "经销商不存在!", locale);

        CpCStore store = request.getCpCStore();
        CpCustomer customer = request.getCpCustomer();

        commitData.put("CP_C_STORE_ID", store.getId());
        commitData.put("CP_C_STORE_ECODE", store.getEcode());
        commitData.put("CP_C_STORE_ENAME", store.getEname());
        commitData.put("BELONGS_LEGAL_ECODE", store.getErpAccount());

        commitData.put("STORE_WAREHOUSE", store.getErpStore());

        commitData.put("CP_C_CUSTOMER_ID", customer.getId());
        commitData.put("CP_C_CUSTOMER_ECODE", customer.getEcode());
        commitData.put("CP_C_CUSTOMER_ENAME", customer.getEname());

//        if (commitData.getDate("BELONGS_LEGAL") == null) {
//            commitData.put("BELONGS_LEGAL_ID", customer.getId());
//            commitData.put("BELONGS_LEGAL_ECODE", customer.getEcode());
//            commitData.put("BELONGS_LEGAL", customer.getEname());
//        }
    }


    /**
     * 批量查询【库存查询】sku数量信息
     *
     * @param loginUser       用户信息
     * @param storeId         店仓ID
     * @param psCProEcodeList psCProEcodeList
     * @return 【库存查询结果】
     */
    private List<SgBStorage> queryPhysicalStorage(User loginUser, Long storeId, List<String> psCProEcodeList) {
        SgStorageQueryCmd storageQueryCmd = (SgStorageQueryCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(), SgStorageQueryCmd.class.getName(),
                SgConstantsIF.GROUP, SgConstantsIF.VERSION);

        List<SgBStorage> storageList = new ArrayList<>();
        List<Long> storeIds = new ArrayList<>();
        storeIds.add(storeId);

        SgStorageQueryRequest request = new SgStorageQueryRequest();
        request.setStoreIds(storeIds);

        int itemNum = psCProEcodeList.size();
        int pageSize = SgConstants.SG_COMMON_INSERT_PAGE_SIZE;
        int page = itemNum / pageSize;
        if (itemNum % pageSize != 0) page++;
        for (int i = 0; i < page; i++) {
            int startIndex = i * pageSize;
            int endIndex = (i + 1) * pageSize;
            List<String> psCProEcodes = psCProEcodeList.subList(startIndex, endIndex < itemNum ? endIndex : itemNum);
            request.setProEcodes(psCProEcodes);
            ValueHolderV14<List<SgBStorage>> v14 = storageQueryCmd.queryStorage(request, loginUser);
            com.jackrain.nea.utils.AssertUtils.notNull(v14, "库存查询未知错误！", loginUser.getLocale());
            if (v14.getCode() == ResultCode.FAIL) {
                com.jackrain.nea.utils.AssertUtils.logAndThrow("库存查询异常：" + v14.getMessage(), loginUser.getLocale());
            }
            storageList.addAll(v14.getData());
        }
        return storageList;
    }
}
