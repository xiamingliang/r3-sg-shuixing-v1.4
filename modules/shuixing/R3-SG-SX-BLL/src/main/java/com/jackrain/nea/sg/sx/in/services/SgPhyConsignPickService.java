package com.jackrain.nea.sg.sx.in.services;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.data.basic.model.request.SkuInfoQueryRequest;
import com.jackrain.nea.data.basic.services.BasicPsQueryService;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.ps.api.result.PsCProSkuResult;
import com.jackrain.nea.psext.api.utils.JsonUtils;
import com.jackrain.nea.resource.SystemUserResource;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.in.mapper.SgBInPickorderMapper;
import com.jackrain.nea.sg.in.mapper.SgBInPickorderNoticeItemMapper;
import com.jackrain.nea.sg.in.mapper.SgBPhyInNoticesMapper;
import com.jackrain.nea.sg.in.model.request.SgPhyInPickOrderSaveRequest;
import com.jackrain.nea.sg.in.model.request.SgPhyInPickUpGoodSaveRequest;
import com.jackrain.nea.sg.in.model.request.SgPhyInPickorderTeusItemSaveRequest;
import com.jackrain.nea.sg.in.model.table.SgBInPickorder;
import com.jackrain.nea.sg.in.model.table.SgBInPickorderNoticeItem;
import com.jackrain.nea.sg.in.model.table.SgBPhyInNotices;
import com.jackrain.nea.sg.in.services.SgPhyInPickOrderGenerateService;
import com.jackrain.nea.sg.in.services.SgPhyInPickOrderSaveService;
import com.jackrain.nea.sg.sx.in.model.request.SgPhyInPickOrderVoidRequest;
import com.jackrain.nea.sg.sx.in.model.request.SgPhyPickTeusItemRequest;
import com.jackrain.nea.sg.unpack.common.SgUnpackConstants;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author: 周琳胜
 * @since: 2019/12/12
 * create at : 2019/12/12 11:27
 */
@Slf4j
@Component
public class SgPhyConsignPickService {

    @Autowired
    SgPhyInPickOrderVoidService sgPhyInPickOrderVoidService;

    @Autowired
    SgPhyInPickOrderGenerateService sgPhyInPickOrderGenerateService;

    @Autowired
    SgBInPickorderNoticeItemMapper sgBInPickorderNoticeItemMapper;

    @Autowired
    SgBInPickorderMapper sgBInPickorderMapper;

    @Autowired
    SgPhyInPickOrderSaveService sgPhyInPickOrderSaveService;


    public ValueHolderV14 consignPick(JSONObject jsonObject) {
        ValueHolderV14 v14 = new ValueHolderV14<>();
        if (log.isDebugEnabled()) {
            log.debug("Start SgPhyConsignPickService.consignPick. ReceiveParams:params:{};"
                    + jsonObject);
        }
        //参数检验
        if (jsonObject == null) {
            throw new NDSException("request is null");
        }

        JSONArray param = jsonObject.getJSONArray("param");
        Map map = new HashMap();
        for (Object o : param) {
            JSONObject object = JSONObject.parseObject(JSON.toJSONString(o));
            JSONObject consignmentpick = object.getJSONObject("CONSIGNMENTPICK");
            String tyNo = consignmentpick.getString("TY_NO");
            JSONArray sourceitem = object.getJSONArray("SOURCEITEM");
            JSONArray boxitem = object.getJSONArray("BOXITEM");
            // jsonarray转成list
            List<String> sourceBillNoList = billNotoList(sourceitem);

            // “销售单号”与入库通知单的“来源单据编号”进行逐一匹配，获取入库通知单的单据编号
            SgBPhyInNoticesMapper noticesMapper = ApplicationContextHandle.getBean(SgBPhyInNoticesMapper.class);
            //sg_b_in_pickorder_notice_item
            List<SgBInPickorderNoticeItem> sgBInPickorderNoticeItem = sgBInPickorderNoticeItemMapper.selectList(new QueryWrapper<SgBInPickorderNoticeItem>().lambda().in(SgBInPickorderNoticeItem::getStockBillNo, sourceBillNoList));
            if (sgBInPickorderNoticeItem == null || sgBInPickorderNoticeItem.size() == 0) {
                AssertUtils.logAndThrow("来源入库通知单为空");
            }
            List<String> list = new ArrayList<>();
            for (SgBInPickorderNoticeItem bInPickorderNoticeItem : sgBInPickorderNoticeItem) {
                list.add(bInPickorderNoticeItem.getSourceBillNo());
            }
            List<SgBPhyInNotices> sgBPhyInNoticesList = noticesMapper.selectList(new QueryWrapper<SgBPhyInNotices>().lambda().in(SgBPhyInNotices::getSourceBillNo, list));
            if (CollectionUtils.isEmpty(sgBPhyInNoticesList)) {
                v14.setCode(ResultCode.FAIL);
                v14.setMessage("根据来源单据编号未查到入库通知单");
                return v14;
            }
            List<String> billNoList = sgBPhyInNoticesList.stream().filter(x -> x.getBillNo() != null).map(SgBPhyInNotices::getBillNo).collect(Collectors.toList());
            map.put("billNoList", billNoList);
            // 通过入库通知单单号查询是否都存在单据状态=未审核，作废状态=未作废的入库单记录
            List<SgBInPickorderNoticeItem> pickorderNoticeItems = sgBInPickorderNoticeItemMapper.selectList(new QueryWrapper<SgBInPickorderNoticeItem>().lambda().in(SgBInPickorderNoticeItem::getNoticeBillNo, billNoList));
            Set<Long> idSet = pickorderNoticeItems.stream().map(SgBInPickorderNoticeItem::getSgBInPickorderId).collect(Collectors.toSet());

            // 根据主表id查 主表记录 判断 单据状态和作废状态
            List<SgBInPickorder> pickOrders = sgBInPickorderMapper.selectList(new QueryWrapper<SgBInPickorder>().lambda().in(SgBInPickorder::getId, idSet));
            // 检查拣货单状态
            this.checkPickOrderStatus(pickOrders);

//            Map<Long, SgBInPickorder> pickOrderMap = pickOrders.stream().collect(Collectors.toMap(SgBInPickorder::getId, x -> x));
            List<String> noticeNoForPick = new ArrayList<>();
            //遍历取参
            pickorderNoticeItems.forEach(pick -> {
//                SgBInPickorder sgBInPickorder = pickOrderMap.get(pick.getSgBInPickorderId());
//                if (SgOutConstantsIF.PICK_ORDER_STATUS_WAIT == sgBInPickorder.getBillStatus()
//                        && SgConstants.IS_ACTIVE_Y.equals(sgBInPickorder.getIsactive())) {
//                }
                noticeNoForPick.add(pick.getNoticeBillNo());
            });
            if (CollectionUtils.isNotEmpty(noticeNoForPick)) {
                try {
                    // 调用作废拣货单服务
                    SgPhyInPickOrderVoidRequest sgPhyInPickOrderVoidRequest = new SgPhyInPickOrderVoidRequest();
                    sgPhyInPickOrderVoidRequest.setIds(new ArrayList<>(idSet));
                    sgPhyInPickOrderVoidRequest.setUser(SystemUserResource.getRootUser());
                    sgPhyInPickOrderVoidService.voidSgbBInPickorder(sgPhyInPickOrderVoidRequest);

                    List<SgBPhyInNotices> NoticesListForPick = noticesMapper.selectList(new QueryWrapper<SgBPhyInNotices>().lambda().in(SgBPhyInNotices::getBillNo, noticeNoForPick));
                    List<Long> noticeIdsForPick = NoticesListForPick.stream().map(SgBPhyInNotices::getId).collect(Collectors.toList());

                    // 查询的入库通知单聚合后调用 ->新增入库拣货单服务
                    SgPhyInPickUpGoodSaveRequest pickUpGoodSaveRequest = new SgPhyInPickUpGoodSaveRequest();
                    pickUpGoodSaveRequest.setInNoticeIdList(noticeIdsForPick);
                    pickUpGoodSaveRequest.setLoginUser(SystemUserResource.getRootUser());
                    ValueHolderV14<Long> inPickUpGood = sgPhyInPickOrderGenerateService.generatePhyInPickUpGood(pickUpGoodSaveRequest);
                    Long pickId = inPickUpGood.getData();

                    // 传入参数“箱码、料号、数量、箱型、重量”写入装箱明细子表
                    SgPhyInPickOrderSaveRequest sgPhyInPickOrderSaveRequest = new SgPhyInPickOrderSaveRequest();
                    sgPhyInPickOrderSaveRequest.setSgPhyBInPickorderId(pickId);

                    // 封装箱明细信息
                    if (boxitem != null) {
                        List<SgPhyInPickorderTeusItemSaveRequest> saveRequests = convertTeusItem(boxitem, pickId);
                        sgPhyInPickOrderSaveRequest.setInPickorderTeusItemList(saveRequests);
                        sgPhyInPickOrderSaveRequest.setLoginUser(SystemUserResource.getRootUser());
                        sgPhyInPickOrderSaveService.saveSgbBInPickorderTeusItem(sgPhyInPickOrderSaveRequest);
                    }

                    // 参数“托运单号”写入入库单主表
                    SgBInPickorder sgBInPickorder = new SgBInPickorder();
                    sgBInPickorder.setId(pickId);
                    sgBInPickorder.setCheckNo(tyNo);
                    sgBInPickorderMapper.updateById(sgBInPickorder);
                } catch (Exception e) {
                    if (log.isDebugEnabled()) {
                        log.debug("Start SgPhyConsignPickService.consignPick.Exception:{};"
                                + e.getMessage());
                    }
                    v14.setMessage("托运拣货失败：" + e.getMessage());
                    v14.setCode(ResultCode.FAIL);
                    return v14;
                }

            } else {
                v14.setMessage("没有符合的入库通知单！");
                v14.setCode(ResultCode.FAIL);
                return v14;
            }

        }
        v14.setCode(ResultCode.SUCCESS);
        v14.setMessage("成功");
        v14.setData(map);
        return v14;
    }

    private void checkPickOrderStatus(List<SgBInPickorder> pickOrders) {

        AssertUtils.cannot(CollectionUtils.isEmpty(pickOrders), "不存在对应的申购入库单，请检查！");
        for (SgBInPickorder pickOrder : pickOrders) {
            Integer status = pickOrder.getStatus();
            AssertUtils.cannot(2 == status, "申购入库单[" + pickOrder.getBillNo() + "]已审核，不允许托运！");
            AssertUtils.cannot(3 == status || "N".equals(pickOrder.getIsactive()), "申购入库单[" + pickOrder.getBillNo() + "]已作废，不允许托运！");
        }
    }


    /**
     * 封装装箱明细request 补充明细信息
     *
     * @param boxitem
     * @param id
     * @return
     */
    private List<SgPhyInPickorderTeusItemSaveRequest> convertTeusItem(JSONArray boxitem, Long id) {
        List<SgPhyPickTeusItemRequest> teusItemRequestList = new ArrayList<>();
        for (Object o : boxitem) {
            JSONObject object = JSONObject.parseObject(JSON.toJSONString(o));
            SgPhyPickTeusItemRequest sgPhyPickTeusItemRequest = JsonUtils.jsonParseClass(object, SgPhyPickTeusItemRequest.class);
            teusItemRequestList.add(sgPhyPickTeusItemRequest);
        }

        // 补充装箱明细信息
        List<SgPhyInPickorderTeusItemSaveRequest> saveRequests = new ArrayList<>();
        // 把传过来的料号过滤出来
        List<String> skuEcode = teusItemRequestList.stream()
                .map(SgPhyPickTeusItemRequest::getEname).collect(Collectors.toList());
        BasicPsQueryService queryService = ApplicationContextHandle.getBean(BasicPsQueryService.class);
        HashMap<String, PsCProSkuResult> queryResult = new HashMap<>(16);
        SkuInfoQueryRequest request = new SkuInfoQueryRequest();
        request.setSkuEcodeList(skuEcode);
        try {
            queryResult = queryService.getSkuInfoByEcode(request);
        } catch (Exception e) {
            AssertUtils.logAndThrow("商品信息获取失败！" + e.getMessage());
        }
        for (SgPhyPickTeusItemRequest teusItemRequest : teusItemRequestList) {
            SgPhyInPickorderTeusItemSaveRequest teusItemSaveRequest = new SgPhyInPickorderTeusItemSaveRequest();
            teusItemSaveRequest.setIsTeus(SgUnpackConstants.IS_TEUS_N);
            teusItemSaveRequest.setPickInTeusId(teusItemRequest.getBoxCode());
            BigDecimal weight = new BigDecimal(StringUtils.isEmpty(teusItemRequest.getWeight().trim()) ? "0" : teusItemRequest.getWeight().trim());
            teusItemSaveRequest.setWeight(weight);
            teusItemSaveRequest.setQtyTeus(new BigDecimal(StringUtils.isEmpty(teusItemRequest.getQty().trim()) ? "0" : teusItemRequest.getQty().trim()));

            PsCProSkuResult skuResult = queryResult.get(teusItemRequest.getEname());
            AssertUtils.notNull(skuResult, "根据料号[" + teusItemRequest.getEname() + "]查询商品信息为空！");

            teusItemSaveRequest.setPsCSpec1Id(skuResult.getPsCSpec1objId());
            teusItemSaveRequest.setPsCSpec1Ecode(skuResult.getClrsEcode());
            teusItemSaveRequest.setPsCSpec1Ename(skuResult.getClrsEname());
            teusItemSaveRequest.setPsCSpec2Id(skuResult.getPsCSpec2objId());
            teusItemSaveRequest.setPsCSpec2Ename(skuResult.getSizesEname());
            teusItemSaveRequest.setPsCSpec2Ecode(skuResult.getClrsEcode());
            teusItemSaveRequest.setPsCProId(skuResult.getPsCProId());
            teusItemSaveRequest.setPsCProEcode(skuResult.getPsCProEcode());
            teusItemSaveRequest.setPsCProEname(skuResult.getPsCProEname());
            teusItemSaveRequest.setPsCSkuId(skuResult.getId());
            teusItemSaveRequest.setPsCSkuEcode(skuResult.getSkuEcode());
            teusItemSaveRequest.setSgBInPickorderId(id);
            saveRequests.add(teusItemSaveRequest);
        }
        return saveRequests;
    }

    /**
     * 单据编号 jsonarray 转 list
     *
     * @param sourceitem
     * @return
     */
    private List<String> billNotoList(JSONArray sourceitem) {
        List<String> billNoList = new ArrayList<>();
        for (Object o : sourceitem) {
            JSONObject object = JSONObject.parseObject(JSON.toJSONString(o));
            //update by 4.09 2020 修改为备货单号
            String billNo = object.getString("BH_NO");
            billNoList.add(billNo);
        }

        return billNoList;
    }
}

