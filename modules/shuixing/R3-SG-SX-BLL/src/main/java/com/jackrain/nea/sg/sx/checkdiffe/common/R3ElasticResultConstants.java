package com.jackrain.nea.sg.sx.checkdiffe.common;

import lombok.Data;

@Data
public class R3ElasticResultConstants {
    public static String KEY_DATA = "data";
    public static String KEY_CODE = "code";
    public static String KEY_MESSAGE = "message";
    public static String KEY_ID = "id";
    public static String CLOUMN_IS_ACTIVE = "isactive";
    public static String VALUE_Y = "Y";
    public static String VALUE_N = "N";
}
