package com.jackrain.nea.sg.sx.baisc.services;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.pagehelper.PageHelper;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.cpext.model.table.CpCStore;
import com.jackrain.nea.oc.sale.api.OcTransLogCmd;
import com.jackrain.nea.oc.sale.model.table.OcBTransLog;
import com.jackrain.nea.sg.basic.mapper.SgBStorageMapper;
import com.jackrain.nea.sg.basic.model.table.SgBStorage;
import com.jackrain.nea.sx.api.CpSxStoreQueryCmd;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.utils.BillType;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.UUID;

/**
 * @author dean
 * @date 2020/3/31
 * sync storage service
 */
@Slf4j
@Component
public class SgStorageSyncService {

    @Autowired
    private SgBStorageMapper sgBStorageMapper;

    public void execute() {
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        int pageSize = 50;
        BillType storeStorageSync = BillType.STORE_STORAGE_SYNC;

        CpSxStoreQueryCmd cpSxStoreQueryCmd = (CpSxStoreQueryCmd)
                ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                        CpSxStoreQueryCmd.class.getName(), "cp-sx", "1.0");

        OcTransLogCmd ocTransLogCmd = (OcTransLogCmd)
                ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                        OcTransLogCmd.class.getName(), "oc", "1.0");

        for (int pageNum = 1; ; pageNum++) {
            List<Long> storeIds = cpSxStoreQueryCmd.findCpCStoreViaManageType("01", pageNum, pageSize);
            int storeIdSize = CollectionUtils.size(storeIds);
            if (storeIdSize == 0) {
                break;
            }
            for (Long storeId : storeIds) {
                CpCStore store = cpSxStoreQueryCmd.queryCpCStoreById(storeId);
                LambdaQueryWrapper<SgBStorage> lq = new QueryWrapper<SgBStorage>().lambda()
                        .eq(SgBStorage::getCpCStoreId, storeId);

                if (StringUtils.isBlank(store.getErpStore())) {
                    log.error(this.getClass().getName()+".execute.store:erp store is null:"+ JSON.toJSONString(store));
                    continue;
                }

                Page<SgBStorage> pagei = new Page<>(1, pageSize);
                pagei.setAsc("id");
                IPage<SgBStorage> pageResult = sgBStorageMapper.selectPage(pagei, lq);
                if (pageResult.getTotal() <= 0) {
                    continue;
                }
                callErpApi(sf, storeStorageSync, ocTransLogCmd, store, pageResult);
                if (pageResult.getTotal() <= pageSize) {
                    continue;
                }

                for (int i = 2; i<= pageResult.getPages(); i++) {
                    pagei.setCurrent(i);
                    pageResult = sgBStorageMapper.selectPage(pagei, lq);
                    callErpApi(sf, storeStorageSync, ocTransLogCmd, store, pageResult);
                }
            }
            if (storeIdSize < pageSize) {
                break;
            }
        }
    }

    private void callErpApi(SimpleDateFormat sf, BillType storeStorageSync, OcTransLogCmd ocTransLogCmd, CpCStore store, IPage<SgBStorage> pageResult) {
        List<SgBStorage> resultList = pageResult.getRecords();
        if (CollectionUtils.isEmpty(resultList)) {
            return;
        }
        JSONArray detailList = new JSONArray();
        JSONObject mianJsonObject = new JSONObject();
        for (SgBStorage sbs : resultList) {
            JSONObject itemJsonObject = new JSONObject();
            itemJsonObject.put("ERP_STORE", store.getErpStore());
            itemJsonObject.put("PS_C_PRO_ECODE", sbs.getPsCProEcode());
            itemJsonObject.put("QTY_STORAGE", sbs.getQtyStorage());
            itemJsonObject.put("QTY_PREIN", sbs.getQtyPrein());
            itemJsonObject.put("MODIFIEDDATE", sf.format(sbs.getModifieddate()));
            detailList.add(itemJsonObject);
        }
        mianJsonObject.put("detailList", detailList);
        JSONObject origBillContent = new JSONObject();
        origBillContent.put("data", mianJsonObject);
        JSONObject service = new JSONObject();
        service.put("name", storeStorageSync.getInteractionService());
        origBillContent.put("service", service);

        OcBTransLog transLog = new OcBTransLog();
        transLog.setOrigBillContent(origBillContent.toJSONString());
        transLog.setSourceBillNo(UUID.randomUUID().toString());
        transLog.setBillType(storeStorageSync.getType());

        ValueHolderV14<OcBTransLog> invoke = ocTransLogCmd.invoke(transLog);
        if ( !invoke.isOK() ) {
            log.error("ocTransLogCmd.invoke.error:"+ JSON.toJSONString(invoke));
        }
    }

}
