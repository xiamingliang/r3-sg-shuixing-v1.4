package com.jackrain.nea.sg.sx.in.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Lists;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.in.api.SgBInPickorderVoidCmd;
import com.jackrain.nea.sg.in.mapper.SgBInPickorderMapper;
import com.jackrain.nea.sg.in.mapper.SgBInPickorderNoticeItemMapper;
import com.jackrain.nea.sg.in.model.table.SgBInPickorder;
import com.jackrain.nea.sg.in.model.table.SgBInPickorderNoticeItem;
import com.jackrain.nea.sg.sx.in.model.request.SgPhyInPickOrderVoidRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.query.DefaultWebEvent;
import com.jackrain.nea.web.query.QuerySessionImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author zhu lin yu
 * @since 2019-11-18
 * create at : 2019-11-18 19:05
 */

@Slf4j
@Component
public class SgPhyInPickOrderVoidService {


    /**
     * 作废入库拣货单
     */
    public ValueHolderV14<Long> voidSgbBInPickorder(SgPhyInPickOrderVoidRequest request) {
        ValueHolderV14 holderV14 = new ValueHolderV14<>();

        List<Long> ids = request.getIds();

        //参数检验
        if (CollectionUtils.isEmpty(ids)) {
            List<String> sourceBillNoList = request.getSourceBillNoList();
            AssertUtils.isTrue(CollectionUtils.isNotEmpty(sourceBillNoList), "来源入库通知单来源单据编号为空！");
            SgBInPickorderNoticeItemMapper noticeItemMapper = ApplicationContextHandle.getBean(SgBInPickorderNoticeItemMapper.class);
            List<SgBInPickorderNoticeItem> sgBInPickorderNoticeItems = noticeItemMapper.selectList(new QueryWrapper<SgBInPickorderNoticeItem>().lambda()
                    .in(SgBInPickorderNoticeItem::getSourceBillNo, sourceBillNoList));
            Set<Long> setIds = sgBInPickorderNoticeItems.stream().map(SgBInPickorderNoticeItem::getSgBInPickorderId).collect(Collectors.toSet());
            ids = new ArrayList<>(setIds);

            if(CollectionUtils.isNotEmpty(ids)){
                SgBInPickorderMapper inPickOrderMapper = ApplicationContextHandle.getBean(SgBInPickorderMapper.class);
                List<SgBInPickorder> sgBInPickorders = inPickOrderMapper.selectIdsByStatus(StringUtils.join(ids, ","));
                if(CollectionUtils.isNotEmpty(sgBInPickorders)){
                    ids=Lists.transform(sgBInPickorders, o ->o.getId());
                }else{
                    ids=Lists.newArrayList();
                }
            }else{
                ids=Lists.newArrayList();
            }

        }
        /**
         * 调用标服作废拣货单
         */
        QuerySessionImpl querySession = new QuerySessionImpl(request.getUser());
        DefaultWebEvent event = new DefaultWebEvent("voidSgBInPickorder", new HashMap(6));
        JSONObject object = new JSONObject();
        object.put("objids", ids);
        event.put("param", object);
        querySession.setEvent(event);
        SgBInPickorderVoidCmd pickorderVoidCmd = (SgBInPickorderVoidCmd)
                ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                        SgBInPickorderVoidCmd.class.getName(), "sg", "1.0");
        ValueHolder execute = pickorderVoidCmd.execute(querySession);
        HashMap jo = execute.getData();
        holderV14.setMessage(jo.get("message").toString());
        holderV14.setCode(execute.isOK() ? ResultCode.SUCCESS : ResultCode.FAIL);
        return holderV14;
    }





}
