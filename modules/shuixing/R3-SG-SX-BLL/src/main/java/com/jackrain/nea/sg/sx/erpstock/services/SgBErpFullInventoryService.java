package com.jackrain.nea.sg.sx.erpstock.services;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.cpext.model.request.CpCPhyWarehouseUpdateCmdRequest;
import com.jackrain.nea.model.util.ModelUtil;
import com.jackrain.nea.sg.sx.erpstock.services.request.SgBErpFullInventoryRequest;
import com.jackrain.nea.sg.sx.erpstock.services.table.SgBErpFullInventory;
import com.jackrain.nea.sg.sx.mapper.SgBErpFullInventoryMapper;
import com.jackrain.nea.sx.model.table.Users;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.assertj.core.util.Lists;
import org.jsoup.helper.DataUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Component
public class SgBErpFullInventoryService {

    @Autowired
    private SgBErpFullInventoryMapper sgBErpFullInventoryMapper;

    public ValueHolderV14 SyncSgErpFullInventory(List<SgBErpFullInventory> request) {
        ValueHolderV14 valueHolderV14 = new ValueHolderV14();
        if ( CollectionUtils.isEmpty(request)) {
            valueHolderV14.setCode(ResultCode.FAIL);
            valueHolderV14.setMessage("参数不能为空");
            return valueHolderV14;
        }
        try {
            List<SgBErpFullInventory> addList= Lists.newArrayList();
            List<SgBErpFullInventory>  inventorylist=request;
            for (Iterator iterator = inventorylist.iterator(); iterator.hasNext();) {
                SgBErpFullInventory sgBErpFullInventory = (SgBErpFullInventory) iterator.next();
                QueryWrapper<SgBErpFullInventory>  queryWrapper=new QueryWrapper<SgBErpFullInventory>();
                queryWrapper.eq("materials_no",sgBErpFullInventory.getMaterialsNo());
                queryWrapper.eq("warehouse_no",sgBErpFullInventory.getWarehouseNo());
                queryWrapper.eq("bin_location",sgBErpFullInventory.getBinLocation());
                queryWrapper.eq("batch_number",sgBErpFullInventory.getBinLocation());
                queryWrapper.eq("belongs_legal",sgBErpFullInventory.getBelongsLegal());
                SgBErpFullInventory newSgFullInv= sgBErpFullInventoryMapper.selectOne(queryWrapper);
                if(StringUtils.isEmpty(newSgFullInv)){
                    sgBErpFullInventory.setId(ModelUtil.getSequence("sg_b_erp_full_inventory"));
                    sgBErpFullInventory.setCreateddate(new Timestamp(System.currentTimeMillis()));
                    sgBErpFullInventory.setModifieddate(new Timestamp(System.currentTimeMillis()));
                    addList.add(sgBErpFullInventory);
                }else{
                    sgBErpFullInventory.setId(newSgFullInv.getId());
                    sgBErpFullInventory.setModifieddate(new Timestamp(System.currentTimeMillis()));
                    sgBErpFullInventoryMapper.updateById(sgBErpFullInventory);
                }
            }

            if(CollectionUtils.isNotEmpty(addList)){
                sgBErpFullInventoryMapper.batchInsert(addList);
            }
        } catch (RuntimeException e) {
            valueHolderV14.setCode(ResultCode.SUCCESS);
            valueHolderV14.setMessage("数据同步错误"+e.getMessage());
            //e.printStackTrace();
        }

        return valueHolderV14;
    }


    public  ValueHolderV14<List<List<SgBErpFullInventory>>> getErpStockInfo(JSONArray array){
        ValueHolderV14<List<List<SgBErpFullInventory>>>  valueHolderV14 = new ValueHolderV14<List<List<SgBErpFullInventory>>>();
        List<SgBErpFullInventory>  paramList=JSONArray.parseArray(array.toJSONString(),SgBErpFullInventory.class);
        if ( CollectionUtils.isEmpty(paramList)) {
            valueHolderV14.setCode(ResultCode.FAIL);
            valueHolderV14.setMessage("参数不能为空");
            return valueHolderV14;
        }
        List<List<SgBErpFullInventory>>  returnList = new ArrayList<>();
        for (Iterator iterator2 = paramList.iterator(); iterator2.hasNext();) {
            SgBErpFullInventory Inventory = (SgBErpFullInventory) iterator2.next();

            QueryWrapper<SgBErpFullInventory>  queryWrapper=new QueryWrapper<SgBErpFullInventory>();
            if(!StringUtils.isEmpty(Inventory.getMaterialsNo())){
                queryWrapper.eq("materials_no",Inventory.getMaterialsNo());
            }

            if(!StringUtils.isEmpty(Inventory.getWarehouseNo())){
                queryWrapper.eq("warehouse_no",Inventory.getWarehouseNo());
            }

            if(!StringUtils.isEmpty(Inventory.getBelongsLegal())){
                queryWrapper.eq("belongs_legal",Inventory.getBelongsLegal());
            }

            List<SgBErpFullInventory> sgList=sgBErpFullInventoryMapper.selectList(queryWrapper);

            returnList.add(sgList);

        }

        valueHolderV14.setData(returnList);

        return valueHolderV14;
    }


    public   ValueHolderV14<Long>  getErpStockInfoByQty(JSONObject json){
        ValueHolderV14<Long>  valueHolderV14 = new ValueHolderV14<Long>();
        if (json.isEmpty()) {
            valueHolderV14.setCode(ResultCode.FAIL);
            valueHolderV14.setMessage("参数不能为空");
            return valueHolderV14;
        }

        String materials_no="";
        if(!json.containsKey("materials_no")  && !StringUtils.isEmpty(json.get("materials_no")) ){
            return valueHolderV14;
        }else{
            materials_no=json.get("materials_no").toString();
        }

        String warehouse_no="";
        if(!json.containsKey("warehouse_no") &&  !StringUtils.isEmpty(json.get("warehouse_no"))){
            return valueHolderV14;
        }else{
            warehouse_no=json.get("warehouse_no").toString();
        }

        String belongs_legal="";
        if(!json.containsKey("belongs_legal") &&  !StringUtils.isEmpty(json.get("belongs_legal"))){
            return valueHolderV14;
        }else{
            belongs_legal=json.get("belongs_legal").toString();
        }


        Long qty=sgBErpFullInventoryMapper.getInventoyByErpQty(materials_no,warehouse_no,belongs_legal);

         valueHolderV14.setData(qty);

         return valueHolderV14;

    }





    public   JSONArray  getSgFullInventoryQtyByTb(JSONObject json){
        try {
            String warehouse="";
            Date start_date=new Date();
            if(json.containsKey("warehouse")){

                    warehouse=json.getString("warehouse");
                   JSONArray array= JSONArray.parseArray(warehouse);
                    StringBuffer sb = new StringBuffer();
                    for (int i = 0; i < array.size(); i++) {
                        sb.append("'").append(array.get(i)).append("'").append(",");
                    }
                    warehouse=sb.toString().substring(0, sb.length() - 1);

            }

            if(json.containsKey("start_date")){
                start_date=json.getDate("start_date");
            }
            List<Map<String,Object>>  tlist=  sgBErpFullInventoryMapper.getStoreQtyByTb(warehouse,start_date);

            return  JSONArray.parseArray(JSON.toJSONString(tlist));
        } catch (Exception e) {

            return new JSONArray();
            // e.printStackTrace();
        }

    }







}
