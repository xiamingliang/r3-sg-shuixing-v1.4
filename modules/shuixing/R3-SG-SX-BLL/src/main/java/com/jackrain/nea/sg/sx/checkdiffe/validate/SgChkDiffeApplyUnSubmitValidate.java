package com.jackrain.nea.sg.sx.checkdiffe.validate;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.sg.sx.checkdiffe.common.SgChkDiffeConstants;
import com.jackrain.nea.tableService.MainTableRecord;
import com.jackrain.nea.tableService.TableServiceContext;
import com.jackrain.nea.tableService.validate.BaseValidator;
import com.jackrain.nea.utils.AssertUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author chenxinxing
 * @since 2020/2/25 15:35
 */

@Slf4j
@Component
public class SgChkDiffeApplyUnSubmitValidate extends BaseValidator {
    @Override
    public void validate(TableServiceContext context, MainTableRecord mainTableRecord) {
        JSONObject origData = mainTableRecord.getOrignalData();
        Integer status = origData.getInteger("STATUS");
        AssertUtils.cannot(status == SgChkDiffeConstants.SGCHKDIFFE_STATUS_ERP_RETUEN, "单据ERP回传，不允许取消审核！", context.getLocale());
    }
}
