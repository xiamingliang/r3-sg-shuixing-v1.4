package com.jackrain.nea.sg.sx.in.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.model.util.AdParamUtil;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.in.mapper.SgBInPickorderMapper;
import com.jackrain.nea.sg.in.mapper.SgBInPickorderNoticeItemMapper;
import com.jackrain.nea.sg.in.mapper.SgBPhyInNoticesMapper;
import com.jackrain.nea.sg.in.model.request.SgPhyInPickOrderAuditRequest;
import com.jackrain.nea.sg.in.model.request.SgPhyInPickUpGoodSaveRequest;
import com.jackrain.nea.sg.in.model.table.SgBInPickorder;
import com.jackrain.nea.sg.in.model.table.SgBInPickorderNoticeItem;
import com.jackrain.nea.sg.in.model.table.SgBPhyInNotices;
import com.jackrain.nea.sg.in.services.SgPhyInPickOrderAuditService;
import com.jackrain.nea.sg.in.services.SgPhyInPickOrderGenerateService;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author zhu lin yu
 * @since 2019-11-20
 * create at : 2019-11-20 16:53
 */
@Slf4j
@Component
public class SgPhyInOrderAuditService {

    /**
     * 入库拣货单审核
     */
    @Transactional(rollbackFor = Exception.class)
    public ValueHolderV14 auditInOrder(SgPhyInPickOrderAuditRequest request) {
        ValueHolderV14 holderV14;

        if (log.isDebugEnabled()) {
            log.debug("Start SgPhyInOrderAuditService.auditInOrder. ReceiveParams:params:{};"
                    + JSONObject.toJSONString(request));
        }

        //参数检验
        if (request == null) {
            throw new NDSException("request is null");
        }

        Long pickOrderId = request.getInPickOrderId();
        AssertUtils.notNull(pickOrderId, "拣货单ID不能为空！");

        // 调用【入库拣货单审核服务】
        SgBInPickorderMapper pickOrderMapper = ApplicationContextHandle.getBean(SgBInPickorderMapper.class);
        SgPhyInPickOrderAuditService auditService = ApplicationContextHandle.getBean(SgPhyInPickOrderAuditService.class);

        // 查询拣货单
        SgBInPickorder pickOrder = pickOrderMapper.selectById(pickOrderId);
        AssertUtils.notNull(pickOrder, "当前拣货单已不存在！" + pickOrderId);

        holderV14 = auditService.auditInPickOrder(request, pickOrder);
        if (log.isDebugEnabled()) {
            log.debug("Start SgPhyInOrderAuditService.auditInOrder.Audit:params:{};"
                    + JSONObject.toJSONString(holderV14));
        }
        if (holderV14.isOK()) {
            // 判断本单的来源入库通知单是否存在差异数量
            Long inPickOrderId = request.getInPickOrderId();
            SgBInPickorderNoticeItemMapper noticeItemMapper = ApplicationContextHandle.getBean(SgBInPickorderNoticeItemMapper.class);
            List<SgBInPickorderNoticeItem> noticeItems = noticeItemMapper.selectList(new QueryWrapper<SgBInPickorderNoticeItem>().lambda()
                    .eq(SgBInPickorderNoticeItem::getSgBInPickorderId, inPickOrderId));

            // 获取来源单据ID，查询入库通知单
            List<Long> sourceBillIds = noticeItems.stream().map(SgBInPickorderNoticeItem::getSourceBillId).collect(Collectors.toList());
            SgBPhyInNoticesMapper inNoticesMapper = ApplicationContextHandle.getBean(SgBPhyInNoticesMapper.class);

            // 2020-04-14填坑，查询入库通知单不带来源单据类型，导致查到多条通知单，此处要加上来源单据类型的条件
            List<SgBPhyInNotices> noticesList = inNoticesMapper.selectList(new QueryWrapper<SgBPhyInNotices>().lambda()
                    .eq(SgBPhyInNotices::getSourceBillType, noticeItems.get(0).getSourceBillType())
                    .in(SgBPhyInNotices::getSourceBillId, sourceBillIds));

            List<Long> inNoticesIds = new ArrayList<>();
            for (SgBPhyInNotices sgBPhyInNotices : noticesList) {
                if (sgBPhyInNotices.getTotQtyDiff().compareTo(BigDecimal.ZERO) > 0) {
                    inNoticesIds.add(sgBPhyInNotices.getId());
                }
            }
            // 如果存在，当【是否自动生成拣货单】=是时，将存在差异的入库通知单汇集调用【生成入库拣货单服务】
            String sysParam = AdParamUtil.getParam("sg.pickorder.enable");
            Boolean flag = Boolean.parseBoolean(sysParam);
            if (flag && CollectionUtils.isNotEmpty(inNoticesIds)) {

                SgPhyInPickOrderGenerateService service = ApplicationContextHandle.getBean(SgPhyInPickOrderGenerateService.class);
                SgPhyInPickUpGoodSaveRequest pickUpGoodSaveRequest = new SgPhyInPickUpGoodSaveRequest();
                pickUpGoodSaveRequest.setInNoticeIdList(inNoticesIds);
                pickUpGoodSaveRequest.setLoginUser(request.getLoginUser());
                // 2020-04-15添加入参，入库序号
                pickUpGoodSaveRequest.setInSeqNo(pickOrder.getInSeqNo());
                ValueHolderV14<Long> longValueHolderV14 = service.generatePhyInPickUpGood(pickUpGoodSaveRequest);

                String checkNo = pickOrder.getCheckNo();
                if (checkNo != null) {
                    Long newPickId = longValueHolderV14.getData();
                    SgBInPickorder updatePickOrder = new SgBInPickorder();
                    updatePickOrder.setId(newPickId);
                    updatePickOrder.setCheckNo(checkNo);
                    pickOrderMapper.updateById(updatePickOrder);
                }

            } else {
                SgBInPickorder sgBInPickorder = new SgBInPickorder();
                sgBInPickorder.setId(request.getInPickOrderId());
                sgBInPickorder.setIsAllRecev("Y");
                pickOrderMapper.updateById(sgBInPickorder);
            }

            /**
             * 调用采购回传
             */
//            SgBInPickOrderTrans2MidSerivce orderTrans2MidSerivce = ApplicationContextHandle.getBean(SgBInPickOrderTrans2MidSerivce.class);
////            ValueHolderV14 v14 = orderTrans2MidSerivce.pickOrderTrans2MidService(request.getInPickOrderId(), request.getLoginUser());
        } else {
            return holderV14;
        }

        holderV14.setCode(ResultCode.SUCCESS);
        holderV14.setMessage("审核成功!");
        return holderV14;
    }

}
