package com.jackrain.nea.sg.sx.mapper;

import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.in.model.table.SgBInPickorder;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author: xiWen.z
 * create at: 2019/12/24 0024
 */
@Mapper
public interface SgSxInPickOrderMapper extends ExtentionMapper<SgBInPickorder> {

    /**
     * 拣货单传Erp中间表
     *
     * @param size 大小
     * @return
     */
    @Select("SELECT * FROM SG_B_IN_PICKORDER WHERE BILL_STATUS=2 AND TRANS_STATUS !=2 AND isactive='Y' AND "
            + "FAIL_NUM < 5 AND stock_bill_no is not null ORDER BY STATUS_TIME DESC LIMIT #{size} OFFSET 0;")
    List<SgBInPickorder> listQueryInPickOrder(@Param("size") int size);

}
