package com.jackrain.nea.sg.sx.processor;

import com.alibaba.fastjson.JSONObject;
import com.aliyun.openservices.ons.api.Action;
import com.aliyun.openservices.ons.api.ConsumeContext;
import com.aliyun.openservices.ons.api.Message;
import com.aliyun.openservices.ons.api.MessageListener;
import com.aliyun.openservices.ons.api.impl.util.MsgConvertUtil;
import com.jackrain.nea.sg.sx.in.api.SgBInPickOrderUpdateByErpCallBackCmd;
import com.jackrain.nea.sg.sx.in.model.consts.SxBillConst;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 * 采购入库回传, MQ 回执
 *
 * @author: xiWen.z
 * create at: 2019/12/19 0019
 */
@Slf4j
@Component
public class SgBInPickOrderTrans2ERPProcessor implements MessageListener {

    @Autowired
    private SgBInPickOrderUpdateByErpCallBackCmd sgBInPickOrderUpdateByErpCallBackCmdImpl;

    @Override
    public Action consume(Message message, ConsumeContext context) {
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + "Sar 接收采购回执mq消息 messageBody {},messageKey {}, messageTopic {}",
                    message.getBody(), message.getKey(), message.getTopic());
        }
        String msgBody = null;
        try {
            msgBody = MsgConvertUtil.objectDeserialize(message.getBody()).toString();
            JSONObject result = checkMsg(msgBody);
            if (Objects.isNull(result)) {
                return Action.CommitMessage;
            }

            JSONObject erpDataJsn = result.getJSONObject(SxBillConst.ERP_CALLBACK_CONTENT_KEY);
            String billNo = erpDataJsn.getString(SxBillConst.ERP_BILL_NO);

            JSONObject param = new JSONObject();
            param.put(SxBillConst.ERP_CALLBACK_ID, result.getLong(SxBillConst.ERP_CALLBACK_ID));
            param.put(SxBillConst.ERP_BILL_NO, billNo);
            ValueHolderV14 vh = sgBInPickOrderUpdateByErpCallBackCmdImpl.modifyInPickOrderByErpCallBack(param);

            if (vh.isOK()) {
                if (log.isDebugEnabled()) {
                    log.debug(this.getClass().getName() + ", 更新成功, msgKey " + message.getKey());
                }
                return Action.CommitMessage;
            } else {
                if (log.isDebugEnabled()) {
                    log.debug(this.getClass().getName() + ", 更新失败, msgKey " + message.getKey());
                }
                return Action.ReconsumeLater;
            }

        } catch (Exception e) {
            log.error(this.getClass().getName() + "接收采购回执mq消息异常, ExpMsg " + e.getMessage());
            return Action.CommitMessage;
        }
    }

    /**
     * validate
     *
     * @param msgBody 消息体
     * @return jsn
     */
    private JSONObject checkMsg(String msgBody) {

        if (msgBody == null) {
            log.error(this.getClass().getName() + "序列化msgBody结果为Null");
        }
        JSONObject result = JSONObject.parseObject(msgBody);
        if (result == null) {
            log.error(this.getClass().getName() + "messageBody解析时为Null");
            return null;
        }

        if (!result.getBooleanValue(SxBillConst.ERP_CODE_SUC_KEY)) {
            JSONObject erpDataJsn = result.getJSONObject(SxBillConst.ERP_CALLBACK_CONTENT_KEY);
            if (erpDataJsn != null) {
                String erpMsg = erpDataJsn.getString(SxBillConst.ERP_CALLBACK_MSG_KEY);
                log.error(this.getClass().getName() + "ERP中间表回执信息 " + erpMsg);
            }
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + "ERP回传失败信息");
            }
            return null;
        }
        return result;
    }

}
