package com.jackrain.nea.sg.sx.in.services;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.Feature;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.burgeon.r3.constant.ApiConstants;
import com.burgeon.r3.constant.ApiRequestJsonNameConstants;
import com.burgeon.r3.constant.MessageTypeConstants;
import com.burgeon.r3.enums.ErpApiResultEnum;
import com.burgeon.r3.util.ErpUtil;
import com.burgeon.r3.util.FreeMarkerUtil;
import com.google.common.collect.Lists;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.cp.result.CpCStore;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.model.util.AdParamUtil;
import com.jackrain.nea.oc.basic.cache.CommonCacheValUtils;
import com.jackrain.nea.oc.basic.common.OcBasicConstants;
import com.jackrain.nea.oc.basic.config.OcBoxConfig;
import com.jackrain.nea.redis.config.CusRedisTemplate;
import com.jackrain.nea.redis.util.RedisOpsUtil;
import com.jackrain.nea.sg.basic.common.R3ParamConstants;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.mapper.CpCPhyWarehouseMapper;
import com.jackrain.nea.sg.basic.model.result.SgR3BaseResult;
import com.jackrain.nea.sg.basic.model.table.CpCPhyWarehouse;
import com.jackrain.nea.sg.basic.utils.StorageUtils;
import com.jackrain.nea.sg.in.api.SgPhyInNoticeVoidCmd;
import com.jackrain.nea.sg.in.mapper.SgBInPickorderMapper;
import com.jackrain.nea.sg.in.model.request.SgPhyInNoticesVoidReqeust;
import com.jackrain.nea.sg.in.model.table.SgBInPickorder;
import com.jackrain.nea.sg.out.api.SgPhyOutNoticesCancelCmd;
import com.jackrain.nea.sg.out.api.SgPhyOutNoticesQueryCmd;
import com.jackrain.nea.sg.out.api.SgPhyOutResultSaveAndAuditCmd;
import com.jackrain.nea.sg.out.common.SgOutConstantsIF;
import com.jackrain.nea.sg.out.model.request.*;
import com.jackrain.nea.sg.out.model.result.SgBPhyOutNoticesAllInfoResult;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutNoticesItem;
import com.jackrain.nea.sg.receive.model.request.SgReceiveBillCleanRequest;
import com.jackrain.nea.sg.receive.services.SgReceiveCleanService;
import com.jackrain.nea.sg.send.utils.SgStoreESUtils;
import com.jackrain.nea.sg.send.utils.SgStoreUtils;
import com.jackrain.nea.sg.sx.in.api.SgPhyInPickOrderVoidCmd;
import com.jackrain.nea.sg.sx.in.model.request.SgPhyInPickOrderVoidRequest;
import com.jackrain.nea.sg.sx.rpc.OcRpcService;
import com.jackrain.nea.sg.transfer.common.ScTransferConstants;
import com.jackrain.nea.sg.transfer.common.SgTransferConstantsIF;
import com.jackrain.nea.sg.transfer.enums.SgTransferBillStatusEnum;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferImpItemMapper;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferItemMapper;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferMapper;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferTeusItemMapper;
import com.jackrain.nea.sg.transfer.model.table.ScBTransfer;
import com.jackrain.nea.sg.transfer.model.table.ScBTransferImpItem;
import com.jackrain.nea.sg.transfer.model.table.ScBTransferItem;
import com.jackrain.nea.sg.transfer.model.table.ScBTransferTeusItem;
import com.jackrain.nea.sg.transfer.services.SgTransferCancelAuditService;
import com.jackrain.nea.sg.transfer.services.SgTransferOutResultService;
import com.jackrain.nea.sg.transfer.services.SgTransferRpcService;
import com.jackrain.nea.sx.model.table.CpCCustomer;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.tableService.constants.Constants;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.utils.AssertUtils;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.query.DefaultWebEvent;
import com.jackrain.nea.web.query.QuerySession;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * @author zh
 * @date 2019/12/23
 * 取消出库
 */
@Slf4j
@Component
public class ScBTranferCancelOutService {
    @Autowired
    private SgStoreESUtils storeESUtils;

    @Autowired
    private CommonCacheValUtils cacheValUtils;

    @Reference(version = "1.0", group = "sg")
    private SgPhyInNoticeVoidCmd sgPhyInNoticeVoidCmd;

    @Reference(version = "1.0", group = "sg")
    SgPhyInPickOrderVoidCmd sgPhyInPickOrderVoidCmd;

    @Reference(group = "sg", version = "1.0")
    SgPhyOutNoticesQueryCmd sgPhyOutNoticesQueryCmd;


    @Autowired
    private OcBoxConfig boxConfig;


    public ValueHolder execute(QuerySession session) {

        User user = session.getUser();
        AssertUtils.notNull(user, "user is null,login first!");

        DefaultWebEvent event = session.getEvent();
        JSONObject param = JSON.parseObject(JSON.toJSONStringWithDateFormat(event.getParameterValue("param"),
                "yyyy-MM-dd HH:mm:ss", SerializerFeature.WriteMapNullValue), Feature.OrderedField);
        if (log.isDebugEnabled()) {
            log.info("ScBTransferLockService 参数:" + JSONObject.toJSONString(param));
        }
        Long id = param.getLong(R3ParamConstants.OBJID);
        return cancelOutTransfer(user, id);
    }

    @Transactional(rollbackFor = Exception.class)
    public ValueHolder cancelOutTransfer(User user, Long id) {
        Locale locale = user.getLocale();
        AssertUtils.notNull(id, "id不能为空", locale);
        CusRedisTemplate<String, String> redisTemplate = SgStoreUtils.initRedisTemplate();
        String lockKsy = ScTransferConstants.TRANSFER_INDEX + "cancelout" + ":" + id;
        if (redisTemplate.opsForValue().get(lockKsy) != null) {
            throw new NDSException(Resources.getMessage("当前记录正在被操作,请稍后重试!", user.getLocale()));
        } else {
            try {
                redisTemplate.opsForValue().set(lockKsy, "OK", 30, TimeUnit.MINUTES);
                ScBTransferMapper mapper = ApplicationContextHandle.getBean(ScBTransferMapper.class);
                ScBTransfer transfer = mapper.selectById(id);
                AssertUtils.notNull(transfer, "调拨单不存在", locale);

                // 检查单据状态
                statusCheck(transfer, locale);

                Long origStoreId = transfer.getCpCOrigId();
                Long destStoreId = transfer.getCpCDestId();
                AssertUtils.notNull(origStoreId, "数据异常-发货店仓id不存在", locale);
                AssertUtils.notNull(destStoreId, "数据异常-收货店仓id不存在", locale);

                // 发货经销商与收货经销商不一致时，则提示：“发货经销商与收货经销商不一致，不允许取消出库！”
                Long customerId = this.checkCustomer(locale, origStoreId, destStoreId);

                // 2020-04-08添加逻辑：调拨性质为“退货调拨”且收货经销商的“是否ERP组织”=是时，则调用“退货出库取消（erp提供接口）”
                if (SgTransferConstantsIF.TRANSFER_PROP_TH_STR.equals(transfer.getSgBTransferPropEname())) {
                    this.checkCustomerErp(user, transfer, customerId);
                }

                // 是否出库减库存
                String outReduce = transfer.getIsOutReduce();
                //是否入库加库存
                String inAdd = transfer.getIsInIncrease();
                // 自动入库
                String autoIn = transfer.getIsAutoIn();

                //当【是否自动生成入库拣货单】=是时，调用【作废入库拣货单】服务；
                String autoGenPick = AdParamUtil.getParam(ScTransferConstants.SYSTEM_AUTO_PICKNO);
                Boolean autoFlag = Boolean.valueOf(autoGenPick);

                if (log.isDebugEnabled()) {
                    log.info("ScBTranferCancelOutService autoFlag:" + autoFlag + ",outReduce:" + outReduce
                            + ",inAdd:" + inAdd + ",autoIn:" + autoIn);
                }

                if (ScTransferConstants.IS_AUTO_Y.equals(outReduce)) {
                    // 当【是否出库减库存】=是时
                    exeOutSubYAndInAddY(transfer, user, mapper, autoFlag, inAdd);

                } else if (ScTransferConstants.IS_AUTO_N.equals(outReduce) && ScTransferConstants.IS_AUTO_Y.equals(inAdd)
                        && ScTransferConstants.IS_AUTO_N.equals(autoIn)) {
                    exeOutSubNAndInAddYAndAutoInN(transfer, user, mapper, autoFlag);
                }

                // 更新退货申请单数量和状态
//                ScBTransfer transferNew = mapper.selectById(id);
//                executeAfterService(transferNew);
                //推送ES
                storeESUtils.pushESByTransfer(id, false, false, null, mapper, null);

                // 更新退货申请单作废状态
                String applyBillNo = transfer.getApplyBillNo();
                if (StringUtils.isNotEmpty(applyBillNo)) {
                    OcRpcService service = ApplicationContextHandle.getBean(OcRpcService.class);
                    ValueHolder valueHolder = service.updateRefundApplyStatus(user, applyBillNo);
                    if (log.isDebugEnabled()) {
                        log.debug(this.getClass().getName() + ",取消出库更新退货申请单出参:" + JSON.toJSONString(valueHolder));
                    }
                }


            } catch (Exception e) {
                if (log.isErrorEnabled()) {
                    log.error(this.getClass().getName() + ",取消出局异常:" + StorageUtils.getExceptionMsg(e));
                }
                AssertUtils.logAndThrow(StorageUtils.getExceptionMsg(e), locale);
            } finally {
                redisTemplate.delete(lockKsy);
            }
        }
        ValueHolder vh = new ValueHolder();
        vh.put("code", ResultCode.SUCCESS);
        vh.put("message", "取消出库成功!");
        return vh;
    }


    private void statusCheck(ScBTransfer transfer, Locale locale) {
        /**
         * a)	【单据状态】已作废，则提示：“当前记录已作废，不允许取消出库！”；
         * b)	【单据状态】未审核，则提示：“当前记录未审核，不允许取消出库！”；
         * c)	【单据状态】已审核未出库，则提示：“当前记录未出库，不允许取消出库！”；
         * d)	【单据状态】<>出库完成未入库，则提示：“当前记录已有入库记录，不允许取消出库！
         */
        AssertUtils.notNull(transfer, "当前单据已不存在!", locale);
        Integer billStatus = transfer.getStatus();
        AssertUtils.notNull(billStatus, "数据异常-STATUS不存在!", locale);
        AssertUtils.cannot(billStatus == SgTransferBillStatusEnum.VOIDED.getVal(), "当前记录已作废，不允许取消出库！", locale);
        AssertUtils.cannot(billStatus == SgTransferBillStatusEnum.UN_AUDITED.getVal(), "当前记录未审核，不允许取消出库！", locale);
        AssertUtils.cannot(billStatus == SgTransferBillStatusEnum.AUDITED_NOT_OUT.getVal(), "当前记录未出库，不允许取消出库！", locale);
        AssertUtils.cannot(billStatus != SgTransferBillStatusEnum.AUDITED_ALL_OUT_NOT_IN.getVal(), "当前记录已有入库记录，不允许取消出库！", locale);
    }


    /**
     * 获取实体仓信息
     * <p>
     * 单条
     *
     * @param storeId 店仓id
     */
    private CpCPhyWarehouse getWareHouseInfo(Long storeId) {
        CpCPhyWarehouseMapper warehouseMapper = ApplicationContextHandle.getBean(CpCPhyWarehouseMapper.class);
        return warehouseMapper.selectByStoreId(storeId);
    }

    /**
     * 根据调拨单查出库通知单
     *
     * @param tran
     * @return
     */
    private SgBPhyOutNoticesAllInfoResult getNoticesOut(ScBTransfer tran) {
        SgPhyOutNoticesQueryRequest request = new SgPhyOutNoticesQueryRequest();
        request.setSourceBillId(tran.getId());
        request.setSourceBillNo(tran.getBillNo());
        request.setSourceBillType(SgConstantsIF.BILL_TYPE_TRANSFER);
        //查询条码明细
        request.setQueryNoticesItem(true);
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getSimpleName() + "出库通知单查询入参=" + JSON.toJSONString(request));
        }
        ValueHolderV14<List<SgBPhyOutNoticesAllInfoResult>> holderV14 = sgPhyOutNoticesQueryCmd.queryOutNoticesAllInfoBySource(request);
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getSimpleName() + "出库通知单查询出参=" + JSON.toJSONString(holderV14));
        }
        if (holderV14.isOK()) {
            return holderV14.getData().get(0);
        }
        return null;
    }


    /**
     * 获取出库通知单的sku
     *
     * @param list
     * @param psCSkuEcode
     * @return
     */
    private Long getNoticeItemIdBySkuEcode(List<SgBPhyOutNoticesItem> list, String psCSkuEcode) {
        //skuEcode
        Long id = null;
        for (SgBPhyOutNoticesItem item : list) {
            if (item.getPsCSkuEcode().equalsIgnoreCase(psCSkuEcode)) {
                id = item.getId();
                break;
            }
        }
        return id;
    }


    /**
     * 新增并审核 出库库结果单服务
     *
     * @param transfer      调拨单
     * @param user          用户
     * @param origWarehouse 发货店仓
     * @param destWarehouse 收货店仓
     */

    public void invokeOutNoticeAndAudit(ScBTransfer transfer, User user, CpCPhyWarehouse origWarehouse, CpCPhyWarehouse destWarehouse) {


        Long objId = transfer.getId();
        // 条码明细
        ScBTransferItemMapper itemMapper = ApplicationContextHandle.getBean(ScBTransferItemMapper.class);
        // 录入明细
        ScBTransferImpItemMapper impItemMapper = ApplicationContextHandle.getBean(ScBTransferImpItemMapper.class);

        List<ScBTransferItem> itemList = itemMapper.selectList(new QueryWrapper<ScBTransferItem>().lambda()
                .eq(ScBTransferItem::getScBTransferId, transfer.getId()));

        // 录入明细和箱内明细只做一次查询
        List<ScBTransferImpItem> impItems = null;
        List<ScBTransferTeusItem> boxItems = null;
        // 箱功能开启 查询录入明细
        impItems = impItemMapper.selectList(
                new QueryWrapper<ScBTransferImpItem>().lambda().
                        eq(ScBTransferImpItem::getScBTransferId, objId));

        // 箱功能开启 查询录入明细
        ScBTransferTeusItemMapper boxMapper = ApplicationContextHandle.getBean(ScBTransferTeusItemMapper.class);
        boxItems = boxMapper.selectList(
                new QueryWrapper<ScBTransferTeusItem>().lambda().
                        eq(ScBTransferTeusItem::getScBTransferId, objId));

        SgBPhyOutNoticesAllInfoResult sgBPhyOutNoticesAllInfoResult = getNoticesOut(transfer);
        SgPhyOutResultSaveAndAuditCmd saveAndAuditCmd = (SgPhyOutResultSaveAndAuditCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                SgPhyOutResultSaveAndAuditCmd.class.getName(), "sg", "1.0");
        SgPhyOutResultBillSaveRequest outRequest = new SgPhyOutResultBillSaveRequest();
        outRequest.setLoginUser(user);
        outRequest.setObjId(-1L);
        SgPhyOutResultSaveRequest outResultRequest = new SgPhyOutResultSaveRequest();

        outResultRequest.setId(-1L);
        outResultRequest.setSgBPhyOutNoticesId(sgBPhyOutNoticesAllInfoResult.getSgBPhyOutNotices().getId());
        outResultRequest.setSgBPhyOutNoticesBillno(sgBPhyOutNoticesAllInfoResult.getSgBPhyOutNotices().getBillNo());
        outResultRequest.setSourceBillId(transfer.getId());
        outResultRequest.setSourceBillNo(transfer.getBillNo());
        outResultRequest.setSourceBillType(SgConstantsIF.BILL_TYPE_TRANSFER);
        outResultRequest.setCpCPhyWarehouseId(origWarehouse.getId());
        outResultRequest.setCpCPhyWarehouseEcode(origWarehouse.getEcode());
        outResultRequest.setCpCPhyWarehouseEname(origWarehouse.getEname());
        outResultRequest.setCpCCustomerWarehouseId(destWarehouse.getId());
        outResultRequest.setCpCCsEname(destWarehouse.getEname());
        outResultRequest.setCpCCsEcode(destWarehouse.getEcode());
        outResultRequest.setIsLast(SgOutConstantsIF.OUT_IS_LAST_N);
        outRequest.setOutResultRequest(outResultRequest);

        // 箱功能开启时，传录入明细和箱内明细
        if (boxConfig.getBoxEnable()) {
            createAndAuditOutResultBox(outRequest, transfer, impItems, boxItems, -1);
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + ",createAndAuditOutResultBoxTransfer param:"
                        + JSON.toJSONString(outRequest));
            }
        } else {
            List<SgPhyOutResultItemSaveRequest> outResultItemRequests = new ArrayList<>();
            itemList.forEach(transferItem -> {
                SgPhyOutResultItemSaveRequest resultItemSaveRequest = new SgPhyOutResultItemSaveRequest();
                BeanUtils.copyProperties(transferItem, resultItemSaveRequest);
                resultItemSaveRequest.setId(-1L);
                resultItemSaveRequest.setPsCSpec1Id(transferItem.getPsCClrId());
                resultItemSaveRequest.setPsCSpec1Ecode(transferItem.getPsCClrEcode());
                resultItemSaveRequest.setPsCSpec1Ename(transferItem.getPsCClrEname());
                resultItemSaveRequest.setPsCSpec2Id(transferItem.getPsCSizeId());
                resultItemSaveRequest.setPsCSpec2Ecode(transferItem.getPsCSizeEcode());
                resultItemSaveRequest.setQty(transferItem.getQty() != null ? transferItem.getQty().negate() : transferItem.getQty());
                resultItemSaveRequest.setPsCSpec2Ename(transferItem.getPsCSizeEname());
                resultItemSaveRequest.setSgBPhyOutNoticesItemId(getNoticeItemIdBySkuEcode(sgBPhyOutNoticesAllInfoResult.getSgBPhyOutNoticesItemList(), transferItem.getPsCSkuEcode()));
                outResultItemRequests.add(resultItemSaveRequest);
            });
            outRequest.setOutResultItemRequests(outResultItemRequests);
        }

        log.debug("invokeOutNoticeAndAudit==> 请求 新增并审核出库结果单的参数" + JSONObject.toJSONString(outRequest));
        ValueHolderV14<SgR3BaseResult> v14 = saveAndAuditCmd.saveOutResultAndAudit(outRequest);
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ",insertAndAuditOutResult,v14:" + JSON.toJSONString(v14));
        }
        if (v14.getCode() == ResultCode.FAIL) {
            AssertUtils.logAndThrow("新增并审核出库结果单失败！" + v14.getMessage(), user.getLocale());
        }
    }


    /**
     * 作废入库通知单服务
     *
     * @param transfe
     */
    public void invokeVoidInNotice(ScBTransfer transfe, User user) {
        SgPhyInNoticesVoidReqeust reqeust = new SgPhyInNoticesVoidReqeust();
        reqeust.setSourceBillType(SgConstantsIF.BILL_TYPE_TRANSFER);
        reqeust.setSourceBillId(transfe.getId());
        reqeust.setLoginUser(user);
        ValueHolderV14<SgR3BaseResult> holderV14 = sgPhyInNoticeVoidCmd.voidSgPhyInNotice(reqeust);
        if (!holderV14.isOK()) {
            AssertUtils.logAndThrow("作废入库通知单服务失败" + holderV14.getMessage());
        }
    }

    /**
     * 作废入库拣货单
     *
     * @param transfer
     * @param user
     */

    public void invokeAutoGeneInPicker(ScBTransfer transfer, User user) {

        //根据入库通知单查入库拣货单
        SgBInPickorderMapper pickorderMapper = ApplicationContextHandle.getBean(SgBInPickorderMapper.class);
        SgBInPickorder sgBInPickorder = pickorderMapper.selectOne(new QueryWrapper<SgBInPickorder>().lambda().
                eq(SgBInPickorder::getSourceBillNo, transfer.getBillNo()).
                eq(SgBInPickorder::getIsactive, "Y"));
        SgPhyInPickOrderVoidRequest request = new SgPhyInPickOrderVoidRequest();
        request.setUser(user);
        request.setIds(Lists.newArrayList(sgBInPickorder.getId()));
        request.setSourceBillNoList(Lists.newArrayList(transfer.getBillNo()));
        ValueHolderV14 holderV14 = sgPhyInPickOrderVoidCmd.voidSgbBInPickorder(request);
        if (!holderV14.isOK()) {
            AssertUtils.logAndThrow("作废入库拣货单失败！" + holderV14.getMessage());
        }
    }

    /**
     * 清空逻辑收货单服务
     *
     * @param transfer
     * @param user
     */

    public void invokeClearLogicRecive(ScBTransfer transfer, User user) {
        SgReceiveCleanService cleanService = ApplicationContextHandle.getBean(SgReceiveCleanService.class);
        SgReceiveBillCleanRequest request = new SgReceiveBillCleanRequest();
        request.setLoginUser(user);
        request.setServiceNode(SgConstantsIF.SERVICE_NODE_TRANSFER_CANCEL_OUT);
        request.setSourceBillId(transfer.getId());
        request.setSourceBillNo(transfer.getBillNo());
        request.setSourceBillType(SgConstantsIF.BILL_TYPE_TRANSFER);
        ValueHolderV14 cleanHolder = cleanService.cleanSgBReceive(request);
        if (!cleanHolder.isOK()) {
            AssertUtils.logAndThrow("清空逻辑收货单服务失败" + cleanHolder.getMessage());
        }
    }

    /**
     * 匹配 要货申请单号
     *
     * @param transfer
     */

    public void executeAfterService(ScBTransfer transfer) {
        // 1
        //根据要货申请单号与要货申请单的要货申请单号进行匹配 ，若匹配成功：则更新要货申请单单据状态为已审核未出库

        //2020.04.10注释该逻辑   暂不回写要货申请
//        String requestBillNo = transfer.getRequestBillNo();
//        if (!StringUtils.isEmpty(requestBillNo)) {
//            OcRequireOrderSearchCmd updateCmd = (OcRequireOrderSearchCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
//                    OcRequireOrderSearchCmd.class.getName(), "oc", "1.0");
//            OcRequireOrderSearchRequest requireOrderSearchRequest = new OcRequireOrderSearchRequest();
//            requireOrderSearchRequest.setBillNo(requestBillNo);
//            requireOrderSearchRequest.setUser(user);
//            updateCmd.execute(requireOrderSearchRequest);
//        }

        // 2020-04-08添加逻辑：调拨性质为“退货调拨”,则：更新退货申请单出库数量和状态
//        if (SgTransferConstantsIF.TRANSFER_PROP_TH_STR.equals(transfer.getSgBTransferPropEname())) {
//            ScBTransferItemMapper itemMapper = ApplicationContextHandle.getBean(ScBTransferItemMapper.class);
//            List<ScBTransferItem> itemList = itemMapper.selectList(new QueryWrapper<ScBTransferItem>().lambda());
//            if (CollectionUtils.isNotEmpty(itemList)) {
//
//                if (log.isDebugEnabled()) {
//                    log.debug(this.getClass().getName() + ",调拨单取消出库更新退货申请单");
//                }
//
//                SgTransferBillUtils billUtils = ApplicationContextHandle.getBean(SgTransferBillUtils.class);
//                billUtils.updateRefundApply(transfer, itemList, ScTransferConstants.ACTION_OUT);
//            }
//        }
    }


    /**
     * 出库减 入库加 都为Y
     *
     * @param transfer
     * @param user
     * @param mapper
     * @param autoFlag
     */
    @Transactional(rollbackFor = Exception.class)
    public void exeOutSubYAndInAddY(ScBTransfer transfer, User user, ScBTransferMapper mapper, boolean autoFlag, String inAdd) {
//        Long origStoreId = transfer.getCpCOrigId();
//        Long destStoreId = transfer.getCpCDestId();
        Long objId = transfer.getId();

        if (ScTransferConstants.IS_AUTO_Y.equals(inAdd)) {
            // 1
            // 作废入库通知单服务  SgPhyInNoticeVoidCmdImpl
            invokeVoidInNotice(transfer, user);

            //6
            // 如果系统参数=出库，调用【清空逻辑收货单服务】；
            String checkPoint = AdParamUtil.getParam(ScTransferConstants.SYSTEM_RECEIVE);
            if (StringUtils.isNotEmpty(checkPoint) && Constants.ACTION_OUT.equals(checkPoint)) {
                invokeClearLogicRecive(transfer, user);
            }

            // 2
            // 调用【作废入库拣货单】
            if (autoFlag) {
                invokeAutoGeneInPicker(transfer, user);
            }
        }

        //  3
        // 新增并审核出库结果单服务
//        CpCPhyWarehouse origWarehouse = getWareHouseInfo(origStoreId);
//        CpCPhyWarehouse destWarehouse = getWareHouseInfo(destStoreId);
//        AssertUtils.notNull(origWarehouse, "数据异常-发货实体仓不存在", user.getLocale());
//        AssertUtils.notNull(destWarehouse, "数据异常-收货实体仓不存在", user.getLocale());
//        invokeOutNoticeAndAudit(transfer, user, origWarehouse, destWarehouse);

        // 3.新增出库结果单并审核（结果单审核不发消息，消息体以出参的方式返回）
        SgOutResultMQRequest mqRequest = createOutResultAndAudit(user, transfer);

        // 更新主表状态为已审核未出库（只有不是出库完成的单子才能调出库服务）
        ScBTransfer transferUpdate = new ScBTransfer();
        transferUpdate.setId(objId);
        transferUpdate.setStatus(SgTransferBillStatusEnum.AUDITED_NOT_OUT.getVal());
        mapper.updateById(transferUpdate);

        // 4.拿到消息体，调用调拨出库服务
        SgTransferOutResultService outService = ApplicationContextHandle.getBean(SgTransferOutResultService.class);
        ValueHolderV14 v14 = outService.writeBackOutResult(mqRequest, true);
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ",调用调拨出库服务出参:" + JSON.toJSONString(v14));
        }
        if (v14.getCode() == ResultCode.FAIL) {
            AssertUtils.logAndThrow("调用调拨出库服务失败！" + v14.getMessage());
        }

        // 4
        //更新【单据状态】=已审核未出库，清空【出库人】、【出库时间】，【拣货状态】=未拣货、清空【拣货人】、【拣货时间】；
        mapper.update(new ScBTransfer(), new UpdateWrapper<ScBTransfer>().lambda()
                .eq(ScBTransfer::getId, transfer.getId())
                .set(ScBTransfer::getOuterId, null)
                .set(ScBTransfer::getOuterName, null)
                .set(ScBTransfer::getOuterEname, null)
                .set(ScBTransfer::getOutDate, null)
                .set(ScBTransfer::getStatus, SgTransferBillStatusEnum.AUDITED_NOT_OUT.getVal())
                .set(ScBTransfer::getPickOutStatus, ScTransferConstants.PICK_STATUS_UNPICKED)
                .set(ScBTransfer::getPickerOutId, null)
                .set(ScBTransfer::getPickerOutName, null)
                .set(ScBTransfer::getPickerOutEname, null)
                .set(ScBTransfer::getPickerOutTime, null)
                .set(ScBTransfer::getModifierid, user.getId())
                .set(ScBTransfer::getModifierename, user.getEname())
                .set(ScBTransfer::getModifiername, user.getName())
                .set(ScBTransfer::getModifieddate, new Date())
                .set(ScBTransfer::getUnouterId, user.getId())
                .set(ScBTransfer::getUnouterEname, user.getEname())
                .set(ScBTransfer::getUnouterName, user.getName())
        );

        // 5
        //调拨单取消审核服务  SgTransferCancelAuditService
        SgTransferCancelAuditService service = ApplicationContextHandle.getBean(SgTransferCancelAuditService.class);
        service.cancelAudit(objId, user, true);
    }

    /**
     * 新增并审核出库结果单
     *
     * @param user     用户信息
     * @param transfer 调拨单
     */
    private SgOutResultMQRequest createOutResultAndAudit(User user, ScBTransfer transfer) {

        SgPhyOutNoticesCancelRequest request = new SgPhyOutNoticesCancelRequest();
        request.setUser(user);
        request.setSourceBillId(transfer.getId());
        request.setIsCleanSend(false);
        request.setSourceBillType(SgConstantsIF.BILL_TYPE_TRANSFER);
        request.setServiceNode(SgConstantsIF.SERVICE_NODE_TRANSFER_CANCEL_OUT);
        request.setRemark("由调拨单[" + transfer.getBillNo() + "]取消出库生成！");
        SgPhyOutNoticesCancelCmd outNoticesCancelCmd = (SgPhyOutNoticesCancelCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(),
                SgPhyOutNoticesCancelCmd.class.getName(), SgConstantsIF.GROUP, SgConstantsIF.VERSION);

        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ",取消出库生成结果单入参:" + JSON.toJSONString(request));
        }
        ValueHolderV14<SgOutResultMQRequest> holderV14 = outNoticesCancelCmd.cancelSgPhyOutNotices(request);
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ",取消出库生成结果单出参:" + JSON.toJSONString(holderV14));
        }
        if (holderV14.getCode() == ResultCode.FAIL) {
            com.jackrain.nea.sg.basic.utils.AssertUtils.logAndThrow("取消出库生成结果单失败！" + holderV14.getMessage());
        }
        if (holderV14.getData() == null) {
            com.jackrain.nea.sg.basic.utils.AssertUtils.logAndThrow("取消出库生成结果单出参为空！" + holderV14.getMessage());
        }
        return holderV14.getData();
    }

    /**
     * 出库减 N 入库加 为Y  自动入库为N
     *
     * @param transfer
     * @param user
     * @param mapper
     * @param autoFlag
     */
    @Transactional(rollbackFor = Exception.class)
    public void exeOutSubNAndInAddYAndAutoInN(ScBTransfer transfer, User user, ScBTransferMapper mapper, boolean autoFlag) {
        //  1
        //更新单据状态为未审核；清空【审核人】、【审核时间】、【出库人】、【出库时间】，【拣货状态】=未拣货、清空【拣货人】、【拣货时间】；
        mapper.update(new ScBTransfer(), new UpdateWrapper<ScBTransfer>().lambda().
                eq(ScBTransfer::getId, transfer.getId())
                .set(ScBTransfer::getStatus, SgTransferBillStatusEnum.UN_AUDITED.getVal())
                .set(ScBTransfer::getStatusId, null)
                .set(ScBTransfer::getStatusTime, null)
                .set(ScBTransfer::getOuterId, null)
                .set(ScBTransfer::getOutTime, null)
                .set(ScBTransfer::getPickStatus, ScTransferConstants.PICK_STATUS_UNPICKED)
                .set(ScBTransfer::getPickerId, null)
                .set(ScBTransfer::getPickerOutTime, null)
                .set(ScBTransfer::getModifierid, user.getId())
                .set(ScBTransfer::getModifierename, user.getEname())
                .set(ScBTransfer::getModifiername, user.getName())
                .set(ScBTransfer::getModifieddate, new Date())
                .set(ScBTransfer::getUnouterId, user.getId())
                .set(ScBTransfer::getUnouterEname, user.getEname())
                .set(ScBTransfer::getUnouterName, user.getName())
        );

        // 2
        //清空出库数量 条码明细,箱条码，直接删除 录入明细  数量 改 0，
        // 条码明细  QTY
        ScBTransferItemMapper itemMapper = ApplicationContextHandle.getBean(ScBTransferItemMapper.class);
        // 录入明细
        ScBTransferImpItemMapper impItemMapper = ApplicationContextHandle.getBean(ScBTransferImpItemMapper.class);
        itemMapper.deleteItemListByFerId(transfer.getId());
        impItemMapper.updateQtyByTranId(transfer.getId(), user, new Date());
        // 更新调拨单的汇总信息：【总出库数量】、【总出库吊牌金额】、【总出库销售金额】；
        mapper.updateMoneyAndNumById(transfer.getId(), user.getId(), user.getName(), user.getEname(), new Date());


        // 3
        //调用【清空逻辑收货单服务】；SgReceiveCleanService
        invokeClearLogicRecive(transfer, user);

        // 4
        // 调用【作废入库通知单服务】，
        invokeVoidInNotice(transfer, user);

        //5
        //当【是否自动生成入库拣货单】=是，调用【作废入库拣货单】服务
        if (autoFlag) {
            invokeAutoGeneInPicker(transfer, user);
        }

    }


    /**
     * 封装新增并审核出库结果单入参（箱功能）
     *
     * @param request  结果单对象
     * @param transfer 调拨单对象
     */
    public void createAndAuditOutResultBox(SgPhyOutResultBillSaveRequest request, ScBTransfer transfer,
                                           List<ScBTransferImpItem> impItems, List<ScBTransferTeusItem> boxItems, int ctl) {
        SgPhyOutResultBillSaveRequest requestRedis = new SgPhyOutResultBillSaveRequest();
        requestRedis.setOutResultRequest(request.getOutResultRequest());

        // 录入明细参数封装
        if (CollectionUtils.isNotEmpty(impItems)) {
            List<SgPhyOutResultImpItemSaveRequest> impItemList = Lists.newArrayList();
            for (ScBTransferImpItem saleImpItem : impItems) {
                SgPhyOutResultImpItemSaveRequest outResultImpItemSaveRequest = new SgPhyOutResultImpItemSaveRequest();
                BeanUtils.copyProperties(saleImpItem, outResultImpItemSaveRequest);

                if (ctl == -1) {
                    outResultImpItemSaveRequest.setQtyOut(saleImpItem.getQty() != null ? saleImpItem.getQty().negate() : saleImpItem.getQty());
                }
                // 来源单据id(销售单id)
                impItemList.add(outResultImpItemSaveRequest);
            }
            requestRedis.setImpItemList(impItemList);
        }

        // 箱内明细参数封装
        if (CollectionUtils.isNotEmpty(boxItems)) {
            List<SgPhyOutResultTeusItemSaveRequest> boxItemList = Lists.newArrayList();
            for (ScBTransferTeusItem boxItem : boxItems) {
                SgPhyOutResultTeusItemSaveRequest outNoticesBoxItemSaveRequest = new SgPhyOutResultTeusItemSaveRequest();
                BeanUtils.copyProperties(boxItem, outNoticesBoxItemSaveRequest);
                if (ctl == -1) {
                    outNoticesBoxItemSaveRequest.setQty(boxItem.getQty() != null ? boxItem.getQty().negate() : boxItem.getQty());
                }
                boxItemList.add(outNoticesBoxItemSaveRequest);
            }
            requestRedis.setTeusItemList(boxItemList);
        }

        // 如果数据(主表 + 箱内明细 + 录入明细)大于4兆，则存redis
        String billSize = requestRedis.toString();
        if (billSize.getBytes().length > OcBasicConstants.REDIS_MEMORY_SIZE) {

            // redisKey："中心名:当前表名:下游表名:单据编号:动作"
            String redisKey = SgConstants.CENTER_NAME_SG + ":" + OcBasicConstants.REDIS_TRANSFER + ":"
                    + OcBasicConstants.REDIS_PHY_OUT_RESULT + ":" + transfer.getBillNo() + ":"
                    + Constants.ACTION_SUBMIT;

            CusRedisTemplate<String, String> redisTemplate = RedisOpsUtil.getStrRedisTemplate();
            redisTemplate.opsForValue().set(redisKey, JSON.toJSONString(requestRedis), 7, TimeUnit.DAYS);
            request.setRedisKey(redisKey);
        } else {
            request.setImpItemList(requestRedis.getImpItemList());
            request.setTeusItemList(requestRedis.getTeusItemList());
        }
    }


    /**
     * 检查经销商
     *
     * @param locale 国际化
     * @param origId 发货店仓
     * @param destId 收货店仓
     */
    private Long checkCustomer(Locale locale, Long origId, Long destId) {

        CpCStore origInfo = cacheValUtils.getStoreInfo(origId);
        CpCStore destInfo = cacheValUtils.getStoreInfo(destId);

        if (origInfo == null || origInfo.getCpCCustomerId() == null) {
            AssertUtils.logAndThrow("发货店仓/经销商查询为空！", locale);
        }

        if (destInfo == null || destInfo.getCpCCustomerId() == null) {
            AssertUtils.logAndThrow("收货店仓/经销商查询为空！", locale);
        }

        // 当发货经销商与收货经销商不一致时，则提示：“发货经销商与收货经销商不一致，不允许取消出库！
        AssertUtils.isTrue(origInfo.getCpCCustomerId().equals(destInfo.getCpCCustomerId()),
                "发货经销商与收货经销商不一致，不允许取消出库！", locale);

        return destInfo.getCpCCustomerId();
    }

    /**
     * 调拨单取消出库调用erp服务
     *
     * @param user       用户信息
     * @param transfer   调拨单
     * @param customerId 收货经销商
     */
    private void checkCustomerErp(User user, ScBTransfer transfer, Long customerId) {

        SgTransferRpcService rpcService = ApplicationContextHandle.getBean(SgTransferRpcService.class);
        CpCCustomer customer = rpcService.queryCpCustomer(customerId);
        if (customer == null) {
            AssertUtils.logAndThrow("收货经销商查询为空！" + customerId, user.getLocale());
        }

        // 是否ERP组织”=是时，则调用“退货出库取消(erp提供接口)”接口：
        if (1 == customer.getIsErpOrg()) {

            HashMap<String, Object> map = new HashMap(16);
            map.put("messageType", MessageTypeConstants.ZT_THD_CANCEL);
            map.put("vpsno", "中台调拨出库取消接口");
            map.put("orderNo", transfer.getBillNo());
            map.put("user", user.getName());
            try {
                //调用总部提供 “退货出库取消” 接口
                FreeMarkerUtil utl = new FreeMarkerUtil();
                String content = utl.getTemplateJsonString(ApiConstants.JSON_REQUEST_FREEMARKER_PATH, ApiRequestJsonNameConstants.PAY_IN_BILL_CANCEL_AUDIT_REQUEST, map);

                if (log.isDebugEnabled()) {
                    log.debug(this.getClass().getName() + ",调拨出库取消,中台调erp入参:" + content + "; map:" + JSON.toJSONString(map));
                }
                String jsonString = ErpUtil.postText(content);
                JSONObject jsonObject = JSONObject.parseObject(jsonString);
                if (log.isDebugEnabled()) {
                    log.debug(this.getClass().getName() + ",调拨出库取消,中台调erp出参:" + jsonObject);
                }

                String code = jsonObject.getString("code");
                AssertUtils.cannot(ErpApiResultEnum.FAIL.getCode().equals(code), jsonObject.getString("message"), user.getLocale());

            } catch (Exception e) {
                if (log.isErrorEnabled()) {
                    log.error(this.getClass().getName() + ",调用ERP取消接口发生异常:" + StorageUtils.getExceptionMsg(e));
                }
                if (e instanceof NDSException) {
                    throw (NDSException) e;
                }
                AssertUtils.logAndThrow("调用ERP取消接口发生异常:" + StorageUtils.getExceptionMsg(e));
            }
        }
    }
}
