package com.jackrain.nea.sg.sx.utils;

import com.alibaba.fastjson.JSON;
import com.jackrain.nea.ac.model.request.AcBOperateCloseCtrlRequest;
import com.jackrain.nea.ac.model.table.AcBOperateClose;
import com.jackrain.nea.sg.sx.rpc.AcRpcService;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.utils.AssertUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author leexh
 * @since 2020/3/27 13:48
 * desc:
 */
@Slf4j
@Component
public class BillUtils {

    /**
     * 关账日期check
     *
     * @param destCode 组织编码
     * @param billDate 入库日期
     */
    public void checkOperateCloseDate(String destCode, Date billDate) {

        AcBOperateCloseCtrlRequest request = new AcBOperateCloseCtrlRequest();
        request.setEcode(destCode);
        request.setCloseDate(billDate);

        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ",营运中心关账单查询入参:" + JSON.toJSONString(request));
        }
        AcRpcService rpcService = ApplicationContextHandle.getBean(AcRpcService.class);
        ValueHolderV14<AcBOperateClose> holderV14 = rpcService.queryAcOperateClose(request);
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ",营运中心关账单查询出参:" + JSON.toJSONString(holderV14));
        }

        // 若返回‘0’则不控制，返回‘-1’，则提示‘此营运中心关账日期为：XX-XX，不允许再操作单据！’
        if (!holderV14.isOK()) {
            Date closeDate = holderV14.getData() != null ? holderV14.getData().getCloseDate() : null;
            AssertUtils.cannot(closeDate == null, "此营运中心关账日期返回为空，请检查！");

            String closeDateStr = new SimpleDateFormat("yyyy-MM-dd").format(closeDate);
            AssertUtils.logAndThrow("此营运中心关账日期为：" + closeDateStr + "，不允许再操作单据！");
        }
    }
}
