package com.jackrain.nea.sg.sx.in.services;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.cpext.model.table.CpCStore;
import com.jackrain.nea.cpext.model.table.CpCustomer;
import com.jackrain.nea.data.basic.model.request.CpcStoreWrapper;
import com.jackrain.nea.data.basic.services.BasicCpQueryService;
import com.jackrain.nea.oc.sale.api.OcSaleQueryCmd;
import com.jackrain.nea.oc.sale.model.table.OcBSale;
import com.jackrain.nea.r3.mq.exception.SendMqException;
import com.jackrain.nea.r3.mq.util.R3MqSendHelper;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.in.mapper.SgBInPickorderMapper;
import com.jackrain.nea.sg.in.model.table.SgBInPickorder;
import com.jackrain.nea.sg.in.model.table.SgBInPickorderItem;
import com.jackrain.nea.sg.in.model.table.SgBInPickorderNoticeItem;
import com.jackrain.nea.sg.sx.checkdiffe.common.SgChkDiffeConstants;
import com.jackrain.nea.sg.sx.config.SxInPickOrderMqConfig;
import com.jackrain.nea.sg.sx.in.model.consts.SxTaskConst;
import com.jackrain.nea.sg.sx.in.model.enums.BillStatusEnums;
import com.jackrain.nea.sg.sx.in.model.enums.OrderAuditStatus;
import com.jackrain.nea.sg.sx.mapper.SgSxInPickOrderItemMapper;
import com.jackrain.nea.sg.sx.mapper.SgSxInPickOrderNoticeItemMapper;
import com.jackrain.nea.sg.sx.rpc.CpRpcCustomerQuery;
import com.jackrain.nea.sg.sx.rpc.CpRpcStoreQuery;
import com.jackrain.nea.sg.transfer.model.table.ScBTransfer;
import com.jackrain.nea.sg.transfer.services.SgTransferQueryService;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.assertj.core.util.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * 采购单回传(拣货)
 *
 * @author: xiWen.z
 * create at: 2019/12/18 0018
 */
@Slf4j
@Component
public class SgBInPickOrderTrans2MidSerivce {

    @Autowired
    private R3MqSendHelper r3MqSendHelper;

    @Autowired
    SgBInPickorderMapper sgBInPickorderMapper;

    @Autowired
    SgSxInPickOrderItemMapper sgSxInPickOrderItemMapper;

    @Autowired
    SgSxInPickOrderNoticeItemMapper sgSxInPickOrderNoticeItemMapper;

    @Autowired
    SxInPickOrderMqConfig sxInPickOrderMqConfig;

    @Autowired
    CpRpcCustomerQuery cpRpcCustomerQuery;

    @Autowired
    CpRpcStoreQuery cpRpcStoreQuery;

    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    /**
     * 采购回传.流向中间表
     *
     * @param order 拣货单
     * @param usr   操作用户
     * @return vh
     */
    public ValueHolderV14 pickOrderTrans2MidService(SgBInPickorder order, User usr) {
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ",查询待传ERP拣货单据对象:" + JSON.toJSONString(order));
        }

        /**
         * 1. 校验
         */
        AssertUtils.notNull(usr, "操作用户不能为空");
        AssertUtils.notNull(order, "拣货单数据为空");
        AssertUtils.notNull(order.getId(), "订单Id不能为空值");

        /**
         * 2. 业务
         */
        //根据拣货id   查询List  拣货明细数据
        List<SgBInPickorderItem> itemList = sgSxInPickOrderItemMapper.selectInpickOrderItemList(order.getId());
        AssertUtils.notEmpty(itemList, "未查询到拣货明细数据, 订单Id:" + order.getId());

        //根据拣货id   查询List  拣货通知明细数据
        List<SgBInPickorderNoticeItem> noticeList = sgSxInPickOrderNoticeItemMapper
                .selectInpickOrderNoticItemList(order.getId());
        AssertUtils.notEmpty(noticeList, "未查询到拣货通知明细数据, 订单Id:" + order.getId());
        ValueHolderV14 vh = new ValueHolderV14();

        //验证单据    ---
        String validStr = validateBill(order, noticeList);
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ",校验结果:" + validStr);
        }
        //返回验证单据  信息
        if (StringUtils.isNotBlank(validStr)) {
            vh.setCode(ResultCode.FAIL);
            vh.setMessage(validStr);
            return vh;
        }

        String key = "SG_IN_PICK_2_ERP_" + order.getId() + "_" + order.getBillNo();
        Object obj = combinationBody(order, itemList, noticeList);
        String msgId;
        try {

            msgId = r3MqSendHelper.sendMessage(obj, sxInPickOrderMqConfig.getSendInPickOrderTransMqTopic(),
                    sxInPickOrderMqConfig.getSendInPickOrderTransMqTag(), key);
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + ",采购回传MQ发送成功,msgId:" + msgId + ",msgKey:" + key);
            }

            vh.setMessage("采购回传MQ发送成功, msgId " + msgId + ", msgKey " + key);
        } catch (SendMqException e) {

            String expMsg = "采购回传MQ发送失败, 订单Id," + order.getId();
            if (log.isErrorEnabled()) {
                log.error(this.getClass().getName() + expMsg + ",异常信息:" + e.getMessage());
            }
            vh.setCode(ResultCode.FAIL);
            vh.setMessage(expMsg);
            return vh;
        }
        vh.setCode(ResultCode.SUCCESS);
        return vh;
    }

    /**
     * @param sg
     * @param itemList
     * @param noticeList
     * @return
     */
    private Object combinationBody(SgBInPickorder sg, List<SgBInPickorderItem> itemList, List<SgBInPickorderNoticeItem> noticeList) {

        // 0. main
        JSONObject mainObj = new JSONObject();
        mainObj.put("SOURCE_BILL_NO", String.valueOf(sg.getId()));
        mainObj.put("BILL_TYPE", SxTaskConst.IN_PICK_TYPE);
        // 1. body
        JSONObject jsnObj = new JSONObject();
        JSONObject header = new JSONObject();
        header.put("name", "purchaseReceipt");
        header.put("ip", "");
        jsnObj.put("service", header);

        // 2.0  content.primary
        JSONObject content = new JSONObject();
        //入库单号
        content.put("tc_aah01", sg.getBillNo());
        //入库时间 == 审核时间
        Date statusTime = sg.getStatusTime();
        String statusTimeStr = null;
        if (statusTime != null) {
            statusTimeStr = sdf.format(statusTime);
        }
        content.put("tc_aah02", statusTimeStr);
        //收货人-> 审核人
        content.put("tc_aah05", sg.getStatusName());
        // 收货经销商
        content.put("tcaahlegal", sg.getCpCCustomerDestEcode());
        // 通知子表备参
        JSONObject tmpJsn = getPickOrder(noticeList);

        //送货客户, << -- notice 子表
        content.put("tc_aah11", tmpJsn.getString("delivery"));
        // 托运单号 ?  == null 子表: 来源入库通知单的备货单号: stock_bill_no
        String billNo = sg.getCheckNo() == null ? tmpJsn.getString("billNo") : sg.getCheckNo();
        content.put("tc_aah17", billNo);
        //入库序号
        content.put("tc_aah19", sg.getInSeqNo());
        // 店铺编码cp_c_dest_ecode
        content.put("tc_aahplant", sg.getCpCDestEcode());
        // 是否收齐
        content.put("tc_aah08", sg.getIsAllRecev());
        // 2.1 content.item
        JSONArray itemAry = new JSONArray();

        for (SgBInPickorderItem item : itemList) {
            JSONObject itemObj = new JSONObject();
            // 料号
            itemObj.put("tc_aai03", item.getPsCProEcode());

            // 应收数量
            itemObj.put("tc_aai10", item.getQtyNotice());
            // 实收数量
            itemObj.put("tc_aai11", item.getQtyIn());
            //备注
            itemObj.put("tc_aai16", item.getRemark());
            itemObj.put("tc_aai02", item.getId());
            itemAry.add(itemObj);
        }

        // 2.2 com
        content.put("detailList", itemAry);

        // 1.2 body
        JSONArray contentAry = new JSONArray();
        // contentAry.add(content);
        jsnObj.put("data", content);
        // 0.1 main
        mainObj.put("ORIG_BILL_CONTENT", jsnObj.toJSONString());
        return mainObj.toJSONString();
    }

    /**
     * 验证单据
     *
     * @param sg 拣货单
     * @return 错误信息
     */
    private String validateBill(SgBInPickorder sg, List<SgBInPickorderNoticeItem> noticeList) {
        if (StringUtils.isBlank(sg.getBillNo())) {
            return "单据编号为空";
        }
        int status = sg.getTransStatus() == null ? 0 : sg.getTransStatus().intValue();
        if (status == BillStatusEnums.TransErpEnum.SUCCESS.toInt()) {
            return "单据状态为已传";
        }
        if (sg.getBillStatus() == null || OrderAuditStatus.AUDITED.intVal() != sg.getBillStatus()) {
            return "单据状态不正确";
        }
        if (StringUtils.isBlank(sg.getCpCDestEcode())) {
            return "店铺编码不能为空";
        }
        if (StringUtils.isBlank(sg.getCpCCustomerDestEcode())) {
            return "收货经销商编码为空值";
        }

        // 根据来源通知单查询上游单据收货店仓（逻辑仓）,经产品确认来源单据：调拨单或者销售单
        // 同一个拣货单中的来源单据收货店仓相同，估取第一条即可
        Long sourceBillId = noticeList.get(0).getSourceBillId();
        Integer sourceBillType = noticeList.get(0).getSourceBillType();

        // 查询来源单据收货店仓
        Long billDestStore = getSourceBillDestStore(sourceBillType, sourceBillId);
        if (billDestStore == null) {
            return "查询来源单据为空:" + sourceBillType + "---" + sourceBillId;
        }

        //获取收货店仓信息
        BasicCpQueryService basicCpQueryService = ApplicationContextHandle.getBean(BasicCpQueryService.class);
        CpcStoreWrapper destStore = basicCpQueryService.findCpcStore(billDestStore);

        //判断收货店仓信息
        if (destStore.getCpCStore() == null) {
            return "未获取到收货店仓信息";
        } else {
            //3)“收货店仓”在店仓档案上的经营类型为集团直营，且来源入库通知单明细表中的“备货单号”不为空的数据；
            //判断经营类型是否为空
            if (StringUtils.isNotEmpty(destStore.getManageTypeCode())) {
                if (destStore.getManageTypeCode().equals(SgChkDiffeConstants.STORE_TYPE)) {
                    return null;
                } else {
                    return "经营类型为:非直营门店";
                }
            } else {
                return "经营类型为空";
            }
        }
    }

    /**
     * 根据来源单号查询收货店仓
     *
     * @param sourceBillType 来源单据类型
     * @param sourceBillId   来源单据ID
     * @return
     */
    private Long getSourceBillDestStore(Integer sourceBillType, Long sourceBillId) {

        Long destStore = null;
        if (SgConstantsIF.BILL_TYPE_TRANSFER == sourceBillType) {

            SgTransferQueryService service = ApplicationContextHandle.getBean(SgTransferQueryService.class);
            ScBTransfer transfer = service.queryTransferByBillNo(sourceBillId);
            if (transfer != null) {
                destStore = transfer.getCpCDestId();
            }

        } else if (SgConstantsIF.BILL_TYPE_SALE == sourceBillType) {

            OcSaleQueryCmd saleQueryCmd = (OcSaleQueryCmd) ReferenceUtil.refer(
                    ApplicationContextHandle.getApplicationContext(), OcSaleQueryCmd.class.getName(),
                    "oc", "1.0");
            List<Long> sourceBillIds = Lists.newArrayList();
            sourceBillIds.add(sourceBillId);
            List<OcBSale> sales = saleQueryCmd.getSaleById(sourceBillIds);
            if (CollectionUtils.isNotEmpty(sales)) {
                destStore = sales.get(0).getCpCDestId();
            }
        }
        return destStore;
    }

    /**
     * 拣货通知子表提取数据赋予主表
     *
     * @param noticeList 拣货通知数据
     * @return 提取值
     */
    private JSONObject getPickOrder(List<SgBInPickorderNoticeItem> noticeList) {
        JSONObject jsnObj = new JSONObject();
        String billNo = null;
        String delivery = null;


        for (SgBInPickorderNoticeItem item : noticeList) {
            if (item == null) {
                continue;
            }
            if (delivery == null) {
                String code = item.getCpCCustomerDeliveryEcode();
                if (StringUtils.isNotBlank(code)) {
                    delivery = code;
                }
            }
            if (billNo == null) {
                String stockBillNo = item.getStockBillNo();
                if (StringUtils.isNotBlank(stockBillNo)) {
                    billNo = stockBillNo;
                }
            }
        }

        jsnObj.put("billNo", billNo);
        jsnObj.put("delivery", delivery);
        return jsnObj;

    }


    /**
     * 获取经销商档案
     *
     * @param id 销商档案id
     * @return 销商档案
     */
    private CpCustomer queryCustomer(Long id) {
        try {
            ValueHolderV14<CpCustomer> vh = cpRpcCustomerQuery.queryCustomerById(id);
            if (vh.isOK()) {
                CpCustomer data = vh.getData();
                return data;
            }
        } catch (Exception e) {
            log.error(this.getClass().getName() + "经销商查询异常,id " + id, e);
        }
        return null;

    }

    /**
     * 查询逻辑仓
     *
     * @param id 发货店仓
     * @return 发货店仓
     */
    private CpCStore queryCpCWareHouse(Long id) {
        try {
            ValueHolderV14<CpCStore> vh = cpRpcStoreQuery.queryCpStoreById(id);
            if (vh.isOK()) {
                return vh.getData();
            }
        } catch (Exception e) {
            log.error(this.getClass().getName() + "逻辑仓查询异常,id " + id, e);
        }
        return null;
    }
}
