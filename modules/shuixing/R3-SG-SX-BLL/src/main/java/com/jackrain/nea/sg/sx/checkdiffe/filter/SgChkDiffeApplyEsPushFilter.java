package com.jackrain.nea.sg.sx.checkdiffe.filter;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.es.util.ElasticSearchUtil;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.sx.checkdiffe.common.SgChkDiffeConstants;
import com.jackrain.nea.sg.sx.checkdiffe.mapper.SgBChkDiffeApplyItemMapper;
import com.jackrain.nea.sg.sx.checkdiffe.mapper.SgBChkDiffeApplyMapper;
import com.jackrain.nea.sg.sx.checkdiffe.model.table.SgBChkDiffeApply;
import com.jackrain.nea.sg.sx.checkdiffe.model.table.SgBChkDiffeApplyItem;
import com.jackrain.nea.tableService.DbRowAction;
import com.jackrain.nea.tableService.MainTableRecord;
import com.jackrain.nea.tableService.RowRecord;
import com.jackrain.nea.tableService.TableServiceContext;
import com.jackrain.nea.tableService.filter.BaseFilter;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Collection;
import java.util.List;

/**
 * @author chenxinxing
 * @description 盘差单推送ES
 * @since 2020/2/22 11:41
 */

@Slf4j
@Component
@Deprecated
public class SgChkDiffeApplyEsPushFilter extends BaseFilter {

    @Override
    public void after(TableServiceContext context, MainTableRecord row) {
        try {
            Long objId = row.getId();
            SgBChkDiffeApplyMapper sgBChkDiffeApplyMapper = ApplicationContextHandle.getBean(SgBChkDiffeApplyMapper.class);
            SgBChkDiffeApply sgBChkDiffeApply = sgBChkDiffeApplyMapper.selectById(objId);

            SgBChkDiffeApplyItemMapper sgBChkDiffeApplyItemMapper = ApplicationContextHandle.getBean(SgBChkDiffeApplyItemMapper.class);
            List<SgBChkDiffeApplyItem> sgBChkDiffeApplyItems = sgBChkDiffeApplyItemMapper.selectList(new QueryWrapper<SgBChkDiffeApplyItem>().lambda().eq(SgBChkDiffeApplyItem::getSgBChkDiffeApplyId, objId));

            String index = SgChkDiffeConstants.CHK_DIFFE_APPLY_INDEX;
            String type = SgChkDiffeConstants.CHK_DIFFE_APPLY_TYPE;
            String itemTable = type.toUpperCase();

            if (DbRowAction.DELETE == row.getAction()) {
                JSONArray ids = new JSONArray();
                if (row.getSubTables() != null && row.getSubTables().size() > 0
                        && row.getSubTables().get(itemTable) != null & CollectionUtils.isNotEmpty(row.getSubTables().get(itemTable).getRows())) {
                    Collection<RowRecord> rows = row.getSubTables().get(itemTable).getRows();
                    rows.forEach(transferItem ->
                            ids.add(transferItem.getId()));
                }
                deleteById(index, type, objId, ids);
                ElasticSearchUtil.indexDocument(index, index, sgBChkDiffeApply, objId);
            } else {
                if (!ElasticSearchUtil.indexExists(index)) {
                    ElasticSearchUtil.indexCreate(SgBChkDiffeApplyItem.class, SgBChkDiffeApply.class);
                }
                pushById(index, type, sgBChkDiffeApply, sgBChkDiffeApplyItems);
            }

        } catch (Exception e) {
            log.error("盘差申请单es推送失败", e);
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            String message = sw.toString().replace("\n", "<br/>").replace("\tat", "");
            throw new NDSException(this.getClass().getName() + ".error" + message);
        }

    }

    /**
     * 盘差申请单单es推送
     *
     * @param sgBChkDiffeApply      主表记录
     * @param sgBChkDiffeApplyItems 子表记录
     */
    private void pushById(String index, String type, SgBChkDiffeApply sgBChkDiffeApply, List<SgBChkDiffeApplyItem> sgBChkDiffeApplyItems) throws IOException {

        // 主表推送es
        if (sgBChkDiffeApply != null) {
            ElasticSearchUtil.indexDocument(index, type, sgBChkDiffeApply, sgBChkDiffeApply.getId());
        }
        // 明细推送es
        if (!CollectionUtils.isEmpty(sgBChkDiffeApplyItems)) {
            // 批量
            ElasticSearchUtil.indexDocuments(index, type, sgBChkDiffeApplyItems, SgChkDiffeConstants.CHKDIFFE_PARENT_FIELD);
        }
    }

    /**
     * 销售单明细es删除
     *
     * @param objId 父ID
     * @param ids   子ID
     */
    private void deleteById(String index, String type, Long objId, JSONArray ids) throws IOException {
        if (!CollectionUtils.isEmpty(ids)) {
            for (int i = 0; i < ids.size(); i++) {
                String id = ids.getString(i);
                ElasticSearchUtil.delDocument(index, type, id, objId);
            }
        }
    }
}
