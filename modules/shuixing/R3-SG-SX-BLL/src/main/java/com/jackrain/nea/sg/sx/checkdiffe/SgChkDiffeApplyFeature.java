package com.jackrain.nea.sg.sx.checkdiffe;

import com.jackrain.nea.oc.basic.filter.SkuInfoWithSkuIdByRedisFilter;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.sx.checkdiffe.filter.SgChkDiffeApplySaveFilter;
import com.jackrain.nea.sg.sx.checkdiffe.filter.SgChkDiffeApplySubmitFilter;
import com.jackrain.nea.sg.sx.checkdiffe.filter.SgChkDiffeApplyVoidFilter;
import com.jackrain.nea.sg.sx.checkdiffe.validate.SgChkDiffeApplySaveValidate;
import com.jackrain.nea.sg.sx.checkdiffe.validate.SgChkDiffeApplySubmitValidate;
import com.jackrain.nea.sg.sx.checkdiffe.validate.SgChkDiffeApplyVoidValidate;
import com.jackrain.nea.tableService.Feature;
import com.jackrain.nea.tableService.constants.Constants;
import com.jackrain.nea.tableService.feature.FeatureAnnotation;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author chenxinxing
 * @since 2020/2/21 14:41
 */

@FeatureAnnotation(value = "SgChkDiffeApplyFeature", description = "盘差单Feature", isSystem = true)
public class SgChkDiffeApplyFeature extends Feature {

    /**
     * 校验器
     */
    @Autowired
    private SgChkDiffeApplySaveValidate chkDiffeApplySaveValidate;
    @Autowired
    private SgChkDiffeApplySubmitValidate chkDiffeApplySubmitValidate;
    @Autowired
    private SgChkDiffeApplyVoidValidate chkDiffeApplyVoidValidate;

    /**
     * 逻辑Filter
     */

    @Autowired
    private SkuInfoWithSkuIdByRedisFilter skuInfoWithSkuIdByRedisFilter;
    @Autowired
    private SgChkDiffeApplySaveFilter chkDiffeApplySaveFilter;
    @Autowired
    private SgChkDiffeApplySubmitFilter chkDiffeApplySubmitFilter;
    @Autowired
    private SgChkDiffeApplyVoidFilter chkDiffeApplyVoidFilter;

    @Override
    protected void initialization() {

        addValidator(chkDiffeApplySaveValidate, (tableName, actionName) -> tableName.equalsIgnoreCase(SgConstants.SG_B_CHK_DIFFE_APPLY) &&
                actionName.equals(Constants.ACTION_SAVE));
        addValidator(chkDiffeApplySubmitValidate, (tableName, actionName) -> tableName.equalsIgnoreCase(SgConstants.SG_B_CHK_DIFFE_APPLY) &&
                actionName.equals(Constants.ACTION_SUBMIT));
        addValidator(chkDiffeApplyVoidValidate, (tableName, actionName) -> tableName.equalsIgnoreCase(SgConstants.SG_B_CHK_DIFFE_APPLY) &&
                actionName.equals(Constants.ACTION_VOID));

        addFilter(skuInfoWithSkuIdByRedisFilter, (tableName, actionName) -> tableName.equalsIgnoreCase(SgConstants.SG_B_CHK_DIFFE_APPLY) &&
                (actionName.equals(Constants.ACTION_SAVE) || actionName.equals(Constants.ACTION_ADD)));

        addFilter(chkDiffeApplySaveFilter, (tableName, actionName) -> tableName.equalsIgnoreCase(SgConstants.SG_B_CHK_DIFFE_APPLY) &&
                (actionName.equals(Constants.ACTION_SAVE) || actionName.equals(Constants.ACTION_ADD)));

        addFilter(chkDiffeApplySubmitFilter, (tableName, actionName) -> tableName.equalsIgnoreCase(SgConstants.SG_B_CHK_DIFFE_APPLY) &&
                actionName.equals(Constants.ACTION_SUBMIT));

        addFilter(chkDiffeApplyVoidFilter, (tableName, actionName) -> tableName.equalsIgnoreCase(SgConstants.SG_B_CHK_DIFFE_APPLY) &&
                actionName.equals(Constants.ACTION_VOID));


    }
}
