package com.jackrain.nea.sg.sx.rpc;

import com.jackrain.nea.ac.api.AcBOperateCloseCtrlCmd;
import com.jackrain.nea.ac.model.request.AcBOperateCloseCtrlRequest;
import com.jackrain.nea.ac.model.table.AcBOperateClose;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Component;

/**
 * @author leexh
 * @since 2020/3/27 11:32
 * desc:
 */
@Slf4j
@Component
public class AcRpcService {

    @Reference(version = "1.0", group = "ac")
    private AcBOperateCloseCtrlCmd cpStoreCustomerQueryCmd;

    /**
     * ‘营运中心关账单’查询服务
     *
     * @param request 入参
     * @return v14
     */
    public ValueHolderV14<AcBOperateClose> queryAcOperateClose(AcBOperateCloseCtrlRequest request) {
        return cpStoreCustomerQueryCmd.queryAcBOperateCloseCtrl(request);
    }
}
