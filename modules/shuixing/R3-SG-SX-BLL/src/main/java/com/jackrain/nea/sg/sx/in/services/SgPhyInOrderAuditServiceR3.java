package com.jackrain.nea.sg.sx.in.services;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.cpext.utils.ValueHolderUtils;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.basic.common.R3ParamConstants;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.basic.utils.R3ParamUtil;
import com.jackrain.nea.sg.in.model.request.SgPhyInPickOrderAuditRequest;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.query.QuerySession;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Locale;

/**
 * @author zhoulinsheng
 * Date: 2019/12/23
 * Description: R3审核
 */

@Slf4j
@Component
public class SgPhyInOrderAuditServiceR3 {

    public ValueHolder auditR3(QuerySession session) throws NDSException {
        SgPhyInOrderAuditService service = ApplicationContextHandle.getBean(SgPhyInOrderAuditService.class);
        Locale locale = session.getLocale();
        JSONObject param = R3ParamUtil.getParamJo(session);
        User user = session.getUser();
        AssertUtils.notNull(user, "参数不存在-user", locale);
        AssertUtils.notNull(param, "参数不存在-param", locale);
        if (log.isDebugEnabled()) {
            log.debug("Start SgPhyInOrderAuditService.audit. ReceiveParams:param="
                    + JSONObject.toJSONString(param) + ";");
        }
        //页面批量审核
        Long objId = param.getLong(R3ParamConstants.OBJID);

        SgPhyInPickOrderAuditRequest request = new SgPhyInPickOrderAuditRequest();

        // 2020-04-04添加逻辑：标记调拨入库单页面过来的审核
        request.setIsTransferIn(SgConstants.SC_B_TRANSFER_IN.equals(param.getString(R3ParamConstants.TABLE)));

        request.setInPickOrderId(objId);
        request.setLoginUser(user);
        ValueHolder result = R3ParamUtil.convertV14(service.auditInOrder(request));
        if ((Integer) result.getData().get(R3ParamConstants.CODE) == -1) {
            Object o = result.getData().get(R3ParamConstants.MESSAGE);
            return ValueHolderUtils.getFailValueHolder(o != null ? o.toString() : "");
        }

        return ValueHolderUtils.getSuccessValueHolder(objId, "审核成功！");

    }
}
