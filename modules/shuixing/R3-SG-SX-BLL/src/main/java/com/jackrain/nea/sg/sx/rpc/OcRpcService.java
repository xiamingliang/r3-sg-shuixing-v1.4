package com.jackrain.nea.sg.sx.rpc;

import com.jackrain.nea.oc.sx.api.OcRefundApplyCancelOutStockCmd;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Component;

/**
 * @author leexh
 * @since 2020/4/23 16:40
 * desc:
 */
@Slf4j
@Component
public class OcRpcService {

    @Reference(version = "1.0", group = "oc")
    private OcRefundApplyCancelOutStockCmd cancelOutStockCmd;

    /**
     * 更新退货申请单作废状态
     *
     * @param user   用户信息
     * @param billNo 退货申请单号
     * @return v14
     */
    public ValueHolder updateRefundApplyStatus(User user, String billNo) {
        return cancelOutStockCmd.cancelOutStockUpdateStatus(user, billNo);
    }
}
