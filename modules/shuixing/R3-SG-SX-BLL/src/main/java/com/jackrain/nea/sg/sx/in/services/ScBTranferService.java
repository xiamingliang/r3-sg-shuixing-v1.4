package com.jackrain.nea.sg.sx.in.services;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.transfer.enums.SgTransferBillStatusEnum;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferDiffMapper;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferMapper;
import com.jackrain.nea.sg.transfer.model.table.ScBTransfer;
import com.jackrain.nea.sg.transfer.model.table.ScBTransferDiff;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class ScBTranferService {
    @Autowired
    ScBTransferMapper scBTransferMapper;
    @Autowired
    ScBTransferDiffMapper scBTransferDiffMapper;

    public ValueHolderV14<ScBTransfer> queryScBTransferByNo(String applyBillNo) {
        ValueHolderV14 valueHolderV14 = new ValueHolderV14();
        ScBTransfer scBTransBill = scBTransferMapper.selectOne(new QueryWrapper<ScBTransfer>().lambda().eq(ScBTransfer::getApplyBillNo, applyBillNo)
                .eq(ScBTransfer::getStatus, SgTransferBillStatusEnum.AUDITED_ALL_OUT_ALL_IN.getVal())
                .eq(ScBTransfer::getIsactive, "Y"));
        valueHolderV14.setData(scBTransBill);
        valueHolderV14.setCode(ResultCode.SUCCESS);
        return valueHolderV14;
    }

    public ValueHolderV14<ScBTransferDiff> queryScBTransferDiffByNo(String billNo) {
        ValueHolderV14 valueHolderV14 = new ValueHolderV14();
        ScBTransferDiff transferDiff = scBTransferDiffMapper.selectOne(new QueryWrapper<ScBTransferDiff>().lambda().eq(ScBTransferDiff::getBillNo, billNo)
                .eq(ScBTransferDiff::getIsactive, "Y"));
        valueHolderV14.setData(transferDiff);
        valueHolderV14.setCode(ResultCode.SUCCESS);
        return valueHolderV14;
    }

}
