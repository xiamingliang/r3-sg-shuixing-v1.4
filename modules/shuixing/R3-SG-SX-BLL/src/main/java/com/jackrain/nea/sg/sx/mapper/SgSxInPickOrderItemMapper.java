package com.jackrain.nea.sg.sx.mapper;

import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.in.model.table.SgBInPickorderItem;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author: xiWen.z
 * create at: 2019/12/19 0019
 */
@Mapper
public interface SgSxInPickOrderItemMapper extends ExtentionMapper<SgBInPickorderItem> {

    @Select("SELECT * FROM SG_B_IN_PICKORDER_ITEM WHERE SG_B_IN_PICKORDER_ID=#{pId}")
    List<SgBInPickorderItem> selectInpickOrderItemList(@Param("pId") Long pId);
}
