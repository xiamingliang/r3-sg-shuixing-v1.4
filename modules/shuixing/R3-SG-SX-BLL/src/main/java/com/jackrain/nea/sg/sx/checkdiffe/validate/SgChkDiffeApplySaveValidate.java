package com.jackrain.nea.sg.sx.checkdiffe.validate;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.sg.sx.checkdiffe.common.SgChkDiffeConstants;
import com.jackrain.nea.tableService.MainTableRecord;
import com.jackrain.nea.tableService.TableServiceContext;
import com.jackrain.nea.tableService.validate.BaseValidator;
import com.jackrain.nea.utils.AssertUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author chenxinxing
 * @since 2020/2/21 14:36
 */

@Slf4j
@Component
public class SgChkDiffeApplySaveValidate  extends BaseValidator {
    @Override
    public void validate(TableServiceContext context, MainTableRecord mainTableRecord) {
        JSONObject origData = mainTableRecord.getOrignalData();
        Integer status = origData.getInteger("STATUS");
        AssertUtils.cannot(status == SgChkDiffeConstants.SGCHKDIFFE_STATUS_AUDIT, "单据已审核，不允许保存！", context.getLocale());
        AssertUtils.cannot(status == SgChkDiffeConstants.SGCHKDIFFE_STATUS_VOID, "单据已作废，不允许保存！", context.getLocale());
        AssertUtils.cannot(status == SgChkDiffeConstants.SGCHKDIFFE_STATUS_ERP_RETUEN, "单据ERP回传，不允许保存！", context.getLocale());
    }
}
