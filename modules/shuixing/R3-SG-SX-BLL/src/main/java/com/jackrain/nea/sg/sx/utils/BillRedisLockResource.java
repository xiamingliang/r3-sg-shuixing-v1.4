package com.jackrain.nea.sg.sx.utils;

/**
 * Redis key
 *
 * @author: xiWen.z
 * create at: 2019/12/25 0025
 */
public class BillRedisLockResource {

    public static String buildLockInPickOrderKey(long inPickOrderId) {

        return "sg:in:pickOrder" + inPickOrderId;
    }
}
