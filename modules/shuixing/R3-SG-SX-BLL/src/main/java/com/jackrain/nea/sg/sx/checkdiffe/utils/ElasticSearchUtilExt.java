package com.jackrain.nea.sg.sx.checkdiffe.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.es.util.ElasticSearchUtil;
import com.jackrain.nea.redis.util.RedisOpsUtil;
import com.jackrain.nea.sg.sx.checkdiffe.common.R3ElasticResultConstants;
import com.jackrain.nea.utils.AssertUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Slf4j
public class ElasticSearchUtilExt {

    public static Long getIdViaBillNo (String index, String cachePrefix, String billNo) {
        Long objId = null;
        try {
            String objIdStr = RedisOpsUtil.<String, String>getStrRedisTemplate().opsForValue().get(cachePrefix + billNo);
            if (objIdStr != null) {
                objId = Long.valueOf(objIdStr);
                return objId;
            }
        } catch (Exception e) {
            log.error(ElasticSearchUtilExt.class.getName()+".getIdViaBillNo.getIdFromRedis.error", e);
        }
        JSONObject whereKeys = new JSONObject();
        whereKeys.put("BILL_NO", billNo);
        String[] returnFileds = {"ID"};
        JSONArray dataArr = getObjects(index, whereKeys, returnFileds);
        if (CollectionUtils.isNotEmpty(dataArr)) {
            objId = dataArr.getJSONObject(0).getLongValue("ID");
            RedisOpsUtil.<String, String>getStrRedisTemplate().opsForValue().set(cachePrefix + billNo, objId+"", 1, TimeUnit.DAYS);
        }
        return objId;
    }

    public static  JSONArray getObjects(String index, JSONObject whereKeys, String[] returnFileds) {
        JSONObject searchResult = ElasticSearchUtil.search(index, index, whereKeys, new JSONObject(), new JSONArray(), 1, 0, returnFileds);
        if (searchResult.getInteger(R3ElasticResultConstants.KEY_CODE) != null &&
                searchResult.getInteger(R3ElasticResultConstants.KEY_CODE) != 0) {
            AssertUtils.logAndThrow("es查询失败," + searchResult.getString(R3ElasticResultConstants.KEY_MESSAGE));
        }

        if (log.isDebugEnabled()) {
            log.debug(ElasticSearchUtilExt.class.getName() + ",es查询结果:" + JSON.toJSONString(searchResult));
        }
        return searchResult.getJSONArray(R3ElasticResultConstants.KEY_DATA);
    }

    public static  JSONArray getObjects(String index, String type, JSONObject whereKeys, JSONObject filterKeys, String[] returnFileds, int range, int startIndex) {
        JSONObject searchResult = getSearchResult( index, type, whereKeys, filterKeys, returnFileds, range, startIndex);
        return searchResult.getJSONArray(R3ElasticResultConstants.KEY_DATA);
    }

    public static  JSONObject getSearchResult(String index, String type, JSONObject whereKeys, JSONObject filterKeys, String[] returnFileds, int range, int startIndex) {
        JSONArray orderKeys = new JSONArray();
        JSONObject asc = new JSONObject();
        asc.put("asc", true);
        asc.put("name", "ID");
        orderKeys.add(asc);
        JSONObject searchResult = ElasticSearchUtil.search(index, index, whereKeys, filterKeys, orderKeys, range, startIndex, returnFileds);
        if (searchResult.getInteger(R3ElasticResultConstants.KEY_CODE) != null &&
                searchResult.getInteger(R3ElasticResultConstants.KEY_CODE) != 0) {
            AssertUtils.logAndThrow("es查询失败," + searchResult.getString(R3ElasticResultConstants.KEY_MESSAGE));
        }
        if (log.isDebugEnabled()) {
            log.debug(ElasticSearchUtilExt.class.getName() + ",es查询结果:" + JSON.toJSONString(searchResult));
        }
        return searchResult;
    }

    public static int getSearchResultCount(String index, String type, JSONObject whereKeys, JSONObject filterKeys) {
        String[] returnFileds = {"ID"};
        JSONObject searchResult = getSearchResult( index, type, whereKeys, filterKeys, returnFileds, 1, 0);
        return searchResult.getInteger("total");
    }

    public static  List<Long> getObjectIds(String index, String type, JSONObject whereKeys, JSONObject filterKeys, int range, int startIndex) {
        String[] returnFileds = {"ID"};
        JSONArray results = getObjects(index, type, whereKeys, filterKeys, returnFileds, range, startIndex);
        if (CollectionUtils.isNotEmpty(results)) {
            return results.stream().map( s -> ((JSONObject)s).getLong("ID")).collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

    public static List<Long> getObjectIds(String index, JSONObject whereKeys, String[] returnFileds) {
        JSONArray results = getObjects(index, whereKeys, returnFileds);
        if (CollectionUtils.isNotEmpty(results)) {
            return results.stream().map( s -> ((JSONObject)s).getLong("ID")).collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

}
