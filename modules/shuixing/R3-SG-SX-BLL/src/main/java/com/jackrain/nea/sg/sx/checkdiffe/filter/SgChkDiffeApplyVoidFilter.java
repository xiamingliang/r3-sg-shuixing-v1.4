package com.jackrain.nea.sg.sx.checkdiffe.filter;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.sg.sx.checkdiffe.common.SgChkDiffeConstants;
import com.jackrain.nea.tableService.MainTableRecord;
import com.jackrain.nea.tableService.TableServiceContext;
import com.jackrain.nea.tableService.filter.BaseFilter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author chenxinxing
 * @description 盘差申请单做废
 * @since 2020/2/14 0:34
 */

@Slf4j
@Component
public class SgChkDiffeApplyVoidFilter extends BaseFilter {

    @Override
    public void before(TableServiceContext context, MainTableRecord row) {
        JSONObject commitData = row.getCommitData();
        commitData.put("STATUS", SgChkDiffeConstants.SGCHKDIFFE_STATUS_VOID);
        commitData.put("ISACTIVE", "N");
    }
}
