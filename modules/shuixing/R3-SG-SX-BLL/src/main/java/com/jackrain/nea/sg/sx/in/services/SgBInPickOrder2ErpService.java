package com.jackrain.nea.sg.sx.in.services;

import com.alibaba.fastjson.JSON;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.redis.tlock.RedisReentrantLock;
import com.jackrain.nea.sg.in.model.table.SgBInPickorder;
import com.jackrain.nea.sg.sx.in.model.enums.BillStatusEnums;
import com.jackrain.nea.sg.sx.in.model.resource.BuildRedisKeyResources;
import com.jackrain.nea.sg.sx.mapper.SgSxInPickOrderMapper;
import com.jackrain.nea.sg.sx.utils.BllRedisLockUtil;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * 查询待传ERP拣货单据
 *
 * @author: xiWen.z
 * create at: 2019/12/24 0024
 */
@Slf4j
@Component
public class SgBInPickOrder2ErpService {

    @Autowired
    private SgSxInPickOrderMapper sgSxInPickOrderMapper;

    @Autowired
    private BllRedisLockUtil bllRedisLockUtil;

    @Autowired
    private SgBInPickOrderTrans2MidSerivce sgBInPickOrderTrans2MidSerivce;

    /**
     * 查询待传ERP拣货单据
     *
     * @param size     拉取单量/轮
     * @param rootUser 操作用户
     * @return vh
     */
    public ValueHolderV14 inPickOrder4TransErp(int size, User rootUser) {

        ValueHolderV14 vh = new ValueHolderV14();

        /**
         * 1. 条件入参
         */
        List<SgBInPickorder> sgBInPickorders = sgSxInPickOrderMapper.listQueryInPickOrder(size);

        if (CollectionUtils.isEmpty(sgBInPickorders)) {
            vh.setCode(ResultCode.SUCCESS);
            vh.setMessage("未获取到符合数据");
            return vh;
        }

        if(log.isDebugEnabled()){
            log.debug(this.getClass().getName() + ",查询待传ERP拣货单据:" + sgBInPickorders.size());
        }

        int success = 0;
        int expCnt = 0;
        for (SgBInPickorder sgBInPickorder : sgBInPickorders) {
            Long id = sgBInPickorder.getId();
            if (id == null) {
                continue;
            }
            String redisKey = BuildRedisKeyResources.buildSgInOrderKey(id);
            RedisReentrantLock redisLock = new RedisReentrantLock(redisKey);
            try {
                if (redisLock.tryLock(bllRedisLockUtil.getLockOrderTimeOut(), TimeUnit.MILLISECONDS)) {
                    if (log.isDebugEnabled()) {
                        log.debug(this.getClass().getName() + ",采购回传Erp锁单,订单Id " + id);
                    }
                    int num = this.trans2ErpMidOrder(sgBInPickorder, rootUser);
                    if (num == 0) {
                        success++;
                    } else {
                        expCnt++;
                    }
                } else {
                    if (log.isDebugEnabled()) {
                        log.debug(this.getClass().getName() + "采购回传Erp,当前订单处于锁定状态, 订单Id " + id);
                    }
                }

            } catch (Exception e) {
                String exMsg = "";
                if (e != null) {
                    e.printStackTrace();
                    exMsg = e.getMessage();
                }
                if (log.isDebugEnabled()) {
                    log.debug(this.getClass().getName() + "采购回传Erp异常,订单Id" + id + ",异常信息 " + exMsg);
                }
            } finally {
                redisLock.unlock();
            }

        }
        vh.setCode(ResultCode.SUCCESS);
        vh.setMessage("拣货单传Erp中间表,本次搜寻条数" + sgBInPickorders.size() + ", MQ发送成功条数"
                + success + ", 异常条数" + expCnt);
        return vh;
    }

    /**
     * mq发送,更新状态
     *
     * @param sgBInPickorder 拣货单
     * @param rootUser       操作用户
     * @return int
     */
    private int trans2ErpMidOrder(SgBInPickorder sgBInPickorder, User rootUser) {

        int flag;
        BigDecimal transNum = sgBInPickorder.getFailNum() == null ? BigDecimal.ZERO : sgBInPickorder.getFailNum();
        ValueHolderV14 v = sgBInPickOrderTrans2MidSerivce.pickOrderTrans2MidService(sgBInPickorder, rootUser);
        SgBInPickorder inPick = new SgBInPickorder();
        inPick.setId(sgBInPickorder.getId());
        if (v.isOK()) {
            inPick.setTransStatus(BillStatusEnums.TransErpEnum.SUCCESS.toInt());
            int i = sgSxInPickOrderMapper.updateById(inPick);
            flag = 0;
            if (i < 1) {
                if(log.isDebugEnabled()){
                    log.debug(this.getClass().getName() + ",拣货单传Erp中间表成功, 更新数据状态失败, 更新结果 {}, 单据Id {}",
                            i, sgBInPickorder.getId());
                }
                flag = 1;
            }
        } else {
            inPick.setFailNum(transNum.add(BigDecimal.valueOf(1)));
            int i = sgSxInPickOrderMapper.updateById(inPick);
            if(log.isDebugEnabled()){
                log.debug(this.getClass().getName() + ",拣货单传Erp中间表Mq发送结果, {}, 更新数据结果 {}, 单据Id {}", v.getCode(),
                        i, sgBInPickorder.getId());
            }
            flag = -1;
        }
        return flag;
    }

}
