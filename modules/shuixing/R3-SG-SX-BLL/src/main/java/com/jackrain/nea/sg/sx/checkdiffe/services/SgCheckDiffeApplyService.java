package com.jackrain.nea.sg.sx.checkdiffe.services;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Lists;
import com.jackrain.nea.cpext.utils.ValueHolderUtils;
import com.jackrain.nea.oc.sale.api.OcTransLogCmd;
import com.jackrain.nea.oc.sale.model.table.OcBTransLog;
import com.jackrain.nea.redis.tlock.RedisReentrantLock;
import com.jackrain.nea.sg.sx.checkdiffe.common.SgChkDiffeConstants;
import com.jackrain.nea.sg.sx.checkdiffe.mapper.SgBChkDiffeApplyItemMapper;
import com.jackrain.nea.sg.sx.checkdiffe.mapper.SgBChkDiffeApplyMapper;
import com.jackrain.nea.sg.sx.checkdiffe.model.table.SgBChkDiffeApply;
import com.jackrain.nea.sg.sx.checkdiffe.model.table.SgBChkDiffeApplyItem;
import com.jackrain.nea.sg.sx.checkdiffe.utils.MapUtils;
import com.jackrain.nea.util.ValueHolder;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @author chenxinxing
 * @description 盘差单申请相关服务
 * @since 2020/2/18 19:55
 */

@Slf4j
@Component
public class SgCheckDiffeApplyService {

    @Autowired
    private SgBChkDiffeApplyMapper sgBChkDiffeApplyMapper;

    @Autowired
    private SgBChkDiffeApplyItemMapper sgBChkDiffeApplyItemMapper;

    @Reference(version = "1.0", group = "oc")
    OcTransLogCmd ocTransLogCmd;

    public ValueHolder syncCheckDiffeApply2Erp() {
        try {

            LambdaQueryWrapper<SgBChkDiffeApply> wrapper = new QueryWrapper<SgBChkDiffeApply>().lambda()
                    .eq(SgBChkDiffeApply::getStatus, SgChkDiffeConstants.SGCHKDIFFE_STATUS_AUDIT)
                    .eq(SgBChkDiffeApply::getTransStatus, SgChkDiffeConstants.TRANS_STATUS_N);
            Integer total = sgBChkDiffeApplyMapper.selectCount(wrapper);
            if (total == 0) {
                return ValueHolderUtils.success("中台没有未传ERP中间表的盘差申请单");
            }
            List<SgBChkDiffeApply> records = sgBChkDiffeApplyMapper.selectList(wrapper);
            List<Long> sgBChkDiffeApplyIds = Lists.transform(records, o -> o.getId());
            List<SgBChkDiffeApplyItem> sgBChkDiffeApplyItems = sgBChkDiffeApplyItemMapper.selectList(new QueryWrapper<SgBChkDiffeApplyItem>().lambda().in(SgBChkDiffeApplyItem::getSgBChkDiffeApplyId, sgBChkDiffeApplyIds));
            Map<Long, List<SgBChkDiffeApplyItem>> sgBChkDiffeApplyId2sgBChkDiffeApplyItems = MapUtils.group2(sgBChkDiffeApplyItems, o -> o.getSgBChkDiffeApplyId());

            for (SgBChkDiffeApply sgBChkDiffeApply : records) {

                RedisReentrantLock lock = new RedisReentrantLock(SgChkDiffeConstants.SG_CHKDIFFE_KEY + sgBChkDiffeApply.getId());
                boolean locked = false;
                try {
                    locked = lock.tryLock(0L, TimeUnit.MILLISECONDS);
                    if (locked) {
                        Long id = sgBChkDiffeApply.getId();
                        String billNo = sgBChkDiffeApply.getBillNo();
                        Date billDate = sgBChkDiffeApply.getBillDate();
                        String cpCStoreEcode = sgBChkDiffeApply.getCpCStoreEcode();
                        String cpCCustomerEcode = sgBChkDiffeApply.getCpCCustomerEcode();
                        String storeWarehouse = sgBChkDiffeApply.getStoreWarehouse();
                        String statusName = sgBChkDiffeApply.getStatusName();
                        String remarkMain = sgBChkDiffeApply.getRemark();

                        List<SgBChkDiffeApplyItem> items = sgBChkDiffeApplyId2sgBChkDiffeApplyItems.get(id);

                        JSONArray detailList = new JSONArray();
                        for (SgBChkDiffeApplyItem sgBChkDiffeApplyItem : items) {
                            String psCProEcode = sgBChkDiffeApplyItem.getPsCProEcode();
                            Long qtyBook = sgBChkDiffeApplyItem.getQtyBook();
                            Long qtyActual = sgBChkDiffeApplyItem.getQtyActual();
                            Long qtyApply = sgBChkDiffeApplyItem.getQtyApply();
                            Long qtyDifference = sgBChkDiffeApplyItem.getQtyDifference();
                            String remark = sgBChkDiffeApplyItem.getRemark();

                            JSONObject itemJsonObject = new JSONObject();
                            itemJsonObject.put("tc_rux03", psCProEcode);
                            itemJsonObject.put("tc_rux04", qtyBook);
                            itemJsonObject.put("tc_rux05", qtyActual);
                            itemJsonObject.put("tc_rux06", qtyApply);
                            itemJsonObject.put("tc_rux07", qtyDifference);
                            itemJsonObject.put("tc_rux08", remark);
                            itemJsonObject.put("tc_ruxplant", cpCStoreEcode);
                            itemJsonObject.put("tc_ruxlegal", cpCCustomerEcode);
                            detailList.add(itemJsonObject);
                        }
                        JSONObject mianJsonObject = new JSONObject();
                        mianJsonObject.put("tc_ruw01", billNo);
                        mianJsonObject.put("tc_ruw02", billDate);
                        mianJsonObject.put("tc_ruw03", storeWarehouse);
                        mianJsonObject.put("tc_ruw04", statusName);
                        mianJsonObject.put("tc_ruw09", remarkMain);
                        mianJsonObject.put("tc_ruwplant", cpCStoreEcode);
                        mianJsonObject.put("tc_ruwlegal", cpCCustomerEcode);
                        mianJsonObject.put("tc_ruwacti", "Y");
                        mianJsonObject.put("tc_ruwuser", statusName);
                        mianJsonObject.put("dataList", detailList);

                        JSONObject origBillContent = new JSONObject();
                        origBillContent.put("data", mianJsonObject);
                        JSONObject service = new JSONObject();
                        service.put("name", "takeStock");
                        origBillContent.put("service", service);
                        OcBTransLog transLog = new OcBTransLog();
                        transLog.setOrigBillContent(origBillContent.toJSONString());
                        transLog.setBillType(7);
                        transLog.setSourceBillNo(billNo);
                        ocTransLogCmd.invoke(transLog);
                        sgBChkDiffeApply.setTransStatus(SgChkDiffeConstants.TRANS_STATUS_Y);
                        sgBChkDiffeApplyMapper.updateById(sgBChkDiffeApply);
                    } else {
                        if (log.isDebugEnabled()) {
                            log.debug(this.getClass().getName() + ".syncCheckDiffeApply2Erp." + sgBChkDiffeApply.getBillNo() + "当前单据正在同步ERP!");
                        }
                        continue;
                    }
                } finally {
                    if (locked) {
                        lock.unlock();
                    }
                }
            }
        } catch (Exception e) {
            log.error("盘差申请单同步异常:", e);
            return ValueHolderUtils.fail("盘差申请单同步异常:" + e.getMessage());
        }
        return ValueHolderUtils.success("盘差申请单同步成功");
    }
}
