package com.jackrain.nea.sg.sx.mapper;

import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.sx.erpstock.services.table.SgBErpFullInventory;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Mapper
public interface SgBErpFullInventoryMapper extends ExtentionMapper<SgBErpFullInventory> {

    @Select("select  sum(qty_storage)  qty_storage from  sg_b_erp_full_inventory  where  materials_no=#{materials_no}  and warehouse_no=#{warehouse_no}  and belongs_legal=#{belongs_legal}  group by  materials_no,warehouse_no,belongs_legal ")
    Long getInventoyByErpQty(@Param("materials_no") String materials_no,@Param("warehouse_no") String warehouse_no,@Param("belongs_legal") String belongs_legal);

    @Select("select  sum(qty_storage) qty, materials_no from  sg_b_erp_full_inventory where modifieddate>=#{modifieddate}  and warehouse_no in (${warehouse_nos}) group by materials_no")
    List<Map<String,Object>> getStoreQtyByTb(@Param("warehouse_nos") String warehouse_nos,@Param("modifieddate") Date modifieddate);





}