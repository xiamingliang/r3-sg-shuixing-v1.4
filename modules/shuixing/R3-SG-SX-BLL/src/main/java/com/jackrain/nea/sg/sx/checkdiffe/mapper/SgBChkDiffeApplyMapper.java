package com.jackrain.nea.sg.sx.checkdiffe.mapper;

import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.sx.checkdiffe.model.table.SgBChkDiffeApply;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SgBChkDiffeApplyMapper extends ExtentionMapper<SgBChkDiffeApply> {
}