package com.jackrain.nea.sg.sx.in.services;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.in.mapper.SgBInPickorderMapper;
import com.jackrain.nea.sg.in.model.table.SgBInPickorder;
import com.jackrain.nea.sg.sx.in.model.consts.SxBillConst;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 * @author: xiWen.z
 * create at: 2019/12/20 0020
 */
@Slf4j
@Component
public class SgBInPickOrderCallBackService {


    @Autowired
    private SgBInPickorderMapper sgBInPickorderMapper;

    /**
     * ERP 回传ERP入库单号
     *
     * @param jsnObj
     * @return
     */
    public ValueHolderV14 updatePickOrderWhenCallBack(JSONObject jsnObj) {

        ValueHolderV14 vh = new ValueHolderV14();

        /**
         * 1. validate
         */
        if (jsnObj == null) {
            vh.setCode(ResultCode.FAIL);
            vh.setMessage("参数为空");
            return vh;
        }
        Long id = jsnObj.getLong(SxBillConst.ERP_CALLBACK_ID);
        String billNo = jsnObj.getString(SxBillConst.ERP_BILL_NO);

        if (Objects.isNull(id) || Objects.isNull(billNo)) {
            vh.setCode(ResultCode.FAIL);
            vh.setMessage("参数出现空值");
            return vh;
        }

        SgBInPickorder sgBInPickorder = new SgBInPickorder();
        sgBInPickorder.setId(id);
        sgBInPickorder.setErpCallbackBillNo(billNo);
        try {
            int i = sgBInPickorderMapper.updateById(sgBInPickorder);
            if (i > ResultCode.SUCCESS) {
                vh.setCode(ResultCode.SUCCESS);
                vh.setMessage("success");
                return vh;
            }
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + "更新拣货单失败, 单据id " + id);
            }
            vh.setMessage("更新拣货单失败, 单据id " + id);
        } catch (Exception e) {
            String msg = " 更新拣货单异常, 单据id " + id;
            log.error(this.getClass().getName() + msg);
            vh.setMessage(msg);
        }
        vh.setCode(ResultCode.FAIL);
        return vh;
    }
}
