package com.jackrain.nea.sg.sx.utils;

import org.springframework.stereotype.Component;

/**
 * @author: xiWen.z
 * create at: 2019/12/29 0029
 */
@Component
public class BllRedisLockUtil {


    /**
     * 默认1分钟
     */
    private static final int LOCK_ORDER_AUTO_TIMEOUT = 0;


    /**
     * 获取单据锁定超时时间
     *
     * @return 单据锁定超时时间
     */
    public long getLockOrderTimeOut() {
        return LOCK_ORDER_AUTO_TIMEOUT;
    }

}
