package com.jackrain.nea.sg.sx.checkdiffe.filter;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.ac.api.AcBOperateCloseCtrlCmd;
import com.jackrain.nea.ac.model.request.AcBOperateCloseCtrlRequest;
import com.jackrain.nea.ac.model.table.AcBOperateClose;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.send.api.SgSendCmd;
import com.jackrain.nea.sg.send.common.SgSendConstantsIF;
import com.jackrain.nea.sg.send.model.request.SgSendBillSaveRequest;
import com.jackrain.nea.sg.send.model.request.SgSendImpItemSaveRequest;
import com.jackrain.nea.sg.send.model.request.SgSendItemSaveRequest;
import com.jackrain.nea.sg.send.model.request.SgSendSaveRequest;
import com.jackrain.nea.sg.send.model.result.SgSendBillSaveResult;
import com.jackrain.nea.sg.sx.checkdiffe.mapper.SgBChkDiffeApplyItemMapper;
import com.jackrain.nea.sg.sx.checkdiffe.mapper.SgBChkDiffeApplyMapper;
import com.jackrain.nea.sg.sx.checkdiffe.model.table.SgBChkDiffeApply;
import com.jackrain.nea.sg.sx.checkdiffe.model.table.SgBChkDiffeApplyItem;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.tableService.MainTableRecord;
import com.jackrain.nea.tableService.TableServiceContext;
import com.jackrain.nea.tableService.filter.BaseFilter;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author chenxinxing
 * @description 盘差申请单审核
 * @since 2020/2/13 22:36
 */

@Slf4j
@Component
public class SgChkDiffeApplySubmitFilter extends BaseFilter {

    @Reference(version = "1.0", group = "sg")
    private SgSendCmd sgSendCmd;

    @Override
    public void before(TableServiceContext context, MainTableRecord row) {
        Long objId = row.getId();

        SgBChkDiffeApplyMapper sgBChkDiffeApplyMapper = ApplicationContextHandle.getBean(SgBChkDiffeApplyMapper.class);
        SgBChkDiffeApply sgBChkDiffeApply = sgBChkDiffeApplyMapper.selectById(objId);
        Date billDate = sgBChkDiffeApply.getBillDate();
        if(billDate==null){
            AssertUtils.logAndThrow("单据时间为空!");
        }

        //关账控制
        String cpCStoreEcode = sgBChkDiffeApply.getCpCStoreEcode();
        AcBOperateCloseCtrlCmd acBOperateCloseCtrlCmd = (AcBOperateCloseCtrlCmd)
                ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                        AcBOperateCloseCtrlCmd.class.getName(), "ac", "1.0");

        AcBOperateCloseCtrlRequest acBOperateCloseCtrlRequest = new AcBOperateCloseCtrlRequest();
        acBOperateCloseCtrlRequest.setEcode(cpCStoreEcode);
        acBOperateCloseCtrlRequest.setCloseDate(billDate);
        ValueHolderV14<AcBOperateClose> acBOperateCloseValueHolderV14 = null;
        try {
            acBOperateCloseValueHolderV14 = acBOperateCloseCtrlCmd.queryAcBOperateCloseCtrl(acBOperateCloseCtrlRequest);
        } catch (NDSException e) {
            log.debug("acBOperateCloseCtrlCmd.queryAcBOperateCloseCtrl. ReceiveParams:params:{};"
                    + e.getMessage());
            AssertUtils.logAndThrow("调用查询关账服务异常");
        }
        if (!acBOperateCloseValueHolderV14.isOK()) {
            Date closeDate = acBOperateCloseValueHolderV14.getData().getCloseDate();
            if(closeDate==null){
                AssertUtils.logAndThrow("未设置关账日期!");
            }
            SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd");
            String closeDateStr = format.format(closeDate);
            AssertUtils.logAndThrow("此营运中心关账日期为" + closeDateStr + "，不允许再操作单据！");
        }

        SgBChkDiffeApplyItemMapper sgBChkDiffeApplyItemMapper = ApplicationContextHandle.getBean(SgBChkDiffeApplyItemMapper.class);
        List<SgBChkDiffeApplyItem> sgBChkDiffeApplyItems = sgBChkDiffeApplyItemMapper.selectList(new QueryWrapper<SgBChkDiffeApplyItem>().lambda().eq(SgBChkDiffeApplyItem::getSgBChkDiffeApplyId, objId));
        if(sgBChkDiffeApplyItems.isEmpty()){
            AssertUtils.logAndThrow( "明细为空,不允许审核！");
        }
    }

    @Override
    public void after(TableServiceContext context, MainTableRecord row) {
        User user = context.getUser();
        Long objId = row.getId();

        SgBChkDiffeApplyMapper sgBChkDiffeApplyMapper = ApplicationContextHandle.getBean(SgBChkDiffeApplyMapper.class);
        SgBChkDiffeApply sgBChkDiffeApply = sgBChkDiffeApplyMapper.selectById(objId);

        SgBChkDiffeApplyItemMapper sgBChkDiffeApplyItemMapper = ApplicationContextHandle.getBean(SgBChkDiffeApplyItemMapper.class);
        List<SgBChkDiffeApplyItem> sgBChkDiffeApplyItems = sgBChkDiffeApplyItemMapper.selectList(new QueryWrapper<SgBChkDiffeApplyItem>().lambda().eq(SgBChkDiffeApplyItem::getSgBChkDiffeApplyId, objId));


        saveSgSend(user,sgBChkDiffeApply,sgBChkDiffeApplyItems);

    }

    /**
     * 调用【新增或更新逻辑发货单】的服务
     *
     * @param user             用户信息
     * @param sgBChkDiffeApply      盘差单
     * @param sgBChkDiffeApplyItems 盘差单明细
     */
    public void saveSgSend(User user, SgBChkDiffeApply sgBChkDiffeApply, List<SgBChkDiffeApplyItem> sgBChkDiffeApplyItems) {
        SgSendBillSaveRequest sendBillSaveRequest = new SgSendBillSaveRequest();

        /** 主表信息 */
        SgSendSaveRequest sgSend = new SgSendSaveRequest();
        sgSend.setSourceBillType(SgConstantsIF.BILL_TYPE_CHKDIFFE);
        sgSend.setServiceNode(SgConstantsIF.SERVICE_NODE_CHKDIFFE_LOCK);
        sgSend.setBillStatus(SgSendConstantsIF.BILL_SEND_STATUS_CREATE);
        sgSend.setSourceBillId(sgBChkDiffeApply.getId());
        sgSend.setSourceBillNo(sgBChkDiffeApply.getBillNo());
        sendBillSaveRequest.setSgSend(sgSend);

        Long storeId = sgBChkDiffeApply.getCpCStoreId();
        String storeCode = sgBChkDiffeApply.getCpCStoreEcode();
        String storeName = sgBChkDiffeApply.getCpCStoreEname();

        /** 录入明细信息 */
        //com.jackrain.nea.sg.send.utils.SgStoreUtils.impRequestConvertMethod
        // AdParamUtil.getParam("r3.box.enable")  默认开启  明细必填
        List<SgSendImpItemSaveRequest> impItemList = new ArrayList<>();
        sgBChkDiffeApplyItems.forEach(item -> {
            SgSendImpItemSaveRequest sgSendImpItemSaveRequest = new SgSendImpItemSaveRequest();
            BeanUtils.copyProperties(item, sgSendImpItemSaveRequest);
            sgSendImpItemSaveRequest.setCpCStoreId(storeId);
            sgSendImpItemSaveRequest.setCpCStoreEcode(storeCode);
            sgSendImpItemSaveRequest.setCpCStoreEname(storeName);
            sgSendImpItemSaveRequest.setSourceBillItemId(item.getId());

            sgSendImpItemSaveRequest.setPsCSkuId(item.getPsCSkuId());
            sgSendImpItemSaveRequest.setPsCSkuEcode(item.getPsCSkuEcode());
            sgSendImpItemSaveRequest.setIsTeus(SgSendConstantsIF.IS_TEUS_N);
            sgSendImpItemSaveRequest.setPsCProId(item.getPsCProId());
            sgSendImpItemSaveRequest.setPsCProEcode(item.getPsCProEcode());
            sgSendImpItemSaveRequest.setPsCProEname(item.getPsCProEname());
            sgSendImpItemSaveRequest.setPsCSpec1Id(item.getPsCClrId());
            sgSendImpItemSaveRequest.setPsCSpec1Ecode(item.getPsCClrEcode());
            sgSendImpItemSaveRequest.setPsCSpec1Ename(item.getPsCClrEname());
            sgSendImpItemSaveRequest.setPsCSpec2Id(item.getPsCSizeId());
            sgSendImpItemSaveRequest.setPsCSpec2Ecode(item.getPsCSizeEcode());
            sgSendImpItemSaveRequest.setPsCSpec2Ename(item.getPsCSizeEname());
            sgSendImpItemSaveRequest.setQtyPreout(new BigDecimal(item.getQtyApply()));
            sgSendImpItemSaveRequest.setQty(new BigDecimal(item.getQtyApply()));
            sgSendImpItemSaveRequest.setQtySend(new BigDecimal(0));
            impItemList.add(sgSendImpItemSaveRequest);
        });
        sendBillSaveRequest.setImpItemList(impItemList);

        /** 明细信息 */
        List<SgSendItemSaveRequest> itemList = new ArrayList<>();
        sgBChkDiffeApplyItems.forEach(item -> {
            SgSendItemSaveRequest sendItemSaveRequest = new SgSendItemSaveRequest();
            BeanUtils.copyProperties(item, sendItemSaveRequest);
            sendItemSaveRequest.setCpCStoreId(storeId);
            sendItemSaveRequest.setCpCStoreEcode(storeCode);
            sendItemSaveRequest.setCpCStoreEname(storeName);
            sendItemSaveRequest.setSourceBillItemId(item.getId());
            sendItemSaveRequest.setPsCSkuId(item.getPsCSkuId());
            sendItemSaveRequest.setPsCSkuEcode(item.getPsCSkuEcode());

            sendItemSaveRequest.setGbcode(item.getGbcode());
            sendItemSaveRequest.setPsCProId(item.getPsCProId());
            sendItemSaveRequest.setPsCProEcode(item.getPsCProEcode());
            sendItemSaveRequest.setPsCProEname(item.getPsCProEname());
            sendItemSaveRequest.setPsCSpec1Id(item.getPsCClrId());
            sendItemSaveRequest.setPsCSpec1Ecode(item.getPsCClrEcode());
            sendItemSaveRequest.setPsCSpec1Ename(item.getPsCClrEname());
            sendItemSaveRequest.setPsCSpec2Id(item.getPsCSizeId());
            sendItemSaveRequest.setPsCSpec2Ecode(item.getPsCSizeEcode());
            sendItemSaveRequest.setPsCSpec2Ename(item.getPsCSizeEname());
            sendItemSaveRequest.setQtyPreout(new BigDecimal(item.getQtyApply().toString()));
            sendItemSaveRequest.setQty(new BigDecimal(item.getQtyApply().toString()));
            sendItemSaveRequest.setQtySend(BigDecimal.ZERO);
            itemList.add(sendItemSaveRequest);
        });
        sendBillSaveRequest.setItemList(itemList);

        sendBillSaveRequest.setPreoutWarningType(SgConstantsIF.PREOUT_RESULT_ERROR);
        sendBillSaveRequest.setUpdateMethod(SgConstantsIF.ITEM_UPDATE_TYPE_ALL);
        sendBillSaveRequest.setLoginUser(user);

        ValueHolderV14<SgSendBillSaveResult> saveSgBSendV14 = sgSendCmd.saveSgBSend(sendBillSaveRequest);
        if (saveSgBSendV14.getCode() == ResultCode.FAIL) {
            AssertUtils.logAndThrow("新增或更新逻辑发货单异常：" + saveSgBSendV14.getMessage(), user.getLocale());
        }
    }


}
