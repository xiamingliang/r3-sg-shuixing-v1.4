package com.jackrain.nea.sg.sx.checkdiffe.common;

public interface SgChkDiffeConstants {

    String SG_B_CHK_DIFFE_APPLY = "sg_b_chk_diffe_apply";  // 盘差单
    String SG_B_CHK_DIFFE_APPLY_ITEM = "sg_b_chk_diffe_apply_item";  // 盘差单明细

    /**
     * 盘差单-单据状态
     * 1.未审核
     * 2.已审核
     * 3.ERP回传
     * 4.已作废
     */
    int SGCHKDIFFE_STATUS_WAIT = 1;
    int SGCHKDIFFE_STATUS_AUDIT = 2;
    int SGCHKDIFFE_STATUS_ERP_RETUEN = 3;
    int SGCHKDIFFE_STATUS_VOID = 4;

    /**
     * 盘差单-传中间表状态
     * 1.未传
     * 2.传中间表成功
     * 3.传中间表失败
     */
    int SGCHKDIFFE_TRANS_STATUS_WAIT = 1;
    int SGCHKDIFFE_TRANS_STATUS_SUCCESS = 2;
    int SGCHKDIFFE_TRANS_STATUS_FAILURE = 3;

    /**
     * 传中间表状态
     */
    int TRANS_STATUS_N = 1;   // 未传
    int TRANS_STATUS_Y = 2;   // 已传

    String CHK_DIFFE_APPLY_INDEX = "sg_b_chk_diffe_apply";
    String CHK_DIFFE_APPLY_TYPE = "sg_b_chk_diffe_apply";

    /**
     * es父ID的key
     */
    String CHKDIFFE_PARENT_FIELD = "SC_B_CHKDIFFE_ID";


    String SG_CHKDIFFE_KEY = "SG:ChkDiffe:";
    /**
     *  采购入库回传
     *  门店类型
     */
    String STORE_TYPE="01";//直营门店
}
