package com.jackrain.nea.sg.sx.in.services;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.resource.SystemUserResource;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.in.mapper.SgBInPickorderMapper;
import com.jackrain.nea.sg.in.mapper.SgBInPickorderNoticeItemMapper;
import com.jackrain.nea.sg.in.mapper.SgBPhyInNoticesMapper;
import com.jackrain.nea.sg.in.model.request.SgPhyInPickUpGoodSaveRequest;
import com.jackrain.nea.sg.in.model.table.SgBInPickorder;
import com.jackrain.nea.sg.in.model.table.SgBInPickorderNoticeItem;
import com.jackrain.nea.sg.in.model.table.SgBPhyInNotices;
import com.jackrain.nea.sg.in.services.SgPhyInPickOrderGenerateService;
import com.jackrain.nea.sg.sx.in.model.request.SgPhyInPickOrderVoidRequest;
import com.jackrain.nea.sg.unpack.common.SgUnpackConstants;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author: 周琳胜
 * @since: 2019/12/12
 * create at : 2019/12/12 11:27
 */
@Slf4j
@Component
public class SgPhyConsignPickCancelService {

    public ValueHolderV14 consignPickCancel(JSONObject jsonObject) {
        ValueHolderV14 v14 = new ValueHolderV14<>();
        if (log.isDebugEnabled()) {
            log.debug("Start SgPhyConsignPickCancelService.consignPickCancel. ReceiveParams:params:{};"
                    + jsonObject);
        }

        //参数检验
        if (jsonObject == null) {
            throw new NDSException("request is null");
        }

//        String param = jsonObject.getString("param");
        JSONArray paramArr = jsonObject.getJSONArray("param");
        for (Object o : paramArr) {
            JSONObject object = JSONObject.parseObject(JSON.toJSONString(o));
            String param = object.getString("TY_NO");
            SgBInPickorderMapper pickorderMapper = ApplicationContextHandle.getBean(SgBInPickorderMapper.class);
            SgBInPickorder sgBInPickorder = pickorderMapper.selectOne(new QueryWrapper<SgBInPickorder>().lambda()
                    .eq(SgBInPickorder::getCheckNo, param)
                    .eq(SgBInPickorder::getIsactive, SgConstants.IS_ACTIVE_Y)
                    .eq(SgBInPickorder::getBillStatus, SgUnpackConstants.BILL_STATUS_NOT_AUDIT));
            if (sgBInPickorder == null) {
                v14.setCode(ResultCode.FAIL);
                v14.setMessage("未查到有效记录！");
                return v14;
            }

            /**
             * 	调用【作废入库拣货单服务】
             */
            // 调用作废拣货单服务
            SgPhyInPickOrderVoidService orderVoidService = ApplicationContextHandle.getBean(SgPhyInPickOrderVoidService.class);
            SgPhyInPickOrderVoidRequest sgPhyInPickOrderVoidRequest = new SgPhyInPickOrderVoidRequest();
            List<Long> ids = new ArrayList<>();
            ids.add(sgBInPickorder.getId());
            sgPhyInPickOrderVoidRequest.setIds(ids);
            sgPhyInPickOrderVoidRequest.setUser(SystemUserResource.getRootUser());
            ValueHolderV14<Long> holderV14 = orderVoidService.voidSgbBInPickorder(sgPhyInPickOrderVoidRequest);
            if (log.isDebugEnabled()) {
                log.debug("Start SgPhyConsignPickCancelService.consignPickCancel. voidPickOrder:params:{};"
                        + v14);
            }
            if (holderV14.isOK()) {
                /**
                 * 根据来源入库通知单明细查出原通知单生成新的拣货单
                 */
                SgBInPickorderNoticeItemMapper noticeItemMapper = ApplicationContextHandle.getBean(SgBInPickorderNoticeItemMapper.class);
                List<SgBInPickorderNoticeItem> pickorderNoticeItems = noticeItemMapper.selectList(new QueryWrapper<SgBInPickorderNoticeItem>().lambda()
                        .eq(SgBInPickorderNoticeItem::getSgBInPickorderId, sgBInPickorder.getId()));
                List<String> noticesBillNo = pickorderNoticeItems.stream().map(SgBInPickorderNoticeItem::getNoticeBillNo).collect(Collectors.toList());
                SgBPhyInNoticesMapper noticesMapper = ApplicationContextHandle.getBean(SgBPhyInNoticesMapper.class);
                List<SgBPhyInNotices> sgBPhyInNoticesList = noticesMapper.selectList(new QueryWrapper<SgBPhyInNotices>().lambda()
                        .in(SgBPhyInNotices::getBillNo, noticesBillNo)
                        .eq(SgBPhyInNotices::getIsactive, SgConstants.IS_ACTIVE_Y));
                List<Long> noticesIds = sgBPhyInNoticesList.stream().map(SgBPhyInNotices::getId).collect(Collectors.toList());

                // 调用拣货单新增服务
                noticesIds.forEach(x -> {
                    SgPhyInPickOrderGenerateService service = ApplicationContextHandle.getBean(SgPhyInPickOrderGenerateService.class);
                    SgPhyInPickUpGoodSaveRequest pickUpGoodSaveRequest = new SgPhyInPickUpGoodSaveRequest();
                    List<Long> noticeId = new ArrayList<>();
                    noticeId.add(x);
                    pickUpGoodSaveRequest.setInNoticeIdList(noticeId);
                    pickUpGoodSaveRequest.setLoginUser(SystemUserResource.getRootUser());
                    if (log.isDebugEnabled()) {
                        log.debug("Start SgPhyConsignPickCancelService.consignPickCancel. savePickOrder:params:{};"
                                + JSONObject.toJSONString(pickUpGoodSaveRequest));
                    }
                    ValueHolderV14<Long> valueHolderV14 = service.generatePhyInPickUpGood(pickUpGoodSaveRequest);
                });
            } else {
                return holderV14;
            }
        }

        v14.setMessage("取消成功!");
        v14.setCode(ResultCode.SUCCESS);
        return v14;
    }
}
