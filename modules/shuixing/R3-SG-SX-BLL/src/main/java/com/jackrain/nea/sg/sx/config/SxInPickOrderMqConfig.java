package com.jackrain.nea.sg.sx.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * @author: xiWen.z
 * create at: 2019/12/19 0019
 */
@Configuration
@Data
public class SxInPickOrderMqConfig {


    @Value("${r3.sg.sx.inpickorder.trans.mq.topic:TRANS_LOG_MESSAGE}")
    private String sendInPickOrderTransMqTopic;


    @Value("${r3.sg.sx.inpickorder.trans.mq.tag:trans_log_message}")
    private String sendInPickOrderTransMqTag;


}
