package com.jackrain.nea.sg.sx.in.services;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.Feature;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.psext.api.utils.JsonUtils;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.in.mapper.SgBInPickorderItemMapper;
import com.jackrain.nea.sg.in.mapper.SgBInPickorderMapper;
import com.jackrain.nea.sg.in.model.table.SgBInPickorder;
import com.jackrain.nea.sg.in.model.table.SgBInPickorderItem;
import com.jackrain.nea.sg.sx.in.model.request.SgPhyInOrderSaveRequest;
import com.jackrain.nea.sg.sx.utils.BillUtils;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.utils.AssertUtils;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.query.DefaultWebEvent;
import com.jackrain.nea.web.query.QuerySession;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @author: 周琳胜
 * @since: 2019/12/23
 * create at : 2019/12/23 11:20
 */
@Slf4j
@Component
public class SgPhyInOrderSaveService {

    private static final String TRANSFER_IN = "SC_B_TRANSFER_IN";
    private static final String TRANSFER_PICK_ITEM = "SC_B_TRANSFER_PICK_ITEM";
    private static final String IN_STORAGE = "SG_B_IN_STORAGE";
    private static final String IN_STORAGE_ITEM = "SG_B_IN_PICKORDER_ITEM";

    public ValueHolder save(QuerySession session) {
        ValueHolder valueHolder = new ValueHolder();
        DefaultWebEvent event = session.getEvent();
        JSONObject param = JSON.parseObject(JSON.toJSONStringWithDateFormat(event.getParameterValue("param"),
                "yyyy-MM-dd HH:mm:ss", SerializerFeature.WriteMapNullValue), Feature.OrderedField);
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + "入参 {}", param.toJSONString());
        }
        User user = session.getUser();
        Long id = param.getLong("objid");
        JSONObject fixColumn = param.getJSONObject("fixcolumn");

        // 兼容调拨入库单（SC_B_TRANSFER_IN、SC_B_TRANSFER_PICK_ITEM）
        String table = param.getString("table");
        if (TRANSFER_IN.equals(table)) {
            fixColumn.put(IN_STORAGE, fixColumn.getJSONObject(TRANSFER_IN));
            fixColumn.put(IN_STORAGE_ITEM, fixColumn.getJSONArray(TRANSFER_PICK_ITEM));
        }

        if (id == -1) {
            // 新增

        } else {
            // 修改
            if (update(id, fixColumn, user)) {
                valueHolder.put("code", ResultCode.SUCCESS);
                valueHolder.put("message", "成功！");
            }
        }

        return valueHolder;
    }


    private Boolean update(Long objId, JSONObject object, User user) {

        SgBInPickorderMapper pickOrderMapper = ApplicationContextHandle.getBean(SgBInPickorderMapper.class);
        SgBInPickorderItemMapper itemMapper = ApplicationContextHandle.getBean(SgBInPickorderItemMapper.class);
        SgPhyInOrderSaveRequest sgPhyInOrderSaveRequest = JsonUtils.jsonParseClass(object, SgPhyInOrderSaveRequest.class);
        if (sgPhyInOrderSaveRequest == null) {
            throw new NDSException("数据异常！");
        }

        SgBInPickorder pickOrderRequest = sgPhyInOrderSaveRequest.getSgBInPickorder();
        SgBInPickorder inPickOrder = pickOrderMapper.selectOne(new QueryWrapper<SgBInPickorder>().lambda().eq(SgBInPickorder::getId, objId));
        AssertUtils.notNull(inPickOrder, "当前记录已不存在，请刷新页面！", user.getLocale());

        // 获取组织编码和入库日期
        String destCode = inPickOrder.getCpCDestEcode();
        Date billDate = (pickOrderRequest != null && pickOrderRequest.getBillDate() != null) ? pickOrderRequest.getBillDate() : inPickOrder.getBillDate();

        // 关账日期check
        BillUtils billUtils = ApplicationContextHandle.getBean(BillUtils.class);
        billUtils.checkOperateCloseDate(destCode, billDate);

        // 更新明细
        List<SgBInPickorderItem> pickOrderItems = sgPhyInOrderSaveRequest.getSgBInPickorderItemList();
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + "是否更新明细{}", JSONObject.toJSONString(pickOrderItems));
        }
        if (CollectionUtils.isNotEmpty(pickOrderItems)) {
            pickOrderItems.forEach(x -> {
                SgBInPickorderItem item = itemMapper.selectOne(new QueryWrapper<SgBInPickorderItem>().lambda().eq(SgBInPickorderItem::getId, x.getId()));
                SgBInPickorderItem pickorderItem = new SgBInPickorderItem();
                pickorderItem.setId(x.getId());
                BigDecimal diff = item.getQtyNotice().subtract(x.getQtyIn());
                pickorderItem.setQtyDiff(diff);
                pickorderItem.setQtyIn(x.getQtyIn());
                StorageESUtils.setBModelDefalutDataByUpdate(pickorderItem, user);
                int i = itemMapper.updateById(pickorderItem);
                if (log.isDebugEnabled()) {
                    log.debug(this.getClass().getName() + "updateItemResult:{}", i);
                }
            });

        }

        // 更新主表
        if (pickOrderRequest != null) {
            SgBInPickorder pickOrder = new SgBInPickorder();
            pickOrder.setId(objId);
            pickOrder.setRemark(pickOrderRequest.getRemark());
            pickOrder.setBillDate(pickOrderRequest.getBillDate());
            StorageESUtils.setBModelDefalutDataByUpdate(pickOrder, user);
            int i = pickOrderMapper.updateById(pickOrder);
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + "updateResult:{}", i);
            }
        }

        return true;
    }

}
