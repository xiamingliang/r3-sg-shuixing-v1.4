package com.jackrain.nea.sg.sx.out.services;

import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.model.result.SgR3BaseResult;
import com.jackrain.nea.sg.out.common.SgOutConstantsIF;
import com.jackrain.nea.sg.out.model.request.SgPhyOutResultBillSaveRequest;
import com.jackrain.nea.sg.out.model.request.SgPhyOutResultItemSaveRequest;
import com.jackrain.nea.sg.out.services.SgPhyOutNoticesSaveService;
import com.jackrain.nea.sg.out.services.SgPhyOutResultSaveAndAuditService;
import com.jackrain.nea.sg.receive.model.result.SgReceiveBillSaveResult;
import com.jackrain.nea.sg.receive.services.SgReceiveSaveService;
import com.jackrain.nea.sg.send.model.result.SgSendBillSaveResult;
import com.jackrain.nea.sg.send.services.SgSendSaveService;
import com.jackrain.nea.sg.sx.out.model.request.SgOutStockRequest;
import com.jackrain.nea.sg.sx.out.model.result.SgOutStockResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;

/**
 * @Project R3-SG-ShuiXing-V1.4
 * @Description: 统一出库服务service
 * @Author: Created by Dean on 2020/6/2 10:48
 **/
@Slf4j
@Component
public class SgBOutStockService {

    @Autowired
    private SgSendSaveService sgSendSaveService;

    @Autowired
    private SgReceiveSaveService sgReceiveSaveService;

    @Autowired
    private SgPhyOutNoticesSaveService outNoticesSaveService;

    @Autowired
    private SgPhyOutResultSaveAndAuditService outResultSaveAndAuditService;

    @Transactional
    public ValueHolderV14<SgOutStockResult> outStock(SgOutStockRequest request) {
        ValueHolderV14<SgOutStockResult> vh = new ValueHolderV14<>();
        SgOutStockResult result = new SgOutStockResult();
        vh.setData(result);

        //逻辑发逻辑
        if (request.getSendBillSaveRequest()!= null) {
            ValueHolderV14<SgSendBillSaveResult> sgSendSaveResult = sgSendSaveService.saveSgBSend(request.getSendBillSaveRequest());
            result.setSendBillSaveResult(sgSendSaveResult.getData());
            if (!sgSendSaveResult.isOK()) {
                vh.setCode(ResultCode.FAIL);
                return vh;
            }
        }

        //逻辑收逻辑
        if (request.getReceiveBillSaveRequest()!= null) {
            ValueHolderV14<SgReceiveBillSaveResult> receiveSaveResult = sgReceiveSaveService.saveSgBReceive(request.getReceiveBillSaveRequest());
            result.setReceiveBillSaveResult(receiveSaveResult.getData());
            if (!receiveSaveResult.isOK()) {
                vh.setCode(ResultCode.FAIL);
                return vh;
            }
        }

        //出库通知单
        if (request.getOutNoticesBillSaveRequest()!= null) {
            ValueHolderV14<SgR3BaseResult> outNoticesSaveResult = outNoticesSaveService.saveSgPhyOutNotices(request.getOutNoticesBillSaveRequest());
            result.setOutNoticesSaveResult(outNoticesSaveResult.getData());
            if (!outNoticesSaveResult.isOK()) {
                vh.setCode(ResultCode.FAIL);
                return vh;
            }
        }

        //出库结果单
        if (request.getOutResultBillSaveRequest()!= null) {
            SgPhyOutResultBillSaveRequest outResultBillSaveRequest = request.getOutResultBillSaveRequest();
            SgR3BaseResult outNoticesSaveResult = result.getOutNoticesSaveResult();
            //添加出库通知单返回值给出库结果单
            long outNoticesId = outNoticesSaveResult.getDataJo().getLongValue("objid");
            String outNoticesBillNo = outNoticesSaveResult.getDataJo().getString("bill_no");
            HashMap outSkuMap = (HashMap) outNoticesSaveResult.getDataJo().get(SgOutConstantsIF.RETURN_OUT_NOTICES_ITEM_KEY);

            outResultBillSaveRequest.getOutResultRequest().setSgBPhyOutNoticesId(outNoticesId);
            outResultBillSaveRequest.getOutResultRequest().setSgBPhyOutNoticesBillno(outNoticesBillNo);


            List<SgPhyOutResultItemSaveRequest> outResultItemRequests = outResultBillSaveRequest.getOutResultItemRequests();

            if (CollectionUtils.isNotEmpty(outResultItemRequests)) {
                for (SgPhyOutResultItemSaveRequest outResultItemSaveRequest: outResultItemRequests) {
                    outResultItemSaveRequest.setSgBPhyOutNoticesItemId((Long) outSkuMap.get(outResultItemSaveRequest.getPsCSkuEcode()));
                }
            }

            ValueHolderV14 outResultAndAuditSaveResult = outResultSaveAndAuditService.saveOutResultAndAudit(outResultBillSaveRequest);
            if (!outResultAndAuditSaveResult.isOK()) {
                vh.setCode(ResultCode.FAIL);
                return vh;
            }
        }
        vh.setCode(ResultCode.SUCCESS);
        return vh;
    }


}
