package com.jackrain.nea.sg.sx.checkdiffe.filter;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.es.util.ElasticSearchUtil;
import com.jackrain.nea.redis.tlock.RedisReentrantLock;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.send.model.request.SgSendBillVoidRequest;
import com.jackrain.nea.sg.send.services.SgSendVoidService;
import com.jackrain.nea.sg.sx.checkdiffe.common.SgChkDiffeConstants;
import com.jackrain.nea.sg.sx.checkdiffe.mapper.SgBChkDiffeApplyMapper;
import com.jackrain.nea.sg.sx.checkdiffe.model.table.SgBChkDiffeApply;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.tableService.MainTableRecord;
import com.jackrain.nea.tableService.TableServiceContext;
import com.jackrain.nea.tableService.filter.BaseFilter;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * @author chenxinxing
 * @description 盘差申请单取消审核
 * @since 2020/2/25 14:19
 */

@Slf4j
@Component
@Deprecated
public class SgChkDiffeApplyUnSubmitFilter extends BaseFilter {

    @Override
    public void before(TableServiceContext context, MainTableRecord row) {
        JSONObject commitData = row.getCommitData();
        commitData.put("TRANS_STATUS", SgChkDiffeConstants.SGCHKDIFFE_TRANS_STATUS_WAIT);
    }

    @Override
    public void after(TableServiceContext context, MainTableRecord row) {
        Long objId = row.getId();
        User user = context.getUser();

        SgBChkDiffeApplyMapper sgBChkDiffeApplyMapper = ApplicationContextHandle.getBean(SgBChkDiffeApplyMapper.class);
        SgBChkDiffeApply sgBChkDiffeApply = sgBChkDiffeApplyMapper.selectById(objId);

        RedisReentrantLock lock = new RedisReentrantLock(SgChkDiffeConstants.SG_CHKDIFFE_KEY + sgBChkDiffeApply.getId());
        boolean locked = false;
        try {
            locked = lock.tryLock(0L, TimeUnit.MILLISECONDS);
            if (locked) {
//                //推送ES
//                try {
//                    String index = SgChkDiffeConstants.CHK_DIFFE_APPLY_INDEX;
//                    String type = SgChkDiffeConstants.CHK_DIFFE_APPLY_TYPE;
//                    JSONObject updateObject = new JSONObject();
//                    updateObject.put("STATUS", SgChkDiffeConstants.SGCHKDIFFE_STATUS_WAIT);
//                    ElasticSearchUtil.updateDocument(index, type, updateObject, sgBChkDiffeApply.getId());
//                } catch (IOException e) {
//                    AssertUtils.logAndThrow("推送ES失败!");
//                }

                cancelSend(sgBChkDiffeApply, user);

            } else {
                AssertUtils.logAndThrow("当前单据正在同步ERP,不允许取消审核!");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (locked) {
                lock.unlock();
            }
        }

        /**
         * 取消逻辑发货单
         */
    }

    private void cancelSend(SgBChkDiffeApply sgBChkDiffeApply, User user) {
        SgSendVoidService voidService = ApplicationContextHandle.getBean(SgSendVoidService.class);
        SgSendBillVoidRequest request = new SgSendBillVoidRequest();
        request.setLoginUser(user);
        request.setSourceBillId(sgBChkDiffeApply.getId());
        request.setSourceBillNo(sgBChkDiffeApply.getBillNo());
        request.setSourceBillType(SgConstantsIF.BILL_TYPE_CHKDIFFE);
        request.setIsExist(true);
        ValueHolderV14 holderV14 = voidService.voidSgBSend(request);
        com.jackrain.nea.utils.AssertUtils.cannot(holderV14.getCode() == ResultCode.FAIL, "取消逻辑发货单失败:" + holderV14.getMessage());
    }
}
