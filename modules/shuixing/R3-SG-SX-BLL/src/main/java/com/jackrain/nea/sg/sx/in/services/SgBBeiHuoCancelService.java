package com.jackrain.nea.sg.sx.in.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.oc.basic.common.OcBasicConstants;
import com.jackrain.nea.oc.sale.api.OcSaleVoidRpcCmd;
import com.jackrain.nea.oc.sale.model.table.OcBSale;

import com.jackrain.nea.oc.sx.api.OcSaleCancelOutStockCmd;
import com.jackrain.nea.oc.sx.api.OcSaleSearchCmd;
import com.jackrain.nea.oc.sx.model.request.OcRequireOrderSearchRequest;
import com.jackrain.nea.redis.config.CusRedisTemplate;
import com.jackrain.nea.sg.basic.common.R3ParamConstants;
import com.jackrain.nea.sg.basic.model.table.SgBStorage;
import com.jackrain.nea.sg.basic.utils.R3ParamUtil;
import com.jackrain.nea.sg.in.mapper.SgBInPickorderMapper;
import com.jackrain.nea.sg.send.utils.SgStoreUtils;
import com.jackrain.nea.sg.transfer.api.SgTransferVoidR3Cmd;
import com.jackrain.nea.sg.transfer.common.ScTransferConstants;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferMapper;
import com.jackrain.nea.sg.transfer.model.table.ScBTransfer;
import com.jackrain.nea.sg.transfer.services.SgTransferVoidService;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.tableService.constants.Constants;
import com.jackrain.nea.tableService.service.TableServiceCmdService;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.query.DefaultWebEvent;
import com.jackrain.nea.web.query.QuerySession;
import com.jackrain.nea.web.query.QuerySessionImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * @author zh
 * @date 2019/12/24
 * 备货出库取消（总代、加盟、直营）
 */

@Slf4j
@Component
public class SgBBeiHuoCancelService {

    @Autowired
    ScBTranferCancelOutService scBTranferCancelOutService;

    @Autowired
    private TableServiceCmdService tableServiceCmdService;

    @Autowired
    SgTransferVoidR3Cmd sgTransferVoidR3Cmd;

    /**
     * 销售单取消出库的rediskey
     * 后端调用oc:sale:cancel:stock:type:销售单id(默认)
     */
    String OC_B_SALE_CANCEL_STOCK_TYPE="oc:sale:cancel:stock:type:";


    public ValueHolder cancel(QuerySession session) {


        ValueHolder valueHolderV14 = new ValueHolder();
        valueHolderV14.put("code",0);
        DefaultWebEvent event = session.getEvent();
        JSONObject param = JSONObject.parseObject(event.getParameterValue(R3ParamConstants.PARAM).toString());
        User user = session.getUser();
        if (null == param) {
            throw new NDSException("request is null");
        }
        String billNo = param.getString("billNo");
        if (!StringUtils.isNotEmpty(billNo)) {
            throw new NDSException("billNo 为 null");
        }
        Integer type = param.getInteger("type");
        log.debug("SgBBeiHuoCancelService==>billNo" + billNo);


        try{
            // 去查 调拨
            ScBTransferMapper mapper = ApplicationContextHandle.getBean(ScBTransferMapper.class);
            ScBTransfer transfer = mapper.selectOne(new QueryWrapper<ScBTransfer>().lambda().
                    eq(ScBTransfer::getBillNo, billNo));
            if (null != transfer) {
                QuerySession sessionToTran = new QuerySessionImpl(user);
                DefaultWebEvent eventToTran = new DefaultWebEvent("deal", new HashMap(16));
                JSONObject paramToTran = new JSONObject();
                paramToTran.put("objid", transfer.getId());
                eventToTran.put("param", paramToTran);
                sessionToTran.setEvent(eventToTran);
                scBTranferCancelOutService.execute(sessionToTran);

                // 调用 【作废调拨单服务】
                SgTransferVoidService service = ApplicationContextHandle.getBean(SgTransferVoidService.class);
                service.cancelTransfer(transfer.getId(), user);

            } else {
                // 去查 销售单
                OcSaleSearchCmd searchCmd = (OcSaleSearchCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                        OcSaleSearchCmd.class.getName(), "oc", "1.0");

                OcBSale ocBSale = searchCmd.getSaleInfo(billNo);
                log.debug("查到的 sale单"+JSONObject.toJSONString(ocBSale));
                if (null == ocBSale) {
                    throw new NDSException("未找到SALE单");
                }
                Long objid = ocBSale.getId();

                // 取消 销售出库
                OcSaleCancelOutStockCmd updateCmd = (OcSaleCancelOutStockCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                        OcSaleCancelOutStockCmd.class.getName(), "oc", "1.0");
                QuerySession sessionToSale = new QuerySessionImpl(user);
                DefaultWebEvent eventToSale = new DefaultWebEvent("deal", new HashMap(16));
                JSONObject paramToSale = new JSONObject();
                paramToSale.put("objid", objid);
                eventToSale.put("param", paramToSale);
                sessionToSale.setEvent(eventToSale);
                ValueHolder valueHolder=    updateCmd.execute(sessionToSale);
                log.debug("取消 销售出库===>"+valueHolder.toJSONObject());
                if(valueHolder.isOK()){
                    //调用销售单作废逻辑
                    JSONObject paramToSaleVoid = new JSONObject();
                    paramToSaleVoid.put(OcBasicConstants.OBJID, objid);
                    paramToSaleVoid.put(OcBasicConstants.TABLE, Constants.OC_B_SALE);
                    DefaultWebEvent eventToSaleVoid = new DefaultWebEvent(Constants.EVENT_VOID, new HashMap());
                    eventToSaleVoid.put(OcBasicConstants.PARAM, paramToSaleVoid);
                    QuerySession sessionToSaleVoid = new QuerySessionImpl(user);
                    sessionToSaleVoid.setEvent(eventToSaleVoid);
                    OcSaleVoidRpcCmd voidRpcCmd = (OcSaleVoidRpcCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                            OcSaleVoidRpcCmd.class.getName(), "oc", "1.0");
                    ValueHolder valueHolder1=    voidRpcCmd.execute(sessionToSaleVoid);
                    if(!valueHolder1.isOK()){
                        valueHolderV14.put("code",-1);
                        valueHolderV14.put("message",valueHolder1);
                        return  valueHolder1;
                    }
                    log.debug("调用销退单作废逻辑===>"+valueHolder1.toJSONObject());
                }
            }
        }catch (Exception E){
            log.debug("备货取消失败===》"+E);
            valueHolderV14.put("code",-1);
            valueHolderV14.put("message",E.getMessage());
        }


        return valueHolderV14;

    }

}
