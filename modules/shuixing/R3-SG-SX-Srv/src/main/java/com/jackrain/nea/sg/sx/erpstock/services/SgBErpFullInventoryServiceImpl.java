package com.jackrain.nea.sg.sx.erpstock.services;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.sg.sx.erpstock.services.api.SgBErpFullInventoryCmd;
import com.jackrain.nea.sg.sx.erpstock.services.request.SgBErpFullInventoryRequest;
import com.jackrain.nea.sg.sx.erpstock.services.table.SgBErpFullInventory;
import com.jackrain.nea.sys.CommandAdapter;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgBErpFullInventoryServiceImpl  extends CommandAdapter implements SgBErpFullInventoryCmd {
    @Autowired
    private  SgBErpFullInventoryService sgBErpFullInventoryService;

    @Override
    public ValueHolderV14 SyncErpStockinfo(List<SgBErpFullInventory> reqData) {
        return sgBErpFullInventoryService.SyncSgErpFullInventory(reqData);
    }

    @Override
    public ValueHolderV14<List<List<SgBErpFullInventory>>> getErpStockInfo(JSONArray array) {
        return sgBErpFullInventoryService.getErpStockInfo(array);
    }

    @Override
    public ValueHolderV14<Long> getErpStockInfoByQty(JSONObject json) {
        return sgBErpFullInventoryService.getErpStockInfoByQty(json);
    }

    @Override
    public JSONArray getSgFullInventoryQtyByTb(JSONObject json) {

        return sgBErpFullInventoryService.getSgFullInventoryQtyByTb(json);
    }

}
