package com.jackrain.nea.sg.sx.in.services;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.sx.in.api.ScBTranferCancelOutCmd;
import com.jackrain.nea.sg.sx.in.api.SgBBeiHuoCancelCmd;
import com.jackrain.nea.sys.CommandAdapter;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.query.QuerySession;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author zh
 * @date 2019/12/24
 * 备货出库取消（总代、加盟、直营）
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgBBeiHuoCancelCmdImpl  extends CommandAdapter implements SgBBeiHuoCancelCmd {

    @Autowired
    private SgBBeiHuoCancelService sgBBeiHuoCancelService;


    @Override
    public ValueHolder execute(QuerySession session) throws NDSException {
        return sgBBeiHuoCancelService.cancel(session);
    }

}
