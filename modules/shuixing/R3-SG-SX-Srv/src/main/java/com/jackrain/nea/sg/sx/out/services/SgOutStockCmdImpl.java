package com.jackrain.nea.sg.sx.out.services;

import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.sx.out.api.SgOutStockCmd;
import com.jackrain.nea.sg.sx.out.model.request.SgOutStockRequest;
import com.jackrain.nea.sg.sx.out.model.result.SgOutStockResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

/**
 * @Project R3-SG-ShuiXing-V1.4
 * @Description: 统一的出库服务
 * @Author: Created by Dean on 2020/6/2 10:08
 **/
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgOutStockCmdImpl implements SgOutStockCmd {

    @Override
    public ValueHolderV14<SgOutStockResult> outStock(SgOutStockRequest request) {
        SgBOutStockService service = ApplicationContextHandle.getBean(SgBOutStockService.class);
        ValueHolderV14<SgOutStockResult> result = new ValueHolderV14<>();
        try {
            result = service.outStock(request);
        } catch (Exception e) {
            log.error(this.getClass().getName()+".outStock.error:", e);
            result.setCode(ResultCode.FAIL);
            result.setMessage(e.getMessage());
        }
        return result;
    }

}
