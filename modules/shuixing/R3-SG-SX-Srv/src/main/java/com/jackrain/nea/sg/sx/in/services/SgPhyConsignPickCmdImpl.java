package com.jackrain.nea.sg.sx.in.services;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.sg.sx.in.api.SgPhyConsignPickCmd;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

/**
 * @author: 周琳胜
 * @since: 2019/12/12
 * create at : 2019/12/12 11:25
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgPhyConsignPickCmdImpl implements SgPhyConsignPickCmd {

    @Override
    public ValueHolderV14 consignPick(JSONObject jsonObject) {
        SgPhyConsignPickService service = ApplicationContextHandle.getBean(SgPhyConsignPickService.class);
        return service.consignPick(jsonObject);
    }
}
