package com.jackrain.nea.sg.sx.in.services;

import com.alibaba.fastjson.JSON;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.basic.model.request.SgR3BaseRequest;
import com.jackrain.nea.sg.out.model.request.SgOutResultMQRequest;
import com.jackrain.nea.sg.sx.in.api.ScBTranferCmd;
import com.jackrain.nea.sg.transfer.model.table.ScBTransfer;
import com.jackrain.nea.sg.transfer.model.table.ScBTransferDiff;
import com.jackrain.nea.sg.transfer.services.ScTransferDiffService;
import com.jackrain.nea.sg.transfer.services.SgTransferOutResultService;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author cxx
 * @date 2019/12/26
 * 调拨单
 */

@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class ScBTranferCmdImpl implements ScBTranferCmd {

    @Autowired
    private ScBTranferService scBTranferService;
    @Autowired
    private ScTransferDiffService scTransferDiffService;


    @Override
    public ValueHolderV14 callSgTransferOutByOutResult(String messageBody) {

        SgOutResultMQRequest sendMsgRequest = JSON.parseObject(messageBody, SgOutResultMQRequest.class);

        SgTransferOutResultService service = ApplicationContextHandle.getBean(SgTransferOutResultService.class);
        ValueHolderV14 v14 = service.writeBackOutResult(sendMsgRequest, false);
        return v14;
    }

    @Override
    public ValueHolderV14 callSgTransferOutByTransferId(User user, Long objId) {
        SgTransferOutResultService service = ApplicationContextHandle.getBean(SgTransferOutResultService.class);
        return service.writeBackOutResultByTransfer(user, objId);
    }

    @Override
    public ValueHolderV14<ScBTransfer> queryScBTransferByNo(String applyBillNo) {
        return scBTranferService.queryScBTransferByNo(applyBillNo);
    }

    @Override
    public ValueHolderV14<ScBTransferDiff> queryScBTransferDiffByNo(String billNo) {
        return scBTranferService.queryScBTransferDiffByNo(billNo);
    }

    @Override
    public ValueHolderV14 scTransferDiff(SgR3BaseRequest request) throws NDSException {
        return scTransferDiffService.dealTransferDiff(request);
    }

}
