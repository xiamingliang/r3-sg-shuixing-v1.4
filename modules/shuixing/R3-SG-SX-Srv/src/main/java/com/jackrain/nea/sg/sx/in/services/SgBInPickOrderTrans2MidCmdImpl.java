package com.jackrain.nea.sg.sx.in.services;

import com.jackrain.nea.sg.in.model.table.SgBInPickorder;
import com.jackrain.nea.sg.sx.in.api.SgBInPickOrderTrans2MidCmd;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author: xiWen.z
 * create at: 2019/12/18 0018
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgBInPickOrderTrans2MidCmdImpl implements SgBInPickOrderTrans2MidCmd {

    @Autowired
    SgBInPickOrderTrans2MidSerivce sgBInPickOrderTrans2MidSerivce;


    @Override
    public ValueHolderV14 inPickOrderTrans2Mid(SgBInPickorder sgBInPickorder, User usr) {
        return sgBInPickOrderTrans2MidSerivce.pickOrderTrans2MidService(sgBInPickorder, usr);
    }
}
