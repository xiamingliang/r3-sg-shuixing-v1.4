package com.jackrain.nea.sg.sx.in.services;

import com.alibaba.fastjson.JSONObject;
import com.google.gson.JsonObject;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.sx.in.api.ScBTranferCancelOutCmd;

import com.jackrain.nea.sys.CommandAdapter;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.query.QuerySession;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author zh
 * @date 2019/12/11
 * 取消出库逻辑
 */

@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class ScBTranferCancelOutCmdImpl extends CommandAdapter implements ScBTranferCancelOutCmd {
    @Autowired
    private ScBTranferCancelOutService scBTranferCancelOutService;


    @Override
    public ValueHolder execute(QuerySession session) throws NDSException {
        return scBTranferCancelOutService.execute(session);
    }

    @Override
    public ValueHolder cancelOutTransfer(User user, Long id) {
        ValueHolder vh = null;
        try {
            vh =  scBTranferCancelOutService.cancelOutTransfer(user,id);
        } catch (Exception e) {
            vh = new ValueHolder();
            vh.put("code", ResultCode.FAIL);
            vh.put("message", "取消出库失败:"+e.getMessage());
        }
        return vh;
    }
}
