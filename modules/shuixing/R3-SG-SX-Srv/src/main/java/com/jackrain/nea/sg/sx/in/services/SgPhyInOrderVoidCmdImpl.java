package com.jackrain.nea.sg.sx.in.services;

import com.jackrain.nea.sg.sx.in.api.SgPhyInPickOrderVoidCmd;
import com.jackrain.nea.sg.sx.in.model.request.SgPhyInPickOrderVoidRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

/**
 * @author zhu lin yu
 * @since 2019-11-20
 * create at : 2019-11-20 16:51
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgPhyInOrderVoidCmdImpl implements SgPhyInPickOrderVoidCmd {


    @Override
    public ValueHolderV14 voidSgbBInPickorder(SgPhyInPickOrderVoidRequest request) {
        SgPhyInPickOrderVoidService service = ApplicationContextHandle.getBean(SgPhyInPickOrderVoidService.class);
        return service.voidSgbBInPickorder(request);
    }
}
