package com.jackrain.nea.sg.sx.checkdiffe.services;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.mapper.CpCPhyWarehouseMapper;
import com.jackrain.nea.sg.basic.model.table.CpCPhyWarehouse;
import com.jackrain.nea.sg.pack.common.SgTeusPackConstants;
import com.jackrain.nea.sg.phyadjust.common.SgPhyAdjustConstants;
import com.jackrain.nea.sg.phyadjust.common.SgPhyAdjustConstantsIF;
import com.jackrain.nea.sg.phyadjust.model.request.SgPhyAdjustBillSaveRequest;
import com.jackrain.nea.sg.phyadjust.model.request.SgPhyAdjustImpItemRequest;
import com.jackrain.nea.sg.phyadjust.model.request.SgPhyAdjustSaveRequest;
import com.jackrain.nea.sg.phyadjust.model.request.SgPhyAdjustStoreRequest;
import com.jackrain.nea.sg.phyadjust.services.SgPhyAdjSaveAndAuditService;
import com.jackrain.nea.sg.send.model.request.SgSendBillVoidRequest;
import com.jackrain.nea.sg.send.services.SgSendVoidService;
import com.jackrain.nea.sg.sx.checkdiffe.api.SgChkDiffeCmd;
import com.jackrain.nea.sg.sx.checkdiffe.common.SgChkDiffeConstants;
import com.jackrain.nea.sg.sx.checkdiffe.mapper.SgBChkDiffeApplyItemMapper;
import com.jackrain.nea.sg.sx.checkdiffe.mapper.SgBChkDiffeApplyMapper;
import com.jackrain.nea.sg.sx.checkdiffe.model.request.ChkDiffeReturnAuditRequest;
import com.jackrain.nea.sg.sx.checkdiffe.model.table.SgBChkDiffeApply;
import com.jackrain.nea.sg.sx.checkdiffe.model.table.SgBChkDiffeApplyItem;
import com.jackrain.nea.sg.sx.checkdiffe.utils.MapUtils;
import com.jackrain.nea.sys.CommandAdapter;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.utils.AssertUtils;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author chenxinxing
 * @description 盘差单接口
 * @since 2020/2/18 14:22
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgChkDiffeCmdImpl extends CommandAdapter implements SgChkDiffeCmd {

    @Override
    public ValueHolderV14 chkDiffeReturnAudit(ChkDiffeReturnAuditRequest chkDiffeReturnAuditRequest, User user) {
        ValueHolderV14 vh = new ValueHolderV14(ResultCode.SUCCESS, "success");
        try {
            String billNo = chkDiffeReturnAuditRequest.getChkDiffeReturnAudit().getBillNo();
            AssertUtils.notBlank(billNo,"盘差申请单回传单据编号为空!");
            List<ChkDiffeReturnAuditRequest.ChkDiffeReturnAuditItem> chkDiffeReturnAuditItems = chkDiffeReturnAuditRequest.getChkDiffeReturnAuditItems();

            SgBChkDiffeApplyMapper sgBChkDiffeApplyMapper = ApplicationContextHandle.getBean(SgBChkDiffeApplyMapper.class);
            SgBChkDiffeApply sgBChkDiffeApply = sgBChkDiffeApplyMapper.selectOne(new QueryWrapper<SgBChkDiffeApply>().lambda().eq(SgBChkDiffeApply::getBillNo,billNo));
            if (sgBChkDiffeApply != null) {
                if(SgChkDiffeConstants.SGCHKDIFFE_STATUS_AUDIT!=sgBChkDiffeApply.getStatus()){
                    vh.setCode(ResultCode.FAIL);
                    vh.setMessage("盘差申请单单据编号:"+billNo+"在中台中单据状态不是已审核状态!");
                    return vh;
                }

                SgBChkDiffeApplyItemMapper sgBChkDiffeApplyItemMapper = ApplicationContextHandle.getBean(SgBChkDiffeApplyItemMapper.class);
                List<SgBChkDiffeApplyItem> sgBChkDiffeApplyItems = sgBChkDiffeApplyItemMapper.selectList(new QueryWrapper<SgBChkDiffeApplyItem>().lambda().eq(SgBChkDiffeApplyItem::getSgBChkDiffeApplyId, sgBChkDiffeApply.getId()));

                if (!(chkDiffeReturnAuditItems.size()==sgBChkDiffeApplyItems.size())) {
                    AssertUtils.logAndThrow("回传审核中的商品编码总数与中台原盘差单明细总数不一致!");
                }

                //生成一张已审核的库存调整单
                ValueHolderV14 valueHolderV14 = generatePhyAdjustBill(sgBChkDiffeApply, sgBChkDiffeApplyItems, chkDiffeReturnAuditItems, user);
                if(!valueHolderV14.isOK()){
                    vh.setCode(ResultCode.FAIL);
                    vh.setMessage("盘差单回传审核失败!" + valueHolderV14.getMessage());
                }

                //取消逻辑发货单
                cancelSend(sgBChkDiffeApply, user);

                //更新盘差单表和子表
                updateCheckDiffed(sgBChkDiffeApply, sgBChkDiffeApplyMapper, sgBChkDiffeApplyItemMapper, sgBChkDiffeApplyItems);

            }else{
                vh.setCode(ResultCode.FAIL);
                vh.setMessage("在中台中未找到盘差申请单-单据编号:"+billNo);
            }
        } catch (Exception e) {
            vh.setCode(ResultCode.FAIL);
            vh.setMessage("盘差单回传审核失败!" + e.getMessage());
        }
        return vh;
    }

    private void updateCheckDiffed(SgBChkDiffeApply sgBChkDiffeApply, SgBChkDiffeApplyMapper sgBChkDiffeApplyMapper, SgBChkDiffeApplyItemMapper sgBChkDiffeApplyItemMapper, List<SgBChkDiffeApplyItem> sgBChkDiffeApplyItems) {
        //更改盘差单状态和子表审批数量、差异数量
        sgBChkDiffeApply.setStatus(SgChkDiffeConstants.SGCHKDIFFE_STATUS_ERP_RETUEN);
        sgBChkDiffeApply.setModifieddate(new Date());
        sgBChkDiffeApplyMapper.updateById(sgBChkDiffeApply);
        for (SgBChkDiffeApplyItem sgBChkDiffeApplyItem : sgBChkDiffeApplyItems) {
            Long qtyApply = sgBChkDiffeApplyItem.getQtyApply();

            //在生成一张已审核的库存调整单时  塞入qtyStatus、opinion
            Long qtyStatus = sgBChkDiffeApplyItem.getQtyStatus();

            BigDecimal qtyApplyNum = new BigDecimal(qtyApply.toString());
            BigDecimal qtyStatusNum = new BigDecimal(qtyStatus.toString());
            BigDecimal qtyDiffeRenceNum = qtyApplyNum.subtract(qtyStatusNum);
            sgBChkDiffeApplyItem.setQtyDifference(qtyDiffeRenceNum.longValue());

            sgBChkDiffeApplyItemMapper.updateById(sgBChkDiffeApplyItem);
        }
    }

    /**
     * 取消逻辑发货单
     *
     * @param sgBChkDiffeApply
     * @param user
     */
    private void cancelSend(SgBChkDiffeApply sgBChkDiffeApply, User user) {
        SgSendVoidService voidService = ApplicationContextHandle.getBean(SgSendVoidService.class);
        SgSendBillVoidRequest request = new SgSendBillVoidRequest();
        request.setLoginUser(user);
        request.setSourceBillId(sgBChkDiffeApply.getId());
        request.setSourceBillNo(sgBChkDiffeApply.getBillNo());
        request.setSourceBillType(SgConstantsIF.BILL_TYPE_CHKDIFFE);
        request.setIsExist(true);
        ValueHolderV14 holderV14 = voidService.voidSgBSend(request);
        AssertUtils.cannot(holderV14.getCode() == ResultCode.FAIL, "取消逻辑发货单失败:" + holderV14.getMessage());
    }

    /**
     * 生成一张已审核的库存调整单
     *
     * @param sgBChkDiffeApply
     * @param user
     */
    private ValueHolderV14 generatePhyAdjustBill(SgBChkDiffeApply sgBChkDiffeApply, List<SgBChkDiffeApplyItem> sgBChkDiffeApplyItems, List<ChkDiffeReturnAuditRequest.ChkDiffeReturnAuditItem> chkDiffeReturnAuditItems, User user) {
        SgPhyAdjSaveAndAuditService saveAndAuditService = ApplicationContextHandle.getBean(SgPhyAdjSaveAndAuditService.class);
        SgPhyAdjustBillSaveRequest request = packparam(sgBChkDiffeApply, sgBChkDiffeApplyItems, chkDiffeReturnAuditItems);
        request.setLoginUser(user);
        ValueHolderV14 valueHolderV14 = saveAndAuditService.saveAndAudit(request);
        return valueHolderV14;
    }


    /**
     * 封装参数
     */
    private SgPhyAdjustBillSaveRequest packparam(SgBChkDiffeApply sgBChkDiffeApply, List<SgBChkDiffeApplyItem> sgBChkDiffeApplyItems, List<ChkDiffeReturnAuditRequest.ChkDiffeReturnAuditItem> chkDiffeReturnAuditItems) {
        SgPhyAdjustBillSaveRequest billSaveRequest = new SgPhyAdjustBillSaveRequest();

        //逻辑仓信息
        SgPhyAdjustStoreRequest sgPhyAdjustStoreRequest = new SgPhyAdjustStoreRequest();
        sgPhyAdjustStoreRequest.setCpCStoreId(sgBChkDiffeApply.getCpCStoreId());
        sgPhyAdjustStoreRequest.setCpCStoreEcode(sgBChkDiffeApply.getCpCStoreEcode());
        sgPhyAdjustStoreRequest.setCpCStoreEname(sgBChkDiffeApply.getCpCStoreEname());
        billSaveRequest.setStoreRequest(sgPhyAdjustStoreRequest);

        // 主表信息
        SgPhyAdjustSaveRequest sgPhyAdjustSaveRequest = new SgPhyAdjustSaveRequest();

        CpCPhyWarehouseMapper warehouseMapper = ApplicationContextHandle.getBean(CpCPhyWarehouseMapper.class);
        CpCPhyWarehouse cpCPhyWarehouse = warehouseMapper.selectByStoreId(sgBChkDiffeApply.getCpCStoreId());

        sgPhyAdjustSaveRequest.setCpCPhyWarehouseId(cpCPhyWarehouse.getId());
        sgPhyAdjustSaveRequest.setCpCPhyWarehouseEcode(cpCPhyWarehouse.getEcode());
        sgPhyAdjustSaveRequest.setCpCPhyWarehouseEname(cpCPhyWarehouse.getEname());
        sgPhyAdjustSaveRequest.setCpCStoreId(sgBChkDiffeApply.getCpCStoreId());
        sgPhyAdjustSaveRequest.setCpCStoreEcode(sgBChkDiffeApply.getCpCStoreEcode());
        sgPhyAdjustSaveRequest.setCpCStoreEname(sgBChkDiffeApply.getCpCStoreEname());
        sgPhyAdjustSaveRequest.setBillType(SgPhyAdjustConstantsIF.BILL_TYPE_NORMAL);
        sgPhyAdjustSaveRequest.setSgBAdjustPropId(SgPhyAdjustConstants.ADJUST_PROP_CHKDIFFE_ADJUST);
        sgPhyAdjustSaveRequest.setSourceBillType(SgPhyAdjustConstantsIF.SOURCE_BILL_TYPE_CHKDIFFE);
        sgPhyAdjustSaveRequest.setSourceBillId(sgBChkDiffeApply.getId());
        sgPhyAdjustSaveRequest.setSourceBillNo(sgBChkDiffeApply.getBillNo());
        sgPhyAdjustSaveRequest.setRemark("盘差单" + sgBChkDiffeApply.getBillNo() + "审核生成！");

        // 封装录入明细
        List<SgPhyAdjustImpItemRequest> impItemRequestList = new ArrayList<>();
        Map<String, ChkDiffeReturnAuditRequest.ChkDiffeReturnAuditItem> ecode2ChkDiffeReturnAuditItem = MapUtils.convert2Map(chkDiffeReturnAuditItems, o -> o.getECode());

        for (SgBChkDiffeApplyItem sgBChkDiffeApplyItem : sgBChkDiffeApplyItems) {
            SgPhyAdjustImpItemRequest impItemRequest = new SgPhyAdjustImpItemRequest();
            impItemRequest.setIsTeus(SgTeusPackConstants.IS_TEUS_N);
            impItemRequest.setId(-1L);
            impItemRequest.setSourceBillItemId(-1L);
            String psCSkuEcode = sgBChkDiffeApplyItem.getPsCSkuEcode();
            ChkDiffeReturnAuditRequest.ChkDiffeReturnAuditItem chkDiffeReturnAuditItem = ecode2ChkDiffeReturnAuditItem.get(psCSkuEcode);
            if (chkDiffeReturnAuditItem == null) {
                AssertUtils.logAndThrow("回传审核中不含商品编码:" + psCSkuEcode);
            }

            BigDecimal qtyDiff = chkDiffeReturnAuditItem.getQtyDiff();
            if(qtyDiff!=null){
                impItemRequest.setQty(qtyDiff);
            }else {
                AssertUtils.logAndThrow("回传审核中数量存在空值!");
            }
            impItemRequest.setPriceList(sgBChkDiffeApplyItem.getPriceList());
            impItemRequest.setPsCSkuId(sgBChkDiffeApplyItem.getPsCSkuId());
            impItemRequest.setPsCSkuEcode(psCSkuEcode);
            impItemRequest.setPsCProId(sgBChkDiffeApplyItem.getPsCProId());
            impItemRequest.setPsCProEcode(sgBChkDiffeApplyItem.getPsCProEcode());
            impItemRequest.setPsCProEname(sgBChkDiffeApplyItem.getPsCProEname());
            impItemRequest.setPsCSpec1Id(sgBChkDiffeApplyItem.getPsCClrId());
            impItemRequest.setPsCSpec1Ecode(sgBChkDiffeApplyItem.getPsCClrEcode());
            impItemRequest.setPsCSpec1Ename(sgBChkDiffeApplyItem.getPsCClrEname());
            impItemRequest.setPsCSpec2Id(sgBChkDiffeApplyItem.getPsCSizeId());
            impItemRequest.setPsCSpec2Ecode(sgBChkDiffeApplyItem.getPsCSizeEcode());
            impItemRequest.setPsCSpec2Ename(sgBChkDiffeApplyItem.getPsCSizeEname());
            impItemRequestList.add(impItemRequest);

            //塞入qtyStatus、opinion
            sgBChkDiffeApplyItem.setQtyStatus(qtyDiff.longValue());
            sgBChkDiffeApplyItem.setOpinion(chkDiffeReturnAuditItem.getRemarkDetail());
        }
        billSaveRequest.setObjId(-1L);
        billSaveRequest.setPhyAdjust(sgPhyAdjustSaveRequest);
        billSaveRequest.setImpItems(impItemRequestList);
        return billSaveRequest;
    }

}
