package com.jackrain.nea.sg.sx.in.services;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.sx.in.api.SgPhyInOrderAuditCmd;
import com.jackrain.nea.sys.CommandAdapter;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.query.QuerySession;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

/**
 * @author zhu lin yu
 * @since 2019-11-20
 * create at : 2019-11-20 16:51
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgPhyInOrderAuditCmdImpl extends CommandAdapter implements SgPhyInOrderAuditCmd {

    @Override
    public ValueHolder execute(QuerySession session) throws NDSException {
        SgPhyInOrderAuditServiceR3 service = ApplicationContextHandle.getBean(SgPhyInOrderAuditServiceR3.class);
        return service.auditR3(session);
    }
}
