package com.jackrain.nea.sg.sx.in.services;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.sg.sx.in.api.SgBInPickOrderUpdateByErpCallBackCmd;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author: xiWen.z
 * create at: 2019/12/20 0020
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgBInPickOrderUpdateByErpCallBackCmdImpl implements SgBInPickOrderUpdateByErpCallBackCmd {

    @Autowired
    SgBInPickOrderCallBackService sgBInPickOrderCallBackService;

    /**
     * @param jsnObj
     * @return
     */
    @Override
    public ValueHolderV14 modifyInPickOrderByErpCallBack(JSONObject jsnObj) {
        return sgBInPickOrderCallBackService.updatePickOrderWhenCallBack(jsnObj);
    }
}
