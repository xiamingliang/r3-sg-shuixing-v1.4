package com.jackrain.nea.sg.sx.task;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.sg.sx.checkdiffe.services.SgCheckDiffeApplyService;
import com.jackrain.nea.task.IR3Task;
import com.jackrain.nea.task.RunTaskResult;
import com.jackrain.nea.util.ValueHolder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author chenxinxing
 * @description 盘差单申请同步ERP Task
 * @since 2020/2/18 19:51
 */

@Slf4j
@Component
public class SynchronizeCheckDiffeApplyTask implements IR3Task {

    @Autowired
    private SgCheckDiffeApplyService sgCheckDiffeApplyService;

    @Override
    public RunTaskResult execute(JSONObject params) {
        log.info("=============SynchronizeCheckDiffeApplyTask beginning=====================");
        RunTaskResult result = new RunTaskResult();
        ValueHolder v = sgCheckDiffeApplyService.syncCheckDiffeApply2Erp();
        result.setSuccess(v.isOK());
        result.setMessage((String) v.get("message"));
        log.info("=============SynchronizeCheckDiffeApplyTask end=====================");
        return result;
    }
}
