package com.jackrain.nea.sg.sx.task;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Lists;
import com.jackrain.nea.model.util.AdParamUtil;
import com.jackrain.nea.resource.SystemUserResource;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.in.common.SgInNoticeConstants;
import com.jackrain.nea.sg.in.mapper.SgBInPickorderMapper;
import com.jackrain.nea.sg.in.mapper.SgBInPickorderNoticeItemMapper;
import com.jackrain.nea.sg.in.mapper.SgBPhyInNoticesMapper;
import com.jackrain.nea.sg.in.model.table.SgBInPickorder;
import com.jackrain.nea.sg.in.model.table.SgBInPickorderNoticeItem;
import com.jackrain.nea.sg.in.model.table.SgBPhyInNotices;
import com.jackrain.nea.sg.sx.in.model.request.SgPhyInPickOrderVoidRequest;
import com.jackrain.nea.sg.sx.in.services.SgPhyInPickOrderVoidService;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.task.IR3Task;
import com.jackrain.nea.task.RunTaskResult;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.util.Sets;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;

/**
 * @Classname SgPhyInPickOrderVoidTask
 * @Description 定时任务作废申购入库单服务
 * @Date 2020/4/3 15:32
 * @Created by chenxinxing
 */

@Slf4j
@Component
@Deprecated
public class SgPhyInPickOrderVoidTask implements IR3Task {

    @Autowired
    private SgPhyInPickOrderVoidService sgPhyInPickOrderVoidService;

    @Autowired
    private SgBInPickorderMapper sgBInPickorderMapper;

    @Autowired
    private SgBInPickorderNoticeItemMapper sgBInPickorderNoticeItemMapper;

    @Autowired
    private SgBPhyInNoticesMapper sgBPhyInNoticesMapper;


    @Override
    public RunTaskResult execute(JSONObject params) {
        RunTaskResult runTaskResult = new RunTaskResult();
        runTaskResult.setSuccess(Boolean.TRUE);

        //查询申购入库单的单据状态为未审核
        List<SgBInPickorder> sgBInPickorders = sgBInPickorderMapper.selectList(new QueryWrapper<SgBInPickorder>().lambda().select(SgBInPickorder::getId).eq(SgBInPickorder::getBillStatus, SgInNoticeConstants.AUDIT_STATUS_NO_AUDIT));
        AssertUtils.notNull(sgBInPickorders, "不存在申购入库单的单据状态为未审核的单据!");

        //查询来源入库通知单明细
        List<Long> sgBInPickorderIds = Lists.transform(sgBInPickorders, o -> o.getId());
        List<SgBInPickorderNoticeItem> sgBInPickorderNoticeItems = sgBInPickorderNoticeItemMapper.selectList(new QueryWrapper<SgBInPickorderNoticeItem>().lambda().select(SgBInPickorderNoticeItem::getNoticeBillNo).in(SgBInPickorderNoticeItem::getSgBInPickorderId, sgBInPickorderIds));
        AssertUtils.notNull(sgBInPickorderNoticeItems, "来源入库通知单明细记录为空!");

        List<String> noticeBillNos = Lists.transform(sgBInPickorderNoticeItems, o -> o.getNoticeBillNo());
        Set<String> noticeBillNosSet = Sets.newHashSet(noticeBillNos);

        //根据“通知单编码”查询入库通知单的状态为“全部入库”的数据
        List<SgBPhyInNotices> sgBPhyInNotices = sgBPhyInNoticesMapper.selectList(new QueryWrapper<SgBPhyInNotices>().lambda().select(SgBPhyInNotices::getId).in(SgBPhyInNotices::getBillNo, noticeBillNosSet).eq(SgBPhyInNotices::getBillStatus, SgInNoticeConstants.BILL_STATUS_IN_ALL));
        List<Long> sgBPhyInNoticesIds = Lists.transform(sgBPhyInNotices, o -> o.getId());

        //TODO
        //if(是否自动入库){
            String pickOrder = AdParamUtil.getParam("sg.pickorder.enable");
            AssertUtils.notBlank(pickOrder, "【是否自动生成入库拣货单】系统参数读取失败！" + pickOrder);
            Boolean auth = Boolean.parseBoolean(pickOrder);
            if (auth) {
                SgPhyInPickOrderVoidRequest request = new SgPhyInPickOrderVoidRequest();
                request.setIds(sgBPhyInNoticesIds);
                request.setUser(SystemUserResource.getRootUser());
                ValueHolderV14<Long> v14 = sgPhyInPickOrderVoidService.voidSgbBInPickorder(request);
                if (!v14.isOK()) {
                    runTaskResult.setSuccess(Boolean.FALSE);
                    runTaskResult.setMessage(v14.getMessage());
                }
            }
        //}
        return runTaskResult;
    }
}
