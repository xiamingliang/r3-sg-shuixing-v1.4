package com.jackrain.nea.sg.sx.task;

import com.alibaba.fastjson.JSONObject;
import com.ctrip.framework.apollo.Config;
import com.ctrip.framework.apollo.ConfigService;
import com.jackrain.nea.resource.SystemUserResource;
import com.jackrain.nea.sg.sx.in.model.consts.SxTaskConst;
import com.jackrain.nea.sg.sx.in.services.SgBInPickOrder2ErpService;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.task.IR3Task;
import com.jackrain.nea.task.RunTaskResult;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 采购入库单回传(直营)(拣货单)
 *
 * @author: xiWen.z
 * create at: 2019/12/18 0018
 */
@Slf4j
@Component
public class AutoTransInPickOrder2MidTask implements IR3Task {

    @Autowired
    private SgBInPickOrder2ErpService sgBInPickOrder2ErpQuerySerivce;

    @Override
    public RunTaskResult execute(JSONObject params) {
        RunTaskResult taskResult = new RunTaskResult();
        User rootUser = SystemUserResource.getRootUser();
        try {

            Config config = ConfigService.getConfig("application");
            Integer size = config.getIntProperty(SxTaskConst.TRANS_IN_PICK_RANGE_KEY, SxTaskConst.TRANS_IN_PICK_SIZE);
            Config inPickConfig = ConfigService.getConfig("application");
            boolean isOpen = inPickConfig.getBooleanProperty(SxTaskConst.TRANS_IN_PICK_SWITCH_KEY, true);
            if (isOpen) {
                if (log.isDebugEnabled()) {
                    log.debug(this.getClass().getName() + ",采购入库单回传,任务开始执行");
                }
                long l0 = System.currentTimeMillis();
                ValueHolderV14 vh = sgBInPickOrder2ErpQuerySerivce.inPickOrder4TransErp(size, rootUser);
                if (vh != null) {
                    if (log.isDebugEnabled()) {
                        log.debug(this.getClass().getName() + "Code{}, Msg{} ", vh.getCode(), vh.getMessage());
                    }
                } else {
                    log.error(this.getClass().getName() + ",采购入库单回传,任务执行结果为Null");
                }

                taskResult.setSuccess(true);
                long l1 = System.currentTimeMillis();
                if (log.isDebugEnabled()) {
                    log.debug(this.getClass().getName() + "执行时间-> " + (l1 - l0));
                }
            } else {
                if (log.isDebugEnabled()) {
                    log.debug(this.getClass().getName() + ",采购入库单回传,自动任务未开启");
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            log.error(this.getClass().getName() + ",采购入库单回传,任务发生异常:", ex);
            taskResult.setSuccess(false);
            taskResult.setMessage(ex.getMessage());
        }
        return taskResult;
    }
}
