package com.jackrain.nea.sg.sx.task;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.sg.sx.baisc.services.SgStorageSyncService;
import com.jackrain.nea.task.IR3Task;
import com.jackrain.nea.task.RunTaskResult;
import com.jackrain.nea.utility.ExceptionUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author dean
 * @description 中台同步库存到ERP
 * @since 2020/03/31 11:51
 */

@Slf4j
@Component
public class SynchronizeStoreStockTask implements IR3Task {

    @Autowired
    private SgStorageSyncService sgStorageSyncService;

    @Override
    public RunTaskResult execute(JSONObject params) {
        log.info("=============SynchronizeStoreStockTask beginning=====================");
        RunTaskResult result = new RunTaskResult();
        try {
            sgStorageSyncService.execute();
            result.setSuccess(true);
            result.setMessage("库存同步成功");
        } catch (Exception e) {
            log.error(this.getClass().getName()+".execute.error:", e);
            result.setSuccess(false);
            result.setMessage("库存同步失败, 异常信息为:"+ ExceptionUtil.getMessage(e));
        }
        log.info("=============SynchronizeStoreStockTask end=====================");
        return result;
    }
}
