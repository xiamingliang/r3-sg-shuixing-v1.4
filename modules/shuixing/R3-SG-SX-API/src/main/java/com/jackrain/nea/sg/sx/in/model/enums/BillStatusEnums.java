package com.jackrain.nea.sg.sx.in.model.enums;

/**
 * 单据链路状态.枚举
 *
 * @author: xiWen.z
 * create at: 2019/12/18 0018
 */
public class BillStatusEnums {


    /**
     * 传Erp状态
     */
    public enum TransErpEnum {

        NO(1, "未传"),
        SUCCESS(2, "已传"),
        FAIL(4, "失败"),
        ERROR(5, "错误");

        int val;
        String text;

        TransErpEnum(int k, String v) {
            this.val = k;
            this.text = v;
        }

        public int getVal() {
            return val;
        }

        public String getText() {
            return text;
        }

        /**
         * 值转换
         *
         * @return int
         */
        public int toInt() {
            switch (this) {
                case NO:
                    return 1;
                case SUCCESS:
                    return 2;
                case FAIL:
                    return 4;
                case ERROR:
                    return 5;
                default:
                    return -1;

            }
        }


    }


}
