package com.jackrain.nea.sg.sx.erpstock.services.request;



import com.jackrain.nea.sg.sx.erpstock.services.table.SgBErpFullInventory;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

import java.util.List;

@Data
@NoArgsConstructor
public class SgBErpFullInventoryRequest implements Serializable {

    List<SgBErpFullInventory> data;
}
