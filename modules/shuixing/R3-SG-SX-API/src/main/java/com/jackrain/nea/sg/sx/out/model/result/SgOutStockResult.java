package com.jackrain.nea.sg.sx.out.model.result;

import com.jackrain.nea.sg.basic.model.result.SgR3BaseResult;
import com.jackrain.nea.sg.receive.model.result.SgReceiveBillSaveResult;
import com.jackrain.nea.sg.send.model.result.SgSendBillSaveResult;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import lombok.experimental.Tolerate;

import java.io.Serializable;

/**
 * @Project R3-SG-ShuiXing-V1.4
 * @Description: 出库服务结果包装类
 * @Author: Created by Dean on 2020/6/2 11:41
 **/
@Data
@Builder
@Accessors(chain = true)
public class SgOutStockResult implements Serializable {

    @Tolerate
    public SgOutStockResult() {
        super();
    }

    private int operation = 0;

    private SgSendBillSaveResult sendBillSaveResult;

    private SgReceiveBillSaveResult receiveBillSaveResult;

    private SgR3BaseResult outNoticesSaveResult;

}
