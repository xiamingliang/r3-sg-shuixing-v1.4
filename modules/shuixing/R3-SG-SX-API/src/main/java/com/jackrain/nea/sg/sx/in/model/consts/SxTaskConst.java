package com.jackrain.nea.sg.sx.in.model.consts;

/**
 * 定时任务常量
 *
 * @author: xiWen.z
 * create at: 2019/12/18 0018
 */
public final class SxTaskConst {

    private SxTaskConst() {
    }


    /**
     * lts.key 采购回传数量.
     */
    public static final String TRANS_IN_PICK_RANGE_KEY = "lts.AutoTransInPickOrder2MidTask.range";

    /**
     * lts.value 采购回传数量
     */
    public static final int TRANS_IN_PICK_SIZE = 50;

    /**
     * lts.key 采购回传是否开启
     */
    public static final String TRANS_IN_PICK_SWITCH_KEY = "AutoTransInPickOrder2MidTask.isOpen";


    /**
     * 采购回传类型(拣货单)
     */
    public static final int IN_PICK_TYPE = 6;
}
