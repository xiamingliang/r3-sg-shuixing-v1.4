package com.jackrain.nea.sg.sx.in.model.enums;

/**
 * 作废状态
 *
 * @author: xiWen.z
 * create at: 2019/12/18 0018
 */
public enum OrderDelStatus {

    /**
     * 已作废
     */
    DELETED,

    /**
     * 未作废
     */
    UN_DELETED;

    /**
     * @return
     */
    public int intVal() {
        if (this == DELETED) {
            return 1;
        } else if (this == UN_DELETED) {
            return 0;
        } else {
            return -1;
        }
    }

}
