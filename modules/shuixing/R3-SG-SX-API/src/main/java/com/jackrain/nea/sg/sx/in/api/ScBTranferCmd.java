package com.jackrain.nea.sg.sx.in.api;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.basic.model.request.SgR3BaseRequest;
import com.jackrain.nea.sg.transfer.model.table.ScBTransfer;
import com.jackrain.nea.sg.transfer.model.table.ScBTransferDiff;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;

/**
 * @author cxx
 * @date 2019/12/26
 * 调拨单
 */
public interface ScBTranferCmd {

    /**
     * 备货出库（出库影响库存，根据结果单出库回写）
     *
     * @param messageBody 入参
     * @return v14
     */
    ValueHolderV14 callSgTransferOutByOutResult(String messageBody);

    /**
     * 备货出库（出库不影响库存，根据调拨单回写）
     *
     * @param user  用户信息
     * @param objId 调拨单ID
     * @return v14
     */
    ValueHolderV14 callSgTransferOutByTransferId(User user, Long objId);


    /**
     * 根据退货申请单号查询调拨单
     *
     * @param applyBillNo
     * @return
     */
    ValueHolderV14<ScBTransfer> queryScBTransferByNo(String applyBillNo);

    /**
     * 功能描述: 根据调拨单号查询调拨差异单
     * @Param: [billNo]
     * @Return:
     * @Author: Hunter
     * @Date: 2020/4/22 20:43
     */
    ValueHolderV14<ScBTransferDiff> queryScBTransferDiffByNo(String billNo);

    /**
     * 功能描述:退货申请调拨差异处理服务<br>
     * @Param: [request]
     * @Return: com.jackrain.nea.sys.domain.ValueHolderV14
     * @Author: Hunter
     * @Date: 2020/4/25 16:53
     */
    ValueHolderV14 scTransferDiff(SgR3BaseRequest request) throws NDSException;

}
