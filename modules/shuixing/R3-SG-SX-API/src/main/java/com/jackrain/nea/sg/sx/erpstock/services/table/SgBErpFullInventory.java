package com.jackrain.nea.sg.sx.erpstock.services.table;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.jackrain.nea.es.annotations.Document;
import com.jackrain.nea.es.annotations.Field;
import com.jackrain.nea.es.constans.FieldType;
import com.jackrain.nea.sys.domain.BaseModel;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;

@TableName(value = "sg_b_erp_full_inventory")
@Data
@Document(index = "sg_b_erp_full_inventory",type = "sg_b_erp_full_inventory")
public class SgBErpFullInventory extends BaseModel {
    @JSONField(name = "ID")
    @TableField(fill = FieldFill.INSERT)
    @TableId(value = "id", type = IdType.INPUT)
    @Field(type = FieldType.Long)
    private Long id;

    @JSONField(name = "MATERIALS_NO")
    @Field(type = FieldType.Keyword)
    private String materialsNo;

    @JSONField(name = "WAREHOUSE_NO")
    @Field(type = FieldType.Keyword)
    private String warehouseNo;

    @JSONField(name = "BIN_LOCATION")
    @Field(type = FieldType.Keyword)
    private String binLocation;

    @JSONField(name = "BATCH_NUMBER")
    @Field(type = FieldType.Keyword)
    private String batchNumber;

    @JSONField(name = "REFERENCE_NUMBER")
    @Field(type = FieldType.Keyword)
    private String referenceNumber;

    @JSONField(name = "REFERENCE_SEQUENCE")
    @Field(type = FieldType.Keyword)
    private String referenceSequence;

    @JSONField(name = "PROCUREMENT_UNIT")
    @Field(type = FieldType.Keyword)
    private String procurementUnit;

    @JSONField(name = "QTY_RECEIVE")
    @Field(type = FieldType.Double)
    private BigDecimal qtyReceive;

    @JSONField(name = "STORAGE_UNIT")
    @Field(type = FieldType.Double)
    private String storageUnit;

    @JSONField(name = "QTY_STORAGE")
    @Field(type = FieldType.Double)
    private BigDecimal qtyStorage;

    @JSONField(name = "CREATEDDATE")
    @Field(type = FieldType.Date)
    private Date createddate;

    @JSONField(name = "LAST_INVENTORY_DATE")
    @Field(type = FieldType.Date)
    private Date lastInventoryDate;

    @JSONField(name = "LAST_PORC_DATE")
    @Field(type = FieldType.Date)
    private Date lastPorcDate;

    @JSONField(name = "LAST_SHIPMENT_DATE")
    @Field(type = FieldType.Date)
    private Date lastShipmentDate;

    @JSONField(name = "LAST_DYSKINESIA_DATE")
    @Field(type = FieldType.Date)
    private Date lastDyskinesiaDate;

    @JSONField(name = "INTEVALIDDATE")
    @Field(type = FieldType.Date)
    private Date intevaliddate;

    @JSONField(name = "STORAGE_LEVEL")
    @Field(type = FieldType.Keyword)
    private String storageLevel;

    @JSONField(name = "UNIT_CONVERSION_RATE")
    @Field(type = FieldType.Double)
    private BigDecimal unitConversionRate;

    @JSONField(name = "MAT_UNIT_CONVERSION_RATE")
    @Field(type = FieldType.Double)
    private BigDecimal matUnitConversionRate;

    @JSONField(name = "WAREHOUSE_CATEGORY")
    @Field(type = FieldType.Keyword)
    private String warehouseCategory;

    @JSONField(name = "IS_USABLE_STORAGE")
    @Field(type = FieldType.Keyword)
    private String isUsableStorage;

    @JSONField(name = "IS_USABLE_MRP_STORAGE")
    @Field(type = FieldType.Keyword)
    private String isUsableMrpStorage;

    @JSONField(name = "IS_BONDED")
    @Field(type = FieldType.Keyword)
    private String isBonded;

    @JSONField(name = "WAREHOUSE_ACCOUNTS")
    @Field(type = FieldType.Keyword)
    private String warehouseAccounts;

    @JSONField(name = "WORK_ORDER_PRIORITY")
    @Field(type = FieldType.Integer)
    private Integer workOrderPriority;

    @JSONField(name = "SALES_DELIVERY_PRIORITY")
    @Field(type = FieldType.Integer)
    private Integer salesDeliveryPriority;

    @JSONField(name = "DIRECT_MATERIAL_COST")
    @Field(type = FieldType.Double)
    private BigDecimal directMaterialCost;

    @JSONField(name = "INDIRECT_MATERIAL_COST")
    @Field(type = FieldType.Double)
    private BigDecimal indirectMaterialCost;

    @JSONField(name = "OUT_MATERIALS_COST")
    @Field(type = FieldType.Double)
    private BigDecimal outMaterialsCost;

    @JSONField(name = "OUT_LABOR_COST")
    @Field(type = FieldType.Double)
    private BigDecimal outLaborCost;

    @JSONField(name = "SC_CONVERSION_RATE")
    @Field(type = FieldType.Double)
    private BigDecimal scConversionRate;

    @JSONField(name = "SPECIAL_CASE_NO")
    @Field(type = FieldType.Keyword)
    private String specialCaseNo;

    @JSONField(name = "GUISE_NO")
    @Field(type = FieldType.Keyword)
    private String guiseNo;

    @JSONField(name = "GLAZED_DATE")
    @Field(type = FieldType.Date)
    private Date glazedDate;

    @JSONField(name = "OPERATING_CENTRE")
    @Field(type = FieldType.Keyword)
    private String operatingCentre;

    @JSONField(name = "BELONGS_LEGAL")
    @Field(type = FieldType.Keyword)
    private String belongsLegal;

    @JSONField(name = "REMARK")
    @Field(type = FieldType.Keyword)
    private String remark;

    @JSONField(name = "OWNERENAME")
    @Field(type = FieldType.Keyword)
    private String ownerename;

    @JSONField(name = "MODIFIERENAME")
    @Field(type = FieldType.Keyword)
    private String modifierename;
}