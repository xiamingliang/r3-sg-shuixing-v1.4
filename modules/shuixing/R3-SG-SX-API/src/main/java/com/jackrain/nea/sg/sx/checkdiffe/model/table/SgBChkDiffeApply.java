package com.jackrain.nea.sg.sx.checkdiffe.model.table;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.jackrain.nea.es.annotations.Document;
import com.jackrain.nea.es.annotations.Field;
import com.jackrain.nea.es.constans.FieldType;
import com.jackrain.nea.sys.domain.BaseModel;
import java.util.Date;
import lombok.Data;

@TableName(value = "sg_b_chk_diffe_apply")
@Data
@Document(index = "sg_b_chk_diffe_apply",type = "sg_b_chk_diffe_apply")
public class SgBChkDiffeApply extends BaseModel {
    @JSONField(name = "ID")
    @TableField(fill = FieldFill.INSERT)
    @TableId(value = "id", type = IdType.INPUT)
    @Field(type = FieldType.Long)
    private Long id;

    @JSONField(name = "BILL_NO")
    @Field(type = FieldType.Keyword)
    private String billNo;

    @JSONField(name = "BILL_DATE")
    @Field(type = FieldType.Long)
    private Date billDate;

    @JSONField(name = "CP_C_STORE_ID")
    @Field(type = FieldType.Long)
    private Long cpCStoreId;

    @JSONField(name = "CP_C_STORE_ECODE")
    @Field(type = FieldType.Keyword)
    private String cpCStoreEcode;

    @JSONField(name = "CP_C_STORE_ENAME")
    @Field(type = FieldType.Keyword)
    private String cpCStoreEname;

    @JSONField(name = "CP_C_CUSTOMER_ID")
    @Field(type = FieldType.Long)
    private Long cpCCustomerId;

    @JSONField(name = "CP_C_CUSTOMER_ECODE")
    @Field(type = FieldType.Keyword)
    private String cpCCustomerEcode;

    @JSONField(name = "CP_C_CUSTOMER_ENAME")
    @Field(type = FieldType.Keyword)
    private String cpCCustomerEname;

    @JSONField(name = "BELONGS_LEGAL_ID")
    @Field(type = FieldType.Long)
    private Long belongsLegalId;

    @JSONField(name = "BELONGS_LEGAL_ECODE")
    @Field(type = FieldType.Keyword)
    private String belongsLegalEcode;

    @JSONField(name = "BELONGS_LEGAL")
    @Field(type = FieldType.Keyword)
    private String belongsLegal;

    @JSONField(name = "STORE_WAREHOUSE")
    @Field(type = FieldType.Keyword)
    private String storeWarehouse;

    @JSONField(name = "TRANS_STATUS")
    @Field(type = FieldType.Keyword)
    private Integer transStatus;

    @JSONField(name = "REMARK")
    @Field(type = FieldType.Keyword)
    private String remark;

    @JSONField(name = "VERSION")
    @Field(type = FieldType.Long)
    private Long version;

    @JSONField(name = "OWNERENAME")
    @Field(type = FieldType.Keyword)
    private String ownerename;

    @JSONField(name = "MODIFIERENAME")
    @Field(type = FieldType.Keyword)
    private String modifierename;

    @JSONField(name = "STATUS")
    @Field(type = FieldType.Keyword)
    private Integer status;

    @JSONField(name = "STATUS_ID")
    @Field(type = FieldType.Long)
    private Long statusId;

    @JSONField(name = "STATUS_ENAME")
    @Field(type = FieldType.Keyword)
    private String statusEname;

    @JSONField(name = "STATUS_NAME")
    @Field(type = FieldType.Keyword)
    private String statusName;

    @JSONField(name = "STATUS_TIME")
    @Field(type = FieldType.Long)
    private Date statusTime;

    @JSONField(name = "DELER_ID")
    @Field(type = FieldType.Long)
    private Long delerId;

    @JSONField(name = "DELER_NAME")
    @Field(type = FieldType.Keyword)
    private String delerName;

    @JSONField(name = "DELER_ENAME")
    @Field(type = FieldType.Keyword)
    private String delerEname;

    @JSONField(name = "DEL_TIME")
    @Field(type = FieldType.Long)
    private Date delTime;

    @JSONField(name = "UNCHECK_ID")
    @Field(type = FieldType.Long)
    private Long uncheckId;

    @JSONField(name = "UNCHECK_ENAME")
    @Field(type = FieldType.Keyword)
    private String uncheckEname;

    @JSONField(name = "UNCHECK_NAME")
    @Field(type = FieldType.Keyword)
    private String uncheckName;

    @JSONField(name = "UNCHECK_TIME")
    @Field(type = FieldType.Long)
    private Date uncheckTime;
}