package com.jackrain.nea.sg.sx.in.model.request;

import lombok.Data;

import java.io.Serializable;

/**
 * @author: 周琳胜
 * @since: 2019/12/12
 * create at : 2019/12/12 11:03
 */
@Data
public class SgPhyInNoticesDetailRequest implements Serializable{

    /**
     * 出货单号(备货单号)
     */
    private String bhNo;

    /**
     * 来源单据编号
     */
    private String sourceBillNo;
}

