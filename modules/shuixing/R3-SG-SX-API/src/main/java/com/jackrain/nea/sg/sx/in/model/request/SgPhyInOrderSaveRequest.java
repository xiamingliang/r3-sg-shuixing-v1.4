package com.jackrain.nea.sg.sx.in.model.request;

import com.alibaba.fastjson.annotation.JSONField;
import com.jackrain.nea.sg.in.model.table.SgBInPickorder;
import com.jackrain.nea.sg.in.model.table.SgBInPickorderItem;
import lombok.Data;

import java.util.List;

/**
 * @author: 周琳胜
 * @since: 2019/12/23
 * create at : 2019/12/23 20:14
 */
@Data
public class SgPhyInOrderSaveRequest {
    @JSONField(name = "objid")
    private Long objid;

    @JSONField(name = "SG_B_IN_STORAGE")
    private SgBInPickorder sgBInPickorder;

    @JSONField(name = "SG_B_IN_PICKORDER_ITEM")
    private List<SgBInPickorderItem> sgBInPickorderItemList;
}
