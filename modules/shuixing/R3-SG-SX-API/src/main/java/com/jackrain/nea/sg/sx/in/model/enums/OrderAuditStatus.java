package com.jackrain.nea.sg.sx.in.model.enums;

/**
 * 审核状态
 *
 * @author: xiWen.z
 * create at: 2019/12/18 0018
 */
public enum OrderAuditStatus {

    /**
     * 已审核
     */
    AUDITED,

    /**
     * 未审核
     */
    UN_AUDITED,

    /**
     * 已作废
     */
    VOID;

    /**
     * change to int value
     *
     * @return int
     */
    public int intVal() {
        if (this == AUDITED) {
            return 2;
        } else if (this == UN_AUDITED) {
            return 1;
        } else if (this == VOID) {
            return 3;
        } else {
            return -1;
        }
    }
}
