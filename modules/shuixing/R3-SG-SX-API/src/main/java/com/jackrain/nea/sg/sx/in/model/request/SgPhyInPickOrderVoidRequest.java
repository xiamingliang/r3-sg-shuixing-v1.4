package com.jackrain.nea.sg.sx.in.model.request;

import com.jackrain.nea.web.face.User;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author: 周琳胜
 * @since: 2019/12/12
 * create at : 2019/12/12 10:54
 */
@Data
public class SgPhyInPickOrderVoidRequest implements Serializable{

    /**
     * 拣货单id
     */
    private List<Long> ids;

    /**
     * 来源入库通知单 来源单据编号
     */
    private List<String> sourceBillNoList;

    /**
     * 操作用户
     */
    private User user;


}
