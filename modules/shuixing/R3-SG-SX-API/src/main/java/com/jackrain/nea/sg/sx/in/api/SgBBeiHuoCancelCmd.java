package com.jackrain.nea.sg.sx.in.api;

import com.jackrain.nea.sys.Command;

/**
 * @author zh
 * @date 2019/12/24
 * 备货出库取消（总代、加盟、直营）
 */
public interface SgBBeiHuoCancelCmd extends Command {

}
