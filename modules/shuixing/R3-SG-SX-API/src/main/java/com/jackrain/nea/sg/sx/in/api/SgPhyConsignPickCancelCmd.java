package com.jackrain.nea.sg.sx.in.api;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 * @author: 周琳胜
 * @since: 2019/12/12
 * create at : 2019/12/12 10:42
 */
public interface SgPhyConsignPickCancelCmd {

    ValueHolderV14 consignPickCancel(JSONObject jsonObject);
}
