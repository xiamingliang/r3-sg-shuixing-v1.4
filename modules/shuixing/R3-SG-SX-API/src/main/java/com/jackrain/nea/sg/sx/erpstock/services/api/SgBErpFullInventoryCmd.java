package com.jackrain.nea.sg.sx.erpstock.services.api;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.sg.sx.erpstock.services.request.SgBErpFullInventoryRequest;
import com.jackrain.nea.sg.sx.erpstock.services.table.SgBErpFullInventory;
import com.jackrain.nea.sys.Command;
import com.jackrain.nea.sys.domain.ValueHolderV14;

import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * @author yangbing
 * @description ERP库存同步接口
 * @since 2020/04/08
 */

public interface SgBErpFullInventoryCmd extends Command {

    //EPR库存同步
    ValueHolderV14 SyncErpStockinfo(List<SgBErpFullInventory> dataList);

    //查询ERP 中间表库存服务
    ValueHolderV14<List<List<SgBErpFullInventory>>> getErpStockInfo(JSONArray array);

    //ERP 库存查询
    ValueHolderV14<Long> getErpStockInfoByQty(JSONObject json);


    //根据仓库+料号 查询中间表加库存存

     JSONArray   getSgFullInventoryQtyByTb(JSONObject json);

}

