package com.jackrain.nea.sg.sx.out.api;

import com.jackrain.nea.sg.sx.out.model.request.SgOutStockRequest;
import com.jackrain.nea.sg.sx.out.model.result.SgOutStockResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 * @Project R3-SG-ShuiXing-V1.4
 * @Description: SG出库服务:
 *  包括逻辑发货单，出库通知单，出库结果单，逻辑收货单
 * @Author: Created by Dean on 2020/6/2 9:07
 **/
public interface SgOutStockCmd {

    ValueHolderV14<SgOutStockResult> outStock(SgOutStockRequest request);

}
