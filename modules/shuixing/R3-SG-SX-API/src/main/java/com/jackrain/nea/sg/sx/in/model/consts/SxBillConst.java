package com.jackrain.nea.sg.sx.in.model.consts;

/**
 * 单据类,key, 状态常量
 *
 * @author: xiWen.z
 * create at: 2019/12/26 0026
 */
public final class SxBillConst {


    /**
     * 1. 拣货单采购回传
     */
    public static final String ERP_CODE_SUC_KEY = "SUCCESS";

    /**
     * 1.1 erp 中间表回传内容
     */
    public static final String ERP_CALLBACK_CONTENT_KEY = "ERP_BACK_CONTENT";

    /**
     * 1.2 erp 中间表回传信息
     */
    public static final String ERP_CALLBACK_MSG_KEY = "MESSAGE";

    /**
     * 1.3 erp 中间表更新拣货单ID
     */
    public static final String ERP_CALLBACK_ID = "SOURCE_BILL_NO";

    /**
     * 1.4 erp 中间表回传入库单号
     */
    public static final String ERP_BILL_NO = "BILL_NO";


    private SxBillConst() {
    }


}
