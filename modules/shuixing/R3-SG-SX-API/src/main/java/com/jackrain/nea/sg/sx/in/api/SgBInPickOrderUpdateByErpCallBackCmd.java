package com.jackrain.nea.sg.sx.in.api;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 * 采购入库回传,回执更新数据库
 *
 * @author: xiWen.z
 * create at: 2019/12/20 0020
 */
public interface SgBInPickOrderUpdateByErpCallBackCmd {

    /**
     * @param jsnObj
     * @return
     */
    ValueHolderV14 modifyInPickOrderByErpCallBack(JSONObject jsnObj);

}
