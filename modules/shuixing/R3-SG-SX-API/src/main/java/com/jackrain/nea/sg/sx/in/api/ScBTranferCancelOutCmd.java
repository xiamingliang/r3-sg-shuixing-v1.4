package com.jackrain.nea.sg.sx.in.api;

import com.jackrain.nea.sys.Command;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.face.User;

/**
 * @author zh
 * @date 2019/12/23
 * 调拨单取消出库
 */
public interface ScBTranferCancelOutCmd extends Command {
    ValueHolder cancelOutTransfer(User user, Long id);
}
