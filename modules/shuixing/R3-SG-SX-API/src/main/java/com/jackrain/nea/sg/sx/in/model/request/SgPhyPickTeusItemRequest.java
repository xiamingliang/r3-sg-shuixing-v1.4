package com.jackrain.nea.sg.sx.in.model.request;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.io.Serializable;

/**
 * @author: 周琳胜
 * @since: 2019/11/13
 * create at : 2019/11/13 20:14
 */
@Data
public class SgPhyPickTeusItemRequest implements Serializable {
    @JSONField(name = "ENAME")
    private String ename;

    @JSONField(name = "BOX_MODEL")
    private String boxModel;

    @JSONField(name = "QTY")
    private String qty;

    @JSONField(name = "WEIGHT")
    private String weight;

    @JSONField(name = "BOX_CODE")
    private String boxCode;
}
