package com.jackrain.nea.sg.sx.in.api;

import com.jackrain.nea.sg.in.model.table.SgBInPickorder;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;

/**
 * 采购入库回传
 *
 * @author: xiWen.z
 * create at: 2019/12/18 0018
 */
public interface SgBInPickOrderTrans2MidCmd {

    /**
     * @param sgBInPickorder
     * @param usr
     * @return
     */
    ValueHolderV14 inPickOrderTrans2Mid(SgBInPickorder sgBInPickorder, User usr);
}
