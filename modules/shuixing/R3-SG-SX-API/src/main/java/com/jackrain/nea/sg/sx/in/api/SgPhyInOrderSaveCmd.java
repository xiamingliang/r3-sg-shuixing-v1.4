package com.jackrain.nea.sg.sx.in.api;

import com.jackrain.nea.sys.Command;

/**
 * @author zhoulinsheng
 * @since 2019-12-20
 * create at : 2019-12-20 16:45
 */
public interface SgPhyInOrderSaveCmd extends Command{
}
