package com.jackrain.nea.sg.sx.in.model.resource;

/**
 * @author: xiWen.z
 * create at: 2019/12/18 0018
 */
public final class BuildRedisKeyResources {

    private BuildRedisKeyResources() {
    }

    public static final long DEFAULT_REDIS_KEY_VALIDATE_TIME = 24 * 60 * 60 * 1000;


    /**
     * 示例代码
     * 获取redis key
     *
     * @param orderId 单据id
     * @return 锁定单据Redis key 值
     */
    public static String buildSgInOrderKey(Long orderId) {
        return "sg:in_order:" + orderId;
    }
}
