package com.jackrain.nea.sg.sx.checkdiffe.model.request;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import org.assertj.core.util.Lists;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author chenxinxing
 * @description 盘差单ERP审核回传实体
 * @since 2020/2/18 13:26
 */
@Data
public class ChkDiffeReturnAuditRequest implements Serializable{

    private static final long serialVersionUID = -7065135471090586305L;

    @JSONField(name = "STOCKINGOUT")
    private ChkDiffeReturnAudit chkDiffeReturnAudit;

    @JSONField(name = "ITEM")
    private List<ChkDiffeReturnAuditItem> chkDiffeReturnAuditItems= Lists.newArrayList();

    @Data
    public static class ChkDiffeReturnAudit implements Serializable {

        private static final long serialVersionUID = 2260800750376332531L;

        @JSONField(name = "PD_NO")
        private String billNo;

        @JSONField(name = "ERPPD_NO")
        private String erppdNo;
    }

    @Data
    public static class ChkDiffeReturnAuditItem implements Serializable {

        private static final long serialVersionUID = -8766869880827720120L;

        @JSONField(name = "ECODE")
        private String eCode;

        @JSONField(name = "ENAME")
        private String ename;

        @JSONField(name = "QTY_DIFF")
        private BigDecimal qtyDiff;

        @JSONField(name = "REMARK_DETAIL")
        private String remarkDetail;

    }
}
