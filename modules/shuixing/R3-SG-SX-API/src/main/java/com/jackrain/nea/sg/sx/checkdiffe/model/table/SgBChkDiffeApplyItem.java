package com.jackrain.nea.sg.sx.checkdiffe.model.table;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.jackrain.nea.es.annotations.Document;
import com.jackrain.nea.es.annotations.Field;
import com.jackrain.nea.es.constans.FieldType;
import com.jackrain.nea.sys.domain.BaseModel;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;

@TableName(value = "sg_b_chk_diffe_apply_item")
@Data
@Document(index = "sg_b_chk_diffe_apply",type = "sg_b_chk_diffe_apply_item")
public class SgBChkDiffeApplyItem extends BaseModel {
    @JSONField(name = "ID")
    @TableField(fill = FieldFill.INSERT)
    @TableId(value = "id", type = IdType.INPUT)
    @Field(type = FieldType.Long)
    private Long id;

    @JSONField(name = "SG_B_CHK_DIFFE_APPLY_ID")
    @Field(type = FieldType.Long)
    private Long sgBChkDiffeApplyId;

    @JSONField(name = "PS_C_SKU_ID")
    @Field(type = FieldType.Long)
    private Long psCSkuId;

    @JSONField(name = "PS_C_SKU_ECODE")
    @Field(type = FieldType.Keyword)
    private String psCSkuEcode;

    @JSONField(name = "PS_C_PRO_ID")
    @Field(type = FieldType.Long)
    private Long psCProId;

    @JSONField(name = "PS_C_PRO_ECODE")
    @Field(type = FieldType.Keyword)
    private String psCProEcode;

    @JSONField(name = "PS_C_PRO_ENAME")
    @Field(type = FieldType.Keyword)
    private String psCProEname;

    @JSONField(name = "QTY_BOOK")
    @Field(type = FieldType.Long)
    private Long qtyBook;

    @JSONField(name = "QTY_ACTUAL")
    @Field(type = FieldType.Long)
    private Long qtyActual;

    @JSONField(name = "QTY_APPLY")
    @Field(type = FieldType.Long)
    private Long qtyApply;

    @JSONField(name = "QTY_STATUS")
    @Field(type = FieldType.Long)
    private Long qtyStatus;

    @JSONField(name = "QTY_DIFFERENCE")
    @Field(type = FieldType.Long)
    private Long qtyDifference;

    @JSONField(name = "PS_C_CLR_ID")
    @Field(type = FieldType.Long)
    private Long psCClrId;

    @JSONField(name = "PS_C_SIZE_ID")
    @Field(type = FieldType.Long)
    private Long psCSizeId;

    @JSONField(name = "BASICUNIT_ENAME")
    @Field(type = FieldType.Keyword)
    private String basicunitEname;

    @JSONField(name = "OPINION")
    @Field(type = FieldType.Keyword)
    private String opinion;

    @JSONField(name = "STATUS_ID")
    @Field(type = FieldType.Long)
    private Long statusId;

    @JSONField(name = "STATUS_ENAME")
    @Field(type = FieldType.Keyword)
    private String statusEname;

    @JSONField(name = "STATUS_NAME")
    @Field(type = FieldType.Keyword)
    private String statusName;

    @JSONField(name = "STATUS_TIME")
    @Field(type = FieldType.Long)
    private Date statusTime;

    @JSONField(name = "REMARK")
    @Field(type = FieldType.Keyword)
    private String remark;

    @JSONField(name = "VERSION")
    @Field(type = FieldType.Long)
    private Long version;

    @JSONField(name = "OWNERENAME")
    @Field(type = FieldType.Keyword)
    private String ownerename;

    @JSONField(name = "MODIFIERENAME")
    @Field(type = FieldType.Keyword)
    private String modifierename;

    @JSONField(name = "PS_C_SKU_ENAME")
    @Field(type = FieldType.Keyword)
    private String psCSkuEname;

    @JSONField(name = "PS_C_CLR_ECODE")
    @Field(type = FieldType.Keyword)
    private String psCClrEcode;

    @JSONField(name = "PS_C_CLR_ENAME")
    @Field(type = FieldType.Keyword)
    private String psCClrEname;

    @JSONField(name = "PS_C_SIZE_ECODE")
    @Field(type = FieldType.Keyword)
    private String psCSizeEcode;

    @JSONField(name = "PS_C_SIZE_ENAME")
    @Field(type = FieldType.Keyword)
    private String psCSizeEname;

    @JSONField(name = "GBCODE")
    @Field(type = FieldType.Keyword)
    private String gbcode;

    @JSONField(name = "PRICE_LIST")
    @Field(type = FieldType.Double)
    private BigDecimal priceList;
}