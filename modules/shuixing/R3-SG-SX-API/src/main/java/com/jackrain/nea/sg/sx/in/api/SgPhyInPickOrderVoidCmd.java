package com.jackrain.nea.sg.sx.in.api;

import com.jackrain.nea.sg.sx.in.model.request.SgPhyInPickOrderVoidRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 * @author zhu lin yu
 * @since 2019-11-18
 * create at : 2019-11-18 17:46
 */
public interface SgPhyInPickOrderVoidCmd {

    ValueHolderV14 voidSgbBInPickorder(SgPhyInPickOrderVoidRequest sgPhyInPickOrderVoidRequest);

}
