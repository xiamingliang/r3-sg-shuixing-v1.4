package com.jackrain.nea.sg.sx.out.model.request;

import com.jackrain.nea.sg.out.model.request.SgPhyOutNoticesBillSaveRequest;
import com.jackrain.nea.sg.out.model.request.SgPhyOutResultBillSaveRequest;
import com.jackrain.nea.sg.receive.model.request.SgReceiveBillSaveRequest;
import com.jackrain.nea.sg.send.model.request.SgSendBillSaveRequest;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.Tolerate;

import java.io.Serializable;

/**
 * @Project R3-SG-ShuiXing-V1.4
 * @Description: 出库服务
 * @Author: Created by Dean on 2020/6/2 9:13
 **/
@Data
@Builder
@Accessors(chain = true)
public class SgOutStockRequest implements Serializable {

    @Tolerate
    public SgOutStockRequest() {
        super();
    }

    private int operation = 0;

    private SgSendBillSaveRequest sendBillSaveRequest;//逻辑发货单

    private SgReceiveBillSaveRequest receiveBillSaveRequest;//逻辑收货单

    private SgPhyOutNoticesBillSaveRequest outNoticesBillSaveRequest;//出库通知单

    private SgPhyOutResultBillSaveRequest outResultBillSaveRequest;//出库结果单

    public void sendBillSaveRequest(SgSendBillSaveRequest sendBillSaveRequest) {
        this.sendBillSaveRequest = sendBillSaveRequest;
        this.operation += 1;
    }

    public void receiveBillSaveRequest(SgReceiveBillSaveRequest receiveBillSaveRequest) {
        this.receiveBillSaveRequest = receiveBillSaveRequest;
        this.operation += 2;
    }

    public void outNoticesBillSaveRequest(SgPhyOutNoticesBillSaveRequest outNoticesBillSaveRequest) {
        this.outNoticesBillSaveRequest = outNoticesBillSaveRequest;
        this.operation += 4;
    }

    public void outResultBillSaveRequest(SgPhyOutResultBillSaveRequest outResultBillSaveRequest) {
        this.outResultBillSaveRequest = outResultBillSaveRequest;
        this.operation += 8;
    }


}
