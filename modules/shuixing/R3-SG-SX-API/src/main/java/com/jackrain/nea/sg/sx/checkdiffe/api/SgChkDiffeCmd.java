package com.jackrain.nea.sg.sx.checkdiffe.api;

import com.jackrain.nea.sg.sx.checkdiffe.model.request.ChkDiffeReturnAuditRequest;
import com.jackrain.nea.sys.Command;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;

/**
 * @author chenxinxing
 * @description 盘差单接口
 * @since 2020/2/13 22:36
 */

public interface SgChkDiffeCmd extends Command {

    //盘差单ERP审核回传
    ValueHolderV14 chkDiffeReturnAudit(ChkDiffeReturnAuditRequest chkDiffeReturnAuditRequest, User user);
}
