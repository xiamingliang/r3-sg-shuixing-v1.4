package com.jackrain.nea.sg.sx.inv.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.oc.basic.api.TableServiceCmd;
import com.jackrain.nea.sg.sx.in.api.ScBTranferCancelOutCmd;
import com.jackrain.nea.sg.sx.in.api.SgBBeiHuoCancelCmd;
import com.jackrain.nea.sg.transfer.api.*;
import com.jackrain.nea.sg.transfer.model.request.SgTransferBillAuditRequest;
import com.jackrain.nea.sg.transfer.model.request.SgTransferBillSaveRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.face.impl.UserImpl;
import com.jackrain.nea.web.query.DefaultWebEvent;
import com.jackrain.nea.web.query.QuerySession;
import com.jackrain.nea.web.query.QuerySessionImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.net.URLDecoder;
import java.util.HashMap;

/**
 * @author csy
 */
@RestController
@Slf4j
@Api(value = "SgTransferCtrlShuixing", description = "逻拨单接口")
public class SgTransferShuixingCtrl {

    @ApiOperation(value = "调拨单取消出库")
    @PostMapping(path = "/sg/store/transfer/cancelOut")
    public JSONObject cancelOut(Long id) {
        User user = getRootUser();
        QuerySession session = new QuerySessionImpl(user);
        DefaultWebEvent event = new DefaultWebEvent("deal", new HashMap(16));
        JSONObject param = new JSONObject();
        param.put("objid", id);
        event.put("param", param);
        session.setEvent(event);
        ScBTranferCancelOutCmd cancelOutCmd = (ScBTranferCancelOutCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                ScBTranferCancelOutCmd.class.getName(), "sg", "1.0");
        ValueHolder result;
        try {
            if (log.isDebugEnabled()) {
                log.debug("调拨单取消出库！");
            }
            result = cancelOutCmd.execute(session);
        } catch (Exception e) {
            result = new ValueHolder();
            result.put("code", ResultCode.FAIL);
            result.put("message", e.getMessage());
        }
        return result.toJSONObject();
    }



    @ApiOperation(value = "备货取消")
    @PostMapping(path = "/api/sg-sx/v1.0/beihuoOut")
    public JSONObject beihuoOut(HttpServletRequest request, @RequestBody String  param ) {

        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + "beihuoOut---ReceiveParams=" + URLDecoder.decode(param));
        }

        JSONObject objJson = JSONObject.parseObject(URLDecoder.decode(param).replaceAll("\\\\", ""));
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + "beihuoOut---objJson=" + objJson);
        }

        User user = getRootUser();
        QuerySession session = new QuerySessionImpl(user);
        DefaultWebEvent event = new DefaultWebEvent("deal", new HashMap(16));
        event.put("param", objJson);
        session.setEvent(event);
        SgBBeiHuoCancelCmd cancelOutCmd = (SgBBeiHuoCancelCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                SgBBeiHuoCancelCmd.class.getName(), "sg", "1.0");
        ValueHolder result;
        try {
            if (log.isDebugEnabled()) {
                log.debug("备货取消！"+param);
            }
            result = cancelOutCmd.execute(session);
        } catch (Exception e) {
            result = new ValueHolder();
            result.put("code", ResultCode.FAIL);
            result.put("message", e.getMessage());
        }
        return result.toJSONObject();
    }

    /**
     * 测试用户
     */
    private User getRootUser() {
        UserImpl user = new UserImpl();
        user.setId(893);
        user.setName("root");
        user.setEname("root");
        user.setActive(true);
        user.setClientId(37);
        user.setOrgId(27);
        user.setIsAdmin(2);
        user.setIsDev(2);
        return user;
    }




    @ApiOperation(value = "调拨单审核")
    @PostMapping(path = "/sg/store/transfer/auditOut")
    public JSONObject auditOut(Long id) {
        User user = getRootUser();
        QuerySession session = new QuerySessionImpl(user);
        DefaultWebEvent event = new DefaultWebEvent("deal", new HashMap(16));
        JSONObject param = new JSONObject();
        param.put("objid", id);
        event.put("param", param);
        session.setEvent(event);
        SgTransferAuditCmd auditOutCmd = (SgTransferAuditCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                SgTransferAuditCmd.class.getName(), "sg", "1.0");
        ValueHolderV14 result;
        try {
            if (log.isDebugEnabled()) {
                log.debug("调拨单审核！");
            }
            SgTransferBillAuditRequest request  = new SgTransferBillAuditRequest();
            request.setSourceBillId(id);
            request.setSourceBillType(7);
            request.setUser(user);
            result = auditOutCmd.audit(request);
        } catch (Exception e) {
            e.printStackTrace();
            result = new ValueHolderV14();
            result.setMessage(e.getMessage());
        }
        return result.toJSONObject();
    }




}
