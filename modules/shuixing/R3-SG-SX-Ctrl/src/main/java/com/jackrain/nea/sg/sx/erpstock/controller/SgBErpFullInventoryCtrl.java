package com.jackrain.nea.sg.sx.erpstock.controller;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.aliyun.openservices.shade.com.alibaba.fastjson.JSON;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.sg.sx.checkdiffe.api.SgChkDiffeCmd;
import com.jackrain.nea.sg.sx.erpstock.services.api.SgBErpFullInventoryCmd;
import com.jackrain.nea.sg.sx.erpstock.services.request.SgBErpFullInventoryRequest;
import com.jackrain.nea.sg.sx.erpstock.services.table.SgBErpFullInventory;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@RestController
@Slf4j
@Api(value = "SgBErpFullInventoryCtrl", description = "ERP库存同步接口")
public class SgBErpFullInventoryCtrl {


    @ApiOperation(value = "ERP库存同步接口")
    @PostMapping(path = "/api/sg-sx/v1.0/SyncSgBErpFullInventory")
    public ValueHolderV14 syncSgBErpFullInventory(HttpServletRequest request, @RequestBody String param) {
        ValueHolderV14 vh=null;

        SgBErpFullInventoryCmd sgBErpFullInventoryCmd = (SgBErpFullInventoryCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(), SgBErpFullInventoryCmd.class.getName(),
                "sg", "1.0");
        JSONObject paramJson=JSONObject.parseObject(param);
        if(!paramJson.isEmpty()){
            List<SgBErpFullInventory>  dataList= JSONArray.parseArray(paramJson.get("data").toString(), SgBErpFullInventory.class);
            vh =sgBErpFullInventoryCmd.SyncErpStockinfo(dataList);
        }else{
            vh.setCode(0);
            vh.setMessage("成功");

        }


        return vh;

    }

    @ApiOperation(value = "ERP库存查询")
    @PostMapping(path = "/api/sg-sx/v1.0/getSgFullInventoryQtyByTb")
    public ValueHolderV14 getSgFullInventoryQtyByTb(HttpServletRequest request, @RequestBody String param) {
      //  {"warehouse":"[\"D0101\",\"HZ02\"]","start_date":1587515401948}
        ValueHolderV14 vh=new ValueHolderV14() ;

        SgBErpFullInventoryCmd sgBErpFullInventoryCmd = (SgBErpFullInventoryCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(), SgBErpFullInventoryCmd.class.getName(),
                "sg", "1.0");
        JSONObject paramJson=JSONObject.parseObject(param);

        if(!paramJson.isEmpty()){
            vh.setData(sgBErpFullInventoryCmd.getSgFullInventoryQtyByTb(paramJson));
        }


        return vh;

    }



}
