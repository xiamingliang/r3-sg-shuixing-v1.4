package com.jackrain.nea.sg.sx.inv.controller;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.sx.in.api.SgPhyConsignPickCancelCmd;
import com.jackrain.nea.sg.sx.in.api.SgPhyConsignPickCmd;
import com.jackrain.nea.sg.sx.in.api.SgPhyInOrderAuditCmd;
import com.jackrain.nea.sg.sx.in.api.SgPhyInOrderSaveCmd;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.face.impl.UserImpl;
import com.jackrain.nea.web.query.DefaultWebEvent;
import com.jackrain.nea.web.query.QuerySession;
import com.jackrain.nea.web.query.QuerySessionImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.net.URLDecoder;
import java.util.HashMap;


/**
 * @author 周琳胜
 * @since 2019-11-14
 * create at : 2019-11-14 13:03
 */
@RestController
@Slf4j
@Api(value = "SgPhyInPickConsignCtrl", description = "拣货单")
public class SgPhyInPickConsignCtrl {



    @ApiOperation(value = "拣货托运")
    @PostMapping(path = "/api/sg-sx/v1.0/consignPick")
    public JSONObject consignPick(HttpServletRequest request, @RequestBody String param) {
        SgPhyConsignPickCmd sgPhyConsignPickCmd = (SgPhyConsignPickCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                SgPhyConsignPickCmd.class.getName(), "sg", "1.0");

        String decode = URLDecoder.decode(param);
        JSONObject jsonObject = JSON.parseObject(decode);
        ValueHolderV14 v14 = sgPhyConsignPickCmd.consignPick(jsonObject);

        return v14.toJSONObject();
    }

    @ApiOperation(value = "托运取消")
    @PostMapping(path = "/api/sg-sx/v1.0/consignPickCancel")
    public JSONObject consignPickCancel(HttpServletRequest request, @RequestBody String param) {
        SgPhyConsignPickCancelCmd sgPhyConsignPickCancelCmd = (SgPhyConsignPickCancelCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                SgPhyConsignPickCancelCmd.class.getName(), "sg", "1.0");

        String decode = URLDecoder.decode(param);
        JSONObject jsonObject = JSON.parseObject(decode);
        ValueHolderV14 v14 = sgPhyConsignPickCancelCmd.consignPickCancel(jsonObject);

        return v14.toJSONObject();
    }

    @ApiOperation(value = "入库单 审核 - 页面")
    @PostMapping(path = "/sg/inPickOrder/auditR3")
    public JSONObject auditforR3(HttpServletRequest request, @RequestBody JSONObject param) {
        SgPhyInOrderAuditCmd auditCmd = (SgPhyInOrderAuditCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                SgPhyInOrderAuditCmd.class.getName(), "sg", "1.0");
        ValueHolder result;
        try {
            log.debug("开始审核");
//            QuerySession session = new QuerySessionImpl(getUser());
            QuerySession session = new QuerySessionImpl(request);
            DefaultWebEvent event = new DefaultWebEvent("auditforR3", new HashMap(16));
            event.put("param", param);
            session.setEvent(event);
            result = auditCmd.execute(session);
        } catch (Exception e) {
            result = new ValueHolder();
            result.put("code", ResultCode.SUCCESS);
            result.put("message", e.getMessage());
        }
        return result.toJSONObject();
    }

    @ApiOperation(value = "入库单-保存")
    @PostMapping(path = "/p/cs/sg/pickOrder/save")
    public JSONObject pickOrderSave(HttpServletRequest request, @RequestParam(value = "param") String param) {
        ValueHolder result;
        Object o = ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                SgPhyInOrderSaveCmd.class.getName(), "sg", "1.0");
//        QuerySessionImpl querySession = new QuerySessionImpl(getUser());
        QuerySessionImpl querySession = new QuerySessionImpl(request);
        JSONObject paramJo = JSON.parseObject(param);
        DefaultWebEvent event = new DefaultWebEvent("pickOrderSave", request, false);
        event.put("param", paramJo);
        querySession.setEvent(event);
        result = ((SgPhyInOrderSaveCmd) o).execute(querySession);

        return result.toJSONObject();
    }

    /***
     * 获取用户
     * @return
     */
    private User getUser() {
        UserImpl user = new UserImpl();
        user.setClientId(37);
        user.setOrgId(27);
        user.setId(100);
        user.setName("root");
        user.setEname("测试用户");
        return user;
    }

}
