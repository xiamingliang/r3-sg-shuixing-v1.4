package com.jackrain.nea.sg.sx.checkdiffe.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.sx.checkdiffe.api.SgChkDiffeCmd;
import com.jackrain.nea.sg.sx.checkdiffe.model.request.ChkDiffeReturnAuditRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.face.impl.UserImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.net.URLDecoder;
import java.util.List;
import java.util.Objects;


/**
 * @author chenxinxing
 * @description 盘差单ERP审核回传
 * @since 2020/2/18 12:08
 */

@RestController
@Slf4j
@Api(value = "SgChkDiffeReturnAuditCtrl", description = "盘差单ERP审核回传")
public class SgChkDiffeReturnAuditCtrl {

    @ApiOperation(value = "盘差单ERP审核回传")
    @PostMapping(path = "/api/sg-sx/v1.0/chkDiffeReturnAudit")
    public ValueHolderV14 chkDiffeReturnAudit(HttpServletRequest request, @RequestBody String param) {
        ValueHolderV14 vh=null;
        try {
            String params = URLDecoder.decode(param);
            params=params.replaceAll("\r","").replaceAll("\n","").replaceAll("\t","").replaceAll("=","");
            if (log.isDebugEnabled()) {
                log.debug("Start.SgChkDiffeReturnAuditCtrl.chkDiffeReturnAudit.ReceiveParams=" + param);
            }
            JSONObject jsonObject = JSONObject.parseObject(params);
            String jsnString = JSONArray.toJSONString(jsonObject.get("param"));
            if (StringUtils.isBlank(jsnString)) {
                vh = new ValueHolderV14();
                vh.setCode(ResultCode.FAIL);
                vh.setMessage("param Is Null");
                if (log.isDebugEnabled()) {
                    log.debug(this.getClass().getName() + "RequestMsg_" + jsonObject.toString());
                }
                return vh;
            }
            List<ChkDiffeReturnAuditRequest> chkDiffeReturnAuditRequests = JSON.parseArray(jsnString, ChkDiffeReturnAuditRequest.class);
            if (Objects.isNull(chkDiffeReturnAuditRequests)) {
                vh = new ValueHolderV14();
                vh.setCode(ResultCode.FAIL);
                vh.setMessage("param Analysis Result Is Null");
                if (log.isDebugEnabled()) {
                    log.debug(this.getClass().getName() + "Result Is Null 4 Parse Param 2 JSONArray");
                }
                return vh;
            }
            User user = getRootUser();
            SgChkDiffeCmd sgChkDiffeCmd = (SgChkDiffeCmd) ReferenceUtil.refer(
                    ApplicationContextHandle.getApplicationContext(), SgChkDiffeCmd.class.getName(),
                    "sg", "1.0");
            ChkDiffeReturnAuditRequest chkDiffeReturnAuditRequest = chkDiffeReturnAuditRequests.get(0);
            vh = sgChkDiffeCmd.chkDiffeReturnAudit(chkDiffeReturnAuditRequest,user);

        } catch (Exception e) {
            e.printStackTrace();
            vh = new ValueHolderV14();
            vh.setCode(ResultCode.FAIL);
            vh.setMessage(e.getMessage());
            log.error(this.getClass().getName() + "SgChkDiffeReturnAuditCtrl Exception", e);
        }
        return vh;

    }

    private User getRootUser() {
        UserImpl user = new UserImpl();
        user.setId(893);
        user.setName("Erp");
        user.setEname("Erp");
        user.setActive(true);
        user.setClientId(37);
        user.setOrgId(27);
        user.setIsAdmin(2);
        user.setIsDev(2);
        return user;
    }
}
