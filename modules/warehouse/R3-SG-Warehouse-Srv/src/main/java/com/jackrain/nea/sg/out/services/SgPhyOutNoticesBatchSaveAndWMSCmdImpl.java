package com.jackrain.nea.sg.out.services;

import com.jackrain.nea.sg.out.api.SgPhyOutNoticesBatchSaveAndWMSCmd;
import com.jackrain.nea.sg.out.model.request.SgPhyOutNoticesBillSaveRequest;
import com.jackrain.nea.sg.out.model.request.SgPhyOutNoticesSaveRequest;
import com.jackrain.nea.sg.out.model.result.SgOutNoticesResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author leexh
 * @since 2019/5/6 14:03
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgPhyOutNoticesBatchSaveAndWMSCmdImpl implements SgPhyOutNoticesBatchSaveAndWMSCmd {

    @Override
    public ValueHolderV14<List<SgOutNoticesResult>> batchSaveOutNoticesAndWMS(List<SgPhyOutNoticesBillSaveRequest> requests, User user) {
        SgPhyOutNoticesBatchSaveAndWMSService bean = ApplicationContextHandle.getBean(SgPhyOutNoticesBatchSaveAndWMSService.class);
        List<SgPhyOutNoticesSaveRequest> notices = requests.stream().map(SgPhyOutNoticesBillSaveRequest::getOutNoticesRequest).collect(Collectors.toList());
        Set<Long> set = notices.stream().map(SgPhyOutNoticesSaveRequest::getSourceBillId).collect(Collectors.toSet());
        List<Long> sourceBillIds = new ArrayList<>(set);

        for (SgPhyOutNoticesBillSaveRequest request : requests) {
            request.setLoginUser(user);
        }
        return bean.batchSaveOutNoticesAndWMS(requests, sourceBillIds, user);
    }
}
