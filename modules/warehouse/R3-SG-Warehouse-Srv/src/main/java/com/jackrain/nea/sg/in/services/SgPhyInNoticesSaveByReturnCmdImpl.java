package com.jackrain.nea.sg.in.services;

import com.jackrain.nea.sg.in.api.SgPhyInNoticesSaveByReturnCmd;
import com.jackrain.nea.sg.in.model.request.SgPhyInNoticesBillSaveRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Author: ChenChen
 * @Description:新增入库通知单服务(退回)
 * @Date: Create in 14:42 2019/5/8
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgPhyInNoticesSaveByReturnCmdImpl implements SgPhyInNoticesSaveByReturnCmd {

    @Autowired
    SgPhyInNoticesSaveService sgPhyInNoticesSaveService;

    @Override
    public ValueHolderV14 saveSgBPhyInNoticesByReturn(List<SgPhyInNoticesBillSaveRequest> requestList) {
        return sgPhyInNoticesSaveService.saveSgBPhyInNoticesByReturnMQ(requestList);
    }

    @Override
    public ValueHolderV14 saveSgBPhyInNoticesByReturnRPC(List<SgPhyInNoticesBillSaveRequest> requestList) {
        return sgPhyInNoticesSaveService.saveSgBPhyInNoticesByReturnRPC(requestList);
    }

    @Override
    public ValueHolderV14 saveSgBPhyInNoticesByRPC(List<SgPhyInNoticesBillSaveRequest> requestList, Boolean isNeedReceive, Boolean isNeedInResult) {
        return sgPhyInNoticesSaveService.saveSgBPhyInNoticesByRPC(requestList, isNeedReceive, isNeedInResult);
    }

}
