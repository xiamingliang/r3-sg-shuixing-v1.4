package com.jackrain.nea.sg.out.services;

import com.jackrain.nea.sg.out.api.SgPhyOutNoticesImpItemQueryCmd;
import com.jackrain.nea.sg.out.model.request.SgPhyOutNoticesQueryRequest;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutNoticesImpItem;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author zhu lin yu
 * @since 2019-10-23
 * create at : 2019-10-23 20:05
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgPhyOutNoticesImpItemQueryCmdImpl implements SgPhyOutNoticesImpItemQueryCmd {
    @Override
    public ValueHolderV14<List<SgBPhyOutNoticesImpItem>> queryOutNoticesItem(SgPhyOutNoticesQueryRequest request) {
        SgPhyOutNoticesImpItemQueryService sgPhyOutNoticesImpItemQueryService = ApplicationContextHandle.getBean(SgPhyOutNoticesImpItemQueryService.class);
        return sgPhyOutNoticesImpItemQueryService.queryOutNoticesItem(request);
    }
}
