package com.jackrain.nea.sg.out.services;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.basic.model.result.SgR3BaseResult;
import com.jackrain.nea.sg.out.api.SgPhyOutNoticesBatchVoidAndWMSCmd;
import com.jackrain.nea.sg.out.model.request.SgPhyOutNoticesBillVoidRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

/**
 * @author leexh
 * @since 2019/5/7 19:47
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgPhyOutNoticesBatchVoidAndWMSCmdImpl implements SgPhyOutNoticesBatchVoidAndWMSCmd {
    @Override
    public ValueHolderV14<SgR3BaseResult> batchVoidSgPhyOutNoticesAndWMS(SgPhyOutNoticesBillVoidRequest request) throws NDSException {
        SgPhyOutNoticesBatchVoidAndWMSService service = ApplicationContextHandle.getBean(SgPhyOutNoticesBatchVoidAndWMSService.class);
        return service.batchVoidSgPhyOutNoticesAndWMS(request);
    }
}
