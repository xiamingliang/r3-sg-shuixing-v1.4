package com.jackrain.nea.sg.phyadjust.services;

import com.jackrain.nea.sg.phyadjust.api.SgPhyAdjustQueryCmd;
import com.jackrain.nea.sg.phyadjust.model.request.SgPhyAdjustBillQueryRequest;
import com.jackrain.nea.sg.phyadjust.model.result.SgPhyAdjustBillQueryResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author: 舒威
 * @since: 2019/7/29
 * create at : 2019/7/29 22:22
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgPhyAdjustQueryCmdImpl implements SgPhyAdjustQueryCmd {

    @Autowired
    private SgPhyAdjustQueryService queryService;

    @Override
    public ValueHolderV14<SgPhyAdjustBillQueryResult> queryPhyAdj(SgPhyAdjustBillQueryRequest request) {
        return queryService.queryPhyAdj(request);
    }
}
