package com.jackrain.nea.sg.out.services;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Lists;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.oc.basic.common.OcBasicConstants;
import com.jackrain.nea.oc.purchase.api.OcRefundPurchaseUpdateCmd;
import com.jackrain.nea.oc.purchase.model.request.OcPurchaseUpdatePickRequest;
import com.jackrain.nea.oc.purchase.model.result.OcPurchaseUpdatePickResult;
import com.jackrain.nea.oc.sale.api.OcRefundSaleUpdateCmd;
import com.jackrain.nea.oc.sale.api.OcSaleUpdateCmd;
import com.jackrain.nea.oc.sale.model.request.OcSaleUpdatePickRequest;
import com.jackrain.nea.oc.sale.model.result.OcSaleUpdatePickResult;
import com.jackrain.nea.redis.config.CusRedisTemplate;
import com.jackrain.nea.redis.util.RedisOpsUtil;
import com.jackrain.nea.sg.basic.common.R3ParamConstants;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.utils.R3ParamUtil;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.in.common.SgInConstants;
import com.jackrain.nea.sg.in.common.SgInNoticeConstants;
import com.jackrain.nea.sg.out.api.SgBOutPickorderVoidCmd;
import com.jackrain.nea.sg.out.common.SgOutConstantsIF;
import com.jackrain.nea.sg.out.mapper.SgBOutPickorderMapper;
import com.jackrain.nea.sg.out.mapper.SgBOutPickorderNoticeItemMapper;
import com.jackrain.nea.sg.out.model.request.SgPhyOutNoticesUpdateRequest;
import com.jackrain.nea.sg.out.model.table.SgBOutPickorder;
import com.jackrain.nea.sg.out.model.table.SgBOutPickorderItem;
import com.jackrain.nea.sg.out.model.table.SgBOutPickorderNoticeItem;
import com.jackrain.nea.sg.transfer.api.SgTransferUpdatePickCmd;
import com.jackrain.nea.sg.transfer.model.request.OcTransferUpdatePickRequest;
import com.jackrain.nea.sg.transfer.model.result.OcTransferUpdatePickResult;
import com.jackrain.nea.sys.CommandAdapter;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.utils.AssertUtils;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.query.QuerySession;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * 出库拣货单作废接口
 *
 * @author: heliu
 * @since: 2019/11/18
 * create at : 2019/11/18 15:17
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgBOutPickorderVoidCmdImpl extends CommandAdapter implements SgBOutPickorderVoidCmd {

    @Override
    public ValueHolder execute(QuerySession session) throws NDSException {

        Locale locale = session.getLocale();
        JSONObject param = R3ParamUtil.getParamJo(session);
        User user = session.getUser();
        AssertUtils.notNull(user, "参数不存在-user", locale);
        AssertUtils.notNull(param, "参数不存在-param", locale);
        if (log.isDebugEnabled()) {
            log.debug("Start " + this.getClass().getName() + ".void ReceiveParams:param=" + JSONObject.toJSONString(param) + ";");
        }
        //页面批量作废
        JSONArray ids = param.getJSONArray("objids");
        if (CollectionUtils.isNotEmpty(ids)) {
            ValueHolder batchResult = new ValueHolder();
            int total = ids.size();
            int totalFail = 0;
            JSONArray errData = new JSONArray();
            for (int i = 0; i < ids.size(); i++) {
                Long id = ids.getLongValue(i);
                try {
                    ValueHolder result = this.doBillVoid(id, user);
                    if (!result.isOK()) {
                        JSONObject errInfo = new JSONObject();
                        errInfo.put(R3ParamConstants.MESSAGE, result.getData().get(R3ParamConstants.MESSAGE));
                        errInfo.put(R3ParamConstants.CODE, ResultCode.FAIL);
                        errInfo.put(R3ParamConstants.OBJID, id);
                        errData.add(errInfo);
                        totalFail++;
                    }
                } catch (Exception e) {
                    JSONObject errInfo = new JSONObject();
                    errInfo.put(R3ParamConstants.MESSAGE, "作废失败:" + e);
                    errInfo.put(R3ParamConstants.CODE, ResultCode.FAIL);
                    errInfo.put(R3ParamConstants.OBJID, id);
                    errData.add(errInfo);
                    totalFail++;
                    log.error("出库拣货单作废失败", e);
                }
            }
            if (totalFail > 0) {
                batchResult.put(R3ParamConstants.CODE, ResultCode.FAIL);
                batchResult.put(R3ParamConstants.MESSAGE, Resources.getMessage("作废成功记录数 :" + (total - totalFail) + "条,作废失败记录数 :" + totalFail + "条", locale));
                batchResult.put(R3ParamConstants.DATA, errData);
            } else {
                batchResult.put(R3ParamConstants.CODE, ResultCode.SUCCESS);
                batchResult.put(R3ParamConstants.MESSAGE, Resources.getMessage("作废成功记录数 :" + total + "条", locale));
            }
            return batchResult;
        } else {
            Long objId = param.getLong(OcBasicConstants.OBJID);
            AssertUtils.notNull(objId, "参数不存在-objId", locale);
            return this.doBillVoid(objId, user);
        }
    }

    //出库拣货单作废逻辑
    @Transactional(rollbackFor = Exception.class)
    public ValueHolder doBillVoid(Long objId, User user) {

        if (log.isDebugEnabled()) {
            log.debug("Start " + this.getClass().getName() + ".doBillVoid. ReceiveParams:objId=" + objId + ";");
        }
        ValueHolder valueHolder = new ValueHolder();
        Locale locale = user.getLocale();
        AssertUtils.notNull(objId, "请选择要要操作的单据！", locale);
        CusRedisTemplate<String, String> redisTemplate = RedisOpsUtil.getObjRedisTemplate();
        String lockKsy = SgConstants.SG_B_OUT_PICKORDER + ":" + objId;
        Boolean blnCanInit = redisTemplate.opsForValue().setIfAbsent(lockKsy, "OK");
        if (blnCanInit) {
            redisTemplate.expire(lockKsy, 30, TimeUnit.MINUTES);
        } else {
            String msg = "当前单据正在被操作！请稍后重试..";
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + "，debug," + msg);
            }
            AssertUtils.logAndThrow(msg, user.getLocale());
        }
        try {
            SgBOutPickorderMapper sgBOutPickorderMapper = ApplicationContextHandle.getBean(SgBOutPickorderMapper.class);
            SgBOutPickorderNoticeItemMapper sgBOutPickorderNoticeItemMapper = ApplicationContextHandle.getBean(SgBOutPickorderNoticeItemMapper.class);
            SgBOutPickorder sgBOutPickorder = sgBOutPickorderMapper.selectById(objId);
            AssertUtils.notNull(sgBOutPickorder, "当前记录已不存在！", locale);
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + ".debug, purchase:" + JSONObject.toJSONString(sgBOutPickorder));
            }
            Integer status = sgBOutPickorder.getBillStatus();
            if (status == SgInConstants.BILL_STATUS_CHECKED) {
                AssertUtils.logAndThrow("当前记录已审核，不允许作废！", locale);
            }
            if (status == SgInConstants.BILL_STATUS_VOID) {
                AssertUtils.logAndThrow("当前记录已作废，不允许重复作废！", locale);
            }
            List<SgBOutPickorderNoticeItem> sgBOutPickorderNoticeItemList = sgBOutPickorderNoticeItemMapper.selectList(new QueryWrapper<SgBOutPickorderNoticeItem>().lambda()
                    .eq(SgBOutPickorderNoticeItem::getSgBOutPickorderId, objId)
                    .eq(SgBOutPickorderNoticeItem::getIsactive, SgConstants.IS_ACTIVE_Y));
            if (CollectionUtils.isEmpty(sgBOutPickorderNoticeItemList)) {
                AssertUtils.logAndThrow("出库拣货单关联的来源出库通知单明细数据不存在！", user.getLocale());
            }
            List<SgPhyOutNoticesUpdateRequest> requests = Lists.newArrayList();
            // 获取出库通知单编号
            sgBOutPickorderNoticeItemList.forEach(sgBOutPickorderNoticeItem -> {
                SgPhyOutNoticesUpdateRequest sgPhyOutNoticesUpdateRequest = new SgPhyOutNoticesUpdateRequest();
                // 出库通知单编号
                sgPhyOutNoticesUpdateRequest.setBillNo(sgBOutPickorderNoticeItem.getNoticeBillNo());
                // 拣货状态-未拣货
                sgPhyOutNoticesUpdateRequest.setPickStatus(SgInNoticeConstants.PICK_STATUS_WAIT);
                requests.add(sgPhyOutNoticesUpdateRequest);
            });
            // 更新出库通知单拣货状态为未拣货
            SgPhyOutNoticesSaveService sgPhyOutNoticesSaveService = ApplicationContextHandle.getBean(SgPhyOutNoticesSaveService.class);
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + ".debug, 更新出库通知单拣货状态为未拣货_param:" + JSONObject.toJSONString(requests));
            }
            ValueHolderV14 v14 = sgPhyOutNoticesSaveService.updateOutNoticesStatus(requests, user);
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + ".debug, 更新出库通知单拣货状态为未拣货_result:" + v14.toJSONObject());
            }
            if (v14.isOK()) {
                //更新出库拣货单的作废状态
                SgBOutPickorder updateModel = new SgBOutPickorder();
                updateModel.setId(objId);
                StorageESUtils.setBModelDefalutDataByUpdate(updateModel, user);
                updateModel.setModifierename(user.getEname());
                updateModel.setIsactive(SgConstants.IS_ACTIVE_N);//
                updateModel.setDelerId(user.getId().longValue());
                updateModel.setDelerName(user.getName());
                updateModel.setDelerEname(user.getEname());
                updateModel.setDelTime(new Date());
                updateModel.setDelStatus(SgInConstants.BILL_STATUS_VOID);//状态改为已作废
                try {
                    if (sgBOutPickorderMapper.updateById(updateModel) > 0) {
                        //推送ES
                        SgBOutPickorder sgBOutPickorderModel = sgBOutPickorderMapper.selectById(objId);
                        String index = SgConstants.SG_B_OUT_PICKORDER;
                        String type = SgConstants.SG_B_OUT_PICKORDER_ITEM;
                        StorageESUtils.pushESBModelByUpdate(sgBOutPickorderModel, null, sgBOutPickorderModel.getId(), null, index, index, type, null, SgBOutPickorder.class, SgBOutPickorderItem.class, false);
                    }
                    //调用小辉接口更新分销单据的拣货状态
                    invokeRpc(sgBOutPickorderNoticeItemList, user);
                } catch (Exception ex) {
                    throw new NDSException("出库拣货单作废失败！" + ex);
                }
            } else {
                AssertUtils.logAndThrow("出库拣货单作废异常！" + v14.getMessage(), user.getLocale());
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(this.getClass().getName() + "出库拣货单作废异常" + e);
            AssertUtils.logAndThrowExtra("出库拣货单作废异常", e);
        } finally {
            try {
                redisTemplate.delete(lockKsy);
            } catch (Exception e) {
                log.error(this.getClass().getName() + "redis锁释放失败，请联系管理员！lockKsy：" + lockKsy);
                AssertUtils.logAndThrow("redis锁释放失败，请联系管理员！");
            }
        }
        valueHolder.put("code", ResultCode.SUCCESS);
        valueHolder.put("message", "出库拣货单作废成功！");
        return valueHolder;
    }

    //根据拣货单对应的出库通知单的来源单据编号+来源单据类型 （调拨单、采购退货单、销售单、销售退货单）调用“来源单据的更新出库拣货状态服务”；
    private void invokeRpc(List<SgBOutPickorderNoticeItem> sgBOutPickorderNoticeItemList, User user) {

        //销售单
        List<Long> saleIds = Lists.newArrayList();
        //调拨单
        List<Long> tranIds = Lists.newArrayList();
        //采购退货单
        List<Long> purchaseReturnIds = Lists.newArrayList();
        //销售退货单
        List<Long> salesReturnIds = Lists.newArrayList();
        sgBOutPickorderNoticeItemList.forEach(sgBOutPickorderNoticeItem -> {
            //销售单
            if (sgBOutPickorderNoticeItem.getSourceBillType() != null && sgBOutPickorderNoticeItem.getSourceBillType().equals(SgConstantsIF.BILL_TYPE_SALE)) {
                saleIds.add(sgBOutPickorderNoticeItem.getSourceBillId());
            }
            //调拨单
            if (sgBOutPickorderNoticeItem.getSourceBillType() != null && sgBOutPickorderNoticeItem.getSourceBillType().equals(SgConstantsIF.BILL_TYPE_TRANSFER)) {
                tranIds.add(sgBOutPickorderNoticeItem.getSourceBillId());
            }
            //采购退货单
            if (sgBOutPickorderNoticeItem.getSourceBillType() != null && sgBOutPickorderNoticeItem.getSourceBillType().equals(SgConstantsIF.BILL_TYPE_PUR_REF)) {
                purchaseReturnIds.add(sgBOutPickorderNoticeItem.getSourceBillId());
            }
            //销售退货单
            if (sgBOutPickorderNoticeItem.getSourceBillType() != null && sgBOutPickorderNoticeItem.getSourceBillType().equals(SgConstantsIF.BILL_TYPE_SALE_REF)) {
                salesReturnIds.add(sgBOutPickorderNoticeItem.getSourceBillId());
            }
        });
        //调用销售单更新拣货类型服务
        if (CollectionUtils.isNotEmpty(saleIds)) {
            OcSaleUpdateCmd ocSaleUpdateCmd = (OcSaleUpdateCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                    OcSaleUpdateCmd.class.getName(), "oc", "1.0");
            OcSaleUpdatePickRequest ocSaleUpdatePickRequest = new OcSaleUpdatePickRequest();
            ocSaleUpdatePickRequest.setIds(saleIds);
            ocSaleUpdatePickRequest.setOutPickStatus(SgOutConstantsIF.PICK_STATUS_WAIT);
            ocSaleUpdatePickRequest.setUser(user);
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + ".debug, 调用销售单更新出库拣货类型服务_param:" + JSONObject.toJSONString(ocSaleUpdatePickRequest));
            }
            ValueHolderV14<List<OcSaleUpdatePickResult>> valueHolderV14 = ocSaleUpdateCmd.batchUpdateSalePickStatus(ocSaleUpdatePickRequest);
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + ".debug, 调用销售单更新出库拣货类型服务_result:" + valueHolderV14.toJSONObject());
            }
        }
        //调用调拨单更新拣货类型服务
        if (CollectionUtils.isNotEmpty(tranIds)) {
            SgTransferUpdatePickCmd sgTransferUpdatePickCmd = (SgTransferUpdatePickCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                    SgTransferUpdatePickCmd.class.getName(), "sg", "1.0");
            OcTransferUpdatePickRequest ocTransferUpdatePickRequest = new OcTransferUpdatePickRequest();
            ocTransferUpdatePickRequest.setIds(tranIds);
            ocTransferUpdatePickRequest.setOutPickStatus(SgOutConstantsIF.PICK_STATUS_WAIT);
            ocTransferUpdatePickRequest.setUser(user);
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + ".debug, 调用调拨单更新出库拣货类型服务_param:" + JSONObject.toJSONString(ocTransferUpdatePickRequest));
            }
            ValueHolderV14<List<OcTransferUpdatePickResult>> valueHolderV14 = sgTransferUpdatePickCmd.batchUpdateTransferPickStatus(ocTransferUpdatePickRequest);
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + ".debug, 调用调拨单更新出库拣货类型服务_result:" + valueHolderV14.toJSONObject());
            }
        }
        //调用销售退货单更新拣货类型服务
        if (CollectionUtils.isNotEmpty(salesReturnIds)) {
            OcRefundSaleUpdateCmd ocRefundSaleUpdateCmd = (OcRefundSaleUpdateCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                    OcRefundSaleUpdateCmd.class.getName(), "oc", "1.0");
            OcSaleUpdatePickRequest ocSaleUpdatePickRequest = new OcSaleUpdatePickRequest();
            ocSaleUpdatePickRequest.setIds(salesReturnIds);
            ocSaleUpdatePickRequest.setOutPickStatus(SgOutConstantsIF.PICK_STATUS_WAIT);
            ocSaleUpdatePickRequest.setUser(user);
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + ".debug, 调用销售退货单新出库拣货类型服务_param:" + JSONObject.toJSONString(ocSaleUpdatePickRequest));
            }
            ValueHolderV14<List<OcSaleUpdatePickResult>> valueHolderV14 = ocRefundSaleUpdateCmd.batchUpdateRefSalePickStatus(ocSaleUpdatePickRequest);
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + ".debug, 调用销售退货单更新出库拣货类型服务_result:" + valueHolderV14.toJSONObject());
            }
        }
        //调用采购退货单更新拣货类型服务
        if (CollectionUtils.isNotEmpty(purchaseReturnIds)) {
            OcRefundPurchaseUpdateCmd ocRefundPurchaseUpdateCmd = (OcRefundPurchaseUpdateCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                    OcRefundPurchaseUpdateCmd.class.getName(), "oc", "1.0");
            OcPurchaseUpdatePickRequest ocPurchaseUpdatePickRequest = new OcPurchaseUpdatePickRequest();
            ocPurchaseUpdatePickRequest.setIds(purchaseReturnIds);
            ocPurchaseUpdatePickRequest.setOutPickStatus(SgOutConstantsIF.PICK_STATUS_WAIT);
            ocPurchaseUpdatePickRequest.setUser(user);
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + ".debug, 调用采购退货单更新出库拣货类型服务_param:" + JSONObject.toJSONString(ocPurchaseUpdatePickRequest));
            }
            ValueHolderV14<List<OcPurchaseUpdatePickResult>> valueHolderV14 = ocRefundPurchaseUpdateCmd.batchUpdateRefPurchasePickStatus(ocPurchaseUpdatePickRequest);
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + ".debug, 调用采购退货单更新出库拣货类型服务_result:" + valueHolderV14.toJSONObject());
            }
        }
    }
}