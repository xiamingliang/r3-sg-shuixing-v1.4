package com.jackrain.nea.sg.out.services;

import com.jackrain.nea.sg.out.api.SgPhyOutCommitForPosCmd;
import com.jackrain.nea.sg.out.model.request.SgPhyOutCommitForPosRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author 舒威
 * @since 2019/11/21
 * create at : 2019/11/21 14:43
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgPhyOutCommitForPosCmdImpl implements SgPhyOutCommitForPosCmd {

    @Autowired
    private SgPhyOutCommitForPosService service;

    @Override
    public ValueHolderV14 saveAndAuditOutBills(SgPhyOutCommitForPosRequest request) {
        return service.saveAndAuditOutBills(request);
    }
}
