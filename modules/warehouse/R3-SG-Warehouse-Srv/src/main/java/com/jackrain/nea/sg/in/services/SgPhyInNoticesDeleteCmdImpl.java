package com.jackrain.nea.sg.in.services;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.in.api.SgPhyInNoticesDeleteCmd;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.query.QuerySession;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;


/**
 * @Author: chenchen
 * @Description:入库通知单删除Cmd
 * @Date: Create in 16:27 2019/4/22
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgPhyInNoticesDeleteCmdImpl implements SgPhyInNoticesDeleteCmd {
    @Autowired
    SgPhyInNoticesDeleteR3Service sgPhyInNoticesDeleteR3Service;

    @Override
    public ValueHolder execute(QuerySession session) throws NDSException {
        return sgPhyInNoticesDeleteR3Service.execute(session);
    }

    @Override
    public ValueHolder execute(HashMap map) throws NDSException {
        return null;
    }
}
