package com.jackrain.nea.sg.out.services;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.Feature;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.basic.common.R3ParamConstants;
import com.jackrain.nea.sg.basic.model.result.SgR3BaseResult;
import com.jackrain.nea.sg.out.api.SgPhyOutResultAuditR3Cmd;
import com.jackrain.nea.sg.out.model.request.SgPhyOutResultBillAuditRequest;
import com.jackrain.nea.sys.CommandAdapter;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.query.DefaultWebEvent;
import com.jackrain.nea.web.query.QuerySession;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

/**
 * @author leexh
 * @since 2019/4/25 14:08
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgPhyOutResultAuditR3CmdImpl extends CommandAdapter implements SgPhyOutResultAuditR3Cmd {

    @Override
    public ValueHolder execute(QuerySession session) throws NDSException {

        ValueHolder vh = new ValueHolder();
        vh.put(R3ParamConstants.CODE, ResultCode.FAIL);
        vh.put(R3ParamConstants.MESSAGE, "审核失败！");

        DefaultWebEvent event = session.getEvent();
        JSONObject param = JSON.parseObject(JSON.toJSONStringWithDateFormat(event.getParameterValue("param"),
                "yyyy-MM-dd HH:mm:ss", SerializerFeature.WriteMapNullValue), Feature.OrderedField);
        log.debug(this.getClass().getName() + ".debug, 入参：" + param);

        //是否列表审核，默认是
        boolean isList = true;
        JSONArray ids;
        ids = param.getJSONArray("ids");
        if (CollectionUtils.isEmpty(ids)) {
            isList = false;
            ids = new JSONArray();
            ids.add(param.getLong(R3ParamConstants.OBJID));
        }

        SgPhyOutResultBillAuditRequest request = new SgPhyOutResultBillAuditRequest();
        request.setLoginUser(session.getUser());
        request.setIds(ids);

        SgPhyOutResultAuditService service = ApplicationContextHandle.getBean(SgPhyOutResultAuditService.class);
        ValueHolderV14<SgR3BaseResult> result = service.auditSgPhyOutResult(request);
        if (result != null) {
            vh.put(R3ParamConstants.CODE, result.getCode());
            String message;
            SgR3BaseResult data = result.getData();
            if (isList) {
                message = result.getMessage();
            } else {
                if (data != null && CollectionUtils.isNotEmpty(data.getDataArr())) {
                    message = data.getDataArr().getJSONObject(0).getString("message");
                } else {
                    message = "审核成功!";
                }
            }
            vh.put(R3ParamConstants.MESSAGE, message);
            if (isList && data != null) {
                vh.put(R3ParamConstants.DATA, data.getDataArr());
            }
        }
        return vh;
    }
}
