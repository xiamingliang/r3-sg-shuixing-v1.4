package com.jackrain.nea.sg.in.services;

import com.jackrain.nea.sg.in.api.SgPhyOneClickLibraryCmd;
import com.jackrain.nea.sg.in.model.request.SgPhyOneClickLibraryRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

/**
 * @author: 舒威
 * @since: 2019/9/9
 * create at : 2019/9/9 9:48
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgPhyOneClickLibraryCmdImpl implements SgPhyOneClickLibraryCmd {

    @Override
    public ValueHolderV14 oneClickLibrary(SgPhyOneClickLibraryRequest request) {
        SgPhyOneClickLibraryService libraryService = ApplicationContextHandle.getBean(SgPhyOneClickLibraryService.class);
        return libraryService.oneClickLibrary(request);
    }
}
