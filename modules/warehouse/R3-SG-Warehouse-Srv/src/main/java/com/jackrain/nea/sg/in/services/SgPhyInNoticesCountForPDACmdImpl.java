package com.jackrain.nea.sg.in.services;

import com.jackrain.nea.sg.in.api.SgPhyInNoticesCountForPDACmd;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author: 周琳胜
 * @since: 2019/11/1
 * create at : 2019/11/1 13:55
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgPhyInNoticesCountForPDACmdImpl implements SgPhyInNoticesCountForPDACmd {
    @Autowired
    SgPhyInNoticesCountForPDAService sgPhyInNoticesCountForPDAService;

    @Override
    public ValueHolderV14 selectTransferTypeNum(Long storeId) {
        return sgPhyInNoticesCountForPDAService.selectTransferTypeNum(storeId);
    }
}
