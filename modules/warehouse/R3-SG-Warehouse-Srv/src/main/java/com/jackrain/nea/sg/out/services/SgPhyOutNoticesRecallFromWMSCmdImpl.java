package com.jackrain.nea.sg.out.services;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.Feature;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.basic.common.R3ParamConstants;
import com.jackrain.nea.sg.out.api.SgPhyOutNoticesRecallFromWMSCmd;
import com.jackrain.nea.sg.out.model.request.SgPhyOutBillRecallFromWmsRequest;
import com.jackrain.nea.sys.CommandAdapter;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.query.DefaultWebEvent;
import com.jackrain.nea.web.query.QuerySession;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

/**
 * 出库通知单从wms撤回
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgPhyOutNoticesRecallFromWMSCmdImpl extends CommandAdapter implements SgPhyOutNoticesRecallFromWMSCmd {
    @Override
    public ValueHolder execute(QuerySession session) throws NDSException {
        ValueHolder vh = new ValueHolder();
        vh.put(R3ParamConstants.CODE, ResultCode.FAIL);
        vh.put(R3ParamConstants.MESSAGE, "从WMS撤回失败 ！");

        DefaultWebEvent event = session.getEvent();
        JSONObject param = JSON.parseObject(JSON.toJSONStringWithDateFormat(event.getParameterValue("param"),
                "yyyy-MM-dd HH:mm:ss", SerializerFeature.WriteMapNullValue), Feature.OrderedField);
        log.debug(this.getClass().getName() + ".debug, 入参：" + param);

        SgPhyOutBillRecallFromWmsRequest request = new SgPhyOutBillRecallFromWmsRequest();
        request.setIsObj(false);
        JSONArray ids;
        ids = param.getJSONArray("ids");
        if (CollectionUtils.isEmpty(ids)) {
            ids = new JSONArray();
            ids.add(param.getLong(R3ParamConstants.OBJID));
            request.setIsObj(true);
        }

        request.setUser(session.getUser());
        request.setIds(ids);

        SgPhyOutNoticesRecallFromWMSService service = ApplicationContextHandle.getBean(SgPhyOutNoticesRecallFromWMSService.class);
        return service.recallPhyOutNoticesFromWMS(request);
    }
}
