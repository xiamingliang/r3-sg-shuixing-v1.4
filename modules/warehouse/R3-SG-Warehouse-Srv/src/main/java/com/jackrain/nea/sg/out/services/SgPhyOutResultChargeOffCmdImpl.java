package com.jackrain.nea.sg.out.services;

import com.jackrain.nea.sg.out.api.SgPhyOutResultChargeOffCmd;
import com.jackrain.nea.sg.out.model.request.SgOutResultMQRequest;
import com.jackrain.nea.sg.out.model.request.SgPhyOutResultBillSaveRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

/**
 * @author 舒威
 * @since 2019/12/13
 * create at : 2019/12/13 15:36
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgPhyOutResultChargeOffCmdImpl implements SgPhyOutResultChargeOffCmd {

    @Override
    public ValueHolderV14<SgOutResultMQRequest> chargeOffSgPhyOutResult(SgPhyOutResultBillSaveRequest request) {
        SgPhyOutResultChargeOffService chargeOffService = ApplicationContextHandle.getBean(SgPhyOutResultChargeOffService.class);
        return chargeOffService.saveAndAuditSgPhyOutResult(request);
    }
}
