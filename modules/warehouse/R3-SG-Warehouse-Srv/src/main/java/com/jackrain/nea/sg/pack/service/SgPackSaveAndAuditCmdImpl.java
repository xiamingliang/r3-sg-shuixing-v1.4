package com.jackrain.nea.sg.pack.service;

import com.jackrain.nea.sg.pack.api.SgPackSaveAndAuditCmd;
import com.jackrain.nea.sg.pack.model.request.SgPackSaveAndAuditRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

/**
 * @author: 周琳胜
 * @since: 2019/12/4
 * create at : 2019/12/4 11:20
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group ="sg")
public class SgPackSaveAndAuditCmdImpl implements SgPackSaveAndAuditCmd {

    @Override
    public ValueHolderV14 saveAndAuditSgPack(SgPackSaveAndAuditRequest request) {
        SgTeusPackSaveAndAuditService service = ApplicationContextHandle.getBean(SgTeusPackSaveAndAuditService.class);
        return service.saveAndAuditSgPack(request);
    }
}
