package com.jackrain.nea.sg.in.services;

import com.jackrain.nea.sg.in.api.SgPhyInNoticesItemQueryCmd;
import com.jackrain.nea.sg.in.model.request.SgPhyInNoticesQueryRequest;
import com.jackrain.nea.sg.in.model.table.SgBPhyInNoticesImpItem;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author 周琳胜
 * @since 2019-10-23
 * create at : 2019-10-23 20:05
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgPhyInNoticesItemQueryCmdImpl implements SgPhyInNoticesItemQueryCmd {
    @Override
    public ValueHolderV14<List<SgBPhyInNoticesImpItem>> queryInNoticesItem(SgPhyInNoticesQueryRequest request) {
        SgPhyInNoticesItemQueryService sgPhyInNoticesItemQueryService = ApplicationContextHandle.getBean(SgPhyInNoticesItemQueryService.class);
        return sgPhyInNoticesItemQueryService.queryInNoticesItem(request);
    }
}
