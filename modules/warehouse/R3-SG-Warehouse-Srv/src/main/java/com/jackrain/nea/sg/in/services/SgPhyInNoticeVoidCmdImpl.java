package com.jackrain.nea.sg.in.services;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.basic.model.result.SgR3BaseResult;
import com.jackrain.nea.sg.in.api.SgPhyInNoticeVoidCmd;
import com.jackrain.nea.sg.in.model.request.SgPhyInNoticesVoidReqeust;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Author: ChenChen
 * @Description:作废入库通知单服务
 * @Date: Create in 14:43 2019/5/5
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgPhyInNoticeVoidCmdImpl implements SgPhyInNoticeVoidCmd {
    @Autowired
    SgPhyInNoticesVoidService sgPhyInNoticesVoidService;

    @Override
    public ValueHolderV14<SgR3BaseResult> voidSgPhyInNotice(SgPhyInNoticesVoidReqeust request) throws NDSException {
        return sgPhyInNoticesVoidService.voidSgBPhyInNotices(request);
    }
}
