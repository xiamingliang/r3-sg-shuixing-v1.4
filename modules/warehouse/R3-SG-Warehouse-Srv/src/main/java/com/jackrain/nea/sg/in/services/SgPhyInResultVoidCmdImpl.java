package com.jackrain.nea.sg.in.services;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.Feature;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.basic.common.R3ParamConstants;
import com.jackrain.nea.sg.in.api.SgPhyInResultVoidCmd;
import com.jackrain.nea.sys.CommandAdapter;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.query.DefaultWebEvent;
import com.jackrain.nea.web.query.QuerySession;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgPhyInResultVoidCmdImpl extends CommandAdapter implements SgPhyInResultVoidCmd {

    /**
     * 入库结果单作废
     * @param session
     * @return
     * @throws NDSException
     */
    @Override
    public ValueHolder execute(QuerySession session) throws NDSException {
        ValueHolder vh=new ValueHolder();
        try{
            SgPhyInResultVoidService service=ApplicationContextHandle.getBean(SgPhyInResultVoidService.class);
            DefaultWebEvent event = session.getEvent();
            JSONObject param = JSON.parseObject(JSON.toJSONStringWithDateFormat(event.getParameterValue("param"),
                    "yyyy-MM-dd HH:mm:ss", SerializerFeature.WriteMapNullValue), Feature.OrderedField);
            if (log.isDebugEnabled()) {
                log.debug("Start SgPhyInResultVoidCmdImpl.ReceiveParams:="+ param + ";");
            }
            JSONArray ids = param.getJSONArray(R3ParamConstants.IDS);
            if (CollectionUtils.isEmpty(ids)) {
                throw new NDSException(Resources.getMessage("入参为空!", session.getUser().getLocale()));
            }
            ValueHolderV14 result= service.voidSgPhyInResult(ids,session.getUser());
            vh.put(R3ParamConstants.CODE,result.getCode());
            vh.put(R3ParamConstants.MESSAGE,result.getMessage());
            vh.put(R3ParamConstants.DATA,result.getData());
            return vh;
        }catch (Exception e){
            log.error(this.getClass().getName()+",error:"+e.toString());
            vh.put(R3ParamConstants.CODE,ResultCode.FAIL);
            vh.put(R3ParamConstants.MESSAGE,e.toString());
            return vh;
        }
    }
}
