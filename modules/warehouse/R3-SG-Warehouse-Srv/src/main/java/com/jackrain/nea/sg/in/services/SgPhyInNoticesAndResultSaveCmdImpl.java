package com.jackrain.nea.sg.in.services;

import com.jackrain.nea.sg.in.api.SgPhyInNoticesAndResultSaveCmd;
import com.jackrain.nea.sg.in.model.request.SgPhyInNoticesBillSaveRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Author: ChenChen
 * @Description:
 * @Date: Create in 14:52 2019/5/28
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgPhyInNoticesAndResultSaveCmdImpl implements SgPhyInNoticesAndResultSaveCmd {
    @Autowired
    SgPhyInNoticesSaveService sgPhyInNoticesSaveService;

    @Override
    public ValueHolderV14 saveSgBPhyInNoticesAndResult(List<SgPhyInNoticesBillSaveRequest> requestList) {
        return sgPhyInNoticesSaveService.saveSgBPhyInNoticesAndResult(requestList);
    }
}
