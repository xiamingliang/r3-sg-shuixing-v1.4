package com.jackrain.nea.sg.out.services;

import com.jackrain.nea.sg.out.api.SgPhyOutCommitForPDACmd;
import com.jackrain.nea.sg.out.model.request.SgPhyOutCommitForPDARequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

/**
 * @author zhu lin yu
 * @since 2019-10-29
 * create at : 2019-10-29 14:11
 */

@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgPhyOutCommitForPDACmdImpl implements SgPhyOutCommitForPDACmd {

    @Override
    public ValueHolderV14 saveAndAuditOutResult(SgPhyOutCommitForPDARequest request) {
        SgPhyOutCommitForPDAService sgPhyOutCommitForPDAService = ApplicationContextHandle.getBean(SgPhyOutCommitForPDAService.class);
        return sgPhyOutCommitForPDAService.saveAndAuditOutResult(request);
    }
}
