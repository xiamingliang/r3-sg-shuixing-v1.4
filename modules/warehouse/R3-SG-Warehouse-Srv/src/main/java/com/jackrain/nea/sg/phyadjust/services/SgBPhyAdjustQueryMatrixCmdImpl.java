package com.jackrain.nea.sg.phyadjust.services;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.phyadjust.api.SgBPhyAdjustQueryMatrixCmd;
import com.jackrain.nea.sys.CommandAdapter;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.query.QuerySession;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;


/**
 * @author csy
 * 库存调整单 商品tab查询
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sc")
public class SgBPhyAdjustQueryMatrixCmdImpl extends CommandAdapter implements SgBPhyAdjustQueryMatrixCmd {
    @Override
    public ValueHolder execute(QuerySession session) throws NDSException {
        SgBPhyAdjustMatrixService services = ApplicationContextHandle.getBean(SgBPhyAdjustMatrixService.class);
        return services.queryMatrix(session);
    }
}
