package com.jackrain.nea.sg.out.services;

import com.jackrain.nea.sg.out.api.SgPhyOutNoticesSaveWMSBackCmd;
import com.jackrain.nea.sg.out.model.request.SgPhyOutNoticesBillWMSBackRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author leexh
 * @since 2019/5/6 15:58
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgPhyOutNoticesSaveWMSBackCmdImpl implements SgPhyOutNoticesSaveWMSBackCmd {
    @Override
    public ValueHolderV14 saveWMSBackOutNotices(List<SgPhyOutNoticesBillWMSBackRequest> requests, User user) {

        SgPhyOutNoticesSaveWMSBackService service = ApplicationContextHandle.getBean(SgPhyOutNoticesSaveWMSBackService.class);
        return service.saveWMSBackOutNotices(requests, user);
    }
}
