package com.jackrain.nea.sg.in.services;

import com.jackrain.nea.sg.in.api.SgBInPickorderItemQueryCmd;
import com.jackrain.nea.sg.in.model.request.SgBInPickorderRequest;
import com.jackrain.nea.sg.in.model.result.SgBInPickorderResult;
import com.jackrain.nea.sg.in.model.table.SgBInPickorder;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

/**
 * @author: heliu
 * @since: 2019/11/18
 * create at : 2019/11/18 12:45
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgBInPickorderItemQueryCmdImpl implements SgBInPickorderItemQueryCmd {

    @Override
    public ValueHolderV14<SgBInPickorderResult> querySgBInPickorderItem(SgBInPickorderRequest request) {
        SgBInPickorderItemQueryService sgBInPickorderItemQueryService = ApplicationContextHandle.getBean(SgBInPickorderItemQueryService.class);
        return sgBInPickorderItemQueryService.querySgBInPickorderItem(request);
    }

    @Override
    public SgBInPickorder queryPickOrderBySourceBillNo(String sourceBillNo) {
        SgBInPickorderItemQueryService service = ApplicationContextHandle.getBean(SgBInPickorderItemQueryService.class);
        return service.queryPickOrderBySourceBillNo(sourceBillNo);
    }
}