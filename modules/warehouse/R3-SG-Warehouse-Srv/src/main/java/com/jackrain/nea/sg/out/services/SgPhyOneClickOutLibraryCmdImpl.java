package com.jackrain.nea.sg.out.services;

import com.jackrain.nea.sg.out.api.SgPhyOneClickOutLibraryCmd;
import com.jackrain.nea.sg.out.model.request.SgPhyOneClickOutLibraryRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

/**
 * @author: 舒威
 * @since: 2019/9/10
 * create at : 2019/9/10 11:16
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgPhyOneClickOutLibraryCmdImpl implements SgPhyOneClickOutLibraryCmd {

    @Override
    public ValueHolderV14 oneClickOutLibrary(SgPhyOneClickOutLibraryRequest request) {
        SgPhyOneClickOutLibraryService outLibraryService = ApplicationContextHandle.getBean(SgPhyOneClickOutLibraryService.class);
        return outLibraryService.oneClickOutLibrary(request);
    }
}
