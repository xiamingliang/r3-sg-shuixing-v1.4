package com.jackrain.nea.sg.out.services;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.Feature;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.basic.utils.R3ParamUtil;
import com.jackrain.nea.sg.out.api.SgPhyOutPickOrderGenerateCmd;
import com.jackrain.nea.sg.out.model.request.SgPhyOutPickUpGoodSaveRequest;
import com.jackrain.nea.sys.CommandAdapter;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.query.DefaultWebEvent;
import com.jackrain.nea.web.query.QuerySession;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

/**
 * @author zhu lin yu
 * @since 2019-11-08
 * create at : 2019-11-08 09:27
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgPhyOutPickOrderGenerateCmdImpl implements SgPhyOutPickOrderGenerateCmd {

    @Override
    public ValueHolderV14<Long> execute(QuerySession session) throws NDSException {
        ValueHolderV14<Long> holderV14 = new ValueHolderV14<>();
        try {
            DefaultWebEvent event = session.getEvent();
            SgPhyOutPickUpGoodSaveRequest request = JSONObject.parseObject(JSON.toJSONStringWithDateFormat(
                    event.getParameterValue("param"), "yyyy-MM-dd HH:mm:ss"), SgPhyOutPickUpGoodSaveRequest.class);
            request.setLoginUser(session.getUser());
            SgPhyOutPickOrderGenerateService service = ApplicationContextHandle.getBean(SgPhyOutPickOrderGenerateService.class);
            holderV14 = service.generatePhyOutPickUpGood(request);
        } catch (Exception e) {
            holderV14.setCode(ResultCode.FAIL);
            holderV14.setMessage(e.getMessage());
        }

        return holderV14;
    }
}
