package com.jackrain.nea.sg.out.services;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.Feature;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.basic.common.R3ParamConstants;
import com.jackrain.nea.sg.basic.model.result.SgR3BaseResult;
import com.jackrain.nea.sg.out.api.SgPhyOutNoticesPosSendCmd;
import com.jackrain.nea.sg.out.model.request.SgPhyOutNoticesPosRequest;
import com.jackrain.nea.sys.CommandAdapter;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.query.DefaultWebEvent;
import com.jackrain.nea.web.query.QuerySession;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 全渠道发货单发货服务
 *
 * @author 舒威
 * @since 2019/12/11
 * create at : 2019/12/11 11:37
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgPhyOutNoticesPosSendCmdImpl extends CommandAdapter implements SgPhyOutNoticesPosSendCmd {

    @Autowired
    private SgPhyOutNoticesPosSendService posSendService;

    @Override
    public ValueHolder execute(QuerySession session) throws NDSException {
        ValueHolder vh = new ValueHolder();
        vh.put(R3ParamConstants.CODE, ResultCode.FAIL);
        vh.put(R3ParamConstants.MESSAGE, "发货失败！");

        DefaultWebEvent event = session.getEvent();
        JSONObject param = JSON.parseObject(JSON.toJSONStringWithDateFormat(event.getParameterValue("param"),
                "yyyy-MM-dd HH:mm:ss", SerializerFeature.WriteMapNullValue), Feature.OrderedField);
        log.debug(this.getClass().getName() + ".debug, 入参：" + param);

        //是否列表发货，默认是
        boolean isList = true;
        JSONArray ids= new JSONArray();;
        if (CollectionUtils.isEmpty(param.getJSONArray("ids"))) {
            ids.add(param.getLong(R3ParamConstants.OBJID));
        }else {
            for (Object id : param.getJSONArray("ids")) {
                ids.add(Long.valueOf((String) id));
            }
        }

        SgPhyOutNoticesPosRequest request = new SgPhyOutNoticesPosRequest();
        request.setLoginUser(session.getUser());
        request.setIds(ids);

        ValueHolderV14<SgR3BaseResult> result = posSendService.sendPosOutNotices(request);
        if (result != null) {
            vh.put(R3ParamConstants.CODE, result.getCode());
            String message;
            SgR3BaseResult data = result.getData();
            if (isList) {
                message = result.getMessage();
            } else {
                if (data != null && CollectionUtils.isNotEmpty(data.getDataArr())) {
                    message = data.getDataArr().getJSONObject(0).getString("message");
                } else {
                    message = "发货成功!";
                }
            }
            vh.put(R3ParamConstants.MESSAGE, message);
            if (isList && data != null) {
                vh.put(R3ParamConstants.DATA, data.getDataArr());
            }
        }
        return vh;
    }
}
