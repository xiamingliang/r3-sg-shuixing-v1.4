package com.jackrain.nea.sg.in.services;



import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.in.api.SgPhyInNoticesSelectByPchR3Cmd;
import com.jackrain.nea.sys.CommandAdapter;
import com.jackrain.nea.util.ValueHolder;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;


@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgPhyInNoticesSelectByPchR3CmdImpl extends CommandAdapter implements SgPhyInNoticesSelectByPchR3Cmd {

    @Autowired
    private  SgPhyInNoticesSelectByPchService SgPhyInNoticesSelectByPchService;

    @Override
    public ValueHolder execute(HashMap hashMap) throws NDSException {
        return SgPhyInNoticesSelectByPchService.getSgPhyInNoticesByPch(hashMap);
    }
}
