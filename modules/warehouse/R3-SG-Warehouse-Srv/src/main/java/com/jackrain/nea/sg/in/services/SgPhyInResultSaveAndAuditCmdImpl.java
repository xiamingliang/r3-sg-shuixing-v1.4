package com.jackrain.nea.sg.in.services;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.in.api.SgPhyInResultSaveAndAuditCmd;
import com.jackrain.nea.sg.in.model.request.SgPhyInResultBillSaveRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgPhyInResultSaveAndAuditCmdImpl implements SgPhyInResultSaveAndAuditCmd {
    @Autowired
    private SgPhyInResultSaveAndAuditService sgPhyInResultSaveAndAuditService;

    @Override
    public ValueHolderV14 saveAndAuditBill(List<SgPhyInResultBillSaveRequest> billList) throws NDSException {
        return sgPhyInResultSaveAndAuditService.saveAndAuditBill(billList);
    }
}
