package com.jackrain.nea.sg.in.services;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.Feature;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.basic.common.R3ParamConstants;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.utils.R3ParamUtil;
import com.jackrain.nea.sg.in.api.SgPhyInResultSaveR3Cmd;
import com.jackrain.nea.sg.in.model.request.SgBPhyInResultItemSaveRequest;
import com.jackrain.nea.sg.in.model.request.SgBPhyInResultSaveRequest;
import com.jackrain.nea.sg.in.model.request.SgPhyInResultBillSaveRequest;
import com.jackrain.nea.sg.in.model.request.SgPhyInResultImpItemSaveRequest;
import com.jackrain.nea.sys.CommandAdapter;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.query.DefaultWebEvent;
import com.jackrain.nea.web.query.QuerySession;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 入库结果单(r3框架实现类)
 *
 * @author jiang.cj
 * @since 2019-4-22
 * create at : 2019-4-22
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgPhyInResultSaveR3CmdImpl extends CommandAdapter implements SgPhyInResultSaveR3Cmd {
    /**
     * 入库结果单新增(页面入口)
     *
     * @param session
     * @return
     * @throws NDSException
     */
    @Override
    public ValueHolder execute(QuerySession session) throws NDSException {
        ValueHolder vh = new ValueHolder();
        try {
            DefaultWebEvent event = session.getEvent();
            JSONObject param = JSON.parseObject(JSON.toJSONStringWithDateFormat(event.getParameterValue("param"),
                    "yyyy-MM-dd HH:mm:ss", SerializerFeature.WriteMapNullValue), Feature.OrderedField);
            log.debug("Start SgPhyInResultSaveR3CmdImpl.execute:入参：" + param);
            JSONObject fixcolumn = param.getJSONObject(R3ParamConstants.FIXCOLUMN);
            JSONObject obj = fixcolumn.getJSONObject(SgConstants.SG_B_PHY_IN_RESULT.toUpperCase());
            Long objId = param.getLongValue(R3ParamConstants.OBJID);

            //设置参数
            SgPhyInResultBillSaveRequest request = new SgPhyInResultBillSaveRequest();
            SgBPhyInResultSaveRequest billRequest = new SgBPhyInResultSaveRequest();
            request.setObjId(objId);
            if (obj != null) {
                if (obj.containsKey("SG_B_PHY_IN_NOTICES_ID")) {
                    Long noticesId = obj.getLongValue("SG_B_PHY_IN_NOTICES_ID");
                    billRequest.setSgBPhyInNoticesId(noticesId);
                }
            }
            request.setSgBPhyInResultSaveRequest(billRequest);

            List<SgBPhyInResultItemSaveRequest> itemList = null;
            if (fixcolumn.containsKey(SgConstants.SG_B_PHY_IN_RESULT_ITEM.toUpperCase())) {
                JSONArray itemArray = fixcolumn.getJSONArray(SgConstants.SG_B_PHY_IN_RESULT_ITEM.toUpperCase());
                itemList = JSONObject.parseArray(JSONObject.toJSONString(itemArray), SgBPhyInResultItemSaveRequest.class);
            }
            if (CollectionUtils.isNotEmpty(itemList)) {
                request.setItemList(itemList);
            }

            List<SgPhyInResultImpItemSaveRequest> impItemList = null;
            if (fixcolumn.containsKey(SgConstants.SG_B_PHY_IN_RESULT_IMP_ITEM.toUpperCase())) {
                JSONArray itemArray = fixcolumn.getJSONArray(SgConstants.SG_B_PHY_IN_RESULT_IMP_ITEM.toUpperCase());
                impItemList = JSONObject.parseArray(JSONObject.toJSONString(itemArray), SgPhyInResultImpItemSaveRequest.class);
            }
            if (CollectionUtils.isNotEmpty(impItemList)) {
                request.setImpItemList(impItemList);
            }

            request.setLoginUser(session.getUser());
            SgPhyInResultSaveR3Service service = ApplicationContextHandle.getBean(SgPhyInResultSaveR3Service.class);
            ValueHolderV14 result = service.saveSgBPhyInResult(request, fixcolumn);
            JSONObject jSONObject = new JSONObject();
            jSONObject.put(R3ParamConstants.TABLENAME, SgConstants.SG_B_PHY_IN_RESULT.toUpperCase());
            jSONObject.put(R3ParamConstants.OBJID, result.getData());
            vh.put(R3ParamConstants.CODE, result.getCode());
            vh.put(R3ParamConstants.MESSAGE, result.getMessage());
            vh.put(R3ParamConstants.DATA, jSONObject);
            return vh;
        } catch (Exception e) {
            log.error(this.getClass().getName() + ",error:" + e.toString());
            vh.put(R3ParamConstants.CODE, ResultCode.FAIL);
            vh.put(R3ParamConstants.MESSAGE, e.toString());
            return vh;
        }

    }
}
