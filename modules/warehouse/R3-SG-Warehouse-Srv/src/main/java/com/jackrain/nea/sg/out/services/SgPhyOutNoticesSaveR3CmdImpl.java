package com.jackrain.nea.sg.out.services;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.Feature;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.basic.utils.R3ParamUtil;
import com.jackrain.nea.sg.out.api.SgPhyOutNoticesSaveR3Cmd;
import com.jackrain.nea.sg.out.model.request.SgPhyOutNoticesBillSaveRequest;
import com.jackrain.nea.sys.CommandAdapter;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.query.DefaultWebEvent;
import com.jackrain.nea.web.query.QuerySession;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

/**
 * @author leexh
 * @since 2019/4/19 14:37
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgPhyOutNoticesSaveR3CmdImpl extends CommandAdapter implements SgPhyOutNoticesSaveR3Cmd {

    @Override
    public ValueHolder execute(QuerySession session) throws NDSException {

        ValueHolder vh = new ValueHolder();
        vh.put("code", ResultCode.FAIL);
        vh.put("message", "保存失败！");

        DefaultWebEvent event = session.getEvent();
        JSONObject param = JSON.parseObject(JSON.toJSONStringWithDateFormat(event.getParameterValue("param"),
                "yyyy-MM-dd HH:mm:ss", SerializerFeature.WriteMapNullValue), Feature.OrderedField);
        log.debug(this.getClass().getName() + ".debug, 入参：" + param);

        SgPhyOutNoticesBillSaveRequest request = R3ParamUtil.parseSaveObject(session, SgPhyOutNoticesBillSaveRequest.class);
        SgPhyOutNoticesSaveService service = ApplicationContextHandle.getBean(SgPhyOutNoticesSaveService.class);
        ValueHolderV14 result = service.saveSgPhyOutNotices(request);

        if (result != null) {
            vh.put("code", result.getCode());
            vh.put("message", result.getMessage());
            vh.put("data", result.getData());
        }
        return vh;
    }
}
