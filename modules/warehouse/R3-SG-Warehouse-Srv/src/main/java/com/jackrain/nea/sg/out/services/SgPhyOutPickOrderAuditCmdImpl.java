package com.jackrain.nea.sg.out.services;

import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.out.api.SgPhyOutPickOrderAuditCmd;
import com.jackrain.nea.sg.out.model.request.SgPhyOutPickOrderAuditRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

/**
 * @author zhu lin yu
 * @since 2019-11-20
 * create at : 2019-11-20 16:51
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgPhyOutPickOrderAuditCmdImpl implements SgPhyOutPickOrderAuditCmd {

    @Override
    public ValueHolderV14 auditOutPickOrder(SgPhyOutPickOrderAuditRequest request) {
        ValueHolderV14 holderV14 = new ValueHolderV14();
        try {
            SgPhyOutPickOrderAuditService service = ApplicationContextHandle.getBean(SgPhyOutPickOrderAuditService.class);
            holderV14 = service.auditOutPickOrder(request);
        } catch (Exception e) {
            holderV14.setMessage(e.getMessage());
            holderV14.setCode(ResultCode.FAIL);
        }
        return holderV14;
    }
}
