package com.jackrain.nea.sg.out.services;

import com.jackrain.nea.sg.basic.model.result.SgR3BaseResult;
import com.jackrain.nea.sg.out.api.SgPhyOutResultSaveCmd;
import com.jackrain.nea.sg.out.model.request.SgPhyOutResultBillSaveRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

/**
 * @author leexh
 * @since 2019/4/24 14:59
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgPhyOutResultSaveCmdImpl implements SgPhyOutResultSaveCmd {

    @Override
    public ValueHolderV14<SgR3BaseResult> saveSgPhyOutResult(SgPhyOutResultBillSaveRequest request) {
        SgPhyOutResultSaveService service = ApplicationContextHandle.getBean(SgPhyOutResultSaveService.class);
        return service.saveSgPhyOutResult(request);
    }
}
