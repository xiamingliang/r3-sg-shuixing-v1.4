package com.jackrain.nea.sg.in.services;

import com.jackrain.nea.sg.in.api.SgPhyInNoticesSelectBySourceCmd;
import com.jackrain.nea.sg.in.model.request.SgPhyInNoticesSelectBySourceRequest;
import com.jackrain.nea.sg.in.model.result.SgBPhyInNoticesResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Author: ChenChen
 * @Description:
 * @Date: Create in 20:13 2019/5/16
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgPhyInNoticesSelectBySourceCmdImpl implements SgPhyInNoticesSelectBySourceCmd {
    @Autowired
    SgPhyInNoticesSelectService sgPhyInNoticesSelectService;

    @Override

    public ValueHolderV14<List<SgBPhyInNoticesResult>> selectBySource(SgPhyInNoticesSelectBySourceRequest request) {
        return sgPhyInNoticesSelectService.selectBySource(request);
    }
}
