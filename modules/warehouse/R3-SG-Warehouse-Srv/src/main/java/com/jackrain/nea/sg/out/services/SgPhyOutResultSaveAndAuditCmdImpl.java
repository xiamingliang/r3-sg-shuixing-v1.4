package com.jackrain.nea.sg.out.services;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.sg.basic.model.result.SgR3BaseResult;
import com.jackrain.nea.sg.out.api.SgPhyOutResultSaveAndAuditCmd;
import com.jackrain.nea.sg.out.model.request.SgPhyOutResultBillSaveRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

/**
 * @author leexh
 * @since 2019/5/7 16:53
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgPhyOutResultSaveAndAuditCmdImpl implements SgPhyOutResultSaveAndAuditCmd {
    @Override
    public ValueHolderV14<SgR3BaseResult> saveOutResultAndAudit(SgPhyOutResultBillSaveRequest request) {
        SgPhyOutResultSaveAndAuditService service = ApplicationContextHandle.getBean(SgPhyOutResultSaveAndAuditService.class);
        return service.saveOutResultAndAudit(request);
    }

    @Override
    public ValueHolderV14 saveOutResultAndAuditTask(JSONObject param, SgPhyOutResultBillSaveRequest requestModel) {
        SgWmsToPhyOutResultService service = ApplicationContextHandle.getBean(SgWmsToPhyOutResultService.class);
        return service.wmsToPhyOutResult(param);
    }
}
