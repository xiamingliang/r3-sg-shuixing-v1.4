package com.jackrain.nea.sg.inv.services;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.inv.api.ScBInventoryVoidCmd;
import com.jackrain.nea.sys.CommandAdapter;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.query.QuerySession;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

/**
 * @author: 周琳胜
 * @since: 2019/11/13
 * create at : 2019/11/13 11:07
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class ScBInventoryVoidCmdImpl extends CommandAdapter implements ScBInventoryVoidCmd {

    @Override
    public ValueHolder execute(QuerySession session) throws NDSException {
        ScBInventoryVoidService service = ApplicationContextHandle.getBean(ScBInventoryVoidService.class);
        return service.scbInventoryVoid(session);
    }
}
