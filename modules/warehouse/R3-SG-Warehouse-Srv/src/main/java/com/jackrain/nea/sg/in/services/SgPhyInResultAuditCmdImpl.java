package com.jackrain.nea.sg.in.services;

import com.jackrain.nea.sg.in.api.SgPhyInResultAuditCmd;
import com.jackrain.nea.sg.in.common.SgInConstants;
import com.jackrain.nea.sg.in.model.request.SgPhyInResultBillAuditRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgPhyInResultAuditCmdImpl implements SgPhyInResultAuditCmd {

    @Autowired
    private SgPhyInResultAuditService sgPhyInResultAuditService;

    @Override
    public ValueHolderV14 auditSgPhyInResult(SgPhyInResultBillAuditRequest request) {
        return sgPhyInResultAuditService.auditSgPhyInResult(request);
    }
}
