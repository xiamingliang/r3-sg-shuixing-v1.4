package com.jackrain.nea.sg.out.services;

import com.jackrain.nea.sg.out.api.SgBOutPickorderItemQueryCmd;
import com.jackrain.nea.sg.out.model.request.SgBOutPickorderRequest;
import com.jackrain.nea.sg.out.model.result.SgBOutPickorderResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

/**
 * @author: heliu
 * @since: 2019/11/18
 * create at : 2019/11/18 12:51
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgBOutPickorderItemQueryItemCmdImpl implements SgBOutPickorderItemQueryCmd {
    @Override
    public ValueHolderV14<SgBOutPickorderResult> querySgBOutPickorderItem(SgBOutPickorderRequest request) {
        SgBOutPickorderItemQueryService sgBOutPickorderItemQueryService = ApplicationContextHandle.getBean(SgBOutPickorderItemQueryService.class);
        return sgBOutPickorderItemQueryService.querySgBOutPickorderItem(request);
    }
}