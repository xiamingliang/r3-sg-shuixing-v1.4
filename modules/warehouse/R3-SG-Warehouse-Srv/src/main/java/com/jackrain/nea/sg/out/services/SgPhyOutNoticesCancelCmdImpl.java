package com.jackrain.nea.sg.out.services;

import com.jackrain.nea.sg.out.api.SgPhyOutNoticesCancelCmd;
import com.jackrain.nea.sg.out.model.request.SgOutResultMQRequest;
import com.jackrain.nea.sg.out.model.request.SgPhyOutNoticesCancelRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 取消出库服务
 *
 * @author 舒威
 * @since 2019/12/11
 * create at : 2019/12/11 15:51
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgPhyOutNoticesCancelCmdImpl implements SgPhyOutNoticesCancelCmd {

    @Autowired
    private SgPhyOutNoticesCancelService cancelService;

    @Override
    public ValueHolderV14<SgOutResultMQRequest> cancelSgPhyOutNotices(SgPhyOutNoticesCancelRequest request) {
        return cancelService.cancelSgPhyOutNotices(request);
    }
}
