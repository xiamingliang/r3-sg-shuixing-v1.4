package com.jackrain.nea.sg.unpack.service;

import com.jackrain.nea.sg.unpack.api.SgUnpackUpdateForWmsCmd;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

/**
 * @author: 周琳胜
 * @since: 2019/12/4
 * create at : 2019/12/4 11:20
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgUnpackUpdateForWmsCmdImpl implements SgUnpackUpdateForWmsCmd {

    @Override
    public ValueHolderV14 saveSgUnpackForWms(ValueHolderV14 holderV14) {
        SgUnpackUpdateForWmsService service = ApplicationContextHandle.getBean(SgUnpackUpdateForWmsService.class);
        return service.updateSgUnpack(holderV14);
    }
}
