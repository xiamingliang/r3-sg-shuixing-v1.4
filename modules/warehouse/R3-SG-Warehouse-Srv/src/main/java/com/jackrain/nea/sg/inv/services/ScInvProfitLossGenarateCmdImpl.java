package com.jackrain.nea.sg.inv.services;

import com.jackrain.nea.sg.inv.api.ScInvProfitLossGenarateCmd;
import com.jackrain.nea.sg.inv.model.request.ScInvProfitLossMarginQueryCmdRequest;
import com.jackrain.nea.sg.inv.model.result.ScInvProfitLossGenarateQueryResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

/**
 * @author 舒威
 * @since 2019/4/3
 * create at : 2019/4/3 11:43
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class ScInvProfitLossGenarateCmdImpl implements ScInvProfitLossGenarateCmd {

    @Override
    public ValueHolderV14<ScInvProfitLossGenarateQueryResult> queryProfitLossGenarateInfo(ScInvProfitLossMarginQueryCmdRequest model) {
        ScInvProfitLossGenarateService service = ApplicationContextHandle.getBean(ScInvProfitLossGenarateService.class);
        return service.queryProfitLossGenarateInfo(model);
    }

    @Override
    public ValueHolderV14 genarateProfitLoss(ScInvProfitLossMarginQueryCmdRequest model) {
        ScInvProfitLossGenarateService service = ApplicationContextHandle.getBean(ScInvProfitLossGenarateService.class);
        return service.genarateProfitLoss(model);
    }

    @Override
    public ValueHolderV14<ScInvProfitLossGenarateQueryResult> queryProfitLossTotalInventory(ScInvProfitLossMarginQueryCmdRequest model) {
        ScInvProfitLossTotalService service = ApplicationContextHandle.getBean(ScInvProfitLossTotalService.class);
        return service.queryProfitLossTotalInventory(model);
    }
}
