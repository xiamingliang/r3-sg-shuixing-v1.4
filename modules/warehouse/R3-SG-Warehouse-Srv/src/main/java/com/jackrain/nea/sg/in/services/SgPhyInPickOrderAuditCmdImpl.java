package com.jackrain.nea.sg.in.services;

import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.in.api.SgPhyInPickOrderAuditCmd;
import com.jackrain.nea.sg.in.model.request.SgPhyInPickOrderAuditRequest;
import com.jackrain.nea.sg.out.api.SgPhyOutPickOrderAuditCmd;
import com.jackrain.nea.sg.out.model.request.SgPhyOutPickOrderAuditRequest;
import com.jackrain.nea.sg.out.services.SgPhyOutPickOrderAuditService;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

/**
 * @author zhu lin yu
 * @since 2019-11-20
 * create at : 2019-11-20 16:51
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgPhyInPickOrderAuditCmdImpl implements SgPhyInPickOrderAuditCmd {

    @Override
    public ValueHolderV14 auditInPickOrder(SgPhyInPickOrderAuditRequest request) {
        SgPhyInPickOrderAuditService service = ApplicationContextHandle.getBean(SgPhyInPickOrderAuditService.class);
        ValueHolderV14 holderV14 = new ValueHolderV14();
        try {
             holderV14 = service.auditInPickOrder(request, null);
        } catch (Exception e) {
            holderV14.setMessage(e.getMessage());
            holderV14.setCode(ResultCode.FAIL);
        }
        return holderV14;
    }
}
