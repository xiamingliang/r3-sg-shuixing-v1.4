package com.jackrain.nea.sg.in.services;

import com.jackrain.nea.sg.in.api.SgPhyInNoticesSendMQCmd;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

import java.util.List;



/**
 *  通过MQ获取通知单，包装成入库结果单，调入库结果单新增服务
 * @author jiang.cj
 * @since 2019-05-07
 * create at : 2019-05-07
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgPhyInNoticesSendMQCmdImpl implements SgPhyInNoticesSendMQCmd {

    @Override
    public ValueHolderV14 saveSgBPhyInResultByMq(List<Long> billIds) {
       SgPhyInNoticesSendMQService sgPhyInNoticesSendMQService=ApplicationContextHandle.getBean(SgPhyInNoticesSendMQService.class);
        return sgPhyInNoticesSendMQService.saveSgBPhyInResult(billIds);
    }
}
