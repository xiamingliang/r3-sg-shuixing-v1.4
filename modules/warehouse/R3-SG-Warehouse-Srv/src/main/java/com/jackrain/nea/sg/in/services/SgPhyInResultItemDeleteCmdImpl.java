package com.jackrain.nea.sg.in.services;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sys.Command;
import com.jackrain.nea.sys.CommandAdapter;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.query.QuerySession;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgPhyInResultItemDeleteCmdImpl  extends CommandAdapter implements Command {


    @Autowired
    private SgPhyInResultSaveR3Service sgPhyInResultSaveR3Service;
    /**
     * 删除入库结果单明细
     * @param session
     * @return
     * @throws NDSException
     */
    @Override
    public ValueHolder execute(QuerySession session) throws NDSException {
        return sgPhyInResultSaveR3Service.deleteSgPhyInResultItem(session);
    }
}
