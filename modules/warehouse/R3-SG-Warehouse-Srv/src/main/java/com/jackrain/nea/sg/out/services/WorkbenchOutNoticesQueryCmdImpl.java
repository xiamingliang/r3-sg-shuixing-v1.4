package com.jackrain.nea.sg.out.services;

import com.jackrain.nea.sg.out.api.WorkbenchOutNoticesQueryCmd;
import com.jackrain.nea.sg.out.model.request.WorkbenchOutNoticesPageQueryRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @description: 工作台-出库通知单服务
 * @author: 郑小龙
 * @date: 2019-05-15 18:04
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class WorkbenchOutNoticesQueryCmdImpl implements WorkbenchOutNoticesQueryCmd {

    @Autowired
    public WorkbenchOutNoticesQueryService service;

    @Override
    public ValueHolderV14 queryPassWmsFailure(WorkbenchOutNoticesPageQueryRequest pageQueryRequest){
        return service.queryPassWmsFailure(pageQueryRequest.getQueryRequest());
    }

    @Override
    public ValueHolderV14 queryWareSuperCutTimeNoHair(WorkbenchOutNoticesPageQueryRequest pageQueryRequest){
        return service.queryWareSuperCutTimeNoHair(pageQueryRequest.getQueryRequest());
    }
}
