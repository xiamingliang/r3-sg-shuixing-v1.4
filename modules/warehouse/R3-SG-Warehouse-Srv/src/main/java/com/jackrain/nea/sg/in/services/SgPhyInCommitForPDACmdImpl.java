package com.jackrain.nea.sg.in.services;

import com.jackrain.nea.sg.in.api.SgPhyInCommitForPDACmd;
import com.jackrain.nea.sg.in.model.request.SgPhyInCommitForPDARequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

/**
 * @author zhoulinsheng
 * @since 2019-10-29
 * create at : 2019-10-29 14:11
 */

@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgPhyInCommitForPDACmdImpl implements SgPhyInCommitForPDACmd {

    @Override
    public ValueHolderV14 saveAndAuditInResult(SgPhyInCommitForPDARequest request) {
        SgPhyInCommitForPDAService sgPhyInCommitForPDAService = ApplicationContextHandle.getBean(SgPhyInCommitForPDAService.class);
        return sgPhyInCommitForPDAService.saveAndAuditInResult(request);
    }
}
