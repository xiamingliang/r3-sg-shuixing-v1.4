package com.jackrain.nea.sg.out.services;

import com.jackrain.nea.sg.basic.model.result.SgR3BaseResult;
import com.jackrain.nea.sg.out.api.SgPhyOutResultAuditCmd;
import com.jackrain.nea.sg.out.model.request.SgPhyOutResultBillAuditRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author leexh
 * @since 2019/4/25 14:05
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgPhyOutResultAuditCmdImpl implements SgPhyOutResultAuditCmd {
    @Override
    public ValueHolderV14<SgR3BaseResult> auditSgPhyOutResult(SgPhyOutResultBillAuditRequest request) {

        SgPhyOutResultAuditService service = ApplicationContextHandle.getBean(SgPhyOutResultAuditService.class);
        return service.auditSgPhyOutResult(request);
    }

    @Override
    public ValueHolderV14 auditSgPhyOutResultByWMS(List<Long> ids, User user) {
        SgPhyOutResultAuditWMSService bean = ApplicationContextHandle.getBean(SgPhyOutResultAuditWMSService.class);
        return bean.auditSgPhyOutResultByWms(ids, user);
    }
}
