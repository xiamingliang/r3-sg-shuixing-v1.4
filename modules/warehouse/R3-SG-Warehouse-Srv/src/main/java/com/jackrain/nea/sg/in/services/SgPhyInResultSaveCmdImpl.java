package com.jackrain.nea.sg.in.services;

import com.jackrain.nea.sg.in.api.SgPhyInResultSaveCmd;
import com.jackrain.nea.sg.in.model.request.SgPhyInResultBillSaveRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 *  入库结果单(包装实现类)
 * @author jiang.cj
 * @since 2019-4-22
 * create at : 2019-4-22
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgPhyInResultSaveCmdImpl implements SgPhyInResultSaveCmd {

    @Autowired
    private SgPhyInResultSaveService sgPhyInResultSaveService;


    /**
     *  入库结果单实现类
     * @author jiang.cj
     * @since 2019-4-22
     * create at : 2019-4-22
     * @return
     */
    @Override
    public ValueHolderV14 saveSgBPhyInResult(SgPhyInResultBillSaveRequest request) {
        return sgPhyInResultSaveService.saveSgBPhyInResult(request);
    }
}
