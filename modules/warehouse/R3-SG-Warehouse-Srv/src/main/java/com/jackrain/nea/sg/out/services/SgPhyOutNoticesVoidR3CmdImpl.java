package com.jackrain.nea.sg.out.services;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.Feature;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.basic.common.R3ParamConstants;
import com.jackrain.nea.sg.out.api.SgPhyOutNoticesVoidR3Cmd;
import com.jackrain.nea.sg.out.model.request.SgPhyOutNoticesBillVoidRequest;
import com.jackrain.nea.sys.CommandAdapter;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.query.DefaultWebEvent;
import com.jackrain.nea.web.query.QuerySession;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author leexh
 * @since 2019/4/23 15:18
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgPhyOutNoticesVoidR3CmdImpl extends CommandAdapter implements SgPhyOutNoticesVoidR3Cmd {

    @Override
    public ValueHolder execute(QuerySession session) throws NDSException {

        ValueHolder vh = new ValueHolder();
        vh.put(R3ParamConstants.CODE, ResultCode.FAIL);
        vh.put(R3ParamConstants.MESSAGE, "作废失败！");

        DefaultWebEvent event = session.getEvent();
        JSONObject param = JSON.parseObject(JSON.toJSONStringWithDateFormat(event.getParameterValue("param"),
                "yyyy-MM-dd HH:mm:ss", SerializerFeature.WriteMapNullValue), Feature.OrderedField);
        log.debug(this.getClass().getName() + ".debug, 入参：" + param);

        List<Long> list = new ArrayList<>();
        list.add(param.getLong(R3ParamConstants.OBJID));

        SgPhyOutNoticesBillVoidRequest request = new SgPhyOutNoticesBillVoidRequest();
        request.setIds(list);

        SgPhyOutNoticesVoidService service = ApplicationContextHandle.getBean(SgPhyOutNoticesVoidService.class);
        ValueHolderV14 result = service.voidSgPhyOutNotices(request);

        if (result != null) {
            vh.put(R3ParamConstants.CODE, result.getCode());
            vh.put(R3ParamConstants.MESSAGE, result.getMessage());
            vh.put(R3ParamConstants.DATA, result.getData());
        }
        return vh;
    }
}
