package com.jackrain.nea.sg.in.services;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.in.api.SgPhyInPickOrderGenerateCmd;
import com.jackrain.nea.sg.in.model.request.SgPhyInPickUpGoodSaveRequest;
import com.jackrain.nea.sg.out.api.SgPhyOutPickOrderGenerateCmd;
import com.jackrain.nea.sg.out.model.request.SgPhyOutPickUpGoodSaveRequest;
import com.jackrain.nea.sg.out.services.SgPhyOutPickOrderGenerateService;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.query.DefaultWebEvent;
import com.jackrain.nea.web.query.QuerySession;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

/**
 * @author zhu lin yu
 * @since 2019-11-08
 * create at : 2019-11-08 09:27
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgPhyInPickOrderGenerateCmdImpl implements SgPhyInPickOrderGenerateCmd {

    @Override
    public ValueHolderV14<Long> execute(QuerySession session) throws NDSException {

        ValueHolderV14<Long> holderV14 = new ValueHolderV14<>();

        try {
            DefaultWebEvent event = session.getEvent();
            SgPhyInPickUpGoodSaveRequest request = JSONObject.parseObject(JSON.toJSONStringWithDateFormat(
                    event.getParameterValue("param"), "yyyy-MM-dd HH:mm:ss"), SgPhyInPickUpGoodSaveRequest.class);
            request.setLoginUser(session.getUser());
            SgPhyInPickOrderGenerateService service = ApplicationContextHandle.getBean(SgPhyInPickOrderGenerateService.class);
            holderV14 = service.generatePhyInPickUpGood(request);
        } catch (Exception e) {
            holderV14.setMessage(e.getMessage());
            holderV14.setCode(ResultCode.FAIL);
        }
        return holderV14;
    }

    @Override
    public ValueHolderV14<Long> invoke(  SgPhyInPickUpGoodSaveRequest request) throws NDSException {

        ValueHolderV14<Long> holderV14 = new ValueHolderV14<>();

        try {
            SgPhyInPickOrderGenerateService service = ApplicationContextHandle.getBean(SgPhyInPickOrderGenerateService.class);
            holderV14 = service.generatePhyInPickUpGood(request);
        } catch (Exception e) {
            holderV14.setMessage(e.getMessage());
            holderV14.setCode(ResultCode.FAIL);
        }
        return holderV14;
    }
}
