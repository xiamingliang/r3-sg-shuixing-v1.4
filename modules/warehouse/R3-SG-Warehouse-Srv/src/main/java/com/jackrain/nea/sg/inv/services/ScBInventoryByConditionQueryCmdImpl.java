package com.jackrain.nea.sg.inv.services;

import com.jackrain.nea.sg.in.model.result.ScBInventoryResult;
import com.jackrain.nea.sg.inv.api.ScBInventoryByConditionQueryCmd;
import com.jackrain.nea.sg.inv.model.request.ScBInventoryQueryRequest;
import com.jackrain.nea.sg.inv.model.table.ScBInventory;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author: 周琳胜
 * @since: 2019/11/26
 * create at : 2019/11/26 21:30
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class ScBInventoryByConditionQueryCmdImpl implements ScBInventoryByConditionQueryCmd{

    @Override
    public ValueHolderV14<ScBInventoryResult> queryInventory(ScBInventoryQueryRequest request) {
        ScBInventoryByConditionQueryService service = ApplicationContextHandle.getBean(ScBInventoryByConditionQueryService.class);
        return service.queryInventory(request);
    }
}
