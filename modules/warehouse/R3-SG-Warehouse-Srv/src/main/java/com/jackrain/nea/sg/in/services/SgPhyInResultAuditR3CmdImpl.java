package com.jackrain.nea.sg.in.services;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.Feature;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.basic.common.R3ParamConstants;
import com.jackrain.nea.sg.in.api.SgPhyInResultAuditR3Cmd;
import com.jackrain.nea.sg.in.model.request.SgPhyInResultBillAuditRequest;
import com.jackrain.nea.sg.in.model.result.ReturnSgPhyInResult;
import com.jackrain.nea.sys.CommandAdapter;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.query.DefaultWebEvent;
import com.jackrain.nea.web.query.QuerySession;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgPhyInResultAuditR3CmdImpl extends CommandAdapter implements SgPhyInResultAuditR3Cmd {

    /**
     * 入库结果单审核(入口页面)
     *
     * @param session
     * @return
     * @throws NDSException
     */
    @Override
    public ValueHolder execute(QuerySession session) throws NDSException {
        ValueHolder vh = new ValueHolder();
        try {
            SgPhyInResultAuditService service = ApplicationContextHandle.getBean(SgPhyInResultAuditService.class);
            DefaultWebEvent event = session.getEvent();
            JSONObject param = JSON.parseObject(JSON.toJSONStringWithDateFormat(event.getParameterValue("param"),
                    "yyyy-MM-dd HH:mm:ss", SerializerFeature.WriteMapNullValue), Feature.OrderedField);
            log.debug(this.getClass().getName() + ".execute,入参:" + param);
            JSONArray ids = null;
            //是否列表审核，默认否
            boolean isList = false;
            if (param.containsKey(R3ParamConstants.IDS)) {
                //列表页面审核
                isList = true;
                ids = param.getJSONArray(R3ParamConstants.IDS);
                if (CollectionUtils.isEmpty(ids)) {
                    throw new NDSException(Resources.getMessage("入参为空!", session.getUser().getLocale()));
                }
            } else {
                //单对象审核
                ids = new JSONArray();
                ids.add(param.getLongValue(R3ParamConstants.OBJID));
            }

            SgPhyInResultBillAuditRequest request = new SgPhyInResultBillAuditRequest();
            request.setIds(ids);
            request.setLoginUser(session.getUser());
            ValueHolderV14<List<ReturnSgPhyInResult>> result = service.auditSgPhyInResult(request);
            List<ReturnSgPhyInResult> list = result.getData();
            int success = 0;
            int fail = 0;
            JSONArray errorArr = new JSONArray();
            if (CollectionUtils.isNotEmpty(list)) {
                for (ReturnSgPhyInResult re : list) {
                    if (re.getCode() == ResultCode.SUCCESS) {
                        success++;
                    } else {
                        fail++;
                        JSONObject errorDate = new JSONObject();
                        errorDate.put(R3ParamConstants.CODE, ResultCode.FAIL);
                        errorDate.put(R3ParamConstants.OBJID, re.getObjid());
                        errorDate.put(R3ParamConstants.MESSAGE, re.getMessage());
                        errorArr.add(errorDate);
                    }
                }
            }
            vh.put(R3ParamConstants.CODE, fail > 0 ? ResultCode.FAIL : ResultCode.SUCCESS);
            vh.put(R3ParamConstants.MESSAGE, getMessage(isList, success, fail, errorArr));
            if (isList && fail > 0) {
                vh.put(R3ParamConstants.DATA, errorArr);
            }
            return vh;
        } catch (Exception e) {
            log.error(this.getClass().getName() + ",error:" + e.toString());
            vh.put(R3ParamConstants.CODE, ResultCode.FAIL);
            vh.put(R3ParamConstants.MESSAGE, e.toString());
            return vh;
        }
    }

    public String getMessage(boolean isList, int success, int fail, JSONArray errorArr) {
        String msg = "";
        if (isList) {
            msg = "审核成功记录数:" + success + ",审核失败记录数:" + fail;
        } else {
            if (fail > 0) {
                JSONObject errObj = errorArr.getJSONObject(0);
                if (errObj != null) {
                    msg = errObj.getString(R3ParamConstants.MESSAGE);
                }
            } else {
                msg = "审核成功!";
            }
        }
        return msg;
    }
}
