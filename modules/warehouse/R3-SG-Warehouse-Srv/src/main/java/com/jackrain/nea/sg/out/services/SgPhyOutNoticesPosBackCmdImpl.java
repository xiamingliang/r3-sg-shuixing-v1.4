package com.jackrain.nea.sg.out.services;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.Feature;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.basic.common.R3ParamConstants;
import com.jackrain.nea.sg.basic.model.result.SgR3BaseResult;
import com.jackrain.nea.sg.out.api.SgPhyOutNoticesPosBackCmd;
import com.jackrain.nea.sg.out.model.request.SgPhyOutNoticesPosRequest;
import com.jackrain.nea.sys.CommandAdapter;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.query.DefaultWebEvent;
import com.jackrain.nea.web.query.QuerySession;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 全渠道发货单退回服务
 *
 * @author 舒威
 * @since 2019/12/11
 * create at : 2019/12/11 14:31
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgPhyOutNoticesPosBackCmdImpl extends CommandAdapter implements SgPhyOutNoticesPosBackCmd {

    @Autowired
    private SgPhyOutNoticesPosBackService posBackService;

    @Override
    public ValueHolder execute(QuerySession session) throws NDSException {
        ValueHolder vh = new ValueHolder();
        vh.put(R3ParamConstants.CODE, ResultCode.FAIL);
        vh.put(R3ParamConstants.MESSAGE, "退回失败！");

        DefaultWebEvent event = session.getEvent();
        JSONObject param = JSON.parseObject(JSON.toJSONStringWithDateFormat(event.getParameterValue("param"),
                "yyyy-MM-dd HH:mm:ss", SerializerFeature.WriteMapNullValue), Feature.OrderedField);
        log.debug(this.getClass().getName() + ".debug, 入参：" + param);

        //是否列表发货，默认是
        boolean isList = true;
        JSONArray ids= new JSONArray();;
        if (CollectionUtils.isEmpty(param.getJSONArray("ids"))) {
            ids.add(param.getLong(R3ParamConstants.OBJID));
        }else {
            for (Object id : param.getJSONArray("ids")) {
                ids.add(Long.valueOf((String) id));
            }
        }

        SgPhyOutNoticesPosRequest request = new SgPhyOutNoticesPosRequest();
        request.setLoginUser(session.getUser());
        request.setIds(ids);

        ValueHolderV14<SgR3BaseResult> result = posBackService.backPosOutNotices(request);
        if (result != null) {
            vh.put(R3ParamConstants.CODE, result.getCode());
            String message = result.getMessage();
            SgR3BaseResult data = result.getData();
            if (!isList && data != null && CollectionUtils.isNotEmpty(data.getDataArr())) {
                message = data.getDataArr().getJSONObject(0).getString("message");
            }
            vh.put(R3ParamConstants.MESSAGE, message);
            if (isList && data != null) {
                vh.put(R3ParamConstants.DATA, data.getDataArr());
            }
        }
        return vh;
    }
}
