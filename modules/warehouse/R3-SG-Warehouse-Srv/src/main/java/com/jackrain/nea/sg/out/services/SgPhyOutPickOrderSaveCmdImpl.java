package com.jackrain.nea.sg.out.services;

import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.out.api.SgPhyOutPickOrderSaveCmd;
import com.jackrain.nea.sg.out.model.request.SgPhyOutPickOrderSaveRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author zhu lin yu
 * @since 2019-11-18
 * create at : 2019-11-18 19:04
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgPhyOutPickOrderSaveCmdImpl implements SgPhyOutPickOrderSaveCmd {

    @Autowired
    private SgPhyOutPickOrderSaveService service;

    @Override
    public ValueHolderV14 saveSgBoutPickorder(SgPhyOutPickOrderSaveRequest request) {
        return service.saveSgbBoutPickorder(request);
    }

    @Override
    public ValueHolderV14 saveSgBoutPickorderItem(SgPhyOutPickOrderSaveRequest request) {
        return service.saveSgbBoutPickorderItem(request);
    }

    @Override
    public ValueHolderV14 saveSgBoutPickorderNoticeItem(SgPhyOutPickOrderSaveRequest request) {
        return service.saveSgbBoutPickorderNoticeItem(request);
    }

    @Override
    public ValueHolderV14 saveSgBoutPickorderResultItem(SgPhyOutPickOrderSaveRequest request) {
        return service.saveSgbBoutPickorderResultItem(request);
    }

    @Override
    public ValueHolderV14 saveSgBoutPickorderTeusItem(SgPhyOutPickOrderSaveRequest request) {
        ValueHolderV14 holderV14 = new ValueHolderV14();
        try {
            holderV14 = service.saveSgbBoutPickorderTeusItem(request);
        } catch (Exception e) {
            holderV14.setCode(ResultCode.FAIL);
            holderV14.setMessage(e.getMessage());
        }

        return holderV14;
    }
}
