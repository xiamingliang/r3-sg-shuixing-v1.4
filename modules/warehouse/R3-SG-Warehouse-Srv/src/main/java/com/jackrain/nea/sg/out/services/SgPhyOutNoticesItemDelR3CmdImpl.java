package com.jackrain.nea.sg.out.services;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.Feature;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.basic.common.R3ParamConstants;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.out.api.SgPhyOutNoticesItemDelR3Cmd;
import com.jackrain.nea.sg.out.model.request.SgPhyOutNoticesItemBillDelRequest;
import com.jackrain.nea.sys.CommandAdapter;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.query.DefaultWebEvent;
import com.jackrain.nea.web.query.QuerySession;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

/**
 * @author leexh
 * @since 2019/4/24 9:30
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgPhyOutNoticesItemDelR3CmdImpl extends CommandAdapter implements SgPhyOutNoticesItemDelR3Cmd {

    @Override
    public ValueHolder execute(QuerySession session) throws NDSException {

        ValueHolder vh = new ValueHolder();
        vh.put(R3ParamConstants.CODE, ResultCode.FAIL);
        vh.put(R3ParamConstants.MESSAGE, "删除失败！");

        DefaultWebEvent event = session.getEvent();
        JSONObject param = JSON.parseObject(JSON.toJSONStringWithDateFormat(event.getParameterValue("param"),
                "yyyy-MM-dd HH:mm:ss", SerializerFeature.WriteMapNullValue), Feature.OrderedField);
        log.debug(this.getClass().getName() + ".debug, 入参：" + param);

        SgPhyOutNoticesItemBillDelRequest request = new SgPhyOutNoticesItemBillDelRequest();
        request.setObjId(param.getLong(R3ParamConstants.OBJID));
        JSONObject data = JSON.parseObject(event.getData().get("DATA").toString());
        request.setIds(data.getJSONArray(SgConstants.SG_B_PHY_OUT_NOTICES_ITEM));

        SgPhyOutNoticesItemDelService service = ApplicationContextHandle.getBean(SgPhyOutNoticesItemDelService.class);
        ValueHolderV14 result = service.delSgPhyOutNoticesItem(request);
        if (result != null) {
            vh.put(R3ParamConstants.CODE, result.getCode());
            vh.put(R3ParamConstants.MESSAGE, result.getMessage());
            vh.put(R3ParamConstants.DATA, result.getData());
        }

        return vh;
    }
}
