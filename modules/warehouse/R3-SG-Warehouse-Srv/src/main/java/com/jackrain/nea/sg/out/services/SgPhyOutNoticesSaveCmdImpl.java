package com.jackrain.nea.sg.out.services;

import com.jackrain.nea.sg.basic.model.result.SgR3BaseResult;
import com.jackrain.nea.sg.out.api.SgPhyOutNoticesSaveCmd;
import com.jackrain.nea.sg.out.model.request.SgPhyOutNoticesBillSaveRequest;
import com.jackrain.nea.sg.out.model.request.SgPhyOutNoticesUpdateRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author leexh
 * @since 2019/4/22 11:33
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgPhyOutNoticesSaveCmdImpl implements SgPhyOutNoticesSaveCmd {

    @Override
    public ValueHolderV14<SgR3BaseResult> saveSgPhyOutNotices(SgPhyOutNoticesBillSaveRequest request) {

        SgPhyOutNoticesSaveService service = ApplicationContextHandle.getBean(SgPhyOutNoticesSaveService.class);
        return service.saveSgPhyOutNoticesThird(request);
    }

    @Override
    public ValueHolderV14 updateOutNoticesStatus(List<SgPhyOutNoticesUpdateRequest> requests, User user) {
        SgPhyOutNoticesSaveService service = ApplicationContextHandle.getBean(SgPhyOutNoticesSaveService.class);
        return service.updateOutNoticesStatus(requests,user);
    }
}
