package com.jackrain.nea.sg.in.services;

import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.in.api.SgPhyInPickOrderSaveCmd;
import com.jackrain.nea.sg.in.model.request.SgPhyInPickOrderSaveRequest;
import com.jackrain.nea.sg.out.api.SgPhyOutPickOrderSaveCmd;
import com.jackrain.nea.sg.out.model.request.SgPhyOutPickOrderSaveRequest;
import com.jackrain.nea.sg.out.services.SgPhyOutPickOrderSaveService;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author zhu lin yu
 * @since 2019-11-18
 * create at : 2019-11-18 19:04
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgPhyInPickOrderSaveCmdImpl implements SgPhyInPickOrderSaveCmd {

    @Autowired
    private SgPhyInPickOrderSaveService service;

    @Override
    public ValueHolderV14 saveSgbBInPickorder(SgPhyInPickOrderSaveRequest request) {
        return service.saveSgbBInPickorder(request);
    }

    @Override
    public ValueHolderV14 saveSgbBInPickorderItem(SgPhyInPickOrderSaveRequest request) {
        return service.saveSgbBInPickorderItem(request);
    }

    @Override
    public ValueHolderV14 saveSgbBInPickorderNoticeItem(SgPhyInPickOrderSaveRequest request) {
        return service.saveSgbBInPickorderNoticeItem(request);
    }

    @Override
    public ValueHolderV14 saveSgbBInPickorderResultItem(SgPhyInPickOrderSaveRequest request) {
        return service.saveSgbBInPickorderResultItem(request);
    }

    @Override
    public ValueHolderV14 saveSgbBInPickorderTeusItem(SgPhyInPickOrderSaveRequest request) {

        ValueHolderV14 holderV14 = new ValueHolderV14();
        try {
            holderV14 = service.saveSgbBInPickorderTeusItem(request);
        } catch (Exception e) {
            holderV14.setCode(ResultCode.FAIL);
            holderV14.setMessage(e.getMessage());
        }

        return holderV14;
    }
}
