package com.jackrain.nea.sg.inv.services;

import com.github.pagehelper.PageInfo;
import com.jackrain.nea.sg.inv.api.ScInvProfitLossMarginQueryCmd;
import com.jackrain.nea.sg.inv.model.request.ScInvProfitLossMarginQueryCmdRequest;
import com.jackrain.nea.sg.inv.model.result.ScInvProfitLossQueryItemResult;
import com.jackrain.nea.sg.inv.model.result.ScInvProfitLossMarginQueryResult;
import com.jackrain.nea.sg.inv.model.result.ScInvProfitLossQueryLastResult;
import com.jackrain.nea.sg.inv.model.table.ScBInventory;
import com.jackrain.nea.sg.inv.model.table.ScBPrePlUnfshBill;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

/**
 * @author 舒威
 * @since 2019/3/27
 * create at : 2019/3/27 16:55
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class ScInvProfitLossMarginQueryCmdImpl implements ScInvProfitLossMarginQueryCmd {

    @Override
    public ValueHolderV14<ScInvProfitLossMarginQueryResult> queryProfitLoss(ScInvProfitLossMarginQueryCmdRequest model) {
        ScInvProfitLossMarginQueryService marginQueryService = ApplicationContextHandle.getBean(
                ScInvProfitLossMarginQueryService.class);
        return marginQueryService.queryProfitLoss(model);
    }

    @Override
    public ValueHolderV14<ScInvProfitLossQueryLastResult> queryLastProfitLossInventory(ScInvProfitLossMarginQueryCmdRequest model) {
        ScInvProfitLossLastQueryService lastQueryService = ApplicationContextHandle.getBean(
                ScInvProfitLossLastQueryService.class);
        return lastQueryService.queryLastProfitLossInventory(model);
    }

    @Override
    public ValueHolderV14<PageInfo<ScBInventory>> queryProfitLossInventory(ScInvProfitLossMarginQueryCmdRequest model) {
        ScInvProfitLossQueryService profitLossQueryService = ApplicationContextHandle.getBean(
                ScInvProfitLossQueryService.class);
        return profitLossQueryService.queryProfitLossInventory(model);
    }

    @Override
    public ValueHolderV14<PageInfo<ScBPrePlUnfshBill>> queryUnfinishProfitLoss(ScInvProfitLossMarginQueryCmdRequest model) {
        ScInvProfitLossUnfinishQueryService unfinishQueryService = ApplicationContextHandle.getBean(
                ScInvProfitLossUnfinishQueryService.class);
        return unfinishQueryService.queryUnfinishProfitLoss(model);
    }

    @Override
    public ValueHolderV14<ScInvProfitLossQueryItemResult> queryProfitLossItem(ScInvProfitLossMarginQueryCmdRequest model) {
        ScInvProfitLossItemQueryService itemQueryService = ApplicationContextHandle.getBean(
                ScInvProfitLossItemQueryService.class);
        return itemQueryService.queryProfitLossItem(model);
    }
}
