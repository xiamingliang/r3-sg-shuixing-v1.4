package com.jackrain.nea.sg.in.services;

import com.jackrain.nea.sg.in.api.SgPhyInNoticesByConditionQueryCmd;
import com.jackrain.nea.sg.in.model.request.SgPhyInNoticesQueryRequest;
import com.jackrain.nea.sg.in.model.table.SgBPhyInNotices;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author 周琳胜
 * @since 2019-10-23
 * create at : 2019-10-23 19:51
 */

@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgPhyInNoticesByConditionQueryCmdImpl implements SgPhyInNoticesByConditionQueryCmd {

    @Override
    public ValueHolderV14<List<SgBPhyInNotices>> queryInNoticesItem(SgPhyInNoticesQueryRequest request) {
        SgPhyInNoticesByConditionQueryService service
                = ApplicationContextHandle.getBean(SgPhyInNoticesByConditionQueryService.class);

        return service.queryInNotices(request);

    }
}
