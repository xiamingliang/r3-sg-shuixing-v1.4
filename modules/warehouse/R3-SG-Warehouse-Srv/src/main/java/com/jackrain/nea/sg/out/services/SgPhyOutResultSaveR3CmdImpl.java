package com.jackrain.nea.sg.out.services;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.Feature;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.basic.model.result.SgR3BaseResult;
import com.jackrain.nea.sg.basic.utils.R3ParamUtil;
import com.jackrain.nea.sg.out.api.SgPhyOutResultSaveR3Cmd;
import com.jackrain.nea.sg.out.model.request.SgPhyOutResultBillSaveRequest;
import com.jackrain.nea.sys.CommandAdapter;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.query.DefaultWebEvent;
import com.jackrain.nea.web.query.QuerySession;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

/**
 * @author leexh
 * @since 2019/4/24 15:00
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgPhyOutResultSaveR3CmdImpl extends CommandAdapter implements SgPhyOutResultSaveR3Cmd {

    @Override
    public ValueHolder execute(QuerySession session) throws NDSException {

        ValueHolder vh = new ValueHolder();
        vh.put("code", ResultCode.FAIL);
        vh.put("message", "保存失败！");

        DefaultWebEvent event = session.getEvent();
        JSONObject param = JSON.parseObject(JSON.toJSONStringWithDateFormat(event.getParameterValue("param"),
                "yyyy-MM-dd HH:mm:ss", SerializerFeature.WriteMapNullValue), Feature.OrderedField);
        log.debug(this.getClass().getName() + ".debug, 入参：" + param);

        SgPhyOutResultBillSaveRequest request = R3ParamUtil.parseSaveObject(session, SgPhyOutResultBillSaveRequest.class);
        SgPhyOutResultSaveService service = ApplicationContextHandle.getBean(SgPhyOutResultSaveService.class);
        ValueHolderV14<SgR3BaseResult> v14 = service.saveSgPhyOutResultByNotices(request);
        if (v14 != null) {
            vh.put("code", v14.getCode());
            vh.put("message", v14.getMessage());
            if (v14.getData() != null) {
                vh.put("data", v14.getData().getDataJo());
            }
        }
        return vh;
    }
}
