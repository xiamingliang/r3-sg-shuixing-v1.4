package com.jackrain.nea.sg.phyadjust.services;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.phyadjust.api.SgPhyAdjSaveAndAuditCmd;
import com.jackrain.nea.sg.phyadjust.model.request.SgPhyAdjustBillSaveRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

/**
 * @author csy
 * Date: 2019/4/29
 * Description:
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgPhyAdjSaveAndAuditCmdImpl implements SgPhyAdjSaveAndAuditCmd {

    @Override
    public ValueHolderV14 saveAndAuditAdj(SgPhyAdjustBillSaveRequest request) throws NDSException {
        SgPhyAdjSaveAndAuditService service = ApplicationContextHandle.getBean(SgPhyAdjSaveAndAuditService.class);
        return service.saveAndAudit(request);
    }
}
