package com.jackrain.nea.sg.phyadjust.services;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.basic.model.result.SgR3BaseResult;
import com.jackrain.nea.sg.phyadjust.api.SgPhyAdjustSaveCmd;
import com.jackrain.nea.sg.phyadjust.model.request.SgPhyAdjustBillSaveRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

/**
 * @author csy
 * Date: 2019/4/29
 * Description:
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgPhyAdjustSaveCmdImpl implements SgPhyAdjustSaveCmd {

    @Override
    public ValueHolderV14<SgR3BaseResult> save(SgPhyAdjustBillSaveRequest request) throws NDSException {
        SgPhyAdjustSaveService service = ApplicationContextHandle.getBean(SgPhyAdjustSaveService.class);
        return service.save(request, false);
    }
}
