package com.jackrain.nea.sg.in.services;


import com.jackrain.nea.sg.in.api.SgPhyInNoticesSaveCmd;
import com.jackrain.nea.sg.in.model.request.SgPhyInNoticesBillSaveRequest;
import com.jackrain.nea.sg.in.model.request.SgPhyInNoticesBillUpdateRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Author: chenchen
 * @Description:新增入库通知单服务
 * @Date: Create in 16:27 2019/4/22
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgPhyInNoticesSaveCmdImpl implements SgPhyInNoticesSaveCmd {

    @Autowired
    private SgPhyInNoticesSaveService saveService;

    @Autowired
    private SgPhyInNoticesUpdateService updateService;

    @Override
    public ValueHolderV14 saveSgBPhyInNotices(List<SgPhyInNoticesBillSaveRequest> requestList) {
        return saveService.saveSgBPhyInNotices(requestList);
    }

    @Override
    public ValueHolderV14 updateSgBPhyInNoticesByPick(SgPhyInNoticesBillUpdateRequest request) {
        return updateService.updateSgBPhyInNoticesByPick(request);
    }
}
