package com.jackrain.nea.sg.out.services;

import com.jackrain.nea.sg.out.api.SgPhyOutQueryCmd;
import com.jackrain.nea.sg.out.model.request.SgPhyOutBillQueryRequest;
import com.jackrain.nea.sg.out.model.result.SgOutQueryResult;
import com.jackrain.nea.sg.out.model.result.SgOutResultSendMsgResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author leexh
 * @since 2019/5/30 18:01
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgPhyOutQueryCmdImpl implements SgPhyOutQueryCmd {
    @Override
    public ValueHolderV14<List<SgOutQueryResult>> queryOutBySource(SgPhyOutBillQueryRequest request) {
        SgPhyOutQueryService service = ApplicationContextHandle.getBean(SgPhyOutQueryService.class);
        return service.queryOutBySource(request);
    }

    @Override
    public ValueHolderV14<List<SgOutResultSendMsgResult>> querySgPhyOutResultMQBody(List<Long> ids, User user) {
        SgPhyOutResultMQBodyQueryService service = ApplicationContextHandle.getBean(SgPhyOutResultMQBodyQueryService.class);
        return service.querySgPhyOutResultMQBody(ids, user);
    }

    @Override
    public ValueHolderV14<List<SgOutResultSendMsgResult>> querySgPhyOutResultJITXMQBody(List<Long> sourceBillIds, User user) {
        SgPhyOutResultMQBodyQueryService service = ApplicationContextHandle.getBean(SgPhyOutResultMQBodyQueryService.class);
        return service.querySgPhyOutResultJITXMQBody(sourceBillIds, user);
    }
}
