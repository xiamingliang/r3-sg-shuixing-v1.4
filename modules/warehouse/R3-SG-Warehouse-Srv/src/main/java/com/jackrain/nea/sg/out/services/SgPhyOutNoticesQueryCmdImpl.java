package com.jackrain.nea.sg.out.services;

import com.jackrain.nea.sg.out.api.SgPhyOutNoticesQueryCmd;
import com.jackrain.nea.sg.out.model.request.SgPhyOutNoticesQueryRequest;
import com.jackrain.nea.sg.out.model.request.SgPhyOutNoticesSaveRequest;
import com.jackrain.nea.sg.out.model.result.SgBPhyOutNoticesAllInfoResult;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutNotices;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author leexh
 * @since 2019/5/15 15:09
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgPhyOutNoticesQueryCmdImpl implements SgPhyOutNoticesQueryCmd {
    @Override
    public ValueHolderV14<List<SgBPhyOutNotices>> queryOutNoticesBySource(List<SgPhyOutNoticesSaveRequest> requests) {
        SgPhyOutNoticesSaveService service = ApplicationContextHandle.getBean(SgPhyOutNoticesSaveService.class);
        return service.queryOutNoticesListBySource(requests);
    }

    @Override
    public ValueHolderV14<List<SgBPhyOutNoticesAllInfoResult>> queryOutNoticesAllInfoBySource(SgPhyOutNoticesQueryRequest request) {
        SgPhyOutQueryService service = ApplicationContextHandle.getBean(SgPhyOutQueryService.class);
        return service.queryOutNoticesAllInfoBySource(request);
    }
}
