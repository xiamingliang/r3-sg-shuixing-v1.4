package com.jackrain.nea.sg.in.services;


import com.jackrain.nea.sg.in.api.SgPhyInNoticesPassCmd;
import com.jackrain.nea.sg.in.model.request.SgPhyInNoticesBillPassRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Author: ChenChen
 * @Description:
 * @Date: Create in 19:06 2019/4/24
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgPhyInNoticesPassCmdImpl implements SgPhyInNoticesPassCmd {
    @Autowired
    SgPhyInNoticesPassService sgPhyInNoticesPassService;

    @Override
    public ValueHolderV14 passSgBPhyInNotices(SgPhyInNoticesBillPassRequest request) {
        return sgPhyInNoticesPassService.updateInPass(request);
    }
}
