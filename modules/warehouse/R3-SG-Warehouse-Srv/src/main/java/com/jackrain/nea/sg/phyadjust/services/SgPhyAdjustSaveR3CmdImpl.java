package com.jackrain.nea.sg.phyadjust.services;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.phyadjust.api.SgPhyAdjustSaveR3Cmd;
import com.jackrain.nea.sys.CommandAdapter;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.query.QuerySession;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

/**
 * @author csy
 * Date: 2019/4/29
 * Description:
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgPhyAdjustSaveR3CmdImpl extends CommandAdapter implements SgPhyAdjustSaveR3Cmd {


    @Override
    public ValueHolder execute(QuerySession session) throws NDSException {
        SgPhyAdjustSaveService service = ApplicationContextHandle.getBean(SgPhyAdjustSaveService.class);
        return service.save(session);
    }
}
