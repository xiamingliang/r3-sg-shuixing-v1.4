package com.jackrain.nea.sg.unpack.service;

import com.jackrain.nea.sg.unpack.api.SgUnpackQueryForWmsCmd;
import com.jackrain.nea.sg.unpack.model.request.SgUnpackForWmsRequest;
import com.jackrain.nea.sg.unpack.model.result.SgUnpackForWmsResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author: 周琳胜
 * @since: 2019/12/4
 * create at : 2019/12/4 11:20
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgUnpackQueryForWmsCmdImpl implements SgUnpackQueryForWmsCmd {
    @Override
    public ValueHolderV14<List<SgUnpackForWmsResult>> querySgUnpackForWms(SgUnpackForWmsRequest request) {
        SgUnpackQueryForWmsService service = ApplicationContextHandle.getBean(SgUnpackQueryForWmsService.class);
        return service.querySgUnpack(request);
    }
}
