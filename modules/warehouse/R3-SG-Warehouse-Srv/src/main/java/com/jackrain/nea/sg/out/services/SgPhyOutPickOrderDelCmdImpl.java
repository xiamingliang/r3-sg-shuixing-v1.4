package com.jackrain.nea.sg.out.services;

import com.jackrain.nea.sg.out.api.SgPhyOutPickOrderDelCmd;
import com.jackrain.nea.sg.out.model.request.SgPhyBOutPickOrderDelRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author zhu lin yu
 * @since 2019-11-19
 * create at : 2019-11-19 16:44
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgPhyOutPickOrderDelCmdImpl implements SgPhyOutPickOrderDelCmd {

    @Autowired
    private SgPhyOutPickOrderDelService sgPhyOutPickorderDelService;

    @Override
    public ValueHolderV14 delOutPickorderItemNum(SgPhyBOutPickOrderDelRequest request) {
        return sgPhyOutPickorderDelService.delOutPickorderItemNum(request);
    }

    @Override
    public ValueHolderV14 clearOutPickorderItemNum(SgPhyBOutPickOrderDelRequest request) {
        return sgPhyOutPickorderDelService.clearOutPickorderItemNum(request);
    }
}
