package com.jackrain.nea.sg.in.services;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.in.api.SgPhyInResultSelectCmd;
import com.jackrain.nea.sg.in.model.request.SgBPhyInResultSaveRequest;
import com.jackrain.nea.sg.in.model.request.SgPhyInResultBillSaveRequest;
import com.jackrain.nea.sg.in.model.table.SgBPhyInResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * 根据来源单据id和来源单据类型获取入库结果单
 *
 * @author jiang.cj
 * @since 2019-05-20
 * create at : 2019-05-20
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgPhyInResultSelectCmdImpl implements SgPhyInResultSelectCmd {


    @Autowired
    private SgPhyInResultSelectService SgPhyInResultSelectService;

    @Override
    public ValueHolderV14<List<SgBPhyInResult>> selectSgPhyInResult(SgBPhyInResultSaveRequest request) throws NDSException {
        return SgPhyInResultSelectService.selectSgPhyInResult(request);
    }

    @Override
    public ValueHolderV14<List<SgPhyInResultBillSaveRequest>> querySgPhyInResultMQBody(List<Long> ids, User user) {
        SgPhyInResultMQBodyQueryService service = ApplicationContextHandle.getBean(SgPhyInResultMQBodyQueryService.class);
        return service.querySgPhyInResultMQBody(ids,user);
    }
}
