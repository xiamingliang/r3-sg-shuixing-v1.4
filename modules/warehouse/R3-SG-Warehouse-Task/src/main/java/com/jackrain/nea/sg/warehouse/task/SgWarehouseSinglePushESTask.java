package com.jackrain.nea.sg.warehouse.task;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.sg.task.SgWarehouseSinglePushESTaskService;
import com.jackrain.nea.task.IR3Task;
import com.jackrain.nea.task.RunTaskResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author 舒威
 * @since 2019/8/2
 * create at : 2019/8/2 16:12
 */
@Slf4j
@Component
public class SgWarehouseSinglePushESTask implements IR3Task {

    @Autowired
    private SgWarehouseSinglePushESTaskService singlePushESTaskService;

    @Override
    public RunTaskResult execute(JSONObject params) {
        log.info("Start SgWarehouseSinglePushESTask.execute. ReceiveParams:params:{};",
                JSONObject.toJSONString(params));

        RunTaskResult runTaskResult = new RunTaskResult();
        runTaskResult.setSuccess(Boolean.TRUE);
        singlePushESTaskService.pushSingleWarehouseES(params);
        return runTaskResult;
    }
}
