package com.jackrain.nea.sg.warehouse.task;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.sg.task.SgPhyOutNoticesBatchFaceTaskService;
import com.jackrain.nea.task.IR3Task;
import com.jackrain.nea.task.RunTaskResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @description:
 * @author: 郑小龙
 * @date: 2019-08-22 20:11
 */
@Slf4j
@Component
public class SgPhyOutNoticesBatchFaceTask implements IR3Task {

    @Autowired
    private SgPhyOutNoticesBatchFaceTaskService service;

    @Override
    public RunTaskResult execute(JSONObject params) {

        RunTaskResult runTaskResult = new RunTaskResult();
        runTaskResult.setSuccess(Boolean.TRUE);
        service.faceTimedTask();
        return runTaskResult;
    }
}
