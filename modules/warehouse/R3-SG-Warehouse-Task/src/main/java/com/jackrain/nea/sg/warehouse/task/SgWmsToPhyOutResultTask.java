package com.jackrain.nea.sg.warehouse.task;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.out.services.SgWmsToPhyOutResultService;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.task.IR3Task;
import com.jackrain.nea.task.RunTaskResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author: 舒威
 * @since: 2019/10/15
 * create at : 2019/10/15 21:11
 */
@Slf4j
@Component
public class SgWmsToPhyOutResultTask implements IR3Task {

    @Autowired
    private SgWmsToPhyOutResultService wmsToPhyOutResultService;

    @Override
    public RunTaskResult execute(JSONObject params) {
        log.info("Start SgWmsToPhyOutResultTask.execute. ReceiveParams:params:{};",
                JSONObject.toJSONString(params));
        RunTaskResult runTaskResult = new RunTaskResult();
        runTaskResult.setSuccess(Boolean.TRUE);
        runTaskResult.setMessage("success");
        try {
            ValueHolderV14 v14 = wmsToPhyOutResultService.wmsToPhyOutResult(params);
            if (!v14.isOK()) {
                runTaskResult.setSuccess(Boolean.FALSE);
                runTaskResult.setMessage(v14.getMessage());
            }
        } catch (Exception e) {
            String messageExtra = AssertUtils.getMessageExtra("回传结果单定时任务执行异常!", e);
            log.error(messageExtra);
            runTaskResult.setSuccess(Boolean.FALSE);
            runTaskResult.setMessage(messageExtra);
        }
        return runTaskResult;
    }
}
