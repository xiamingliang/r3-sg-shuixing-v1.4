package com.jackrain.nea.sg.phyadjust.model.result;

import com.alibaba.fastjson.JSONArray;
import com.jackrain.nea.sg.phyadjust.model.table.SgBPhyAdjustImpItem;
import com.jackrain.nea.sg.phyadjust.model.table.SgBPhyAdjustItem;
import lombok.Data;

import java.util.List;

/**
 * @author csy
 * Date: 2019/6/13
 * Description:
 */
@Data
public class FillSkuResult {

    private List<SgBPhyAdjustItem> itemList;

    private List<SgBPhyAdjustImpItem> impItems;

    private JSONArray errArr;
}
