package com.jackrain.nea.sg.out.model.result;

import com.jackrain.nea.sg.out.model.request.SgPhyOutNoticesBillSaveRequest;
import com.jackrain.nea.web.face.User;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author: 舒威
 * @since: 2019/8/5
 * create at : 2019/8/5 15:45
 */
@Data
public class SgOutNoticesAndWMSMqResult implements Serializable {

    private List<SgPhyOutNoticesBillSaveRequest> noticesBills;

    private User loginUser;
}
