package com.jackrain.nea.sg.in.model.request;



import com.jackrain.nea.sys.domain.BaseModel;
import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.Date;


@Data
@ToString
public class SgBPhyInResultSaveRequest extends BaseModel {

    private Long id;

    private Long cpCPhyWarehouseId;

    private String cpCPhyWarehouseEcode;

    private String cpCPhyWarehouseEname;

    private String billNo;

    private Long sgBPhyInNoticesId;

    private String goodsOwner;

    private Date billDate;

    private Integer billStatus;

    private Date inTime;

    private Long inId;

    private String inEcode;

    private String inEname;

    private String inName;

    private Integer inType;


    private Long cpCCustomerWarehouseId;

    private String cpCCsEcode;

    private String cpCCsEname;

    private Long cpCSupplierId;

    private Integer sourceBillType;

    private Long sourceBillId;

    private String sourceBillNo;

    private String sendName;

    private String sendMobile;


    private String sendPhone;

    private Long cpCRegionProvinceId;

    private String cpCRegionProvinceEcode;

    private String cpCRegionProvinceEname;

    private Long cpCRegionCityId;

    private String cpCRegionCityEcode;

    private String cpCRegionCityEname;

    private Long cpCRegionAreaId;

    private String cpCRegionAreaEcode;

    private String cpCRegionAreaEname;

    private String sendAddress;

    private String sendZip;

    private Long cpCLogisticsId;

    private String cpCLogisticsEcode;

    private String cpCLogisticsEname;

    private String logisticNumber;

    private Long cpCShopId;

    private String cpCShopTitle;

    private String sourcecode;

    private String sellerRemark;

    private String buyerRemark;

    private BigDecimal totQtyIn;

    private BigDecimal totAmtListIn;

    private String wmsBillNo;

    private String remark;

    private String property1;

    private String property2;

    private String property3;

    private String property4;

    private String property5;

    private String property6;

    private String property7;

    private String property8;

    private String property9;

    private String property10;

    private String ownerename;

    private String modifierename;

    private String sgBPhyInNoticesBillno;

    private Integer isLast;
    /**
     * 是否真的是最后一次入库(销售退需要)
     */
    private Boolean isAll;
}