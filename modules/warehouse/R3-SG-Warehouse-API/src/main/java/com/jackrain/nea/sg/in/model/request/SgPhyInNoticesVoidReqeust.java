package com.jackrain.nea.sg.in.model.request;

import com.jackrain.nea.web.face.User;
import lombok.Data;

import java.io.Serializable;

/**
 * @Author: ChenChen
 * @Description:
 * @Date: Create in 15:04 2019/5/5
 */
@Data
public class SgPhyInNoticesVoidReqeust implements Serializable {

    private static final long serialVersionUID = 3168016001552706664L;


    /**
     * 通知单id
     */
    private Long id;

    /**
     * 来源单据类型
     */
    private Integer sourceBillType;

    /**
     * 来源单据id
     */
    private Long sourceBillId;

    private User loginUser;
}
