package com.jackrain.nea.sg.in.api;

import com.jackrain.nea.sg.in.model.request.SgPhyInCommitForPDARequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 * @author zhoulinsheng
 * @since 2019-10-29
 * create at : 2019-10-29 14:11
 */
public interface SgPhyInCommitForPDACmd {
    ValueHolderV14 saveAndAuditInResult(SgPhyInCommitForPDARequest request);
}
