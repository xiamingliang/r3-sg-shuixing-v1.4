package com.jackrain.nea.sg.out.model.request;

import com.jackrain.nea.sg.basic.model.request.SgR3BaseRequest;
import lombok.Data;

import java.io.Serializable;

/**
 * @author leexh
 * @since 2019/5/30 17:46
 */
@Data
public class SgPhyOutBillBaseRequest extends SgR3BaseRequest implements Serializable {

    /**
     * 来源单据ID
     */
    private Long sourceBillId;

    /**
     * 来源单据编号
     */
    private String sourceBillNo;

    /**
     * 来源单据类型
     */
    private Integer sourceBillType;
}
