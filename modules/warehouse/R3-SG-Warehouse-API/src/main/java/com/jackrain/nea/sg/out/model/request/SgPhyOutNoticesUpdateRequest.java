package com.jackrain.nea.sg.out.model.request;

import lombok.Data;

import java.io.Serializable;

/**
 * @author zhu lin yu
 * @since 2019-11-13
 * create at : 2019-11-13 19:20
 */
@Data
public class SgPhyOutNoticesUpdateRequest implements Serializable {

    private Long id;

    private String billNo;

    private Integer outType;

    private Integer sourceBillType;

    private Integer billStatus;

    private Long sourceBillId;

    private String sourceBillNo;

    private Long wmsStatus;

    private String wmsFailReason;

    private Long wmsFailCount;

    private String wmsBillNo;

    private Integer pickStatus;
}
