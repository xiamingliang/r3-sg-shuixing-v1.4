package com.jackrain.nea.sg.out.model.request;

import lombok.Data;

import java.io.Serializable;

/**
 * @description: 工作台查询出库通知入参实体
 * @author: 郑小龙
 * @date: 2019-05-15 15:46
 */
@Data
public class WorkbenchOutNoticesPageQueryRequest implements Serializable {
    private WorkbenchOutNoticesQueryRequest queryRequest;
    private WorkbenchOutNoticesPageRequest pageRequest;
}
