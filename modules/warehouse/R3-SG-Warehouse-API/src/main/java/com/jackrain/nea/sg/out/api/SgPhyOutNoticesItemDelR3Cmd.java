package com.jackrain.nea.sg.out.api;

import com.jackrain.nea.sys.Command;

/**
 * @author leexh
 * @since 2019/4/24 9:29
 */
public interface SgPhyOutNoticesItemDelR3Cmd extends Command {
}
