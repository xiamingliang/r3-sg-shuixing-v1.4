package com.jackrain.nea.sg.out.model.request;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author leexh
 * @since 2019/4/23 10:38
 */
@Data
public class SgPhyOutNoticesSaveRequest implements Serializable {

    private Long id;

    private Long cpCPhyWarehouseId;

    private String cpCPhyWarehouseEcode;

    private String cpCPhyWarehouseEname;

    private String billNo;

    private String goodsOwner;

    private Date billDate;

    private Date payTime;

    private Date outTime;

    private Date preTime;

    private Long cpCCustomerWarehouseId;

    private String cpCCsEcode;

    private String cpCCsEname;

    private Long cpCSupplierId;

    private Date orderTime;

    private Integer outType;

    private Integer sourceBillType;

    private Integer billStatus;

    private Long sourceBillId;

    private String sourceBillNo;

    private String receiverName;

    private String receiverMobile;

    private String receiverPhone;

    private Long cpCRegionProvinceId;

    private String cpCRegionProvinceEcode;

    private String cpCRegionProvinceEname;

    private Long cpCRegionCityId;

    private String cpCRegionCityEcode;

    private String cpCRegionCityEname;

    private Long cpCRegionAreaId;

    private String cpCRegionAreaEcode;

    private String cpCRegionAreaEname;

    private String receiverAddress;

    private String receiverZip;

    private Long cpCLogisticsId;

    private String cpCLogisticsEcode;

    private String cpCLogisticsEname;

    private String logisticNumber;

    private String deliveryMethod;

    private BigDecimal amtPayment;

    private Integer isUrgent;

    private Long cpCShopId;

    private String cpCShopTitle;

    private String sourcecode;

    private String sellerRemark;

    private String buyerRemark;

    private BigDecimal totQty;

    private BigDecimal totQtyOut;

    private BigDecimal totQtyDiff;

    private BigDecimal totAmtList;

    private BigDecimal totAmtCost;

    private BigDecimal totAmtListOut;

    private BigDecimal totAmtCostOut;

    private BigDecimal totAmtListDiff;

    private BigDecimal totAmtCostDiff;

    private Long wmsStatus;

    private String wmsFailReason;

    private Long wmsFailCount;

    private String remark;

    private String ownerename;

    private String modifierename;

    private Integer isPassWms;

    private String outBillNo;

    private Date dateinExpect;

    private Long psCBrandId;

    private String psCBrandEcode;

    private String psCBrandEname;

    private Integer isCod;

    private String shortAddress;

    private String cpCCustomerEcode;

    private Long reserveBigint01;

    private String poNo;

    private String pickNo;

    private BigDecimal amtService;

    private String wmsBillNo;

    private Integer isAirEmbargo;

    private Long sgBOutPropId;

    private String sgBOutPropEname;

    private Integer isDiffDeal;

    /**
     *订单类型
     */
    private Long reserveBigint02;

    private Integer jdExpressCancelStatus;

    private String jdExpressCancelInfo;
}
