package com.jackrain.nea.sg.out.model.request;

import lombok.Data;

import java.io.Serializable;

/**
 * @description: 工作台查询出库通知单入参实体-分页实体类
 * @author: 郑小龙
 * @date: 2019-05-15 15:49
 */
@Data
public class WorkbenchOutNoticesPageRequest implements Serializable {

    private int pageNum;

    private int pageSize;

    private String sortKeys;

    public WorkbenchOutNoticesPageRequest(){
        this.pageNum = 1;
        this.pageSize = 10;
    }
}
