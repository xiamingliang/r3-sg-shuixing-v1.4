package com.jackrain.nea.sg.out.model.result;

import com.jackrain.nea.sg.out.model.table.SgBPhyOutResult;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutResultImpItem;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutResultItem;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutResultTeusItem;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author 舒威
 * @since 2019/12/13
 * create at : 2019/12/13 13:43
 */
@Data
public class SgOutResultQueryResult implements Serializable {

    private SgBPhyOutResult outResult;

    private List<SgBPhyOutResultItem> outResultItems;

    private List<SgBPhyOutResultImpItem> outResultImpItems;

    private List<SgBPhyOutResultTeusItem> outResultTeusItems;
}
