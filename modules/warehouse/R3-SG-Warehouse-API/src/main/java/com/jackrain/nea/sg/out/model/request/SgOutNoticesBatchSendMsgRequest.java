package com.jackrain.nea.sg.out.model.request;

import com.jackrain.nea.sg.out.model.table.SgBPhyOutNotices;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutNoticesItem;
import com.jackrain.nea.web.face.User;
import lombok.Data;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

/**
 * @description: 出库通知单mq  批量
 * @author: 郑小龙
 * @date: 2019-08-22 11:11
 */
@Data
public class SgOutNoticesBatchSendMsgRequest implements Serializable {

    /**
     * 出库通知单信息
     */
    private HashMap<SgBPhyOutNotices, List<SgBPhyOutNoticesItem>> outNoticesMap;

    /**
     * 挂靠店铺信息
     */
    private HashMap<Long, Long> shopIdMap;

    /**
     * 电子面单类型信息
     */
    private HashMap<Long, Integer> etypeMap;

    /**
     * 用户信息
     */
    private User loginUser;
}
