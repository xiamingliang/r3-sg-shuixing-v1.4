package com.jackrain.nea.sg.out.model.request;

import com.jackrain.nea.web.face.User;
import lombok.Data;

import java.io.Serializable;

/**
 * @author 舒威
 * @since 2019/12/11
 * create at : 2019/12/11 15:44
 */
@Data
public class SgPhyOutNoticesCancelRequest implements Serializable {

    private Long sourceBillId;

    private Integer sourceBillType;

    private String remark;

    private Long serviceNode;

    private Boolean isCleanSend;

    private User user;

    public SgPhyOutNoticesCancelRequest() {
        this.isCleanSend = true;
    }
}
