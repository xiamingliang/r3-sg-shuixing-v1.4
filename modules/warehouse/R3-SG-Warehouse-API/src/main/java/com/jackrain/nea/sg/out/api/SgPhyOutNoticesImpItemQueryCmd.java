package com.jackrain.nea.sg.out.api;

import com.jackrain.nea.sg.out.model.request.SgPhyOutNoticesQueryRequest;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutNoticesImpItem;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutNoticesItem;
import com.jackrain.nea.sys.domain.ValueHolderV14;

import java.util.List;
import java.util.Map;

/**
 * @author zhu lin yu
 * @since 2019-10-23
 * create at : 2019-10-23 20:01
 */
public interface SgPhyOutNoticesImpItemQueryCmd {
    ValueHolderV14<List<SgBPhyOutNoticesImpItem>> queryOutNoticesItem(SgPhyOutNoticesQueryRequest request);

}
