package com.jackrain.nea.sg.out.model.result;

import com.jackrain.nea.sg.out.model.request.SgPhyOutDeliverySaveRequest;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutResultImpItem;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutResultTeusItem;
import com.jackrain.nea.web.face.User;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author leexh
 * @since 2019/5/10 15:49
 */
@Data
public class SgOutResultSendMsgResult implements Serializable {

    /**
     * 出库结果单
     */
    private SgOutResultMqResult mqResult;

    /**
     * 出库结果单明细
     */
    private List<SgOutResultItemMqResult> mqResultItem;

    /**
     * 多包裹明细：
     *  零售订单用
     */
    private List<SgPhyOutDeliverySaveRequest> deliveryItem;

    /**
     * 出库结果单录入明细
     */
    private List<SgBPhyOutResultImpItem> impItems;

    /**
     * 出库结果单箱明细
     */
    private List<SgBPhyOutResultTeusItem> teusItems;

    /**
     * 用户信息
     */
    private User loginUser;
}
