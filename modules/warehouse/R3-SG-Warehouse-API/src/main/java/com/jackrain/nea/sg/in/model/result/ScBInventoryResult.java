package com.jackrain.nea.sg.in.model.result;

import com.jackrain.nea.sg.inv.model.table.ScBInventory;
import com.jackrain.nea.sg.inv.model.table.ScBInventoryImpItem;
import lombok.Data;

import java.io.Serializable;
import java.util.List;


@Data
public class ScBInventoryResult implements Serializable {


    /**
     * 盘点主表
     */
    List<ScBInventory> inventoryList;

    /**
     * 盘点明细
     */
    List<ScBInventoryImpItem> inventoryImpItemList;
}