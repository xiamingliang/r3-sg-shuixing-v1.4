package com.jackrain.nea.sg.out.api;

import com.jackrain.nea.sys.Command;

/**
 * 全渠道发货单退回服务
 *
 * @author 舒威
 * @since 2019/12/11
 * create at : 2019/12/11 14:27
 */
public interface SgPhyOutNoticesPosBackCmd extends Command {
}
