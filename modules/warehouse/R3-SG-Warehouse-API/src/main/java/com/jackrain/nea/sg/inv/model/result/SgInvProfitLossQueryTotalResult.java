package com.jackrain.nea.sg.inv.model.result;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author 舒威
 * @since 2019/4/11
 * create at : 2019/4/11 16:29
 */
@Data
public class SgInvProfitLossQueryTotalResult implements Serializable {

    private BigDecimal qtty_loss; //盘盈

    private BigDecimal qtty_profit; //盘亏
}
