package com.jackrain.nea.sg.out.model.request;

import com.jackrain.nea.web.face.User;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author zhu lin yu
 * @since 2019-11-19
 * create at : 2019-11-19 16:47
 */
@Data
public class SgPhyBOutPickOrderDelRequest implements Serializable {
    private Boolean isClear;

    private List<SgPhyBOutPickOrderDelItemRequest> delItemRequest;

    private Long id;

    private User loginUser;
}
