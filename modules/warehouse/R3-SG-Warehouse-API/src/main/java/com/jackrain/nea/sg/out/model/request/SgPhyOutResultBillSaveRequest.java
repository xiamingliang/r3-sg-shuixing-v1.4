package com.jackrain.nea.sg.out.model.request;

import com.alibaba.fastjson.annotation.JSONField;
import com.jackrain.nea.sg.basic.model.request.SgR3BaseRequest;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author leexh
 * @since 2019/4/23 11:42
 */
@Data
public class SgPhyOutResultBillSaveRequest extends SgR3BaseRequest implements Serializable {

    /**
     * 出库结果单
     */
    @JSONField(name = "SG_B_PHY_OUT_RESULT")
    private SgPhyOutResultSaveRequest outResultRequest;

    /**
     * 出库结果单明细集合
     */
    @JSONField(name = "SG_B_PHY_OUT_RESULT_ITEM")
    private List<SgPhyOutResultItemSaveRequest> outResultItemRequests;

    /**
     * 多包裹明细集合
     */
    @JSONField(name = "SG_B_OUT_DELIVERY")
    private List<SgPhyOutDeliverySaveRequest> outDeliverySaveRequests;

    /**
     * 是否一键出库
     */
    private Boolean isOneClickOutLibrary;

    /**
     * 录入明细信息
     */
    @JSONField(name = "SG_B_PHY_OUT_RESULT_IMP_ITEM")
    private List<SgPhyOutResultImpItemSaveRequest> impItemList;

    /**
     * 箱内明细信息
     */
    @JSONField(name = "SG_B_PHY_OUT_RESULT_TEUS_ITEM")
    private List<SgPhyOutResultTeusItemSaveRequest> teusItemList;

    /**
     * 录入明细是否从reids获取(默认fasle)
     */
    @JSONField(name = "REDIS_KEY")
    private String redisKey;

    /**
     * 是否是wms回传
     */
    @JSONField(name = "WMS_BACK")
    private Boolean wmsBack;

    /**
     * 是否由拣货单生成的
     */
    @JSONField(name = "PACK_ORDER_ID")
    private Long packOrderId;

    /**
     * 是否生成多包裹明細
     */
    private Boolean isCreateDelivers;

    public SgPhyOutResultBillSaveRequest() {
        this.wmsBack = false;
        this.isCreateDelivers = false;
    }

}
