package com.jackrain.nea.sg.out.model.request;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author zhu lin yu
 * @since 2019-10-31
 * create at : 2019-10-31 14:56
 */
@Data
public class SgPhyOutCommitForPDADetailRequest implements Serializable {

    private String skuEcode;

    private BigDecimal qty;

    private Integer isTeus;

    private Long psCTeusId;

}
