package com.jackrain.nea.sg.inv.model.table;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.*;
import com.jackrain.nea.es.annotations.Document;
import com.jackrain.nea.es.annotations.Field;
import com.jackrain.nea.es.constans.FieldType;
import com.jackrain.nea.sys.domain.BaseModelES;
import lombok.Data;

import java.math.BigDecimal;

@TableName(value = "sc_b_inventory_item")
@Data
@Document(index = "sc_b_inventory",type = "sc_b_inventory_item")
public class SgBInventoryItem extends BaseModelES {
    @JSONField(name = "ID")
    @TableField(fill = FieldFill.INSERT)
    @TableId(value = "id", type = IdType.INPUT)
    @Field(type = FieldType.Long)
    private Long id;

    @JSONField(name = "SC_B_INVENTORY_ID")
    @Field(type = FieldType.Long)
    private Long scBInventoryId;

    @JSONField(name = "PIC_PATH")
    @Field(type = FieldType.Keyword)
    private String picPath;

    @JSONField(name = "PS_C_PRO_ID")
    @Field(type = FieldType.Long)
    private Long psCProId;

    @JSONField(name = "PS_C_PRO_ECODE")
    @Field(type = FieldType.Keyword)
    private String psCProEcode;

    @JSONField(name = "PS_C_PRO_ENAME")
    @Field(type = FieldType.Keyword)
    private String psCProEname;

    @JSONField(name = "PS_C_SKU_ID")
    @Field(type = FieldType.Long)
    private Long psCSkuId;

    @JSONField(name = "PS_C_SKU_ECODE")
    @Field(type = FieldType.Keyword)
    private String psCSkuEcode;

    @JSONField(name = "PS_C_CLR_ID")
    @Field(type = FieldType.Long)
    private Long psCClrId;

    @JSONField(name = "PS_C_CLR_ECODE")
    @Field(type = FieldType.Keyword)
    private String psCClrEcode;

    @JSONField(name = "PS_C_CLR_ENAME")
    @Field(type = FieldType.Keyword)
    private String psCClrEname;

    @JSONField(name = "PS_C_SIZE_ID")
    @Field(type = FieldType.Long)
    private Long psCSizeId;

    @JSONField(name = "PS_C_SIZE_ECODE")
    @Field(type = FieldType.Keyword)
    private String psCSizeEcode;

    @JSONField(name = "PS_C_SIZE_ENAME")
    @Field(type = FieldType.Keyword)
    private String psCSizeEname;

    @JSONField(name = "QTY")
    @Field(type = FieldType.Double)
    private BigDecimal qty;

    @JSONField(name = "PRICE_LIST")
    @Field(type = FieldType.Double)
    private BigDecimal priceList;

    @JSONField(name = "AMT_LIST")
    @Field(type = FieldType.Double)
    private BigDecimal amtList;

    @JSONField(name = "VERSION")
    @Field(type = FieldType.Long)
    private Long version;

    @JSONField(name = "OWNERENAME")
    @Field(type = FieldType.Keyword)
    private String ownerename;

    @JSONField(name = "MODIFIERENAME")
    @Field(type = FieldType.Keyword)
    private String modifierename;
}