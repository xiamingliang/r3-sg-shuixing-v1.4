package com.jackrain.nea.sg.pack.api;

import com.jackrain.nea.sg.pack.model.request.SgPackSaveAndAuditRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 * @author: 周琳胜
 * @since: 2019/11/26
 * create at : 2019/11/26 21:28
 */
public interface SgPackSaveAndAuditCmd {

    ValueHolderV14 saveAndAuditSgPack(SgPackSaveAndAuditRequest request);
}
