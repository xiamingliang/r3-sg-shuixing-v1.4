package com.jackrain.nea.sg.out.api;

import com.jackrain.nea.sg.out.model.request.SgPhyBOutPickOrderDelRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 * @author zhu lin yu
 * @since 2019-11-19
 * create at : 2019-11-19 16:40
 */
public interface SgPhyOutPickOrderDelCmd {

    /**
     * 出库拣货单页面删除功能
     */
    ValueHolderV14 delOutPickorderItemNum(SgPhyBOutPickOrderDelRequest request);

    /**
     * 出库拣货单页面清空功能
     */
    ValueHolderV14 clearOutPickorderItemNum(SgPhyBOutPickOrderDelRequest request);
}
