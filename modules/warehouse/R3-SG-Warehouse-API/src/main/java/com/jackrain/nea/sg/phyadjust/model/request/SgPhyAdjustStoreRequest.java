package com.jackrain.nea.sg.phyadjust.model.request;

import com.alibaba.fastjson.annotation.JSONField;
import com.jackrain.nea.es.annotations.Field;
import com.jackrain.nea.es.constans.FieldType;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author 舒威
 * @since 2019/12/7
 * create at : 2019/12/7 17:32
 */
@Data
public class SgPhyAdjustStoreRequest implements Serializable {

    @JSONField(name = "CP_C_STORE_ID")
    private Long cpCStoreId;

    @JSONField(name = "CP_C_STORE_ECODE")
    private String cpCStoreEcode;

    @JSONField(name = "CP_C_STORE_ENAME")
    private String cpCStoreEname;

    @JSONField(name = "ITEM_STORE_INFO")
    private List<SgPhyAdjustItemStoreRequest> itemStoreList;
}
