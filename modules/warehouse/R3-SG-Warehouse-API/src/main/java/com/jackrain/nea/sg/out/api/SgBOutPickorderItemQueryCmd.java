package com.jackrain.nea.sg.out.api;

import com.jackrain.nea.sg.out.model.request.SgBOutPickorderRequest;
import com.jackrain.nea.sg.out.model.result.SgBOutPickorderResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 * @author: heliu
 * @since: 2019/11/18
 * create at : 2019/11/18 11:45
 */
public interface SgBOutPickorderItemQueryCmd {
    ValueHolderV14<SgBOutPickorderResult> querySgBOutPickorderItem(SgBOutPickorderRequest request);
}
