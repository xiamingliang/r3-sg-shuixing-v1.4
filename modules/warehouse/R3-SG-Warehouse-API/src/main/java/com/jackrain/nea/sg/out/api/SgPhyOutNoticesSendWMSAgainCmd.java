package com.jackrain.nea.sg.out.api;

import com.jackrain.nea.sys.Command;

/**
 * @author leexh
 * @since 2019/6/24 15:44
 * desc: 重传WMS服务
 */
public interface SgPhyOutNoticesSendWMSAgainCmd extends Command {
}
