package com.jackrain.nea.sg.out.api;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.basic.model.result.SgR3BaseResult;
import com.jackrain.nea.sg.out.model.request.SgPhyOutResultBillSaveRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 * @author leexh
 * @since 2019/4/24 14:56
 */
public interface SgPhyOutResultSaveCmd {

    ValueHolderV14<SgR3BaseResult> saveSgPhyOutResult(SgPhyOutResultBillSaveRequest request) throws NDSException;
}
