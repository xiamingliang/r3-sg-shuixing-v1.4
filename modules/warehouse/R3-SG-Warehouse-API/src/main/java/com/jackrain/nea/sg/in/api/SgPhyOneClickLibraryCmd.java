package com.jackrain.nea.sg.in.api;

import com.jackrain.nea.sg.in.model.request.SgPhyOneClickLibraryRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 * 一键入库
 *
 * @author: 舒威
 * @since: 2019/9/9
 * create at : 2019/9/9 9:47
 */
public interface SgPhyOneClickLibraryCmd {

    ValueHolderV14 oneClickLibrary(SgPhyOneClickLibraryRequest request);
}
