package com.jackrain.nea.sg.out.model.table;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.*;
import com.jackrain.nea.es.annotations.Document;
import com.jackrain.nea.es.annotations.Field;
import com.jackrain.nea.es.constans.FieldType;
import com.jackrain.nea.sys.domain.BaseModelES;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@TableName(value = "sg_b_phy_out_result")
@Data
@Document(index = "sg_b_phy_out_result",type = "sg_b_phy_out_result")
@ApiModel
public class SgBPhyOutResult extends BaseModelES {
    @JSONField(name = "ID")
    @TableField(fill = FieldFill.INSERT)
    @TableId(value = "id", type = IdType.INPUT)
    @Field(type = FieldType.Long)
    @ApiModelProperty(name = "ID")
    private Long id;

    @JSONField(name = "CP_C_PHY_WAREHOUSE_ID")
    @Field(type = FieldType.Long)
    @ApiModelProperty(name = "CP_C_PHY_WAREHOUSE_ID")
    private Long cpCPhyWarehouseId;

    @JSONField(name = "CP_C_PHY_WAREHOUSE_ECODE")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "CP_C_PHY_WAREHOUSE_ECODE")
    private String cpCPhyWarehouseEcode;

    @JSONField(name = "CP_C_PHY_WAREHOUSE_ENAME")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "CP_C_PHY_WAREHOUSE_ENAME")
    private String cpCPhyWarehouseEname;

    @JSONField(name = "BILL_NO")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "BILL_NO")
    private String billNo;

    @JSONField(name = "GOODS_OWNER")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "GOODS_OWNER")
    private String goodsOwner;

    @JSONField(name = "BILL_DATE")
    @Field(type = FieldType.Long)
    @ApiModelProperty(name = "BILL_DATE")
    private Date billDate;

    @JSONField(name = "BILL_STATUS")
    @Field(type = FieldType.Integer)
    @ApiModelProperty(name = "BILL_STATUS")
    private Integer billStatus;

    @JSONField(name = "OUT_TIME")
    @Field(type = FieldType.Long)
    @ApiModelProperty(name = "OUT_TIME")
    private Date outTime;

    @JSONField(name = "OUT_ID")
    @Field(type = FieldType.Long)
    @ApiModelProperty(name = "OUT_ID")
    private Long outId;

    @JSONField(name = "OUT_ECODE")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "OUT_ECODE")
    private String outEcode;

    @JSONField(name = "OUT_ENAME")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "OUT_ENAME")
    private String outEname;

    @JSONField(name = "OUT_NAME")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "OUT_NAME")
    private String outName;

    @JSONField(name = "CP_C_CUSTOMER_WAREHOUSE_ID")
    @Field(type = FieldType.Long)
    @ApiModelProperty(name = "CP_C_CUSTOMER_WAREHOUSE_ID")
    private Long cpCCustomerWarehouseId;

    @JSONField(name = "CP_C_CS_ECODE")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "CP_C_CS_ECODE")
    private String cpCCsEcode;

    @JSONField(name = "CP_C_CS_ENAME")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "CP_C_CS_ENAME")
    private String cpCCsEname;

    @JSONField(name = "CP_C_SUPPLIER_ID")
    @Field(type = FieldType.Long)
    @ApiModelProperty(name = "CP_C_SUPPLIER_ID")
    private Long cpCSupplierId;

    @JSONField(name = "OUT_TYPE")
    @Field(type = FieldType.Integer)
    @ApiModelProperty(name = "OUT_TYPE")
    private Integer outType;

    @JSONField(name = "SOURCE_BILL_TYPE")
    @Field(type = FieldType.Integer)
    @ApiModelProperty(name = "SOURCE_BILL_TYPE")
    private Integer sourceBillType;

    @JSONField(name = "SOURCE_BILL_ID")
    @Field(type = FieldType.Long)
    @ApiModelProperty(name = "SOURCE_BILL_ID")
    private Long sourceBillId;

    @JSONField(name = "SOURCE_BILL_NO")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "SOURCE_BILL_NO")
    private String sourceBillNo;

    @JSONField(name = "SG_B_PHY_OUT_NOTICES_ID")
    @Field(type = FieldType.Long)
    @ApiModelProperty(name = "SG_B_PHY_OUT_NOTICES_ID")
    private Long sgBPhyOutNoticesId;

    @JSONField(name = "RECEIVER_NAME")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "RECEIVER_NAME")
    private String receiverName;

    @JSONField(name = "RECEIVER_MOBILE")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "RECEIVER_MOBILE")
    private String receiverMobile;

    @JSONField(name = "RECEIVER_PHONE")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "RECEIVER_PHONE")
    private String receiverPhone;

    @JSONField(name = "CP_C_REGION_PROVINCE_ID")
    @Field(type = FieldType.Long)
    @ApiModelProperty(name = "CP_C_REGION_PROVINCE_ID")
    private Long cpCRegionProvinceId;

    @JSONField(name = "CP_C_REGION_PROVINCE_ECODE")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "CP_C_REGION_PROVINCE_ECODE")
    private String cpCRegionProvinceEcode;

    @JSONField(name = "CP_C_REGION_PROVINCE_ENAME")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "CP_C_REGION_PROVINCE_ENAME")
    private String cpCRegionProvinceEname;

    @JSONField(name = "CP_C_REGION_CITY_ID")
    @Field(type = FieldType.Long)
    @ApiModelProperty(name = "CP_C_REGION_CITY_ID")
    private Long cpCRegionCityId;

    @JSONField(name = "CP_C_REGION_CITY_ECODE")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "CP_C_REGION_CITY_ECODE")
    private String cpCRegionCityEcode;

    @JSONField(name = "CP_C_REGION_CITY_ENAME")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "CP_C_REGION_CITY_ENAME")
    private String cpCRegionCityEname;

    @JSONField(name = "CP_C_REGION_AREA_ID")
    @Field(type = FieldType.Long)
    @ApiModelProperty(name = "CP_C_REGION_AREA_ID")
    private Long cpCRegionAreaId;

    @JSONField(name = "CP_C_REGION_AREA_ECODE")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "CP_C_REGION_AREA_ECODE")
    private String cpCRegionAreaEcode;

    @JSONField(name = "CP_C_REGION_AREA_ENAME")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "CP_C_REGION_AREA_ENAME")
    private String cpCRegionAreaEname;

    @JSONField(name = "RECEIVER_ADDRESS")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "RECEIVER_ADDRESS")
    private String receiverAddress;

    @JSONField(name = "RECEIVER_ZIP")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "RECEIVER_ZIP")
    private String receiverZip;

    @JSONField(name = "CP_C_LOGISTICS_ID")
    @Field(type = FieldType.Long)
    @ApiModelProperty(name = "CP_C_LOGISTICS_ID")
    private Long cpCLogisticsId;

    @JSONField(name = "CP_C_LOGISTICS_ECODE")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "CP_C_LOGISTICS_ECODE")
    private String cpCLogisticsEcode;

    @JSONField(name = "CP_C_LOGISTICS_ENAME")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "CP_C_LOGISTICS_ENAME")
    private String cpCLogisticsEname;

    @JSONField(name = "LOGISTIC_NUMBER")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "LOGISTIC_NUMBER")
    private String logisticNumber;

    @JSONField(name = "DELIVERY_METHOD")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "DELIVERY_METHOD")
    private String deliveryMethod;

    @JSONField(name = "AMT_PAYMENT")
    @Field(type = FieldType.Double)
    @ApiModelProperty(name = "AMT_PAYMENT")
    private BigDecimal amtPayment;

    @JSONField(name = "IS_URGENT")
    @Field(type = FieldType.Integer)
    @ApiModelProperty(name = "IS_URGENT")
    private Integer isUrgent;

    @JSONField(name = "CP_C_SHOP_ID")
    @Field(type = FieldType.Long)
    @ApiModelProperty(name = "CP_C_SHOP_ID")
    private Long cpCShopId;

    @JSONField(name = "CP_C_SHOP_TITLE")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "CP_C_SHOP_TITLE")
    private String cpCShopTitle;

    @JSONField(name = "SOURCECODE")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "SOURCECODE")
    private String sourcecode;

    @JSONField(name = "SELLER_REMARK")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "SELLER_REMARK")
    private String sellerRemark;

    @JSONField(name = "BUYER_REMARK")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "BUYER_REMARK")
    private String buyerRemark;

    @JSONField(name = "REMARK")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "REMARK")
    private String remark;

    @JSONField(name = "TOT_QTY_OUT")
    @Field(type = FieldType.Double)
    @ApiModelProperty(name = "TOT_QTY_OUT")
    private BigDecimal totQtyOut;

    @JSONField(name = "TOT_AMT_LIST_OUT")
    @Field(type = FieldType.Double)
    @ApiModelProperty(name = "TOT_AMT_LIST_OUT")
    private BigDecimal totAmtListOut;

    @JSONField(name = "WMS_BILL_NO")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "WMS_BILL_NO")
    private String wmsBillNo;

    @JSONField(name = "PROPERTY1")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "PROPERTY1")
    private String property1;

    @JSONField(name = "PROPERTY2")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "PROPERTY2")
    private String property2;

    @JSONField(name = "PROPERTY3")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "PROPERTY3")
    private String property3;

    @JSONField(name = "PROPERTY4")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "PROPERTY4")
    private String property4;

    @JSONField(name = "PROPERTY5")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "PROPERTY5")
    private String property5;

    @JSONField(name = "PROPERTY6")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "PROPERTY6")
    private String property6;

    @JSONField(name = "PROPERTY7")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "PROPERTY7")
    private String property7;

    @JSONField(name = "PROPERTY8")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "PROPERTY8")
    private String property8;

    @JSONField(name = "PROPERTY9")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "PROPERTY9")
    private String property9;

    @JSONField(name = "PROPERTY10")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "PROPERTY10")
    private String property10;

    @JSONField(name = "AD_CLIENT_ID")
    @Field(type = FieldType.Long)
    @ApiModelProperty(name = "AD_CLIENT_ID")
    private Long adClientId;

    @JSONField(name = "AD_ORG_ID")
    @Field(type = FieldType.Long)
    @ApiModelProperty(name = "AD_ORG_ID")
    private Long adOrgId;

    @JSONField(name = "ISACTIVE")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "ISACTIVE")
    private String isactive;

    @JSONField(name = "OWNERID")
    @Field(type = FieldType.Long)
    @ApiModelProperty(name = "OWNERID")
    private Long ownerid;

    @JSONField(name = "OWNERENAME")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "OWNERENAME")
    private String ownerename;

    @JSONField(name = "OWNERNAME")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "OWNERNAME")
    private String ownername;

    @JSONField(name = "CREATIONDATE")
    @Field(type = FieldType.Long)
    @ApiModelProperty(name = "CREATIONDATE")
    private Date creationdate;

    @JSONField(name = "MODIFIERID")
    @Field(type = FieldType.Long)
    @ApiModelProperty(name = "MODIFIERID")
    private Long modifierid;

    @JSONField(name = "MODIFIERENAME")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "MODIFIERENAME")
    private String modifierename;

    @JSONField(name = "MODIFIERNAME")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "MODIFIERNAME")
    private String modifiername;

    @JSONField(name = "MODIFIEDDATE")
    @Field(type = FieldType.Long)
    @ApiModelProperty(name = "MODIFIEDDATE")
    private Date modifieddate;

    @JSONField(name = "SG_B_PHY_OUT_NOTICES")
    @Field(type = FieldType.Long)
    @ApiModelProperty(name = "SG_B_PHY_OUT_NOTICES")
    private Long sgBPhyOutNotices;

    @JSONField(name = "SG_B_PHY_OUT_NOTICES_BILLNO")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "SG_B_PHY_OUT_NOTICES_BILLNO")
    private String sgBPhyOutNoticesBillno;

    @JSONField(name = "ACTUAL_WEIGHT")
    @Field(type = FieldType.Double)
    @ApiModelProperty(name = "ACTUAL_WEIGHT")
    private BigDecimal actualWeight;

    @JSONField(name = "IS_LAST")
    @Field(type = FieldType.Integer)
    @ApiModelProperty(name = "IS_LAST")
    private Integer isLast;

    @JSONField(name = "THEORETICAL_WEIGHT")
    @Field(type = FieldType.Double)
    @ApiModelProperty(name = "THEORETICAL_WEIGHT")
    private BigDecimal theoreticalWeight;

    @JSONField(name = "OUT_BILL_NO")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "OUT_BILL_NO")
    private String outBillNo;

    @JSONField(name = "DATEIN_EXPECT")
    @Field(type = FieldType.Long)
    @ApiModelProperty(name = "DATEIN_EXPECT")
    private Date dateinExpect;

    @JSONField(name = "PO_ID")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "PO_ID")
    private String poId;

    @JSONField(name = "PS_C_BRAND_ID")
    @Field(type = FieldType.Long)
    @ApiModelProperty(name = "PS_C_BRAND_ID")
    private Long psCBrandId;

    @JSONField(name = "PS_C_BRAND_ECODE")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "PS_C_BRAND_ECODE")
    private String psCBrandEcode;

    @JSONField(name = "PS_C_BRAND_ENAME")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "PS_C_BRAND_ENAME")
    private String psCBrandEname;

    @JSONField(name = "DELER_ID")
    @Field(type = FieldType.Long)
    @ApiModelProperty(name = "DELER_ID")
    private Long delerId;

    @JSONField(name = "DELER_ENAME")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "DELER_ENAME")
    private String delerEname;

    @JSONField(name = "DELER_NAME")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "DELER_NAME")
    private String delerName;

    @JSONField(name = "DEL_TIME")
    @Field(type = FieldType.Long)
    @ApiModelProperty(name = "DEL_TIME")
    private Date delTime;

    @JSONField(name = "RESERVE_BIGINT01")
    @Field(type = FieldType.Long)
    @ApiModelProperty(name = "RESERVE_BIGINT01")
    private Long reserveBigint01;

    @JSONField(name = "RESERVE_BIGINT02")
    @Field(type = FieldType.Long)
    @ApiModelProperty(name = "RESERVE_BIGINT02")
    private Long reserveBigint02;

    @JSONField(name = "RESERVE_BIGINT03")
    @Field(type = FieldType.Long)
    @ApiModelProperty(name = "RESERVE_BIGINT03")
    private Long reserveBigint03;

    @JSONField(name = "RESERVE_BIGINT04")
    @Field(type = FieldType.Long)
    @ApiModelProperty(name = "RESERVE_BIGINT04")
    private Long reserveBigint04;

    @JSONField(name = "RESERVE_BIGINT05")
    @Field(type = FieldType.Long)
    @ApiModelProperty(name = "RESERVE_BIGINT05")
    private Long reserveBigint05;

    @JSONField(name = "RESERVE_BIGINT06")
    @Field(type = FieldType.Long)
    @ApiModelProperty(name = "RESERVE_BIGINT06")
    private Long reserveBigint06;

    @JSONField(name = "RESERVE_BIGINT07")
    @Field(type = FieldType.Long)
    @ApiModelProperty(name = "RESERVE_BIGINT07")
    private Long reserveBigint07;

    @JSONField(name = "RESERVE_BIGINT08")
    @Field(type = FieldType.Long)
    @ApiModelProperty(name = "RESERVE_BIGINT08")
    private Long reserveBigint08;

    @JSONField(name = "RESERVE_BIGINT09")
    @Field(type = FieldType.Long)
    @ApiModelProperty(name = "RESERVE_BIGINT09")
    private Long reserveBigint09;

    @JSONField(name = "RESERVE_BIGINT10")
    @Field(type = FieldType.Long)
    @ApiModelProperty(name = "RESERVE_BIGINT10")
    private Long reserveBigint10;

    @JSONField(name = "RESERVE_VARCHAR01")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "RESERVE_VARCHAR01")
    private String reserveVarchar01;

    @JSONField(name = "RESERVE_VARCHAR02")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "RESERVE_VARCHAR02")
    private String reserveVarchar02;

    @JSONField(name = "RESERVE_VARCHAR03")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "RESERVE_VARCHAR03")
    private String reserveVarchar03;

    @JSONField(name = "RESERVE_VARCHAR04")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "RESERVE_VARCHAR04")
    private String reserveVarchar04;

    @JSONField(name = "RESERVE_VARCHAR05")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "RESERVE_VARCHAR05")
    private String reserveVarchar05;

    @JSONField(name = "RESERVE_VARCHAR06")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "RESERVE_VARCHAR06")
    private String reserveVarchar06;

    @JSONField(name = "RESERVE_VARCHAR07")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "RESERVE_VARCHAR07")
    private String reserveVarchar07;

    @JSONField(name = "RESERVE_VARCHAR08")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "RESERVE_VARCHAR08")
    private String reserveVarchar08;

    @JSONField(name = "RESERVE_VARCHAR09")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "RESERVE_VARCHAR09")
    private String reserveVarchar09;

    @JSONField(name = "RESERVE_VARCHAR10")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "RESERVE_VARCHAR10")
    private String reserveVarchar10;

    @JSONField(name = "RESERVE_DECIMAL01")
    @Field(type = FieldType.Double)
    @ApiModelProperty(name = "RESERVE_DECIMAL01")
    private BigDecimal reserveDecimal01;

    @JSONField(name = "RESERVE_DECIMAL02")
    @Field(type = FieldType.Double)
    @ApiModelProperty(name = "RESERVE_DECIMAL02")
    private BigDecimal reserveDecimal02;

    @JSONField(name = "RESERVE_DECIMAL03")
    @Field(type = FieldType.Double)
    @ApiModelProperty(name = "RESERVE_DECIMAL03")
    private BigDecimal reserveDecimal03;

    @JSONField(name = "RESERVE_DECIMAL04")
    @Field(type = FieldType.Double)
    @ApiModelProperty(name = "RESERVE_DECIMAL04")
    private BigDecimal reserveDecimal04;

    @JSONField(name = "RESERVE_DECIMAL05")
    @Field(type = FieldType.Double)
    @ApiModelProperty(name = "RESERVE_DECIMAL05")
    private BigDecimal reserveDecimal05;

    @JSONField(name = "RESERVE_DECIMAL06")
    @Field(type = FieldType.Double)
    @ApiModelProperty(name = "RESERVE_DECIMAL06")
    private BigDecimal reserveDecimal06;

    @JSONField(name = "RESERVE_DECIMAL07")
    @Field(type = FieldType.Double)
    @ApiModelProperty(name = "RESERVE_DECIMAL07")
    private BigDecimal reserveDecimal07;

    @JSONField(name = "RESERVE_DECIMAL08")
    @Field(type = FieldType.Double)
    @ApiModelProperty(name = "RESERVE_DECIMAL08")
    private BigDecimal reserveDecimal08;

    @JSONField(name = "RESERVE_DECIMAL09")
    @Field(type = FieldType.Double)
    @ApiModelProperty(name = "RESERVE_DECIMAL09")
    private BigDecimal reserveDecimal09;

    @JSONField(name = "RESERVE_DECIMAL10")
    @Field(type = FieldType.Double)
    @ApiModelProperty(name = "RESERVE_DECIMAL10")
    private BigDecimal reserveDecimal10;
}