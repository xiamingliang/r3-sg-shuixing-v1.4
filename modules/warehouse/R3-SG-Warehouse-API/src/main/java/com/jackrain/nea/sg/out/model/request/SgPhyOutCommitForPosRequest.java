package com.jackrain.nea.sg.out.model.request;

import com.jackrain.nea.sg.basic.model.request.SgR3BaseRequest;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @author 舒威
 * @since 2019/11/21
 * create at : 2019/11/21 14:20
 */
@Data
public class SgPhyOutCommitForPosRequest extends SgR3BaseRequest implements Serializable {

    /**
     * 零售单id
     */
    private Long id;

    /**
     * 零售单号
     */
    private String billNo;

    /**
     * 零售单日期
     */
    private Date billDate;

    /**
     * 门店id
     */
    private Long cpCStoreId;

    private String cpCStoreEcode;

    private String cpCStoreEname;

    private Long cpCPhyWarehouseId;

    private String cpCPhyWarehouseEcode;

    private String cpCPhyWarehouseEname;

    private String receiverName;

    private String receiverMobile;

    private String receiverPhone;

    private Long cpCRegionProvinceId;

    private String cpCRegionProvinceEcode;

    private String cpCRegionProvinceEname;

    private Long cpCRegionCityId;

    private String cpCRegionCityEcode;

    private String cpCRegionCityEname;

    private Long cpCRegionAreaId;

    private String cpCRegionAreaEcode;

    private String cpCRegionAreaEname;

    private String receiverAddress;

    private String receiverZip;

    private Long cpCLogisticsId;

    private String cpCLogisticsEcode;

    private String cpCLogisticsEname;

    private String logisticNumber;

    private String deliveryMethod;

    private BigDecimal amtPayment;

    private Integer isUrgent;

    private Long cpCShopId;

    private String cpCShopTitle;

    private String sourcecode;

    private String cpCCsEcode;

    private String cpCCsEname;

    private Long cpCSupplierId;

    private String sellerRemark;

    private String buyerRemark;

    private String remark;

    private List<SgPhyOutCommitItemForPosRequest> items;

}
