package com.jackrain.nea.sg.in.api;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.basic.model.result.SgR3BaseResult;
import com.jackrain.nea.sg.in.model.request.SgPhyInNoticesVoidReqeust;
import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 * @Author: ChenChen
 * @Description:作废入库通知单
 * @Date: Create in 14:41 2019/5/5
 */
public interface SgPhyInNoticeVoidCmd {
    ValueHolderV14<SgR3BaseResult> voidSgPhyInNotice(SgPhyInNoticesVoidReqeust request) throws NDSException;
}
