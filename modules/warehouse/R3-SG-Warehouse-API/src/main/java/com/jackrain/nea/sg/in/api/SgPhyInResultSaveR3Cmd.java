package com.jackrain.nea.sg.in.api;

import com.jackrain.nea.sys.Command;

/**
 *  入库结果单(r3框架接口)
 * @author jiang.cj
 * @since 2019-4-22
 * create at : 2019-4-22
 */
public interface SgPhyInResultSaveR3Cmd extends Command {
}
