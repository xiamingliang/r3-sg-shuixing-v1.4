package com.jackrain.nea.sg.out.api;

import com.jackrain.nea.sys.Command;

/**
 * @author leexh
 * @since 2019/4/19 14:37
 */
public interface SgPhyOutNoticesSaveR3Cmd extends Command {
}
