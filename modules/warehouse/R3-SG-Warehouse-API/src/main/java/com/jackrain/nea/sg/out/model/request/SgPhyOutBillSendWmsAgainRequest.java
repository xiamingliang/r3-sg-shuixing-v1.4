package com.jackrain.nea.sg.out.model.request;


import com.alibaba.fastjson.JSONArray;
import com.jackrain.nea.web.face.User;
import lombok.Data;

import java.io.Serializable;

/**
 * @author leexh
 * @since 2019/6/24 16:01
 * desc:
 */
@Data
public class SgPhyOutBillSendWmsAgainRequest implements Serializable {

    /**
     * 主键ID
     */
    private JSONArray ids;

    /**
     * 是否是单对象
     * true=单对象
     * false=列表
     *  注：单对象和列表返回的message不同
     */
    private Boolean isObj;

    /**
     * 用户信息
     */
    private User user;
}
