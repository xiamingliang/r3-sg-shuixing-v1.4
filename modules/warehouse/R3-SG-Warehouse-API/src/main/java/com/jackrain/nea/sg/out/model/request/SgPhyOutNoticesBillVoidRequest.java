package com.jackrain.nea.sg.out.model.request;

import com.jackrain.nea.web.face.User;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author leexh
 * @since 2019/4/23 15:54
 */
@Data
public class SgPhyOutNoticesBillVoidRequest implements Serializable {

    private List<Long> ids;

    private List<SgPhyOutNoticesSaveRequest> noticesSaveRequests;

    private User loginUser;

    private Boolean posCancel;
}
