package com.jackrain.nea.sg.out.api;

import com.jackrain.nea.sys.Command;

/**
 * @author leexh
 * @since 2019/4/24 14:59
 */
public interface SgPhyOutResultSaveR3Cmd extends Command {
}
