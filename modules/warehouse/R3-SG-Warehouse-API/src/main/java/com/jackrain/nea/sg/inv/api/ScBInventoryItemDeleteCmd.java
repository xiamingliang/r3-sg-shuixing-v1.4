package com.jackrain.nea.sg.inv.api;

import com.jackrain.nea.sys.Command;

/**
 * @author: 周琳胜
 * @since: 2019/11/13
 * create at : 2019/11/13 10:58
 */
public interface ScBInventoryItemDeleteCmd extends Command{
}
