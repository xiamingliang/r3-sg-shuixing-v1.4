package com.jackrain.nea.sg.phyadjust.model.table;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.*;
import com.jackrain.nea.es.annotations.Document;
import com.jackrain.nea.es.annotations.Field;
import com.jackrain.nea.es.constans.FieldType;
import com.jackrain.nea.sys.domain.BaseModelES;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@TableName(value = "sg_b_phy_adjust")
@Data
@Document(index = "sg_b_phy_adjust",type = "sg_b_phy_adjust")
public class SgBPhyAdjust extends BaseModelES {
    @JSONField(name = "ID")
    @TableField(fill = FieldFill.INSERT)
    @TableId(value = "id", type = IdType.INPUT)
    @Field(type = FieldType.Long)
    private Long id;

    @JSONField(name = "BILL_NO")
    @Field(type = FieldType.Keyword)
    private String billNo;

    @JSONField(name = "CP_C_PHY_WAREHOUSE_ID")
    @Field(type = FieldType.Long)
    private Long cpCPhyWarehouseId;

    @JSONField(name = "CP_C_PHY_WAREHOUSE_ECODE")
    @Field(type = FieldType.Keyword)
    private String cpCPhyWarehouseEcode;

    @JSONField(name = "CP_C_PHY_WAREHOUSE_ENAME")
    @Field(type = FieldType.Keyword)
    private String cpCPhyWarehouseEname;

    @JSONField(name = "BILL_TYPE")
    @Field(type = FieldType.Integer)
    private Integer billType;

    @JSONField(name = "SG_B_ADJUST_PROP_ID")
    @Field(type = FieldType.Long)
    private Long sgBAdjustPropId;

    @JSONField(name = "SOURCE_BILL_TYPE")
    @Field(type = FieldType.Integer)
    private Integer sourceBillType;

    @JSONField(name = "SOURCE_BILL_ID")
    @Field(type = FieldType.Long)
    private Long sourceBillId;

    @JSONField(name = "SOURCE_BILL_NO")
    @Field(type = FieldType.Keyword)
    private String sourceBillNo;

    @JSONField(name = "BILL_STATUS")
    @Field(type = FieldType.Integer)
    private Integer billStatus;

    @JSONField(name = "TOT_QTY")
    @Field(type = FieldType.Double)
    private BigDecimal totQty;

    @JSONField(name = "CHECKER_ID")
    @Field(type = FieldType.Long)
    private Long checkerId;

    @JSONField(name = "CHECKER_ENAME")
    @Field(type = FieldType.Keyword)
    private String checkerEname;

    @JSONField(name = "CHECKER_NAME")
    @Field(type = FieldType.Keyword)
    private String checkerName;

    @JSONField(name = "CHECK_TIME")
    @Field(type = FieldType.Long)
    private Date checkTime;

    @JSONField(name = "AD_CLIENT_ID")
    @Field(type = FieldType.Long)
    private Long adClientId;

    @JSONField(name = "AD_ORG_ID")
    @Field(type = FieldType.Long)
    private Long adOrgId;

    @JSONField(name = "ISACTIVE")
    @Field(type = FieldType.Keyword)
    private String isactive;

    @JSONField(name = "OWNERID")
    @Field(type = FieldType.Long)
    private Long ownerid;

    @JSONField(name = "OWNERENAME")
    @Field(type = FieldType.Keyword)
    private String ownerename;

    @JSONField(name = "OWNERNAME")
    @Field(type = FieldType.Keyword)
    private String ownername;

    @JSONField(name = "CREATIONDATE")
    @Field(type = FieldType.Long)
    private Date creationdate;

    @JSONField(name = "MODIFIERID")
    @Field(type = FieldType.Long)
    private Long modifierid;

    @JSONField(name = "MODIFIERENAME")
    @Field(type = FieldType.Keyword)
    private String modifierename;

    @JSONField(name = "MODIFIERNAME")
    @Field(type = FieldType.Keyword)
    private String modifiername;

    @JSONField(name = "MODIFIEDDATE")
    @Field(type = FieldType.Long)
    private Date modifieddate;

    @JSONField(name = "BILL_DATE")
    @Field(type = FieldType.Long)
    private Date billDate;

    @JSONField(name = "WMS_BILL_NO")
    @Field(type = FieldType.Keyword)
    private String wmsBillNo;

    @JSONField(name = "RESERVE_BIGINT01")
    @Field(type = FieldType.Long)
    private Long reserveBigint01;

    @JSONField(name = "RESERVE_BIGINT02")
    @Field(type = FieldType.Long)
    private Long reserveBigint02;

    @JSONField(name = "RESERVE_BIGINT03")
    @Field(type = FieldType.Long)
    private Long reserveBigint03;

    @JSONField(name = "RESERVE_BIGINT04")
    @Field(type = FieldType.Long)
    private Long reserveBigint04;

    @JSONField(name = "RESERVE_BIGINT05")
    @Field(type = FieldType.Long)
    private Long reserveBigint05;

    @JSONField(name = "RESERVE_BIGINT06")
    @Field(type = FieldType.Long)
    private Long reserveBigint06;

    @JSONField(name = "RESERVE_BIGINT07")
    @Field(type = FieldType.Long)
    private Long reserveBigint07;

    @JSONField(name = "RESERVE_BIGINT08")
    @Field(type = FieldType.Long)
    private Long reserveBigint08;

    @JSONField(name = "RESERVE_BIGINT09")
    @Field(type = FieldType.Long)
    private Long reserveBigint09;

    @JSONField(name = "RESERVE_BIGINT10")
    @Field(type = FieldType.Long)
    private Long reserveBigint10;

    @JSONField(name = "RESERVE_VARCHAR01")
    @Field(type = FieldType.Keyword)
    private String reserveVarchar01;

    @JSONField(name = "RESERVE_VARCHAR02")
    @Field(type = FieldType.Keyword)
    private String reserveVarchar02;

    @JSONField(name = "RESERVE_VARCHAR03")
    @Field(type = FieldType.Keyword)
    private String reserveVarchar03;

    @JSONField(name = "RESERVE_VARCHAR04")
    @Field(type = FieldType.Keyword)
    private String reserveVarchar04;

    @JSONField(name = "RESERVE_VARCHAR05")
    @Field(type = FieldType.Keyword)
    private String reserveVarchar05;

    @JSONField(name = "RESERVE_VARCHAR06")
    @Field(type = FieldType.Keyword)
    private String reserveVarchar06;

    @JSONField(name = "RESERVE_VARCHAR07")
    @Field(type = FieldType.Keyword)
    private String reserveVarchar07;

    @JSONField(name = "RESERVE_VARCHAR08")
    @Field(type = FieldType.Keyword)
    private String reserveVarchar08;

    @JSONField(name = "RESERVE_VARCHAR09")
    @Field(type = FieldType.Keyword)
    private String reserveVarchar09;

    @JSONField(name = "RESERVE_VARCHAR10")
    @Field(type = FieldType.Keyword)
    private String reserveVarchar10;

    @JSONField(name = "RESERVE_DECIMAL01")
    @Field(type = FieldType.Double)
    private BigDecimal reserveDecimal01;

    @JSONField(name = "RESERVE_DECIMAL02")
    @Field(type = FieldType.Double)
    private BigDecimal reserveDecimal02;

    @JSONField(name = "RESERVE_DECIMAL03")
    @Field(type = FieldType.Double)
    private BigDecimal reserveDecimal03;

    @JSONField(name = "RESERVE_DECIMAL04")
    @Field(type = FieldType.Double)
    private BigDecimal reserveDecimal04;

    @JSONField(name = "RESERVE_DECIMAL05")
    @Field(type = FieldType.Double)
    private BigDecimal reserveDecimal05;

    @JSONField(name = "RESERVE_DECIMAL06")
    @Field(type = FieldType.Double)
    private BigDecimal reserveDecimal06;

    @JSONField(name = "RESERVE_DECIMAL07")
    @Field(type = FieldType.Double)
    private BigDecimal reserveDecimal07;

    @JSONField(name = "RESERVE_DECIMAL08")
    @Field(type = FieldType.Double)
    private BigDecimal reserveDecimal08;

    @JSONField(name = "RESERVE_DECIMAL09")
    @Field(type = FieldType.Double)
    private BigDecimal reserveDecimal09;

    @JSONField(name = "RESERVE_DECIMAL10")
    @Field(type = FieldType.Double)
    private BigDecimal reserveDecimal10;

    @JSONField(name = "REMARK")
    @Field(type = FieldType.Keyword)
    private String remark;

    @JSONField(name = "PERSON_LIABLE")
    @Field(type = FieldType.Keyword)
    private String personLiable;

    @JSONField(name = "FULL_LINK")
    @Field(type = FieldType.Keyword)
    private String fullLink;

    @JSONField(name = "CP_C_STORE_ID")
    @Field(type = FieldType.Long)
    private Long cpCStoreId;

    @JSONField(name = "CP_C_STORE_ECODE")
    @Field(type = FieldType.Keyword)
    private String cpCStoreEcode;

    @JSONField(name = "CP_C_STORE_ENAME")
    @Field(type = FieldType.Keyword)
    private String cpCStoreEname;

    @JSONField(name = "TRADE_MARK")
    @Field(type = FieldType.Keyword)
    private String tradeMark;

    @JSONField(name = "OC_B_REFUND_IN_ID")
    @Field(type = FieldType.Long)
    private Long ocBRefundInId;

    @JSONField(name = "ADJUST_FEE")
    @Field(type = FieldType.Double)
    private BigDecimal adjustFee;
}