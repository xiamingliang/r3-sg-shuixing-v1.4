package com.jackrain.nea.sg.in.model.table;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.jackrain.nea.es.annotations.Document;
import com.jackrain.nea.es.annotations.Field;
import com.jackrain.nea.es.constans.FieldType;
import com.jackrain.nea.sys.domain.BaseModelES;
import lombok.Data;

import java.math.BigDecimal;

@TableName(value = "sg_b_phy_in_notices_imp_item")
@Data
@Document(index = "sg_b_phy_in_notices",type = "sg_b_phy_in_notices_imp_item")
public class SgBPhyInNoticesImpItem extends BaseModelES {
    @JSONField(name = "ID")
    @TableField(fill = FieldFill.INSERT)
    @TableId(value = "id", type = IdType.INPUT)
    @Field(type = FieldType.Long)
    private Long id;

    @JSONField(name = "SG_B_PHY_IN_NOTICES_ID")
    @Field(type = FieldType.Long)
    private Long sgBPhyInNoticesId;

    @JSONField(name = "SOURCE_BILL_ITEM_ID")
    @Field(type = FieldType.Long)
    private Long sourceBillItemId;

    @JSONField(name = "IS_TEUS")
    @Field(type = FieldType.Integer)
    private Integer isTeus;

    @JSONField(name = "PS_C_TEUS_ID")
    @Field(type = FieldType.Long)
    private Long psCTeusId;

    @JSONField(name = "PS_C_TEUS_ECODE")
    @Field(type = FieldType.Keyword)
    private String psCTeusEcode;

    @JSONField(name = "PS_C_MATCHSIZE_ID")
    @Field(type = FieldType.Long)
    private Long psCMatchsizeId;

    @JSONField(name = "PS_C_MATCHSIZE_ECODE")
    @Field(type = FieldType.Keyword)
    private String psCMatchsizeEcode;

    @JSONField(name = "PS_C_MATCHSIZE_ENAME")
    @Field(type = FieldType.Keyword)
    private String psCMatchsizeEname;

    @JSONField(name = "PS_C_SKU_ID")
    @Field(type = FieldType.Long)
    private Long psCSkuId;

    @JSONField(name = "PS_C_SKU_ECODE")
    @Field(type = FieldType.Keyword)
    private String psCSkuEcode;

    @JSONField(name = "PS_C_PRO_ID")
    @Field(type = FieldType.Long)
    private Long psCProId;

    @JSONField(name = "PS_C_PRO_ECODE")
    @Field(type = FieldType.Keyword)
    private String psCProEcode;

    @JSONField(name = "PS_C_PRO_ENAME")
    @Field(type = FieldType.Keyword)
    private String psCProEname;

    @JSONField(name = "PS_C_SPEC1_ID")
    @Field(type = FieldType.Long)
    private Long psCSpec1Id;

    @JSONField(name = "PS_C_SPEC1_ECODE")
    @Field(type = FieldType.Keyword)
    private String psCSpec1Ecode;

    @JSONField(name = "PS_C_SPEC1_ENAME")
    @Field(type = FieldType.Keyword)
    private String psCSpec1Ename;

    @JSONField(name = "PS_C_SPEC2_ID")
    @Field(type = FieldType.Long)
    private Long psCSpec2Id;

    @JSONField(name = "PS_C_SPEC2_ECODE")
    @Field(type = FieldType.Keyword)
    private String psCSpec2Ecode;

    @JSONField(name = "PS_C_SPEC2_ENAME")
    @Field(type = FieldType.Keyword)
    private String psCSpec2Ename;

    @JSONField(name = "PRICE_LIST")
    @Field(type = FieldType.Double)
    private BigDecimal priceList;

    @JSONField(name = "QTY")
    @Field(type = FieldType.Double)
    private BigDecimal qty;

    @JSONField(name = "QTY_IN")
    @Field(type = FieldType.Double)
    private BigDecimal qtyIn;

    @JSONField(name = "QTY_DIFF")
    @Field(type = FieldType.Double)
    private BigDecimal qtyDiff;

    @JSONField(name = "REMARK")
    @Field(type = FieldType.Keyword)
    private String remark;

    @JSONField(name = "VERSION")
    @Field(type = FieldType.Long)
    private Long version;

    @JSONField(name = "OWNERENAME")
    @Field(type = FieldType.Keyword)
    private String ownerename;

    @JSONField(name = "MODIFIERENAME")
    @Field(type = FieldType.Keyword)
    private String modifierename;

    @JSONField(name = "QTY_SCAN")
    @Field(type = FieldType.Double)
    private BigDecimal qtyScan;
}