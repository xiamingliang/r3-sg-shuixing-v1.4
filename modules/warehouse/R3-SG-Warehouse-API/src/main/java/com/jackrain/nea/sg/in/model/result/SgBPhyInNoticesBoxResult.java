package com.jackrain.nea.sg.in.model.result;

import com.jackrain.nea.sg.in.model.table.SgBPhyInNotices;
import com.jackrain.nea.sg.in.model.table.SgBPhyInNoticesImpItem;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 入库通知单查询结果返回实体(箱功能)
 */
@Data
public class SgBPhyInNoticesBoxResult implements Serializable {

    private static final long serialVersionUID = -3755700430431708016L;
    SgBPhyInNotices notices;
    List<SgBPhyInNoticesImpItem> itemList;

}
