package com.jackrain.nea.sg.in.model.request;

import com.alibaba.fastjson.annotation.JSONField;
import com.jackrain.nea.web.face.User;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Author: chenchen
 * @Description:入库通知单Requst
 * @Date: Create in 16:18 2019/4/22
 */
@Data
public class SgPhyInNoticesBillSaveRequest implements Serializable {

    private static final long serialVersionUID = -4168155439300140407L;

    @JSONField(name = "SG_B_PHY_IN_NOTICES")
    private SgPhyInNoticesRequest notices;

    private List<SgPhyInNoticesItemRequest> itemList;

    @JSONField(name = "USER")
    private User loginUser;

    /**
     * 录入明细信息
     */
    @JSONField(name = "SG_B_PHY_IN_NOTICES_IMP_ITEM")
    private List<SgPhyInNoticesImpItemSaveRequest> impItemList;

    /**
     * 箱内明细信息
     */
    @JSONField(name = "SG_B_PHY_IN_NOTICES_TEUS_ITEM")
    private List<SgPhyInNoticesTeusItemSaveRequest> teusItemList;

    /**
     * 录入明细是否从reids获取(默认fasle)
     */
    @JSONField(name = "REDIS_KEY")
    private String redisKey;

}
