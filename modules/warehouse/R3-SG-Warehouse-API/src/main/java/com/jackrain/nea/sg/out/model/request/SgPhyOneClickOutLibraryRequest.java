package com.jackrain.nea.sg.out.model.request;

import com.jackrain.nea.web.face.User;
import lombok.Data;

import java.io.Serializable;

/**
 * @author: 舒威
 * @since: 2019/9/10
 * create at : 2019/9/10 11:15
 */
@Data
public class SgPhyOneClickOutLibraryRequest implements Serializable {

    private Long sourceBillId;

    private Integer sourceBillType;

    private String lockKey;

    private User loginUser;
}
