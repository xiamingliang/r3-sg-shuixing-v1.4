package com.jackrain.nea.sg.phyadjust.api;

import com.jackrain.nea.sg.phyadjust.model.request.SgPhyAdjustBillQueryRequest;
import com.jackrain.nea.sg.phyadjust.model.result.SgPhyAdjustBillQueryResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 * @author: 舒威
 * @since: 2019/7/29
 * create at : 2019/7/29 22:17
 */
public interface SgPhyAdjustQueryCmd {

    /**
     * 查询库存调整单
     *
     * @param request 新增的参数
     * @return result
     */
    ValueHolderV14<SgPhyAdjustBillQueryResult> queryPhyAdj(SgPhyAdjustBillQueryRequest request);
}
