package com.jackrain.nea.sg.out.model.request;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author 舒威
 * @since 2019/11/21
 * create at : 2019/11/21 14:23
 */
@Data
public class SgPhyOutCommitItemForPosRequest implements Serializable {

    private Long id;

    private Long psCSkuId;

    private String psCSkuEcode;

    private Long psCProId;

    private String psCProEcode;

    private String psCProEname;

    private Long psCSpec1Id;

    private String psCSpec1Ecode;

    private String psCSpec1Ename;

    private Long psCSpec2Id;

    private String psCSpec2Ecode;

    private String psCSpec2Ename;

    private BigDecimal priceList;

    private BigDecimal priceCost;

    private BigDecimal amtCost;

    private BigDecimal qty;
}
