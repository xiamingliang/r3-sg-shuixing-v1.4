package com.jackrain.nea.sg.out.api;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.basic.model.result.SgR3BaseResult;
import com.jackrain.nea.sg.out.model.request.SgPhyOutNoticesItemBillDelRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 * @author leexh
 * @since 2019/4/24 9:25
 */
public interface SgPhyOutNoticesItemDelCmd {
    ValueHolderV14<SgR3BaseResult> delSgPhyOutNoticesItem(SgPhyOutNoticesItemBillDelRequest request) throws NDSException;
}
