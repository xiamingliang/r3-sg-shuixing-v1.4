package com.jackrain.nea.sg.out.api;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.basic.model.result.SgR3BaseResult;
import com.jackrain.nea.sg.out.model.request.SgPhyOutResultBillAuditRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;

import java.util.List;

/**
 * @author leexh
 * @since 2019/4/25 14:00
 */
public interface SgPhyOutResultAuditCmd {

    ValueHolderV14<SgR3BaseResult> auditSgPhyOutResult(SgPhyOutResultBillAuditRequest request) throws NDSException;

    ValueHolderV14 auditSgPhyOutResultByWMS(List<Long> ids, User user);
}
