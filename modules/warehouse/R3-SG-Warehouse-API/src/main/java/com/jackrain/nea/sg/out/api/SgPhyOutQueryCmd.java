package com.jackrain.nea.sg.out.api;

import com.jackrain.nea.sg.out.model.request.SgPhyOutBillQueryRequest;
import com.jackrain.nea.sg.out.model.result.SgOutQueryResult;
import com.jackrain.nea.sg.out.model.result.SgOutResultSendMsgResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;

import java.util.List;

/**
 * @author leexh
 * @since 2019/5/30 17:58
 * desc: 出库通知单/结果单查询接口
 */
public interface SgPhyOutQueryCmd {

    ValueHolderV14<List<SgOutQueryResult>> queryOutBySource(SgPhyOutBillQueryRequest request);

    ValueHolderV14<List<SgOutResultSendMsgResult>> querySgPhyOutResultMQBody(List<Long> ids, User user);

    ValueHolderV14<List<SgOutResultSendMsgResult>> querySgPhyOutResultJITXMQBody(List<Long> sourceBillIds, User user);
}
