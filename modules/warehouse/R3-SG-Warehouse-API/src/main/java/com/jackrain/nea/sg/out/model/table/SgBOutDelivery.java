package com.jackrain.nea.sg.out.model.table;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.*;
import com.jackrain.nea.sys.domain.BaseModel;
import lombok.Data;

import java.math.BigDecimal;

@TableName(value = "sg_b_out_delivery")
@Data
public class SgBOutDelivery extends BaseModel {
    @JSONField(name = "ID")
    @TableField(fill = FieldFill.INSERT)
    @TableId(value = "id", type = IdType.INPUT)
    private Long id;

    @JSONField(name = "CP_C_LOGISTICS_ID")
    private String cpCLogisticsId;

    @JSONField(name = "CP_C_LOGISTICS_ECODE")
    private String cpCLogisticsEcode;

    @JSONField(name = "CP_C_LOGISTICS_ENAME")
    private String cpCLogisticsEname;

    @JSONField(name = "LOGISTIC_NUMBER")
    private String logisticNumber;

    @JSONField(name = "PKG_PRODUCTS")
    private String pkgProducts;

    @JSONField(name = "PS_C_PRO_ID")
    private Long psCProId;

    @JSONField(name = "PS_C_PRO_ECODE")
    private String psCProEcode;

    @JSONField(name = "PS_C_PRO_ENAME")
    private String psCProEname;

    @JSONField(name = "PS_C_SKU_ID")
    private Long psCSkuId;

    @JSONField(name = "PS_C_SKU_ECODE")
    private String psCSkuEcode;

    @JSONField(name = "PS_C_CLR_ID")
    private Long psCClrId;

    @JSONField(name = "PS_C_CLR_ECODE")
    private String psCClrEcode;

    @JSONField(name = "PS_C_CLR_ENAME")
    private String psCClrEname;

    @JSONField(name = "PS_C_SIZE_ID")
    private Long psCSizeId;

    @JSONField(name = "PS_C_SIZE_ECODE")
    private String psCSizeEcode;

    @JSONField(name = "PS_C_SIZE_ENAME")
    private String psCSizeEname;

    @JSONField(name = "QTY")
    private BigDecimal qty;

    @JSONField(name = "WEIGHT")
    private BigDecimal weight;

    @JSONField(name = "SIZE")
    private BigDecimal size;

    @JSONField(name = "OC_B_ORDER_ID")
    private Long ocBOrderId;

    @JSONField(name = "VERSION")
    private Long version;

    @JSONField(name = "OWNERENAME")
    private String ownerename;

    @JSONField(name = "MODIFIERENAME")
    private String modifierename;

    @JSONField(name = "SG_B_PHY_OUT_RESULT_ID")
    private Long sgBPhyOutResultId;

    @JSONField(name = "GBCODE")
    private String gbcode;
}