package com.jackrain.nea.sg.in.model.request;

import com.jackrain.nea.sg.basic.model.request.SgR3BaseRequest;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

@Data
@ToString
public class SgPhyInResultBillSaveRequest extends SgR3BaseRequest implements Serializable {


    private SgBPhyInResultSaveRequest sgBPhyInResultSaveRequest;

    private List<SgBPhyInResultItemSaveRequest> itemList;

    private List<SgBPhyInResultDefectItemSaveRequest> defectItemList;

    /**
     * 录入明细信息
     */
    private List<SgPhyInResultImpItemSaveRequest> impItemList;

    /**
     * 箱内明细信息
     */
    private List<SgPhyInResultTeusItemSaveRequest> teusItemList;


    /**
     * 录入明细是否从reids获取(默认fasle)
     */
    private String redisKey;

    /**
     * 是否一键入库
     */
    private Boolean isOneClickLibrary;

    /**
     * 逻辑仓id(退货单用)
     */
    private Long cpCStoreId;

    /**
     * 逻辑仓ecode(退货单用)
     */
    private String cpCStoreEcode;

    /**
     * 逻辑仓ename(退货单用)
     */
    private String cpCStoreEname;

    private Long packOrderId;

}
