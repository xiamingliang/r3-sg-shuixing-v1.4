package com.jackrain.nea.sg.in.api;

import com.jackrain.nea.sg.in.model.request.SgPhyInNoticesBillSaveRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;

import java.util.List;

/**
 * @Author: chenchen
 * @Description:入库通知单(退回)
 * @Date: Create in 16:17 2019/4/22
 */
public interface SgPhyInNoticesSaveByReturnCmd {
    ValueHolderV14 saveSgBPhyInNoticesByReturn(List<SgPhyInNoticesBillSaveRequest> requestList);

    ValueHolderV14 saveSgBPhyInNoticesByReturnRPC(List<SgPhyInNoticesBillSaveRequest> requestList);

    ValueHolderV14 saveSgBPhyInNoticesByRPC(List<SgPhyInNoticesBillSaveRequest> requestList, Boolean isNeedReceive, Boolean isNeedInResult);
}
