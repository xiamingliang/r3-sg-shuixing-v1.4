package com.jackrain.nea.sg.phyadjust.model.request;

import com.alibaba.fastjson.annotation.JSONField;
import com.jackrain.nea.web.face.User;
import lombok.Data;

import java.io.Serializable;

/**
 * @author csy
 * Date: 2019/4/29
 * Description: 库存调整单 审核request
 */
@Data
public class SgPhyAdjustBillAuditRequest implements Serializable {
    /**
     * 原单据类型
     */
    @JSONField(name = "SOURCE_BILL_TYPE")
    private Integer sourceBillType;

    /**
     * 原单据id
     */
    @JSONField(name = "SOURCE_BILL_ID")
    private Long sourceBillId;

    /**
     * 用户
     */
    private User user;

    /**
     * 录入明细redisKey
     */
    @JSONField(name = "REDIS_KEY")
    private String redisKey;
}
