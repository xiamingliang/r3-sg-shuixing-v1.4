package com.jackrain.nea.sg.in.model.result;

import com.jackrain.nea.sg.in.model.table.SgBPhyInNotices;
import com.jackrain.nea.sg.in.model.table.SgBPhyInNoticesItem;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Author: ChenChen
 * @Description: 入库通知单查询结果返回实体
 * @Date: Create in 20:15 2019/5/7
 */
@Data
public class SgBPhyInNoticesResult implements Serializable {
    private static final long serialVersionUID = 4056086506571255286L;
    SgBPhyInNotices notices;
    List<SgBPhyInNoticesItem> itemList;
}
