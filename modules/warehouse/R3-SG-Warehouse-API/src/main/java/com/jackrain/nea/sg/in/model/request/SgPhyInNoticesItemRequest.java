package com.jackrain.nea.sg.in.model.request;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Author: ChenChen
 * @Description:
 * @Date: Create in 16:54 2019/4/23
 */
@Data
public class SgPhyInNoticesItemRequest implements Serializable {

    private static final long serialVersionUID = 900697160161533416L;
    /**
     * 来源单据明细id
     */
    private Long sourceBillItemId;


    /**
     * 条码ID
     */
    private Long psCSkuId;

    /**
     * 条码code
     */
    private String psCSkuEcode;


    /**
     * 商品id
     */
    private Long psCProId;

    /**
     * 商品code
     */
    private String psCProEcode;

    /**
     * 商品名称
     */
    private String psCProEname;


    @JSONField(name = "ID")
    private Long id;


    @JSONField(name = "SG_B_PHY_IN_NOTICES_ID")
    private Long sgBPhyInNoticesId;


    /**
     * 规格1Id
     */
    private Long psCSpec1Id;

    /**
     * 规格1Code
     */
    private String psCSpec1Ecode;

    /**
     * 规格1名称
     */
    private String psCSpec1Ename;

    /**
     * 规格2Id
     */
    private Long psCSpec2Id;

    /**
     * 规格2Code
     */
    private String psCSpec2Ecode;

    /**
     * 规格2名称
     */
    private String psCSpec2Ename;
    /**
     * 吊牌价
     */
    private BigDecimal priceList;

    /**
     * 成交价（非必传）
     */
    private BigDecimal priceCost;

    /**
     * 通知数量
     */
    private BigDecimal qty;

    /**
     * 入库数量（创建入库通知单不用传）
     */
    BigDecimal qtyIn;

    /**
     * 国标码
     */
    private String gbcode;
}
