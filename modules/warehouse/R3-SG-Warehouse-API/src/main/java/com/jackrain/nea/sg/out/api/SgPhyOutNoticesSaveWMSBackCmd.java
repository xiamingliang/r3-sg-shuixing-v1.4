package com.jackrain.nea.sg.out.api;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.out.model.request.SgPhyOutNoticesBillWMSBackRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;

import java.util.List;

/**
 * @author leexh
 * @since 2019/5/6 15:47
 */
public interface SgPhyOutNoticesSaveWMSBackCmd {

    ValueHolderV14 saveWMSBackOutNotices(List<SgPhyOutNoticesBillWMSBackRequest> requests, User user) throws NDSException;
}
