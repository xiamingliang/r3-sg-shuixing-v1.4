package com.jackrain.nea.sg.inv.model.table;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.*;
import com.jackrain.nea.es.annotations.Document;
import com.jackrain.nea.es.annotations.Field;
import com.jackrain.nea.es.constans.FieldType;
import com.jackrain.nea.sys.domain.BaseModelES;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@TableName(value = "sc_b_inventory")
@Data
@Document(index = "sc_b_inventory",type = "sc_b_inventory")
public class SgBInventory extends BaseModelES {
    @JSONField(name = "ID")
    @TableField(fill = FieldFill.INSERT)
    @TableId(value = "id", type = IdType.INPUT)
    @Field(type = FieldType.Long)
    private Long id;

    @JSONField(name = "BILL_NO")
    @Field(type = FieldType.Keyword)
    private String billNo;

    @JSONField(name = "BILL_DATE")
    @Field(type = FieldType.Long)
    private Date billDate;

    @JSONField(name = "CP_C_STORE_ID")
    @Field(type = FieldType.Long)
    private Long cpCStoreId;

    @JSONField(name = "CP_C_STORE_ECODE")
    @Field(type = FieldType.Keyword)
    private String cpCStoreEcode;

    @JSONField(name = "CP_C_STORE_ENAME")
    @Field(type = FieldType.Keyword)
    private String cpCStoreEname;

    @JSONField(name = "INVENTORY_TYPE")
    @Field(type = FieldType.Keyword)
    private String inventoryType;

    @JSONField(name = "INVENTORY_DATE")
    @Field(type = FieldType.Long)
    private Date inventoryDate;

    @JSONField(name = "POL_STATUS")
    @Field(type = FieldType.Integer)
    private Integer polStatus;

    @JSONField(name = "TOT_QTY_INVENTORY")
    @Field(type = FieldType.Double)
    private BigDecimal totQtyInventory;

    @JSONField(name = "TOT_AMT_LIST")
    @Field(type = FieldType.Double)
    private BigDecimal totAmtList;

    @JSONField(name = "REMARK")
    @Field(type = FieldType.Keyword)
    private String remark;

    @JSONField(name = "POLER_ID")
    @Field(type = FieldType.Long)
    private Long polerId;

    @JSONField(name = "POLER_ENAME")
    @Field(type = FieldType.Keyword)
    private String polerEname;

    @JSONField(name = "POLER_NAME")
    @Field(type = FieldType.Keyword)
    private String polerName;

    @JSONField(name = "POLER_TIME")
    @Field(type = FieldType.Long)
    private Date polerTime;

    @JSONField(name = "DELER_ID")
    @Field(type = FieldType.Long)
    private Long delerId;

    @JSONField(name = "DELER_ENAME")
    @Field(type = FieldType.Keyword)
    private String delerEname;

    @JSONField(name = "DELER_NAME")
    @Field(type = FieldType.Keyword)
    private String delerName;

    @JSONField(name = "DEL_TIME")
    @Field(type = FieldType.Long)
    private Date delTime;

    @JSONField(name = "VERSION")
    @Field(type = FieldType.Long)
    private Long version;

    @JSONField(name = "OWNERENAME")
    @Field(type = FieldType.Keyword)
    private String ownerename;

    @JSONField(name = "MODIFIERENAME")
    @Field(type = FieldType.Keyword)
    private String modifierename;
}