package com.jackrain.nea.sg.in.model.request;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author 周琳胜
 * @since 2019-10-23
 * create at : 2019-10-23 19:43
 */
@Data
public class SgPhyInNoticesQueryRequest implements Serializable {
    //入库通知单id
    private Long phyInNoticesId;

    //单据类型
    private Integer sourceBillType;

    //单据状态
    private List<Integer> billStatusList;

    //单据编号
    private String billNo;

    //开始时间
    private Date startTime;

    //开始时间
    private Date endTime;

    //实体仓id
    private Long cpCPhyWarehouseId;

    //页数
    private int pageNum;

    //每页数量
    private int pageSize;

    public SgPhyInNoticesQueryRequest(){
        this.pageNum = 1;
        this.pageSize = 20;
    }
}
