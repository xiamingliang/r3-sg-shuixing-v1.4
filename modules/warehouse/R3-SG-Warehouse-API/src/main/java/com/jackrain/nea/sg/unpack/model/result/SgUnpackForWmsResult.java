package com.jackrain.nea.sg.unpack.model.result;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author 周琳胜
 * @since 2019-10-23
 * create at : 2019-10-23 19:43
 */
@Data
public class SgUnpackForWmsResult implements Serializable {

    //单据编号
    private String ecode;

    //单据日期
    private Date billDate;

    //实体仓编码
    private String cpCPhyWarehouseEcode;

    // 箱号
    private List<String> teusEcodeList;


}
