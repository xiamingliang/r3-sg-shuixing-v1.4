package com.jackrain.nea.sg.in.api;

import com.jackrain.nea.sys.Command;

/**
 * @Author: ChenChen
 * @Description:入库通知单删除
 * @Date: Create in 10:14 2019/4/24
 */
public interface SgPhyInNoticesDeleteCmd extends Command {
}
