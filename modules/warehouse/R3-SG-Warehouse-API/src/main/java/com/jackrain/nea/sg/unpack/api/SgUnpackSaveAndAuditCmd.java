package com.jackrain.nea.sg.unpack.api;

import com.jackrain.nea.sg.unpack.model.request.SgUnpackSaveAndAuditRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 * @author: 周琳胜
 * @since: 2019/11/26
 * create at : 2019/11/26 21:28
 */
public interface SgUnpackSaveAndAuditCmd {

    ValueHolderV14 saveAndAuditSgUnpack(SgUnpackSaveAndAuditRequest request);
}
