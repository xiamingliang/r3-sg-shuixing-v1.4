package com.jackrain.nea.sg.inv.model.result;

import com.alibaba.fastjson.annotation.JSONField;
import com.jackrain.nea.sg.basic.model.table.SgBAdjustProp;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author 舒威
 * @since 2019/12/4
 * create at : 2019/12/4 19:32
 */
@Data
public class ScInvProfitLossGenarateQueryResult implements Serializable {

    private String totalMsg;

    private BigDecimal qtyProfit;

    private BigDecimal qtyLoss;

    private BigDecimal teusQtyProfit;

    private BigDecimal teusQtyLoss;

    private List<SgBAdjustProp> adjustProps;

    private List<Integer> inventoryProps;
}
