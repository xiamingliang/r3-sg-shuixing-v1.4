package com.jackrain.nea.sg.out.model.result;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author: 舒威
 * @since: 2019/8/29
 * create at : 2019/8/29 19:44
 */
@Data
public class SgOutResultToJITXItem implements Serializable {

    private Long sourceBillItemId;

    private Long psCSpec1Id;

    private String psCProEname;

    private String psCSpec1Ecode;

    private String psCSpec1Ename;

    private Long psCSpec2Id;

    private String psCSpec2Ecode;

    private String psCSpec2Ename;

    private BigDecimal priceList;

    private BigDecimal priceCost;

    private BigDecimal noticesQty;
}
