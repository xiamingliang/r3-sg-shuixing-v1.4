package com.jackrain.nea.sg.inv.api;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.inv.model.request.ScInvProfitLossGenarateRequest;
import com.jackrain.nea.sg.inv.model.request.ScInvProfitLossMarginQueryCmdRequest;
import com.jackrain.nea.sg.inv.model.request.ScInvProfitLossQueryCmdRequest;
import com.jackrain.nea.sg.inv.model.result.ScInvProfitLossGenarateQueryResult;
import com.jackrain.nea.sg.inv.model.result.ScInvProfitLossQueryItemResult;
import com.jackrain.nea.sg.inv.model.result.SgInvProfitLossQueryTotalResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 * @author: 舒威
 * @since: 2019/12/4
 * create at : 2019/12/4 17:26
 */
public interface ScInvProfitLossGenarateCmd {

    /**
     * 查询生成盈亏按钮渲染信息
     */
    ValueHolderV14<ScInvProfitLossGenarateQueryResult> queryProfitLossGenarateInfo(ScInvProfitLossMarginQueryCmdRequest model) throws NDSException;


    /**
     * 生成盈亏
     */
    ValueHolderV14 genarateProfitLoss(ScInvProfitLossMarginQueryCmdRequest model) throws NDSException;

    /**
     * 查询盈亏明细合计
     */
    ValueHolderV14<ScInvProfitLossGenarateQueryResult> queryProfitLossTotalInventory(ScInvProfitLossMarginQueryCmdRequest model);
}
