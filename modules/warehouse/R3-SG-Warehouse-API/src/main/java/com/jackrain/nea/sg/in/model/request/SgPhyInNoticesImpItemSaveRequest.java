package com.jackrain.nea.sg.in.model.request;

import com.jackrain.nea.sg.in.model.table.SgBPhyInNoticesImpItem;
import lombok.Data;

import java.io.Serializable;

/**
 * @author zhoulinsheng
 * @since 2019-10-22
 * create at : 2019-10-22 13:47
 */
@Data
public class SgPhyInNoticesImpItemSaveRequest extends SgBPhyInNoticesImpItem implements Serializable {


}
