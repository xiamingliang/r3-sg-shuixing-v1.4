package com.jackrain.nea.sg.out.model.request;

import com.alibaba.fastjson.JSONArray;
import com.jackrain.nea.web.face.User;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author leexh
 * @since 2019/4/25 14:37
 */
@Data
public class SgPhyOutResultBillAuditRequest implements Serializable {

    /**
     * 出库结果单ID集合
     */
    private JSONArray ids;

    /**
     * 出库通知单对象集合
     */
    private List<SgPhyOutResultSaveRequest> outResultRequests;

    /**
     * 用户信息
     */
    private User loginUser;
}
