package com.jackrain.nea.sg.out.model.request;

import com.alibaba.fastjson.JSONArray;
import com.jackrain.nea.web.face.User;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author 舒威
 * @since 2019/12/11
 * create at : 2019/12/11 11:43
 */
@Data
public class SgPhyOutNoticesPosRequest implements Serializable {
    /**
     * 出库通知单ID集合
     */
    private JSONArray ids;


    /**
     * 用户信息
     */
    private User loginUser;
}
