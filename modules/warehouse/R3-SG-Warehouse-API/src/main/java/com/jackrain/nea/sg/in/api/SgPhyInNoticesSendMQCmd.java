package com.jackrain.nea.sg.in.api;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sys.domain.ValueHolderV14;

import java.util.List;

public interface SgPhyInNoticesSendMQCmd {
    ValueHolderV14  saveSgBPhyInResultByMq(List<Long> billIds) throws NDSException;;
}
