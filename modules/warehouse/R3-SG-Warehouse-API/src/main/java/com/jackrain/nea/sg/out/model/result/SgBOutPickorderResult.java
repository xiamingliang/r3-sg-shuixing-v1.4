package com.jackrain.nea.sg.out.model.result;

import com.jackrain.nea.sg.out.model.table.*;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author: heliu
 * @since: 2019/11/18
 * create at : 2019/11/18 12:41
 */
@Data
public class SgBOutPickorderResult implements Serializable {

    private static final long serialVersionUID = -3755700430431708016L;

    /**
     * 出库拣货单
     */
    SgBOutPickorder sgBOutPickorder;

    /**
     * 拣货明细
     */
    List<SgBOutPickorderItem> sgBOutPickorderItemList;

    /**
     * 装箱明细
     */
    List<SgBOutPickorderTeusItem> sgBOutPickorderTeusItemList;

    /**
     * 来源出库通知单
     */
    List<SgBOutPickorderNoticeItem> sgBOutPickorderNoticeItemList;

    /**
     * 出库结果单
     */
    List<SgBOutPickorderResultItem> sgBOutPickorderResultItemList;
}