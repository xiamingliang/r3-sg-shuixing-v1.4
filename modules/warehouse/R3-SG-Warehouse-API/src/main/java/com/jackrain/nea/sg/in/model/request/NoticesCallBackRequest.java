package com.jackrain.nea.sg.in.model.request;

import com.jackrain.nea.sys.domain.BaseModel;
import lombok.Data;

@Data
public class NoticesCallBackRequest extends BaseModel {

    /**
     * 返回状态码
     */
    private Integer code;
    /**
     * 返回信息
     */
    private String message;
    /**
     * 入库通知单单据编号
     */
    private String orderNo;

    /**
     * WMS单据编号
     */
    private String entryOrderId;
}
