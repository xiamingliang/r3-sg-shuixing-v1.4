package com.jackrain.nea.sg.unpack.api;

import com.jackrain.nea.sg.unpack.model.request.SgUnpackForWmsRequest;
import com.jackrain.nea.sg.unpack.model.result.SgUnpackForWmsResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;

import java.util.List;

/**
 * @author: 周琳胜
 * @since: 2019/11/26
 * create at : 2019/11/26 21:28
 */
public interface SgUnpackQueryForWmsCmd {

    ValueHolderV14<List<SgUnpackForWmsResult>> querySgUnpackForWms(SgUnpackForWmsRequest request);
}
