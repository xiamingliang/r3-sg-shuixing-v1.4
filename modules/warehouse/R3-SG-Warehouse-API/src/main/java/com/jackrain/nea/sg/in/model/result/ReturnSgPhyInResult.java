package com.jackrain.nea.sg.in.model.result;


import lombok.Data;

import java.io.Serializable;


@Data
public class ReturnSgPhyInResult implements Serializable {

    private Long objid;

    private String tablename;

    private Integer code;

    private String message;
}
