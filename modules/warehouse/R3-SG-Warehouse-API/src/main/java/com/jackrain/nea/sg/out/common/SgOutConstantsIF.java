package com.jackrain.nea.sg.out.common;

/**
 * @author leexh
 * @since 2019/4/23 17:22
 */
public interface SgOutConstantsIF {

    /**
     * 返回码
     * 1=已传WMS
     * 2=不需传WMS
     * 3=传WMS失败次数>5、面单信息为空、新增出库通知单失败
     * 4=新增出库通知单失败,记录到订单-备注,下次定时任务还会拉取
     * 9=获取电子面单失败
     */
    int RESULT_PASS_WMS = 1;
    int RESULT_NOT_WMS = 2;
    int RESULT_WMS_FAIL = 3;
    int RESULT_REMARK_FAIL = 4;
    int RESULT_FACE_FAIL = 9;

    /**
     * 出库类型
     */
    int OUT_TYPE_ELECTRICITY = 1;   // 电商出库
    int OUT_TYPE_BIG_GOODS = 2;     // 大货出库
    int OUT_TYPE_RETAIL = 3;  //零售出库

    /**
     * 出库通知单-单据状态
     */
    int OUT_NOTICES_STATUS_WAIT = 1;    // 待出库
    int OUT_NOTICES_STATUS_PART = 2;    // 部分出库
    int OUT_NOTICES_STATUS_ALL = 3;     // 全部出库
    int OUT_NOTICES_STATUS_VOID = 4;    // 已作废

    /**
     * 出库结果单-单据状态
     */
    int OUT_RESULT_STATUS_WAIT = 1;    // 待审核;
    int OUT_RESULT_STATUS_AUDIT = 2;   // 已审核;
    int OUT_RESULT_STATUS_VOID = 3;    // 已作废;

    /**
     * 返回出库单信息
     */
    int RETURN_OUT_NOTICES = 1; // 通知单
    int RETURN_OUT_RESULT = 2;  // 结果单
    int RETURN_OUT_ALL = 3;     // 通知单+结果单

    /**
     * 传WMS状态
     */
    long WMS_STATUS_WAIT_PASS = 0;       // 未传WMS
    long WMS_STATUS_PASSING = 1;         // 传WMS中
    long WMS_STATUS_PASS_SUCCESS = 2;    // 传WMS成功
    long WMS_STATUS_PASS_FAILED = 3;     // 传WMS失败

    /**
     * 是否是最后一次出库（和WMS保持一致）
     */
    int OUT_IS_LAST_Y = 0;  // 是最后一次出库
    int OUT_IS_LAST_N = 1;  // 不是最后一次出库

    /**
     * 出库通知单新增，明细集合key
     */
    String RETURN_OUT_NOTICES_ITEM_KEY = "RETURN_OUT_NOTICES_ITEM_KEY";

    /**
     * 出库通知单 调用 电子面单服务 mq
     */
    String SGB_CHANNEL_SG_OUT_NOTICES_FACE_TAGS = "sg_to_out_notices_face";

    /**
     * 出库通知单作废调用 京东快递取消服务 mq
     */
    String SG_OUT_NOTICES_JD_CANCEL_TAGS = "sg_to_out_notices_jd_cancel";

    /**
     * 电子面单类型
     */
    Integer EWAYBILL_TYPE_STRAIGHT = 1;    // 直连
    Integer EWAYBILL_TYPE_ROOKIE = 2;    // 菜鸟
    Integer EWAYBILL_TYPE_JDQL = 3;     // 京东青龙
    Integer EWAYBILL_TYPE_JDWJ = 4;    // 京东无界

    /**
     * 物流公司编码
     */
    String LOGISTICS_JD = "JD";//京东

    /**
     * 是否获取电子面单
     */
    int IS_ENABLEEWAYBILL_Y = 1;//是
    int IS_ENABLEEWAYBILL_N = 0;//否

    /**
     * 电子面单获取状态
     */

    int EWAYBILL_STATUS_SECTION_SUC = 2;//部分成功(只有快递单号，没有大头笔信息)
    int EWAYBILL_STATUS_SUCCESS = 1;//成功
    int EWAYBILL_STATUS_FAIL = 0;//失败

    /**
     * 电子面单调用次数
     */
    int FACE_EXEC_BYNBER = 5;
    /**
     * 电子面单查询数据的时间间隔
     */
    int FACE_EXEC_MINUTE = 5;

    /**
     * 销售平台： 京东商城 0010001；天猫商城 0010002，苏宁易购0010003，亚马逊中国 0010004，其他平台0030001
     */
    String SALEPLAT_JINGDONG = "0010001";
    String SALEPLAT_TAOBAO = "0010002";
    String SALEPLAT_SUNING = "0010003";
    String SALEPLAT_AMAZON = "0010004";
    String SALEPLAT_OTHER = "0030001";

    /**
     * 平台
     */
    String PLATFORMTYPE_TAOBAO = "2";
    String PLATFORMTYPE_TAOBAO_DISTRIBUTION = "3";
    String PLATFORMTYPE_JINGDONG = "4";
    String PLATFORMTYPE_SUNING = "12";
    String PLATFORMTYPE_AMAZON = "10";
    String PLATFORMTYPE_PINDUODUO = "27";
    String PLATFORMTYPE_TAOBAO_JINGXIAO = "77";
    String PLATFORMTYPE_TAOBAO_FENXIAO = "3";
    String PLATFORMTYPE_WUXIANGYUN = "37";


    /**
     * 出库性质
     * 1 正品出库
     * 2 次品出库
     */
    Long OUT_PROP_ZP = 1L;
    Long OUT_PROP_CP = 2L;

    /**
     * 是否差异处理
     * 1 = 是
     * 0 = 否
     */
    Integer IS_DIFF_DEAL_Y = 1;
    Integer IS_DIFF_DEAL_N = 0;

    String SGPHYOUTRESULT_TEMPLATE_UPLOAD_PATH = "sg.rpt.storage_export_upload_base";
    String ENDPOINT_KEY = "r3.oss.endpoint";
    String ACCESS_KEY = "r3.oss.accessKey";
    String SECRET_KEY = "r3.oss.secretKey";
    String BUCKET_NAME = "r3.oss.bucketName";
    String TIMEOUT = "r3.oss.timeout";

    /**
     * 拣货单状态
     */
    int PICK_ORDER_STATUS_WAIT = 1;
    int PICK_ORDER_STATUS_AUDIT = 2;
    int PICK_ORDER_STATUS_VOID = 3;

    int IS_PASS_WMS_Y = 1;
    int IS_PASS_WMS_N = 0;

    int PICK_STATUS_WAIT = 1;
    int PICK_STATUS_ING = 2;
    int PICK_STATUS_SUCCESS = 3;
}