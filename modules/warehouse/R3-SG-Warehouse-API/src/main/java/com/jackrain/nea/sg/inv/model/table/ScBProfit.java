package com.jackrain.nea.sg.inv.model.table;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.jackrain.nea.es.annotations.Document;
import com.jackrain.nea.es.annotations.Field;
import com.jackrain.nea.es.constans.FieldType;
import java.math.BigDecimal;
import java.util.Date;

import com.jackrain.nea.sys.domain.BaseModelES;
import lombok.Data;

@TableName(value = "sc_b_profit")
@Data
@Document(index = "sc_b_profit",type = "sc_b_profit")
public class ScBProfit extends BaseModelES {
    @JSONField(name = "ID")
    @TableField(fill = FieldFill.INSERT)
    @TableId(value = "id", type = IdType.INPUT)
    @Field(type = FieldType.Long)
    private Long id;

    @JSONField(name = "BILL_NO")
    @Field(type = FieldType.Keyword)
    private String billNo;

    @JSONField(name = "BILL_DATE")
    @Field(type = FieldType.Date)
    private Date billDate;

    @JSONField(name = "CP_C_STORE_ID")
    @Field(type = FieldType.Long)
    private Long cpCStoreId;

    @JSONField(name = "CP_C_STORE_ECODE")
    @Field(type = FieldType.Keyword)
    private String cpCStoreEcode;

    @JSONField(name = "CP_C_STORE_ENAME")
    @Field(type = FieldType.Keyword)
    private String cpCStoreEname;

    @JSONField(name = "CP_C_PHY_WAREHOUSE_ID")
    @Field(type = FieldType.Long)
    private Long cpCPhyWarehouseId;

    @JSONField(name = "CP_C_PHY_WAREHOUSE_ECODE")
    @Field(type = FieldType.Keyword)
    private String cpCPhyWarehouseEcode;

    @JSONField(name = "CP_C_PHY_WAREHOUSE_ENAME")
    @Field(type = FieldType.Keyword)
    private String cpCPhyWarehouseEname;

    @JSONField(name = "TOT_QTY_TEUS_DIFF")
    @Field(type = FieldType.Double)
    private BigDecimal totQtyTeusDiff;

    @JSONField(name = "TOT_QTY_DIFF")
    @Field(type = FieldType.Double)
    private BigDecimal totQtyDiff;

    @JSONField(name = "STATUS")
    @Field(type = FieldType.Integer)
    private Integer status;

    @JSONField(name = "STATUS_ID")
    @Field(type = FieldType.Long)
    private Long statusId;

    @JSONField(name = "STATUS_ENAME")
    @Field(type = FieldType.Keyword)
    private String statusEname;

    @JSONField(name = "STATUS_NAME")
    @Field(type = FieldType.Keyword)
    private String statusName;

    @JSONField(name = "STATUS_TIME")
    @Field(type = FieldType.Date)
    private Date statusTime;

    @JSONField(name = "REMARK")
    @Field(type = FieldType.Keyword)
    private String remark;

    @JSONField(name = "VERSION")
    @Field(type = FieldType.Long)
    private Long version;

    @JSONField(name = "OWNERENAME")
    @Field(type = FieldType.Keyword)
    private String ownerename;

    @JSONField(name = "MODIFIERENAME")
    @Field(type = FieldType.Keyword)
    private String modifierename;
}