package com.jackrain.nea.sg.in.model.request;

import com.alibaba.fastjson.JSONArray;
import com.jackrain.nea.web.face.User;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class SgPhyInResultBillAuditRequest implements Serializable {

    /**
     * 入库结果单主表ID
     */
    private JSONArray ids;


    /**
     * 入库结果单
     */
    private SgBPhyInResultSaveRequest sgBPhyInResultSaveRequest;

    /**
     * 入库结果单明细
     */
    private List<SgBPhyInResultItemSaveRequest> itemList;


    /**
     * 用户信息
     */
    private User loginUser;


    /**
     * 是否一键入库
     */
    private Boolean isOneClickLibrary;


    private Long pickOrderId;


}
