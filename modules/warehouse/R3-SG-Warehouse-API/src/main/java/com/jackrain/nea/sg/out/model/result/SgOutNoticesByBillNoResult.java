package com.jackrain.nea.sg.out.model.result;

import com.jackrain.nea.sg.out.model.table.SgBPhyOutNotices;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutNoticesImpItem;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutNoticesItem;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutNoticesTeusItem;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author leexh
 * @since 2019/5/7 14:19
 */
@Data
public class SgOutNoticesByBillNoResult implements Serializable {

    /**
     * 出库通知单
     */
    private SgBPhyOutNotices outNotices;

    /**
     * 出库通知单明细
     */
    private List<SgBPhyOutNoticesItem> items;

    /**
     * 录入明细
     */
    private List<SgBPhyOutNoticesImpItem> impItems;
    /**出库通知单箱明细
     *
     */
    private List<SgBPhyOutNoticesTeusItem> sgBPhyOutNoticesTeusItemList;
}
