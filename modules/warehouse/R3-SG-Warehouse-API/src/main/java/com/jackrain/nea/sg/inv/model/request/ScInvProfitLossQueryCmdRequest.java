package com.jackrain.nea.sg.inv.model.request;

import com.jackrain.nea.web.face.User;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author: 舒威
 * @since: 2019/4/3
 * create at : 2019/4/3 11:33
 */
@Data
public class ScInvProfitLossQueryCmdRequest implements Serializable{

    private Long storeId;

    private Long warehouseId;

    private Integer inventoryType; //盘点类型

    private Date inventoryDate; //盘点日期

    private Boolean isCreateProf;  //生成盈亏标志

    private Boolean isExport; //导出标志

    private Boolean isQueryItems; //是否查询盈亏明细标志

    private Boolean isQueryTeus; //是否查询盈亏明细(箱)标志

    private String orderByName; //排序字段 默认创建日期

    private Boolean orderByAsc; //排序方式 默认降序

    private Integer range; //每页展示条数

    private Integer currentPage; //当前页

    private User loginUser;
}
