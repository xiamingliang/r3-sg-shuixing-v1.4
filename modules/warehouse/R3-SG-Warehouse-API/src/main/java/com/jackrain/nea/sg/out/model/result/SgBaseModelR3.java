package com.jackrain.nea.sg.out.model.result;

import lombok.Data;

import java.io.Serializable;

/**
 * @author leexh
 * @since 2019/4/22 19:44
 */
@Data
public class SgBaseModelR3 implements Serializable {

    private Long id;

    private Integer code;

    private String message;
}
