package com.jackrain.nea.sg.in.api;

import com.jackrain.nea.sg.in.model.request.SgPhyInNoticesQueryRequest;
import com.jackrain.nea.sg.in.model.table.SgBPhyInNotices;
import com.jackrain.nea.sys.domain.ValueHolderV14;

import java.util.List;

/**
 * @author 周琳胜
 * @since 2019-10-23
 * create at : 2019-10-23 19:39
 */
public interface SgPhyInNoticesByConditionQueryCmd {
    ValueHolderV14<List<SgBPhyInNotices>> queryInNoticesItem(SgPhyInNoticesQueryRequest request);
}
