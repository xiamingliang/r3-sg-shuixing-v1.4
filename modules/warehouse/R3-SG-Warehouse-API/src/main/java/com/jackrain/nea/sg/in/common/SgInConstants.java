package com.jackrain.nea.sg.in.common;

/**
 * @Author: ChenChen
 * @Description:
 * @Date: Create in 16:28 2019/4/24
 */
public interface SgInConstants {

    /**
     * 配送方式
     */
    int DELIVERY_METHOD_LAND = 1;//路运
    int DELIVERY_METHOD_AIR = 1;//空运

    int IN_IS_LAST_Y = 0;  // 是最后一次入库
    int IN_IS_LAST_N = 1;  // 不是最后一次入库

    /**
     * 入库类型
     */
    int IN_TYPE_E_COMMERCE = 1;//电商入库
    int IN_TYPE_BIG_CARGO = 2;//大货入库

    //审核类型
    int BILL_STATUS_UNCHECKED = 1;//未审核
    int BILL_STATUS_CHECKED = 2;//已审核
    int BILL_STATUS_VOID = 3;//已作废

    int NOTICE_IS_PASS_WMS_YES = 1;//是
    int NOTICE_IS_PASS_WMS_NO = 0;//否






    int SG_PHY_IN_RESULT_CREATE_TYPE_MQ=1;//mq创建入库结果单

    int SG_PHY_IN_RESULT_CREATE_TYPE_WMS=2;//WMS创建入库结果单

    /**
     * =============================================================================
     * 入库结果单审核MQ调用
     */

    //目前配置统一
    String SG_PHY_RESULT_IN_MQ_CONFINGNAME = "default";

    //零售退货单TOPIC
    String SG_PHY_RESULT_IN_MQ_TOPIC = "BJ_DEV_R3_SG_CALLBACK";

    //零售退货单TAG
    String SG_PHY_RESULT_IN_MQ_TAG = "sg_to_oms_in_result_verify_postback";

    /**
     * 入库通知发MQ生成入库结果单TOPIC
     */
    String SG_PHY_IN_NOTICES_TO_RESULT_MQ_TAG = "IN_NOTICES_TO_IN_RESULT";

    //topic(四个统一)
    String  SG_PHY_IN_RESULT_ADUIT_TOPIC="BJ_DEV_R3_SG_CALLBACK";

    //采购单TAG
    String SG_PHY_IN_RESULT_ADUIT_TAG_PURCHASE="sg_to_oc_purchase_in_verify_postback";

    //销售单TAG
    String SG_PHY_IN_RESULT_ADUIT_TAG_SALE="sg_to_oc_sale_in_verify_postback";

    //销售退货单TAG
    String SG_PHY_IN_RESULT_ADUIT_TAG_SALE_REF="sg_to_oc_ref_sale_in_verify_postback";

    //调拨单TAG
    String SG_PHY_IN_RESULT_ADUIT_TAG_TRANSFER="sg_to_sg_transfer_in_verify_postback";

    String R3_OMS_SG_OUT_MQ_TOPIC_KEY="r3.oms.sg.out.mq.topic";
    /**
     * ===========================================================================
     */


    /**
     * 入库结果单单据号
     */
    String SEQ_SG_B_PHY_IN_RESULT="SEQ_SG_B_PHY_IN_RESULT";

    /**
     * 入库通知明细表 主表id字段
     */
    String SG_B_PHY_IN_NOTICES_ID="sg_b_phy_in_notices_id";

    /**
     * 入库结果明细表 主表id字段
     */
    String SG_B_PHY_IN_RESULT_ID="sg_b_phy_in_result_id";
}
