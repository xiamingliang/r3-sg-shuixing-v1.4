package com.jackrain.nea.sg.in.model.request;

import com.jackrain.nea.sg.basic.model.request.SgR3BaseRequest;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author zhoulinsheng
 * @since 2019-10-29
 * create at : 2019-10-29 14:31
 */
@Data
public class SgPhyInCommitForPDARequest extends SgR3BaseRequest implements Serializable {

    private Long sgBPhyInNoticesId;

    private Integer isLast;

    private List<SgPhyInCommitForPDADetailRequest> detailRequests;
}
