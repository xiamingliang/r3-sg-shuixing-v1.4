package com.jackrain.nea.sg.out.model.request;

import com.jackrain.nea.web.face.User;
import lombok.Data;

import java.io.Serializable;

/**
 * @author zhu lin yu
 * @since 2019-11-20
 * create at : 2019-11-20 16:47
 */
@Data
public class SgPhyOutPickOrderAuditRequest implements Serializable {
    private Long outPickOrderId;

    private User loginUser;
}
