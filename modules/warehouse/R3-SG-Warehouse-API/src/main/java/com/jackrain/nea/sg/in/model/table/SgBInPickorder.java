package com.jackrain.nea.sg.in.model.table;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.*;
import com.jackrain.nea.es.annotations.Document;
import com.jackrain.nea.es.annotations.Field;
import com.jackrain.nea.es.constans.FieldType;
import com.jackrain.nea.sys.domain.BaseModelES;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@TableName(value = "sg_b_in_pickorder")
@Data
@Document(index = "sg_b_in_pickorder", type = "sg_b_in_pickorder")
public class SgBInPickorder extends BaseModelES {
    @JSONField(name = "ID")
    @TableField(fill = FieldFill.INSERT)
    @TableId(value = "id", type = IdType.INPUT)
    @Field(type = FieldType.Long)
    private Long id;

    @JSONField(name = "BILL_NO")
    @Field(type = FieldType.Keyword)
    private String billNo;

    @JSONField(name = "BILL_DATE")
    @Field(type = FieldType.Long)
    private Date billDate;

    @JSONField(name = "CP_C_ORIG_ID")
    @Field(type = FieldType.Long)
    private Long cpCOrigId;

    @JSONField(name = "CP_C_ORIG_ECODE")
    @Field(type = FieldType.Keyword)
    private String cpCOrigEcode;

    @JSONField(name = "CP_C_ORIG_ENAME")
    @Field(type = FieldType.Keyword)
    private String cpCOrigEname;

    @JSONField(name = "CP_C_DEST_ID")
    @Field(type = FieldType.Long)
    private Long cpCDestId;

    @JSONField(name = "CP_C_DEST_ECODE")
    @Field(type = FieldType.Keyword)
    private String cpCDestEcode;

    @JSONField(name = "CP_C_DEST_ENAME")
    @Field(type = FieldType.Keyword)
    private String cpCDestEname;

    @JSONField(name = "BILL_STATUS")
    @Field(type = FieldType.Integer)
    private Integer billStatus;

    @JSONField(name = "DELER_ID")
    @Field(type = FieldType.Long)
    private Long delerId;

    @JSONField(name = "DELER_NAME")
    @Field(type = FieldType.Keyword)
    private String delerName;

    @JSONField(name = "DELER_ENAME")
    @Field(type = FieldType.Keyword)
    private String delerEname;

    @JSONField(name = "DEL_TIME")
    @Field(type = FieldType.Long)
    private Date delTime;

    @JSONField(name = "DEL_STATUS")
    @Field(type = FieldType.Integer)
    private Integer delStatus;

    @JSONField(name = "REMARK")
    @Field(type = FieldType.Keyword)
    private String remark;

    @JSONField(name = "VERSION")
    @Field(type = FieldType.Long)
    private Long version;

    @JSONField(name = "OWNERENAME")
    @Field(type = FieldType.Keyword)
    private String ownerename;

    @JSONField(name = "MODIFIERENAME")
    @Field(type = FieldType.Keyword)
    private String modifierename;

    @JSONField(name = "STATUS_ID")
    @Field(type = FieldType.Integer)
    private Integer statusId;

    @JSONField(name = "STATUS_ENAME")
    @Field(type = FieldType.Keyword)
    private String statusEname;

    @JSONField(name = "STATUS_NAME")
    @Field(type = FieldType.Keyword)
    private String statusName;

    @JSONField(name = "STATUS_TIME")
    @Field(type = FieldType.Long)
    private Date statusTime;

    @JSONField(name = "CHECK_NO")
    @Field(type = FieldType.Keyword)
    private String checkNo;

    @JSONField(name = "IN_SEQ_NO")
    @Field(type = FieldType.Keyword)
    private String inSeqNo;

    @JSONField(name = "QTY_IN_TEUS")
    @Field(type = FieldType.Double)
    private BigDecimal qtyInTeus;

    @JSONField(name = "QTY_IN_PACK")
    @Field(type = FieldType.Double)
    private BigDecimal qtyInPack;

    @JSONField(name = "QTY_IN_UNIT")
    @Field(type = FieldType.Double)
    private BigDecimal qtyInUnit;

    @JSONField(name = "CP_C_CUSTOMER_DEST_ID")
    @Field(type = FieldType.Long)
    private Long cpCCustomerDestId;

    @JSONField(name = "CP_C_CUSTOMER_DEST_ECODE")
    @Field(type = FieldType.Keyword)
    private String cpCCustomerDestEcode;

    @JSONField(name = "CP_C_CUSTOMER_DEST_ENAME")
    @Field(type = FieldType.Keyword)
    private String cpCCustomerDestEname;

    @JSONField(name = "IS_ALL_RECEV")
    @Field(type = FieldType.Keyword)
    private String isAllRecev;

    @JSONField(name = "CP_C_CUSTOMER_ORIG_ID")
    @Field(type = FieldType.Long)
    private Long cpCCustomerOrigId;

    @JSONField(name = "CP_C_CUSTOMER_ORIG_ECODE")
    @Field(type = FieldType.Keyword)
    private String cpCCustomerOrigEcode;

    @JSONField(name = "CP_C_CUSTOMER_ORIG_ENAME")
    @Field(type = FieldType.Keyword)
    private String cpCCustomerOrigEname;

    @JSONField(name = "CP_C_LOGISTICS_ID")
    @Field(type = FieldType.Long)
    private Long cpCLogisticsId;

    @JSONField(name = "ERP_CALLBACK_BILL_NO")
    @Field(type = FieldType.Keyword)
    private String erpCallbackBillNo;

    @JSONField(name = "STATUS")
    @Field(type = FieldType.Integer)
    private Integer status;

    @JSONField(name = "TRANS_STATUS")
    @Field(type = FieldType.Integer)
    private Integer transStatus;

    @JSONField(name = "FAIL_NUM")
    @Field(type = FieldType.Double)
    private BigDecimal failNum;

    @JSONField(name = "BELONGS_LEGAL")
    @Field(type = FieldType.Keyword)
    private String belongsLegal;

    @JSONField(name = "SOURCE_BILL_ID")
    @Field(type = FieldType.Long)
    private Long sourceBillId;

    @JSONField(name = "SOURCE_BILL_NO")
    @Field(type = FieldType.Keyword)
    private String sourceBillNo;

    @JSONField(name = "STOCK_BILL_NO")
    @Field(type = FieldType.Keyword)
    private String stockBillNo;

    @JSONField(name = "CP_C_ORIG_LEGAL")
    @Field(type = FieldType.Keyword)
    private String cpCOrigLegal;

    @JSONField(name = "TRANSFER_IN_BILL_NO")
    @Field(type = FieldType.Keyword)
    private String transferInBillNo;

    @JSONField(name = "SOURCE_BILL_PROP_ID")
    @Field(type = FieldType.Long)
    private Long sourceBillPropId;
}