package com.jackrain.nea.sg.inv.model.request;

import com.jackrain.nea.web.face.User;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author: 舒威
 * @since: 2019/4/9
 * create at : 2019/4/9 11:39
 */
@Data
public class SgInvProfitLossSaveAsyCmdRequest implements Serializable {

    private Long storeId;

    private String inventoryType; //盘点类型

    private Date inventoryDate; //盘点日期

    private Integer sumQtyProfit; //盘盈数量

    private Integer sumQtyLoss; //盘亏数量

    private Long adjTypeId;  //调整类型

    private Boolean isCreateProf; //生成盈亏标志

    private String orderByName; //排序字段 默认创建日期

    private Boolean orderByAsc; //排序方式 默认降序

    private Integer range; //每页展示条数

    private Integer currentPage; //当前页

    private User loginUser;

}
