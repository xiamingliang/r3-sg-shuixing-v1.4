package com.jackrain.nea.sg.out.api;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.basic.model.result.SgR3BaseResult;
import com.jackrain.nea.sg.out.model.request.SgPhyOutNoticesBillVoidRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 * @author leexh
 * @since 2019/4/23 15:05
 */
public interface SgPhyOutNoticesVoidCmd {
    ValueHolderV14<SgR3BaseResult> voidSgPhyOutNotices(SgPhyOutNoticesBillVoidRequest request) throws NDSException;
}
