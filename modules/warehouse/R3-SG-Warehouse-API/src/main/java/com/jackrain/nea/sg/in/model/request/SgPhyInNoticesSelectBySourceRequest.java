package com.jackrain.nea.sg.in.model.request;

import com.alibaba.fastjson.annotation.JSONField;
import com.jackrain.nea.web.face.User;
import lombok.Data;

import java.io.Serializable;

/**
 * @Author: ChenChen
 * @Description:
 * @Date: Create in 20:33 2019/5/16
 */
@Data
public class SgPhyInNoticesSelectBySourceRequest implements Serializable {
    private static final long serialVersionUID = 445753927416688232L;
    @JSONField(name = "USER")
    private User loginUser;
    /**
     * 来源单据类型
     */
    private Integer sourceBillType;

    /**
     * 来源单据id
     */
    private Long sourceBillId;
}
