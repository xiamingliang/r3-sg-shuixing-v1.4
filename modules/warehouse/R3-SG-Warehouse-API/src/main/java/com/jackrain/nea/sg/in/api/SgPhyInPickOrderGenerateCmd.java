package com.jackrain.nea.sg.in.api;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.in.model.request.SgPhyInPickUpGoodSaveRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.query.QuerySession;

/**
 * @author zhu lin yu
 * @since 2019-11-08
 * create at : 2019-11-08 09:24
 */
public interface SgPhyInPickOrderGenerateCmd {
    ValueHolderV14<Long> execute(QuerySession session) throws NDSException;

    ValueHolderV14<Long> invoke(SgPhyInPickUpGoodSaveRequest request) throws NDSException;
}
