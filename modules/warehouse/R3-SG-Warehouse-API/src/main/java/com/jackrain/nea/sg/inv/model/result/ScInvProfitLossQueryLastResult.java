package com.jackrain.nea.sg.inv.model.result;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.io.Serializable;

/**
 * @author 舒威
 * @since 2019/4/11
 * create at : 2019/4/11 10:07
 */
@Data
public class ScInvProfitLossQueryLastResult implements Serializable {

    @JSONField(name = "INVENTORY_TYPE")
    private Integer inventoryType;

    @JSONField(name = "INVENTORY_DATE")
    private String inventoryDate;
}
