package com.jackrain.nea.sg.in.model.request;


import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;

@Data
@ToString
public class SgBPhyInResultDefectItemSaveRequest {

    private Long id;

    private Long sgBPhyInResultId;

    private String sgBPhyInResultBillNo;

    private Long psCSkuId;

    private String psCSkuEcode;

    private Long psCProId;

    private String psCProEcode;

    private String psCProEname;

    private Long psCSpec1Id;

    private String psCSpec1Ecode;

    private String psCSpec1Ename;

    private Long psCSpec2Id;

    private String psCSpec2Ecode;

    private String psCSpec2Ename;

    private BigDecimal qtyIn;

    private BigDecimal priceList;

    private BigDecimal amtListIn;

    private Long sgBPhyInNoticesItemId;
    /**
     * 国标码
     */
    private String gbcode;
}