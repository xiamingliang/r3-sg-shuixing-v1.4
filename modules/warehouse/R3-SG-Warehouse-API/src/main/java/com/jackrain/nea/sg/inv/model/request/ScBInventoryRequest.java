package com.jackrain.nea.sg.inv.model.request;

import com.alibaba.fastjson.annotation.JSONField;
import com.jackrain.nea.sg.inv.model.table.ScBInventory;
import com.jackrain.nea.sg.inv.model.table.ScBInventoryImpItem;
import lombok.Data;

import java.util.List;

/**
 * @author: 周琳胜
 * @since: 2019/11/13
 * create at : 2019/11/13 20:14
 */
@Data
public class ScBInventoryRequest {
    @JSONField(name = "objid")
    private Long objid;

    @JSONField(name = "SC_B_INVENTORY")
    private ScBInventory scBInventory;

    @JSONField(name = "SC_B_INVENTORY_IMP_ITEM")
    private List<ScBInventoryImpItem> scBInventoryImpItems;
}
