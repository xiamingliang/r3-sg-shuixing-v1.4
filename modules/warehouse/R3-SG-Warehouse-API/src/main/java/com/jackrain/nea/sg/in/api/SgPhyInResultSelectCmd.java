package com.jackrain.nea.sg.in.api;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.in.model.request.SgBPhyInResultSaveRequest;
import com.jackrain.nea.sg.in.model.request.SgPhyInResultBillSaveRequest;
import com.jackrain.nea.sg.in.model.table.SgBPhyInResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;

import java.util.List;

/**
 * 根据来源单据id和来源单据类型获取入库结果单
 *
 * @author jiang.cj
 * @since 2019-05-20
 * create at : 2019-05-20
 */
public interface SgPhyInResultSelectCmd {


    ValueHolderV14<List<SgBPhyInResult>> selectSgPhyInResult(SgBPhyInResultSaveRequest request) throws NDSException;

    ValueHolderV14<List<SgPhyInResultBillSaveRequest>> querySgPhyInResultMQBody(List<Long> ids, User user);
}
