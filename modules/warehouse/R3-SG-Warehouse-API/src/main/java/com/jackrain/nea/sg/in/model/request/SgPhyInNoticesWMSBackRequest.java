package com.jackrain.nea.sg.in.model.request;

import com.alibaba.fastjson.JSONArray;
import com.jackrain.nea.web.face.User;
import lombok.Data;

import java.io.Serializable;

/**
 * @author: 舒威
 * @since: 2019/9/4
 * create at : 2019/9/4 10:08
 */
@Data
public class SgPhyInNoticesWMSBackRequest implements Serializable{

    /**
     * 主键ID
     */
    private JSONArray ids;

    /**
     * 是否是单对象
     * true=单对象
     * false=列表
     *  注：单对象和列表返回的message不同
     */
    private Boolean isObj;

    /**
     * 用户信息
     */
    private User user;
}
