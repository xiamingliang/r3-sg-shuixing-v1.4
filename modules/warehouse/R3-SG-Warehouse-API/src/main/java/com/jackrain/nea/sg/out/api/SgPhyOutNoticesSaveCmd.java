package com.jackrain.nea.sg.out.api;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.basic.model.result.SgR3BaseResult;
import com.jackrain.nea.sg.out.model.request.SgPhyOutNoticesBillSaveRequest;
import com.jackrain.nea.sg.out.model.request.SgPhyOutNoticesUpdateRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;

import java.util.List;

/**
 * @author leexh
 * @since 2019/4/22 11:29
 */
public interface SgPhyOutNoticesSaveCmd {

    ValueHolderV14<SgR3BaseResult> saveSgPhyOutNotices(SgPhyOutNoticesBillSaveRequest request) throws NDSException;

    ValueHolderV14 updateOutNoticesStatus(List<SgPhyOutNoticesUpdateRequest> requests, User user);
}