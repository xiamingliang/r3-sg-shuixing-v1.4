package com.jackrain.nea.sg.out.model.request;

import com.alibaba.fastjson.annotation.JSONField;
import com.jackrain.nea.sys.domain.BaseModel;
import com.jackrain.nea.web.face.User;
import lombok.Data;

import java.util.List;

/**
 * @author zhu lin yu
 * @since 2019-11-18
 * create at : 2019-11-18 17:53
 */
@Data
public class SgPhyOutPickOrderSaveRequest extends BaseModel {

    @JSONField(name = "ID")
    private Long sgPhyBOutPickorderId;

    private List<SgPhyOutNoticesSaveRequest> outNoticesList;

    private List<SgPhyOutResultSaveRequest> outResultList;

    private List<SgPhyOutNoticesImpItemSaveRequest> outNoticesImpItemList;

    private List<SgOutPickorderResultItemSaveRequest> outPickorderResultItemList;

    @JSONField(name = "SG_B_OUT_PICKORDER_TEUS_ITEM")
    private List<SgOutPickorderTeusItemSaveRequest> outPickorderTeusItemList;

    private User loginUser;
}
