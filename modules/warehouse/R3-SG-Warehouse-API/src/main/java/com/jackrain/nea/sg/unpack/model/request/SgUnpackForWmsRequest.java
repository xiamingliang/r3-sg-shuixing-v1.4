package com.jackrain.nea.sg.unpack.model.request;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author 周琳胜
 * @since 2019-10-23
 * create at : 2019-10-23 19:43
 */
@Data
public class SgUnpackForWmsRequest implements Serializable {

    //是否传WMS
    private Integer isTowms;

    //传WMS状态
    private List<Integer> wmsStatus;

    //失败次数
    private Integer wmsFailCount;

    // 失败原因
    private String wmsFailReason;

    // 审核状态
    private Integer status;

    //页数
    private int pageNum;

    //每页数量
    private int pageSize;

    public SgUnpackForWmsRequest(){
        this.pageNum = 1;
        this.pageSize = 20;
    }
}
