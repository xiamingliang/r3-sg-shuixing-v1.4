package com.jackrain.nea.sg.in.api;

import com.jackrain.nea.sg.in.model.request.SgPhyInNoticesBillPassRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 * @Author: ChenChen
 * @Description:
 * @Date: Create in 19:05 2019/4/24
 */
public interface SgPhyInNoticesPassCmd {
    ValueHolderV14 passSgBPhyInNotices(SgPhyInNoticesBillPassRequest request);
}
