package com.jackrain.nea.sg.out.model.request;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @description: 工作台查出库通知单入参实体-查询实体类
 * @author: 郑小龙
 * @date: 2019-05-15 15:48
 */
@Data
public class WorkbenchOutNoticesQueryRequest implements Serializable {
    //店铺id集合
    private List<Long> shopList;
    //是否是京东平台 true 京东平台；false 不是京东平台
    private Boolean isJdPlatform;

    public WorkbenchOutNoticesQueryRequest(){
        this.isJdPlatform = false;
    }

}
