package com.jackrain.nea.sg.in.api;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.in.model.request.SgPhyInResultBillSaveRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;

import java.util.List;

public interface SgPhyInResultSaveAndAuditCmd {

    ValueHolderV14 saveAndAuditBill(List<SgPhyInResultBillSaveRequest> billList)  throws NDSException;
}
