package com.jackrain.nea.sg.in.model.result;

import lombok.Data;

import java.io.Serializable;


@Data
public class SgBaseModelR3 implements Serializable {

    private Long id;

    private Integer code;

    private String message;
}
