package com.jackrain.nea.sg.phyadjust.model.request;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.io.Serializable;

/**
 * @author: 舒威
 * @since: 2019/12/8
 * create at : 2019/12/8 22:12
 */
@Data
public class SgPhyAdjustItemStoreRequest implements Serializable {

    @JSONField(name = "IS_TEUS")
    private Integer isTeus;

    @JSONField(name = "PS_C_SKU_ID")
    private Long psCSkuId;

    @JSONField(name = "PS_C_TEUS_ID")
    private Long psCTeusId;

    @JSONField(name = "CP_C_STORE_ID")
    private Long cpCStoreId;

    @JSONField(name = "CP_C_STORE_ECODE")
    private String cpCStoreEcode;

    @JSONField(name = "CP_C_STORE_ENAME")
    private String cpCStoreEname;

}
