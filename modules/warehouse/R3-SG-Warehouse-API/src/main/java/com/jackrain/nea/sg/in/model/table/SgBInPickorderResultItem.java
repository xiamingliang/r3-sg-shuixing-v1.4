package com.jackrain.nea.sg.in.model.table;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.*;
import com.jackrain.nea.es.annotations.Document;
import com.jackrain.nea.es.annotations.Field;
import com.jackrain.nea.es.constans.FieldType;
import com.jackrain.nea.sys.domain.BaseModelES;
import lombok.Data;

import java.util.Date;

@TableName(value = "sg_b_in_pickorder_result_item")
@Data
@Document(index = "sg_b_in_pickorder", type = "sg_b_in_pickorder_result_item")
public class SgBInPickorderResultItem extends BaseModelES {
    @JSONField(name = "ID")
    @TableField(fill = FieldFill.INSERT)
    @TableId(value = "id", type = IdType.INPUT)
    @Field(type = FieldType.Long)
    private Long id;

    @JSONField(name = "SG_B_IN_PICKORDER_ID")
    @Field(type = FieldType.Long)
    private Long sgBInPickorderId;

    @JSONField(name = "BILL_NO")
    @Field(type = FieldType.Keyword)
    private String billNo;

    @JSONField(name = "BILL_DATE")
    @Field(type = FieldType.Long)
    private Date billDate;

    @JSONField(name = "CP_C_PHY_WAREHOUSE_ID")
    @Field(type = FieldType.Long)
    private Long cpCPhyWarehouseId;

    @JSONField(name = "CP_C_PHY_WAREHOUSE_ECODE")
    @Field(type = FieldType.Keyword)
    private String cpCPhyWarehouseEcode;

    @JSONField(name = "CP_C_PHY_WAREHOUSE_ENAME")
    @Field(type = FieldType.Keyword)
    private String cpCPhyWarehouseEname;

    @JSONField(name = "CP_C_PHY_WAREHOUSE_ORIG_ID")
    @Field(type = FieldType.Long)
    private Long cpCPhyWarehouseOrigId;

    @JSONField(name = "CP_C_PHY_WAREHOUSE_ORIG_ECODE")
    @Field(type = FieldType.Keyword)
    private String cpCPhyWarehouseOrigEcode;

    @JSONField(name = "CP_C_PHY_WAREHOUSE_ORIG_ENAME")
    @Field(type = FieldType.Keyword)
    private String cpCPhyWarehouseOrigEname;

    @JSONField(name = "REMARK")
    @Field(type = FieldType.Keyword)
    private String remark;

    @JSONField(name = "VERSION")
    @Field(type = FieldType.Long)
    private Long version;

    @JSONField(name = "AD_ORG_ID")
    @Field(type = FieldType.Long)
    private Long adOrgId;

    @JSONField(name = "ISACTIVE")
    @Field(type = FieldType.Keyword)
    private String isactive;

    @JSONField(name = "AD_CLIENT_ID")
    @Field(type = FieldType.Long)
    private Long adClientId;

    @JSONField(name = "OWNERID")
    @Field(type = FieldType.Long)
    private Long ownerid;

    @JSONField(name = "OWNERENAME")
    @Field(type = FieldType.Keyword)
    private String ownerename;

    @JSONField(name = "OWNERNAME")
    @Field(type = FieldType.Keyword)
    private String ownername;

    @JSONField(name = "CREATIONDATE")
    @Field(type = FieldType.Long)
    private Date creationdate;

    @JSONField(name = "MODIFIERID")
    @Field(type = FieldType.Long)
    private Long modifierid;

    @JSONField(name = "MODIFIERENAME")
    @Field(type = FieldType.Keyword)
    private String modifierename;

    @JSONField(name = "MODIFIERNAME")
    @Field(type = FieldType.Keyword)
    private String modifiername;

    @JSONField(name = "MODIFIEDDATE")
    @Field(type = FieldType.Long)
    private Date modifieddate;
}