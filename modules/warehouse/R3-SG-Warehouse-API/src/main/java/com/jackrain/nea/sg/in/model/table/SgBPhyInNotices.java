package com.jackrain.nea.sg.in.model.table;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.*;
import com.jackrain.nea.es.annotations.Document;
import com.jackrain.nea.es.annotations.Field;
import com.jackrain.nea.es.constans.FieldType;
import com.jackrain.nea.sys.domain.BaseModelES;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@TableName(value = "sg_b_phy_in_notices")
@Data
@Document(index = "sg_b_phy_in_notices",type = "sg_b_phy_in_notices")
public class SgBPhyInNotices extends BaseModelES {
    @JSONField(name = "ID")
    @TableField(fill = FieldFill.INSERT)
    @TableId(value = "id", type = IdType.INPUT)
    @Field(type = FieldType.Long)
    private Long id;

    @JSONField(name = "CP_C_PHY_WAREHOUSE_ID")
    @Field(type = FieldType.Long)
    private Long cpCPhyWarehouseId;

    @JSONField(name = "CP_C_PHY_WAREHOUSE_ECODE")
    @Field(type = FieldType.Keyword)
    private String cpCPhyWarehouseEcode;

    @JSONField(name = "CP_C_PHY_WAREHOUSE_ENAME")
    @Field(type = FieldType.Keyword)
    private String cpCPhyWarehouseEname;

    @JSONField(name = "BILL_NO")
    @Field(type = FieldType.Keyword)
    private String billNo;

    @JSONField(name = "GOODS_OWNER")
    @Field(type = FieldType.Keyword)
    private String goodsOwner;

    @JSONField(name = "BILL_DATE")
    @Field(type = FieldType.Long)
    private Date billDate;

    @JSONField(name = "IN_TIME")
    @Field(type = FieldType.Long)
    private Date inTime;

    @JSONField(name = "RECEIVE_TIME")
    @Field(type = FieldType.Long)
    private Date receiveTime;

    @JSONField(name = "CP_C_CUSTOMER_WAREHOUSE_ID")
    @Field(type = FieldType.Long)
    private Long cpCCustomerWarehouseId;

    @JSONField(name = "CP_C_CS_ECODE")
    @Field(type = FieldType.Keyword)
    private String cpCCsEcode;

    @JSONField(name = "CP_C_CS_ENAME")
    @Field(type = FieldType.Keyword)
    private String cpCCsEname;

    @JSONField(name = "CP_C_SUPPLIER_ID")
    @Field(type = FieldType.Long)
    private Long cpCSupplierId;

    @JSONField(name = "ORDER_TIME")
    @Field(type = FieldType.Long)
    private Date orderTime;

    @JSONField(name = "IN_TYPE")
    @Field(type = FieldType.Integer)
    private Integer inType;

    @JSONField(name = "SOURCE_BILL_TYPE")
    @Field(type = FieldType.Integer)
    private Integer sourceBillType;

    @JSONField(name = "BILL_STATUS")
    @Field(type = FieldType.Integer)
    private Integer billStatus;

    @JSONField(name = "SOURCE_BILL_ID")
    @Field(type = FieldType.Long)
    private Long sourceBillId;

    @JSONField(name = "SOURCE_BILL_NO")
    @Field(type = FieldType.Keyword)
    private String sourceBillNo;

    @JSONField(name = "SEND_NAME")
    @Field(type = FieldType.Keyword)
    private String sendName;

    @JSONField(name = "SEND_MOBILE")
    @Field(type = FieldType.Keyword)
    private String sendMobile;

    @JSONField(name = "SEND_PHONE")
    @Field(type = FieldType.Keyword)
    private String sendPhone;

    @JSONField(name = "CP_C_REGION_PROVINCE_ID")
    @Field(type = FieldType.Long)
    private Long cpCRegionProvinceId;

    @JSONField(name = "CP_C_REGION_PROVINCE_ECODE")
    @Field(type = FieldType.Keyword)
    private String cpCRegionProvinceEcode;

    @JSONField(name = "CP_C_REGION_PROVINCE_ENAME")
    @Field(type = FieldType.Keyword)
    private String cpCRegionProvinceEname;

    @JSONField(name = "CP_C_REGION_CITY_ID")
    @Field(type = FieldType.Long)
    private Long cpCRegionCityId;

    @JSONField(name = "CP_C_REGION_CITY_ECODE")
    @Field(type = FieldType.Keyword)
    private String cpCRegionCityEcode;

    @JSONField(name = "CP_C_REGION_CITY_ENAME")
    @Field(type = FieldType.Keyword)
    private String cpCRegionCityEname;

    @JSONField(name = "CP_C_REGION_AREA_ID")
    @Field(type = FieldType.Long)
    private Long cpCRegionAreaId;

    @JSONField(name = "CP_C_REGION_AREA_ECODE")
    @Field(type = FieldType.Keyword)
    private String cpCRegionAreaEcode;

    @JSONField(name = "CP_C_REGION_AREA_ENAME")
    @Field(type = FieldType.Keyword)
    private String cpCRegionAreaEname;

    @JSONField(name = "SEND_ADDRESS")
    @Field(type = FieldType.Keyword)
    private String sendAddress;

    @JSONField(name = "SEND_ZIP")
    @Field(type = FieldType.Keyword)
    private String sendZip;

    @JSONField(name = "CP_C_LOGISTICS_ID")
    @Field(type = FieldType.Long)
    private Long cpCLogisticsId;

    @JSONField(name = "CP_C_LOGISTICS_ECODE")
    @Field(type = FieldType.Keyword)
    private String cpCLogisticsEcode;

    @JSONField(name = "CP_C_LOGISTICS_ENAME")
    @Field(type = FieldType.Keyword)
    private String cpCLogisticsEname;

    @JSONField(name = "LOGISTIC_NUMBER")
    @Field(type = FieldType.Keyword)
    private String logisticNumber;

    @JSONField(name = "CP_C_SHOP_ID")
    @Field(type = FieldType.Long)
    private Long cpCShopId;

    @JSONField(name = "CP_C_SHOP_TITLE")
    @Field(type = FieldType.Keyword)
    private String cpCShopTitle;

    @JSONField(name = "SOURCECODE")
    @Field(type = FieldType.Keyword)
    private String sourcecode;

    @JSONField(name = "SELLER_REMARK")
    @Field(type = FieldType.Keyword)
    private String sellerRemark;

    @JSONField(name = "BUYER_REMARK")
    @Field(type = FieldType.Keyword)
    private String buyerRemark;

    @JSONField(name = "TOT_QTY")
    @Field(type = FieldType.Double)
    private BigDecimal totQty;

    @JSONField(name = "TOT_QTY_IN")
    @Field(type = FieldType.Double)
    private BigDecimal totQtyIn;

    @JSONField(name = "TOT_QTY_DIFF")
    @Field(type = FieldType.Double)
    private BigDecimal totQtyDiff;

    @JSONField(name = "TOT_AMT_LIST")
    @Field(type = FieldType.Double)
    private BigDecimal totAmtList;

    @JSONField(name = "TOT_AMT_COST")
    @Field(type = FieldType.Double)
    private BigDecimal totAmtCost;

    @JSONField(name = "TOT_AMT_LIST_IN")
    @Field(type = FieldType.Double)
    private BigDecimal totAmtListIn;

    @JSONField(name = "TOT_AMT_COST_IN")
    @Field(type = FieldType.Double)
    private BigDecimal totAmtCostIn;

    @JSONField(name = "TOT_AMT_LIST_DIFF")
    @Field(type = FieldType.Double)
    private BigDecimal totAmtListDiff;

    @JSONField(name = "TOT_AMT_COST_DIFF")
    @Field(type = FieldType.Double)
    private BigDecimal totAmtCostDiff;

    @JSONField(name = "WMS_STATUS")
    @Field(type = FieldType.Long)
    private Long wmsStatus;

    @JSONField(name = "WMS_FAIL_REASON")
    @Field(type = FieldType.Keyword)
    private String wmsFailReason;

    @JSONField(name = "WMS_FAIL_COUNT")
    @Field(type = FieldType.Long)
    private Long wmsFailCount;

    @JSONField(name = "REMARK")
    @Field(type = FieldType.Keyword)
    private String remark;

    @JSONField(name = "PROPERTY1")
    @Field(type = FieldType.Keyword)
    private String property1;

    @JSONField(name = "PROPERTY2")
    @Field(type = FieldType.Keyword)
    private String property2;

    @JSONField(name = "PROPERTY3")
    @Field(type = FieldType.Keyword)
    private String property3;

    @JSONField(name = "PROPERTY4")
    @Field(type = FieldType.Keyword)
    private String property4;

    @JSONField(name = "PROPERTY5")
    @Field(type = FieldType.Keyword)
    private String property5;

    @JSONField(name = "PROPERTY6")
    @Field(type = FieldType.Keyword)
    private String property6;

    @JSONField(name = "PROPERTY7")
    @Field(type = FieldType.Keyword)
    private String property7;

    @JSONField(name = "PROPERTY8")
    @Field(type = FieldType.Keyword)
    private String property8;

    @JSONField(name = "PROPERTY9")
    @Field(type = FieldType.Keyword)
    private String property9;

    @JSONField(name = "PROPERTY10")
    @Field(type = FieldType.Keyword)
    private String property10;

    @JSONField(name = "AD_CLIENT_ID")
    @Field(type = FieldType.Long)
    private Long adClientId;

    @JSONField(name = "AD_ORG_ID")
    @Field(type = FieldType.Long)
    private Long adOrgId;

    @JSONField(name = "ISACTIVE")
    @Field(type = FieldType.Keyword)
    private String isactive;

    @JSONField(name = "OWNERID")
    @Field(type = FieldType.Long)
    private Long ownerid;

    @JSONField(name = "OWNERENAME")
    @Field(type = FieldType.Keyword)
    private String ownerename;

    @JSONField(name = "OWNERNAME")
    @Field(type = FieldType.Keyword)
    private String ownername;

    @JSONField(name = "CREATIONDATE")
    @Field(type = FieldType.Long)
    private Date creationdate;

    @JSONField(name = "MODIFIERID")
    @Field(type = FieldType.Long)
    private Long modifierid;

    @JSONField(name = "MODIFIERENAME")
    @Field(type = FieldType.Keyword)
    private String modifierename;

    @JSONField(name = "MODIFIERNAME")
    @Field(type = FieldType.Keyword)
    private String modifiername;

    @JSONField(name = "MODIFIEDDATE")
    @Field(type = FieldType.Long)
    private Date modifieddate;

    @JSONField(name = "IS_PASS_WMS")
    @Field(type = FieldType.Integer)
    private Integer isPassWms;

    @JSONField(name = "IS_MQ_CONSUME")
    @Field(type = FieldType.Integer)
    private Integer isMqConsume;

    @JSONField(name = "IS_AIR_EMBARGO")
    @Field(type = FieldType.Integer)
    private Integer isAirEmbargo;

    @JSONField(name = "PASS_WMS_TIME")
    @Field(type = FieldType.Long)
    private Date passWmsTime;

    @JSONField(name = "RESERVE_BIGINT01")
    @Field(type = FieldType.Long)
    private Long reserveBigint01;

    @JSONField(name = "RESERVE_BIGINT02")
    @Field(type = FieldType.Long)
    private Long reserveBigint02;

    @JSONField(name = "RESERVE_BIGINT03")
    @Field(type = FieldType.Long)
    private Long reserveBigint03;

    @JSONField(name = "RESERVE_BIGINT04")
    @Field(type = FieldType.Long)
    private Long reserveBigint04;

    @JSONField(name = "RESERVE_BIGINT05")
    @Field(type = FieldType.Long)
    private Long reserveBigint05;

    @JSONField(name = "RESERVE_BIGINT06")
    @Field(type = FieldType.Long)
    private Long reserveBigint06;

    @JSONField(name = "RESERVE_BIGINT07")
    @Field(type = FieldType.Long)
    private Long reserveBigint07;

    @JSONField(name = "RESERVE_BIGINT08")
    @Field(type = FieldType.Long)
    private Long reserveBigint08;

    @JSONField(name = "RESERVE_BIGINT09")
    @Field(type = FieldType.Long)
    private Long reserveBigint09;

    @JSONField(name = "RESERVE_BIGINT10")
    @Field(type = FieldType.Long)
    private Long reserveBigint10;

    @JSONField(name = "RESERVE_VARCHAR01")
    @Field(type = FieldType.Keyword)
    private String reserveVarchar01;

    @JSONField(name = "RESERVE_VARCHAR02")
    @Field(type = FieldType.Keyword)
    private String reserveVarchar02;

    @JSONField(name = "RESERVE_VARCHAR03")
    @Field(type = FieldType.Keyword)
    private String reserveVarchar03;

    @JSONField(name = "RESERVE_VARCHAR04")
    @Field(type = FieldType.Keyword)
    private String reserveVarchar04;

    @JSONField(name = "RESERVE_VARCHAR05")
    @Field(type = FieldType.Keyword)
    private String reserveVarchar05;

    @JSONField(name = "RESERVE_VARCHAR06")
    @Field(type = FieldType.Keyword)
    private String reserveVarchar06;

    @JSONField(name = "RESERVE_VARCHAR07")
    @Field(type = FieldType.Keyword)
    private String reserveVarchar07;

    @JSONField(name = "RESERVE_VARCHAR08")
    @Field(type = FieldType.Keyword)
    private String reserveVarchar08;

    @JSONField(name = "RESERVE_VARCHAR09")
    @Field(type = FieldType.Keyword)
    private String reserveVarchar09;

    @JSONField(name = "RESERVE_VARCHAR10")
    @Field(type = FieldType.Keyword)
    private String reserveVarchar10;

    @JSONField(name = "RESERVE_DECIMAL01")
    @Field(type = FieldType.Double)
    private BigDecimal reserveDecimal01;

    @JSONField(name = "RESERVE_DECIMAL02")
    @Field(type = FieldType.Double)
    private BigDecimal reserveDecimal02;

    @JSONField(name = "RESERVE_DECIMAL03")
    @Field(type = FieldType.Double)
    private BigDecimal reserveDecimal03;

    @JSONField(name = "RESERVE_DECIMAL04")
    @Field(type = FieldType.Double)
    private BigDecimal reserveDecimal04;

    @JSONField(name = "RESERVE_DECIMAL05")
    @Field(type = FieldType.Double)
    private BigDecimal reserveDecimal05;

    @JSONField(name = "RESERVE_DECIMAL06")
    @Field(type = FieldType.Double)
    private BigDecimal reserveDecimal06;

    @JSONField(name = "RESERVE_DECIMAL07")
    @Field(type = FieldType.Double)
    private BigDecimal reserveDecimal07;

    @JSONField(name = "RESERVE_DECIMAL08")
    @Field(type = FieldType.Double)
    private BigDecimal reserveDecimal08;

    @JSONField(name = "RESERVE_DECIMAL09")
    @Field(type = FieldType.Double)
    private BigDecimal reserveDecimal09;

    @JSONField(name = "RESERVE_DECIMAL10")
    @Field(type = FieldType.Double)
    private BigDecimal reserveDecimal10;

    @JSONField(name = "WMS_BILL_NO")
    @Field(type = FieldType.Keyword)
    private String wmsBillNo;

    @JSONField(name = "PICK_STATUS")
    @Field(type = FieldType.Integer)
    private Integer pickStatus;

    @JSONField(name = "IS_DIFF_DEAL")
    @Field(type = FieldType.Integer)
    private Integer isDiffDeal;

    @JSONField(name = "STATUS")
    @Field(type = FieldType.Integer)
    private Integer status;

    @JSONField(name = "STATUS_ID")
    @Field(type = FieldType.Long)
    private Long statusId;

    @JSONField(name = "STATUS_TIME")
    @Field(type = FieldType.Long)
    private Date statusTime;

    @JSONField(name = "qty_tot_scan")
    @Field(type = FieldType.Double)
    private BigDecimal qtyTotScan;
}