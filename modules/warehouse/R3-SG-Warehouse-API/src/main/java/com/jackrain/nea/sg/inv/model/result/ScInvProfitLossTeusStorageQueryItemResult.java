package com.jackrain.nea.sg.inv.model.result;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author 舒威
 * @since 2019/12/3
 * create at : 2019/12/3 21:51
 */
@Data
public class ScInvProfitLossTeusStorageQueryItemResult implements Serializable {

    private List teusList;

    private Long total;

    private Long totalPages;
}
