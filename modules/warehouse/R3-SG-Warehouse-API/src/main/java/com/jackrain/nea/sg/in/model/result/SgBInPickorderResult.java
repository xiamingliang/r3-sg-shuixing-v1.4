package com.jackrain.nea.sg.in.model.result;

import com.jackrain.nea.sg.in.model.table.*;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author: heliu
 * @since: 2019/11/18
 * create at : 2019/11/18 12:36
 */
@Data
public class SgBInPickorderResult implements Serializable {

    private static final long serialVersionUID = -3755700430431708016L;
    /**
     * 入库拣货单
     */
    SgBInPickorder sgBInPickorder;

    /**
     * 拣货明细
     */
    List<SgBInPickorderItem> sgBInPickorderItemList;

    /**
     * 装箱明细
     */
    List<SgBInPickorderTeusItem> sgBInPickorderTeusItemList;

    /**
     * 来源入库通知单
     */
    List<SgBInPickorderNoticeItem> sgBInPickorderNoticeItemList;

    /**
     * 入库结果单
     */
    List<SgBInPickorderResultItem> sgBInPickorderResultItemList;
}