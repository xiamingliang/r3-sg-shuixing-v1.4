package com.jackrain.nea.sg.in.api;

import com.jackrain.nea.sg.in.model.request.SgPhyInNoticesSelectBySourceRequest;
import com.jackrain.nea.sg.in.model.result.SgBPhyInNoticesResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;

import java.util.List;

/**
 * @Author: ChenChen
 * @Description:通过来源id和类型查询通知单
 * @Date: Create in 19:07 2019/5/16
 */
public interface SgPhyInNoticesSelectBySourceCmd {
    ValueHolderV14<List<SgBPhyInNoticesResult>> selectBySource(SgPhyInNoticesSelectBySourceRequest request);
}
