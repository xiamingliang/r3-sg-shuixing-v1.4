package com.jackrain.nea.sg.out.model.request;

import com.alibaba.fastjson.annotation.JSONField;
import com.jackrain.nea.sg.basic.model.request.SgR3BaseRequest;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author zhu lin yu
 * @since 2019-11-13
 * create at : 2019-11-13 14:20
 */
@Data
public class SgPhyOutPickUpGoodSaveRequest extends SgR3BaseRequest implements Serializable {

    @JSONField(name = "ids")
    private List<Long> outNoticeIdList;


}
