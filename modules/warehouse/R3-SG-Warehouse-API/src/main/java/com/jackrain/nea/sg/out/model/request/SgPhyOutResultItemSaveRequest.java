package com.jackrain.nea.sg.out.model.request;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author leexh
 * @since 2019/4/23 11:40
 */
@Data
public class SgPhyOutResultItemSaveRequest implements Serializable {

    private Long id;

    private Long sgBPhyOutResultId;

    private Long psCSkuId;

    private String psCSkuEcode;

    private Long psCProId;

    private String psCProEcode;

    private String psCProEname;

    private Long psCSpec1Id;

    private String psCSpec1Ecode;

    private String psCSpec1Ename;

    private Long psCSpec2Id;

    private String psCSpec2Ecode;

    private String psCSpec2Ename;

    private BigDecimal qty;

    private BigDecimal priceList;

    private BigDecimal amtListOut;

    private Long sgBPhyOutNoticesItemId;

    private String gbcode;
}
