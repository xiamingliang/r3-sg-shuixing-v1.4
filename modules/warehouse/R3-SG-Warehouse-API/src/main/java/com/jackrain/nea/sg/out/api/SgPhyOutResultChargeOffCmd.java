package com.jackrain.nea.sg.out.api;

import com.jackrain.nea.sg.out.model.request.SgOutResultMQRequest;
import com.jackrain.nea.sg.out.model.request.SgPhyOutResultBillSaveRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 * @author 舒威
 * @since 2019/12/13
 * create at : 2019/12/13 15:34
 */
public interface SgPhyOutResultChargeOffCmd {

    ValueHolderV14<SgOutResultMQRequest> chargeOffSgPhyOutResult(SgPhyOutResultBillSaveRequest request);
}
