package com.jackrain.nea.sg.out.model.request;

import com.jackrain.nea.sg.basic.model.request.SgR3BaseRequest;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author zhu lin yu
 * @since 2019-10-29
 * create at : 2019-10-29 14:31
 */
@Data
public class SgPhyOutCommitForPDARequest extends SgR3BaseRequest implements Serializable {

    private Long sgBPhyOutNoticesId;

    private Integer isLast;

    private List<SgPhyOutCommitForPDADetailRequest> detailRequests;
}
