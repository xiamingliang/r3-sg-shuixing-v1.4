package com.jackrain.nea.sg.inv.model.result;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;

import java.io.Serializable;

/**
 * @author 舒威
 * @since 2019/4/11
 * create at : 2019/4/11 11:26
 */
@Data
public class ScInvProfitLossMarginQueryResult implements Serializable {

    private JSONObject pand_result;

    private JSONObject unfish_result;

    private JSONObject profit_item_result;
}
