package com.jackrain.nea.sg.out.api;

import com.jackrain.nea.sg.out.model.request.SgPhyOutPickOrderSaveRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 * @author zhu lin yu
 * @since 2019-11-18
 * create at : 2019-11-18 17:46
 */
public interface SgPhyOutPickOrderSaveCmd {
    /**
     *出库拣货单
     */
    ValueHolderV14 saveSgBoutPickorder(SgPhyOutPickOrderSaveRequest request);

    /**
     *出库拣货单-拣货明细
     */
    ValueHolderV14 saveSgBoutPickorderItem(SgPhyOutPickOrderSaveRequest request);

    /**
     *出库拣货单-来源出库通知单
     */
    ValueHolderV14 saveSgBoutPickorderNoticeItem(SgPhyOutPickOrderSaveRequest request);

    /**
     *出库拣货单-出库结果单明细
     */
    ValueHolderV14 saveSgBoutPickorderResultItem(SgPhyOutPickOrderSaveRequest request);

    /**
     *出库拣货单-装箱明细
     */
    ValueHolderV14 saveSgBoutPickorderTeusItem(SgPhyOutPickOrderSaveRequest request);
}
