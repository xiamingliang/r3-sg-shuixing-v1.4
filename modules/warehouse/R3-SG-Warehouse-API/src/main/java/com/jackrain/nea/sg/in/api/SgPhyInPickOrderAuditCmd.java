package com.jackrain.nea.sg.in.api;

import com.jackrain.nea.sg.in.model.request.SgPhyInPickOrderAuditRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 * @author zhu lin yu
 * @since 2019-11-20
 * create at : 2019-11-20 16:45
 */
public interface SgPhyInPickOrderAuditCmd {
    ValueHolderV14 auditInPickOrder(SgPhyInPickOrderAuditRequest request);
}
