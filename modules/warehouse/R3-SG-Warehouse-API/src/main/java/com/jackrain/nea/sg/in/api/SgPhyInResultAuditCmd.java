package com.jackrain.nea.sg.in.api;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.in.model.request.SgPhyInResultBillAuditRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;

/***
 * 审核入库结果单
 */
public interface SgPhyInResultAuditCmd {

    ValueHolderV14 auditSgPhyInResult(SgPhyInResultBillAuditRequest request) throws NDSException;
}
