package com.jackrain.nea.sg.in.model.request;

import com.jackrain.nea.web.face.User;
import lombok.Data;

import java.io.Serializable;

/**
 * @author: 舒威
 * @since: 2019/9/2
 * create at : 2019/9/2 20:33
 */
@Data
public class SgInResultMQRequest implements Serializable {

    /**
     * 入库结果单ID
     */
    private Long id;

    /**
     * 用户信息
     */
    private User loginUser;


    /**
     * 是否一键入库
     */
    private Boolean isOneClickLibrary;


    public SgInResultMQRequest() {
        this.isOneClickLibrary = false;
    }
}
