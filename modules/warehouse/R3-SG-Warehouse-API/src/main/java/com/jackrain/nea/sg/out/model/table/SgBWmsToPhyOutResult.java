package com.jackrain.nea.sg.out.model.table;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.*;
import com.jackrain.nea.es.annotations.Document;
import com.jackrain.nea.es.annotations.Field;
import com.jackrain.nea.es.constans.FieldType;
import com.jackrain.nea.sys.domain.BaseModelES;
import lombok.Data;

import java.util.Date;

@TableName(value = "sg_b_wms_to_phy_out_result")
@Data
@Document(index = "sg_b_wms_to_phy_out_result", type = "sg_b_wms_to_phy_out_result")
public class SgBWmsToPhyOutResult extends BaseModelES {
    @JSONField(name = "ID")
    @TableField(fill = FieldFill.INSERT)
    @TableId(value = "id", type = IdType.INPUT)
    @Field(type = FieldType.Long)
    private Long id;

    @JSONField(name = "OUT_TYPE")
    @Field(type = FieldType.Integer)
    private Integer outType;

    @JSONField(name = "OUT_RESULT_STR")
    @Field(type = FieldType.Keyword)
    private String outResultStr;

    @JSONField(name = "STATUS")
    @Field(type = FieldType.Integer)
    private Integer status;

    @JSONField(name = "WMS_FAILED_COUNT")
    @Field(type = FieldType.Integer)
    private Integer wmsFailedCount;

    @JSONField(name = "AD_CLIENT_ID")
    @Field(type = FieldType.Long)
    private Long adClientId;

    @JSONField(name = "AD_ORG_ID")
    @Field(type = FieldType.Long)
    private Long adOrgId;

    @JSONField(name = "ISACTIVE")
    @Field(type = FieldType.Keyword)
    private String isactive;

    @JSONField(name = "OWNERID")
    @Field(type = FieldType.Long)
    private Long ownerid;

    @JSONField(name = "OWNERENAME")
    @Field(type = FieldType.Keyword)
    private String ownerename;

    @JSONField(name = "OWNERNAME")
    @Field(type = FieldType.Keyword)
    private String ownername;

    @JSONField(name = "CREATIONDATE")
    @Field(type = FieldType.Long)
    private Date creationdate;

    @JSONField(name = "MODIFIERID")
    @Field(type = FieldType.Long)
    private Long modifierid;

    @JSONField(name = "MODIFIERENAME")
    @Field(type = FieldType.Keyword)
    private String modifierename;

    @JSONField(name = "MODIFIERNAME")
    @Field(type = FieldType.Keyword)
    private String modifiername;

    @JSONField(name = "MODIFIEDDATE")
    @Field(type = FieldType.Long)
    private Date modifieddate;

    @JSONField(name = "NOTICES_BILL_NO")
    @Field(type = FieldType.Keyword)
    private String noticesBillNo;

    @JSONField(name = "WMS_FAILED_REASON")
    @Field(type = FieldType.Keyword)
    private String wmsFailedReason;

    @JSONField(name = "WMS_BACK_COUNT")
    @Field(type = FieldType.Long)
    private Long wmsBackCount;

    @JSONField(name = "RESULT_ID")
    @Field(type = FieldType.Long)
    private Long resultId;

    @JSONField(name = "IS_AUTO_AUDIT")
    @Field(type = FieldType.Keyword)
    private String isAutoAudit;

    @JSONField(name = "AUTO_AUDIT_FAILED_REASON")
    @Field(type = FieldType.Keyword)
    private String autoAuditFailedReason;

    @JSONField(name = "AUTO_AUDIT_CNT")
    @Field(type = FieldType.Integer)
    private Integer autoAuditCnt;

}