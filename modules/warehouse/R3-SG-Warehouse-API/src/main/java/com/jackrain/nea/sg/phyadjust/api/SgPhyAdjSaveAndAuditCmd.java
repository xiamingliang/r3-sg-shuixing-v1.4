package com.jackrain.nea.sg.phyadjust.api;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.phyadjust.model.request.SgPhyAdjustBillSaveRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 *@author csy
 * */
public interface SgPhyAdjSaveAndAuditCmd {
    /**
     * 保存并审核
     *
     * @param request 新增的参数
     * @return result
     * @throws NDSException 框架封装的异常
     */
    ValueHolderV14 saveAndAuditAdj(SgPhyAdjustBillSaveRequest request) throws NDSException;
}
