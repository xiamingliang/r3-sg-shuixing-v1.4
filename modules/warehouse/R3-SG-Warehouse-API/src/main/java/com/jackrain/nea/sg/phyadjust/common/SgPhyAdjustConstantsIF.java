package com.jackrain.nea.sg.phyadjust.common;

/**
 * @author csy
 * Date: 2019/5/6
 * Description:
 */
public interface SgPhyAdjustConstantsIF {

    //来源单据类型 -退货入库单
    int SOURCE_BILL_TYPE_REF_IN = 1;

    //来源单据类型 -盈亏单
    int SOURCE_BILL_TYPE_PAND = 2;

    //来源单据类型  -零售退货单
    int SOURCE_BILL_TYPE_RETAIL_REF = 3;

    //来源单据类型  -零售退货单
    int SOURCE_BILL_TYPE_TRANSFER = 4;

    //来源单据类型  -正次品调整
    int SOURCE_BILL_TYPE_ZPCC = 5;

    //来源单据类型  -拆箱单
    int SOURCE_BILL_TYPE_UNPACK = 6;

    int SOURCE_BILL_TYPE_PACK = 7;

    int SOURCE_BILL_TYPE_HANDWORK = 8;

    int SOURCE_BILL_TYPE_CHKDIFFE = 9;

    //单据类型-正常调整
    int BILL_TYPE_NORMAL = 1;

    //单据类型-差异调整
    int BILL_TYPE_DIFF = 2;


    /**
     * 调整性质损益调整
     */
    long ADJUST_PROP_PROFIT = 8;
}
