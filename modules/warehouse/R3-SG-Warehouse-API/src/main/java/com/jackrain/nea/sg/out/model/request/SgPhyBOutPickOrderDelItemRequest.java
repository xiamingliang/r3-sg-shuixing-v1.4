package com.jackrain.nea.sg.out.model.request;

import lombok.Data;

import java.io.Serializable;

/**
 * @author zhu lin yu
 * @since 2019-11-19
 * create at : 2019-11-19 17:25
 */
@Data
public class SgPhyBOutPickOrderDelItemRequest implements Serializable {
    private Long id;

    private Long pickOutTeusId;

    private int isTeus;
}
