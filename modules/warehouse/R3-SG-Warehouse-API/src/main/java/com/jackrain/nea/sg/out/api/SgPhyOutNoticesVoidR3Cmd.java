package com.jackrain.nea.sg.out.api;

import com.jackrain.nea.sys.Command;

/**
 * @author leexh
 * @since 2019/4/23 15:06
 */
public interface SgPhyOutNoticesVoidR3Cmd extends Command {
}
