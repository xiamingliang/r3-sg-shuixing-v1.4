package com.jackrain.nea.sg.in.model.request;

import com.alibaba.fastjson.annotation.JSONField;
import com.jackrain.nea.sg.basic.model.request.SgR3BaseRequest;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author zhu lin yu
 * @since 2019-11-13
 * create at : 2019-11-13 14:20
 */
@Data
public class SgPhyInPickUpGoodSaveRequest extends SgR3BaseRequest implements Serializable {

    @JSONField(name = "ids")
    private List<Long> inNoticeIdList;


    // ================兼容调拨出库单生成拣货单传参==================
    /**
     * 标记是否是调拨出库单过来的数据
     */
    private Boolean isTransferOut;

    /**
     * 发货店仓法人
     */
    private String belongsLegal;

    /**
     * 来源单据性质
     */
    private Long sourceBillProp;

    /**
     * 2020-04-15添加字段
     * 入库序号
     */
    private String inSeqNo;

    public SgPhyInPickUpGoodSaveRequest() {
        this.isTransferOut = false;
        this.inSeqNo = "0";
    }
}
