package com.jackrain.nea.sg.out.api;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.out.model.request.SgPhyOutNoticesBillSaveRequest;
import com.jackrain.nea.sg.out.model.result.SgOutNoticesResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;

import java.util.List;

/**
 * @author leexh
 * @since 2019/5/6 13:59
 */
public interface SgPhyOutNoticesBatchSaveAndWMSCmd {

    ValueHolderV14<List<SgOutNoticesResult>> batchSaveOutNoticesAndWMS(List<SgPhyOutNoticesBillSaveRequest> requests, User user) throws NDSException;
}
