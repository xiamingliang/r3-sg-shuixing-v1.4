package com.jackrain.nea.sg.out.model.request;

import com.jackrain.nea.sg.basic.model.table.CpCStore;
import com.jackrain.nea.web.face.User;
import lombok.Data;

import java.io.Serializable;
import java.util.Map;

/**
 * @author 舒威
 * @since 2019/11/22
 * create at : 2019/11/22 17:36
 */
@Data
public class SgPhyOutNoticesSaveRPCRequest implements Serializable {

    private SgPhyOutNoticesBillSaveRequest billSaveRequest;

    /**
     * 逻辑仓id - 若指定逻辑仓 逻辑发货单明细取改逻辑仓，否则默认取 通知单实体仓下主逻辑仓
     */
    private Long cpCStoreId;

    private String cpCStoreEcode;

    private String cpCStoreEname;

    private Map<Long, CpCStore> negativeStock;

    /**
     * 是否需要生成逻辑发货单
     */
    private Boolean sgSendFlag;

    private Boolean sgOutResultFlag;

    public SgPhyOutNoticesSaveRPCRequest() {
        this.sgSendFlag = true;
        this.sgOutResultFlag = true;
    }
}
