package com.jackrain.nea.sg.out.api;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.basic.model.result.SgR3BaseResult;
import com.jackrain.nea.sg.out.model.request.SgPhyOutNoticesBillVoidRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 * @author leexh
 * @since 2019/5/7 19:40
 */
public interface SgPhyOutNoticesBatchVoidAndWMSCmd {

    ValueHolderV14<SgR3BaseResult> batchVoidSgPhyOutNoticesAndWMS(SgPhyOutNoticesBillVoidRequest request) throws NDSException;

}
