package com.jackrain.nea.sg.inv.model.request;

import lombok.Data;

import java.io.Serializable;

/**
 * @author: 舒威
 * @since: 2019/5/13
 * create at : 2019/5/13 14:04
 */
@Data
public class ScInvProfitLossBasePageRequest implements Serializable {

    private Integer pageSize; //每页展示条数

    private Integer currentPage; //当前页

    private String name; //排序字段 默认创建日期

    private Boolean isasc; //排序方式 默认降序

    public ScInvProfitLossBasePageRequest() {
        this.currentPage = 1;
        this.pageSize = 10;
        this.name = "CREATIONDATE";
        this.isasc = false;
    }
}
