package com.jackrain.nea.sg.out.model.request;

import com.jackrain.nea.web.face.User;
import lombok.Data;

import java.io.Serializable;

/**
 * @author: 舒威
 * @since: 2019/9/2
 * create at : 2019/9/2 20:30
 */
@Data
public class SgOutResultMQRequest implements Serializable {

    /**
     * 出库结果单ID
     */
    private Long id;

    /**
     * 用户信息
     */
    private User loginUser;

    /**
     * 是否一键出库
     */
    private Boolean isOneClickOutLibrary;

    public SgOutResultMQRequest() {
        this.isOneClickOutLibrary = false;
    }
}
