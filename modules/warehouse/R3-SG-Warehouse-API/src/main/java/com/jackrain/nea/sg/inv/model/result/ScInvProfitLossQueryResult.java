package com.jackrain.nea.sg.inv.model.result;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author 舒威
 * @since 2019/3/27
 * create at : 2019/3/27 16:56
 */
@Data
public class ScInvProfitLossQueryResult implements Serializable {

    private List results; //结果集

    private Long total;//总条数

    private Integer pageSize; //每页多少条数据

    private Integer pageNo; //第几页

    private Long totalPages;//总页数

    private Integer topPageNo;//首页

    private Long previousPageNo;//上一页

    private Long nextPageNo;//下一页

    private Long bottomPageNo;//尾页

    /**
     * 总页数
     */
    public Long getTotalPages() {
        return (total + pageSize - 1) / pageSize;
    }

    /**
     * 取得首页
     */
    public int getTopPageNo() {
        return 1;
    }

    /**
     * 上一页
     */
    public Long getPreviousPageNo() {
        if (pageNo <= 1) {
            return 1L;
        }
        return pageNo - 1L;
    }

    /**
     * 下一页
     */
    public Long getNextPageNo() {
        if (pageNo >= getBottomPageNo()) {
            return getBottomPageNo();
        }
        return pageNo + 1L;
    }

    /**
     * 取得尾页
     */
    public Long getBottomPageNo() {
        return getTotalPages();
    }
}
