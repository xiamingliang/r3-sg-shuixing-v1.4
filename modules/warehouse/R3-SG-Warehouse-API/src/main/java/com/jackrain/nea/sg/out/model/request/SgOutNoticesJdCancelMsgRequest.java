package com.jackrain.nea.sg.out.model.request;

import com.jackrain.nea.web.face.User;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Author chenjinjun
 * @Description 出库通知单作废JD快递取消请求Bean
 * @Date  2019-10-9
 **/
@Data
public class SgOutNoticesJdCancelMsgRequest implements Serializable {

    private List<Long> resultList;

    /**
     * 用户信息
     */
    private User loginUser;
}