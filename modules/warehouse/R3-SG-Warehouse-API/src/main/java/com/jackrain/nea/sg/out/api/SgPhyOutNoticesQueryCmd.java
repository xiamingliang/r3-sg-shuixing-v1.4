package com.jackrain.nea.sg.out.api;

import com.jackrain.nea.sg.out.model.request.SgPhyOutNoticesQueryRequest;
import com.jackrain.nea.sg.out.model.request.SgPhyOutNoticesSaveRequest;
import com.jackrain.nea.sg.out.model.result.SgBPhyOutNoticesAllInfoResult;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutNotices;
import com.jackrain.nea.sys.domain.ValueHolderV14;

import java.util.List;

/**
 * @author leexh
 * @since 2019/5/15 15:06
 */
public interface SgPhyOutNoticesQueryCmd {

    ValueHolderV14<List<SgBPhyOutNotices>> queryOutNoticesBySource(List<SgPhyOutNoticesSaveRequest> requests);

    ValueHolderV14<List<SgBPhyOutNoticesAllInfoResult>> queryOutNoticesAllInfoBySource(SgPhyOutNoticesQueryRequest request);

}
