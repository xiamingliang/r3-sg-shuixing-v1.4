package com.jackrain.nea.sg.in.model.request;

import com.jackrain.nea.web.face.User;
import lombok.Data;

import java.io.Serializable;

/**
 * @author zhu lin yu
 * @since 2019-11-20
 * create at : 2019-11-20 16:47
 */
@Data
public class SgPhyInPickOrderAuditRequest implements Serializable {
    private Long inPickOrderId;

    private User loginUser;

    /**
     *  标记是否是调拨入库单过来的审核
     */
    private Boolean isTransferIn;

    public SgPhyInPickOrderAuditRequest(){
        this.isTransferIn = false;
    }
}
