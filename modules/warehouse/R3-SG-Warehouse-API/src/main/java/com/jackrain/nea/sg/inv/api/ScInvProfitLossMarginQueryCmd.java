package com.jackrain.nea.sg.inv.api;

import com.github.pagehelper.PageInfo;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.inv.model.request.ScInvProfitLossMarginQueryCmdRequest;
import com.jackrain.nea.sg.inv.model.result.ScInvProfitLossQueryItemResult;
import com.jackrain.nea.sg.inv.model.result.ScInvProfitLossMarginQueryResult;
import com.jackrain.nea.sg.inv.model.result.ScInvProfitLossQueryLastResult;
import com.jackrain.nea.sg.inv.model.table.ScBInventory;
import com.jackrain.nea.sg.inv.model.table.ScBPrePlUnfshBill;
import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 * 查询预盈亏
 *
 * @author 舒威
 * @since 2019/3/27
 * create at : 2019/3/27 16:46
 */
public interface ScInvProfitLossMarginQueryCmd {

    /**
     * 查询
    *
     * */
    ValueHolderV14<ScInvProfitLossMarginQueryResult> queryProfitLoss(ScInvProfitLossMarginQueryCmdRequest model) throws NDSException;


    /**
     * 查询最近一次未盈亏盘点单
     *
     * */
    ValueHolderV14<ScInvProfitLossQueryLastResult> queryLastProfitLossInventory(ScInvProfitLossMarginQueryCmdRequest model) throws NDSException;

    /**
     * 查询未盈亏盘点单
     *
     * */
    ValueHolderV14<PageInfo<ScBInventory>> queryProfitLossInventory(ScInvProfitLossMarginQueryCmdRequest model) throws NDSException;

    /**
     * 查询未完成单据
     *
     * */
    ValueHolderV14<PageInfo<ScBPrePlUnfshBill>> queryUnfinishProfitLoss(ScInvProfitLossMarginQueryCmdRequest model) throws NDSException;

    /**
     * 查询预盈亏明细
     *
     * */
    ValueHolderV14<ScInvProfitLossQueryItemResult> queryProfitLossItem(ScInvProfitLossMarginQueryCmdRequest model) throws NDSException;
}
