package com.jackrain.nea.sg.pack.model.request;

import com.jackrain.nea.sg.pack.model.table.SgBTeusPackItem;
import com.jackrain.nea.web.face.User;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author 周琳胜
 * @since 2019-10-23
 * create at : 2019-10-23 19:43
 */
@Data
public class SgPackSaveAndAuditRequest implements Serializable {

    //单据日期
    private Date biiiDate;

    //实体仓id
    private Long cpCPhyWarehouseId;

    //备注
    private String remark;

    // 拆箱明细
    private List<SgBTeusPackItem> itemList;

    //用户
    private User user;

}
