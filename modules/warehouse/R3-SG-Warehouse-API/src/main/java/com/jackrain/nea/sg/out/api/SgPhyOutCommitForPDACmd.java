package com.jackrain.nea.sg.out.api;

import com.jackrain.nea.sg.out.model.request.SgPhyOutCommitForPDARequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 * @author zhu lin yu
 * @since 2019-10-29
 * create at : 2019-10-29 14:11
 */
public interface SgPhyOutCommitForPDACmd {
    public ValueHolderV14 saveAndAuditOutResult(SgPhyOutCommitForPDARequest request);
}
