package com.jackrain.nea.sg.out.model.request;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author zhu lin yu
 * @since 2019-10-23
 * create at : 2019-10-23 19:43
 */
@Data
public class SgPhyOutNoticesQueryRequest implements Serializable {
    //出库通知单id
    private Long phyOutNoticesId;

    //单据类型
    private Integer sourceBillType;

    //单据状态
    private List<Integer> billStatusList;

    //单据编号
    private String billNo;

    //开始时间
    private Date startTime;

    //开始时间
    private Date endTime;

    //实体仓id
    private Long cpCPhyWarehouseId;

    //页数
    private int pageNum;

    //每页数量
    private int pageSize;

    //传wms状态
    private List<Long> wmsStatusList;

    //是否差异处理
    private Integer isDiffDeal;

    //来源单据编号
    private String sourceBillNo;

    //来源单据编号
    private Long sourceBillId;

    //是否查询查询条码明细
    private Boolean queryNoticesItem;

    //是否特定查询(传wms失败且<6次,或者未传wms)
    private Boolean isSpecialQuery;

    public SgPhyOutNoticesQueryRequest(){
        this.pageNum = 1;
        this.pageSize = 20;
        this.queryNoticesItem = false;
        this.isSpecialQuery = false;
    }
}
