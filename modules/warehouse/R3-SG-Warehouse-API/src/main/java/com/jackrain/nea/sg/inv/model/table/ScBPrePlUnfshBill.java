package com.jackrain.nea.sg.inv.model.table;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.*;
import com.jackrain.nea.es.annotations.Document;
import com.jackrain.nea.es.annotations.Field;
import com.jackrain.nea.es.constans.FieldType;
import com.jackrain.nea.sys.domain.BaseModel;
import lombok.Data;

import java.util.Date;

@TableName(value = "sc_b_pre_pl_unfsh_bill")
@Data
@Document(index = "sc_b_pre_pl_unfsh_bill", type = "sc_b_pre_pl_unfsh_bill")
public class ScBPrePlUnfshBill extends BaseModel {
    @JSONField(name = "ID")
    @TableField(fill = FieldFill.INSERT)
    @TableId(value = "id", type = IdType.INPUT)
    @Field(type = FieldType.Long)
    private Long id;

    @JSONField(name = "QUERY_ID")
    @Field(type = FieldType.Keyword)
    private String queryId;

    @JSONField(name = "BILL_NO")
    @Field(type = FieldType.Keyword)
    private String billNo;

    @JSONField(name = "CP_C_STORE_ID")
    @Field(type = FieldType.Long)
    private Long cpCStoreId;

    @JSONField(name = "CP_C_STORE_ECODE")
    @Field(type = FieldType.Keyword)
    private String cpCStoreEcode;

    @JSONField(name = "CP_C_STORE_ENAME")
    @Field(type = FieldType.Keyword)
    private String cpCStoreEname;

    @JSONField(name = "BILL_DATE",format = "yyyy-MM-dd")
    @Field(type = FieldType.Date)
    private Date billDate;

    @JSONField(name = "BILL_TYPE")
    @Field(type = FieldType.Keyword)
    private String billType;

    @JSONField(name = "BILL_TYPE_NAME")
    @Field(type = FieldType.Keyword)
    private String billTypeName;

    @JSONField(name = "STATUS")
    @Field(type = FieldType.Integer)
    private Integer status;

    @JSONField(name = "STATUS_NAME")
    @Field(type = FieldType.Keyword)
    private String statusName;

    @JSONField(name = "REMARK")
    @Field(type = FieldType.Keyword)
    private String remark;

    @JSONField(name = "VERSION")
    @Field(type = FieldType.Long)
    private Long version;

    @JSONField(name = "OWNERENAME")
    @Field(type = FieldType.Keyword)
    private String ownerename;

    @JSONField(name = "MODIFIERENAME")
    @Field(type = FieldType.Keyword)
    private String modifierename;
}