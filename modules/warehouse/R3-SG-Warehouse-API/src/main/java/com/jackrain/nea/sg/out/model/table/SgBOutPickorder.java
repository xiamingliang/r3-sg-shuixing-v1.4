package com.jackrain.nea.sg.out.model.table;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.*;
import com.jackrain.nea.es.annotations.Document;
import com.jackrain.nea.es.annotations.Field;
import com.jackrain.nea.es.constans.FieldType;
import com.jackrain.nea.sys.domain.BaseModelES;
import lombok.Data;

import java.util.Date;


@TableName(value = "sg_b_out_pickorder")
@Data
@Document(index = "sg_b_out_pickorder", type = "sg_b_out_pickorder")
public class SgBOutPickorder extends BaseModelES {
    @JSONField(name = "ID")
    @TableField(fill = FieldFill.INSERT)
    @TableId(value = "id", type = IdType.INPUT)
    @Field(type = FieldType.Long)
    private Long id;

    @JSONField(name = "BILL_NO")
    @Field(type = FieldType.Keyword)
    private String billNo;

    @JSONField(name = "BILL_DATE")
    @Field(type = FieldType.Long)
    private Date billDate;

    @JSONField(name = "CP_C_ORIG_ID")
    @Field(type = FieldType.Long)
    private Long cpCOrigId;

    @JSONField(name = "CP_C_ORIG_ECODE")
    @Field(type = FieldType.Keyword)
    private String cpCOrigEcode;

    @JSONField(name = "CP_C_ORIG_ENAME")
    @Field(type = FieldType.Keyword)
    private String cpCOrigEname;

    @JSONField(name = "CP_C_DEST_ID")
    @Field(type = FieldType.Long)
    private Long cpCDestId;

    @JSONField(name = "CP_C_DEST_ECODE")
    @Field(type = FieldType.Keyword)
    private String cpCDestEcode;

    @JSONField(name = "CP_C_DEST_ENAME")
    @Field(type = FieldType.Keyword)
    private String cpCDestEname;

    @JSONField(name = "BILL_STATUS")
    @Field(type = FieldType.Integer)
    private Integer billStatus;

    @JSONField(name = "DELER_ID")
    @Field(type = FieldType.Long)
    private Long delerId;

    @JSONField(name = "DELER_NAME")
    @Field(type = FieldType.Keyword)
    private String delerName;

    @JSONField(name = "DELER_ENAME")
    @Field(type = FieldType.Keyword)
    private String delerEname;

    @JSONField(name = "DEL_TIME")
    @Field(type = FieldType.Long)
    private Date delTime;

    @JSONField(name = "DEL_STATUS")
    @Field(type = FieldType.Integer)
    private Integer delStatus;

    @JSONField(name = "REMARK")
    @Field(type = FieldType.Keyword)
    private String remark;

    @JSONField(name = "VERSION")
    @Field(type = FieldType.Long)
    private Long version;

    @JSONField(name = "AD_ORG_ID")
    @Field(type = FieldType.Long)
    private Long adOrgId;

    @JSONField(name = "ISACTIVE")
    @Field(type = FieldType.Keyword)
    private String isactive;

    @JSONField(name = "AD_CLIENT_ID")
    @Field(type = FieldType.Long)
    private Long adClientId;

    @JSONField(name = "OWNERID")
    @Field(type = FieldType.Long)
    private Long ownerid;

    @JSONField(name = "OWNERENAME")
    @Field(type = FieldType.Keyword)
    private String ownerename;

    @JSONField(name = "OWNERNAME")
    @Field(type = FieldType.Keyword)
    private String ownername;

    @JSONField(name = "CREATIONDATE")
    @Field(type = FieldType.Long)
    private Date creationdate;

    @JSONField(name = "MODIFIERID")
    @Field(type = FieldType.Long)
    private Long modifierid;

    @JSONField(name = "MODIFIERENAME")
    @Field(type = FieldType.Keyword)
    private String modifierename;

    @JSONField(name = "MODIFIERNAME")
    @Field(type = FieldType.Keyword)
    private String modifiername;

    @JSONField(name = "MODIFIEDDATE")
    @Field(type = FieldType.Long)
    private Date modifieddate;

    @JSONField(name = "STATUS_ID")
    @Field(type = FieldType.Integer)
    private Integer statusId;

    @JSONField(name = "STATUS_ENAME")
    @Field(type = FieldType.Keyword)
    private String statusEname;

    @JSONField(name = "STATUS_NAME")
    @Field(type = FieldType.Keyword)
    private String statusName;

    @JSONField(name = "STATUS_TIME")
    @Field(type = FieldType.Long)
    private Date statusTime;
}