package com.jackrain.nea.sg.in.api;

import com.jackrain.nea.sys.Command;

/**
 * @author leexh
 * @since 2019/6/24 19:43
 * desc: 重传WMS服务
 */
public interface SgPhyInNoticesSendWMSAgainCmd  extends Command {
}
