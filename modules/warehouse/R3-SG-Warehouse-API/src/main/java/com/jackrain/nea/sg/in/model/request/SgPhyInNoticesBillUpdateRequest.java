package com.jackrain.nea.sg.in.model.request;

import com.jackrain.nea.web.face.User;
import lombok.Data;

import java.util.List;

/**
 * @author: 舒威
 * @since: 2019/11/20
 * create at : 2019/11/20 10:14
 */
@Data
public class SgPhyInNoticesBillUpdateRequest {

    /**
     *   通知单单据编号
     */
   private List<String> noticesBillNos;

   private User loginUser;
}
