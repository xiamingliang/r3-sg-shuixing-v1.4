package com.jackrain.nea.sg.phyadjust.api;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.phyadjust.model.request.SgPhyAdjustBillAuditRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 * @author csy
 * Date: 2019/4/29
 * Description:
 */
public interface SgPhyAdjustAuditCmd {

    /**
     * 库存调整单保存（实体仓）-接口调用
     *
     * @param request request
     * @return result
     * @throws NDSException 框架封装异常
     */
    ValueHolderV14 audit(SgPhyAdjustBillAuditRequest request) throws NDSException;
}
