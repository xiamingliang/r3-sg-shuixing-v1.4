package com.jackrain.nea.sg.inv.model.request;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author 舒威
 * @since 2019/12/5
 * create at : 2019/12/5 10:35
 */
@Data
public class ScInvProfitLossGenarateRequest implements Serializable{

    @JSONField(name = "QTY_PROFIT")
    private BigDecimal qtyProfit; //散件盘盈数量

    @JSONField(name = "QTY_LOSS")
    private BigDecimal qtyLoss; //散件盘亏数量

    @JSONField(name = "TEUS_QTY_PROFIT")
    private BigDecimal teusQtyProfit; //箱盘盈数量

    @JSONField(name = "TEUS_QTY_LOSS")
    private BigDecimal teusQtyLoss; //箱盘亏数量

    @JSONField(name = "ADJ_PROP")
    private Long adjProp;  //调整类型

    @JSONField(name = "INVENTORY_PROP")
    private Long inventoryProp; //盘点性质

    @JSONField(name = "UNFINISH_BILL_TYPE")
    private List<String> unfshBillType; //未完成单据check单据类型

}
