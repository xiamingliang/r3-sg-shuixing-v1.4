package com.jackrain.nea.sg.out.model.request;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author zhu lin yu
 * @since 2019-11-22
 * create at : 2019-11-22 13:59
 */
@Data
public class SgOutPickorderResultItemSaveRequest implements Serializable {
    private Long id;

    private Long sgBOutPickorderId;

    private String billNo;

    private Date billDate;

    private Long cpCPhyWarehouseId;

    private String cpCPhyWarehouseEcode;

    private String cpCPhyWarehouseEname;

    private Long cpCPhyWarehouseDestId;

    private String cpCPhyWarehouseDestEcode;

    private String cpCPhyWarehouseDestEname;

}
