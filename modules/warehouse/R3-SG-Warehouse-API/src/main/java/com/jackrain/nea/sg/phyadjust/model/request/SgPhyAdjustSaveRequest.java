package com.jackrain.nea.sg.phyadjust.model.request;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.io.Serializable;

/**
 * @author csy
 * Date: 2019/4/29
 * Description:
 */
@Data
public class SgPhyAdjustSaveRequest implements Serializable {
    /**
     * 单据编号
     * 非必传 (不传自动取值 传入按传入单据编号赋值)
     */
    @JSONField(name = "BILL_NO")
    private String billNo;

    /**
     * 实体仓id
     * 必传
     */
    @JSONField(name = "CP_C_PHY_WAREHOUSE_ID")
    private Long cpCPhyWarehouseId;

    /**
     * 实体仓编码
     * 必传
     */
    @JSONField(name = "CP_C_PHY_WAREHOUSE_ECODE")
    private String cpCPhyWarehouseEcode;

    /**
     * 实体仓名称
     * 必传
     */
    @JSONField(name = "CP_C_PHY_WAREHOUSE_ENAME")
    private String cpCPhyWarehouseEname;


    /**
     * 单据类型
     * 静态类 SgPhyAdjustConstantsIF中找到对应值
     * 必传
     */
    @JSONField(name = "BILL_TYPE")
    private Integer billType;


    /**
     * 调整单性质id (扩展属性？现在不知道是干啥的)
     * 非必传
     */
    @JSONField(name = "SG_B_ADJUST_PROP_ID")
    private Long sgBAdjustPropId;


    /**
     * 来源单据类型
     * 静态类 SgPhyAdjustConstantsIF中找到对应值
     * 必传
     */
    @JSONField(name = "SOURCE_BILL_TYPE")
    private Integer sourceBillType;

    /**
     * 来源单据id
     * 必传
     */
    @JSONField(name = "SOURCE_BILL_ID")
    private Long sourceBillId;

    /**
     * 来源单据编号（没有的话不必传）
     * 非必传
     */
    @JSONField(name = "SOURCE_BILL_NO")
    private String sourceBillNo;

    /**
     * wms单据编号
     * 非必传
     */
    @JSONField(name = "WMS_BILL_NO")
    private String wmsBillNo;

    /**
     * 备注
     * 非必传
     */
    @JSONField(name = "REMARK")
    private String remark;

    /**
     * 责任人
     * 非必传
     */
    @JSONField(name = "PERSON_LIABLE")
    private String personLiable;

    @JSONField(name = "FULL_LINK")
    private String fullLink;

    /**
     * 逻辑仓ID
     * 非必传
     */
    @JSONField(name = "CP_C_STORE_ID")
    private Long cpCStoreId;

    /**
     * 逻辑仓ECODE
     * 非必传
     */
    @JSONField(name = "CP_C_STORE_ECODE")
    private String cpCStoreEcode;

    /**
     * 逻辑仓ENAME
     * 非必传
     */
    @JSONField(name = "CP_C_STORE_ENAME")
    private String cpCStoreEname;

    /**
     * 商品标识
     */
    @JSONField(name = "TRADE_MARK")
    private String tradeMark;

    /**
     * 【退货地址】
     *  非必传
     */
    @JSONField(name = "RESERVE_VARCHAR01")
    private String reserveVarchar01;

    /**
     * 【退货物流】
     * 非必传
     */
    @JSONField(name = "RESERVE_VARCHAR02")
    private String reserveVarchar02;

    /**
     * 【退货入库单ID】
     * 非必传
     */
    @JSONField(name = "OC_B_REFUND_IN_ID")
    private Long ocBRefundInId;
}
