package com.jackrain.nea.sg.in.api;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.in.model.request.SgPhyInResultBillSaveRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 *  入库结果单
 * @author jiang.cj
 * @since 2019-4-22
 * create at : 2019-4-22
 */
public interface SgPhyInResultSaveCmd {


     ValueHolderV14 saveSgBPhyInResult(SgPhyInResultBillSaveRequest request) throws NDSException;
}
