package com.jackrain.nea.sg.phyadjust.model.result;

import com.jackrain.nea.sg.phyadjust.model.table.SgBPhyAdjust;
import com.jackrain.nea.sg.phyadjust.model.table.SgBPhyAdjustImpItem;
import com.jackrain.nea.sg.phyadjust.model.table.SgBPhyAdjustItem;
import com.jackrain.nea.sg.phyadjust.model.table.SgBPhyAdjustTeusItem;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author: 舒威
 * @since: 2019/7/29
 * create at : 2019/7/29 22:21
 */
@Data
public class SgPhyAdjustBillQueryResult implements Serializable {

    private SgBPhyAdjust adjust;

    private List<SgBPhyAdjustItem> adjustItems;

    private List<SgBPhyAdjustImpItem> adjustImpItems;

    private List<SgBPhyAdjustTeusItem> adjustTeusItems;

    private String redisKey;
}
