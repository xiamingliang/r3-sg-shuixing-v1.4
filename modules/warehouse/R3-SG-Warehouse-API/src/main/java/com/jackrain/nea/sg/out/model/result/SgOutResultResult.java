package com.jackrain.nea.sg.out.model.result;

import com.jackrain.nea.sg.out.model.table.SgBPhyOutResult;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutResultItem;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author leexh
 * @since 2019/6/5 10:59
 * desc:
 */
@Data
public class SgOutResultResult implements Serializable {

    /**
     * 出库结果单
     */
    private SgBPhyOutResult outResult;

    /**
     * 出库结果单明细
     */
    private List<SgBPhyOutResultItem> items;
}
