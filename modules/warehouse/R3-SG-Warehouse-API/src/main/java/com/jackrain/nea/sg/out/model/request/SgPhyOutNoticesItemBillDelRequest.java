package com.jackrain.nea.sg.out.model.request;

import com.alibaba.fastjson.JSONArray;
import com.jackrain.nea.web.face.User;
import lombok.Data;

import java.io.Serializable;

/**
 * @author leexh
 * @since 2019/4/24 10:08
 */
@Data
public class SgPhyOutNoticesItemBillDelRequest implements Serializable {

    /**
     * 主单据ID
     */
    private Long objId;

    /**
     * 主单据对象（接口调用时，传来源单据信息）
     */
    private SgPhyOutNoticesSaveRequest noticesRequest;

    /**
     * 明细ID集合
     */
    private JSONArray ids;

    /**
     * 来源单据明细ID集合
     */
    private JSONArray sourceBillItemIds;

    /**
     * 用户信息
     */
    private User loginUser;
}
