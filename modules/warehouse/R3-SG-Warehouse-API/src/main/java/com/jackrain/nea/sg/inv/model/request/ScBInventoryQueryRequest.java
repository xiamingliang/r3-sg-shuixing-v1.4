package com.jackrain.nea.sg.inv.model.request;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author 周琳胜
 * @since 2019-10-23
 * create at : 2019-10-23 19:43
 */
@Data
public class ScBInventoryQueryRequest implements Serializable {

    //盘点类型
    private Integer inventoryType;

    //盈亏状态
    private Integer polStatus;

    //盘点日期
    private Date inventoryDate;

    // 开始时间
    private Date startTime;

    // 结束时间
    private Date endTime;

    // 单据编号
    private String billNo;

    //页数
    private int pageNum;

    //每页数量
    private int pageSize;

    // 主表id
    private Long objId;

    // 是否查明细
    private Boolean isSelectItem;

    //条码编号
    private String itemSkuEcode;

    //箱号
    private String itemTeusEcode;

    public ScBInventoryQueryRequest(){
        this.pageNum = 1;
        this.pageSize = 20;
    }
}
