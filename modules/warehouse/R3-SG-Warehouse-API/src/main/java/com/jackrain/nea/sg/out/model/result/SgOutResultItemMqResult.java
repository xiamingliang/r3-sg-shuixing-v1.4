package com.jackrain.nea.sg.out.model.result;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author leexh
 * @since 2019/5/11 15:06
 */
@Data
public class SgOutResultItemMqResult implements Serializable {

    private static final long serialVersionUID = -3640864045635856322L;

    /**
     * 出库结果单明细ID
     */
    private Long id;

    /**
     * 出库结果单ID
     */
    private Long sgBPhyOutResultId;

    /**
     * 条码ID
     */
    private Long psCSkuId;

    /**
     * 条码编码
     */
    private String psCSkuEcode;

    /**
     * 商品ID
     */
    private Long psCProId;

    /**
     * 商品编码
     */
    private String psCProEcode;

    /**
     * 出库数量
     */
    private BigDecimal qty;

    /**
     * 国标码
     */
    private String gbcode;

    private SgOutResultToJITXItem jitxItem;

}
