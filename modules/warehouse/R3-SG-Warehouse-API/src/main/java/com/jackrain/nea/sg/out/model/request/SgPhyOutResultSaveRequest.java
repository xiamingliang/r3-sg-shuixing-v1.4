package com.jackrain.nea.sg.out.model.request;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author leexh
 * @since 2019/4/23 11:34
 */
@Data
public class SgPhyOutResultSaveRequest implements Serializable {

    private Long id;

    private Long cpCPhyWarehouseId;

    private String cpCPhyWarehouseEcode;

    private String cpCPhyWarehouseEname;

    private String billNo;

    private String goodsOwner;

    private Date billDate;

    private Integer billStatus;

    private Date outTime;

    private Long outId;

    private String outEcode;

    private String outEname;

    private String outName;

    private Long cpCCustomerWarehouseId;

    private String cpCCsEcode;

    private String cpCCsEname;

    private Long cpCSupplierId;

    private Integer outType;

    private Integer sourceBillType;

    private Long sourceBillId;

    private String sourceBillNo;

    private Long sgBPhyOutNoticesId;

    private String receiverName;

    private String receiverMobile;

    private String receiverPhone;

    private Long cpCRegionProvinceId;

    private String cpCRegionProvinceEcode;

    private String cpCRegionProvinceEname;

    private Long cpCRegionCityId;

    private String cpCRegionCityEcode;

    private String cpCRegionCityEname;

    private Long cpCRegionAreaId;

    private String cpCRegionAreaEcode;

    private String cpCRegionAreaEname;

    private String receiverAddress;

    private String receiverZip;

    private Long cpCLogisticsId;

    private String cpCLogisticsEcode;

    private String cpCLogisticsEname;

    private String logisticNumber;

    private String deliveryMethod;

    private BigDecimal amtPayment;

    private Integer isUrgent;

    private Long cpCShopId;

    private String cpCShopTitle;

    private String sourcecode;

    private String sellerRemark;

    private String buyerRemark;

    private String remark;

    private BigDecimal totQtyOut;

    private BigDecimal totAmtListOut;

    private String wmsBillNo;

    private String ownerename;

    private String modifierename;

    private String sgBPhyOutNoticesBillno;

    private Integer isLast;

    private BigDecimal theoreticalWeight;

    private BigDecimal actualWeight;

}
