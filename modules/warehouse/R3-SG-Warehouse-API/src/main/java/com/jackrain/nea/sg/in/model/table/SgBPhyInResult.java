package com.jackrain.nea.sg.in.model.table;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.*;
import com.jackrain.nea.es.annotations.Document;
import com.jackrain.nea.es.annotations.Field;
import com.jackrain.nea.es.constans.FieldType;
import com.jackrain.nea.sys.domain.BaseModelES;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@TableName(value = "sg_b_phy_in_result")
@Data
@Document(index = "sg_b_phy_in_result",type = "sg_b_phy_in_result")
public class SgBPhyInResult extends BaseModelES {
    @JSONField(name = "ID")
    @TableField(fill = FieldFill.INSERT)
    @TableId(value = "id", type = IdType.INPUT)
    @Field(type = FieldType.Long)
    private Long id;

    @JSONField(name = "CP_C_PHY_WAREHOUSE_ID")
    @Field(type = FieldType.Long)
    private Long cpCPhyWarehouseId;

    @JSONField(name = "CP_C_PHY_WAREHOUSE_ECODE")
    @Field(type = FieldType.Keyword)
    private String cpCPhyWarehouseEcode;

    @JSONField(name = "CP_C_PHY_WAREHOUSE_ENAME")
    @Field(type = FieldType.Keyword)
    private String cpCPhyWarehouseEname;

    @JSONField(name = "BILL_NO")
    @Field(type = FieldType.Keyword)
    private String billNo;

    @JSONField(name = "SG_B_PHY_IN_NOTICES_ID")
    @Field(type = FieldType.Long)
    private Long sgBPhyInNoticesId;

    @JSONField(name = "GOODS_OWNER")
    @Field(type = FieldType.Keyword)
    private String goodsOwner;

    @JSONField(name = "BILL_DATE")
    @Field(type = FieldType.Long)
    private Date billDate;

    @JSONField(name = "BILL_STATUS")
    @Field(type = FieldType.Integer)
    private Integer billStatus;

    @JSONField(name = "IN_TIME")
    @Field(type = FieldType.Long)
    private Date inTime;

    @JSONField(name = "IN_ID")
    @Field(type = FieldType.Long)
    private Long inId;

    @JSONField(name = "IN_ECODE")
    @Field(type = FieldType.Keyword)
    private String inEcode;

    @JSONField(name = "IN_ENAME")
    @Field(type = FieldType.Keyword)
    private String inEname;

    @JSONField(name = "IN_NAME")
    @Field(type = FieldType.Keyword)
    private String inName;

    @JSONField(name = "IN_TYPE")
    @Field(type = FieldType.Integer)
    private Integer inType;

    @JSONField(name = "CP_C_CUSTOMER_WAREHOUSE_ID")
    @Field(type = FieldType.Long)
    private Long cpCCustomerWarehouseId;

    @JSONField(name = "CP_C_CS_ECODE")
    @Field(type = FieldType.Keyword)
    private String cpCCsEcode;

    @JSONField(name = "CP_C_CS_ENAME")
    @Field(type = FieldType.Keyword)
    private String cpCCsEname;

    @JSONField(name = "CP_C_SUPPLIER_ID")
    @Field(type = FieldType.Long)
    private Long cpCSupplierId;

    @JSONField(name = "SOURCE_BILL_TYPE")
    @Field(type = FieldType.Integer)
    private Integer sourceBillType;

    @JSONField(name = "SOURCE_BILL_ID")
    @Field(type = FieldType.Long)
    private Long sourceBillId;

    @JSONField(name = "SOURCE_BILL_NO")
    @Field(type = FieldType.Keyword)
    private String sourceBillNo;

    @JSONField(name = "SEND_NAME")
    @Field(type = FieldType.Keyword)
    private String sendName;

    @JSONField(name = "SEND_MOBILE")
    @Field(type = FieldType.Keyword)
    private String sendMobile;

    @JSONField(name = "SEND_PHONE")
    @Field(type = FieldType.Keyword)
    private String sendPhone;

    @JSONField(name = "CP_C_REGION_PROVINCE_ID")
    @Field(type = FieldType.Long)
    private Long cpCRegionProvinceId;

    @JSONField(name = "CP_C_REGION_PROVINCE_ECODE")
    @Field(type = FieldType.Keyword)
    private String cpCRegionProvinceEcode;

    @JSONField(name = "CP_C_REGION_PROVINCE_ENAME")
    @Field(type = FieldType.Keyword)
    private String cpCRegionProvinceEname;

    @JSONField(name = "CP_C_REGION_CITY_ID")
    @Field(type = FieldType.Long)
    private Long cpCRegionCityId;

    @JSONField(name = "CP_C_REGION_CITY_ECODE")
    @Field(type = FieldType.Keyword)
    private String cpCRegionCityEcode;

    @JSONField(name = "CP_C_REGION_CITY_ENAME")
    @Field(type = FieldType.Keyword)
    private String cpCRegionCityEname;

    @JSONField(name = "CP_C_REGION_AREA_ID")
    @Field(type = FieldType.Long)
    private Long cpCRegionAreaId;

    @JSONField(name = "CP_C_REGION_AREA_ECODE")
    @Field(type = FieldType.Keyword)
    private String cpCRegionAreaEcode;

    @JSONField(name = "CP_C_REGION_AREA_ENAME")
    @Field(type = FieldType.Keyword)
    private String cpCRegionAreaEname;

    @JSONField(name = "SEND_ADDRESS")
    @Field(type = FieldType.Keyword)
    private String sendAddress;

    @JSONField(name = "SEND_ZIP")
    @Field(type = FieldType.Keyword)
    private String sendZip;

    @JSONField(name = "CP_C_LOGISTICS_ID")
    @Field(type = FieldType.Long)
    private Long cpCLogisticsId;

    @JSONField(name = "CP_C_LOGISTICS_ECODE")
    @Field(type = FieldType.Keyword)
    private String cpCLogisticsEcode;

    @JSONField(name = "CP_C_LOGISTICS_ENAME")
    @Field(type = FieldType.Keyword)
    private String cpCLogisticsEname;

    @JSONField(name = "LOGISTIC_NUMBER")
    @Field(type = FieldType.Keyword)
    private String logisticNumber;

    @JSONField(name = "CP_C_SHOP_ID")
    @Field(type = FieldType.Long)
    private Long cpCShopId;

    @JSONField(name = "CP_C_SHOP_TITLE")
    @Field(type = FieldType.Keyword)
    private String cpCShopTitle;

    @JSONField(name = "SOURCECODE")
    @Field(type = FieldType.Keyword)
    private String sourcecode;

    @JSONField(name = "SELLER_REMARK")
    @Field(type = FieldType.Keyword)
    private String sellerRemark;

    @JSONField(name = "BUYER_REMARK")
    @Field(type = FieldType.Keyword)
    private String buyerRemark;

    @JSONField(name = "TOT_QTY_IN")
    @Field(type = FieldType.Double)
    private BigDecimal totQtyIn;

    @JSONField(name = "TOT_AMT_LIST_IN")
    @Field(type = FieldType.Double)
    private BigDecimal totAmtListIn;

    @JSONField(name = "WMS_BILL_NO")
    @Field(type = FieldType.Keyword)
    private String wmsBillNo;

    @JSONField(name = "REMARK")
    @Field(type = FieldType.Keyword)
    private String remark;

    @JSONField(name = "PROPERTY1")
    @Field(type = FieldType.Keyword)
    private String property1;

    @JSONField(name = "PROPERTY2")
    @Field(type = FieldType.Keyword)
    private String property2;

    @JSONField(name = "PROPERTY3")
    @Field(type = FieldType.Keyword)
    private String property3;

    @JSONField(name = "PROPERTY4")
    @Field(type = FieldType.Keyword)
    private String property4;

    @JSONField(name = "PROPERTY5")
    @Field(type = FieldType.Keyword)
    private String property5;

    @JSONField(name = "PROPERTY6")
    @Field(type = FieldType.Keyword)
    private String property6;

    @JSONField(name = "PROPERTY7")
    @Field(type = FieldType.Keyword)
    private String property7;

    @JSONField(name = "PROPERTY8")
    @Field(type = FieldType.Keyword)
    private String property8;

    @JSONField(name = "PROPERTY9")
    @Field(type = FieldType.Keyword)
    private String property9;

    @JSONField(name = "PROPERTY10")
    @Field(type = FieldType.Keyword)
    private String property10;

    @JSONField(name = "AD_CLIENT_ID")
    @Field(type = FieldType.Long)
    private Long adClientId;

    @JSONField(name = "AD_ORG_ID")
    @Field(type = FieldType.Long)
    private Long adOrgId;

    @JSONField(name = "ISACTIVE")
    @Field(type = FieldType.Keyword)
    private String isactive;

    @JSONField(name = "OWNERID")
    @Field(type = FieldType.Long)
    private Long ownerid;

    @JSONField(name = "OWNERENAME")
    @Field(type = FieldType.Keyword)
    private String ownerename;

    @JSONField(name = "OWNERNAME")
    @Field(type = FieldType.Keyword)
    private String ownername;

    @JSONField(name = "CREATIONDATE")
    @Field(type = FieldType.Long)
    private Date creationdate;

    @JSONField(name = "MODIFIERID")
    @Field(type = FieldType.Long)
    private Long modifierid;

    @JSONField(name = "MODIFIERENAME")
    @Field(type = FieldType.Keyword)
    private String modifierename;

    @JSONField(name = "MODIFIERNAME")
    @Field(type = FieldType.Keyword)
    private String modifiername;

    @JSONField(name = "MODIFIEDDATE")
    @Field(type = FieldType.Long)
    private Date modifieddate;

    @JSONField(name = "IS_LAST")
    @Field(type = FieldType.Integer)
    private Integer isLast;

    @JSONField(name = "SG_B_PHY_IN_NOTICES_BILLNO")
    @Field(type = FieldType.Keyword)
    private String sgBPhyInNoticesBillno;

    @JSONField(name = "DELER_ID")
    @Field(type = FieldType.Long)
    private Long delerId;

    @JSONField(name = "DELER_ENAME")
    @Field(type = FieldType.Keyword)
    private String delerEname;

    @JSONField(name = "DELER_NAME")
    @Field(type = FieldType.Keyword)
    private String delerName;

    @JSONField(name = "DEL_TIME")
    @Field(type = FieldType.Long)
    private Date delTime;

    @JSONField(name = "RESERVE_BIGINT01")
    @Field(type = FieldType.Long)
    private Long reserveBigint01;

    @JSONField(name = "RESERVE_BIGINT02")
    @Field(type = FieldType.Long)
    private Long reserveBigint02;

    @JSONField(name = "RESERVE_BIGINT03")
    @Field(type = FieldType.Long)
    private Long reserveBigint03;

    @JSONField(name = "RESERVE_BIGINT04")
    @Field(type = FieldType.Long)
    private Long reserveBigint04;

    @JSONField(name = "RESERVE_BIGINT05")
    @Field(type = FieldType.Long)
    private Long reserveBigint05;

    @JSONField(name = "RESERVE_BIGINT06")
    @Field(type = FieldType.Long)
    private Long reserveBigint06;

    @JSONField(name = "RESERVE_BIGINT07")
    @Field(type = FieldType.Long)
    private Long reserveBigint07;

    @JSONField(name = "RESERVE_BIGINT08")
    @Field(type = FieldType.Long)
    private Long reserveBigint08;

    @JSONField(name = "RESERVE_BIGINT09")
    @Field(type = FieldType.Long)
    private Long reserveBigint09;

    @JSONField(name = "RESERVE_BIGINT10")
    @Field(type = FieldType.Long)
    private Long reserveBigint10;

    @JSONField(name = "RESERVE_VARCHAR01")
    @Field(type = FieldType.Keyword)
    private String reserveVarchar01;

    @JSONField(name = "RESERVE_VARCHAR02")
    @Field(type = FieldType.Keyword)
    private String reserveVarchar02;

    @JSONField(name = "RESERVE_VARCHAR03")
    @Field(type = FieldType.Keyword)
    private String reserveVarchar03;

    @JSONField(name = "RESERVE_VARCHAR04")
    @Field(type = FieldType.Keyword)
    private String reserveVarchar04;

    @JSONField(name = "RESERVE_VARCHAR05")
    @Field(type = FieldType.Keyword)
    private String reserveVarchar05;

    @JSONField(name = "RESERVE_VARCHAR06")
    @Field(type = FieldType.Keyword)
    private String reserveVarchar06;

    @JSONField(name = "RESERVE_VARCHAR07")
    @Field(type = FieldType.Keyword)
    private String reserveVarchar07;

    @JSONField(name = "RESERVE_VARCHAR08")
    @Field(type = FieldType.Keyword)
    private String reserveVarchar08;

    @JSONField(name = "RESERVE_VARCHAR09")
    @Field(type = FieldType.Keyword)
    private String reserveVarchar09;

    @JSONField(name = "RESERVE_VARCHAR10")
    @Field(type = FieldType.Keyword)
    private String reserveVarchar10;

    @JSONField(name = "RESERVE_DECIMAL01")
    @Field(type = FieldType.Double)
    private BigDecimal reserveDecimal01;

    @JSONField(name = "RESERVE_DECIMAL02")
    @Field(type = FieldType.Double)
    private BigDecimal reserveDecimal02;

    @JSONField(name = "RESERVE_DECIMAL03")
    @Field(type = FieldType.Double)
    private BigDecimal reserveDecimal03;

    @JSONField(name = "RESERVE_DECIMAL04")
    @Field(type = FieldType.Double)
    private BigDecimal reserveDecimal04;

    @JSONField(name = "RESERVE_DECIMAL05")
    @Field(type = FieldType.Double)
    private BigDecimal reserveDecimal05;

    @JSONField(name = "RESERVE_DECIMAL06")
    @Field(type = FieldType.Double)
    private BigDecimal reserveDecimal06;

    @JSONField(name = "RESERVE_DECIMAL07")
    @Field(type = FieldType.Double)
    private BigDecimal reserveDecimal07;

    @JSONField(name = "RESERVE_DECIMAL08")
    @Field(type = FieldType.Double)
    private BigDecimal reserveDecimal08;

    @JSONField(name = "RESERVE_DECIMAL09")
    @Field(type = FieldType.Double)
    private BigDecimal reserveDecimal09;

    @JSONField(name = "RESERVE_DECIMAL10")
    @Field(type = FieldType.Double)
    private BigDecimal reserveDecimal10;
}