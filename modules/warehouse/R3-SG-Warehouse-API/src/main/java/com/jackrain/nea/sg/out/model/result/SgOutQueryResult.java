package com.jackrain.nea.sg.out.model.result;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author leexh
 * @since 2019/5/30 17:50
 */
@Data
public class SgOutQueryResult implements Serializable {

    /**
     * 出库通知单
     */
    private SgOutNoticesByBillNoResult notices;

    /**
     * 出库结果单集合
     */
    private List<SgOutResultResult> results;

    /**
     * 出库结果单数量
     */
    private int outResultNum;
}
