package com.jackrain.nea.sg.in.model.request;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;


@Data
public class SgPhyInResultTeusItemSaveRequest implements Serializable {
    private Long id;

    private Long sgBPhyInResultId;

    private Long psCTeusId;

    private String psCTeusEcode;

    private Long psCMatchsizeId;

    private String psCMatchsizeEcode;

    private String psCMatchsizeEname;

    private Long psCSkuId;

    private String psCSkuEcode;

    private Long psCProId;

    private String psCProEcode;

    private String psCProEname;

    private Long psCSpec1Id;

    private String psCSpec1Ecode;

    private String psCSpec1Ename;

    private Long psCSpec2Id;

    private String psCSpec2Ecode;

    private String psCSpec2Ename;

    private BigDecimal qty;

    private BigDecimal priceList;

}