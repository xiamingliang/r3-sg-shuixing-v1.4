package com.jackrain.nea.sg.out.model.request;

import lombok.Data;

import java.io.Serializable;

/**
 * @author leexh
 * @since 2019/5/6 15:50
 */
@Data
public class SgPhyOutNoticesBillWMSBackRequest implements Serializable {

    /**
     * 出库通知单号
     */
    private String orderNo;

    /**
     * 返回状态
     */
    private int code;

    /**
     * 返回信息
     */
    private String message;

    /**
     * 是否是大货出库
     */
    private Integer flag;

    /**
     * WMS单据编号
     */
    private String deliveryOrderId;
}
