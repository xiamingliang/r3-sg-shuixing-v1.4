package com.jackrain.nea.sg.out.api;

import com.jackrain.nea.sg.out.model.request.SgPhyOutCommitForPosRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 * @author 舒威
 * @since 2019/11/21
 * create at : 2019/11/21 14:18
 */
public interface SgPhyOutCommitForPosCmd {

    ValueHolderV14 saveAndAuditOutBills(SgPhyOutCommitForPosRequest request);
}
