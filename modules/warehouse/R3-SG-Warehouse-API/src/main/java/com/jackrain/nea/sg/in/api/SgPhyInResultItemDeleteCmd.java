package com.jackrain.nea.sg.in.api;

import com.jackrain.nea.sys.Command;

/**
 *  入库结果单明细删除(逻辑删除)
 * @author jiang.cj
 * @since 2019-4-22
 * create at : 2019-4-22
 */
public interface SgPhyInResultItemDeleteCmd  extends Command {
}
