package com.jackrain.nea.sg.in.api;

import com.jackrain.nea.sg.in.model.request.SgPhyInPickOrderSaveRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 * @author zhu lin yu
 * @since 2019-11-18
 * create at : 2019-11-18 17:46
 */
public interface SgPhyInPickOrderSaveCmd {
    /**
     *入库拣货单
     */
    ValueHolderV14 saveSgbBInPickorder(SgPhyInPickOrderSaveRequest request);

    /**
     *入库拣货单-拣货明细
     */
    ValueHolderV14 saveSgbBInPickorderItem(SgPhyInPickOrderSaveRequest request);

    /**
     *入库拣货单-来源入库通知单
     */
    ValueHolderV14 saveSgbBInPickorderNoticeItem(SgPhyInPickOrderSaveRequest request);

    /**
     *入库拣货单-入库结果单明细
     */
    ValueHolderV14 saveSgbBInPickorderResultItem(SgPhyInPickOrderSaveRequest request);

    /**
     *入库拣货单-装箱明细
     */
    ValueHolderV14 saveSgbBInPickorderTeusItem(SgPhyInPickOrderSaveRequest request);
}
