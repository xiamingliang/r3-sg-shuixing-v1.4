package com.jackrain.nea.sg.inv.model.request;

import com.alibaba.fastjson.annotation.JSONField;
import com.jackrain.nea.web.face.User;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author: 舒威
 * @since: 2019/3/27
 * create at : 2019/3/27 16:52
 */
@Data
public class ScInvProfitLossMarginQueryCmdRequest implements Serializable {

    @JSONField(name = "CP_C_STORE_ID")
    private Long storeId;

    @JSONField(name = "CP_C_PHY_WAREHOUSE_ID")
    private Long warehouseId;

    @JSONField(name = "INVENTORY_TYPE")
    private Integer inventoryType; //盘点类型

    @JSONField(name = "INVENTORY_DATE")
    private Date inventoryDate; //盘点日期

    @JSONField(name = "POL_STATUS")
    private Integer polStatus;  //盈亏标志(1.已盈亏 2.未盈亏)

    @JSONField(name = "QUERY_ID")
    private String queryId; //查询标识

    @JSONField(name = "PAGE")
    private ScInvProfitLossBasePageRequest page; //分页信息

    @JSONField(name = "IS_QUERY_ITEMS")
    private Boolean isQueryItems; //是否查询盈亏明细标志

    @JSONField(name = "IS_QUERY_TEUS")
    private Boolean isQueryTeus; //是否查询盈亏明细(箱)标志 默认不查箱

    @JSONField(name = "IS_CREATE_PROF")
    private Boolean isCreateProf;  //生成盈亏标志

    @JSONField(name = "IS_EXPORT")
    private Boolean isExport; //导出标志

    @JSONField(name = "GENARATE_INFO")
    private ScInvProfitLossGenarateRequest genarateInfo;

    private User loginUser;

    public ScInvProfitLossMarginQueryCmdRequest() {
        this.polStatus = 1;
        this.isQueryItems = true;
        this.isQueryTeus = false;
        this.isCreateProf = false;
        this.isExport = false;
        this.genarateInfo = new ScInvProfitLossGenarateRequest();
        this.page = new ScInvProfitLossBasePageRequest();
    }
}
