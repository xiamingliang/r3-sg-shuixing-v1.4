package com.jackrain.nea.sg.in.model.request;

import com.alibaba.fastjson.annotation.JSONField;
import com.jackrain.nea.es.annotations.Field;
import com.jackrain.nea.es.constans.FieldType;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author: ChenChen
 * @Description:
 * @Date: Create in 16:33 2019/4/23
 */
@Data
public class SgPhyInNoticesRequest implements Serializable {

    private static final long serialVersionUID = 7752411793015685713L;
    @JSONField(name = "ID")
    private Long id;

    /**
     * 实体仓ID
     */
    private Long cpCPhyWarehouseId;

    /**
     * 实体仓CODE
     */
    private String cpCPhyWarehouseEcode;
    /**
     * 实体仓名称
     */
    private String cpCPhyWarehouseEname;
    /**
     * 入库类型
     */
    private Integer inType;
    /**
     * 来源单据类型
     */
    private Integer sourceBillType;
    /**
     * 来源单据ID
     */
    private Long sourceBillId;

    /**
     * 来源单据编号
     */
    private String sourceBillNo;

    /**
     * 下方都为非必填字段
     **/

    /**
     * 单据状态
     */
    private Integer billStatus;

    /**
     * 是否传wms
     */
    private Integer isPassWms;
    /**
     * 货主编号
     */
    private String goodsOwner;

    /**
     * 预计收货时间
     */
    private Date receiveTime;

    /**
     * 经销商实体仓ID（供应商id？？？）
     */
    private Long cpCSupplierId;

    /**
     * 下单时间
     */
    private Date orderTime;


    /**
     * 传wms状态
     */
    private Long wmsStatus;


    /**
     * 发货人名字
     */
    private String sendName;


    /**
     * 发货人手机
     */
    private String sendMobile;

    /**
     * 发货人电话
     */
    private String sendPhone;


    /**
     * 省id
     */
    private Long cpCRegionProvinceId;
    /**
     * 省Code
     */
    private String cpCRegionProvinceEcode;
    /**
     * 省名称
     */
    private String cpCRegionProvinceEname;

    /**
     * 市id
     */
    private Long cpCRegionCityId;

    /**
     * 市Code
     */
    private String cpCRegionCityEcode;

    /**
     * 市名称
     */
    private String cpCRegionCityEname;

    /**
     * 区id
     */
    private Long cpCRegionAreaId;

    /**
     * 区Code
     */
    private String cpCRegionAreaEcode;

    /**
     * 区名称
     */
    private String cpCRegionAreaEname;


    /**
     * 发货人地址
     */
    private String sendAddress;

    /**
     * 发货人邮编
     */
    private String sendZip;


    /**
     * 物流公司ID
     */
    private Long cpCLogisticsId;

    /**
     * 物流公司Code
     */
    private String cpCLogisticsEcode;

    /**
     * 物流公司名称
     */
    private String cpCLogisticsEname;

    /**
     * 物流单号
     */
    private String logisticNumber;


    /**
     * 平台店铺ID
     */
    private Long cpCShopId;

    /**
     * 平台店铺标题
     */
    private String cpCShopTitle;

    /**
     * 平台单号
     */
    private String sourcecode;


    /**
     * 客服备注
     */
    private String sellerRemark;

    /**
     * 买家备注
     */
    private String buyerRemark;


    /**
     * 备注
     */
    private String remark;


    /**
     * 经销商实体仓ID
     */
    private Long cpCCustomerWarehouseId;

    /**
     * 对应编码
     */
    private String cpCCsEcode;

    /**
     * 对应名称
     */
    private String cpCCsEname;


    /**
     * 逻辑收货单单据状态(逻辑收货单单据状态)
     */
    private Integer receiveBillStatus;


    /**
     * 入库时间
     */
    private Date inTime;


    /**逻辑后货单子表需要，但是子表仓都一样。暂放在这**/
    /**
     * 仓库id
     */
    private Long cpCStoreId;
    /**
     * 仓库Ecode
     */
    private String cpCStoreEcode;

    /**
     * 仓库Name
     */
    private String cpCStoreEname;

    /**
     * 来源类型: 1 调拨差异
     */
    private Long reserveBigint01;
    private String billNo;

}
