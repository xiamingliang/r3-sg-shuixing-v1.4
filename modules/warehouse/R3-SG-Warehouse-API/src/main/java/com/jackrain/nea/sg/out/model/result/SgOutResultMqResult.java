package com.jackrain.nea.sg.out.model.result;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author leexh
 * @since 2019/5/11 14:59
 */
@Data
public class SgOutResultMqResult implements Serializable {

    private static final long serialVersionUID = -3640864045635856320L;

    /**
     * 出库结果单ID
     */
    private Long id;

    /**
     * 出库结果单 单据编号
     */
    private String billNo;

    /**
     * 单据日期
     */
    private Date billDate;

    /**
     * 出库人ID
     */
    private Long outId;

    /**
     * 出库人eCode
     */
    private String outEcode;

    /**
     * 出库人eName
     */
    private String outEname;

    /**
     * 出库人name
     */
    private String outName;

    /**
     * 出库时间
     */
    private Date outTime;

    /**
     * 来源单据类型
     */
    private Integer sourceBillType;

    /**
     * 出库类型
     */
    private Integer outType;

    /**
     * 来源单据ID
     */
    private Long sourceBillId;

    /**
     * 来源单据编号
     */
    private String sourceBillNo;

    /**
     * 总出库数量
     */
    private BigDecimal totQtyOut;

    /**
     * 物流公司ID
     */
    private Long cpCLogisticsId;

    /**
     * 物流公司编码
     */
    private String cpCLogisticsEcode;

    /**
     * 物流公司名称
     */
    private String cpCLogisticsEname;

    /**
     * 物流单号
     */
    private String logisticNumber;

    /**
     * 出仓单号
     */
    private String outBillNo;

    /**
     * 配送方式
     */
    private String deliveryMethod;

    /**
     * po号
     */
    private String poId;

    /**
     * 平台单号
     */
    private String sourcecode;

    /**
     * 理论重量
     */
    private BigDecimal theoreticalWeight;

    /**
     * 最后一次出库
     * 1=否
     * 0=是
     */
    private Integer isLast;

    /**
     * 最后一次出库为是的时候使用该字段
     * 是否是真正的全部出库，既所有sku都已出完
     */
    private Boolean isAll;

    /**
     * 备注信息
     */
    private String remark;
}
