package com.jackrain.nea.sg.out.api;

import com.jackrain.nea.sg.out.model.request.WorkbenchOutNoticesPageQueryRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;

import java.util.List;

/**
 * @Description 工作台-出库通知单服务
 * @author 郑小龙
 * @date 2019-05-15 17:41
 */
public interface WorkbenchOutNoticesQueryCmd {

    /**
     * @Description 查询-客服工作台-传WMS失败
     * @author 郑小龙
     * @date 2019-05-15 18:00
     * @param pageQueryRequest 请求数据实体类
     * @return int
     */
    ValueHolderV14 queryPassWmsFailure(WorkbenchOutNoticesPageQueryRequest pageQueryRequest);

    /**
     * @Description 查询- 客服工作台- 仓库超截单时间未发
     * @author 郑小龙
     * @date 2019-05-15 18:02
     * @param pageQueryRequest 请求数据实体类
     * @return int
     */
    ValueHolderV14 queryWareSuperCutTimeNoHair(WorkbenchOutNoticesPageQueryRequest pageQueryRequest);

}
