package com.jackrain.nea.sg.in.api;

import com.jackrain.nea.sg.in.model.request.SgPhyInNoticesBillSaveRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;

import java.util.List;

/**
 * @Author: ChenChen
 * @Description:新增入库通知单和入库结果单
 * @Date: Create in 14:51 2019/5/28
 */
public interface SgPhyInNoticesAndResultSaveCmd {
    ValueHolderV14 saveSgBPhyInNoticesAndResult(List<SgPhyInNoticesBillSaveRequest> requestList);
}
