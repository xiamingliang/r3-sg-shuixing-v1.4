package com.jackrain.nea.sg.out.api;

import com.jackrain.nea.sg.out.model.request.SgOutResultMQRequest;
import com.jackrain.nea.sg.out.model.request.SgPhyOutNoticesCancelRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 * 取消出库服务
 *
 * @author 舒威
 * @since 2019/12/11
 * create at : 2019/12/11 15:43
 */
public interface SgPhyOutNoticesCancelCmd {

     ValueHolderV14<SgOutResultMQRequest> cancelSgPhyOutNotices(SgPhyOutNoticesCancelRequest request);
}
