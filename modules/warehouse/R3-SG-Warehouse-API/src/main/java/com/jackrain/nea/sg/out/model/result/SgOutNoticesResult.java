package com.jackrain.nea.sg.out.model.result;

import lombok.Data;

import java.io.Serializable;

/**
 * @author leexh
 * @since 2019/4/22 19:24
 */
@Data
public class SgOutNoticesResult implements Serializable {

    /**
     * 出库通知单ID
     */
    private Long id;

    /**
     * 来源单据ID
     */
    private Long sourceBilId;

    /**
     * 标识
     */
    private String flag;

    /**
     * 返回码
     *  0=出库通知单新增成功
     *  -1=出库通知单新增失败
     *  1=已传WMS
     *  2=不需传WMS
     *  3=传WMS失败次数>5
     *  4=物流单号或大头笔为空
     */
    private int code;
}
