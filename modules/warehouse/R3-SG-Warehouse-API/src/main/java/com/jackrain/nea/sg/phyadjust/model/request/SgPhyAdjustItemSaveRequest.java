package com.jackrain.nea.sg.phyadjust.model.request;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author csy
 * Date: 2019/4/29
 * Description:
 */
@Data
public class SgPhyAdjustItemSaveRequest implements Serializable {

    /**
     * -1表示新增
     * 非必传
     */
    @JSONField(name = "ID")
    private Long id;


    /**
     * 来源明细id
     * 必传
     */
    @JSONField(name = "SOURCE_BILL_ITEM_ID")
    private Long sourceBillItemId;

    /**
     * 数量(调整数量)
     * 必传
     */
    @JSONField(name = "QTY")
    private BigDecimal qty;

    /**
     * sku-id
     * 必传
     */
    @JSONField(name = "PS_C_SKU_ID")
    private Long psCSkuId;

    /**
     * sku-eCode
     * 必传
     */
    @JSONField(name = "PS_C_SKU_ECODE")
    private String psCSkuEcode;

    /**
     * 商品 pro-id
     * 必传
     */
    @JSONField(name = "PS_C_PRO_ID")
    private Long psCProId;

    /**
     * 商品 pro-eCode
     * 必传
     */
    @JSONField(name = "PS_C_PRO_ECODE")
    private String psCProEcode;

    /**
     * 商品 pro-eName
     * 必传
     */
    @JSONField(name = "PS_C_PRO_ENAME")
    private String psCProEname;

    /**
     * 规格1 id（颜色）
     * 必传
     */
    @JSONField(name = "PS_C_SPEC1_ID")
    private Long psCSpec1Id;

    /**
     * 规格1 eCode（颜色）
     * 必传
     */
    @JSONField(name = "PS_C_SPEC1_ECODE")
    private String psCSpec1Ecode;

    /**
     * 规格1 eName（颜色）
     * 必传
     */
    @JSONField(name = "PS_C_SPEC1_ENAME")
    private String psCSpec1Ename;

    /**
     * 规格2 id（尺寸）
     * 必传
     */
    @JSONField(name = "PS_C_SPEC2_ID")
    private Long psCSpec2Id;

    /**
     * 规格2 eCode（尺寸）
     * 必传
     */
    @JSONField(name = "PS_C_SPEC2_ECODE")
    private String psCSpec2Ecode;

    /**
     * 规格2 eName（尺寸）
     * 必传
     */
    @JSONField(name = "PS_C_SPEC2_ENAME")
    private String psCSpec2Ename;

    /**
     * 吊牌价
     * 非必传
     */
    @JSONField(name = "PRICE_LIST")
    private BigDecimal priceList;

    /**
     * 国标码
     * 必传
     */
    @JSONField(name = "GBCODE")
    private String gbcode;
}
