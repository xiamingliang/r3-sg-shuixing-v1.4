package com.jackrain.nea.sg.phyadjust.model.request;

import lombok.Data;

import java.io.Serializable;

/**
 * @author: 舒威
 * @since: 2019/7/29
 * create at : 2019/7/29 22:19
 */
@Data
public class SgPhyAdjustBillQueryRequest implements Serializable{
    /**
     * 原单据类型
     */
    private Integer sourceBillType;

    /**
     * 原单据id
     */
    private Long sourceBillId;


    /**
     * 原单据编号
     */
    private String sourceBillNo;
}
