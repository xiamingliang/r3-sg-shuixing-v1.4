package com.jackrain.nea.sg.out.api;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.query.QuerySession;

/**
 * @author zhu lin yu
 * @since 2019-11-08
 * create at : 2019-11-08 09:24
 */
public interface SgPhyOutPickOrderGenerateCmd {
    ValueHolderV14<Long> execute(QuerySession session) throws NDSException;

}
