package com.jackrain.nea.sg.out.api;

import com.jackrain.nea.sg.out.model.request.SgPhyOneClickOutLibraryRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 * 一键出库
 *
 * @author: 舒威
 * @since: 2019/9/10
 * create at : 2019/9/10 11:15
 */
public interface SgPhyOneClickOutLibraryCmd {

    ValueHolderV14 oneClickOutLibrary(SgPhyOneClickOutLibraryRequest request);
}
