package com.jackrain.nea.sg.out.api;

import com.jackrain.nea.sg.out.model.request.SgPhyOutNoticesQueryRequest;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutNotices;
import com.jackrain.nea.sys.domain.ValueHolderV14;

import java.util.List;

/**
 * @author zhu lin yu
 * @since 2019-10-23
 * create at : 2019-10-23 19:39
 */
public interface SgPhyOutNoticesMultiConditionQueryCmd {
    ValueHolderV14<List<SgBPhyOutNotices>> queryOutNoticesItem(SgPhyOutNoticesQueryRequest request);
}
