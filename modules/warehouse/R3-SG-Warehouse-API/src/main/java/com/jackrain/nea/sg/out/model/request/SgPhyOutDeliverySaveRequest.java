package com.jackrain.nea.sg.out.model.request;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author leexh
 * @since 2019/7/16 18:41
 * desc: 多包裹入参对象，出库回传时使用
 */
@Data
public class SgPhyOutDeliverySaveRequest  implements Serializable {

    private Long id;

    private String cpCLogisticsId;

    private String cpCLogisticsEcode;

    private String cpCLogisticsEname;

    private String logisticNumber;

    private String pkgProducts;

    private Long psCProId;

    private String psCProEcode;

    private String psCProEname;

    private Long psCSkuId;

    private String psCSkuEcode;

    private Long psCClrId;

    private String psCClrEcode;

    private String psCClrEname;

    private Long psCSizeId;

    private String psCSizeEcode;

    private String psCSizeEname;

    private BigDecimal qty;

    private BigDecimal weight;

    private BigDecimal size;

    private Long ocBOrderId;

    private Long version;

    private String ownerename;

    private String modifierename;

    private Long sgBPhyOutResultId;

    private String gbcode;
}
