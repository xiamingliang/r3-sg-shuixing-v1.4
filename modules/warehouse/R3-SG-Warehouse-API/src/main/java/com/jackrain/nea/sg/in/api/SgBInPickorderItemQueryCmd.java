package com.jackrain.nea.sg.in.api;

import com.jackrain.nea.sg.in.model.request.SgBInPickorderRequest;
import com.jackrain.nea.sg.in.model.result.SgBInPickorderResult;
import com.jackrain.nea.sg.in.model.table.SgBInPickorder;
import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 * @author: heliu
 * @since: 2019/11/18
 * create at : 2019/11/18 11:45
 */
public interface SgBInPickorderItemQueryCmd {
    ValueHolderV14<SgBInPickorderResult> querySgBInPickorderItem(SgBInPickorderRequest request);

    /**
     * 根据来源单号查询拣货单号
     *
     * @param sourceBillNo 来源单号
     * @return 入库拣货单对象
     */
    SgBInPickorder queryPickOrderBySourceBillNo(String sourceBillNo);
}