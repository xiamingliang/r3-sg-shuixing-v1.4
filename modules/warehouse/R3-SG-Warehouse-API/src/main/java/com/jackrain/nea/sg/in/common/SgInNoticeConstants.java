package com.jackrain.nea.sg.in.common;

/**
 * @Author: ChenChen
 * @Description:入库通知单Constants
 * @Date: Create in 17:32 2019/4/24
 */
public interface SgInNoticeConstants {


    /**
     * 单据状态
     */
    int BILL_STATUS_IN_PENDING = 1;//待入库
    int BILL_STATUS_IN_PART = 2;//部分入库
    int BILL_STATUS_IN_ALL = 3;//全部入库
    int BILL_STATUS_IN_VOID = 4;//已作废
    int WMS_UPLOAD_STATUTS_NO = 0;//0未传WMS
    int WMS_UPLOAD_STATUTS_UPLOADING = 1;//1传WMS中
    int WMS_UPLOAD_STATUTS_SUCCESS = 2;//2传WMS成功
    int WMS_UPLOAD_STATUTS_FAIL = 3;//3传WMS失败

    /**拣货状态*/
    int PICK_STATUS_WAIT = 1;  //未拣货
    int PICK_STATUS_ING = 2;  //拣货中
    int PICK_STATUS_SUCCESS = 3;  //已拣货

    String SEQ_SG_B_PHY_IN_NOTICES = "SEQ_SG_B_PHY_IN_NOTICES";

    /**
     * 单据类型 1:调拨差异
     */
    Long BILL_TYPE_TRANSFER_DIFF = 1L;


    /**
     * 奇门单据类型
     * <p>
     * JYCK=一般交易出库单;
     * HHCK= 换货出库;
     * BFCK=补发出库;
     * PTCK=普通出库单;
     * DBCK=调拨出库;
     * B2BRK=B2B入库;
     * B2BCK=B2B出库;
     * QTCK=其他出库;
     * SCRK=生产入库;
     * LYRK=领用入库;
     * CCRK=残次品入库;
     * CGRK=采购入库;
     * DBRK= 调拨入库;
     * QTRK=其他入库;
     * XTRK= 销退入库;
     * THRK=退货入库;
     * HHRK= 换货入库;
     * CNJG= 仓内加工单;
     * CGTH=采购退货出库单
     * JITCK=唯品会出库
     */
    String QM_BILL_TYPE_B2B_IN = "B2BRK";
    String QM_BILL_TYPE_PURCHASE_IN = "CGRK";
    String QM_BILL_TYPE_TRANSFER_IN = "DBRK";
    String QM_BILL_TYPE_OTHER_IN = "QTRK";
    String QM_DEFAULT_PLATFORM_CODE = "OTHER";  // 奇门默认入参
    String QM_BILL_USER_ECODE="root";//单据操作人

    String QM_BILL_TYPE_B2B_OUT = "B2BCK";
    String QM_BILL_TYPE_TRANSFER_OUT = "DBCK";
    String QM_BILL_TYPE_PURCHASE_REF_OUT = "CGTH";
    String QM_BILL_TYPE_WEIPINHUI_OUT = "JITCK";
    String QM_BILL_TYPE_NORMAL_OUT = "JYCK";


    int AUDIT_STATUS_NO_AUDIT= 1;
    int AUDIT_STATUS_AUDIT = 2;
    int AUDIT_STATUS_VOID = 3;//已作废

    String SG_B_PHY_IN_NOTICES_POS = "SG_B_PHY_IN_NOTICES_POS";
    String SG_B_PHY_IN_NOTICES_POS_IMP_ITEM = "SG_B_PHY_IN_NOTICES_POS_IMP_ITEM";
}
