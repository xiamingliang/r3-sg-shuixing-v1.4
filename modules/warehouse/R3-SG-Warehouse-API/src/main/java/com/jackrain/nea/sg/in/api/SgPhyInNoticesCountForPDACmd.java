package com.jackrain.nea.sg.in.api;

import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 * @author: 周琳胜
 * @since: 2019/11/1
 * 查询调拨类型的入库通知单数量
 * create at : 2019/11/1 13:49
 */
public interface SgPhyInNoticesCountForPDACmd {
    ValueHolderV14 selectTransferTypeNum(Long storeId);
}
