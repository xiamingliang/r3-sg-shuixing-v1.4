package com.jackrain.nea.sg.inv.model.result;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author 舒威
 * @since 2019/4/11
 * create at : 2019/4/11 10:48
 */
@Data
public class ScInvProfitLossQueryItemResult implements Serializable {

    @JSONField(name = "tHead")
    private List tHead;

    private ScInvProfitLossStorageQueryItemResult itemStorageResult;

    private ScInvProfitLossTeusStorageQueryItemResult itemTeusStorageResult;
}
