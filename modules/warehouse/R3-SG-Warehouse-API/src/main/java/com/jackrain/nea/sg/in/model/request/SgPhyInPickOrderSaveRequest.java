package com.jackrain.nea.sg.in.model.request;

import com.alibaba.fastjson.annotation.JSONField;
import com.jackrain.nea.sys.domain.BaseModel;
import com.jackrain.nea.web.face.User;
import lombok.Data;

import java.util.List;

/**
 * @author zhu lin yu
 * @since 2019-11-18
 * create at : 2019-11-18 17:53
 */
@Data
public class SgPhyInPickOrderSaveRequest extends BaseModel {

    @JSONField(name = "ID")
    private Long sgPhyBInPickorderId;

    private List<SgPhyInNoticesRequest> inNoticesList;

    private List<SgBPhyInResultSaveRequest> inResultList;

    private List<SgPhyInNoticesImpItemSaveRequest> inNoticesImpItemList;

    private List<SgPhyInPickorderResultItemSaveRequest> inPickorderResultItemList;

    @JSONField(name = "SG_B_IN_PICKORDER_TEUS_ITEM")
    private List<SgPhyInPickorderTeusItemSaveRequest> inPickorderTeusItemList;

    private User loginUser;

    // ================兼容调拨出库单生成拣货单传参==================
    /**
     * 标记是否是调拨出库单过来的数据
     */
    private Boolean isTransferOut;

    /**
     * 发货店仓法人
     */
    private String belongsLegal;

    /**
     * 来源单据性质
     */
    private Long sourceBillProp;

    /**
     * 2020-04-15添加字段
     * 入库序号
     */
    private String inSeqNo;

    public SgPhyInPickOrderSaveRequest() {
        this.isTransferOut = false;
        this.inSeqNo = "0";
    }
}
