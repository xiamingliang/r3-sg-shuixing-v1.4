package com.jackrain.nea.sg.in.api;

import com.jackrain.nea.sg.in.model.request.SgPhyInNoticesBillSaveRequest;
import com.jackrain.nea.sg.in.model.request.SgPhyInNoticesBillUpdateRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;

import java.util.List;

/**
 * @Author: chenchen
 * @Description:入库通知单
 * @Date: Create in 16:17 2019/4/22
 */
public interface SgPhyInNoticesSaveCmd {

    ValueHolderV14 saveSgBPhyInNotices(List<SgPhyInNoticesBillSaveRequest> requestList);

    /**
     * 更新入库通知单-拣货
     */
    ValueHolderV14 updateSgBPhyInNoticesByPick(SgPhyInNoticesBillUpdateRequest request);
}
