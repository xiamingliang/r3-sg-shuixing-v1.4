package com.jackrain.nea.sg.out.model.request;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author leexh
 * @since 2019/6/5 10:45
 * desc:
 */
@Data
public class SgPhyOutBillQueryRequest implements Serializable {

    /**
     * 来源单据信息集合
     */
    private List<SgPhyOutBillBaseRequest> baseRequests;


    /**
     * 返回信息:
     *  1=通知单
     *  2=结果单
     *  3=通知单+结果单
     */
    private Integer returnData;

    /**
     * 是否返回明细信息
     */
    private Boolean isReturnItem;
}
