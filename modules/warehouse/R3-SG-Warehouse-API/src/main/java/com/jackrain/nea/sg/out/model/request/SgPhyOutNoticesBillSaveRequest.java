package com.jackrain.nea.sg.out.model.request;

import com.alibaba.fastjson.annotation.JSONField;
import com.jackrain.nea.sg.basic.model.request.SgR3BaseRequest;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author leexh
 * @since 2019/4/19 15:13
 */
@Data
public class SgPhyOutNoticesBillSaveRequest extends SgR3BaseRequest implements Serializable {


    /**
     * 出库通知单主表
     */
    @JSONField(name = "SG_B_PHY_OUT_NOTICES")
    private SgPhyOutNoticesSaveRequest outNoticesRequest;

    /**
     * 出库通知单明细
     */
    @JSONField(name = "SG_B_PHY_OUT_NOTICES_ITEM")
    private List<SgPhyOutNoticesItemSaveRequest> outNoticesItemRequests;

    /**
     * 录入明细信息
     */
    @JSONField(name = "SG_B_PHY_OUT_NOTICES_IMP_ITEM")
    private List<SgPhyOutNoticesImpItemSaveRequest> impItemList;

    /**
     * 箱内明细信息
     */
    @JSONField(name = "SG_B_PHY_OUT_NOTICES_TEUS_ITEM")
    private List<SgPhyOutNoticesTeusItemSaveRequest> teusItemList;

    /**
     * 录入明细是否从reids获取(默认fasle)
     */
    @JSONField(name = "REDIS_KEY")
    private String redisKey;

    /**
     * 是否pos
     */
    private Boolean isPos;

    public SgPhyOutNoticesBillSaveRequest() {
        this.isPos = false;
    }
}
