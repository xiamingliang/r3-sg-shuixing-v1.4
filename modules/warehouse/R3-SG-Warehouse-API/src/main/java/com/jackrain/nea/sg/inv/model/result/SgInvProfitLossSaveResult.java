package com.jackrain.nea.sg.inv.model.result;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author 舒威
 * @since 2019/4/11
 * create at : 2019/4/11 11:37
 */
@Data
public class SgInvProfitLossSaveResult implements Serializable {

    private List<Long> ids; //未盈亏盘点单ids

    private Boolean isCreateInvadj; //是否生成库存调整单标志

    private JSONObject row; //盈亏明细
}
