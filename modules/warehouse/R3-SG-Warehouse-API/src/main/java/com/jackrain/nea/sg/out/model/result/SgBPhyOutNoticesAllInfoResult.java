package com.jackrain.nea.sg.out.model.result;

import com.jackrain.nea.sg.out.model.table.SgBPhyOutNotices;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutNoticesImpItem;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutNoticesItem;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutNoticesTeusItem;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author zhu lin yu
 * @since 2019-11-07
 * create at : 2019-11-07 15:08
 */
@Data
public class SgBPhyOutNoticesAllInfoResult implements Serializable {

    //出库通知单主表信息
    private SgBPhyOutNotices sgBPhyOutNotices;

    //出库通知单条码信息
    private List<SgBPhyOutNoticesItem> sgBPhyOutNoticesItemList;

    //出库通知单录入明细
    private List<SgBPhyOutNoticesImpItem> sgBPhyOutNoticesImpItemList;

    //出库通知单箱明细
    private List<SgBPhyOutNoticesTeusItem> sgBPhyOutNoticesTeusItemList;
}
