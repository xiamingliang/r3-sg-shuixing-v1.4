package com.jackrain.nea.sg.out.api;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.basic.model.result.SgR3BaseResult;
import com.jackrain.nea.sg.out.model.request.SgPhyOutResultBillSaveRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 * @author leexh
 * @since 2019/5/7 16:51
 */
public interface SgPhyOutResultSaveAndAuditCmd {

    ValueHolderV14<SgR3BaseResult> saveOutResultAndAudit(SgPhyOutResultBillSaveRequest request) throws NDSException;

    ValueHolderV14 saveOutResultAndAuditTask(JSONObject param, SgPhyOutResultBillSaveRequest requestModel);
}
