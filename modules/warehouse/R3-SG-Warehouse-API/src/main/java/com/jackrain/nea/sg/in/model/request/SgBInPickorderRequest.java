package com.jackrain.nea.sg.in.model.request;

import com.jackrain.nea.web.face.User;
import lombok.Data;

import java.io.Serializable;

/**
 * @author: heliu
 * @since: 2019/11/18
 * create at : 2019/11/18 11:47
 */
@Data
public class SgBInPickorderRequest implements Serializable {

    private static final long serialVersionUID = 3168016001552706664L;

    /**
     * 入库拣货单id
     */
    private Long id;


    private User loginUser;
}