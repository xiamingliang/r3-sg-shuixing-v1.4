package com.jackrain.nea.sg.out.model.request;

import com.jackrain.nea.sys.domain.BaseModel;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author leexh
 * @since 2019/4/19 18:08
 */
@Data
public class SgPhyOutNoticesItemSaveRequest extends BaseModel implements Serializable {

    /**
     * 明细ID(R3页面编辑时传入)
     */
    private Long id;

    private Long sgBPhyOutNoticesId;

    private Long sourceBillItemId;

    private Long psCSkuId;

    private String psCSkuEcode;

    private Long psCProId;

    private String psCProEcode;

    private String psCProEname;

    private Long psCSpec1Id;

    private String psCSpec1Ecode;

    private String psCSpec1Ename;

    private Long psCSpec2Id;

    private String psCSpec2Ecode;

    private String psCSpec2Ename;

    private BigDecimal priceList;

    private BigDecimal priceCost;

    private BigDecimal qty;

    private BigDecimal qtyOut;

    private BigDecimal qtyDiff;

    private BigDecimal amtList;

    private BigDecimal amtCost;

    private BigDecimal amtListOut;

    private BigDecimal amtCostOut;

    private BigDecimal amtListDiff;

    private BigDecimal amtCostDiff;

    private String boxNo;

    private String gbcode;
}
