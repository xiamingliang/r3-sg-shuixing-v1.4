package com.jackrain.nea.sg.inv.api;

import com.jackrain.nea.sg.in.model.result.ScBInventoryResult;
import com.jackrain.nea.sg.inv.model.request.ScBInventoryQueryRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 * @author: 周琳胜
 * @since: 2019/11/26
 * create at : 2019/11/26 21:28
 */
public interface ScBInventoryByConditionQueryCmd {

    ValueHolderV14<ScBInventoryResult> queryInventory(ScBInventoryQueryRequest request);
}
