package com.jackrain.nea.sg.in.model.request;

import com.jackrain.nea.web.face.User;
import lombok.Data;

import java.io.Serializable;

/**
 * @author: 舒威
 * @since: 2019/9/9
 * create at : 2019/9/9 9:50
 */
@Data
public class SgPhyOneClickLibraryRequest implements Serializable {

    private Long sourceBillId;

    private Integer sourceBillType;

    private String lockKey;

    private User loginUser;
}
