package com.jackrain.nea.sg.in.model.request;

import com.jackrain.nea.web.face.User;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * @Author: ChenChen
 * @Description:入库审核通过后回写入库通知单Request
 * @Date: Create in 15:28 2019/4/23
 */
@Data
public class SgPhyInNoticesBillPassRequest implements Serializable {

    private Long id;
    private static final long serialVersionUID = 4786609716121535622L;
    private List<SgPhyInNoticesItemRequest> itemList;
    private User loginUser;
    private Boolean isLast;
    private Date inTime;
    private Long pickOrderId;

}
