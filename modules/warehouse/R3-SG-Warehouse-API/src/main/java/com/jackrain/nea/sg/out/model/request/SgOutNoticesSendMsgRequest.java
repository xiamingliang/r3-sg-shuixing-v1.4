package com.jackrain.nea.sg.out.model.request;

import com.jackrain.nea.sg.out.model.table.SgBPhyOutNotices;
import lombok.Data;

import java.io.Serializable;

/**
 * @description: 出库通知单 mq
 * @author: 郑小龙
 * @date: 2019-07-17 20:45
 */
@Data
public class SgOutNoticesSendMsgRequest implements Serializable {
    /**
     * 出库通知单
     */
    private SgBPhyOutNotices outNotices;

    /**
     * 电子面单类型
     */
    private Integer etype;

    /**
     * 电子面单中的挂靠店铺id
     */
    private long affiliatedShop;
}
