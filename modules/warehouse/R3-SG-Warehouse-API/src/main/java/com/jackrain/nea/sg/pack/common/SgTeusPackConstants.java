package com.jackrain.nea.sg.pack.common;

/**
 * @author: 周琳胜
 * @since: 2019/11/27
 * create at : 2019/11/27 14:47
 */
public interface SgTeusPackConstants {

    /**
     * 单据状态
     * 1=未审核
     * 2=已审核
     * 3=已作废
     */
    int BILL_STATUS_NOT_AUDIT = 1;
    int BILL_STATUS_AUDIT = 2;
    int BILL_STATUS_VOID = 3;

    String SG_B_TEUS_PACK = "SG_B_TEUS_PACK";  // 拆箱单
    String SG_B_TEUS_PACK_ITEM = "SG_B_TEUS_PACK_ITEM"; // 拆箱明细表

    /**
     * 箱状态（箱定义）
     * 1=待入库
     * 2=已入库未拆箱
     * 3=已拆箱
     */
    int BOX_STATUS_WAIT_IN = 1;
    int BOX_STATUS_IN = 2;
    int BOX_STATUS_SPLIT = 3;

    /**
     * 是否箱
     */
    Integer IS_TEUS_N = 0;
    Integer IS_TEUS_Y = 1;

}
