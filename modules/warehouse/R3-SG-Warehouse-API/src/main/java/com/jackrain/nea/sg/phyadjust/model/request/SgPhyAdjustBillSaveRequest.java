package com.jackrain.nea.sg.phyadjust.model.request;

import com.alibaba.fastjson.annotation.JSONField;
import com.jackrain.nea.sg.basic.model.request.SgR3BaseRequest;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author csy
 * Date: 2019/4/29
 * Description:
 */

@Data
public class SgPhyAdjustBillSaveRequest extends SgR3BaseRequest implements Serializable {
    /**
     * 主表信息
     */
    @JSONField(name = "SG_B_PHY_ADJUST")
    private SgPhyAdjustSaveRequest phyAdjust;

    /**
     * 明细信息
     */
    @JSONField(name = "SG_B_PHY_ADJUST_ITEM")
    private List<SgPhyAdjustItemSaveRequest> items;

    /**
     * 录入明细信息
     */
    @JSONField(name = "SG_B_PHY_ADJUST_IMP_ITEM")
    private List<SgPhyAdjustImpItemRequest> impItems;

    /**
     * 箱明细
     */
    @JSONField(name = "SG_B_PHY_ADJUST_TEUS_ITEM")
    private List<SgPhyAdjustTeusItemRequest> teusItems;

    /**
     * 录入明细是否从reids获取(默认fasle)
     */
    @JSONField(name = "REDIS_KEY")
    private String redisKey;

    /**
     * 逻辑仓信息(若指定了逻辑仓即不需要寻仓)
     */
    @JSONField(name = "STORE_INFO")
    private SgPhyAdjustStoreRequest storeRequest;
}
