package com.jackrain.nea.sg.out.api;

import com.jackrain.nea.sg.out.model.request.SgPhyOutPickOrderAuditRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 * @author zhu lin yu
 * @since 2019-11-20
 * create at : 2019-11-20 16:45
 */
public interface SgPhyOutPickOrderAuditCmd {
    ValueHolderV14 auditOutPickOrder(SgPhyOutPickOrderAuditRequest request);
}
