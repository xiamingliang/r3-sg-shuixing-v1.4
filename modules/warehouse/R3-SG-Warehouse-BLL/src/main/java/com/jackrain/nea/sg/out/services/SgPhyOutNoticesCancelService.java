package com.jackrain.nea.sg.out.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Lists;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.out.common.SgOutConstantsIF;
import com.jackrain.nea.sg.out.mapper.SgBPhyOutNoticesMapper;
import com.jackrain.nea.sg.out.model.request.SgOutResultMQRequest;
import com.jackrain.nea.sg.out.model.request.SgPhyOutNoticesCancelRequest;
import com.jackrain.nea.sg.out.model.request.SgPhyOutResultBillSaveRequest;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutNotices;
import com.jackrain.nea.sg.send.model.request.SgSendBillCleanRequest;
import com.jackrain.nea.sg.send.model.result.SgSendBillCleanResult;
import com.jackrain.nea.sg.send.services.SgSendCleanService;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 取消出库服务
 *
 * @author 舒威
 * @since 2019/12/11
 * create at : 2019/12/11 15:52
 */
@Slf4j
@Component
public class SgPhyOutNoticesCancelService {

    @Autowired
    private SgPhyOutNoticesSaveRPCService noticesSaveRPCService;

    @Autowired
    private SgPhyOutResultChargeOffService chargeOffService;

    @Autowired
    private SgBPhyOutNoticesMapper noticesMapper;

    @Transactional(rollbackFor = Exception.class)
    public ValueHolderV14<SgOutResultMQRequest> cancelSgPhyOutNotices(SgPhyOutNoticesCancelRequest request) {
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ".debug, param:" + JSONObject.toJSONString(request));
        }
        ValueHolderV14<SgOutResultMQRequest> result = new ValueHolderV14<>(ResultCode.SUCCESS, "取消出库成功");
        Long id = checkParams(request);
        List<Long> ids = Lists.newArrayList();
        ids.add(id);
        //根据通知单已出库数量生成一张逆向的结果单
        List<SgPhyOutResultBillSaveRequest> resultBillSaveRequests = noticesSaveRPCService.getAssemblyParameters(ids, request.getUser(), false, false, true);
        SgPhyOutResultBillSaveRequest resultBillSaveRequest = resultBillSaveRequests.get(0);
        resultBillSaveRequest.getOutResultRequest().setRemark(request.getRemark());
        ValueHolderV14<SgOutResultMQRequest> outResultAndAudit = chargeOffService.saveAndAuditSgPhyOutResult(resultBillSaveRequest);
        if (log.isDebugEnabled()) {
            log.debug("SgPhyOutResultChargeOffService. Return:{};", JSONObject.toJSONString(outResultAndAudit));
        }
        if (!outResultAndAudit.isOK()) {
            AssertUtils.logAndThrow("取消出库失败!" + outResultAndAudit.getMessage());
        }
        result.setData(outResultAndAudit.getData());
        //清空逻辑发货单
        if (request.getIsCleanSend()) {
            SgSendCleanService cleanService = ApplicationContextHandle.getBean(SgSendCleanService.class);
            SgSendBillCleanRequest cleanRequest = new SgSendBillCleanRequest();
            cleanRequest.setSourceBillId(request.getSourceBillId());
            cleanRequest.setSourceBillType(request.getSourceBillType());
            cleanRequest.setServiceNode(request.getServiceNode());
            cleanRequest.setRemark(request.getRemark());
            cleanRequest.setLoginUser(request.getUser());
            cleanRequest.setIsRepeatClean(true);
            ValueHolderV14<SgSendBillCleanResult> cleanResult = cleanService.cleanSgBSend(cleanRequest);
            if (log.isDebugEnabled()) {
                log.debug("SgSendCleanService. Return:{};", JSONObject.toJSONString(cleanResult));
            }
            if (!cleanResult.isOK()) {
                AssertUtils.logAndThrow("取消出库失败!" + cleanResult.getMessage());
            }
        }
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ".debug, return:" + JSONObject.toJSONString(result));
        }
        return result;
    }

    public Long checkParams(SgPhyOutNoticesCancelRequest request) {
        User loginUser = request.getUser();
        AssertUtils.notNull(loginUser, "用户信息不能为空！");
        if (request.getSourceBillId() == null || request.getSourceBillType() == null) {
            AssertUtils.logAndThrow("来源单据id或来源单据类型为空！", loginUser.getLocale());
        }
        List<SgBPhyOutNotices> notices = noticesMapper.selectList(new QueryWrapper<SgBPhyOutNotices>().lambda()
                .eq(SgBPhyOutNotices::getSourceBillId, request.getSourceBillId())
                .eq(SgBPhyOutNotices::getSourceBillType, request.getSourceBillType())
                .eq(SgBPhyOutNotices::getIsactive, SgConstants.IS_ACTIVE_Y));
        AssertUtils.notNull(notices, "当前记录已不存在！", loginUser.getLocale());
        Integer billStatus = notices.get(0).getBillStatus();
        AssertUtils.isTrue(billStatus != SgOutConstantsIF.OUT_NOTICES_STATUS_VOID, "单据已作废，不允许取消出库！");
        AssertUtils.isTrue(billStatus != SgOutConstantsIF.OUT_NOTICES_STATUS_WAIT, "单据未出库，不允许取消出库！");
        return notices.get(0).getId();
    }
}
