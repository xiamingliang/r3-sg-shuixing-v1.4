package com.jackrain.nea.sg.in.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.in.mapper.SgBPhyInNoticesImpItemMapper;
import com.jackrain.nea.sg.in.model.request.SgPhyInNoticesQueryRequest;
import com.jackrain.nea.sg.in.model.table.SgBPhyInNoticesImpItem;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author 周琳胜
 * @since 2019-10-23
 * create at : 2019-10-23 20:03
 */

@Slf4j
@Component
public class SgPhyInNoticesItemQueryService {
    public ValueHolderV14<List<SgBPhyInNoticesImpItem>> queryInNoticesItem(SgPhyInNoticesQueryRequest request) {
        ValueHolderV14<List<SgBPhyInNoticesImpItem>> valueHolderV14 = new ValueHolderV14<>();

        if (log.isDebugEnabled()) {
            log.debug("Start SgPhyInNoticesItemQueryService.queryInNoticesItem. ReceiveParams:requests:{}"
                    , JSONObject.toJSONString(request));
        }

        if (request == null || request.getPhyInNoticesId() == null) {
            valueHolderV14.setCode(ResultCode.FAIL);
            valueHolderV14.setMessage("请传入查询参数");
            return valueHolderV14;
        }

        Long phyInNoticesId = request.getPhyInNoticesId();

        SgBPhyInNoticesImpItemMapper sgBPhyInNoticesItemMapper = ApplicationContextHandle.getBean(SgBPhyInNoticesImpItemMapper.class);
        List<SgBPhyInNoticesImpItem> sgBPhyInNoticesItems
                = sgBPhyInNoticesItemMapper.selectList(new QueryWrapper<SgBPhyInNoticesImpItem>().lambda().eq(SgBPhyInNoticesImpItem::getSgBPhyInNoticesId, phyInNoticesId));

        valueHolderV14.setCode(ResultCode.SUCCESS);
        valueHolderV14.setMessage("查询成功");
        valueHolderV14.setData(sgBPhyInNoticesItems);

        if (log.isDebugEnabled()) {
            log.debug("Finsh SgPhyInNoticesItemQueryService.queryInNoticesItem. Result:valueHolderV14:{},spend time:{}ms"
                    , JSONObject.toJSONString(valueHolderV14));
        }

        return valueHolderV14;
    }
}
