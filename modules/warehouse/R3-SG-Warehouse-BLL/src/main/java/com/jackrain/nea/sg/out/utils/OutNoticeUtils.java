package com.jackrain.nea.sg.out.utils;

import com.jackrain.nea.sg.out.common.SgOutConstantsIF;
import com.jackrain.nea.sg.out.mapper.SgBPhyOutNoticesMapper;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutNotices;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author leexh
 * @since 2020/5/29 19:54
 * desc:
 */
@Slf4j
@Component
public class OutNoticeUtils {

    /**
     * 查询通知单的来源单据是否是差异单
     *
     * @param outNoticeId 通知单ID
     * @return true=是；false=否
     */
    public boolean allowNegative(Long outNoticeId) {
        if (outNoticeId != null) {
            SgBPhyOutNoticesMapper noticesMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesMapper.class);
            SgBPhyOutNotices outNotices = noticesMapper.selectById(outNoticeId);
            return outNotices != null && SgOutConstantsIF.IS_DIFF_DEAL_Y.equals(outNotices.getIsDiffDeal()) ? true : false;
        } else {
            return false;
        }
    }
}
