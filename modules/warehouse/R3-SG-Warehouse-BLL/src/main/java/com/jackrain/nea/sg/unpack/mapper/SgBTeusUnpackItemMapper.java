package com.jackrain.nea.sg.unpack.mapper;

import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.unpack.model.table.SgBTeusUnpackItem;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SgBTeusUnpackItemMapper extends ExtentionMapper<SgBTeusUnpackItem> {
}