package com.jackrain.nea.sg.out.config;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * @author leexh
 * @since 2019/5/15 19:19
 */
@Slf4j
@Configuration
@Data
public class SgOutMqConfig {

    @Value("${r3.mq.default.configName}")
    private String configName;

    @Value("${r3.oms.sg.out.mq.topic}")
    private String mqTopic;

    @Value("${r3.oms.sg.to.vip.mq.topic}")
    private String vipMqTopic;

    @Value("${r3.oms.sg.to.oms.mq.topic}")
    private String omsMqTopic;


}
