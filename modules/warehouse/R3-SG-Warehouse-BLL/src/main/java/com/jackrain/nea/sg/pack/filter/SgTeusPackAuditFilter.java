package com.jackrain.nea.sg.pack.filter;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.psext.api.PsCTeusSaveCmd;
import com.jackrain.nea.psext.request.PsTeusItemSaveRequest;
import com.jackrain.nea.psext.request.PsTeusUpdateRequest;
import com.jackrain.nea.sg.basic.utils.StorageUtils;
import com.jackrain.nea.sg.pack.common.SgTeusPackConstants;
import com.jackrain.nea.sg.pack.mapper.SgBTeusPackItemMapper;
import com.jackrain.nea.sg.pack.mapper.SgBTeusPackMapper;
import com.jackrain.nea.sg.pack.model.table.SgBTeusPack;
import com.jackrain.nea.sg.pack.model.table.SgBTeusPackItem;
import com.jackrain.nea.sg.phyadjust.common.SgPhyAdjustConstants;
import com.jackrain.nea.sg.phyadjust.common.SgPhyAdjustConstantsIF;
import com.jackrain.nea.sg.phyadjust.model.request.SgPhyAdjustBillSaveRequest;
import com.jackrain.nea.sg.phyadjust.model.request.SgPhyAdjustImpItemRequest;
import com.jackrain.nea.sg.phyadjust.model.request.SgPhyAdjustSaveRequest;
import com.jackrain.nea.sg.phyadjust.model.request.SgPhyAdjustStoreRequest;
import com.jackrain.nea.sg.phyadjust.services.SgPhyAdjSaveAndAuditService;
import com.jackrain.nea.sg.unpack.common.SgUnpackConstants;
import com.jackrain.nea.tableService.MainTableRecord;
import com.jackrain.nea.tableService.TableServiceContext;
import com.jackrain.nea.tableService.filter.BaseFilter;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author zhoulisnheng
 * @since 2019/11/27 18:04
 * desc: 拆箱单保存
 */
@Slf4j
@Component
public class SgTeusPackAuditFilter extends BaseFilter {

    @Override
    public void before(TableServiceContext context, MainTableRecord row) {

        /**
         * 根据当前明细记录，按照“箱号”分组：
         * 删除对应箱号在箱定义表中的【箱明细】
         * 更新当前箱号在明细表中涉及的条码，写入到箱定义的【箱明细】中。
         * 更新对应箱号在箱定义的“箱状态”=已入库未拆箱。
         */
        Long objId = row.getId();
        SgBTeusPackItemMapper packItemMapper = ApplicationContextHandle.getBean(SgBTeusPackItemMapper.class);

        List<SgBTeusPackItem> packItemList = packItemMapper.selectList(new QueryWrapper<SgBTeusPackItem>().lambda().eq(SgBTeusPackItem::getSgBTeusPackId, objId));
        Map<Long, List<SgBTeusPackItem>> collect = packItemList.stream().collect(Collectors.groupingBy(SgBTeusPackItem::getPsCTeusId));

        List<Long> teusIdList = new ArrayList<>(collect.keySet());


        PsCTeusSaveCmd psCTeusSaveCmd = (PsCTeusSaveCmd)
                ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                        PsCTeusSaveCmd.class.getName(), "ps-ext", "1.0");
        PsTeusUpdateRequest teusUpdateRequest = new PsTeusUpdateRequest();
        List<PsTeusItemSaveRequest> itemList = new ArrayList<>();

        for (SgBTeusPackItem sgBTeusPackItem : packItemList) {
            PsTeusItemSaveRequest psTeusItemSaveRequest = new PsTeusItemSaveRequest();
            BeanUtils.copyProperties(sgBTeusPackItem, psTeusItemSaveRequest);
            itemList.add(psTeusItemSaveRequest);
        }

        teusUpdateRequest.setBoxStatus(SgTeusPackConstants.BOX_STATUS_IN);
        teusUpdateRequest.setIds(teusIdList);
        teusUpdateRequest.setBoxItemList(itemList);
        teusUpdateRequest.setIsDelItem(true);
        psCTeusSaveCmd.batchUpdateBoxForPurIn(context.getUser(), teusUpdateRequest);


    }

    /**
     * 封装参数
     */
    private SgPhyAdjustBillSaveRequest packparam(SgBTeusPack sgBTeuspack, List<SgBTeusPackItem> packItemList) {
        SgPhyAdjustBillSaveRequest billSaveRequest = new SgPhyAdjustBillSaveRequest();

        //逻辑仓信息
        SgPhyAdjustStoreRequest sgPhyAdjustStoreRequest = new SgPhyAdjustStoreRequest();
        sgPhyAdjustStoreRequest.setCpCStoreId(sgBTeuspack.getCpCStoreId());
        sgPhyAdjustStoreRequest.setCpCStoreEcode(sgBTeuspack.getCpCStoreEcode());
        sgPhyAdjustStoreRequest.setCpCStoreEname(sgBTeuspack.getCpCStoreEname());
        billSaveRequest.setStoreRequest(sgPhyAdjustStoreRequest);

        // 主表信息
        SgPhyAdjustSaveRequest sgPhyAdjustSaveRequest = new SgPhyAdjustSaveRequest();
        sgPhyAdjustSaveRequest.setCpCPhyWarehouseId(sgBTeuspack.getCpCPhyWarehouseId());
        sgPhyAdjustSaveRequest.setCpCPhyWarehouseEname(sgBTeuspack.getCpCPhyWarehouseEname());
        sgPhyAdjustSaveRequest.setCpCPhyWarehouseEcode(sgBTeuspack.getCpCPhyWarehouseEcode());
        sgPhyAdjustSaveRequest.setBillType(SgPhyAdjustConstantsIF.BILL_TYPE_NORMAL);
        sgPhyAdjustSaveRequest.setSgBAdjustPropId(SgPhyAdjustConstants.ADJUST_PROP_NORMAL_ADJUST);
        sgPhyAdjustSaveRequest.setSourceBillType(SgPhyAdjustConstantsIF.SOURCE_BILL_TYPE_PACK);
        sgPhyAdjustSaveRequest.setSourceBillId(sgBTeuspack.getId());
        sgPhyAdjustSaveRequest.setSourceBillNo(sgBTeuspack.getEcode());
        sgPhyAdjustSaveRequest.setRemark("由装箱单" + sgBTeuspack.getEcode() + "审核生成！");

        // 封装录入明细
        List<SgPhyAdjustImpItemRequest> impItemRequestList = new ArrayList<>();
        Map<String, List<SgBTeusPackItem>> collectTeusMap = packItemList.stream().collect(Collectors.groupingBy(SgBTeusPackItem::getPsCTeusEcode));

        for (String teusEcode : collectTeusMap.keySet()) {
            SgPhyAdjustImpItemRequest impItemRequest = new SgPhyAdjustImpItemRequest();
            SgBTeusPackItem sgBTeusPackItem = collectTeusMap.get(teusEcode).get(0);
            impItemRequest.setIsTeus(SgTeusPackConstants.IS_TEUS_Y);
            impItemRequest.setId(-1L);
            impItemRequest.setSourceBillItemId(-1L);
            impItemRequest.setQty(new BigDecimal(1));
            impItemRequest.setPsCTeusId(sgBTeusPackItem.getPsCTeusId());
            impItemRequest.setPsCTeusEcode(sgBTeusPackItem.getPsCTeusEcode());
            impItemRequestList.add(impItemRequest);
        }

        Map<String, List<SgBTeusPackItem>> collectSkuMap = packItemList.stream().collect(Collectors.groupingBy(SgBTeusPackItem::getPsCSkuEcode));
        for (String skuEcode : collectSkuMap.keySet()) {
            List<SgBTeusPackItem> sgBTeusPackItems = collectSkuMap.get(skuEcode);
            SgBTeusPackItem sgBTeusPackItem = sgBTeusPackItems.get(0);
            BigDecimal totalQty = sgBTeusPackItems.stream().map(packItem ->
                    packItem.getQty() != null ? packItem.getQty() : BigDecimal.ZERO
            ).reduce(BigDecimal::add).orElse(BigDecimal.ZERO);

            SgPhyAdjustImpItemRequest impItemRequest = new SgPhyAdjustImpItemRequest();
            impItemRequest.setId(-1L);
            impItemRequest.setQty(totalQty.negate());
            impItemRequest.setPsCSkuId(sgBTeusPackItem.getPsCSkuId());
            impItemRequest.setPsCSkuEcode(sgBTeusPackItem.getPsCSkuEcode());
            impItemRequest.setIsTeus(SgTeusPackConstants.IS_TEUS_N);
            impItemRequest.setSourceBillItemId(-1L);
            impItemRequestList.add(impItemRequest);
        }

        billSaveRequest.setObjId(-1L);
        billSaveRequest.setPhyAdjust(sgPhyAdjustSaveRequest);
        billSaveRequest.setImpItems(impItemRequestList);
        return billSaveRequest;
    }

    @Override
    public void after(TableServiceContext context, MainTableRecord row) {
        Long objId = row.getId();
        SgBTeusPackMapper packMapper = ApplicationContextHandle.getBean(SgBTeusPackMapper.class);
        SgBTeusPack sgBTeusPack = packMapper.selectById(objId);

        SgBTeusPackItemMapper packItemMapper = ApplicationContextHandle.getBean(SgBTeusPackItemMapper.class);
        List<SgBTeusPackItem> packItemList = packItemMapper.selectList(new QueryWrapper<SgBTeusPackItem>().lambda().eq(SgBTeusPackItem::getSgBTeusPackId, objId));

        //*生成一张已审核的库存调整单
        SgPhyAdjSaveAndAuditService saveAndAuditService = ApplicationContextHandle.getBean(SgPhyAdjSaveAndAuditService.class);
        SgPhyAdjustBillSaveRequest request = packparam(sgBTeusPack, packItemList);
        request.setLoginUser(context.getUser());
        saveAndAuditService.saveAndAudit(request);
    }
}
