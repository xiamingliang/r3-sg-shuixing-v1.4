package com.jackrain.nea.sg.out.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.model.util.AdParamUtil;
import com.jackrain.nea.r3.mq.util.R3MqSendHelper;
import com.jackrain.nea.redis.config.CusRedisTemplate;
import com.jackrain.nea.redis.util.RedisOpsUtil;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.out.common.SgOutConstants;
import com.jackrain.nea.sg.out.common.SgOutConstantsIF;
import com.jackrain.nea.sg.out.config.SgOutMqConfig;
import com.jackrain.nea.sg.out.mapper.SgBPhyOutResultItemMapper;
import com.jackrain.nea.sg.out.mapper.SgBPhyOutResultMapper;
import com.jackrain.nea.sg.out.model.result.SgOutResultSendMsgResult;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutResult;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutResultItem;
import com.jackrain.nea.sg.out.utils.OutNoticeUtils;
import com.jackrain.nea.sg.out.utils.SgWarehouseESUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * 批量审核出库结果单-WMS回传用
 *
 * @author: 舒威
 * @since: 2019/10/18
 * create at : 2019/10/18 12:12
 */
@Slf4j
@Component
public class SgPhyOutResultAuditWMSService {

    @Autowired
    private SgBPhyOutResultMapper resultMapper;

    @Autowired
    SgBPhyOutResultItemMapper resultItemMapper;

    @Autowired
    private SgPhyOutNoticesSaveService noticesSaveService;

    @Autowired
    private SgPhyOutRPCService rpcService;

    @Autowired
    private R3MqSendHelper r3MqSendHelper;

    @Autowired
    private SgOutMqConfig mqConfig;

    @Autowired
    private SgWarehouseESUtils warehouseESUtils;

    public ValueHolderV14<JSONObject> auditSgPhyOutResultByWms(List<Long> ids, User user) {
        ValueHolderV14<JSONObject> vh = new ValueHolderV14<>(ResultCode.SUCCESS, "success");
        JSONObject dataObj = new JSONObject();
        HashMap<Long, String> auditErrMap = Maps.newHashMap();
        ValueHolderV14<HashMap<String, Object>> checkResult = checkParams(ids, user);
        if (!checkResult.isOK()) {
            HashMap<String, Object> data = checkResult.getData();
            auditErrMap = (HashMap<Long, String>) data.get("error");
            vh.setCode(ResultCode.FAIL);
            vh.setMessage("批量审核出库结果单失败!");
        } else {
            HashMap<String, Object> data = checkResult.getData();
            //check通过数据
            Map<Long, SgBPhyOutResult> resultMap = (Map<Long, SgBPhyOutResult>) data.get("resultMap");
            Map<Long, List<SgBPhyOutResultItem>> resultItemMap = (Map<Long, List<SgBPhyOutResultItem>>) data.get("resultItemMap");
            List<Long> resultIds = (List<Long>) data.get("resultIds");
            auditErrMap = (HashMap<Long, String>) data.get("error");
            String sysParam = (String) data.get("sysParam");

            List<Long> auditSuccess = batchAuditOutResults(resultIds, resultMap, resultItemMap, auditErrMap, sysParam, user);
            if (CollectionUtils.isEmpty(auditSuccess)) {
                vh.setCode(ResultCode.FAIL);
                vh.setMessage("批量审核出库结果单失败!");
            } else {
                dataObj.put("auditData", auditSuccess);
            }
        }
        dataObj.put("errMap", auditErrMap);
        vh.setData(dataObj);
        return vh;
    }

    public List<Long> batchAuditOutResults(List<Long> resultIds, Map<Long, SgBPhyOutResult> resultMap,
                                           Map<Long, List<SgBPhyOutResultItem>> resultItemMap, HashMap<Long, String> auditErrMap, String sysParam, User user) {
        //批量加锁
        List<Long> auditIds = setLocks(resultIds, resultMap, auditErrMap);

        List<Long> ids = null;

        try {
            if (CollectionUtils.isNotEmpty(auditIds)) {
                //审核更新
                List<Long> mqids = auditBills(auditIds, resultMap, resultItemMap, auditErrMap, sysParam, user);

                if (CollectionUtils.isNotEmpty(mqids)) {
                    //发送MQ通知业务单据
                    ids = batchSendMQ(mqids, resultMap, resultItemMap, user, auditErrMap);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            String messageExtra = AssertUtils.getMessageExtra("结果单ids" + auditIds + "批量审核异常!", e);
            log.error(messageExtra);
        } finally {
            try {
                CusRedisTemplate<String, String> redisTemplate = RedisOpsUtil.getStrRedisTemplate();
                for (Long id : resultIds) {
                    SgBPhyOutResult result = resultMap.get(id);
                    Long sourceBillId = result.getSourceBillId();
                    Integer sourceBillType = result.getSourceBillType();
                    String lockKsy = SgConstants.SG_B_PHY_OUT_RESULT + ":" + sourceBillId + ":" + sourceBillType;
                    redisTemplate.delete(lockKsy);
                }
            } catch (Exception ex) {
                String messageExtra2 = AssertUtils.getMessageExtra(this.getClass().getName() + "redis锁释放失败，请联系管理员！", ex);
                log.error(messageExtra2);
            }
        }

        return ids;
    }

    public List<Long> batchSendMQ(List<Long> mqids, Map<Long, SgBPhyOutResult> resultMap, Map<Long, List<SgBPhyOutResultItem>> resultItemMap, User user, HashMap<Long, String> auditErrMap) {
        //获取body
        List<SgOutResultSendMsgResult> bodys = Lists.newArrayList();
        //默认都是最后一次出库
        JSONObject isLastObj = new JSONObject();
        isLastObj.put("isLast", SgOutConstantsIF.OUT_IS_LAST_Y);
        isLastObj.put("isAll", true);
        List<Long> sends = Lists.newArrayList();
        for (Long mqid : mqids) {
            try {
                ValueHolderV14<SgOutResultSendMsgResult> result = rpcService.getSgPhyOutMQResult(mqid, resultMap.get(mqid), resultItemMap.get(mqid), null, null, isLastObj, user);
                if (result.isOK()) {
                    bodys.add(result.getData());
                    sends.add(mqid);
                } else {
                    auditErrMap.put(mqid, "结果单id" + mqid + ",获取mqbody失败!" + result.getMessage());
                }
            } catch (Exception e) {
                e.printStackTrace();
                auditErrMap.put(mqid, "结果单id" + mqid + ",获取mqbody失败!" + e.getMessage());
            }
        }

        if (CollectionUtils.isNotEmpty(bodys)) {
            try {
                String configName = mqConfig.getConfigName();
                String msgKey = UUID.randomUUID().toString().replaceAll("-", "");
                String body = JSONObject.toJSONString(bodys);
                String msgID = r3MqSendHelper.sendMessage(configName, body, mqConfig.getOmsMqTopic(), SgOutConstants.TAG_OUT_RETAIL_VERIFY_BACK, msgKey);
                log.debug("结果单" + sends + ",审核回写订单MQ发送成功,msgID:{};", msgID);
                return sends;
            } catch (Exception e) {
                e.printStackTrace();
                log.error("结果单" + sends + ",审核回写订单MQ发送失败!" + e.getMessage());
                for (Long id : sends) {
                    auditErrMap.put(id, "结果单" + id + ",审核回写订单MQ发送失败!" + e.getMessage());
                }
                return null;
            }
        } else {
            return null;
        }
    }

    public List<Long> auditBills(List<Long> auditIds, Map<Long, SgBPhyOutResult> resultMap,
                                 Map<Long, List<SgBPhyOutResultItem>> resultItemMap, HashMap<Long, String> auditErrMap, String sysParam, User user) {
        SgPhyOutResultAuditWMSService bean = ApplicationContextHandle.getBean(SgPhyOutResultAuditWMSService.class);
        List<Long> ids = Lists.newArrayList();
        for (Long auditId : auditIds) {
            try {
                SgBPhyOutResult result = resultMap.get(auditId);
                List<SgBPhyOutResultItem> resultItems = resultItemMap.get(auditId);
                bean.auditSingleBill(result, resultItems, result.getOutTime(), sysParam, user);
                ids.add(auditId);
                //推送es
                warehouseESUtils.pushESByOutResult(auditId, false, false, null, resultMapper, null);
            } catch (Exception e) {
                e.printStackTrace();
                String messageExtra = AssertUtils.getMessageExtra("结果单" + auditId + ",审核异常!", e);
                log.error(messageExtra);
                auditErrMap.put(auditId, messageExtra);
            }
        }
        return ids;
    }

    @Transactional(rollbackFor = Exception.class)
    public void auditSingleBill(SgBPhyOutResult result, List<SgBPhyOutResultItem> resultItems, Date outTime, String sysParam, User user) {
        //回写通知单
        noticesSaveService.updateOutNoticesByResult(user, result.getSgBPhyOutNoticesId(), result.getIsLast(), resultItems, outTime, result,null);

        //更新结果单
        SgBPhyOutResult update = new SgBPhyOutResult();
        update.setId(result.getId());
        update.setBillStatus(SgOutConstantsIF.OUT_RESULT_STATUS_AUDIT);
        update.setOutId((long) user.getId());
        update.setOutName(user.getName());
        update.setOutEname(user.getEname());
        update.setOutTime(outTime != null ? outTime : new Date());
        StorageESUtils.setBModelDefalutDataByUpdate(update, user);
        resultMapper.updateById(update);

        //调用逻辑发货单出库服务
        OutNoticeUtils noticeUtils = ApplicationContextHandle.getBean(OutNoticeUtils.class);
        boolean isDiffDeal = noticeUtils.allowNegative(result.getSgBPhyOutNoticesId());
        ValueHolderV14 sendResult = rpcService.sgBSendOut(result, resultItems, null, null, user, false, isDiffDeal);
        if (!sendResult.isOK()) {
            throw new NDSException(Resources.getMessage(sendResult.getMessage(), user.getLocale()));
        }

        //TODO 考虑库存更新后如何救回redis
        //更新实体仓库存
        ValueHolderV14 storageResult = rpcService.updateSgPhyStorage(result, resultItems, null, null, user, sysParam);
        if (!storageResult.isOK()) {
            throw new NDSException(Resources.getMessage(storageResult.getMessage(), user.getLocale()));
        }
    }


    public List<Long> setLocks(List<Long> resultIds, Map<Long, SgBPhyOutResult> resultMap, HashMap<Long, String> auditErrMap) {

        List<Long> auditIds = Lists.newArrayList();
        CusRedisTemplate<String, String> redisTemplate = RedisOpsUtil.getStrRedisTemplate();
        for (Long resultId : resultIds) {
            SgBPhyOutResult result = resultMap.get(resultId);
            Long sourceBillId = result.getSourceBillId();
            Integer sourceBillType = result.getSourceBillType();
            String lockKsy = SgConstants.SG_B_PHY_OUT_RESULT + ":" + sourceBillId + ":" + sourceBillType;
            if (redisTemplate.opsForValue().get(lockKsy) == null) {
                try {
                    redisTemplate.opsForValue().set(lockKsy, "OK", 30, TimeUnit.MINUTES);
                    auditIds.add(resultId);
                } catch (Exception e) {
                    String messageExtra = AssertUtils.getMessageExtra(lockKsy + "加锁失败!", e);
                    log.error(messageExtra);
                    auditErrMap.put(resultId, messageExtra);
                }
            } else {
                String msg = "当前单据审核中，请稍后重试！来源单据ID：" + sourceBillId + "来源单据类型：" + sourceBillType;
                log.debug(msg);
                auditErrMap.put(resultId, msg);
            }
        }
        return auditIds;
    }

    public ValueHolderV14<HashMap<String, Object>> checkParams(List<Long> ids, User user) {
        ValueHolderV14<HashMap<String, Object>> vh = new ValueHolderV14<>(ResultCode.SUCCESS, "success");
        HashMap<String, Object> data = Maps.newHashMap();
        if (CollectionUtils.isEmpty(ids)) {
            vh.setCode(ResultCode.FAIL);
            vh.setMessage("出库结果单不能为空!");
        } else {
            HashMap<Long, String> auditErrMap = Maps.newHashMap();
            List<Long> resultIds = Lists.newArrayList();
            List<Long> auditIds = Lists.newArrayList();

            List<SgBPhyOutResult> outResults = resultMapper.selectList(new QueryWrapper<SgBPhyOutResult>().lambda()
                    .in(SgBPhyOutResult::getId, ids)
                    .eq(SgBPhyOutResult::getBillStatus, SgOutConstantsIF.OUT_RESULT_STATUS_WAIT)
                    .eq(SgBPhyOutResult::getIsactive, SgConstants.IS_ACTIVE_Y));
            if (CollectionUtils.isEmpty(outResults)) {
                vh.setCode(ResultCode.FAIL);
                vh.setMessage("不存在待审核的出库结果单!");
            } else {
                //结果单id - 结果单
                Map<Long, SgBPhyOutResult> resultMap = outResults.stream().collect(Collectors.toMap(SgBPhyOutResult::getId, o -> o));
                data.put("resultMap", resultMap);

                //校验结果单状态是否为待审核
                for (Long id : ids) {
                    if (!resultMap.containsKey(id)) {
                        auditErrMap.put(id, "不存在待审核的出库结果单!");
                    } else {
                        auditIds.add(id);
                    }
                }

                //通知单id - 结果单List
                Map<Long, List<SgBPhyOutResult>> noticesMap = Maps.newHashMap();
                for (SgBPhyOutResult outResult : outResults) {
                    Long noticesId = outResult.getSgBPhyOutNoticesId();
                    if (noticesMap.containsKey(noticesId)) {
                        List<SgBPhyOutResult> results = noticesMap.get(noticesId);
                        results.add(outResult);
                    } else {
                        List<SgBPhyOutResult> results = Lists.newArrayList();
                        results.add(outResult);
                        noticesMap.put(noticesId, results);
                    }
                }

                //结果单id - 明细
                Map<Long, List<SgBPhyOutResultItem>> resultItemMap = Maps.newHashMap();
                List<SgBPhyOutResultItem> outResultItems = resultItemMapper.selectList(new QueryWrapper<SgBPhyOutResultItem>().lambda()
                        .in(SgBPhyOutResultItem::getSgBPhyOutResultId, auditIds));
                if (CollectionUtils.isEmpty(outResultItems)) {
                    vh.setCode(ResultCode.FAIL);
                    vh.setMessage("出库结果单明细为空，不允许操作！");
                } else {
                    for (SgBPhyOutResultItem outResultItem : outResultItems) {
                        Long resultId = outResultItem.getSgBPhyOutResultId();
                        if (resultItemMap.containsKey(resultId)) {
                            List<SgBPhyOutResultItem> resultItems = resultItemMap.get(resultId);
                            resultItems.add(outResultItem);
                        } else {
                            List<SgBPhyOutResultItem> resultItems = Lists.newArrayList();
                            resultItems.add(outResultItem);
                            resultItemMap.put(resultId, resultItems);
                        }
                    }

                    data.put("resultItemMap", resultItemMap);

                    //校验结果单明细是否为空
                    String sysParam = AdParamUtil.getParam(SgOutConstants.SYSTEM_WAREHOUSE);
                    log.debug("出库结果单审核是否允许负库存系统参数:" + sysParam);

                    data.put("sysParam", sysParam);

                    SgPhyOutResultAuditService auditSingleService = ApplicationContextHandle.getBean(SgPhyOutResultAuditService.class);
                    for (Long auditId : auditIds) {
                        List<SgBPhyOutResultItem> resultItems = resultItemMap.get(auditId);
                        if (CollectionUtils.isEmpty(resultItems)) {
                            auditErrMap.put(auditId, "出库结果单明细为空，不允许操作！");
                        } else {
                            SgBPhyOutResult result = resultMap.get(auditId);
                            try {
                                if (result.getIsLast() == SgOutConstantsIF.OUT_IS_LAST_Y) {
                                    List<SgBPhyOutResult> results = noticesMap.get(result.getSgBPhyOutNoticesId());
                                    if (results.size() > 1) {
                                        auditErrMap.put(auditId, "当前出库结果单为最后一次出库，存在未审核的出库结果单，不允许审核！");
                                        continue;
                                    }
                                }
                                //校验实体仓库存
                                auditSingleService.queryPhysicalStorage(user, result, resultItems, sysParam);
                                resultIds.add(auditId);
                            } catch (Exception e) {
                                e.printStackTrace();
                                log.error("实体仓库存不足!" + e.getMessage());
                                auditErrMap.put(auditId, "实体仓库存不足!" + e.getMessage());
                            }
                        }
                    }
                }

                if (CollectionUtils.isNotEmpty(resultIds)) {
                    data.put("resultIds", resultIds);
                    data.put("error", auditErrMap);
                    vh.setData(data);
                } else {
                    vh.setCode(ResultCode.FAIL);
                    vh.setMessage("参数check异常!");
                    data.put("error", auditErrMap);
                    vh.setData(data);
                }
            }
        }

        log.debug("check结束:{};", JSONObject.toJSONString(vh));
        return vh;
    }
}
