package com.jackrain.nea.sg.out.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.out.mapper.SgBOutPickorderItemMapper;
import com.jackrain.nea.sg.out.mapper.SgBOutPickorderTeusItemMapper;
import com.jackrain.nea.sg.out.model.request.SgPhyBOutPickOrderDelItemRequest;
import com.jackrain.nea.sg.out.model.request.SgPhyBOutPickOrderDelRequest;
import com.jackrain.nea.sg.out.model.table.SgBOutPickorderItem;
import com.jackrain.nea.sg.transfer.common.SgTransferConstantsIF;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author zhu lin yu
 * @since 2019-11-19
 * create at : 2019-11-19 16:45
 */
@Slf4j
@Component
public class SgPhyOutPickOrderDelService {

    @Transactional(rollbackFor = Exception.class)
    public ValueHolderV14 delOutPickorderItemNum(SgPhyBOutPickOrderDelRequest request) {
        ValueHolderV14 holderV14 = new ValueHolderV14(ResultCode.SUCCESS, "删除成功");

        if (log.isDebugEnabled()) {
            log.debug("Start SgPhyBOutPickorderDelService.delOutPickorderItemNum. ReceiveParams:params:{};"
                    + JSONObject.toJSONString(request));
        }

        if (request == null) {
            throw new NDSException("request is null");
        }

        AssertUtils.isTrue(request.getId() != null, "拣货单主表id不能为空");
        AssertUtils.isTrue(request.getLoginUser() != null, "用户信息不能为空");
        AssertUtils.isTrue(CollectionUtils.isNotEmpty(request.getDelItemRequest()), "删除的明细不能为空");


        try {
            List<SgPhyBOutPickOrderDelItemRequest> delItemRequestList = request.getDelItemRequest();

            //查询箱内被删除数量的记录的原始数量并按照箱号或者skuId聚合数量
            List<Long> teusItemIdList = new ArrayList<>();
            List<Long> teusIdList = new ArrayList<>();
            List<Long> skuIdList = new ArrayList<>();
            for (SgPhyBOutPickOrderDelItemRequest delRequest : delItemRequestList) {
                if (delRequest.getIsTeus() == SgTransferConstantsIF.IS_TEUS_Y) {
                    teusIdList.add(delRequest.getPickOutTeusId());
                } else {
                    skuIdList.add(delRequest.getPickOutTeusId());
                }
                teusItemIdList.add(delRequest.getId());
            }

            SgBOutPickorderTeusItemMapper teusItemMapper = ApplicationContextHandle.getBean(SgBOutPickorderTeusItemMapper.class);
            Map<Long, BigDecimal> teusSumQty = new HashMap<>();
            Map<Long, BigDecimal> skuSumQty = new HashMap<>();
            if (CollectionUtils.isNotEmpty(teusIdList)) {
                teusSumQty = teusItemMapper.selectTeusSumQty(teusIdList, request.getId());
            }

            if (CollectionUtils.isNotEmpty(skuIdList)) {
                skuSumQty = teusItemMapper.selectSkuSumQty(skuIdList, request.getId());
            }


            //查询拣货明细的原始数量
            List<SgBOutPickorderItem> sgBOutPickorderItems = new ArrayList<>();
            SgBOutPickorderItemMapper itemMapper = ApplicationContextHandle.getBean(SgBOutPickorderItemMapper.class);

            if (MapUtils.isNotEmpty(teusSumQty)) {
                List<Long> teusIds = new ArrayList<>(teusSumQty.keySet());
                sgBOutPickorderItems = itemMapper.selectList(new QueryWrapper<SgBOutPickorderItem>().lambda()
                        .eq(SgBOutPickorderItem::getSgBOutPickorderId, request.getId())
                        .in(SgBOutPickorderItem::getPsCTeusId, teusIds));
            }

            if (MapUtils.isNotEmpty(skuSumQty)) {
                List<Long> skuIds = new ArrayList<>(skuSumQty.keySet());
                sgBOutPickorderItems = itemMapper.selectList(new QueryWrapper<SgBOutPickorderItem>().lambda()
                        .eq(SgBOutPickorderItem::getSgBOutPickorderId, request.getId())
                        .in(SgBOutPickorderItem::getPsCSkuId, skuIds));
            }

            //跟新拣货明细的数量
            for (SgBOutPickorderItem sgBOutPickorderItem : sgBOutPickorderItems) {

                BigDecimal qty = teusSumQty.get(sgBOutPickorderItem.getPsCTeusId());
                if (qty != null) {
                    SgBOutPickorderItem updateItem = new SgBOutPickorderItem();
                    updateItem.setId(sgBOutPickorderItem.getId());
                    updateItem.setQtyOut(sgBOutPickorderItem.getQtyOut().subtract(qty));
                    updateItem.setQtyDiff(sgBOutPickorderItem.getQtyDiff().add(qty));
                    itemMapper.updateById(updateItem);
                }
            }

            //删除装箱明细
            teusItemMapper.deleteBatchIds(teusItemIdList);
        } catch (Exception e) {
            log.error(this.getClass().getName() + " 删除失败,{}",e.getMessage(),e);
            throw new NDSException(e.getMessage());
        }

        return holderV14;

    }

    @Transactional(rollbackFor = Exception.class)
    public ValueHolderV14 clearOutPickorderItemNum(SgPhyBOutPickOrderDelRequest request) {
        ValueHolderV14 holderV14 = new ValueHolderV14(ResultCode.SUCCESS, "清除成功");

        if (log.isDebugEnabled()) {
            log.debug("Start SgPhyBOutPickorderDelService.clearOutPickorderItemNum. ReceiveParams:params:{};"
                    + JSONObject.toJSONString(request));
        }

        if (request == null) {
            throw new NDSException("request is null");
        }

        AssertUtils.isTrue(request.getId() != null, "拣货单主表id不能为空");
        AssertUtils.isTrue(request.getLoginUser() != null, "用户信息不能为空");
        AssertUtils.isTrue(CollectionUtils.isNotEmpty(request.getDelItemRequest()), "清除的明细不能为空");


        try {
            List<SgPhyBOutPickOrderDelItemRequest> delItemRequestList = request.getDelItemRequest();

            //删除装箱明细
            List<Long> teusItemIdList = delItemRequestList.stream().map(SgPhyBOutPickOrderDelItemRequest::getId).collect(Collectors.toList());

            SgBOutPickorderTeusItemMapper teusItemMapper = ApplicationContextHandle.getBean(SgBOutPickorderTeusItemMapper.class);
            teusItemMapper.deleteBatchIds(teusItemIdList);


            //清空拣货明细的数量
            SgBOutPickorderItemMapper itemMapper = ApplicationContextHandle.getBean(SgBOutPickorderItemMapper.class);

            List<SgBOutPickorderItem> sgBOutPickorderItemList = itemMapper.selectList(new QueryWrapper<SgBOutPickorderItem>().lambda()
                    .eq(SgBOutPickorderItem::getSgBOutPickorderId, request.getId()));

            if (CollectionUtils.isNotEmpty(sgBOutPickorderItemList)) {
                sgBOutPickorderItemList.forEach(item -> {
                    SgBOutPickorderItem outPickorderItem = new SgBOutPickorderItem();
                    outPickorderItem.setId(item.getId());
                    outPickorderItem.setQtyDiff(item.getQtyNotice());
                    outPickorderItem.setQtyOut(BigDecimal.ZERO);
                    itemMapper.updateById(outPickorderItem);
                });
            }
        } catch (Exception e) {
            log.error(this.getClass().getName() + " 清空失败,{}",e.getMessage(),e);
            throw new NDSException(e.getMessage());
        }

        return holderV14;
    }
}
