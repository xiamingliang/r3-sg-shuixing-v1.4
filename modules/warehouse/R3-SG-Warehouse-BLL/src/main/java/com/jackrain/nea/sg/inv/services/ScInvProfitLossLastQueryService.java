package com.jackrain.nea.sg.inv.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.inv.mapper.ScBInventoryMapper;
import com.jackrain.nea.sg.inv.model.request.ScInvProfitLossMarginQueryCmdRequest;
import com.jackrain.nea.sg.inv.model.result.ScInvProfitLossQueryLastResult;
import com.jackrain.nea.sg.inv.model.table.ScBInventory;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.DateUtil;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Locale;

/**
 * @author 舒威
 * @since 2019/4/9
 * create at : 2019/4/9 10:04
 */
@Slf4j
@Component
public class ScInvProfitLossLastQueryService {

    @Autowired
    private ScBInventoryMapper inventoryMapper;

    /**
     * 查询最近一次未盈亏盘点单
     */
    ValueHolderV14<ScInvProfitLossQueryLastResult> queryLastProfitLossInventory(ScInvProfitLossMarginQueryCmdRequest model) {
        if (log.isDebugEnabled()) {
            log.debug("Start ScInvProfitLossLastQueryService.queryLastProfitLossInventory. ReceiveParams:request={};" + JSONObject.toJSONString(model));
        }

        User loginUser = model.getLoginUser();
        AssertUtils.notNull(loginUser, "当前用户未登录！");
        Locale locale = loginUser.getLocale();
        AssertUtils.isTrue(model.getStoreId() != null || model.getWarehouseId() != null, "请选择盘点店仓！", locale);

        ValueHolderV14<ScInvProfitLossQueryLastResult> v14 = new ValueHolderV14<>(ResultCode.SUCCESS, "success");
        //默认按创建时间降序排列
        List<ScBInventory> inventories = inventoryMapper.selectList(new QueryWrapper<ScBInventory>().lambda()
                .eq(model.getWarehouseId() != null, ScBInventory::getCpCPhyWarehouseId, model.getWarehouseId())
                .eq(model.getStoreId() != null, ScBInventory::getCpCStoreId, model.getStoreId())
                .eq(model.getPolStatus() != null, ScBInventory::getPolStatus, model.getPolStatus())
                .eq(ScBInventory::getIsactive, SgConstants.IS_ACTIVE_Y)
                .orderByDesc(ScBInventory::getCreationdate));
        if (CollectionUtils.isNotEmpty(inventories)) {
            ScInvProfitLossQueryLastResult lastResult = new ScInvProfitLossQueryLastResult();
            lastResult.setInventoryType(inventories.get(0).getInventoryType());
            lastResult.setInventoryDate(DateUtil.format(inventories.get(0).getInventoryDate(), "yyyy-MM-dd"));
            v14.setData(lastResult);
        }
        return v14;
    }
}
