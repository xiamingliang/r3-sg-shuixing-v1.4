package com.jackrain.nea.sg.phyadjust.mapper;

import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.phyadjust.model.table.SgBPhyAdjustImpItem;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.math.BigDecimal;

@Mapper
public interface SgBPhyAdjustImpItemMapper extends ExtentionMapper<SgBPhyAdjustImpItem> {

    /**
     * 查询录入明细 金额的汇总
     *
     * @param adjId 主表id
     * @return fee汇总
     */
    @Select("select sum(adjust_fee) from sg_b_phy_adjust_imp_item where sg_b_phy_adjust_id = #{adjId} ")
    BigDecimal selectSumFeeByAdjId(@Param("adjId") Long adjId);
}