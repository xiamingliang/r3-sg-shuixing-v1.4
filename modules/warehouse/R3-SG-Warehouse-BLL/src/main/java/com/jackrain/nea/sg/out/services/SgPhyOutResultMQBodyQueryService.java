package com.jackrain.nea.sg.out.services;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.in.mapper.SgBPhyInNoticesMapper;
import com.jackrain.nea.sg.in.model.table.SgBPhyInNotices;
import com.jackrain.nea.sg.out.common.SgOutConstantsIF;
import com.jackrain.nea.sg.out.mapper.SgBPhyOutResultMapper;
import com.jackrain.nea.sg.out.model.result.SgOutResultSendMsgResult;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author: 舒威
 * @since: 2019/9/2
 * create at : 2019/9/2 20:37
 */
@Slf4j
@Component
public class SgPhyOutResultMQBodyQueryService {

    public ValueHolderV14<List<SgOutResultSendMsgResult>> querySgPhyOutResultMQBody(List<Long> ids, User user) {
        if (log.isDebugEnabled()) {
            log.debug("出库结果单获取mqbody入参:" + ids);
        }
        ValueHolderV14<List<SgOutResultSendMsgResult>> vh = new ValueHolderV14<>(ResultCode.SUCCESS, "success");
        SgPhyOutRPCService rpcService = ApplicationContextHandle.getBean(SgPhyOutRPCService.class);
        List<SgOutResultSendMsgResult> data = Lists.newArrayList();
        List<Long> error = Lists.newArrayList();
        List<Long> success = Lists.newArrayList();
        for (Long id : ids) {
            try {
                ValueHolderV14<SgOutResultSendMsgResult> mqResult = rpcService.getSgPhyOutMQResult(id, null, null, null,null,null, user);
                if (mqResult.isOK()) {
                    data.add(mqResult.getData());
                    success.add(id);
                } else {
                    AssertUtils.logAndThrow("出库结果单id[" + id + "]查询mqbody失败!" + mqResult.getMessage());
                }
            } catch (Exception e) {
                log.error("出库结果单id[" + id + "]查询mqbody异常!" + e.getMessage());
                error.add(id);
            }
        }
        if (CollectionUtils.isNotEmpty(data)) {
            vh.setData(data);
        } else {
            vh.setCode(ResultCode.FAIL);
            vh.setMessage("查询mqbody失败!失败出库结果单ids:" + error);
        }
        if (log.isDebugEnabled()) {
            log.debug("出库结果单获取mqbody完成!总待补全ids:{},成功ids:{},失败结果单ids:{}", ids, success, error);
        }
        return vh;
    }


    public ValueHolderV14<List<SgOutResultSendMsgResult>> querySgPhyOutResultJITXMQBody(List<Long> sourceBillIds, User user) {
        if (log.isDebugEnabled()) {
            log.debug("JITX补偿任务查询生成3张单据入参:" + sourceBillIds);
        }
        long time = System.currentTimeMillis();
        ValueHolderV14<List<SgOutResultSendMsgResult>> vh = new ValueHolderV14<>(ResultCode.SUCCESS, "success");
        if (CollectionUtils.isNotEmpty(sourceBillIds)) {
            //仅支持50/次查询
            if (sourceBillIds.size() > SgConstants.SG_COMMON_MAX_QUERY_PAGE_SIZE) {
                vh.setCode(ResultCode.FAIL);
                vh.setMessage("查询长度超过最大限制数" + SgConstants.SG_COMMON_MAX_QUERY_PAGE_SIZE + ",请分批次查询!");
                return vh;
            }
            /*若来源单据已生成入库通知单，则不需要进行补偿*/
            //记录需要补偿的来源单据ids
            List<Long> sources = Lists.newArrayList();
            SgBPhyInNoticesMapper inNoticesMapper = ApplicationContextHandle.getBean(SgBPhyInNoticesMapper.class);
            List<SgBPhyInNotices> notices = inNoticesMapper.selectList(new QueryWrapper<SgBPhyInNotices>().lambda().select(SgBPhyInNotices::getSourceBillId)
                    .in(SgBPhyInNotices::getSourceBillId, sourceBillIds)
                    .eq(SgBPhyInNotices::getSourceBillType, SgConstantsIF.BILL_TYPE_RETAIL)
                    .eq(SgBPhyInNotices::getIsactive, SgConstants.IS_ACTIVE_Y));
            if (CollectionUtils.isNotEmpty(notices)) {
                List<Long> collect = notices.stream().map(SgBPhyInNotices::getSourceBillId).collect(Collectors.toList());
                HashMap<Long, Object> map = Maps.newHashMap();
                for (Long aLong : collect) {
                    map.put(aLong, null);
                }
                for (Long billId : sourceBillIds) {
                    if (!map.containsKey(billId)) {
                        sources.add(billId);
                    } else {
                        log.error("JITX:订单id[" + billId + "],已生成入库通知单!");
                    }
                }
                if (CollectionUtils.isEmpty(sources)) {
                    vh.setCode(ResultCode.FAIL);
                    vh.setMessage("JITX:订单ids" + sourceBillIds + ",已生成入库通知单!");
                    return vh;
                }
            } else {
                sources = sourceBillIds;
            }

            SgBPhyOutResultMapper resultMapper = ApplicationContextHandle.getBean(SgBPhyOutResultMapper.class);
            List<SgBPhyOutResult> outResults = resultMapper.selectList(new QueryWrapper<SgBPhyOutResult>().lambda()
                    .select(SgBPhyOutResult::getId)
                    .in(SgBPhyOutResult::getSourceBillId, sources)
                    .eq(SgBPhyOutResult::getSourceBillType, SgConstantsIF.BILL_TYPE_RETAIL)
                    .eq(SgBPhyOutResult::getBillStatus, SgOutConstantsIF.OUT_RESULT_STATUS_AUDIT)
                    .eq(SgBPhyOutResult::getIsactive, SgConstants.IS_ACTIVE_Y));

            if (CollectionUtils.isNotEmpty(outResults)) {
                List<Long> ids = outResults.stream().map(SgBPhyOutResult::getId).distinct().collect(Collectors.toList());
                try {
                    ValueHolderV14<List<SgOutResultSendMsgResult>> queryResult = querySgPhyOutResultMQBody(ids, user);
                    if (queryResult.isOK()) {
                        List<SgOutResultSendMsgResult> resultData = queryResult.getData();
                        if (CollectionUtils.isNotEmpty(resultData)) {
                            vh.setData(resultData);
                        }
                    } else {
                        AssertUtils.logAndThrow(queryResult.getMessage());
                    }
                } catch (Exception e) {
                    vh.setCode(ResultCode.FAIL);
                    vh.setMessage("JITX:出库结果单ids[" + ids + "]获取mqbody信息接口异常!" + e.getMessage());
                }
            } else {
                vh.setCode(ResultCode.FAIL);
                vh.setMessage("JITX:订单ids[" + sources + "]没有对应的出库结果单!");
            }
        } else {
            vh.setCode(ResultCode.FAIL);
            vh.setMessage("来源单据暂未生成结果单!");
        }
        if (log.isDebugEnabled()) {
            log.debug("JITX补偿任务查询生成3张单据成功!return result:{},spend time:{}",
                    JSONObject.toJSONString(vh, SerializerFeature.WriteMapNullValue), System.currentTimeMillis() - time);
        }
        return vh;
    }
}
