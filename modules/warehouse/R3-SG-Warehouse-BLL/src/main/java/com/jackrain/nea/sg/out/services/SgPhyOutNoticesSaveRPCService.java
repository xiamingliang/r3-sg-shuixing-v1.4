package com.jackrain.nea.sg.out.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.model.util.ModelUtil;
import com.jackrain.nea.sg.basic.common.R3ParamConstants;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.common.SystemUserResource;
import com.jackrain.nea.sg.basic.config.SgStorageBoxConfig;
import com.jackrain.nea.sg.basic.model.result.SgR3BaseResult;
import com.jackrain.nea.sg.basic.model.result.SgStorageUpdateResult;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.out.mapper.SgBPhyOutNoticesImpItemMapper;
import com.jackrain.nea.sg.out.mapper.SgBPhyOutNoticesItemMapper;
import com.jackrain.nea.sg.out.mapper.SgBPhyOutNoticesMapper;
import com.jackrain.nea.sg.out.model.request.*;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutNotices;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutNoticesImpItem;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutNoticesItem;
import com.jackrain.nea.sg.send.common.SgSendConstants;
import com.jackrain.nea.sg.send.common.SgSendConstantsIF;
import com.jackrain.nea.sg.send.mapper.SgBSendImpItemMapper;
import com.jackrain.nea.sg.send.mapper.SgBSendItemMapper;
import com.jackrain.nea.sg.send.mapper.SgBSendMapper;
import com.jackrain.nea.sg.send.model.request.SgSendBillSaveRequest;
import com.jackrain.nea.sg.send.model.request.SgSendImpItemSaveRequest;
import com.jackrain.nea.sg.send.model.request.SgSendItemSaveRequest;
import com.jackrain.nea.sg.send.model.request.SgSendSaveRequest;
import com.jackrain.nea.sg.send.model.table.SgBSend;
import com.jackrain.nea.sg.send.model.table.SgBSendImpItem;
import com.jackrain.nea.sg.send.model.table.SgBSendItem;
import com.jackrain.nea.sg.send.services.SgSendStorageService;
import com.jackrain.nea.sg.send.utils.SgStoreESUtils;
import com.jackrain.nea.sg.send.utils.SgStoreUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * 同时生成三张单据-逻辑发货单、出库通知单、出库结果单
 *
 * @author 舒威
 * @since 2019/11/22
 * create at : 2019/11/22 17:32
 */
@Slf4j
@Component
public class SgPhyOutNoticesSaveRPCService {

    @Autowired
    private SgPhyOutNoticesSaveService noticesSaveService;

    @Autowired
    private SgStorageBoxConfig storageBoxConfig;

    @Autowired
    private SgStoreESUtils storeESUtils;

    public ValueHolderV14 batchSaveOutBills(List<SgPhyOutNoticesSaveRPCRequest> requests) {
        if (log.isDebugEnabled()) {
            log.debug("Start SgPhyOutNoticesSaveRPCService.batchSaveOutBills. ReceiveParams:request:{};", JSONObject.toJSONString(requests));
        }

        ValueHolderV14 vh = new ValueHolderV14(ResultCode.SUCCESS, "success");

        checkParams(requests);

        List<Long> ids = Lists.newArrayList();
        for (SgPhyOutNoticesSaveRPCRequest request : requests) {
            SgPhyOutNoticesBillSaveRequest billSaveRequest = request.getBillSaveRequest();
            billSaveRequest.getOutNoticesRequest().setId(-1L);
            //1.新增出库通知单
            ValueHolderV14<SgR3BaseResult> saveOutNoticesResult = noticesSaveService.saveSgPhyOutNotices(billSaveRequest);
            if (!saveOutNoticesResult.isOK()) {
                vh.setCode(ResultCode.FAIL);
                vh.setMessage("新增出库通知单失败！" + saveOutNoticesResult.getMessage());
                return vh;
            }

            SgR3BaseResult data = saveOutNoticesResult.getData();
            Long noticesId = data.getDataJo().getLong(R3ParamConstants.OBJID);
            ids.add(noticesId);

            //2.新增逻辑发货单
            if (request.getSgSendFlag()) {
                try {
                    saveSgBSend(request);
                } catch (Exception e) {
                    e.printStackTrace();
                    vh.setCode(ResultCode.FAIL);
                    vh.setMessage("新增逻辑发货单失败！" + e.getMessage());
                    return vh;
                }
            }
        }

        //3.新增并审核出库结果单
        List<SgPhyOutResultBillSaveRequest> resultsBill = getBatchResultsBill(ids, null, false, false, false);
        SgPhyOutResultSaveAndAuditService saveAndAuditService = ApplicationContextHandle.getBean(SgPhyOutResultSaveAndAuditService.class);
        for (SgPhyOutResultBillSaveRequest request : resultsBill) {
            ValueHolderV14 outResultAndAudit = saveAndAuditService.saveOutResultAndAudit(request);
            if (!outResultAndAudit.isOK()) {
                vh.setCode(ResultCode.FAIL);
                vh.setMessage("新增并审核出库结果单失败！" + outResultAndAudit.getMessage());
                return vh;
            }
        }

        return vh;
    }

    public List<SgPhyOutResultBillSaveRequest> getBatchResultsBill(List<Long> billIds, User loginUser,
                                                                   boolean isOneClickLibrary, boolean isSendPos, boolean isCancelOut) {
        List<SgPhyOutResultBillSaveRequest> allList = new ArrayList<>();
        //分批获取出库通知单(每次500条)
        int listSize = billIds.size();
        int pageSize = SgConstants.SG_COMMON_INSERT_PAGE_SIZE;
        int page = listSize / pageSize;

        if (listSize % pageSize != 0) {
            page++;
        }

        List<List<Long>> idsList = Lists.newArrayList();
        for (int i = 0; i < page; i++) {
            List<Long> pageList = billIds.subList(i * pageSize,
                    (((i + 1) * pageSize > listSize ? listSize : pageSize * (i + 1))));
            idsList.add(pageList);
        }

        if (CollectionUtils.isNotEmpty(idsList)) {
            for (List<Long> ids : idsList) {
                //获取本批所有的通知单(已组装好的数据)
                List<SgPhyOutResultBillSaveRequest> list = getAssemblyParameters(ids, loginUser, isOneClickLibrary, isSendPos, isCancelOut);
                allList.addAll(list);
            }
        }

        return allList;
    }

    public List<SgPhyOutResultBillSaveRequest> getAssemblyParameters(List<Long> ids, User loginUser,
                                                                     boolean isOneClickLibrary, boolean isSendPos, boolean isCancelOut) {
        SgBPhyOutNoticesMapper noticesMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesMapper.class);
        List<SgPhyOutResultBillSaveRequest> billList = Lists.newArrayList();
        //获取出库通知单主表数据
        List<SgBPhyOutNotices> noticesList = noticesMapper.selectList(new QueryWrapper<SgBPhyOutNotices>().lambda().in(SgBPhyOutNotices::getId, ids));

        User user = Optional.ofNullable(loginUser).orElse(SystemUserResource.getRootUser());

        noticesList.forEach(notices -> {
            SgPhyOutResultBillSaveRequest resultBillSaveRequest = new SgPhyOutResultBillSaveRequest();
            resultBillSaveRequest.setObjId(-1L);
            SgPhyOutResultSaveRequest resultSaveRequest = new SgPhyOutResultSaveRequest();
            resultSaveRequest.setId(-1L);
            resultSaveRequest.setCpCPhyWarehouseId(notices.getCpCPhyWarehouseId());
            resultSaveRequest.setCpCPhyWarehouseEcode(notices.getCpCPhyWarehouseEcode());
            resultSaveRequest.setCpCPhyWarehouseEname(notices.getCpCPhyWarehouseEname());
            resultSaveRequest.setBillNo(notices.getBillNo());
            resultSaveRequest.setSgBPhyOutNoticesId(notices.getId());
            resultSaveRequest.setSgBPhyOutNoticesBillno(notices.getBillNo());
            resultSaveRequest.setGoodsOwner(notices.getGoodsOwner());
            resultSaveRequest.setOutId(user.getId().longValue());
            resultSaveRequest.setOutEcode("");
            resultSaveRequest.setOutEname(user.getEname());
            resultSaveRequest.setOutName(user.getName());
            Date inTime = notices.getOutTime() != null ? notices.getOutTime() : new Date();
            resultSaveRequest.setOutTime(inTime);
            resultSaveRequest.setOutType(notices.getOutType());
            resultSaveRequest.setCpCCustomerWarehouseId(notices.getCpCCustomerWarehouseId());
            resultSaveRequest.setCpCCsEcode(notices.getCpCCsEcode());
            resultSaveRequest.setCpCCsEname(notices.getCpCCsEname());
            resultSaveRequest.setCpCSupplierId(notices.getCpCSupplierId());
            resultSaveRequest.setSourceBillType(notices.getSourceBillType());
            resultSaveRequest.setSourceBillId(notices.getSourceBillId());
            resultSaveRequest.setSourceBillNo(notices.getSourceBillNo());
            resultSaveRequest.setReceiverName(notices.getReceiverName());
            resultSaveRequest.setReceiverMobile(notices.getReceiverMobile());
            resultSaveRequest.setReceiverPhone(notices.getReceiverPhone());
            resultSaveRequest.setCpCRegionProvinceId(notices.getCpCRegionProvinceId());
            resultSaveRequest.setCpCRegionProvinceEcode(notices.getCpCRegionProvinceEcode());
            resultSaveRequest.setCpCRegionProvinceEname(notices.getCpCRegionProvinceEname());
            resultSaveRequest.setCpCRegionCityId(notices.getCpCRegionCityId());
            resultSaveRequest.setCpCRegionCityEcode(notices.getCpCRegionCityEcode());
            resultSaveRequest.setCpCRegionCityEname(notices.getCpCRegionCityEname());
            resultSaveRequest.setCpCRegionAreaId(notices.getCpCRegionAreaId());
            resultSaveRequest.setCpCRegionAreaEcode(notices.getCpCRegionAreaEcode());
            resultSaveRequest.setCpCRegionAreaEname(notices.getCpCRegionAreaEname());
            resultSaveRequest.setReceiverAddress(notices.getReceiverAddress());
            resultSaveRequest.setReceiverZip(notices.getReceiverZip());
            resultSaveRequest.setCpCLogisticsId(notices.getCpCLogisticsId());
            resultSaveRequest.setCpCLogisticsEcode(notices.getCpCLogisticsEcode());
            resultSaveRequest.setCpCLogisticsEname(notices.getCpCLogisticsEname());
            resultSaveRequest.setLogisticNumber(notices.getLogisticNumber());
            resultSaveRequest.setCpCShopId(notices.getCpCShopId());
            resultSaveRequest.setCpCShopTitle(notices.getCpCShopTitle());
            resultSaveRequest.setSourcecode(notices.getSourcecode());
            resultSaveRequest.setSellerRemark(notices.getSellerRemark());
            resultSaveRequest.setBuyerRemark(notices.getBuyerRemark());
            resultSaveRequest.setTotQtyOut(notices.getTotQtyOut());
            resultSaveRequest.setTotAmtListOut(notices.getTotAmtListOut());
            resultSaveRequest.setRemark(notices.getRemark());
            resultSaveRequest.setIsLast(SgConstants.IS_LAST_YES);
            //设置主表数据
            resultBillSaveRequest.setOutResultRequest(resultSaveRequest);
            //设置明细数据
            getItemList(notices.getId(), isOneClickLibrary, isSendPos, isCancelOut, resultBillSaveRequest);

            resultBillSaveRequest.setLoginUser(user);
            billList.add(resultBillSaveRequest);
        });
        return billList;
    }

    public void getItemList(Long id, boolean isOneClickLibrary, boolean isSendPos, boolean isCancelOut, SgPhyOutResultBillSaveRequest resultBillSaveRequest) {
        if (storageBoxConfig.getBoxEnable()) {
            SgBPhyOutNoticesImpItemMapper noticesImpItemMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesImpItemMapper.class);
            List<SgBPhyOutNoticesImpItem> impItemList = noticesImpItemMapper.selectList(new QueryWrapper<SgBPhyOutNoticesImpItem>().lambda()
                    .eq(SgBPhyOutNoticesImpItem::getSgBPhyOutNoticesId, id));
            List<SgPhyOutResultImpItemSaveRequest> impList = Lists.newArrayList();
            Boolean isScanZero = true;
            for (SgBPhyOutNoticesImpItem item : impItemList) {
                SgPhyOutResultImpItemSaveRequest impItemSaveRequest = new SgPhyOutResultImpItemSaveRequest();
                BeanUtils.copyProperties(item, impItemSaveRequest);
                impItemSaveRequest.setId(-1L);
                //TODO 录入明细一键出入库 入库数量待确认
                if (isSendPos) {
                    //全渠道发货单 发货数量取扫描数量
                    BigDecimal qtyDiff = Optional.ofNullable(item.getQtyDiff()).orElse(BigDecimal.ZERO);
                    BigDecimal qtyScan = Optional.ofNullable(item.getQtyScan()).orElse(BigDecimal.ZERO);
                    if (BigDecimal.ZERO.compareTo(qtyScan) != 0) {
                        isScanZero = false;
                    }
                    if (qtyScan.abs().compareTo(qtyDiff.abs()) != 0) {
                        AssertUtils.logAndThrow("商品" + item.getPsCSkuEcode() + "扫描部分商品不允许发货！");
                    }
                    impItemSaveRequest.setQtyOut(qtyScan);
                } else if (isCancelOut) {
                    //取消出库 发货数量取原通知单明细的逆向数量
                    BigDecimal qtyOut = Optional.ofNullable(item.getQtyOut()).orElse(BigDecimal.ZERO);
                    impItemSaveRequest.setQtyOut(qtyOut.negate());
                } else {
                    //正常出库
                    BigDecimal qtyOut = Optional.ofNullable(item.getQty()).orElse(BigDecimal.ZERO);
                    impItemSaveRequest.setQtyOut(qtyOut);
                }
                impItemSaveRequest.setPriceList(item.getPriceList());
                impList.add(impItemSaveRequest);
            }
            if (isSendPos && isScanZero) {
                AssertUtils.logAndThrow("扫描商品数量全部为0,请确认实际出库数量后再操作！");
            }
            resultBillSaveRequest.setImpItemList(impList);
        } else {
            SgBPhyOutNoticesItemMapper noticesItemMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesItemMapper.class);
            List<SgBPhyOutNoticesItem> itemList = noticesItemMapper.selectList(new QueryWrapper<SgBPhyOutNoticesItem>().lambda()
                    .eq(SgBPhyOutNoticesItem::getSgBPhyOutNoticesId, id));
            List<SgPhyOutResultItemSaveRequest> list = Lists.newArrayList();
            itemList.forEach(item -> {
                SgPhyOutResultItemSaveRequest itemRequest = new SgPhyOutResultItemSaveRequest();
                BeanUtils.copyProperties(item, itemRequest);
                itemRequest.setId(-1L);
                BigDecimal qtyOut = isOneClickLibrary ? item.getQty() : item.getReserveDecimal01();
                qtyOut = Optional.ofNullable(qtyOut).orElse(BigDecimal.ZERO);
                itemRequest.setQty(qtyOut);
                itemRequest.setSgBPhyOutNoticesItemId(item.getId());
                itemRequest.setGbcode(item.getGbcode());
                list.add(itemRequest);
            });
            resultBillSaveRequest.setOutResultItemRequests(list);
        }
    }

    /**
     * 新增逻辑发货单-先占单后生成库存单据
     */
    private void saveSgBSend(SgPhyOutNoticesSaveRPCRequest request) {
        SgPhyOutNoticesBillSaveRequest noticesBillSaveRequest = request.getBillSaveRequest();
        User loginUser = noticesBillSaveRequest.getLoginUser();
        //TODO 通知单会合并相同sku记录 避免影响逻辑发货单 此处获取通知单明细合并后数据
        //来源单据id、来源单据类型
        Long sourceBillId = noticesBillSaveRequest.getOutNoticesRequest().getSourceBillId();
        Integer sourceBillType = noticesBillSaveRequest.getOutNoticesRequest().getSourceBillType();
        SgBPhyOutNoticesItemMapper noticesItemMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesItemMapper.class);
        SgBPhyOutNoticesImpItemMapper noticesImpItemMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesImpItemMapper.class);
        List<SgBPhyOutNoticesItem> outNoticesItems = noticesItemMapper.selectListBySourceBill(sourceBillId, sourceBillType);
        List<SgBPhyOutNoticesImpItem> outNoticesImpItems = noticesImpItemMapper.selectListBySourceBill(sourceBillId, sourceBillType);

        SgSendBillSaveRequest sendBillSaveRequest = new SgSendBillSaveRequest();
        BigDecimal totQtyPreout = BigDecimal.ZERO;
        List<SgSendItemSaveRequest> var = Lists.newArrayList();
        if (CollectionUtils.isNotEmpty(outNoticesItems)) {
            for (SgBPhyOutNoticesItem noticesItem : outNoticesItems) {
                SgSendItemSaveRequest itemSaveRequest = new SgSendItemSaveRequest();
                BeanUtils.copyProperties(noticesItem, itemSaveRequest);
                BigDecimal qty = itemSaveRequest.getQty();
                itemSaveRequest.setCpCStoreId(request.getCpCStoreId());
                itemSaveRequest.setCpCStoreEcode(request.getCpCStoreEcode());
                itemSaveRequest.setCpCStoreEname(request.getCpCStoreEname());
                itemSaveRequest.setSourceBillItemId(noticesItem.getSourceBillItemId());
                itemSaveRequest.setQtyPreout(qty);
                itemSaveRequest.setId(ModelUtil.getSequence(SgSendConstants.BILL_SEND_ITEM));
                var.add(itemSaveRequest);
                totQtyPreout = totQtyPreout.add(qty);
            }
        }
        sendBillSaveRequest.setItemList(var);

        List<SgSendImpItemSaveRequest> impItemSaveRequests = Lists.newArrayList();
        if (CollectionUtils.isNotEmpty(outNoticesImpItems)) {
            for (SgBPhyOutNoticesImpItem noticesImpItem : outNoticesImpItems) {
                SgSendImpItemSaveRequest impItemSaveRequest = new SgSendImpItemSaveRequest();
                BeanUtils.copyProperties(noticesImpItem, impItemSaveRequest);
                BigDecimal qty = noticesImpItem.getQty();
                impItemSaveRequest.setCpCStoreId(request.getCpCStoreId());
                impItemSaveRequest.setCpCStoreEcode(request.getCpCStoreEcode());
                impItemSaveRequest.setCpCStoreEname(request.getCpCStoreEname());
                impItemSaveRequest.setSourceBillItemId(noticesImpItem.getSourceBillItemId());
                impItemSaveRequest.setQtyPreout(qty);
                impItemSaveRequest.setId(ModelUtil.getSequence(SgConstants.SG_B_SEND_IMP_ITEM));
                impItemSaveRequests.add(impItemSaveRequest);
            }
        }
        sendBillSaveRequest.setImpItemList(impItemSaveRequests);
        // 2020-04-21 23:31修改：pos过来允许负库存
        sendBillSaveRequest.setIsNegativePreout(Boolean.TRUE);

        //逻辑发货单主表
        SgBSend send = new SgBSend();
        Long id = ModelUtil.getSequence(SgSendConstants.BILL_SEND);
        send.setId(id);
        String billNo = SgStoreUtils.getBillNo(SgSendConstants.SEQ_SEND.toUpperCase(), SgSendConstants.BILL_SEND.toUpperCase(), send, loginUser.getLocale());
        send.setBillNo(billNo);
        SgPhyOutNoticesSaveRequest outNoticesRequest = noticesBillSaveRequest.getOutNoticesRequest();
        send.setBillDate(outNoticesRequest.getBillDate());
        send.setSourceBillId(outNoticesRequest.getSourceBillId());
        send.setSourceBillNo(outNoticesRequest.getSourceBillNo());
        send.setSourceBillType(outNoticesRequest.getSourceBillType());
        send.setTotQtyPreout(totQtyPreout);
        send.setBillStatus(SgSendConstantsIF.BILL_SEND_STATUS_CREATE);
        send.setOwnerename(loginUser.getEname());
        send.setModifierename(loginUser.getEname());
        send.setServiceNode(updateServiceNode(send.getSourceBillType()));
        StorageESUtils.setBModelDefalutData(send, loginUser);

        SgSendSaveRequest sendSaveRequest = new SgSendSaveRequest();
        BeanUtils.copyProperties(send, sendSaveRequest);
        sendBillSaveRequest.setSgSend(sendSaveRequest);
        sendBillSaveRequest.setLoginUser(loginUser);

        //执行占单
        SgSendStorageService storageService = ApplicationContextHandle.getBean(SgSendStorageService.class);
        ValueHolderV14<SgStorageUpdateResult> storageResult = storageService.updateSendStoragePreout(send, null, sendBillSaveRequest, Maps.newHashMap(), null, loginUser, false, request.getNegativeStock(), false, false, null, null, null, null, null, Maps.newHashMap());
        if (storageResult.isOK()) {
            SgStoreUtils.isSuccess("逻辑发货单保存失败!", storageResult);
            //占单成功 新增逻辑发货单
            try {
                //新增主表
                SgBSendMapper sendMapper = ApplicationContextHandle.getBean(SgBSendMapper.class);
                sendMapper.insert(send);

                //新增-逻辑发货单明细
                List<SgBSendItem> items = Lists.newArrayList();
                if (CollectionUtils.isNotEmpty(var)) {
                    for (SgSendItemSaveRequest item : var) {
                        SgBSendItem sendItem = new SgBSendItem();
                        BeanUtils.copyProperties(item, sendItem);
                        sendItem.setSgBSendId(send.getId());
                        sendItem.setOwnerename(loginUser.getEname());
                        sendItem.setModifierename(loginUser.getEname());
                        StorageESUtils.setBModelDefalutData(sendItem, loginUser);
                        items.add(sendItem);
                    }
                }

                SgBSendItemMapper itemMapper = ApplicationContextHandle.getBean(SgBSendItemMapper.class);
                if (CollectionUtils.isNotEmpty(items)) {
                    SgStoreUtils.batchInsertTeus(items, "逻辑发货单条码明细", SgConstants.SG_COMMON_INSERT_PAGE_SIZE, itemMapper);
                }

                if (storageBoxConfig.getBoxEnable()) {
                    //新增-逻辑发货单录入明细
                    List<SgBSendImpItem> impItems = Lists.newArrayList();
                    for (SgSendImpItemSaveRequest item : impItemSaveRequests) {
                        SgBSendImpItem impItem = new SgBSendImpItem();
                        BeanUtils.copyProperties(item, impItem);
                        impItem.setSgBSendId(send.getId());
                        impItem.setOwnerename(loginUser.getEname());
                        impItem.setModifierename(loginUser.getEname());
                        StorageESUtils.setBModelDefalutData(impItem, loginUser);
                        impItems.add(impItem);
                    }

                    SgBSendImpItemMapper impItemMapper = ApplicationContextHandle.getBean(SgBSendImpItemMapper.class);
                    if (CollectionUtils.isNotEmpty(impItems)) {
                        SgStoreUtils.batchInsertTeus(impItems, "逻辑发货单录入明细", SgConstants.SG_COMMON_INSERT_PAGE_SIZE, impItemMapper);
                    }
                }

                //推送ES－条码明细
                storeESUtils.pushESBySend(send.getId(), true, false, null, sendMapper, itemMapper);
            } catch (Exception e) {
                e.printStackTrace();
                AssertUtils.logAndThrow("保存逻辑发货单异常:" + e.getMessage(), loginUser.getLocale());
            }
        } else {
            AssertUtils.logAndThrow("占单失败！" + storageResult.getMessage());
        }
    }

    private void checkParams(List<SgPhyOutNoticesSaveRPCRequest> requests) {
        AssertUtils.notEmpty(requests, "参数不能为空!");
        for (SgPhyOutNoticesSaveRPCRequest request : requests) {
            // 根据来源单据ID和类型判断出库通知单是否已经存在
            SgPhyOutNoticesSaveService noticesSaveService = ApplicationContextHandle.getBean(SgPhyOutNoticesSaveService.class);
            SgPhyOutNoticesSaveRequest outNoticesRequest = request.getBillSaveRequest().getOutNoticesRequest();
            SgBPhyOutNotices outNotices = noticesSaveService.queryNoticesBySource(outNoticesRequest);
            AssertUtils.isTrue(outNotices == null, outNoticesRequest.getSourceBillNo() + "已存在出库通知单,不允许重复新增!");
            if (request.getSgSendFlag() && (request.getCpCStoreId() == null ||
                    StringUtils.isEmpty(request.getCpCStoreEcode()) || StringUtils.isEmpty(request.getCpCStoreEname()))) {
                AssertUtils.logAndThrow(outNotices.getSourceBillNo() + "逻辑仓信息不能为空!");
            }
        }
    }

    private Long updateServiceNode(Integer billType) {
        switch (billType) {
            case SgConstantsIF.BILL_TYPE_SALE:
                return SgConstantsIF.SERVICE_NODE_SALE_SUBMIT;
            case SgConstantsIF.BILL_TYPE_TRANSFER:
                return SgConstantsIF.SERVICE_NODE_TRANSFER_SUBMIT;
            case SgConstantsIF.BILL_TYPE_RETAIL_POS:
                return SgConstantsIF.SERVICE_NODE_RETAIL_POS_SUBMIT;
            default:
                return null;
        }
    }
}

