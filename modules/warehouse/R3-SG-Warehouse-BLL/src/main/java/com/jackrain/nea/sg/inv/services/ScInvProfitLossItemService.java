package com.jackrain.nea.sg.inv.services;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.util.TypeUtils;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.basic.config.SgStorageBoxConfig;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.inv.constants.SgInvConstants;
import com.jackrain.nea.sg.inv.mapper.ScProfitLossMapper;
import com.jackrain.nea.sg.inv.model.request.ScInvProfitLossBasePageRequest;
import com.jackrain.nea.sg.inv.model.request.ScInvProfitLossMarginQueryCmdRequest;
import com.jackrain.nea.sg.inv.model.result.ScInvProfitLossQueryItemResult;
import com.jackrain.nea.sg.inv.model.result.ScInvProfitLossStorageQueryItemResult;
import com.jackrain.nea.sg.inv.model.result.ScInvProfitLossTeusStorageQueryItemResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.DateUtil;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author 舒威
 * @since 2019/4/3
 * create at : 2019/4/3 11:45
 */
@Slf4j
@Component
public class ScInvProfitLossItemService {

    @Autowired
    private SgStorageBoxConfig storageBoxConfig;

    ValueHolderV14<ScInvProfitLossQueryItemResult> queryProfitLossItemInventory(ScInvProfitLossMarginQueryCmdRequest model) {
        if (log.isDebugEnabled()) {
            log.debug("Start ScInvProfitLossItemService.queryProfitLossItemInventory. Receive:request:{}", JSONObject.toJSONString(model));
        }
        Long storeId = model.getWarehouseId();
        Integer inventoryType = model.getInventoryType();
        Date inventoryDate = model.getInventoryDate();
        Boolean isCreateProf = model.getIsCreateProf();
        Boolean isExport = model.getIsExport();
        Boolean isQueryItems = model.getIsQueryItems();
        Boolean isQueryTeus = model.getIsQueryTeus();
        ScInvProfitLossBasePageRequest page = Optional.ofNullable(model.getPage()).orElse(new ScInvProfitLossBasePageRequest());
        Integer currentPage = page.getCurrentPage();
        Integer range = page.getPageSize();
        String orderByName = page.getName();
        Boolean orderByAsc = page.getIsasc();
        User loginUser = model.getLoginUser();

        isCreateProf = null == isCreateProf ? false : isCreateProf;
        isExport = null == isExport ? false : isExport;
        isQueryItems = null == isQueryItems ? false : isQueryItems;
        isQueryTeus = null == isQueryTeus ? false : isQueryTeus;
        currentPage = null == currentPage ? 1 : currentPage;
        range = null == range ? 10 : range;
        orderByAsc = null == orderByAsc ? true : orderByAsc;

        if (StringUtils.isEmpty(orderByName)) {
            orderByName = isCreateProf ? "PS_C_PRO_ECODE" : "PS_C_PRO_ECODE";
        } else {
            orderByName += orderByAsc ? "" : " DESC";
        }

        Integer startIndex = (currentPage - 1) * range;//记录条数
        Locale locale = loginUser.getLocale();
        ScProfitLossMapper profitLossMapper = ApplicationContextHandle.getBean(ScProfitLossMapper.class);

        checkParams(storeId, inventoryType, inventoryDate, locale);
        Boolean flag = inventoryDate.getTime() >= DateUtil.stringToDate(DateUtil.getDateTime("yyyy-MM-dd")).getTime(); //逐日库存标志
        checkStorageSyn(inventoryDate, isQueryItems, isQueryTeus, profitLossMapper, flag, locale);

        ScInvProfitLossQueryItemResult itemResult = new ScInvProfitLossQueryItemResult();
        if (isQueryItems) {
            List<JSONObject> profItems = new ArrayList<>();
            Long total = 0L;
            if (SgInvConstants.PAND_TYPE_QP.equals(inventoryType)) {
                if (isCreateProf) {
                    profItems = profitLossMapper.queryProfItemByQPC(storeId, inventoryDate, inventoryType, flag);
                } else {
                    profItems = profitLossMapper.queryProfItemByQP(storeId, inventoryDate, inventoryType,
                            startIndex, range, orderByName, flag, false, isExport);
                    total = profitLossMapper.getCountProfItemByQP(storeId, inventoryDate, inventoryType,
                            startIndex, range, orderByName, flag, true, isExport);
                }
            } else if (SgInvConstants.PAND_TYPE_CP.equals(inventoryType)) {
                if (isCreateProf) {
                    profItems = profitLossMapper.queryProfItemByCPC(storeId, inventoryDate, inventoryType,
                            startIndex, range, orderByName, flag, false);
                } else {
                    profItems = profitLossMapper.queryProfItemByCP(storeId, inventoryDate, inventoryType,
                            startIndex, range, orderByName, flag, false, isExport);
                    total = profitLossMapper.getCountProfItemByCP(storeId, inventoryDate, inventoryType,
                            startIndex, range, orderByName, flag, true, isExport);
                }
            }
            ScInvProfitLossStorageQueryItemResult itemStorageResult = new ScInvProfitLossStorageQueryItemResult();
            itemStorageResult.setItemList(profItems);
            itemStorageResult.setTotal(total);
            itemStorageResult.setTotalPages((total + range - 1) / range);
            itemResult.setItemStorageResult(itemStorageResult);
        }

        if (storageBoxConfig.getBoxEnable() && isQueryTeus) {
            List<JSONObject> profTeusItems = new ArrayList<>();
            Long total = 0L;
            if (SgInvConstants.PAND_TYPE_QP.equals(inventoryType)) {
                profTeusItems = profitLossMapper.queryProfTeusItemByQP(storeId, inventoryDate, inventoryType,
                        startIndex, range, orderByName, flag, false, isExport, isCreateProf);
                if (!isCreateProf) {
                    total = profitLossMapper.getCountProfTeusItemByQP(storeId, inventoryDate, inventoryType,
                            startIndex, range, orderByName, flag, true, isExport, isCreateProf);
                }
            } else if (SgInvConstants.PAND_TYPE_CP.equals(inventoryType)) {
                profTeusItems = profitLossMapper.queryProfTeusItemByCP(storeId, inventoryDate, inventoryType,
                        startIndex, range, orderByName, flag, false, isExport, isCreateProf);
                if (!isCreateProf) {
                    total = profitLossMapper.getCountProfTeusItemByCP(storeId, inventoryDate, inventoryType,
                            startIndex, range, orderByName, flag, true, isExport, isCreateProf);
                }
            }
            ScInvProfitLossTeusStorageQueryItemResult teusItemStorageResult = new ScInvProfitLossTeusStorageQueryItemResult();
            teusItemStorageResult.setTeusList(profTeusItems);
            teusItemStorageResult.setTotal(total);
            teusItemStorageResult.setTotalPages((total + range - 1) / range);
            itemResult.setItemTeusStorageResult(teusItemStorageResult);
        }

        ValueHolderV14<ScInvProfitLossQueryItemResult> v14 = new ValueHolderV14<>();
        v14.setCode(ResultCode.SUCCESS);
        v14.setMessage("success");
        v14.setData(itemResult);
        return v14;
    }

    /**
     * 检测入参
     */
    public void checkParams(Long storeId, Integer inventoryType, Date inventoryDate, Locale locale) {
        AssertUtils.notNull(storeId, "请选择盘点店仓！", locale);
        AssertUtils.notNull(inventoryType, "请选择盘点类型！", locale);
        AssertUtils.notNull(inventoryDate, "请选择盘点日期！", locale);
    }

    /**
     * 检测实时库存数据是否同步
     */
    private void checkStorageSyn(Date inventoryDate, Boolean isQueryItems, Boolean isQueryTeus, ScProfitLossMapper profitLossMapper, Boolean flag, Locale locale) {
        try {
            if (!flag) {
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                String inventoryDateStr = dateFormat.format(TypeUtils.castToDate(inventoryDate));
                Date date = dateFormat.parse(inventoryDateStr);
                if (isQueryItems) {
                    Long count = profitLossMapper.checkProfItemByDaily(date);
                    if (count == null || count == 0L) {
                        throw new NDSException(Resources.getMessage("当天账面库存未生成，请联系系统管理员！", locale));
                    }
                }
                if (storageBoxConfig.getBoxEnable() && isQueryTeus) {
                    Long teusCount = profitLossMapper.checkProfItemByTeusDaily(date);
                    if (teusCount == null || teusCount == 0L) {
                        throw new NDSException(Resources.getMessage("逻辑仓箱库存当天账面库存未生成，请联系系统管理员！", locale));
                    }
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
            throw new NDSException(Resources.getMessage("查询账面库存异常!" + e.getMessage(), locale));
        }
    }
}
