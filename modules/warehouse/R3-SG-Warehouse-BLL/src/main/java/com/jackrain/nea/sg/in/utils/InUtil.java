package com.jackrain.nea.sg.in.utils;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.data.basic.model.request.CpcStoreWrapper;
import com.jackrain.nea.data.basic.services.BasicCpQueryService;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.transfer.common.ScTransferConstants;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferMapper;
import com.jackrain.nea.sg.transfer.model.table.ScBTransfer;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.Locale;

/**
 * @author leexh
 * @since 2020/4/7 10:58
 * desc: 入库相关工具类
 */
@Slf4j
@Component
public class InUtil {

    /**
     * 检查收发货店仓是否是‘集团直营’
     *
     * @param locale         国际化
     * @param transferBillNo 调拨单单据编号
     * @return boolean
     */
    public boolean checkStoreIsDirectly(Locale locale, String transferBillNo) {

        // 根据来源单号查询调拨收发货店仓
        ScBTransferMapper mapper = ApplicationContextHandle.getBean(ScBTransferMapper.class);
        ScBTransfer transfer = mapper.selectOne(new QueryWrapper<ScBTransfer>().lambda()
                .eq(ScBTransfer::getBillNo, transferBillNo));
        AssertUtils.notNull(transfer, "调拨入库单根据来源单号查询调拨单为空！" + transferBillNo, locale);

        BasicCpQueryService psQueryService = ApplicationContextHandle.getBean(BasicCpQueryService.class);
        CpcStoreWrapper origStore = psQueryService.findCpcStore(transfer.getCpCOrigId());
        CpcStoreWrapper destStore = psQueryService.findCpcStore(transfer.getCpCDestId());
        if (origStore == null || destStore == null) {
            AssertUtils.logAndThrow("收发货店仓查询为空,请检查！", locale);
        }

        String origManageTypeCode = origStore.getManageTypeCode();
        String destManageTypeCode = destStore.getManageTypeCode();

        if (StringUtils.isEmpty(origManageTypeCode) || StringUtils.isEmpty(destManageTypeCode)) {
            AssertUtils.logAndThrow("收发货店仓经营类型查询为空,请检查！", locale);
        }

        return ScTransferConstants.MANAGE_TYPE_CODE_DIRECTLY.equals(origManageTypeCode)
                && ScTransferConstants.MANAGE_TYPE_CODE_DIRECTLY.equals(destManageTypeCode);
    }

}
