package com.jackrain.nea.sg.pack.validate;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.data.basic.model.request.StoreInfoQueryRequest;
import com.jackrain.nea.data.basic.services.BasicCpQueryService;
import com.jackrain.nea.data.basic.services.BasicPsQueryService;
import com.jackrain.nea.psext.model.table.PsCTeus;
import com.jackrain.nea.sg.basic.api.SgStorageQueryCmd;
import com.jackrain.nea.sg.basic.api.SgTeusPhyStorageQueryCmd;
import com.jackrain.nea.sg.basic.mapper.CpStoreMapper;
import com.jackrain.nea.sg.basic.model.request.SgStorageQueryRequest;
import com.jackrain.nea.sg.basic.model.request.SgTeusPhyStorageQueryRequest;
import com.jackrain.nea.sg.basic.model.table.CpCStore;
import com.jackrain.nea.sg.basic.model.table.SgBPhyTeusStorage;
import com.jackrain.nea.sg.basic.model.table.SgBStorage;
import com.jackrain.nea.sg.pack.common.SgTeusPackConstants;
import com.jackrain.nea.sg.pack.mapper.SgBTeusPackItemMapper;
import com.jackrain.nea.sg.pack.mapper.SgBTeusPackMapper;
import com.jackrain.nea.sg.pack.model.table.SgBTeusPack;
import com.jackrain.nea.sg.pack.model.table.SgBTeusPackItem;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.tableService.MainTableRecord;
import com.jackrain.nea.tableService.TableServiceContext;
import com.jackrain.nea.tableService.validate.BaseValidator;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.utils.AssertUtils;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author zhoulinsheng
 * @since 2019/11/27 14:02
 */
@Slf4j
@Component
public class SgTeusPackAuditValidate extends BaseValidator {

    @Override
    public void validate(TableServiceContext context, MainTableRecord currentRow) {

        Long objId = currentRow.getId();
        AssertUtils.notNull(objId, "装箱单ID为空！", context.getLocale());

        SgBTeusPackMapper packMapper = ApplicationContextHandle.getBean(SgBTeusPackMapper.class);
        SgBTeusPack sgBTeusPack = packMapper.selectById(objId);
        AssertUtils.notNull(sgBTeusPack, "当前记录已不存在！", context.getLocale());

        // 校验单据状态
        Integer status = sgBTeusPack.getStatus();
        Long packCpCStoreId = sgBTeusPack.getCpCStoreId();

        if (SgTeusPackConstants.BILL_STATUS_AUDIT == status) {
            AssertUtils.logAndThrow("当前记录已审核，不允许重复审核！", context.getLocale());
        } else if (SgTeusPackConstants.BILL_STATUS_VOID == status) {
            AssertUtils.logAndThrow("当前记录已作废，不允许审核！", context.getLocale());
        }

        // 删除明细中拆箱数量为0的记录
        SgBTeusPackItemMapper packItemMapper = ApplicationContextHandle.getBean(SgBTeusPackItemMapper.class);
        packItemMapper.delete(new UpdateWrapper<SgBTeusPackItem>().lambda().eq(SgBTeusPackItem::getQty, BigDecimal.ZERO));

        // 判断明细记录是否为0   如果是 则不允许审核
        List<SgBTeusPackItem> packItemList = packItemMapper.selectList(new QueryWrapper<SgBTeusPackItem>().lambda().eq(SgBTeusPackItem::getSgBTeusPackId, objId));
        AssertUtils.isTrue(packItemList.size() > 0, "当前记录无明细，不允许审核", context.getLocale());

        Map<String, SgBTeusPackItem> itemMapCode = packItemList.stream().collect(Collectors.toMap(SgBTeusPackItem::getPsCSkuEcode, o -> o));

        BasicPsQueryService queryService = ApplicationContextHandle.getBean(BasicPsQueryService.class);

        //校验箱的状态
        List<String> teusEcodeList = packItemList.stream()
                .map(SgBTeusPackItem::getPsCTeusEcode)
                .distinct()
                .collect(Collectors.toList());

        HashMap<String, PsCTeus> teusHashMap = queryService.queryTeusInfoByEcode(teusEcodeList);

        teusEcodeList.forEach(x ->{
            PsCTeus psCTeus = teusHashMap.get(x);
            AssertUtils.notNull(psCTeus, "箱[" + x + "]不存在", context.getLocale());
            AssertUtils.isTrue(SgTeusPackConstants.BOX_STATUS_SPLIT == psCTeus.getTeusStatus(), Resources.getMessage("当前箱：" + x + "不为已拆箱，不允许装箱！", context.getLocale()));
        });

        /**
         * 【箱号】+【店仓】在【实体仓箱库存】不存在记录，则提示“当前店仓不存在箱：XXX，不允许再次装箱！”；
         * 【箱号】+【店仓】在【实体仓箱库存】中“在库箱数”>0，则提示“当前箱：XXX未拆箱，不允许再次装箱！”；
         */

        SgTeusPhyStorageQueryCmd phyStorageQueryCmd = (SgTeusPhyStorageQueryCmd)
                ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                        SgTeusPhyStorageQueryCmd.class.getName(), "sg", "1.0");

        SgTeusPhyStorageQueryRequest phyStorageQueryRequest = new SgTeusPhyStorageQueryRequest();
        List<Long> phyWarehouseIds = new ArrayList<>();
        phyWarehouseIds.add(sgBTeusPack.getCpCPhyWarehouseId());
        phyStorageQueryRequest.setPhyWarehouseIds(phyWarehouseIds);
        phyStorageQueryRequest.setTeusEcodes(teusEcodeList);

        ValueHolderV14<List<SgBPhyTeusStorage>> v14 = phyStorageQueryCmd.queryTeusPhyStorage(phyStorageQueryRequest, context.getUser());

        if (v14.isOK()) {
            List<SgBPhyTeusStorage> phyTeusStorageList = v14.getData();
            Map<String, SgBPhyTeusStorage> storageMap = phyTeusStorageList.stream().filter(o -> StringUtils.isNotEmpty(o.getPsCTeusEcode()))
                    .collect(Collectors.toMap(SgBPhyTeusStorage::getPsCTeusEcode, o -> o));
            teusEcodeList.forEach(x -> {
                SgBPhyTeusStorage phyTeusStorage = storageMap.get(x);
                AssertUtils.notNull(phyTeusStorage, "当前箱：" + x + "不存在于店仓" + sgBTeusPack.getCpCPhyWarehouseEcode() + "中!", context.getLocale());
                BigDecimal qtyStorage = phyTeusStorage.getQtyStorage();
                AssertUtils.isTrue((qtyStorage.compareTo(BigDecimal.ZERO) <= 0), "当前箱：" + x + "不存在于店仓" + sgBTeusPack.getCpCPhyWarehouseEcode() + "中!", context.getLocale());
            });
        }


        /**
         * 根据条码+实体仓对应的逻辑仓在【逻辑仓库存】中可用数>0”
         */
        List<Long> storeIds = new ArrayList<>();
        if (packCpCStoreId == null) {
            CpStoreMapper cpStoreMapper = ApplicationContextHandle.getBean(CpStoreMapper.class);

            List<CpCStore> cpCStoreList = cpStoreMapper.selectList(new QueryWrapper<CpCStore>().lambda().eq(CpCStore::getCpCPhyWarehouseId, sgBTeusPack.getCpCPhyWarehouseId()));
            AssertUtils.isTrue(CollectionUtils.isNotEmpty(cpCStoreList), "根据此实体仓未查到逻辑仓!", context.getLocale());
            storeIds = cpCStoreList.stream().map(CpCStore::getId).collect(Collectors.toList());
        } else {
            storeIds.add(packCpCStoreId);
        }


        List<String> skuEcodeList = packItemList.stream().map(SgBTeusPackItem::getPsCSkuEcode).collect(Collectors.toList());
        SgStorageQueryCmd storageQueryCmd = (SgStorageQueryCmd)
                ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                        SgStorageQueryCmd.class.getName(), "sg", "1.0");
        SgStorageQueryRequest request = new SgStorageQueryRequest();
        request.setStoreIds(storeIds);
        request.setSkuEcodes(skuEcodeList);

        ValueHolderV14<List<SgBStorage>> holderV14 = storageQueryCmd.queryStorage(request, context.getUser());
        if (holderV14.isOK()) {

            List<SgBStorage> storageList = holderV14.getData();
            Map<Long, Map<String, SgBStorage>> map1 = new HashMap<>();

            Map<Long, List<SgBStorage>> map2 = new HashMap<>();
            Set<Long> sets = new HashSet<>();
            for (SgBStorage storage : storageList) {
                if (sets.add(storage.getCpCStoreId())){
                    List<SgBStorage> list = new ArrayList<>();
                    list.add(storage);
                    map2.put(storage.getCpCStoreId(), list);
                    continue;
                }
                List<SgBStorage> sgBStorages = map2.get(storage.getCpCStoreId());
                sgBStorages.add(storage);
            }

            for (Map.Entry<Long , List<SgBStorage>> longListEntry : map2.entrySet()) {
                Map<String, SgBStorage> map3 = new HashMap<>();
                Long key = longListEntry.getKey();
                List<SgBStorage> value = longListEntry.getValue();
                for (SgBStorage sgBStorage : value) {
                    if (map3.keySet().contains(sgBStorage.getPsCSkuEcode())){
                        continue;
                    }
                    map3.put(sgBStorage.getPsCSkuEcode(), sgBStorage);
                }
                map1.put(key, map3);
            }


            /**
             * 判断是否有符合所有箱的逻辑仓
             */
            Long resultStoreId = null;
            Integer count = 0;
            for (Long storeId : storeIds) {
                for (String skuEcode : skuEcodeList) {
                    Map<String, SgBStorage> stringListMap = map1.get(storeId);
                    SgBStorage storage = stringListMap.get(skuEcode);
                    BigDecimal qtyAvailable = storage.getQtyAvailable();
                    SgBTeusPackItem sgBTeusPackItem = itemMapCode.get(skuEcode);
                    if (qtyAvailable.compareTo(sgBTeusPackItem.getQty()) < 0){
                        continue;
                    }
                    count++;
                }
                if (count == skuEcodeList.size()) {
                    resultStoreId = storeId;
                }
            }

            if (resultStoreId != null) {
                BasicCpQueryService cpQueryService = ApplicationContextHandle.getBean(BasicCpQueryService.class);
                StoreInfoQueryRequest storeInfoQueryRequest = new StoreInfoQueryRequest();
                List<Long> ids = new ArrayList<>();
                ids.add(resultStoreId);
                storeInfoQueryRequest.setIds(ids);
                HashMap<Long, com.jackrain.nea.cp.result.CpCStore> storeInfo = new HashMap<>();
                try {
                    storeInfo = cpQueryService.getStoreInfo(storeInfoQueryRequest);
                } catch (Exception e) {
                    AssertUtils.logAndThrow("查询逻辑店仓信息异常！" + e.getMessage(), context.getLocale());
                }

                // 补充实体仓逻辑仓信息 并更新
                com.jackrain.nea.cp.result.CpCStore cpCStore = storeInfo.get(resultStoreId);

                User user = context.getUser();
                List<SgBTeusPackItem> itemList = packItemMapper.selectList(new QueryWrapper<SgBTeusPackItem>().lambda()
                        .eq(SgBTeusPackItem::getSgBTeusPackId, objId));

                // 计算总装箱数
                Map<Long, List<SgBTeusPackItem>> collect = itemList.stream().collect(Collectors.groupingBy(SgBTeusPackItem::getPsCTeusId));
                BigDecimal countPack = new BigDecimal(collect.keySet().size());

                // 计算总箱内数
                BigDecimal countTeus = itemList.stream()
                        .map(SgBTeusPackItem::getQty).reduce(BigDecimal.ZERO, BigDecimal::add);

                JSONObject commitData = currentRow.getCommitData();
                commitData.put("QTY_TOT_PACK", countPack);
                commitData.put("QTY_TOT_TEUS", countTeus);
                commitData.put("STATUS_ID", Long.valueOf(user.getId()));
                commitData.put("STATUS_NAME", user.getName());
                commitData.put("STATUS_TIME", new Date());
                commitData.put("CP_C_STORE_ID", resultStoreId);
                commitData.put("CP_C_STORE_ECODE", cpCStore.getEcode());
                commitData.put("CP_C_STORE_ENAME", cpCStore.getEname());
            } else {
                AssertUtils.logAndThrow( "未找到符合拆箱数量的逻辑仓！", context.getLocale());
            }
        }
    }
}