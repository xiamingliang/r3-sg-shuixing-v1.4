package com.jackrain.nea.sg.out.services;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.util.StringUtil;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.cpext.model.table.CpShop;
import com.jackrain.nea.data.basic.model.request.ShopQueryRequest;
import com.jackrain.nea.data.basic.services.BasicCpQueryService;
import com.jackrain.nea.ip.api.jingdong.JdOrderServiceCmd;
import com.jackrain.nea.ip.model.jingdong.ReceiveRequest;
import com.jackrain.nea.sg.basic.mapper.CpCPhyWarehouseMapper;
import com.jackrain.nea.sg.basic.model.table.CpCPhyWarehouse;
import com.jackrain.nea.sg.out.common.SgOutConstants;
import com.jackrain.nea.sg.out.common.SgOutConstantsIF;
import com.jackrain.nea.sg.out.mapper.SgBPhyOutNoticesMapper;
import com.jackrain.nea.sg.out.model.request.SgOutNoticesBatchSendMsgRequest;
import com.jackrain.nea.sg.out.model.result.SgOutNoticesResult;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutNotices;
import com.jackrain.nea.sg.out.utils.SgWarehouseESUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.util.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @description: 出库通知单保存电子面单服务
 * @author: 郑小龙
 * @date: 2019-07-10 19:02
 */
@Slf4j
@Component
public class SgPhyOutNoticesSaveFaceService {

    @Autowired
    private BasicCpQueryService basicCpQueryService;

    @Autowired
    private SgWarehouseESUtils warehouseESUtils;

    public ValueHolderV14 electronicFaceSaveList(SgOutNoticesBatchSendMsgRequest result) {
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + " execFaceSaveList.result=" + JSONObject.toJSONString(result));
        }
        ValueHolderV14 holderV14 = new ValueHolderV14<List<SgOutNoticesResult>>(ResultCode.SUCCESS, "");
        User user = result.getLoginUser();
        SgBPhyOutNoticesMapper noticesMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesMapper.class);

        Iterator<SgBPhyOutNotices> iterator = result.getOutNoticesMap().keySet().iterator();
        while (iterator.hasNext()) {
            SgBPhyOutNotices outNotices = iterator.next();
            //获取电子面单为“是”的才调用接口获取面单信息
            if (null != outNotices.getIsEnableewaybill() &&
                    SgOutConstantsIF.IS_ENABLEEWAYBILL_Y == outNotices.getIsEnableewaybill()) {
                Long objId = outNotices.getId();
                //挂靠店铺id
                Long shopid = result.getShopIdMap().get(objId);
                //电子面单类型
                Integer etyep = result.getEtypeMap().get(objId);
                //获取电子面单
                ValueHolderV14 valueHolder = getFaceInfo(outNotices, shopid, etyep, user);
                //判断获取电子面单是否成功，拼接大头笔
               checkingFaceIsSuc(outNotices, valueHolder);
                if (log.isDebugEnabled()) {
                    log.debug("electronicFaceSaveList.outNotices=" + JSONObject.toJSONString(outNotices));
                }

                SgBPhyOutNotices newOutNotices = new SgBPhyOutNotices();
                newOutNotices.setId(outNotices.getId());
                newOutNotices.setEwaybillStatus(outNotices.getEwaybillStatus());//电子面单状态
                newOutNotices.setLogisticNumber(outNotices.getLogisticNumber());//快递单号
                newOutNotices.setEwaybillFailReason(outNotices.getEwaybillFailReason());//电子面单信息
                newOutNotices.setReserveBigint03(outNotices.getReserveBigint03());//电子面单次数
                newOutNotices.setReserveVarchar01(outNotices.getReserveVarchar01());//电子面单最后保存时间
                newOutNotices.setShortAddress(outNotices.getShortAddress());//电子面单大头笔

                noticesMapper.updateById(newOutNotices);
                //推送es
                warehouseESUtils.pushESByOutNotices(objId, false, false, null, noticesMapper, null);
            }
        }
        return holderV14;
    }

    /**
     * 获取面单信息
     */
    public ValueHolderV14 getFaceInfo(SgBPhyOutNotices outNotices,Long shopid,Integer etype, User user) {
        ValueHolderV14 valueHolder = new ValueHolderV14(ResultCode.SUCCESS, "");
        try {
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + " outNotices.id=" + outNotices.getId()
                        + "electronicFace.param=" + JSONObject.toJSONString(outNotices));
            }
            ValueHolderV14<CpShop> v14 = getCpShop(shopid);
            if (!v14.isOK() || null == v14.getData()) {
                valueHolder.setCode(ResultCode.FAIL);
                valueHolder.setMessage(v14.getMessage());
                return valueHolder;
            }
            CpShop cpShop = v14.getData();
            String customerCode = "";
            if (SgOutConstantsIF.EWAYBILL_TYPE_STRAIGHT.equals(etype)) {
                //获取账号信息
                customerCode = cpShop.getQinglongCode();
            }
            //获取卖家昵称
            String nickName = cpShop.getSellerNick();
            String salePlat = getSalePlat(outNotices.getReserveBigint01().toString());
            return createGetEwaybill(outNotices, customerCode, salePlat, nickName);
        }catch (Exception e) {
            valueHolder.setCode(ResultCode.FAIL);
            valueHolder.setMessage(e.getMessage());
        }
        return valueHolder;
    }

    /**
     * 封装数据调rpc接口 获取电子面单信息
     */
    private ValueHolderV14 createGetEwaybill(SgBPhyOutNotices outNotices, String customerCode, String salePlat, String nickName) {
        ReceiveRequest request = new ReceiveRequest();
        CpCPhyWarehouseMapper warehouseMapper = ApplicationContextHandle.getBean(CpCPhyWarehouseMapper.class);
        CpCPhyWarehouse warehouse = warehouseMapper.selectById(outNotices.getCpCPhyWarehouseId());
        ValueHolderV14 valueHolder = new ValueHolderV14(ResultCode.SUCCESS,"");
        valueHolder =checkData(outNotices,warehouse);
        if (!valueHolder.isOK()){
            if (log.isDebugEnabled()) {
                log.debug("createGetEwaybill.valueHolder:" + JSONObject.toJSONString(valueHolder));
            }
            return valueHolder;
        }

        request.setSalePlat(salePlat);//必填 //销售平台
        request.setCustomerCode(customerCode);//必填//物流公司础资料的【账号】
        request.setOrderId(outNotices.getBillNo());//必填//出库通知单号
        request.setThrOrderId(outNotices.getSourcecode());//平台单号
        //寄件人 信息
        request.setSenderName(warehouse.getContactName());//必填 //实体仓资料中的联系人
        request.setSenderAddress(warehouse.getSendAddress());//必填 //实体仓资料中的详细地址
        request.setSenderTel(warehouse.getPhoneNum());//实体仓资料中的电话
        request.setSenderMobile(warehouse.getMobilephoneNum());//实体仓资料中的手机号
        request.setSenderPostcode(warehouse.getSellerZip());
        //收件人 信息
        String provinceName = outNotices.getCpCRegionProvinceEname();
        String cityName = outNotices.getCpCRegionCityEname();
        String areaName = outNotices.getCpCRegionAreaEname();
        String receiverAddress = outNotices.getReceiverAddress();

        request.setReceiveName(outNotices.getReceiverName());//必填 //收件人姓名
        request.setReceiveAddress(provinceName + cityName + areaName + receiverAddress);//必填 //收件详细地址
        request.setProvince(provinceName);//收货省份名称
        request.setCity(cityName);//收货城市名称
        request.setCounty(areaName);//收货区名称
        request.setProvinceId(outNotices.getCpCRegionProvinceId() != null ? outNotices.getCpCRegionProvinceId().intValue() : null);//收件人省编码
        request.setCityId(outNotices.getCpCRegionCityId() != null ? outNotices.getCpCRegionCityId().intValue() : null);//收货城市编码
        request.setCountyId(outNotices.getCpCRegionAreaId() != null ? outNotices.getCpCRegionAreaId().intValue() : null);//收货区编码
        request.setReceiveMobile(outNotices.getReceiverMobile());//收件人手机号
        request.setReceiveTel(outNotices.getReceiverPhone());//收件人电话号
        request.setPostcode(outNotices.getReceiverZip());//收件人邮编
        //包裹 信息
        request.setPackageCount(1);//必填 //包裹数(大于0，小于1000) 默认“1”
        request.setWeight(1.0);//必填 //重量(单位：kg，保留小数点后两位) 默认“1”
        request.setVloumn(1.0);//必填 //体积(单位：cm3，保留小数点后两位) 默认“1”

        //货到付款、付款金额
        if (outNotices.getIsCod() != null && outNotices.getIsCod() == SgOutConstants.PAY_TYPE_ARRIVE) {
            request.setCollectionValue(1);
            BigDecimal amtPayment = Optional.ofNullable(outNotices.getAmtPayment()).orElse(BigDecimal.ZERO);
            request.setCollectionMoney(amtPayment.doubleValue());
        }

        JdOrderServiceCmd createCmd = (JdOrderServiceCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(), JdOrderServiceCmd.class.getName(), "ip", "1.4.0");
        try {
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + " outNotices.id"+outNotices.getId()+",receive.param=" + JSONObject.toJSONString(request));
            }
            valueHolder = createCmd.receive(request, nickName);
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + "outNotices.id"+outNotices.getId()+",receive.valueHolder=" + JSONObject.toJSONString(valueHolder));
            }
        } catch (Exception e) {
            log.error(this.getClass().getName() + "outNotices.id"+outNotices.getId()+",receive.error=" + e.getMessage(), e);
            valueHolder = new ValueHolderV14();
            valueHolder.setCode(ResultCode.FAIL);
            valueHolder.setMessage("京东电子面单调用RPC出错！");
        }
        return valueHolder;
    }

    /**
     * 判断获取电子面单是否成功
     */
    private void checkingFaceIsSuc(SgBPhyOutNotices outNotices,ValueHolderV14 v14) {
        if (log.isDebugEnabled()) {
            log.debug("updateOutNotices.v14=" + JSONObject.toJSONString(v14));
        }
        int frequency = outNotices.getReserveBigint03() == null ? 0 : outNotices.getReserveBigint03().intValue();
        ValueHolderV14<SgBPhyOutNotices> holderV14 = new ValueHolderV14(ResultCode.SUCCESS,"");
        outNotices.setEwaybillStatus(SgOutConstantsIF.EWAYBILL_STATUS_FAIL);
        outNotices.setLogisticNumber("");
        outNotices.setReserveBigint03(Long.valueOf(frequency));
        outNotices.setShortAddress("");
        SimpleDateFormat sdfMilli = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        if (v14.isOK() && v14.getData() != null) {
            JSONObject object = JSON.parseObject(v14.getData().toString());
            JSONObject preSortResult = object.getJSONObject("preSortResult");
            //物流单号
            String logisticNumber = object.getString("deliveryId");
            outNotices.setLogisticNumber(logisticNumber);
            if (preSortResult != null && preSortResult.size() > 0) {
                outNotices.setEwaybillFailReason("");
                outNotices.setShortAddress(getShortAddress(preSortResult));
                outNotices.setEwaybillStatus(SgOutConstantsIF.EWAYBILL_STATUS_SUCCESS);
                outNotices.setReserveVarchar01(sdfMilli.format(new Date()));
                return;
            } else {
                String message = object.getString("resultMessage");
                //这里记录京东接口返回的错误
                outNotices.setEwaybillFailReason(message);
            }
        } else {
            //这里记录系统报错
            outNotices.setEwaybillFailReason(v14.getMessage());
        }

        outNotices.setEwaybillStatus(SgOutConstantsIF.EWAYBILL_STATUS_SECTION_SUC);
        frequency++;
        if (frequency >= SgOutConstantsIF.FACE_EXEC_BYNBER) {
            outNotices.setEwaybillStatus(SgOutConstantsIF.EWAYBILL_STATUS_FAIL);
        }
        outNotices.setReserveBigint03(Long.valueOf(frequency));
        outNotices.setReserveVarchar01(sdfMilli.format(new Date()));
    }

    /**
     * 字段验证
     */
    private ValueHolderV14 checkData(SgBPhyOutNotices outNotices,CpCPhyWarehouse warehouse) {
        ValueHolderV14 valueHolder = new ValueHolderV14(ResultCode.SUCCESS, "");
        if (null == outNotices.getBillNo()) {
            valueHolder.setCode(ResultCode.FAIL);
            valueHolder.setMessage("出库通知单 单号 为空!");
            return valueHolder;
        }
        if (null == warehouse) {
            valueHolder.setCode(ResultCode.FAIL);
            valueHolder.setMessage("查询实体仓信息 为空!");
            return valueHolder;
        }
        if (StringUtil.isEmpty(warehouse.getContactName())) {
            valueHolder.setCode(ResultCode.FAIL);
            valueHolder.setMessage("实体仓资料 联系人 为空!");
            return valueHolder;
        }
        if (StringUtil.isEmpty(warehouse.getSendAddress())) {
            valueHolder.setCode(ResultCode.FAIL);
            valueHolder.setMessage("实体仓资料 详细地址 为空!");
            return valueHolder;
        }
        if (StringUtil.isEmpty(outNotices.getCpCRegionProvinceEname())) {
            valueHolder.setCode(ResultCode.FAIL);
            valueHolder.setMessage("出库通知单 省份 为空!");
            return valueHolder;
        }
        if (StringUtil.isEmpty(outNotices.getCpCRegionCityEname())) {
            valueHolder.setCode(ResultCode.FAIL);
            valueHolder.setMessage("出库通知单 城市 为空!");
            return valueHolder;
        }
        if (StringUtil.isEmpty(outNotices.getReceiverAddress())) {
            valueHolder.setCode(ResultCode.FAIL);
            valueHolder.setMessage("出库通知单 详细地址 为空!");
            return valueHolder;
        }
        return valueHolder;
    }
    /**
     * 获取店铺信息
     */
    private ValueHolderV14<CpShop> getCpShop(Long shopid){
        ValueHolderV14<CpShop> valueHolder = new ValueHolderV14(ResultCode.SUCCESS, "");
        ShopQueryRequest shopQueryRequest = new ShopQueryRequest();
        shopQueryRequest.setShopIds(Lists.newArrayList(shopid));
        CpShop cpShop;
        try {
            log.info(this.getClass().getName() + " 开始调用店铺查询接口,店铺id:{}", shopid);
            HashMap<Long, CpShop> shopInfo = basicCpQueryService.getShopInfo(shopQueryRequest);
            cpShop = shopInfo.get(shopid);
        } catch (Exception e) {
            log.error(this.getClass().getName() + " getShopInfo.shopid:{}", shopid);
            valueHolder.setCode(ResultCode.FAIL);
            valueHolder.setMessage("查询店铺报错,店铺id" + shopid);
            return valueHolder;
        }
        if (null == cpShop) {
            valueHolder.setCode(ResultCode.FAIL);
            valueHolder.setMessage("查询店铺信息数据为空,店铺id" + shopid);
            return valueHolder;
        }
        if (StringUtil.isEmpty(cpShop.getSellerNick())) {
            valueHolder.setCode(ResultCode.FAIL);
            valueHolder.setMessage("卖家昵称为空,店铺id" + shopid);
            return valueHolder;
        }
        if (StringUtil.isEmpty(cpShop.getQinglongCode())) {
            valueHolder.setCode(ResultCode.FAIL);
            valueHolder.setMessage("青龙账号为空,店铺id" + shopid);
            return valueHolder;
        }
        valueHolder.setData(cpShop);
        return valueHolder;
    }
    /**
     * 获取销售平台信息
     */
    private String getSalePlat(String platForm) {
        switch (platForm) {
            case SgOutConstantsIF.PLATFORMTYPE_TAOBAO://淘宝平台
            case SgOutConstantsIF.PLATFORMTYPE_TAOBAO_DISTRIBUTION://淘宝分销
                return SgOutConstantsIF.SALEPLAT_TAOBAO;//天猫商城 0010002
            case SgOutConstantsIF.PLATFORMTYPE_JINGDONG://京东平台
                return SgOutConstantsIF.SALEPLAT_JINGDONG;//京东商城 0010001
            case SgOutConstantsIF.PLATFORMTYPE_SUNING://苏宁
                return SgOutConstantsIF.SALEPLAT_SUNING;//苏宁易购0010003
            case SgOutConstantsIF.PLATFORMTYPE_AMAZON://亚马逊
                return SgOutConstantsIF.SALEPLAT_AMAZON;//亚马逊中国 0010004
            default:
                return SgOutConstantsIF.SALEPLAT_OTHER;//其他平台0030001
        }
    }


    /**
     * 拼接大头笔信息
     * @param preSortResult
     * @return
     */
    public String getShortAddress(JSONObject preSortResult){

        //始发地
        String sourceSortCenterName = preSortResult.getString("sourceSortCenterName");
        //始发道口号
        String sourceCrossCode = preSortResult.getString("sourceCrossCode");
        //始发笼车号
        String sourceTabletrolleyCode = preSortResult.getString("sourceTabletrolleyCode");
        //目的地
        String targetSortCenterName = preSortResult.getString("targetSortCenterName");
        //目的道口号
        String slideNo = preSortResult.getString("slideNo");
        //目的笼车号
        String targetTabletrolleyCode = preSortResult.getString("targetTabletrolleyCode");
        //目地站点
        String siteName = preSortResult.getString("siteName");
        //路区
        String road = preSortResult.getString("road");

        String shortAddress = sourceSortCenterName +
                "@" + sourceCrossCode +
                "@" + sourceTabletrolleyCode +
                "@" + targetSortCenterName +
                "@" + slideNo +
                "@" + targetTabletrolleyCode +
                "@" + siteName +
                "@" + road;

        return shortAddress;
    }

}
