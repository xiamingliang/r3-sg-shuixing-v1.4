package com.jackrain.nea.sg.out.services;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.core.db.Tools;
import com.jackrain.nea.cpext.utils.ValueHolderUtils;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.ext.sequence.util.SequenceGenUtil;
import com.jackrain.nea.oc.oms.api.AgainWmsCmd;
import com.jackrain.nea.sg.basic.common.R3ParamConstants;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.config.SgStorageBoxConfig;
import com.jackrain.nea.sg.basic.mapper.CpCPhyWarehouseMapper;
import com.jackrain.nea.sg.basic.model.result.SgR3BaseResult;
import com.jackrain.nea.sg.basic.model.table.CpCPhyWarehouse;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.out.common.SgOutConstants;
import com.jackrain.nea.sg.out.common.SgOutConstantsIF;
import com.jackrain.nea.sg.out.mapper.SgBPhyOutNoticesImpItemMapper;
import com.jackrain.nea.sg.out.mapper.SgBPhyOutNoticesItemMapper;
import com.jackrain.nea.sg.out.mapper.SgBPhyOutNoticesMapper;
import com.jackrain.nea.sg.out.mapper.SgBPhyOutNoticesTeusItemMapper;
import com.jackrain.nea.sg.out.model.request.*;
import com.jackrain.nea.sg.out.model.result.SgBaseModelR3;
import com.jackrain.nea.sg.out.model.result.SgOutNoticesByBillNoResult;
import com.jackrain.nea.sg.out.model.table.*;
import com.jackrain.nea.sg.out.utils.SgWarehouseESUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.*;

/**
 * @author leexh
 * @since 2019/4/19 14:42
 */
@Slf4j
@Component
public class SgPhyOutNoticesSaveService {

    @Autowired
    private SgWarehouseESUtils warehouseESUtils;

    @Autowired
    private SgStorageBoxConfig storageBoxConfig;

    /**
     * 接口调用：
     * 不希望抛异常
     * 自己捕捉return出去
     *
     * @param request 入参
     * @return
     */
    public ValueHolderV14<SgR3BaseResult> saveSgPhyOutNoticesThird(SgPhyOutNoticesBillSaveRequest request) {
        ValueHolderV14<SgR3BaseResult> result = new ValueHolderV14<>();
        try {
            result = saveSgPhyOutNotices(request);
        } catch (Exception e) {
            log.error(this.getClass().getName() + ".error:" + e, e);
            result.setCode(ResultCode.FAIL);
            result.setMessage(e.getMessage());
        }
        return result;
    }


    /**
     * 出库通知单编辑/保存
     * 通过主单据ID判断(页面传过来为objid,实现层做转换)：
     * objId < 0       新增，
     * objId > 0       更新，页面
     * objId = null    更新，接口调用
     *
     * @param request
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public ValueHolderV14<SgR3BaseResult> saveSgPhyOutNotices(SgPhyOutNoticesBillSaveRequest request) {

        ValueHolderV14<SgR3BaseResult> result;
        AssertUtils.notNull(request, "参数不能为空！");
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ".debug, saveSgPhyOutNotices-param:" + JSONObject.toJSONString(request));
        }

        // 参数校验
        checkParams(request);

        SgBPhyOutNoticesMapper noticesMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesMapper.class);
        SgBPhyOutNoticesItemMapper noticesItemMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesItemMapper.class);
        if (request.getObjId() < 0) {
            /*新增出库通知单 */
            result = insertOutNotices(request, noticesMapper, noticesItemMapper);
        } else {

            /*更新出库通知单 */
            result = upDateOutNotices(request, noticesMapper, noticesItemMapper);
        }

        if (result != null && result.getData() != null) {
            //推送ES
            JSONObject dataJo = result.getData().getDataJo();
            if (dataJo != null) {
                Long objid = dataJo.getLong(R3ParamConstants.OBJID);
                warehouseESUtils.pushESByOutNotices(objid, true, false, null, noticesMapper, noticesItemMapper);
            }
        }
        return result;
    }

    /**
     * 出库通知单主子表新增
     *
     * @param request           入参
     * @param noticesMapper     主表mapper
     * @param noticesItemMapper 明细mapper
     * @return
     */
    public ValueHolderV14<SgR3BaseResult> insertOutNotices(SgPhyOutNoticesBillSaveRequest request,
                                                           SgBPhyOutNoticesMapper noticesMapper,
                                                           SgBPhyOutNoticesItemMapper noticesItemMapper) {

        ValueHolderV14<SgR3BaseResult> v14 = new ValueHolderV14();
        User loginUser = request.getLoginUser();
        String eName = loginUser.getEname();

        // 新增出库通知单
        BigDecimal zero = BigDecimal.ZERO;
        SgBPhyOutNotices outNotices = new SgBPhyOutNotices();
        BeanUtils.copyProperties(request.getOutNoticesRequest(), outNotices);

        // 获取单据ID
        String table = SgConstants.SG_B_PHY_OUT_NOTICES;
        Long objId = Tools.getSequence(table);
        outNotices.setId(objId);

        // 出库通知单明细新增
        HashMap skuMap = insertOutNoticesItem(objId, request.getOutNoticesItemRequests(), request.getLoginUser());

        if (storageBoxConfig.getBoxEnable()) {
            SgPhyOutNoticesConvertService service = ApplicationContextHandle.getBean(SgPhyOutNoticesConvertService.class);
            if (CollectionUtils.isNotEmpty(request.getTeusItemList())) {
                //出库通知单箱明细新增
                service.insertOutNoticesTeusItem(objId, request.getTeusItemList(), request.getLoginUser());
            }
            //出库通知单录入明细新增
            service.insertOutNoticesImpItem(objId, request.getImpItemList(), request.getLoginUser());
        }

        // 获取单据编号
        JSONObject obj = new JSONObject();
        obj.put(SgConstants.SG_B_PHY_OUT_NOTICES.toUpperCase(), outNotices);
        String billNo = SequenceGenUtil.generateSquence(SgOutConstants.SQE_SG_B_OUT_NOTICES, obj, loginUser.getLocale(), false);
        outNotices.setBillNo(billNo);
        if (outNotices.getBillDate() == null) {
            outNotices.setBillDate(new Date());
        }

        // 总通知数量、总吊牌金额、 总成交金额
        setOutResultTot(objId, noticesItemMapper, outNotices, null);

        outNotices.setTotQtyOut(zero);  // 总出库数量
        outNotices.setTotAmtCostOut(zero);  // 总出库成交金额
        outNotices.setTotAmtListOut(zero);  // 总出库吊牌金额
        outNotices.setIsactive(SgConstants.IS_ACTIVE_Y);    // 未作废
        outNotices.setBillStatus(SgOutConstantsIF.OUT_NOTICES_STATUS_WAIT);   // 待出库
        outNotices.setPickStatus(SgOutConstantsIF.PICK_STATUS_WAIT);  //未拣货

        //集成京東電子面單-更新是否获取电子面单
        // if (request.getOutNoticesRequest().getSourceBillType() == SgConstantsIF.BILL_TYPE_RETAIL) {
        //   updateEwaybillMq(outNotices, loginUser);
        //  }

        StorageESUtils.setBModelDefalutData(outNotices, loginUser);
        outNotices.setOwnerename(eName);
        outNotices.setModifierename(eName);

        //2019-09-06 截取备注等超长字段
        outNotices.setRemark(StorageESUtils.strSubString(outNotices.getRemark(), SgConstants.SG_COMMON_REMARK_SIZE));
        outNotices.setBuyerRemark(StorageESUtils.strSubString(outNotices.getBuyerRemark(), SgConstants.SG_COMMON_REMARK_SIZE));
        outNotices.setSellerRemark(StorageESUtils.strSubString(outNotices.getSellerRemark(), SgConstants.SG_COMMON_REMARK_SIZE));
        outNotices.setWmsFailReason(StorageESUtils.strSubString(outNotices.getWmsFailReason(), SgConstants.SG_COMMON_STRING_SIZE));

        int insert = noticesMapper.insert(outNotices);
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ".debug, 出库通知单新增结果：" + insert);
        }

        SgR3BaseResult date = new SgR3BaseResult();
        JSONObject dataJo = new JSONObject();
        dataJo.put(R3ParamConstants.OBJID, objId);
        dataJo.put(R3ParamConstants.TABLENAME, SgConstants.SG_B_PHY_OUT_NOTICES.toUpperCase());
        dataJo.put("bill_no", billNo);
        dataJo.put(SgOutConstantsIF.RETURN_OUT_NOTICES_ITEM_KEY, skuMap);
        date.setDataJo(dataJo);
        v14.setCode(ResultCode.SUCCESS);
        v14.setMessage("保存成功！");
        v14.setData(date);
        return v14;
    }


    /**
     * @return
     * @Description 更新出库通知单 是否获取电子面单 并 异步调用MQ  获取电子面单服务
     * @author 郑小龙
     * @date 2019-07-18 16:30
     */
 /*   public void updateEwaybillMq(SgBPhyOutNotices outNotices, User user) {
        try {
            StrategyCenterCmd serviceCmd = (StrategyCenterCmd) ReferenceUtil.refer(
                    ApplicationContextHandle.getApplicationContext(), StrategyCenterCmd.class.getName(), "st", "1.0");
            ValueHolderV14<EwayBillResult> queryEwayBillResult = serviceCmd.queryEwayBillByshopId(outNotices.getCpCShopId(), outNotices.getCpCLogisticsId());
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + "出库单号[" + outNotices.getBillNo() + "] 查询电子面单策略出参" + queryEwayBillResult);
            }
            outNotices.setIsEnableewaybill(SgOutConstantsIF.IS_ENABLEEWAYBILL_N);
            if (queryEwayBillResult.isOK()) {
                EwayBillResult data = queryEwayBillResult.getData();
                if (data != null) {
                    StCEwaybillLogisticsDO ewaybillLogisticsDO = data.getStCEwaybillLogisticsDO();
                    if (ewaybillLogisticsDO != null) {
                        if (log.isDebugEnabled()) {
                            log.debug(this.getClass().getName() + " 电子面单策略-物流信息" + ewaybillLogisticsDO);
                        }
                        Integer etype = ewaybillLogisticsDO.getEtype();
                        //电子面单类型为直连，物流公司为京东  更新出库通知单 是否获取电子面单 为 是
                        if (etype != null && etype.equals(SgOutConstantsIF.EWAYBILL_TYPE_STRAIGHT) &&
                                SgOutConstantsIF.LOGISTICS_JD.equals(outNotices.getCpCLogisticsEcode())) {
                            outNotices.setIsEnableewaybill(SgOutConstantsIF.IS_ENABLEEWAYBILL_Y);
                            //调用MQ
                            SgOutNoticesSendMsgRequest result = new SgOutNoticesSendMsgRequest();
                            result.setOutNotices(outNotices);
                            result.setAffiliatedShop(outNotices.getCpCShopId());
                            if (StringUtils.isNotEmpty(ewaybillLogisticsDO.getAffiliatedShop())) {
                                result.setAffiliatedShop(Long.parseLong(ewaybillLogisticsDO.getAffiliatedShop()));
                            }
                            result.setEtype(etype);

                            SgOutNoticesBatchSendMsgRequest batchResult = new SgOutNoticesBatchSendMsgRequest();
                            List<SgOutNoticesSendMsgRequest> resultList = new ArrayList<>();
                            resultList.add(result);

                            batchResult.setLoginUser(user);
                            batchResult.setResultList(resultList);

                            String configName = SgInConstants.SG_PHY_RESULT_IN_MQ_CONFINGNAME;
                            String topic = pconf.getProperty(SgInConstants.R3_OMS_SG_OUT_MQ_TOPIC_KEY);
                            String tag = SgOutConstantsIF.SGB_CHANNEL_SG_OUT_NOTICES_FACE_TAGS;
                            String body = JSONObject.toJSONString(batchResult);
                            String msgKey = outNotices.getBillNo() + DateUtil.getDateTime(SgOutConstants.MQ_FORMAT);
                            r3MqSendHelper.sendMessage(configName, body, topic, tag, msgKey);
                        }
                    }
                }
            } else {
                log.error(this.getClass().getName() + " 查询电子面单策略失敗!");
            }
        } catch (Exception e) {
            log.error(this.getClass().getName() + ".error:" + e.getMessage(), e);
            AssertUtils.logAndThrow("电子面单服务异常 " + e.getMessage());
        }
    }

    /**
     * 设置主表数量和金额
     *
     * @param objId             出库通知单ID
     * @param noticesItemMapper mapper
     * @param outNotices        出库通知单对象
     */
    public void setOutResultTot(Long objId, SgBPhyOutNoticesItemMapper noticesItemMapper, SgBPhyOutNotices outNotices, Integer isUpdate) {
        BigDecimal zero = BigDecimal.ZERO;

        SgBPhyOutNoticesItem noticesItem = noticesItemMapper.selectSumAmt(objId);
        outNotices.setTotQty(noticesItem != null ? noticesItem.getQty() : zero);         // 总通知数量
        outNotices.setTotAmtList(noticesItem != null ? noticesItem.getAmtList() : zero); // 总吊牌金额
        outNotices.setTotAmtCost(noticesItem != null ? noticesItem.getAmtCost() : zero); // 总成交金额
        outNotices.setTotQtyDiff(noticesItem != null ? noticesItem.getQtyDiff() : zero);       // 总差异数量
        outNotices.setTotAmtCostDiff(noticesItem != null ? noticesItem.getAmtCostDiff() : zero);   // 总差异成交金额
        outNotices.setTotAmtListDiff(noticesItem != null ? noticesItem.getAmtListDiff() : zero);   // 总差异吊牌金额
        if (isUpdate != null && isUpdate == ResultCode.SUCCESS) {
            outNotices.setTotQtyOut(noticesItem != null ? noticesItem.getQtyOut() : zero); // 总出库数量
            outNotices.setTotAmtCostOut(noticesItem != null ? noticesItem.getAmtCostOut() : zero); // 总出库成交金额
            outNotices.setTotAmtListOut(noticesItem != null ? noticesItem.getAmtListOut() : zero); // 总出库吊牌金额
        }
    }

    /**
     * 出库通知单主子表更新
     *
     * @param request           入参
     * @param noticesMapper     主表mapper
     * @param noticesItemMapper 明细mapper
     * @return
     */
    public ValueHolderV14<SgR3BaseResult> upDateOutNotices(SgPhyOutNoticesBillSaveRequest request,
                                                           SgBPhyOutNoticesMapper noticesMapper,
                                                           SgBPhyOutNoticesItemMapper noticesItemMapper) {

        ValueHolderV14<SgR3BaseResult> v14 = new ValueHolderV14<>();
        User loginUser = request.getLoginUser();
        String eName = loginUser.getEname();

        Long objId = request.getObjId();
        if (!CollectionUtils.isEmpty(request.getOutNoticesItemRequests())) {

            List<SgPhyOutNoticesItemSaveRequest> insertItemsRequest = new ArrayList<>();
            for (SgPhyOutNoticesItemSaveRequest item : request.getOutNoticesItemRequests()) {
                Long sourceBillItemId = item.getSourceBillItemId();

                // 查询明细信息
                QueryWrapper<SgBPhyOutNoticesItem> queryWrapper = new QueryWrapper<>();
                SgBPhyOutNoticesItem queryParam = new SgBPhyOutNoticesItem();
                queryParam.setSgBPhyOutNoticesId(objId);
                queryParam.setSourceBillItemId(sourceBillItemId);
                queryWrapper.setEntity(queryParam);
                SgBPhyOutNoticesItem queryNoticesItem = noticesItemMapper.selectOne(queryWrapper);
                if (queryNoticesItem != null) {

                    SgBPhyOutNoticesItem noticesItem = new SgBPhyOutNoticesItem();
                    BeanUtils.copyProperties(item, noticesItem);

                    // 明细信息已存在，更新明细
                    noticesItem.setId(queryNoticesItem.getId());
                    if (item.getQty() != null) {
                        noticesItem.setAmtList(queryNoticesItem.getPriceList().multiply(item.getQty()));
                        if (queryNoticesItem.getPriceCost() != null) {
                            noticesItem.setAmtCost(queryNoticesItem.getPriceCost().multiply(item.getQty()));
                        }
                    }
                    StorageESUtils.setBModelDefalutDataByUpdate(noticesItem, loginUser);
                    noticesItem.setModifierename(eName);
                    noticesItemMapper.updateById(noticesItem);
                } else {
                    // 不存在，则收集批量新增明细
                    insertItemsRequest.add(item);
                }
            }

            // 批量新增明细
            if (insertItemsRequest.size() > 0) {
                insertOutNoticesItem(objId, insertItemsRequest, request.getLoginUser());
            }

            // 更新主表-接口调用
            SgBPhyOutNotices updateNotices = new SgBPhyOutNotices();
            BeanUtils.copyProperties(request.getOutNoticesRequest(), updateNotices);
            setOutResultTot(objId, noticesItemMapper, updateNotices, ResultCode.SUCCESS);
            updateNotices.setId(objId);
            StorageESUtils.setBModelDefalutDataByUpdate(updateNotices, loginUser);
            updateNotices.setModifierename(eName);
            //2019-09-06 截取备注等超长字段
            updateNotices.setRemark(StorageESUtils.strSubString(updateNotices.getRemark(), SgConstants.SG_COMMON_REMARK_SIZE));
            updateNotices.setBuyerRemark(StorageESUtils.strSubString(updateNotices.getBuyerRemark(), SgConstants.SG_COMMON_REMARK_SIZE));
            updateNotices.setSellerRemark(StorageESUtils.strSubString(updateNotices.getSellerRemark(), SgConstants.SG_COMMON_REMARK_SIZE));
            updateNotices.setWmsFailReason(StorageESUtils.strSubString(updateNotices.getWmsFailReason(), SgConstants.SG_COMMON_STRING_SIZE));
            noticesMapper.updateById(updateNotices);
        }

        SgR3BaseResult date = new SgR3BaseResult();
        JSONObject dataJo = new JSONObject();
        dataJo.put(R3ParamConstants.OBJID, objId);
        dataJo.put(R3ParamConstants.TABLENAME, SgConstants.SG_B_PHY_OUT_NOTICES.toUpperCase());
        date.setDataJo(dataJo);
        v14.setCode(ResultCode.SUCCESS);
        v14.setMessage("保存成功！");
        v14.setData(date);
        return v14;
    }


    /**
     * 根据出库结果单更新出库通知单
     *
     * @param user        用户信息
     * @param objId       出库通知单ID
     * @param resultItems 出库结果单明细
     * @param outResult   出库结果单
     */
    public JSONObject updateOutNoticesByResult(User user, Long objId, int isLast,
                                               List<SgBPhyOutResultItem> resultItems, Date outTime, SgBPhyOutResult outResult, Long pickOrderId) {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        Locale locale = user.getLocale();
        String eName = user.getEname();

        SgBPhyOutNoticesMapper noticesMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesMapper.class);
        SgBPhyOutNoticesItemMapper noticesItemMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesItemMapper.class);

        //记录待推es的明细
        List<SgBPhyOutNoticesItem> updateNoticesItems = Lists.newArrayList();

        /**
         * 更新出库通知单子表:
         *  出库数量
         *  差异数量
         *  出库吊牌金额
         *  出库成交金额
         *  差异吊牌金额
         *  差异成交金额
         */
        resultItems.forEach(outResultItem -> {
            LambdaQueryWrapper<SgBPhyOutNoticesItem> noticesWrapper = new QueryWrapper<SgBPhyOutNoticesItem>().lambda();
            noticesWrapper.eq(SgBPhyOutNoticesItem::getSgBPhyOutNoticesId, objId);
            Long sgBPhyOutNoticesItemId = outResultItem.getSgBPhyOutNoticesItemId();
            if (sgBPhyOutNoticesItemId != null) {
                noticesWrapper.eq(SgBPhyOutNoticesItem::getId, outResultItem.getSgBPhyOutNoticesItemId());
            } else {
                noticesWrapper.eq(SgBPhyOutNoticesItem::getPsCSkuEcode, outResultItem.getPsCSkuEcode());
            }
            SgBPhyOutNoticesItem outNoticesItem = noticesItemMapper.selectOne(noticesWrapper);
            if (outNoticesItem != null) {
                BigDecimal priceList = outNoticesItem.getPriceList();
                BigDecimal priceCost = outNoticesItem.getPriceCost();
                if (priceCost != null) {
                    outNoticesItem.setAmtCostOut(outNoticesItem.getQtyOut().multiply(priceCost)); // 出库成交金额
                    outNoticesItem.setAmtCostDiff(outNoticesItem.getQtyDiff().multiply(priceCost)); // 差异成交金额
                }
                outNoticesItem.setQtyOut(outNoticesItem.getQtyOut().add(outResultItem.getQty())); // 出库数量
                outNoticesItem.setQtyDiff(outNoticesItem.getQty().subtract(outNoticesItem.getQtyOut())); // 差异数量
                outNoticesItem.setAmtListOut(outNoticesItem.getQtyOut().multiply(priceList)); // 出库吊牌金额
                outNoticesItem.setAmtListDiff(outNoticesItem.getQtyDiff().multiply(priceList)); // 差异吊牌金额
                StorageESUtils.setBModelDefalutDataByUpdate(outNoticesItem, user);
                outNoticesItem.setModifierename(eName);
               /* if (outNoticesItem.getQtyOut().compareTo(outNoticesItem.getQty()) > 0) {
                    AssertUtils.logAndThrow("出库数量不能大于通知数量,sku=" + outResultItem.getPsCSkuEcode(), user.getLocale());
                }*/
                noticesItemMapper.updateById(outNoticesItem);
                updateNoticesItems.add(outNoticesItem);
            }
        });

        //check出库数量不能大于通知数量
        List<JSONObject> sumGroupBySku = noticesItemMapper.selectSumGroupBySku(objId);
        if (CollectionUtils.isNotEmpty(sumGroupBySku)) {
            if (log.isDebugEnabled()) {
                log.debug("出库通知明细按条码分组查询结果" + JSONObject.toJSONString(sumGroupBySku));
            }
            for (JSONObject var : sumGroupBySku) {
                String ps_c_sku_ecode = var.getString("ps_c_sku_ecode");
                BigDecimal tot_qty = Optional.ofNullable(var.getBigDecimal("tot_qty")).orElse(BigDecimal.ZERO);
                BigDecimal tot_qty_out = Optional.ofNullable(var.getBigDecimal("tot_qty_out")).orElse(BigDecimal.ZERO);
                if (tot_qty_out.compareTo(tot_qty) > 0) {
                    AssertUtils.logAndThrow("条码" + ps_c_sku_ecode + "出库数量不能大于通知数量！");
                }
            }
        }

        /**
         * 更新出库通知单主表(直接sum字表明细字段)
         *  单据状态(根据 差异数量=0:全部出库；差异数量>0:部分出库)
         *  出库时间
         *  总出库数量
         *  总差异数量
         *  总出库吊牌金额
         *  总出库成交金额
         *  总差异吊牌金额
         *  总差异成交金额
         */
        SgBPhyOutNotices updateNotices = noticesMapper.selectById(objId);
        AssertUtils.notNull(updateNotices, "不存在对应的出库通知单！objId=" + objId, locale);

        Integer billStatus = updateNotices.getBillStatus();
        if (billStatus != null) {
            if (billStatus == SgOutConstantsIF.OUT_NOTICES_STATUS_VOID || StringUtils.equals(updateNotices.getIsactive(), SgConstants.IS_ACTIVE_N)) {
                AssertUtils.logAndThrow("当前结果单所关联的通知单已作废，不允许操作！通知单单据编号：" + updateNotices.getBillNo(), locale);
            }
            //start sunjunlei 去掉控制
//            if (billStatus == SgOutConstantsIF.OUT_NOTICES_STATUS_ALL) {
//                AssertUtils.logAndThrow("当前结果单所关联的通知单已全部出库，不允许审核！", locale);
//            }
            //end sunjunlei 去掉控制
        }

        SgBPhyOutNoticesItem noticesItem = noticesItemMapper.selectSumAmt(objId);
        AssertUtils.notNull(noticesItem, "出库通知单明细为空！，objId=" + objId, locale);
        BigDecimal totQtyOut = noticesItem.getQtyOut();
        BigDecimal qty = noticesItem.getQty(); // 总通知数量
        updateNotices.setTotQtyOut(totQtyOut);  // 总出库数量
        updateNotices.setTotQtyDiff(qty.subtract(totQtyOut)); // 总差异数量
        updateNotices.setTotAmtListOut(noticesItem.getAmtListOut()); // 总出库吊牌金额
        updateNotices.setTotAmtCostOut(noticesItem.getAmtCostOut()); // 总出库成交金额
        updateNotices.setTotAmtListDiff(noticesItem.getAmtListDiff()); // 总差异吊牌金额
        updateNotices.setTotAmtCostDiff(noticesItem.getAmtCostDiff()); // 总差异成交金额
        updateNotices.setOutTime(outTime != null ? outTime : timestamp);  // 出库时间

        // 2019-07-09添加逻辑：如果是最后一次出库则判断是否确实是最后一次出库
        JSONObject isLastObj = new JSONObject();
        isLastObj.put("isAll", false);
        // 单据状态
        BigDecimal totQtyDiff = updateNotices.getTotQtyDiff();
        if (isLast == SgOutConstantsIF.OUT_IS_LAST_Y) {
            if (totQtyDiff.compareTo(BigDecimal.ZERO) == 0) {
                isLastObj.put("isAll", true);
            }
            updateNotices.setBillStatus(SgOutConstantsIF.OUT_NOTICES_STATUS_ALL);
            if (pickOrderId != null) {
                updateNotices.setPickStatus(SgOutConstantsIF.PICK_STATUS_SUCCESS);
            }
        } else {
            if (totQtyDiff.compareTo(BigDecimal.ZERO) == 0) {
                updateNotices.setBillStatus(SgOutConstantsIF.OUT_NOTICES_STATUS_ALL);
                if (pickOrderId != null) {
                    updateNotices.setPickStatus(SgOutConstantsIF.PICK_STATUS_SUCCESS);
                }
                // 2019-05-30添加逻辑：如果差异数量等于0，认为全部出库
                isLast = SgOutConstantsIF.OUT_IS_LAST_Y;
                isLastObj.put("isAll", true);
                // --------------水星20191225 周琳胜  start
            } else if (totQtyDiff.compareTo(BigDecimal.ZERO) > 0 && totQtyDiff.compareTo(qty) < 0) {
                updateNotices.setBillStatus(SgOutConstantsIF.OUT_NOTICES_STATUS_PART);
                if (pickOrderId != null) {
                    updateNotices.setPickStatus(SgOutConstantsIF.PICK_STATUS_WAIT);
                }
            } else if (totQtyDiff.compareTo(qty) == 0){
                updateNotices.setBillStatus(SgOutConstantsIF.OUT_NOTICES_STATUS_WAIT);
                if (pickOrderId != null) {
                    updateNotices.setPickStatus(SgOutConstantsIF.PICK_STATUS_WAIT);
                }
            }
            // --------------水星20191225 周琳胜  end
//            } else {
//                updateNotices.setBillStatus(SgOutConstantsIF.OUT_NOTICES_STATUS_PART);
//                if (pickOrderId != null) {
//                    updateNotices.setPickStatus(SgOutConstantsIF.PICK_STATUS_WAIT);
//                }
//            }
        }
        isLastObj.put("isLast", isLast);
        StorageESUtils.setBModelDefalutDataByUpdate(updateNotices, user);
        updateNotices.setModifierename(eName);
        if (outResult != null) {
            Long logisticsId = outResult.getCpCLogisticsId();
            String logisticsEcode = outResult.getCpCLogisticsEcode();
            String logisticsEname = outResult.getCpCLogisticsEname();
            String logisticNumber = outResult.getLogisticNumber();
            if (log.isDebugEnabled()) {
                log.debug("出库结果回写通知单物流信息:" + logisticsId + "," + logisticsEcode + "," + logisticsEname + "," + logisticNumber);
            }
            updateNotices.setCpCLogisticsId(logisticsId);
            updateNotices.setCpCLogisticsEcode(logisticsEcode);
            updateNotices.setCpCLogisticsEname(logisticsEname);
            updateNotices.setLogisticNumber(logisticNumber);
        }
        noticesMapper.updateById(updateNotices);

        //推送ES
        warehouseESUtils.pushESByOutNotices(objId, true, false, null, noticesMapper, noticesItemMapper);

        return isLastObj;
    }

    /**
     * 出库通知单明细新增
     *
     * @param objId
     * @param items
     * @param loginUser
     * @return
     */
    public HashMap insertOutNoticesItem(Long objId, List<SgPhyOutNoticesItemSaveRequest> items, User loginUser) {
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ",SgPhyOutNoticesItemSaveRequest items:" + JSON.toJSONString(items));
        }
        SgBPhyOutNoticesItemMapper noticesItemMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesItemMapper.class);
        List<SgBPhyOutNoticesItem> outNoticesItems = new ArrayList<>();
        BigDecimal zero = BigDecimal.ZERO;
        String eName = loginUser.getEname();


        //2019-08-12 通知单保存时合并相同sku的明细
        Map<Long, SgPhyOutNoticesItemSaveRequest> requestMap = Maps.newHashMap();
        for (SgPhyOutNoticesItemSaveRequest noticesItemRequest : items) {
            Long psCSkuId = noticesItemRequest.getPsCSkuId();
            if (requestMap.containsKey(psCSkuId)) {
                SgPhyOutNoticesItemSaveRequest orgNoticesItemRequest = requestMap.get(psCSkuId);
                BigDecimal orgQty = Optional.ofNullable(orgNoticesItemRequest.getQty()).orElse(BigDecimal.ZERO);
                BigDecimal qty = Optional.ofNullable(noticesItemRequest.getQty()).orElse(BigDecimal.ZERO);
                orgNoticesItemRequest.setQty(qty.add(orgQty));
            } else {
                requestMap.put(psCSkuId, noticesItemRequest);
            }
        }
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ",requestMap.values:" + JSON.toJSONString(requestMap.values()));
        }
        //合并后的明细
        List<SgPhyOutNoticesItemSaveRequest> mergeItemList = new ArrayList<>(requestMap.values());

        HashMap skuMap = new HashMap(); // 收集sku信息
        mergeItemList.forEach(item -> {
            BigDecimal qty = item.getQty();             // 通知数量
            BigDecimal priceList = item.getPriceList(); // 吊牌价
            BigDecimal amtList = priceList.multiply(qty);  // 吊牌金额

            SgBPhyOutNoticesItem noticesItem = new SgBPhyOutNoticesItem();
            BeanUtils.copyProperties(item, noticesItem);
            Long id = Tools.getSequence(SgConstants.SG_B_PHY_OUT_NOTICES_ITEM);
            noticesItem.setId(id);
            noticesItem.setSgBPhyOutNoticesId(objId);

            noticesItem.setQty(qty);            // 通知数量
            noticesItem.setQtyDiff(qty);        // 差异数量（通知数量-出库数量）
            noticesItem.setAmtList(amtList);    // 吊牌金额
            noticesItem.setAmtListDiff(amtList);// 差异吊牌金额
            noticesItem.setQtyOut(zero);        // 出库数量
            noticesItem.setAmtListOut(zero);    // 出库吊牌金额
            noticesItem.setAmtCostOut(zero);    // 出库成交金额

            // 成交金额、差异成交金额
            if (item.getPriceCost() != null) {
                BigDecimal amtCost = item.getPriceCost().multiply(qty);
                noticesItem.setAmtCost(amtCost);
                noticesItem.setAmtCostDiff(amtCost);
            } else {
                noticesItem.setAmtCost(zero);
                noticesItem.setAmtCostDiff(zero);
            }

            StorageESUtils.setBModelDefalutData(noticesItem, loginUser);
            noticesItem.setOwnerename(eName);
            noticesItem.setModifierename(eName);
            outNoticesItems.add(noticesItem);
            skuMap.put(item.getPsCSkuEcode(), id);
        });

        List<List<SgBPhyOutNoticesItem>> baseModelPageList = StorageESUtils.getBaseModelPageList(
                outNoticesItems, SgOutConstants.INSERT_PAGE_SIZE);

        int result = 0;
        for (List<SgBPhyOutNoticesItem> pageList : baseModelPageList) {

            if (CollectionUtils.isEmpty(pageList)) {
                continue;
            }
            int count = noticesItemMapper.batchInsert(pageList);
            if (count != pageList.size()) {
                log.error(this.getClass().getName() + " 出库通知单明细批量新增失败！");
                AssertUtils.logAndThrow("出库通知单明细批量新增失败！", loginUser.getLocale());
            }
            result += count;
        }
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ".debug, 出库通知单明细新增结果：" + result);
        }
        return skuMap;
    }

    /**
     * 参数校验(接口调用)
     *
     * @param request 入参
     */
    public void checkParams(SgPhyOutNoticesBillSaveRequest request) {

        if (storageBoxConfig.getBoxEnable()) {
            SgPhyOutNoticesConvertService sgPhyOutNoticesConvertService = ApplicationContextHandle.getBean(SgPhyOutNoticesConvertService.class);
            sgPhyOutNoticesConvertService.sgPhyOutNoticesRequestConvert(request);
        }

        AssertUtils.notNull(request.getLoginUser(), "用户信息不能为空！");

        SgPhyOutNoticesSaveRequest outNoticesRequest = request.getOutNoticesRequest();
        AssertUtils.notNull(outNoticesRequest, "主表信息不能为空！");

        AssertUtils.notNull(outNoticesRequest.getOutType(), "出库类型不能为空！");
        AssertUtils.notNull(outNoticesRequest.getSourceBillId(), "来源单据ID不能为空！");
        AssertUtils.notNull(outNoticesRequest.getSourceBillNo(), "来源单据编号不能为空！");
        AssertUtils.notNull(outNoticesRequest.getSourceBillType(), "来源单据类型不能为空！");

        // 根据来源单据ID和类型判断出库通知单是否已经存在
        SgBPhyOutNotices sgBPhyOutNotices = queryNoticesBySource(outNoticesRequest);
        if (sgBPhyOutNotices != null) {

            Long noticesId = sgBPhyOutNotices.getId();
            request.setObjId(noticesId);
            // 出库通知单已存在，做更新的check
            if (!CollectionUtils.isEmpty(request.getOutNoticesItemRequests())) {
                request.getOutNoticesItemRequests().forEach(item -> {
                    AssertUtils.notNull(item.getSourceBillItemId(), "明细来源单据ID不能为空！");
                    item.setSgBPhyOutNoticesId(noticesId);
                });
            }
        } else {

            request.setObjId(-1l);
            // 不存在，做新增check
            AssertUtils.notNull(outNoticesRequest.getCpCPhyWarehouseId(), "实体仓ID不能为空！");
            AssertUtils.notNull(outNoticesRequest.getCpCPhyWarehouseEcode(), "实体仓编码不能为空！");
            AssertUtils.notNull(outNoticesRequest.getCpCPhyWarehouseEname(), "实体仓名称不能为空！");
            if (CollectionUtils.isEmpty(request.getOutNoticesItemRequests())) {
                AssertUtils.logAndThrow("空单不允许新增！");
            }

            // 查询实体仓信息
            CpCPhyWarehouseMapper warehouseMapper = ApplicationContextHandle.getBean(CpCPhyWarehouseMapper.class);
            CpCPhyWarehouse cpCPhyWarehouse = warehouseMapper.selectById(outNoticesRequest.getCpCPhyWarehouseId());
            AssertUtils.notNull(cpCPhyWarehouse, "实体仓档案中不存在该实体仓信息！");
            /**
             * 8.判断主表中【是否传wms】是否存在为空值
             *  若存在，则判断主表中的实体仓在【实体仓档案】中的【WMS管控仓】是否勾选
             *      若未勾选，则生成出库通知单的时候【是否传WMS】字段赋值为“0-否”
             *      若勾选，则生成出库通知的时候【是否传WMS】字段赋值为“1-是”
             *  若不存在，则生成出库通知的时候【是否传WMS】字段取传入的信息。
             */
            outNoticesRequest.setIsPassWms(cpCPhyWarehouse.getWmsControlWarehouse());
            outNoticesRequest.setGoodsOwner(cpCPhyWarehouse.getOwnerCode());

            //集成京東电子面单-增加 物流公司和店铺信息非空判断
            //2019-08-04  若为货到付款需要check付款金额是否为空
            if (!request.getIsPos() && outNoticesRequest.getSourceBillType() == SgConstantsIF.BILL_TYPE_RETAIL) {
                if (outNoticesRequest.getCpCLogisticsId() == null ||
                        StringUtils.isEmpty(outNoticesRequest.getCpCLogisticsEcode()) ||
                        outNoticesRequest.getCpCShopId() == null) {
                    AssertUtils.logAndThrow("物流公司id 物流公司编码 店铺id 不能为空！");
                }
                if (outNoticesRequest.getIsCod() != null &&
                        outNoticesRequest.getIsCod() == SgOutConstants.PAY_TYPE_ARRIVE &&
                        outNoticesRequest.getAmtPayment() == null) {
                    AssertUtils.logAndThrow("付款类型为货到付款,付款金额不能为空!");
                }
            }

            // 校验明细必传字段
            request.getOutNoticesItemRequests().forEach(item -> {
                AssertUtils.notNull(item.getSourceBillItemId(), "明细来源单据ID不能为空！");
                AssertUtils.notNull(item.getPsCSkuId(), "条码ID不能为空！");
                AssertUtils.notNull(item.getPsCSkuEcode(), "条码编码不能为空！");
//                AssertUtils.notNull(item.getGbcode(), "国标码不能为空！");
                AssertUtils.notNull(item.getPsCProId(), "商品ID不能为空！");
                AssertUtils.notNull(item.getPsCProEcode(), "商品编码不能为空！");
                AssertUtils.notNull(item.getPsCProEname(), "商品名称不能为空！");
                AssertUtils.notNull(item.getPsCSpec1Id(), "规格1ID不能为空！");
                AssertUtils.notNull(item.getPsCSpec1Ecode(), "规格1编码不能为空！");
                AssertUtils.notNull(item.getPsCSpec1Ename(), "规格1名称不能为空！");
                AssertUtils.notNull(item.getPsCSpec2Id(), "规格2ID不能为空！");
                AssertUtils.notNull(item.getPsCSpec2Ecode(), "规格2编码不能为空！");
                AssertUtils.notNull(item.getPsCSpec2Ename(), "规格2名称不能为空！");
                AssertUtils.notNull(item.getPriceList(), "明细吊牌价不能为空！");
                AssertUtils.notNull(item.getQty(), "通知数量不能为空！");
            });
        }

    }

    /**
     * 批量查询出库通知单
     *
     * @param requests
     * @return
     */
    public ValueHolderV14<List<SgBPhyOutNotices>> queryOutNoticesListBySource(List<SgPhyOutNoticesSaveRequest> requests) {
        AssertUtils.notNull(requests, "参数不能为空！");
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ".debug, queryOutNoticesByBillNo-param:" + JSONObject.toJSONString(requests));
        }

        ValueHolderV14<List<SgBPhyOutNotices>> v14 = new ValueHolderV14<>();
        // 批量查询出库通知单
        List<SgBPhyOutNotices> sgBPhyOutNotices = new ArrayList<>();
        for (SgPhyOutNoticesSaveRequest request : requests) {
            if (StringUtils.isEmpty(request.getBillNo())) {
                if (request.getSourceBillType() == null || request.getSourceBillId() == null) {
                    v14.setCode(ResultCode.FAIL);
                    v14.setMessage("来源单据类型/来源单据ID不能为空！");
                    return v14;
                }
            }
            sgBPhyOutNotices.add(queryNoticesBySource(request));
        }

        v14.setCode(ResultCode.SUCCESS);
        v14.setMessage("查询成功！");
        v14.setData(sgBPhyOutNotices);
        return v14;
    }


    /**
     * 根据单据编号批量查询出库通知单
     *
     * @param requests
     */
    public ValueHolderV14<List<SgOutNoticesByBillNoResult>> queryOutNoticesByBillNo(List<SgPhyOutNoticesSaveRequest> requests) {

        AssertUtils.notNull(requests, "参数不能为空！");
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ".debug, queryOutNoticesByBillNo-param:" + JSONObject.toJSONString(requests));
        }

        ValueHolderV14<List<SgOutNoticesByBillNoResult>> v14 = new ValueHolderV14<>();
        v14.setCode(ResultCode.FAIL);

        try {
            List<SgOutNoticesByBillNoResult> list = new ArrayList<>();

            ValueHolderV14<List<SgBPhyOutNotices>> holderV14 = queryOutNoticesListBySource(requests);
            if (holderV14.getCode() == ResultCode.FAIL) {
                v14.setMessage(holderV14.getMessage());
                return v14;
            }

            List<SgBPhyOutNotices> sgBPhyOutNotices = holderV14.getData();
            if (!CollectionUtils.isEmpty(sgBPhyOutNotices)) {
                SgBPhyOutNoticesItemMapper noticesItemMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesItemMapper.class);
                SgBPhyOutNoticesImpItemMapper noticesImpItemMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesImpItemMapper.class);
                SgBPhyOutNoticesTeusItemMapper noticesTeusItemMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesTeusItemMapper.class);

                sgBPhyOutNotices.forEach(notices -> {
                    SgOutNoticesByBillNoResult noticesByBillNoResult = new SgOutNoticesByBillNoResult();
                    noticesByBillNoResult.setOutNotices(notices);
                    if (storageBoxConfig.getBoxEnable()) {//alter by nrl 添加箱定义
                        QueryWrapper<SgBPhyOutNoticesImpItem> query = new QueryWrapper<>();
                        query.eq(SgOutConstants.OUT_NOTICES_PAREN_FIELD, notices.getId());
                        List<SgBPhyOutNoticesImpItem> outNoticesItems = noticesImpItemMapper.selectList(query);
                        noticesByBillNoResult.setImpItems(outNoticesItems);

                        QueryWrapper<SgBPhyOutNoticesTeusItem> query2 = new QueryWrapper<>();
                        query2.eq(SgOutConstants.OUT_NOTICES_PAREN_FIELD, notices.getId());
                        List<SgBPhyOutNoticesTeusItem> outNoticesTeusItems = noticesTeusItemMapper.selectList(query2);
                        noticesByBillNoResult.setSgBPhyOutNoticesTeusItemList(outNoticesTeusItems);
                    } else {
                        QueryWrapper<SgBPhyOutNoticesItem> query = new QueryWrapper<>();
                        query.eq(SgOutConstants.OUT_NOTICES_PAREN_FIELD, notices.getId());
                        List<SgBPhyOutNoticesItem> outNoticesItems = noticesItemMapper.selectList(query);
                        noticesByBillNoResult.setItems(outNoticesItems);
                    }
                    list.add(noticesByBillNoResult);


                });
            }
            v14.setCode(ResultCode.SUCCESS);
            v14.setMessage("查询成功！");
            v14.setData(list);
        } catch (Exception e) {
            log.error(this.getClass().getName() + e.getMessage(), e);
            v14.setMessage(e.getMessage());
        }
        return v14;
    }

    /**
     * 根据来源单据 查询出库通知单
     *
     * @param noticesRequest
     * @return
     */
    public SgBPhyOutNotices queryNoticesBySource(SgPhyOutNoticesSaveRequest noticesRequest) {
        SgBPhyOutNoticesMapper noticesMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesMapper.class);
        QueryWrapper<SgBPhyOutNotices> queryWrapper = new QueryWrapper<>();
        SgBPhyOutNotices queryParam = new SgBPhyOutNotices();
        queryParam.setIsactive(SgConstants.IS_ACTIVE_Y);
        if (!StringUtils.isEmpty(noticesRequest.getBillNo())) {
            queryParam.setBillNo(noticesRequest.getBillNo());
        } else {
            queryParam.setSourceBillType(noticesRequest.getSourceBillType());
            if (noticesRequest.getSourceBillId() != null) {
                queryParam.setSourceBillId(noticesRequest.getSourceBillId());
            } else {
                queryParam.setSourceBillNo(noticesRequest.getSourceBillNo());
            }
        }
        queryWrapper.setEntity(queryParam);
        return noticesMapper.selectOne(queryWrapper);
    }

    /**
     * 重传WMS
     *
     * @param request 入参
     * @return 出参
     */
    public ValueHolder sendWmsAgainOutNotices(SgPhyOutBillSendWmsAgainRequest request) {

        AssertUtils.notNull(request, "参数不能为空！");
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ".debug, SendWmsAgainOutNotices-param:" + JSONObject.toJSONString(request));
        }

        User user = request.getUser();
        AssertUtils.notNull(user, "用户信息不能为空！");

        JSONArray ids = request.getIds();
        if (CollectionUtils.isEmpty(ids)) {
            AssertUtils.logAndThrow("请选择数据！", user.getLocale());
        }

        ValueHolder vh = new ValueHolder();
        SgPhyOutNoticesSaveService noticesSaveService = ApplicationContextHandle.getBean(SgPhyOutNoticesSaveService.class);
        // 单对象页面返回
        if (request.getIsObj()) {
            try {
                SgBaseModelR3 baseModelR3 = noticesSaveService.updateOutNotices(user, ids.getLong(0));
                vh.put(R3ParamConstants.CODE, baseModelR3.getCode());
                vh.put(R3ParamConstants.MESSAGE, baseModelR3.getMessage());
            } catch (Exception e) {
                log.error("重传wms接口异常:" + e.getMessage());
                vh.put(R3ParamConstants.CODE, ResultCode.FAIL);
                vh.put(R3ParamConstants.MESSAGE, "重传wms接口异常:" + e.getMessage());
            }
            return vh;
        }

        JSONArray errorArr = new JSONArray();
        Integer accSuccess = 0, accFailed = 0;
        for (int i = 0; i < ids.size(); i++) {
            try {
                SgBaseModelR3 baseModelR3 = noticesSaveService.updateOutNotices(user, ids.getLong(i));
                Integer code = baseModelR3.getCode();
                if (code == ResultCode.SUCCESS) {
                    accSuccess++;
                } else {
                    throw new NDSException(Resources.getMessage(baseModelR3.getMessage()));
                }
            } catch (Exception e) {
                e.printStackTrace();
                log.error("objid:" + ids.getLong(i) + ",重传wms接口异常:" + e.getMessage());
                JSONObject errorDate = new JSONObject();
                errorDate.put(R3ParamConstants.CODE, ResultCode.FAIL);
                errorDate.put(R3ParamConstants.OBJID, ids.getLong(i));
                errorDate.put(R3ParamConstants.MESSAGE, "重传wms失败:" + e.getMessage());
                errorArr.add(errorDate);
                accFailed++;
            }
        }

        String message = "重传WMS成功记录数：" + accSuccess;
        if (!CollectionUtils.isEmpty(errorArr)) {
            message += "，重传WMS失败记录数：" + accFailed;
            return ValueHolderUtils.fail(message, errorArr);
        } else {
            return ValueHolderUtils.success(message);
        }
    }

    /**
     * 更新WMS状态
     *
     * @param objId 主键ID
     * @param user  用户信息
     * @return 返回
     */
    @Transactional(rollbackFor = Exception.class)
    public SgBaseModelR3 updateOutNotices(User user, Long objId) {
        SgBaseModelR3 result = new SgBaseModelR3();
        SgBPhyOutNoticesMapper noticesMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesMapper.class);
        SgBPhyOutNotices outNotices = noticesMapper.selectById(objId);
        result.setId(objId);
        result.setCode(ResultCode.FAIL);
        if (outNotices == null) {
            result.setMessage(Resources.getMessage("当前记录不存在！"));
            return result;
        }

        Long wmsStatus = outNotices.getWmsStatus();
        if (wmsStatus != null && wmsStatus != SgOutConstantsIF.WMS_STATUS_PASS_FAILED) {
            result.setMessage(Resources.getMessage("当前记录不是“传WMS失败”状态，不允许重传！"));
            return result;
        }

        Long sourceBillId = outNotices.getSourceBillId();
        Integer sourceBillType = outNotices.getSourceBillType();
        //2019-08-30  来源单据类型为零售发货的时候 ，调订单接口清空传wms失败次数
        if (sourceBillId != null && sourceBillType != null && sourceBillType == SgConstantsIF.BILL_TYPE_RETAIL) {
            AgainWmsCmd againWmsCmd = (AgainWmsCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                    AgainWmsCmd.class.getName(), "oms-fi", "1.0");
            if (log.isDebugEnabled()) {
                log.debug("调用订单清空wms失败次数入参:" + sourceBillId);
            }
            ValueHolderV14 againWms = againWmsCmd.againWms(sourceBillId);
            if (log.isDebugEnabled()) {
                log.debug("调用订单清空wms失败次数出参:" + JSONObject.toJSONString(againWms));
            }
            if (!againWms.isOK()) {
                result.setMessage("调用订单清空wms失败次数失败！" + againWms.getMessage());
                return result;
            }
        }

        // 更新单据状态
        SgBPhyOutNotices updateNotices = new SgBPhyOutNotices();
        updateNotices.setWmsStatus(SgOutConstantsIF.WMS_STATUS_WAIT_PASS);
        updateNotices.setWmsFailCount(BigDecimal.ZERO.longValue());
        updateNotices.setModifierid((long) user.getId());
        updateNotices.setModifiername(user.getName());
        updateNotices.setModifierename(user.getEname());
        updateNotices.setModifieddate(new Timestamp(System.currentTimeMillis()));
        int count = noticesMapper.update(updateNotices, new UpdateWrapper<SgBPhyOutNotices>().lambda()
                .eq(SgBPhyOutNotices::getId, objId)
                .set(SgBPhyOutNotices::getWmsFailReason, null));

        if (count <= 0) {
            result.setMessage(Resources.getMessage("重传WMS失败，系统错误！"));
        } else {
            result.setCode(ResultCode.SUCCESS);
            result.setMessage(Resources.getMessage("重传WMS成功！"));
        }

        //推送es
        warehouseESUtils.pushESByOutNotices(objId, false, false, null, noticesMapper, null);

        return result;
    }

    public ValueHolderV14 updateOutNoticesStatus(List<SgPhyOutNoticesUpdateRequest> requests, User user) {

        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ".debug, param:" + JSONObject.toJSONString(requests));
        }

        long start = System.currentTimeMillis();
        ValueHolderV14 v14 = new ValueHolderV14();
        if (CollectionUtils.isEmpty(requests)) {
            AssertUtils.logAndThrow("参数不能为空！");
        }

        AssertUtils.notNull(user, "用户信息不能为空！");
        String eName = user.getEname();

        try {
            SgBPhyOutNoticesMapper outNoticesMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesMapper.class);
            for (int i = 0; i < requests.size(); i++) {
                SgPhyOutNoticesUpdateRequest request = requests.get(i);
                String billNo = request.getBillNo();

                if (StringUtils.isNotEmpty(billNo)) {

                    // 根据出库通知单单据编号查询出库通知单
                    SgBPhyOutNotices outNotices = outNoticesMapper
                            .selectOne(new QueryWrapper<SgBPhyOutNotices>().lambda().eq(SgBPhyOutNotices::getBillNo, billNo));

                    if (outNotices != null) {

                        log.info(this.getClass().getName() + ",updateOutNoticesWmsStatus,outNotices" + JSONObject.toJSONString(outNotices));

                        SgBPhyOutNotices updateOutNotices = new SgBPhyOutNotices();
                        updateOutNotices.setId(outNotices.getId());

                        if (request.getWmsStatus() != null && SgOutConstantsIF.WMS_STATUS_PASS_SUCCESS == request.getWmsStatus()) {
                            updateOutNotices.setWmsStatus(request.getWmsStatus());
                            updateOutNotices.setPassWmsTime(new Timestamp(System.currentTimeMillis()));
                            StorageESUtils.setBModelDefalutDataByUpdate(updateOutNotices, user);
                            updateOutNotices.setModifierename(eName);
                            updateOutNotices.setWmsFailCount(0L);
                            updateOutNotices.setWmsBillNo(request.getWmsBillNo());
                            outNoticesMapper.update(updateOutNotices, new UpdateWrapper<SgBPhyOutNotices>().lambda()
                                    .eq(SgBPhyOutNotices::getId, outNotices.getId())
                                    .set(SgBPhyOutNotices::getWmsFailReason, null));
                        } else if (request.getWmsStatus() != null && SgOutConstantsIF.WMS_STATUS_PASS_FAILED == request.getWmsStatus()) {
                            String backResultMessgae = request.getWmsFailReason();

                            //失败
                            updateOutNotices.setWmsStatus(request.getWmsStatus());
                            updateOutNotices.setWmsFailReason(StorageESUtils.strSubString(backResultMessgae, SgConstants.SG_COMMON_STRING_SIZE));
                            Long wmsFailCount = outNotices.getWmsFailCount();
                            if (wmsFailCount == null) {
                                updateOutNotices.setWmsFailCount(1L);
                            } else {
                                updateOutNotices.setWmsFailCount(wmsFailCount + 1);
                            }
                            StorageESUtils.setBModelDefalutDataByUpdate(updateOutNotices, user);
                            updateOutNotices.setModifierename(eName);
                            outNoticesMapper.updateById(updateOutNotices);
                        } else {
                            if (request.getWmsStatus() != null) {
                                updateOutNotices.setWmsStatus(request.getWmsStatus());
                            }

                            if (request.getPickStatus() != null) {
                                updateOutNotices.setPickStatus(request.getPickStatus());
                            }
                            outNoticesMapper.update(updateOutNotices, new UpdateWrapper<SgBPhyOutNotices>().lambda()
                                    .eq(SgBPhyOutNotices::getId, outNotices.getId()));
                        }

                        //推送ES
                        String index = SgConstants.SG_B_PHY_OUT_NOTICES;
                        String type = SgConstants.SG_B_PHY_OUT_NOTICES_ITEM;
                        SgBPhyOutNotices sgBPhyOutNotices = outNoticesMapper.selectById(outNotices.getId());
                        if (log.isDebugEnabled()) {
                            log.debug(this.getClass().getName() + ",updateOutNoticesWmsStatus,sgBPhyOutNotices:"
                                    + JSON.toJSONString(sgBPhyOutNotices));
                        }
                        StorageESUtils.pushESBModelByUpdate(sgBPhyOutNotices, null, outNotices.getId(),
                                null, index, index, type, null, SgBPhyOutNotices.class, SgBPhyOutNoticesItem.class, false);
                    }
                }
            }
        } catch (Exception e) {
            log.error(this.getClass().getName() + ".error:" + e.getMessage(), e);
            v14.setCode(ResultCode.FAIL);
            v14.setMessage(e.getMessage());
            return v14;
        }

        v14.setCode(ResultCode.SUCCESS);
        v14.setMessage("成功！");
        if (log.isDebugEnabled()) {
            log.debug("Finish SgPhyOutNoticesSaveService.updateOutNoticesStatus. ReturnResult:time consuming:{}ms;"
                    , System.currentTimeMillis() - start);
        }
        return v14;
    }

}
