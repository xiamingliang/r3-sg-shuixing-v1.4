package com.jackrain.nea.sg.in.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.github.pagehelper.PageHelper;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.oc.purchase.api.OcDirectUpdateCmd;
import com.jackrain.nea.oc.purchase.api.OcPurchaseUpdateCmd;
import com.jackrain.nea.oc.purchase.model.request.OcPurchaseUpdatePickRequest;
import com.jackrain.nea.oc.purchase.model.result.OcPurchaseUpdatePickResult;
import com.jackrain.nea.oc.sale.api.OcRefundSaleUpdateCmd;
import com.jackrain.nea.oc.sale.api.OcSaleUpdateCmd;
import com.jackrain.nea.oc.sale.model.request.OcSaleUpdatePickRequest;
import com.jackrain.nea.oc.sale.model.result.OcSaleUpdatePickResult;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.basic.utils.StorageUtils;
import com.jackrain.nea.sg.in.mapper.SgBPhyInNoticesImpItemMapper;
import com.jackrain.nea.sg.in.mapper.SgBPhyInNoticesMapper;
import com.jackrain.nea.sg.in.model.request.SgPhyInNoticesImpItemSaveRequest;
import com.jackrain.nea.sg.in.model.request.SgPhyInNoticesRequest;
import com.jackrain.nea.sg.in.model.request.SgPhyInPickOrderSaveRequest;
import com.jackrain.nea.sg.in.model.request.SgPhyInPickUpGoodSaveRequest;
import com.jackrain.nea.sg.in.model.table.SgBPhyInNotices;
import com.jackrain.nea.sg.in.model.table.SgBPhyInNoticesImpItem;
import com.jackrain.nea.sg.out.common.SgOutConstantsIF;
import com.jackrain.nea.sg.transfer.api.SgTransferUpdatePickCmd;
import com.jackrain.nea.sg.transfer.model.request.OcTransferUpdatePickRequest;
import com.jackrain.nea.sg.transfer.model.result.OcTransferUpdatePickResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author zhu lin yu
 * @since 2019-11-13
 * create at : 2019-11-13 14:06
 */
@Slf4j
@Component
public class SgPhyInPickOrderGenerateService {

    /**
     * 入库通知单生成拣货单
     *
     * @param request 入库通知单id集合
     */
    @Transactional(rollbackFor = Exception.class)
    public ValueHolderV14<Long> generatePhyInPickUpGood(SgPhyInPickUpGoodSaveRequest request) {

        ValueHolderV14<Long> valueHolderV14;

        if (log.isDebugEnabled()) {
            log.debug("Start SgPhyInPickOrderGenerateService.generatePhyInPickUpGood. ReceiveParams:params:{};"
                    + JSONObject.toJSONString(request));
        }

        //参数校验
        valueHolderV14 = checkParam(request);
        if (!valueHolderV14.isOK()) {
            return valueHolderV14;
        }

        User loginUser = request.getLoginUser();

        //根据入库通知单id查询所有的入库通知单
        List<Long> inNoticeIdList = request.getInNoticeIdList();
        SgBPhyInNoticesMapper sgBPhyInNoticesMapper = ApplicationContextHandle.getBean(SgBPhyInNoticesMapper.class);
        List<SgBPhyInNotices> sgBPhyInNoticeList = sgBPhyInNoticesMapper.selectList(new QueryWrapper<SgBPhyInNotices>()
                .lambda().in(SgBPhyInNotices::getId, inNoticeIdList));
        log.info(this.getClass().getName() + " 生成拣货单的入库通知单主表参数,{}", JSONObject.toJSONString(sgBPhyInNoticeList));
        if (CollectionUtils.isEmpty(sgBPhyInNoticeList) || inNoticeIdList.size() != sgBPhyInNoticeList.size()) {
            valueHolderV14.setMessage("入库通知单不存在或数量不符合!");
            return valueHolderV14;
        }

        /**
         * 如果未选择入库通知单明细，则提示：“请选择需要拣货的数据！”；
         * 如果选择的入库通知单中存在单据状态不为待入库或部分入库，则提示：“单据为非待入库或部分入库，不允许拣货！”；
         * 如果选择的入库通知单中存在是否传WMS为是，则提示：“是否传WMS为是，不允许拣货！”；
         * 如果所选择的入库通知单的拣货状态不为未拣货，则提示：“拣货状态不为未拣货，不允许拣货！”；
         * 如果选择多张入库通知单，入库通知单实体仓、对应名称、来源单据类型（并且非零售）不一致，则提示：“入库通知单实体仓、对应名称、来源单据类型不一致，不允许拣货！”
         */
        List<Long> cpCPhyWarehouseList = new ArrayList<>();
        List<String> receiverNameList = new ArrayList<>();
        List<Integer> sourceBillTypeList = new ArrayList<>();

        for (SgBPhyInNotices sgBPhyInNotice : sgBPhyInNoticeList) {

            if (SgOutConstantsIF.OUT_NOTICES_STATUS_WAIT != sgBPhyInNotice.getBillStatus()
                    && SgOutConstantsIF.OUT_NOTICES_STATUS_PART != sgBPhyInNotice.getBillStatus()) {
                AssertUtils.logAndThrow(" 存在全部入库的记录，不允许拣货!入库通知单为：" + sgBPhyInNotice.getBillNo());

            } else if (SgOutConstantsIF.IS_PASS_WMS_Y == sgBPhyInNotice.getIsPassWms()) {
                AssertUtils.logAndThrow(" 存在传WMS的记录，不允许拣货!出库通知单为：" + sgBPhyInNotice.getBillNo());

            } else if (SgOutConstantsIF.PICK_STATUS_SUCCESS == sgBPhyInNotice.getPickStatus()
                    || SgOutConstantsIF.PICK_STATUS_ING == sgBPhyInNotice.getPickStatus()) {
                AssertUtils.logAndThrow(" 存在拣货中或拣货完成的记录，不允许拣货!入库通知单为：" + sgBPhyInNotice.getBillNo());

            }

            //判断多张入库通知单，出库通知单实体仓、收货方名称、来源单据类型（非零售出库类型）是否一致
            if (!cpCPhyWarehouseList.contains(sgBPhyInNotice.getCpCPhyWarehouseId())) {
                cpCPhyWarehouseList.add(sgBPhyInNotice.getCpCPhyWarehouseId());
            }
            // 20191226 zhoulinsheng -----start水星托运拣货是可能存在多个不同的实体仓
//            if (!receiverNameList.contains(sgBPhyInNotice.getCpCCsEcode())) {
//                receiverNameList.add(sgBPhyInNotice.getCpCCsEcode());
//            }
            // 20191226 zhoulinsheng -----end水星托运拣货是可能存在多个不同的实体仓

            if (SgConstantsIF.BILL_TYPE_RETAIL != sgBPhyInNotice.getSourceBillType() && !sourceBillTypeList.contains(sgBPhyInNotice.getSourceBillType())) {
                sourceBillTypeList.add(sgBPhyInNotice.getSourceBillType());
            }
        }

        if (cpCPhyWarehouseList.size() > 1 || receiverNameList.size() > 1 || sourceBillTypeList.size() > 1) {
            AssertUtils.logAndThrow(" 入库通知单实体仓、对应名称、来源单据类型不一致，不允许拣货!");
        }


        SgBPhyInNoticesImpItemMapper inNoticesImpItemMapper = ApplicationContextHandle.getBean(SgBPhyInNoticesImpItemMapper.class);
        Integer totalQtty = inNoticesImpItemMapper.selectCount(new QueryWrapper<SgBPhyInNoticesImpItem>()
                .lambda().in(SgBPhyInNoticesImpItem::getSgBPhyInNoticesId, inNoticeIdList)
                .gt(SgBPhyInNoticesImpItem::getQtyDiff, BigDecimal.ZERO));


        //分页处理
        List<SgBPhyInNoticesImpItem> allInNoticesImpItem = new ArrayList<>();
        int pageSize = SgConstants.SG_NORMAL_MAX_QUERY_PAGE_SIZE;
        int listSize = totalQtty;
        int page = listSize / pageSize;

        if (listSize % pageSize != 0) {
            page++;
        }

        //分页批量查询
        for (int i = 0; i < page; i++) {

            PageHelper.startPage(i + 1, pageSize);
            List<SgBPhyInNoticesImpItem> inNoticesImpItemList = inNoticesImpItemMapper.selectList(new QueryWrapper<SgBPhyInNoticesImpItem>()
                    .lambda().in(SgBPhyInNoticesImpItem::getSgBPhyInNoticesId, inNoticeIdList)
                    .gt(SgBPhyInNoticesImpItem::getQtyDiff, BigDecimal.ZERO));

            if (CollectionUtils.isNotEmpty(inNoticesImpItemList)) {
                allInNoticesImpItem.addAll(inNoticesImpItemList);
                log.info(this.getClass().getName() + " 第{}批,生成拣货单的拣货明细参数,{}", i + 1, JSONObject.toJSONString(inNoticesImpItemList));
            }
        }


        /**
         * 生成拣货单
         */
        SgPhyInPickOrderSaveService saveService = ApplicationContextHandle.getBean(SgPhyInPickOrderSaveService.class);

        //生成入库拣货单主表信息
        List<SgPhyInNoticesRequest> sgPhyInNoticesSaveRequests = new ArrayList<>();
        for (SgBPhyInNotices sgBPhyInNotices : sgBPhyInNoticeList) {
            SgPhyInNoticesRequest saveRequest = new SgPhyInNoticesRequest();
            BeanUtils.copyProperties(sgBPhyInNotices, saveRequest);
            sgPhyInNoticesSaveRequests.add(saveRequest);
        }

        SgPhyInPickOrderSaveRequest pickOrderRequest = new SgPhyInPickOrderSaveRequest();
        pickOrderRequest.setInNoticesList(sgPhyInNoticesSaveRequests);
        pickOrderRequest.setLoginUser(loginUser);

        // 2020-04-03添加逻辑：同级调拨参数
        suppTransferOutParam(pickOrderRequest, request);

        // 2020-04-15添加逻辑：入库序号
        pickOrderRequest.setInSeqNo(request.getInSeqNo());
        ValueHolderV14<Long> holderV14 = saveService.saveSgbBInPickorder(pickOrderRequest);
        AssertUtils.isTrue(holderV14.isOK(), "新增拣货单主表信息失败");
        Long sgBInPickOrderId = holderV14.getData();

        //生成拣货明细信息
        SgPhyInPickOrderSaveRequest pickOrderItemRequest = new SgPhyInPickOrderSaveRequest();

        // 2020-04-03添加逻辑：同级调拨参数
        suppTransferOutParam(pickOrderItemRequest, request);
        pickOrderItemRequest.setSgPhyBInPickorderId(sgBInPickOrderId);
        List<SgPhyInNoticesImpItemSaveRequest> requests = new ArrayList<>();
        for (SgBPhyInNoticesImpItem sgBPhyInNoticesImpItem : allInNoticesImpItem) {
            SgPhyInNoticesImpItemSaveRequest impItemSaveRequest = new SgPhyInNoticesImpItemSaveRequest();
            BeanUtils.copyProperties(sgBPhyInNoticesImpItem, impItemSaveRequest);
            requests.add(impItemSaveRequest);
        }
        pickOrderItemRequest.setInNoticesImpItemList(requests);
        pickOrderItemRequest.setLoginUser(loginUser);
        saveService.saveSgbBInPickorderItem(pickOrderItemRequest);

        //生成来源出库通知单信息
        SgPhyInPickOrderSaveRequest pickOrderNoticeItemRequest = new SgPhyInPickOrderSaveRequest();
        pickOrderNoticeItemRequest.setSgPhyBInPickorderId(sgBInPickOrderId);
        pickOrderNoticeItemRequest.setInNoticesList(sgPhyInNoticesSaveRequests);
        pickOrderNoticeItemRequest.setLoginUser(loginUser);
        saveService.saveSgbBInPickorderNoticeItem(pickOrderNoticeItemRequest);

        /**
         * 更新对应的入库通知单的拣货状态为拣货中
         */
        SgBPhyInNotices updatePhyOutNotice = new SgBPhyInNotices();
        updatePhyOutNotice.setPickStatus(SgOutConstantsIF.PICK_STATUS_ING);
        sgBPhyInNoticesMapper.update(updatePhyOutNotice, new UpdateWrapper<SgBPhyInNotices>()
                .lambda().in(SgBPhyInNotices::getId, inNoticeIdList));


        /**
         * 根据对应的入库通知单的来源单据编号+来源单据类型（调拨单、采购单、直发单、销售单、销售退货单）更新来源单据“入库拣货状态”为拣货中；
         */

        Map<Integer, List<SgBPhyInNotices>> collect = sgBPhyInNoticeList.stream().collect(Collectors.groupingBy(SgBPhyInNotices::getSourceBillType));
        try {
            updateSourceBillPickOrderStatus(collect, SgOutConstantsIF.PICK_STATUS_ING, loginUser);
        } catch (Exception e) {
            log.error(this.getClass().getName() + " 入库拣货单更新上游单据拣货状态失败,拣货单id为{},错误信息:{}", sgBInPickOrderId, StorageUtils.getExceptionMsg(e));
        }


        valueHolderV14.setCode(ResultCode.SUCCESS);
        valueHolderV14.setMessage("拣货单生成成功");
        valueHolderV14.setData(sgBInPickOrderId);

        if (log.isDebugEnabled()) {
            log.debug("Finish SgPhyInPickOrderGenerateService.generatePhyInPickUpGood. ResultParams:params:{};"
                    + JSONObject.toJSONString(valueHolderV14));
        }

        return valueHolderV14;
    }


    /**
     * 参数校验
     */
    private ValueHolderV14<Long> checkParam(SgPhyInPickUpGoodSaveRequest request) {

        ValueHolderV14<Long> valueHolderV14 = new ValueHolderV14<>(ResultCode.FAIL, "");

        if (request == null) {
            valueHolderV14.setMessage("request is null");
            return valueHolderV14;

        } else if (CollectionUtils.isEmpty(request.getInNoticeIdList())) {
            valueHolderV14.setMessage("请选择需要拣货的数据！");
            return valueHolderV14;
        } else if (request.getLoginUser() == null) {
            valueHolderV14.setMessage("用户信息不能为空！");
            return valueHolderV14;
        }

        valueHolderV14.setCode(ResultCode.SUCCESS);
        return valueHolderV14;
    }

    /**
     * 拣货单更改上游单据状态
     */
    private void updateSourceBillPickOrderStatus(Map<Integer, List<SgBPhyInNotices>> map, Integer pickUpStatus, User user) {
        int error = 0;

        for (Integer sourceBillTyte : map.keySet()) {
            try {
                switch (sourceBillTyte) {
                    case SgConstantsIF.BILL_TYPE_PUR:
                        //采购单
                        OcPurchaseUpdateCmd pubUpdateCmd = (OcPurchaseUpdateCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                                OcPurchaseUpdateCmd.class.getName(), "oc", "1.0");
                        OcPurchaseUpdatePickRequest pubUpdatePickRequest = new OcPurchaseUpdatePickRequest();
                        List<SgBPhyInNotices> sgBPhyInNotices1 = map.get(sourceBillTyte);
                        List<Long> pubId = sgBPhyInNotices1.stream().map(SgBPhyInNotices::getSourceBillId).collect(Collectors.toList());
                        pubUpdatePickRequest.setIds(pubId);
                        pubUpdatePickRequest.setInPickStatus(pickUpStatus);
                        pubUpdatePickRequest.setUser(user);
                        ValueHolderV14<List<OcPurchaseUpdatePickResult>> pubHolderV14 = pubUpdateCmd.batchUpdatePurchasePickStatus(pubUpdatePickRequest);
                        if (!pubHolderV14.isOK()) {
                            error++;
                            log.error(" 更新采购单失败,{}", JSONObject.toJSONString(pubHolderV14));
                        }
                        break;
                    case SgConstantsIF.BILL_TYPE_DIRECT:
                        //直发单
                        OcDirectUpdateCmd directUpdateCmd = (OcDirectUpdateCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                                OcDirectUpdateCmd.class.getName(), "oc", "1.0");
                        OcPurchaseUpdatePickRequest directUpdatePickRequest = new OcPurchaseUpdatePickRequest();
                        List<SgBPhyInNotices> directInNotices = map.get(sourceBillTyte);
                        List<Long> pubRefId = directInNotices.stream().map(SgBPhyInNotices::getSourceBillId).collect(Collectors.toList());
                        directUpdatePickRequest.setIds(pubRefId);
                        directUpdatePickRequest.setInPickStatus(pickUpStatus);
                        directUpdatePickRequest.setUser(user);
                        ValueHolderV14<List<OcPurchaseUpdatePickResult>> pubRefHolderV14 = directUpdateCmd.batchUpdateDirectPickStatus(directUpdatePickRequest);
                        if (!pubRefHolderV14.isOK()) {
                            error++;
                            log.error(" 更新直发单失败,{}" + JSONObject.toJSONString(pubRefHolderV14));
                        }
                        break;
                    case SgConstantsIF.BILL_TYPE_SALE:
                        //销售单
                        OcSaleUpdateCmd saleUpdateCmd = (OcSaleUpdateCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                                OcSaleUpdateCmd.class.getName(), "oc", "1.0");
                        OcSaleUpdatePickRequest salePickRequest = new OcSaleUpdatePickRequest();
                        List<SgBPhyInNotices> sgBPhyInNotices3 = map.get(sourceBillTyte);
                        List<Long> saleId = sgBPhyInNotices3.stream().map(SgBPhyInNotices::getSourceBillId).collect(Collectors.toList());
                        salePickRequest.setIds(saleId);
                        salePickRequest.setInPickStatus(pickUpStatus);
                        salePickRequest.setUser(user);
                        ValueHolderV14<List<OcSaleUpdatePickResult>> saleHolderV14 = saleUpdateCmd.batchUpdateSalePickStatus(salePickRequest);
                        if (!saleHolderV14.isOK()) {
                            error++;
                            log.error(" 更新销售单失败,{}" + JSONObject.toJSONString(saleHolderV14));
                        }
                        break;
                    case SgConstantsIF.BILL_TYPE_SALE_REF:
                        //销售退货单
                        OcRefundSaleUpdateCmd saleRefUpdateCmd = (OcRefundSaleUpdateCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                                OcRefundSaleUpdateCmd.class.getName(), "oc", "1.0");
                        OcSaleUpdatePickRequest ocSaleUpdatePickRequest = new OcSaleUpdatePickRequest();
                        List<SgBPhyInNotices> sgBPhyInNotices4 = map.get(sourceBillTyte);
                        List<Long> saleRefId = sgBPhyInNotices4.stream().map(SgBPhyInNotices::getSourceBillId).collect(Collectors.toList());
                        ocSaleUpdatePickRequest.setIds(saleRefId);
                        ocSaleUpdatePickRequest.setInPickStatus(pickUpStatus);
                        ocSaleUpdatePickRequest.setUser(user);
                        ValueHolderV14<List<OcSaleUpdatePickResult>> saleRefHolderV14 = saleRefUpdateCmd.batchUpdateRefSalePickStatus(ocSaleUpdatePickRequest);
                        if (!saleRefHolderV14.isOK()) {
                            error++;
                            log.error(" 更新销售退货单失败,{}" + JSONObject.toJSONString(saleRefHolderV14));
                        }
                        break;
                    case SgConstantsIF.BILL_TYPE_TRANSFER:
                        //调拨单
                        SgTransferUpdatePickCmd tranUpdateCmd = (SgTransferUpdatePickCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                                SgTransferUpdatePickCmd.class.getName(), "sg", "1.0");
                        OcTransferUpdatePickRequest tranUpdatePickRequest = new OcTransferUpdatePickRequest();
                        List<SgBPhyInNotices> tranPhyOutNotices = map.get(sourceBillTyte);
                        List<Long> transId = tranPhyOutNotices.stream().map(SgBPhyInNotices::getSourceBillId).collect(Collectors.toList());
                        tranUpdatePickRequest.setIds(transId);
                        tranUpdatePickRequest.setInPickStatus(pickUpStatus);
                        tranUpdatePickRequest.setUser(user);
                        ValueHolderV14<List<OcTransferUpdatePickResult>> transferHolderV14 = tranUpdateCmd.batchUpdateTransferPickStatus(tranUpdatePickRequest);
                        if (!transferHolderV14.isOK()) {
                            error++;
                            log.error(" 更新调拨单失败,{}", JSONObject.toJSONString(transferHolderV14));
                        }
                        break;
                    default:
                        throw new NDSException(Resources.getMessage("SourceBillType Exception:" + sourceBillTyte));
                }
            } catch (NDSException e) {
                error++;
            }
        }

        AssertUtils.isTrue(error == 0, " 入库拣货单更新上游单据拣货状态失败");

    }

    /**
     * 补充调拨出库单 同级调拨参数
     *
     * @param pickOrder 拣货单对象
     * @param request   调拨出库入参
     */
    private void suppTransferOutParam(SgPhyInPickOrderSaveRequest pickOrder, SgPhyInPickUpGoodSaveRequest request) {

        if (pickOrder != null && request != null) {
            pickOrder.setIsTransferOut(request.getIsTransferOut());
            pickOrder.setBelongsLegal(request.getBelongsLegal());
            pickOrder.setSourceBillProp(request.getSourceBillProp());
        }
    }
}
