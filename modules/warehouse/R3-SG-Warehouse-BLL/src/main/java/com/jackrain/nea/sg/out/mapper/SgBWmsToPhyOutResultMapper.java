package com.jackrain.nea.sg.out.mapper;

import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.out.common.SgOutConstants;
import com.jackrain.nea.sg.out.common.SgOutConstantsIF;
import com.jackrain.nea.sg.out.model.table.SgBWmsToPhyOutResult;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface SgBWmsToPhyOutResultMapper extends ExtentionMapper<SgBWmsToPhyOutResult> {

    @Select("SELECT * FROM sg_b_wms_to_phy_out_result WHERE out_type = " + SgOutConstantsIF.OUT_TYPE_ELECTRICITY +
            " AND status IN (${status}) AND wms_failed_count < 5 ORDER BY status limit ${cnt} offset 0")
    List<SgBWmsToPhyOutResult> selectListByWms(@Param("status") String status, @Param("cnt") Integer cnt);

    @Update("UPDATE sg_b_wms_to_phy_out_result\n" +
            "SET status = " + SgOutConstants.WMS_TO_PHY_RESULT_STATUS_FAILED + ",\n" +
            " wms_failed_count = wms_failed_count + 1,\n" +
            " wms_failed_reason = '${failedReason}'\n" +
            "WHERE\n" +
            "\tnotices_bill_no IN (${billnos});")
    int batchUpdateFailedRecord(@Param("failedReason") String failedReason, @Param("billnos") String billnos);

    @Update("UPDATE sg_b_wms_to_phy_out_result\n" +
            "SET is_auto_audit = '${flag}',\n" +
            " auto_audit_cnt = auto_audit_cnt + 1,\n" +
            " auto_audit_failed_reason = '${failedReason}'\n" +
            "WHERE\n" +
            "\tnotices_bill_no IN (${billnos});")
    int batchUpdateAutoAuditRecord(@Param("failedReason") String failedReason, @Param("billnos") String billnos, @Param("flag") String flag);
}