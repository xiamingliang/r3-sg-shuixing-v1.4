package com.jackrain.nea.sg.in.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.in.mapper.SgBPhyInNoticesMapper;
import com.jackrain.nea.sg.in.mapper.SgBPhyInResultMapper;
import com.jackrain.nea.sg.in.model.request.SgPhyInNoticesQueryRequest;
import com.jackrain.nea.sg.in.model.table.SgBPhyInNotices;
import com.jackrain.nea.sg.in.model.table.SgBPhyInResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author 周琳胜
 * @since 2019-10-23
 * create at : 2019-10-23 19:52
 */
@Slf4j
@Component
public class SgPhyInNoticesByConditionQueryService {

    public ValueHolderV14<List<SgBPhyInNotices>> queryInNotices(SgPhyInNoticesQueryRequest request) {
        ValueHolderV14<List<SgBPhyInNotices>> valueHolderV14 = new ValueHolderV14<>();

        if (log.isDebugEnabled()) {
            log.debug("Start SgPhyInNoticesByConditionQueryService.queryOutNotices. ReceiveParams:requests:{}"
                    , JSONObject.toJSONString(request));
        }

        PageHelper.startPage(request.getPageNum(), request.getPageSize());

        SgBPhyInNoticesMapper sgBPhyInNoticesMapper = ApplicationContextHandle.getBean(SgBPhyInNoticesMapper.class);
        LambdaQueryWrapper<SgBPhyInNotices> lambda = new QueryWrapper<SgBPhyInNotices>().lambda()
                .eq(request.getSourceBillType() != null,SgBPhyInNotices::getSourceBillType,request.getSourceBillType())
                .in(CollectionUtils.isNotEmpty(request.getBillStatusList()),SgBPhyInNotices::getBillStatus,request.getBillStatusList())
                .eq(request.getCpCPhyWarehouseId() != null,SgBPhyInNotices::getCpCPhyWarehouseId,request.getCpCPhyWarehouseId())
                .like(StringUtils.isNotEmpty(request.getBillNo()), SgBPhyInNotices::getBillNo,request.getBillNo());

        if (request.getStartTime() != null) {
            if (request.getEndTime() == null) {
                lambda.gt(SgBPhyInNotices::getModifieddate, request.getStartTime());
            } else {
                lambda.between(SgBPhyInNotices::getModifieddate, request.getStartTime(), request.getEndTime());
            }
        }
        lambda.orderByDesc(SgBPhyInNotices::getModifieddate);
        List<SgBPhyInNotices> sgBPhyInNoticesList = sgBPhyInNoticesMapper.selectList(lambda);
        if (CollectionUtils.isEmpty(sgBPhyInNoticesList)) {
            valueHolderV14.setCode(ResultCode.SUCCESS);
            valueHolderV14.setMessage("无更多数据！");
            return valueHolderV14;
        }

        List<Long> noticeIds = sgBPhyInNoticesList.stream().map(SgBPhyInNotices::getId)
                .collect(Collectors.toList());
        SgBPhyInResultMapper sgBPhyInResultMapper = ApplicationContextHandle.getBean(SgBPhyInResultMapper.class);
        Map<Long, List<SgBPhyInResult>> resultMap = sgBPhyInResultMapper.selectList(new QueryWrapper<SgBPhyInResult>().lambda()
                .in(SgBPhyInResult::getSgBPhyInNoticesId, noticeIds)
                .eq(SgBPhyInResult::getIsLast, SgConstants.IS_LAST_YES)
                .eq(SgBPhyInResult::getIsactive, SgConstants.IS_ACTIVE_Y)
                .orderByDesc(SgBPhyInResult::getModifieddate))
                .stream().collect(Collectors.groupingBy(SgBPhyInResult::getSgBPhyInNoticesId));

        Iterator<SgBPhyInNotices> noticesIterator = sgBPhyInNoticesList.iterator();
        while (noticesIterator.hasNext()) {
            SgBPhyInNotices sgBPhyInNotices = noticesIterator.next();
            List<SgBPhyInResult> sgBPhyInResults = resultMap.get(sgBPhyInNotices.getId());
            if (CollectionUtils.isNotEmpty(sgBPhyInResults)) {
                noticesIterator.remove();
            }
        }
        PageInfo<SgBPhyInNotices> resultPage = new PageInfo<>(sgBPhyInNoticesList);
        List<SgBPhyInNotices> list = resultPage.getList();
        valueHolderV14.setCode(ResultCode.SUCCESS);
        valueHolderV14.setMessage("查询成功");
        valueHolderV14.setData(list);

        if (log.isDebugEnabled()) {
            log.debug("Finsh SgPhyInNoticesByConditionQueryService.queryOutNotices. Result:valueHolderV14:{},spend time:{}ms"
                    , JSONObject.toJSONString(valueHolderV14));
        }
        return valueHolderV14;
    }
}
