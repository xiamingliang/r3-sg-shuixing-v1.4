package com.jackrain.nea.sg.out.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.redis.config.CusRedisTemplate;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.out.mapper.SgBPhyOutNoticesMapper;
import com.jackrain.nea.sg.out.model.request.SgPhyOneClickOutLibraryRequest;
import com.jackrain.nea.sg.out.model.request.SgPhyOutResultBillSaveRequest;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutNotices;
import com.jackrain.nea.sg.send.utils.SgStoreUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @author: 舒威
 * @since: 2019/9/10
 * create at : 2019/9/10 11:17
 */
@Slf4j
@Component
public class SgPhyOneClickOutLibraryService {

    public ValueHolderV14 oneClickOutLibrary(SgPhyOneClickOutLibraryRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("Start SgPhyOneClickOutLibraryService.oneClickOutLibrary. ReceiveParams:request={};", JSONObject.toJSONString(request));
        }
        long start = System.currentTimeMillis();

        if (request == null || request.getSourceBillId() == null || request.getSourceBillType() == null || StringUtils.isEmpty(request.getLockKey())) {
            AssertUtils.logAndThrow("参数不能为空!");
        }

        if (request.getLoginUser() == null) {
            AssertUtils.logAndThrow("请先登录!");
        }

        ValueHolderV14 result = outLibrary(request);
        if (log.isDebugEnabled()) {
            log.debug("Finish SgPhyOneClickOutLibraryService.oneClickOutLibrary. ReceiveParams:result={},spend time={};",
                    JSONObject.toJSONString(result), System.currentTimeMillis() - start);
        }
        return result;
    }

    public ValueHolderV14 outLibrary(SgPhyOneClickOutLibraryRequest request) {
        ValueHolderV14 result = new ValueHolderV14();
        CusRedisTemplate<String, String> redisTemplate = SgStoreUtils.initRedisTemplate();
        String lockKsy = request.getLockKey();
        Long sourceBillId = request.getSourceBillId();
        Integer sourceBillType = request.getSourceBillType();
        User loginUser = request.getLoginUser();
        if (redisTemplate.opsForValue().get(lockKsy) == null) {
            try {
                redisTemplate.opsForValue().set(lockKsy, "OK", 60, TimeUnit.MINUTES);
                SgBPhyOutNoticesMapper outNoticesMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesMapper.class);
                List<SgBPhyOutNotices> notices = outNoticesMapper.selectList(new QueryWrapper<SgBPhyOutNotices>().lambda()
                        .eq(SgBPhyOutNotices::getSourceBillId, sourceBillId)
                        .eq(SgBPhyOutNotices::getSourceBillType, sourceBillType)
                        .eq(SgBPhyOutNotices::getIsactive, SgConstants.IS_ACTIVE_Y));
                if (CollectionUtils.isEmpty(notices)) {
                    AssertUtils.logAndThrow("不存在来源单据id[" + sourceBillId + "],来源单据类型[" + sourceBillType + "]的出库通知单!");
                }
                List<Long> ids = notices.stream().map(SgBPhyOutNotices::getId).collect(Collectors.toList());
                //新增并审核结果单
                SgPhyOneClickOutLibraryService clickOutLibraryService = ApplicationContextHandle.getBean(SgPhyOneClickOutLibraryService.class);
                clickOutLibraryService.saveAndAuditOutResult(ids, result, loginUser);
            } catch (Exception e) {
                e.printStackTrace();
                result.setCode(ResultCode.FAIL);
                result.setMessage(e.getMessage());
            } finally {
                try {
                    redisTemplate.delete(lockKsy);
                } catch (Exception e) {
                    e.printStackTrace();
                    log.error("一键出库释放锁失败,lockKsy:" + lockKsy + "，请联系管理员！");
                    result.setCode(ResultCode.FAIL);
                    result.setMessage("一键出库释放锁失败,lockKsy:" + lockKsy + "，请联系管理员！" + e.getMessage());
                }
            }
        }
        return result;
    }

    @Transactional(rollbackFor = Exception.class)
    public void saveAndAuditOutResult(List<Long> ids, ValueHolderV14 result, User loginUser) {
        SgPhyOutRPCService rpcService = ApplicationContextHandle.getBean(SgPhyOutRPCService.class);
        SgPhyOutResultSaveAndAuditService saveAndAuditService = ApplicationContextHandle.getBean(SgPhyOutResultSaveAndAuditService.class);
        for (Long id : ids) {
            ValueHolderV14<SgPhyOutResultBillSaveRequest> ret = rpcService.getSgPhyOutResultRequest(id, loginUser);
            if (!ret.isOK()) {
                AssertUtils.logAndThrow("获取新增出库结果单参数失败!" + ret.getMessage());
            }
            SgPhyOutResultBillSaveRequest request = ret.getData();
            if (request != null) {
                request.setIsOneClickOutLibrary(true);
            }
            ValueHolderV14 resultRet = saveAndAuditService.saveOutResultAndAudit(request);
            if (log.isDebugEnabled()) {
                log.debug("一键出库新增并审核出库结果单出参:{}", JSONObject.toJSONString(resultRet));
            }
            if (resultRet.isOK()) {
                result.setCode(ResultCode.SUCCESS);
                result.setMessage("一键出库成功");
            } else {
                AssertUtils.logAndThrow("新增并审核出库结果单失败!" + resultRet.getMessage());
            }
        }
    }
}
