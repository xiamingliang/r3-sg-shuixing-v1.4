package com.jackrain.nea.sg.pack.validate;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.cp.result.CpCStore;
import com.jackrain.nea.cpext.api.GeneralOrganizationCmd;
import com.jackrain.nea.cpext.model.table.CpCPhyWarehouse;
import com.jackrain.nea.data.basic.model.request.StoreInfoQueryRequest;
import com.jackrain.nea.data.basic.services.BasicCpQueryService;
import com.jackrain.nea.sg.pack.common.SgTeusPackConstants;
import com.jackrain.nea.sg.pack.mapper.SgBTeusPackMapper;
import com.jackrain.nea.sg.pack.model.table.SgBTeusPack;
import com.jackrain.nea.tableService.DbRowAction;
import com.jackrain.nea.tableService.MainTableRecord;
import com.jackrain.nea.tableService.TableServiceContext;
import com.jackrain.nea.tableService.validate.BaseValidator;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.utils.AssertUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * @author zhu lin yu
 * @since 2019-12-03
 * create at : 2019-12-03 19:27
 */
@Slf4j
@Component
public class SgTeusPackSaveValidate extends BaseValidator {

    @Override
    public void validate(TableServiceContext context, MainTableRecord currentRow) {
        Long objId = currentRow.getId();
        AssertUtils.notNull(objId, "拆箱单ID为空！", context.getLocale());

        if (currentRow.getAction().equals(DbRowAction.INSERT)) {
            JSONObject commitData = currentRow.getCommitData();
            // 单据日期
            Date billDate = commitData.getDate("BILL_DATE");
            AssertUtils.notNull(billDate, "单据日期不能为空！", context.getLocale());

            // 实体店仓
            Long cpCPhyWarehouseId = commitData.getLong("CP_C_PHY_WAREHOUSE_ID");
            AssertUtils.notNull(cpCPhyWarehouseId, "实体店仓id不能为空！", context.getLocale());

            //查询实体仓信息
            GeneralOrganizationCmd generalOrganizationCmd = (GeneralOrganizationCmd)
                    ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                            GeneralOrganizationCmd.class.getName(), "cp-ext", "1.0");
            CpCPhyWarehouse cpCPhyWarehouse = generalOrganizationCmd.queryByWarehouseId(cpCPhyWarehouseId);
            AssertUtils.notNull(cpCPhyWarehouse, "未查到该实体仓信息！", context.getLocale());


            commitData.put("CP_C_PHY_WAREHOUSE_ECODE", cpCPhyWarehouse.getEcode());
            commitData.put("CP_C_PHY_WAREHOUSE_ENAME", cpCPhyWarehouse.getEname());

            // 封装逻辑仓信息
            Long cpCStoreId = commitData.getLong("CP_C_STORE_ID");
            if (cpCStoreId != null) {
                BasicCpQueryService queryService = ApplicationContextHandle.getBean(BasicCpQueryService.class);
                StoreInfoQueryRequest storeInfoQueryRequest = new StoreInfoQueryRequest();
                List<Long> ids = new ArrayList<>();
                ids.add(cpCStoreId);
                storeInfoQueryRequest.setIds(ids);
                HashMap<Long, CpCStore> storeInfo = new HashMap<>();

                try {
                    storeInfo = queryService.getStoreInfo(storeInfoQueryRequest);
                } catch (Exception e) {
                    AssertUtils.logAndThrow("查询逻辑店仓信息异常！", context.getLocale());
                }

                // 补充逻辑仓信息
                CpCStore cpCStore = storeInfo.get(cpCStoreId);
                commitData.put("CP_C_STORE_ECODE", cpCStore.getEcode());
                commitData.put("CP_C_STORE_ENAME", cpCStore.getEname());
            }

        } else if (currentRow.getAction().equals(DbRowAction.UPDATE)) {
            SgBTeusPackMapper packMapper = ApplicationContextHandle.getBean(SgBTeusPackMapper.class);
            SgBTeusPack sgBTeusPack = packMapper.selectById(objId);

            AssertUtils.notNull(sgBTeusPack, "当前记录已不存在！", context.getLocale());
            Integer status = sgBTeusPack.getStatus();

            if (SgTeusPackConstants.BILL_STATUS_AUDIT == status) {
                AssertUtils.logAndThrow("当前记录已审核，不允许编辑！", context.getLocale());
            } else if (SgTeusPackConstants.BILL_STATUS_VOID == status) {
                AssertUtils.logAndThrow("当前记录已作废，不允许编辑！", context.getLocale());
            }
        }

    }
}
