package com.jackrain.nea.sg.out.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.out.common.SgOutConstants;
import com.jackrain.nea.sg.out.common.SgOutConstantsIF;
import com.jackrain.nea.sg.out.mapper.*;
import com.jackrain.nea.sg.out.model.request.SgPhyOutBillBaseRequest;
import com.jackrain.nea.sg.out.model.request.SgPhyOutBillQueryRequest;
import com.jackrain.nea.sg.out.model.request.SgPhyOutNoticesQueryRequest;
import com.jackrain.nea.sg.out.model.result.SgBPhyOutNoticesAllInfoResult;
import com.jackrain.nea.sg.out.model.result.SgOutNoticesByBillNoResult;
import com.jackrain.nea.sg.out.model.result.SgOutQueryResult;
import com.jackrain.nea.sg.out.model.result.SgOutResultResult;
import com.jackrain.nea.sg.out.model.table.*;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author leexh
 * @since 2019/5/30 18:42
 * desc:批量查询出库通知单/结果单
 */
@Slf4j
@Component
public class SgPhyOutQueryService {

    public ValueHolderV14<List<SgOutQueryResult>> queryOutBySource(SgPhyOutBillQueryRequest request) {

        ValueHolderV14<List<SgOutQueryResult>> v14 = new ValueHolderV14<>();
        v14.setCode(ResultCode.FAIL);

        if (request == null) {
            v14.setMessage("参数不能为空！");
            return v14;
        }

        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ".debug, queryOutBySource-request:" + JSONObject.toJSONString(request));
        }

        Boolean isReturnItem = request.getIsReturnItem() != null ? request.getIsReturnItem() : false;
        int returnData = request.getReturnData() != null ? request.getReturnData() : SgOutConstantsIF.RETURN_OUT_NOTICES;

        List<SgOutQueryResult> data;
        List<SgPhyOutBillBaseRequest> baseRequests = request.getBaseRequests();
        if (!CollectionUtils.isEmpty(baseRequests)) {

            data = new ArrayList<>();
            for (SgPhyOutBillBaseRequest baseRequest : baseRequests) {
                Long sourceBillId = baseRequest.getSourceBillId();
                Integer sourceBillType = baseRequest.getSourceBillType();
                if (sourceBillType != null && sourceBillId != null) {
                    SgOutQueryResult queryResult = queryOutBySourceSingle(baseRequest, isReturnItem, returnData);
                    if (queryResult != null) {
                        data.add(queryResult);
                    }
                }
            }
        } else {
            v14.setMessage("来源单据信息不能为空！");
            return v14;
        }

        v14.setCode(ResultCode.SUCCESS);
        v14.setMessage("查询成功！");
        if (data.size() > 0) {
            v14.setData(data);
        } else {
            v14.setData(null);
        }

        return v14;
    }

    /**
     * 根据来源单据信息查询通知单/结果单
     *
     * @param request      来源单据信息
     * @param isReturnItem 是否返回明细信息
     * @param returnData   通知单/结果单/全部
     * @return 返回
     */
    public SgOutQueryResult queryOutBySourceSingle(SgPhyOutBillBaseRequest request, Boolean isReturnItem, int returnData) {
        SgOutQueryResult result = null;

        if (returnData == SgOutConstantsIF.RETURN_OUT_RESULT) {
            // 只要结果单的情况
            List<SgOutResultResult> resultList = queryOutNoticesByNotices(request, null, isReturnItem);
            if (!CollectionUtils.isEmpty(resultList)) {
                result = new SgOutQueryResult();
                result.setResults(resultList);
                result.setOutResultNum(resultList.size());
            }

        } else if (returnData == SgOutConstantsIF.RETURN_OUT_ALL) {
            // 既要通知单又要结果单
            SgOutNoticesByBillNoResult billNoResult = queryOutNoticesBySource(request, isReturnItem);
            if (billNoResult != null && billNoResult.getOutNotices() != null) {
                result = new SgOutQueryResult();
                result.setNotices(billNoResult);
                Long noticesId = billNoResult.getOutNotices().getId();
                List<SgOutResultResult> resultList = queryOutNoticesByNotices(null, noticesId, isReturnItem);
                if (!CollectionUtils.isEmpty(resultList)) {
                    result.setResults(resultList);
                    result.setOutResultNum(resultList.size());
                }
            }
        } else {
            // 只要通知单的情况(默认)
            SgOutNoticesByBillNoResult billNoResult = queryOutNoticesBySource(request, isReturnItem);
            if (billNoResult != null) {
                result = new SgOutQueryResult();
                result.setNotices(billNoResult);
            }
        }
        return result;
    }

    /**
     * 查询出库通知单
     *
     * @param request      来源单据信息
     * @param isReturnItem 是否返回明细信息
     * @return 返回
     */
    public SgOutNoticesByBillNoResult queryOutNoticesBySource(SgPhyOutBillBaseRequest request, Boolean isReturnItem) {
        SgOutNoticesByBillNoResult result = null;

        Long sourceBillId = request.getSourceBillId();
        Integer sourceBillType = request.getSourceBillType();
        if (sourceBillId != null && sourceBillType != null) {
            SgBPhyOutNoticesMapper noticesMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesMapper.class);
            SgBPhyOutNotices notices = noticesMapper.selectOne(new QueryWrapper<SgBPhyOutNotices>()
                    .eq(SgOutConstants.SOURCE_BILL_ID, sourceBillId)
                    .eq(SgOutConstants.SOURCE_BILL_TYPE, sourceBillType)
                    .eq(SgOutConstants.IS_ACTIVE, SgConstants.IS_ACTIVE_Y));

            if (notices != null) {
                result = new SgOutNoticesByBillNoResult();
                result.setOutNotices(notices);
                if (isReturnItem) {
                    Long objId = notices.getId();
                    SgBPhyOutNoticesItemMapper itemMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesItemMapper.class);
                    result.setItems(itemMapper.selectList(new QueryWrapper<SgBPhyOutNoticesItem>()
                            .eq(SgOutConstants.OUT_NOTICES_PAREN_FIELD, objId)));
                }
            }
        }
        return result;
    }

    /**
     * 查询结果单信息
     *
     * @param noticesId    通知单ID
     * @param isReturnItem 是否返回明细信息
     * @return 返回
     */
    public List<SgOutResultResult> queryOutNoticesByNotices(SgPhyOutBillBaseRequest request, Long noticesId, Boolean isReturnItem) {
        List<SgOutResultResult> results = new ArrayList<>();

        // 查询通知单ID
        if (noticesId == null) {
            SgOutNoticesByBillNoResult billNoResult = queryOutNoticesBySource(request, false);
            if (billNoResult != null && billNoResult.getOutNotices() != null) {
                noticesId = billNoResult.getOutNotices().getId();
            }
        }

        if (noticesId != null) {
            SgBPhyOutResultMapper resultMapper = ApplicationContextHandle.getBean(SgBPhyOutResultMapper.class);
            List<SgBPhyOutResult> outResults = resultMapper.selectList(new QueryWrapper<SgBPhyOutResult>()
                    .eq(SgOutConstants.OUT_NOTICES_PAREN_FIELD, noticesId)
                    .eq(SgOutConstants.IS_ACTIVE, SgConstants.IS_ACTIVE_Y));

            if (!CollectionUtils.isEmpty(outResults)) {
                SgBPhyOutResultItemMapper itemMapper = ApplicationContextHandle.getBean(SgBPhyOutResultItemMapper.class);
                outResults.forEach(outResult -> {
                    SgOutResultResult outResultResult = new SgOutResultResult();
                    outResultResult.setOutResult(outResult);

                    // 查询明细
                    if (isReturnItem) {
                        outResultResult.setItems(itemMapper.selectList(new QueryWrapper<SgBPhyOutResultItem>()
                                .eq(SgOutConstants.OUT_RESULT_PAREN_FIELD, outResult.getId())));
                    }
                    results.add(outResultResult);
                });
            }
        }
        return results;
    }

    /**
     * 根据条件查询出库通知单主表,录入明细，箱明细
     */
    public ValueHolderV14<List<SgBPhyOutNoticesAllInfoResult>> queryOutNoticesAllInfoBySource(SgPhyOutNoticesQueryRequest request) {

        ValueHolderV14<List<SgBPhyOutNoticesAllInfoResult>> valueHolderV14 = new ValueHolderV14<>();

        if (request == null) {
            valueHolderV14.setCode(ResultCode.FAIL);
            valueHolderV14.setMessage("request is null");
            return valueHolderV14;
        }

        String billNo = request.getBillNo();
        String sourceBillNo = request.getSourceBillNo();
        Integer sourceBillType = request.getSourceBillType();
        Long sourceBillId = request.getSourceBillId();
        List<Long> wmsStatusList = request.getWmsStatusList();
        List<Integer> billStatusList = request.getBillStatusList();
        Integer isDiffDeal = request.getIsDiffDeal();
        Boolean isSpecialQuery = request.getIsSpecialQuery();


        SgBPhyOutNoticesMapper noticesMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesMapper.class);
        List<SgBPhyOutNotices> sgBPhyOutNotices;
        if (isSpecialQuery) {
            sgBPhyOutNotices = noticesMapper.selectOutNoticesToWmsForCancelDiff();
        } else {
            sgBPhyOutNotices = noticesMapper.selectList(new QueryWrapper<SgBPhyOutNotices>().lambda()
                    .eq(sourceBillType != null, SgBPhyOutNotices::getSourceBillType, sourceBillType)
                    .eq(sourceBillId != null, SgBPhyOutNotices::getSourceBillId, sourceBillId)
                    .eq(isDiffDeal != null, SgBPhyOutNotices::getIsDiffDeal, isDiffDeal)
                    .in(CollectionUtils.isNotEmpty(wmsStatusList), SgBPhyOutNotices::getWmsStatus, wmsStatusList)
                    .in(CollectionUtils.isNotEmpty(billStatusList), SgBPhyOutNotices::getBillStatus, billStatusList)
                    .eq(StringUtils.isNotEmpty(billNo), SgBPhyOutNotices::getBillNo, billNo)
                    .eq(StringUtils.isNotEmpty(sourceBillNo), SgBPhyOutNotices::getSourceBillNo, sourceBillNo)
                    .eq(SgBPhyOutNotices::getIsactive, SgConstants.IS_ACTIVE_Y));
        }
        //查询出库通知单主表信息


        if (CollectionUtils.isEmpty(sgBPhyOutNotices)) {
            valueHolderV14.setCode(ResultCode.FAIL);
            valueHolderV14.setMessage("未查到出库通知单信息");
        } else {

            List<SgBPhyOutNoticesAllInfoResult> sgBPhyOutNoticesAllInfoResults = new ArrayList<>();
            for (SgBPhyOutNotices sgBPhyOutNotice : sgBPhyOutNotices) {
                SgBPhyOutNoticesAllInfoResult sgBPhyOutNoticesAllInfoResult = new SgBPhyOutNoticesAllInfoResult();
                sgBPhyOutNoticesAllInfoResult.setSgBPhyOutNotices(sgBPhyOutNotice);

                //查询录入明细
                SgBPhyOutNoticesImpItemMapper impItemMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesImpItemMapper.class);
                List<SgBPhyOutNoticesImpItem> sgBPhyOutNoticesImpItems = impItemMapper.selectList(new QueryWrapper<SgBPhyOutNoticesImpItem>().lambda()
                        .eq(SgBPhyOutNoticesImpItem::getSgBPhyOutNoticesId, sgBPhyOutNotice.getId()));
                sgBPhyOutNoticesAllInfoResult.setSgBPhyOutNoticesImpItemList(sgBPhyOutNoticesImpItems);

                //查询箱明细
                SgBPhyOutNoticesTeusItemMapper teusItemMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesTeusItemMapper.class);
                List<SgBPhyOutNoticesTeusItem> sgBPhyOutNoticesteusItems = teusItemMapper.selectList(new QueryWrapper<SgBPhyOutNoticesTeusItem>().lambda()
                        .eq(SgBPhyOutNoticesTeusItem::getSgBPhyOutNoticesId, sgBPhyOutNotice.getId()));
                sgBPhyOutNoticesAllInfoResult.setSgBPhyOutNoticesTeusItemList(sgBPhyOutNoticesteusItems);

                //查询条码明细
                if (request.getQueryNoticesItem()) {
                    SgBPhyOutNoticesItemMapper itemMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesItemMapper.class);
                    List<SgBPhyOutNoticesItem> sgBPhyOutNoticesItems = itemMapper.selectList(new QueryWrapper<SgBPhyOutNoticesItem>().lambda()
                            .eq(SgBPhyOutNoticesItem::getSgBPhyOutNoticesId, sgBPhyOutNotice.getId()));
                    sgBPhyOutNoticesAllInfoResult.setSgBPhyOutNoticesItemList(sgBPhyOutNoticesItems);
                }
                sgBPhyOutNoticesAllInfoResults.add(sgBPhyOutNoticesAllInfoResult);
            }

            valueHolderV14.setCode(ResultCode.SUCCESS);
            valueHolderV14.setMessage("查询成功");
            valueHolderV14.setData(sgBPhyOutNoticesAllInfoResults);
        }

        return valueHolderV14;

    }
}
