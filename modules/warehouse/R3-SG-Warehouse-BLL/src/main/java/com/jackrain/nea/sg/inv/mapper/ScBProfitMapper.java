package com.jackrain.nea.sg.inv.mapper;

import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.inv.model.table.ScBProfit;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ScBProfitMapper extends ExtentionMapper<ScBProfit> {
}