package com.jackrain.nea.sg.in.services;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Lists;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.oc.purchase.api.OcDirectUpdateCmd;
import com.jackrain.nea.oc.purchase.api.OcPurchaseUpdateCmd;
import com.jackrain.nea.oc.purchase.model.request.OcPurchaseUpdatePickRequest;
import com.jackrain.nea.oc.purchase.model.result.OcPurchaseUpdatePickResult;
import com.jackrain.nea.oc.sale.api.OcRefundSaleUpdateCmd;
import com.jackrain.nea.oc.sale.api.OcSaleUpdateCmd;
import com.jackrain.nea.oc.sale.model.request.OcSaleUpdatePickRequest;
import com.jackrain.nea.oc.sale.model.result.OcSaleUpdatePickResult;
import com.jackrain.nea.redis.config.CusRedisTemplate;
import com.jackrain.nea.redis.util.RedisOpsUtil;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.basic.utils.StorageUtils;
import com.jackrain.nea.sg.in.common.SgInConstants;
import com.jackrain.nea.sg.in.common.SgInNoticeConstants;
import com.jackrain.nea.sg.in.mapper.SgBInPickorderMapper;
import com.jackrain.nea.sg.in.mapper.SgBInPickorderNoticeItemMapper;
import com.jackrain.nea.sg.in.model.request.SgPhyInNoticesBillUpdateRequest;
import com.jackrain.nea.sg.in.model.request.SgPhyInNoticesVoidReqeust;
import com.jackrain.nea.sg.in.model.table.SgBInPickorder;
import com.jackrain.nea.sg.in.model.table.SgBInPickorderItem;
import com.jackrain.nea.sg.in.model.table.SgBInPickorderNoticeItem;
import com.jackrain.nea.sg.in.utils.InUtil;
import com.jackrain.nea.sg.receive.model.request.SgReceiveBillCleanRequest;
import com.jackrain.nea.sg.receive.model.result.SgReceiveBillCleanResult;
import com.jackrain.nea.sg.receive.services.SgReceiveCleanService;
import com.jackrain.nea.sg.transfer.api.SgTransferUpdatePickCmd;
import com.jackrain.nea.sg.transfer.model.request.OcTransferUpdatePickRequest;
import com.jackrain.nea.sg.transfer.model.result.OcTransferUpdatePickResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @author leexh
 * @since 2020/4/7 11:51
 * desc: 入库拣货单作废服务
 */
@Slf4j
@Component
public class SgPhyInPickOrdersVoidService {

    /**
     * 入库拣货单作废逻辑
     *
     * @param user         用户信息
     * @param objId        主键ID
     * @param isTransferIn 是否是调拨入库单
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public ValueHolder doBillVoid(User user, Long objId, Boolean isTransferIn) {

        if (log.isDebugEnabled()) {
            log.debug("Start " + this.getClass().getName() + ".doBillVoid. ReceiveParams:objId=" + objId + ";");
        }
        ValueHolder valueHolder = new ValueHolder();
        Locale locale = user.getLocale();
        AssertUtils.notNull(objId, "请选择要要操作的单据！", locale);
        CusRedisTemplate<String, String> redisTemplate = RedisOpsUtil.getObjRedisTemplate();
        String lockKsy = SgConstants.SG_B_IN_PICKORDER + ":" + objId;
        Boolean blnCanInit = redisTemplate.opsForValue().setIfAbsent(lockKsy, "OK");
        if (blnCanInit != null && blnCanInit) {
            redisTemplate.expire(lockKsy, 30, TimeUnit.MINUTES);
        } else {
            String msg = "当前单据正在被操作！请稍后重试..";
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + "，debug," + msg);
            }
            AssertUtils.logAndThrow(msg, user.getLocale());
        }
        try {
            SgBInPickorderMapper inPickOrderMapper = ApplicationContextHandle.getBean(SgBInPickorderMapper.class);
            SgBInPickorderNoticeItemMapper inPickOrderNoticeItemMapper = ApplicationContextHandle.getBean(SgBInPickorderNoticeItemMapper.class);
            SgBInPickorder inPickOrder = inPickOrderMapper.selectById(objId);
            AssertUtils.notNull(inPickOrder, "当前记录已不存在！", locale);
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + ".debug, purchase:" + JSONObject.toJSONString(inPickOrder));
            }
            Integer status = inPickOrder.getBillStatus();
            if (status == SgInConstants.BILL_STATUS_CHECKED) {
                AssertUtils.logAndThrow("当前记录已审核，不允许作废！", locale);
            }
            if (status == SgInConstants.BILL_STATUS_VOID) {
                AssertUtils.logAndThrow("当前记录已作废，不允许重复作废！", locale);
            }

            // 2020-04-07添加逻辑：调拨入库单独有逻辑
            if (isTransferIn) {
                this.checkStore(locale, inPickOrder.getSourceBillNo());
            }

            List<SgBInPickorderNoticeItem> inPickOrderNoticeItemList = inPickOrderNoticeItemMapper.selectList(new QueryWrapper<SgBInPickorderNoticeItem>().lambda()
                    .eq(SgBInPickorderNoticeItem::getSgBInPickorderId, objId)
                    .eq(SgBInPickorderNoticeItem::getIsactive, SgConstants.IS_ACTIVE_Y));
            if (CollectionUtils.isEmpty(inPickOrderNoticeItemList)) {
                AssertUtils.logAndThrow("入库拣货单关联的来源入库通知单明细数据不存在！", user.getLocale());
            }
            // 获取入库通知单编号
            List<String> billNoList = inPickOrderNoticeItemList.stream().map(SgBInPickorderNoticeItem::getNoticeBillNo).collect(Collectors.toList());
            // 更新入库通知单拣货状态为未拣货
            SgPhyInNoticesUpdateService sgPhyInNoticesUpdateService = ApplicationContextHandle.getBean(SgPhyInNoticesUpdateService.class);
            SgPhyInNoticesBillUpdateRequest sgPhyInNoticesBillUpdateRequest = new SgPhyInNoticesBillUpdateRequest();
            sgPhyInNoticesBillUpdateRequest.setNoticesBillNos(billNoList);
            sgPhyInNoticesBillUpdateRequest.setLoginUser(user);
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + ".debug, 更新入库通知单拣货状态为未拣货_param:" + JSONObject.toJSONString(sgPhyInNoticesBillUpdateRequest));
            }
            ValueHolderV14 v14 = sgPhyInNoticesUpdateService.updateSgBPhyInNoticesByPick(sgPhyInNoticesBillUpdateRequest);
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + ".debug, 更新入库通知单拣货状态为未拣货_result:" + v14.toJSONObject());
            }
            if (v14.isOK()) {
                //更新入库拣货单的作废状态
                SgBInPickorder updateModel = new SgBInPickorder();
                updateModel.setId(objId);
                StorageESUtils.setBModelDefalutDataByUpdate(updateModel, user);
                updateModel.setModifierename(user.getEname());
                updateModel.setIsactive(SgConstants.IS_ACTIVE_N);
                updateModel.setDelerId(user.getId().longValue());
                updateModel.setDelerName(user.getName());
                updateModel.setDelerEname(user.getEname());
                updateModel.setDelTime(new Date());
                //状态改为已作废
                updateModel.setDelStatus(2);
                // 更新已作废状态
                updateModel.setStatus(SgInConstants.BILL_STATUS_VOID);
                updateModel.setBillStatus(SgInConstants.BILL_STATUS_VOID);
                if (inPickOrderMapper.updateById(updateModel) > 0) {
                    //推送ES
                    SgBInPickorder inPickOrderModel = inPickOrderMapper.selectById(objId);
                    String index = SgConstants.SG_B_IN_PICKORDER;
                    String type = SgConstants.SG_B_IN_PICKORDER_ITEM;
                    StorageESUtils.pushESBModelByUpdate(inPickOrderModel, null, inPickOrderModel.getId(), null, index, index, type, null, SgBInPickorder.class, SgBInPickorderItem.class, false);
                }

                // 2020-04-07添加逻辑：调拨入库单独有逻辑
                if (isTransferIn) {
                    // 1.调用清空逻辑收货单服务; 2.调用作废入库通知单服务
                    SgPhyInPickOrdersVoidService service = ApplicationContextHandle.getBean(SgPhyInPickOrdersVoidService.class);
                    service.cleanReceiveAndVoidInNotice(user, inPickOrder);
                }

                //调用小辉接口更新分销单据的拣货状态
                invokeRpc(inPickOrderNoticeItemList, user);
//                try {
//                } catch (Exception ex) {
//                    if (log.isErrorEnabled()) {
//                        log.error(this.getClass().getName() + ",入库拣货单作废异常:" + StorageUtils.getExceptionMsg(ex));
//                    }
//                    AssertUtils.logAndThrowExtra("入库拣货单作废失败！", ex);
//                }
            } else {
                AssertUtils.logAndThrow("入库拣货单作废失败！" + v14.getMessage(), user.getLocale());
            }
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error(this.getClass().getName() + ",入库拣货单作废异常:" + StorageUtils.getExceptionMsg(e));
            }
            AssertUtils.logAndThrowExtra("入库拣货单作废异常", e);
        } finally {
            try {
                redisTemplate.delete(lockKsy);
            } catch (Exception e) {
                log.error(this.getClass().getName() + "redis锁释放失败，请联系管理员！lockKsy：" + lockKsy);
                AssertUtils.logAndThrow("redis锁释放失败，请联系管理员！");
            }
        }
        valueHolder.put("code", ResultCode.SUCCESS);
        valueHolder.put("message", "入库拣货单作废成功！");
        return valueHolder;
    }


    /**
     * 根据拣货单对应的入库通知单的来源单据编号+来源单据类型
     * （调拨单、直发单、采购单、销售单、销售退货单）调用“更新来源单据的入库拣货状态服务”；
     * 调拨入库服务加【串码入库】
     *
     * @param inPickOrderNoticeItemList 拣货单明细
     * @param user                      用户信息
     */
    private void invokeRpc(List<SgBInPickorderNoticeItem> inPickOrderNoticeItemList, User user) {

        //销售单
        List<Long> saleIds = Lists.newArrayList();
        //调拨单
        List<Long> tranIds = Lists.newArrayList();
        //直发单
        List<Long> directIds = Lists.newArrayList();
        //采购单
        List<Long> purchaseIds = Lists.newArrayList();
        //销售退货单
        List<Long> salesReturnIds = Lists.newArrayList();
        //根据单据类型取出对应单据id
        inPickOrderNoticeItemList.forEach(sgBInPickorderNoticeItem -> {
            //销售单
            if (sgBInPickorderNoticeItem.getSourceBillType() != null && sgBInPickorderNoticeItem.getSourceBillType().equals(SgConstantsIF.BILL_TYPE_SALE)) {
                saleIds.add(sgBInPickorderNoticeItem.getSourceBillId());
            }
            //调拨单
            if (sgBInPickorderNoticeItem.getSourceBillType() != null && sgBInPickorderNoticeItem.getSourceBillType().equals(SgConstantsIF.BILL_TYPE_TRANSFER)) {
                tranIds.add(sgBInPickorderNoticeItem.getSourceBillId());
            }
            //采购单
            if (sgBInPickorderNoticeItem.getSourceBillType() != null && sgBInPickorderNoticeItem.getSourceBillType().equals(SgConstantsIF.BILL_TYPE_PUR)) {
                purchaseIds.add(sgBInPickorderNoticeItem.getSourceBillId());
            }
            //销售退货单
            if (sgBInPickorderNoticeItem.getSourceBillType() != null && sgBInPickorderNoticeItem.getSourceBillType().equals(SgConstantsIF.BILL_TYPE_SALE_REF)) {
                salesReturnIds.add(sgBInPickorderNoticeItem.getSourceBillId());
            }
            //直发单
            if (sgBInPickorderNoticeItem.getSourceBillType() != null && sgBInPickorderNoticeItem.getSourceBillType().equals(SgConstantsIF.BILL_TYPE_DIRECT)) {
                directIds.add(sgBInPickorderNoticeItem.getSourceBillId());
            }
        });
        //调用销售单更新拣货类型服务
        if (CollectionUtils.isNotEmpty(saleIds)) {
            OcSaleUpdateCmd ocSaleUpdateCmd = (OcSaleUpdateCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                    OcSaleUpdateCmd.class.getName(), "oc", "1.0");
            OcSaleUpdatePickRequest ocSaleUpdatePickRequest = new OcSaleUpdatePickRequest();
            ocSaleUpdatePickRequest.setIds(saleIds);
            ocSaleUpdatePickRequest.setInPickStatus(SgInNoticeConstants.PICK_STATUS_WAIT);
            ocSaleUpdatePickRequest.setUser(user);
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + ".debug, 调用销售单更新入库拣货类型服务_param:" + JSONObject.toJSONString(ocSaleUpdatePickRequest));
            }
            ValueHolderV14<List<OcSaleUpdatePickResult>> valueHolderV14 = ocSaleUpdateCmd.batchUpdateSalePickStatus(ocSaleUpdatePickRequest);
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + ".debug, 调用销售单更新入库拣货类型服务_result:" + valueHolderV14.toJSONObject());
            }
        }
        //调用销售退货单更新拣货类型服务
        if (CollectionUtils.isNotEmpty(salesReturnIds)) {
            OcRefundSaleUpdateCmd ocRefundSaleUpdateCmd = (OcRefundSaleUpdateCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                    OcRefundSaleUpdateCmd.class.getName(), "oc", "1.0");
            OcSaleUpdatePickRequest ocSaleUpdatePickRequest = new OcSaleUpdatePickRequest();
            ocSaleUpdatePickRequest.setIds(salesReturnIds);
            ocSaleUpdatePickRequest.setInPickStatus(SgInNoticeConstants.PICK_STATUS_WAIT);
            ocSaleUpdatePickRequest.setUser(user);
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + ".debug, 调用销售退货单新入库拣货类型服务_param:" + JSONObject.toJSONString(ocSaleUpdatePickRequest));
            }
            ValueHolderV14<List<OcSaleUpdatePickResult>> valueHolderV14 = ocRefundSaleUpdateCmd.batchUpdateRefSalePickStatus(ocSaleUpdatePickRequest);
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + ".debug, 调用销售退货单更新入库拣货类型服务_result:" + valueHolderV14.toJSONObject());
            }
        }
        //调用采购单更新拣货类型服务
        if (CollectionUtils.isNotEmpty(purchaseIds)) {
            OcPurchaseUpdateCmd ocPurchaseUpdateCmd = (OcPurchaseUpdateCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                    OcPurchaseUpdateCmd.class.getName(), "oc", "1.0");
            OcPurchaseUpdatePickRequest ocPurchaseUpdatePickRequest = new OcPurchaseUpdatePickRequest();
            ocPurchaseUpdatePickRequest.setIds(purchaseIds);
            ocPurchaseUpdatePickRequest.setInPickStatus(SgInNoticeConstants.PICK_STATUS_WAIT);
            ocPurchaseUpdatePickRequest.setUser(user);
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + ".debug, 调用采购单更新入库拣货类型服务_param:" + JSONObject.toJSONString(ocPurchaseUpdatePickRequest));
            }
            ValueHolderV14<List<OcPurchaseUpdatePickResult>> valueHolderV14 = ocPurchaseUpdateCmd.batchUpdatePurchasePickStatus(ocPurchaseUpdatePickRequest);
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + ".debug, 调用采购单更新入库拣货类型服务_result:" + valueHolderV14.toJSONObject());
            }
        }
        //调用直发单更新拣货类型服务
        if (CollectionUtils.isNotEmpty(directIds)) {
            OcDirectUpdateCmd ocDirectUpdateCmd = (OcDirectUpdateCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                    OcDirectUpdateCmd.class.getName(), "oc", "1.0");
            OcPurchaseUpdatePickRequest ocPurchaseUpdatePickRequest = new OcPurchaseUpdatePickRequest();
            ocPurchaseUpdatePickRequest.setIds(directIds);
            ocPurchaseUpdatePickRequest.setInPickStatus(SgInNoticeConstants.PICK_STATUS_WAIT);
            ocPurchaseUpdatePickRequest.setUser(user);
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + ".debug, 调用直发单更新入库拣货类型服务_param:" + JSONObject.toJSONString(ocPurchaseUpdatePickRequest));
            }
            ValueHolderV14<List<OcPurchaseUpdatePickResult>> valueHolderV14 = ocDirectUpdateCmd.batchUpdateDirectPickStatus(ocPurchaseUpdatePickRequest);
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + ".debug, 调用直发单更新入库拣货类型服务_result:" + valueHolderV14.toJSONObject());
            }
        }
        //调用调拨单更新拣货类型服务
        if (CollectionUtils.isNotEmpty(tranIds)) {
            SgTransferUpdatePickCmd sgTransferUpdatePickCmd = (SgTransferUpdatePickCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                    SgTransferUpdatePickCmd.class.getName(), "sg", "1.0");
            OcTransferUpdatePickRequest ocTransferUpdatePickRequest = new OcTransferUpdatePickRequest();
            ocTransferUpdatePickRequest.setIds(tranIds);
            ocTransferUpdatePickRequest.setInPickStatus(SgInNoticeConstants.PICK_STATUS_WAIT);
            ocTransferUpdatePickRequest.setUser(user);
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + ".debug, 调用调拨单更新入库拣货类型服务_param:" + JSONObject.toJSONString(ocTransferUpdatePickRequest));
            }
            ValueHolderV14<List<OcTransferUpdatePickResult>> valueHolderV14 = sgTransferUpdatePickCmd.batchUpdateTransferPickStatus(ocTransferUpdatePickRequest);
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + ".debug, 调用调拨单更新入库拣货类型服务_result:" + valueHolderV14.toJSONObject());
            }
        }
    }


    /**
     * 清空逻辑收货单、作废入库通知单
     *
     * @param user      用户信息
     * @param pickOrder 拣货单信息
     */
    @Transactional(rollbackFor = Exception.class)
    public void cleanReceiveAndVoidInNotice(User user, SgBInPickorder pickOrder) {

        // 1.清空逻辑收货单
        SgReceiveBillCleanRequest cleanRequest = new SgReceiveBillCleanRequest();
        cleanRequest.setLoginUser(user);
        cleanRequest.setSourceBillId(pickOrder.getSourceBillId());
        cleanRequest.setSourceBillNo(pickOrder.getSourceBillNo());
        cleanRequest.setSourceBillType(SgConstantsIF.BILL_TYPE_TRANSFER);
        cleanRequest.setServiceNode(SgConstantsIF.SERVICE_NODE_TRANSFER_IN_VOID);

        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ",调拨入库单调用清空逻辑收货单入参:" + JSON.toJSONString(cleanRequest));
        }
        SgReceiveCleanService service = ApplicationContextHandle.getBean(SgReceiveCleanService.class);
        ValueHolderV14<SgReceiveBillCleanResult> cleanHolderV14 = service.cleanSgBReceive(cleanRequest);
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ",调拨入库单调用清空逻辑收货单出参:" + JSON.toJSONString(cleanHolderV14));
        }

        AssertUtils.cannot(ResultCode.FAIL == cleanHolderV14.getCode(),
                "调拨入库单调用清空逻辑收货单失败！" + cleanHolderV14.getMessage(), user.getLocale());


        // 2.作废入库通知单
        SgPhyInNoticesVoidReqeust voidRequest = new SgPhyInNoticesVoidReqeust();
        voidRequest.setLoginUser(user);
        voidRequest.setSourceBillId(pickOrder.getSourceBillId());
        voidRequest.setSourceBillType(SgConstantsIF.BILL_TYPE_TRANSFER);

        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ",调拨入库单调用作废入库通知单入参:" + JSON.toJSONString(cleanRequest));
        }
        SgPhyInNoticesVoidService voidService = ApplicationContextHandle.getBean(SgPhyInNoticesVoidService.class);
        ValueHolderV14 voidHolderV14 = voidService.voidSgBPhyInNotices(voidRequest);
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ",调拨入库单调用作废入库通知单出参:" + JSON.toJSONString(voidHolderV14));
        }

        AssertUtils.cannot(ResultCode.FAIL == cleanHolderV14.getCode(),
                "调拨入库单调用作废入库通知单失败！" + voidHolderV14.getMessage(), user.getLocale());
    }


    /**
     * 检查收发货店仓是否是‘集团直营’
     *
     * @param locale         国际化
     * @param transferBillNo 调拨单单据编号
     */
    private void checkStore(Locale locale, String transferBillNo) {
        InUtil inUtil = ApplicationContextHandle.getBean(InUtil.class);
        boolean isDirectly = inUtil.checkStoreIsDirectly(locale, transferBillNo);
        if (isDirectly) {
            AssertUtils.logAndThrow("收发货门店为集团直营，不允许作废！", locale);
        }
    }

}
