package com.jackrain.nea.sg.unpack.service;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.unpack.common.SgUnpackConstants;
import com.jackrain.nea.sg.unpack.mapper.SgBTeusUnpackMapper;
import com.jackrain.nea.sg.unpack.model.table.SgBTeusUnpack;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author: 周琳胜
 * @since: 2019/12/4
 * create at : 2019/12/4 11:32
 */
@Slf4j
@Component
public class SgUnpackUpdateForWmsService {

    public ValueHolderV14 updateSgUnpack(ValueHolderV14 holderV14) {
        ValueHolderV14 v14 = new ValueHolderV14();
        if (log.isDebugEnabled()) {
            log.debug("Start SgUnpackUpdateForWmsService.updateSgUnpack. ReceiveParams:requests:{}"
                    , JSONObject.toJSONString(holderV14));
        }
        if (holderV14 == null) {
            v14.setCode(ResultCode.FAIL);
            v14.setMessage("参数不能为空！");
        }

        try {
            if (holderV14.isOK()) {
                JSONObject data = (JSONObject) holderV14.getData();
                Long id = data.getLong("ID");
                SgBTeusUnpackMapper teusUnpackMapper = ApplicationContextHandle.getBean(SgBTeusUnpackMapper.class);
                SgBTeusUnpack sgBTeusUnpack = new SgBTeusUnpack();
                sgBTeusUnpack.setId(id);
                sgBTeusUnpack.setPassWmsTime(new Date());
                sgBTeusUnpack.setWmsStatus(SgUnpackConstants.WMS_STATUS_PASS_SUCCESS);
                sgBTeusUnpack.setWmsFailReason("");
                teusUnpackMapper.updateById(sgBTeusUnpack);
            } else if (holderV14.getCode() == -1){
                JSONObject data = (JSONObject) holderV14.getData();
                Long id = data.getLong("ID");
                SgBTeusUnpackMapper teusUnpackMapper = ApplicationContextHandle.getBean(SgBTeusUnpackMapper.class);
                SgBTeusUnpack teusUnpack = teusUnpackMapper.selectOne(new QueryWrapper<SgBTeusUnpack>().lambda().eq(SgBTeusUnpack::getId, id));
                SgBTeusUnpack sgBTeusUnpack = new SgBTeusUnpack();
                sgBTeusUnpack.setId(id);
                sgBTeusUnpack.setWmsFailCount(teusUnpack.getWmsFailCount().add(BigDecimal.ONE));
                sgBTeusUnpack.setWmsStatus(SgUnpackConstants.WMS_STATUS_PASS_FAILED);
                sgBTeusUnpack.setWmsFailReason(holderV14.getMessage());
                teusUnpackMapper.updateById(sgBTeusUnpack);
            } else {
                JSONObject data = (JSONObject) holderV14.getData();
                Long id = data.getLong("ID");
                SgBTeusUnpackMapper teusUnpackMapper = ApplicationContextHandle.getBean(SgBTeusUnpackMapper.class);
                SgBTeusUnpack sgBTeusUnpack = new SgBTeusUnpack();
                sgBTeusUnpack.setId(id);
                sgBTeusUnpack.setWmsStatus(SgUnpackConstants.WMS_STATUS_PASSING);
                teusUnpackMapper.updateById(sgBTeusUnpack);
            }
        } catch (Exception e) {
            v14.setCode(ResultCode.FAIL);
            v14.setMessage(e.getMessage());
            return v14;
        }


        if (log.isDebugEnabled()) {
            log.debug("Finsh SgUnpackUpdateForWmsService.updateSgUnpack. Result:valueHolderV14:{}"
                    , JSONObject.toJSONString(v14));
        }
        v14.setCode(ResultCode.SUCCESS);
        v14.setMessage("成功！");
        return v14;
    }

}
