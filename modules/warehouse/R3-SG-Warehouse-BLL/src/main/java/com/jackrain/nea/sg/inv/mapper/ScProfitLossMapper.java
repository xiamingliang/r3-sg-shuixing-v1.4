package com.jackrain.nea.sg.inv.mapper;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.inv.constants.SgInvConstants;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;

import java.util.Date;
import java.util.List;

/**
 * @author: 舒威
 * @since: 2019/4/3
 * create at : 2019/4/3 14:23
 */
@Mapper
public interface ScProfitLossMapper  extends ExtentionMapper {

    @Select("SELECT stock_flag FROM " + SgInvConstants.SC_B_STOCK_DAILY_FLAG + " WHERE stock_date=#{date}")
    Integer checkProfItemByDailyFlag(@Param("date") Date date);

    @Select("SELECT id FROM " + SgInvConstants.SG_B_STOCK_DAILY + " WHERE rpt_date=#{date} limit 1")
    Long checkProfItemByDaily(@Param("date") Date date);

    @Select("SELECT id FROM " + SgInvConstants.SG_B_TEUS_STOCK_DAILY + " WHERE rpt_date=#{date} limit 1")
    Long checkProfItemByTeusDaily(@Param("date") Date date);

    @SelectProvider(type = ScProfitSqlProvider.class, method = "queryProfItemByQPC")
    List<JSONObject> queryProfItemByQPC(@Param("storeId") Long storeId,
                                        @Param("inventoryDate") Date inventoryDate,
                                        @Param("inventoryType") Integer inventoryType,
                                        @Param("flag") Boolean flag);


    @SelectProvider(type = ScProfitSqlProvider.class, method = "queryProfItemByQP")
    List<JSONObject> queryProfItemByQP(@Param("storeId") Long storeId,
                                       @Param("inventoryDate") Date inventoryDate,
                                       @Param("inventoryType") Integer inventoryType,
                                       @Param("startIndex") Integer startIndex,
                                       @Param("range") Integer range,
                                       @Param("orderByName") String orderByName,
                                       @Param("flag") Boolean flag,
                                       @Param("getCount") boolean getCount,
                                       @Param("isExport") Boolean isExport);

    @SelectProvider(type = ScProfitSqlProvider.class, method = "queryProfItemByQP")
    Long getCountProfItemByQP(@Param("storeId") Long storeId,
                              @Param("inventoryDate") Date inventoryDate,
                              @Param("inventoryType") Integer inventoryType,
                              @Param("startIndex") Integer startIndex,
                              @Param("range") Integer range,
                              @Param("orderByName") String orderByName,
                              @Param("flag") Boolean flag,
                              @Param("getCount") boolean getCount,
                              @Param("isExport") Boolean isExport);

    @SelectProvider(type = ScProfitSqlProvider.class, method = "queryProfItemByCPC")
    List<JSONObject> queryProfItemByCPC(@Param("storeId") Long storeId,
                                        @Param("inventoryDate") Date inventoryDate,
                                        @Param("inventoryType") Integer inventoryType,
                                        @Param("startIndex") Integer startIndex,
                                        @Param("range") Integer range,
                                        @Param("orderByName") String orderByName,
                                        @Param("flag") Boolean flag,
                                        @Param("getCount") boolean getCount);

    @SelectProvider(type = ScProfitSqlProvider.class, method = "queryProfItemByCP")
    List<JSONObject> queryProfItemByCP(@Param("storeId") Long storeId,
                                       @Param("inventoryDate") Date inventoryDate,
                                       @Param("inventoryType") Integer inventoryType,
                                       @Param("startIndex") Integer startIndex,
                                       @Param("range") Integer range,
                                       @Param("orderByName") String orderByName,
                                       @Param("flag") Boolean flag,
                                       @Param("getCount") boolean getCount,
                                       @Param("isExport") Boolean isExport);

    @SelectProvider(type = ScProfitSqlProvider.class, method = "queryProfItemByCP")
    Long getCountProfItemByCP(@Param("storeId") Long storeId,
                              @Param("inventoryDate") Date inventoryDate,
                              @Param("inventoryType") Integer inventoryType,
                              @Param("startIndex") Integer startIndex,
                              @Param("range") Integer range,
                              @Param("orderByName") String orderByName,
                              @Param("flag") Boolean flag,
                              @Param("getCount") boolean getCount,
                              @Param("isExport") Boolean isExport);

    @SelectProvider(type = ScProfitTeusSqlProvider.class, method = "queryProfTeusItemByCP")
    List<JSONObject> queryProfTeusItemByCP(@Param("storeId") Long storeId,
                                           @Param("inventoryDate") Date inventoryDate,
                                           @Param("inventoryType") Integer inventoryType,
                                           @Param("startIndex") Integer startIndex,
                                           @Param("range") Integer range,
                                           @Param("orderByName") String orderByName,
                                           @Param("flag") Boolean flag,
                                           @Param("getCount") boolean getCount,
                                           @Param("isExport") Boolean isExport,
                                           @Param("isCreateProf") Boolean isCreateProf);

    @SelectProvider(type = ScProfitTeusSqlProvider.class, method = "queryProfTeusItemByCP")
    Long getCountProfTeusItemByCP(@Param("storeId") Long storeId,
                                  @Param("inventoryDate") Date inventoryDate,
                                  @Param("inventoryType") Integer inventoryType,
                                  @Param("startIndex") Integer startIndex,
                                  @Param("range") Integer range,
                                  @Param("orderByName") String orderByName,
                                  @Param("flag") Boolean flag,
                                  @Param("getCount") boolean getCount,
                                  @Param("isExport") Boolean isExport,
                                  @Param("isCreateProf") Boolean isCreateProf);

    @SelectProvider(type = ScProfitTeusSqlProvider.class, method = "queryProfTeusItemByQP")
    List<JSONObject> queryProfTeusItemByQP(@Param("storeId") Long storeId,
                                           @Param("inventoryDate") Date inventoryDate,
                                           @Param("inventoryType") Integer inventoryType,
                                           @Param("startIndex") Integer startIndex,
                                           @Param("range") Integer range,
                                           @Param("orderByName") String orderByName,
                                           @Param("flag") Boolean flag,
                                           @Param("getCount") boolean getCount,
                                           @Param("isExport") Boolean isExport,
                                           @Param("isCreateProf") Boolean isCreateProf);

    @SelectProvider(type = ScProfitTeusSqlProvider.class, method = "queryProfTeusItemByQP")
    Long getCountProfTeusItemByQP(@Param("storeId") Long storeId,
                                  @Param("inventoryDate") Date inventoryDate,
                                  @Param("inventoryType") Integer inventoryType,
                                  @Param("startIndex") Integer startIndex,
                                  @Param("range") Integer range,
                                  @Param("orderByName") String orderByName,
                                  @Param("flag") Boolean flag,
                                  @Param("getCount") boolean getCount,
                                  @Param("isExport") Boolean isExport,
                                  @Param("isCreateProf") Boolean isCreateProf);
}
