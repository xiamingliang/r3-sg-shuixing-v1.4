package com.jackrain.nea.sg.in.mapper;

import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.in.model.table.SgBPhyInResultDefectItem;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SgBPhyInResultDefectItemMapper extends ExtentionMapper<SgBPhyInResultDefectItem> {
}