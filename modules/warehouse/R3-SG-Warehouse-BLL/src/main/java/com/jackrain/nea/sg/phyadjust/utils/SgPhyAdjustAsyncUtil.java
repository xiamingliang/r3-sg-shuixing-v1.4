package com.jackrain.nea.sg.phyadjust.utils;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.jackrain.nea.sg.basic.config.SgStorageBoxConfig;
import com.jackrain.nea.sg.phyadjust.mapper.SgBPhyAdjustImpItemMapper;
import com.jackrain.nea.sg.phyadjust.mapper.SgBPhyAdjustItemMapper;
import com.jackrain.nea.sg.phyadjust.model.table.SgBPhyAdjustImpItem;
import com.jackrain.nea.sg.phyadjust.model.table.SgBPhyAdjustItem;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

/**
 * @author: 舒威
 * @since: 2019/11/30
 * create at : 2019/11/30 16:57
 */
@Component
@Slf4j
public class SgPhyAdjustAsyncUtil {

    @Autowired
    private SgStorageBoxConfig storageBoxConfig;


    @Autowired
    private SgBPhyAdjustItemMapper itemMapper;

    /**
     * 异步删除明细中调整数量为0的记录
     * 避免删除后空单审核后事务回滚
     *
     * @param id          主表Id
     */
    @Async("esAsync")
    public void deleteAdjustItemsByZero(Long id) {
        if (storageBoxConfig.getBoxEnable()) {
            //删除录入明细中调整数量为0的记录
            SgBPhyAdjustImpItemMapper impItemMapper = ApplicationContextHandle.getBean(SgBPhyAdjustImpItemMapper.class);
            int delete = impItemMapper.delete(new UpdateWrapper<SgBPhyAdjustImpItem>().lambda()
                    .eq(SgBPhyAdjustImpItem::getSgBPhyAdjustId, id)
                    .eq(SgBPhyAdjustImpItem::getQty, BigDecimal.ZERO));
            if (log.isDebugEnabled()) {
                log.debug("成功删除录入明细中调整数量为0的记录数:" + delete + ";");
            }
        }else {
            //删除明细中调整数量为0的记录
            int delete = itemMapper.delete(new UpdateWrapper<SgBPhyAdjustItem>().lambda()
                    .eq(SgBPhyAdjustItem::getSgBPhyAdjustId, id)
                    .eq(SgBPhyAdjustItem::getQty, BigDecimal.ZERO));
            if (log.isDebugEnabled()) {
                log.debug("成功删除明细中调整数量为0的记录数:" + delete + ";");
            }
        }
    }
}
