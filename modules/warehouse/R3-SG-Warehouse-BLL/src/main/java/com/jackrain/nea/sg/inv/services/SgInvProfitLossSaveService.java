/*
package com.jackrain.nea.sg.inv.services;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Lists;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sc.inv.constants.ScConstants;
import com.jackrain.nea.sc.inv.model.request.ScInvProfitLossMarginQueryCmdRequest;
import com.jackrain.nea.sc.inv.model.request.ScInvProfitLossSaveAsyCmdRequest;
import com.jackrain.nea.sc.inv.model.result.ScInvProfitLossQueryResult;
import com.jackrain.nea.sc.inv.model.result.ScInvProfitLossSaveResult;
import com.jackrain.nea.sc.inv.model.table.ScBPrePlUnfshBill;
import com.jackrain.nea.sg.inv.api.SgInvProfitLossQueryCmd;
import com.jackrain.nea.sg.inv.model.request.SgInvProfitLossQueryCmdRequest;
import com.jackrain.nea.sg.inv.model.result.SgInvProfitLossQueryItemResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.utils.AssertUtils;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

*/
/**
 * @author 舒威
 * @since 2019/4/9
 * create at : 2019/4/9 13:27
 *//*

@Slf4j
@Component
public class SgInvProfitLossSaveService {

    */
/**
     * 生成盈亏
     *//*

    ValueHolderV14<ScInvProfitLossSaveResult> saveProfitLoss(ScInvProfitLossSaveAsyCmdRequest model) {
        ValueHolderV14<ScInvProfitLossSaveResult> v14 = new ValueHolderV14<>();
        Long storeId = model.getStoreId();
        String inventoryType = model.getInventoryType();
        Date inventoryDate = model.getInventoryDate();
        Long adjTypeId = model.getAdjTypeId();

        User loginUser = model.getLoginUser();
        AssertUtils.notNull(loginUser, "当前用户未登录！");
        Locale locale = loginUser.getLocale();
        AssertUtils.notNull(storeId, "请选择盘点店仓！", locale);
        AssertUtils.notNull(inventoryType, "请选择盘点类型！", locale);
        AssertUtils.notNull(inventoryDate, "请选择盘点日期！", locale);

        SgInvProfitLossQueryService profitLossQueryService = ApplicationContextHandle.getBean(
                SgInvProfitLossQueryService.class);
        SgInvProfitLossUnfinishQueryService unfinishQueryService = ApplicationContextHandle.getBean(
                SgInvProfitLossUnfinishQueryService.class);

        */
/*TODO 查询是否存在未盈亏盘点单*//*

        ScInvProfitLossMarginQueryCmdRequest queryModel = new ScInvProfitLossMarginQueryCmdRequest();
        BeanUtils.copyProperties(model, queryModel);
        queryModel.setStatus(ScConstants.PAND_UNPOL);
        ValueHolderV14<ScInvProfitLossQueryResult> queryResult = profitLossQueryService.queryProfitLossInventory(queryModel);
        if (ResultCode.FAIL == queryResult.getCode()) {
            v14.setCode(ResultCode.FAIL);
            v14.setMessage(queryResult.getMessage());
            return v14;
        }

        */
/*TODO 查询是否存在未完成单据*//*

        ValueHolderV14<PageInfo<ScBPrePlUnfshBill>> unfinishQueryResult = unfinishQueryService.queryUnfinishProfitLoss(queryModel);
        if (ResultCode.SUCCESS == unfinishQueryResult.getCode()) {
            v14.setCode(ResultCode.FAIL);
            v14.setMessage("存在未完成单据!");
            return v14;
        }

        ScInvProfitLossQueryResult scInvProfitLossQueryResult = queryResult.getData();
        List<String> billNos = Lists.newArrayList();
        List<Long> ids = Lists.newArrayList();
        scInvProfitLossQueryResult.getResults().forEach(o -> {
            HashMap map = (HashMap) o;
            billNos.add((String) map.get("BILL_NO"));
            ids.add((Long) map.get("ID"));
        });
        String remark = "由盘点单" + StringUtils.join(billNos, "\\") + "生成盈亏生成!";

         */
/*TODO 分批查询盈亏明细*//*

        List<JSONObject> adjItems = Lists.newArrayList();
        Object o = ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                ScInvProfitLossGenarateCmd.class.getName(), "sg", "1.0");
        if (null != o) {
            //先执行一次  获取totalPages
            Long totalPages = null;
            ScInvProfitLossQueryCmdRequest sgQueryModel = new ScInvProfitLossQueryCmdRequest();
            BeanUtils.copyProperties(model, sgQueryModel);
            sgQueryModel.setCurrentPage(1);
            sgQueryModel.setRange(500);
            ValueHolderV14<ScInvProfitLossQueryStorageResult> itemQueryResult =
                    ((ScInvProfitLossGenarateCmd) o).queryProfitLossItemInventory(sgQueryModel);
            if (null == itemQueryResult || itemQueryResult.getCode() == ResultCode.FAIL) {
                AssertUtils.logAndThrow("查询预盈亏明细失败！", locale);
            } else {
                ScInvProfitLossQueryStorageResult sgItemResult = itemQueryResult.getData();
                List profItems = sgItemResult.getList();
                totalPages = sgItemResult.getTotalPages();
                adjItems.addAll(getAdjItems(profItems));
            }

            if (totalPages > 1) {
                for (int i = 2; i < totalPages; i++) {
                    sgQueryModel.setCurrentPage(i);
                    itemQueryResult = ((ScInvProfitLossGenarateCmd) o).queryProfitLossItemInventory(sgQueryModel);
                    if (null == itemQueryResult || itemQueryResult.getCode() == ResultCode.FAIL) {
                        AssertUtils.logAndThrow("查询预盈亏明细失败！", locale);
                    } else {
                        ScInvProfitLossQueryStorageResult sgItemResult = itemQueryResult.getData();
                        List profItems = sgItemResult.getList();
                        adjItems.addAll(getAdjItems(profItems));
                    }
                }
            }
        } else {
            AssertUtils.logAndThrow("未获取到对应的bean", locale);
        }

        Boolean isCreateInvadj = false;
        for (JSONObject adjItem : adjItems) {
            BigDecimal qty = adjItem.getBigDecimal("QTY");
            qty = qty.stripTrailingZeros();
            if (null != qty && !BigDecimal.ZERO.equals(qty)) {
                isCreateInvadj = true;
                break;
            }
        }

        JSONObject param = getAdjParam(storeId, ids, adjTypeId, remark, adjItems);
        ScInvProfitLossSaveResult saveResult=new ScInvProfitLossSaveResult();
        saveResult.setIsCreateInvadj(isCreateInvadj);
        saveResult.setRow(param);
        saveResult.setIds(ids);

        v14.setCode(ResultCode.SUCCESS);
        v14.setMessage("success");
        v14.setData(saveResult);
        return v14;
    }

    */
/**
     * 封装库存调整单入参
     *//*

    private JSONObject getAdjParam(Long storeId, List<Long> ids, Long adjTypeId, String remark, List<JSONObject> adjItems) {
        JSONObject mainObj = new JSONObject();
        mainObj.put("BILL_DATE", System.currentTimeMillis());
        mainObj.put("CP_C_STORE_ID", storeId);
//        mainObj.put("STORAGE_DATE",); //库存日期待定
        mainObj.put("SC_B_INVENTORY_ID", ids); //盘点单号待定
        mainObj.put("ADJUST_TYPE", adjTypeId);
//        mainObj.put("MONTH_INVENTORY_DATE",); //计划月盘日
        mainObj.put("REMARK", remark);

        JSONObject fixColumn = new JSONObject();
        fixColumn.put("SC_B_STORAGE_ADJUST", mainObj);
        fixColumn.put("SC_B_STORAGE_ADJUST_ITEM", adjItems);

        JSONObject param = new JSONObject();
        param.put("objid", -1);
        param.put("table", "SC_B_STORAGE_ADJUST");
//        param.put("isPassErr", true);
        param.put("fixcolumn", fixColumn);
        return param;
    }


    */
/**
     * 封装库存调整单明细
     *//*

    private List<JSONObject> getAdjItems(List<JSONObject> profItems) {
        List<JSONObject> adjItems = Lists.newArrayList();
        if (CollectionUtils.isNotEmpty(profItems)) {
            for (JSONObject profItem : profItems) {
                Long ps_c_sku_id = profItem.getLong("ps_c_sku_id");
                String ps_c_sku_ecode = profItem.getString("ps_c_sku_ecode");
                Long ps_c_pro_id = profItem.getLong("ps_c_pro_id");
                String ps_c_pro_ecode = profItem.getString("ps_c_pro_ecode");
                String ps_c_pro_ename = profItem.getString("ps_c_pro_ename");
                Long ps_c_clr_id = profItem.getLong("ps_c_clr_id");
                String ps_c_clr_ecode = profItem.getString("ps_c_clr_ecode");
                String ps_c_clr_ename = profItem.getString("ps_c_clr_ename");
                Long ps_c_size_id = profItem.getLong("ps_c_size_id");
                String ps_c_size_ecode = profItem.getString("ps_c_size_ecode");
                String ps_c_size_ename = profItem.getString("ps_c_size_ename");
                BigDecimal price_list = null == profItem.getBigDecimal("price_list") ?
                        BigDecimal.ZERO : profItem.getBigDecimal("price_list");
                BigDecimal qty_bill = null == profItem.getBigDecimal("sum_qty_bill") ?
                        BigDecimal.ZERO : profItem.getBigDecimal("sum_qty_bill"); //实际盘点数量
                BigDecimal qty_stock = null == profItem.getBigDecimal("qty_stock") ?
                        BigDecimal.ZERO : profItem.getBigDecimal("qty_stock");//账面库存
                BigDecimal qty = qty_bill.subtract(qty_stock);//差异数量(调整数量)
                BigDecimal amt_list = price_list.multiply(qty);//差异金额

                JSONObject adjItem = new JSONObject();
                adjItem.put("ID", -1L);
                adjItem.put("PS_C_PRO_ID", ps_c_pro_id);
                adjItem.put("PS_C_PRO_ECODE", ps_c_pro_ecode);
                adjItem.put("PS_C_PRO_ENAME", ps_c_pro_ename);
                adjItem.put("PS_C_SKU_ID", ps_c_sku_id);
                adjItem.put("PS_C_SKU_ECODE", ps_c_sku_ecode);
                adjItem.put("PS_C_CLR_ID", ps_c_clr_id);
                adjItem.put("PS_C_CLR_ECODE", ps_c_clr_ecode);
                adjItem.put("PS_C_CLR_ENAME", ps_c_clr_ename);
                adjItem.put("PS_C_SIZE_ID", ps_c_size_id);
                adjItem.put("PS_C_SIZE_ECODE", ps_c_size_ecode);
                adjItem.put("PS_C_SIZE_ENAME", ps_c_size_ename);
                adjItem.put("PRICE_LIST", price_list);
                adjItem.put("QTY", qty);
                adjItem.put("AMT_LIST", amt_list);
                adjItems.add(adjItem);
            }
        }
        return adjItems;
    }
}
*/
