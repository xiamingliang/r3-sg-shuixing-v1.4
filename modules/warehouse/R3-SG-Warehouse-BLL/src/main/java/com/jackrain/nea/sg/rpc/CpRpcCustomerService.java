package com.jackrain.nea.sg.rpc;

import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.cpext.api.CpStoreCustomerQueryCmd;
import com.jackrain.nea.cpext.model.table.CpCustomer;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 * @author: xiWen.z
 * create at: 2019/12/20 0020
 */
@Slf4j
@Component
public class CpRpcCustomerService {

    @Reference(version = "1.0", group = "cp-ext")
    private CpStoreCustomerQueryCmd cpStoreCustomerQueryCmd;

    /**
     * 查询经销商档案
     *
     * @param id id
     * @return 经销商
     */
    public ValueHolderV14<CpCustomer> queryCustomerById(Long id) {
        ValueHolderV14 vh = new ValueHolderV14<>();
        try {
            CpCustomer cpCustomer = cpStoreCustomerQueryCmd.getCpCustomer(id);
            if (Objects.isNull(cpCustomer)) {
                vh.setCode(ResultCode.FAIL);
                return vh;
            }
            vh.setCode(ResultCode.SUCCESS);
            vh.setData(cpCustomer);
            return vh;
        } catch (Exception e) {
            e.printStackTrace();
            log.error(this.getClass().getName() + " Rpc查询经销商异常", e.getMessage());
            vh.setCode(ResultCode.FAIL);
            vh.setMessage(" Rpc查询经销商异常" + e.getMessage());
            return vh;
        }
    }
}
