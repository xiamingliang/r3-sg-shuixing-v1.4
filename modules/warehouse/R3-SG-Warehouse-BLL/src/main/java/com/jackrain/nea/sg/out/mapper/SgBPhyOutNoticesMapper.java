package com.jackrain.nea.sg.out.mapper;

import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.in.common.SgInNoticeConstants;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutNotices;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface SgBPhyOutNoticesMapper extends ExtentionMapper<SgBPhyOutNotices> {


    /**
     * 分页查询出库通知单
     */
    @Select("SELECT * FROM " + SgConstants.SG_B_PHY_OUT_NOTICES + " ORDER BY ID LIMIT #{page} OFFSET #{offset}")
    List<SgBPhyOutNotices> selectListByPage(@Param("page") int page, @Param("offset") int offset);


    /**
     * 查询出库通知单
     * 单据状态为 待出库
     * 是否获取电子面单为 是
     * 电子面单获取状态为 部分成功
     * 电子面大获取次数 小于5
     * 获取电子面大执行的时间间隔 15分钟
     *
     * @param status
     * @param isewaybill
     * @param ewaystatus
     * @return
     */
    @Select(" select * from  sg_b_phy_out_notices " +
            " where bill_status = #{status} " +
            " and isactive = 'Y' " +
            " and is_enableewaybill = #{isewaybill} " +
            " and ewaybill_status = #{ewaystatus} " +
            " and reserve_bigint03 < #{facenumber} " +
            " and round(date_part('epoch', current_timestamp - to_timestamp(reserve_varchar01,'yyyy-MM-dd hh24:mi:ss'))::NUMERIC / 60) > #{minute} " +
            " limit #{limitsize} offset #{offsetsize}")
    List<SgBPhyOutNotices> selectListByParam(@Param("status") int status,
                                             @Param("isewaybill") int isewaybill,
                                             @Param("ewaystatus") int ewaystatus,
                                             @Param("facenumber") int facenumber,
                                             @Param("minute") int minute,
                                             @Param("limitsize") int limitsize,
                                             @Param("offsetsize") int offsetsize);


    @Select(" select count(1) from  sg_b_phy_out_notices " +
            " where bill_status = #{status} " +
            " and isactive = 'Y' " +
            " and is_enableewaybill = #{isewaybill} " +
            " and ewaybill_status = #{ewaystatus} " +
            " and reserve_bigint03 < #{facenumber} " +
            " and round(date_part('epoch', current_timestamp - to_timestamp(reserve_varchar01,'yyyy-MM-dd hh24:mi:ss'))::NUMERIC / 60) > #{minute} ")
    int selectOutNoticesCount(@Param("status") int status,
                              @Param("isewaybill") int isewaybill,
                              @Param("ewaystatus") int ewaystatus,
                              @Param("facenumber") int facenumber,
                              @Param("minute") int minute);


    @Select("SELECT * FROM SG_B_PHY_OUT_NOTICES WHERE IS_PASS_WMS = 1 AND BILL_STATUS = 3 AND IS_DIFF_DEAL = 0 AND ISACTIVE = 'Y' AND (WMS_STATUS = 0 OR (WMS_STATUS = 3 AND WMS_FAIL_COUNT < 6))")
    List<SgBPhyOutNotices> selectOutNoticesToWmsForCancelDiff();

    @Update("UPDATE sg_b_phy_out_notices " +
            "SET wms_status = " + SgInNoticeConstants.WMS_UPLOAD_STATUTS_FAIL + "," +
            " wms_fail_reason = '${str}'," +
            " wms_fail_count = wms_fail_count + 1 " +
            "WHERE " +
            "ID IN (${ids});")
    int batchUpdateOutNoticesByWMS(@Param("ids") String ids, @Param("str") String errMsg);
}