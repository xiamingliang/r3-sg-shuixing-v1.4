package com.jackrain.nea.sg.pack.filter;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.sg.basic.utils.StorageUtils;
import com.jackrain.nea.sg.pack.mapper.SgBTeusPackItemMapper;
import com.jackrain.nea.sg.pack.mapper.SgBTeusPackMapper;
import com.jackrain.nea.sg.pack.model.table.SgBTeusPack;
import com.jackrain.nea.sg.pack.model.table.SgBTeusPackItem;
import com.jackrain.nea.tableService.MainTableRecord;
import com.jackrain.nea.tableService.TableServiceContext;
import com.jackrain.nea.tableService.filter.BaseFilter;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author zhoulisnheng
 * @since 2019/11/27 18:04
 * desc: 拆箱单明细删除
 */
@Slf4j
@Component
public class SgTeusPackDelFilter extends BaseFilter {

    @Override
    public void after(TableServiceContext context, MainTableRecord row) {

        Long objId = row.getId();
        SgBTeusPackItemMapper packItemMapper = ApplicationContextHandle.getBean(SgBTeusPackItemMapper.class);
        SgBTeusPackMapper packMapper = ApplicationContextHandle.getBean(SgBTeusPackMapper.class);

        List<SgBTeusPackItem> itemList = packItemMapper.selectList(new QueryWrapper<SgBTeusPackItem>().lambda()
                .eq(SgBTeusPackItem::getSgBTeusPackId, objId));

        // 计算总箱内数量
        BigDecimal countTeus = itemList.stream()
                .map(SgBTeusPackItem::getQty).reduce(BigDecimal.ZERO, BigDecimal::add);

        // 计算总装箱箱数
        Map<Long, List<SgBTeusPackItem>> collect = itemList.stream().collect(Collectors.groupingBy(SgBTeusPackItem::getPsCTeusId));
        BigDecimal countPack = new BigDecimal(collect.keySet().size());

        SgBTeusPack sgBTeusPack = new SgBTeusPack();
        sgBTeusPack.setId(objId);
        sgBTeusPack.setQtyTotPack(countPack);
        sgBTeusPack.setQtyTotTeus(countTeus);
        StorageUtils.setBModelDefalutDataByUpdate(sgBTeusPack, context.getUser());
        packMapper.updateById(sgBTeusPack);
    }




}
