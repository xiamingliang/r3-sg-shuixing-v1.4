package com.jackrain.nea.sg.in.services;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.cpext.utils.ValueHolderUtils;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.ip.api.qimen.QimenOrderCancelCmd;
import com.jackrain.nea.ip.model.qimen.QimenOrderCancelModel;
import com.jackrain.nea.sg.basic.common.R3ParamConstants;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.mapper.CpCPhyWarehouseMapper;
import com.jackrain.nea.sg.basic.model.table.CpCPhyWarehouse;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.in.common.SgInConstants;
import com.jackrain.nea.sg.in.common.SgInNoticeConstants;
import com.jackrain.nea.sg.in.mapper.SgBPhyInNoticesMapper;
import com.jackrain.nea.sg.in.model.request.SgPhyInNoticesWMSBackRequest;
import com.jackrain.nea.sg.in.model.table.SgBPhyInNotices;
import com.jackrain.nea.sg.out.utils.SgWarehouseESUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author: 舒威
 * @since: 2019/9/4
 * create at : 2019/9/4 10:15
 */
@Slf4j
@Component
public class SgPhyInNoticesWMSBackService {

    @Autowired
    private SgBPhyInNoticesMapper noticesMapper;

    @Autowired
    private SgWarehouseESUtils warehouseESUtils;

    public ValueHolder recallSgPhyInNoticesWMS(SgPhyInNoticesWMSBackRequest request) throws NDSException {

        User user = request.getUser();
        AssertUtils.notNull(user, "用户信息不能为空！");

        JSONArray ids = request.getIds();
        if (CollectionUtils.isEmpty(ids)) {
            AssertUtils.logAndThrow("请选择数据！", user.getLocale());
        }

        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ".debug, param:" + JSONObject.toJSONString(request));
        }

        ValueHolder vh = new ValueHolder();

        // 单对象页面返回
        if (request.getIsObj()) {
            try {
                ValueHolderV14 cancelWMSResult = cancelWMS(user, ids.getLong(0));
                vh.put(R3ParamConstants.CODE, cancelWMSResult.getCode());
                vh.put(R3ParamConstants.MESSAGE, cancelWMSResult.getMessage());
            } catch (Exception e) {
                log.error("从WMS撤回接口异常:" + e.getMessage());
                vh.put(R3ParamConstants.CODE, ResultCode.FAIL);
                vh.put(R3ParamConstants.MESSAGE, "从WMS撤回接口异常:" + e.getMessage());
            }
            return vh;
        }

        JSONArray errorArr = new JSONArray();
        Integer accSuccess = 0, accFailed = 0;
        for (int i = 0; i < ids.size(); i++) {
            try {
                ValueHolderV14 cancelWMSResult = cancelWMS(user, ids.getLong(i));
                if (cancelWMSResult.isOK()) {
                    accSuccess++;
                } else {
                    throw new NDSException(Resources.getMessage(cancelWMSResult.getMessage()));
                }
            } catch (Exception e) {
                e.printStackTrace();
                log.error("objid:" + ids.getLong(i) + ",从WMS撤回接口异常:" + e.getMessage());
                JSONObject errorDate = new JSONObject();
                errorDate.put(R3ParamConstants.CODE, ResultCode.FAIL);
                errorDate.put(R3ParamConstants.OBJID, ids.getLong(i));
                errorDate.put(R3ParamConstants.MESSAGE, "从WMS撤回失败:" + e.getMessage());
                errorArr.add(errorDate);
                accFailed++;
            }
        }

        String message = "从WMS撤回成功记录数：" + accSuccess;
        if (CollectionUtils.isNotEmpty(errorArr)) {
            message += "，从WMS撤回失败记录数：" + accFailed;
            return ValueHolderUtils.fail(message, errorArr);
        } else {
            return ValueHolderUtils.success(message);
        }
    }


    /**
     * 取消WMS
     *
     * @param user
     * @param id   入库通知单id
     * @return
     */
    public ValueHolderV14 cancelWMS(User user, Long id) {
        ValueHolderV14 vh = new ValueHolderV14(ResultCode.SUCCESS, "从WMS撤回成功");
        SgBPhyInNotices notices = noticesMapper.selectById(id);
        if (notices == null) {
            vh.setCode(ResultCode.FAIL);
            vh.setMessage("单据不存在");
            return vh;
        }
        if (!(notices.getIsPassWms() == SgInConstants.NOTICE_IS_PASS_WMS_YES &&
                notices.getBillStatus() == SgInNoticeConstants.BILL_STATUS_IN_PENDING)) {
            vh.setCode(ResultCode.FAIL);
            vh.setMessage("单据状态异常，不允许从WMS撤回");
            return vh;
        }
        if (notices.getWmsStatus() == SgInNoticeConstants.WMS_UPLOAD_STATUTS_NO ||
                notices.getWmsStatus() == SgInNoticeConstants.WMS_UPLOAD_STATUTS_FAIL) {
            vh.setCode(ResultCode.FAIL);
            vh.setMessage("单据传WMS状态异常，不允许从WMS撤回");
            return vh;
        }
        QimenOrderCancelCmd orderCancelCmd = (QimenOrderCancelCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(), QimenOrderCancelCmd.class.getName(), "ip", "1.4.0");

        CpCPhyWarehouseMapper warehouseMapper = ApplicationContextHandle.getBean(CpCPhyWarehouseMapper.class);
        CpCPhyWarehouse warehouse = warehouseMapper.selectById(notices.getCpCPhyWarehouseId());

        QimenOrderCancelModel cancelModel = new QimenOrderCancelModel();
        cancelModel.setOperateUser(user);
        cancelModel.setWarehouseCode(warehouse != null ? warehouse.getWmsWarehouseCode() : "");
        cancelModel.setOrderCode(notices.getBillNo());
        cancelModel.setOrderType(getOrderType(notices.getSourceBillType()));
        cancelModel.setCustomerId(warehouse != null ? warehouse.getWmsAccount() : "");
        cancelModel.setOwnerCode(notices.getGoodsOwner());
        cancelModel.setOrderId(notices.getWmsBillNo());
        ValueHolderV14 qimenResult = orderCancelCmd.cancelQimenOrder(cancelModel);
        if (qimenResult.isOK()) {
            SgBPhyInNotices update = new SgBPhyInNotices();
            update.setId(notices.getId());
            update.setIsPassWms(SgInConstants.NOTICE_IS_PASS_WMS_NO);
            update.setWmsStatus((long) SgInNoticeConstants.WMS_UPLOAD_STATUTS_NO);
            StorageESUtils.setBModelDefalutDataByUpdate(update, user);
            int i = noticesMapper.updateById(update);
            if (i > 0) {
                log.info("从WMS撤回成功,更新通知单状态成功!");
            } else {
                vh.setCode(ResultCode.FAIL);
                vh.setMessage("从WMS撤回失败,更新通知单状态失败!");
                return vh;
            }
            //推送es
            warehouseESUtils.pushESByInNotices(id, false, false, null, noticesMapper, null);
            return vh;
        } else {
            vh.setCode(ResultCode.FAIL);
            vh.setMessage("从WMS撤回失败," + qimenResult.getMessage());
            return vh;
        }
    }

    /**
     * 传奇门类型转换
     *
     * @param sourceType
     * @return
     */
    public String getOrderType(Integer sourceType) {
        String orderType = SgInNoticeConstants.QM_BILL_TYPE_OTHER_IN;
        switch (sourceType) {
            case SgConstantsIF.BILL_TYPE_SALE_REF:
                orderType = SgInNoticeConstants.QM_BILL_TYPE_B2B_IN;
                break;
            case SgConstantsIF.BILL_TYPE_PUR:
                orderType = SgInNoticeConstants.QM_BILL_TYPE_PURCHASE_IN;
                break;
            case SgConstantsIF.BILL_TYPE_TRANSFER:
                orderType = SgInNoticeConstants.QM_BILL_TYPE_TRANSFER_IN;
                break;
            default:
                orderType = SgInNoticeConstants.QM_BILL_TYPE_OTHER_IN;
                break;
        }
        return orderType;
    }

}
