package com.jackrain.nea.sg.inv.services;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Lists;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.inv.model.request.ScInvProfitLossMarginQueryCmdRequest;
import com.jackrain.nea.sg.inv.model.result.ScInvProfitLossQueryItemResult;
import com.jackrain.nea.sg.inv.model.result.ScInvProfitLossMarginQueryResult;
import com.jackrain.nea.sg.inv.model.table.ScBInventory;
import com.jackrain.nea.sg.inv.model.table.ScBPrePlUnfshBill;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 * @author 舒威
 * @since 2019/3/27
 * create at : 2019/3/27 16:56
 */
@Slf4j
@Component
public class ScInvProfitLossMarginQueryService {

    @Autowired
    private ScInvProfitLossQueryService profitLossQueryService;

    @Autowired
    private ScInvProfitLossUnfinishQueryService unfinishQueryService;

    @Autowired
    private ScInvProfitLossItemQueryService itemQueryService;

    ValueHolderV14<ScInvProfitLossMarginQueryResult> queryProfitLoss(ScInvProfitLossMarginQueryCmdRequest model) {
        if (log.isDebugEnabled()) {
            log.debug("Start ScInvProfitLossMarginQueryService.queryProfitLoss. Receive:request:{}", JSONObject.toJSONString(model));
        }
        ValueHolderV14<ScInvProfitLossMarginQueryResult> v14 = new ValueHolderV14<>();
        JSONObject pandObj = new JSONObject();
        JSONObject unfinishObj = new JSONObject();
        JSONObject profItemObj = new JSONObject();

        try {
            ValueHolderV14<PageInfo<ScBInventory>> queryResult = profitLossQueryService.queryProfitLossInventory(model);
            if (queryResult.isOK()) {
                PageInfo<ScBInventory> inventoryPageInfo = queryResult.getData();
                pandObj.put("pand_list", inventoryPageInfo.getList());
                pandObj.put("total", inventoryPageInfo.getTotal());
            } else {
                v14.setCode(ResultCode.FAIL);
                v14.setMessage(queryResult.getMessage());
                return v14;
            }

            ValueHolderV14<PageInfo<ScBPrePlUnfshBill>> unfinishQueryResult = unfinishQueryService.queryUnfinishProfitLoss(model);
            PageInfo<ScBPrePlUnfshBill> pageInfo = unfinishQueryResult.getData();
            unfinishObj.put("unfinish_list", pageInfo == null ? Lists.newArrayList() : pageInfo.getList());
            unfinishObj.put("total", pageInfo == null ? 0L : pageInfo.getTotal());
            unfinishObj.put("QUERY_ID", pageInfo == null ? "" :
                    CollectionUtils.isNotEmpty(pageInfo.getList()) ? pageInfo.getList().get(0).getQueryId() : "");

            ValueHolderV14<ScInvProfitLossQueryItemResult> itemQueryResult = itemQueryService.queryProfitLossItem(model);
            if (itemQueryResult.isOK()) {
                ScInvProfitLossQueryItemResult itemQueryResultData = itemQueryResult.getData();
                profItemObj.put("tHead", itemQueryResultData.getTHead());
                profItemObj.put("itemStorageResult", itemQueryResultData.getItemStorageResult());
                profItemObj.put("itemTeusStorageResult", itemQueryResultData.getItemTeusStorageResult());
            } else {
                v14.setCode(ResultCode.FAIL);
                v14.setMessage(itemQueryResult.getMessage());
                return v14;
            }
        } catch (Exception e) {
            e.printStackTrace();
            AssertUtils.getMessageExtra("预盈亏查询失败！", e);
        }

        ScInvProfitLossMarginQueryResult marginQueryResult = new ScInvProfitLossMarginQueryResult();
        marginQueryResult.setPand_result(pandObj);
        marginQueryResult.setUnfish_result(unfinishObj);
        marginQueryResult.setProfit_item_result(profItemObj);

        v14.setCode(ResultCode.SUCCESS);
        v14.setMessage("success");
        v14.setData(marginQueryResult);
        return v14;
    }
}
