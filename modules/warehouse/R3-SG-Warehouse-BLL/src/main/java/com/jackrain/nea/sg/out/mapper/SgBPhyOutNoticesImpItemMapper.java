package com.jackrain.nea.sg.out.mapper;

import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutNoticesImpItem;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface SgBPhyOutNoticesImpItemMapper extends ExtentionMapper<SgBPhyOutNoticesImpItem> {

    @Select("SELECT * from sg_b_phy_out_notices_imp_item WHERE SG_B_PHY_OUT_NOTICES_ID in " +
            "(SELECT id FROM sg_b_phy_out_notices WHERE SOURCE_BILL_ID=#{sourceBillId} and SOURCE_BILL_TYPE=#{sourceBillType});")
    List<SgBPhyOutNoticesImpItem> selectListBySourceBill(@Param("sourceBillId") Long sourceBillId, @Param("sourceBillType") Integer sourceBillType);
}