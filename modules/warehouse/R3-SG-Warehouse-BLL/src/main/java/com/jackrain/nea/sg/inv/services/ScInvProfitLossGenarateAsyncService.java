package com.jackrain.nea.sg.inv.services;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.jackrain.nea.model.util.ModelUtil;
import com.jackrain.nea.oc.basic.common.OcBasicConstants;
import com.jackrain.nea.sg.adjust.common.SgAdjustConstants;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.basic.utils.StorageUtils;
import com.jackrain.nea.sg.inv.constants.SgInvConstants;
import com.jackrain.nea.sg.inv.mapper.ScBInventoryMapper;
import com.jackrain.nea.sg.inv.mapper.ScBProfitImpItemMapper;
import com.jackrain.nea.sg.inv.mapper.ScBProfitMapper;
import com.jackrain.nea.sg.inv.model.request.ScInvProfitLossMarginQueryCmdRequest;
import com.jackrain.nea.sg.inv.model.result.ScInvProfitLossQueryItemResult;
import com.jackrain.nea.sg.inv.model.table.ScBProfit;
import com.jackrain.nea.sg.inv.model.table.ScBProfitImpItem;
import com.jackrain.nea.sg.phyadjust.common.SgPhyAdjustConstantsIF;
import com.jackrain.nea.sg.phyadjust.model.request.SgPhyAdjustBillSaveRequest;
import com.jackrain.nea.sg.phyadjust.model.request.SgPhyAdjustImpItemRequest;
import com.jackrain.nea.sg.phyadjust.model.request.SgPhyAdjustSaveRequest;
import com.jackrain.nea.sg.phyadjust.services.SgPhyAdjSaveAndAuditService;
import com.jackrain.nea.sg.send.utils.SgStoreUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;

/**
 * @author 舒威
 * @since 2019/12/5
 * create at : 2019/12/5 19:44
 */
@Slf4j
@Component
public class ScInvProfitLossGenarateAsyncService {

    @Async("esAsync")
    public void genarateProfitLoss(ScInvProfitLossMarginQueryCmdRequest model, HashMap<String, String> inventoryMap) {
        String billNos = inventoryMap.get("billNos");
        if (log.isDebugEnabled()) {
            log.debug("Start ScInvProfitLossGenarateAsyncService.genarateProfitLoss BillNos:{};", billNos);
        }
        long start = System.currentTimeMillis();

        //查询预盈亏明细
        ScInvProfitLossQueryItemResult queryItemResult = queryProfitLossItems(model);
        List teusList = queryItemResult.getItemTeusStorageResult().getTeusList();
        List itemList = queryItemResult.getItemStorageResult().getItemList();
        if (CollectionUtils.isEmpty(teusList) && CollectionUtils.isEmpty(itemList)) {
            AssertUtils.logAndThrow("盈亏明细不能为空！");
        }
        ScInvProfitLossGenarateAsyncService service = ApplicationContextHandle.getBean(ScInvProfitLossGenarateAsyncService.class);
        service.genarateProfitBills(model, inventoryMap, teusList, itemList, billNos);
        if (log.isDebugEnabled()) {
            log.debug("Finish ScInvProfitLossGenarateAsyncService.genarateProfitLoss Spend Time:{}ms;", start - System.currentTimeMillis());
        }
    }

    @Transactional
    public void genarateProfitBills(ScInvProfitLossMarginQueryCmdRequest model, HashMap<String, String> inventoryMap, List teusList, List itemList, String billNos) {
        //生成库存调整单
        genaratePhyAdjust(model, inventoryMap, teusList, itemList);
        //生成盈亏单
        genarateProfit(model, inventoryMap, teusList, itemList);
        //更新盘点单状态已盈亏
        ScBInventoryMapper inventoryMapper = ApplicationContextHandle.getBean(ScBInventoryMapper.class);
        List<String> billNoList = Lists.newArrayList();
        for (String billNo : billNos.split(",")) {
            billNoList.add("'" + billNo + "'");
        }
        String join = StringUtils.join(billNoList, ",");
        if (log.isDebugEnabled()) {
            log.debug("Update Inventory BillNos:{};", join);
        }
        inventoryMapper.updateInventoryByProfitLoss(SgInvConstants.PAND_POL, model.getLoginUser(), join);
    }


    public ScInvProfitLossQueryItemResult queryProfitLossItems(ScInvProfitLossMarginQueryCmdRequest model) {
        long start = System.currentTimeMillis();
        ScInvProfitLossItemService profitLossItemService = ApplicationContextHandle.getBean(ScInvProfitLossItemService.class);
        ValueHolderV14<ScInvProfitLossQueryItemResult> queryResult = profitLossItemService.queryProfitLossItemInventory(model);
        if (log.isDebugEnabled()) {
            log.debug("Finish ScInvProfitLossGenarateAsyncService.queryProfitLossItems Return.Results:{},Spend Time:{}ms;",
                    queryResult, start - System.currentTimeMillis());
        }
        if (!queryResult.isOK() || queryResult.getData() == null) {
            AssertUtils.logAndThrow("查询预盈亏明细失败！");
        }
        return queryResult.getData();
    }

    public void genaratePhyAdjust(ScInvProfitLossMarginQueryCmdRequest model, HashMap<String, String> inventoryMap, List teusList, List itemList) {
        long start = System.currentTimeMillis();
        SgPhyAdjustBillSaveRequest billSaveRequest = new SgPhyAdjustBillSaveRequest();
        // 主表信息
        SgPhyAdjustSaveRequest adjustSaveRequest = new SgPhyAdjustSaveRequest();
        adjustSaveRequest.setCpCPhyWarehouseId(model.getWarehouseId());
        adjustSaveRequest.setCpCPhyWarehouseEname(inventoryMap.get("warehouseEname"));
        adjustSaveRequest.setCpCPhyWarehouseEcode(inventoryMap.get("warehouseEcode"));
        adjustSaveRequest.setBillType(SgPhyAdjustConstantsIF.BILL_TYPE_DIFF);
        adjustSaveRequest.setSgBAdjustPropId(SgPhyAdjustConstantsIF.ADJUST_PROP_PROFIT);
        adjustSaveRequest.setRemark("由盘点单:" + inventoryMap.get("billNos") + "盈亏生成！");

        // 封装录入明细
        List<SgPhyAdjustImpItemRequest> impItemRequestList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(itemList)) {
            for (Object o : itemList) {
                JSONObject item = (JSONObject) o;
                SgPhyAdjustImpItemRequest impItem = new SgPhyAdjustImpItemRequest();
                impItem.setId(-1L);
                impItem.setIsTeus(OcBasicConstants.IS_MATCH_SIZE_N);
                impItem.setPsCSkuId(item.getLong("PS_C_SKU_ID"));
                impItem.setPsCSkuEcode(item.getString("PS_C_SKU_ECODE"));
                impItem.setPsCProId(item.getLong("PS_C_PRO_ID"));
                impItem.setPsCProEcode(item.getString("PS_C_PRO_ECODE"));
                impItem.setPsCProEname(item.getString("PS_C_PRO_ENAME"));
                impItem.setQty(item.getBigDecimal("QTY_DIFF"));
                impItemRequestList.add(impItem);
            }
        }

        if (CollectionUtils.isNotEmpty(teusList)) {
            for (Object o : teusList) {
                JSONObject item = (JSONObject) o;
                SgPhyAdjustImpItemRequest impItem = new SgPhyAdjustImpItemRequest();
                impItem.setId(-1L);
                impItem.setIsTeus(OcBasicConstants.IS_MATCH_SIZE_Y);
                impItem.setPsCTeusId(item.getLong("PS_C_TEUS_ID"));
                impItem.setPsCTeusEcode(item.getString("PS_C_TEUS_ECODE"));
                impItem.setPsCMatchsizeId(item.getLong("PS_C_MATCHSIZE_ID"));
                impItem.setPsCMatchsizeEcode(item.getString("PS_C_MATCHSIZE_ECODE"));
                impItem.setPsCMatchsizeEname(item.getString("PS_C_MATCHSIZE_ENAME"));
                impItem.setPsCProId(item.getLong("PS_C_PRO_ID"));
                impItem.setPsCProEcode(item.getString("PS_C_PRO_ECODE"));
                impItem.setPsCProEname(item.getString("PS_C_PRO_ENAME"));
                impItem.setQty(item.getBigDecimal("QTY_DIFF"));
                impItemRequestList.add(impItem);
            }
        }
        billSaveRequest.setPhyAdjust(adjustSaveRequest);
        billSaveRequest.setImpItems(impItemRequestList);
        billSaveRequest.setLoginUser(model.getLoginUser());
        billSaveRequest.setObjId(-1L);
        try {
            SgPhyAdjSaveAndAuditService saveAndAuditService = ApplicationContextHandle.getBean(SgPhyAdjSaveAndAuditService.class);
            ValueHolderV14 saveAndAudit = saveAndAuditService.saveAndAudit(billSaveRequest);
            if (log.isDebugEnabled()) {
                log.debug("Finish ScInvProfitLossGenarateAsyncService.saveAndAuditPhyAdjustService Return.Results:{},Spend Time:{}ms;",
                        saveAndAudit, start - System.currentTimeMillis());
            }
            if (!saveAndAudit.isOK()) {
                AssertUtils.logAndThrow(saveAndAudit.getMessage());
            }
        } catch (Exception e) {
            AssertUtils.logAndThrow("新增并审核库存调整单失败！" + e.getMessage());
        }
    }

    public void genarateProfit(ScInvProfitLossMarginQueryCmdRequest model, HashMap<String, String> inventoryMap, List teusList, List itemList) {
        ScBProfit profit = new ScBProfit();
        profit.setId(ModelUtil.getSequence(SgConstants.SC_B_PROFIT));
        //TODO 待配置单据编号
        profit.setBillNo(SgStoreUtils.getBillNo(SgInvConstants.SEQ_SC_B_PROFIT, SgConstants.SC_B_PROFIT.toUpperCase(), profit, model.getLoginUser().getLocale()));
        profit.setBillDate(new Date());
        profit.setStatus(SgInvConstants.STATUS_UNSUBMIT);
        profit.setStatusId((long) model.getLoginUser().getId());
        profit.setStatusEname(model.getLoginUser().getEname());
        profit.setStatusName(model.getLoginUser().getName());
        profit.setStatusTime(new Date());
        profit.setCpCPhyWarehouseId(model.getWarehouseId());
        profit.setCpCPhyWarehouseEcode(inventoryMap.get("warehouseEcode"));
        profit.setCpCPhyWarehouseEname(inventoryMap.get("warehouseEname"));
        profit.setRemark("由盘点单:" + inventoryMap.get("billNos") + "盈亏生成！");
        BigDecimal qtyProfit = Optional.ofNullable(model.getGenarateInfo().getQtyProfit()).orElse(BigDecimal.ZERO);
        BigDecimal qtyLoss = Optional.ofNullable(model.getGenarateInfo().getQtyLoss()).orElse(BigDecimal.ZERO);
        BigDecimal teusQtyProfit = Optional.ofNullable(model.getGenarateInfo().getTeusQtyProfit()).orElse(BigDecimal.ZERO);
        BigDecimal teusQtyLoss = Optional.ofNullable(model.getGenarateInfo().getTeusQtyLoss()).orElse(BigDecimal.ZERO);
        profit.setTotQtyDiff(qtyProfit.add(qtyLoss));
        profit.setTotQtyTeusDiff(teusQtyProfit.add(teusQtyLoss));
        StorageESUtils.setBModelDefalutData(profit, model.getLoginUser());

        //TODO 待补充颜色尺寸
        List<ScBProfitImpItem> impItems = Lists.newArrayList();
        if (CollectionUtils.isNotEmpty(itemList)) {
            for (Object o : itemList) {
                JSONObject item = (JSONObject) o;
                ScBProfitImpItem impItem = new ScBProfitImpItem();
                impItem.setId(ModelUtil.getSequence(SgConstants.SC_B_PROFIT_IMP_ITEM));
                impItem.setScBProfitId(profit.getId());
                impItem.setIsTeus(OcBasicConstants.IS_MATCH_SIZE_N);
                impItem.setPsCSkuId(item.getLong("PS_C_SKU_ID"));
                impItem.setPsCSkuEcode(item.getString("PS_C_SKU_ECODE"));
                impItem.setPsCProId(item.getLong("PS_C_PRO_ID"));
                impItem.setPsCProEcode(item.getString("PS_C_PRO_ECODE"));
                impItem.setPsCProEname(item.getString("PS_C_PRO_ENAME"));
                impItem.setPsCSpec1Id(item.getLong("PS_C_SPEC1_ID"));
                impItem.setPsCSpec1Ecode(item.getString("PS_C_SPEC1_ECODE"));
                impItem.setPsCSpec1Ename(item.getString("PS_C_SPEC1_ENAME"));
                impItem.setPsCSpec2Id(item.getLong("PS_C_SPEC2_ID"));
                impItem.setPsCSpec2Ecode(item.getString("PS_C_SPEC2_ECODE"));
                impItem.setPsCSpec2Ename(item.getString("PS_C_SPEC2_ENAME"));
                impItem.setQtyInventory(item.getBigDecimal("SUM_QTY"));
                impItem.setQtyBook(item.getBigDecimal("QTY_STORAGE"));
                impItem.setQtyDiff(item.getBigDecimal("QTY_DIFF"));
                impItem.setOwnerename(model.getLoginUser().getEname());
                impItem.setModifierename(model.getLoginUser().getEname());
                impItem.setInventoryNature(Math.toIntExact(model.getGenarateInfo().getInventoryProp()));
                StorageUtils.setBModelDefalutData(impItem, model.getLoginUser());
                impItems.add(impItem);
            }
        }

        if (CollectionUtils.isNotEmpty(teusList)) {
            for (Object o : teusList) {
                JSONObject item = (JSONObject) o;
                ScBProfitImpItem impItem = new ScBProfitImpItem();
                impItem.setId(ModelUtil.getSequence(SgConstants.SC_B_PROFIT_IMP_ITEM));
                impItem.setScBProfitId(profit.getId());
                impItem.setIsTeus(OcBasicConstants.IS_MATCH_SIZE_Y);
                impItem.setPsCTeusId(item.getLong("PS_C_TEUS_ID"));
                impItem.setPsCTeusEcode(item.getString("PS_C_TEUS_ECODE"));
                impItem.setPsCMatchsizeId(item.getLong("PS_C_MATCHSIZE_ID"));
                impItem.setPsCMatchsizeEcode(item.getString("PS_C_MATCHSIZE_ECODE"));
                impItem.setPsCMatchsizeEname(item.getString("PS_C_MATCHSIZE_ENAME"));
                impItem.setPsCProId(item.getLong("PS_C_PRO_ID"));
                impItem.setPsCProEcode(item.getString("PS_C_PRO_ECODE"));
                impItem.setPsCProEname(item.getString("PS_C_PRO_ENAME"));
                impItem.setPsCSpec1Id(item.getLong("PS_C_SPEC1_ID"));
                impItem.setPsCSpec1Ecode(item.getString("PS_C_SPEC1_ECODE"));
                impItem.setPsCSpec1Ename(item.getString("PS_C_SPEC1_ENAME"));
                impItem.setQtyInventory(item.getBigDecimal("SUM_QTY"));
                impItem.setQtyBook(item.getBigDecimal("QTY_STORAGE"));
                impItem.setQtyDiff(item.getBigDecimal("QTY_DIFF"));
                impItem.setOwnerename(model.getLoginUser().getEname());
                impItem.setModifierename(model.getLoginUser().getEname());
                impItem.setInventoryNature(Math.toIntExact(model.getGenarateInfo().getInventoryProp()));
                StorageUtils.setBModelDefalutData(impItem, model.getLoginUser());
                impItems.add(impItem);
            }
        }

        ScBProfitMapper profitMapper = ApplicationContextHandle.getBean(ScBProfitMapper.class);
        profitMapper.insert(profit);
        ScBProfitImpItemMapper impItemMapper = ApplicationContextHandle.getBean(ScBProfitImpItemMapper.class);
        SgStoreUtils.batchInsertTeus(impItems, "盈亏单明细", SgConstants.SG_COMMON_ITEM_INSERT_PAGE_SIZE, impItemMapper);
    }
}
