package com.jackrain.nea.sg.inv.services;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.Feature;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.inv.constants.SgInvConstants;
import com.jackrain.nea.sg.inv.mapper.ScBInventoryMapper;
import com.jackrain.nea.sg.inv.model.table.ScBInventory;
import com.jackrain.nea.sg.inv.model.table.ScBInventoryImpItem;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.utils.AssertUtils;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.query.DefaultWebEvent;
import com.jackrain.nea.web.query.QuerySession;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @author: 周琳胜
 * @since: 2019/11/13
 * create at : 2019/11/13 11:20
 */
@Slf4j
@Component
public class ScBInventoryVoidService {

    public ValueHolder scbInventoryVoid(QuerySession session) {
        ValueHolder valueHolder = new ValueHolder();
        DefaultWebEvent event = session.getEvent();
        JSONObject param = JSON.parseObject(JSON.toJSONStringWithDateFormat(event.getParameterValue("param"),
                "yyyy-MM-dd HH:mm:ss", SerializerFeature.WriteMapNullValue), Feature.OrderedField);
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + "入参{}", param.toJSONString());
        }
        User user = session.getUser();
        Long objid = param.getLong("objid");
        ScBInventoryMapper scBInventoryMapper = ApplicationContextHandle.getBean(ScBInventoryMapper.class);
        ScBInventory scBInventory = scBInventoryMapper.selectOne(new QueryWrapper<ScBInventory>().lambda().eq(ScBInventory::getId, objid));
        AssertUtils.notNull(scBInventory, Resources.getMessage("当前记录已不存在！"),user.getLocale());
        Integer polStatus = scBInventory.getPolStatus();
        AssertUtils.cannot(SgInvConstants.PAND_POL.equals(polStatus), Resources.getMessage("当前记录已盈亏，不允许删除明细！", user.getLocale()));
        AssertUtils.cannot(SgInvConstants.PAND_POL_VOID.equals(polStatus), Resources.getMessage("当前记录已作废，不允许删除明细！", user.getLocale()));
        ScBInventory inventory = new ScBInventory();
        inventory.setId(objid);
        inventory.setIsactive(SgConstants.IS_ACTIVE_N);
        inventory.setPolStatus(SgInvConstants.PAND_POL_VOID);
        inventory.setModifierid(Long.valueOf(user.getId()));
        inventory.setModifiername(user.getName());
        inventory.setModifieddate(new Date());
        inventory.setDelTime(new Date());
        inventory.setDelerId(user.getId().longValue());
        inventory.setDelerEname(user.getEname());
        inventory.setDelerName(user.getName());
        int result = scBInventoryMapper.updateById(inventory);
        if (result > 0) {
            //推送ES
            String index = SgInvConstants.SC_B_INVENTORY;
            String type = SgInvConstants.SC_B_INVENTORY_IMP_ITEM;
            ScBInventory bInventory = scBInventoryMapper.selectById(objid);
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + ",作废推ES:"
                        + JSON.toJSONString(bInventory));
            }
            StorageESUtils.pushESBModelByUpdate(bInventory, null, objid,
                    null, index, index, type, null, ScBInventory.class, ScBInventoryImpItem.class, false);

            valueHolder.put("code", ResultCode.SUCCESS);
            valueHolder.put("message", "作废成功！");
        }
        return valueHolder;
    }


}
