package com.jackrain.nea.sg.inv.services;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.config.SgStorageBoxConfig;
import com.jackrain.nea.sg.inv.constants.SgInvConstants;
import com.jackrain.nea.sg.inv.mapper.ScProfitLossTotalMapper;
import com.jackrain.nea.sg.inv.model.request.ScInvProfitLossMarginQueryCmdRequest;
import com.jackrain.nea.sg.inv.model.result.ScInvProfitLossGenarateQueryResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author 舒威
 * @since 2019/4/11
 * create at : 2019/4/11 15:55
 */
@Component
public class ScInvProfitLossTotalService {

    @Autowired
    private SgStorageBoxConfig storageBoxConfig;

    ValueHolderV14<ScInvProfitLossGenarateQueryResult> queryProfitLossTotalInventory(ScInvProfitLossMarginQueryCmdRequest model) {
        ValueHolderV14<ScInvProfitLossGenarateQueryResult> v14 = new ValueHolderV14<>(ResultCode.SUCCESS, "success");
        Long storeId = model.getWarehouseId();
        Integer inventoryType = model.getInventoryType();
        Date inventoryDate = model.getInventoryDate();

        ScInvProfitLossItemService itemService = ApplicationContextHandle.getBean(ScInvProfitLossItemService.class);
        itemService.checkParams(storeId, inventoryType, inventoryDate, model.getLoginUser().getLocale());
        Boolean flag = true;
//        Boolean flag = inventoryDate.getTime() >= new Date().getTime(); //逐日库存标志
        ScProfitLossTotalMapper mapper = ApplicationContextHandle.getBean(ScProfitLossTotalMapper.class);

        JSONObject profitCountResult = null;
        if (SgInvConstants.PAND_TYPE_QP.equals(inventoryType)) {
            profitCountResult = mapper.queryTotalProfByQP(storeId, inventoryType, inventoryDate, flag);
        } else if (SgInvConstants.PAND_TYPE_CP.equals(inventoryType)) {
            profitCountResult = mapper.queryTotalProfByCP(storeId, inventoryType, inventoryDate, flag);
        }

        JSONObject profitTeusCountResult = null;
        if (storageBoxConfig.getBoxEnable()) {
            if (SgInvConstants.PAND_TYPE_QP.equals(inventoryType)) {
                profitTeusCountResult = mapper.queryTeusTotalProfByQP(storeId, inventoryType, inventoryDate, flag);
            } else if (SgInvConstants.PAND_TYPE_CP.equals(inventoryType)) {
                profitTeusCountResult = mapper.queryTeusTotalProfByCP(storeId, inventoryType, inventoryDate, flag);
            }
        }

        if (null == profitCountResult && null == profitTeusCountResult) {
            v14.setCode(ResultCode.FAIL);
            v14.setMessage("无符合条件数据！");
            return v14;
        } else {
            BigDecimal qty_profit = BigDecimal.ZERO;
            BigDecimal qty_loss = BigDecimal.ZERO;
            if (null != profitCountResult) {
                if (null != profitCountResult.getBigDecimal("QTY_PROFIT")) {
                    qty_profit = profitCountResult.getBigDecimal("QTY_PROFIT");
                }
                if (null != profitCountResult.getBigDecimal("QTY_LOSS")) {
                    qty_loss = profitCountResult.getBigDecimal("QTY_LOSS");
                }
            }

            BigDecimal teus_qty_profit = BigDecimal.ZERO;
            BigDecimal teus_qty_loss = BigDecimal.ZERO;
            if (null != profitTeusCountResult) {
                if (null != profitTeusCountResult.getBigDecimal("QTY_PROFIT")) {
                    teus_qty_profit = profitTeusCountResult.getBigDecimal("QTY_PROFIT");
                }
                if (null != profitTeusCountResult.getBigDecimal("QTY_LOSS")) {
                    teus_qty_loss = profitTeusCountResult.getBigDecimal("QTY_LOSS");
                }
            }

            String msg = "请仔细核对盈亏结果：【箱】多货" + teus_qty_profit.stripTrailingZeros().toPlainString() + "箱，少货"
                    + teus_qty_loss.stripTrailingZeros().toPlainString() + "箱；" + "【散件】多货" + qty_profit.stripTrailingZeros().toPlainString() +
                    "件，少货" + qty_loss.stripTrailingZeros().toPlainString() + "件；单击确定会生成库存调整单和盈亏单，一旦生成不可修改！";
            ScInvProfitLossGenarateQueryResult queryResult = new ScInvProfitLossGenarateQueryResult();
            queryResult.setTotalMsg(msg);
            queryResult.setQtyProfit(qty_profit);
            queryResult.setQtyLoss(qty_loss);
            queryResult.setTeusQtyProfit(teus_qty_profit);
            queryResult.setTeusQtyLoss(teus_qty_loss);
            v14.setData(queryResult);
        }
        return v14;
    }
}
