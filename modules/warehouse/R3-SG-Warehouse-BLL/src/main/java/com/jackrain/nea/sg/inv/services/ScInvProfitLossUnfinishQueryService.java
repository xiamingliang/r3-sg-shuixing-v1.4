package com.jackrain.nea.sg.inv.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Lists;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.model.util.ModelUtil;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.basic.utils.StorageUtils;
import com.jackrain.nea.sg.in.common.SgInNoticeConstants;
import com.jackrain.nea.sg.inv.constants.SgInvConstants;
import com.jackrain.nea.sg.inv.mapper.ScBInventoryImpItemMapper;
import com.jackrain.nea.sg.inv.mapper.ScBPrePlUnfshBillMapper;
import com.jackrain.nea.sg.inv.model.request.ScInvProfitLossBasePageRequest;
import com.jackrain.nea.sg.inv.model.request.ScInvProfitLossGenarateRequest;
import com.jackrain.nea.sg.inv.model.request.ScInvProfitLossMarginQueryCmdRequest;
import com.jackrain.nea.sg.inv.model.table.ScBPrePlUnfshBill;
import com.jackrain.nea.sg.out.common.SgOutConstantsIF;
import com.jackrain.nea.sg.phyadjust.common.SgPhyAdjustConstants;
import com.jackrain.nea.sg.send.utils.SgStoreUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * 查询未完成单据
 *
 * @author 舒威
 * @since 2019/4/8
 * create at : 2019/4/8 16:17
 */
@Slf4j
@Component
public class ScInvProfitLossUnfinishQueryService {

    @Autowired
    private ScBInventoryImpItemMapper inventoryItemMapper;

    @Autowired
    private ScBPrePlUnfshBillMapper unfshBillMapper;

    /**
     * 查询未完成单据
     */
    ValueHolderV14<PageInfo<ScBPrePlUnfshBill>> queryUnfinishProfitLoss(ScInvProfitLossMarginQueryCmdRequest model) {
        if (log.isDebugEnabled()) {
            log.debug("Start ScInvProfitLossUnfinishQueryService.queryUnfinishProfitLoss. Receive:request:{}", JSONObject.toJSONString(model));
        }
        ValueHolderV14<PageInfo<ScBPrePlUnfshBill>> v14 = new ValueHolderV14<>();
        Long warehouseId = model.getWarehouseId();
        Integer inventoryType = model.getInventoryType();
        Date inventoryDate = model.getInventoryDate();
        Integer polStatus = model.getPolStatus();
        String queryId = model.getQueryId();

        User loginUser = model.getLoginUser();
        AssertUtils.notNull(loginUser, "当前用户未登录！");
        Locale locale = loginUser.getLocale();
        AssertUtils.notNull(warehouseId, "请选择盘点店仓！", locale);
        AssertUtils.notNull(inventoryType, "请选择盘点类型！", locale);
        AssertUtils.notNull(inventoryDate, "请选择盘点日期！", locale);
        ScInvProfitLossBasePageRequest page = Optional.ofNullable(model.getPage()).orElse(new ScInvProfitLossBasePageRequest());

        if (StringUtils.isEmpty(queryId)) {
             /*TODO 查询标识query_id=Null,第一次查询*/
            queryId = UUID.randomUUID().toString();
            log.debug("新生成的query_id:" + queryId);

            String proEcodesStr = "";
            List<String> proEcodes = inventoryItemMapper.selectProEcodes(warehouseId, inventoryType, inventoryDate, polStatus);
            if (CollectionUtils.isEmpty(proEcodes)) {
                AssertUtils.logAndThrow("存在盘点单明细为空，不允许预盈亏！", locale);
            } else {
                List<String> pros = Lists.newArrayList();
                for (String proEcode : proEcodes) {
                    pros.add("'" + proEcode + "'");
                }
                if (pros.size() > 1) {
                    proEcodesStr = StringUtils.join(pros, ",");
                } else {
                    proEcodesStr = pros.get(0);
                }
            }

            List<ScBPrePlUnfshBill> unfishList = Lists.newArrayList();
            insertRow(unfishList, warehouseId, inventoryDate, inventoryType, SgInvConstants.SC_B_INVENTORY, proEcodesStr, queryId, loginUser, locale);
            insertRow(unfishList, warehouseId, inventoryDate, inventoryType, SgConstants.SG_B_PHY_ADJUST, proEcodesStr, queryId, loginUser, locale);
            insertRow(unfishList, warehouseId, inventoryDate, inventoryType, SgConstants.SG_B_PHY_IN_NOTICES, proEcodesStr, queryId, loginUser, locale);
            insertRow(unfishList, warehouseId, inventoryDate, inventoryType, SgConstants.SG_B_PHY_OUT_NOTICES, proEcodesStr, queryId, loginUser, locale);
            insertRow(unfishList, warehouseId, inventoryDate, inventoryType, SgConstants.SC_B_TRANSFER_DIFF, proEcodesStr, queryId, loginUser, locale);

            if (CollectionUtils.isNotEmpty(unfishList)) {
                SgStoreUtils.batchInsertTeus(unfishList, "未完成单据", SgConstants.SG_COMMON_ITEM_INSERT_PAGE_SIZE, unfshBillMapper);
            }
        }

        String orderByName = StringUtils.isEmpty(page.getName()) ? "CREATIONDATE" : page.getName();
        String order = page.getIsasc() ? " asc" : " desc";
        PageHelper.startPage(page.getCurrentPage(), page.getPageSize(), orderByName + order);
        List<ScBPrePlUnfshBill> unfshBills = unfshBillMapper.selectList(new QueryWrapper<ScBPrePlUnfshBill>().lambda()
                .eq(ScBPrePlUnfshBill::getQueryId, queryId)
                .in(model.getIsCreateProf(), ScBPrePlUnfshBill::getBillType, model.getGenarateInfo().getUnfshBillType()));
        PageInfo<ScBPrePlUnfshBill> pageInfo = new PageInfo<>(unfshBills);
        v14.setCode(ResultCode.SUCCESS);
        v14.setMessage("success");
        v14.setData(pageInfo);

        if (log.isDebugEnabled()) {
            log.debug("Finish ScInvProfitLossUnfinishQueryService.queryUnfinishProfitLoss. Return:results:{}", JSONObject.toJSONString(v14));
        }
        return v14;
    }

    /**
     * 插入未完成单据
     * <p>
     * 【未完成单据】状态节点判断逻辑：
     * 入库通知单：【实体仓】=【盘点店仓】、【单据状态】=待入库或者部分入库的入库通知单
     * 出库通知单：【实体仓】=【盘点店仓】、【单据状态】=待出库或者部分出库的出库通知单
     * 库存调整单：【调整店仓】=【盘点店仓】、【单据日期】<=【盘点日期】、【单据状态】=未审核的库存调整单
     * 盘点单：【盘点店仓】=【盘点店仓】、【单据日期】<【盘点日期】、【盈亏状态】=未盈亏的库存调整单
     * 调拨差异单：【发货店仓】/【收货店仓】=【盘点店仓】、【单据状态】=未审核的调拨差异单
     * 销售差异单：【发货店仓】/【收货店仓】=【盘点店仓】、【单据状态】=未审核的销售差异单
     * 销售退货差异单：【发货店仓】/【收货店仓】=【盘点店仓】、【单据状态】=未审核的销售差异单
     * 零售单：【零售店仓】=【盘点店仓】、【零售日期】<=【盘点日期】、【收银状态】=未收银的零售单
     * </p>
     */
    private void insertRow(List<ScBPrePlUnfshBill> unfishList, Long warehouseId, Date inventoryDate, Integer inventoryType,
                           String billType, String proEcodes, String queryId,
                           User loginUser, Locale locale) {
        List<JSONObject> list = Lists.newArrayList();
        switch (billType) {
            case SgConstants.SG_B_PHY_IN_NOTICES:
                ArrayList<Integer> status = Lists.newArrayList(SgInNoticeConstants.BILL_STATUS_IN_PENDING, SgInNoticeConstants.BILL_STATUS_IN_PART);
                String join = StringUtils.join(status, ",");
                list = unfshBillMapper.selectListByInNotices(warehouseId, join, proEcodes);
                break;
            case SgConstants.SG_B_PHY_OUT_NOTICES:
                status = Lists.newArrayList(SgOutConstantsIF.OUT_NOTICES_STATUS_WAIT, SgOutConstantsIF.OUT_NOTICES_STATUS_PART);
                join = StringUtils.join(status, ",");
                list = unfshBillMapper.selectListByOutNotices(warehouseId, join, proEcodes);
                break;
            case SgConstants.SG_B_PHY_ADJUST:
                list = unfshBillMapper.selectListByPhyAdjust(warehouseId, inventoryDate, SgPhyAdjustConstants.ADJ_AUDIT_STATUS_N, proEcodes);
                break;
            case SgInvConstants.SC_B_INVENTORY:
                list = unfshBillMapper.selectListByInventory(warehouseId, inventoryDate, SgInvConstants.PAND_UNPOL, proEcodes);
                break;
            case SgConstants.SC_B_TRANSFER_DIFF:
                list = unfshBillMapper.selectListByPhyTransferDiff(warehouseId, SgInvConstants.PAND_UNPOL, proEcodes);
                break;
            default:
                AssertUtils.logAndThrow("未完成单据解析异常:暂不支持的单据类型" + billType, locale);
        }

        if (CollectionUtils.isNotEmpty(list)) {
            for (JSONObject object : list) {
                ScBPrePlUnfshBill unfshBill = createUnfishBill(object, billType, warehouseId, loginUser, queryId);
                unfishList.add(unfshBill);
            }
        }
    }

    /**
     * 封装未完成单据model
     */
    private ScBPrePlUnfshBill createUnfishBill(JSONObject object, String billType, Long warehouseId, User loginUser, String queryId) {
        ScBPrePlUnfshBill model = new ScBPrePlUnfshBill();
        model.setId(ModelUtil.getSequence(SgInvConstants.SC_B_PRE_PL_UNFSH_BILL.toLowerCase()));
        model.setQueryId(queryId);
        model.setBillNo(object.getString("bill_no"));
        //店仓名称
        storeExchange(billType, object, warehouseId, model);
        model.setBillDate(object.getDate("bill_date"));
        model.setBillType(billType);
        model.setBillTypeName(billTypeExchange(billType, loginUser.getLocale()));
        model.setStatus(SgInvConstants.SC_B_INVENTORY.equals(billType) ? object.getInteger("pol_status") : object.getInteger("bill_status"));
        model.setStatusName(statusExchange(billType, model.getStatus(), loginUser.getLocale()));
        model.setRemark(object.getString("remark"));
        model.setIsactive(SgConstants.IS_ACTIVE_Y);
        model.setOwnerename(loginUser.getEname());
        model.setModifierename(loginUser.getEname());
        StorageUtils.setBModelDefalutData(model, loginUser);
        return model;
    }

    /**
     * 店仓名称转换
     */
    private void storeExchange(String billType, JSONObject object, Long warehouseId, ScBPrePlUnfshBill model) {
        if (SgConstants.SG_B_PHY_IN_NOTICES.equals(billType) ||
                SgConstants.SG_B_PHY_OUT_NOTICES.equals(billType) ||
                SgConstants.SG_B_PHY_ADJUST.equals(billType) ||
                SgInvConstants.SC_B_INVENTORY.equals(billType)) {
            model.setCpCStoreId(object.getLong("cp_c_phy_warehouse_id"));
            model.setCpCStoreEcode(object.getString("cp_c_phy_warehouse_ecode"));
            model.setCpCStoreEname(object.getString("cp_c_phy_warehouse_ename"));
        } else if (SgConstants.SC_B_TRANSFER_DIFF.equals(billType)) {
            Long cp_c_store_id_send = object.getLong("cp_c_store_id_send");
            Long cp_c_store_id = object.getLong("cp_c_store_id");
            if (warehouseId.equals(cp_c_store_id_send)) {
                model.setCpCStoreId(cp_c_store_id_send);
                model.setCpCStoreEcode(object.getString("cp_c_store_ecode_send"));
                model.setCpCStoreEname(object.getString("cp_c_store_ename_send"));
            } else if (warehouseId.equals(cp_c_store_id)) {
                model.setCpCStoreId(cp_c_store_id);
                model.setCpCStoreEcode(object.getString("cp_c_store_ecode"));
                model.setCpCStoreEname(object.getString("cp_c_store_ename"));
            }
        } else {
            AssertUtils.logAndThrow("单据类型暂未开放");
        }
    }

    /**
     * 状态名称转换
     */
    private String statusExchange(String billType, Integer status, Locale locale) {
        String statusName = "";
        switch (billType) {
            case SgConstants.SG_B_PHY_IN_NOTICES:
                if (SgInNoticeConstants.BILL_STATUS_IN_PART == status) {
                    statusName = "部分入库";
                } else if (SgInNoticeConstants.BILL_STATUS_IN_PENDING == status) {
                    statusName = "待入库";
                } else {
                    statusName = "状态异常";
                }
                break;
            case SgConstants.SG_B_PHY_OUT_NOTICES:
                if (SgOutConstantsIF.OUT_NOTICES_STATUS_PART == status) {
                    statusName = "部分出库";
                } else if (SgOutConstantsIF.OUT_NOTICES_STATUS_WAIT == status) {
                    statusName = "待出库";
                } else {
                    statusName = "状态异常";
                }
                break;
            case SgConstants.SG_B_PHY_ADJUST:
                statusName = "未审核";
                break;
            case SgConstants.SC_B_TRANSFER_DIFF:
                statusName = "未审核";
                break;
            case SgInvConstants.OC_B_RETAIL:
                statusName = "未收银";
                break;
            case SgInvConstants.SC_B_INVENTORY:
                statusName = "未盈亏";
                break;
            default:
                AssertUtils.logAndThrow("单据类型异常！", locale);
        }
        return statusName;
    }

    /**
     * 单据类型转换
     */
    private String billTypeExchange(String billType, Locale locale) {
        String type = "";
        switch (billType) {
            case SgConstants.SG_B_PHY_IN_NOTICES:
                type = "入库通知单";
                break;
            case SgConstants.SG_B_PHY_OUT_NOTICES:
                type = "出库通知单";
                break;
            case SgConstants.SG_B_PHY_ADJUST:
                type = "库存调整单";
                break;
            case SgConstants.SC_B_TRANSFER_DIFF:
                type = "调拨差异单";
                break;
            case SgInvConstants.OC_B_RETAIL:
                type = "零售单";
                break;
            case SgInvConstants.SC_B_INVENTORY:
                type = "盘点单";
                break;
            case SgInvConstants.OC_B_ORDER:
                type = "订单管理";
            default:
                AssertUtils.logAndThrow("单据类型异常！", locale);
        }
        return type;
    }

}
