package com.jackrain.nea.sg.out.services;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.common.R3ParamConstants;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.model.result.SgR3BaseResult;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.out.model.request.SgOutResultMQRequest;
import com.jackrain.nea.sg.out.model.request.SgPhyOutResultBillSaveRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * 出库结果单冲单 - 新增并审核结果单时不校验通知单状态,最后将通知单改成待出库
 *
 * @author 舒威
 * @since 2019/12/12
 * create at : 2019/12/12 17:37
 */
@Slf4j
@Component
public class SgPhyOutResultChargeOffService {

    @Transactional(rollbackFor = Exception.class)
    public ValueHolderV14<SgOutResultMQRequest> saveAndAuditSgPhyOutResult(SgPhyOutResultBillSaveRequest request) {
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ".debug, param:" + JSONObject.toJSONString(request));
        }
        ValueHolderV14<SgOutResultMQRequest> result = new ValueHolderV14<>(ResultCode.SUCCESS, "冲单成功！");
        try {
            SgPhyOutResultChargeOffSaveService saveService = ApplicationContextHandle.getBean(SgPhyOutResultChargeOffSaveService.class);
            ValueHolderV14<SgR3BaseResult> saveResult = saveService.saveSgPhyOutResult(request);
            if (saveResult.isOK()) {
                SgR3BaseResult data = saveResult.getData();
                Long objId = data.getDataJo().getLong(R3ParamConstants.OBJID);
                SgPhyOutResultChargeOffAuditService auditService = ApplicationContextHandle.getBean(SgPhyOutResultChargeOffAuditService.class);
                SgOutResultMQRequest auditResult = auditService.auditSgOutResult(objId, request.getLoginUser(), null, false);
                result.setData(auditResult);
            } else {
                result.setCode(ResultCode.FAIL);
                result.setMessage(saveResult.getMessage());
            }
        } catch (Exception e) {
            String errMsg = StorageESUtils.strSubString("冲单异常!"+e.getMessage(), SgConstants.SG_COMMON_STRING_SIZE);
            log.error(errMsg);
            result.setCode(ResultCode.FAIL);
            result.setMessage(errMsg);
        }
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ".debug, return:" + JSONObject.toJSONString(result));
        }
        return result;
    }
}
