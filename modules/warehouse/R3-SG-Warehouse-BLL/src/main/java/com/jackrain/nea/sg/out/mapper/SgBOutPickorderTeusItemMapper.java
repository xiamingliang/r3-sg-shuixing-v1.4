package com.jackrain.nea.sg.out.mapper;

import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.out.model.table.SgBOutPickorderTeusItem;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.SelectProvider;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@Mapper
public interface SgBOutPickorderTeusItemMapper extends ExtentionMapper<SgBOutPickorderTeusItem> {


    @SelectProvider(type = SgBOutPickorderTeusItemProvider.class, method = "selectTeusSumQty")
    Map<Long, BigDecimal> selectTeusSumQty(List<Long> teusIdList, Long id);

    @SelectProvider(type = SgBOutPickorderTeusItemProvider.class, method = "selectSkuSumQty")
    Map<Long, BigDecimal> selectSkuSumQty(List<Long> skuIdList, Long id);

    class SgBOutPickorderTeusItemProvider {

        public String selectTeusSumQty(List<Long> teusIdList, Long id) {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT PS_C_TEUS_ID,SUM(QTY_TEUS) FROM SG_B_OUT_PICKORDER_TEUS_ITEM WHERE SG_B_OUT_PICKORDER_ID =")
                    .append(id)
                    .append(" and PS_C_TEUS_ID in (");
            for (int i = 0; i < teusIdList.size(); i++) {
                sql.append(teusIdList.get(i));
                if (i != teusIdList.size() - 1) {
                    sql.append(",");
                }
            }
            sql.append(")")
                    .append(" GROUP BY PS_C_TEUS_ID");
            return sql.toString();
        }

        public String selectSkuSumQty(List<Long> skuIdList, Long id) {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT PS_C_SKU_ID,SUM(QTY_TEUS) FROM SG_B_OUT_PICKORDER_TEUS_ITEM WHERE SG_B_OUT_PICKORDER_ID =")
                    .append(id)
                    .append(" and PS_C_SKU_ID in (");
            for (int i = 0; i < skuIdList.size(); i++) {
                sql.append(skuIdList.get(i));
                if (i != skuIdList.size() - 1) {
                    sql.append(",");
                }
            }
            sql.append(")")
                    .append(" GROUP BY PS_C_SKU_ID");
            return sql.toString();
        }
    }
}