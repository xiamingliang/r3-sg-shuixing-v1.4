package com.jackrain.nea.sg.inv.mapper;

import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.inv.model.table.ScBProfitImpItem;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ScBProfitImpItemMapper extends ExtentionMapper<ScBProfitImpItem> {
}