package com.jackrain.nea.sg.inv.mapper;

import com.alibaba.fastjson.JSONObject;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;

import java.util.Date;

/**
 * @author 舒威
 * @since 2019/4/11
 * create at : 2019/4/11 16:09
 */
@Mapper
public interface ScProfitLossTotalMapper {

    @SelectProvider(type = ScProfitTotalSqlProvider.class, method = "queryTotalProfByCP")
    JSONObject queryTotalProfByCP(@Param("storeId") Long storeId,
                                  @Param("inventoryType") Integer inventoryType,
                                  @Param("inventoryDate") Date inventoryDate,
                                  @Param("flag") Boolean flag);

    @SelectProvider(type = ScProfitTotalSqlProvider.class, method = "queryTotalProfByQP")
    JSONObject queryTotalProfByQP(@Param("storeId") Long storeId,
                                  @Param("inventoryType") Integer inventoryType,
                                  @Param("inventoryDate") Date inventoryDate,
                                  @Param("flag") Boolean flag);


    @SelectProvider(type = ScProfitTeusTotalSqlProvider.class, method = "queryTeusTotalProfByCP")
    JSONObject queryTeusTotalProfByCP(@Param("storeId") Long storeId,
                                      @Param("inventoryType") Integer inventoryType,
                                      @Param("inventoryDate") Date inventoryDate,
                                      @Param("flag") Boolean flag);

    @SelectProvider(type = ScProfitTeusTotalSqlProvider.class, method = "queryTeusTotalProfByQP")
    JSONObject queryTeusTotalProfByQP(@Param("storeId") Long storeId,
                                      @Param("inventoryType") Integer inventoryType,
                                      @Param("inventoryDate") Date inventoryDate,
                                      @Param("flag") Boolean flag);
}
