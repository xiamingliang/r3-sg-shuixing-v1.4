package com.jackrain.nea.sg.out.services;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.data.basic.services.BasicPsQueryService;
import com.jackrain.nea.ip.api.qimen.QmDeliveryorderCreateCmd;
import com.jackrain.nea.ip.model.qimen.QmDeliveryorderCreateModel;
import com.jackrain.nea.ip.model.result.QimenAsyncMqResult;
import com.jackrain.nea.model.util.AdParamUtil;
import com.jackrain.nea.ps.api.result.PsCProResult;
import com.jackrain.nea.psext.request.PsToWmsRequest;
import com.jackrain.nea.r3.mq.util.R3MqSendHelper;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.model.table.CpCPhyWarehouse;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.in.common.SgInNoticeConstants;
import com.jackrain.nea.sg.out.common.SgOutConstants;
import com.jackrain.nea.sg.out.common.SgOutConstantsIF;
import com.jackrain.nea.sg.out.common.enums.SourcePlatformCode;
import com.jackrain.nea.sg.out.config.SgOutMqConfig;
import com.jackrain.nea.sg.out.mapper.SgBPhyOutNoticesMapper;
import com.jackrain.nea.sg.out.model.request.SgOutNoticesBatchSendMsgRequest;
import com.jackrain.nea.sg.out.model.request.SgPhyOutNoticesBillSaveRequest;
import com.jackrain.nea.sg.out.model.result.SgOutNoticesByBillNoResult;
import com.jackrain.nea.sg.out.model.result.SgOutNoticesResult;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutNotices;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutNoticesItem;
import com.jackrain.nea.sg.out.utils.SgWarehouseESUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.DateUtil;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.face.impl.UserImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author leexh
 * @since 2019/5/6 14:05
 * 1. 新增通知单
 * 2. 校验电子面单状态 >4(5)
 * 3. 航空禁运 、菜鸟仓itemid写死 (3) 获取(4)
 * 4. 传奇门 >1 (2)
 * 5. 推es  >5 (6)
 */
@Slf4j
@Component
public class SgPhyOutNoticesBatchSaveAndWMSService {

    @Autowired
    private SgBPhyOutNoticesMapper outNoticesMapper;

    @Autowired
    private SgPhyOutNoticesSaveWMSService saveWMSService;

    @Autowired
    private R3MqSendHelper r3MqSendHelper;

    @Autowired
    private SgOutMqConfig mqConfig;

    @Autowired
    private SgWarehouseESUtils warehouseESUtils;

    @Value("${sg.bill.phy_out_notices_to_wms_max_threads:10}")
    private Integer threadWms;

    public ValueHolderV14<List<SgOutNoticesResult>> batchSaveOutNoticesAndWMS(List<SgPhyOutNoticesBillSaveRequest> requests, List<Long> sourceBillIds, User user) {
        ValueHolderV14<List<SgOutNoticesResult>> v14 = new ValueHolderV14();
        if (CollectionUtils.isEmpty(requests)) {
            AssertUtils.logAndThrow("参数不能为空！");
        }
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ".debug, param:{};", JSONObject.toJSONString(requests));
        }

        //批量获取已存在出库通知单/明细
        ValueHolderV14<HashMap<Long, HashMap<SgBPhyOutNotices, List<SgBPhyOutNoticesItem>>>> batchQueryResults =
                saveWMSService.batchQueryOutNotices(sourceBillIds, SgConstantsIF.BILL_TYPE_RETAIL);
        HashMap<Long, HashMap<SgBPhyOutNotices, List<SgBPhyOutNoticesItem>>> batchQueryResultsData = batchQueryResults.getData();

        List<List<SgPhyOutNoticesBillSaveRequest>> requestsList = StorageESUtils.averageAssign(requests, threadWms);

        for (List<SgPhyOutNoticesBillSaveRequest> requestList : requestsList) {
            Runnable task = null;
            if (CollectionUtils.isNotEmpty(requestList)) {
                task = () -> {
                    //记录 当前线程来源单据id
                    List<Long> threadSourceBillIds = Lists.newArrayList();
                    try {
                        //记录 返回订单的信息
                        List<SgOutNoticesResult> data = new ArrayList<>();
                        //记录 单据编号-通知单
                        HashMap<String, SgBPhyOutNotices> noticesStrMap = Maps.newHashMap();

                        //记录 请求体的 通知单-明细
                        HashMap<SgBPhyOutNotices, List<SgBPhyOutNoticesItem>> noticesMap = Maps.newHashMap();
                        //记录 批量新增返回的 通知单-明细
                        HashMap<SgBPhyOutNotices, List<SgBPhyOutNoticesItem>> newMap = Maps.newHashMap();

                        HashMap<Long, Long> shopIdMap = Maps.newHashMap();
                        HashMap<Long, Integer> etypeMap = Maps.newHashMap();

                        //记录 待新增的通知单
                        List<SgPhyOutNoticesBillSaveRequest> addNotices = Lists.newArrayList();

                        for (SgPhyOutNoticesBillSaveRequest request : requestList) {
                            threadSourceBillIds.add(request.getOutNoticesRequest().getSourceBillId());
                        }

                        log.debug("來源单据ids" + threadSourceBillIds + ",开始批量新增出库通知单并传WMS!");

                        //记录 当前线程需要新增通知单的来源单据id
                        List<Long> threadAddSourceBillIds = Lists.newArrayList();
                        List<SgPhyOutNoticesBillSaveRequest> threadAddOutNoticesList = Lists.newArrayList();

                        for (SgPhyOutNoticesBillSaveRequest request : requestList) {
                            Long sourceBillId = request.getOutNoticesRequest().getSourceBillId();
                            if (MapUtils.isEmpty(batchQueryResultsData) || !batchQueryResultsData.containsKey(sourceBillId)) {
                                //数据库不存在通知单,记录待新增集合中
                                addNotices.add(request);
                                threadAddSourceBillIds.add(sourceBillId);
                                threadAddOutNoticesList.add(request);
                            } else {
                                //已新增过通知单
                                noticesMap.putAll(batchQueryResultsData.get(sourceBillId));
                            }
                        }

                        // 记录 批量新增通知单失败信息
                        HashMap<Long, String> err = Maps.newHashMap();

                        //批量新增通知单
                        if (CollectionUtils.isNotEmpty(addNotices)) {
                            if (log.isDebugEnabled()) {
                                log.debug("來源单据ids[" + threadAddSourceBillIds + "],需要批量新增逻辑发货单!");
                            }

                            long noticesTime = System.currentTimeMillis();
                            try {
                                ValueHolderV14<JSONObject> addResult = saveWMSService.saveSgPhyOutNoticesByWms(threadAddOutNoticesList, user);
                                if (addResult.isOK()) {
                                    JSONObject addData = addResult.getData();
                                    err = (HashMap<Long, String>) addData.get("error");
                                    newMap = (HashMap<SgBPhyOutNotices, List<SgBPhyOutNoticesItem>>) addData.get("data");
                                    shopIdMap = (HashMap<Long, Long>) addData.get("shopIdMap");
                                    etypeMap = (HashMap<Long, Integer>) addData.get("etypeMap");
                                } else {
                                    // 新增出库通知单全部失败
                                    JSONObject addData = addResult.getData();
                                    err = (HashMap<Long, String>) addData.get("error");
                                }
                            } catch (Exception e) {
                                String messageExtra = AssertUtils.getMessageExtra("新增出库通知单异常!", e);
                                log.error("來源单据ids[" + threadAddSourceBillIds + "],批量" + messageExtra);
                                String flag = StorageESUtils.strSubString(messageExtra, SgConstants.SG_COMMON_REMARK_SIZE);
                                for (Long sourceBillId : threadAddSourceBillIds) {
                                    resultMsg(SgOutConstantsIF.RESULT_REMARK_FAIL, -1L, sourceBillId, flag, data);
                                }
                            }

                            if (log.isDebugEnabled()) {
                                log.debug("批量新增出库通知单耗时:{}ms;", System.currentTimeMillis() - noticesTime);
                            }
                        }

                        if (MapUtils.isNotEmpty(err)) {
                            // 记录 新增出库通知单失败信息
                            err.forEach((sourceBillId, errMsg) -> {
                                String flag = StorageESUtils.strSubString(errMsg, SgConstants.SG_COMMON_REMARK_SIZE);
                                resultMsg(SgOutConstantsIF.RESULT_REMARK_FAIL, -1L, sourceBillId, flag, data);
                            });
                        }

                        if (MapUtils.isNotEmpty(newMap)) {
                            long faceTime = System.currentTimeMillis();
                            try {
                                SgPhyOutNoticesSaveFaceService faceService = ApplicationContextHandle.getBean(SgPhyOutNoticesSaveFaceService.class);
                                SgOutNoticesBatchSendMsgRequest faceRequest = new SgOutNoticesBatchSendMsgRequest();
                                faceRequest.setOutNoticesMap(newMap);
                                faceRequest.setShopIdMap(shopIdMap);
                                faceRequest.setEtypeMap(etypeMap);
                                faceRequest.setLoginUser(user);
                                faceService.electronicFaceSaveList(faceRequest);
                            } catch (Exception e) {
                                e.printStackTrace();
                                String messageExtra = AssertUtils.getMessageExtra("获取电子面单信息失败!", e);
                                log.error(messageExtra);
                            }
                            log.debug("获取电子面单耗时:{}ms;", System.currentTimeMillis() - faceTime);
                            noticesMap.putAll(newMap);
                        }

                        //批量校验 电子面单、通知单传WMS状态
                        List<SgOutNoticesByBillNoResult> qimenList = checkFaceAndWms(noticesMap, data, noticesStrMap);

                        //RPC调用奇门 并且将失败信息回写订单
                        bacthCreateDeliveryOrders(qimenList, data, noticesStrMap, user);

                    } catch (Exception e) {
                        String messageExtra = AssertUtils.getMessageExtra("來源单据ids" + threadSourceBillIds + ",新增出库通知单并传WMS异常!", e);
                        log.error(messageExtra);
                    }
                };
                new Thread(task).start();
            }
        }

        v14.setCode(ResultCode.SUCCESS);
        v14.setMessage("成功！");
        return v14;
    }


    /**
     * 批量校验 电子面单、通知单传WMS状态
     *
     * @param noticesMap    待校验的 通知单map
     * @param data          记录成功失败信息
     * @param noticesStrMap 记录通知单号-通知单信息，方便根据奇门结果(仅有通知单号)更新通知单信息
     * @return qimenList 待传奇门List
     */
    public List<SgOutNoticesByBillNoResult> checkFaceAndWms(HashMap<SgBPhyOutNotices, List<SgBPhyOutNoticesItem>> noticesMap,
                                                            List<SgOutNoticesResult> data, HashMap<String, SgBPhyOutNotices> noticesStrMap) {

        List<SgOutNoticesByBillNoResult> qimenList = Lists.newArrayList();

        long checkTime = System.currentTimeMillis();
        //noticesMap 中是 数据库中已存在的通知单+新增成功的出库通知单
        Iterator<SgBPhyOutNotices> iterator = noticesMap.keySet().iterator();

        while (iterator.hasNext()) {
            SgBPhyOutNotices notices = iterator.next();
            Long objId = notices.getId();
            Long sourceBillId = notices.getSourceBillId();
            Integer wayStatus = notices.getIsEnableewaybill();
            //判断电子面单获取状态
            if (wayStatus != null && wayStatus == SgOutConstantsIF.IS_ENABLEEWAYBILL_Y) {
                Integer ewaybillStatus = notices.getEwaybillStatus();
                Long faceFailedCount = Optional.ofNullable(notices.getReserveBigint03()).orElse(1L);
                String failReason = StorageESUtils.strSubString("获取电子面单失败！" + notices.getEwaybillFailReason(), SgConstants.SG_COMMON_STRING_SIZE);
                log.error(failReason);
                //电子面单状态为空(或者部分成功,或者失败且失败次数<5次),返回订单code=9(订单不会改状态.下次不会再被拉取)
                if (ewaybillStatus == null ||
                        ewaybillStatus == SgOutConstantsIF.EWAYBILL_STATUS_SECTION_SUC ||
                        (ewaybillStatus == SgOutConstantsIF.EWAYBILL_STATUS_FAIL && faceFailedCount < 5)) {
                    resultMsg(SgOutConstantsIF.RESULT_FACE_FAIL, objId, sourceBillId, failReason, data);
                    continue;
                } else if (ewaybillStatus == SgOutConstantsIF.EWAYBILL_STATUS_FAIL && faceFailedCount >= 5) {
                    //电子面单状态为失败 并且失败次数>=5次 返回code=3,订单改为已审核,下次不会再被拉取
                    resultMsg(SgOutConstantsIF.RESULT_WMS_FAIL, objId, sourceBillId, failReason, data);
                    continue;
                }
            }

            //判断通知单的传WMS状态
            Integer isPassWms = notices.getIsPassWms();
            if (isPassWms == SgOutConstants.IS_PASS_WMS_Y) {
                Long wmsStatus = Optional.ofNullable(notices.getWmsStatus()).orElse(0L);
                Long wmsFailCount = notices.getWmsFailCount();
                if (wmsStatus == SgOutConstantsIF.WMS_STATUS_PASS_SUCCESS) {
                    // 若出库通知单传WMS状态为【传WMS成功】，则返回信息（来源单号ID、出库通知单ID、标识（已传WMS）、返回信息）；
                    resultMsg(SgOutConstantsIF.RESULT_PASS_WMS, notices.getId(), sourceBillId, "已传WMS", data);
                } else if (wmsStatus == SgOutConstantsIF.WMS_STATUS_PASS_FAILED && wmsFailCount > 5) {
                    // 2019-06-06添加：如果传WMS失败次数大于5，则返回给调用方
                    String wmsFailReason = notices.getWmsFailReason();
                    resultMsg(SgOutConstantsIF.RESULT_WMS_FAIL, notices.getId(), sourceBillId, "传WMS失败次数超过5次，不再传WMS！" + wmsFailReason, data);
                } else if (wmsStatus == SgOutConstantsIF.WMS_STATUS_WAIT_PASS || (wmsStatus == SgOutConstantsIF.WMS_STATUS_PASS_FAILED && wmsFailCount <= 5)) {
                    List<SgBPhyOutNoticesItem> items = noticesMap.get(notices);
                    if (CollectionUtils.isEmpty(items)) {
                        resultMsg(SgOutConstantsIF.RESULT_REMARK_FAIL, notices.getId(), sourceBillId, "新增出库通知单异常!通知单返回明细为空!", data);
                        continue;
                    }
                    SgOutNoticesByBillNoResult billNoResult = new SgOutNoticesByBillNoResult();
                    billNoResult.setItems(items);
                    billNoResult.setOutNotices(notices);
                    qimenList.add(billNoResult);
                    noticesStrMap.put(notices.getBillNo(), notices);
                }
            } else if (isPassWms == SgOutConstants.IS_PASS_WMS_N) {
                resultMsg(SgOutConstantsIF.RESULT_NOT_WMS, objId, sourceBillId, "不需传WMS", data);
            }
        }

        if (log.isDebugEnabled()) {
            log.debug("批量校验电子面单、通知单传WMS状态耗时:{}ms;", System.currentTimeMillis() - checkTime);
        }

        return qimenList;
    }


    /**
     * RPC调用奇门 并且将失败信息回写订单
     *
     * @param qmList 传奇门参数
     * @param data   记录成功失败信息
     * @param user
     * @return
     */
    public void bacthCreateDeliveryOrders(List<SgOutNoticesByBillNoResult> qmList, List<SgOutNoticesResult> data,
                                          HashMap<String, SgBPhyOutNotices> noticesStrMap, User user) {
        // 批量调用奇门中间接口
        if (CollectionUtils.isNotEmpty(qmList)) {
            long qmTime = System.currentTimeMillis();
            ValueHolderV14<List<QimenAsyncMqResult>> qmResult = createDeliveryOrder(user, qmList);
            if (log.isDebugEnabled()) {
                log.debug("传奇门结果:{},耗时:{}ms;", JSONObject.toJSONString(qmResult), System.currentTimeMillis() - qmTime);
            }

            if (qmResult == null || qmResult.getData() == null) {
                //调用传奇门接口 失败  list 全部返回失败 并记录失败次数
                List<Long> noticesids = Lists.newArrayList();
                String errMsg = StorageESUtils.strSubString("传奇门服务异常!" + qmResult.getMessage(), SgConstants.SG_COMMON_STRING_SIZE);
                for (SgOutNoticesByBillNoResult billNoResult : qmList) {
                    long sourceBillId = billNoResult.getOutNotices().getSourceBillId();
                    noticesids.add(billNoResult.getOutNotices().getId());
                    resultMsg(-1, null, sourceBillId, errMsg, data);
                }
                String join = StringUtils.join(noticesids, ",");
                outNoticesMapper.batchUpdateOutNoticesByWMS(join, errMsg);
                //推送ES
                for (Long noticesid : noticesids) {
                    warehouseESUtils.pushESByOutNotices(noticesid, false, false, null, outNoticesMapper, null);
                }
            } else if (!qmResult.isOK()) {
                HashMap hashMap = getSourceBillIdMap(qmList);
                for (QimenAsyncMqResult mqResult : qmResult.getData()) {
                    //20019-09-26 云枢纽返回失败 更新出库通知单传WMS状态为传WMS失败，失败次数+1，记录失败原因
                    if (!Boolean.valueOf(mqResult.getIsSend())) {
                        long sourceBillId = (long) hashMap.get(mqResult.getBillNo());
                        resultMsg(-1, null, sourceBillId, mqResult.getMessage(), data);
                        try {
                            SgBPhyOutNotices outNotices = noticesStrMap.get(mqResult.getBillNo());
                            SgBPhyOutNotices notices = new SgBPhyOutNotices();
                            notices.setId(outNotices.getId());
                            notices.setWmsStatus((long) SgInNoticeConstants.WMS_UPLOAD_STATUTS_FAIL);
                            notices.setWmsFailReason(mqResult.getMessage());
                            Long wmsFailCount = Optional.ofNullable(outNotices.getWmsFailCount()).orElse(0L);
                            wmsFailCount++;
                            notices.setWmsFailCount(wmsFailCount);
                            StorageESUtils.setBModelDefalutDataByUpdate(notices, user);
                            notices.setModifierename(user.getEname());
                            outNoticesMapper.updateById(notices);
                            //推送ES
                            warehouseESUtils.pushESByOutNotices(outNotices.getId(), false, false, null, outNoticesMapper, null);
                        } catch (Exception e) {
                            String messageExtra = AssertUtils.getMessageExtra("传奇门接口失败,更新出库通知单失败!", e);
                            log.error(messageExtra);
                        }
                    }
                }
            }
        }

        if (log.isDebugEnabled()) {
            log.debug("新增出库通知传奇门结果!data:{};", JSONObject.toJSONString(data));
        }

        // 将未成功的结果回传至订单传WMS
        List<SgOutNoticesResult> passData = Lists.newArrayList();
        if (CollectionUtils.isNotEmpty(data)) {
            for (SgOutNoticesResult datum : data) {
                if (datum.getCode() != ResultCode.SUCCESS) {
                    passData.add(datum);
                }
            }
        }

        if (CollectionUtils.isNotEmpty(passData)) {
            JSONArray body = JSONArray.parseArray(JSONObject.toJSONString(passData));
            JSONObject ret = new JSONObject();
            ret.put("type", 2);
            ret.put("body", body);
            String configName = mqConfig.getConfigName();
            String mqTopic = mqConfig.getOmsMqTopic();
            String tag = SgOutConstants.TAG_OUT_NOTICES_CREATE_RECEIPT;
            String msgKey = UUID.randomUUID().toString().replaceAll("-", "");
            try {
                String msgId = r3MqSendHelper.sendMessage(configName, ret.toJSONString(), mqTopic, tag, msgKey);
                if (log.isDebugEnabled()) {
                    log.debug("新增出库通知并传WMS回传订单成功!body:{},msgId:{};", body.toJSONString(), msgId);
                }
            } catch (Exception e) {
                String messageExtra = AssertUtils.getMessageExtra("MQ回传订单传奇门结果异常!", e);
                log.error(messageExtra);
            }
        }
    }


    public HashMap getSourceBillIdMap(List<SgOutNoticesByBillNoResult> list) {
        HashMap hashMap = new HashMap();
        if (!CollectionUtils.isEmpty(list)) {
            for (int i = 0; i < list.size(); i++) {
                SgOutNoticesByBillNoResult billNoResult = list.get(i);
                hashMap.put(billNoResult.getOutNotices().getBillNo(), billNoResult.getOutNotices().getSourceBillId());
            }
        }
        return hashMap;
    }


    /**
     * RPC调用发货单创建（接口平台MQ通知奇门）
     *
     * @param user
     * @param list
     * @return
     */
    public ValueHolderV14<List<QimenAsyncMqResult>> createDeliveryOrder(User user, List<SgOutNoticesByBillNoResult> list) {
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ",debug,createDeliveryOrder,list=" + JSON.toJSONString(list));
        }

        /*2019-08-30 判断商品是否含有航空禁运品*/
        Map<String, PsCProResult> proResultMap = Maps.newHashMap();
        Map<Long, CpCPhyWarehouse> warehouseMap = Maps.newHashMap();
        long feeTime = System.currentTimeMillis();
        try {
            List<SgBPhyOutNoticesItem> allitems = Lists.newArrayList();
            Set<Long> warehouseIds = new HashSet<>();
            for (SgOutNoticesByBillNoResult billNoResult : list) {
                Long cpCPhyWarehouseId = billNoResult.getOutNotices().getCpCPhyWarehouseId();
                warehouseIds.add(cpCPhyWarehouseId);
                List<SgBPhyOutNoticesItem> items = billNoResult.getItems();
                if (CollectionUtils.isNotEmpty(items)) allitems.addAll(items);
            }
            Set<String> proEcodes = allitems.stream().map(SgBPhyOutNoticesItem::getPsCProEcode).collect(Collectors.toSet());
            List<String> pros = new ArrayList<>(proEcodes);
            //分批获取商品信息  500/次
            List<PsCProResult> littleProInfo = StorageESUtils.getPros(pros, SgConstants.SG_COMMON_INSERT_PAGE_SIZE);
            for (PsCProResult psCProResult : littleProInfo) {
                String ecode = psCProResult.getEcode();
                if (StringUtils.isEmpty(ecode)) {
                    log.error("补充的商品信息ecode为空!pro:" + JSONObject.toJSONString(psCProResult, SerializerFeature.WriteMapNullValue));
                    continue;
                }
                if (!proResultMap.containsKey(ecode)) {
                    proResultMap.put(ecode, psCProResult);
                }
            }
        } catch (Exception e) {
            String messageExtra = AssertUtils.getMessageExtra("批量查询商品信息异常:", e);
            log.error(messageExtra);
        }
        log.debug("航空禁运信息采集耗时:{}ms;", System.currentTimeMillis() - feeTime);

        Set<Long> warehouseIds = new HashSet<>();
        for (SgOutNoticesByBillNoResult billNoResult : list) {
            Long cpCPhyWarehouseId = billNoResult.getOutNotices().getCpCPhyWarehouseId();
            warehouseIds.add(cpCPhyWarehouseId);
        }
        //批量查询实体仓档案信息
        List<Long> warehouses = Lists.newArrayList(warehouseIds);
        ValueHolderV14<Map<Long, CpCPhyWarehouse>> queryWarehouseResult = saveWMSService.batchQueryWarehouse(warehouses);
        if (queryWarehouseResult.isOK()) {
            warehouseMap.putAll(queryWarehouseResult.getData());
        } else {
            log.error("实体仓查询失败!" + queryWarehouseResult.getMessage());
        }

        ValueHolderV14<List<QimenAsyncMqResult>> valueHolder;
        QmDeliveryorderCreateCmd createCmd = (QmDeliveryorderCreateCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(), QmDeliveryorderCreateCmd.class.getName(), "ip", "1.4.0");

        long paramTime = System.currentTimeMillis();
        //k-v customerid - List<model>
        HashMap<String, List<QmDeliveryorderCreateModel>> qimenMap = Maps.newHashMap();
        //List<QmDeliveryorderCreateModel> createModels = new ArrayList<>();
        for (SgOutNoticesByBillNoResult model : list) {
            SgBPhyOutNotices outNotices = model.getOutNotices();
            try {
                QmDeliveryorderCreateModel createModel = new QmDeliveryorderCreateModel();
                // 奇门订单平台来源编码转换
                String sourcePlatformCode = outNotices.getReserveBigint01() != null
                        ? SourcePlatformCode.valueOf(outNotices.getReserveBigint01().intValue()).getDesc()
                        : SgOutConstants.QM_DEFAULT_PLATFORM_CODE;

                CpCPhyWarehouse cpCPhyWarehouse = warehouseMap.get(outNotices.getCpCPhyWarehouseId());
                if (cpCPhyWarehouse == null) {
                    AssertUtils.logAndThrow("实体仓id" + outNotices.getCpCPhyWarehouseId() + ",实体仓信息为空!");
                }
                /** 主表信息 */
                QmDeliveryorderCreateModel.DeliveryOrder deliveryOrder = new QmDeliveryorderCreateModel.DeliveryOrder();
                deliveryOrder.setDeliveryOrderCode(outNotices.getBillNo());
                deliveryOrder.setOrderType(SgOutConstants.QM_BILL_TYPE_GENERAL_OUT);    // 奇门入参默认“JYCK”
                deliveryOrder.setWarehouseCode(cpCPhyWarehouse.getWmsWarehouseCode());  // 实体仓档案取WMS仓库编码
                deliveryOrder.setSourcePlatformCode(sourcePlatformCode);
                // 2019-06-28添加字段
                deliveryOrder.setServiceFee(outNotices.getAmtService() == null ? "0.00"
                        : outNotices.getAmtService().setScale(2, BigDecimal.ROUND_HALF_UP).toString());
                deliveryOrder.setCreateTime(DateUtil.getDateTime());
                deliveryOrder.setPlaceOrderTime(DateUtil.format(outNotices.getOrderTime(), "yyyy-MM-dd HH:mm:ss"));
                deliveryOrder.setPayTime(DateUtil.format(outNotices.getPayTime(), "yyyy-MM-dd HH:mm:ss"));
                deliveryOrder.setOperateTime(DateUtil.getDateTime());
                deliveryOrder.setShopNick(outNotices.getCpCShopTitle());
                deliveryOrder.setTotalAmount(outNotices.getTotAmtCost().setScale(2, BigDecimal.ROUND_HALF_UP).toString());
                deliveryOrder.setLogisticsCode(outNotices.getCpCLogisticsEcode());
                deliveryOrder.setLogisticsName(outNotices.getCpCLogisticsEname());
                deliveryOrder.setExpressCode(outNotices.getLogisticNumber());
                deliveryOrder.setBuyerMessage(outNotices.getBuyerRemark());
                deliveryOrder.setSellerMessage(outNotices.getSellerRemark());
                //deliveryOrder.setRemark(outNotices.getRemark());
                /* 2019-05-09主表添加字段 */
                Integer isCod = outNotices.getIsCod();
                deliveryOrder.setOrderFlag((isCod != null && isCod == 2) ? "COD" : "");
                deliveryOrder.setArAmount(outNotices.getAmtPayment() == null ? "0.00"
                        : outNotices.getAmtPayment().setScale(2, BigDecimal.ROUND_HALF_UP).toString());
                deliveryOrder.setLogisticsAreaCode(outNotices.getShortAddress());
                deliveryOrder.setServiceCode(outNotices.getCpCCustomerEcode());
                //添加字段2019-10-22
                deliveryOrder.setOperatorName(((UserImpl)user).getUserOption("ECODE","root"));//操作员 (审核员) 编码

                 /* 2019-08-19主表添加字段 */
                 /*订单类型=正常 或 null时,传JYCK,不等于正常 或 不为空时,传BFCK*/
                /*if (outNotices.getReserveBigint02() == null || outNotices.getReserveBigint02() == 1) {
                    deliveryOrder.setOrderType("JYCK");
                } else {
                    deliveryOrder.setOrderType("BFCK");
                }*/

                HashMap<String, String> psToWmsInfo = null;
                List<SgBPhyOutNoticesItem> noticesItems = model.getItems();
                if (CollectionUtils.isEmpty(noticesItems)) {
                    AssertUtils.logAndThrow("来源单据id[" + outNotices.getSourceBillId() + "],通知单明细为空!");
                }

                long adParamTime = System.currentTimeMillis();
                /*2019-08-19 主表添加字段  订单类型=正常 或 null时,传JYCK,不等于正常 或 不为空时,传BFCK*/
                /*2019-09-16 由于菜鸟仓，平台单号重复不允许传JYCK，故orderType接口字段全部传BFCK*/
                /*2019-09-19 orderType默认传JYCK，当发货仓customerid等于菜鸟仓customerid时,传BFCK*/
                try {
                    //系统参数-菜鸟customerid
                    String sysCustoemerId = AdParamUtil.getParam(SgOutConstants.SYSTEM_WAREHOUSE_CUSTOMERID);
                    log.debug("出库通知传WMS获取菜鸟仓customerid:" + sysCustoemerId);
                    if (StringUtils.isNotEmpty(sysCustoemerId)) {
                        if (sysCustoemerId.equals(cpCPhyWarehouse.getWmsAccount())) {
                            deliveryOrder.setOrderType(SgOutConstants.QM_BILL_TYPE_BFCK_OUT);

                            // 2019-07-11添加逻辑：菜鸟仓对接时所需的itemId获取
                            List<PsToWmsRequest> itemIdRequest = new ArrayList<>();
                            noticesItems.forEach(itemIdWms -> {
                                PsToWmsRequest wmsRequest = new PsToWmsRequest();
                                wmsRequest.setSku(itemIdWms.getPsCSkuEcode());
                                wmsRequest.setCustomerId(cpCPhyWarehouse.getWmsAccount());
                                itemIdRequest.add(wmsRequest);
                            });

                            try {
                                BasicPsQueryService psQueryService = ApplicationContextHandle.getBean(BasicPsQueryService.class);
                                psToWmsInfo = psQueryService.getPsToWmsInfo(itemIdRequest);
                                if (log.isDebugEnabled()) {
                                    log.debug("psToWmsInfo:" + psToWmsInfo);
                                }
                            } catch (Exception e) {
                                String messageExtra = AssertUtils.getMessageExtra("查询ItemId异常：", e);
                                log.error(messageExtra);
                            }
                        }
                    }
                } catch (Exception e) {
                    String messageExtra = AssertUtils.getMessageExtra("系统参数菜鸟仓customerid获取异常!", e);
                    log.error(messageExtra);
                }

                log.debug("菜鸟仓系统参数相关逻辑处理耗时:{}ms;", System.currentTimeMillis() - adParamTime);

                // 收件人信息
                QmDeliveryorderCreateModel.ReceiverInfo receiverInfo = new QmDeliveryorderCreateModel.ReceiverInfo();

                String provinceName = outNotices.getCpCRegionProvinceEname();
                String cityName = outNotices.getCpCRegionCityEname();
                String areaName = outNotices.getCpCRegionAreaEname();
                String receiverAddress = outNotices.getReceiverAddress();
                receiverInfo.setName(outNotices.getReceiverName());
                receiverInfo.setZipCode(outNotices.getReceiverZip());
                //2019-08-30  若手机号码为空，取电话号码
                String receiverPhone = outNotices.getReceiverPhone();
                receiverInfo.setTel(receiverPhone);
                String receiverMobile = outNotices.getReceiverMobile();
                if (StringUtils.isEmpty(receiverMobile)) {
                    receiverMobile = receiverPhone;
                }
                receiverInfo.setMobile(receiverMobile);
                receiverInfo.setProvince(provinceName);
                receiverInfo.setCity(cityName);
                receiverInfo.setArea(areaName);
                receiverInfo.setDetailAddress(provinceName + cityName + areaName + receiverAddress);
                receiverInfo.setRemark(outNotices.getRemark());
                deliveryOrder.setReceiverInfo(receiverInfo);

                // 发件人信息：产品说都传“0”
                QmDeliveryorderCreateModel.SenderInfo senderInfo = new QmDeliveryorderCreateModel.SenderInfo();
                senderInfo.setName(SgOutConstants.QM_DEFAULT_SENDER_NAME);
                senderInfo.setZipCode(SgOutConstants.QM_DEFAULT_SENDER_ZIP_CODE);
                senderInfo.setTel(SgOutConstants.QM_DEFAULT_SENDER_TEL);
                senderInfo.setMobile(SgOutConstants.QM_DEFAULT_SENDER_MOBILE);
                senderInfo.setProvince(SgOutConstants.QM_DEFAULT_SENDER_PROVINCE);
                senderInfo.setCity(SgOutConstants.QM_DEFAULT_SENDER_CITY);
                senderInfo.setArea(SgOutConstants.QM_DEFAULT_SENDER_AREA);
                senderInfo.setDetailAddress(SgOutConstants.QM_DEFAULT_SENDER_DETAIL_ADDRESS);
                senderInfo.setRemark(SgOutConstants.QM_DEFAULT_SENDER_INFO);
                // 添加字段2019-10-22
                senderInfo.setCompany(SgOutConstants.QM_DEFAULT_SENDER_COMPANYNAME);

                deliveryOrder.setSenderInfo(senderInfo);

                createModel.setDeliveryOrder(deliveryOrder);

                //扩展字段 是否航空禁运
                Map<Object, Object> extendProps = Maps.newHashMap();
                int isAirforbidden = 0;
                /** 子表信息 */
                String goodsOwner = outNotices.getGoodsOwner(); // 货主编号
                String sourcecode = outNotices.getSourcecode();
                List<QmDeliveryorderCreateModel.OrderLine> orderLines = new ArrayList<>();
                for (SgBPhyOutNoticesItem noticesItem : noticesItems) {
                    QmDeliveryorderCreateModel.OrderLine orderLine = new QmDeliveryorderCreateModel.OrderLine();
                    orderLine.setOwnerCode(goodsOwner);
                    orderLine.setItemCode(noticesItem.getPsCSkuEcode());
                    orderLine.setPlanQty(noticesItem.getQty() == null ? "" : noticesItem.getQty().setScale(0, BigDecimal.ROUND_HALF_UP).toString());
                    orderLine.setActualPrice(noticesItem.getPriceCost() == null ? "0.00" : noticesItem.getPriceCost().setScale(2, BigDecimal.ROUND_HALF_UP).toString());
                    orderLine.setSourceOrderCode(sourcecode);
                    // 2019-07-18添加字段：产品说写死"ZP"
                    orderLine.setInventoryType(SgConstantsIF.TRADE_MARK_ZP);
                    // 2019-07-11添加逻辑：菜鸟仓对接时所需的itemId获取
                    if (MapUtils.isNotEmpty(psToWmsInfo)
                            && psToWmsInfo.containsKey(noticesItem.getPsCSkuEcode())) {
                        orderLine.setItemId(psToWmsInfo.get(noticesItem.getPsCSkuEcode()));
                    } else {
                        orderLine.setItemId(null);
                    }
                    orderLines.add(orderLine);
                    //判断明细是否包含航空禁运
                    String proEcode = noticesItem.getPsCProEcode();
                    if (StringUtils.isNotEmpty(proEcode)) {
                        PsCProResult proResult = proResultMap.get(proEcode);
                        if (proResult != null) {
                            String airforbidden = proResult.getIsAirforbidden();
                            if (StringUtils.equals(airforbidden, SgConstants.IS_ACTIVE_Y)) {
                                isAirforbidden = 1;
                            }
                        }
                    }
                }
                createModel.setOrderLines(orderLines);
                // 查询实体仓档案WMS账号
                String customerId = cpCPhyWarehouse.getWmsAccount();
                createModel.setCustomerId(customerId);
                //是否航空禁运
                extendProps.put("isAirforbidden", isAirforbidden);
                //2019-10-22新增
                extendProps.put("Storageplace",outNotices.getCpCPhyWarehouseEcode());//实体仓编码
                extendProps.put("Requestedshipdate",outNotices.getBillDate());//单据日期
                extendProps.put("referencelocation",outNotices.getCpCPhyWarehouseEname());//实体仓名称

                createModel.setExtendProps(extendProps);
                //createModels.add(createModel);

                if (StringUtils.isNotEmpty(customerId)) {
                    if (qimenMap.containsKey(customerId)) {
                        List<QmDeliveryorderCreateModel> models = qimenMap.get(customerId);
                        models.add(createModel);
                    } else {
                        List<QmDeliveryorderCreateModel> models = Lists.newArrayList();
                        models.add(createModel);
                        qimenMap.put(customerId, models);
                    }
                }
            } catch (Exception e) {
                String messageExtra = AssertUtils.getMessageExtra("来源单据id[" + outNotices.getSourceBillId() + "],封装奇门参数异常", e);
                log.error(messageExtra);
            }
        }

        if (log.isDebugEnabled()) {
            log.debug("奇门参数:{},参数组装耗时:{}ms;", JSONObject.toJSONString(qimenMap), System.currentTimeMillis() - paramTime);
        }
        try {
            long qmTime = System.currentTimeMillis();
            valueHolder = createCmd.deliveryorderCreate(qimenMap, SgConstantsIF.GROUP, user);
            log.debug("奇门接口返回耗时:{}ms;", System.currentTimeMillis() - qmTime);
        } catch (Exception e) {
            String messageExtra = AssertUtils.getMessageExtra(this.getClass().getName() + ".error:", e);
            log.error(messageExtra);
            valueHolder = new ValueHolderV14();
            valueHolder.setCode(ResultCode.FAIL);
            valueHolder.setMessage("奇门中间接口调用RPC出错：" + e.getMessage());
        }
        return valueHolder;
    }

    /**
     * 错误数据收集
     *
     * @param objId
     * @param flag
     * @param list
     */
    public void resultMsg(Integer code, Long objId, Long sourceBillId, String flag, List<SgOutNoticesResult> list) {
        SgOutNoticesResult result = new SgOutNoticesResult();
        result.setCode(code);
        result.setId(objId);
        result.setSourceBilId(sourceBillId);
        result.setFlag(flag);
        list.add(result);
    }

}