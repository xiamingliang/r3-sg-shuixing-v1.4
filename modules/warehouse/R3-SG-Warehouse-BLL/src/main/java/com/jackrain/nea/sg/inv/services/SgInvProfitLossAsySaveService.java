/*
package com.jackrain.nea.sg.inv.services;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.redis.config.CusRedisTemplate;
import com.jackrain.nea.redis.util.RedisOpsUtil;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.DateUtil;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

*/
/**
 * @author 舒威
 * @since 2019/4/9
 * create at : 2019/4/9 17:22
 *//*

@Slf4j
@Component
public class SgInvProfitLossAsySaveService {

    ValueHolderV14 asySaveProfitLoss(ScInvProfitLossSaveAsyCmdRequest model) {
        ValueHolderV14 v14 = new ValueHolderV14();
        Long storeId = model.getStoreId();
        String inventoryType = model.getInventoryType();
        Date inventoryDate = model.getInventoryDate();

        User loginUser = model.getLoginUser();
        Locale locale = loginUser.getLocale();

        CusRedisTemplate<String, String> redisTemplate = RedisOpsUtil.getObjRedisTemplate();
        String LockKsy = "BProfitSave" + storeId + inventoryType + DateUtil.format(inventoryDate, "yyyyMMdd");
        if (null == redisTemplate.opsForValue().get(LockKsy)) {
            try {
                redisTemplate.opsForValue().set(LockKsy, "OK", 60, TimeUnit.SECONDS);
                */
/*生成盈亏*//*

                SgInvProfitLossSaveService saveService = ApplicationContextHandle.getBean(SgInvProfitLossSaveService.class);
                ValueHolderV14<ScInvProfitLossSaveResult> saveResult = saveService.saveProfitLoss(model);
                if (ResultCode.SUCCESS == saveResult.getCode()) {
                    ScInvProfitLossSaveResult saveResultData = saveResult.getData();
                    JSONObject row = saveResultData.getRow(); //库存调整单（保存）参数
                    Boolean isCreateInvadj = saveResultData.getIsCreateInvadj();//是否生成库存调整单标志
                    List<Long> ids = saveResultData.getIds(); //未盈亏盘点单ids
                    if (isCreateInvadj) {
                         */
/*库存调整单（新增）*//*

                        SgInvProfitLossRPCService rpcService = ApplicationContextHandle.getBean(SgInvProfitLossRPCService.class);
                        ValueHolder bStorageAdjust = rpcService.adjTableService(ScConstants.SC_B_STORAGE_ADJUST.toUpperCase(),
                                row, loginUser, "dosave");
                        if (null != bStorageAdjust && bStorageAdjust.isOK()) {
                            try {
                            */
/*库存调整单（提交）*//*

                                Long id = bStorageAdjust.toJSONObject().getLong("objid");
                                JSONObject submitParam = new JSONObject();
                                submitParam.put("objid", id);
                                submitParam.put("table", ScConstants.SC_B_STORAGE_ADJUST.toUpperCase());
                                ValueHolder submitResult = rpcService.adjTableService(ScConstants.SC_B_STORAGE_ADJUST.toUpperCase(),
                                        submitParam, loginUser, "dosubmit");
                                if (null != submitResult && submitResult.isOK()) {
                                    if (ScConstants.PAND_TYPE_QP.equals(inventoryType)) {
                                        try {
                                        */
/* 全盘-库存调整单（验收）*//*

                                            JSONObject acceptParam = new JSONObject();
                                            acceptParam.put("objid", id);
                                            acceptParam.put("table", ScConstants.SC_B_STORAGE_ADJUST.toUpperCase());
                                            ValueHolder acceptResult = rpcService.acceptStorageAdj(acceptParam, loginUser, "doaccept");
                                            if (acceptResult == null || !acceptResult.isOK()) {
                                                */
/*TODO 库存调整单(验收)失败，准备回滚(删除)库存调整单*//*


                                                v14.setCode(ResultCode.FAIL);
                                                v14.setMessage("生成盈亏失败：库存调整单验收失败！");
                                                return v14;
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                           */
/*TODO 库存调整单(验收)失败，准备回滚)删除)库存调整单*//*

                                            AssertUtils.logAndThrow("库存调整验收异常！" + e.getMessage(), loginUser.getLocale());
                                        }
                                    }

                                    try {
                                         */
/* 更新盘点单状态*//*

                                        SgInvProfitLossService profitLossService = ApplicationContextHandle.getBean(SgInvProfitLossService.class);
                                        profitLossService.updateInventory(ids, loginUser);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        */
/*TODO 更新盘点单状态失败，开始回滚库存调整单相关服务*//*

                                        AssertUtils.logAndThrow("更新盘点单异常！" + e.getMessage(), loginUser.getLocale());
                                    }

                                } else if (null == submitResult || !submitResult.isOK()) {
                                    */
/*TODO 库存调整单(提交)失败，准备回滚(删除)库存调整单*//*


                                    v14.setCode(ResultCode.FAIL);
                                    v14.setMessage("生成盈亏失败：库存调整单提交失败！");
                                    return v14;
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                 */
/*TODO 库存调整单(提交)失败，准备回滚（删除）库存调整单*//*

                                AssertUtils.logAndThrow("库存调整单提交异常！" + e.getMessage(), loginUser.getLocale());
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                AssertUtils.logAndThrow(e.getMessage(), locale);
            } finally {
                try {
                    redisTemplate.delete(LockKsy);
                } catch (Exception e) {
                    e.printStackTrace();
                    AssertUtils.logAndThrow("释放锁失败，请联系管理员！", locale);
                }
            }
        } else {
            v14.setCode(ResultCode.FAIL);
            v14.setMessage("当前记录正在被操作，请稍后重试！");
            return v14;
        }

        v14.setCode(ResultCode.SUCCESS);
        v14.setMessage("生成盈亏成功！");
        return v14;
    }
}
*/
