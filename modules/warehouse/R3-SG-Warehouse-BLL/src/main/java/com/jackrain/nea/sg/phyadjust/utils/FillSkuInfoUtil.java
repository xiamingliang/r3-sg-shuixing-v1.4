package com.jackrain.nea.sg.phyadjust.utils;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Maps;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.data.basic.model.request.SkuInfoQueryByGbcodeRequest;
import com.jackrain.nea.data.basic.model.request.SkuInfoQueryRequest;
import com.jackrain.nea.data.basic.services.BasicPsQueryService;
import com.jackrain.nea.oc.basic.common.OcBasicConstants;
import com.jackrain.nea.ps.api.result.PsCProSkuResult;
import com.jackrain.nea.ps.api.result.PsSkuResult;
import com.jackrain.nea.ps.api.table.ProSku;
import com.jackrain.nea.psext.model.table.PsCTeus;
import com.jackrain.nea.sg.basic.common.R3ParamConstants;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.phyadjust.model.result.FillSkuResult;
import com.jackrain.nea.sg.phyadjust.model.table.SgBPhyAdjustImpItem;
import com.jackrain.nea.sg.phyadjust.model.table.SgBPhyAdjustItem;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.utils.AssertUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author csy
 * Date: 2019/5/14
 * Description:
 */
@Component
public class FillSkuInfoUtil {

    @Autowired
    private BasicPsQueryService psQueryService;

    /**
     * 补充调整单条码信息
     */
    public ValueHolderV14<FillSkuResult> fillPhyAdjItems(List<SgBPhyAdjustItem> items, Locale locale, boolean isRollBackAll) {
        ValueHolderV14<FillSkuResult> holderV14 = new ValueHolderV14<>(ResultCode.SUCCESS, SgConstants.MESSAGE_STATUS_SUCCESS);
        List<String> skuECodes = items.stream().map(SgBPhyAdjustItem::getPsCSkuEcode).collect(Collectors.toList());
        SkuInfoQueryRequest request = new SkuInfoQueryRequest();
        request.setSkuEcodeList(skuECodes);
        HashMap<String, PsCProSkuResult> result = new HashMap<>(16);
        try {
            result = psQueryService.getSkuInfoByEcode(request);
        } catch (Exception e) {
            AssertUtils.logAndThrow("条码信息查询失败:" + e.getMessage(), locale);
        }
        JSONArray errArr = new JSONArray();
        List<SgBPhyAdjustItem> itemList = new ArrayList<>();
        for (SgBPhyAdjustItem adjustItem : items) {
            String skuECode = adjustItem.getPsCSkuEcode();
            PsCProSkuResult skuResult = result.get(skuECode);
            if (skuResult == null) {
                if (isRollBackAll) {
                    AssertUtils.logAndThrow("条码" + skuECode + "不存在!", locale);
                } else {
                    String errMsg = "条码" + skuECode + "不存在!";
                    JSONObject errJo = new JSONObject();
                    errJo.put(R3ParamConstants.CODE, -1);
                    errJo.put(R3ParamConstants.MESSAGE, errMsg);
                    errArr.add(errJo);
                }
            } else {
                adjustItem.setGbcode(skuResult.getGbcode());
                adjustItem.setPsCProEcode(skuResult.getPsCProEcode());
                adjustItem.setPsCProEname(skuResult.getPsCProEname());
                adjustItem.setPsCProId(skuResult.getPsCProId());
                adjustItem.setPsCSkuId(skuResult.getId());
                adjustItem.setPsCSpec1Ecode(skuResult.getClrsEcode());
                adjustItem.setPsCSpec1Ename(skuResult.getClrsEname());
                adjustItem.setPsCSpec1Id(skuResult.getPsCSpec1objId());
                adjustItem.setPsCSpec2Id(skuResult.getPsCSpec2objId());
                adjustItem.setPsCSpec2Ecode(skuResult.getSizesEcode());
                adjustItem.setPsCSpec2Ename(skuResult.getSizesEname());
                adjustItem.setPriceList(skuResult.getPricelist());
                if (skuResult.getPricelist() != null) {
                    adjustItem.setAmtList(adjustItem.getQty().multiply(skuResult.getPricelist()));
                }
                itemList.add(adjustItem);
            }
        }

        FillSkuResult skuResult = new FillSkuResult();
        skuResult.setErrArr(errArr);
        skuResult.setItemList(itemList);
        if (errArr.size() > 0) {
            holderV14.setCode(ResultCode.FAIL);
            holderV14.setMessage(Resources.getMessage("有" + errArr.size() + "条条码信息有误!", locale));
        }
        holderV14.setData(skuResult);
        return holderV14;
    }


    /**
     * 补充调整单条码信息-支持箱功能
     */
    public ValueHolderV14<FillSkuResult> fillPhyAdjItemsBox(List<SgBPhyAdjustImpItem> items, Locale locale, boolean isRollBackAll) {
        ValueHolderV14<FillSkuResult> holderV14 = new ValueHolderV14<>(ResultCode.SUCCESS, SgConstants.MESSAGE_STATUS_SUCCESS);
        List<String> skuEcodes = items.stream()
                .filter(o -> o.getIsTeus().equals(OcBasicConstants.IS_MATCH_SIZE_N) && StringUtils.isNotEmpty(o.getPsCSkuEcode()))
                .map(SgBPhyAdjustImpItem::getPsCSkuEcode).collect(Collectors.toList());
        List<String> boxCodeList = items.stream()
                .filter(o -> o.getIsTeus().equals(OcBasicConstants.IS_MATCH_SIZE_Y) && StringUtils.isNotEmpty(o.getPsCTeusEcode()))
                .map(SgBPhyAdjustImpItem::getPsCTeusEcode).collect(Collectors.toList());

        HashMap<String, PsCTeus> boxQueryResult = new HashMap<>();
        HashMap<String, PsCProSkuResult> skuQueryResult = new HashMap<>();

        try {
            if (CollectionUtils.isNotEmpty(skuEcodes)) {
                SkuInfoQueryRequest request = new SkuInfoQueryRequest();
                request.setSkuEcodeList(skuEcodes);
                skuQueryResult = psQueryService.getSkuInfoByEcode(request);
            }
            if (CollectionUtils.isNotEmpty(boxCodeList)) {
                boxQueryResult = psQueryService.queryTeusInfoByEcode(boxCodeList);
            }
        } catch (Exception e) {
            AssertUtils.logAndThrow("商品信息获取失败！" + e.getMessage(), locale);
        }

        JSONArray errArr = new JSONArray();
        List<SgBPhyAdjustImpItem> itemList = new ArrayList<>();
        for (SgBPhyAdjustImpItem impItem : items) {
            if (impItem.getIsTeus().equals(OcBasicConstants.IS_MATCH_SIZE_N)) {
                String skuECode = impItem.getPsCSkuEcode();
                PsCProSkuResult skuResult = skuQueryResult.get(skuECode);
                if (skuResult == null) {
                    if (isRollBackAll) {
                        AssertUtils.logAndThrow("条码" + skuECode + "不存在!", locale);
                    } else {
                        String errMsg = "条码" + skuECode + "不存在!";
                        JSONObject errJo = new JSONObject();
                        errJo.put(R3ParamConstants.CODE, -1);
                        errJo.put(R3ParamConstants.MESSAGE, errMsg);
                        errArr.add(errJo);
                    }
                } else {
                    impItem.setPsCProEcode(skuResult.getPsCProEcode());
                    impItem.setPsCProEname(skuResult.getPsCProEname());
                    impItem.setPsCProId(skuResult.getPsCProId());
                    impItem.setPsCSkuId(skuResult.getId());
                    impItem.setPsCSpec1Ecode(skuResult.getClrsEcode());
                    impItem.setPsCSpec1Ename(skuResult.getClrsEname());
                    impItem.setPsCSpec1Id(skuResult.getPsCSpec1objId());
                    impItem.setPsCSpec2Id(skuResult.getPsCSpec2objId());
                    impItem.setPsCSpec2Ecode(skuResult.getSizesEcode());
                    impItem.setPsCSpec2Ename(skuResult.getSizesEname());
                    impItem.setPriceList(skuResult.getPricelist());
                    itemList.add(impItem);
                }
            } else {
                String teusEcode = impItem.getPsCTeusEcode();
                PsCTeus psCTeus = boxQueryResult.get(teusEcode);
                impItem.setPsCTeusId(psCTeus.getId());
                impItem.setPsCProEcode(psCTeus.getPsCProEcode());
                impItem.setPsCProEname(psCTeus.getPsCProEname());
                impItem.setPsCProId(psCTeus.getPsCProId());
                impItem.setPsCMatchsizeId(psCTeus.getPsCMatchsizeId());
                impItem.setPsCMatchsizeEcode(psCTeus.getPsCMatchsizeEcode());
                impItem.setPsCMatchsizeEname(psCTeus.getPsCMatchsizeEname());
                impItem.setPsCSpec1Id(psCTeus.getPsCSpec1Id());
                impItem.setPsCSpec1Ecode(psCTeus.getPsCSpec1Ecode());
                impItem.setPsCSpec1Ename(psCTeus.getPsCSpec1Ename());
                impItem.setPriceList(psCTeus.getPriceList());
                itemList.add(impItem);
            }
        }

        FillSkuResult skuResult = new FillSkuResult();
        skuResult.setErrArr(errArr);
        skuResult.setImpItems(itemList);
        if (errArr.size() > 0) {
            holderV14.setCode(ResultCode.FAIL);
            holderV14.setMessage(Resources.getMessage("有" + errArr.size() + "条条码信息有误!", locale));
        }
        holderV14.setData(skuResult);
        return holderV14;
    }

    /**
     * 补充调整单条码信息(条码明细为空、国标码不为空)-根据国标码补充条码信息
     */
    public void fillPhyAdjItemsGbcode(List<SgBPhyAdjustItem> items, Locale locale, boolean isRollBackAll) {
        Set<String> gbCodes = items.stream().filter(item -> StringUtils.isEmpty(item.getPsCSkuEcode()) && StringUtils.isNotEmpty(item.getGbcode()))
                .map(SgBPhyAdjustItem::getGbcode).collect(Collectors.toSet());
        if (CollectionUtils.isNotEmpty(gbCodes)) {
            HashMap<String, String> gbcodeMap = Maps.newHashMap();
            BasicPsQueryService psQueryService = ApplicationContextHandle.getBean(BasicPsQueryService.class);
            SkuInfoQueryByGbcodeRequest gbcodeRequest = new SkuInfoQueryByGbcodeRequest();
            List<String> gbCodeList = new ArrayList<>(gbCodes);
            gbcodeRequest.setSkuGbcodeList(gbCodeList);
            try {
                PsSkuResult skuInfoByGbcode = psQueryService.getSkuInfoByGbcode(gbcodeRequest);
                if (skuInfoByGbcode != null && CollectionUtils.isNotEmpty(skuInfoByGbcode.getProSkus())) {
                    for (ProSku proSku : skuInfoByGbcode.getProSkus()) {
                        String gbcode = proSku.getGbcode();
                        String ecode = proSku.getEcode();
                        if (StringUtils.isNotEmpty(gbcode))
                            gbcodeMap.put(gbcode, ecode);
                    }
                } else {
                    AssertUtils.logAndThrow("国标码查询条码信息为空!");
                }
            } catch (Exception e) {
                AssertUtils.logAndThrow("根据国标码查询条码信息异常!" + e.getMessage());
            }
            //将国标查询结果填充到明细中
            items.stream().filter(item ->
                    StringUtils.isEmpty(item.getPsCSkuEcode()) && StringUtils.isNotEmpty(item.getGbcode())
            ).forEach(item -> item.setPsCSkuEcode(gbcodeMap.get(item.getGbcode())));
        }
    }
}
