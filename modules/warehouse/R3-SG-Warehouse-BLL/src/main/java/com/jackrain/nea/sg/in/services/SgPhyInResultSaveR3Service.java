package com.jackrain.nea.sg.in.services;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.Feature;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Lists;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.config.SgStorageBoxConfig;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.in.common.SgInConstants;
import com.jackrain.nea.sg.in.mapper.*;
import com.jackrain.nea.sg.in.model.request.SgBPhyInResultItemSaveRequest;
import com.jackrain.nea.sg.in.model.request.SgBPhyInResultSaveRequest;
import com.jackrain.nea.sg.in.model.request.SgPhyInResultBillSaveRequest;
import com.jackrain.nea.sg.in.model.request.SgPhyInResultImpItemSaveRequest;
import com.jackrain.nea.sg.in.model.table.SgBPhyInNotices;
import com.jackrain.nea.sg.in.model.table.SgBPhyInNoticesImpItem;
import com.jackrain.nea.sg.in.model.table.SgBPhyInNoticesItem;
import com.jackrain.nea.sg.in.model.table.SgBPhyInResult;
import com.jackrain.nea.sg.out.utils.SgWarehouseESUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.query.DefaultWebEvent;
import com.jackrain.nea.web.query.QuerySession;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * 入库结果单业务处理
 *
 * @author jiang.cj
 * @since 2019-4-22
 * create at : 2019-4-22
 */
@Slf4j
@Component
public class SgPhyInResultSaveR3Service {

    @Autowired
    private SgStorageBoxConfig storageBoxConfig;

    @Autowired
    private SgWarehouseESUtils warehouseESUtils;

    /**
     * 入库结果单明细删除
     *
     * @param session
     * @return
     */
    public ValueHolder deleteSgPhyInResultItem(QuerySession session) {
        if (log.isDebugEnabled()) {
            log.debug("Start SgPhyInResultSaveR3Service.deleteSgPhyInResultItem");
        }
        ValueHolder vh = new ValueHolder();
        SgBPhyInResultItemMapper sgBPhyInResultItemMapper = ApplicationContextHandle.getBean(SgBPhyInResultItemMapper.class);
        DefaultWebEvent event = session.getEvent();
        JSONObject param = JSON.parseObject(JSON.toJSONStringWithDateFormat(event.getParameterValue("param"),
                "yyyy-MM-dd HH:mm:ss", SerializerFeature.WriteMapNullValue), Feature.OrderedField);
        JSONObject fixcolumn = param.getJSONObject("fixcolumn");
        JSONArray itemArray = fixcolumn.getJSONArray(SgConstants.SG_B_PHY_IN_RESULT_ITEM.toUpperCase());
        try {
            Long inResultId = sgBPhyInResultItemMapper.selectById(itemArray.getLong(0)).getSgBPhyInResultId();
            int count = sgBPhyInResultItemMapper.deleteBatchIds(itemArray.toJavaList(Long.class));
            if (count <= 0) {
                vh.put("code", -1);
                vh.put("message", "入库结果单明细删除失败！");
                return vh;
            }

            //推送es
            SgBPhyInResultMapper resultMapper = ApplicationContextHandle.getBean(SgBPhyInResultMapper.class);
            warehouseESUtils.pushESByInResult(inResultId, false, true, itemArray, resultMapper, null);

            vh.put("code", 0);
            vh.put("message", "入库结果单明细删除成功！");
            return vh;
        } catch (Exception e) {
            log.error(this.getClass().getName() + ",error:" + e.toString());
            vh.put("code", -1);
            vh.put("message", "入库结果单明细删除失败！");
            return vh;
        }
    }

    /**
     * 入库结果单新增(页面)
     *
     * @param request
     * @return
     */
    public ValueHolderV14 saveSgBPhyInResult(SgPhyInResultBillSaveRequest request, JSONObject fixcolumn) {
        if (log.isDebugEnabled()) {
            log.debug("Start SgPhyInResultSaveR3Service.saveSgBPhyInResult:param=" + JSONObject.toJSONString(request));
        }

        User loginUser = request.getLoginUser();
        AssertUtils.notNull(loginUser, "用户信息不能为空！");

        //更新或者新增
        Long objId = request.getObjId();

        if (objId == null || objId <= 0) {
            return insertSgInResult(request, loginUser);
        } else {
            return updateSgInResult(request, objId, loginUser, fixcolumn); //入库结果单
        }
    }

    /**
     * 执行页面新增
     *
     * @return
     */
    private ValueHolderV14 insertSgInResult(SgPhyInResultBillSaveRequest request, User loginUser) {
        SgBPhyInNoticesMapper noticesMapper = ApplicationContextHandle.getBean(SgBPhyInNoticesMapper.class);
        SgBPhyInNoticesItemMapper SgBPhyInNoticesItemMapper = ApplicationContextHandle.getBean(SgBPhyInNoticesItemMapper.class);

        //通知单ID
        Long noticesId = request.getSgBPhyInResultSaveRequest().getSgBPhyInNoticesId();
        AssertUtils.notNull(noticesId, "关联通知单ID不能为空！");

        //获取入库通知单
        SgBPhyInNotices notices = noticesMapper.selectById(noticesId);
        AssertUtils.notNull(notices, "入库通知单不存在！");

        //封装主表信息
        SgBPhyInResultSaveRequest inResultSaveRequest = request.getSgBPhyInResultSaveRequest();
        BeanUtils.copyProperties(notices, inResultSaveRequest);
        inResultSaveRequest.setId(-1L);
        Date inTime = notices.getInTime() != null ? notices.getInTime() : new Date();
        inResultSaveRequest.setInTime(inTime);
        inResultSaveRequest.setSgBPhyInNoticesId(noticesId);
        inResultSaveRequest.setSgBPhyInNoticesBillno(notices.getBillNo());

        //开启箱功能 保存录入明细，未开启保存条码明细
        if (storageBoxConfig.getBoxEnable()) {
            //获取入库通知单录入明细
            SgBPhyInNoticesImpItemMapper noticesImpItemMapper = ApplicationContextHandle.getBean(SgBPhyInNoticesImpItemMapper.class);
            List<SgBPhyInNoticesImpItem> noticesImpItems = noticesImpItemMapper.selectList(new QueryWrapper<SgBPhyInNoticesImpItem>().lambda()
                    .eq(SgBPhyInNoticesImpItem::getSgBPhyInNoticesId, noticesId));
            AssertUtils.isNotEmpty("入库通知单录入明细为空！", noticesImpItems);
            List<SgPhyInResultImpItemSaveRequest> impItemSaveRequests = Lists.newArrayList();
            for (SgBPhyInNoticesImpItem noticesImpItem : noticesImpItems) {
                SgPhyInResultImpItemSaveRequest impItemSaveRequest = new SgPhyInResultImpItemSaveRequest();
                BeanUtils.copyProperties(noticesImpItem, impItemSaveRequest);
                impItemSaveRequest.setId(-1L);
                //将差异数量设置成入库数量
                impItemSaveRequest.setQtyIn(noticesImpItem.getQtyDiff());
                impItemSaveRequests.add(impItemSaveRequest);
            }
            request.setImpItemList(impItemSaveRequests);
        } else {
            //获取入库通知单明细
            List<SgBPhyInNoticesItem> list = SgBPhyInNoticesItemMapper.selectList(new QueryWrapper<SgBPhyInNoticesItem>().lambda()
                    .eq(SgBPhyInNoticesItem::getSgBPhyInNoticesId, noticesId));
            AssertUtils.isNotEmpty("入库通知单明细为空！", list);
            List<SgBPhyInResultItemSaveRequest> itemList = Lists.newArrayList();
            list.forEach(item -> {
                SgBPhyInResultItemSaveRequest itemRequest = new SgBPhyInResultItemSaveRequest();
                BeanUtils.copyProperties(item, itemRequest);
                itemRequest.setId(-1L);
                itemRequest.setSgBPhyInNoticesItemId(item.getId());
                //将入库数量设置成差异数量
                itemRequest.setQtyIn(item.getQtyDiff());
                itemRequest.setAmtListIn(item.getAmtListDiff());
                itemList.add(itemRequest);
            });
            request.setItemList(itemList);
        }

        request.setSgBPhyInResultSaveRequest(inResultSaveRequest);
        request.setLoginUser(loginUser);
        SgPhyInResultSaveService service = ApplicationContextHandle.getBean(SgPhyInResultSaveService.class);
        return service.saveSgBPhyInResult(request);
    }

    /**
     * 执行更新
     *
     * @param request
     * @return
     */
    private ValueHolderV14 updateSgInResult(SgPhyInResultBillSaveRequest request, Long objId, User loginUser, JSONObject fixcolumn) {
        ValueHolderV14 result = new ValueHolderV14(ResultCode.SUCCESS, "更新成功!");
        SgBPhyInResultMapper resultMapper = ApplicationContextHandle.getBean(SgBPhyInResultMapper.class);
        SgBPhyInResultItemMapper resultItemMapper = ApplicationContextHandle.getBean(SgBPhyInResultItemMapper.class);

        SgBPhyInResult inResult = resultMapper.selectById(objId);
        AssertUtils.notNull(inResult, "入库结果单已不存在!", loginUser.getLocale());
        AssertUtils.isTrue(SgInConstants.BILL_STATUS_CHECKED != inResult.getBillStatus(), "入库结果单已审核,不允许操作!", loginUser.getLocale());

        //更新主表
        SgBPhyInResult billResult = new SgBPhyInResult();
        billResult.setId(objId);
        JSONObject obj = fixcolumn.getJSONObject(SgConstants.SG_B_PHY_IN_RESULT.toUpperCase());
        if (obj != null) {
            if (obj.containsKey("IS_LAST")) {
                billResult.setIsLast(obj.getInteger("IS_LAST"));
            }
        }

        //开启箱功能 更新录入明细，未开启更新条码明细、主表合计信息
        if (storageBoxConfig.getBoxEnable() && CollectionUtils.isNotEmpty(request.getImpItemList())) {
            //更新录入明细
            SgBPhyInResultImpItemMapper impItemMapper = ApplicationContextHandle.getBean(SgBPhyInResultImpItemMapper.class);
            request.getImpItemList().forEach(impItem -> {
                BigDecimal qtyin = Optional.ofNullable(impItem.getQtyIn()).orElse(BigDecimal.ZERO);
                impItemMapper.updateItemById(qtyin, loginUser, impItem.getId());
            });
        } else if (CollectionUtils.isNotEmpty(request.getItemList())) {
            //更新条码明细
            request.getItemList().forEach(item -> {
                BigDecimal qtyin = Optional.ofNullable(item.getQtyIn()).orElse(BigDecimal.ZERO);
                resultItemMapper.updateItemById(qtyin, loginUser, item.getId());
            });
            //重新计算总数量、总入库金额
            JSONObject sumObj = resultItemMapper.selectSumById(objId);
            BigDecimal tot_qty_in = sumObj.getBigDecimal("totqtyin");
            BigDecimal tot_amt_list_in = sumObj.getBigDecimal("totamtlistin");
            billResult.setTotQtyIn(tot_qty_in);
            billResult.setTotAmtListIn(tot_amt_list_in);
        }

        StorageESUtils.setBModelDefalutDataByUpdate(billResult, loginUser);
        int count = resultMapper.updateById(billResult);
        if (count > 0) {
            //推送es
            warehouseESUtils.pushESByInResult(billResult.getId(), true, false, null, resultMapper, resultItemMapper);
        } else {
            result.setCode(ResultCode.FAIL);
            result.setMessage("保存失败!");
        }
        result.setData(objId);
        return result;
    }
}
