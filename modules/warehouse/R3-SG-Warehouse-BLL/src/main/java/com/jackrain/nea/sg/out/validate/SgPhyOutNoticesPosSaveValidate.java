package com.jackrain.nea.sg.out.validate;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.out.common.SgOutConstantsIF;
import com.jackrain.nea.tableService.MainTableRecord;
import com.jackrain.nea.tableService.SubTableRecord;
import com.jackrain.nea.tableService.TableServiceContext;
import com.jackrain.nea.tableService.validate.BaseValidator;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.MapUtils;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author 舒威
 * @since 2019/12/10
 * create at : 2019/12/10 14:34
 */
@Slf4j
@Component
public class SgPhyOutNoticesPosSaveValidate extends BaseValidator {

    @Override
    public void validate(TableServiceContext context, MainTableRecord row) {
        //获取原始数据
        JSONObject data = row.getMainData().getOrignalData();
        if (data == null) {
            AssertUtils.logAndThrow("当前记录已不存在！", context.getLocale());
        } else {
            Integer status = data.getInteger("BILL_STATUS");
            if (status != null && status == SgOutConstantsIF.OUT_NOTICES_STATUS_VOID) {
                AssertUtils.logAndThrow("当前记录已作废,不允许保存！", context.getLocale());
            }
            if (status != null && status == SgOutConstantsIF.OUT_NOTICES_STATUS_ALL) {
                AssertUtils.logAndThrow("当前记录已全部出库,不允许保存！", context.getLocale());
            }
        }

        Map<String, SubTableRecord> subTables = row.getSubTables();
        if (MapUtils.isNotEmpty(subTables)) {
            subTables.values().forEach(subTableRecord -> {
                JSONArray commitDatas = subTableRecord.getCommitDatas();
                if (commitDatas != null && commitDatas.size() > 0) {
                    for (int i = 0; i < commitDatas.size(); i++) {
                        JSONObject commitData = commitDatas.getJSONObject(i);
                        String psCSkuEcode = commitData.getString("PS_C_SKU_ECODE");
                        if (-1 == commitData.getLongValue("ID")) {
                            AssertUtils.logAndThrow("扫描明细中不存在商品" + psCSkuEcode + "不存在！");
                        } else if (commitData.containsKey("QTY") && !commitData.containsKey("QTY_SCAN")) {
                            AssertUtils.logAndThrow("扫描商品" + psCSkuEcode + "不允许修改通知数量！");
                        }
                        commitData.remove("QTY");
                    }
                }
            });
        }
    }
}
