package com.jackrain.nea.sg.pack.validate;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.sg.pack.common.SgTeusPackConstants;
import com.jackrain.nea.sg.pack.mapper.SgBTeusPackItemMapper;
import com.jackrain.nea.sg.pack.mapper.SgBTeusPackMapper;
import com.jackrain.nea.sg.pack.model.table.SgBTeusPack;
import com.jackrain.nea.sg.pack.model.table.SgBTeusPackItem;
import com.jackrain.nea.tableService.MainTableRecord;
import com.jackrain.nea.tableService.TableServiceContext;
import com.jackrain.nea.tableService.validate.BaseValidator;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.utils.AssertUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author zhoulinsheng
 * @since 2019/12/3 11:13
 * desc: 拆箱单明细删除
 */
@Slf4j
@Component
public class SgTeusPackDelValidate extends BaseValidator {

    @Override
    public void validate(TableServiceContext context, MainTableRecord row) {
        Long objId = row.getId();
        SgBTeusPackMapper packMapper = ApplicationContextHandle.getBean(SgBTeusPackMapper.class);
        SgBTeusPackItemMapper itemMapper = ApplicationContextHandle.getBean(SgBTeusPackItemMapper.class);

        SgBTeusPack sgBTeuspack = packMapper.selectById(objId);
        AssertUtils.notNull(sgBTeuspack, "当前记录不存在！", context.getLocale());

        List<SgBTeusPackItem> impItems = itemMapper.selectList(new QueryWrapper<SgBTeusPackItem>().lambda().eq(SgBTeusPackItem::getSgBTeusPackId, objId));
        AssertUtils.isTrue(CollectionUtils.isNotEmpty(impItems), "当前记录已不存在！！", context.getLocale());

        Integer status = sgBTeuspack.getStatus();
        if (status != null) {
            if (status == SgTeusPackConstants.BILL_STATUS_AUDIT) {
                AssertUtils.logAndThrow("当前记录已审核，不允许删除明细！", context.getLocale());
            }
        } else {
            AssertUtils.logAndThrow("当前记录单据状态为空！", context.getLocale());
        }
    }
}
