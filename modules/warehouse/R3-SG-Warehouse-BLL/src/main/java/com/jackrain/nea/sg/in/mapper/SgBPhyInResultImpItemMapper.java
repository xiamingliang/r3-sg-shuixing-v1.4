package com.jackrain.nea.sg.in.mapper;

import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.in.model.table.SgBPhyInResultImpItem;
import com.jackrain.nea.web.face.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import java.math.BigDecimal;

@Mapper
public interface SgBPhyInResultImpItemMapper extends ExtentionMapper<SgBPhyInResultImpItem> {

    @Update("UPDATE " + SgConstants.SG_B_PHY_IN_RESULT_IMP_ITEM + " SET QTY_IN=#{qtyIn}" +
            ",modifierid=#{user.id},modifierename=#{user.ename},modifiername=#{user.name},modifieddate=now() WHERE ID=#{id}")
    int updateItemById(@Param("qtyIn") BigDecimal qtyIn, @Param("user") User user, @Param("id") Long id);
}