package com.jackrain.nea.sg.out.services;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.cpext.utils.ValueHolderUtils;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.ip.api.qimen.QimenOrderCancelCmd;
import com.jackrain.nea.ip.model.qimen.QimenOrderCancelModel;
import com.jackrain.nea.sg.basic.common.R3ParamConstants;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.mapper.CpCPhyWarehouseMapper;
import com.jackrain.nea.sg.basic.model.table.CpCPhyWarehouse;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.in.common.SgInConstants;
import com.jackrain.nea.sg.in.common.SgInNoticeConstants;
import com.jackrain.nea.sg.out.common.SgOutConstantsIF;
import com.jackrain.nea.sg.out.mapper.SgBPhyOutNoticesMapper;
import com.jackrain.nea.sg.out.model.request.SgPhyOutBillRecallFromWmsRequest;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutNotices;
import com.jackrain.nea.sg.out.utils.SgWarehouseESUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Component
public class SgPhyOutNoticesRecallFromWMSService {

    @Autowired
    private SgWarehouseESUtils warehouseESUtils;

    public ValueHolder recallPhyOutNoticesFromWMS(SgPhyOutBillRecallFromWmsRequest request) {
        AssertUtils.notNull(request, "参数不能为空");
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ".debug, recallPhyOutNoticesFromWMS-param:" + JSONObject.toJSONString(request));
        }

        User user = request.getUser();
        AssertUtils.notNull(user, "用户信息不能为空！");

        JSONArray ids = request.getIds();
        if (CollectionUtils.isEmpty(ids)) {
            AssertUtils.logAndThrow("请至少选择一条数据！！", user.getLocale());
        }

        ValueHolder vh = new ValueHolder();
        SgPhyOutNoticesRecallFromWMSService sgPhyOutNoticesRecallFromWMSService = ApplicationContextHandle.getBean(SgPhyOutNoticesRecallFromWMSService.class);


        // 单对象页面返回
        if (request.getIsObj()) {
            try {
                ValueHolderV14 baseModelR3 = sgPhyOutNoticesRecallFromWMSService.cancelWms(user, ids.getLong(0));
                vh.put(R3ParamConstants.CODE, baseModelR3.getCode());
                vh.put(R3ParamConstants.MESSAGE, baseModelR3.getMessage());
            } catch (Exception e) {
                log.error("wms撤回接口异常:" + e.getMessage());
                vh.put(R3ParamConstants.CODE, ResultCode.FAIL);
                vh.put(R3ParamConstants.MESSAGE, "wms撤回接口异常:" + e.getMessage());
            }
            return vh;
        }

        //列表页面返回
        JSONArray errorArr = new JSONArray();
        Integer accSuccess = 0, accFailed = 0;
        for (int i = 0; i < ids.size(); i++) {
            try {
                ValueHolderV14 baseModelR3 = cancelWms(user, ids.getLong(i));
                Integer code = baseModelR3.getCode();
                if (code == ResultCode.SUCCESS) {
                    accSuccess++;
                } else {
                    throw new NDSException(Resources.getMessage(baseModelR3.getMessage()));
                }
            } catch (Exception e) {
                e.printStackTrace();
                log.error("objid:" + ids.getLong(i) + ",wms撤回接口异常:" + e.getMessage());
                JSONObject errorDate = new JSONObject();
                errorDate.put(R3ParamConstants.CODE, ResultCode.FAIL);
                errorDate.put(R3ParamConstants.OBJID, ids.getLong(i));
                errorDate.put(R3ParamConstants.MESSAGE, "wms撤回失败:" + e.getMessage());
                errorArr.add(errorDate);
                accFailed++;
            }
        }

        String message = "wms撤回成功记录数：" + accSuccess;
        if (!CollectionUtils.isEmpty(errorArr)) {
            message += "，wms撤回失败记录数：" + accFailed;
            return ValueHolderUtils.fail(message, errorArr);
        } else {
            return ValueHolderUtils.success(message);
        }
    }

    /**
     * 更新WMS状态
     *
     * @param objId 主键ID
     * @param user  用户信息
     * @return 返回
     */
    @Transactional(rollbackFor = Exception.class)
    public ValueHolderV14 cancelWms(User user, Long objId) {

        ValueHolderV14 vh = new ValueHolderV14(ResultCode.SUCCESS, "从WMS撤回成功");
        SgBPhyOutNoticesMapper noticesMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesMapper.class);
        SgBPhyOutNotices outNotices = noticesMapper.selectById(objId);


        if (outNotices == null) {
            vh.setMessage(Resources.getMessage("当前记录不存在！"));
            vh.setCode(ResultCode.FAIL);
            return vh;
        }

        Integer billStatus = outNotices.getBillStatus();
        if (billStatus != null && billStatus != SgOutConstantsIF.OUT_NOTICES_STATUS_WAIT) {
            vh.setMessage(Resources.getMessage("单据状态异常，不允许从wms撤回！"));
            vh.setCode(ResultCode.FAIL);
            return vh;
        }

        Long wmsStatus = outNotices.getWmsStatus();
        if (wmsStatus != null && wmsStatus != SgOutConstantsIF.WMS_STATUS_PASSING
                && wmsStatus != SgOutConstantsIF.WMS_STATUS_PASS_SUCCESS) {
            vh.setCode(ResultCode.FAIL);
            vh.setMessage(Resources.getMessage("单据传wms状态异常，不允许从wms撤回！"));
            return vh;
        }



        QimenOrderCancelCmd orderCancelCmd = (QimenOrderCancelCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(), QimenOrderCancelCmd.class.getName(), "ip", "1.4.0");

        CpCPhyWarehouseMapper warehouseMapper = ApplicationContextHandle.getBean(CpCPhyWarehouseMapper.class);
        CpCPhyWarehouse warehouse = warehouseMapper.selectById(outNotices.getCpCPhyWarehouseId());

        QimenOrderCancelModel cancelModel = new QimenOrderCancelModel();
        cancelModel.setOperateUser(user);
        cancelModel.setWarehouseCode(warehouse != null ? warehouse.getWmsWarehouseCode() : "");
        cancelModel.setOrderCode(outNotices.getBillNo());
        cancelModel.setOrderType(getOrderType(outNotices.getSourceBillType()));
        cancelModel.setCustomerId(warehouse != null ? warehouse.getWmsAccount() : "");
        cancelModel.setOwnerCode(outNotices.getGoodsOwner());
        cancelModel.setOrderId(outNotices.getWmsBillNo());
        ValueHolderV14 qimenResult = orderCancelCmd.cancelQimenOrder(cancelModel);

        if (qimenResult.isOK()) {
            SgBPhyOutNotices update = new SgBPhyOutNotices();
            update.setId(outNotices.getId());
            update.setIsPassWms(SgInConstants.NOTICE_IS_PASS_WMS_NO);
            update.setWmsStatus((long) SgInNoticeConstants.WMS_UPLOAD_STATUTS_NO);
            StorageESUtils.setBModelDefalutDataByUpdate(update, user);
            int i = noticesMapper.updateById(update);
            if (i > 0) {
                log.info("从WMS撤回成功,更新通知单状态成功!");
            } else {
                vh.setCode(ResultCode.FAIL);
                vh.setMessage("从WMS撤回失败,更新通知单状态失败!");
                return vh;
            }

            //推送es
            warehouseESUtils.pushESByOutNotices(objId, false, false, null, noticesMapper, null);
            return vh;
        } else {
            vh.setCode(ResultCode.FAIL);
            vh.setMessage("从WMS撤回失败," + qimenResult.getMessage());
            return vh;
        }
    }

    /**
     * 传奇门类型转换
     *
     * @param sourceType
     * @return
     */
    public String getOrderType(Integer sourceType) {
        String orderType;
        switch (sourceType) {
            case SgConstantsIF.BILL_TYPE_SALE:
                orderType = SgInNoticeConstants.QM_BILL_TYPE_B2B_OUT;
                break;
            case SgConstantsIF.BILL_TYPE_TRANSFER:
                orderType = SgInNoticeConstants.QM_BILL_TYPE_TRANSFER_OUT;
                break;
            case SgConstantsIF.BILL_TYPE_PUR_REF:
                orderType = SgInNoticeConstants.QM_BILL_TYPE_PURCHASE_REF_OUT;
                break;
            case SgConstantsIF.BILL_TYPE_VIPSHOP:
                orderType = SgInNoticeConstants.QM_BILL_TYPE_WEIPINHUI_OUT;
                break;
            default:
                orderType = SgInNoticeConstants.QM_BILL_TYPE_NORMAL_OUT;
                break;
        }
        return orderType;
    }

}
