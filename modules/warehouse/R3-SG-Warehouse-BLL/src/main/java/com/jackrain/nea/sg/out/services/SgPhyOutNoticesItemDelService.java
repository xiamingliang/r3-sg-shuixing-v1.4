package com.jackrain.nea.sg.out.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.model.result.SgR3BaseResult;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.out.common.SgOutConstants;
import com.jackrain.nea.sg.out.common.SgOutConstantsIF;
import com.jackrain.nea.sg.out.mapper.SgBPhyOutNoticesItemMapper;
import com.jackrain.nea.sg.out.mapper.SgBPhyOutNoticesMapper;
import com.jackrain.nea.sg.out.model.request.SgPhyOutNoticesItemBillDelRequest;
import com.jackrain.nea.sg.out.model.request.SgPhyOutNoticesSaveRequest;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutNotices;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutNoticesItem;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * @author leexh
 * @since 2019/4/24 10:17
 */
@Slf4j
@Component
public class SgPhyOutNoticesItemDelService {

    public ValueHolderV14<SgR3BaseResult> delSgPhyOutNoticesItem(SgPhyOutNoticesItemBillDelRequest request) {

        ValueHolderV14<SgR3BaseResult> v14 = new ValueHolderV14();
        AssertUtils.notNull(request, "参数不能为空！");
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ".debug, param:" + JSONObject.toJSONString(request));
        }
        // 参数校验
        checkParams(request);
        User loginUser = request.getLoginUser();
        long userId = loginUser.getId().longValue();
        String name = loginUser.getName();
        String eName = loginUser.getEname();
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());

        Long objId = request.getObjId();
        if (objId == null) {

            /**
             * 条件1：出库通知单单据编号
             * 条件2：来源单据ID + 来源单据类型
             * 条件3：来源单据编号 + 来源单据类型
             */
            SgPhyOutNoticesSaveService saveService = ApplicationContextHandle.getBean(SgPhyOutNoticesSaveService.class);
            SgPhyOutNoticesSaveRequest noticesRequest = request.getNoticesRequest();
            SgBPhyOutNotices sgBPhyOutNotices = saveService.queryNoticesBySource(noticesRequest);
            AssertUtils.notNull(sgBPhyOutNotices, "当前记录已不存在！", loginUser.getLocale());
            Integer billStatus = sgBPhyOutNotices.getBillStatus();
            if (billStatus != null && billStatus != SgOutConstantsIF.OUT_NOTICES_STATUS_WAIT) {
                AssertUtils.logAndThrow("当前记录不是待出库状态，不允许操作！", loginUser.getLocale());
            }
            objId = sgBPhyOutNotices.getId();
        }

        // 删除明细
        SgBPhyOutNoticesItemMapper noticesItemMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesItemMapper.class);
        QueryWrapper<SgBPhyOutNoticesItem> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(SgOutConstants.OUT_NOTICES_PAREN_FIELD, objId);
        if (!CollectionUtils.isEmpty(request.getIds())) {
            queryWrapper.in("id", request.getIds());
        } else {
            queryWrapper.in(SgOutConstants.SOURCE_BILL_ITEM_ID, request.getSourceBillItemIds());
        }
        int count = noticesItemMapper.delete(queryWrapper);
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ".debug, 出库通知单明细删除成功：" + count);
        }

        // 更新主表
        if (count > 0) {

            SgBPhyOutNoticesMapper noticesMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesMapper.class);
            BigDecimal totAmtList = BigDecimal.ZERO;
            BigDecimal totAmtCost = BigDecimal.ZERO;
            SgBPhyOutNoticesItem noticesItem = noticesItemMapper.selectSumAmt(objId);
            if (noticesItem != null) {
                totAmtList = noticesItem.getAmtList();
                totAmtCost = noticesItem.getAmtCost();
            }

            SgBPhyOutNotices outNotices = new SgBPhyOutNotices();
            outNotices.setId(objId);
            outNotices.setTotAmtList(totAmtList);
            outNotices.setTotAmtCost(totAmtCost);
            outNotices.setModifierid(userId);
            outNotices.setModifiername(name);
            outNotices.setModifierename(eName);
            outNotices.setModifieddate(timestamp);
            noticesMapper.updateById(outNotices);
        }

        v14.setCode(ResultCode.SUCCESS);
        v14.setMessage("删除成功！删除明细数量：" + count);
        return v14;
    }

    /**
     * 参数合法性校验
     *
     * @param request
     */
    public void checkParams(SgPhyOutNoticesItemBillDelRequest request) {

        User loginUser = request.getLoginUser();
        AssertUtils.notNull(loginUser, "请传入用户信息！");
        if (request.getObjId() == null) {

            // 接口入参
            SgPhyOutNoticesSaveRequest outNotices = request.getNoticesRequest();
            AssertUtils.notNull(outNotices, "请传入主单据信息！", loginUser.getLocale());
            String billNo = outNotices.getBillNo();
            Integer sourceBillType = outNotices.getSourceBillType();
            Long sourceBillId = outNotices.getSourceBillId();
            String sourceBillNo = outNotices.getSourceBillNo();

            if (!((sourceBillType != null && sourceBillId != null) || (sourceBillType != null && StringUtils.isNotEmpty(sourceBillNo)) || StringUtils.isNotEmpty(billNo))) {
                AssertUtils.logAndThrow("来源单据ID或者来源单据编号和来源单据类型不能同时为空！", loginUser.getLocale());
            }

            if (CollectionUtils.isEmpty(request.getSourceBillItemIds())) {
                AssertUtils.logAndThrow("请传入来源单据明细ID！", loginUser.getLocale());
            }

        } else {

            // 页面入参
            if (CollectionUtils.isEmpty(request.getIds())) {
                AssertUtils.logAndThrow("请选择需要删除的明细数据！", loginUser.getLocale());
            }
        }
    }
}
