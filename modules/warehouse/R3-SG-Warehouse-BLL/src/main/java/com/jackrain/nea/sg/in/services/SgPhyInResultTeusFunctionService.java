package com.jackrain.nea.sg.in.services;

import com.alibaba.fastjson.JSONArray;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.jackrain.nea.core.db.Tools;
import com.jackrain.nea.model.util.ModelUtil;
import com.jackrain.nea.oc.basic.common.OcBasicConstants;
import com.jackrain.nea.redis.config.CusRedisTemplate;
import com.jackrain.nea.redis.util.RedisOpsUtil;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.in.mapper.SgBPhyInResultImpItemMapper;
import com.jackrain.nea.sg.in.mapper.SgBPhyInResultTeusItemMapper;
import com.jackrain.nea.sg.in.model.request.SgBPhyInResultItemSaveRequest;
import com.jackrain.nea.sg.in.model.request.SgPhyInResultBillSaveRequest;
import com.jackrain.nea.sg.in.model.request.SgPhyInResultImpItemSaveRequest;
import com.jackrain.nea.sg.in.model.request.SgPhyInResultTeusItemSaveRequest;
import com.jackrain.nea.sg.in.model.table.SgBPhyInResultImpItem;
import com.jackrain.nea.sg.in.model.table.SgBPhyInResultTeusItem;
import com.jackrain.nea.sg.send.utils.SgStoreUtils;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.*;

/**
 * @author zhoulinsheng
 * @since 2019-10-22
 * create at : 2019-10-22 15:07
 */
@Slf4j
@Component
public class SgPhyInResultTeusFunctionService {

    /**
     * 入库结果单-录入明细转换成条码明细/箱内明细
     *
     * @param request 录入明细
     */
    public void impRequestConvertMethod(SgPhyInResultBillSaveRequest request) {
        if (CollectionUtils.isEmpty(request.getImpItemList()) && StringUtils.isEmpty(request.getRedisKey())) {
            AssertUtils.logAndThrow("录入明细不能为空!");
        } else if (CollectionUtils.isEmpty(request.getImpItemList()) && StringUtils.isNotEmpty(request.getRedisKey())) {
            CusRedisTemplate<String, String> redisTemplate = RedisOpsUtil.getStrRedisTemplate();
            String result = redisTemplate.opsForValue().get(request.getRedisKey());
            SgPhyInResultBillSaveRequest saveRequest = JSONArray.parseObject(result, SgPhyInResultBillSaveRequest.class);
            if (saveRequest == null || CollectionUtils.isEmpty(saveRequest.getImpItemList())) {
                AssertUtils.logAndThrow("[" + request.getRedisKey() + "]录入明细不能为空!");
            } else {
                request.setImpItemList(saveRequest.getImpItemList());
                request.setTeusItemList(saveRequest.getTeusItemList());
            }
        }

        HashMap<Long, SgPhyInResultImpItemSaveRequest> impItemSaveRequestHashMap = Maps.newHashMap();
        List<SgBPhyInResultItemSaveRequest> itemList = Lists.newArrayList();

        //录入明细不为空
        for (SgPhyInResultImpItemSaveRequest impItem : request.getImpItemList()) {
            if (impItem.getIsTeus().equals(OcBasicConstants.IS_MATCH_SIZE_N)) {
                //散件条码转条码明细
                SgBPhyInResultItemSaveRequest itemSaveRequest = new SgBPhyInResultItemSaveRequest();
                BeanUtils.copyProperties(impItem, itemSaveRequest);
                itemSaveRequest.setQtyIn(impItem.getQtyIn());
                itemList.add(itemSaveRequest);
            } else {
                impItemSaveRequestHashMap.put(impItem.getPsCTeusId(), impItem);
            }
        }

        //箱明细不为空
        if (CollectionUtils.isNotEmpty(request.getTeusItemList())) {
            //箱内明细转条码明细
            for (SgPhyInResultTeusItemSaveRequest teusItemSaveRequest : request.getTeusItemList()) {
                SgBPhyInResultItemSaveRequest itemSaveRequest = new SgBPhyInResultItemSaveRequest();
                SgPhyInResultImpItemSaveRequest impItemSaveRequest = impItemSaveRequestHashMap.get(teusItemSaveRequest.getPsCTeusId());
                BeanUtils.copyProperties(teusItemSaveRequest, itemSaveRequest);
                itemSaveRequest.setSgBPhyInResultId(teusItemSaveRequest.getSgBPhyInResultId());
                itemSaveRequest.setPriceList(impItemSaveRequest.getPriceList());
                itemSaveRequest.setQtyIn(teusItemSaveRequest.getQty());
                itemList.add(itemSaveRequest);
            }
        }

        request.setItemList(itemList);
    }


    /**
     * 入库结果单-录入明细新增
     *
     * @param objId       通知单id
     * @param impItemList 录入明细
     * @param loginUser   用户
     */
    public void insertInResultImpItem(Long objId, List<SgPhyInResultImpItemSaveRequest> impItemList, User loginUser) {
        if (CollectionUtils.isNotEmpty(impItemList)) {
            SgBPhyInResultImpItemMapper impItemMapper = ApplicationContextHandle.getBean(SgBPhyInResultImpItemMapper.class);

            Map<Long, SgPhyInResultImpItemSaveRequest> skuRequestMap = Maps.newHashMap();
            Map<Long, SgPhyInResultImpItemSaveRequest> teusRequestMap = Maps.newHashMap();
            for (SgPhyInResultImpItemSaveRequest ImpItemRequest : impItemList) {
                if (ImpItemRequest.getIsTeus().equals(OcBasicConstants.IS_MATCH_SIZE_N)) {
                    //散件合并相同sku的明细
                    Long psCSkuId = ImpItemRequest.getPsCSkuId();
                    if (skuRequestMap.containsKey(psCSkuId)) {
                        SgPhyInResultImpItemSaveRequest orgImpitem = skuRequestMap.get(psCSkuId);
                        BigDecimal orgQty = Optional.ofNullable(orgImpitem.getQtyIn()).orElse(BigDecimal.ZERO);
                        BigDecimal qty = Optional.ofNullable(ImpItemRequest.getQtyIn()).orElse(BigDecimal.ZERO);
                        orgImpitem.setQtyIn(qty.add(orgQty));
                    } else {
                        skuRequestMap.put(psCSkuId, ImpItemRequest);
                    }

                } else {
                    //合并相同箱
                    Long psCTeusId = ImpItemRequest.getPsCTeusId();
                    if (teusRequestMap.containsKey(psCTeusId)) {
                        SgPhyInResultImpItemSaveRequest orgInResultImpItemSaveRequest = teusRequestMap.get(psCTeusId);
                        BigDecimal orgQty = Optional.ofNullable(orgInResultImpItemSaveRequest.getQtyIn()).orElse(BigDecimal.ZERO);
                        BigDecimal qty = Optional.ofNullable(ImpItemRequest.getQtyIn()).orElse(BigDecimal.ZERO);
                        orgInResultImpItemSaveRequest.setQtyIn(qty.add(orgQty));
                    } else {
                        teusRequestMap.put(psCTeusId, ImpItemRequest);
                    }
                }
            }
            //合并后的明细
            List<SgPhyInResultImpItemSaveRequest> allImpItemList = new ArrayList<>();
            allImpItemList.addAll(skuRequestMap.values());
            allImpItemList.addAll(teusRequestMap.values());

            List<SgBPhyInResultImpItem> impItems = new ArrayList<>();
            allImpItemList.forEach(item -> {
                SgBPhyInResultImpItem impItem = new SgBPhyInResultImpItem();
                Long id = Tools.getSequence(SgConstants.SG_B_PHY_IN_RESULT_IMP_ITEM);
                BeanUtils.copyProperties(item, impItem);
                impItem.setId(id);
                impItem.setSgBPhyInResultId(objId);
                impItem.setQtyIn(item.getQtyIn());        // 入库数量
                impItem.setOwnerename(loginUser.getEname());
                impItem.setModifierename(loginUser.getEname());
                StorageESUtils.setBModelDefalutData(impItem, loginUser);
                impItems.add(impItem);
            });

            //分批次，批量新增
            SgStoreUtils.batchInsertTeus(impItems, "入库结果单单录入明细", SgConstants.SG_COMMON_MAX_INSERT_PAGE_SIZE, impItemMapper);
        }
    }

    /**
     * 入库结果单-箱明细新增
     *
     * @param objId        通知单id
     * @param teusItemList 录入明细
     * @param loginUser    用户
     */
    public void insertInResultTeusItem(Long objId, List<SgPhyInResultTeusItemSaveRequest> teusItemList, User loginUser) {
        if (CollectionUtils.isNotEmpty(teusItemList)) {
            SgBPhyInResultTeusItemMapper teusItemMapper = ApplicationContextHandle.getBean(SgBPhyInResultTeusItemMapper.class);
            List<SgBPhyInResultTeusItem> teusItems = new ArrayList<>();

            //封装入库通知单箱明细其他参数
            for (SgPhyInResultTeusItemSaveRequest teusItemSaveRequest : teusItemList) {
                SgBPhyInResultTeusItem teusItem = new SgBPhyInResultTeusItem();
                Long id = ModelUtil.getSequence(SgConstants.SG_B_PHY_IN_RESULT_TEUS_ITEM);

                BeanUtils.copyProperties(teusItemSaveRequest, teusItem);
                teusItem.setId(id);
                teusItem.setSgBPhyInResultId(objId);
                teusItem.setOwnerename(loginUser.getEname());
                teusItem.setModifierename(loginUser.getEname());
                StorageESUtils.setBModelDefalutData(teusItem, loginUser);
                teusItems.add(teusItem);
            }

            //分批次，批量新增
            SgStoreUtils.batchInsertTeus(teusItems, "入库结果单箱内明细", SgConstants.SG_COMMON_MAX_INSERT_PAGE_SIZE, teusItemMapper);
        }
    }


    /**
     * 入库结果单-录入明细更新
     *
     * @param objId       通知单id
     * @param impItemList 录入明细
     * @param loginUser   用户
     */
    public void updateInResultImpItem(Long objId, List<SgPhyInResultImpItemSaveRequest> impItemList, User loginUser) {
        if (CollectionUtils.isNotEmpty(impItemList)) {
            SgBPhyInResultImpItemMapper impItemMapper = ApplicationContextHandle.getBean(SgBPhyInResultImpItemMapper.class);
            List<SgBPhyInResultImpItem> insertList = Lists.newArrayList();
            for (SgPhyInResultImpItemSaveRequest impItemSaveRequest : impItemList) {
                SgBPhyInResultImpItem item = new SgBPhyInResultImpItem();
                BeanUtils.copyProperties(impItemSaveRequest, item);
                item.setModifierename(loginUser.getEname());
                if (item.getId() != null && item.getId() > 0) {
                    StorageESUtils.setBModelDefalutDataByUpdate(item, loginUser);
                    impItemMapper.updateById(item);
                } else {
                    item.setOwnerename(loginUser.getEname());
                    item.setSgBPhyInResultId(objId);
                    StorageESUtils.setBModelDefalutData(item, loginUser);
                    insertList.add(item);
                }
            }
            if (CollectionUtils.isNotEmpty(insertList)) {
                SgStoreUtils.batchInsertTeus(insertList, "入库结果单录入明细", SgConstants.SG_COMMON_INSERT_PAGE_SIZE, impItemMapper);
            }
        }
    }

    /**
     * 入库结果单-箱内明细更新
     *
     * @param objId       通知单id
     * @param teusItemList 箱内明细
     * @param loginUser   用户
     */
    public void updateInResultTeusItem(Long objId, List<SgPhyInResultTeusItemSaveRequest> teusItemList, User loginUser) {
        if (CollectionUtils.isNotEmpty(teusItemList)) {
            SgBPhyInResultTeusItemMapper teusItemMapper = ApplicationContextHandle.getBean(SgBPhyInResultTeusItemMapper.class);
            List<SgBPhyInResultTeusItem> insertList = Lists.newArrayList();
            for (SgPhyInResultTeusItemSaveRequest teusItemSaveRequest : teusItemList) {
                SgBPhyInResultTeusItem item = new SgBPhyInResultTeusItem();
                BeanUtils.copyProperties(teusItemSaveRequest, item);
                item.setModifierename(loginUser.getEname());
                if (item.getId() != null && item.getId() > 0) {
                    StorageESUtils.setBModelDefalutDataByUpdate(item, loginUser);
                    teusItemMapper.updateById(item);
                } else {
                    item.setOwnerename(loginUser.getEname());
                    item.setSgBPhyInResultId(objId);
                    StorageESUtils.setBModelDefalutData(item, loginUser);
                    insertList.add(item);
                }
            }
            if (CollectionUtils.isNotEmpty(insertList)) {
                SgStoreUtils.batchInsertTeus(insertList, "入库结果单箱内明细", SgConstants.SG_COMMON_INSERT_PAGE_SIZE, teusItemMapper);
            }
        }
    }
}
