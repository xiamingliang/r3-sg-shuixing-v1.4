package com.jackrain.nea.sg.inv.mapper;

import com.jackrain.nea.oc.basic.common.OcBasicConstants;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.inv.constants.SgInvConstants;
import org.apache.ibatis.annotations.Param;

import java.util.Date;

/**
 * @author 舒威
 * @since 2019/12/4
 * create at : 2019/12/4 15:42
 */
public class ScProfitTeusTotalSqlProvider {

    /**
     * 抽盘-合计盈亏数量
     */
    public String queryTeusTotalProfByCP(@Param("storeId") Long storeId,
                                         @Param("inventoryType") Integer inventoryType,
                                         @Param("inventoryDate") Date inventoryDate,
                                         @Param("flag") Boolean flag) {
        StringBuilder sql = new StringBuilder("SELECT\n" +
                "SUM ( CASE WHEN COALESCE (P .SUM_QTY, 0) - COALESCE (D.QTY_STORAGE, 0) >= 0 " +
                "THEN COALESCE (P .SUM_QTY, 0) - COALESCE (D.QTY_STORAGE, 0) ELSE 0 END ) AS \"QTY_PROFIT\", \n" +
                "SUM ( CASE WHEN COALESCE (P .SUM_QTY, 0) - COALESCE (D.QTY_STORAGE, 0) < 0" +
                "THEN COALESCE (P .SUM_QTY, 0) - COALESCE (D.QTY_STORAGE, 0) ELSE 0 END ) AS \"QTY_LOSS\"\n" +
                "FROM\n" +
                "\t(\n" +
                "\t\tSELECT\n" +
                "\t\t\tSG_B_PHY_TEUS_STORAGE_DAILY.CP_C_PHY_WAREHOUSE_ID,\n" +
                "\t\t\tSG_B_PHY_TEUS_STORAGE_DAILY.PS_C_TEUS_ID,\n" +
                "\t\t\tSG_B_PHY_TEUS_STORAGE_DAILY.QTY_STORAGE");
        if (flag) {//盘点日期=系统日期
            sql.append("\tFROM SG_B_PHY_TEUS_STORAGE SG_B_PHY_TEUS_STORAGE_DAILY");
        } else {
            sql.append("\tFROM SG_B_PHY_TEUS_STORAGE_DAILY");
        }
        sql.append("\tWHERE\n" +
                "\t\t\tEXISTS (\n" +
                "\t\t\t\tSELECT\n" +
                "\t\t\t\t\t1\n" +
                "\t\t\t\tFROM\n" +
                "\t\t\t\t\tSC_B_INVENTORY\n" +
                "\t\t\t\tINNER JOIN SC_B_INVENTORY_IMP_ITEM ON SC_B_INVENTORY. ID = SC_B_INVENTORY_IMP_ITEM.SC_B_INVENTORY_ID\n" +
                "\t\t\t\tWHERE\n" +
                "\t\t\t\t\tSG_B_PHY_TEUS_STORAGE_DAILY.PS_C_TEUS_ID = SC_B_INVENTORY_IMP_ITEM.PS_C_TEUS_ID\n" +
                "\t\t\t\tAND SG_B_PHY_TEUS_STORAGE_DAILY.CP_C_PHY_WAREHOUSE_ID = SC_B_INVENTORY.CP_C_PHY_WAREHOUSE_ID\n" +
                "\t\t\t\tAND SC_B_INVENTORY.CP_C_PHY_WAREHOUSE_ID = " + storeId +
                "\t\t\t\tAND SC_B_INVENTORY.INVENTORY_DATE = '" + inventoryDate + "'\n" +
                "\t\t\t\tAND SC_B_INVENTORY.INVENTORY_TYPE = " + inventoryType +
                "\t\t\t\tAND SC_B_INVENTORY.ISACTIVE = '" + SgConstants.IS_ACTIVE_Y + "'\n" +
                "\t\t\t\tAND SC_B_INVENTORY.POL_STATUS = " + SgInvConstants.PAND_UNPOL +
                "\t\t\t\tAND SC_B_INVENTORY_IMP_ITEM.IS_TEUS = " + OcBasicConstants.IS_MATCH_SIZE_Y +
                "\t\t\t)\n" +
                "\t\tAND SG_B_PHY_TEUS_STORAGE_DAILY.CP_C_PHY_WAREHOUSE_ID = " + storeId);
        if (!flag) {//盘点日期=系统日期
            sql.append("\tAND SG_B_PHY_TEUS_STORAGE_DAILY.STOCK_DATE = '" + inventoryDate + "'");
        }
        sql.append(") D\n" +
                "FULL OUTER JOIN (\n" +
                "\tSELECT\n" +
                "\t\tSC_B_INVENTORY.CP_C_PHY_WAREHOUSE_ID,\n" +
                "\t\tSC_B_INVENTORY_IMP_ITEM.PS_C_TEUS_ID,\n" +
                "\t\tSUM (SC_B_INVENTORY_IMP_ITEM.QTY) AS SUM_QTY\n" +
                "\tFROM\n" +
                "\t\tSC_B_INVENTORY\n" +
                "\tINNER JOIN SC_B_INVENTORY_IMP_ITEM ON SC_B_INVENTORY. ID = SC_B_INVENTORY_IMP_ITEM.SC_B_INVENTORY_ID\n" +
                "\tWHERE\n" +
                "\t\tSC_B_INVENTORY.CP_C_PHY_WAREHOUSE_ID = " + storeId +
                "\tAND SC_B_INVENTORY.INVENTORY_DATE = '" + inventoryDate + "'\n" +
                "\tAND SC_B_INVENTORY.INVENTORY_TYPE = " + inventoryType +
                "\tAND SC_B_INVENTORY.ISACTIVE = '" + SgConstants.IS_ACTIVE_Y + "'\n" +
                "\tAND SC_B_INVENTORY.POL_STATUS = " + SgInvConstants.PAND_UNPOL +
                "\tAND SC_B_INVENTORY_IMP_ITEM.IS_TEUS = " + OcBasicConstants.IS_MATCH_SIZE_Y +
                "\tGROUP BY\n" +
                "\t\tSC_B_INVENTORY.CP_C_PHY_WAREHOUSE_ID,\n" +
                "\t\tSC_B_INVENTORY_IMP_ITEM.PS_C_TEUS_ID\n" +
                ") P ON D.PS_C_TEUS_ID = P .PS_C_TEUS_ID\n" +
                "AND D.CP_C_PHY_WAREHOUSE_ID = P .CP_C_PHY_WAREHOUSE_ID");
        return sql.toString();
    }

    /**
     * 全盘-合计盈亏数量
     */
    public String queryTeusTotalProfByQP(@Param("storeId") Long storeId,
                                         @Param("inventoryType") Integer inventoryType,
                                         @Param("inventoryDate") Date inventoryDate,
                                         @Param("flag") Boolean flag) {
        StringBuilder sql = new StringBuilder("SELECT\n" +
                "SUM ( CASE WHEN COALESCE (P .SUM_QTY, 0) - COALESCE (D.QTY_STORAGE, 0) >= 0 " +
                "THEN COALESCE (P .SUM_QTY, 0) - COALESCE (D.QTY_STORAGE, 0) ELSE 0 END ) AS \"QTY_PROFIT\", \n" +
                "SUM ( CASE WHEN COALESCE (P .SUM_QTY, 0) - COALESCE (D.QTY_STORAGE, 0) < 0" +
                "THEN COALESCE (P .SUM_QTY, 0) - COALESCE (D.QTY_STORAGE, 0) ELSE 0 END ) AS \"QTY_LOSS\"\n" +
                "FROM\n" +
                "\t(\n" +
                "\t\tSELECT\n" +
                "\t\t\tSG_B_PHY_TEUS_STORAGE_DAILY.CP_C_PHY_WAREHOUSE_ID,\n" +
                "\t\t\tSG_B_PHY_TEUS_STORAGE_DAILY.PS_C_TEUS_ID,\n" +
                "\t\t\tSG_B_PHY_TEUS_STORAGE_DAILY.QTY_STORAGE");
        if (flag) {//盘点日期=系统日期
            sql.append("\tFROM SG_B_PHY_TEUS_STORAGE SG_B_PHY_TEUS_STORAGE_DAILY\n" +
                    "\t\t\tWHERE SG_B_PHY_TEUS_STORAGE_DAILY.CP_C_PHY_WAREHOUSE_ID = " + storeId);
        } else {
            sql.append("\tFROM SG_B_PHY_TEUS_STORAGE_DAILY\n" +
                    "\t\t\tWHERE SG_B_PHY_TEUS_STORAGE_DAILY.CP_C_PHY_WAREHOUSE_ID = " + storeId +
                    "\tAND SG_B_PHY_TEUS_STORAGE_DAILY.STOCK_DATE = '" + inventoryDate + "'");
        }
        sql.append(") D\n" +
                "FULL OUTER JOIN (\n" +
                "\t\tSELECT\n" +
                "\t\tSC_B_INVENTORY.CP_C_PHY_WAREHOUSE_ID,\n" +
                "\t\tSC_B_INVENTORY_IMP_ITEM.PS_C_TEUS_ID,\n" +
                "\t\tSUM (SC_B_INVENTORY_IMP_ITEM.QTY) AS SUM_QTY\n" +
                "\tFROM\n" +
                "\t\tSC_B_INVENTORY\n" +
                "\tINNER JOIN SC_B_INVENTORY_IMP_ITEM ON SC_B_INVENTORY. ID = SC_B_INVENTORY_IMP_ITEM.SC_B_INVENTORY_ID\n" +
                "\tWHERE\n" +
                "\t\tSC_B_INVENTORY.CP_C_PHY_WAREHOUSE_ID = " + storeId +
                "\tAND SC_B_INVENTORY.INVENTORY_DATE = '" + inventoryDate + "'\n" +
                "\tAND SC_B_INVENTORY.INVENTORY_TYPE = " + inventoryType +
                "\tAND SC_B_INVENTORY.ISACTIVE = '" + SgConstants.IS_ACTIVE_Y + "'\n" +
                "\tAND SC_B_INVENTORY.POL_STATUS = " + SgInvConstants.PAND_UNPOL +
                "\tAND SC_B_INVENTORY_IMP_ITEM.IS_TEUS = " + OcBasicConstants.IS_MATCH_SIZE_Y +
                "\tGROUP BY\n" +
                "\t\tSC_B_INVENTORY.CP_C_PHY_WAREHOUSE_ID,\n" +
                "\t\tSC_B_INVENTORY_IMP_ITEM.PS_C_TEUS_ID\n" +
                ") P on D.PS_C_TEUS_ID = P .PS_C_TEUS_ID\n" +
                "and D.CP_C_PHY_WAREHOUSE_ID = P .CP_C_PHY_WAREHOUSE_ID");
        return sql.toString();
    }
}
