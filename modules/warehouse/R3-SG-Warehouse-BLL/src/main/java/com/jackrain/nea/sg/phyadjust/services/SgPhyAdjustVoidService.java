package com.jackrain.nea.sg.phyadjust.services;

import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.redis.config.CusRedisTemplate;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.utils.R3ParamUtil;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.out.utils.SgWarehouseESUtils;
import com.jackrain.nea.sg.phyadjust.common.SgPhyAdjustConstants;
import com.jackrain.nea.sg.phyadjust.mapper.SgBPhyAdjustMapper;
import com.jackrain.nea.sg.phyadjust.model.table.SgBPhyAdjust;
import com.jackrain.nea.sg.send.utils.SgStoreUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.utility.ExceptionUtil;
import com.jackrain.nea.utils.AssertUtils;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * @description: 库存调整单-作废
 * @author: 郑小龙
 * @date: 2019-08-12 19:15
 */
@Slf4j
@Component
public class SgPhyAdjustVoidService {

    @Autowired
    private SgWarehouseESUtils warehouseESUtils;

    @Transactional(rollbackFor = Exception.class)
    public ValueHolder phyAdjustBillVoid(Long objId, User user) {
        if (log.isDebugEnabled()) {
            log.debug("Start " + this.getClass().getName() + ".phyAdjustBillVoid. ReceiveParams:objId="
                    + objId + ";");
        }
        SgBPhyAdjustMapper mapper = ApplicationContextHandle.getBean(SgBPhyAdjustMapper.class);
        SgBPhyAdjust adjust = mapper.selectById(objId);
        return R3ParamUtil.convertV14(adjustBillVoid(adjust, mapper, user));
    }

    /**
     * 作废
     *
     * @param adjust
     * @param mapper
     * @param user
     * @return
     */
    private ValueHolderV14 adjustBillVoid(SgBPhyAdjust adjust, SgBPhyAdjustMapper mapper, User user) {
        Locale locale = user.getLocale();
        statusCheck(adjust, locale);
        CusRedisTemplate<String, String> redisTemplate = SgStoreUtils.initRedisTemplate();
        String lockKsy = SgConstants.SG_B_PHY_ADJUST + ":" + adjust.getBillNo();
        if (redisTemplate.opsForValue().get(lockKsy) != null) {
            throw new NDSException(Resources.getMessage("当前记录正在被操作,请稍后重试!", user.getLocale()));
        } else {
            try {
                redisTemplate.opsForValue().set(lockKsy, "OK", 30, TimeUnit.MINUTES);
                long objId = adjust.getId();
                SgBPhyAdjust updateAdj = new SgBPhyAdjust();
                updateAdj.setId(objId);
                StorageESUtils.setBModelDefalutDataByUpdate(updateAdj, user);
                updateAdj.setModifierename(user.getEname());
                updateAdj.setIsactive(SgConstants.IS_ACTIVE_N);//
                updateAdj.setBillStatus(SgPhyAdjustConstants.ADJ_AUDIT_STATUS_V);//状态改为 已作废
                mapper.updateById(updateAdj);

                //推送es
                warehouseESUtils.pushESByPhyAdjust(objId, false, false, null, mapper, null);
            } catch (Exception e) {
                AssertUtils.logAndThrow(ExceptionUtil.getMessage(e), locale);
            } finally {
                redisTemplate.delete(lockKsy);
            }
            return new ValueHolderV14(ResultCode.SUCCESS, Resources.getMessage("作废成功!", locale));
        }
    }

    /**
     * 单据状态校验
     */
    private void statusCheck(SgBPhyAdjust adjust, Locale locale) {
        AssertUtils.notNull(adjust, "当前单据已不存在!", locale);
        AssertUtils.notNull(adjust.getBillStatus(), "当数据错误-bill_status不存在!", locale);
        AssertUtils.cannot(adjust.getBillStatus() == SgPhyAdjustConstants.ADJ_AUDIT_STATUS_Y, "当前单据已审核，不能作废!", locale);
        AssertUtils.cannot(adjust.getBillStatus() == SgPhyAdjustConstants.ADJ_AUDIT_STATUS_V, "当前单据已作废，不能重复作废!", locale);
    }

}

