package com.jackrain.nea.sg.phyadjust.common;

/**
 * @author csy
 * Date: 2019/4/29
 * Description:
 */
public interface SgPhyAdjustConstants {

    String PHY_ADJUST_SEQ = "SEQ_SG_B_PHY_ADJUST";

    /**
     * 审核状态 未审核
     */
    int ADJ_AUDIT_STATUS_N = 1;

    /**
     * 审核状态 已审核
     */
    int ADJ_AUDIT_STATUS_Y = 2;

    /**
     * 审核状态 已作废
     */
    int ADJ_AUDIT_STATUS_V = 3;

    /**
     * 未完成
     */
    int STATUS_N = 1;

    /**
     * 已完成
     */
    int STATUS_Y = 2;


    /**
     * mq msgKey的时间规则
     */
    String MQ_MSG_KEY_TIME_PATTERN = "yyyyMMddHHmmssSSS";

    /**
     * 库存消息tag
     */
    String MQ_TAG = "phy_storage_phy_adjust";

    /**
     * 明细表 主表ID字段
     */
    String SG_B_PHY_ADJUST_ID = "sg_b_phy_adjust_id";

    /**
     * sg_b_adjust_prop表做相应添加  add chenxinxing  2020/02/26
     *
     * 调整性质
     * 无头件=1
     * 冲无头件=2
     * 错发调整=3
     * 损益调整=8
     * 盘差调整=12
     * 正常调整
     */
    long ADJUST_PROP_NOHEAD = 1;
    long ADJUST_PROP_FLUSH_NOHEAD = 2;
    long ADJUST_PROP_WRONG_ADJUST = 3;
    long ADJUST_PROP_PROFITLOSS_ADJUST =8;
    long ADJUST_PROP_NORMAL_ADJUST =10;
    long ADJUST_PROP_CHKDIFFE_ADJUST =12;
}
