package com.jackrain.nea.sg.out.services;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.core.db.Tools;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.oc.basic.common.OcBasicConstants;
import com.jackrain.nea.redis.config.CusRedisTemplate;
import com.jackrain.nea.redis.util.RedisOpsUtil;
import com.jackrain.nea.sg.basic.api.SgTeusPhyStorageQueryCmd;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.model.request.SgTeusPhyStorageQueryRequest;
import com.jackrain.nea.sg.basic.model.table.SgBPhyTeusStorage;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.out.common.SgOutConstants;
import com.jackrain.nea.sg.out.mapper.SgBPhyOutNoticesImpItemMapper;
import com.jackrain.nea.sg.out.mapper.SgBPhyOutNoticesTeusItemMapper;
import com.jackrain.nea.sg.out.model.request.SgPhyOutNoticesBillSaveRequest;
import com.jackrain.nea.sg.out.model.request.SgPhyOutNoticesImpItemSaveRequest;
import com.jackrain.nea.sg.out.model.request.SgPhyOutNoticesItemSaveRequest;
import com.jackrain.nea.sg.out.model.request.SgPhyOutNoticesTeusItemSaveRequest;
import com.jackrain.nea.sg.out.model.table.*;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author zhu lin yu
 * @since 2019-10-22
 * create at : 2019-10-22 15:07
 */
@Slf4j
@Component
public class SgPhyOutNoticesConvertService {

    /**
     * 出库通知单-录入明细转换成条码明细
     *
     * @param request 录入明细和箱明细
     */
    public void sgPhyOutNoticesRequestConvert(SgPhyOutNoticesBillSaveRequest request) {
        List<SgPhyOutNoticesImpItemSaveRequest> impItemList = new ArrayList<>();
        List<SgPhyOutNoticesTeusItemSaveRequest> teusItemList = new ArrayList<>();

        if (CollectionUtils.isEmpty(request.getImpItemList()) && StringUtils.isEmpty(request.getRedisKey())) {

            //录入明细model为空，且未存redis
            AssertUtils.logAndThrow("录入明细不能全为空!");

        } else if (StringUtils.isNotEmpty(request.getRedisKey())) {

            //从redis获取录入明细和箱明细
            CusRedisTemplate<String, String> redisTemplate = RedisOpsUtil.getStrRedisTemplate();
            String result = redisTemplate.opsForValue().get(request.getRedisKey());

            if (StringUtils.isNotEmpty(result)) {
                SgPhyOutNoticesBillSaveRequest saveRequest = JSONArray.parseObject(result, SgPhyOutNoticesBillSaveRequest.class);
                impItemList.addAll(saveRequest.getImpItemList());
                teusItemList.addAll(saveRequest.getTeusItemList());
            } else {
                AssertUtils.logAndThrow("redis中出库通知单参数不能为空");
            }
        } else {
            impItemList.addAll(request.getImpItemList());
            if (CollectionUtils.isNotEmpty(request.getTeusItemList())) {
                teusItemList.addAll(request.getTeusItemList());
            }
        }

        HashMap<Long, SgPhyOutNoticesImpItemSaveRequest> impItemSaveRequestHashMap = Maps.newHashMap();
        List<SgPhyOutNoticesItemSaveRequest> itemList = Lists.newArrayList();

        //录入明细不为空
        if (CollectionUtils.isNotEmpty(impItemList)) {

            for (SgPhyOutNoticesImpItemSaveRequest impItem : impItemList) {
                if (impItem.getIsTeus().equals(OcBasicConstants.IS_MATCH_SIZE_N)) {

                    //散件条码转条码明细
                    SgPhyOutNoticesItemSaveRequest itemSaveRequest = new SgPhyOutNoticesItemSaveRequest();
                    BeanUtils.copyProperties(impItem, itemSaveRequest);
                    itemList.add(itemSaveRequest);
                } else {
                    impItemSaveRequestHashMap.put(impItem.getPsCTeusId(), impItem);
                }
            }
        }

        //箱明细不为空
        if (CollectionUtils.isNotEmpty(teusItemList)) {

            //箱内明细转条码明细
            for (SgPhyOutNoticesTeusItemSaveRequest teusItemSaveRequest : teusItemList) {

                SgPhyOutNoticesItemSaveRequest itemSaveRequest = new SgPhyOutNoticesItemSaveRequest();
                SgPhyOutNoticesImpItemSaveRequest impItemSaveRequest = impItemSaveRequestHashMap.get(teusItemSaveRequest.getPsCTeusId());
                BeanUtils.copyProperties(teusItemSaveRequest, itemSaveRequest);
                itemSaveRequest.setSgBPhyOutNoticesId(teusItemSaveRequest.getSgBPhyOutNoticesId());
                itemSaveRequest.setPriceList(impItemSaveRequest.getPriceList());
                itemSaveRequest.setSourceBillItemId(impItemSaveRequest.getSourceBillItemId());
                itemList.add(itemSaveRequest);
            }
        }

        request.setImpItemList(impItemList);
        request.setTeusItemList(teusItemList);
        request.setOutNoticesItemRequests(itemList);
    }


    /**
     * 出库通知单录入明细新增
     *
     * @param objId       通知单id
     * @param impItemList 录入明细
     * @param loginUser   用户
     */
    public void insertOutNoticesImpItem(Long objId, List<SgPhyOutNoticesImpItemSaveRequest> impItemList, User loginUser) {
        SgBPhyOutNoticesImpItemMapper noticesImpItemMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesImpItemMapper.class);
        List<SgBPhyOutNoticesImpItem> outNoticesImpItemList = new ArrayList<>();
        BigDecimal zero = BigDecimal.ZERO;
        String eName = loginUser.getEname();


        //2019-08-12 通知单保存时合并相同sku的明细
        Map<Long, SgPhyOutNoticesImpItemSaveRequest> skuRequestMap = Maps.newHashMap();
        Map<Long, SgPhyOutNoticesImpItemSaveRequest> teusRequestMap = Maps.newHashMap();
        for (SgPhyOutNoticesImpItemSaveRequest noticesImpItemRequest : impItemList) {

            if (noticesImpItemRequest.getPsCTeusId() == null) {
                //散件合并相同sku的明细
                Long psCSkuId = noticesImpItemRequest.getPsCSkuId();
                if (skuRequestMap.containsKey(psCSkuId)) {
                    SgPhyOutNoticesImpItemSaveRequest orgOutNoticesImpItemSaveRequest = skuRequestMap.get(psCSkuId);
                    BigDecimal orgQty = Optional.ofNullable(orgOutNoticesImpItemSaveRequest.getQty()).orElse(BigDecimal.ZERO);
                    BigDecimal qty = Optional.ofNullable(noticesImpItemRequest.getQty()).orElse(BigDecimal.ZERO);
                    orgOutNoticesImpItemSaveRequest.setQty(qty.add(orgQty));
                } else {
                    skuRequestMap.put(psCSkuId, noticesImpItemRequest);
                }

            } else {
                //合并相同箱
                Long psCTeusId = noticesImpItemRequest.getPsCTeusId();
                if (teusRequestMap.containsKey(psCTeusId)) {
                    SgPhyOutNoticesImpItemSaveRequest orgOutNoticesImpItemSaveRequest = teusRequestMap.get(psCTeusId);
                    BigDecimal orgQty = Optional.ofNullable(orgOutNoticesImpItemSaveRequest.getQty()).orElse(BigDecimal.ZERO);
                    BigDecimal qty = Optional.ofNullable(noticesImpItemRequest.getQty()).orElse(BigDecimal.ZERO);
                    orgOutNoticesImpItemSaveRequest.setQty(qty.add(orgQty));
                } else {
                    teusRequestMap.put(psCTeusId, noticesImpItemRequest);
                }

            }

        }
        //合并后的明细
        List<SgPhyOutNoticesImpItemSaveRequest> allImpItemList = new ArrayList<>();
        List<SgPhyOutNoticesImpItemSaveRequest> mergeImpItemSkuList = new ArrayList<>(skuRequestMap.values());
        List<SgPhyOutNoticesImpItemSaveRequest> mergeImpItemTuesList = new ArrayList<>(teusRequestMap.values());
        allImpItemList.addAll(mergeImpItemSkuList);
        allImpItemList.addAll(mergeImpItemTuesList);

        allImpItemList.forEach(item -> {
            BigDecimal qty = item.getQty();          // 通知数量

            SgBPhyOutNoticesImpItem noticesImpItem = new SgBPhyOutNoticesImpItem();
            Long id = Tools.getSequence(SgConstants.SG_B_PHY_OUT_NOTICES_IMP_ITEM);

            BeanUtils.copyProperties(item, noticesImpItem);
            noticesImpItem.setId(id);
            noticesImpItem.setSgBPhyOutNoticesId(objId);
            noticesImpItem.setQtyDiff(qty);        // 差异数量（通知数量-出库数量）
            noticesImpItem.setQtyOut(zero);        // 出库数量
            noticesImpItem.setOwnerename(eName);
            noticesImpItem.setModifierename(eName);
            StorageESUtils.setBModelDefalutData(noticesImpItem, loginUser);

            outNoticesImpItemList.add(noticesImpItem);
        });

        //分批次，批量新增
        List<List<SgBPhyOutNoticesImpItem>> baseModelPageList
                = StorageESUtils.getBaseModelPageList(outNoticesImpItemList, SgOutConstants.INSERT_PAGE_SIZE);

        int result = 0;
        for (List<SgBPhyOutNoticesImpItem> pageList : baseModelPageList) {

            if (CollectionUtils.isEmpty(pageList)) {
                continue;
            }

            int count = noticesImpItemMapper.batchInsert(pageList);

            if (count != pageList.size()) {
                AssertUtils.logAndThrow(" 出库通知单录入明细批量新增失败！", loginUser.getLocale());
            }
            result += count;
        }

        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + " 出库通知单录入明细新增结果：" + result);
        }
    }

    /**
     * 出库通知单箱明细新增
     *
     * @param objId        通知单id
     * @param teusItemList 录入明细
     * @param loginUser    用户
     */
    public void insertOutNoticesTeusItem(Long objId, List<SgPhyOutNoticesTeusItemSaveRequest> teusItemList, User loginUser) {
        SgBPhyOutNoticesTeusItemMapper noticesTeusItemMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesTeusItemMapper.class);
        List<SgBPhyOutNoticesTeusItem> outNoticesTeusItemList = new ArrayList<>();
        String eName = loginUser.getEname();

        //封装出库通知单箱明细其他参数
        for (SgPhyOutNoticesTeusItemSaveRequest teusItemSaveRequest : teusItemList) {

            SgBPhyOutNoticesTeusItem sgBPhyOutNoticesTeusItem = new SgBPhyOutNoticesTeusItem();
            Long id = Tools.getSequence(SgConstants.SG_B_PHY_OUT_NOTICES_TEUS_ITEM);

            BeanUtils.copyProperties(teusItemSaveRequest, sgBPhyOutNoticesTeusItem);
            sgBPhyOutNoticesTeusItem.setId(id);
            sgBPhyOutNoticesTeusItem.setSgBPhyOutNoticesId(objId);
            sgBPhyOutNoticesTeusItem.setOwnerename(eName);
            sgBPhyOutNoticesTeusItem.setModifierename(eName);
            StorageESUtils.setBModelDefalutData(sgBPhyOutNoticesTeusItem, loginUser);
            outNoticesTeusItemList.add(sgBPhyOutNoticesTeusItem);
        }

        //分批次，批量新增
        List<List<SgBPhyOutNoticesTeusItem>> baseModelPageList
                = StorageESUtils.getBaseModelPageList(outNoticesTeusItemList, SgOutConstants.INSERT_PAGE_SIZE);

        int result = 0;
        for (List<SgBPhyOutNoticesTeusItem> pageList : baseModelPageList) {

            if (CollectionUtils.isEmpty(pageList)) {
                continue;
            }

            int count = noticesTeusItemMapper.batchInsert(pageList);

            if (count != pageList.size()) {
                AssertUtils.logAndThrow(" 出库通知单箱明细批量新增失败！", loginUser.getLocale());
            }
            result += count;
        }

        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + " 出库通知单箱明细新增结果：" + result);
        }

    }

    /**
     * 出库结果单审核修改出库通知单的录入明细
     *
     * @param user
     * @param sgBPhyOutNoticesId
     * @param outTime
     * @param resultImpItems
     */
    public void updateOutNoticesImpItemByResult(User user, Long sgBPhyOutNoticesId, Date outTime, List<SgBPhyOutResultImpItem> resultImpItems) {

        SgBPhyOutNoticesImpItemMapper noticesImpItemMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesImpItemMapper.class);

        Map<Long, SgBPhyOutResultImpItem> skuImpItems = new HashMap<>();
        Map<Long, SgBPhyOutResultImpItem> teusImpItems = new HashMap<>();

        for (SgBPhyOutResultImpItem resultImpItem : resultImpItems) {
            if (resultImpItem.getPsCTeusId() == null) {
                skuImpItems.put(resultImpItem.getPsCSkuId(), resultImpItem);
            } else {
                teusImpItems.put(resultImpItem.getPsCTeusId(), resultImpItem);
            }
        }

        Set<Long> skuIdSet = skuImpItems.keySet();
        Set<Long> teusIdSet = teusImpItems.keySet();
        List<SgBPhyOutNoticesImpItem> sgBPhyOutNoticesImpItems = noticesImpItemMapper.selectList(new QueryWrapper<SgBPhyOutNoticesImpItem>().lambda()
                .eq(SgBPhyOutNoticesImpItem::getSgBPhyOutNoticesId, sgBPhyOutNoticesId)
                .and(o -> o.in(CollectionUtils.isNotEmpty(skuIdSet), SgBPhyOutNoticesImpItem::getPsCSkuId, skuIdSet)
                        .or(CollectionUtils.isNotEmpty(teusIdSet))
                        .in(CollectionUtils.isNotEmpty(teusIdSet), SgBPhyOutNoticesImpItem::getPsCTeusId, teusIdSet)));

        for (SgBPhyOutNoticesImpItem outNoticesImpItem : sgBPhyOutNoticesImpItems) {

            SgBPhyOutResultImpItem sgBPhyOutResultImpItem;

            if (outNoticesImpItem.getPsCTeusId() == null) {
                sgBPhyOutResultImpItem = skuImpItems.get(outNoticesImpItem.getPsCSkuId());
            } else {
                sgBPhyOutResultImpItem = teusImpItems.get(outNoticesImpItem.getPsCTeusId());
            }


            if (sgBPhyOutResultImpItem != null) {
                SgBPhyOutNoticesImpItem sgBPhyOutNoticesImpItem = new SgBPhyOutNoticesImpItem();

                if (sgBPhyOutResultImpItem.getQtyOut().compareTo(outNoticesImpItem.getQty()) > 0) {
                    AssertUtils.logAndThrow("出库结果单的录入明细数量出库大于出库通知单录入明细的通知数量");
                }

                sgBPhyOutNoticesImpItem.setId(outNoticesImpItem.getId());
                sgBPhyOutNoticesImpItem.setQtyOut(outNoticesImpItem.getQtyOut().add(sgBPhyOutResultImpItem.getQtyOut())); // 出库数量
                sgBPhyOutNoticesImpItem.setQtyDiff(outNoticesImpItem.getQty().subtract(sgBPhyOutNoticesImpItem.getQtyOut())); // 差异数量
                sgBPhyOutNoticesImpItem.setModifierename(user.getEname());
                noticesImpItemMapper.updateById(sgBPhyOutNoticesImpItem);
            }
        }
    }

    public void queryTeusPhyStorage(User user, SgBPhyOutResult outResult, List<SgBPhyOutResultImpItem> resultItems, String sysParam) {
        SgTeusPhyStorageQueryCmd sgTeusPhyStorageQueryCmd = (SgTeusPhyStorageQueryCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(),
                SgTeusPhyStorageQueryCmd.class.getName(),
                SgConstantsIF.GROUP, SgConstantsIF.VERSION);

        List<Long> wareHouseIds = new ArrayList<>();
        wareHouseIds.add(outResult.getCpCPhyWarehouseId());

        List<SgBPhyTeusStorage> storageList = new ArrayList<>();
        List<SgBPhyOutResultImpItem> teusImpItemList = resultItems.stream().filter(x -> x.getPsCTeusEcode() != null).collect(Collectors.toList());

        if (CollectionUtils.isNotEmpty(teusImpItemList)) {
            List<String> teusEcodeList = teusImpItemList.stream().map(SgBPhyOutResultImpItem::getPsCTeusEcode).collect(Collectors.toList());

            SgTeusPhyStorageQueryRequest request = new SgTeusPhyStorageQueryRequest();
            request.setPhyWarehouseIds(wareHouseIds);

            int itemNum = teusEcodeList.size();
            int pageSize = SgConstants.SG_COMMON_INSERT_PAGE_SIZE;
            int page = itemNum / pageSize;
            if (itemNum % pageSize != 0) page++;
            for (int i = 0; i < page; i++) {
                int startIndex = i * pageSize;
                int endIndex = (i + 1) * pageSize;
                List<String> teusCodes = teusEcodeList.subList(startIndex, endIndex < itemNum ? endIndex : itemNum);
                request.setTeusEcodes(teusCodes);
                ValueHolderV14<List<SgBPhyTeusStorage>> v14 = sgTeusPhyStorageQueryCmd.queryTeusPhyStorage(request, user);
                AssertUtils.notNull(v14, "箱库存查询未知错误！", user.getLocale());
                if (v14.getCode() == ResultCode.FAIL) {
                    AssertUtils.logAndThrow("箱库存查询异常：" + v14.getMessage(), user.getLocale());
                }
                storageList.addAll(v14.getData());
            }

            HashMap<String, BigDecimal> storageTeusMap = new HashMap<>();
            storageList.forEach(storage ->
                    storageTeusMap.put(storage.getPsCTeusEcode(), storage.getQtyStorage())
            );

            /**
             *  1.库存查询不存在的teusEcode默认数量为0；
             *  2.判断出库数量是否大于箱库存；
             *  3.系统参数是否允许负库存
             */
            JSONArray errorSku = new JSONArray();
            teusImpItemList.forEach(teusImpItem -> {
                BigDecimal qtyOut = teusImpItem.getQtyOut();
                String teusEcode = teusImpItem.getPsCTeusEcode();
                BigDecimal qtyStorage = storageTeusMap.get(teusEcode);
                if (qtyStorage == null) {
                    qtyStorage = BigDecimal.ZERO;
                }

                // 如果箱库存小于出库数量，则判断是否允许负库存
                if (qtyOut.compareTo(qtyStorage) > 0) {
                    if (StringUtils.isEmpty(sysParam) || !Boolean.valueOf(sysParam)) {
                        JSONObject skuInfo = new JSONObject();
                        skuInfo.put("teusEcode", teusEcode);
                        skuInfo.put("qty_out", qtyOut);
                        skuInfo.put("qty_storage", qtyStorage);
                        errorSku.add(skuInfo);
                    }
                }
            });

            if (errorSku.size() > 0) {
                String errorMsg = "实体仓箱库存不允许为负:" + errorSku.toString();
                log.error(this.getClass().getName() + ",queryPhysicalStorage:" + errorMsg);
                AssertUtils.logAndThrow(errorMsg, user.getLocale());
            }
        }

    }
}
