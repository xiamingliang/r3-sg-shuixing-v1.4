package com.jackrain.nea.sg.in.filter;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Lists;
import com.jackrain.nea.sg.in.common.SgInConstants;
import com.jackrain.nea.sg.in.mapper.SgBPhyInNoticesImpItemMapper;
import com.jackrain.nea.sg.in.mapper.SgBPhyInNoticesMapper;
import com.jackrain.nea.sg.in.model.request.SgBPhyInResultSaveRequest;
import com.jackrain.nea.sg.in.model.request.SgPhyInResultBillSaveRequest;
import com.jackrain.nea.sg.in.model.request.SgPhyInResultImpItemSaveRequest;
import com.jackrain.nea.sg.in.model.table.SgBPhyInNotices;
import com.jackrain.nea.sg.in.model.table.SgBPhyInNoticesImpItem;
import com.jackrain.nea.sg.in.services.SgPhyInResultSaveAndAuditService;
import com.jackrain.nea.sg.in.services.SgPhyInResultSaveService;
import com.jackrain.nea.tableService.MainTableRecord;
import com.jackrain.nea.tableService.TableServiceContext;
import com.jackrain.nea.tableService.filter.BaseFilter;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zhu lin yu
 * @since 2019/12/11
 * create at : 2019/12/11 2:32 下午
 */
@Slf4j
@Component
public class SgPhyInNoticesPosAuditFilter extends BaseFilter {

    @Override
    public void before(TableServiceContext context, MainTableRecord row) {
        Long objId = row.getId();
        User user = context.getUser();

        SgBPhyInNoticesMapper mapper = ApplicationContextHandle.getBean(SgBPhyInNoticesMapper.class);
        SgBPhyInNoticesImpItemMapper impItemMapper = ApplicationContextHandle.getBean(SgBPhyInNoticesImpItemMapper.class);
        SgBPhyInNotices sgBPhyInNotices = mapper.selectById(objId);
        List<SgBPhyInNoticesImpItem> sgBPhyInNoticesImpItems = impItemMapper.selectList(new QueryWrapper<SgBPhyInNoticesImpItem>().lambda()
                .eq(SgBPhyInNoticesImpItem::getSgBPhyInNoticesId, objId));


        SgPhyInResultSaveAndAuditService saveAndAuditService = ApplicationContextHandle.getBean(SgPhyInResultSaveAndAuditService.class);
        SgPhyInResultBillSaveRequest request = new SgPhyInResultBillSaveRequest();
        SgBPhyInResultSaveRequest inResultRequest = new SgBPhyInResultSaveRequest();
        List<SgPhyInResultImpItemSaveRequest> impItemList = new ArrayList<>();

        //主表信息
        BeanUtils.copyProperties(sgBPhyInNotices,inResultRequest);
        inResultRequest.setSgBPhyInNoticesId(sgBPhyInNotices.getId());
        inResultRequest.setInType(SgInConstants.IN_TYPE_BIG_CARGO);
        inResultRequest.setIsLast(SgInConstants.IN_IS_LAST_Y);
        inResultRequest.setId(-1L);
        request.setSgBPhyInResultSaveRequest(inResultRequest);

        //录入明细
        for (SgBPhyInNoticesImpItem impItem : sgBPhyInNoticesImpItems) {
            SgPhyInResultImpItemSaveRequest impRequest = new SgPhyInResultImpItemSaveRequest();
            BeanUtils.copyProperties(impItem,impRequest);
            impRequest.setQtyIn(impItem.getQtyScan());
            impItemList.add(impRequest);
        }
        request.setImpItemList(impItemList);
        request.setObjId(-1L);
        request.setLoginUser(user);
        saveAndAuditService.saveAndAuditBill(Lists.newArrayList(request));
    }
}
