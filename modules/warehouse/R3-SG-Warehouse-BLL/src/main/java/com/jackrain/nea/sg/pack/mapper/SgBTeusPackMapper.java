package com.jackrain.nea.sg.pack.mapper;

import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.pack.model.table.SgBTeusPack;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SgBTeusPackMapper extends ExtentionMapper<SgBTeusPack> {
}