package com.jackrain.nea.sg.pack;

import com.jackrain.nea.sg.pack.common.SgTeusPackConstants;
import com.jackrain.nea.sg.pack.filter.SgTeusPackAuditFilter;
import com.jackrain.nea.sg.pack.filter.SgTeusPackDelFilter;
import com.jackrain.nea.sg.pack.filter.SgTeusPackSaveFilter;
import com.jackrain.nea.sg.pack.validate.SgTeusPackAuditValidate;
import com.jackrain.nea.sg.pack.validate.SgTeusPackDelValidate;
import com.jackrain.nea.sg.pack.validate.SgTeusPackSaveValidate;
import com.jackrain.nea.sg.pack.validate.SgTeusPackVoidValidate;
import com.jackrain.nea.tableService.Feature;
import com.jackrain.nea.tableService.constants.Constants;
import com.jackrain.nea.tableService.feature.FeatureAnnotation;
import com.jackrain.nea.util.ApplicationContextHandle;

/**
 * @author zhoulinsheng
 * @since 2019/11/27 14:02
 */
@FeatureAnnotation(value = "SgTeusPackFeature", description = "装箱Feature", isSystem = true)
public class SgTeusPackFeature extends Feature {

    @Override
    protected void initialization() {



        // 装箱单保存
        SgTeusPackSaveValidate sgPackSaveValidate = ApplicationContextHandle.getBean(SgTeusPackSaveValidate.class);
        addValidator(sgPackSaveValidate, (tableName, actionName) -> tableName.equalsIgnoreCase(SgTeusPackConstants.SG_B_TEUS_PACK)
                && (actionName.equals(Constants.ACTION_SAVE)|| actionName.equals(Constants.ACTION_ADD)));

        // 删除
        SgTeusPackDelValidate sgPackDelValidate = ApplicationContextHandle.getBean(SgTeusPackDelValidate.class);
        addValidator(sgPackDelValidate, (tableName, actionName) -> tableName.equalsIgnoreCase(SgTeusPackConstants.SG_B_TEUS_PACK)
                && (actionName.equals(Constants.ACTION_DELETE)));

        // 作废
        SgTeusPackVoidValidate sgPackVoidValidate = ApplicationContextHandle.getBean(SgTeusPackVoidValidate.class);
        addValidator(sgPackVoidValidate, (tableName, actionName) -> tableName.equalsIgnoreCase(SgTeusPackConstants.SG_B_TEUS_PACK)
                && (actionName.equals(Constants.ACTION_VOID)));

        // 审核
        SgTeusPackAuditValidate sgPackAuditValidate = ApplicationContextHandle.getBean(SgTeusPackAuditValidate.class);
        addValidator(sgPackAuditValidate, (tableName, actionName) -> tableName.equalsIgnoreCase(SgTeusPackConstants.SG_B_TEUS_PACK)
                && (actionName.equals(Constants.ACTION_SUBMIT)));


        SgTeusPackSaveFilter saveFilter = ApplicationContextHandle.getBean(SgTeusPackSaveFilter.class);
        addFilter(saveFilter, (tableName, actionName) -> tableName.equalsIgnoreCase(SgTeusPackConstants.SG_B_TEUS_PACK)
                && (actionName.equals(Constants.ACTION_SAVE) || actionName.equals(Constants.ACTION_ADD)));

        SgTeusPackDelFilter sgPackDelFilter = ApplicationContextHandle.getBean(SgTeusPackDelFilter.class);
        addFilter(sgPackDelFilter, (tableName, actionName) -> tableName.equalsIgnoreCase(SgTeusPackConstants.SG_B_TEUS_PACK)
                && (actionName.equals(Constants.ACTION_DELETE)));

        SgTeusPackAuditFilter sgPackAuditFilter = ApplicationContextHandle.getBean(SgTeusPackAuditFilter.class);
        addFilter(sgPackAuditFilter, (tableName, actionName) -> tableName.equalsIgnoreCase(SgTeusPackConstants.SG_B_TEUS_PACK)
                && (actionName.equals(Constants.ACTION_SUBMIT)));



    }
}
