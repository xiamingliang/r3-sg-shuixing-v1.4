package com.jackrain.nea.sg.out.common.enums;

/**
 * @author leexh
 * @since 2019/6/16 18:29
 * desc: 奇门订单平台来源编码枚举
 */
public enum SourcePlatformCode {

    QM_SOURCE_PLATFORM_CODE_OTHER(-1, "OTHER"),
    QM_SOURCE_PLATFORM_CODE_TB(2, "TB"),
    QM_SOURCE_PLATFORM_CODE_JD(4, "JD"),
    QM_SOURCE_PLATFORM_CODE_PP(5, "PP"),
    QM_SOURCE_PLATFORM_CODE_QQ(6, "QQ"),
    QM_SOURCE_PLATFORM_CODE_DD(7, "DD"),
    QM_SOURCE_PLATFORM_CODE_YHD(8, "YHD"),
    QM_SOURCE_PLATFORM_CODE_AMAZON(10, "AMAZON"),
    QM_SOURCE_PLATFORM_CODE_JM(13, "JM"),
    QM_SOURCE_PLATFORM_CODE_YT(18, "YT"),
    QM_SOURCE_PLATFORM_CODE_MGJ(25, "MGJ"),
    QM_SOURCE_PLATFORM_CODE_WPH(50, "WPH"),
    QM_SOURCE_PLATFORM_CODE_SN(40, "SN");

    private String desc;
    private Integer value;

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    SourcePlatformCode(Integer value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public static SourcePlatformCode valueOf(Integer value) {
        switch (value) {
            case 2:
                return QM_SOURCE_PLATFORM_CODE_TB;
            case 4:
                return QM_SOURCE_PLATFORM_CODE_JD;
            case 5:
                return QM_SOURCE_PLATFORM_CODE_PP;
            case 6:
                return QM_SOURCE_PLATFORM_CODE_QQ;
            case 7:
                return QM_SOURCE_PLATFORM_CODE_DD;
            case 8:
                return QM_SOURCE_PLATFORM_CODE_YHD;
            case 10:
                return QM_SOURCE_PLATFORM_CODE_AMAZON;
            case 13:
                return QM_SOURCE_PLATFORM_CODE_JM;
            case 18:
                return QM_SOURCE_PLATFORM_CODE_YT;
            case 25:
                return QM_SOURCE_PLATFORM_CODE_MGJ;
            case 40:
                return QM_SOURCE_PLATFORM_CODE_SN;
            case 50:
                return QM_SOURCE_PLATFORM_CODE_WPH;
            default:
                return QM_SOURCE_PLATFORM_CODE_OTHER;
        }
    }
}
