package com.jackrain.nea.sg.out.filter;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.out.common.SgOutConstants;
import com.jackrain.nea.sg.out.mapper.SgBPhyOutNoticesItemMapper;
import com.jackrain.nea.sg.out.mapper.SgBPhyOutNoticesMapper;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutNotices;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutNoticesItem;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutResult;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutResultItem;
import com.jackrain.nea.tableService.DbRowAction;
import com.jackrain.nea.tableService.MainTableRecord;
import com.jackrain.nea.tableService.TableServiceContext;
import com.jackrain.nea.tableService.filter.BaseFilter;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;

/**
 * @author leexh
 * @since 2019/5/27 18:04
 * desc: 出库结果单保存
 */
@Slf4j
@Component
public class SgOutResultSaveFilter extends BaseFilter {

    @Override
    public void before(TableServiceContext context, MainTableRecord row) {


        if (row.getAction().equals(DbRowAction.INSERT)) {
            JSONObject commitData = row.getCommitData();
            Long noticesId = commitData.getLong("SG_B_PHY_OUT_NOTICES_ID");
            AssertUtils.notNull(noticesId, "关联出库通知单ID不能为空！", context.getLocale());

            // 复制出库通知单主表信息到结果单
            SgBPhyOutNoticesMapper noticesMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesMapper.class);
            SgBPhyOutResult outResult = new SgBPhyOutResult();
            SgBPhyOutNotices outNotices = noticesMapper.selectById(noticesId);
            AssertUtils.notNull(outNotices, "关联的出库通知单不存在！", context.getLocale());
            BeanUtils.copyProperties(outNotices, outResult);
            JSONObject outResultObj = JSON.parseObject(JSONObject.toJSONString(outResult));
            row.getCommitData().putAll(outResultObj);
            keyStampToDate(row.getCommitData());

            // 复制出库通知单明细信息到结果单
            SgBPhyOutNoticesItemMapper noticesItemMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesItemMapper.class);
            List<SgBPhyOutNoticesItem> noticesItems = noticesItemMapper.selectList(
                    new QueryWrapper<SgBPhyOutNoticesItem>()
                            .eq(SgOutConstants.OUT_NOTICES_PAREN_FIELD, noticesId));

            if (!CollectionUtils.isEmpty(noticesItems)) {


                noticesItems.forEach(item -> {
                    SgBPhyOutResultItem outResultItem = new SgBPhyOutResultItem();
                    BeanUtils.copyProperties(item, outResultItem);
                });
            }
        }
    }

    /**
     * JSONObject key转大写
     *
     * @param obj 对象
     * @return 对象
     */
    public JSONObject objKeyToUpperCase(JSONObject obj) {
        JSONObject jsonObject = new JSONObject();
        if (obj == null) {
            return obj;
        }

        Set<String> strings = obj.keySet();
        strings.forEach(key -> {
            String keyUp = key.toUpperCase();
            jsonObject.put(keyUp, obj.get(key));
        });
        return jsonObject;
    }

    /**
     * CommitData 时间类型字段 时间戳转date
     *
     * @param obj json对象
     */
    public void keyStampToDate(JSONObject obj) {
        if (obj != null) {
            for (String key : obj.keySet()) {
                if (key.contains("DATE") || key.contains("TIME")) {
                    String value = obj.getString(key);
                    obj.put(key, DateUtil.stringToDate(value));
                }
            }
        }
    }
}
