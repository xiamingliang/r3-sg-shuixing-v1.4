package com.jackrain.nea.sg.phyadjust.mapper;

import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.phyadjust.model.table.SgBPhyAdjustItem;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

@Mapper
public interface SgBPhyAdjustItemMapper extends ExtentionMapper<SgBPhyAdjustItem> {


    /**
     * 查询明细 数量的汇总
     *
     * @param adjId 主表id
     * @return qty汇总
     */
    @Select("select sum(qty) from sg_b_phy_adjust_item where sg_b_phy_adjust_id = #{adjId} ")
    BigDecimal selectSumQtyByAdjId(@Param("adjId") Long adjId);


    /**
     * 查询 明细矩阵
     *
     * @param id 主表id
     * @return 矩阵信息
     */
    @Select("SELECT   MAX (PS_C_PRO_ID) AS PS_C_PRO_ID, " +
            "   MAX (PS_C_PRO_ECODE) AS PS_C_PRO_ECODE, " +
            "   MAX (PS_C_PRO_ENAME) AS PS_C_PRO_ENAME, " +
            "   AVG (PRICE_LIST) AS PRICE_LIST, " +
            "   COALESCE (SUM(QTY), 0) AS QTY, " +
            "   COALESCE (SUM(amt_list), 0) AS AMT_LIST" +
            " FROM sg_b_phy_adjust_item WHERE sg_b_phy_adjust_id = #{id} GROUP BY ps_c_pro_ecode")
    List<HashMap> selectByCondition(Long id);

    /**
     * 分页查询库存调整单明细
     */
    @Select("SELECT * FROM " + SgConstants.SG_B_PHY_ADJUST_ITEM+ " ORDER BY ID LIMIT #{page} OFFSET #{offset}")
    List<SgBPhyAdjustItem> selectListByPage(@Param("page") int page, @Param("offset") int offset);

}