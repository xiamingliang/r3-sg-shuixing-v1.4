package com.jackrain.nea.sg.out.services;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Maps;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.config.SgStorageBoxConfig;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.out.common.SgOutConstantsIF;
import com.jackrain.nea.sg.out.mapper.*;
import com.jackrain.nea.sg.out.model.request.*;
import com.jackrain.nea.sg.out.model.table.*;
import com.jackrain.nea.sg.transfer.common.SgTransferConstantsIF;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.*;

/**
 * @author zhu lin yu
 * @since 2019-10-29
 * create at : 2019-10-29 14:15
 */

@Slf4j
@Component
public class SgPhyOutCommitForPDAService {

    @Autowired
    private SgStorageBoxConfig storageBoxConfig;

    /**
     * @return
     */
    public ValueHolderV14 saveAndAuditOutResult(SgPhyOutCommitForPDARequest request) {
        SgPhyOutResultBillSaveRequest sgPhyOutResultBillSaveRequest = new SgPhyOutResultBillSaveRequest();
        SgBPhyOutResultMapper resultMapper = ApplicationContextHandle.getBean(SgBPhyOutResultMapper.class);
        SgBPhyOutNoticesMapper noticesMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesMapper.class);
        List<Long> teusId = new ArrayList<>();
        List<String> skuEcodeList = new ArrayList<>();

        //参数校验
        AssertUtils.isTrue(request != null, "参数不能为空！");
        AssertUtils.isTrue(CollectionUtils.isNotEmpty(request.getDetailRequests()), "参数不能为空！");

        User loginUser = request.getLoginUser();
        AssertUtils.notNull(loginUser, "用户信息不能为空！");

        Long sgBPhyOutNoticesId = request.getSgBPhyOutNoticesId();
        AssertUtils.isTrue(sgBPhyOutNoticesId != null, "关联的出库通知单ID不能为空！", loginUser.getLocale());

        SgBPhyOutNotices outNotices = noticesMapper.selectOne(new QueryWrapper<SgBPhyOutNotices>().lambda()
                .eq(SgBPhyOutNotices::getId, sgBPhyOutNoticesId)
                .eq(SgBPhyOutNotices::getIsactive, SgConstants.IS_ACTIVE_Y));
        AssertUtils.notNull(outNotices, "关联的出库通知单不存在！", loginUser.getLocale());
        Integer billStatus = outNotices.getBillStatus();

        if (billStatus != null && billStatus != SgOutConstantsIF.OUT_NOTICES_STATUS_WAIT && billStatus != SgOutConstantsIF.OUT_NOTICES_STATUS_PART) {
            AssertUtils.logAndThrow("通知单已全部出库,不允许新增结果单!", loginUser.getLocale());
        }

        List<SgBPhyOutResult> outResults = resultMapper.selectList(new QueryWrapper<SgBPhyOutResult>().lambda()
                .eq(SgBPhyOutResult::getSgBPhyOutNoticesId, sgBPhyOutNoticesId)
                .eq(SgBPhyOutResult::getIsLast, SgOutConstantsIF.OUT_IS_LAST_Y)
                .eq(SgBPhyOutResult::getIsactive, SgConstants.IS_ACTIVE_Y));
        AssertUtils.isTrue(CollectionUtils.isEmpty(outResults), "已存在此通知单关联的最后一次出库的出库结果单，不允许保存!", loginUser.getLocale());

        //拆分是散码还是箱,并获取数量
        HashMap<Long, BigDecimal> teusQtyMap = new HashMap<>();
        HashMap<String, BigDecimal> skuQtyMap = new HashMap<>();

        List<SgPhyOutCommitForPDADetailRequest> requests = request.getDetailRequests();

        for (SgPhyOutCommitForPDADetailRequest detailRequest : requests) {
            if (SgTransferConstantsIF.IS_TEUS_Y == detailRequest.getIsTeus()) {
                teusId.add(detailRequest.getPsCTeusId());
                teusQtyMap.put(detailRequest.getPsCTeusId(), detailRequest.getQty());
            } else {
                skuEcodeList.add(detailRequest.getSkuEcode());
                skuQtyMap.put(detailRequest.getSkuEcode(), detailRequest.getQty());
            }
        }

        /**复制出库通知单主表信息到出库结果单*/
        SgPhyOutResultSaveRequest outResultRequest = new SgPhyOutResultSaveRequest();
        BeanUtils.copyProperties(outNotices, outResultRequest);
        outResultRequest.setSgBPhyOutNoticesId(outNotices.getId());
        outResultRequest.setSgBPhyOutNoticesBillno(outNotices.getBillNo());
        outResultRequest.setOutId(loginUser.getId().longValue());
        outResultRequest.setIsLast(request.getIsLast());
        outResultRequest.setOutName(loginUser.getName());
        outResultRequest.setOutEname(loginUser.getEname());
        outResultRequest.setOutTime(new Timestamp(System.currentTimeMillis()));
//        outResultRequest.setTotQtyOut();
        sgPhyOutResultBillSaveRequest.setOutResultRequest(outResultRequest);


        /**复制出库通知单录入明细信息到出库结果单*/
        HashMap<Long, BigDecimal> priceListMap = new HashMap<>();
        HashMap<Long, BigDecimal> origTeusQtyMap = new HashMap<>();
        SgBPhyOutNoticesImpItemMapper impItemMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesImpItemMapper.class);
        List<SgBPhyOutNoticesImpItem> impItems = impItemMapper.selectList(
                new QueryWrapper<SgBPhyOutNoticesImpItem>().lambda()
                        .eq(SgBPhyOutNoticesImpItem::getSgBPhyOutNoticesId, sgBPhyOutNoticesId)
                        .and(x -> x.in(SgBPhyOutNoticesImpItem::getPsCTeusId, teusId).or().in(SgBPhyOutNoticesImpItem::getPsCSkuEcode, skuEcodeList)));
        AssertUtils.isTrue(CollectionUtils.isNotEmpty(impItems), "关联的出库通知单录入明细为空，不允许新增出库结果单！", loginUser.getLocale());

        List<SgPhyOutResultImpItemSaveRequest> outResultImpItemRequests = new ArrayList<>();
        impItems.forEach(item -> {
            SgPhyOutResultImpItemSaveRequest resultItem = new SgPhyOutResultImpItemSaveRequest();
            BeanUtils.copyProperties(item, resultItem);
            if (item.getPsCTeusId() != null) {
                priceListMap.put(item.getPsCTeusId(), item.getPriceList());
                origTeusQtyMap.put(item.getPsCTeusId(), item.getQty());

                //录入明细的箱数量
                resultItem.setQtyOut(teusQtyMap.get(item.getPsCTeusId()));
            } else {
                //录入明细的散码数量
                resultItem.setQtyOut(skuQtyMap.get(item.getPsCSkuEcode()));
            }
            outResultImpItemRequests.add(resultItem);
        });
        sgPhyOutResultBillSaveRequest.setImpItemList(outResultImpItemRequests);

        /**复制出库通知单箱明细信息到出库结果单*/
        List<SgPhyOutResultItemSaveRequest> outResultItemRequests = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(teusId)) {
            SgBPhyOutNoticesTeusItemMapper teusItemMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesTeusItemMapper.class);
            List<SgBPhyOutNoticesTeusItem> teusItems = teusItemMapper.selectList(
                    new QueryWrapper<SgBPhyOutNoticesTeusItem>().lambda()
                            .eq(SgBPhyOutNoticesTeusItem::getSgBPhyOutNoticesId, sgBPhyOutNoticesId)
                            .in(SgBPhyOutNoticesTeusItem::getPsCTeusId, teusId));

            List<SgPhyOutResultTeusItemSaveRequest> outResultTeusItemRequests = new ArrayList<>();
            teusItems.forEach(item -> {
                //出库通知单箱明细转出库结果单箱明细
                SgPhyOutResultTeusItemSaveRequest resultItem = new SgPhyOutResultTeusItemSaveRequest();
                BeanUtils.copyProperties(item, resultItem);

                //通过箱内(条码数量/箱数) x 扫描录入的箱数 = 扫描录入的箱内条码数
                BigDecimal skuQty = item.getQty().divide(origTeusQtyMap.get(item.getPsCTeusId()),BigDecimal.ROUND_HALF_UP)
                        .multiply(teusQtyMap.get(item.getPsCTeusId()));
                resultItem.setQty(skuQty);
                outResultTeusItemRequests.add(resultItem);

                //出库通知单箱明细转出库结果单条码明细
                SgPhyOutResultItemSaveRequest itemSaveRequest = new SgPhyOutResultItemSaveRequest();
                BeanUtils.copyProperties(item, itemSaveRequest);
                itemSaveRequest.setPriceList(priceListMap.get(item.getPsCTeusId()));
                outResultItemRequests.add(itemSaveRequest);
            });

            sgPhyOutResultBillSaveRequest.setTeusItemList(outResultTeusItemRequests);
        }


        /** 复制出库通知单明细信息到出库结果单*/
        SgBPhyOutNoticesItemMapper noticesItemMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesItemMapper.class);
        List<SgBPhyOutNoticesItem> noticesItems = noticesItemMapper.selectList(
                new QueryWrapper<SgBPhyOutNoticesItem>().lambda()
                        .eq(SgBPhyOutNoticesItem::getSgBPhyOutNoticesId, sgBPhyOutNoticesId)
                        .in(SgBPhyOutNoticesItem::getPsCSkuEcode, skuEcodeList));
        AssertUtils.isTrue(CollectionUtils.isNotEmpty(noticesItems), "关联的出库通知单明细为空，不允许新增出库结果单！", loginUser.getLocale());

        noticesItems.forEach(item -> {
            SgPhyOutResultItemSaveRequest resultItem = new SgPhyOutResultItemSaveRequest();
            BeanUtils.copyProperties(item, resultItem);
            resultItem.setSgBPhyOutNoticesItemId(item.getId());
            resultItem.setQty(item.getQtyDiff());
            resultItem.setAmtListOut(item.getPriceList().multiply(item.getQtyDiff()));
            outResultItemRequests.add(resultItem);
        });


        //合并相同sku的条码明细
        Map<Long, SgPhyOutResultItemSaveRequest> skuRequestMap = Maps.newHashMap();
        for (SgPhyOutResultItemSaveRequest itemSaveRequest : outResultItemRequests) {

            Long psCSkuId = itemSaveRequest.getPsCSkuId();
            if (skuRequestMap.containsKey(psCSkuId)) {
                SgPhyOutResultItemSaveRequest orgItemSaveRequest = skuRequestMap.get(psCSkuId);
                BigDecimal orgQty = Optional.ofNullable(orgItemSaveRequest.getQty()).orElse(BigDecimal.ZERO);
                BigDecimal qty = Optional.ofNullable(itemSaveRequest.getQty()).orElse(BigDecimal.ZERO);
                orgItemSaveRequest.setQty(qty.add(orgQty));
            } else {
                skuRequestMap.put(psCSkuId, itemSaveRequest);
            }

        }

        //调用新增并审核出库结果单服务
        sgPhyOutResultBillSaveRequest.setOutResultItemRequests(new ArrayList<>(skuRequestMap.values()));
        sgPhyOutResultBillSaveRequest.setLoginUser(loginUser);
        sgPhyOutResultBillSaveRequest.setObjId(-1L);
        SgPhyOutResultSaveAndAuditService saveAndAuditService = ApplicationContextHandle.getBean(SgPhyOutResultSaveAndAuditService.class);
        return saveAndAuditService.saveOutResultAndAudit(sgPhyOutResultBillSaveRequest);
    }
}