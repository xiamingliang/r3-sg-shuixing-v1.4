package com.jackrain.nea.sg.unpack.validate;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.sg.unpack.common.SgUnpackConstants;
import com.jackrain.nea.tableService.MainTableRecord;
import com.jackrain.nea.tableService.TableServiceContext;
import com.jackrain.nea.tableService.validate.BaseValidator;
import com.jackrain.nea.utils.AssertUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author zhoulinsheng
 * @since 2019/12/3 11:13
 * desc: 拆箱单作废
 */
@Slf4j
@Component
public class SgUnpackVoidValidate extends BaseValidator {

    @Override
    public void validate(TableServiceContext context, MainTableRecord currentRow) {

        // 获取单据状态
        Integer status = currentRow.getOrignalData().getInteger("STATUS");

        AssertUtils.cannot(status == SgUnpackConstants.BILL_STATUS_AUDIT, "当前记录已审核，不允许作废",
                context.getLocale());
        AssertUtils.cannot(status == SgUnpackConstants.BILL_STATUS_VOID, "当前记录已作废，不允许重复作废",
                context.getLocale());

        JSONObject commitData = currentRow.getCommitData();
        commitData.put("STATUS", SgUnpackConstants.BILL_STATUS_VOID);
    }
}
