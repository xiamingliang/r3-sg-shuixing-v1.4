package com.jackrain.nea.sg.unpack.filter;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.data.basic.services.BasicPsQueryService;
import com.jackrain.nea.psext.model.table.PsCTeus;
import com.jackrain.nea.sg.unpack.common.SgUnpackConstants;
import com.jackrain.nea.sg.unpack.mapper.SgBTeusUnpackItemMapper;
import com.jackrain.nea.sg.unpack.mapper.SgBTeusUnpackMapper;
import com.jackrain.nea.sg.unpack.model.table.SgBTeusUnpack;
import com.jackrain.nea.sg.unpack.model.table.SgBTeusUnpackItem;
import com.jackrain.nea.tableService.DbRowAction;
import com.jackrain.nea.tableService.MainTableRecord;
import com.jackrain.nea.tableService.RowRecord;
import com.jackrain.nea.tableService.SubTableRecord;
import com.jackrain.nea.tableService.TableServiceContext;
import com.jackrain.nea.tableService.filter.BaseFilter;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.utils.AssertUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author zhoulisnheng
 * @since 2019/11/27 18:04
 * desc: 拆箱单保存
 */
@Slf4j
@Component
public class SgUnpackSaveFilter extends BaseFilter {

    @Override
    public void before(TableServiceContext context, MainTableRecord row) {

        // 初始化主单据字段
        if (DbRowAction.INSERT.equals(row.getAction())) {
            JSONObject commitData = row.getCommitData();
            commitData.put("STATUS", SgUnpackConstants.BILL_STATUS_NOT_AUDIT);
            commitData.put("QTY_TOT_UNPACK", BigDecimal.ZERO);
            commitData.put("QTY_TOT_TEUS", BigDecimal.ZERO);
            commitData.put("WMS_STATUS", SgUnpackConstants.WMS_STATUS_WAIT_PASS);
            commitData.put("WMS_FAIL_COUNT", SgUnpackConstants.COUNT_ZERO);
        }

        // 明细
        String impItemName = SgUnpackConstants.SG_B_TEUS_UNPACK_ITEM.toUpperCase();
        Map<String, SubTableRecord> subTables = row.getSubTables();
        if (MapUtils.isNotEmpty(subTables) && subTables.get(impItemName) != null) {
            Collection<RowRecord> rowRecords = subTables.get(impItemName).getRows();
            if (CollectionUtils.isNotEmpty(rowRecords)) {
                for (RowRecord record : rowRecords) {
                    JSONObject commitData = record.getCommitData();
                    List<Long> teusIds = new ArrayList<>();
                    Long teusId = commitData.getLong("PS_C_TEUS_ID");
                    teusIds.add(teusId);
                    if (DbRowAction.INSERT.equals(record.getAction())) {
                        BasicPsQueryService queryService = ApplicationContextHandle.getBean(BasicPsQueryService.class);
                        HashMap<Long, PsCTeus> teusHashMap = queryService.queryTeusInfoById(teusIds);
                        PsCTeus psCTeus = teusHashMap.get(teusId);
                        AssertUtils.notNull(psCTeus, "箱[" + teusId + "]不存在", context.getLocale());
                        // 补充信息
                        suppTeusInfo(commitData, psCTeus);

                    }
                }

            }
        }

    }

    private void suppTeusInfo(JSONObject commitData, PsCTeus psCTeus) {

        // 补充条码信息
        commitData.put("PS_C_PRO_ID", psCTeus.getPsCProId());
        commitData.put("PS_C_PRO_ECODE", psCTeus.getPsCProEcode());
        commitData.put("PS_C_PRO_ENAME", psCTeus.getPsCProEname());
        commitData.put("PS_C_SPEC1_ID", psCTeus.getPsCSpec1Id());
        commitData.put("PS_C_SPEC1_ECODE", psCTeus.getPsCSpec1Ecode());
        commitData.put("PS_C_SPEC1_ENAME", psCTeus.getPsCSpec1Ename());
        commitData.put("PS_C_MATCHSIZE_ID", psCTeus.getPsCMatchsizeId());
        commitData.put("PS_C_MATCHSIZE_ECODE", psCTeus.getPsCMatchsizeEcode());
        commitData.put("PS_C_MATCHSIZE_ENAME", psCTeus.getPsCMatchsizeEname());
        commitData.put("PS_C_TEUS_ECODE", psCTeus.getEcode());
    }

    @Override
    public void after(TableServiceContext context, MainTableRecord row) {
        Long objId = row.getId();
        SgBTeusUnpackItemMapper unpackItemMapper = ApplicationContextHandle.getBean(SgBTeusUnpackItemMapper.class);
        SgBTeusUnpackMapper unpackMapper = ApplicationContextHandle.getBean(SgBTeusUnpackMapper.class);

        List<SgBTeusUnpackItem> itemList = unpackItemMapper.selectList(new QueryWrapper<SgBTeusUnpackItem>().lambda()
                .eq(SgBTeusUnpackItem::getSgBTeusUnpackId, objId));
        // 计算总拆箱数
        BigDecimal countTeus = itemList.stream()
                .map(o -> o.getQty() == null ? BigDecimal.ZERO : o.getQty())
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        SgBTeusUnpack sgBTeusUnpack = new SgBTeusUnpack();
        sgBTeusUnpack.setId(objId);
        sgBTeusUnpack.setQtyTotUnpack(countTeus);
        unpackMapper.updateById(sgBTeusUnpack);

    }


}
