package com.jackrain.nea.sg.out.services;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.r3.mq.util.R3MqSendHelper;
import com.jackrain.nea.redis.config.CusRedisTemplate;
import com.jackrain.nea.sg.basic.common.R3ParamConstants;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.model.result.SgR3BaseResult;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.out.common.SgOutConstants;
import com.jackrain.nea.sg.out.common.SgOutConstantsIF;
import com.jackrain.nea.sg.out.config.SgOutMqConfig;
import com.jackrain.nea.sg.out.mapper.SgBPhyOutNoticesMapper;
import com.jackrain.nea.sg.out.model.request.SgOutNoticesJdCancelMsgRequest;
import com.jackrain.nea.sg.out.model.request.SgPhyOutNoticesBillVoidRequest;
import com.jackrain.nea.sg.out.model.request.SgPhyOutNoticesSaveRequest;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutNotices;
import com.jackrain.nea.sg.out.utils.SgWarehouseESUtils;
import com.jackrain.nea.sg.send.utils.SgStoreUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author leexh
 * @since 2019/4/23 15:12
 */
@Slf4j
@Component
public class SgPhyOutNoticesVoidService {

    @Autowired
    private SgOutMqConfig mqConfig;

    @Autowired
    private R3MqSendHelper r3MqSendHelper;

    @Autowired
    private SgWarehouseESUtils warehouseESUtils;

    public ValueHolderV14<SgR3BaseResult> voidSgPhyOutNoticesThird(SgPhyOutNoticesBillVoidRequest request) {
        ValueHolderV14<SgR3BaseResult> result = new ValueHolderV14<>();
        try {
            result = voidSgPhyOutNotices(request);
        } catch (Exception e) {
            log.error(this.getClass().getName() + ".error:" + e, e);
            result.setCode(ResultCode.FAIL);
            result.setMessage(e.getMessage());
        }
        return result;
    }

    /**
     * 出库通知单作废
     *
     * @param request
     * @return
     */
    public ValueHolderV14<SgR3BaseResult> voidSgPhyOutNotices(SgPhyOutNoticesBillVoidRequest request) {

        ValueHolderV14<SgR3BaseResult> v14 = new ValueHolderV14();
        AssertUtils.notNull(request, "参数不能为空！");
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ".debug, param:" + JSONObject.toJSONString(request));
        }

        // 1.参数检查
        checkParams(request);
        User loginUser = request.getLoginUser();
        JSONArray errorArr = new JSONArray();
        Integer accSuccess = 0, accFailed = 0;

        // 2.接口调用时，原单据编号+类型查询主单据ID
        SgPhyOutNoticesSaveService saveService = ApplicationContextHandle.getBean(SgPhyOutNoticesSaveService.class);
        SgBPhyOutNoticesMapper noticesMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesMapper.class);
        List<SgBPhyOutNotices> noticesList = new ArrayList<>();
        if (request.getIds() == null) {
            List<SgPhyOutNoticesSaveRequest> noticesSaveRequests = request.getNoticesSaveRequests();
            for (int i = 0; i < noticesSaveRequests.size(); i++) {
                /**
                 * 条件1：出库通知单单据编号
                 * 条件2：来源单据ID + 来源单据类型
                 * 条件3：来源单据编号 + 来源单据类型
                 */
                SgPhyOutNoticesSaveRequest noticesSaveRequest = noticesSaveRequests.get(i);
                SgBPhyOutNotices outNotices = saveService.queryNoticesBySource(noticesSaveRequest);
                if (outNotices == null) {
                    accFailed = errorRecord(-1l, "不存在来源单据编号=" + noticesSaveRequest.getSourceBillNo() +
                            ",来源单据类型=" + noticesSaveRequest.getSourceBillType() + "的出库单", errorArr, accFailed);
                    continue;
                }
                noticesList.add(outNotices);
            }
        } else {
            for (Long objId : request.getIds()) {
                SgBPhyOutNotices notices = noticesMapper.selectById(objId);
                if (notices != null) {
                    noticesList.add(notices);
                } else {
                    accFailed = errorRecord(objId, "当前记录不存在！", errorArr, accFailed);
                    continue;
                }
            }
        }

        //记录物流公司是京东快递的通知单id
        List<Long> noticesIdsByJD = Lists.newArrayList();
        if (!CollectionUtils.isEmpty(noticesList)) {
            for (int i = 0; i < noticesList.size(); i++) {
                SgBPhyOutNotices outNotices = noticesList.get(i);
                try {
                    voidSgPhyOutNoticesSingle(outNotices, loginUser, noticesMapper, false);
                    accSuccess++;
                    if (SgOutConstantsIF.LOGISTICS_JD.equals(outNotices.getCpCLogisticsEcode())) {
                        noticesIdsByJD.add(outNotices.getId());
                    }
                } catch (Exception e) {
                    accFailed = errorRecord(outNotices.getId(), e.getMessage(), errorArr, accFailed);
                    continue;
                }
            }
        }

        if (CollectionUtils.isNotEmpty(noticesIdsByJD)) {
            //2019-10-12 增加取消京东快递逻辑
            try {
                SgOutNoticesJdCancelMsgRequest cancelMsgRequest = new SgOutNoticesJdCancelMsgRequest();
                cancelMsgRequest.setResultList(noticesIdsByJD);
                cancelMsgRequest.setLoginUser(loginUser);
                String configName = mqConfig.getConfigName();
                String topic = mqConfig.getMqTopic();
                String msgKey = UUID.randomUUID().toString().replaceAll("-", "");
                String body = JSONObject.toJSONString(cancelMsgRequest);
                String msgId = r3MqSendHelper.sendMessage(configName, body, topic, SgOutConstants.TAG_OUT_NOTICES_JD_CANCEL, msgKey);
                log.debug("作废出库通知单ids" + noticesIdsByJD + "取消京东快递mqMsgId:" + msgId);
            } catch (Exception e) {
                log.error("取消京东快递mq异常!" + e.getMessage());
            }
        }

        if (errorArr.size() > 0) {
            SgR3BaseResult baseResult = new SgR3BaseResult();
            baseResult.setDataArr(errorArr);
            v14.setCode(ResultCode.FAIL);
            v14.setMessage("作废成功记录数：" + accSuccess + "，作废失败记录数：" + accFailed);
            v14.setData(baseResult);
        } else {
            v14.setCode(ResultCode.SUCCESS);
            v14.setMessage("验收成功记录数：" + accSuccess);
        }
        return v14;
    }

    /**
     * 单条作废
     *
     * @param outNotices  通知单对象
     * @param user        用户信息
     * @param isCancelOut 是否pos取消出库
     */
    public void voidSgPhyOutNoticesSingle(SgBPhyOutNotices outNotices, User user, SgBPhyOutNoticesMapper noticesMapper, Boolean isCancelOut) {
        // 判断订单状态是否为“已作废”，若是，则提示： “单据已作废，无需作废！”
        if (SgConstants.IS_ACTIVE_N.equals(outNotices.getIsactive()) &&
                outNotices.getBillStatus() == SgOutConstantsIF.OUT_NOTICES_STATUS_VOID) {
            AssertUtils.logAndThrow("单据已作废，无需作废！", user.getLocale());
        }

        // 判断订单状态是否为“待出库”若不是，则提示： “出库通知单状态不正确，不允许操作”
        if (!isCancelOut && outNotices.getBillStatus() != SgOutConstantsIF.OUT_NOTICES_STATUS_WAIT) {
            AssertUtils.logAndThrow("当前出库通知单已存在出库记录，不允许作废！", user.getLocale());
        }
        outNotices.setIsactive(SgConstants.IS_ACTIVE_N);
        outNotices.setBillStatus(SgOutConstantsIF.OUT_NOTICES_STATUS_VOID);
        StorageESUtils.setBModelDefalutDataByUpdate(outNotices, user);
        outNotices.setModifierename(user.getEname());
        noticesMapper.updateById(outNotices);

        //释放新增通知单传WMS时添加的redis锁
        CusRedisTemplate<String, String> redisTemplate = SgStoreUtils.initRedisTemplate();
        String lockKsy = SgConstants.SG_B_PHY_OUT_NOTICES.toUpperCase() + ":" + outNotices.getSourceBillId() + ":" + outNotices.getSourceBillType() + ":" + "ADD";
        try {
            redisTemplate.delete(lockKsy);
        } catch (Exception e) {
            e.printStackTrace();
            log.error(this.getClass().getName() + " " + lockKsy + " 释放锁失败，请联系管理员！");
        }
        //推送es
        warehouseESUtils.pushESByOutNotices(outNotices.getId(), false, false, null, noticesMapper, null);
    }


    /**
     * 参数校验
     *
     * @param request
     */
    public void checkParams(SgPhyOutNoticesBillVoidRequest request) {
        User loginUser = request.getLoginUser();
        AssertUtils.notNull(loginUser, "请传入用户信息！");
        if (request.getIds() == null) {
            // 接口调用
            AssertUtils.notNull(request.getNoticesSaveRequests(), "参数不能为空！", loginUser.getLocale());
            request.getNoticesSaveRequests().forEach(outNotices -> {
                String billNo = outNotices.getBillNo();
                Integer sourceBillType = outNotices.getSourceBillType();
                Long sourceBillId = outNotices.getSourceBillId();
                String sourceBillNo = outNotices.getSourceBillNo();
                if (!((sourceBillType != null && sourceBillId != null) || (sourceBillType != null && StringUtils.isNotEmpty(sourceBillNo)) || StringUtils.isNotEmpty(billNo))) {
                    AssertUtils.logAndThrow("来源单据ID或者来源单据编号和来源单据类型不能同时为空！", loginUser.getLocale());
                }
            });
        } else {
            // 页面调用
            if (request.getIds().size() <= 0) {
                AssertUtils.logAndThrow("请选择需要作废的数据！", loginUser.getLocale());
            }
        }
    }

    /**
     * 错误信息收集
     *
     * @param mainId    记录ID
     * @param message   错误信息
     * @param errorArr  错误集合
     * @param accFailed 错误数量
     * @return 错误记录数
     */
    public Integer errorRecord(Long mainId, String message, JSONArray errorArr, Integer accFailed) {
        JSONObject errorDate = new JSONObject();
        errorDate.put(R3ParamConstants.CODE, ResultCode.FAIL);
        errorDate.put(R3ParamConstants.OBJID, mainId);
        errorDate.put(R3ParamConstants.MESSAGE, message);
        errorArr.add(errorDate);
        return ++accFailed;
    }
}
