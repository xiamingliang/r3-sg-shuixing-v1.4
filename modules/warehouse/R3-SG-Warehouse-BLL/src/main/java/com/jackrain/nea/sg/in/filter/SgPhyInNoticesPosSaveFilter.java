package com.jackrain.nea.sg.in.filter;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.sg.in.mapper.SgBPhyInNoticesImpItemMapper;
import com.jackrain.nea.sg.in.mapper.SgBPhyInNoticesMapper;
import com.jackrain.nea.sg.in.model.table.SgBPhyInNotices;
import com.jackrain.nea.sg.in.model.table.SgBPhyInNoticesImpItem;
import com.jackrain.nea.tableService.MainTableRecord;
import com.jackrain.nea.tableService.TableServiceContext;
import com.jackrain.nea.tableService.filter.BaseFilter;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author zhu lin yu
 * @since 2019/12/11
 * create at : 2019/12/11 4:56 下午
 */
@Slf4j
@Component
public class SgPhyInNoticesPosSaveFilter extends BaseFilter {

    @Override
    public void after(TableServiceContext context, MainTableRecord row) {
        Long objId = row.getId();

        SgBPhyInNoticesImpItemMapper impItemMapper = ApplicationContextHandle.getBean(SgBPhyInNoticesImpItemMapper.class);
        List<SgBPhyInNoticesImpItem> itemList = impItemMapper.selectList(new QueryWrapper<SgBPhyInNoticesImpItem>().lambda()
                .eq(SgBPhyInNoticesImpItem::getSgBPhyInNoticesId, objId));


        BigDecimal totQtyScan = itemList.stream()
                .map(SgBPhyInNoticesImpItem::getQtyScan).reduce(BigDecimal.ZERO, BigDecimal::add);

        SgBPhyInNoticesMapper packMapper = ApplicationContextHandle.getBean(SgBPhyInNoticesMapper.class);
        SgBPhyInNotices sgBPhyInNotices = new SgBPhyInNotices();
        sgBPhyInNotices.setId(objId);
        sgBPhyInNotices.setQtyTotScan(totQtyScan);
        packMapper.updateById(sgBPhyInNotices);
    }
}
