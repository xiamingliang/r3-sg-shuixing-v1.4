package com.jackrain.nea.sg.inv.mapper;

import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.inv.constants.SgInvConstants;
import com.jackrain.nea.sg.inv.model.table.ScBInventoryImpItem;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.Date;
import java.util.List;

@Mapper
public interface ScBInventoryImpItemMapper extends ExtentionMapper<ScBInventoryImpItem> {

    @Select("SELECT DISTINCT\n" +
            "\tPS_C_PRO_ECODE\n" +
            "FROM\n" +
            "\t" + SgInvConstants.SC_B_INVENTORY_IMP_ITEM + "\n" +
            "WHERE\n" +
            "\tSC_B_INVENTORY_ID IN (\n" +
            "\t\tSELECT\n" +
            "\t\t\tID\n" +
            "\t\tFROM\n" +
            "\t\t\t" + SgInvConstants.SC_B_INVENTORY + "\n" +
            "\t\tWHERE\n" +
            "\t\t\tCP_C_PHY_WAREHOUSE_ID = #{warehouseId}\n" +
            "\t\tAND INVENTORY_TYPE = #{inventoryType}\n" +
            "\t\tAND INVENTORY_DATE = #{inventoryDate}\n" +
            "\t\tAND POL_STATUS = #{polStatus}\n" +
            "\t\tAND ISACTIVE = '" + SgConstants.IS_ACTIVE_Y + "'\n" +
            "\t)")
    List<String> selectProEcodes(@Param("warehouseId") Long warehouseId,
                                 @Param("inventoryType") Integer inventoryType,
                                 @Param("inventoryDate") Date inventoryDate,
                                 @Param("polStatus") Integer polStatus);
}