package com.jackrain.nea.sg.out.services;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.core.db.Tools;
import com.jackrain.nea.data.basic.model.request.EwayBillQueryRequest;
import com.jackrain.nea.data.basic.model.request.GenerateSerialNumberRequest;
import com.jackrain.nea.data.basic.services.BasicAdQueryService;
import com.jackrain.nea.data.basic.services.BasicStQueryService;
import com.jackrain.nea.oc.basic.common.OcBasicConstants;
import com.jackrain.nea.redis.config.CusRedisTemplate;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.config.SgStorageBoxConfig;
import com.jackrain.nea.sg.basic.mapper.CpCPhyWarehouseMapper;
import com.jackrain.nea.sg.basic.model.table.CpCPhyWarehouse;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.out.common.SgOutConstants;
import com.jackrain.nea.sg.out.common.SgOutConstantsIF;
import com.jackrain.nea.sg.out.mapper.SgBPhyOutNoticesImpItemMapper;
import com.jackrain.nea.sg.out.mapper.SgBPhyOutNoticesItemMapper;
import com.jackrain.nea.sg.out.mapper.SgBPhyOutNoticesMapper;
import com.jackrain.nea.sg.out.model.request.SgPhyOutNoticesBillSaveRequest;
import com.jackrain.nea.sg.out.model.request.SgPhyOutNoticesItemSaveRequest;
import com.jackrain.nea.sg.out.model.request.SgPhyOutNoticesSaveRequest;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutNotices;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutNoticesImpItem;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutNoticesItem;
import com.jackrain.nea.sg.out.utils.SgWarehouseESUtils;
import com.jackrain.nea.sg.send.common.SgSendConstants;
import com.jackrain.nea.sg.send.utils.SgStoreUtils;
import com.jackrain.nea.st.model.result.EwayBillResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * 批量新增出库通知单-传WMS用
 *
 * @author: 舒威
 * @since: 2019/10/11
 * create at : 2019/10/11 19:00
 */
@Slf4j
@Component
public class SgPhyOutNoticesSaveWMSService {

    @Autowired
    private SgWarehouseESUtils warehouseESUtils;

    @Autowired
    private SgStorageBoxConfig storageBoxConfig;

    public ValueHolderV14<Map<Long, CpCPhyWarehouse>> batchQueryWarehouse(List<Long> ids) {
        ValueHolderV14<Map<Long, CpCPhyWarehouse>> vh = new ValueHolderV14<>(ResultCode.SUCCESS, "success");
        CpCPhyWarehouseMapper warehouseMapper = ApplicationContextHandle.getBean(CpCPhyWarehouseMapper.class);
        List<CpCPhyWarehouse> warehouseList = warehouseMapper.selectList(new QueryWrapper<CpCPhyWarehouse>().lambda().in(CpCPhyWarehouse::getId, ids));
        if (CollectionUtils.isNotEmpty(warehouseList)) {
            Map<Long, CpCPhyWarehouse> warehouseMap = warehouseList.stream().collect(Collectors.toMap(CpCPhyWarehouse::getId, warehouse -> warehouse));
            vh.setData(warehouseMap);
        } else {
            vh.setCode(ResultCode.FAIL);
            vh.setMessage("实体仓ids" + ids + "不存在于PG实体仓档案中!");
        }
        return vh;
    }

    public ValueHolderV14<HashMap<Long, HashMap<SgBPhyOutNotices, List<SgBPhyOutNoticesItem>>>> batchQueryOutNotices(List<Long> sourceBillIds, Integer sourceBillType) {
        ValueHolderV14<HashMap<Long, HashMap<SgBPhyOutNotices, List<SgBPhyOutNoticesItem>>>> vh = new ValueHolderV14<>(ResultCode.SUCCESS, "success");
        SgBPhyOutNoticesMapper noticesMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesMapper.class);
        SgBPhyOutNoticesItemMapper noticesItemMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesItemMapper.class);
        HashMap<Long, HashMap<SgBPhyOutNotices, List<SgBPhyOutNoticesItem>>> map = Maps.newHashMap();
        List<SgBPhyOutNotices> notices = noticesMapper.selectList(new QueryWrapper<SgBPhyOutNotices>().lambda()
                .in(SgBPhyOutNotices::getSourceBillId, sourceBillIds)
                .eq(SgBPhyOutNotices::getSourceBillType, sourceBillType)
                .eq(SgBPhyOutNotices::getIsactive, SgConstants.IS_ACTIVE_Y));
        List<Long> ids = Lists.newArrayList();
        HashMap<Long, List<SgBPhyOutNoticesItem>> itemMap = Maps.newHashMap();
        if (CollectionUtils.isNotEmpty(notices)) {
            ids = notices.stream().map(SgBPhyOutNotices::getId).collect(Collectors.toList());
            List<SgBPhyOutNoticesItem> items = noticesItemMapper.selectList(new QueryWrapper<SgBPhyOutNoticesItem>().lambda()
                    .in(SgBPhyOutNoticesItem::getSgBPhyOutNoticesId, ids));
            if (CollectionUtils.isNotEmpty(items)) {
                for (SgBPhyOutNoticesItem item : items) {
                    Long outNoticesId = item.getSgBPhyOutNoticesId();
                    if (itemMap.containsKey(outNoticesId)) {
                        List<SgBPhyOutNoticesItem> items1 = itemMap.get(outNoticesId);
                        items1.add(item);
                    } else {
                        List<SgBPhyOutNoticesItem> items1 = Lists.newArrayList();
                        items1.add(item);
                        itemMap.put(outNoticesId, items1);
                    }
                }
                for (SgBPhyOutNotices notice : notices) {
                    HashMap<SgBPhyOutNotices, List<SgBPhyOutNoticesItem>> data = Maps.newHashMap();
                    data.put(notice, itemMap.get(notice.getId()));
                    map.put(notice.getSourceBillId(), data);
                }
                vh.setData(map);
            } else {
                vh.setCode(ResultCode.FAIL);
                vh.setMessage("出库通知为空单!");
            }
        }
        return vh;
    }

    public ValueHolderV14<JSONObject> saveSgPhyOutNoticesByWms(List<SgPhyOutNoticesBillSaveRequest> requests, User user) {
        ValueHolderV14<JSONObject> vh = new ValueHolderV14<>(ResultCode.SUCCESS, "success");
        JSONObject dataObj = new JSONObject();
        HashMap<Long, String> err = Maps.newHashMap();
        ValueHolderV14<HashMap<String, Object>> checkResult = checkParams(requests);
        if (!checkResult.isOK()) {
            HashMap<String, Object> data = checkResult.getData();
            err = (HashMap<Long, String>) data.get("error");
            vh.setCode(ResultCode.FAIL);
            vh.setMessage("批量新增出库通知单失败!");
        } else {
            HashMap<String, Object> data = checkResult.getData();
            //check通过数据
            List<SgPhyOutNoticesBillSaveRequest> requestList = (List<SgPhyOutNoticesBillSaveRequest>) data.get("data");
            HashMap<Long, String> stMap = (HashMap<Long, String>) data.get("stMap");
            HashMap<String, EwayBillResult> ewayBillMap = (HashMap<String, EwayBillResult>) data.get("ewayBillMap");
            err = (HashMap<Long, String>) data.get("error");
            if (CollectionUtils.isEmpty(requestList)) {
                vh.setCode(ResultCode.FAIL);
                vh.setMessage("批量新增出库通知单失败!");
            } else {
                //获取批量新增的数据
                ValueHolderV14<JSONObject> paramsResult = getBatchSaveParams(requestList, stMap, err, ewayBillMap, user);
                if (paramsResult.isOK()) {
                    JSONObject resultData = paramsResult.getData();
                    HashMap<SgBPhyOutNotices, List<SgBPhyOutNoticesItem>> paramsMap = (HashMap<SgBPhyOutNotices, List<SgBPhyOutNoticesItem>>) resultData.get("data");
                    HashMap<Long, Long> shopIdMap = (HashMap<Long, Long>) resultData.get("shopIdMap");
                    HashMap<Long, Integer> etypeMap = (HashMap<Long, Integer>) resultData.get("etypeMap");
                    //批量新增
                    ValueHolderV14<HashMap<SgBPhyOutNotices, List<SgBPhyOutNoticesItem>>> batchSaveResult = batchSaveOutNotices(paramsMap, err);
                    if (!batchSaveResult.isOK()) {
                        vh.setCode(ResultCode.FAIL);
                        vh.setMessage("批量新增出库通知单失败!");
                    } else {
                        //新增成功的 通知单-明细
                        HashMap<SgBPhyOutNotices, List<SgBPhyOutNoticesItem>> successData = batchSaveResult.getData();
                        //解析错误信息
                        vh.setMessage("批量新增出库通知单成功!");
                        dataObj.put("data", successData);
                        dataObj.put("shopIdMap", shopIdMap);
                        dataObj.put("etypeMap", etypeMap);
                    }
                }
            }
        }
        dataObj.put("error", err);
        vh.setData(dataObj);
        return vh;
    }

    public ValueHolderV14<HashMap<SgBPhyOutNotices, List<SgBPhyOutNoticesItem>>> batchSaveOutNotices(HashMap<SgBPhyOutNotices, List<SgBPhyOutNoticesItem>> paramsMap, HashMap<Long, String> err) {
        SgPhyOutNoticesSaveWMSService bean = ApplicationContextHandle.getBean(SgPhyOutNoticesSaveWMSService.class);
        SgBPhyOutNoticesMapper noticesMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesMapper.class);
        SgBPhyOutNoticesItemMapper noticesItemMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesItemMapper.class);
        ValueHolderV14<HashMap<SgBPhyOutNotices, List<SgBPhyOutNoticesItem>>> v14 = new ValueHolderV14<>(ResultCode.SUCCESS, "success");
        boolean flag = false;
        HashMap<SgBPhyOutNotices, List<SgBPhyOutNoticesItem>> noticesMap = Maps.newHashMap();
        Iterator<Map.Entry<SgBPhyOutNotices, List<SgBPhyOutNoticesItem>>> iterator = paramsMap.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<SgBPhyOutNotices, List<SgBPhyOutNoticesItem>> next = iterator.next();
            SgBPhyOutNotices notices = next.getKey();
            List<SgBPhyOutNoticesItem> items = next.getValue();
            CusRedisTemplate<String, String> redisTemplate = SgStoreUtils.initRedisTemplate();
            String lockKsy = SgConstants.SG_B_PHY_OUT_NOTICES.toUpperCase() + ":" + notices.getSourceBillId() + ":" + notices.getSourceBillType() + ":" + "ADD";
            try {
                Boolean ifAbsent = redisTemplate.opsForValue().setIfAbsent(lockKsy, "OK");
                if (ifAbsent != null && ifAbsent) {
                    redisTemplate.expire(lockKsy, 1, TimeUnit.DAYS);
                    bean.saveOutNotices(notices, items, noticesMapper, noticesItemMapper);
                    flag = true;
                    noticesMap.put(notices, items);
                    //推送ES
                    warehouseESUtils.pushESByOutNotices(notices.getId(), true, false, null, noticesMapper, noticesItemMapper);
                } else {
                    AssertUtils.logAndThrow("来源单据id[" + notices.getSourceBillId() + "],存在多张有效通知单,不允许新增！");
                }
            } catch (Exception e) {
                try {
                    redisTemplate.delete(lockKsy);
                } catch (Exception e2) {
                    log.error(this.getClass().getName() + " " + lockKsy + " 释放锁失败，请联系管理员！");
                }
                String messageExtra = AssertUtils.getMessageExtra("来源单据id[" + notices.getSourceBillId() + "],出库通知单新增失败！", e);
                log.error(messageExtra);
                err.put(notices.getSourceBillId(), messageExtra);
            }
        }
        if (!flag) {
            v14.setCode(ResultCode.FAIL);
            v14.setMessage("批量新增出库通知单失败!");
        } else {
            v14.setData(noticesMap);
        }
        return v14;
    }

    @Transactional(rollbackFor = Exception.class)
    public void saveOutNotices(SgBPhyOutNotices notices, List<SgBPhyOutNoticesItem> items,
                               SgBPhyOutNoticesMapper noticesMapper, SgBPhyOutNoticesItemMapper noticesItemMapper) {
        Integer cnt = noticesMapper.selectCount(new QueryWrapper<SgBPhyOutNotices>().lambda()
                .eq(SgBPhyOutNotices::getSourceBillId, notices.getSourceBillId())
                .eq(SgBPhyOutNotices::getSourceBillType, notices.getSourceBillType())
                .eq(SgBPhyOutNotices::getIsactive, SgConstants.IS_ACTIVE_Y));
        if (log.isDebugEnabled()) {
            log.debug("来源单据id[" + notices.getSourceBillId() + "],出库通知单数量:" + cnt);
        }
        if (cnt != null && cnt > 0) {
            AssertUtils.logAndThrow("来源单据id[" + notices.getSourceBillId() + "],存在多张有效通知单,不允许新增！");
        }
        int count = noticesMapper.insert(notices);
        if (count < 1) {
            AssertUtils.logAndThrow("来源单据id[" + notices.getSourceBillId() + "],出库通知单新增失败！");
        }
        batchSaveOutNoticesItems(items, noticesItemMapper, notices.getSourceBillId());
        if (storageBoxConfig.getBoxEnable()) {
            batchSaveOutNoticesImpItems(items, notices.getSourceBillId());
        }
    }

    private void batchSaveOutNoticesImpItems(List<SgBPhyOutNoticesItem> items, Long sourceBillId) {
        List<SgBPhyOutNoticesImpItem> impItems = Lists.newArrayList();
        for (SgBPhyOutNoticesItem item : items) {
            SgBPhyOutNoticesImpItem impItem = new SgBPhyOutNoticesImpItem();
            BeanUtils.copyProperties(item, impItem);
            impItem.setId(Tools.getSequence(SgConstants.SG_B_PHY_OUT_NOTICES_IMP_ITEM));
            impItem.setIsTeus(OcBasicConstants.IS_MATCH_SIZE_N);
            impItems.add(impItem);
        }
        List<List<SgBPhyOutNoticesImpItem>> baseModelPageList = StorageESUtils.getBaseModelPageList(
                impItems, SgOutConstants.INSERT_PAGE_SIZE);

        SgBPhyOutNoticesImpItemMapper impItemMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesImpItemMapper.class);
        for (List<SgBPhyOutNoticesImpItem> pageList : baseModelPageList) {
            if (CollectionUtils.isEmpty(pageList)) {
                continue;
            }
            int count = impItemMapper.batchInsert(pageList);
            if (count != pageList.size()) {
                AssertUtils.logAndThrow("来源单据id[" + sourceBillId + "],出库通知单录入明细批量新增失败！");
            }
        }
    }

    public void batchSaveOutNoticesItems(List<SgBPhyOutNoticesItem> items, SgBPhyOutNoticesItemMapper noticesItemMapper, Long sourceBillId) {
        List<List<SgBPhyOutNoticesItem>> baseModelPageList = StorageESUtils.getBaseModelPageList(
                items, SgOutConstants.INSERT_PAGE_SIZE);

        for (List<SgBPhyOutNoticesItem> pageList : baseModelPageList) {
            if (CollectionUtils.isEmpty(pageList)) {
                continue;
            }
            int count = noticesItemMapper.batchInsert(pageList);
            if (count != pageList.size()) {
                AssertUtils.logAndThrow("来源单据id[" + sourceBillId + "],出库通知单明细批量新增失败！");
            }
        }
    }

    public ValueHolderV14<JSONObject> getBatchSaveParams(List<SgPhyOutNoticesBillSaveRequest> requestList,
                                                         HashMap<Long, String> stMap, HashMap<Long, String> err,
                                                         HashMap<String, EwayBillResult> ewayBillMap, User user) {
        ValueHolderV14<JSONObject> vh = new ValueHolderV14<>(ResultCode.SUCCESS, "success");

        JSONObject dataObj = new JSONObject();
        HashMap<SgBPhyOutNotices, List<SgBPhyOutNoticesItem>> data = Maps.newHashMap();
        //挂靠店铺信息
        HashMap<Long, Long> shopIdMap = Maps.newHashMap();
        //电子面单类型信息
        HashMap<Long, Integer> etypeMap = Maps.newHashMap();
//        BasicAdQueryService adQueryService = ApplicationContextHandle.getBean(BasicAdQueryService.class);

        for (SgPhyOutNoticesBillSaveRequest request : requestList) {
            //获取通知单主表信息
            SgBPhyOutNotices notices = new SgBPhyOutNotices();
            try {
                BeanUtils.copyProperties(request.getOutNoticesRequest(), notices);
                Long objId = Tools.getSequence(SgConstants.SG_B_PHY_OUT_NOTICES);
                notices.setId(objId);
                // 获取单据编号
                String billNo = null;
                try {
                    billNo=SgStoreUtils.getBillNo(SgOutConstants.SQE_SG_B_OUT_NOTICES, SgConstants.SG_B_PHY_OUT_NOTICES.toUpperCase(), notices, user.getLocale());
                } catch (Exception e) {
                    String wmsErrMessage = AssertUtils.getWMSErrMessage(e, "单据编号采集异常!");
                    log.error(wmsErrMessage);
                    AssertUtils.logAndThrow(wmsErrMessage);
                }
                notices.setBillNo(billNo);
                notices.setBillDate(Optional.ofNullable(notices.getBillDate()).orElse(new Date()));
                notices.setTotQtyOut(BigDecimal.ZERO);  // 总出库数量
                notices.setTotAmtCostOut(BigDecimal.ZERO);  // 总出库成交金额
                notices.setTotAmtListOut(BigDecimal.ZERO);  // 总出库吊牌金额

                notices.setIsactive(SgConstants.IS_ACTIVE_Y);    // 未作废
                notices.setBillStatus(SgOutConstantsIF.OUT_NOTICES_STATUS_WAIT);   // 待出库
                StorageESUtils.setBModelDefalutData(notices, user);
                notices.setOwnerename(user.getEname());
                notices.setModifierename(user.getEname());
                //2019-09-06 截取备注等超长字段
                notices.setRemark(StorageESUtils.strSubString(notices.getRemark(), SgConstants.SG_COMMON_REMARK_SIZE));
                notices.setBuyerRemark(StorageESUtils.strSubString(notices.getBuyerRemark(), SgConstants.SG_COMMON_REMARK_SIZE));
                notices.setSellerRemark(StorageESUtils.strSubString(notices.getSellerRemark(), SgConstants.SG_COMMON_REMARK_SIZE));
                notices.setWmsFailReason(StorageESUtils.strSubString(notices.getWmsFailReason(), SgConstants.SG_COMMON_STRING_SIZE));

                //合并相同sku的通知单明细
                List<SgPhyOutNoticesItemSaveRequest> itemRequests = request.getOutNoticesItemRequests();
                Map<Long, SgPhyOutNoticesItemSaveRequest> itemsMap = Maps.newHashMap();
                for (SgPhyOutNoticesItemSaveRequest itemRequest : itemRequests) {
                    Long psCSkuId = itemRequest.getPsCSkuId();
                    if (itemsMap.containsKey(psCSkuId)) {
                        SgPhyOutNoticesItemSaveRequest orgItemRequest = itemsMap.get(psCSkuId);
                        BigDecimal orgQty = Optional.ofNullable(orgItemRequest.getQty()).orElse(BigDecimal.ZERO);
                        BigDecimal qty = Optional.ofNullable(itemRequest.getQty()).orElse(BigDecimal.ZERO);
                        orgItemRequest.setQty(qty.add(orgQty));
                    } else {
                        itemsMap.put(psCSkuId, itemRequest);
                    }
                }
                List<SgPhyOutNoticesItemSaveRequest> mergeItemList = new ArrayList<>(itemsMap.values());
                //获取通知单明细信息
                //获取总通知数量、总吊牌金额、总成交金额、总差异数量、总差异成交金额、总差异吊牌金额
                BigDecimal totQty = BigDecimal.ZERO;
                BigDecimal totAmtList = BigDecimal.ZERO;
                BigDecimal totAmtCost = BigDecimal.ZERO;
                BigDecimal totQtyDiff = BigDecimal.ZERO;
                BigDecimal totAmtCostDiff = BigDecimal.ZERO;
                BigDecimal totAmtListDiff = BigDecimal.ZERO;
                List<SgBPhyOutNoticesItem> items = new ArrayList<>();
                for (SgPhyOutNoticesItemSaveRequest item : mergeItemList) {
                    BigDecimal qty = item.getQty();             // 通知数量
                    BigDecimal priceList = item.getPriceList(); // 吊牌价
                    BigDecimal amtList = priceList.multiply(qty);  // 吊牌金额
                    BigDecimal amtCost = BigDecimal.ZERO;
                    BigDecimal amtCostDiff = BigDecimal.ZERO;

                    SgBPhyOutNoticesItem noticesItem = new SgBPhyOutNoticesItem();
                    BeanUtils.copyProperties(item, noticesItem);
                    noticesItem.setId(Tools.getSequence(SgConstants.SG_B_PHY_OUT_NOTICES_ITEM));
                    noticesItem.setSgBPhyOutNoticesId(objId);
                    noticesItem.setQty(qty);    // 通知数量
                    noticesItem.setQtyDiff(qty);   // 差异数量（通知数量-出库数量）
                    noticesItem.setAmtList(amtList);  //吊牌金额
                    noticesItem.setAmtListDiff(amtList);  //差异吊牌金额
                    noticesItem.setQtyOut(BigDecimal.ZERO);  //出库数量
                    noticesItem.setAmtListOut(BigDecimal.ZERO);  //出库吊牌金额
                    noticesItem.setAmtCostOut(BigDecimal.ZERO);    //出库成交金额

                    if (item.getPriceCost() != null) { // 成交金额、差异成交金额
                        amtCost = item.getPriceCost().multiply(qty);
                        amtCostDiff = amtCost;
                    }
                    noticesItem.setAmtCost(amtCost);
                    noticesItem.setAmtCostDiff(amtCostDiff);

                    StorageESUtils.setBModelDefalutData(noticesItem, user);
                    noticesItem.setOwnerename(user.getEname());
                    noticesItem.setModifierename(user.getEname());
                    items.add(noticesItem);

                    totQty = totQty.add(qty);
                    totAmtList = totAmtList.add(amtList);
                    totQtyDiff = totQtyDiff.add(qty);
                    totAmtListDiff = totAmtListDiff.add(amtList);
                    totAmtCost = totAmtCost.add(amtCost);
                    totAmtCostDiff = totAmtCostDiff.add(amtCostDiff);
                }

                notices.setTotQty(totQty);
                notices.setTotAmtList(totAmtList);
                notices.setTotAmtCost(totAmtCost);
                notices.setTotQtyDiff(totQtyDiff);
                notices.setTotAmtCostDiff(totAmtCostDiff);
                notices.setTotAmtListDiff(totAmtListDiff);

                //集成京东电子面单-更新是否获取电子面单 默认是否获取电子面单为否
                notices.setIsEnableewaybill(SgOutConstantsIF.IS_ENABLEEWAYBILL_N);
                if (request.getOutNoticesRequest().getSourceBillType() == SgConstantsIF.BILL_TYPE_RETAIL) {
                    String key = stMap.get(notices.getSourceBillId());
                    if (MapUtils.isNotEmpty(ewayBillMap)) {
                        EwayBillResult ewayBillResult = ewayBillMap.get(key);
                        log.debug("key:{},电子策略信息:{};", key, JSONObject.toJSONString(ewayBillResult));
                        if (ewayBillResult != null && ewayBillResult.getStCEwaybillLogisticsDO() != null) {
                            Integer etype = ewayBillResult.getStCEwaybillLogisticsDO().getEtype();
                            String affiliatedShop = ewayBillResult.getStCEwaybillLogisticsDO().getAffiliatedShop();
                            if (etype != null && etype.equals(SgOutConstantsIF.EWAYBILL_TYPE_STRAIGHT) &&
                                    SgOutConstantsIF.LOGISTICS_JD.equals(notices.getCpCLogisticsEcode())) {
                                notices.setIsEnableewaybill(SgOutConstantsIF.IS_ENABLEEWAYBILL_Y);
                                etypeMap.put(notices.getId(), etype);
                                if (StringUtils.isNotEmpty(affiliatedShop)) {
                                    shopIdMap.put(notices.getId(), Long.parseLong(affiliatedShop));
                                }
                            }
                        }
                    }
                }

                data.put(notices, items);
            } catch (Exception e) {
                String messageExtra = AssertUtils.getMessageExtra("来源单据id[" + notices.getSourceBillId() + "],组装通知单新增数据异常!", e);
                log.error(messageExtra);
                err.put(notices.getSourceBillId(), messageExtra);
            }
        }

        if (MapUtils.isEmpty(data)) {
            vh.setCode(ResultCode.FAIL);
        } else {
            dataObj.put("data", data);
            dataObj.put("shopIdMap", shopIdMap);
            dataObj.put("etypeMap", etypeMap);
            vh.setData(dataObj);
        }
        return vh;
    }

    public ValueHolderV14<HashMap<String, Object>> checkParams(List<SgPhyOutNoticesBillSaveRequest> requests) {
        ValueHolderV14<HashMap<String, Object>> vh = new ValueHolderV14<>(ResultCode.SUCCESS, "success");
        Set<Long> wareHouseIds = new HashSet<>();
        List<EwayBillQueryRequest> ewayBillQueryRequests = Lists.newArrayList();
        //来源单据id - 电子面单策略key：shopid_logiscid
        HashMap<Long, String> stMap = Maps.newHashMap();
        //电子面单策略key：
        HashMap<String, EwayBillResult> ewayBillMap = Maps.newHashMap();
        HashMap<String, Object> data = Maps.newHashMap();
        HashMap<Long, String> err = Maps.newHashMap();
        List<SgPhyOutNoticesBillSaveRequest> var1 = Lists.newArrayList();
        List<SgPhyOutNoticesBillSaveRequest> var2 = Lists.newArrayList();
        t:
        for (SgPhyOutNoticesBillSaveRequest request : requests) {
            SgPhyOutNoticesSaveRequest notices = request.getOutNoticesRequest();
            if (request.getLoginUser() == null ||
                    notices.getOutType() == null ||
                    notices.getSourceBillId() == null ||
                    notices.getSourceBillNo() == null ||
                    notices.getSourceBillType() == null ||
                    notices.getCpCPhyWarehouseId() == null ||
                    notices.getCpCPhyWarehouseEcode() == null ||
                    notices.getCpCPhyWarehouseEname() == null ||
                    CollectionUtils.isEmpty(request.getOutNoticesItemRequests())) {
                err.put(notices.getSourceBillId(), "来源单据id[" + notices.getSourceBillId() + "],新增出库通知单异常!出库通知单信息缺失:出库类型、来源单据信息、实体仓、明细信息不能为空!");
                continue;
            }

            for (SgPhyOutNoticesItemSaveRequest item : request.getOutNoticesItemRequests()) {
                if (item.getSourceBillItemId() == null ||
                        item.getPsCSkuId() == null ||
                        item.getPsCSkuEcode() == null ||
                        item.getPsCProId() == null ||
                        item.getPsCProEcode() == null ||
                        item.getPsCProEname() == null ||
                        item.getPsCSpec1Id() == null ||
                        item.getPsCSpec1Ecode() == null ||
                        item.getPsCSpec1Ename() == null ||
                        item.getPsCSpec2Id() == null ||
                        item.getPsCSpec2Ecode() == null ||
                        item.getPsCSpec2Ename() == null ||
                        item.getPriceList() == null ||
                        item.getQty() == null) {
                    err.put(notices.getSourceBillId(), "来源单据id[" + notices.getSourceBillId() + "],新增出库通知单异常!出库通知单信息缺失:来源单据明细id、条码、商品、规格、吊牌价、通知数量不能为空!");
                    continue t;
                }
            }

            //集成京東电子面单-增加 物流公司和店铺信息非空判断
            //2019-08-04  若为货到付款需要check付款金额是否为空
            if (notices.getSourceBillType() == SgConstantsIF.BILL_TYPE_RETAIL) {
                Long cpCLogisticsId = notices.getCpCLogisticsId();
                String cpCLogisticsEcode = notices.getCpCLogisticsEcode();
                Long cpCShopId = notices.getCpCShopId();

                if (cpCLogisticsId == null || StringUtils.isEmpty(cpCLogisticsEcode) || cpCShopId == null) {
                    err.put(notices.getSourceBillId(), "来源单据id[" + notices.getSourceBillId() + "],新增出库通知单异常!出库通知单信息缺失:物流公司id、物流公司编码、店铺id不能为空！");
                    continue;
                }
                if (notices.getIsCod() != null && notices.getIsCod() == SgOutConstants.PAY_TYPE_ARRIVE && notices.getAmtPayment() == null) {
                    err.put(notices.getSourceBillId(), "来源单据id[" + notices.getSourceBillId() + "],新增出库通知单异常!出库通知单信息缺失:付款类型为货到付款,付款金额不能为空!");
                    continue;
                }

                EwayBillQueryRequest queryRequest = new EwayBillQueryRequest();
                queryRequest.setLogiscId(cpCLogisticsId);
                queryRequest.setShopId(cpCShopId);
                ewayBillQueryRequests.add(queryRequest);

                stMap.put(notices.getSourceBillId(), cpCShopId + "_" + cpCLogisticsId);
            }

            wareHouseIds.add(notices.getCpCPhyWarehouseId());
            var1.add(request);
        }

        //批量查询电子面单策略
        if (CollectionUtils.isNotEmpty(ewayBillQueryRequests)) {
            try {
                BasicStQueryService basicStQueryService = ApplicationContextHandle.getBean(BasicStQueryService.class);
                ewayBillMap = (HashMap<String, EwayBillResult>) basicStQueryService.getEwayBillByShopId(ewayBillQueryRequests);
                log.debug("批量查询电子面单策略出参:" + JSONObject.toJSONString(ewayBillMap, SerializerFeature.WriteMapNullValue));
            } catch (Exception e) {
                String messageExtra = AssertUtils.getMessageExtra("批量查询电子面单策略失败!", e);
                log.error(messageExtra);
            }
        }

        /**
         * 判断主表中【是否传wms】是否存在为空值
         *  若存在，则判断主表中的实体仓在【实体仓档案】中的【WMS管控仓】是否勾选
         *      若未勾选，则生成出库通知单的时候【是否传WMS】字段赋值为“0-否”
         *      若勾选，则生成出库通知的时候【是否传WMS】字段赋值为“1-是”
         *  若不存在，则生成出库通知的时候【是否传WMS】字段取传入的信息。
         */
        if (CollectionUtils.isNotEmpty(var1)) {
            // 查询实体仓信息
            CpCPhyWarehouseMapper mapper = ApplicationContextHandle.getBean(CpCPhyWarehouseMapper.class);
            List<Long> warehouses = new ArrayList<>(wareHouseIds);
            List<CpCPhyWarehouse> warehouseList = mapper.selectList(new QueryWrapper<CpCPhyWarehouse>().lambda().in(CpCPhyWarehouse::getId, warehouses));
            Map<Long, CpCPhyWarehouse> warehouseMap = warehouseList.stream().collect(Collectors.toMap(CpCPhyWarehouse::getId, warehouse -> warehouse));
            for (SgPhyOutNoticesBillSaveRequest request : var1) {
                SgPhyOutNoticesSaveRequest notices = request.getOutNoticesRequest();
                CpCPhyWarehouse warehouse = warehouseMap.get(notices.getCpCPhyWarehouseId());
                if (warehouse == null) {
                    err.put(notices.getSourceBillId(), "来源单据id[" + notices.getSourceBillId() + "],新增出库通知单异常!PG实体仓档案中不存在该实体仓[" + notices.getCpCPhyWarehouseId() + "]信息！");
                    continue;
                }
                notices.setIsPassWms(warehouse.getWmsControlWarehouse());
                notices.setGoodsOwner(warehouse.getOwnerCode());
                var2.add(request);
            }
        }

        if (CollectionUtils.isNotEmpty(var1) || CollectionUtils.isNotEmpty(var2)) {
            data.put("data", CollectionUtils.isNotEmpty(var2) ? var2 : var1);
            data.put("stMap", stMap);
            data.put("ewayBillMap", ewayBillMap);
            data.put("error", err);
            vh.setData(data);
        } else {
            vh.setCode(ResultCode.FAIL);
            vh.setMessage("参数check异常!");
            data.put("error", err);
            vh.setData(data);
        }
        log.debug("check结束:{};", JSONObject.toJSONString(vh));
        return vh;
    }

}
