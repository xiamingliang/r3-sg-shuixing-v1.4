package com.jackrain.nea.sg.in.validate;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.cp.result.CpCStore;
import com.jackrain.nea.data.basic.model.request.StoreInfoQueryRequest;
import com.jackrain.nea.data.basic.services.BasicCpQueryService;
import com.jackrain.nea.sg.in.common.SgInNoticeConstants;
import com.jackrain.nea.sg.in.mapper.SgBPhyInNoticesImpItemMapper;
import com.jackrain.nea.sg.in.mapper.SgBPhyInNoticesMapper;
import com.jackrain.nea.sg.in.model.table.SgBPhyInNotices;
import com.jackrain.nea.sg.in.model.table.SgBPhyInNoticesImpItem;
import com.jackrain.nea.tableService.MainTableRecord;
import com.jackrain.nea.tableService.TableServiceContext;
import com.jackrain.nea.tableService.validate.BaseValidator;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.utils.AssertUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author zhu lin yu
 * @since 2019/12/11
 * create at : 2019/12/11 11:20 上午
 */
@Slf4j
@Component
public class SgPhyInNoticesPosAuditValidate extends BaseValidator {
    @Override
    public void validate(TableServiceContext context, MainTableRecord currentRow) {
        Long objId = currentRow.getId();
        AssertUtils.notNull(objId, "门店入库单ID为空！", context.getLocale());

        //查询入库结果单主表
        SgBPhyInNoticesMapper mapper = ApplicationContextHandle.getBean(SgBPhyInNoticesMapper.class);
        SgBPhyInNotices sgBPhyInNotices = mapper.selectById(objId);
        AssertUtils.notNull(sgBPhyInNotices, "当前记录已不存在！", context.getLocale());

        // 校验单据状态
        Integer status = sgBPhyInNotices.getStatus();

        if (SgInNoticeConstants.AUDIT_STATUS_AUDIT == status) {
            AssertUtils.logAndThrow("当前记录已审核，不允许重复入库！", context.getLocale());
        } else if (SgInNoticeConstants.AUDIT_STATUS_VOID == status) {
            AssertUtils.logAndThrow("当前记录已作废，不允许审核!", context.getLocale());
        }

        //查询入库结果单明细表
        SgBPhyInNoticesImpItemMapper impItemMapper = ApplicationContextHandle.getBean(SgBPhyInNoticesImpItemMapper.class);
        List<SgBPhyInNoticesImpItem> sgBPhyInNoticesImpItems = impItemMapper.selectList(new QueryWrapper<SgBPhyInNoticesImpItem>().lambda()
                .eq(SgBPhyInNoticesImpItem::getSgBPhyInNoticesId, objId));
        AssertUtils.isTrue(CollectionUtils.isNotEmpty(sgBPhyInNoticesImpItems), "当前记录明细已不存在！", context.getLocale());

        for (SgBPhyInNoticesImpItem sgBPhyInNoticesImpItem : sgBPhyInNoticesImpItems) {
            BigDecimal qtyScan = sgBPhyInNoticesImpItem.getQtyScan();
            BigDecimal qtyDiff = sgBPhyInNoticesImpItem.getQtyDiff();
            if (qtyScan.compareTo(qtyDiff) > 0) {
                AssertUtils.logAndThrow("扫描数量不允许大于差异数量!", context.getLocale());
            }
        }
        List<SgBPhyInNoticesImpItem> collect = sgBPhyInNoticesImpItems.stream()
                .filter(o -> o.getQtyScan().compareTo(BigDecimal.ZERO) <= 0).collect(Collectors.toList());


        if (CollectionUtils.isNotEmpty(collect) && sgBPhyInNoticesImpItems.size() == collect.size()) {
            AssertUtils.logAndThrow("当前扫描数量全为0，不允许入库!", context.getLocale());
        }


        /**
         * 如果本单的出库日期 - 当前日期 >  当前收货店仓（为实体仓）对应逻辑仓在【逻辑仓档案】
         * 中的“门店确认收货期限（天）”，则提示：“已超出确认收货期限，不允许入库！”
         */
        BasicCpQueryService cpQueryService = ApplicationContextHandle.getBean(BasicCpQueryService.class);
        StoreInfoQueryRequest request = new StoreInfoQueryRequest();
        request.setPhyId(sgBPhyInNotices.getCpCPhyWarehouseId());
        HashMap<Long, List<CpCStore>> storeInfoByPhyId = new HashMap<>();
        try {
            storeInfoByPhyId = cpQueryService.getStoreInfoByPhyId(request);
        } catch (Exception e) {
            AssertUtils.logAndThrow("查询逻辑店仓信息异常！" + e.getMessage(), context.getLocale());
        }

        List<CpCStore> cpCStores = storeInfoByPhyId.get(sgBPhyInNotices.getCpCPhyWarehouseId());
        AssertUtils.isTrue(CollectionUtils.isNotEmpty(cpCStores), "未查到实体仓对应的逻辑仓！", context.getLocale());

        Long storeConfirmTime = cpCStores.get(0).getStoreConfirmTime();
        if (storeConfirmTime != null) {
            Date inTime = sgBPhyInNotices.getInTime();
            LocalDate localDate1 = inTime.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            Date before = java.sql.Date.valueOf(localDate1);

            LocalDate localDate2 = new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            Date after = java.sql.Date.valueOf(localDate2);

            Calendar aCalendar = Calendar.getInstance();
            aCalendar.setTime(before);
            int beforeYear = aCalendar.get(Calendar.YEAR);
            int day1 = aCalendar.get(Calendar.DAY_OF_YEAR);
            aCalendar.setTime(after);
            int afterYear = aCalendar.get(Calendar.YEAR);
            int day2 = aCalendar.get(Calendar.DAY_OF_YEAR);

            if (beforeYear == afterYear && day2 - day1 > storeConfirmTime) {
                AssertUtils.logAndThrow("已超出确认收货期限，不允许入库！", context.getLocale());
            }
        }
    }
}
