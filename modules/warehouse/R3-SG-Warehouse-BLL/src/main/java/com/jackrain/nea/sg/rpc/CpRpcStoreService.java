package com.jackrain.nea.sg.rpc;

import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.cpext.api.CpStoreQueryCmd;
import com.jackrain.nea.cpext.model.table.CpCStore;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 * 逻辑仓查询
 *
 * @author: xiWen.z
 * create at: 2019/12/21 0021
 */
@Slf4j
@Component
public class CpRpcStoreService {

    @Reference(group = "cp-ext", version = "1.0")
    CpStoreQueryCmd cpStoreQueryCmd;


    /**
     * 查询逻辑仓档案
     *
     * @param id
     * @return
     */
    public ValueHolderV14<CpCStore> queryCpStoreById(Long id) {
        ValueHolderV14 vh = new ValueHolderV14<>();
        try {
            CpCStore cpCStore = cpStoreQueryCmd.getCpCStoreById(id);
            if (Objects.isNull(cpCStore)) {
                vh.setCode(ResultCode.FAIL);
                return vh;
            }
            vh.setData(cpCStore);
            vh.setCode(ResultCode.SUCCESS);
            return vh;
        } catch (Exception e) {
            e.printStackTrace();
            log.error(this.getClass().getName() + " Rpc查询逻辑仓档案异常", e.getMessage());
            vh.setCode(ResultCode.FAIL);
            vh.setMessage(" Rpc查询逻辑仓档案异常" + e.getMessage());
            return vh;
        }
    }

    /**
     * 查询主逻辑仓档案
     *
     * @param id
     * @return
     */
    public ValueHolderV14<CpCStore> queryMainStoreByWareHouseId(Long id) {
        ValueHolderV14 vh = new ValueHolderV14<>();
        try {
            ValueHolderV14<CpCStore> cpCStore = cpStoreQueryCmd.queryMainStoreByWareHouseId(id);
            if (Objects.isNull(cpCStore)) {
                vh.setCode(ResultCode.FAIL);
                return vh;
            }
            vh.setData(cpCStore.getData());
            vh.setCode(ResultCode.SUCCESS);
            return vh;
        } catch (Exception e) {
            e.printStackTrace();
            log.error(this.getClass().getName() + " Rpc查询主逻辑仓档案异常", e.getMessage());
            vh.setCode(ResultCode.FAIL);
            vh.setMessage(" Rpc查询主逻辑仓档案异常" + e.getMessage());
            return vh;
        }
    }
}
