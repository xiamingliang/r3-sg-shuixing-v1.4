package com.jackrain.nea.sg.in.services;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.ip.api.qimen.QimenOrderCancelCmd;
import com.jackrain.nea.ip.model.qimen.QimenOrderCancelModel;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.mapper.CpCPhyWarehouseMapper;
import com.jackrain.nea.sg.basic.model.table.CpCPhyWarehouse;
import com.jackrain.nea.sg.in.common.SgInNoticeConstants;
import com.jackrain.nea.sg.in.mapper.SgBPhyInNoticesMapper;
import com.jackrain.nea.sg.in.model.request.SgPhyInNoticesVoidReqeust;
import com.jackrain.nea.sg.in.model.table.SgBPhyInNotices;
import com.jackrain.nea.sg.out.utils.SgWarehouseESUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author: ChenChen
 * @Description:作废入库通知单服务
 * @Date: Create in 14:50 2019/5/5
 */
@Service
public class SgPhyInNoticesVoidService {

    @Autowired
    private SgWarehouseESUtils warehouseESUtils;

    public ValueHolderV14 voidSgBPhyInNotices(SgPhyInNoticesVoidReqeust sgPhyInNoticesVoidReqeust) {
        String resultSaveRequestParms = checkParams(sgPhyInNoticesVoidReqeust);
        ValueHolderV14 result = new ValueHolderV14();
        if (!StringUtils.isEmpty(resultSaveRequestParms)) {
            result.setMessage(resultSaveRequestParms);
            result.setCode(ResultCode.FAIL);
            return result;
        }

        SgBPhyInNoticesMapper noticesMapper = ApplicationContextHandle.getBean(SgBPhyInNoticesMapper.class);
        SgBPhyInNotices notices;
        if (sgPhyInNoticesVoidReqeust.getId() != null) {
            notices = noticesMapper.selectById(sgPhyInNoticesVoidReqeust.getId());
        } else {
            QueryWrapper queryWrapper = new QueryWrapper<SgBPhyInNotices>();
            queryWrapper.eq("source_bill_id", sgPhyInNoticesVoidReqeust.getSourceBillId());
            queryWrapper.eq("source_bill_type", sgPhyInNoticesVoidReqeust.getSourceBillType());
            queryWrapper.eq("isactive", SgConstants.IS_ACTIVE_Y);
            notices = noticesMapper.selectOne(queryWrapper);
        }
        if (notices == null) {
            result.setMessage(Resources.getMessage("没用找到对应的入库通知单！",
                    sgPhyInNoticesVoidReqeust.getLoginUser().getLocale()));
            result.setCode(ResultCode.FAIL);
            return result;
        }
        if (notices.getBillStatus() != null && notices.getBillStatus() != SgInNoticeConstants.BILL_STATUS_IN_PENDING) {
            result.setMessage(Resources.getMessage("已经入库不能作废！",
                    sgPhyInNoticesVoidReqeust.getLoginUser().getLocale()));
            result.setCode(ResultCode.FAIL);
            return result;
        }
        if (SgInNoticeConstants.WMS_UPLOAD_STATUTS_UPLOADING == notices.getWmsStatus()) {
            result.setMessage(Resources.getMessage("此单传WMS中,请稍后重试！",
                    sgPhyInNoticesVoidReqeust.getLoginUser().getLocale()));
            result.setCode(ResultCode.FAIL);
            return result;
        } else if (SgInNoticeConstants.WMS_UPLOAD_STATUTS_SUCCESS == notices.getWmsStatus()) {
            ValueHolderV14 wmsHolder = sendWMSCancel(notices, sgPhyInNoticesVoidReqeust.getLoginUser());
            if (wmsHolder.getCode() == ResultCode.FAIL) {
                return wmsHolder;
            }
        }
        SgBPhyInNotices update = new SgBPhyInNotices();
        update.setId(notices.getId());
        update.setIsactive(SgConstants.IS_ACTIVE_N);
        update.setBillStatus(SgInNoticeConstants.BILL_STATUS_IN_VOID);
        if (noticesMapper.updateById(update) > 0) {
            result.setCode(ResultCode.SUCCESS);
            result.setMessage("success");
            //推送es
            warehouseESUtils.pushESByInNotices(update.getId(), false, false, null, noticesMapper, null);
        } else {
            result.setMessage(Resources.getMessage("插入是失败！",
                    sgPhyInNoticesVoidReqeust.getLoginUser().getLocale()));
            result.setCode(ResultCode.FAIL);
        }
        return result;
    }


    public ValueHolderV14 sendWMSCancel(SgBPhyInNotices notices, User loginUser) {
        try {
            QimenOrderCancelCmd orderCancelCmd = (QimenOrderCancelCmd) ReferenceUtil.refer(
                    ApplicationContextHandle.getApplicationContext(), QimenOrderCancelCmd.class.getName(), "ip", "1.4.0");
            QimenOrderCancelModel model = new QimenOrderCancelModel();
            CpCPhyWarehouseMapper warehouseMapper = ApplicationContextHandle.getBean(CpCPhyWarehouseMapper.class);
            CpCPhyWarehouse cpCPhyWarehouse = warehouseMapper.selectById(notices.getCpCPhyWarehouseId());
            if (cpCPhyWarehouse != null) {
                model.setCustomerId(cpCPhyWarehouse.getWmsAccount());
                model.setWarehouseCode(cpCPhyWarehouse.getWmsWarehouseCode());
            }
            model.setOrderId(notices.getWmsBillNo());
            model.setOwnerCode(notices.getGoodsOwner());
            model.setOrderCode(notices.getBillNo());

            // model.setOrderId();
            if (notices.getSourceBillType() == SgConstantsIF.BILL_TYPE_PUR) {
                model.setOrderType("CGRK");
            } else if (notices.getSourceBillType() == SgConstantsIF.BILL_TYPE_SALE_REF) {
                model.setOrderType("B2BRK");
            } else if (notices.getSourceBillType() == SgConstantsIF.BILL_TYPE_TRANSFER) {
                model.setOrderType("DBRK");
            } else {
                model.setOrderType("");
            }
            model.setOperateUser(loginUser);
            return orderCancelCmd.cancelQimenOrder(model);
        } catch (Exception ex) {


            ValueHolderV14<Long> result = new ValueHolderV14();
            result.setCode(ResultCode.FAIL);
            result.setMessage(ex.getMessage());
            return result;
        }

    }

    /**
     * 参数校验
     */
    private String checkParams(SgPhyInNoticesVoidReqeust sgPhyInNoticesVoidReqeust) {
        ValueHolderV14 result = new ValueHolderV14();
        StringBuilder sb = new StringBuilder();
        if (sgPhyInNoticesVoidReqeust.getId() == null) {
            if (sgPhyInNoticesVoidReqeust.getSourceBillId() == null) {
                sb.append("来源单据ID不能为空");
            }
            if (sgPhyInNoticesVoidReqeust.getSourceBillType() == null) {
                sb.append("来源单据类型不能为空");
            }
        }
        return sb.toString();
    }
}
