package com.jackrain.nea.sg.task;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.in.common.SgInConstants;
import com.jackrain.nea.sg.in.mapper.SgBPhyInNoticesItemMapper;
import com.jackrain.nea.sg.in.mapper.SgBPhyInNoticesMapper;
import com.jackrain.nea.sg.in.mapper.SgBPhyInResultItemMapper;
import com.jackrain.nea.sg.in.mapper.SgBPhyInResultMapper;
import com.jackrain.nea.sg.in.model.table.SgBPhyInNotices;
import com.jackrain.nea.sg.in.model.table.SgBPhyInNoticesItem;
import com.jackrain.nea.sg.in.model.table.SgBPhyInResult;
import com.jackrain.nea.sg.in.model.table.SgBPhyInResultItem;
import com.jackrain.nea.sg.out.common.SgOutConstants;
import com.jackrain.nea.sg.out.mapper.SgBPhyOutNoticesItemMapper;
import com.jackrain.nea.sg.out.mapper.SgBPhyOutNoticesMapper;
import com.jackrain.nea.sg.out.mapper.SgBPhyOutResultItemMapper;
import com.jackrain.nea.sg.out.mapper.SgBPhyOutResultMapper;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutNotices;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutNoticesItem;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutResult;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutResultItem;
import com.jackrain.nea.sg.phyadjust.common.SgPhyAdjustConstants;
import com.jackrain.nea.sg.phyadjust.mapper.SgBPhyAdjustItemMapper;
import com.jackrain.nea.sg.phyadjust.mapper.SgBPhyAdjustMapper;
import com.jackrain.nea.sg.phyadjust.model.table.SgBPhyAdjust;
import com.jackrain.nea.sg.phyadjust.model.table.SgBPhyAdjustItem;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author 舒威
 * @since 2019/8/2
 * create at : 2019/8/2 15:54
 */
@Slf4j
@Component
public class SgWarehouseSinglePushESTaskService {


    /**
     * 刷新实体仓相关业务es
     */
    public ValueHolderV14 pushSingleWarehouseES(JSONObject params) {
        if (log.isDebugEnabled()) {
            log.debug("Start Single Push ES Warehouse. ReceiveParams:params:{};", JSONObject.toJSONString(params));
        }

        long startTime = System.currentTimeMillis();
        ValueHolderV14 result = new ValueHolderV14(ResultCode.SUCCESS, "message");
        if (params != null) {
            String table = params.getString("table");
            JSONArray ids = params.getJSONArray("ids");
            if (CollectionUtils.isNotEmpty(ids)) {
                switch (table) {
                    case SgConstants.SG_B_PHY_IN_NOTICES:
                        pushSingleESInNotices(ids);
                        break;
                    case SgConstants.SG_B_PHY_IN_RESULT:
                        pushSingleESInResults(ids);
                        break;
                    case SgConstants.SG_B_PHY_OUT_NOTICES:
                        pushSingleESOutNotices(ids);
                        break;
                    case SgConstants.SG_B_PHY_OUT_RESULT:
                        pushSingleESOutResults(ids);
                        break;
                    case SgConstants.SG_B_PHY_ADJUST:
                        pushSingleESPhyAdjust(ids);
                        break;
                    default:
                        log.debug(table + "暂不支持推送es!");
                }
            }
        }

        if (log.isDebugEnabled()) {
            log.debug("Finish  Single Push ES Warehouse: spend time:{}ms;", System.currentTimeMillis() - startTime);
        }

        return result;
    }

    /**
     * 刷新库存调整单es
     */
    private void pushSingleESPhyAdjust(JSONArray ids) {
        if (log.isDebugEnabled()) {
            log.debug("Start Single Push ES PhyAdjust");
        }

        long startTime = System.currentTimeMillis();
        SgBPhyAdjustMapper adjustMapper = ApplicationContextHandle.getBean(SgBPhyAdjustMapper.class);
        SgBPhyAdjustItemMapper adjustItemMapper = ApplicationContextHandle.getBean(SgBPhyAdjustItemMapper.class);
        String index = SgConstants.SG_B_PHY_ADJUST;
        String childType = SgConstants.SG_B_PHY_ADJUST_ITEM;

        //批量推送主表
        List<SgBPhyAdjust> adjusts = adjustMapper.selectList(new QueryWrapper<SgBPhyAdjust>().lambda().in(SgBPhyAdjust::getId, ids));
        StorageESUtils.singlePushES(adjusts, ids, index, null, null, SgBPhyAdjust.class, SgBPhyAdjustItem.class);


        //批量推送明细表
        List<SgBPhyAdjustItem> adjustitems = adjustItemMapper.selectList(new QueryWrapper<SgBPhyAdjustItem>().lambda().in(SgBPhyAdjustItem::getSgBPhyAdjustId, ids));
        StorageESUtils.singlePushES(adjustitems, ids, index, childType, SgPhyAdjustConstants.SG_B_PHY_ADJUST_ID.toUpperCase(), SgBPhyAdjust.class, SgBPhyAdjustItem.class);

        if (log.isDebugEnabled()) {
            log.debug("Finish  Single Push ES PhyAdjust: spend time:{}ms;", System.currentTimeMillis() - startTime);
        }
    }


    /**
     * 刷新出库结果单es
     */
    private void pushSingleESOutResults(JSONArray ids) {
        if (log.isDebugEnabled()) {
            log.debug("Start Single Push ES OutResults");
        }

        long startTime = System.currentTimeMillis();
        SgBPhyOutResultMapper outResultMapper = ApplicationContextHandle.getBean(SgBPhyOutResultMapper.class);
        SgBPhyOutResultItemMapper outResultItemMapper = ApplicationContextHandle.getBean(SgBPhyOutResultItemMapper.class);
        String index = SgConstants.SG_B_PHY_OUT_RESULT;
        String childType = SgConstants.SG_B_PHY_OUT_RESULT_ITEM;


        //批量推送主表
        List<SgBPhyOutResult> outResults = outResultMapper.selectList(new QueryWrapper<SgBPhyOutResult>().lambda().in(SgBPhyOutResult::getId, ids));
        StorageESUtils.singlePushES(outResults, ids, index, null, null, SgBPhyOutResult.class, SgBPhyOutResultItem.class);


        //批量推送明细表
        List<SgBPhyOutResultItem> outResultitems = outResultItemMapper.selectList(new QueryWrapper<SgBPhyOutResultItem>().lambda()
                .in(SgBPhyOutResultItem::getSgBPhyOutResultId, ids));
        StorageESUtils.singlePushES(outResultitems, ids, index, childType, SgOutConstants.OUT_RESULT_PAREN_FIELD.toUpperCase(), SgBPhyOutResult.class, SgBPhyOutResultItem.class);

        if (log.isDebugEnabled()) {
            log.debug("Finish  Single Push ES OutResults: spend time:{}ms;", System.currentTimeMillis() - startTime);
        }
    }

    /**
     * 刷新出库通知单es
     */
    private void pushSingleESOutNotices(JSONArray ids) {
        if (log.isDebugEnabled()) {
            log.debug("Start Single Push ES OutNotices");
        }

        long startTime = System.currentTimeMillis();
        SgBPhyOutNoticesMapper noticesMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesMapper.class);
        SgBPhyOutNoticesItemMapper noticesItemMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesItemMapper.class);
        String index = SgConstants.SG_B_PHY_OUT_NOTICES;
        String childType = SgConstants.SG_B_PHY_OUT_NOTICES_ITEM;

        //批量推送主表
        List<SgBPhyOutNotices> notices = noticesMapper.selectList(new QueryWrapper<SgBPhyOutNotices>().lambda().in(SgBPhyOutNotices::getId, ids));
        StorageESUtils.singlePushES(notices, ids, index, null, null, SgBPhyOutNotices.class, SgBPhyOutNoticesItem.class);

        //批量推送明细表
        List<SgBPhyOutNoticesItem> outNoticesItems = noticesItemMapper.selectList(new QueryWrapper<SgBPhyOutNoticesItem>().lambda()
                .in(SgBPhyOutNoticesItem::getSgBPhyOutNoticesId, ids));
        StorageESUtils.singlePushES(outNoticesItems, ids, index, childType, SgOutConstants.OUT_NOTICES_PAREN_FIELD.toUpperCase(), SgBPhyOutNotices.class, SgBPhyOutNoticesItem.class);

        if (log.isDebugEnabled()) {
            log.debug("Finish  Single Push ES OutNotices: spend time:{}ms;", System.currentTimeMillis() - startTime);
        }
    }

    /**
     * 刷新入库结果单es
     */
    private void pushSingleESInResults(JSONArray ids) {
        if (log.isDebugEnabled()) {
            log.debug("Start Single Push ES InResults");
        }

        long startTime = System.currentTimeMillis();
        SgBPhyInResultMapper inResultMapper = ApplicationContextHandle.getBean(SgBPhyInResultMapper.class);
        SgBPhyInResultItemMapper inResultItemMapper = ApplicationContextHandle.getBean(SgBPhyInResultItemMapper.class);
        String index = SgConstants.SG_B_PHY_IN_RESULT;
        String childType = SgConstants.SG_B_PHY_IN_RESULT_ITEM;

        //批量推送主表
        List<SgBPhyInResult> results = inResultMapper.selectList(new QueryWrapper<SgBPhyInResult>().lambda().in(SgBPhyInResult::getId, ids));
        StorageESUtils.singlePushES(results, ids, index, null, null, SgBPhyInResult.class, SgBPhyInResultItem.class);

        //批量推送明细表
        List<SgBPhyInResultItem> resultitems = inResultItemMapper.selectList(new QueryWrapper<SgBPhyInResultItem>().lambda()
                .in(SgBPhyInResultItem::getSgBPhyInResultId, ids));
        StorageESUtils.singlePushES(resultitems, ids, index, childType, SgInConstants.SG_B_PHY_IN_RESULT_ID.toUpperCase(), SgBPhyInResult.class, SgBPhyInResultItem.class);

        if (log.isDebugEnabled()) {
            log.debug("Finish  Single Push ES InResults: spend time:{}ms;", System.currentTimeMillis() - startTime);
        }
    }

    /**
     * 刷新入库通知单单es
     */
    private void pushSingleESInNotices(JSONArray ids) {
        if (log.isDebugEnabled()) {
            log.debug("Start Single Push ES InNotices");
        }

        long startTime = System.currentTimeMillis();
        SgBPhyInNoticesMapper inNoticesMapper = ApplicationContextHandle.getBean(SgBPhyInNoticesMapper.class);
        SgBPhyInNoticesItemMapper inNoticesItemMapper = ApplicationContextHandle.getBean(SgBPhyInNoticesItemMapper.class);
        String index = SgConstants.SG_B_PHY_IN_NOTICES;
        String childType = SgConstants.SG_B_PHY_IN_NOTICES_ITEM;

        //批量推送主表
        List<SgBPhyInNotices> notices = inNoticesMapper.selectList(new QueryWrapper<SgBPhyInNotices>().lambda().in(SgBPhyInNotices::getId, ids));
        StorageESUtils.singlePushES(notices, ids, index, null, null, SgBPhyInNotices.class, SgBPhyInNoticesItem.class);

        //批量推送明细表
        List<SgBPhyInNoticesItem> inNoticesItems = inNoticesItemMapper.selectList(new QueryWrapper<SgBPhyInNoticesItem>().lambda()
                .in(SgBPhyInNoticesItem::getSgBPhyInNoticesId, ids));
        StorageESUtils.singlePushES(inNoticesItems, ids, index, childType, SgInConstants.SG_B_PHY_IN_NOTICES_ID.toUpperCase(), SgBPhyInNotices.class, SgBPhyInNoticesItem.class);

        if (log.isDebugEnabled()) {
            log.debug("Finish  Single Push ES InNotices: spend time:{}ms;", System.currentTimeMillis() - startTime);
        }
    }

}
