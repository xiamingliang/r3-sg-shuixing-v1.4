package com.jackrain.nea.sg.in.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.in.common.SgInNoticeConstants;
import com.jackrain.nea.sg.in.mapper.SgBPhyInNoticesMapper;
import com.jackrain.nea.sg.in.model.request.NoticesCallBackRequest;
import com.jackrain.nea.sg.in.model.table.SgBPhyInNotices;
import com.jackrain.nea.sg.out.utils.SgWarehouseESUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

/**
 * 入库通知单上传WMS回调
 *
 * @author jiang.cj
 * @since 2019-05-08
 * create at : 2019-05-08
 */
@Slf4j
@Component
public class SgPhyInNoticesCallBackService {

    @Autowired
    private SgWarehouseESUtils warehouseESUtils;

    public ValueHolderV14<NoticesCallBackRequest> callBackSgPhyInNotices(List<NoticesCallBackRequest> noticesCallBackList, User user) {
        if (log.isDebugEnabled()) {
            log.debug("Start SgPhyInNoticesCallBackService.callBackSgPhyInNotices. ReceiveParams:noticesCallBackList="
                    + JSONObject.toJSONString(noticesCallBackList) + ";");
        }
        ValueHolderV14 result = new ValueHolderV14();
        SgBPhyInNoticesMapper noticesMapper = ApplicationContextHandle.getBean(SgBPhyInNoticesMapper.class);
        noticesCallBackList.forEach(request -> {
            //根据单据编号查询入库通知单
            QueryWrapper<SgBPhyInNotices> queryWrapper = new QueryWrapper();
            queryWrapper.eq("bill_no", request.getOrderNo());
            SgBPhyInNotices inNotices = noticesMapper.selectOne(queryWrapper);
            if (inNotices != null) {
                SgBPhyInNotices notice = new SgBPhyInNotices();
                notice.setId(inNotices.getId());
                StorageESUtils.setBModelDefalutDataByUpdate(notice, user);
                notice.setModifierename(user.getEname());
                if (request.getCode() == ResultCode.SUCCESS) {
                    //如果上传成功,则更新入库通知单【传WMS状态】为已传WMS
                    notice.setWmsStatus((long) SgInNoticeConstants.WMS_UPLOAD_STATUTS_SUCCESS);
                    notice.setPassWmsTime(new Date());
                    notice.setWmsFailCount(0L);
                    notice.setWmsBillNo(request.getEntryOrderId());
                    noticesMapper.update(notice, new UpdateWrapper<SgBPhyInNotices>().lambda()
                            .eq(SgBPhyInNotices::getId, inNotices.getId())
                            .set(SgBPhyInNotices::getWmsFailReason, null));
                } else {
                    //如果上传失败,更新入库通知单【传WMS状态】为传WMS失败,【失败次数】+1
                    notice.setWmsStatus((long) SgInNoticeConstants.WMS_UPLOAD_STATUTS_FAIL);
                    Long faliCount = inNotices.getWmsFailCount() != null ? inNotices.getWmsFailCount() : 0;
                    //2019-09-06 截取备注等超长字段  - 失败原因
                    notice.setWmsFailReason(StorageESUtils.strSubString(request.getMessage(), SgConstants.SG_COMMON_STRING_SIZE));
                    notice.setWmsFailCount(faliCount + 1);
                    noticesMapper.updateById(notice);
                }
                //推送es
                warehouseESUtils.pushESByInNotices(notice.getId(), false, false, null, noticesMapper, null);
            }

        });
        result.setCode(ResultCode.SUCCESS);
        result.setMessage("回调成功!");
        return result;
    }
}
