package com.jackrain.nea.sg.in.services;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.in.common.SgInConstants;
import com.jackrain.nea.sg.in.common.SgInNoticeConstants;
import com.jackrain.nea.sg.in.mapper.SgBPhyInNoticesImpItemMapper;
import com.jackrain.nea.sg.in.mapper.SgBPhyInNoticesItemMapper;
import com.jackrain.nea.sg.in.mapper.SgBPhyInNoticesMapper;
import com.jackrain.nea.sg.in.mapper.SgBPhyInNoticesTeusItemMapper;
import com.jackrain.nea.sg.in.model.request.SgPhyInNoticesSelectBySourceRequest;
import com.jackrain.nea.sg.in.model.result.SgBPhyInNoticesBoxResult;
import com.jackrain.nea.sg.in.model.result.SgBPhyInNoticesResult;
import com.jackrain.nea.sg.in.model.table.SgBPhyInNotices;
import com.jackrain.nea.sg.in.model.table.SgBPhyInNoticesImpItem;
import com.jackrain.nea.sg.in.model.table.SgBPhyInNoticesItem;
import com.jackrain.nea.sg.in.model.table.SgBPhyInNoticesTeusItem;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Author: ChenChen
 * @Description:
 * @Date: Create in 19:54 2019/5/7
 */
@Slf4j
@Component
public class SgPhyInNoticesSelectService {

    public ValueHolderV14<SgBPhyInNoticesResult> selectSgBPhyInNoticesByBillNo(String billNo) {
        ValueHolderV14 result = new ValueHolderV14();
        if (billNo == null) {
            result.setMessage("插入失败，当前订单号已经存在！");
            result.setCode(ResultCode.FAIL);
        } else {
            QueryWrapper queryWrapper = new QueryWrapper<SgBPhyInNotices>();
            queryWrapper.eq("bill_no", billNo);
            SgBPhyInNoticesMapper noticesMapper = ApplicationContextHandle.getBean(SgBPhyInNoticesMapper.class);
            SgBPhyInNoticesItemMapper noticesItemMMapper = ApplicationContextHandle.getBean(SgBPhyInNoticesItemMapper.class);
            SgBPhyInNotices notices = noticesMapper.selectOne(queryWrapper);
            if (notices == null) {
                result.setMessage("未找到对应的入库通知单！");
                result.setCode(ResultCode.FAIL);
            } else {
                SgBPhyInNoticesResult sgBPhyInNoticesResult = new SgBPhyInNoticesResult();
                sgBPhyInNoticesResult.setNotices(notices);
                QueryWrapper queryItemWrapper = new QueryWrapper<SgBPhyInNoticesItem>();
                queryItemWrapper.eq("sg_b_phy_in_notices_id", notices.getId());
                sgBPhyInNoticesResult.setItemList(noticesItemMMapper.selectList(queryItemWrapper));
                result.setData(sgBPhyInNoticesResult);
                result.setCode(ResultCode.SUCCESS);
            }
        }

        return result;
    }

    /**
     * 大货入库查询
     *
     * @return
     */
    public ValueHolderV14<List<SgBPhyInNoticesResult>> selectByBigCrgo() {
        ValueHolderV14<List<SgBPhyInNoticesResult>> result = new ValueHolderV14();
        QueryWrapper queryWrapper = new QueryWrapper<SgBPhyInNotices>();
        queryWrapper.eq("bill_status", SgInNoticeConstants.BILL_STATUS_IN_PENDING);
        queryWrapper.eq("in_type", SgInConstants.IN_TYPE_BIG_CARGO);
        queryWrapper.eq("is_pass_wms", SgConstants.IS_YES);
        queryWrapper.in("source_bill_type", new Integer[]{
                SgConstantsIF.BILL_TYPE_SALE_REF, SgConstantsIF.BILL_TYPE_PUR, SgConstantsIF.BILL_TYPE_TRANSFER
        });
        Integer[] wmsStatusArr = new Integer[]{
                SgInNoticeConstants.WMS_UPLOAD_STATUTS_NO, SgInNoticeConstants.WMS_UPLOAD_STATUTS_FAIL};

        queryWrapper.in("wms_status", wmsStatusArr);
        queryWrapper.le(true, "wms_fail_count", 5);
        SgBPhyInNoticesMapper noticesMapper = ApplicationContextHandle.getBean(SgBPhyInNoticesMapper.class);
        List<SgBPhyInNotices> noticesList = noticesMapper.selectList(queryWrapper);
        if (noticesList != null && noticesList.size() > 0) {
            result.setData(getSgBPhyInNoticesResult(noticesList));
        }
        result.setCode(ResultCode.SUCCESS);
        result.setMessage("success");
        return result;
    }

    /**
     * 大货入库查询（支持箱功能）
     *
     * @return
     */
    public ValueHolderV14<List<SgBPhyInNoticesBoxResult>> selectBigCrgoBox() {
        ValueHolderV14<List<SgBPhyInNoticesBoxResult>> listResult = new ValueHolderV14();

        SgBPhyInNoticesMapper noticesMapper = ApplicationContextHandle.getBean(SgBPhyInNoticesMapper.class);
        List<SgBPhyInNotices> noticesList = noticesMapper.selectList(new QueryWrapper<SgBPhyInNotices>().lambda()
                .eq(SgBPhyInNotices::getBillStatus, SgInNoticeConstants.BILL_STATUS_IN_PENDING)
                .eq(SgBPhyInNotices::getInType, SgInConstants.IN_TYPE_BIG_CARGO)
                .eq(SgBPhyInNotices::getIsPassWms, SgConstants.IS_YES)
                .in(SgBPhyInNotices::getSourceBillType, new Integer[]{
                        SgConstantsIF.BILL_TYPE_SALE_REF, SgConstantsIF.BILL_TYPE_TRANSFER
                })
                .in(SgBPhyInNotices::getWmsStatus, new Integer[]{
                        SgInNoticeConstants.WMS_UPLOAD_STATUTS_NO, SgInNoticeConstants.WMS_UPLOAD_STATUTS_FAIL})
                .le(SgBPhyInNotices::getWmsFailCount, 5));
        if (noticesList != null && noticesList.size() > 0) {
            listResult.setData(getSgBPhyInNoticesItemResultBox(noticesList));
        }
        listResult.setCode(ResultCode.SUCCESS);
        listResult.setMessage("success");
        return listResult;

    }

    public ValueHolderV14<List<SgBPhyInNoticesResult>> selectBySource(SgPhyInNoticesSelectBySourceRequest request) {
        ValueHolderV14<List<SgBPhyInNoticesResult>> result = new ValueHolderV14();
        String resultSaveRequestParms = checkParams(request);
        if (!StringUtils.isEmpty(resultSaveRequestParms)) {
            result.setMessage(resultSaveRequestParms);
            result.setCode(ResultCode.FAIL);
            return result;
        }
        QueryWrapper queryWrapper = new QueryWrapper<SgBPhyInNotices>();
        queryWrapper.eq("source_bill_id", request.getSourceBillId());
        queryWrapper.eq("source_bill_type", request.getSourceBillType());
        queryWrapper.eq("isactive", SgConstants.IS_ACTIVE_Y);
        SgBPhyInNoticesMapper noticesMapper = ApplicationContextHandle.getBean(SgBPhyInNoticesMapper.class);
        List<SgBPhyInNotices> noticesList = noticesMapper.selectList(queryWrapper);
        if (noticesList != null && noticesList.size() > 0) {
            result.setData(getSgBPhyInNoticesResult(noticesList));
            result.setCode(ResultCode.SUCCESS);
            result.setMessage("success");
        } else {
            result.setCode(ResultCode.FAIL);
            result.setMessage(Resources.getMessage("入库通知单不存在！", request.getLoginUser().getLocale()));
        }
        return result;
    }

    public List<SgBPhyInNoticesResult> getSgBPhyInNoticesResult(List<SgBPhyInNotices> noticesList) {
        SgBPhyInNoticesItemMapper noticesItemMMapper = ApplicationContextHandle.getBean(SgBPhyInNoticesItemMapper.class);
        List<SgBPhyInNoticesResult> noticesResultList = new ArrayList<>();
        for (SgBPhyInNotices notices : noticesList) {
            SgBPhyInNoticesResult sgBPhyInNoticesResult = new SgBPhyInNoticesResult();
            sgBPhyInNoticesResult.setNotices(notices);
            sgBPhyInNoticesResult.setItemList(noticesItemMMapper.selectList(new QueryWrapper<SgBPhyInNoticesItem>().lambda().eq(SgBPhyInNoticesItem::getSgBPhyInNoticesId, notices.getId())));
            noticesResultList.add(sgBPhyInNoticesResult);
        }
        return noticesResultList;
    }

    //根据入库单id获取入库单明细（支持箱功能）
    public List<SgBPhyInNoticesBoxResult> getSgBPhyInNoticesItemResultBox(List<SgBPhyInNotices> noticesList) {
        SgBPhyInNoticesImpItemMapper noticesImpItemMapper = ApplicationContextHandle.getBean(SgBPhyInNoticesImpItemMapper.class);
        SgBPhyInNoticesTeusItemMapper sgBPhyInNoticesTeusItemMapper = ApplicationContextHandle.getBean(SgBPhyInNoticesTeusItemMapper.class);
        List<SgBPhyInNoticesBoxResult> noticesResultList = new ArrayList<>();
        for (SgBPhyInNotices notices : noticesList) {
            SgBPhyInNoticesBoxResult sgBPhyInNoticesResult = new SgBPhyInNoticesBoxResult();
            sgBPhyInNoticesResult.setNotices(notices);
            //获取录入明细
            List<SgBPhyInNoticesImpItem> sgBPhyInNoticesImpItems=noticesImpItemMapper.selectList(new QueryWrapper<SgBPhyInNoticesImpItem>().lambda()
            .eq(SgBPhyInNoticesImpItem::getSgBPhyInNoticesId,notices.getId()));

            //获取箱内明细
            List<SgBPhyInNoticesTeusItem> sgBPhyInNoticesTeusItems = sgBPhyInNoticesTeusItemMapper.selectList(new QueryWrapper<SgBPhyInNoticesTeusItem>().lambda()
            .eq(SgBPhyInNoticesTeusItem::getSgBPhyInNoticesId,notices.getId()));
            //处理明细信息
            List<SgBPhyInNoticesImpItem> sgBPhyInNoticesImpItemsAll = new ArrayList();
            for (SgBPhyInNoticesImpItem sgBPhyInNoticesImpItem : sgBPhyInNoticesImpItems) {

                if (sgBPhyInNoticesImpItem.getPsCTeusId() == null || sgBPhyInNoticesImpItem.getPsCTeusId() == 0L)//散件
                {
                    sgBPhyInNoticesImpItemsAll.add(sgBPhyInNoticesImpItem);
                } else {//箱明细 从箱内明细获取具体明细信息
                    List<SgBPhyInNoticesTeusItem> sgBPhyInNoticesTeusItembox = sgBPhyInNoticesTeusItems.stream().filter(item -> item.getPsCTeusId().equals(sgBPhyInNoticesImpItem.getPsCTeusId())).collect(Collectors.toList());
                    for (SgBPhyInNoticesTeusItem obj1 : sgBPhyInNoticesTeusItembox) {
                        SgBPhyInNoticesImpItem sgBPhyInNoticesImpItem1 = new SgBPhyInNoticesImpItem();
                        sgBPhyInNoticesImpItem1.setPsCSkuEcode(obj1.getPsCSkuEcode());
                        sgBPhyInNoticesImpItem1.setPsCSkuId(obj1.getPsCSkuId());
                        sgBPhyInNoticesImpItem1.setPsCProEname(obj1.getPsCProEname());
                        sgBPhyInNoticesImpItem1.setPsCTeusEcode(obj1.getPsCTeusEcode());
                        sgBPhyInNoticesImpItem1.setPsCTeusId(obj1.getPsCTeusId());
                        sgBPhyInNoticesImpItem1.setQty(obj1.getQty());
                        sgBPhyInNoticesImpItem1.setId(obj1.getId());
                        sgBPhyInNoticesImpItemsAll.add(sgBPhyInNoticesImpItem1);
                    }
                }
            }
            sgBPhyInNoticesResult.setItemList(sgBPhyInNoticesImpItemsAll);
            noticesResultList.add(sgBPhyInNoticesResult);
        }
        return noticesResultList;
    }

    private String checkParams(SgPhyInNoticesSelectBySourceRequest request) {
        StringBuilder sb = new StringBuilder();
        if (request.getLoginUser() == null) {
            sb.append("用户未登录！");
        }
        if (request.getSourceBillId() == null || request.getSourceBillType() == null) {
            sb.append("来源单据id/来源单据类型不能为空！");
        }
        return sb.toString();

    }

}

