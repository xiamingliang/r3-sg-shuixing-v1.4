package com.jackrain.nea.sg.task;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.jackrain.nea.data.basic.model.request.EwayBillQueryRequest;
import com.jackrain.nea.data.basic.services.BasicStQueryService;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.out.common.SgOutConstants;
import com.jackrain.nea.sg.out.common.SgOutConstantsIF;
import com.jackrain.nea.sg.out.mapper.SgBPhyOutNoticesItemMapper;
import com.jackrain.nea.sg.out.mapper.SgBPhyOutNoticesMapper;
import com.jackrain.nea.sg.out.model.request.SgOutNoticesBatchSendMsgRequest;
import com.jackrain.nea.sg.out.model.result.SgOutNoticesByBillNoResult;
import com.jackrain.nea.sg.out.model.result.SgOutNoticesResult;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutNotices;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutNoticesItem;
import com.jackrain.nea.sg.out.services.SgPhyOutNoticesBatchSaveAndWMSService;
import com.jackrain.nea.sg.out.services.SgPhyOutNoticesSaveFaceService;
import com.jackrain.nea.sg.out.utils.SgWarehouseESUtils;
import com.jackrain.nea.st.model.result.EwayBillResult;
import com.jackrain.nea.st.model.table.StCEwaybillLogisticsDO;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.face.impl.UserImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @description: 获取电子面单补偿任务
 * @author: 郑小龙
 * @date: 2019-08-22 10:02
 */
@Slf4j
@Component
public class SgPhyOutNoticesBatchFaceTaskService {

    @Autowired
    private SgPhyOutNoticesBatchSaveAndWMSService service;

    @Autowired
    private SgWarehouseESUtils warehouseESUtils;

    public void faceTimedTask() {
        //查询间隔5分钟的数据
        int minute = SgOutConstantsIF.FACE_EXEC_MINUTE;//
        int pageSize = SgConstants.SG_COMMON_MAX_INSERT_PAGE_SIZE;
        SgBPhyOutNoticesMapper mapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesMapper.class);
        SgBPhyOutNoticesItemMapper itemMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesItemMapper.class);

        //获取符合条件的总数量
        int outNoticesCount = mapper.selectOutNoticesCount(
                SgOutConstantsIF.OUT_NOTICES_STATUS_WAIT,
                SgOutConstantsIF.IS_ENABLEEWAYBILL_Y,
                SgOutConstantsIF.EWAYBILL_STATUS_SECTION_SUC,
                SgOutConstantsIF.FACE_EXEC_BYNBER,minute);

        if (outNoticesCount <= 0) {
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + " 没有符合条件的电子面单数据 faceTimedTask.nodate");
            }
            return;
        }

        int page = outNoticesCount / pageSize;
        if (outNoticesCount % pageSize != 0) {
            page++;
        }

        //保存电子面单策略
        Map<String, EwayBillResult> mapEway = Maps.newHashMap();
        Map<String, EwayBillQueryRequest> mapRequest = Maps.newHashMap();

        for (int i = 0; i < page; i++) {
            int offsetSize = i * pageSize;//
            List<SgBPhyOutNotices> outNoticesList = mapper.selectListByParam(
                    SgOutConstantsIF.OUT_NOTICES_STATUS_WAIT,
                    SgOutConstantsIF.IS_ENABLEEWAYBILL_Y,
                    SgOutConstantsIF.EWAYBILL_STATUS_SECTION_SUC,
                    SgOutConstantsIF.FACE_EXEC_BYNBER, minute,pageSize, offsetSize);

            //记录 返回订单的信息
            List<SgOutNoticesResult> resultData = Lists.newArrayList();
            //记录 单据编号-通知单
            HashMap<String, SgBPhyOutNotices> noticesStrMap = Maps.newHashMap();
            //保存 出库通知单信息
            HashMap<SgBPhyOutNotices, List<SgBPhyOutNoticesItem>> outNoticesMap = Maps.newHashMap();
            //记录查询电子面单策略失败的出库通知单
            HashMap<SgBPhyOutNotices, List<SgBPhyOutNoticesItem>> outNoticesErrMap = Maps.newHashMap();
            //保存 挂靠店铺信息
            HashMap<Long, Long> shopIdMap = Maps.newHashMap();
            //保存 电子面单类型信息
            HashMap<Long, Integer> etypeMap = Maps.newHashMap();

            //批量查询电子面单策略入参新
            List<EwayBillQueryRequest> requestList = new ArrayList<>();
            for (SgBPhyOutNotices outNotices : outNoticesList) {
                String dicStr = outNotices.getCpCShopId() + "_" + outNotices.getCpCLogisticsId();
                if (!mapRequest.containsKey(dicStr) && !mapEway.containsKey(dicStr)) {
                    EwayBillQueryRequest request = new EwayBillQueryRequest();
                    request.setLogiscId(outNotices.getCpCLogisticsId());
                    request.setShopId(outNotices.getCpCShopId());
                    requestList.add(request);
                    mapRequest.put(dicStr, request);
                }
            }

            try {
                BasicStQueryService basicStQueryService = ApplicationContextHandle.getBean(BasicStQueryService.class);
                Map<String, EwayBillResult> ewayBillMap = basicStQueryService.getEwayBillByShopId(requestList);
                if (log.isDebugEnabled()) {
                    log.debug("SgPhyOutNoticesBatchFaceTaskService.faceTimedTask.batchquery.ewayBillMap=" +
                            JSONObject.toJSONString(ewayBillMap, SerializerFeature.WriteMapNullValue));
                }
                mapEway.putAll(ewayBillMap);
                if (log.isDebugEnabled()) {
                    log.debug("SgPhyOutNoticesBatchFaceTaskService.faceTimedTask.query.mapEway=" +
                            JSONObject.toJSONString(mapEway, SerializerFeature.WriteMapNullValue));
                }
            } catch (Exception e) {
                String messageExtra = AssertUtils.getMessageExtra("批量查询电子面单策略失败!", e);
                log.error(messageExtra);
            }

            for (SgBPhyOutNotices outNotices : outNoticesList) {
                try {
                    //人工预分拣 没有具体类型，根据错误记录是否包含“人工预分拣”来判断是否15分钟执行一次
                    if (StringUtils.isNotEmpty(outNotices.getReserveVarchar01())) {
                        if (outNotices.getEwaybillFailReason().contains("人工预分拣")) {
                            Date d = new Date();
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            Calendar calendar = Calendar.getInstance();
                            calendar.setTime(d);
                            calendar.add(Calendar.MINUTE, -15);//当前时间减去15分钟
                            Date dt1 = sdf.parse(outNotices.getReserveVarchar01());
                            Date dt2 = calendar.getTime();
                            //当前时间减去15分钟小于上次执行时间，则不执行调用电子面单任务
                            if (dt2.getTime() < dt1.getTime()) {
                                continue;
                            }
                        }
                    }

                    String dicStr = outNotices.getCpCShopId() + "_" + outNotices.getCpCLogisticsId();
                    EwayBillResult data = mapEway.get(dicStr);

                    QueryWrapper<SgBPhyOutNoticesItem> queryWrapper = new QueryWrapper<>();
                    queryWrapper.eq(SgOutConstants.OUT_NOTICES_PAREN_FIELD, outNotices.getId());
                    queryWrapper.eq(SgOutConstants.IS_ACTIVE,SgConstants.IS_ACTIVE_Y);
                    List<SgBPhyOutNoticesItem> items = itemMapper.selectList(queryWrapper);

                    if (null == data) {
                        String flag = "电子面单策略为空" + dicStr;
                        flag = StorageESUtils.strSubString(flag, SgConstants.SG_COMMON_REMARK_SIZE);
                        if (log.isDebugEnabled()) {
                            log.debug("SgPhyOutNoticesBatchFaceTaskService.faceTimedTask.outNotices.id="
                                    + outNotices.getBillNo() + "  " + flag);
                        }

                        outNotices.setEwaybillFailReason(flag);
                        outNoticesErrMap.put(outNotices, items);
                        continue;
                    }

                    StCEwaybillLogisticsDO ewaybillLogisticsDO = data.getStCEwaybillLogisticsDO();
                    if (null == ewaybillLogisticsDO) {
                        String flag = "电子面单策略-物流公司信息为空"+dicStr;
                        flag = StorageESUtils.strSubString(flag, SgConstants.SG_COMMON_REMARK_SIZE);

                        if (log.isDebugEnabled()) {
                            log.debug("SgPhyOutNoticesBatchFaceTaskService.faceTimedTask.outNotices.id="
                                    + outNotices.getBillNo() + " " + flag);
                        }
                        outNotices.setEwaybillFailReason(flag);
                        outNoticesErrMap.put(outNotices, items);
                        continue;
                    }

                    if (null == ewaybillLogisticsDO.getEtype()) {
                        String flag = "电子面单策略-电子面单类型为空" + dicStr;
                        flag = StorageESUtils.strSubString(flag, SgConstants.SG_COMMON_REMARK_SIZE);
                        if (log.isDebugEnabled()) {
                            log.debug("SgPhyOutNoticesBatchFaceTaskService.faceTimedTask.outNotices.id="
                                    + outNotices.getBillNo() + " " + flag);
                        }
                        outNotices.setEwaybillFailReason(flag);
                        outNoticesErrMap.put(outNotices, items);
                        continue;
                    }

                    Long shipid = outNotices.getCpCShopId();
                    if (StringUtils.isNotEmpty(ewaybillLogisticsDO.getAffiliatedShop())) {
                        shipid = Long.parseLong(ewaybillLogisticsDO.getAffiliatedShop());
                    }

                    outNoticesMap.put(outNotices, items);
                    shopIdMap.put(outNotices.getId(), shipid);
                    etypeMap.put(outNotices.getId(), ewaybillLogisticsDO.getEtype());
                } catch (Exception e) {
                    log.error(this.getClass().getName() + " faceTimedTask.error01:" + e.getMessage(), e);
                    continue;
                }
            }

            try {
                if (MapUtils.isNotEmpty(outNoticesMap)) {
                    User user = getRootUser();
                    SgOutNoticesBatchSendMsgRequest request = new SgOutNoticesBatchSendMsgRequest();
                    request.setLoginUser(user);
                    request.setOutNoticesMap(outNoticesMap);
                    request.setShopIdMap(shopIdMap);
                    request.setEtypeMap(etypeMap);

                    if (log.isDebugEnabled()) {
                        log.debug(this.getClass().getName() + " electronicFaceSaveList.batchResult" +
                                JSONObject.toJSONString(request));
                    }

                    SgPhyOutNoticesSaveFaceService faceService = ApplicationContextHandle.getBean(SgPhyOutNoticesSaveFaceService.class);
                    faceService.electronicFaceSaveList(request);

                    if (outNoticesErrMap.size()>0) {
                        //更新获取电子面单策略失败的出库通知单
                        Iterator<SgBPhyOutNotices> iterator = outNoticesErrMap.keySet().iterator();
                        while (iterator.hasNext()) {
                            SgBPhyOutNotices notices = iterator.next();
                            int frequency = notices.getReserveBigint03() == null ? 0 : notices.getReserveBigint03().intValue();
                            frequency++;
                            notices.setEwaybillStatus(SgOutConstantsIF.EWAYBILL_STATUS_SECTION_SUC);
                            if (frequency >= SgOutConstantsIF.FACE_EXEC_BYNBER) {
                                notices.setEwaybillStatus(SgOutConstantsIF.EWAYBILL_STATUS_FAIL);
                            }
                            notices.setReserveBigint03(Long.valueOf(frequency));
                            SimpleDateFormat sdfMilli = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
                            notices.setReserveVarchar01(sdfMilli.format(new Date()));

                            SgBPhyOutNotices newOutNotices = new SgBPhyOutNotices();
                            newOutNotices.setId(notices.getId());
                            newOutNotices.setEwaybillFailReason(notices.getEwaybillFailReason());
                            newOutNotices.setEwaybillStatus(notices.getEwaybillStatus());
                            newOutNotices.setReserveBigint03(notices.getReserveBigint03());
                            newOutNotices.setReserveVarchar01(notices.getReserveVarchar01());
                            mapper.updateById(newOutNotices);

                            //推送es
                            warehouseESUtils.pushESByOutNotices(newOutNotices.getId(), false, false, null, mapper, null);
                        }
                    }

                    //把获取电子面单策略失败的出库通知单记录到  通知单信息里面
                    outNoticesMap.putAll(outNoticesErrMap);

                    //批量校验 电子面单、通知单传WMS状态
                    List<SgOutNoticesByBillNoResult> qimenList = service.checkFaceAndWms(outNoticesMap, resultData, noticesStrMap);
                    //RPC调用奇门 并且将失败信息回写订单
                    service.bacthCreateDeliveryOrders(qimenList, resultData, noticesStrMap, user);
                }

            } catch (Exception e) {
                log.error(this.getClass().getName() + " faceTimedTask.error02:" + e.getMessage(), e);
                continue;
            }
        }
    }

    /**
     * 错误数据收集
     *
     * @param objId
     * @param flag
     * @param list
     */
    public void resultMsg(Integer code, Long objId, Long sourceBillId, String flag, List<SgOutNoticesResult> list) {
        SgOutNoticesResult result = new SgOutNoticesResult();
        result.setCode(code);
        result.setId(objId);
        result.setSourceBilId(sourceBillId);
        result.setFlag(flag);
        list.add(result);
    }

    /**
     * 获取root用户
     *
     * @return 返回用户
     */
    private User getRootUser() {
        UserImpl user = new UserImpl();
        user.setId(893);
        user.setName("root");
        user.setEname("系统管理员");
        user.setTruename("系统管理员");
        user.setClientId(37);
        user.setOrgId(27);
        return user;
    }

}