package com.jackrain.nea.sg.unpack.filter;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.sg.basic.utils.StorageUtils;
import com.jackrain.nea.sg.unpack.mapper.SgBTeusUnpackItemMapper;
import com.jackrain.nea.sg.unpack.mapper.SgBTeusUnpackMapper;
import com.jackrain.nea.sg.unpack.model.table.SgBTeusUnpack;
import com.jackrain.nea.sg.unpack.model.table.SgBTeusUnpackItem;
import com.jackrain.nea.tableService.MainTableRecord;
import com.jackrain.nea.tableService.TableServiceContext;
import com.jackrain.nea.tableService.filter.BaseFilter;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author zhoulisnheng
 * @since 2019/11/27 18:04
 * desc: 拆箱单明细删除
 */
@Slf4j
@Component
public class SgUnpackItemDelFilter extends BaseFilter {

    @Override
    public void after(TableServiceContext context, MainTableRecord row) {

        Long objId = row.getId();
        SgBTeusUnpackItemMapper unpackItemMapper = ApplicationContextHandle.getBean(SgBTeusUnpackItemMapper.class);
        SgBTeusUnpackMapper unpackMapper = ApplicationContextHandle.getBean(SgBTeusUnpackMapper.class);

        List<SgBTeusUnpackItem> itemList = unpackItemMapper.selectList(new QueryWrapper<SgBTeusUnpackItem>().lambda()
                .eq(SgBTeusUnpackItem::getSgBTeusUnpackId, objId));
        // 计算总拆箱数
        BigDecimal countUnTeus = itemList.stream()
                .map(SgBTeusUnpackItem::getQty).reduce(BigDecimal.ZERO, (a, b) -> a.add(b));
        SgBTeusUnpack sgBTeusUnpack = new SgBTeusUnpack();
        sgBTeusUnpack.setId(objId);
        sgBTeusUnpack.setQtyTotUnpack(countUnTeus);
        StorageUtils.setBModelDefalutDataByUpdate(sgBTeusUnpack, context.getUser());
        unpackMapper.updateById(sgBTeusUnpack);



    }




}
