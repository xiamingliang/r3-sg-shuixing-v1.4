package com.jackrain.nea.sg.pack.filter;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.data.basic.model.request.SkuInfoQueryRequest;
import com.jackrain.nea.data.basic.services.BasicPsQueryService;
import com.jackrain.nea.ps.api.result.PsCProSkuResult;
import com.jackrain.nea.psext.model.table.PsCTeus;
import com.jackrain.nea.sg.pack.common.SgTeusPackConstants;
import com.jackrain.nea.sg.pack.mapper.SgBTeusPackItemMapper;
import com.jackrain.nea.sg.pack.mapper.SgBTeusPackMapper;
import com.jackrain.nea.sg.pack.model.table.SgBTeusPack;
import com.jackrain.nea.sg.pack.model.table.SgBTeusPackItem;
import com.jackrain.nea.sg.unpack.common.SgUnpackConstants;
import com.jackrain.nea.tableService.*;
import com.jackrain.nea.tableService.filter.BaseFilter;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.utils.AssertUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author zhoulisnheng
 * @since 2019/11/27 18:04
 * desc: 拆箱单保存
 */
@Slf4j
@Component
public class SgTeusPackSaveFilter extends BaseFilter {

    @Override
    public void before(TableServiceContext context, MainTableRecord row) {

        log.info(this.getClass().getName() + " 主表保存,{}", row.getAction());
        // 初始化主单据字段
        if (DbRowAction.INSERT.equals(row.getAction())) {
            JSONObject commitData = row.getCommitData();
            commitData.put("STATUS", SgUnpackConstants.BILL_STATUS_NOT_AUDIT);
            commitData.put("QTY_TOT_PACK", BigDecimal.ZERO);
            commitData.put("QTY_TOT_TEUS", BigDecimal.ZERO);
        }

        // 明细保存
        String impItemName = SgTeusPackConstants.SG_B_TEUS_PACK_ITEM;
        Map<String, SubTableRecord> subTables = row.getSubTables();
        if (MapUtils.isNotEmpty(subTables) && subTables.get(impItemName) != null) {
            Collection<RowRecord> rowRecords = subTables.get(impItemName).getRows();
            if (CollectionUtils.isNotEmpty(rowRecords)) {
                Set<Long> skuIdSet = new HashSet<>();
                Set<Long> teusIdSet = new HashSet<>();
                List<RowRecord> rowRecordList = new ArrayList<>();
                for (RowRecord record : rowRecords) {
                    JSONObject commitData = record.getCommitData();
                    Long skuId = commitData.getLong("PS_C_SKU_ID");
                    Long teusId = commitData.getLong("PS_C_TEUS_ID");
                    log.info(this.getClass().getName() + " 明细保存,{},skuId,{},teusId,{}", record.getAction(), skuId, teusId);
                    if (DbRowAction.INSERT.equals(record.getAction())) {
                        skuIdSet.add(skuId);
                        teusIdSet.add(teusId);
                        rowRecordList.add(record);
                    }
                }

                //封装条码冗余信息和箱冗余信息
                BasicPsQueryService service = ApplicationContextHandle.getBean(BasicPsQueryService.class);
                SkuInfoQueryRequest skuInfoQueryRequest = new SkuInfoQueryRequest();
                skuInfoQueryRequest.setSkuIdList(new ArrayList<>(skuIdSet));
                HashMap<Long, PsCProSkuResult> skuInfo = new HashMap<>();
                HashMap<Long, PsCTeus> teusHashMap = new HashMap<>();
                log.info(this.getClass().getName() + " 商品信息:{},箱信息:{}", JSONObject.toJSONString(skuInfo), JSONObject.toJSONString(teusHashMap));

                try {
                    teusHashMap = service.queryTeusInfoById(new ArrayList<>(teusIdSet));
                    skuInfo = service.getSkuInfo(skuInfoQueryRequest);
                } catch (Exception e) {
                    AssertUtils.logAndThrow(e.getMessage(), context.getLocale());
                }

                if (CollectionUtils.isNotEmpty(rowRecordList)) {
                    if (MapUtils.isNotEmpty(skuInfo) && MapUtils.isNotEmpty(teusHashMap)) {
                        for (RowRecord rowRecord : rowRecordList) {
                            JSONObject commitData = rowRecord.getCommitData();
                            suppTeusInfo(commitData, skuInfo, teusHashMap);
                        }
                    } else {
                        AssertUtils.logAndThrow("未查到条码信息或箱信息！", context.getLocale());
                    }
                }

            }
        }

    }

    private void suppTeusInfo(JSONObject commitData, HashMap<Long, PsCProSkuResult> skuInfo, HashMap<Long, PsCTeus> teusHashMap) {
        Long skuId = commitData.getLong("PS_C_SKU_ID");

        PsCProSkuResult psCProSkuResult = skuInfo.get(skuId);
        Long teusId = commitData.getLong("PS_C_TEUS_ID");
        PsCTeus psCTeus = teusHashMap.get(teusId);
        AssertUtils.notNull(psCProSkuResult, skuId + "未查到条码属性信息！");
        AssertUtils.notNull(psCTeus, teusId + "未查到箱信息！");

        // 补充条码信息
        commitData.put("PS_C_PRO_ID", psCProSkuResult.getPsCProId());
        commitData.put("PS_C_PRO_ECODE", psCProSkuResult.getPsCProEcode());
        commitData.put("PS_C_PRO_ENAME", psCProSkuResult.getPsCProEname());
        commitData.put("PS_C_SPEC1_ID", psCProSkuResult.getPsCSpec1objId());
        commitData.put("PS_C_SPEC1_ECODE", psCProSkuResult.getClrsEcode());
        commitData.put("PS_C_SPEC1_ENAME", psCProSkuResult.getClrsEname());
        commitData.put("PS_C_SPEC2_ID", psCProSkuResult.getPsCSpec2objId());
        commitData.put("PS_C_SPEC2_ECODE", psCProSkuResult.getSizesEcode());
        commitData.put("PS_C_SPEC2_ENAME", psCProSkuResult.getSizesEname());
        commitData.put("PS_C_SKU_ECODE", psCProSkuResult.getSkuEcode());
        commitData.put("PS_C_MATCHSIZE_ID", psCTeus.getPsCMatchsizeId());
        commitData.put("PS_C_MATCHSIZE_ECODE", psCTeus.getPsCMatchsizeEcode());
        commitData.put("PS_C_MATCHSIZE_ENAME", psCTeus.getPsCMatchsizeEname());
        commitData.put("PS_C_TEUS_ECODE", psCTeus.getEcode());
    }

    @Override
    public void after(TableServiceContext context, MainTableRecord row) {
        Long objId = row.getId();
        SgBTeusPackItemMapper itemMapper = ApplicationContextHandle.getBean(SgBTeusPackItemMapper.class);
        SgBTeusPackMapper packMapper = ApplicationContextHandle.getBean(SgBTeusPackMapper.class);

        List<SgBTeusPackItem> itemList = itemMapper.selectList(new QueryWrapper<SgBTeusPackItem>().lambda()
                .eq(SgBTeusPackItem::getSgBTeusPackId, objId));

        // 计算总装箱数
        Map<Long, List<SgBTeusPackItem>> collect = itemList.stream().collect(Collectors.groupingBy(SgBTeusPackItem::getPsCTeusId));
        BigDecimal countPack = new BigDecimal(collect.keySet().size());

        //计算总箱内数量
        BigDecimal countTeus = itemList.stream()
                .map(o -> o.getQty() == null ? BigDecimal.ZERO : o.getQty())
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        SgBTeusPack sgBTeusPack = new SgBTeusPack();
        sgBTeusPack.setId(objId);
        sgBTeusPack.setQtyTotTeus(countTeus);
        sgBTeusPack.setQtyTotPack(countPack);
        packMapper.updateById(sgBTeusPack);
    }
}
