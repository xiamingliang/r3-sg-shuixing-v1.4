package com.jackrain.nea.sg.inv.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.in.model.result.ScBInventoryResult;
import com.jackrain.nea.sg.inv.mapper.ScBInventoryImpItemMapper;
import com.jackrain.nea.sg.inv.mapper.ScBInventoryMapper;
import com.jackrain.nea.sg.inv.model.request.ScBInventoryQueryRequest;
import com.jackrain.nea.sg.inv.model.table.ScBInventory;
import com.jackrain.nea.sg.inv.model.table.ScBInventoryImpItem;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author: 周琳胜
 * @since: 2019/11/26
 * create at : 2019/11/26 20:32
 */
@Slf4j
@Component
public class ScBInventoryByConditionQueryService {

    public ValueHolderV14<ScBInventoryResult> queryInventory(ScBInventoryQueryRequest request) {
        ValueHolderV14<ScBInventoryResult> v14 = new ValueHolderV14<>();

        if (log.isDebugEnabled()) {
            log.debug("Start ScbInventoryByConditionQueryService.queryInventory. ReceiveParams:requests:{}"
                    , JSONObject.toJSONString(request));
        }
        Boolean isSelectItem = request.getIsSelectItem();
        if (isSelectItem == null) {
            v14.setCode(ResultCode.FAIL);
            v14.setMessage("请设置是否查询明细！");
            return v14;
        }
        PageHelper.startPage(request.getPageNum(), request.getPageSize());

        if (!isSelectItem) {
            ScBInventoryMapper scBInventoryMapper = ApplicationContextHandle.getBean(ScBInventoryMapper.class);

            LambdaQueryWrapper<ScBInventory> lambdaQueryWrapper = new QueryWrapper<ScBInventory>().lambda()
                    .eq(request.getInventoryType() != null, ScBInventory::getInventoryType, request.getInventoryType())
                    .eq(request.getPolStatus() != null, ScBInventory::getPolStatus, request.getPolStatus())
                    .ge(request.getInventoryDate() != null, ScBInventory::getInventoryDate,request.getInventoryDate())
                    .like(StringUtils.isNotEmpty(request.getBillNo()), ScBInventory::getBillNo, request.getBillNo())
                    .orderByDesc(ScBInventory::getInventoryDate)
                    .orderByDesc(ScBInventory::getModifieddate);

            if (request.getStartTime() != null) {
                if (request.getEndTime() == null) {
                    lambdaQueryWrapper.gt(ScBInventory::getInventoryDate, request.getStartTime());
                } else {
                    lambdaQueryWrapper.between(ScBInventory::getInventoryDate, request.getStartTime(), request.getEndTime());
                }
            }
            List<ScBInventory> scBInventoryList = scBInventoryMapper.selectList(lambdaQueryWrapper);
            if (CollectionUtils.isEmpty(scBInventoryList)) {
                v14.setCode(ResultCode.SUCCESS);
                v14.setMessage("无更多数据！");
                return v14;
            }

            PageInfo<ScBInventory> pageInfo = new PageInfo<>(scBInventoryList);
            List<ScBInventory> list = pageInfo.getList();
            ScBInventoryResult result = new ScBInventoryResult();
            result.setInventoryList(list);
            v14.setData(result);
        } else {
            ScBInventoryImpItemMapper scBInventoryImpItemMapper = ApplicationContextHandle.getBean(ScBInventoryImpItemMapper.class);

            QueryWrapper<ScBInventoryImpItem> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("SC_B_INVENTORY_ID", request.getObjId());
            if (StringUtils.isNotEmpty(request.getItemSkuEcode()) || StringUtils.isNotEmpty(request.getItemTeusEcode())) {
                queryWrapper.and(scBInventoryImpItemQueryWrapper -> scBInventoryImpItemQueryWrapper
                        .eq(StringUtils.isNotEmpty(request.getItemSkuEcode()), "PS_C_SKU_ECODE", request.getItemSkuEcode())
                        .or()
                        .eq(StringUtils.isNotEmpty(request.getItemTeusEcode()), "PS_C_TEUS_ECODE", request.getItemTeusEcode()));
            }
            List<ScBInventoryImpItem> items = scBInventoryImpItemMapper.selectList(queryWrapper);
            if (CollectionUtils.isEmpty(items)) {
                v14.setCode(ResultCode.SUCCESS);
                v14.setMessage("无更多数据！");
                return v14;
            }
            PageInfo<ScBInventoryImpItem> pageInfo = new PageInfo<>(items);
            List<ScBInventoryImpItem> list = pageInfo.getList();
            ScBInventoryResult result = new ScBInventoryResult();
            result.setInventoryImpItemList(list);

            v14.setData(result);
        }


        if (log.isDebugEnabled()) {
            log.debug("Finsh ScbInventoryByConditionQueryService.queryInventory. Result:valueHolderV14:{}"
                    , JSONObject.toJSONString(v14));
        }
        v14.setCode(ResultCode.SUCCESS);
        v14.setMessage("成功！");
        return v14;
    }


}
