package com.jackrain.nea.sg.inv.constants;

/**
 * @author leexh
 * @since 2019/3/27 17:30
 */
public interface SgInvConstants {

    String SEQ_SC_B_INVENTORY = "SEQ_SC_B_INVENTORY";
    /**
     * 表
     */
    String SC_B_INVENTORY = "sc_b_inventory";   // 盘点单
    String SC_B_INVENTORY_ID = "sc_b_inventory_id";
    String SC_B_INVENTORY_IMP_ITEM = "sc_b_inventory_imp_item"; // 盘点单明细
    String SC_B_OUT = "sc_b_out";
    String SC_B_STORAGE_ADJUST = "sc_b_storage_adjust";
    String OC_B_RETAIL = "oc_b_retail";
    String OC_B_SALE = "oc_b_sale";
    String SC_B_PRE_PL_UNFSH_BILL = "sc_b_pre_pl_unfsh_bill";
    String SC_B_STOCK_DAILY_FLAG = "sc_b_stock_daily_flag";
    String SG_B_STOCK_DAILY = "rpt_sg_b_phy_storage_daily"; //逐日库存
    String SG_B_TEUS_STOCK_DAILY = "rpt_sg_b_phy_teus_storage_daily"; //逐日库存
    String SG_B_STORAGE = "sg_b_storage"; //逻辑库存
    String SG_B_PHY_ADJUST = "sg_b_phy_adjust"; //库存调整单
    String SG_B_PHY_OUT_NOTICES = "sg_b_phy_out_notices"; //出库通知单
    String OC_B_ORDER = "oc_b_order"; //订单管理

    /**
     * 单据编号
     */
    String SC_B_INVENTORY_BILL_NO = "SQE_SC_B_INVENTORY";
    String SEQ_SC_B_PROFIT="SEQ_SC_B_PROFIT";

    /**
     * 主子表关联字段
     */
    String PAND_PARENT_FIELD = "SC_B_INVENTORY_ID";


    /**
     * 盘点类型
     */
    Integer PAND_TYPE_CP = 1;  //抽盘
    Integer PAND_TYPE_QP = 2;  //全盘

    /**
     * 盈亏状态
     */
    Integer PAND_UNPOL = 1;    // 未盈亏
    Integer PAND_POL = 2;      // 已盈亏
    Integer PAND_POL_VOID = 3;      // 已作废


    /**
     * 盈亏单-单据状态
     */
    Integer STATUS_UNSUBMIT = 1;    // 未提交
    Integer STATUS__SUBMIT = 2;      // 已提交
    Integer STATUS_VOID = 3;      // 已作废


    int DISTRIB_ID_ZERO = 0; // 配销中心ID

    String ACTION_ADD = "ADD";
    String ACTION_SAVE = "SAVE";

    /**
     * 盘点性质  1盘盈 2盘亏 3差异
     */
    Integer INVENTORY_PROPS_PROFIT = 1;
    Integer INVENTORY_PROPS_LOSS = 2;
    Integer INVENTORY_PROPS_DIFF = 3;
}
