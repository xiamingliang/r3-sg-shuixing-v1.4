package com.jackrain.nea.sg.out.services;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.ip.api.qimen.QimenOrderCancelCmd;
import com.jackrain.nea.ip.model.qimen.QimenOrderCancelModel;
import com.jackrain.nea.model.util.AdParamUtil;
import com.jackrain.nea.r3.mq.util.R3MqSendHelper;
import com.jackrain.nea.sg.basic.common.R3ParamConstants;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.mapper.CpCPhyWarehouseMapper;
import com.jackrain.nea.sg.basic.model.result.SgR3BaseResult;
import com.jackrain.nea.sg.basic.model.table.CpCPhyWarehouse;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.out.common.SgOutConstants;
import com.jackrain.nea.sg.out.common.SgOutConstantsIF;
import com.jackrain.nea.sg.out.config.SgOutMqConfig;
import com.jackrain.nea.sg.out.mapper.SgBPhyOutNoticesMapper;
import com.jackrain.nea.sg.out.model.request.SgOutNoticesJdCancelMsgRequest;
import com.jackrain.nea.sg.out.model.request.SgPhyOutNoticesBillVoidRequest;
import com.jackrain.nea.sg.out.model.request.SgPhyOutNoticesSaveRequest;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutNotices;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.UUID;

/**
 * @author leexh
 * @since 2019/5/7 19:48
 */
@Slf4j
@Component
public class SgPhyOutNoticesBatchVoidAndWMSService {

    @Autowired
    private SgOutMqConfig mqConfig;

    @Autowired
    private R3MqSendHelper r3MqSendHelper;

    public ValueHolderV14<SgR3BaseResult> batchVoidSgPhyOutNoticesAndWMS(SgPhyOutNoticesBillVoidRequest requests) {
        ValueHolderV14<SgR3BaseResult> v14 = new ValueHolderV14();

        AssertUtils.notNull(requests, "参数不能为空！");
        User loginUser = requests.getLoginUser();
        AssertUtils.notNull(loginUser, "用户信息不能为空！");
        Locale locale = loginUser.getLocale();

        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ".debug, param:" + JSONObject.toJSONString(requests));
        }

        List<SgPhyOutNoticesSaveRequest> saveRequests = requests.getNoticesSaveRequests();
        if (CollectionUtils.isEmpty(saveRequests)) {
            AssertUtils.logAndThrow("参数不能为空！", locale);
        }

        // 取消出库使用
        Boolean isCancelOut = Optional.ofNullable(requests.getPosCancel()).orElse(false);

        SgPhyOutNoticesVoidService voidService = ApplicationContextHandle.getBean(SgPhyOutNoticesVoidService.class);
        SgPhyOutNoticesSaveService saveService = ApplicationContextHandle.getBean(SgPhyOutNoticesSaveService.class);
        List<SgPhyOutNoticesSaveRequest> noticesSaveRequests = requests.getNoticesSaveRequests();
        SgBPhyOutNoticesMapper noticesMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesMapper.class);
        JSONArray errorArr = new JSONArray();
        List<Long> noticesIdsByJD = Lists.newArrayList();
        for (int i = 0; i < noticesSaveRequests.size(); i++) {
            SgPhyOutNoticesSaveRequest noticesSaveRequest = noticesSaveRequests.get(i);
            SgBPhyOutNotices outNotices = saveService.queryNoticesBySource(noticesSaveRequest);
            if (outNotices != null) {
                try {
                    // 传WMS成功，RPC调“撤回WMS”接口
                    Long wmsStatus = outNotices.getWmsStatus();
                    if (wmsStatus == SgOutConstantsIF.WMS_STATUS_PASS_SUCCESS || wmsStatus == SgOutConstantsIF.WMS_STATUS_PASS_FAILED) {
                        // 撤回WMS...
                        ValueHolderV14 qmResult = cancelWMS(loginUser, outNotices);
                        log.debug("作废出库通知单撤回WMS结果:" + JSONObject.toJSONString(qmResult));
                        if (qmResult != null) {
                            if (qmResult.getCode() == ResultCode.SUCCESS) {
                                voidService.voidSgPhyOutNoticesSingle(outNotices, loginUser, noticesMapper, isCancelOut);
                                if (SgOutConstantsIF.LOGISTICS_JD.equals(outNotices.getCpCLogisticsEcode())) {
                                    noticesIdsByJD.add(outNotices.getId());
                                }
                                errorRecord(noticesSaveRequest.getSourceBillId(), ResultCode.SUCCESS, "作废成功！", errorArr, null);
                            } else {
                                String qmResultMessage = qmResult.getMessage();
                                try {
                                    String failedReason = AdParamUtil.getParam(SgOutConstants.SYSTEM_CANCELWMS_FAILED_REASON);
                                    log.debug("系统参数从WMS撤回失败原因:" + failedReason);
                                    if (StringUtils.isNotEmpty(failedReason)) {
                                        String[] reasons = failedReason.split(";;");
                                        List<String> failedReasons = Lists.newArrayList();
                                        for (String reason : reasons) {
                                            String replace = reason.replace("[", "").replace("]", "");
                                            failedReasons.add(replace);
                                        }
                                        boolean contains = false;
                                        for (String reason : failedReasons) {
                                            if (qmResultMessage.contains(reason)) {
                                                contains = true;
                                                break;
                                            }
                                        }
                                        if (contains) {
                                            voidService.voidSgPhyOutNoticesSingle(outNotices, loginUser, noticesMapper, isCancelOut);
                                            if (SgOutConstantsIF.LOGISTICS_JD.equals(outNotices.getCpCLogisticsEcode())) {
                                                noticesIdsByJD.add(outNotices.getId());
                                            }
                                            errorRecord(noticesSaveRequest.getSourceBillId(), ResultCode.SUCCESS, "作废成功！", errorArr, null);
                                        } else {
                                            errorRecord(noticesSaveRequest.getSourceBillId(), qmResult.getCode(), qmResultMessage, errorArr, null);
                                        }
                                    } else {
                                        errorRecord(noticesSaveRequest.getSourceBillId(), qmResult.getCode(), qmResultMessage, errorArr, null);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    String messageExtra = AssertUtils.getMessageExtra("系统参数从WMS撤回失败原因解析异常!", e);
                                    log.error(messageExtra);
                                    errorRecord(noticesSaveRequest.getSourceBillId(), qmResult.getCode(), messageExtra, errorArr, null);
                                }

                            }
                        }
                    } else if (wmsStatus == SgOutConstantsIF.WMS_STATUS_WAIT_PASS) {
                        voidService.voidSgPhyOutNoticesSingle(outNotices, loginUser, noticesMapper, isCancelOut);
                        if (SgOutConstantsIF.LOGISTICS_JD.equals(outNotices.getCpCLogisticsEcode())) {
                            noticesIdsByJD.add(outNotices.getId());
                        }
                        errorRecord(noticesSaveRequest.getSourceBillId(), ResultCode.SUCCESS, "作废成功！", errorArr, null);
                    } else if (wmsStatus == SgOutConstantsIF.WMS_STATUS_PASSING) {
                        errorRecord(noticesSaveRequest.getSourceBillId(), ResultCode.FAIL, "此单传WMS中,请稍后重试！", errorArr, null);
                    }
                } catch (Exception e) {
                    log.error(this.getClass().getName() + ".error:" + e.getMessage(), e);
                    errorRecord(noticesSaveRequest.getSourceBillId(), ResultCode.FAIL, e.getMessage(), errorArr, null);
                }
            } else {
                errorRecord(noticesSaveRequest.getSourceBillId(), ResultCode.FAIL, "记录不存在！", errorArr, true);
            }
        }

        if (CollectionUtils.isNotEmpty(noticesIdsByJD)) {
            //2019-10-12 增加取消京东快递逻辑
            try {
                SgOutNoticesJdCancelMsgRequest cancelMsgRequest = new SgOutNoticesJdCancelMsgRequest();
                cancelMsgRequest.setResultList(noticesIdsByJD);
                cancelMsgRequest.setLoginUser(loginUser);
                String configName = mqConfig.getConfigName();
                String topic = mqConfig.getMqTopic();
                String msgKey = UUID.randomUUID().toString().replaceAll("-", "");
                String body = JSONObject.toJSONString(cancelMsgRequest);
                String msgId = r3MqSendHelper.sendMessage(configName, body, topic, SgOutConstants.TAG_OUT_NOTICES_JD_CANCEL, msgKey);
                log.debug("作废出库通知单ids" + noticesIdsByJD + "取消京东快递mqMsgId:" + msgId);
            } catch (Exception e) {
                log.error("取消京东快递mq异常!" + e.getMessage());
            }
        }

        if (errorArr.size() > 0) {
            SgR3BaseResult data = new SgR3BaseResult();
            data.setDataArr(errorArr);
            v14.setCode(ResultCode.SUCCESS);
            v14.setData(data);
            v14.setMessage("成功！");
        } else {
            v14.setCode(ResultCode.SUCCESS);
            v14.setMessage("成功！");
        }
        if (log.isDebugEnabled()) {
            log.debug("作废出库通知单并从WMS撤回出参:" + JSONObject.toJSONString(v14));
        }
        return v14;
    }

    /**
     * 取消WMS
     *
     * @param user
     * @param outNotices
     * @return
     */
    public ValueHolderV14 cancelWMS(User user, SgBPhyOutNotices outNotices) {

        QimenOrderCancelCmd orderCancelCmd = (QimenOrderCancelCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(), QimenOrderCancelCmd.class.getName(), "ip", "1.4.0");

        CpCPhyWarehouseMapper warehouseMapper = ApplicationContextHandle.getBean(CpCPhyWarehouseMapper.class);
        CpCPhyWarehouse warehouse = warehouseMapper.selectById(outNotices.getCpCPhyWarehouseId());

        QimenOrderCancelModel cancelModel = new QimenOrderCancelModel();
        cancelModel.setOperateUser(user);
        cancelModel.setWarehouseCode(warehouse != null ? warehouse.getWmsWarehouseCode() : "");
        cancelModel.setOrderCode(outNotices.getBillNo());
        cancelModel.setOrderType(getOrderType(outNotices.getSourceBillType()));
        cancelModel.setCustomerId(warehouse != null ? warehouse.getWmsAccount() : "");
        cancelModel.setOwnerCode(outNotices.getGoodsOwner());
        cancelModel.setOrderId(outNotices.getWmsBillNo());
        return orderCancelCmd.cancelQimenOrder(cancelModel);
    }

    /**
     * 传奇门类型转换
     *
     * @param sourceType
     * @return
     */
    public String getOrderType(Integer sourceType) {
        String orderType = SgOutConstants.QM_BILL_TYPE_OTHER_OUT;
        switch (sourceType) {
            case SgConstantsIF.BILL_TYPE_RETAIL:
                orderType = SgOutConstants.QM_BILL_TYPE_GENERAL_OUT;
                break;
            case SgConstantsIF.BILL_TYPE_SALE:
                orderType = SgOutConstants.QM_BILL_TYPE_B2B_OUT;
                break;
            case SgConstantsIF.BILL_TYPE_PUR_REF:
                orderType = SgOutConstants.QM_BILL_TYPE_REF_PUR_OUT;
                break;
            case SgConstantsIF.BILL_TYPE_TRANSFER:
                orderType = SgOutConstants.QM_BILL_TYPE_TRANSFER_OUT;
                break;
            case SgConstantsIF.BILL_TYPE_VIPSHOP:
                orderType = SgOutConstants.QM_BILL_TYPE_B2B_OUT;
                break;
        }
        return orderType;
    }

    /**
     * 错误信息收集
     *
     * @param mainId   记录ID
     * @param message  错误信息
     * @param errorArr 错误集合
     * @return 错误记录数
     */
    public void errorRecord(Long mainId, Integer code, String message, JSONArray errorArr, Boolean isExist) {
        Boolean flag = false;
        if (isExist != null) {
            flag = isExist;
        }
        JSONObject errorDate = new JSONObject();
        errorDate.put(R3ParamConstants.CODE, code);
        errorDate.put(R3ParamConstants.OBJID, mainId);
        errorDate.put(R3ParamConstants.MESSAGE, message);
        if (flag) {
            errorDate.put("is_exist", null);
        }
        errorArr.add(errorDate);
    }
}
