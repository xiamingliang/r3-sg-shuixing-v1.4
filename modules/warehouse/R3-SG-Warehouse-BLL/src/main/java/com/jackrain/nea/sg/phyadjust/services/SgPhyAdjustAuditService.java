package com.jackrain.nea.sg.phyadjust.services;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.model.util.AdParamUtil;
import com.jackrain.nea.model.util.ModelUtil;
import com.jackrain.nea.oc.basic.common.OcBasicConstants;
import com.jackrain.nea.oc.basic.model.request.QueryBoxRequest;
import com.jackrain.nea.oc.basic.services.QueryBoxItemsService;
import com.jackrain.nea.psext.model.table.PsCTeusItem;
import com.jackrain.nea.redis.config.CusRedisTemplate;
import com.jackrain.nea.sg.basic.api.SgPhyStorageNoTransUpdateCmd;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.config.SgStorageBoxConfig;
import com.jackrain.nea.sg.basic.model.request.*;
import com.jackrain.nea.sg.basic.utils.R3ParamUtil;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.out.utils.SgWarehouseESUtils;
import com.jackrain.nea.sg.phyadjust.common.SgPhyAdjustConstants;
import com.jackrain.nea.sg.phyadjust.mapper.SgBPhyAdjustImpItemMapper;
import com.jackrain.nea.sg.phyadjust.mapper.SgBPhyAdjustItemMapper;
import com.jackrain.nea.sg.phyadjust.mapper.SgBPhyAdjustMapper;
import com.jackrain.nea.sg.phyadjust.mapper.SgBPhyAdjustTeusItemMapper;
import com.jackrain.nea.sg.phyadjust.model.request.SgPhyAdjustBillAuditRequest;
import com.jackrain.nea.sg.phyadjust.model.request.SgPhyAdjustStoreRequest;
import com.jackrain.nea.sg.phyadjust.model.result.SgPhyAdjustBillQueryResult;
import com.jackrain.nea.sg.phyadjust.model.table.SgBPhyAdjust;
import com.jackrain.nea.sg.phyadjust.model.table.SgBPhyAdjustImpItem;
import com.jackrain.nea.sg.phyadjust.model.table.SgBPhyAdjustItem;
import com.jackrain.nea.sg.phyadjust.model.table.SgBPhyAdjustTeusItem;
import com.jackrain.nea.sg.phyadjust.utils.SgPhyAdjustAsyncUtil;
import com.jackrain.nea.sg.send.common.SgSendConstantsIF;
import com.jackrain.nea.sg.send.utils.SgStoreUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.utility.ExceptionUtil;
import com.jackrain.nea.utils.AssertUtils;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static com.jackrain.nea.sg.send.utils.SgStoreUtils.initRedisTemplate;

/**
 * @author csy
 * Date: 2019/4/29
 * Description: 库存调整单-实体仓-审核
 */

@Slf4j
@Component
public class SgPhyAdjustAuditService {

    @Autowired
    private SgBPhyAdjustMapper mapper;

    @Autowired
    private SgBPhyAdjustItemMapper itemMapper;

    @Autowired
    private SgStorageBoxConfig storageBoxConfig;

    @Autowired
    private SgWarehouseESUtils warehouseESUtils;

    /**
     * 接口入参
     */
    @Transactional(rollbackFor = Exception.class)
    public ValueHolderV14 audit(SgPhyAdjustBillAuditRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("Start SgPhyAdjustAuditService.audit. ReceiveParams:SgPhyAdjustBillAuditRequest="
                    + JSONObject.toJSONString(request) + ";");
        }
        AssertUtils.notNull(request, "参数为空-request");
        User user = request.getUser();
        Long sourceId = request.getSourceBillId();
        Integer sourceBillType = request.getSourceBillType();
        AssertUtils.notNull(user, "参数为空-user");
        AssertUtils.notNull(sourceId, "参数为空-sourceId");
        AssertUtils.notNull(sourceBillType, "参数为空-sourceBillType");
        SgBPhyAdjust adjust = mapper.selectOne(new QueryWrapper<SgBPhyAdjust>().lambda()
                .eq(SgBPhyAdjust::getSourceBillId, sourceId)
                .eq(SgBPhyAdjust::getSourceBillType, sourceBillType));
        return audit(adjust, mapper, user, null);
    }


    @Transactional(rollbackFor = Exception.class)
    public ValueHolder audit(Long objId, User user, SgPhyAdjustStoreRequest storeRequest) {
        if (log.isDebugEnabled()) {
            log.debug("Start SgPhyAdjustAuditService.audit. ReceiveParams:objId="
                    + objId + ";");
        }
        SgBPhyAdjust adjust = mapper.selectById(objId);
        return R3ParamUtil.convertV14(audit(adjust, mapper, user, storeRequest));
    }

    /**
     * 审核
     *
     * @param user   用户
     * @param adjust 主表信息
     * @param mapper mapper
     */
    public ValueHolderV14 audit(SgBPhyAdjust adjust, SgBPhyAdjustMapper mapper, User user, SgPhyAdjustStoreRequest storeRequest) {
        //===单据状态校验===
        Locale locale = user.getLocale();
        SgPhyAdjustBillQueryResult checkResult = statusCheck(adjust, locale, user);
        CusRedisTemplate<String, String> redisTemplate = initRedisTemplate();
        String lockKsy = SgConstants.SG_B_PHY_ADJUST + ":" + adjust.getBillNo();
        Boolean blnCanInit = redisTemplate.opsForValue().setIfAbsent(lockKsy, "OK");
        if (blnCanInit != null && blnCanInit) {
            redisTemplate.expire(lockKsy, 30, TimeUnit.MINUTES);
        } else {
            AssertUtils.logAndThrow("当前记录正在被操作,请稍后重试!");
        }
        try {
            Long objId = adjust.getId();
            //===跟新单据审核状态 审核人等相关信息===
            SgBPhyAdjust updateAdj = new SgBPhyAdjust();
            updateAdj.setId(objId);
            StorageESUtils.setBModelDefalutDataByUpdate(updateAdj, user);
            updateAdj.setModifierename(user.getEname());
            updateAdj.setBillStatus(SgPhyAdjustConstants.ADJ_AUDIT_STATUS_Y);
            updateAdj.setCheckerId(user.getId().longValue());
            updateAdj.setCheckerEname(user.getEname());
            updateAdj.setCheckerName(user.getName());
            updateAdj.setCheckTime(new Date());
            //启用箱库存审核更新总数量
            if (storageBoxConfig.getBoxEnable()) {
                List<SgBPhyAdjustItem> adjustItems = checkResult.getAdjustItems();
                AssertUtils.isNotEmpty("当前记录明细为空，不允许审核!", adjustItems);
                BigDecimal totQty = adjustItems.stream().map(SgBPhyAdjustItem::getQty).reduce(BigDecimal.ZERO, BigDecimal::add);
                updateAdj.setTotQty(totQty);
            }
            mapper.updateById(updateAdj);
            //=====调用自动生成逻辑调整单服务======
            SgPhyAdjustGenerateLogicAdjService logicAdjService = ApplicationContextHandle.getBean(SgPhyAdjustGenerateLogicAdjService.class);
            logicAdjService.generateLogicAdj(checkResult, user, locale, storeRequest);
            //=====调用库存服务 ==
            changeStorage(checkResult, user);

            //推送es
            warehouseESUtils.pushESByPhyAdjust(objId, false, false, null, mapper, null);
        } catch (Exception e) {
            AssertUtils.logAndThrow(ExceptionUtil.getMessage(e), locale);
        } finally {
            redisTemplate.delete(lockKsy);
        }
        return new ValueHolderV14(ResultCode.SUCCESS, Resources.getMessage("审核成功!", locale));

    }


    /**
     * 单据状态校验
     */
    public SgPhyAdjustBillQueryResult statusCheck(SgBPhyAdjust adjust, Locale locale, User user) {
        SgPhyAdjustBillQueryResult checkResult = new SgPhyAdjustBillQueryResult();
        AssertUtils.notNull(adjust, "当前单据已不存在!", locale);
        AssertUtils.notNull(adjust.getBillStatus(), "当数据错误-bill_status不存在!", locale);
        AssertUtils.cannot(adjust.getBillStatus() == SgPhyAdjustConstants.ADJ_AUDIT_STATUS_Y, "当前单据已审核，不能重复审核!", locale);
        AssertUtils.cannot(adjust.getBillStatus() == SgPhyAdjustConstants.ADJ_AUDIT_STATUS_V, "当前单据已作废，不能审核!", locale);

        SgBPhyAdjust adjustTemp = mapper.selectById(adjust.getId());
        AssertUtils.notNull(adjustTemp, "当前单据已经不存在!", locale);
        checkResult.setAdjust(adjustTemp);

        //异步删除明细中调整数量为0的记录
        //考虑到拆箱/装箱 剔除该逻辑
//        SgPhyAdjustAsyncUtil asynUtil = ApplicationContextHandle.getBean(SgPhyAdjustAsyncUtil.class);
//        asynUtil.deleteAdjustItemsByZero(adjust.getId());

        //启用箱库存
        if (storageBoxConfig.getBoxEnable()) {
            SgBPhyAdjustImpItemMapper impItemMapper = ApplicationContextHandle.getBean(SgBPhyAdjustImpItemMapper.class);
            List<SgBPhyAdjustImpItem> impItems = impItemMapper.selectList(new QueryWrapper<SgBPhyAdjustImpItem>().lambda()
                    .eq(SgBPhyAdjustImpItem::getSgBPhyAdjustId, adjust.getId())
                    .ne(SgBPhyAdjustImpItem::getQty, BigDecimal.ZERO));
            AssertUtils.isTrue(CollectionUtils.isNotEmpty(impItems), "库存调整单录入明细不能为空!");
            checkResult.setAdjustImpItems(impItems);
            impItemsConvertMethod(checkResult, adjust.getId(), user);
        }
        //录入明细转箱内明细
        return checkResult;
    }

    /**
     * 录入明细转箱明细和条码明细
     *
     * @param checkResult 数据源
     * @param id          主表Id
     * @param user        操作用户
     */
    private void impItemsConvertMethod(SgPhyAdjustBillQueryResult checkResult, Long id, User user) {
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ", checkResult={}", JSON.toJSONString(checkResult, SerializerFeature.WriteMapNullValue));
        }
        List<SgBPhyAdjustImpItem> impitemList = checkResult.getAdjustImpItems();
        List<SgBPhyAdjustTeusItem> teusItemList = Lists.newArrayList();
        HashMap<Long, SgBPhyAdjustItem> itemsMap = Maps.newHashMap();
        //箱id-箱明细request
        HashMap<String, SgBPhyAdjustImpItem> teusMap = Maps.newHashMap();
        for (SgBPhyAdjustImpItem impitem : impitemList) {
            if ((SgSendConstantsIF.IS_TEUS_Y).equals(impitem.getIsTeus())) {
                AssertUtils.isTrue(StringUtils.isNotEmpty(impitem.getPsCTeusEcode()), "录入明细" + impitem.getPsCProEcode() + ",箱号为空!");
                AssertUtils.isTrue(!teusMap.containsKey(impitem.getPsCTeusEcode()), "存在重复箱号" + impitem.getPsCTeusEcode() + "录入明细!");
                teusMap.put(impitem.getPsCTeusEcode(), impitem);
            } else {
                SgBPhyAdjustItem item = new SgBPhyAdjustItem();
                BeanUtils.copyProperties(impitem, item);
                item.setId(ModelUtil.getSequence(SgConstants.SG_B_PHY_ADJUST_ITEM));
                item.setSgBPhyAdjustId(id);
                item.setOwnerename(user.getEname());
                item.setModifierename(user.getEname());

                //start impitem.getPriceList() 不为null  add cxx
                BigDecimal priceList = impitem.getPriceList();
                if(priceList==null){
                    priceList=BigDecimal.ZERO;
                }
                //end
                item.setAmtList(priceList.multiply(impitem.getQty()));
                StorageESUtils.setBModelDefalutData(item, user);
                itemsMap.put(item.getPsCSkuId(), item);
            }
        }

        if (CollectionUtils.isNotEmpty(teusMap.keySet())) {
            //获取箱内明细，并且 箱内明细转换成条码明细
            try {
                QueryBoxItemsService queryTeusService = ApplicationContextHandle.getBean(QueryBoxItemsService.class);
                QueryBoxRequest boxRequest = new QueryBoxRequest();
                List<String> teusEcodes = new ArrayList<>(teusMap.keySet());
                boxRequest.setBoxECodes(teusEcodes);
                boxRequest.setBoxStatus(OcBasicConstants.BOX_STATUS_IN);
                List<PsCTeusItem> psCTeusItems = queryTeusService.queryBoxItemsByBoxInfo(boxRequest);
                if (CollectionUtils.isNotEmpty(psCTeusItems)) {
                    for (PsCTeusItem psCTeusItem : psCTeusItems) {
                        SgBPhyAdjustTeusItem teusItem = new SgBPhyAdjustTeusItem();
                        BeanUtils.copyProperties(psCTeusItem, teusItem);
                        //给箱内明细赋值商品信息
                        SgBPhyAdjustImpItem impItem = teusMap.get(psCTeusItem.getPsCTeusEcode());
                        //R3矩阵保存没有箱id,在此补充
                        if (impItem.getPsCTeusId() == null) {
                            impItem.setPsCTeusId(psCTeusItem.getPsCTeusId());
                        }
                        StorageESUtils.setBModelDefalutData(impItem, user);
                        teusItem.setQty(impItem.getQty().multiply(teusItem.getQty()));
                        teusItem.setSourceBillItemId(impItem.getSourceBillItemId());
                        teusItem.setPsCProId(impItem.getPsCProId());
                        teusItem.setPsCProEcode(impItem.getPsCProEcode());
                        teusItem.setPsCProEname(impItem.getPsCProEname());
                        teusItem.setId(ModelUtil.getSequence(SgConstants.SG_B_ADJUST_TEUS_ITEM));
                        teusItem.setSgBPhyAdjustId(id);
                        teusItem.setPriceList(impItem.getPriceList());
                        teusItem.setOwnerename(user.getEname());
                        teusItem.setModifierename(user.getEname());
                        teusItemList.add(teusItem);
                    }
                    //箱内明细转条码明细
                    for (SgBPhyAdjustTeusItem teusItem : teusItemList) {
                        Long psCSkuId = teusItem.getPsCSkuId();
                        if (itemsMap.containsKey(psCSkuId)) {
                            SgBPhyAdjustItem item = itemsMap.get(psCSkuId);
                            BigDecimal qty = Optional.ofNullable(teusItem.getQty()).orElse(BigDecimal.ZERO);
                            item.setQty(qty.add(item.getQty()));
                            item.setAmtList(teusItem.getPriceList().multiply(item.getQty()));
                        } else {
                            SgBPhyAdjustItem item = new SgBPhyAdjustItem();
                            BeanUtils.copyProperties(teusItem, item);
                            item.setId(ModelUtil.getSequence(SgConstants.SG_B_PHY_ADJUST_ITEM));
                            item.setSgBPhyAdjustId(id);
                            item.setOwnerename(user.getEname());
                            item.setModifierename(user.getEname());
                            item.setAmtList(teusItem.getPriceList().multiply(teusItem.getQty()));
                            StorageESUtils.setBModelDefalutData(item, user);
                            itemsMap.put(psCSkuId, item);
                        }
                    }
                } else {
                    AssertUtils.logAndThrow("根据箱号获取箱明细为空!");
                }
            } catch (Exception e) {
                e.printStackTrace();
                AssertUtils.logAndThrow("解析箱明细异常!" + e.getMessage());
            }
        }

        if (CollectionUtils.isNotEmpty(teusItemList)) {
            SgBPhyAdjustTeusItemMapper teusItemMapper = ApplicationContextHandle.getBean(SgBPhyAdjustTeusItemMapper.class);
            SgStoreUtils.batchInsertTeus(teusItemList, "库存调整单箱内明细", SgConstants.SG_COMMON_ITEM_INSERT_PAGE_SIZE, teusItemMapper);
            checkResult.setAdjustTeusItems(teusItemList);
        }

        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + " 开始库存调整单条码明细");
        }

        if (MapUtils.isNotEmpty(itemsMap)) {
            List<SgBPhyAdjustItem> itemList = new ArrayList<>(itemsMap.values());
            SgStoreUtils.batchInsertTeus(itemList, "库存调整单条码明细", SgConstants.SG_COMMON_ITEM_INSERT_PAGE_SIZE, itemMapper);
            checkResult.setAdjustItems(itemList);
        }
    }


    /**
     * 调用库存服务
     */
    private void changeStorage(SgPhyAdjustBillQueryResult queryResult, User user) {
        String allowNegativeStorage = AdParamUtil.getParam("warehouse.allow_negative_storage");
        boolean isAllowNeg = Boolean.valueOf(allowNegativeStorage);
        if (log.isDebugEnabled()) {
            log.debug("调整单是否负库存系统参数:" + isAllowNeg);
        }

        Long startTime = System.currentTimeMillis();
        SgBPhyAdjust adjust = queryResult.getAdjust();
        List<SgBPhyAdjustItem> items = queryResult.getAdjustItems();
        if (CollectionUtils.isNotEmpty(items)) {
            SgPhyStorageNoTransUpdateCmd service = (SgPhyStorageNoTransUpdateCmd) ReferenceUtil.refer(
                    ApplicationContextHandle.getApplicationContext(),
                    SgPhyStorageNoTransUpdateCmd.class.getName(),
                    SgConstantsIF.GROUP, SgConstantsIF.VERSION);

            SgPhyStorageUpdateBillRequest stockRequest = new SgPhyStorageUpdateBillRequest();
            stockRequest.setBillDate(adjust.getBillDate());
            stockRequest.setBillId(adjust.getId());
            stockRequest.setBillNo(adjust.getBillNo());
            stockRequest.setBillType(SgConstantsIF.BILL_TYPE_ADJUST);
            stockRequest.setChangeDate(new Date());
            stockRequest.setIsCancel(false);

            Map<Long, BigDecimal> skuTeusCounts = Maps.newHashMap();
            if (storageBoxConfig.getBoxEnable()) {
                if (CollectionUtils.isNotEmpty(queryResult.getAdjustTeusItems())) {
                    skuTeusCounts = queryResult.getAdjustTeusItems().stream().collect(Collectors.groupingBy(SgBPhyAdjustTeusItem::getPsCSkuId,
                            Collectors.reducing(BigDecimal.ZERO, SgBPhyAdjustTeusItem::getQty, BigDecimal::add)));
                }

                List<SgPhyTeusStorageUpdateBillItemRequest> teusList = Lists.newArrayList();
                queryResult.getAdjustImpItems().stream()
                        .filter(o -> o.getIsTeus() != null && o.getIsTeus().equals(OcBasicConstants.IS_MATCH_SIZE_Y))
                        .forEach(o -> {
                            SgPhyTeusStorageUpdateBillItemRequest teusStorageUpdateBillItemRequest = new SgPhyTeusStorageUpdateBillItemRequest();
                            BeanUtils.copyProperties(o, teusStorageUpdateBillItemRequest);
                            teusStorageUpdateBillItemRequest.setBillItemId(o.getId());
                            teusStorageUpdateBillItemRequest.setQtyChange(o.getQty());
                            teusStorageUpdateBillItemRequest.setCpCPhyWarehouseId(adjust.getCpCPhyWarehouseId());
                            teusStorageUpdateBillItemRequest.setCpCPhyWarehouseEcode(adjust.getCpCPhyWarehouseEcode());
                            teusStorageUpdateBillItemRequest.setCpCPhyWarehouseEname(adjust.getCpCPhyWarehouseEname());
                            teusList.add(teusStorageUpdateBillItemRequest);
                        });
                stockRequest.setTeusList(teusList);
            }


            List<SgPhyStorageUpdateBillItemRequest> itemRequests = new ArrayList<>();
            for (SgBPhyAdjustItem adjustItem : items) {
                SgPhyStorageUpdateBillItemRequest itemRequest = new SgPhyStorageUpdateBillItemRequest();
                BeanUtils.copyProperties(adjustItem, itemRequest);
                itemRequest.setBillItemId(adjustItem.getId());
                itemRequest.setCpCPhyWarehouseId(adjust.getCpCPhyWarehouseId());
                itemRequest.setCpCPhyWarehouseEcode(adjust.getCpCPhyWarehouseEcode());
                itemRequest.setCpCPhyWarehouseEname(adjust.getCpCPhyWarehouseEname());
                itemRequest.setPsCSkuId(adjustItem.getPsCSkuId());
                itemRequest.setQtyChange(adjustItem.getQty());
                BigDecimal teusQty = skuTeusCounts.get(adjustItem.getPsCSkuId());
                if (teusQty != null) {
                    itemRequest.setQtyTeusChange(teusQty);
                }
                itemRequests.add(itemRequest);
            }
            stockRequest.setItemList(itemRequests);
            SgStorageUpdateControlRequest controlRequest = new SgStorageUpdateControlRequest();
            controlRequest.setNegativeStorage(isAllowNeg);
            //2019-09-05 若调整性质为无头件或冲无头件，开启实体仓负库存
            if (adjust.getSgBAdjustPropId() == SgPhyAdjustConstants.ADJUST_PROP_NOHEAD ||
                    adjust.getSgBAdjustPropId() == SgPhyAdjustConstants.ADJUST_PROP_FLUSH_NOHEAD ||
                    adjust.getSgBAdjustPropId() == SgPhyAdjustConstants.ADJUST_PROP_WRONG_ADJUST) {
                controlRequest.setNegativeStorage(true);
            }
            SgPhyStorageSingleUpdateRequest request = new SgPhyStorageSingleUpdateRequest();
            request.setBill(stockRequest);
            request.setControlModel(controlRequest);
            request.setLoginUser(user);
            request.setMessageKey(SgConstants.MSG_TAG_PHY_ADJUST + ":" + adjust.getBillNo());
            ValueHolderV14 holderV14 = service.updateStorageBillNoTrans(request);
            if (log.isDebugEnabled()) {
                log.debug("Finish SgPhyStorageNoTransUpdateService.updatePhyStorageBill. Return:Results:{} spend time:{};",
                        holderV14.toJSONObject(), System.currentTimeMillis() - startTime);
            }
            AssertUtils.cannot(holderV14.getCode() == ResultCode.FAIL, "实体仓库存调整失败:" + holderV14.getMessage(), user.getLocale());
        }
    }
}
