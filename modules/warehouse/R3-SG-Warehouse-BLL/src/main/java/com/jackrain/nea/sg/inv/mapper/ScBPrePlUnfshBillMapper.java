package com.jackrain.nea.sg.inv.mapper;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.inv.constants.SgInvConstants;
import com.jackrain.nea.sg.inv.model.table.ScBPrePlUnfshBill;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.Date;
import java.util.List;

@Mapper
public interface ScBPrePlUnfshBillMapper extends ExtentionMapper<ScBPrePlUnfshBill> {

    /**
     * 插入未完成单据
     * <p>
     * 【未完成单据】状态节点判断逻辑：
     * 入库通知单：【实体仓】=【盘点店仓】、【单据状态】=待入库或者部分入库的入库通知单
     * 出库通知单：【实体仓】=【盘点店仓】、【单据状态】=待出库或者部分出库的出库通知单
     * 库存调整单：【调整店仓】=【盘点店仓】、【单据日期】<=【盘点日期】、【单据状态】=未审核的库存调整单
     * 调拨差异单：【发货店仓】/【收货店仓】=【盘点店仓】、【单据状态】=未审核的调拨差异单
     * 销售差异单：【发货店仓】/【收货店仓】=【盘点店仓】、【单据状态】=未审核的销售差异单
     * 销售退货差异单：【发货店仓】/【收货店仓】=【盘点店仓】、【单据状态】=未审核的销售差异单
     * 销售退货差异单：【发货店仓】/【收货店仓】=【盘点店仓】、【单据状态】=未审核的销售差异单
     * 零售单：【零售店仓】=【盘点店仓】、【零售日期】<=【盘点日期】、【收银状态】=未收银的零售单
     * </p>
     */

    @Select("SELECT * FROM " + SgConstants.SG_B_PHY_IN_NOTICES + " WHERE cp_c_phy_warehouse_id = #{warehouseId} " +
            "AND bill_status in (${status}) AND isactive='" + SgConstants.IS_ACTIVE_Y + "' " +
            "AND EXISTS (SELECT 1 FROM " + SgConstants.SG_B_PHY_IN_NOTICES_IMP_ITEM + " WHERE PS_C_PRO_ECODE IN (${proEcodes}))")
    List<JSONObject> selectListByInNotices(@Param("warehouseId") Long warehouseId, @Param("status") String status, @Param("proEcodes") String proEcodes);

    @Select("SELECT * FROM " + SgConstants.SG_B_PHY_OUT_NOTICES + " WHERE cp_c_phy_warehouse_id = #{warehouseId} " +
            "AND bill_status in (${status}) AND isactive='" + SgConstants.IS_ACTIVE_Y + "' " +
            "AND EXISTS (SELECT 1 FROM " + SgConstants.SG_B_PHY_OUT_NOTICES_IMP_ITEM + " WHERE PS_C_PRO_ECODE IN (${proEcodes}))")
    List<JSONObject> selectListByOutNotices(@Param("warehouseId") Long warehouseId, @Param("status") String status, @Param("proEcodes") String proEcodes);

    @Select("SELECT * FROM " + SgConstants.SG_B_PHY_ADJUST + " WHERE cp_c_phy_warehouse_id = #{warehouseId} " +
            "AND bill_date <= #{inventoryDate} AND bill_status = #{status} AND isactive='" + SgConstants.IS_ACTIVE_Y + "' " +
            "AND EXISTS (SELECT 1 FROM " + SgConstants.SG_B_PHY_ADJUST_IMP_ITEM + " WHERE PS_C_PRO_ECODE IN (${proEcodes}))")
    List<JSONObject> selectListByPhyAdjust(@Param("warehouseId") Long warehouseId, @Param("inventoryDate") Date inventoryDate, @Param("status") Integer status, @Param("proEcodes") String proEcodes);

    @Select("SELECT * FROM " + SgInvConstants.SC_B_INVENTORY + " WHERE cp_c_phy_warehouse_id = #{warehouseId} " +
            "AND inventory_date < #{inventoryDate} AND pol_status = #{polStatus} AND isactive='" + SgConstants.IS_ACTIVE_Y + "' " +
            "AND EXISTS (SELECT 1 FROM " + SgInvConstants.SC_B_INVENTORY_IMP_ITEM + " WHERE PS_C_PRO_ECODE IN (${proEcodes}))")
    List<JSONObject> selectListByInventory(@Param("warehouseId") Long warehouseId, @Param("inventoryDate") Date inventoryDate, @Param("polStatus") Integer polStatus, @Param("proEcodes") String proEcodes);

    @Select("SELECT * FROM \n" +
            SgConstants.SC_B_TRANSFER_DIFF +" diff\n"+
            "\tLEFT JOIN cp_c_store store ON diff.cp_c_store_id_send = store. ID\n" +
            "OR diff.cp_c_store_id = store. ID\n" +
            "WHERE\n" +
            "\tdiff.bill_status = #{status}\n" +
            "AND diff.isactive = '" + SgConstants.IS_ACTIVE_Y + "'\n" +
            "AND store.cp_c_phy_warehouse_id = #{warehouseId}\n" +
            "AND EXISTS (\n" +
            "\tSELECT 1 FROM\n" +
            SgConstants.SC_B_TRANSFER_DIFF_ITEM +
            "\tWHERE ps_c_pro_ecode IN (${proEcodes}))")
    List<JSONObject> selectListByPhyTransferDiff(@Param("warehouseId") Long warehouseId, @Param("status") Integer status, @Param("proEcodes") String proEcodes);

}