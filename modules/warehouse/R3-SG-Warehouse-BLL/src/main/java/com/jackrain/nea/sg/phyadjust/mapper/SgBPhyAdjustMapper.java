package com.jackrain.nea.sg.phyadjust.mapper;

import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.phyadjust.model.table.SgBPhyAdjust;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface SgBPhyAdjustMapper extends ExtentionMapper<SgBPhyAdjust> {

    /**
     * 分页查询库存调整单
     */
    @Select("SELECT * FROM " + SgConstants.SG_B_PHY_ADJUST+ " ORDER BY ID LIMIT #{page} OFFSET #{offset}")
    List<SgBPhyAdjust> selectListByPage(@Param("page") int page, @Param("offset") int offset);
}