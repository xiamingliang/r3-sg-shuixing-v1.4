package com.jackrain.nea.sg.out.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.api.SgPhyStorageBillUpdateCmd;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.config.SgStorageBoxConfig;
import com.jackrain.nea.sg.basic.model.request.*;
import com.jackrain.nea.sg.basic.model.result.SgPhyStorageUpdateResult;
import com.jackrain.nea.sg.out.common.SgOutConstantsIF;
import com.jackrain.nea.sg.out.mapper.*;
import com.jackrain.nea.sg.out.model.request.SgPhyOutDeliverySaveRequest;
import com.jackrain.nea.sg.out.model.request.SgPhyOutResultBillSaveRequest;
import com.jackrain.nea.sg.out.model.request.SgPhyOutResultItemSaveRequest;
import com.jackrain.nea.sg.out.model.request.SgPhyOutResultSaveRequest;
import com.jackrain.nea.sg.out.model.result.SgOutResultItemMqResult;
import com.jackrain.nea.sg.out.model.result.SgOutResultMqResult;
import com.jackrain.nea.sg.out.model.result.SgOutResultSendMsgResult;
import com.jackrain.nea.sg.out.model.result.SgOutResultToJITXItem;
import com.jackrain.nea.sg.out.model.table.*;
import com.jackrain.nea.sg.send.model.request.SgSendBillSubmitRequest;
import com.jackrain.nea.sg.send.model.result.SgSendBillSubmitResult;
import com.jackrain.nea.sg.send.services.SgSendSubmitService;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.*;

/**
 * @author: 舒威
 * @since: 2019/9/2
 * create at : 2019/9/2 14:00
 */
@Slf4j
@Component
public class SgPhyOutRPCService {

    @Autowired
    private SgStorageBoxConfig storageBoxConfig;

    /**
     * 逻辑发货单出库
     *
     * @param outResult      出库结果单
     * @param outResultitems 出库结果单明细
     * @param user           用户
     * @param isRepeatOut
     * @param isDiffDeal     是否差异单
     */
    public ValueHolderV14 sgBSendOut(SgBPhyOutResult outResult, List<SgBPhyOutResultItem> outResultitems,
                                     List<SgBPhyOutResultImpItem> impItems, List<SgBPhyOutResultTeusItem> teusItems,
                                     User user, boolean isRepeatOut, boolean isDiffDeal) {
        long start = System.currentTimeMillis();
        SgSendSubmitService service = ApplicationContextHandle.getBean(SgSendSubmitService.class);
        SgSendBillSubmitRequest request = new SgSendBillSubmitRequest();
        request.setSourceBillId(outResult.getSourceBillId());
        request.setSourceBillType(outResult.getSourceBillType());
        request.setLoginUser(user);
        request.setPhyOutResult(outResult);
        request.setPhyOutResultItems(outResultitems);
        request.setIsRepeatOut(isRepeatOut);
        if (storageBoxConfig.getBoxEnable()) {
            request.setPhyOutResultImpItems(impItems);
            request.setPhyOutResultTeusItems(teusItems);
        }

        // 2020-04-22添加逻辑：来源单据未零售单是 允许负库存
        Integer sourceBillType = outResult.getSourceBillType();
        if (SgConstantsIF.BILL_TYPE_RETAIL == sourceBillType || SgConstantsIF.BILL_TYPE_RETAIL_REF == sourceBillType || isDiffDeal) {
            request.setIsNegativePreout(true);
        }

        ValueHolderV14<SgSendBillSubmitResult> vh = service.submitSgBSend(request);
        if (log.isDebugEnabled()) {
            log.debug("来源单据id[" + outResult.getSourceBillId() + "],出库结果单审核调用逻辑发货单出库服务:spend time:{}ms,result:;"
                    , System.currentTimeMillis() - start, JSONObject.toJSONString(vh));
        }
        return vh;
    }

    /**
     * 修改实体仓库存
     *
     * @param outResult      出库结果单
     * @param outResultitems 出库结果单明细
     * @param user           用户
     */
    public ValueHolderV14 updateSgPhyStorage(SgBPhyOutResult outResult, List<SgBPhyOutResultItem> outResultitems,
                                             List<SgBPhyOutResultImpItem> impItems, List<SgBPhyOutResultTeusItem> teusItems,
                                             User user, String sysParam) {
        Boolean boxEnable = storageBoxConfig.getBoxEnable();
        HashMap<String, BigDecimal> teusSkuQtyMap = new HashMap<>();
        SgPhyStorageUpdateBillRequest billRequest = new SgPhyStorageUpdateBillRequest();
        List<SgPhyTeusStorageUpdateBillItemRequest> teusList = new ArrayList<>();

        //开启箱后，条码拆分总变化量和箱内变化量,以及封装箱信息
        if (boxEnable) {
            HashMap<Long, BigDecimal> teusQtyMap = new HashMap<>();
            for (SgBPhyOutResultImpItem impItem : impItems) {
                if (impItem.getPsCTeusId() != null) {
                    teusQtyMap.put(impItem.getPsCTeusId(), impItem.getQtyOut());
                    //封装箱入参
                    SgPhyTeusStorageUpdateBillItemRequest sgPhyTeusStorageUpdateBillItemRequest = new SgPhyTeusStorageUpdateBillItemRequest();
                    BeanUtils.copyProperties(impItem, sgPhyTeusStorageUpdateBillItemRequest);
                    sgPhyTeusStorageUpdateBillItemRequest.setBillItemId(impItem.getId());
                    sgPhyTeusStorageUpdateBillItemRequest.setQtyChange(impItem.getQtyOut().negate());
                    sgPhyTeusStorageUpdateBillItemRequest.setCpCPhyWarehouseId(outResult.getCpCPhyWarehouseId());
                    sgPhyTeusStorageUpdateBillItemRequest.setCpCPhyWarehouseEcode(outResult.getCpCPhyWarehouseEcode());
                    sgPhyTeusStorageUpdateBillItemRequest.setCpCPhyWarehouseEname(outResult.getCpCPhyWarehouseEname());
                    teusList.add(sgPhyTeusStorageUpdateBillItemRequest);
                }
            }

            billRequest.setTeusList(teusList);

            //合并箱内明细数量
            Map<Long, SgBPhyOutResultTeusItem> requestMap = Maps.newHashMap();
            for (SgBPhyOutResultTeusItem teusItem : teusItems) {
                Long psCSkuId = teusItem.getPsCSkuId();
                if (requestMap.containsKey(psCSkuId)) {
                    SgBPhyOutResultTeusItem orgTeusItem = requestMap.get(psCSkuId);
                    BigDecimal orgQty = Optional.ofNullable(orgTeusItem.getQty()).orElse(BigDecimal.ZERO);
                    BigDecimal qty = Optional.ofNullable(teusItem.getQty()).orElse(BigDecimal.ZERO);
                    orgTeusItem.setQty(qty.add(orgQty));

                } else {
                    requestMap.put(psCSkuId, teusItem);
                }
            }

            //合并后的明细
            List<SgBPhyOutResultTeusItem> mergeTeusList = new ArrayList<>(requestMap.values());
            for (SgBPhyOutResultTeusItem sgBPhyOutResultTeusItem : mergeTeusList) {
                teusSkuQtyMap.put(sgBPhyOutResultTeusItem.getPsCSkuEcode(), sgBPhyOutResultTeusItem.getQty());
            }
        }

        long start = System.currentTimeMillis();
        SgPhyStorageBillUpdateCmd sgPhyStorageBillUpdateCmd = (SgPhyStorageBillUpdateCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(),
                SgPhyStorageBillUpdateCmd.class.getName(),
                SgConstantsIF.GROUP, SgConstantsIF.VERSION);


        SgPhyStorageSingleUpdateRequest request = new SgPhyStorageSingleUpdateRequest();
        billRequest.setBillId(outResult.getId());
        billRequest.setBillNo(outResult.getBillNo());
        billRequest.setSourceBillId(outResult.getSourceBillId());
        billRequest.setSourceBillNo(outResult.getSourceBillNo());
        billRequest.setBillDate(outResult.getBillDate());
        billRequest.setBillType(outResult.getSourceBillType());
        billRequest.setChangeDate(new Timestamp(System.currentTimeMillis()));

        //单据明细信息
        Long warehouseId = outResult.getCpCPhyWarehouseId();
        String warehouseCode = outResult.getCpCPhyWarehouseEcode();
        String warehouseName = outResult.getCpCPhyWarehouseEname();
        List<SgPhyStorageUpdateBillItemRequest> itemList = new ArrayList<>();
        outResultitems.forEach(resultItem -> {
            SgPhyStorageUpdateBillItemRequest item = new SgPhyStorageUpdateBillItemRequest();
            BeanUtils.copyProperties(resultItem, item);
            item.setBillItemId(resultItem.getId());
            item.setCpCPhyWarehouseId(warehouseId);
            item.setCpCPhyWarehouseEcode(warehouseCode);
            item.setCpCPhyWarehouseEname(warehouseName);
            item.setQtyChange(resultItem.getQty().negate());
            if (boxEnable) {
                BigDecimal qtyTeusChange = teusSkuQtyMap.get(resultItem.getPsCSkuEcode());
                if (qtyTeusChange != null) {
                    item.setQtyTeusChange(qtyTeusChange.negate());
                } else {
                    item.setQtyTeusChange(BigDecimal.ZERO);
                }
            }
            itemList.add(item);
        });
        billRequest.setItemList(itemList);

        SgStorageUpdateControlRequest controlModel = new SgStorageUpdateControlRequest();
        controlModel.setNegativeStorage(Boolean.valueOf(sysParam));
        request.setControlModel(controlModel);
        request.setMessageKey(SgConstants.MSG_TAG_PHY_OUT_RESULT + ":" + outResult.getBillNo());
        request.setBill(billRequest);
        request.setLoginUser(user);

        ValueHolderV14<SgPhyStorageUpdateResult> vh = sgPhyStorageBillUpdateCmd.updateStorageBillWithTrans(request);
        if (log.isDebugEnabled()) {
            log.debug("来源单据id[" + outResult.getSourceBillId() + "],出库结果单审核更新实体仓库存:spend time:{}ms,result:;"
                    , System.currentTimeMillis() - start, JSONObject.toJSONString(vh));
        }
        return vh;
    }

    /**
     * 获取出库结果单审核回写mq
     *
     * @param outResult      出库结果单
     * @param outResultitems 出库结果单明细
     * @param user           用户
     */
    public ValueHolderV14<SgOutResultSendMsgResult> getSgPhyOutMQResult(Long outResultId, SgBPhyOutResult outResult, List<SgBPhyOutResultItem> outResultitems
            , List<SgBPhyOutResultImpItem> impItems, List<SgBPhyOutResultTeusItem> teusItems, JSONObject isLastObj, User user) {
        ValueHolderV14<SgOutResultSendMsgResult> vh = new ValueHolderV14<>(ResultCode.SUCCESS, "success");
        if (outResultId == null && (outResult == null || CollectionUtils.isEmpty(outResultitems))) {
            vh.setCode(ResultCode.FAIL);
            vh.setMessage("获取结果单mqbody失败,结果单信息不能为空!");
            return vh;
        }

        SgBPhyOutResult result = outResult;
        List<SgBPhyOutResultItem> items = outResultitems;
        if (result == null) {
            SgBPhyOutResultMapper outResultMapper = ApplicationContextHandle.getBean(SgBPhyOutResultMapper.class);
            result = outResultMapper.selectById(outResultId);
        }
        if (CollectionUtils.isEmpty(outResultitems)) {
            SgBPhyOutResultItemMapper outResultItemMapper = ApplicationContextHandle.getBean(SgBPhyOutResultItemMapper.class);
            items = outResultItemMapper.selectList(new QueryWrapper<SgBPhyOutResultItem>().lambda()
                    .eq(SgBPhyOutResultItem::getSgBPhyOutResultId, outResultId));
        }

        //开启箱功能后，当通过出库结果单id查询时，返回录入明细和箱明细
        List<SgBPhyOutResultImpItem> sgBPhyOutResultImpItems = impItems;
        List<SgBPhyOutResultTeusItem> sgBPhyOutResultTeusItems = teusItems;
        if (storageBoxConfig.getBoxEnable()) {
            if (CollectionUtils.isEmpty(sgBPhyOutResultImpItems)) {
                SgBPhyOutResultImpItemMapper impItemResultMapper = ApplicationContextHandle.getBean(SgBPhyOutResultImpItemMapper.class);
                sgBPhyOutResultImpItems = impItemResultMapper.selectList(new QueryWrapper<SgBPhyOutResultImpItem>()
                        .lambda().eq(SgBPhyOutResultImpItem::getSgBPhyOutResultId, outResultId));
            }

            if (CollectionUtils.isEmpty(sgBPhyOutResultTeusItems)) {
                SgBPhyOutResultTeusItemMapper impItemResultMapper = ApplicationContextHandle.getBean(SgBPhyOutResultTeusItemMapper.class);
                sgBPhyOutResultTeusItems = impItemResultMapper.selectList(new QueryWrapper<SgBPhyOutResultTeusItem>()
                        .lambda().eq(SgBPhyOutResultTeusItem::getSgBPhyOutResultId, outResultId));
            }

        }

        SgOutResultSendMsgResult sendMsgResult = new SgOutResultSendMsgResult();
        SgOutResultMqResult mqResult = new SgOutResultMqResult();
        List<SgOutResultItemMqResult> mqResultItem = new ArrayList<>();

        BeanUtils.copyProperties(result, mqResult);
        if (isLastObj != null) {
            mqResult.setIsLast(isLastObj.getInteger("isLast"));  //是否最后一次出库(根据通知单差异数量帕努的那)
            mqResult.setIsAll(isLastObj.getBoolean("isAll"));
        } else {
            SgBPhyOutNoticesMapper noticesMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesMapper.class);
            SgBPhyOutNotices notices = noticesMapper.selectById(result.getSgBPhyOutNoticesId());
            if (mqResult.getIsLast() == SgOutConstantsIF.OUT_IS_LAST_Y) {
                if (notices.getTotQtyDiff().compareTo(BigDecimal.ZERO) == 0) {
                    mqResult.setIsAll(true);
                }
            } else {
                if (notices.getTotQtyDiff().compareTo(BigDecimal.ZERO) == 0) {
                    // 2019-05-30添加逻辑：如果差异数量等于0，认为全部出库
                    mqResult.setIsLast(SgOutConstantsIF.OUT_IS_LAST_Y);
                    mqResult.setIsAll(true);
                } else {
                    mqResult.setIsAll(false);
                }
            }
        }

        //2019-08-30 结果单回写订单(JITX)新增入库通知、入库结果、逻辑收三张单据 明细增加通知数量、成交价、来源明细id、颜色尺寸等信息
        HashMap<String, SgBPhyOutNoticesItem> map = Maps.newHashMap();
        if (mqResult.getSourceBillType() == SgConstantsIF.BILL_TYPE_RETAIL) {
            SgBPhyOutNoticesItemMapper noticesItemMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesItemMapper.class);
            List<SgBPhyOutNoticesItem> noticesItems = noticesItemMapper.selectList(new QueryWrapper<SgBPhyOutNoticesItem>().lambda()
                    .eq(SgBPhyOutNoticesItem::getSgBPhyOutNoticesId, result.getSgBPhyOutNoticesId())
                    .eq(SgBPhyOutNoticesItem::getIsactive, SgConstants.IS_ACTIVE_Y));
            for (SgBPhyOutNoticesItem noticesItem : noticesItems) {
                map.put(noticesItem.getPsCSkuEcode(), noticesItem);
            }
        }

        items.forEach(item -> {
            SgOutResultItemMqResult qiMenItem = new SgOutResultItemMqResult();
            BeanUtils.copyProperties(item, qiMenItem);
            SgBPhyOutNoticesItem noticesItem = map.get(item.getPsCSkuEcode());
            if (noticesItem != null) {
                Long sourceBillItemId = noticesItem.getSourceBillItemId();
                BigDecimal priceCost = noticesItem.getPriceCost();
                BigDecimal qty = noticesItem.getQty();
                SgOutResultToJITXItem jitxItem = new SgOutResultToJITXItem();
                BeanUtils.copyProperties(item, jitxItem);
                jitxItem.setSourceBillItemId(sourceBillItemId);
                jitxItem.setPriceCost(priceCost);
                jitxItem.setNoticesQty(qty);
                qiMenItem.setJitxItem(jitxItem);
            }
            mqResultItem.add(qiMenItem);
        });

        // 零售订单存在多包裹情况
        if (mqResult.getSourceBillType() == SgConstantsIF.BILL_TYPE_RETAIL) {
            SgBOutDeliveryMapper deliveryMapper = ApplicationContextHandle.getBean(SgBOutDeliveryMapper.class);
            List<SgBOutDelivery> deliveries = deliveryMapper.selectList(
                    new QueryWrapper<SgBOutDelivery>().lambda()
                            .eq(SgBOutDelivery::getSgBPhyOutResultId, result.getId()));

            if (!CollectionUtils.isEmpty(deliveries)) {
                List<SgPhyOutDeliverySaveRequest> deliveryList = new ArrayList<>();
                deliveries.forEach(delivery -> {
                    SgPhyOutDeliverySaveRequest deliverySaveRequest = new SgPhyOutDeliverySaveRequest();
                    BeanUtils.copyProperties(delivery, deliverySaveRequest);
                    deliveryList.add(deliverySaveRequest);
                });
                sendMsgResult.setDeliveryItem(deliveryList);
            }
        }

        sendMsgResult.setMqResult(mqResult);
        sendMsgResult.setMqResultItem(mqResultItem);
        sendMsgResult.setLoginUser(user);
        sendMsgResult.setImpItems(sgBPhyOutResultImpItems);
        sendMsgResult.setTeusItems(sgBPhyOutResultTeusItems);
        vh.setData(sendMsgResult);
        return vh;
    }


    /**
     * 根据通知单id获取新增出库结果单参数
     *
     * @param outNoticesId 出库通知单id
     * @param user         用户
     */
    public ValueHolderV14<SgPhyOutResultBillSaveRequest> getSgPhyOutResultRequest(Long outNoticesId, User user) {
        ValueHolderV14<SgPhyOutResultBillSaveRequest> vh = new ValueHolderV14<>(ResultCode.SUCCESS, "success");
        if (outNoticesId == null || user == null) {
            vh.setCode(ResultCode.FAIL);
            vh.setMessage("通知单id或用户为空!");
            return vh;
        }
        SgBPhyOutNoticesMapper outNoticesMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesMapper.class);
        SgBPhyOutNoticesItemMapper outNoticesItemMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesItemMapper.class);
        List<SgBPhyOutNotices> notices = outNoticesMapper.selectList(new QueryWrapper<SgBPhyOutNotices>().lambda()
                .eq(SgBPhyOutNotices::getId, outNoticesId)
                .eq(SgBPhyOutNotices::getIsactive, SgConstants.IS_ACTIVE_Y));
        if (CollectionUtils.isEmpty(notices)) {
            vh.setCode(ResultCode.FAIL);
            vh.setMessage("objid:" + outNoticesId + "出库通知单不存在!");
            return vh;
        }
        List<SgBPhyOutNoticesItem> outNoticesItems = outNoticesItemMapper.selectList(new QueryWrapper<SgBPhyOutNoticesItem>().lambda()
                .eq(SgBPhyOutNoticesItem::getSgBPhyOutNoticesId, outNoticesId));
        if (CollectionUtils.isEmpty(outNoticesItems)) {
            vh.setCode(ResultCode.FAIL);
            vh.setMessage("objid:" + outNoticesId + "出库通知单明细为空!");
            return vh;
        }
        SgBPhyOutNotices outNotices = notices.get(0);
        //构造主表信息
        SgPhyOutResultSaveRequest outResultSaveRequest = new SgPhyOutResultSaveRequest();
        BeanUtils.copyProperties(outNotices, outResultSaveRequest);
        outResultSaveRequest.setId(null);
        outResultSaveRequest.setTotQtyOut(outNotices.getTotQty());
        outResultSaveRequest.setSgBPhyOutNoticesId(outNotices.getId());
        outResultSaveRequest.setSgBPhyOutNoticesBillno(outNotices.getBillNo());
        //构造明细信息
        List<SgPhyOutResultItemSaveRequest> outResultItemSaveRequests = Lists.newArrayList();
        for (SgBPhyOutNoticesItem outNoticesItem : outNoticesItems) {
            SgPhyOutResultItemSaveRequest itemSaveRequest = new SgPhyOutResultItemSaveRequest();
            BeanUtils.copyProperties(outNoticesItem, itemSaveRequest);
            itemSaveRequest.setId(-1L);
            itemSaveRequest.setSgBPhyOutNoticesItemId(outNoticesItem.getId());
            outResultItemSaveRequests.add(itemSaveRequest);
        }
        SgPhyOutResultBillSaveRequest data = new SgPhyOutResultBillSaveRequest();
        data.setOutResultRequest(outResultSaveRequest);
        data.setOutResultItemRequests(outResultItemSaveRequests);
        data.setLoginUser(user);
        data.setObjId(-1L);
        vh.setData(data);
        return vh;
    }
}
