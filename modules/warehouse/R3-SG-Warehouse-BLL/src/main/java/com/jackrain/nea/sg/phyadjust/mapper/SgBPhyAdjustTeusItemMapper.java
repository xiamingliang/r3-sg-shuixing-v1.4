package com.jackrain.nea.sg.phyadjust.mapper;

import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.phyadjust.model.table.SgBPhyAdjustTeusItem;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SgBPhyAdjustTeusItemMapper extends ExtentionMapper<SgBPhyAdjustTeusItem> {
}