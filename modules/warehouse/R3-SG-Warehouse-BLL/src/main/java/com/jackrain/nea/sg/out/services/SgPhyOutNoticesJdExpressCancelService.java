/*
package com.jackrain.nea.sg.out.services;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.cpext.api.GeneralOrganizationCmd;
import com.jackrain.nea.cpext.model.table.CpShop;
import com.jackrain.nea.ip.api.jingdong.JdOrderServiceCmd;
import com.jackrain.nea.ip.model.jingdong.ReceiveOrderInterceptRequest;
import com.jackrain.nea.sg.out.mapper.SgBPhyOutNoticesMapper;
import com.jackrain.nea.sg.out.model.request.SgOutNoticesJdCancelMsgRequest;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutNotices;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.utility.ExceptionUtil;
import com.jackrain.nea.utils.AssertUtils;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

*/
/**
 * @Author chenjinjun
 * @Description 京东快递取消服务
 * @Date  2019-10-9
 **//*

@Slf4j
@Component
public class SgPhyOutNoticesJdExpressCancelService {
    @Reference(group = "cp-ext",version = "1.0")
    private GeneralOrganizationCmd generalOrganizationCmd;

    @Reference(group = "ip", version = "1.4.0")
    private JdOrderServiceCmd jdOrderServiceCmd;
    @Autowired
    private SgBPhyOutNoticesMapper sgBPhyOutNoticesMapper;


    public ValueHolderV14 cancelJdExpressList(SgOutNoticesJdCancelMsgRequest request) {
        AssertUtils.notNull(request, "请求参数为空!");
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ".debug, param:" + JSONObject.toJSONString(request));
        }
        SgPhyOutNoticesJdExpressCancelService service = ApplicationContextHandle.getBean(SgPhyOutNoticesJdExpressCancelService.class);
        ValueHolderV14 valueHolder = new ValueHolderV14(ResultCode.SUCCESS, "");
        for (Long id : request.getResultList()) {
            SgBPhyOutNotices outNotices = new SgBPhyOutNotices();
            try {
                //通过ID查询出库通知单
                outNotices = sgBPhyOutNoticesMapper.selectById(id);
                if (null == outNotices){
                    String err = "; 出库通知单[" + id + "]";
                    valueHolder.setMessage(valueHolder.getMessage().concat(err));
                    continue;
                }
                //调用取消方法
                ValueHolderV14 v14 = service.cancelJdExpress(outNotices);
                //调用更新状态和信息方法
                updateCancleFlag(outNotices,v14.getCode(),v14.getMessage(),request.getLoginUser());
                if (!v14.isOK()) {
                    if (log.isDebugEnabled()) {
                        log.debug(outNotices.getBillNo()+" SgPhyOutNoticesJdExpressCancelService.cancelJdExpressList:"
                                + JSONObject.toJSONString(v14));
                    }
                    String err = "; 出库通知单[" + outNotices.getBillNo() + "],调用JD快递取消出错:" + v14.getMessage();
                    valueHolder.setMessage(valueHolder.getMessage().concat(err));
                }
            } catch (Exception e) {
                if (log.isDebugEnabled()) {
                    log.debug(outNotices.getBillNo()+" SgPhyOutNoticesSaveFaceService.Exception:"
                            + JSONObject.toJSONString(ExceptionUtil.getMessage(e)));
                }
                String err = "; 出库通知单[" + outNotices.getBillNo() + "],调用JD快递取消异常:" + e.getMessage();
                log.error(this.getClass().getName() + ".error:" + e.getMessage(), e);
                valueHolder.setCode(ResultCode.FAIL);
                valueHolder.setMessage(valueHolder.getMessage().concat(err));
            }
        }
        return valueHolder;
    }

    */
/**
     * @Author chenjinjun
     * @Description  京东快递取消
     * @Date  2019-9-25
     * @Param [sgBPhyOutNotices]
     * @return ValueHolderV14
     **//*

    public ValueHolderV14 cancelJdExpress(SgBPhyOutNotices sgBPhyOutNotices){
        ValueHolderV14 result = new ValueHolderV14();
        Long outNoticesId = -1L;
        try{
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName()+"【京东快递取消日志】作废出库通知单："+JSON.toJSONString(sgBPhyOutNotices));
            }
            if (null == sgBPhyOutNotices){
                if (log.isDebugEnabled()) {
                    log.debug(this.getClass().getName()+"【京东快递取消日志】出库通知单为空！");
                }
                result.setCode(ResultCode.FAIL);
                result.setMessage("出库通知单为空");
                return result;
            }
            //作废出库通知单，满足条件：快递公司为京东快递并存在运单号，出库通知单作废成功后发送MQ京东快递的取消接口
            //通过物流编码判断是否使用京东快递
            //快递编码
            outNoticesId = sgBPhyOutNotices.getId();
            String logisticsCode = sgBPhyOutNotices.getCpCLogisticsEcode();
            if (StringUtils.isBlank(logisticsCode)){
                if (log.isDebugEnabled()) {
                    log.debug(this.getClass().getName()+"【京东快递取消日志】快递编码为空，出库通知单ID："+outNoticesId);
                }
                result.setCode(ResultCode.FAIL);
                result.setMessage("ID="+outNoticesId+"出库通知单快递编码为空！");
                return result;
            }
            //判断是否分配运单号
            String expressCode = sgBPhyOutNotices.getLogisticNumber();
            if (StringUtils.isBlank(expressCode)){
                if (log.isDebugEnabled()) {
                    log.debug(this.getClass().getName()+"【京东快递取消日志】运单号不存在，出库通知单ID："+outNoticesId);
                }
                result.setCode(ResultCode.FAIL);
                result.setMessage("ID="+outNoticesId+"出库通知单运单号不存在！");
                return result;
            }
            //获取店铺ID
            Long shopId = sgBPhyOutNotices.getCpCShopId();
            if (null == shopId){
                if (log.isDebugEnabled()) {
                    log.debug(this.getClass().getName()+"【京东快递取消日志】店铺ID不存在，出库通知单ID："+outNoticesId);
                }
                result.setCode(ResultCode.FAIL);
                result.setMessage("ID="+outNoticesId+"出库通知单店铺ID不存在！");
                return result;
            }
            //查询店铺信息
            CpShop cpShop = generalOrganizationCmd.queryShopById(shopId);
            if (null == cpShop){
                if (log.isDebugEnabled()) {
                    log.debug(this.getClass().getName()+"【京东快递取消日志】查询店铺信息不存在，店铺ID："+shopId);
                }
                result.setCode(ResultCode.FAIL);
                result.setMessage("ID="+outNoticesId+",shopId = "+shopId+"出库通知单查询店铺信息不存在！");
                return result;
            }
            //青龙账号信息
            String qingLongCode = cpShop.getQinglongCode();
            if (StringUtils.isBlank(qingLongCode)){
                if (log.isDebugEnabled()) {
                    log.debug(this.getClass().getName()+"【京东快递取消日志】店铺青龙账号不存在，店铺ID："+shopId);
                }
                result.setCode(ResultCode.FAIL);
                result.setMessage("ID="+outNoticesId+",shopId = "+shopId+"出库通知单店铺青龙账号不存在！");
                return result;
            }
            //卖家昵称
            String selleNick = cpShop.getSellerNick();
            if (StringUtils.isBlank(selleNick)){
                if (log.isDebugEnabled()) {
                    log.debug(this.getClass().getName()+"【京东快递取消日志】店铺卖家昵称不存在，店铺ID："+shopId);
                }
                result.setCode(ResultCode.FAIL);
                result.setMessage("ID="+outNoticesId+",shopId = "+shopId+"出库通知单店铺卖家昵称不存在！");
                return result;
            }
            //请求Bean
            ReceiveOrderInterceptRequest receiveOrderInterceptRequest = new ReceiveOrderInterceptRequest();
            receiveOrderInterceptRequest.setDeliveryId(expressCode);
            receiveOrderInterceptRequest.setInterceptReason("订单修改地址或退款");
            receiveOrderInterceptRequest.setVendorCode(qingLongCode);
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName()+"【京东快递取消日志】调用接口入参："+ JSON.toJSONString(receiveOrderInterceptRequest));
                log.debug(this.getClass().getName()+"【京东快递取消日志】调用接口卖家昵称入参："+ selleNick);
            }
            //调用接口RPC
            ValueHolderV14 vh14 = jdOrderServiceCmd.receiveOrderIntercept(receiveOrderInterceptRequest,selleNick);
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName()+"【京东快递取消日志】调用接口出参："+ JSON.toJSONString(vh14));
            }
            if(vh14.getCode() == ResultCode.SUCCESS){
                if (log.isDebugEnabled()) {
                    log.debug(this.getClass().getName()+"【京东快递取消日志】调用京东快递取消接口成功！");
                }
                result.setCode(ResultCode.SUCCESS);
                result.setMessage("调用京东快递取消接口成功");
                return result;
            }else{
                if (log.isDebugEnabled()) {
                    log.debug(this.getClass().getName()+"【京东快递取消日志】调用京东快递取消接口返回失败，失败信息："+vh14.getMessage());
                }
                result.setCode(ResultCode.FAIL);
                result.setMessage("ID="+outNoticesId+"出库通知单调用京东快递取消接口返回失败，失败信息："+vh14.getMessage());
                return result;
            }
        }catch (Exception e){
            e.printStackTrace();
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName()+"【京东快递取消日志】调用京东快递取消接口异常，异常信息："+e.getMessage());
            }
            result.setCode(ResultCode.FAIL);
            result.setMessage("ID="+outNoticesId+"出库通知单调用京东快递取消接口异常，异常信息："+e.getMessage());
            return result;
        }
    }
    */
/**
     * @Author chenjinjun
     * @Description 更新出库通知单京东快递取消状态
     * @Date  2019-10-9
     * @Param [sgBPhyOutNotices]
     * @return void
     **//*

    @Transactional(rollbackFor = Exception.class)
    public void updateCancleFlag(SgBPhyOutNotices sgBPhyOutNotices, int status, String message, User user){
        if(null == sgBPhyOutNotices){
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName()+"【京东快递取消日志】无对应出库通知单！");
            }
            return;
        }
        SgBPhyOutNotices sgBPhyOutNoticesDo = new SgBPhyOutNotices();
        sgBPhyOutNoticesDo.setId(sgBPhyOutNotices.getId());
        sgBPhyOutNoticesDo.setJdExpressCancelStatus(status);
        sgBPhyOutNoticesDo.setModifierid(Long.valueOf(user.getId()));
        sgBPhyOutNoticesDo.setModifierename(user.getEname());
        sgBPhyOutNoticesDo.setModifiername(user.getName());
        sgBPhyOutNoticesDo.setModifieddate(new Date());
        sgBPhyOutNoticesDo.setJdExpressCancelInfo(message.length()>200?message.substring(0,199):message);
        try{
            sgBPhyOutNoticesMapper.updateById(sgBPhyOutNoticesDo);
        }catch (Exception e){
            e.printStackTrace();
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName()+"【京东快递取消日志】更新出库通知单京东快递取消状态，异常信息："+e.getMessage());
            }
        }
    }
}
*/
