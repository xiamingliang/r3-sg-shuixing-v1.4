package com.jackrain.nea.sg.phyadjust.services;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.oc.basic.common.OcBasicConstants;
import com.jackrain.nea.sg.adjust.model.request.SgAdjustBillSaveRequest;
import com.jackrain.nea.sg.adjust.model.request.SgAdjustImpItemSaveRequest;
import com.jackrain.nea.sg.adjust.model.request.SgAdjustItemSaveRequest;
import com.jackrain.nea.sg.adjust.model.request.SgAdjustSaveRequest;
import com.jackrain.nea.sg.adjust.services.SgAdjSaveAndAuditService;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.config.SgStorageBoxConfig;
import com.jackrain.nea.sg.basic.model.request.SgQtyStorageByWareCalcItemRequest;
import com.jackrain.nea.sg.basic.model.request.SgQtyStorageByWareCalcRequest;
import com.jackrain.nea.sg.basic.model.request.SgQtyTeusStorageByWareCalcItemRequest;
import com.jackrain.nea.sg.basic.model.result.SgQtyStorageByWareCalcItemResult;
import com.jackrain.nea.sg.basic.model.result.SgQtyStorageByWareCalcResult;
import com.jackrain.nea.sg.basic.model.result.SgQtyTeusStorageByWareCalcItemResult;
import com.jackrain.nea.sg.basic.services.SgStorageQtyCalculateService;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.phyadjust.common.SgPhyAdjustConstants;
import com.jackrain.nea.sg.phyadjust.model.request.SgPhyAdjustItemStoreRequest;
import com.jackrain.nea.sg.phyadjust.model.request.SgPhyAdjustStoreRequest;
import com.jackrain.nea.sg.phyadjust.model.result.SgPhyAdjustBillQueryResult;
import com.jackrain.nea.sg.phyadjust.model.table.SgBPhyAdjust;
import com.jackrain.nea.sg.phyadjust.model.table.SgBPhyAdjustImpItem;
import com.jackrain.nea.sg.phyadjust.model.table.SgBPhyAdjustItem;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author csy
 * Date: 2019/4/30
 * Description:
 */


@Slf4j
@Component
public class SgPhyAdjustGenerateLogicAdjService {

    @Autowired
    private SgStorageBoxConfig storageBoxConfig;

    /**
     * 生成 逻辑调整单
     *
     * @param queryResult 封装对象
     * @param user        用户
     */
    public void generateLogicAdj(SgPhyAdjustBillQueryResult queryResult, User user, Locale locale, SgPhyAdjustStoreRequest storeRequest) {
        SgBPhyAdjust adjust = queryResult.getAdjust();
        List<SgBPhyAdjustItem> items = queryResult.getAdjustItems();
        AssertUtils.cannot(CollectionUtils.isEmpty(items), "明细为空，不生成逻辑调整单!", locale);

        //封装新增并审核逻辑调整单入参
        SgAdjustBillSaveRequest request = new SgAdjustBillSaveRequest();
        SgAdjustSaveRequest adjRequest = createAdjSaveRequest(adjust);
        if (storeRequest == null || (storeRequest.getCpCStoreId() == null && CollectionUtils.isEmpty(storeRequest.getItemStoreList()))) {
            SgQtyStorageByWareCalcResult resultData = queryStrorageQtyCalculate(queryResult, user);
            createAdjItemSaveRequest(queryResult.getAdjustItems(), queryResult.getAdjustImpItems(), resultData, request);
        } else {
            createAdjItemSaveRequest(queryResult.getAdjustItems(), queryResult.getAdjustImpItems(), request, storeRequest);
        }
        request.setObjId(-1L);
        request.setSgBAdjust(adjRequest);
        request.setLoginUser(user);
        try {
            SgAdjSaveAndAuditService service = ApplicationContextHandle.getBean(SgAdjSaveAndAuditService.class);
            ValueHolderV14 holderV14 = service.saveAndAudit(request);
            if (holderV14.getCode() == ResultCode.FAIL) {
                AssertUtils.logAndThrow("逻辑调整单新增审核失败：" + holderV14.getMessage(), locale);
            }
        } catch (NDSException e) {
            AssertUtils.logAndThrow("逻辑调整单新增审核异常" + e.getMessage(), locale);
        }
    }

    /**
     * 寻仓
     */
    private SgQtyStorageByWareCalcResult queryStrorageQtyCalculate(SgPhyAdjustBillQueryResult queryResult, User user) {
        SgBPhyAdjust adjust = queryResult.getAdjust();
        List<SgBPhyAdjustItem> items = queryResult.getAdjustItems();

        Long warehouseId = adjust.getCpCPhyWarehouseId();
        String warehouseECode = adjust.getCpCPhyWarehouseEcode();

        //寻仓 -查找对应逻辑仓库存 变动
        Long startTime = System.currentTimeMillis();
        SgStorageQtyCalculateService calculateService = ApplicationContextHandle.getBean(SgStorageQtyCalculateService.class);
        List<SgQtyStorageByWareCalcItemRequest> calcItemRequests = new ArrayList<>();
        HashMap<Long, SgBPhyAdjustItem> origMap = new HashMap<>(64);
        items.forEach(item -> {
            SgQtyStorageByWareCalcItemRequest calcItemRequest = new SgQtyStorageByWareCalcItemRequest();
            calcItemRequest.setPsCSkuId(item.getPsCSkuId());
            calcItemRequest.setPsCSkuEcode(item.getPsCSkuEcode());
            calcItemRequest.setQtyChange(item.getQty());
            calcItemRequests.add(calcItemRequest);
            origMap.put(item.getPsCSkuId(), item);
        });
        SgQtyStorageByWareCalcRequest calcRequest = new SgQtyStorageByWareCalcRequest();
        calcRequest.setItemList(calcItemRequests);
        calcRequest.setSupplyPhyWarehouseEcode(warehouseECode);
        calcRequest.setSupplyPhyWarehouseId(warehouseId);
        calcRequest.setSupplyStoreId(adjust.getCpCStoreId());
        calcRequest.setSupplyStoreEcode(adjust.getCpCStoreEcode());
        calcRequest.setSupplyStoreEname(adjust.getCpCStoreEname());
        calcRequest.setTradeMark(adjust.getTradeMark());
        calcRequest.setLoginUser(user);

        if (storageBoxConfig.getBoxEnable()) {
            List<SgQtyTeusStorageByWareCalcItemRequest> teusList = Lists.newArrayList();
            queryResult.getAdjustImpItems().stream()
                    .filter(o -> o.getIsTeus() != null && o.getIsTeus().equals(OcBasicConstants.IS_MATCH_SIZE_Y))
                    .forEach(o -> {
                        SgQtyTeusStorageByWareCalcItemRequest calcItemRequest = new SgQtyTeusStorageByWareCalcItemRequest();
                        calcItemRequest.setPsCTeusId(o.getPsCTeusId());
                        calcItemRequest.setPsCTeusEcode(o.getPsCTeusEcode());
                        calcItemRequest.setQtyChange(o.getQty());
                        teusList.add(calcItemRequest);
                    });
            calcRequest.setTeusList(teusList);
        }

        //2019-09-05 若调整性质为无头件或冲无头件，开启实体仓负库存
        if (adjust.getSgBAdjustPropId() != null && (adjust.getSgBAdjustPropId() == SgPhyAdjustConstants.ADJUST_PROP_NOHEAD ||
                adjust.getSgBAdjustPropId() == SgPhyAdjustConstants.ADJUST_PROP_FLUSH_NOHEAD ||
                adjust.getSgBAdjustPropId() == SgPhyAdjustConstants.ADJUST_PROP_WRONG_ADJUST)) {
            calcRequest.setIsNegativeAvailable(true);
        }
        ValueHolderV14<SgQtyStorageByWareCalcResult> calcResult = calculateService.calcSgQtyStorageByWare(calcRequest);
        if (log.isDebugEnabled()) {
            log.debug("Finish SgStorageQtyCalculateService.calcSgQtyStorageByWare. Return:Results:{} spend time:{};",
                    calcResult.toJSONObject(), System.currentTimeMillis() - startTime);
        }
        AssertUtils.cannot(calcResult.getCode() != ResultCode.SUCCESS, "逻辑仓库存计算异常:" + calcResult.getMessage(), user.getLocale());
        AssertUtils.cannot(calcResult.getData().getChangeUpdateResult() != SgConstantsIF.PREOUT_RESULT_SUCCESS, "逻辑仓库存计算异常:" + calcResult.getMessage(), user.getLocale());

        SgQtyStorageByWareCalcResult resultData = calcResult.getData();
        if (resultData == null || CollectionUtils.isEmpty(resultData.getItemResultList())) {
            throw new NDSException(Resources.getMessage("逻辑仓库存计算返回data为空", user.getLocale()));
        }
        return resultData;
    }

    /**
     * 生成 逻辑调整单主表信息
     */
    private SgAdjustSaveRequest createAdjSaveRequest(SgBPhyAdjust adjust) {
        SgAdjustSaveRequest request = new SgAdjustSaveRequest();
        BeanUtils.copyProperties(adjust, request);
        request.setBillNo(null);
        request.setSgBPhyAdjustId(adjust.getId());
        request.setSgBPhyAdjustBillNo(adjust.getBillNo());
        return request;
    }

    /**
     * 生成 逻辑调整单明细信息(寻仓)
     */
    private void createAdjItemSaveRequest(List<SgBPhyAdjustItem> adjustItems,
                                          List<SgBPhyAdjustImpItem> adjustImpItems,
                                          SgQtyStorageByWareCalcResult resultData,
                                          SgAdjustBillSaveRequest request) {
        //寻仓结果可能会分仓
        Map<Long, List<SgQtyStorageByWareCalcItemResult>> calcItemMap = resultData.getItemResultList().stream()
                .collect(Collectors.groupingBy(SgQtyStorageByWareCalcItemResult::getPsCSkuId));

        //根据库存调整单条码明细 来组装逻辑调整单录入明细
        if (storageBoxConfig.getBoxEnable()) {
            Map<Long, List<SgQtyTeusStorageByWareCalcItemResult>> calcTeusItemMap = CollectionUtils.isEmpty(resultData.getTeusResultList()) ?
                    Maps.newHashMap() : resultData.getTeusResultList().stream()
                    .collect(Collectors.groupingBy(SgQtyTeusStorageByWareCalcItemResult::getPsCTeusId));

            List<SgAdjustImpItemSaveRequest> impItemsRequest = new ArrayList<>();
            adjustImpItems.forEach(phyAdjustImpItem -> {
                if (phyAdjustImpItem.getIsTeus().equals(OcBasicConstants.IS_MATCH_SIZE_N)) {
                    List<SgQtyStorageByWareCalcItemResult> calcItemResults = calcItemMap.get(phyAdjustImpItem.getPsCSkuId());
                    AssertUtils.isNotEmpty("调整单寻仓失败,条码id[" + phyAdjustImpItem.getPsCSkuId() + "],未返回寻仓结果!", calcItemResults);
                    /*
                     * 1.若散码数量<=寻仓返回数量(寻仓list.size=1)，逻辑调整单对应就一行明细，调整数量为库存调整单录入明细散码数量
                     * 2.若散码数量<=寻仓返回数量(寻仓list.size>1)，逻辑调整单对应多行明细，优先分配散码，多余分配至箱内明细
                     * 3.若散码数量>寻仓返回数量，库存不足，报错
                     **/
                    //库存调整单录入明细散码数量
                    BigDecimal qty = phyAdjustImpItem.getQty();
                    for (SgQtyStorageByWareCalcItemResult calcItemResult : calcItemResults) {
                        SgAdjustImpItemSaveRequest itemSaveRequest = new SgAdjustImpItemSaveRequest();
                        BeanUtils.copyProperties(phyAdjustImpItem, itemSaveRequest);
                        itemSaveRequest.setId(-1L);
                        itemSaveRequest.setCpCStoreId(calcItemResult.getCpCStoreId());
                        itemSaveRequest.setCpCStoreEcode(calcItemResult.getCpCStoreEcode());
                        itemSaveRequest.setCpCStoreEname(calcItemResult.getCpCStoreEname());
                        //寻仓返回数量
                        BigDecimal qtyStorage = Optional.ofNullable(calcItemResult.getQtyStorage()).orElse(BigDecimal.ZERO);
                        BigDecimal subtractQty = qty.abs().subtract(qtyStorage.abs());
                        if (BigDecimal.ZERO.compareTo(subtractQty) >= 0) {
                            itemSaveRequest.setQty(qty);
                            impItemsRequest.add(itemSaveRequest);
                            break;
                        } else {
                            itemSaveRequest.setQty(calcItemResult.getQtyStorage());
                            impItemsRequest.add(itemSaveRequest);
                            qty = subtractQty;
                        }
                    }
                } else {
                    List<SgQtyTeusStorageByWareCalcItemResult> calcTeusItemResults = calcTeusItemMap.get(phyAdjustImpItem.getPsCTeusId());
                    AssertUtils.isNotEmpty("调整单寻仓失败,箱号[" + phyAdjustImpItem.getPsCTeusEcode() + "],未返回寻仓结果!", calcTeusItemResults);
                    for (SgQtyTeusStorageByWareCalcItemResult calcTeusItemResult : calcTeusItemResults) {
                        SgAdjustImpItemSaveRequest itemSaveRequest = new SgAdjustImpItemSaveRequest();
                        BeanUtils.copyProperties(phyAdjustImpItem, itemSaveRequest);
                        itemSaveRequest.setId(-1L);
                        itemSaveRequest.setCpCStoreId(calcTeusItemResult.getCpCStoreId());
                        itemSaveRequest.setCpCStoreEcode(calcTeusItemResult.getCpCStoreEcode());
                        itemSaveRequest.setCpCStoreEname(calcTeusItemResult.getCpCStoreEname());
                        itemSaveRequest.setQty(calcTeusItemResult.getQtyStorage());
                        //TODO 需要db表加字段 关联库存调整单录入明细id
                        //itemSaveRequest.setSgBPhyAdjustItemId(phyAdjustItem.getId());
                        impItemsRequest.add(itemSaveRequest);
                    }
                }
            });
            request.setImpItems(impItemsRequest);
        } else {
            //根据库存调整单条码明细 来组装逻辑调整单条码明细
            List<SgAdjustItemSaveRequest> itemsRequest = new ArrayList<>();
            adjustItems.forEach(phyAdjustItem -> {
                List<SgQtyStorageByWareCalcItemResult> calcItemResults = calcItemMap.get(phyAdjustItem.getPsCSkuId());
                AssertUtils.isNotEmpty("调整单寻仓失败,条码id[" + phyAdjustItem.getPsCSkuId() + "],未返回寻仓结果!", calcItemResults);
                for (SgQtyStorageByWareCalcItemResult calcItemResult : calcItemResults) {
                    SgAdjustItemSaveRequest itemSaveRequest = new SgAdjustItemSaveRequest();
                    BeanUtils.copyProperties(phyAdjustItem, itemSaveRequest);
                    itemSaveRequest.setId(-1L);
                    itemSaveRequest.setCpCStoreId(calcItemResult.getCpCStoreId());
                    itemSaveRequest.setCpCStoreEcode(calcItemResult.getCpCStoreEcode());
                    itemSaveRequest.setCpCStoreEname(calcItemResult.getCpCStoreEname());
                    itemSaveRequest.setQty(calcItemResult.getQtyStorage());
                    itemSaveRequest.setSgBPhyAdjustItemId(phyAdjustItem.getId());
                    itemsRequest.add(itemSaveRequest);
                }
            });
            request.setItems(itemsRequest);
        }
    }

    /**
     * 生成 逻辑调整单明细信息
     */
    private void createAdjItemSaveRequest(List<SgBPhyAdjustItem> adjustItems, List<SgBPhyAdjustImpItem> adjustImpItems, SgAdjustBillSaveRequest request, SgPhyAdjustStoreRequest storeRequest) {
        //根据库存调整单条码明细 来组装逻辑调整单录入明细
        if (storageBoxConfig.getBoxEnable()) {
            List<SgAdjustImpItemSaveRequest> impItemsRequest = new ArrayList<>();
            for (SgBPhyAdjustImpItem phyAdjustImpItem : adjustImpItems) {
                SgAdjustImpItemSaveRequest itemSaveRequest = new SgAdjustImpItemSaveRequest();
                BeanUtils.copyProperties(phyAdjustImpItem, itemSaveRequest);
                itemSaveRequest.setId(-1L);
                impItemsRequest.add(itemSaveRequest);
            }
            packagePhyAdjustItem(null, impItemsRequest, storeRequest);
            request.setImpItems(impItemsRequest);
        } else {
            //根据库存调整单条码明细 来组装逻辑调整单条码明细
            List<SgAdjustItemSaveRequest> itemsRequest = new ArrayList<>();
            for (SgBPhyAdjustItem phyAdjustItem : adjustItems) {
                SgAdjustItemSaveRequest itemSaveRequest = new SgAdjustItemSaveRequest();
                BeanUtils.copyProperties(phyAdjustItem, itemSaveRequest);
                itemSaveRequest.setId(-1L);
                itemSaveRequest.setSgBPhyAdjustItemId(phyAdjustItem.getId());
                itemsRequest.add(itemSaveRequest);
            }
            packagePhyAdjustItem(itemsRequest, null, storeRequest);
            request.setItems(itemsRequest);
        }
    }

    private void packagePhyAdjustItem(List<SgAdjustItemSaveRequest> itemSaveRequests, List<SgAdjustImpItemSaveRequest> impItemSaveRequests, SgPhyAdjustStoreRequest storeRequest) {
        List<SgPhyAdjustItemStoreRequest> itemStoreList = storeRequest.getItemStoreList();
        if (CollectionUtils.isEmpty(itemStoreList)) {
            if (CollectionUtils.isNotEmpty(itemSaveRequests)) {
                itemSaveRequests.forEach(o -> {
                    o.setCpCStoreId(storeRequest.getCpCStoreId());
                    o.setCpCStoreEcode(storeRequest.getCpCStoreEcode());
                    o.setCpCStoreEname(storeRequest.getCpCStoreEname());
                });
            }
            if (CollectionUtils.isNotEmpty(impItemSaveRequests)) {
                impItemSaveRequests.forEach(o -> {
                    o.setCpCStoreId(storeRequest.getCpCStoreId());
                    o.setCpCStoreEcode(storeRequest.getCpCStoreEcode());
                    o.setCpCStoreEname(storeRequest.getCpCStoreEname());
                });
            }
        } else {
            Map<Long, SgPhyAdjustItemStoreRequest> teusStoreMap = itemStoreList.stream()
                    .filter(o -> o.getIsTeus().equals(OcBasicConstants.IS_MATCH_SIZE_Y))
                    .collect(Collectors.toMap(SgPhyAdjustItemStoreRequest::getPsCTeusId, o -> o));
            Map<Long, SgPhyAdjustItemStoreRequest> skuStoreMap = itemStoreList.stream()
                    .filter(o -> o.getIsTeus().equals(OcBasicConstants.IS_MATCH_SIZE_N))
                    .collect(Collectors.toMap(SgPhyAdjustItemStoreRequest::getPsCSkuId, o -> o));
            if (CollectionUtils.isNotEmpty(itemSaveRequests)) {
                itemSaveRequests.forEach(o -> {
                    SgPhyAdjustItemStoreRequest itemStoreRequest = skuStoreMap.get(o.getPsCSkuId());
                    if (itemStoreRequest == null) {
                        AssertUtils.logAndThrow("条码:" + o.getPsCSkuEcode() + "未指定逻辑仓！");
                    }
                    o.setCpCStoreId(itemStoreRequest.getCpCStoreId());
                    o.setCpCStoreEcode(itemStoreRequest.getCpCStoreEcode());
                    o.setCpCStoreEname(itemStoreRequest.getCpCStoreEname());
                });
            }
            if (CollectionUtils.isNotEmpty(impItemSaveRequests)) {
                impItemSaveRequests.forEach(o -> {
                    SgPhyAdjustItemStoreRequest itemStoreRequest;
                    if (o.getIsTeus().equals(OcBasicConstants.IS_MATCH_SIZE_Y)) {
                        itemStoreRequest = teusStoreMap.get(o.getPsCTeusId());
                        if (itemStoreRequest == null) {
                            AssertUtils.logAndThrow("箱号:" + o.getPsCTeusEcode() + "未指定逻辑仓！");
                        }
                    } else {
                        itemStoreRequest = skuStoreMap.get(o.getPsCSkuId());
                        if (itemStoreRequest == null) {
                            AssertUtils.logAndThrow("条码:" + o.getPsCSkuEcode() + "未指定逻辑仓！");
                        }
                    }
                    o.setCpCStoreId(itemStoreRequest.getCpCStoreId());
                    o.setCpCStoreEcode(itemStoreRequest.getCpCStoreEcode());
                    o.setCpCStoreEname(itemStoreRequest.getCpCStoreEname());
                });
            }
        }
    }

}
