package com.jackrain.nea.sg.unpack;

import com.jackrain.nea.sg.unpack.common.SgUnpackConstants;
import com.jackrain.nea.sg.unpack.filter.SgUnpackAuditFilter;
import com.jackrain.nea.sg.unpack.filter.SgUnpackItemDelFilter;
import com.jackrain.nea.sg.unpack.filter.SgUnpackSaveFilter;
import com.jackrain.nea.sg.unpack.validate.SgUnpackAuditValidate;
import com.jackrain.nea.sg.unpack.validate.SgUnpackItemDelValidate;
import com.jackrain.nea.sg.unpack.validate.SgUnpackSaveValidate;
import com.jackrain.nea.sg.unpack.validate.SgUnpackVoidValidate;
import com.jackrain.nea.tableService.Feature;
import com.jackrain.nea.tableService.constants.Constants;
import com.jackrain.nea.tableService.feature.FeatureAnnotation;
import com.jackrain.nea.util.ApplicationContextHandle;

/**
 * @author zhoulinsheng
 * @since 2019/11/27 14:02
 */
@FeatureAnnotation(value = "SgUnpackFeature", description = "拆箱Feature", isSystem = true)
public class SgUnpackFeature extends Feature {

    @Override
    protected void initialization() {


        // 拆箱单保存
        SgUnpackSaveValidate sgUnpackSaveValidate = ApplicationContextHandle.getBean(SgUnpackSaveValidate.class);
        addValidator(sgUnpackSaveValidate, (tableName, actionName) -> tableName.equalsIgnoreCase(SgUnpackConstants.SG_B_TEUS_UNPACK)
                && (actionName.equals(Constants.ACTION_SAVE) || actionName.equals(Constants.ACTION_ADD)));

        // 删除
        SgUnpackItemDelValidate sgUnpackItemDelValidate = ApplicationContextHandle.getBean(SgUnpackItemDelValidate.class);
        addValidator(sgUnpackItemDelValidate, (tableName, actionName) -> tableName.equalsIgnoreCase(SgUnpackConstants.SG_B_TEUS_UNPACK)
                && (actionName.equals(Constants.ACTION_DELETE)));

        // 作废
        SgUnpackVoidValidate sgUnpackVoidValidate = ApplicationContextHandle.getBean(SgUnpackVoidValidate.class);
        addValidator(sgUnpackVoidValidate, (tableName, actionName) -> tableName.equalsIgnoreCase(SgUnpackConstants.SG_B_TEUS_UNPACK)
                && (actionName.equals(Constants.ACTION_VOID)));

        // 审核
        SgUnpackAuditValidate sgUnpackAuditValidate = ApplicationContextHandle.getBean(SgUnpackAuditValidate.class);
        addValidator(sgUnpackAuditValidate, (tableName, actionName) -> tableName.equalsIgnoreCase(SgUnpackConstants.SG_B_TEUS_UNPACK)
                && (actionName.equals(Constants.ACTION_SUBMIT)));


        SgUnpackSaveFilter saveFilter = ApplicationContextHandle.getBean(SgUnpackSaveFilter.class);
        addFilter(saveFilter, (tableName, actionName) -> tableName.equalsIgnoreCase(SgUnpackConstants.SG_B_TEUS_UNPACK)
                && (actionName.equals(Constants.ACTION_SAVE) || actionName.equals(Constants.ACTION_ADD)));

        SgUnpackItemDelFilter sgUnpackItemDelFilter = ApplicationContextHandle.getBean(SgUnpackItemDelFilter.class);
        addFilter(sgUnpackItemDelFilter, (tableName, actionName) -> tableName.equalsIgnoreCase(SgUnpackConstants.SG_B_TEUS_UNPACK)
                && (actionName.equals(Constants.ACTION_DELETE)));

        SgUnpackAuditFilter sgUnpackAuditFilter = ApplicationContextHandle.getBean(SgUnpackAuditFilter.class);
        addFilter(sgUnpackAuditFilter, (tableName, actionName) -> tableName.equalsIgnoreCase(SgUnpackConstants.SG_B_TEUS_UNPACK)
                && (actionName.equals(Constants.ACTION_SUBMIT)));


    }
}
