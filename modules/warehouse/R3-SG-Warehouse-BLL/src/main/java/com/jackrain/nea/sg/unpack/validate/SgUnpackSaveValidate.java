package com.jackrain.nea.sg.unpack.validate;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.cp.result.CpCStore;
import com.jackrain.nea.cpext.api.GeneralOrganizationCmd;
import com.jackrain.nea.cpext.model.table.CpCPhyWarehouse;
import com.jackrain.nea.data.basic.model.request.StoreInfoQueryRequest;
import com.jackrain.nea.data.basic.services.BasicCpQueryService;
import com.jackrain.nea.sg.unpack.common.SgUnpackConstants;
import com.jackrain.nea.sg.unpack.mapper.SgBTeusUnpackMapper;
import com.jackrain.nea.sg.unpack.model.table.SgBTeusUnpack;
import com.jackrain.nea.tableService.DbRowAction;
import com.jackrain.nea.tableService.MainTableRecord;
import com.jackrain.nea.tableService.TableServiceContext;
import com.jackrain.nea.tableService.validate.BaseValidator;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.utils.AssertUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * @author zhoulinsheng
 * @since 2019/11/27 14:02
 */
@Slf4j
@Component
public class SgUnpackSaveValidate extends BaseValidator {

    @Override
    public void validate(TableServiceContext context, MainTableRecord currentRow) {

        Long objId = currentRow.getId();
        AssertUtils.notNull(objId, "拆箱单ID为空！", context.getLocale());

        if (currentRow.getAction().equals(DbRowAction.INSERT)) {
            JSONObject commitData = currentRow.getCommitData();
            // 单据日期
            Date billDate = commitData.getDate("BILL_DATE");
            AssertUtils.notNull(billDate, "单据日期不能为空！", context.getLocale());
            // 实体店仓
            Long cpCPhyWarehouseId = commitData.getLong("CP_C_PHY_WAREHOUSE_ID");
            AssertUtils.notNull(cpCPhyWarehouseId, "实体店仓id不能为空！", context.getLocale());

            GeneralOrganizationCmd generalOrganizationCmd = (GeneralOrganizationCmd)
                    ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                            GeneralOrganizationCmd.class.getName(), "cp-ext", "1.0");
            CpCPhyWarehouse cpCPhyWarehouse = generalOrganizationCmd.queryByWarehouseId(cpCPhyWarehouseId);
            AssertUtils.notNull(cpCPhyWarehouse, "未查到该实体仓信息！", context.getLocale());
            Integer wmsControlWarehouse = cpCPhyWarehouse.getWmsControlWarehouse();
            if (wmsControlWarehouse == 0) {
                commitData.put("IS_TOWMS", SgUnpackConstants.IS_TOWNS_N);
            } else {
                commitData.put("IS_TOWMS", SgUnpackConstants.IS_TOWNS_Y);
            }

            commitData.put("CP_C_PHY_WAREHOUSE_ECODE", cpCPhyWarehouse.getEcode());
            commitData.put("CP_C_PHY_WAREHOUSE_ENAME", cpCPhyWarehouse.getEname());
            // 逻辑店仓
            Long cpCStoreId = commitData.getLong("CP_C_STORE_ID");
            if (cpCStoreId != null) {
                BasicCpQueryService queryService = ApplicationContextHandle.getBean(BasicCpQueryService.class);
                StoreInfoQueryRequest storeInfoQueryRequest = new StoreInfoQueryRequest();
                List<Long> ids = new ArrayList<>();
                ids.add(cpCStoreId);
                storeInfoQueryRequest.setIds(ids);
                HashMap<Long, CpCStore> storeInfo = new HashMap<>();
                try {
                    storeInfo = queryService.getStoreInfo(storeInfoQueryRequest);
                } catch (Exception e) {
                    AssertUtils.logAndThrow("查询逻辑店仓信息异常！", context.getLocale());
                }

                // 补充实体仓逻辑仓信息
                CpCStore cpCStore = storeInfo.get(cpCStoreId);
                commitData.put("CP_C_STORE_ECODE", cpCStore.getEcode());
                commitData.put("CP_C_STORE_ENAME", cpCStore.getEname());
            }

        } else if (currentRow.getAction().equals(DbRowAction.UPDATE)) {
            SgBTeusUnpackMapper unpackMapper = ApplicationContextHandle.getBean(SgBTeusUnpackMapper.class);
            SgBTeusUnpack sgBTeusUnpack = unpackMapper.selectById(objId);
            AssertUtils.notNull(sgBTeusUnpack, "当前记录已不存在！", context.getLocale());
            Integer status = sgBTeusUnpack.getStatus();

            if (SgUnpackConstants.BILL_STATUS_AUDIT == status) {
                AssertUtils.logAndThrow("当前记录已审核，不允许编辑！", context.getLocale());
            } else if (SgUnpackConstants.BILL_STATUS_VOID == status) {
                AssertUtils.logAndThrow("当前记录已作废，不允许编辑！", context.getLocale());
            }
        }
    }
}