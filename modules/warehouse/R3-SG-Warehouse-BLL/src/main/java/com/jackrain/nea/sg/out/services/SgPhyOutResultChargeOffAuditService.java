package com.jackrain.nea.sg.out.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.model.util.AdParamUtil;
import com.jackrain.nea.redis.config.CusRedisTemplate;
import com.jackrain.nea.redis.util.RedisOpsUtil;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.config.SgStorageBoxConfig;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.out.common.SgOutConstants;
import com.jackrain.nea.sg.out.common.SgOutConstantsIF;
import com.jackrain.nea.sg.out.mapper.*;
import com.jackrain.nea.sg.out.model.request.SgOutResultMQRequest;
import com.jackrain.nea.sg.out.model.result.SgOutResultQueryResult;
import com.jackrain.nea.sg.out.model.table.*;
import com.jackrain.nea.sg.out.utils.OutNoticeUtils;
import com.jackrain.nea.sg.out.utils.SgWarehouseESUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * @author 舒威
 * @since 2019/12/13
 * create at : 2019/12/13 9:53
 */
@Slf4j
@Component
public class SgPhyOutResultChargeOffAuditService {

    @Autowired
    private SgStorageBoxConfig storageBoxConfig;

    @Autowired
    private SgPhyOutResultAuditService auditService;

    /**
     * 结果单审核(冲单)-不通过MQ回写上游单据,body直接返回
     */
    public SgOutResultMQRequest auditSgOutResult(Long objId, User user, Date outTime, Boolean isOneClickOutLibrary) {
        this.auditSingleSgOutResult(objId, user, outTime);
        SgOutResultMQRequest mqRequest = new SgOutResultMQRequest();
        mqRequest.setId(objId);
        mqRequest.setLoginUser(user);
        Boolean flag = Optional.ofNullable(isOneClickOutLibrary).orElse(false);
        mqRequest.setIsOneClickOutLibrary(flag);
        return mqRequest;
    }

    /**
     * 结果单审核(冲单)-不校验通知单状态、数量
     */
    public void auditSingleSgOutResult(Long objId, User user, Date outTime) {
        if (log.isDebugEnabled()) {
            log.debug("Start SgPhyOutResultChargeOffAuditService .auditSingleSgOutResult:objid={},outTime={};", objId, outTime);
        }

        long startTime = System.currentTimeMillis();
        Boolean boxEnable = storageBoxConfig.getBoxEnable();

        if (boxEnable) {
            SgBPhyOutResultImpItemMapper mapper = ApplicationContextHandle.getBean(SgBPhyOutResultImpItemMapper.class);
            SgPhyOutResultConvertService service = ApplicationContextHandle.getBean(SgPhyOutResultConvertService.class);
            List<SgBPhyOutResultImpItem> sgBPhyOutResultImpItems = mapper.selectList(new QueryWrapper<SgBPhyOutResultImpItem>().lambda().eq(SgBPhyOutResultImpItem::getSgBPhyOutResultId, objId));
            service.impItemConvertTeusMethod(sgBPhyOutResultImpItems, null);
        }

        SgBPhyOutResultMapper resultMapper = ApplicationContextHandle.getBean(SgBPhyOutResultMapper.class);
        SgBPhyOutResultItemMapper resultItemMapper = ApplicationContextHandle.getBean(SgBPhyOutResultItemMapper.class);

        /*1.参数校验*/
        SgOutResultQueryResult checkResult = auditService.checkParams(objId, resultMapper, resultItemMapper);
        SgBPhyOutResult outResult = checkResult.getOutResult();
        List<SgBPhyOutResultItem> resultItems = checkResult.getOutResultItems();

        /*2.添加redis锁 30min*/
        Long sourceBillId = outResult.getSourceBillId();
        Integer sourceBillType = outResult.getSourceBillType();
        CusRedisTemplate<String, String> redisTemplate = RedisOpsUtil.getStrRedisTemplate();
        String lockKsy = SgConstants.SG_B_PHY_OUT_RESULT + ":" + sourceBillId + ":" + sourceBillType;
        Boolean blnCanInit = redisTemplate.opsForValue().setIfAbsent(lockKsy, "ok");
        if (blnCanInit != null && blnCanInit) {
            redisTemplate.expire(lockKsy, 30, TimeUnit.MINUTES);
        } else {
            String msg = "当前单据审核中，请稍后重试！来源单据ID：" + sourceBillId + "来源单据类型：" + sourceBillType;
            AssertUtils.logAndThrow(msg);
        }

        try {
            /* 3.更新出库结果单状态、出库人、修改人 */
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            long userId = user.getId().longValue();
            BigDecimal totQtyOut = resultItems.stream().map(SgBPhyOutResultItem::getQty).reduce(BigDecimal.ZERO, BigDecimal::add);
            BigDecimal totAmtListOut = resultItems.stream().map(SgBPhyOutResultItem::getAmtListOut).reduce(BigDecimal.ZERO, BigDecimal::add);
            outResult.setTotQtyOut(totQtyOut);
            outResult.setTotAmtListOut(totAmtListOut);
            outResult.setBillStatus(SgOutConstantsIF.OUT_RESULT_STATUS_AUDIT);
            outResult.setOutId(userId);
            outResult.setOutName(user.getName());
            outResult.setOutEname(user.getEname());
            outResult.setOutTime(outTime != null ? outTime : timestamp);
            StorageESUtils.setBModelDefalutDataByUpdate(outResult, user);
            outResult.setModifierename(user.getEname());
            resultMapper.updateById(outResult);

            /*4.判断实体仓库存负库存*/
            String sysParam = AdParamUtil.getParam(SgOutConstants.SYSTEM_WAREHOUSE);
            log.debug("来源单据id[" + sourceBillId + "],出库结果单审核是否允许负库存系统参数:" + sysParam);
            auditService.queryPhysicalStorage(user, outResult, resultItems, sysParam);

            /* 开启箱功能*/
            List<SgBPhyOutResultTeusItem> resultTeusItems = new ArrayList<>();
            List<SgBPhyOutResultImpItem> resultImpItems = new ArrayList<>();
            if (boxEnable) {
                SgPhyOutNoticesConvertService service = ApplicationContextHandle.getBean(SgPhyOutNoticesConvertService.class);
                /*5.1 判断箱的实体库存是否大于等于出库数量*/
                SgBPhyOutResultImpItemMapper ImpItemMapper = ApplicationContextHandle.getBean(SgBPhyOutResultImpItemMapper.class);
                resultImpItems = ImpItemMapper.selectList(new QueryWrapper<SgBPhyOutResultImpItem>().lambda()
                        .eq(SgBPhyOutResultImpItem::getSgBPhyOutResultId, objId));

                if (CollectionUtils.isEmpty(resultImpItems)) {
                    AssertUtils.logAndThrow("出库结果单录入明细为空，不允许操作！", user.getLocale());
                } else {
                    service.queryTeusPhyStorage(user, outResult, resultImpItems, sysParam);
                }

                SgBPhyOutResultTeusItemMapper teusItemMapper = ApplicationContextHandle.getBean(SgBPhyOutResultTeusItemMapper.class);
                resultTeusItems = teusItemMapper.selectList(new QueryWrapper<SgBPhyOutResultTeusItem>().lambda()
                        .eq(SgBPhyOutResultTeusItem::getSgBPhyOutResultId, objId));
                /*5.2 更新出库通知单录入明细*/
                service.updateOutNoticesImpItemByResult(user, outResult.getSgBPhyOutNoticesId(), outTime, resultImpItems);
            }

            /* 6 更新出库通知单主表和条码明细 */
            JSONObject isLastObj = updateOutNoticesByResult(user, outResult.getSgBPhyOutNoticesId(),
                    outResult.getIsLast(), resultItems, outTime, outResult);

            /*2019-05-30添加逻辑：如果是最后一次出库审核，则判断除本单外有没有未审核的出库单*/
            Integer isLast = isLastObj.getInteger("isLast");
            if (isLast == SgOutConstantsIF.OUT_IS_LAST_Y) {
                List<SgBPhyOutResult> outResults = resultMapper.selectList(new QueryWrapper<SgBPhyOutResult>().lambda()
                        .eq(SgBPhyOutResult::getSgBPhyOutNoticesId, outResult.getSgBPhyOutNoticesId())
                        .eq(SgBPhyOutResult::getBillStatus, SgOutConstantsIF.OUT_RESULT_STATUS_WAIT)
                        .eq(SgBPhyOutResult::getIsactive, SgConstants.IS_ACTIVE_Y));
                if (!CollectionUtils.isEmpty(outResults)) {
                    AssertUtils.logAndThrow("当前出库结果单为最后一次出库，存在未审核的出库结果单，不允许审核！");
                }
            }

            /* 6.同步调用逻辑发货单出库服务 */
            SgPhyOutRPCService rpcService = ApplicationContextHandle.getBean(SgPhyOutRPCService.class);
            ValueHolderV14 sendResult;

            OutNoticeUtils noticeUtils = ApplicationContextHandle.getBean(OutNoticeUtils.class);
            boolean isDiffDeal = noticeUtils.allowNegative(outResult.getSgBPhyOutNoticesId());

            if (boxEnable) {
                sendResult = rpcService.sgBSendOut(outResult, resultItems, resultImpItems, resultTeusItems, user, true, isDiffDeal);
            } else {
                sendResult = rpcService.sgBSendOut(outResult, resultItems, null, null, user, true, isDiffDeal);
            }
            if (!sendResult.isOK()) {
                AssertUtils.logAndThrow(sendResult.getMessage());
            }

            /*7.调库存相关服务*/
            ValueHolderV14 storageResult;
            if (boxEnable) {
                storageResult = rpcService.updateSgPhyStorage(outResult, resultItems, resultImpItems, resultTeusItems, user, sysParam);
            } else {
                storageResult = rpcService.updateSgPhyStorage(outResult, resultItems, null, null, user, sysParam);
            }
            if (!storageResult.isOK()) {
                AssertUtils.logAndThrow(storageResult.getMessage());
            }

            //推送es
            SgWarehouseESUtils warehouseESUtils = ApplicationContextHandle.getBean(SgWarehouseESUtils.class);
            warehouseESUtils.pushESByOutResult(objId, false, false, null, resultMapper, null);
            if (log.isDebugEnabled()) {
                log.debug("出库结果单id[" + objId + "]审核完成!spend time:" + (System.currentTimeMillis() - startTime));
            }
        } catch (Exception e) {
            AssertUtils.logAndThrowExtra(e, user.getLocale());
        } finally {
            redisTemplate.delete(lockKsy);
        }
    }

    public JSONObject updateOutNoticesByResult(User user, Long objId, int isLast, List<SgBPhyOutResultItem> resultItems, Date outTime, SgBPhyOutResult outResult) {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        Locale locale = user.getLocale();
        String eName = user.getEname();

        SgBPhyOutNoticesMapper noticesMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesMapper.class);
        SgBPhyOutNoticesItemMapper noticesItemMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesItemMapper.class);

        /*更新出库通知单明细*/
        resultItems.forEach(outResultItem -> {
            LambdaQueryWrapper<SgBPhyOutNoticesItem> noticesWrapper = new QueryWrapper<SgBPhyOutNoticesItem>().lambda();
            noticesWrapper.eq(SgBPhyOutNoticesItem::getSgBPhyOutNoticesId, objId);
            Long sgBPhyOutNoticesItemId = outResultItem.getSgBPhyOutNoticesItemId();
            if (sgBPhyOutNoticesItemId != null) {
                noticesWrapper.eq(SgBPhyOutNoticesItem::getId, outResultItem.getSgBPhyOutNoticesItemId());
            } else {
                noticesWrapper.eq(SgBPhyOutNoticesItem::getPsCSkuEcode, outResultItem.getPsCSkuEcode());
            }
            SgBPhyOutNoticesItem outNoticesItem = noticesItemMapper.selectOne(noticesWrapper);
            if (outNoticesItem != null) {
                BigDecimal priceList = outNoticesItem.getPriceList();
                BigDecimal priceCost = outNoticesItem.getPriceCost();
                if (priceCost != null) {
                    outNoticesItem.setAmtCostOut(outNoticesItem.getQtyOut().multiply(priceCost)); // 出库成交金额
                    outNoticesItem.setAmtCostDiff(outNoticesItem.getQtyDiff().multiply(priceCost)); // 差异成交金额
                }
                outNoticesItem.setQtyOut(outNoticesItem.getQtyOut().add(outResultItem.getQty())); // 出库数量
                outNoticesItem.setQtyDiff(outNoticesItem.getQty().subtract(outNoticesItem.getQtyOut())); // 差异数量
                outNoticesItem.setAmtListOut(outNoticesItem.getQtyOut().multiply(priceList)); // 出库吊牌金额
                outNoticesItem.setAmtListDiff(outNoticesItem.getQtyDiff().multiply(priceList)); // 差异吊牌金额
                StorageESUtils.setBModelDefalutDataByUpdate(outNoticesItem, user);
                outNoticesItem.setModifierename(eName);
                noticesItemMapper.updateById(outNoticesItem);
            }
        });

        /*更新出库通知单主表(直接sum字表明细字段)*/
        SgBPhyOutNotices updateNotices = noticesMapper.selectById(objId);
        AssertUtils.notNull(updateNotices, "不存在对应的出库通知单！objId=" + objId, locale);

        Integer billStatus = updateNotices.getBillStatus();
        if (billStatus != null && billStatus == SgOutConstantsIF.OUT_NOTICES_STATUS_VOID || StringUtils.equals(updateNotices.getIsactive(), SgConstants.IS_ACTIVE_N)) {
            AssertUtils.logAndThrow("当前结果单所关联的通知单已作废，不允许操作！通知单单据编号：" + updateNotices.getBillNo(), locale);
        }

        SgBPhyOutNoticesItem noticesItem = noticesItemMapper.selectSumAmt(objId);
        AssertUtils.notNull(noticesItem, "出库通知单明细为空！，objId=" + objId, locale);
        BigDecimal totQtyOut = noticesItem.getQtyOut();
        BigDecimal qty = noticesItem.getQty(); // 总通知数量
        updateNotices.setTotQtyOut(totQtyOut);  // 总出库数量
        updateNotices.setTotQtyDiff(qty.subtract(totQtyOut)); // 总差异数量
        updateNotices.setTotAmtListOut(noticesItem.getAmtListOut()); // 总出库吊牌金额
        updateNotices.setTotAmtCostOut(noticesItem.getAmtCostOut()); // 总出库成交金额
        updateNotices.setTotAmtListDiff(noticesItem.getAmtListDiff()); // 总差异吊牌金额
        updateNotices.setTotAmtCostDiff(noticesItem.getAmtCostDiff()); // 总差异成交金额
        updateNotices.setOutTime(outTime != null ? outTime : timestamp);  // 出库时间
        updateNotices.setBillStatus(SgOutConstantsIF.OUT_NOTICES_STATUS_ALL);
        JSONObject isLastObj = new JSONObject();
        isLastObj.put("isAll", false);
        isLastObj.put("isLast", isLast);
        isLastObj.put("isAll", true);
        StorageESUtils.setBModelDefalutDataByUpdate(updateNotices, user);
        updateNotices.setModifierename(eName);
        if (outResult != null) {
            Long logisticsId = outResult.getCpCLogisticsId();
            String logisticsEcode = outResult.getCpCLogisticsEcode();
            String logisticsEname = outResult.getCpCLogisticsEname();
            String logisticNumber = outResult.getLogisticNumber();
            if (log.isDebugEnabled()) {
                log.debug("出库结果回写通知单物流信息:" + logisticsId + "," + logisticsEcode + "," + logisticsEname + "," + logisticNumber);
            }
            updateNotices.setCpCLogisticsId(logisticsId);
            updateNotices.setCpCLogisticsEcode(logisticsEcode);
            updateNotices.setCpCLogisticsEname(logisticsEname);
            updateNotices.setLogisticNumber(logisticNumber);
        }
        noticesMapper.updateById(updateNotices);

        //推送ES
        SgWarehouseESUtils warehouseESUtils = ApplicationContextHandle.getBean(SgWarehouseESUtils.class);
        warehouseESUtils.pushESByOutNotices(objId, true, false, null, noticesMapper, noticesItemMapper);
        return isLastObj;
    }
}
