package com.jackrain.nea.sg.in.services;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.jackrain.nea.config.PropertiesConf;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.cpext.utils.ValueHolderUtils;
import com.jackrain.nea.ext.sequence.util.SequenceGenUtil;
import com.jackrain.nea.model.util.ModelUtil;
import com.jackrain.nea.oc.basic.common.OcBasicConstants;
import com.jackrain.nea.r3.mq.util.R3MqSendHelper;
import com.jackrain.nea.sg.basic.common.R3ParamConstants;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.config.SgStorageBoxConfig;
import com.jackrain.nea.sg.basic.config.SgStorageMqConfig;
import com.jackrain.nea.sg.basic.mapper.CpCPhyWarehouseMapper;
import com.jackrain.nea.sg.basic.mapper.CpStoreMapper;
import com.jackrain.nea.sg.basic.model.result.SgR3BaseResult;
import com.jackrain.nea.sg.basic.model.table.CpCPhyWarehouse;
import com.jackrain.nea.sg.basic.model.table.CpCStore;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.basic.utils.StorageMQAsyncUtils;
import com.jackrain.nea.sg.in.common.SgInConstants;
import com.jackrain.nea.sg.in.common.SgInNoticeConstants;
import com.jackrain.nea.sg.in.mapper.SgBPhyInNoticesItemMapper;
import com.jackrain.nea.sg.in.mapper.SgBPhyInNoticesMapper;
import com.jackrain.nea.sg.in.model.request.*;
import com.jackrain.nea.sg.in.model.result.ReturnSgPhyInResult;
import com.jackrain.nea.sg.in.model.result.SgBaseModelR3;
import com.jackrain.nea.sg.in.model.table.SgBPhyInNotices;
import com.jackrain.nea.sg.in.model.table.SgBPhyInNoticesItem;
import com.jackrain.nea.sg.out.common.SgOutConstantsIF;
import com.jackrain.nea.sg.out.model.request.SgPhyOutBillSendWmsAgainRequest;
import com.jackrain.nea.sg.receive.model.request.*;
import com.jackrain.nea.sg.out.utils.SgWarehouseESUtils;
import com.jackrain.nea.sg.receive.model.request.SgReceiveBillSaveRequest;
import com.jackrain.nea.sg.receive.model.request.SgReceiveItemSaveRequest;
import com.jackrain.nea.sg.receive.model.request.SgReceiveSaveRequest;
import com.jackrain.nea.sg.receive.model.result.SgReceiveBillSaveResult;
import com.jackrain.nea.sg.receive.services.SgReceiveSaveService;
import com.jackrain.nea.sg.send.utils.SgStoreUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.*;

/**
 * @Author: ChenChen
 * @Description:入库通知单新增service
 * @Date: Create in 16:32 2019/4/22
 */
@Slf4j
@Component
public class SgPhyInNoticesSaveService {

    @Autowired
    private SgBPhyInNoticesMapper noticesMapper;

    @Autowired
    private SgBPhyInNoticesItemMapper noticesItemMapper;

    @Autowired
    private SgReceiveSaveService sgReceiveSaveService;

    @Autowired
    private R3MqSendHelper r3MqSendHelper;

    @Autowired
    private SgStorageMqConfig mqConfig;

    @Autowired
    private PropertiesConf pconf;

    @Autowired
    private SgStorageBoxConfig storageBoxConfig;

    @Autowired
    private SgWarehouseESUtils warehouseESUtils;


    /**
     * <生成三张单据> 新增入库通知单、逻辑收货单、入库结果单</生成三张单据>
     */
    public ValueHolderV14 saveSgBPhyInNoticesByRPC(List<SgPhyInNoticesBillSaveRequest> requestList, Boolean isNeedReceive, Boolean isNeedInResult) {
        ValueHolderV14<SgR3BaseResult> result = new ValueHolderV14<>();
        try {
            SgPhyInNoticesSaveService obj = ApplicationContextHandle.getBean(SgPhyInNoticesSaveService.class);
            result = obj.saveSgBPhyInNoticesByRPCMian(requestList, isNeedReceive, isNeedInResult);
        } catch (Exception e) {
            log.error(this.getClass().getName() + ".error:" + e, e);
            result.setCode(ResultCode.FAIL);
            result.setMessage(e.getMessage());
        }
        return result;
    }

    /**
     * <退回专用> 新增入库通知单-RPC生成入库结果</退回专用>
     */
    public ValueHolderV14 saveSgBPhyInNoticesByReturnRPC(List<SgPhyInNoticesBillSaveRequest> requestList) {
        ValueHolderV14<SgR3BaseResult> result = new ValueHolderV14<>();
        try {
            SgPhyInNoticesSaveService obj = ApplicationContextHandle.getBean(SgPhyInNoticesSaveService.class);
            result = obj.saveSgBPhyInNoticesByReturnMian(requestList, true, false);
        } catch (Exception e) {
            log.error(this.getClass().getName() + ".error:" + e, e);
            result.setCode(ResultCode.FAIL);
            result.setMessage(e.getMessage());
        }
        return result;
    }

    /**
     * <退回专用> 新增入库通知单-MQ生成入库结果</退回专用>
     */
    public ValueHolderV14 saveSgBPhyInNoticesByReturnMQ(List<SgPhyInNoticesBillSaveRequest> requestList) {
        ValueHolderV14<SgR3BaseResult> result = new ValueHolderV14<>();
        try {
            SgPhyInNoticesSaveService obj = ApplicationContextHandle.getBean(SgPhyInNoticesSaveService.class);
            result = obj.saveSgBPhyInNoticesByReturnMian(requestList, true, true);
        } catch (Exception e) {
            log.error(this.getClass().getName() + ".error:" + e, e);
            result.setCode(ResultCode.FAIL);
            result.setMessage(e.getMessage());
        }
        return result;
    }

    /**
     * 生成入库通知单和入库结果单
     */
    public ValueHolderV14 saveSgBPhyInNoticesAndResult(List<SgPhyInNoticesBillSaveRequest> requestList) {
        ValueHolderV14<SgR3BaseResult> result = new ValueHolderV14<>();
        try {
            SgPhyInNoticesSaveService obj = ApplicationContextHandle.getBean(SgPhyInNoticesSaveService.class);
            result = obj.saveSgBPhyInNoticesByReturnMian(requestList, false, true);
        } catch (Exception e) {
            log.error(this.getClass().getName() + ".error:" + e, e);
            result.setCode(ResultCode.FAIL);
            result.setMessage(e.getMessage());
        }
        return result;
    }

    @Transactional(rollbackFor = Exception.class)
    public ValueHolderV14 saveSgBPhyInNoticesByRPCMian(List<SgPhyInNoticesBillSaveRequest> requestList, Boolean isNeedReceive, Boolean isNeedInResult) {
        if (log.isDebugEnabled()) {
            log.debug("Start SgPhyInNoticesSaveService.saveSgBPhyInNoticesByRPCMian. ReceiveParams:request:{} sgReceiveFlag{} isSendMQ{};",
                    JSONObject.toJSONString(requestList), isNeedReceive, isNeedReceive);
        }
        long startTime = System.currentTimeMillis();
        ValueHolderV14 holder = new ValueHolderV14();
        String resultSaveRequestParms = checkParams(requestList, true);
        if (StringUtils.isNotEmpty(resultSaveRequestParms)) {
            holder.setMessage("入库通知单参数校验失败:" + resultSaveRequestParms);
            holder.setCode(ResultCode.FAIL);
            return holder;
        }
        List<Long> ids = new ArrayList<>();
        User user = null;
        for (SgPhyInNoticesBillSaveRequest request : requestList) {
            Long id = request.getNotices().getId();
            user = request.getLoginUser();
            if (id == null || -1 == id) {
                //1.新增入库通知单
                long insertInNoticesTime = System.currentTimeMillis();
                ValueHolderV14<Long> insertInNoticesResult = insertMain(request, user, false);
                log.info("新增入库通知单耗时{}", System.currentTimeMillis() - insertInNoticesTime);
                if (!insertInNoticesResult.isOK())
                    return insertInNoticesResult;
                ids.add(insertInNoticesResult.getData());
                //2.新增逻辑收货单
                if (isNeedReceive) {
                    long insertReceiveTime = System.currentTimeMillis();
                    ValueHolderV14<SgReceiveBillSaveResult> insertReceiveResult = saveSgBReceive(request, user);
                    log.info("新增逻辑收货单耗时{}", System.currentTimeMillis() - insertReceiveTime);
                    if (!insertReceiveResult.isOK()) {
                        AssertUtils.logAndThrow("插入逻辑收货单失败！" + insertReceiveResult.getMessage(), user.getLocale());
                    }
                }

            }
        }

        //3.mq发送给入库结果单
        if (isNeedInResult) {
            SgPhyInNoticesSendMQService mqService = ApplicationContextHandle.getBean(SgPhyInNoticesSendMQService.class);
            SgPhyInResultSaveAndAuditService inResultSaveAndAuditService = ApplicationContextHandle.getBean(SgPhyInResultSaveAndAuditService.class);
            List<SgPhyInResultBillSaveRequest> billList = mqService.getBatchNoticesBill(ids, user, false);
            ValueHolderV14 result = inResultSaveAndAuditService.saveAndAuditBill(billList);
            if (result.isOK()) {
                holder.setCode(ResultCode.SUCCESS);
                List<ReturnSgPhyInResult> data = (List<ReturnSgPhyInResult>) result.getData();
                for (ReturnSgPhyInResult datum : data) {
                    if (datum.getCode() == ResultCode.FAIL) {
                        AssertUtils.logAndThrow("新增并审核入库结果单失败!" + datum.getMessage());
                    }
                }
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("Finish SgPhyInNoticesSaveService.saveSgBPhyInNoticesByRPCMian. ReturnResults:{} spend time:{};",
                    holder.toJSONObject(), System.currentTimeMillis() - startTime);
        }
        return holder;

    }

    /**
     * 新增入库通知单
     */
    public ValueHolderV14 saveSgBPhyInNotices(List<SgPhyInNoticesBillSaveRequest> requestList) {
        ValueHolderV14<SgR3BaseResult> result = new ValueHolderV14<>();
        try {
            SgPhyInNoticesSaveService obj = ApplicationContextHandle.getBean(SgPhyInNoticesSaveService.class);
            result = obj.saveSgBPhyInNoticesMain(requestList);
        } catch (Exception e) {
            log.error(this.getClass().getName() + ".error:" + e, e);
            result.setCode(ResultCode.FAIL);
            result.setMessage(e.getMessage());
        }
        return result;
    }

    /**
     * 新增入库通知单。退回专用
     *
     * @param requestList
     * @param sgReceiveFlag 是否生成逻辑收货单
     * @param isSendMQ      是否MQ生成入库结果单
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public ValueHolderV14 saveSgBPhyInNoticesByReturnMian(List<SgPhyInNoticesBillSaveRequest> requestList, boolean sgReceiveFlag, boolean isSendMQ) {
        if (log.isDebugEnabled()) {
            log.debug("Start SgPhyInNoticesSaveService.saveSgBPhyInNoticesByReturn. ReceiveParams:request:{} sgReceiveFlag{} isSendMQ{};",
                    JSONObject.toJSONString(requestList), sgReceiveFlag, isSendMQ);
        }
        long startTime = System.currentTimeMillis();
        ValueHolderV14 holder = new ValueHolderV14();
        String resultSaveRequestParms = checkParams(requestList, true);
        if (!StringUtils.isEmpty(resultSaveRequestParms)) {
            holder.setMessage(resultSaveRequestParms);
            holder.setCode(ResultCode.FAIL);
            log.error("入库通知单参数校验失败:" + resultSaveRequestParms);
            return holder;
        }
        List<Long> ids = new ArrayList<>();
        User user = null;
        for (SgPhyInNoticesBillSaveRequest request : requestList) {
            Long id = request.getNotices().getId();
            user = request.getLoginUser();
            if (id == null || -1 == id) {
                //1.新增入库通知单
                long insertInNoticesTime = System.currentTimeMillis();
                ValueHolderV14<Long> insertInNoticesResult = insertMain(request, user, false);
                log.info("新增入库通知单耗时{}", System.currentTimeMillis() - insertInNoticesTime);
                if (!insertInNoticesResult.isOK())
                    return insertInNoticesResult;
                ids.add(insertInNoticesResult.getData());
                //2.新增逻辑收货单
                if (sgReceiveFlag) {
                    long insertReceiveTime = System.currentTimeMillis();
                    ValueHolderV14<SgReceiveBillSaveResult> insertReceiveResult = saveSgBReceive(request, user);
                    log.info("新增逻辑收货单耗时{}", System.currentTimeMillis() - insertReceiveTime);
                    if (!insertReceiveResult.isOK()) {
                        AssertUtils.logAndThrow("插入逻辑收货单失败！" + insertReceiveResult.getMessage(), user.getLocale());
                    }
                }

            }
        }

        //3.mq发送给入库结果单
        isSendMQ = false;
        if (isSendMQ) {
            String configName = mqConfig.getChannelSynchConfigName();
            String topic = pconf.getProperty(SgInConstants.R3_OMS_SG_OUT_MQ_TOPIC_KEY);
            String tag = SgInConstants.SG_PHY_IN_NOTICES_TO_RESULT_MQ_TAG;
            String msgKey = "in_notices_to_in_result_" + ids.size() + "_" + new Date().getTime();
            //2018-08-30 入库通知mq生成结果单改延时发消息
            StorageMQAsyncUtils asyncUtils = ApplicationContextHandle.getBean(StorageMQAsyncUtils.class);
            asyncUtils.sendDelayMessage(configName, JSONObject.toJSONString(ids), topic, tag, msgKey, 3000L, r3MqSendHelper);
            holder.setCode(ResultCode.SUCCESS);
        } else {
            SgPhyInNoticesSendMQService mqService = ApplicationContextHandle.getBean(SgPhyInNoticesSendMQService.class);
            SgPhyInResultSaveAndAuditService inResultSaveAndAuditService = ApplicationContextHandle.getBean(SgPhyInResultSaveAndAuditService.class);
            List<SgPhyInResultBillSaveRequest> billList = mqService.getBatchNoticesBill(ids, user, false);
            ValueHolderV14 result = inResultSaveAndAuditService.saveAndAuditBill(billList);
            if (result.isOK()) {
                holder.setCode(ResultCode.SUCCESS);
                List<ReturnSgPhyInResult> data = (List<ReturnSgPhyInResult>) result.getData();
                for (ReturnSgPhyInResult datum : data) {
                    if (datum.getCode() == ResultCode.FAIL) {
                        AssertUtils.logAndThrow("新增并审核入库结果单失败!" + datum.getMessage());
                    }
                }
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("Finish SgPhyInNoticesSaveService.saveSgBPhyInNoticesByReturn. ReturnResults:{} spend time:{};",
                    holder.toJSONObject(), System.currentTimeMillis() - startTime);
        }
        return holder;
    }

    /**
     * 新增入库通知单
     *
     * @param requestList
     * @return
     */
    @Transactional
    public ValueHolderV14 saveSgBPhyInNoticesMain(List<SgPhyInNoticesBillSaveRequest> requestList) {
        if (log.isDebugEnabled()) {
            log.debug("Start SgPhyInNoticesSaveService.saveSgBPhyInNotices. ReceiveParams:ist<SgPhyInNoticesBillSaveRequest>="
                    + JSONObject.toJSONString(requestList) + ";");
        }
        ValueHolderV14 result = new ValueHolderV14();
        String resultSaveRequestParms = checkParams(requestList, false);
        if (!StringUtils.isEmpty(resultSaveRequestParms)) {
            result.setMessage(resultSaveRequestParms);
            result.setCode(ResultCode.FAIL);
            return result;
        }
        for (SgPhyInNoticesBillSaveRequest request : requestList) {
            Long id = request.getNotices().getId();
            User user = request.getLoginUser();
            if (id == null || -1 == id) {
                //1.插入入库通知单
                ValueHolderV14<Long> resultMain = insertMain(request, user, false);
                log.debug("新增入库通知单1==>"+resultMain);
                if (resultMain.getCode() == ResultCode.FAIL)
                    return resultMain;
                result.setData(resultMain.getData());
            }
        }
        result.setMessage("success");
        result.setCode(ResultCode.SUCCESS);
        log.debug("新增入库通知单2==>"+result);
        return result;
    }


    /**
     * 插入入库通知单主表
     *
     * @param request
     * @param user
     * @return
     */
    private ValueHolderV14<Long> insertMain(SgPhyInNoticesBillSaveRequest request, User user, boolean isReturn) {

        ValueHolderV14<Long> result = new ValueHolderV14<Long>();
        SgPhyInNoticesRequest noticesRequest = request.getNotices();

        SgBPhyInNotices notices = new SgBPhyInNotices();
        BeanUtils.copyProperties(noticesRequest, notices);
        //获取id
        Long id = ModelUtil.getSequence(SgConstants.SG_B_PHY_IN_NOTICES);
        notices.setId(id);
        //获取单据编号
        JSONObject obj = new JSONObject();
        obj.put(SgConstants.SG_B_PHY_IN_NOTICES.toUpperCase(), notices);
        String billNo = SequenceGenUtil.generateSquence(SgInNoticeConstants.SEQ_SG_B_PHY_IN_NOTICES, obj, user.getLocale(), false);
        notices.setBillNo(billNo);
        //单据状态
        if (notices.getBillStatus() == null) {
            notices.setBillStatus(SgInNoticeConstants.BILL_STATUS_IN_PENDING);
        }
        //单据日期
        if (notices.getBillDate() == null) {
            notices.setBillDate(new Date());
        }

        //插入条码明细
        insertSubRecords(request.getItemList(), notices, user, noticesRequest.getReserveBigint01());

        // 开启箱功能时
        if (storageBoxConfig.getBoxEnable()) {
            SgPhyInNoticesTeusFunctionService service = ApplicationContextHandle.getBean(SgPhyInNoticesTeusFunctionService.class);
            //录入明细新增
            service.insertInNoticesImpItem(id, request.getImpItemList(), request.getLoginUser());
            //箱明细新增
            service.insertInNoticesTeusItem(id, request.getTeusItemList(), request.getLoginUser());
        }

        StorageESUtils.setBModelDefalutData(notices, user);
        notices.setOwnerename(user.getEname());
        notices.setModifierename(user.getEname());

        if (isReturn) {//退回默认要改给个入库时间
            if (notices.getTotQtyDiff().compareTo(BigDecimal.ZERO) <= 0) {
                //全部入库
                notices.setBillStatus(SgInNoticeConstants.BILL_STATUS_IN_ALL);
            } else {
                //部分入库
                notices.setBillStatus(SgInNoticeConstants.BILL_STATUS_IN_PART);
            }
        }

        //2019-09-06 截取备注等超长字段
        notices.setRemark(StorageESUtils.strSubString(notices.getRemark(), SgConstants.SG_COMMON_REMARK_SIZE));
        notices.setBuyerRemark(StorageESUtils.strSubString(notices.getBuyerRemark(), SgConstants.SG_COMMON_REMARK_SIZE));
        notices.setSellerRemark(StorageESUtils.strSubString(notices.getSellerRemark(), SgConstants.SG_COMMON_REMARK_SIZE));
        notices.setWmsFailReason(StorageESUtils.strSubString(notices.getWmsFailReason(), SgConstants.SG_COMMON_STRING_SIZE));
        notices.setPickStatus(SgOutConstantsIF.PICK_STATUS_WAIT);
        noticesMapper.insert(notices);

        //推送ES
        SgBPhyInNoticesItemMapper noticesItemMapper = ApplicationContextHandle.getBean(SgBPhyInNoticesItemMapper.class);
        warehouseESUtils.pushESByInNotices(notices.getId(), true, false, null, noticesMapper, noticesItemMapper);

        result.setData(id);
        result.setCode(ResultCode.SUCCESS);
        result.setMessage("success");
        return result;
    }


    //插入条码明细
    private void insertSubRecords(List<SgPhyInNoticesItemRequest> itemList, SgBPhyInNotices main, User user, Long reserveBigint01) {
        //2019-08-12 通知单保存时合并相同sku的明细
        Map<Long, SgPhyInNoticesItemRequest> requestMap = Maps.newHashMap();
        for (SgPhyInNoticesItemRequest noticesItemRequest : itemList) {
            Long psCSkuId = noticesItemRequest.getPsCSkuId();
            if (requestMap.containsKey(psCSkuId)) {
                SgPhyInNoticesItemRequest orgNoticesItemRequest = requestMap.get(psCSkuId);
                BigDecimal orgQty = Optional.ofNullable(orgNoticesItemRequest.getQty()).orElse(BigDecimal.ZERO);
                BigDecimal qty = Optional.ofNullable(noticesItemRequest.getQty()).orElse(BigDecimal.ZERO);
                orgNoticesItemRequest.setQty(qty.add(orgQty));
                BigDecimal orgQtyIn = Optional.ofNullable(orgNoticesItemRequest.getQtyIn()).orElse(BigDecimal.ZERO);
                BigDecimal qtyIn = Optional.ofNullable(noticesItemRequest.getQtyIn()).orElse(BigDecimal.ZERO);
                orgNoticesItemRequest.setQtyIn(qtyIn.add(orgQtyIn));
            } else {
                requestMap.put(psCSkuId, noticesItemRequest);
            }
        }
        //合并后的明细
        List<SgPhyInNoticesItemRequest> mergeItemList = new ArrayList<>(requestMap.values());

        BigDecimal totAmtList = BigDecimal.ZERO;//总吊牌金额
        BigDecimal totAmtCost = BigDecimal.ZERO;//总成交金额
        BigDecimal totQty = BigDecimal.ZERO;//总通知数量
        BigDecimal qtyInCount = BigDecimal.ZERO;//总入库数量
        BigDecimal amtListInSum = BigDecimal.ZERO;//总入库吊牌金额
        BigDecimal amtCostInSum = BigDecimal.ZERO;//总入库成交金额
        Locale locale = user.getLocale();
        List<SgBPhyInNoticesItem> batchInsertList = Lists.newArrayList();
        for (SgPhyInNoticesItemRequest item : mergeItemList) {
            if (item.getQtyIn() != null && item.getQtyIn().compareTo(item.getQty()) > 0
                    && (reserveBigint01 == null || reserveBigint01.compareTo(SgInNoticeConstants.BILL_TYPE_TRANSFER_DIFF) != 0)) {
                AssertUtils.logAndThrow("入库数量不能大于通知数量！", user.getLocale());
            } else if (item.getQtyIn() == null) {
                item.setQtyIn(BigDecimal.ZERO);
            }

            SgBPhyInNoticesItem sgBPhyInNoticesItem = new SgBPhyInNoticesItem();
            BeanUtils.copyProperties(item, sgBPhyInNoticesItem);
            Long id = ModelUtil.getSequence(SgConstants.SG_B_PHY_IN_NOTICES_ITEM);
            sgBPhyInNoticesItem.setId(id);
            sgBPhyInNoticesItem.setSgBPhyInNoticesId(main.getId());
            sgBPhyInNoticesItem.setOwnerename(user.getEname());
            sgBPhyInNoticesItem.setModifierename(user.getEname());
            //2019-07-31   退回-新增入库通知单时 入库数量录入预留字段 不直接录入明细信息
            sgBPhyInNoticesItem.setReserveDecimal01(item.getQtyIn());
            sgBPhyInNoticesItem.setQtyIn(BigDecimal.ZERO);


            //总通知数量+=当前通知数量
            totQty = totQty.add(item.getQty());
            //总入库数量+=当前入库数量
            qtyInCount = qtyInCount.add(item.getQtyIn());
            //差异数量=通知数量-入库数量
            sgBPhyInNoticesItem.setQtyDiff(sgBPhyInNoticesItem.getQty().subtract(sgBPhyInNoticesItem.getQtyIn()));

            //吊牌金额=明细吊牌价X通知数量
            sgBPhyInNoticesItem.setAmtList(sgBPhyInNoticesItem.getPriceList().multiply(sgBPhyInNoticesItem.getQty()));
            //总吊牌金额+=当前吊牌金额
            totAmtList = totAmtList.add(sgBPhyInNoticesItem.getAmtList());
            //入库吊牌金额=吊牌金额*入库数量
            sgBPhyInNoticesItem.setAmtListIn(sgBPhyInNoticesItem.getPriceList().multiply(sgBPhyInNoticesItem.getQtyIn()));
            //总入库吊牌金额+=总入库吊牌金额
            amtListInSum = amtListInSum.add(sgBPhyInNoticesItem.getAmtListIn());
            //差异吊牌金额=吊牌金额-入库吊牌金额
            sgBPhyInNoticesItem.setAmtListDiff(sgBPhyInNoticesItem.getAmtList().subtract(sgBPhyInNoticesItem.getAmtListIn()));
            if (sgBPhyInNoticesItem.getPriceCost() != null) {
                //成交金额=明细成交价X通知数量
                sgBPhyInNoticesItem.setAmtCost(sgBPhyInNoticesItem.getPriceCost().multiply(sgBPhyInNoticesItem.getQty()));
                //总成交金额+=当前成交金额
                totAmtCost = totAmtCost.add(sgBPhyInNoticesItem.getAmtCost());
                //入库成交金额=明细吊牌价成交价X数量
                sgBPhyInNoticesItem.setAmtCostIn(item.getQtyIn().multiply(sgBPhyInNoticesItem.getPriceCost()));
                //总入库成交金额+=总成交吊牌金额
                amtCostInSum = amtCostInSum.add(sgBPhyInNoticesItem.getAmtCostIn());
                //差异成交金额= 成交金额-入库成交金额
                sgBPhyInNoticesItem.setAmtCostDiff(sgBPhyInNoticesItem.getAmtCost().subtract(sgBPhyInNoticesItem.getAmtCostIn()));

            }
            StorageESUtils.setBModelDefalutData(sgBPhyInNoticesItem, user);
            batchInsertList.add(sgBPhyInNoticesItem);


        }
        /*更新总数量,总入库数量,总差异数量;  总差异数量=总数量-总入库数量
         总吊牌金额,总入库吊牌金额,总差异吊牌金额;  总差异吊牌金额=总吊牌金额-总入库吊牌金额
         总成交金额,总入库成交金额，总差异成交金额; 总差异成交金额= 总成交金额-总入库成交金额*/

        // 2019-07-31 原逻辑： 退回生成三张单据(入库通知、逻辑收、入库结果)，入库数量等信息在入库通知单直接新增的时候更新
        //现逻辑：新增入库通知单时不更新入库相关的信息，入库结果审核时回写
        main.setTotQty(totQty);
        main.setTotQtyIn(BigDecimal.ZERO);
        main.setTotQtyDiff(totQty);

        main.setTotAmtList(totAmtList);
        main.setTotAmtListIn(BigDecimal.ZERO);
        main.setTotAmtListDiff(totAmtList);

        if (totAmtCost.compareTo(BigDecimal.ZERO) > 0) {
            main.setTotAmtCost(totAmtCost);
            main.setTotAmtCostIn(BigDecimal.ZERO);
            main.setTotAmtCostDiff(totAmtCost);
        }

        if (CollectionUtils.isNotEmpty(batchInsertList)) {
            SgStoreUtils.batchInsertTeus(batchInsertList, "入库通知单明细", SgConstants.SG_COMMON_MAX_INSERT_PAGE_SIZE, noticesItemMapper);
        }
    }

    /**
     * 参数校验
     */
    private String checkParams(List<SgPhyInNoticesBillSaveRequest> noticesBillSaveRequestList, boolean isReturn) {

        StringBuilder resultSb = new StringBuilder();
        for (int i = 0; i < noticesBillSaveRequestList.size(); i++) {
            SgPhyInNoticesBillSaveRequest noticesBillSaveRequest = noticesBillSaveRequestList.get(i);

            // 校验参数前判断是否开启箱功能
            if (storageBoxConfig.getBoxEnable()) {
                //若只有条码明细，没有录入明细，需将条码明细转换成散码copy一份至录入明细，零售退货用
                List<SgPhyInNoticesItemRequest> itemList = noticesBillSaveRequest.getItemList();
                List<SgPhyInNoticesImpItemSaveRequest> impItemList = noticesBillSaveRequest.getImpItemList();
                if (CollectionUtils.isEmpty(impItemList) && CollectionUtils.isNotEmpty(itemList)) {
                    List<SgPhyInNoticesImpItemSaveRequest> impItemSaveRequests = Lists.newArrayList();
                    for (SgPhyInNoticesItemRequest itemRequest : itemList) {
                        SgPhyInNoticesImpItemSaveRequest impItemSaveRequest = new SgPhyInNoticesImpItemSaveRequest();
                        BeanUtils.copyProperties(itemRequest, impItemSaveRequest);
                        impItemSaveRequest.setIsTeus(OcBasicConstants.IS_MATCH_SIZE_N);
                        impItemSaveRequests.add(impItemSaveRequest);
                    }
                    noticesBillSaveRequest.setImpItemList(impItemSaveRequests);
                }
                SgPhyInNoticesTeusFunctionService forTeusService = ApplicationContextHandle.getBean(SgPhyInNoticesTeusFunctionService.class);
                forTeusService.impRequestConvertMethod(noticesBillSaveRequest);
            }

            StringBuilder sb = new StringBuilder();

            if (noticesBillSaveRequest.getLoginUser() == null) {
                sb.append("用户未登录！");
            }

            SgPhyInNoticesRequest noticesRequest = noticesBillSaveRequest.getNotices();
            if (noticesRequest == null) {
                sb.append("入库通知单不能为空！");
            } else {
                if (noticesRequest.getCpCPhyWarehouseId() == null || StringUtils.isEmpty(noticesRequest.getCpCPhyWarehouseEname())) {
                    sb.append("当前来源单据的实体仓信息不能为空！");
                } else {
                    CpCPhyWarehouseMapper warehouseMapper = ApplicationContextHandle.getBean(CpCPhyWarehouseMapper.class);
                    CpCPhyWarehouse cpCPhyWarehouse = warehouseMapper.selectById(noticesRequest.getCpCPhyWarehouseId());
                    if (cpCPhyWarehouse == null) {
                        sb.append("未找到实体仓信息！");
                    } else {
                        if (noticesRequest.getIsPassWms() == null) {
                            if (cpCPhyWarehouse.getWmsControlWarehouse() == null || cpCPhyWarehouse.getWmsControlWarehouse() == 0) {
                                noticesRequest.setIsPassWms(SgInConstants.NOTICE_IS_PASS_WMS_NO);
                            } else {
                                noticesRequest.setIsPassWms(SgInConstants.NOTICE_IS_PASS_WMS_YES);
                            }
                        }
                        noticesRequest.setGoodsOwner(cpCPhyWarehouse.getOwnerCode());
                    }
                }

                if (noticesRequest.getSourceBillType() == null || noticesRequest.getSourceBillId() == null) {
                    sb.append("当前来源单据ID/来源单据类型不能为空！");
                } else {
                    //TODO 销售单和调拨单可以重复新增，但是肯定需要一个判断重复的条件，产品还没给
                    if (SgConstantsIF.BILL_TYPE_SALE != noticesRequest.getSourceBillType()
                            && SgConstantsIF.BILL_TYPE_SALE_REF != noticesRequest.getSourceBillType()
                            && SgConstantsIF.BILL_TYPE_TRANSFER != noticesRequest.getSourceBillType()) {
                        Integer count = noticesMapper.selectCount(new QueryWrapper<SgBPhyInNotices>().lambda()
                                .eq(SgBPhyInNotices::getSourceBillId, noticesRequest.getSourceBillId())
                                .eq(SgBPhyInNotices::getSourceBillType, noticesRequest.getSourceBillType())
                                .eq(SgBPhyInNotices::getIsactive, SgConstants.IS_ACTIVE_Y));
                        if (count != null && count > 0) {
                            sb.append("当前订单已存在入库通知单！");
                        }
                    }
                }

                if (noticesRequest.getInType() == null) {
                    sb.append("当前入库类型不能为空！");
                } else if (noticesRequest.getInType() == SgInConstants.IN_TYPE_BIG_CARGO) {
                    if (StringUtils.isEmpty(noticesRequest.getCpCCsEcode()) || StringUtils.isEmpty(noticesRequest.getCpCCsEname())) {
                        sb.append("供应商编码/供应商名称不能为空！");
                    }
                }
            }

            if (CollectionUtils.isEmpty(noticesBillSaveRequest.getItemList())) {
                sb.append("入库通知单明细不能为空！");
            } else {
                int itemLength = noticesBillSaveRequest.getItemList().size();
                StringBuilder itemSbList = new StringBuilder();
                for (int j = 0; j < itemLength; j++) {
                    StringBuilder itemSb = new StringBuilder();
                    SgPhyInNoticesItemRequest itemRequest = noticesBillSaveRequest.getItemList().get(j);

                    if (itemRequest.getSourceBillItemId() == null) {
                        itemSb.append("当前来源单据明细ID不能为空");
                    }
                    // 【条码ID】【条码编码】【商品id】【商品编码】【商品名称】【规格id】【规格编码】【规格名称】
                    if (itemRequest.getPsCSkuId() == null
                            || StringUtils.isEmpty(itemRequest.getPsCSkuEcode())
                            || itemRequest.getPsCProId() == null
                            || StringUtils.isEmpty(itemRequest.getPsCProEname())
                            || StringUtils.isEmpty(itemRequest.getPsCProEcode())
                            || itemRequest.getPsCSpec1Id() == null
                            || StringUtils.isEmpty(itemRequest.getPsCSpec1Ename())
                            || StringUtils.isEmpty(itemRequest.getPsCSpec1Ecode())
                            || itemRequest.getPsCSpec2Id() == null
                            || StringUtils.isEmpty(itemRequest.getPsCSpec2Ename())
                            || StringUtils.isEmpty(itemRequest.getPsCSpec2Ecode())) {
                        itemSb.append("当前来源单据明细的条码/商品/规格信息不能为空");
                    }

                    if (itemRequest.getPriceList() == null || itemRequest.getQty() == null) {
                        itemSb.append("当前来源单据吊牌价/数量不能为空");
                    }

                    if (isReturn && itemRequest.getQtyIn() == null) {
                        itemSb.append("入库数量不能为空");
                    }

                    if (StringUtils.isNotEmpty(itemSb)) {
                        itemSbList.append("第" + (j + 1) + "行：" + itemSb);
                    }
                }
                if (StringUtils.isNotEmpty(itemSbList)) {
                    sb.append("明细：" + itemSbList);
                }
            }
            if (StringUtils.isNotEmpty(sb)) {
                resultSb.append("第" + (i + 1) + "行：" + sb);
            }
        }
        return resultSb.toString();
    }

    /**
     * 写入逻辑收货单
     *
     * @param inNoticesBillSaveRequest 通知单
     * @param user                     登陆用户
     * @return
     */
    public ValueHolderV14<SgReceiveBillSaveResult> saveSgBReceive(SgPhyInNoticesBillSaveRequest inNoticesBillSaveRequest, User user) {
        SgPhyInNoticesRequest noticesRequest = inNoticesBillSaveRequest.getNotices();
        SgReceiveBillSaveRequest request = new SgReceiveBillSaveRequest();
        //逻辑收货单主表
        SgReceiveSaveRequest mainRequest = new SgReceiveSaveRequest();
        mainRequest.setSourceBillId(noticesRequest.getSourceBillId());
        mainRequest.setSourceBillNo(noticesRequest.getSourceBillNo());
        if (noticesRequest.getReceiveBillStatus() != null) {
            mainRequest.setBillStatus(noticesRequest.getReceiveBillStatus());
        }
        mainRequest.setSourceBillType(noticesRequest.getSourceBillType());
        mainRequest.setCpCShopId(noticesRequest.getCpCShopId());
        mainRequest.setCpCShopTitle(noticesRequest.getCpCShopTitle());
        mainRequest.setSourcecode(noticesRequest.getSourcecode());
        mainRequest.setRemark(noticesRequest.getRemark());

        if (noticesRequest.getCpCStoreId() == null) {
            //获取逻辑主仓
            CpStoreMapper cpStoreMapper = ApplicationContextHandle.getBean(CpStoreMapper.class);
            List<CpCStore> storeList = cpStoreMapper.selectList(new QueryWrapper<CpCStore>()
                    .eq("cp_c_phy_warehouse_id", noticesRequest.getCpCPhyWarehouseId())
                    .orderByDesc("is_main_warehouse", "id"));

            if (CollectionUtils.isEmpty(storeList)) {
                AssertUtils.logAndThrow("入库通知单新增逻辑收货单失败，未找到逻辑主仓！", user.getLocale());
            }
            CpCStore mainWarehouse = storeList.get(0);
            noticesRequest.setCpCStoreId(mainWarehouse.getId());
            noticesRequest.setCpCStoreEcode(mainWarehouse.getCpCStoreEcode());
            noticesRequest.setCpCStoreEname(mainWarehouse.getCpCStoreEname());
        }

        //逻辑收货单明细
        List<SgReceiveItemSaveRequest> itemRequestList = new ArrayList<>();
        for (SgPhyInNoticesItemRequest item : inNoticesBillSaveRequest.getItemList()) {
            SgReceiveItemSaveRequest itemSaveRequest = new SgReceiveItemSaveRequest();
            BeanUtils.copyProperties(item, itemSaveRequest);
            itemSaveRequest.setCpCStoreId(noticesRequest.getCpCStoreId());
            itemSaveRequest.setCpCStoreEcode(noticesRequest.getCpCStoreEcode());
            itemSaveRequest.setCpCStoreEname(noticesRequest.getCpCStoreEname());
            itemSaveRequest.setQtyPrein(item.getQty());
            itemRequestList.add(itemSaveRequest);
        }

        if (storageBoxConfig.getBoxEnable()) {
            if (CollectionUtils.isNotEmpty(inNoticesBillSaveRequest.getImpItemList())) {
                List<SgReceiveImpItemSaveRequest> impItemSaveRequestList = Lists.newArrayList();
                for (SgPhyInNoticesImpItemSaveRequest impItem : inNoticesBillSaveRequest.getImpItemList()) {
                    SgReceiveImpItemSaveRequest impItemSaveRequest = new SgReceiveImpItemSaveRequest();
                    BeanUtils.copyProperties(impItem, impItemSaveRequest);
                    impItemSaveRequest.setCpCStoreId(noticesRequest.getCpCStoreId());
                    impItemSaveRequest.setCpCStoreEcode(noticesRequest.getCpCStoreEcode());
                    impItemSaveRequest.setCpCStoreEname(noticesRequest.getCpCStoreEname());
                    impItemSaveRequest.setQtyPrein(impItem.getQty());
                    impItemSaveRequestList.add(impItemSaveRequest);
                }
                request.setImpItemList(impItemSaveRequestList);
            }
            if (CollectionUtils.isNotEmpty(inNoticesBillSaveRequest.getTeusItemList())) {
                List<SgReceiveTeusItemSaveRequest> teusItemSaveRequestList = Lists.newArrayList();
                for (SgPhyInNoticesTeusItemSaveRequest teusItem : inNoticesBillSaveRequest.getTeusItemList()) {
                    SgReceiveTeusItemSaveRequest teusItemSaveRequest = new SgReceiveTeusItemSaveRequest();
                    BeanUtils.copyProperties(teusItem, teusItemSaveRequest);
                    teusItemSaveRequest.setCpCStoreId(noticesRequest.getCpCStoreId());
                    teusItemSaveRequest.setCpCStoreEcode(noticesRequest.getCpCStoreEcode());
                    teusItemSaveRequest.setCpCStoreEname(noticesRequest.getCpCStoreEname());
                    teusItemSaveRequestList.add(teusItemSaveRequest);
                }
                request.setTeusItemList(teusItemSaveRequestList);
            }
        }

        request.setSgReceive(mainRequest);
        request.setLoginUser(user);
        request.setItemList(itemRequestList);
        return sgReceiveSaveService.saveSgBReceive(request);
    }


    /**
     * 更新WMS状态
     *
     * @param objId 主键ID
     * @param user  用户信息
     * @return 返回
     */
    private SgBaseModelR3 updateInNotices(User user, Long objId) {
        SgBaseModelR3 result = new SgBaseModelR3();
        SgBPhyInNoticesMapper noticesMapper = ApplicationContextHandle.getBean(SgBPhyInNoticesMapper.class);
        SgBPhyInNotices inNotices = noticesMapper.selectById(objId);
        result.setId(objId);
        result.setCode(ResultCode.FAIL);
        if (inNotices == null) {
            result.setMessage(Resources.getMessage("当前记录不存在！"));
            return result;
        }

        Long wmsStatus = inNotices.getWmsStatus();
        if (wmsStatus != null && wmsStatus != SgOutConstantsIF.WMS_STATUS_PASS_FAILED) {
            result.setMessage(Resources.getMessage("当前记录不是“传WMS失败”状态，不允许重传！"));
            return result;
        }

        // 更新单据状态
        SgBPhyInNotices updateNotices = new SgBPhyInNotices();
        updateNotices.setWmsStatus(SgOutConstantsIF.WMS_STATUS_WAIT_PASS);
        updateNotices.setWmsFailCount(BigDecimal.ZERO.longValue());
        updateNotices.setModifierid((long) user.getId());
        updateNotices.setModifiername(user.getName());
        updateNotices.setModifierename(user.getEname());
        updateNotices.setModifieddate(new Timestamp(System.currentTimeMillis()));
        int count = noticesMapper.update(updateNotices, new UpdateWrapper<SgBPhyInNotices>().lambda()
                .eq(SgBPhyInNotices::getId, objId)
                .set(SgBPhyInNotices::getWmsFailReason, null));
        if (count <= 0) {
            result.setMessage(Resources.getMessage("重传WMS失败，系统错误！"));
        } else {
            result.setCode(ResultCode.SUCCESS);
            result.setMessage(Resources.getMessage("重传WMS成功！"));
            //推送es
            warehouseESUtils.pushESByInNotices(objId, false, false, null, noticesMapper, null);
        }
        return result;
    }

    /**
     * 重传WMS
     *
     * @param request 入参
     * @return 出参
     */
    public ValueHolder sendWmsAgainInNotices(SgPhyOutBillSendWmsAgainRequest request) {

        AssertUtils.notNull(request, "参数不能为空！");
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ".debug, sendWmsAgainInNotices-param:" + JSONObject.toJSONString(request));
        }

        User user = request.getUser();
        AssertUtils.notNull(user, "用户信息不能为空！");

        JSONArray ids = request.getIds();
        if (CollectionUtils.isEmpty(ids)) {
            AssertUtils.logAndThrow("请选择数据！", user.getLocale());
        }

        ValueHolder vh = new ValueHolder();
        // 单对象页面返回
        if (request.getIsObj()) {
            SgBaseModelR3 baseModelR3 = updateInNotices(user, ids.getLong(0));
            vh.put(R3ParamConstants.CODE, baseModelR3.getCode());
            vh.put(R3ParamConstants.MESSAGE, baseModelR3.getMessage());
            return vh;
        }

        JSONArray errorArr = new JSONArray();
        Integer accSuccess = 0, accFailed = 0;
        for (int i = 0; i < ids.size(); i++) {
            SgBaseModelR3 baseModelR3 = updateInNotices(user, ids.getLong(i));
            Integer code = baseModelR3.getCode();
            if (code == ResultCode.SUCCESS) {
                accSuccess++;
            } else {
                JSONObject errorDate = new JSONObject();
                errorDate.put(R3ParamConstants.CODE, code);
                errorDate.put(R3ParamConstants.OBJID, baseModelR3.getId());
                errorDate.put(R3ParamConstants.MESSAGE, baseModelR3.getMessage());
                errorArr.add(errorDate);
                accFailed++;
            }
        }

        String message = "重传成功记录数：" + accSuccess;
        if (!CollectionUtils.isEmpty(errorArr)) {
            message += "，重传失败记录数：" + accFailed;
            return ValueHolderUtils.fail(message, errorArr);
        } else {
            return ValueHolderUtils.success(message);
        }
    }
}
