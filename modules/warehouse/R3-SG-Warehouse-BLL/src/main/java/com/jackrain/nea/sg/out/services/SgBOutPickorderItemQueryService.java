package com.jackrain.nea.sg.out.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.out.mapper.*;
import com.jackrain.nea.sg.out.model.request.SgBOutPickorderRequest;
import com.jackrain.nea.sg.out.model.result.SgBOutPickorderResult;
import com.jackrain.nea.sg.out.model.table.*;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Locale;

/**
 * @author: heliu
 * @since: 2019/11/18
 * create at : 2019/11/18 12:52
 */
@Slf4j
@Component
public class SgBOutPickorderItemQueryService {

    /**
     * 定制接口查询出库拣货单明细和装箱明细
     *
     * @param request
     * @return
     */
    public ValueHolderV14<SgBOutPickorderResult> querySgBOutPickorderItem(SgBOutPickorderRequest request) {

        if (log.isDebugEnabled()) {
            log.debug("Finsh SgBOutPickorderItemQueryService.querySgBOutPickorderItem. Result:valueHolderV14:{},spend time:{}ms"
                    , JSONObject.toJSONString(request));
        }
        ValueHolderV14<SgBOutPickorderResult> valueHolderV14 = new ValueHolderV14<>();
        SgBOutPickorderResult sgBOutPickorderResult = new SgBOutPickorderResult();
        SgBOutPickorderMapper sgBOutPickorderMapper = ApplicationContextHandle.getBean(SgBOutPickorderMapper.class);
        SgBOutPickorderItemMapper sgBOutPickorderItemMapper = ApplicationContextHandle.getBean(SgBOutPickorderItemMapper.class);
        SgBOutPickorderTeusItemMapper sgBOutPickorderTeusItemMapper = ApplicationContextHandle.getBean(SgBOutPickorderTeusItemMapper.class);
        SgBOutPickorderNoticeItemMapper sgBOutPickorderNoticeItemMapper = ApplicationContextHandle.getBean(SgBOutPickorderNoticeItemMapper.class);
        SgBOutPickorderResultItemMapper sgBOutPickorderResultItemMapper = ApplicationContextHandle.getBean(SgBOutPickorderResultItemMapper.class);

        //参数校验
        AssertUtils.isTrue(request != null, "出库拣货单参数不能为空！");
        User user = request.getLoginUser();
        AssertUtils.notNull(user, "出库拣货单用户信息不能为空！");
        Locale locale = user.getLocale();
        AssertUtils.notNull(request.getId(), "出库拣货单Id不能为空！", locale);

        //查询出库拣货单
        SgBOutPickorder sgBOutPickorder = sgBOutPickorderMapper.selectOne(new QueryWrapper<SgBOutPickorder>().lambda()
                .eq(SgBOutPickorder::getId, request.getId())
                .eq(SgBOutPickorder::getIsactive, SgConstants.IS_ACTIVE_Y));
        AssertUtils.notNull(sgBOutPickorder, "出库拣货单不存在！", locale);
        sgBOutPickorderResult.setSgBOutPickorder(sgBOutPickorder);

        //查询拣货明细
        List<SgBOutPickorderItem> sgBOutPickorderItemList = sgBOutPickorderItemMapper.selectList(new QueryWrapper<SgBOutPickorderItem>().lambda()
                .eq(SgBOutPickorderItem::getSgBOutPickorderId, request.getId())
                .eq(SgBOutPickorderItem::getIsactive, SgConstants.IS_ACTIVE_Y));
        sgBOutPickorderResult.setSgBOutPickorderItemList(sgBOutPickorderItemList);

        //查询来源出库通知单明细
        List<SgBOutPickorderNoticeItem> sgBOutPickorderNoticeItemList = sgBOutPickorderNoticeItemMapper.selectList(new QueryWrapper<SgBOutPickorderNoticeItem>().lambda()
                .eq(SgBOutPickorderNoticeItem::getSgBOutPickorderId, request.getId())
                .eq(SgBOutPickorderNoticeItem::getIsactive, SgConstants.IS_ACTIVE_Y));
        sgBOutPickorderResult.setSgBOutPickorderNoticeItemList(sgBOutPickorderNoticeItemList);

        //查询出库结果单
        List<SgBOutPickorderResultItem> sgBOutPickorderResultItemList = sgBOutPickorderResultItemMapper.selectList(new QueryWrapper<SgBOutPickorderResultItem>().lambda()
                .eq(SgBOutPickorderResultItem::getSgBOutPickorderId, request.getId())
                .eq(SgBOutPickorderResultItem::getIsactive, SgConstants.IS_ACTIVE_Y));
        sgBOutPickorderResult.setSgBOutPickorderResultItemList(sgBOutPickorderResultItemList);

        //查询装箱明细
        List<SgBOutPickorderTeusItem> sgBOutPickorderTeusItemList = sgBOutPickorderTeusItemMapper.selectList(new QueryWrapper<SgBOutPickorderTeusItem>().lambda()
                .eq(SgBOutPickorderTeusItem::getSgBOutPickorderId, request.getId())
                .eq(SgBOutPickorderTeusItem::getIsactive, SgConstants.IS_ACTIVE_Y));
        sgBOutPickorderResult.setSgBOutPickorderTeusItemList(sgBOutPickorderTeusItemList);

        valueHolderV14.setCode(ResultCode.SUCCESS);
        valueHolderV14.setMessage("查询成功");
        valueHolderV14.setData(sgBOutPickorderResult);

        if (log.isDebugEnabled()) {
            log.debug("Finsh SgBInPickorderItemQueryService.querySgBOutPickorderItem. Result:valueHolderV14:{},spend time:{}ms"
                    , valueHolderV14.toJSONObject());
        }
        return valueHolderV14;
    }
}