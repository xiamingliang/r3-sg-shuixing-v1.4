package com.jackrain.nea.sg.inv.mapper;

import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.inv.model.table.SgBInventory;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SgBInventoryMapper extends ExtentionMapper<SgBInventory> {
}