package com.jackrain.nea.sg.out.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.google.common.collect.Lists;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.core.db.Tools;
import com.jackrain.nea.ext.sequence.util.SequenceGenUtil;
import com.jackrain.nea.oc.basic.common.OcBasicConstants;
import com.jackrain.nea.sg.basic.common.R3ParamConstants;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.config.SgStorageBoxConfig;
import com.jackrain.nea.sg.basic.model.result.SgR3BaseResult;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.out.common.SgOutConstants;
import com.jackrain.nea.sg.out.common.SgOutConstantsIF;
import com.jackrain.nea.sg.out.mapper.*;
import com.jackrain.nea.sg.out.model.request.*;
import com.jackrain.nea.sg.out.model.table.*;
import com.jackrain.nea.sg.out.utils.SgWarehouseESUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * @author leexh
 * @since 2019/4/24 15:01
 */
@Slf4j
@Component
public class SgPhyOutResultSaveService {
    @Autowired
    SgPhyOutResultSaveAndAuditService sgPhyOutResultSaveAndAuditService;

    @Autowired
    private SgStorageBoxConfig storageBoxConfig;

    @Autowired
    private SgWarehouseESUtils warehouseESUtils;

    @Transactional(rollbackFor = Exception.class)
    public ValueHolderV14<SgR3BaseResult> saveSgPhyOutResult(SgPhyOutResultBillSaveRequest request) {

        ValueHolderV14<SgR3BaseResult> result;
        AssertUtils.notNull(request, "参数不能为空！");
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ".debug, param:" + JSONObject.toJSONString(request));
        }

        // 参数校验
        checkParams(request);

        SgBPhyOutResultMapper resultMapper = ApplicationContextHandle.getBean(SgBPhyOutResultMapper.class);
        SgBPhyOutResultItemMapper resultItemMapper = ApplicationContextHandle.getBean(SgBPhyOutResultItemMapper.class);

        Long objId = request.getObjId();
        if (objId < 0) {

            SgBPhyOutNoticesMapper noticesMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesMapper.class);
            SgBPhyOutNotices outNotices = noticesMapper.selectById(request.getOutResultRequest().getSgBPhyOutNoticesId());
            // 判断相关联的出库通知单是否存在，若不存在，则结束服务
            AssertUtils.notNull(outNotices, "不存在对应的出库通知单，不允许新增出库结果单！", request.getLoginUser().getLocale());

            // 出库通知单状态为全部出库，不允许新增出库结果单

            //start sunjunlei 取消控制
//            Integer billStatus = outNotices.getBillStatus();
//            if (billStatus == SgOutConstantsIF.OUT_NOTICES_STATUS_ALL) {
//                AssertUtils.logAndThrow("出库状态已为全部出库，不允许新增出库结果单！", request.getLoginUser().getLocale());
//            }

            //新增出库结果单，判断如果已经存在此通知单关联的是否最后一次出库为是的未作废的出库结果单，则不允许保存
//            List<SgBPhyOutResult> outResults = resultMapper.selectList(new QueryWrapper<SgBPhyOutResult>().lambda()
//                    .eq(SgBPhyOutResult::getSgBPhyOutNoticesId, outNotices.getId())
//                    .eq(SgBPhyOutResult::getIsLast, SgOutConstantsIF.OUT_IS_LAST_Y)
//                    .eq(SgBPhyOutResult::getIsactive, SgConstants.IS_ACTIVE_Y));
//            if (CollectionUtils.isNotEmpty(outResults)) {
//                AssertUtils.logAndThrow("已存在此通知单关联的最后一次出库的出库结果单，不允许保存!");
//            }
            //end sunjunlei 取消控制

            // 新增出库结果单
            result = insertSgOutResult(request, resultMapper, resultItemMapper);
        } else {
            // 更新出库结果单
            result = updateSgOutResult(request, resultMapper, resultItemMapper);
        }
        if (result != null && result.getData() != null) {
            JSONObject dataJo = result.getData().getDataJo();
            if (dataJo != null) {
                Long objid = dataJo.getLong(R3ParamConstants.OBJID);
                //推送es
                warehouseESUtils.pushESByOutResult(objid, true, false, null, resultMapper, resultItemMapper);
            }
        }
        return result;
    }


    /**
     * 页面入口
     *
     * @param request
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public ValueHolderV14<SgR3BaseResult> saveSgPhyOutResultByNotices(SgPhyOutResultBillSaveRequest request) {
        ValueHolderV14<SgR3BaseResult> result;
        AssertUtils.notNull(request, "参数不能为空！");
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ".debug, param:" + JSONObject.toJSONString(request));
        }

        User loginUser = request.getLoginUser();
        AssertUtils.notNull(loginUser, "用户信息不能为空！");

        Long objId = request.getObjId();
        AssertUtils.notNull(objId, "主单据ID不能为空！", loginUser.getLocale());

        SgBPhyOutResultMapper resultMapper = ApplicationContextHandle.getBean(SgBPhyOutResultMapper.class);
        SgBPhyOutResultItemMapper resultItemMapper = ApplicationContextHandle.getBean(SgBPhyOutResultItemMapper.class);
        if (objId < 0) {
            SgPhyOutResultSaveRequest outResultRequest = request.getOutResultRequest();
            if (outResultRequest == null || outResultRequest.getSgBPhyOutNoticesId() == null) {
                AssertUtils.logAndThrow("关联的出库通知单ID不能为空！", loginUser.getLocale());
            }
            Long noticesId = outResultRequest.getSgBPhyOutNoticesId();
            SgBPhyOutNoticesMapper noticesMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesMapper.class);
            //2019-08-30 页面复制带过来的通知单号需要check
            SgBPhyOutNotices outNotices = noticesMapper.selectOne(new QueryWrapper<SgBPhyOutNotices>().lambda()
                    .eq(SgBPhyOutNotices::getId, noticesId)
                    .eq(SgBPhyOutNotices::getIsactive, SgConstants.IS_ACTIVE_Y));
            AssertUtils.notNull(outNotices, "关联的出库通知单不存在！", loginUser.getLocale());
            Integer isPassWms = outNotices.getIsPassWms();
            Integer billStatus = outNotices.getBillStatus();
            if (isPassWms != null && isPassWms == SgOutConstants.IS_PASS_WMS_Y) {
                AssertUtils.logAndThrow("通知单是否传WMS为是,不允许新增结果单!", loginUser.getLocale());
            }
            if (billStatus != null && billStatus != SgOutConstantsIF.OUT_NOTICES_STATUS_WAIT && billStatus != SgOutConstantsIF.OUT_NOTICES_STATUS_PART) {
                AssertUtils.logAndThrow("通知单已全部出库,不允许新增结果单!", loginUser.getLocale());
            }

            //2019-08-30 页面新增出库结果单，判断如果已经存在此通知单关联的是否最后一次出库为是的未作废的出库结果单，则不允许保存
            List<SgBPhyOutResult> outResults = resultMapper.selectList(new QueryWrapper<SgBPhyOutResult>().lambda()
                    .eq(SgBPhyOutResult::getSgBPhyOutNoticesId, noticesId)
                    .eq(SgBPhyOutResult::getIsLast, SgOutConstantsIF.OUT_IS_LAST_Y)
                    .eq(SgBPhyOutResult::getIsactive, SgConstants.IS_ACTIVE_Y));
            if (CollectionUtils.isNotEmpty(outResults)) {
                AssertUtils.logAndThrow("已存在此通知单关联的最后一次出库的出库结果单，不允许保存!", loginUser.getLocale());
            }

            // 复制出库通知单主表信息到出库结果单
            BeanUtils.copyProperties(outNotices, outResultRequest);
            outResultRequest.setSgBPhyOutNoticesId(outNotices.getId());
            outResultRequest.setSgBPhyOutNoticesBillno(outNotices.getBillNo());

            // 出库人信息
            outResultRequest.setOutId(loginUser.getId().longValue());
            outResultRequest.setOutName(loginUser.getName());
            outResultRequest.setOutEname(loginUser.getEname());
            outResultRequest.setOutTime(new Timestamp(System.currentTimeMillis()));

            request.setOutResultRequest(outResultRequest);


            //如果启用箱，则在保存出库结果单的时候不保存条码明细
            if (!storageBoxConfig.getBoxEnable()) {
                // 复制出库通知单明细信息到出库结果单
                SgBPhyOutNoticesItemMapper noticesItemMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesItemMapper.class);
                List<SgBPhyOutNoticesItem> noticesItems = noticesItemMapper.selectList(new QueryWrapper<SgBPhyOutNoticesItem>()
                                .eq(SgOutConstants.OUT_NOTICES_PAREN_FIELD, noticesId));
                if (CollectionUtils.isEmpty(noticesItems)) {
                    AssertUtils.logAndThrow("关联的出库通知单明细为空，不允许新增出库结果单！", loginUser.getLocale());
                }
                List<SgPhyOutResultItemSaveRequest> outResultItemRequests = new ArrayList<>();
                noticesItems.forEach(item -> {
                    SgPhyOutResultItemSaveRequest resultItem = new SgPhyOutResultItemSaveRequest();
                    BeanUtils.copyProperties(item, resultItem);
                    resultItem.setSgBPhyOutNoticesItemId(item.getId());
                    resultItem.setQty(item.getQtyDiff());
                    resultItem.setAmtListOut(item.getPriceList().multiply(item.getQtyDiff()));
                    outResultItemRequests.add(resultItem);
                });
                request.setOutResultItemRequests(outResultItemRequests);
            }

            /**
             * 开启箱功能后，新增出库结果单时,保存录入明细
             */
            if (storageBoxConfig.getBoxEnable()) {
                // 复制出库结果单录入明细信息到出库结果单
                SgBPhyOutNoticesImpItemMapper impItemMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesImpItemMapper.class);
                List<SgBPhyOutNoticesImpItem> impItems = impItemMapper.selectList( new QueryWrapper<SgBPhyOutNoticesImpItem>()
                                .eq(SgOutConstants.OUT_NOTICES_PAREN_FIELD, noticesId));
                if (CollectionUtils.isEmpty(impItems)) {
                    AssertUtils.logAndThrow("关联的出库通知单录入明细为空，不允许新增出库结果单！", loginUser.getLocale());
                }

                List<SgPhyOutResultImpItemSaveRequest> outResultImpItemRequests = new ArrayList<>();
                impItems.forEach(item -> {
                    SgPhyOutResultImpItemSaveRequest resultItem = new SgPhyOutResultImpItemSaveRequest();
                    BeanUtils.copyProperties(item, resultItem);
                    resultItem.setQtyOut(item.getQty().subtract(item.getQtyOut()));
                    outResultImpItemRequests.add(resultItem);
                });
                request.setImpItemList(outResultImpItemRequests);
            }
            // 调用保存
            result = insertSgOutResult(request, resultMapper, resultItemMapper);
        } else {
            result = updateSgOutResult(request, resultMapper, resultItemMapper);
        }
        if (result != null && result.getData() != null) {
            JSONObject dataJo = result.getData().getDataJo();
            if (dataJo != null) {
                Long objid = dataJo.getLong(R3ParamConstants.OBJID);
                SgBPhyOutResult results = resultMapper.selectOne(new QueryWrapper<SgBPhyOutResult>().lambda().eq(SgBPhyOutResult::getId, objid));
                // 零售发货单- 新增多包裹明细
                if (results.getSourceBillType() == SgConstantsIF.BILL_TYPE_RETAIL) {
                    //删除原结果单相关联的多包裹明细
                    SgBOutDeliveryMapper deliveryMapper = ApplicationContextHandle.getBean(SgBOutDeliveryMapper.class);
                    int count = deliveryMapper.delete(new UpdateWrapper<SgBOutDelivery>().lambda()
                            .eq(SgBOutDelivery::getSgBPhyOutResultId, results.getId()));
                    if (log.isDebugEnabled()) {
                        log.debug("删除原结果单相关联的多包裹明细" + count + "条");
                    }
                    List<SgPhyOutDeliverySaveRequest> deliveryList = Lists.newArrayList();
                    if (storageBoxConfig.getBoxEnable()) {
                        SgBPhyOutResultImpItemMapper impItemMapper = ApplicationContextHandle.getBean(SgBPhyOutResultImpItemMapper.class);
                        List<SgBPhyOutResultImpItem> resultImpItems = impItemMapper.selectList(new QueryWrapper<SgBPhyOutResultImpItem>().lambda()
                                .eq(SgBPhyOutResultImpItem::getSgBPhyOutResultId, objid));
                        for (SgBPhyOutResultImpItem resultsItem : resultImpItems) {
                            SgPhyOutDeliverySaveRequest deliverySaveRequest = new SgPhyOutDeliverySaveRequest();
                            BeanUtils.copyProperties(resultsItem, deliverySaveRequest);
                            Long cpCLogisticsId = results.getCpCLogisticsId();
                            String cpCLogisticsEcode = results.getCpCLogisticsEcode();
                            String cpCLogisticsEname = results.getCpCLogisticsEname();
                            String logisticNumber = results.getLogisticNumber();
                            Long sourceBillId = results.getSourceBillId();
                            deliverySaveRequest.setCpCLogisticsId(cpCLogisticsId != null ? cpCLogisticsId.toString() : "");
                            deliverySaveRequest.setCpCLogisticsEcode(cpCLogisticsEcode);
                            deliverySaveRequest.setCpCLogisticsEname(cpCLogisticsEname);
                            deliverySaveRequest.setLogisticNumber(logisticNumber);
                            deliverySaveRequest.setOcBOrderId(sourceBillId);
                            deliverySaveRequest.setQty(resultsItem.getQtyOut());
                            deliverySaveRequest.setPsCClrId(resultsItem.getPsCSpec1Id());
                            deliverySaveRequest.setPsCClrEcode(resultsItem.getPsCSpec1Ecode());
                            deliverySaveRequest.setPsCClrEname(resultsItem.getPsCSpec1Ename());
                            deliverySaveRequest.setPsCSizeId(resultsItem.getPsCSpec2Id());
                            deliverySaveRequest.setPsCSizeEcode(resultsItem.getPsCSpec2Ecode());
                            deliverySaveRequest.setPsCSizeEname(resultsItem.getPsCSpec2Ename());
                            deliverySaveRequest.setWeight(BigDecimal.ONE);
                            deliverySaveRequest.setSize(BigDecimal.ONE);
                            deliveryList.add(deliverySaveRequest);
                        }
                    }else {
                        List<SgBPhyOutResultItem> resultsItems = resultItemMapper.selectList(new QueryWrapper<SgBPhyOutResultItem>().lambda()
                                .eq(SgBPhyOutResultItem::getSgBPhyOutResultId, objid));
                        for (SgBPhyOutResultItem resultsItem : resultsItems) {
                            SgPhyOutDeliverySaveRequest deliverySaveRequest = new SgPhyOutDeliverySaveRequest();
                            BeanUtils.copyProperties(resultsItem, deliverySaveRequest);
                            Long cpCLogisticsId = results.getCpCLogisticsId();
                            String cpCLogisticsEcode = results.getCpCLogisticsEcode();
                            String cpCLogisticsEname = results.getCpCLogisticsEname();
                            String logisticNumber = results.getLogisticNumber();
                            Long sourceBillId = results.getSourceBillId();
                            deliverySaveRequest.setCpCLogisticsId(cpCLogisticsId != null ? cpCLogisticsId.toString() : "");
                            deliverySaveRequest.setCpCLogisticsEcode(cpCLogisticsEcode);
                            deliverySaveRequest.setCpCLogisticsEname(cpCLogisticsEname);
                            deliverySaveRequest.setLogisticNumber(logisticNumber);
                            deliverySaveRequest.setOcBOrderId(sourceBillId);
                            deliverySaveRequest.setPsCClrId(resultsItem.getPsCSpec1Id());
                            deliverySaveRequest.setPsCClrEcode(resultsItem.getPsCSpec1Ecode());
                            deliverySaveRequest.setPsCClrEname(resultsItem.getPsCSpec1Ename());
                            deliverySaveRequest.setPsCSizeId(resultsItem.getPsCSpec2Id());
                            deliverySaveRequest.setPsCSizeEcode(resultsItem.getPsCSpec2Ecode());
                            deliverySaveRequest.setPsCSizeEname(resultsItem.getPsCSpec2Ename());
                            deliverySaveRequest.setWeight(BigDecimal.ONE);
                            deliverySaveRequest.setSize(BigDecimal.ONE);
                            deliveryList.add(deliverySaveRequest);
                        }
                    }
                    SgPhyOutResultSaveAndAuditService saveAndAuditService = ApplicationContextHandle.getBean(SgPhyOutResultSaveAndAuditService.class);
                    saveAndAuditService.addOutDelivery(loginUser, deliveryList, results.getId(), loginUser.getLocale());
                }
                //推送es
                warehouseESUtils.pushESByOutResult(objid, true, false, null, resultMapper, resultItemMapper);
            }
        }
        return result;
    }


    /**
     * 更新出库结果单
     *
     * @param request
     * @param resultMapper
     * @param resultItemMapper
     * @return
     */
    public ValueHolderV14<SgR3BaseResult> updateSgOutResult(SgPhyOutResultBillSaveRequest request,
                                                            SgBPhyOutResultMapper resultMapper,
                                                            SgBPhyOutResultItemMapper resultItemMapper) {

        ValueHolderV14<SgR3BaseResult> v14 = new ValueHolderV14<>();
        User loginUser = request.getLoginUser();
        String eName = loginUser.getEname();

        Long objId = request.getObjId();
        SgBPhyOutResult outResult = resultMapper.selectById(objId);
        AssertUtils.notNull(outResult, "当前出库结果单已不存在！objId=" + objId, loginUser.getLocale());

        SgBPhyOutResultItemMapper bPhyOutResultItemMapper = ApplicationContextHandle.getBean(SgBPhyOutResultItemMapper.class);

        if (!storageBoxConfig.getBoxEnable()) {
            // 更新明细
            if (!CollectionUtils.isEmpty(request.getOutResultItemRequests())) {
                request.getOutResultItemRequests().forEach(item -> {
                    SgBPhyOutResultItem outResultItem = new SgBPhyOutResultItem();
                    BeanUtils.copyProperties(item, outResultItem);
                    BigDecimal priceList = item.getPriceList();
                    BigDecimal qty = Optional.ofNullable(item.getQty()).orElse(BigDecimal.ZERO);
                    if (priceList == null) {
                        //通过原明细获取吊牌价
                        SgBPhyOutResultItem orgOutResultItem = bPhyOutResultItemMapper.selectById(item.getId());
                        priceList = Optional.ofNullable(orgOutResultItem.getPriceList()).orElse(BigDecimal.ZERO);
                    }
                    //更新出库吊牌金额
                    outResultItem.setAmtListOut(qty.multiply(priceList));
                    StorageESUtils.setBModelDefalutDataByUpdate(outResultItem, loginUser);
                    outResultItem.setModifierename(eName);

                    UpdateWrapper<SgBPhyOutResultItem> updateWrapper = new UpdateWrapper<>();
                    SgBPhyOutResultItem updateParam = new SgBPhyOutResultItem();
                    updateParam.setSgBPhyOutResultId(objId);
                    updateParam.setId(item.getId());
                    updateWrapper.setEntity(updateParam);
                    resultItemMapper.update(outResultItem, updateWrapper);
                });
            }
        } else {
            if (!CollectionUtils.isEmpty(request.getImpItemList())) {
                SgBPhyOutResultImpItemMapper impItemMapper = ApplicationContextHandle.getBean(SgBPhyOutResultImpItemMapper.class);
                request.getImpItemList().forEach(item -> {
                    SgBPhyOutResultImpItem outResultImpItem = new SgBPhyOutResultImpItem();
                    BeanUtils.copyProperties(item, outResultImpItem);
                    StorageESUtils.setBModelDefalutDataByUpdate(outResultImpItem, loginUser);
                    outResultImpItem.setModifierename(eName);

                    UpdateWrapper<SgBPhyOutResultImpItem> updateWrapper = new UpdateWrapper<>();
                    SgBPhyOutResultImpItem updateParam = new SgBPhyOutResultImpItem();
                    updateParam.setSgBPhyOutResultId(objId);
                    updateParam.setId(item.getId());
                    updateWrapper.setEntity(updateParam);
                    impItemMapper.update(outResultImpItem, updateWrapper);
                });
            }

        }

        // 更新主表
        SgBPhyOutResult updateOutResult = new SgBPhyOutResult();
        if (request.getOutResultRequest() != null) {
            BeanUtils.copyProperties(request.getOutResultRequest(), updateOutResult);
            updateOutResult.setId(request.getObjId());
        } else {
            updateOutResult = resultMapper.selectById(request.getObjId());
        }

        if (!storageBoxConfig.getBoxEnable()) {
            setOutResultTot(objId, resultItemMapper, updateOutResult);
        }
        updateOutResult.setId(objId);
        StorageESUtils.setBModelDefalutDataByUpdate(updateOutResult, loginUser);
        updateOutResult.setModifierename(eName);
        resultMapper.updateById(updateOutResult);

        SgR3BaseResult data = new SgR3BaseResult();
        JSONObject dataJo = new JSONObject();
        dataJo.put(R3ParamConstants.OBJID, objId);
        dataJo.put(R3ParamConstants.TABLENAME, SgConstants.SG_B_PHY_OUT_RESULT);
        data.setDataJo(dataJo);
        v14.setCode(ResultCode.SUCCESS);
        v14.setMessage("保存成功！");
        v14.setData(data);
        return v14;
    }


    /**
     * 新增出库结果单
     *
     * @param request 入参
     * @return 出参
     */
    public ValueHolderV14<SgR3BaseResult> insertSgOutResult(SgPhyOutResultBillSaveRequest request,
                                                            SgBPhyOutResultMapper resultMapper,
                                                            SgBPhyOutResultItemMapper resultItemMapper) {

        ValueHolderV14<SgR3BaseResult> v14 = new ValueHolderV14<>();
        User loginUser = request.getLoginUser();
        String eName = loginUser.getEname();

        SgBPhyOutResult outResult = new SgBPhyOutResult();
        BeanUtils.copyProperties(request.getOutResultRequest(), outResult);

        // 获取单据ID
        String table = SgConstants.SG_B_PHY_OUT_RESULT;
        Long objId = Tools.getSequence(table);
        outResult.setId(objId);

        if (!storageBoxConfig.getBoxEnable()) {
            int insertItemResult = insertOutResultItem(objId, request.getOutResultItemRequests(), request.getLoginUser());
            log.debug(this.getClass().getName() + ".debug, 出库结果单明细新增结果：" + insertItemResult);
        }

        if (storageBoxConfig.getBoxEnable()) {
            SgPhyOutResultConvertService service = ApplicationContextHandle.getBean(SgPhyOutResultConvertService.class);
            //出库通结果录入明细新增
            service.insertOutResultImpItem(objId, request.getImpItemList(), request.getLoginUser());
        }
        // 获取单据编号
        JSONObject obj = new JSONObject();
        obj.put(SgConstants.SG_B_PHY_OUT_NOTICES.toUpperCase(), outResult);
        String billNo = SequenceGenUtil.generateSquence(SgOutConstants.SQE_SG_B_OUT_RESULT, obj, loginUser.getLocale(), false);
//        String billNo = "OR" + new Date().getTime();
        outResult.setBillNo(billNo);
        if (outResult.getBillDate() == null) {
            outResult.setBillDate(new Date());
        }

        if (!storageBoxConfig.getBoxEnable()) {
            setOutResultTot(objId, resultItemMapper, outResult);
        }
        outResult.setIsactive(SgConstants.IS_ACTIVE_Y);    // 未作废
        outResult.setBillStatus(SgOutConstantsIF.OUT_NOTICES_STATUS_WAIT);   // 待出库

        StorageESUtils.setBModelDefalutData(outResult, loginUser);
        outResult.setOwnerename(eName);
        outResult.setModifierename(eName);

        int insert = resultMapper.insert(outResult);
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ".debug, 出库通知单新增结果：" + insert);
        }

        SgR3BaseResult data = new SgR3BaseResult();
        JSONObject dataJo = new JSONObject();
        dataJo.put(R3ParamConstants.OBJID, objId);
        dataJo.put(R3ParamConstants.TABLENAME, SgConstants.SG_B_PHY_OUT_RESULT);
        data.setDataJo(dataJo);
        v14.setCode(ResultCode.SUCCESS);
        v14.setMessage("保存成功！");
        v14.setData(data);
        return v14;
    }


    /**
     * 设置主表数量和金额
     *
     * @param objId            主单据ID
     * @param resultItemMapper mapper
     * @param outResult        主表对象
     */
    public void setOutResultTot(Long objId, SgBPhyOutResultItemMapper resultItemMapper, SgBPhyOutResult outResult) {
        SgBPhyOutResultItem outResultItem = resultItemMapper.selectSumAmt(objId);
        BigDecimal totQtyOut = BigDecimal.ZERO;
        BigDecimal totAmtListOut = BigDecimal.ZERO;
        if (outResultItem != null) {
            totQtyOut = outResultItem.getQty();
            totAmtListOut = outResultItem.getAmtListOut();
        }

        outResult.setTotQtyOut(totQtyOut);  // 总出库数量
        outResult.setTotAmtListOut(totAmtListOut);  // 总出库吊牌金额
    }

    /**
     * 出库通结果明细新增
     *
     * @param objId
     * @param items
     * @param user
     * @return
     */
    public int insertOutResultItem(Long objId, List<SgPhyOutResultItemSaveRequest> items, User user) {

        SgBPhyOutResultItemMapper outResultItemMapper = ApplicationContextHandle.getBean(SgBPhyOutResultItemMapper.class);
        List<SgBPhyOutResultItem> outResultItems = new ArrayList<>();
        String eName = user.getEname();

        items.forEach(item -> {
            SgBPhyOutResultItem outResultItem = new SgBPhyOutResultItem();
            BeanUtils.copyProperties(item, outResultItem);
            Long id = Tools.getSequence(SgConstants.SG_B_PHY_OUT_RESULT_ITEM);
            outResultItem.setId(id);
            outResultItem.setSgBPhyOutResultId(objId);
            outResultItem.setAmtListOut(item.getPriceList().multiply(item.getQty()));    // 出库吊牌金额

            StorageESUtils.setBModelDefalutData(outResultItem, user);
            outResultItem.setOwnerename(eName);
            outResultItem.setModifierename(eName);
            outResultItems.add(outResultItem);
        });

        List<List<SgBPhyOutResultItem>> baseModelPageList = StorageESUtils.getBaseModelPageList(
                outResultItems, SgConstants.SG_COMMON_INSERT_PAGE_SIZE);

        int result = 0;
        for (List<SgBPhyOutResultItem> pageList : baseModelPageList) {

            if (CollectionUtils.isEmpty(pageList)) {
                continue;
            }
            int count = outResultItemMapper.batchInsert(pageList);
            if (count != pageList.size()) {
                log.error(this.getClass().getName() + " 出库结果单明细批量新增失败！");
                AssertUtils.logAndThrow("出库结果单明细批量新增失败！", user.getLocale());
            }
            result += count;
        }
        return result;
    }

    /**
     * 参数校验
     *
     * @param request
     */
    public void checkParams(SgPhyOutResultBillSaveRequest request) {

        AssertUtils.notNull(request.getLoginUser(), "用户信息不能为空！");

        if (storageBoxConfig.getBoxEnable()) {
            SgPhyOutResultConvertService sgPhyOutNoticesConvertService = ApplicationContextHandle.getBean(SgPhyOutResultConvertService.class);
            sgPhyOutNoticesConvertService.sgPhyOutResultRequestConvert(request);
        }

        Long objId = request.getObjId();
        AssertUtils.notNull(objId, "出库结果单ID不能为空！");
        if (objId < 0) {
            SgPhyOutResultSaveRequest outResultRequest = request.getOutResultRequest();

            AssertUtils.notNull(outResultRequest, "出库结果单不能为空！");
            AssertUtils.notNull(outResultRequest.getSgBPhyOutNoticesId(), "关联出库通知单ID不能为空！");
            AssertUtils.notNull(outResultRequest.getCpCPhyWarehouseId(), "实体仓ID不能为空！");
            AssertUtils.notNull(outResultRequest.getCpCPhyWarehouseEcode(), "实体仓编码不能为空！");
            AssertUtils.notNull(outResultRequest.getCpCPhyWarehouseEname(), "实体仓名称不能为空！");
            AssertUtils.notNull(outResultRequest.getSourceBillType(), "来源单据类型不能为空！");
            AssertUtils.notNull(outResultRequest.getSourceBillId(), "来源单据ID不能为空！");
            AssertUtils.notNull(outResultRequest.getSourceBillNo(), "来源单据编号不能为空！");

            // 出库日期不传时，默认当前日期
            if (outResultRequest.getOutTime() == null) {
                outResultRequest.setOutTime(new Date());
            }

            if (storageBoxConfig.getBoxEnable()) {
                if (CollectionUtils.isEmpty(request.getImpItemList())) {
                    AssertUtils.logAndThrow("出库单结果单录入明细为空，不允许新增！");
                }
                request.getImpItemList().forEach(item -> {
                    AssertUtils.notNull(item.getIsTeus(), "是否为箱标志不能为空!");
                    if (item.getIsTeus().equals(OcBasicConstants.IS_MATCH_SIZE_Y)) {
                        AssertUtils.notNull(item.getPsCTeusId(), "箱id不能为空!");
                        AssertUtils.notNull(item.getPsCTeusEcode(), "箱号不能为空!");
                    } else {
                        AssertUtils.notNull(item.getPsCSkuId(), "条码ID不能为空!");
                        AssertUtils.notNull(item.getPsCSkuEcode(), "条码编码不能为空!");
                    }
                    AssertUtils.notNull(item.getQtyOut(), "出库数量不能为空！");
                    AssertUtils.notNull(item.getPsCProId(), "商品ID不能为空！");
                    AssertUtils.notNull(item.getPsCProEcode(), "商品编码不能为空！");
                    AssertUtils.notNull(item.getPsCProEname(), "商品名称不能为空！");
                    AssertUtils.notNull(item.getPsCSpec1Id(), "商品规格1ID不能为空！");
                    AssertUtils.notNull(item.getPsCSpec1Ecode(), "商品规格1编码不能为空！");
                    AssertUtils.notNull(item.getPsCSpec1Ename(), "商品规格1名称不能为空！");
                });
            } else {
                if (CollectionUtils.isEmpty(request.getOutResultItemRequests())) {
                    AssertUtils.logAndThrow("出库单结果单明细为空，不允许新增！");
                }

                request.getOutResultItemRequests().forEach(item -> {
//                AssertUtils.notNull(item.getSgBPhyOutNoticesItemId(), "所关联的出库通知单单明细ID不能为空！");
                    AssertUtils.notNull(item.getQty(), "出库数量不能为空！");
                    AssertUtils.notNull(item.getPsCProId(), "商品ID不能为空！");
                    AssertUtils.notNull(item.getPsCProEcode(), "商品编码不能为空！");
                    AssertUtils.notNull(item.getPsCProEname(), "商品名称不能为空！");
                    AssertUtils.notNull(item.getPsCSkuId(), "skuID不能为空！");
                    AssertUtils.notNull(item.getPsCSkuEcode(), "sku编码不能为空！");
//                AssertUtils.notNull(item.getGbcode(), "国标码不能为空！");
                    AssertUtils.notNull(item.getPsCSpec1Id(), "商品规格1ID不能为空！");
                    AssertUtils.notNull(item.getPsCSpec1Ecode(), "商品规格1编码不能为空！");
                    AssertUtils.notNull(item.getPsCSpec1Ename(), "商品规格1名称不能为空！");
                    AssertUtils.notNull(item.getPsCSpec2Id(), "商品规格2ID不能为空！");
                    AssertUtils.notNull(item.getPsCSpec2Ecode(), "商品规格2编码不能为空！");
                    AssertUtils.notNull(item.getPsCSpec2Ename(), "商品规格2名称不能为空！");
                });
            }

        }

    }
}
