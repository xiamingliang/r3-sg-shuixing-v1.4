package com.jackrain.nea.sg.in.services;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.Feature;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.in.mapper.SgBPhyInNoticesItemMapper;
import com.jackrain.nea.sg.in.mapper.SgBPhyInNoticesMapper;
import com.jackrain.nea.sg.out.utils.SgWarehouseESUtils;
import com.jackrain.nea.sys.CommandAdapter;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.query.DefaultWebEvent;
import com.jackrain.nea.web.query.QuerySession;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Author: ChenChen
 * @Description:入库通知单删除服务
 * @Date: Create in 10:15 2019/4/24
 */
@Slf4j
@Component
public class SgPhyInNoticesDeleteR3Service extends CommandAdapter {

    @Autowired
    private SgWarehouseESUtils warehouseESUtils;

    @Override
    public ValueHolder execute(QuerySession session) throws NDSException {
        ValueHolder holder = new ValueHolder();
        DefaultWebEvent event = session.getEvent();
        JSONObject param = JSON.parseObject(JSON.toJSONStringWithDateFormat(event.getParameterValue("param"),
                "yyyy-MM-dd HH:mm:ss", SerializerFeature.WriteMapNullValue), Feature.OrderedField);
        JSONObject fixcolumn = param.getJSONObject("fixcolumn");
        JSONArray itemArray = fixcolumn.getJSONArray("");
        if (itemArray.size() > 0) {
            SgBPhyInNoticesItemMapper noticesItemMapper = ApplicationContextHandle.getBean(SgBPhyInNoticesItemMapper.class);
            long inNoticesId = noticesItemMapper.selectById(itemArray.getLong(0)).getSgBPhyInNoticesId();
            if (noticesItemMapper.deleteBatchIds(itemArray.toJavaList(Long.class)) <= 0) {

                holder.put("code", ResultCode.FAIL);
                holder.put("message", Resources.getMessage("删除入库通知单明细失败！", session.getLocale()));
                return holder;
            }
            //推送es
            SgBPhyInNoticesMapper noticesMapper = ApplicationContextHandle.getBean(SgBPhyInNoticesMapper.class);
            warehouseESUtils.pushESByInNotices(inNoticesId, false, true, itemArray, noticesMapper, null);
        }
        holder.put("code", ResultCode.SUCCESS);
        return holder;
    }
}
