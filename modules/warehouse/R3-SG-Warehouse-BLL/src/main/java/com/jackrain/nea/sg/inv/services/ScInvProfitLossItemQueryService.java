package com.jackrain.nea.sg.inv.services;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.inv.model.request.ScInvProfitLossMarginQueryCmdRequest;
import com.jackrain.nea.sg.inv.model.result.ScInvProfitLossQueryItemResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * 查询预盈亏明细
 *
 * @author: 舒威
 * @since: 2019/4/8
 * create at : 2019/4/8 16:21
 */
@Slf4j
@Component
public class ScInvProfitLossItemQueryService {

    /**
     * 查询预盈亏明细
     */
    public ValueHolderV14<ScInvProfitLossQueryItemResult> queryProfitLossItem(ScInvProfitLossMarginQueryCmdRequest model) {
        if (log.isDebugEnabled()) {
            log.debug("Start ScInvProfitLossItemQueryService.queryProfitLossItem. Receive:request:{}", JSONObject.toJSONString(model));
        }
        Long storeId = model.getWarehouseId();
        Integer inventoryType = model.getInventoryType();
        Date inventoryDate = model.getInventoryDate();

        User loginUser = model.getLoginUser();
        AssertUtils.notNull(loginUser, "当前用户未登录！");
        Locale locale = loginUser.getLocale();
        AssertUtils.notNull(storeId, "请选择盘点店仓！", locale);
        AssertUtils.notNull(inventoryType, "请选择盘点类型！", locale);
        AssertUtils.notNull(inventoryDate, "请选择盘点日期！", locale);

        try {
            ScInvProfitLossItemService profitLossItemService = ApplicationContextHandle.getBean(ScInvProfitLossItemService.class);
            ValueHolderV14<ScInvProfitLossQueryItemResult> queryResult = profitLossItemService.queryProfitLossItemInventory(model);
            if (null == queryResult || queryResult.getCode() == ResultCode.FAIL || queryResult.getData() == null) {
                return new ValueHolderV14<>(ResultCode.FAIL, "查询预盈亏明细失败！");
            } else {
                queryResult.getData().setTHead(getTHead(model.getIsQueryTeus()));
                return queryResult;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ValueHolderV14<>(ResultCode.FAIL, e.getMessage());
        }
    }

    /**
     * 封装表头
     *
     * @param isQueryTeus 是否查询箱
     */
    public List getTHead(Boolean isQueryTeus) {
        List tHeadList = new ArrayList();
        if (isQueryTeus) {
            tHeadList.add(addTHeadMap("箱号", "PS_C_TEUS_ECODE", true, false));
            tHeadList.add(addTHeadMap("配码", "PS_C_MATCHSIZE_ECODE", true, false));
        }
        tHeadList.add(addTHeadMap("商品编码", "PS_C_PRO_ECODE", true, false));
        tHeadList.add(addTHeadMap("商品名称", "PS_C_PRO_ENAME", false, false));
        tHeadList.add(addTHeadMap("实际盘点数量", "SUM_QTY", false, true));
        tHeadList.add(addTHeadMap("账面库存数量", "QTY_STORAGE", false, true));
        tHeadList.add(addTHeadMap("差异数量", "QTY_DIFF", false, true));
        //tHeadList.add(addTHeadMap("吊牌价", "PRICE_RETAIL", false, false));
        //tHeadList.add(addTHeadMap("差异金额", "DIFF_PRICE", false, true));
        return tHeadList;
    }

    private HashMap addTHeadMap(String label, String val, boolean isOrder, Boolean isTotal) {
        HashMap tHead = new HashMap();
        tHead.put("label", label);
        tHead.put("name", val);
        if (isOrder) {
            tHead.put("order", isOrder);
        }
        if (isTotal) {
            tHead.put("total", isTotal);
        }
        return tHead;
    }
}
