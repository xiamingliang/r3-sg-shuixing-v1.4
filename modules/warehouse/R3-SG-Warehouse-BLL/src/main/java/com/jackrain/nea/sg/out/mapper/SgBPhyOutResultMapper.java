package com.jackrain.nea.sg.out.mapper;

import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutResult;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface SgBPhyOutResultMapper extends ExtentionMapper<SgBPhyOutResult> {

    /**
     * 分页查询出库结果单
     */
    @Select("SELECT * FROM " + SgConstants.SG_B_PHY_OUT_RESULT + " ORDER BY ID LIMIT #{page} OFFSET #{offset}")
    List<SgBPhyOutResult> selectListByPage(@Param("page") int page, @Param("offset") int offset);
}