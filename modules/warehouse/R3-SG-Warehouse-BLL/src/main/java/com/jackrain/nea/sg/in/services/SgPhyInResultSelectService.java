package com.jackrain.nea.sg.in.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.in.mapper.SgBPhyInResultMapper;
import com.jackrain.nea.sg.in.model.request.SgBPhyInResultSaveRequest;
import com.jackrain.nea.sg.in.model.table.SgBPhyInResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
public class SgPhyInResultSelectService {
    public ValueHolderV14<List<SgBPhyInResult>> selectSgPhyInResult(SgBPhyInResultSaveRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("Start SgPhyInResultSelectService.selectSgPhyInResult. ReceiveParams:SgPhyInResultBillSaveRequest="
                    + JSONObject.toJSONString(request) + ";");
        }
        ValueHolderV14<List<SgBPhyInResult>> vh = new ValueHolderV14();
        SgBPhyInResultMapper resultMapper = ApplicationContextHandle.getBean(SgBPhyInResultMapper.class);

        if (request == null) {
            throw new NDSException(Resources.getMessage("参数为空!"));
        }
        Long source_id = request.getSourceBillId();
        Integer source_type = request.getSourceBillType();
        if (source_id == null) {
            throw new NDSException(Resources.getMessage("来源单据ID为空!"));
        }
        if (source_type == null) {
            throw new NDSException(Resources.getMessage("来源单据类型为空!"));
        }

        List<SgBPhyInResult> billList = resultMapper.selectList(new QueryWrapper<SgBPhyInResult>().lambda()
                .eq(SgBPhyInResult::getSourceBillId, source_id)
                .eq(SgBPhyInResult::getSourceBillType, source_type)
                .eq(request.getBillStatus() != null, SgBPhyInResult::getBillStatus, request.getBillStatus())
                .eq(SgBPhyInResult::getIsactive, SgConstants.IS_ACTIVE_Y));
        vh.setCode(ResultCode.SUCCESS);
        vh.setMessage("获取入库结果单成功!");
        vh.setData(billList);
        return vh;
    }
}
