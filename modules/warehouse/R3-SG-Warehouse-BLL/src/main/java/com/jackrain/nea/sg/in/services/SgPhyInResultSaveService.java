package com.jackrain.nea.sg.in.services;


import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.ext.sequence.util.SequenceGenUtil;
import com.jackrain.nea.model.util.ModelUtil;
import com.jackrain.nea.oc.basic.common.OcBasicConstants;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.config.SgStorageBoxConfig;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.in.common.SgInConstants;
import com.jackrain.nea.sg.in.mapper.SgBPhyInResultDefectItemMapper;
import com.jackrain.nea.sg.in.mapper.SgBPhyInResultItemMapper;
import com.jackrain.nea.sg.in.mapper.SgBPhyInResultMapper;
import com.jackrain.nea.sg.in.model.request.*;
import com.jackrain.nea.sg.in.model.table.SgBPhyInResult;
import com.jackrain.nea.sg.in.model.table.SgBPhyInResultDefectItem;
import com.jackrain.nea.sg.in.model.table.SgBPhyInResultItem;
import com.jackrain.nea.sg.out.utils.SgWarehouseESUtils;
import com.jackrain.nea.sg.send.utils.SgStoreUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.assertj.core.util.Lists;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * 入库结果单业务处理
 *
 * @author jiang.cj
 * @since 2019-4-22
 * create at : 2019-4-22
 */
@Slf4j
@Component
public class SgPhyInResultSaveService {

    @Autowired
    private SgStorageBoxConfig storageBoxConfig;

    @Autowired
    private SgBPhyInResultMapper inResultMapper;

    @Autowired
    private SgBPhyInResultItemMapper inResultItemMapper;

    @Autowired
    private SgBPhyInResultMapper mapper;

    @Autowired
    private SgBPhyInResultItemMapper itemMapper;

    @Autowired
    private SgWarehouseESUtils warehouseESUtils;

    /**
     * 添加(更新)入库结果单
     *
     * @param request request-model
     * @return
     */
    @Transactional(rollbackFor = NDSException.class)
    public ValueHolderV14 saveSgBPhyInResult(SgPhyInResultBillSaveRequest request) throws NDSException {
        if (log.isDebugEnabled()) {
            log.debug("Start SgPhyInResultSaveService.saveSgBPhyInResult. ReceiveParams:SgPhyInResultBillSaveRequest=" + JSONObject.toJSONString(request) + ";");
        }
        ValueHolderV14 vh = new ValueHolderV14(ResultCode.SUCCESS, "success");

        User user = request.getLoginUser();
        Long objId = request.getSgBPhyInResultSaveRequest().getId();
        if (objId == null || objId < 0) {
            //校验参数
            checkParams(request);
            //执行新增操作
            objId = insertSgPhyInResult(request, user);
        } else {
            //执行更新操作
            objId = updateSgPhyInResult(request, user);
        }
        //推送es
        warehouseESUtils.pushESByInResult(objId, true, false, null, mapper, itemMapper);
        vh.setData(objId);
        return vh;
    }


    /****
     * 新增入库结果单
     * @param request request-model
     * @param user  用戶
     */
    private Long insertSgPhyInResult(SgPhyInResultBillSaveRequest request, User user) {
        Long objId = ModelUtil.getSequence(SgConstants.SG_B_PHY_IN_RESULT);

        //插入主表
        String billNo = insertMainRecord(request, objId, user);

        if (storageBoxConfig.getBoxEnable()) {
            //录入明细新增
            SgPhyInResultTeusFunctionService service = ApplicationContextHandle.getBean(SgPhyInResultTeusFunctionService.class);
            service.insertInResultImpItem(objId, request.getImpItemList(), user);
            //箱明细新增(审核时再转换)
            //service.insertInResultTeusItem(objId, request.getTeusItemList(), user);
        } else {
            //明细表新增
            insertSubRecords(request.getItemList(), objId, user);
        }

        if (CollectionUtils.isNotEmpty(request.getDefectItemList())) {
            //插入残次品明细表
            insertSubRecordsDefect(request.getDefectItemList(), objId, billNo, user);
        }
        return objId;
    }

    /**
     * 入库结果残次品明细插入
     *
     * @param defectItemList 残次品明细
     * @param objId          入库结果单id
     * @param billNo         入库结果单单据编号
     * @param user           用户  @return 新增成功数量
     */
    private void insertSubRecordsDefect(List<SgBPhyInResultDefectItemSaveRequest> defectItemList, Long objId, String billNo, User user) {
        SgBPhyInResultDefectItemMapper defectItemMapper = ApplicationContextHandle.getBean(SgBPhyInResultDefectItemMapper.class);
        List<SgBPhyInResultDefectItem> list = Lists.newArrayList();
        defectItemList.forEach(item -> {
            SgBPhyInResultDefectItem defectItem = new SgBPhyInResultDefectItem();
            BeanUtils.copyProperties(item, defectItem);
            Long id = ModelUtil.getSequence(SgConstants.SG_B_PHY_IN_RESULT_DEFECT_ITEM);
            defectItem.setId(id);
            defectItem.setSgBPhyInResultId(objId);
            defectItem.setSgBPhyInResultBillNo(billNo);
            defectItem.setAmtListIn(item.getQtyIn().multiply(item.getPriceList()));
            defectItem.setOwnerename(user.getEname());
            defectItem.setModifierename(user.getEname());
            //设置明细用户信息字段
            StorageESUtils.setBModelDefalutData(defectItem, user);
            list.add(defectItem);
        });

        SgStoreUtils.batchInsertTeus(list, "入库结果单残次品明细", SgConstants.SG_COMMON_INSERT_PAGE_SIZE, defectItemMapper);
    }

    /**
     * 入库结果明细插入
     *
     * @param itemList 明細request-model
     * @param objId    主表ID
     * @param user     用戶
     */
    private void insertSubRecords(List<SgBPhyInResultItemSaveRequest> itemList, Long objId, User user) {
        List<SgBPhyInResultItem> list = Lists.newArrayList();
        itemList.forEach(item -> {
            SgBPhyInResultItem itemRequest = new SgBPhyInResultItem();
            //复制字段属性值
            BeanUtils.copyProperties(item, itemRequest);
            Long itemId = ModelUtil.getSequence(SgConstants.SG_B_PHY_IN_RESULT_ITEM);
            itemRequest.setId(itemId);
            itemRequest.setSgBPhyInResultId(objId);
            itemRequest.setAmtListIn(item.getQtyIn().multiply(item.getPriceList()));
            itemRequest.setOwnerename(user.getEname());
            itemRequest.setModifierename(user.getEname());
            //设置明细用户信息字段
            StorageESUtils.setBModelDefalutData(itemRequest, user);
            list.add(itemRequest);
        });

        SgStoreUtils.batchInsertTeus(list, "入库结果单明细", SgConstants.SG_COMMON_INSERT_PAGE_SIZE, inResultItemMapper);
    }

    /**
     * 入库结果单新增(主表)
     *
     * @param request request-model
     * @param objId   主表id
     * @param user    用戶
     */
    private String insertMainRecord(SgPhyInResultBillSaveRequest request, Long objId, User user) {
        SgBPhyInResult inResult = new SgBPhyInResult();
        BeanUtils.copyProperties(request.getSgBPhyInResultSaveRequest(), inResult);
        inResult.setId(objId);
        inResult.setBillDate(new Date());
        inResult.setBillStatus(SgInConstants.BILL_STATUS_UNCHECKED);
        //获取单据编号
        JSONObject obj = new JSONObject();
        obj.put(SgConstants.SG_B_PHY_IN_RESULT.toUpperCase(), inResult);
        String billNo = SequenceGenUtil.generateSquence(SgInConstants.SEQ_SG_B_PHY_IN_RESULT, obj, user.getLocale(), false);

        inResult.setBillNo(billNo);
        inResult.setInId(user.getId().longValue());
        inResult.setInEcode(SgConstants.DEFAULT_INFO_ZERO);
        inResult.setInEname(user.getEname());
        inResult.setInName(user.getName());
        inResult.setOwnerename(user.getEname());
        inResult.setModifierename(user.getEname());
        StorageESUtils.setBModelDefalutData(inResult, user);
        //开启箱功能 保存只保存录入明细 不合计总数量,审核时合计
        if (!storageBoxConfig.getBoxEnable()) {
            BigDecimal totQtyIn = request.getItemList().stream().map(SgBPhyInResultItemSaveRequest::getQtyIn).reduce(BigDecimal.ZERO, BigDecimal::add);
            BigDecimal totAmtListIn = BigDecimal.ZERO;
            for (SgBPhyInResultItemSaveRequest item : request.getItemList()) {
                totAmtListIn = totAmtListIn.add(item.getQtyIn().multiply(item.getPriceList()));
            }
            inResult.setTotQtyIn(totQtyIn);
            inResult.setTotAmtListIn(totAmtListIn);
        }

        inResultMapper.insert(inResult);
        return billNo;
    }

    /**
     * 入库结果单更新(主表)
     *
     * @param request request-model
     * @param user    用戶
     */
    private Long updateSgPhyInResult(SgPhyInResultBillSaveRequest request, User user) {
        //更新主表
        SgBPhyInResult inResult = new SgBPhyInResult();
        BeanUtils.copyProperties(request.getSgBPhyInResultSaveRequest(), inResult);

        Long objId = request.getSgBPhyInResultSaveRequest().getId();

        if (storageBoxConfig.getBoxEnable()) {
            SgPhyInResultTeusFunctionService service = ApplicationContextHandle.getBean(SgPhyInResultTeusFunctionService.class);
            //录入明细更新
            service.updateInResultImpItem(objId, request.getImpItemList(), user);
            //箱明细更新(审核时解析)
            //service.updateInResultTeusItem(objId, request.getTeusItemList(), user);
        } else {
            if (CollectionUtils.isNotEmpty(request.getItemList())) {
                //条码明细更新
                updateSubRecords(request.getItemList(), objId, user);
                //重新计算总数量、总入库金额
                JSONObject sumObj = inResultItemMapper.selectSumById(objId);
                BigDecimal tot_qty_in = sumObj.getBigDecimal("totqtyin");
                BigDecimal tot_amt_list_in = sumObj.getBigDecimal("totamtlistin");
                inResult.setTotQtyIn(tot_amt_list_in);
                inResult.setTotAmtListIn(tot_qty_in);
            }
        }

        inResult.setModifierename(user.getEname());
        StorageESUtils.setBModelDefalutDataByUpdate(inResult, user);
        inResultMapper.updateById(inResult);
        return objId;
    }


    /**
     * 入库结果明细更新
     *
     * @param itemList 明細request-model
     * @param objId    主表ID
     * @param user     用戶
     */
    private void updateSubRecords(List<SgBPhyInResultItemSaveRequest> itemList, Long objId, User user) {
        //更新入库结果明细
        List<SgBPhyInResultItem> insertList = Lists.newArrayList();
        for (SgBPhyInResultItemSaveRequest itemSaveRequest : itemList) {
            BigDecimal qtyin = Optional.ofNullable(itemSaveRequest.getQtyIn()).orElse(BigDecimal.ZERO);
            if (itemSaveRequest.getId() != null && itemSaveRequest.getId() > 0) {
                inResultItemMapper.updateItemById(qtyin, user, itemSaveRequest.getId());
            } else {
                SgBPhyInResultItem item = new SgBPhyInResultItem();
                BeanUtils.copyProperties(itemSaveRequest, item);
                item.setSgBPhyInResultId(objId);
                BigDecimal priceList = Optional.ofNullable(itemSaveRequest.getPriceList()).orElse(BigDecimal.ZERO);
                item.setAmtListIn(qtyin.multiply(priceList));
                item.setModifierename(user.getEname());
                item.setOwnerename(user.getEname());
                StorageESUtils.setBModelDefalutData(item, user);
                insertList.add(item);
            }
        }
        if (CollectionUtils.isNotEmpty(insertList)) {
            SgStoreUtils.batchInsertTeus(insertList, "入库结果单明细", SgConstants.SG_COMMON_INSERT_PAGE_SIZE, inResultItemMapper);
        }
    }

    /**
     * 校验参数(有待完善)
     *
     * @param request request-model
     */
    private void checkParams(SgPhyInResultBillSaveRequest request) {
        AssertUtils.notNull(request.getLoginUser(), "用户未登录！");
        SgBPhyInResultSaveRequest billRequest = request.getSgBPhyInResultSaveRequest();
        AssertUtils.notNull(request, "请求参数不能为空！");
        AssertUtils.notNull(billRequest, "入库结果单不能为空！");
        AssertUtils.notNull(billRequest.getCpCPhyWarehouseId(), "实体仓ID不能为空!");
        AssertUtils.notNull(billRequest.getCpCPhyWarehouseEcode(), "实体仓CODE不能为空!");
        AssertUtils.notNull(billRequest.getCpCPhyWarehouseEname(), "实体仓名称不能为空!");
        AssertUtils.notNull(billRequest.getSourceBillType(), "来源单据类型不能为空!");
        AssertUtils.notNull(billRequest.getSourceBillId(), "来源单据ID不能为空!");
        //AssertUtils.notNull(billRequest.getSourceBillNo(),"来源单据编号不能为空!");

        if (storageBoxConfig.getBoxEnable()) {
            //校验录入明细
            List<SgPhyInResultImpItemSaveRequest> impItemList = request.getImpItemList();
            AssertUtils.notNull(impItemList, "录入明细不能为空!");
            for (SgPhyInResultImpItemSaveRequest impItem : impItemList) {
                AssertUtils.notNull(impItem.getIsTeus(), "是否为箱标志不能为空!");
                if (impItem.getIsTeus().equals(OcBasicConstants.IS_MATCH_SIZE_Y)) {
                    AssertUtils.notNull(impItem.getPsCTeusId(), "箱id不能为空!");
                    AssertUtils.notNull(impItem.getPsCTeusEcode(), "箱号不能为空!");
                } else {
                    AssertUtils.notNull(impItem.getPsCSkuId(), "条码ID不能为空!");
                    AssertUtils.notNull(impItem.getPsCSkuEcode(), "条码编码不能为空!");
                }
                AssertUtils.notNull(impItem.getPsCProId(), "商品ID不能为空!");
                AssertUtils.notNull(impItem.getPsCProEcode(), "商品编码不能为空!");
                AssertUtils.notNull(impItem.getPsCProEname(), "商品名称不能为空!");
                AssertUtils.notNull(impItem.getPsCSpec1Id(), "规格1ID不能为空!");
                AssertUtils.notNull(impItem.getPsCSpec1Ecode(), "规格1编码不能为空!");
                AssertUtils.notNull(impItem.getPsCSpec1Ename(), "规格1名称不能为空!");
                AssertUtils.notNull(impItem.getQtyIn(), "入库数量不能为空!");
                AssertUtils.notNull(impItem.getPriceList(), "吊牌价不能为空!");
            }
        } else {
            //校验条码明细
            List<SgBPhyInResultItemSaveRequest> itemList = request.getItemList();
            AssertUtils.notNull(itemList, "明细不能为空!");
            for (SgBPhyInResultItemSaveRequest item : itemList) {
                AssertUtils.notNull(item.getPsCSkuId(), "条码ID不能为空!");
                AssertUtils.notNull(item.getPsCSkuEcode(), "条码编码不能为空!");
                AssertUtils.notNull(item.getPsCProId(), "商品ID不能为空!");
                AssertUtils.notNull(item.getPsCProEcode(), "商品编码不能为空!");
                AssertUtils.notNull(item.getPsCProEname(), "商品名称不能为空!");
                AssertUtils.notNull(item.getPsCSpec1Id(), "规格1ID不能为空!");
                AssertUtils.notNull(item.getPsCSpec1Ecode(), "规格1编码不能为空!");
                AssertUtils.notNull(item.getPsCSpec1Ename(), "规格1名称不能为空!");
                AssertUtils.notNull(item.getPsCSpec2Id(), "规格2ID不能为空!");
                AssertUtils.notNull(item.getPsCSpec2Ecode(), "规格2编码不能为空!");
                AssertUtils.notNull(item.getPsCSpec2Ename(), "规格2名称不能为空!");
                AssertUtils.notNull(item.getQtyIn(), "入库数量不能为空!");
                AssertUtils.notNull(item.getPriceList(), "吊牌价不能为空!");
            }
        }
    }
}
