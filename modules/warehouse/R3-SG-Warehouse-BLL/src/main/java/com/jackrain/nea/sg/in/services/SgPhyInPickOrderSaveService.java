package com.jackrain.nea.sg.in.services;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.data.basic.model.request.ProInfoQueryRequest;
import com.jackrain.nea.data.basic.model.request.SkuInfoQueryRequest;
import com.jackrain.nea.data.basic.services.BasicPsQueryService;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.ext.sequence.util.SequenceGenUtil;
import com.jackrain.nea.model.util.ModelUtil;
import com.jackrain.nea.oc.sale.api.OcSaleQueryCmd;
import com.jackrain.nea.oc.sale.model.table.OcBSale;
import com.jackrain.nea.ps.api.result.PsCProSkuResult;
import com.jackrain.nea.ps.api.table.PsCPro;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.basic.utils.StorageUtils;
import com.jackrain.nea.sg.in.common.SgInConstants;
import com.jackrain.nea.sg.in.mapper.*;
import com.jackrain.nea.sg.in.model.request.*;
import com.jackrain.nea.sg.in.model.table.*;
import com.jackrain.nea.sg.out.common.SgOutConstants;
import com.jackrain.nea.sg.out.common.SgOutConstantsIF;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferMapper;
import com.jackrain.nea.sg.transfer.model.table.ScBTransfer;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author zhu lin yu
 * @since 2019-11-18
 * create at : 2019-11-18 19:05
 */

@Slf4j
@Component
public class SgPhyInPickOrderSaveService {


    /**
     * 新增或修改入库拣货单
     */
    public ValueHolderV14<Long> saveSgbBInPickorder(SgPhyInPickOrderSaveRequest request) {
        ValueHolderV14<Long> holderV14 = new ValueHolderV14<>(ResultCode.SUCCESS, "新增入库拣货单主表信息成功");

        //参数检验
        if (request == null) {
            throw new NDSException("request is null");
        }
        log.debug("入库单新增入参,{}", JSONObject.toJSONString(request));
        List<SgPhyInNoticesRequest> inNoticesList = request.getInNoticesList();
        AssertUtils.isTrue(CollectionUtils.isNotEmpty(inNoticesList), " 生成入库拣货单主表入参不能为空");
        AssertUtils.isTrue(request.getLoginUser() != null, " 用户信息不能为空");

        //保存
        Long sgBInPickorderId;
        try {
            SgBInPickorderMapper inPickorderMapper = ApplicationContextHandle.getBean(SgBInPickorderMapper.class);

            SgBInPickorder sgBInPickorder = new SgBInPickorder();
            SgPhyInNoticesRequest saveRequest = inNoticesList.get(0);

            StorageESUtils.setBModelDefalutData(sgBInPickorder, request.getLoginUser());
            sgBInPickorder.setOwnerename(request.getLoginUser().getEname());
            sgBInPickorder.setModifierename(request.getLoginUser().getEname());
            sgBInPickorderId = ModelUtil.getSequence(SgConstants.SG_B_IN_PICKORDER);
            sgBInPickorder.setId(sgBInPickorderId);
            JSONObject obj = new JSONObject();
            obj.put(SgConstants.SG_B_IN_PICKORDER.toUpperCase(), sgBInPickorder);
            String billNo = SequenceGenUtil.generateSquence(SgOutConstants.SQE_SG_B_OUT_PICK, obj, request.getLoginUser().getLocale(), false);
//            String billNo = "测试2";
            sgBInPickorder.setBillNo(billNo);
            sgBInPickorder.setBillStatus(SgOutConstantsIF.PICK_ORDER_STATUS_WAIT);
            sgBInPickorder.setBillDate(new Date());
            sgBInPickorder.setCpCOrigEcode(saveRequest.getCpCCsEcode());
            sgBInPickorder.setCpCOrigEname(saveRequest.getCpCCsEname());
            sgBInPickorder.setCpCDestId(saveRequest.getCpCPhyWarehouseId());
            sgBInPickorder.setCpCDestEcode(saveRequest.getCpCPhyWarehouseEcode());
            sgBInPickorder.setCpCDestEname(saveRequest.getCpCPhyWarehouseEname());
            sgBInPickorder.setIsactive(SgConstants.IS_ACTIVE_Y);
            // 水星新加字段20191218 周琳胜--------------------------------------------------------begin
            List<String> billNoList = inNoticesList.stream().map(SgPhyInNoticesRequest::getSourceBillNo).collect(Collectors.toList());
            // 根据通知单id查出所有明细  计算明细的差异数量之和
            List<Long> noticesIds = inNoticesList.stream().map(SgPhyInNoticesRequest::getId).collect(Collectors.toList());
            SgBPhyInNoticesImpItemMapper impItemMapper = ApplicationContextHandle.getBean(SgBPhyInNoticesImpItemMapper.class);
            List<SgBPhyInNoticesImpItem> sgBPhyInNoticesImpItems = impItemMapper.selectList(new QueryWrapper<SgBPhyInNoticesImpItem>().lambda()
                    .in(SgBPhyInNoticesImpItem::getSgBPhyInNoticesId, noticesIds));
            BigDecimal qtyDiff = sgBPhyInNoticesImpItems.stream().map(SgBPhyInNoticesImpItem::getQtyDiff).reduce(BigDecimal.ZERO, (a, b) -> a.add(b));
            sgBInPickorder.setIsAllRecev(qtyDiff.compareTo(BigDecimal.ZERO) == 0 ? SgConstants.IS_ACTIVE_Y : SgConstants.IS_ACTIVE_N);
            Integer sourceBillType = saveRequest.getSourceBillType();
            if (SgConstantsIF.BILL_TYPE_SALE == sourceBillType) {
                //todo 调用oc 销售单查询接口
                OcSaleQueryCmd ocSaleQueryCmd = (OcSaleQueryCmd)
                        ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                                OcSaleQueryCmd.class.getName(), "oc", "1.0");
                List<OcBSale> saleList = ocSaleQueryCmd.getSaleByCode(billNoList);
                AssertUtils.isTrue(CollectionUtils.isNotEmpty(saleList), "根据原始单据编号未查到对应的销售单！");
                // 箱装数量
                BigDecimal qtyInTeus = saleList.stream()
                        .map(OcBSale::getQtyTeus).reduce(BigDecimal.ZERO, (a, b) -> a.add(b));
                // 标准型数量
                BigDecimal qtyTeusStandard = saleList.stream()
                        .map(OcBSale::getQtyTeusStandard).reduce(BigDecimal.ZERO, (a, b) -> a.add(b));
                // 床垫箱数量
                BigDecimal qtyTeusMattress = saleList.stream()
                        .map(OcBSale::getQtyTeusMattress).reduce(BigDecimal.ZERO, (a, b) -> a.add(b));
                // 床垫加大箱数量
                BigDecimal qtyTeusMattressEnlarge = saleList.stream()
                        .map(OcBSale::getQtyTeusMattressEnlarge).reduce(BigDecimal.ZERO, (a, b) -> a.add(b));
                // 包装数量
                BigDecimal qtyPack = saleList.stream()
                        .map(OcBSale::getQtyPack).reduce(BigDecimal.ZERO, (a, b) -> a.add(b));
                // 加大包数
                BigDecimal qtyEnlarge = saleList.stream()
                        .map(OcBSale::getQtyEnlarge).reduce(BigDecimal.ZERO, (a, b) -> a.add(b));
                // 中包数量
                BigDecimal qtyMedium = saleList.stream()
                        .map(OcBSale::getQtyMiddle).reduce(BigDecimal.ZERO, (a, b) -> a.add(b));
                // 附件数量总和
                BigDecimal qtyAnnex = saleList.stream()
                        .map(OcBSale::getQtyAnnex).reduce(BigDecimal.ZERO, (a, b) -> a.add(b));
                sgBInPickorder.setQtyInTeus(qtyInTeus.add(qtyTeusStandard).add(qtyTeusMattress).add(qtyTeusMattressEnlarge));
                sgBInPickorder.setQtyInPack(qtyPack.add(qtyEnlarge).add(qtyMedium));
                sgBInPickorder.setQtyInUnit(qtyAnnex);
                sgBInPickorder.setCpCCustomerDestId(saleList.get(0).getCpCCustomerId());
                sgBInPickorder.setCpCCustomerDestEcode(saleList.get(0).getCpCCustomerEcode());
                sgBInPickorder.setCpCCustomerDestEname(saleList.get(0).getCpCCustomerEname());
                sgBInPickorder.setCpCCustomerOrigId(saleList.get(0).getCpCCustomerupId());
                sgBInPickorder.setCpCCustomerOrigEcode(saleList.get(0).getCpCCustomerupEcode());
                sgBInPickorder.setCpCCustomerOrigEname(saleList.get(0).getCpCCustomerupEname());
                // 所属法人
                sgBInPickorder.setBelongsLegal(saleList.get(0).getBelongsLegal());
                // 2020-04-15填坑：备货单号为空的不能存null字符串到数据库
                String stockBillNo = jointStockBillNo(saleList, null);
                if (StringUtils.isNotEmpty(stockBillNo)) {
                    sgBInPickorder.setStockBillNo(stockBillNo);
                }
            } else {
                ScBTransferMapper transferMapper = ApplicationContextHandle.getBean(ScBTransferMapper.class);
                List<ScBTransfer> transferList = transferMapper.selectList(new QueryWrapper<ScBTransfer>().lambda().in(ScBTransfer::getBillNo, billNoList));
                AssertUtils.isTrue(CollectionUtils.isNotEmpty(transferList), "根据原始单据编号未查到对应的调拨单！");

                if (log.isDebugEnabled()) {
                    log.debug("入库拣货单查询调拨单:" + JSON.toJSONString(transferList));
                }

                // 箱装数量
                BigDecimal qtyInTeus = transferList.stream()
                        .map(ScBTransfer::getQtyTeusLoad).reduce(BigDecimal.ZERO, BigDecimal::add);
                // 标准型数量
                BigDecimal qtyTeusStandard = transferList.stream()
                        .map(ScBTransfer::getQtyTeusStandard).reduce(BigDecimal.ZERO, BigDecimal::add);
                // 床垫箱数量
                BigDecimal qtyTeusMattress = transferList.stream()
                        .map(ScBTransfer::getQtyTeusMattress).reduce(BigDecimal.ZERO, BigDecimal::add);
                // 床垫加大箱数量
                BigDecimal qtyTeusMattressEnlarge = transferList.stream()
                        .map(ScBTransfer::getQtyTeusMattressEnlarge).reduce(BigDecimal.ZERO, BigDecimal::add);
                // 包装数量
                BigDecimal qtyPack = transferList.stream()
                        .map(ScBTransfer::getQtyPack).reduce(BigDecimal.ZERO, BigDecimal::add);
                // 加大包数
                BigDecimal qtyEnlarge = transferList.stream()
                        .map(ScBTransfer::getQtyEnlarge).reduce(BigDecimal.ZERO, BigDecimal::add);
                // 中包数量
                BigDecimal qtyMedium = transferList.stream()
                        .map(ScBTransfer::getQtyMedium).reduce(BigDecimal.ZERO, BigDecimal::add);
                // 附件数量总和
                BigDecimal qtyAnnex = transferList.stream()
                        .map(ScBTransfer::getQtyAnnex).reduce(BigDecimal.ZERO, BigDecimal::add);

                sgBInPickorder.setQtyInTeus(qtyInTeus.add(qtyTeusStandard).add(qtyTeusMattress).add(qtyTeusMattressEnlarge));
                sgBInPickorder.setQtyInPack(qtyPack.add(qtyEnlarge).add(qtyMedium));
                sgBInPickorder.setQtyInUnit(qtyAnnex);
                sgBInPickorder.setCpCCustomerDestId(transferList.get(0).getCpCCustomerId());
                sgBInPickorder.setCpCCustomerDestEcode(transferList.get(0).getCpCCustomerEcode());
                sgBInPickorder.setCpCCustomerDestEname(transferList.get(0).getCpCCustomerEname());
                if (null != transferList.get(0).getCpCCustomerupId()) {
                    sgBInPickorder.setCpCCustomerOrigId((Long.valueOf(transferList.get(0).getCpCCustomerupId())));
                }

                sgBInPickorder.setCpCCustomerOrigEcode(transferList.get(0).getCpCCustomerupEcode());
                sgBInPickorder.setCpCCustomerOrigEname(transferList.get(0).getCpCCustomerupEname());
                // 所属法人
//                sgBInPickorder.setBelongsLegal(saleList.get(0).getBelongsLegal());

                // 2020-04-15填坑：备货单号为空的不能存null字符串到数据库
                String stockBillNo = jointStockBillNo(null, transferList);
                if (StringUtils.isNotEmpty(stockBillNo)) {
                    sgBInPickorder.setStockBillNo(stockBillNo);
                }
            }

            // 计算入库序号
            sgBInPickorder.setInSeqNo(NumberUtils.toInt(request.getInSeqNo(), 0) + 1 + "");
//            sgBInPickorder.setDelerId(Long.valueOf(request.getLoginUser().getId()));
//            sgBInPickorder.setDelStatus(1);
//            sgBInPickorder.setDelerName(request.getLoginUser().getName());
            sgBInPickorder.setCpCOrigId(saveRequest.getCpCCustomerWarehouseId());
            sgBInPickorder.setTransStatus(1);
            sgBInPickorder.setFailNum(BigDecimal.ZERO);
            // 来源单据编号
            String sourceBillNos = jointSourceBillNo(inNoticesList);
            if (StringUtils.isNotEmpty(sourceBillNos)) {
                sgBInPickorder.setSourceBillNo(sourceBillNos);
            }
            // 水星新加字段20191218 周琳胜--------------------------------------------------------end

            // 2020-04-03添加逻辑：兼容调拨入库单新增
            if (request.getIsTransferOut()) {
                sgBInPickorder.setCpCOrigLegal(request.getBelongsLegal());
                sgBInPickorder.setSourceBillPropId(request.getSourceBillProp());
            }

            // 2020-04-14添加逻辑：补充单据状态
            sgBInPickorder.setStatus(SgInConstants.BILL_STATUS_UNCHECKED);
            sgBInPickorder.setBillStatus(SgInConstants.BILL_STATUS_UNCHECKED);
            inPickorderMapper.insert(sgBInPickorder);
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error(this.getClass().getName() + " 新增入库拣货单主表信息异常:" + StorageUtils.getExceptionMsg(e));
            }

            holderV14.setCode(ResultCode.FAIL);
            holderV14.setMessage("新增入库拣货单主表信息异常:" + StorageUtils.getExceptionMsg(e));
            return holderV14;
//            throw new NDSException("新增入库拣货单主表信息异常:" + StorageUtils.getExceptionMsg(e));
        }

        holderV14.setData(sgBInPickorderId);
        return holderV14;
    }
    // 水星新加方法20191218 周琳胜--------------------------------------------------------begin

    /**
     * 备货单号冗余到主表
     */
    private String jointStockBillNo(List<OcBSale> saleList, List<ScBTransfer> transferList) {
        StringBuilder stringBuilder = new StringBuilder();
        if (CollectionUtils.isNotEmpty(saleList)) {
            for (OcBSale sale : saleList) {
                String stockBillNo = sale.getStockBillNo();
                if (StringUtils.isNotEmpty(stockBillNo)) {
                    stringBuilder.append(stockBillNo + ",");
                }
            }
        } else {
            for (ScBTransfer transfer : transferList) {
                String stockBillNo = transfer.getStockBillNo();
                if (StringUtils.isNotEmpty(stockBillNo)) {
                    stringBuilder.append(stockBillNo + ",");
                }
            }
        }

        String stockBillNos = stringBuilder.toString();
        return StringUtils.isNotEmpty(stockBillNos) ? stockBillNos.substring(0, stockBillNos.length() - 1) : null;
    }

    /**
     * 来源单据编号冗余到主表
     *
     * @param noticesRequests
     * @return
     */
    private String jointSourceBillNo(List<SgPhyInNoticesRequest> noticesRequests) {
        StringBuilder stringBuilder = new StringBuilder();
        for (SgPhyInNoticesRequest noticesRequest : noticesRequests) {
            String stockBillNo = noticesRequest.getSourceBillNo();
            if (StringUtils.isNotEmpty(stockBillNo)) {
                stringBuilder.append(stockBillNo + ",");
            }
        }
        String stockBillNos = stringBuilder.toString();
        return StringUtils.isNotEmpty(stockBillNos) ? stockBillNos.substring(0, stockBillNos.length() - 1) : null;
    }
    // 水星新加字段20191218 周琳胜--------------------------------------------------------end

    /**
     * 新增入库拣货单明细
     */
    public ValueHolderV14 saveSgbBInPickorderItem(SgPhyInPickOrderSaveRequest request) {
        ValueHolderV14 holderV14 = new ValueHolderV14<>(ResultCode.SUCCESS, "新增入库拣货单明细成功");

        //参数检验
        if (request == null) {
            throw new NDSException("request is null");
        }
        List<SgPhyInNoticesImpItemSaveRequest> inNoticesImpItemList = request.getInNoticesImpItemList();
        AssertUtils.isTrue(CollectionUtils.isNotEmpty(inNoticesImpItemList), " 生成入库拣货明细入参不能为空");
        AssertUtils.isTrue(request.getLoginUser() != null, " 用户信息不能为空");
        AssertUtils.isTrue(request.getSgPhyBInPickorderId() != null, " 主表ID不能为空");
        User loginUser = request.getLoginUser();

        try {
            SgBInPickorderItemMapper inPickorderItemMapper = ApplicationContextHandle.getBean(SgBInPickorderItemMapper.class);
            List<SgBInPickorderItem> inPickorderItems = new ArrayList<>();


            //查询图片
            BasicPsQueryService queryService = ApplicationContextHandle.getBean(BasicPsQueryService.class);
            List<Long> proIdList = inNoticesImpItemList.stream().map(SgPhyInNoticesImpItemSaveRequest::getPsCProId).collect(Collectors.toList());
            ProInfoQueryRequest proInfoQueryRequest = new ProInfoQueryRequest();
            proInfoQueryRequest.setProIdList(proIdList);
            List<PsCPro> proInfo = queryService.getProInfo(proInfoQueryRequest);
            AssertUtils.isTrue(CollectionUtils.isNotEmpty(proInfo), " 未查到对应的商品信息");
            Map<Long, List<PsCPro>> collect = proInfo.stream().collect(Collectors.groupingBy(PsCPro::getId));
            log.error(this.getClass().getName() + " 新增入库拣货单明细查询商品信息结果,{}", JSON.toJSONString(collect));

            for (SgPhyInNoticesImpItemSaveRequest sgBPhyInNoticesImpItem : inNoticesImpItemList) {
                SgBInPickorderItem inPickorderItem = new SgBInPickorderItem();
                BeanUtils.copyProperties(sgBPhyInNoticesImpItem, inPickorderItem);
                StorageESUtils.setBModelDefalutData(inPickorderItem, loginUser);
                inPickorderItem.setId(ModelUtil.getSequence(SgConstants.SG_B_IN_PICKORDER_ITEM));
                inPickorderItem.setSgBInPickorderId(request.getSgPhyBInPickorderId());
                inPickorderItem.setQtyDiff(BigDecimal.ZERO);
                inPickorderItem.setModifierename(loginUser.getEname());
                inPickorderItem.setOwnerename(loginUser.getEname());
                inPickorderItem.setQtyNotice(sgBPhyInNoticesImpItem.getQtyDiff());
                List<PsCPro> psCPros = collect.get(sgBPhyInNoticesImpItem.getPsCProId());
                String image = psCPros.get(0).getImage();
                inPickorderItem.setImage(image);
                // 水星新加字段20191218 周琳胜--------------------------------------------------------begin
                inPickorderItem.setQtyIn(sgBPhyInNoticesImpItem.getQtyDiff());
                // 水星新加字段20191218 周琳胜--------------------------------------------------------end
                inPickorderItems.add(inPickorderItem);
            }

            //合并相同的条码或箱的数量
            Map<Long, SgBInPickorderItem> requestTeusMap = Maps.newHashMap();
            Map<Long, SgBInPickorderItem> requestSkuMap = Maps.newHashMap();

            for (SgBInPickorderItem item : inPickorderItems) {

                BigDecimal qtyNotice = item.getQtyNotice();
                if (item.getPsCTeusId() == null) {
                    Long psCSkuId = item.getPsCSkuId();
                    if (requestSkuMap.containsKey(psCSkuId)) {
//                        SgBInPickorderItem orgItem = requestSkuMap.get(psCSkuId);
//                        BigDecimal orgQty = Optional.ofNullable(orgItem.getQtyNotice()).orElse(BigDecimal.ZERO);
//                        BigDecimal qty = Optional.ofNullable(item.getQtyNotice()).orElse(BigDecimal.ZERO);
//                        BigDecimal totalQty = qty.add(orgQty);
//                        orgItem.setQtyNotice(totalQty);
//                        orgItem.setQtyIn(totalQty);

                        this.suppOrderItem(requestSkuMap.get(psCSkuId), qtyNotice);
                    } else {
                        requestSkuMap.put(psCSkuId, item);
                    }
                } else {
                    Long psCTeusId = item.getPsCTeusId();
                    if (requestTeusMap.containsKey(psCTeusId)) {
//                        SgBInPickorderItem orgItem = requestTeusMap.get(psCTeusId);
//                        BigDecimal orgQty = Optional.ofNullable(orgItem.getQtyNotice()).orElse(BigDecimal.ZERO);
//                        BigDecimal qty = Optional.ofNullable(item.getQtyNotice()).orElse(BigDecimal.ZERO);
//                        BigDecimal totalQty = qty.add(orgQty);
//                        orgItem.setQtyNotice(totalQty);
//                        orgItem.setQtyIn(totalQty);

                        this.suppOrderItem(requestTeusMap.get(psCTeusId), qtyNotice);
                    } else {
                        requestTeusMap.put(psCTeusId, item);
                    }
                }

            }

            //合并后的明细
            List<SgBInPickorderItem> mergeList = new ArrayList<>();
            if (MapUtils.isNotEmpty(requestTeusMap)) {
                List<SgBInPickorderItem> requestTeusList = new ArrayList<>(requestTeusMap.values());
                mergeList.addAll(requestTeusList);
            }
            if (MapUtils.isNotEmpty(requestSkuMap)) {
                List<SgBInPickorderItem> requestSkuList = new ArrayList<>(requestSkuMap.values());
                mergeList.addAll(requestSkuList);
            }

            // 2020-04-03添加逻辑：调拨出库单（同级调拨）过来的数据明细
            // 调拨价：对应商品档案上的“出厂价
            // 单位：取对
            if (request.getIsTransferOut()) {
                suppOrderItemInfo(mergeList);
            }

            //分批次新增，批量500
            List<List<SgBInPickorderItem>> partition = Lists.partition(mergeList, SgConstants.SG_COMMON_INSERT_PAGE_SIZE);
            for (List<SgBInPickorderItem> sgBInPickorderItems : partition) {
                int i = inPickorderItemMapper.batchInsert(sgBInPickorderItems);
                if (log.isDebugEnabled()) {
                    log.debug(this.getClass().getName() + ",新增入库拣货单明细结果,{}", i);
                }
            }

        } catch (Exception e) {
            String exceptionMsg = StorageUtils.getExceptionMsg(e);
            if (log.isErrorEnabled()) {
                log.error(this.getClass().getName() + ",新增入库拣货单明细异常,{}", exceptionMsg);
            }
            AssertUtils.logAndThrow("新增入库拣货单明细异常," + exceptionMsg);
        }

        return holderV14;
    }

    /**
     * 计算通知数量和入库数量
     *
     * @param orgItem   原明细
     * @param qtyNotice 通知数量
     */
    private void suppOrderItem(SgBInPickorderItem orgItem, BigDecimal qtyNotice) {
        BigDecimal orgQty = Optional.ofNullable(orgItem.getQtyNotice()).orElse(BigDecimal.ZERO);
        BigDecimal qty = Optional.ofNullable(qtyNotice).orElse(BigDecimal.ZERO);
        BigDecimal totalQty = qty.add(orgQty);
        orgItem.setQtyNotice(totalQty);
        orgItem.setQtyIn(totalQty);
    }

    /**
     * 新增入库拣货单来源入库通知单信息
     */
    public ValueHolderV14 saveSgbBInPickorderNoticeItem(SgPhyInPickOrderSaveRequest request) {
        ValueHolderV14 holderV14 = new ValueHolderV14<>(ResultCode.SUCCESS, "新增入库拣货单来源入库通知单信息成功");

        //参数检验
        if (request == null) {
            throw new NDSException("request is null");
        }
        List<SgPhyInNoticesRequest> inNoticesList = request.getInNoticesList();
        AssertUtils.isTrue(CollectionUtils.isNotEmpty(inNoticesList), " 入库拣货单来源入库通知单信息入参不能为空");
        AssertUtils.isTrue(request.getLoginUser() != null, " 用户信息不能为空");
        AssertUtils.isTrue(request.getSgPhyBInPickorderId() != null, " 主表ID不能为空");

        //保存
        try {
            SgBInPickorderNoticeItemMapper inPickorderNoticeItemMapper = ApplicationContextHandle.getBean(SgBInPickorderNoticeItemMapper.class);
            List<SgBInPickorderNoticeItem> noticeItemList = new ArrayList<>();
// 水星新加字段20191218 周琳胜--------------------------------------------------------begin
            List<String> billNoList = inNoticesList.stream().map(SgPhyInNoticesRequest::getSourceBillNo).collect(Collectors.toList());
            SgPhyInNoticesRequest sgPhyInNoticesRequest = inNoticesList.get(0);
            Map<String, OcBSale> ocBSaleMap = null;
            Map<String, ScBTransfer> scBTransferMap = null;
            if (SgConstantsIF.BILL_TYPE_SALE == sgPhyInNoticesRequest.getSourceBillType()) {
                //todo 调用oc 销售单查询接口
                OcSaleQueryCmd ocSaleQueryCmd = (OcSaleQueryCmd)
                        ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(), OcSaleQueryCmd.class.getName(), "oc", "1.0");
                List<OcBSale> saleList = ocSaleQueryCmd.getSaleByCode(billNoList);
                AssertUtils.isTrue(CollectionUtils.isNotEmpty(saleList), "根据原始单据编号未查到对应的销售单！");
                ocBSaleMap = saleList.stream().collect(Collectors.toMap(OcBSale::getBillNo, o -> o));
            } else {
                ScBTransferMapper transferMapper = ApplicationContextHandle.getBean(ScBTransferMapper.class);
                List<ScBTransfer> transferList = transferMapper.selectList(new QueryWrapper<ScBTransfer>().lambda().in(ScBTransfer::getBillNo, billNoList));
                AssertUtils.isTrue(CollectionUtils.isNotEmpty(transferList), "根据原始单据编号未查到对应的调拨单！");
                scBTransferMap = transferList.stream().collect(Collectors.toMap(ScBTransfer::getBillNo, o -> o));
            }
            // 水星新加字段20191218 周琳胜--------------------------------------------------------end
            for (SgPhyInNoticesRequest inNotices : inNoticesList) {
                SgBInPickorderNoticeItem inPickorderNoticeItem = new SgBInPickorderNoticeItem();
                BeanUtils.copyProperties(inNotices, inPickorderNoticeItem);
                StorageESUtils.setBModelDefalutData(inPickorderNoticeItem, request.getLoginUser());
                inPickorderNoticeItem.setId(ModelUtil.getSequence(SgConstants.SG_B_IN_PICKORDER_NOTICE_ITEM));
                inPickorderNoticeItem.setSgBInPickorderId(request.getSgPhyBInPickorderId());
                inPickorderNoticeItem.setNoticeBillNo(inNotices.getBillNo());
                inPickorderNoticeItem.setCpCPhyWarehouseOrigEname(inNotices.getCpCCsEname());
                inPickorderNoticeItem.setCpCPhyWarehouseOrigEcode(inNotices.getCpCCsEcode());
                // 水星新加字段20191218 周琳胜--------------------------------------------------------begin
                inPickorderNoticeItem.setSourceBillNo(inNotices.getSourceBillNo());
                inPickorderNoticeItem.setSourceBillType(inNotices.getSourceBillType());
                if (SgConstantsIF.BILL_TYPE_SALE == inNotices.getSourceBillType()) {
                    OcBSale ocBSale = ocBSaleMap.get(inNotices.getSourceBillNo());
                    inPickorderNoticeItem.setCpCCustomerDeliveryId(ocBSale.getCpCClientId());
                    inPickorderNoticeItem.setCpCCustomerDestId(ocBSale.getCpCCustomerId());
                    inPickorderNoticeItem.setCpCCustomerOrigId(ocBSale.getCpCCustomerupId());
                    inPickorderNoticeItem.setCpCCustomerOrigEcode(ocBSale.getCpCCustomerupEcode());
                    inPickorderNoticeItem.setCpCCustomerOrigEname(ocBSale.getCpCCustomerupEname());
                    inPickorderNoticeItem.setStockBillNo(ocBSale.getStockBillNo());
                } else {
                    ScBTransfer scBTransfer = scBTransferMap.get(inNotices.getSourceBillNo());
                    inPickorderNoticeItem.setCpCCustomerDeliveryId(scBTransfer.getCpCClientId());
                    inPickorderNoticeItem.setCpCCustomerDestId(scBTransfer.getCpCCustomerId());
                    if (null != scBTransfer.getCpCCustomerupId()) {
                        inPickorderNoticeItem.setCpCCustomerOrigId(Long.valueOf(scBTransfer.getCpCCustomerupId()));
                    }

                    inPickorderNoticeItem.setCpCCustomerOrigEcode(scBTransfer.getCpCCustomerupEcode());
                    inPickorderNoticeItem.setCpCCustomerOrigEname(scBTransfer.getCpCCustomerupEname());
                    inPickorderNoticeItem.setStockBillNo(scBTransfer.getStockBillNo());
                }
                // 水星新加字段20191218 周琳胜--------------------------------------------------------end
                noticeItemList.add(inPickorderNoticeItem);
            }
            inPickorderNoticeItemMapper.batchInsert(noticeItemList);
        } catch (Exception e) {
            log.debug("保存入库拣货单来源入库通知单信息异常" + e);
            log.error(this.getClass().getName() + " 保存入库拣货单来源入库通知单信息异常");
            throw new NDSException("保存入库拣货单来源入库通知单信息异常");
        }
        return holderV14;
    }

    /**
     * 新增入库拣货单来源入库结果单信息
     */
    public ValueHolderV14 saveSgbBInPickorderResultItem(SgPhyInPickOrderSaveRequest request) {
        ValueHolderV14 holderV14 = new ValueHolderV14<>(ResultCode.SUCCESS, "保存成功");

        //参数检验
        if (request == null) {
            throw new NDSException("request is null");
        }
        List<SgBPhyInResultSaveRequest> inResultList = request.getInResultList();
        AssertUtils.isTrue(CollectionUtils.isNotEmpty(inResultList), " 入库拣货单来源出库结果单信息入参不能为空");
        AssertUtils.isTrue(request.getLoginUser() != null, " 用户信息不能为空");
        AssertUtils.isTrue(request.getSgPhyBInPickorderId() != null, " 主表ID不能为空");

        try {
            SgBInPickorderResultItemMapper inPickOrderResultItemMapper = ApplicationContextHandle.getBean(SgBInPickorderResultItemMapper.class);
            List<SgBInPickorderResultItem> resultItems = new ArrayList<>();

            for (SgBPhyInResultSaveRequest outResult : inResultList) {
                SgBInPickorderResultItem inPickOrderResultItem = new SgBInPickorderResultItem();
                BeanUtils.copyProperties(outResult, inPickOrderResultItem);
                StorageESUtils.setBModelDefalutData(inPickOrderResultItem, request.getLoginUser());
                inPickOrderResultItem.setId(ModelUtil.getSequence(SgConstants.SG_B_IN_PICKORDER_RESULT_ITEM));
                inPickOrderResultItem.setSgBInPickorderId(request.getSgPhyBInPickorderId());
                inPickOrderResultItem.setCpCPhyWarehouseOrigEname(outResult.getCpCCsEname());
                inPickOrderResultItem.setCpCPhyWarehouseOrigEcode(outResult.getCpCCsEcode());
                resultItems.add(inPickOrderResultItem);
            }
            inPickOrderResultItemMapper.batchInsert(resultItems);
        } catch (Exception e) {
            log.error(this.getClass().getName() + " 保存入库拣货单来源入库通知单信息异常");
            throw new NDSException("保存入库拣货单来源入库通知单信息异常");
        }
        return holderV14;
    }


    /**
     * 入库拣货单装箱信息
     */
    public ValueHolderV14 saveSgbBInPickorderTeusItem(SgPhyInPickOrderSaveRequest request) {
        ValueHolderV14 holderV14 = new ValueHolderV14<>(ResultCode.SUCCESS, "保存成功");

        //参数检验
        if (request == null) {
            throw new NDSException("request is null");
        }
        if (log.isDebugEnabled()) {
            log.debug("Start saveSgbBInPickorderTeusItem.params:{};"
                    + JSONObject.toJSONString(request));
        }
//        AssertUtils.isTrue(CollectionUtils.isNotEmpty(inPickorderTeusItemList), "入库拣货单箱信息入参不能为空");
        AssertUtils.isTrue(request.getLoginUser() != null, " 用户信息不能为空");
        AssertUtils.isTrue(request.getSgPhyBInPickorderId() != null, " 主表ID不能为空");

        List<SgPhyInPickorderTeusItemSaveRequest> inPickorderTeusItemList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(request.getInPickorderTeusItemList())) {
            inPickorderTeusItemList.addAll(request.getInPickorderTeusItemList());
        }

        //判断出库拣货单是否已经审核
        SgBInPickorderMapper inPickorderMapper = ApplicationContextHandle.getBean(SgBInPickorderMapper.class);
        SgBInPickorder sgBInPickorder = inPickorderMapper.selectById(request.getSgPhyBInPickorderId());
        if (sgBInPickorder == null) {
            throw new NDSException("该入库拣货单已不存在");
        }
        if (sgBInPickorder.getBillStatus() == SgOutConstantsIF.PICK_ORDER_STATUS_AUDIT) {
            AssertUtils.logAndThrow("单据已已审核，不允许保存");
        }
        if (sgBInPickorder.getBillStatus() == SgOutConstantsIF.PICK_ORDER_STATUS_VOID) {
            AssertUtils.logAndThrow("单据已作废，不允许保存");
        }


        SgBInPickorderTeusItemMapper inPickOrderTeusItemMapper = ApplicationContextHandle.getBean(SgBInPickorderTeusItemMapper.class);
        SgBInPickorderItemMapper inPickorderItemMapper = ApplicationContextHandle.getBean(SgBInPickorderItemMapper.class);

        //判断新增的数量是否大于拣货明细数量
        List<SgBInPickorderItem> inPickorderItems = inPickorderItemMapper.selectList(new QueryWrapper<SgBInPickorderItem>().lambda()
                .eq(SgBInPickorderItem::getSgBInPickorderId, request.getSgPhyBInPickorderId()));
        Map<Long, BigDecimal> teusItemQtyMap = new HashMap<>();
        Map<Long, BigDecimal> skuItemQtyMap = new HashMap<>();

        for (SgBInPickorderItem inPickorderItem : inPickorderItems) {
            if (inPickorderItem.getPsCTeusId() != null) {
                teusItemQtyMap.put(inPickorderItem.getPsCTeusId(), inPickorderItem.getQtyNotice());
            } else {
                skuItemQtyMap.put(inPickorderItem.getPsCSkuId(), inPickorderItem.getQtyNotice());
            }
        }

        for (SgPhyInPickorderTeusItemSaveRequest teusItemSaveRequest : inPickorderTeusItemList) {
            Long psCTeusId = teusItemSaveRequest.getPsCTeusId();
            Long psCSkuId = teusItemSaveRequest.getPsCSkuId();
            BigDecimal qtyTeus = teusItemSaveRequest.getQtyTeus();
            if (psCTeusId != null && teusItemQtyMap.containsKey(psCTeusId) && qtyTeus.compareTo(teusItemQtyMap.get(psCTeusId)) > 0) {
                AssertUtils.logAndThrow("装箱明细数量大于拣货明细,箱号为" + teusItemSaveRequest.getPsCTeusEcode());
            } else if (psCSkuId != null && skuItemQtyMap.containsKey(psCSkuId) && qtyTeus.compareTo(skuItemQtyMap.get(psCSkuId)) > 0) {
                AssertUtils.logAndThrow("装箱明细数量大于拣货明细,条码为" + teusItemSaveRequest.getPsCSkuEcode());
            }
        }

        try {
            //删除该拣货单的所有装箱明细
            inPickOrderTeusItemMapper.delete(new UpdateWrapper<SgBInPickorderTeusItem>().lambda()
                    .eq(SgBInPickorderTeusItem::getSgBInPickorderId, request.getSgPhyBInPickorderId()));

            //批量新增装箱明细
            Map<Long, BigDecimal> teusQtyMap = new HashMap<>();
            Map<Long, BigDecimal> skuQtyMap = new HashMap<>();


            List<SgBInPickorderTeusItem> sgBInPickorderTeusItems = new ArrayList<>();
            for (SgPhyInPickorderTeusItemSaveRequest item : inPickorderTeusItemList) {
                SgBInPickorderTeusItem sgBInPickorderTeusItem = new SgBInPickorderTeusItem();
                BeanUtils.copyProperties(item, sgBInPickorderTeusItem);
                StorageESUtils.setBModelDefalutData(sgBInPickorderTeusItem, request.getLoginUser());
                sgBInPickorderTeusItem.setId(ModelUtil.getSequence(SgConstants.SG_B_IN_PICKORDER_TEUS_ITEM));
                sgBInPickorderTeusItem.setSgBInPickorderId(request.getSgPhyBInPickorderId());
                sgBInPickorderTeusItems.add(sgBInPickorderTeusItem);
                Long psCTeusId = item.getPsCTeusId();
                Long psCSkuId = item.getPsCSkuId();

                if (psCTeusId == null) {
                    if (skuQtyMap.containsKey(psCSkuId)) {
                        BigDecimal qty = skuQtyMap.get(psCSkuId);
                        skuQtyMap.put(psCSkuId, qty.add(item.getQtyTeus()));
                    } else {
                        skuQtyMap.put(item.getPsCSkuId(), item.getQtyTeus());
                    }
                } else {
                    if (teusQtyMap.containsKey(psCTeusId)) {
                        BigDecimal qty = skuQtyMap.get(psCTeusId);
                        teusQtyMap.put(psCTeusId, qty.add(item.getQtyTeus()));
                    } else {
                        teusQtyMap.put(psCTeusId, item.getQtyTeus());
                    }
                }
            }
            if (CollectionUtils.isNotEmpty(sgBInPickorderTeusItems)) {
                inPickOrderTeusItemMapper.batchInsert(sgBInPickorderTeusItems);
            }

            if (log.isDebugEnabled()) {
                log.debug("Start saveSgbBInPickorderTeusItem.teusQtyMap:{}," + "skuQtyMap:{}," + "inPickorderItems:{};"
                        + JSONObject.toJSONString(teusQtyMap), JSONObject.toJSONString(skuQtyMap), JSONObject.toJSONString(inPickorderItems));
            }
            for (SgBInPickorderItem inPickorderItem : inPickorderItems) {
                SgBInPickorderItem updateItem = new SgBInPickorderItem();
                updateItem.setId(inPickorderItem.getId());

                if (inPickorderItem.getPsCTeusId() != null && teusQtyMap.containsKey(inPickorderItem.getPsCTeusId())) {
                    updateItem.setQtyIn(teusQtyMap.get(inPickorderItem.getPsCTeusId()));
                    updateItem.setQtyDiff(inPickorderItem.getQtyNotice().subtract(updateItem.getQtyIn()));
                } else if (inPickorderItem.getPsCSkuId() != null && skuQtyMap.containsKey(inPickorderItem.getPsCSkuId())) {
                    updateItem.setQtyIn(skuQtyMap.get(inPickorderItem.getPsCSkuId()));
                    updateItem.setQtyDiff(inPickorderItem.getQtyNotice().subtract(updateItem.getQtyIn()));
                } else {
                    updateItem.setQtyIn(BigDecimal.ZERO);
                    updateItem.setQtyDiff(inPickorderItem.getQtyNotice());
                }
                inPickorderItemMapper.updateById(updateItem);
            }

        } catch (Exception e) {
            log.error(this.getClass().getName() + " 入库拣货单装箱保存失败");
            throw new NDSException("入库拣货单装箱保存失败");
        }
        return holderV14;
    }

    /**
     * 统计调拨时需要补充：
     * 调拨价、单位
     *
     * @param mergeList 拣货明细
     */
    private void suppOrderItemInfo(List<SgBInPickorderItem> mergeList) {

        if (CollectionUtils.isNotEmpty(mergeList)) {
            List<String> skuList = mergeList.stream().map(SgBInPickorderItem::getPsCSkuEcode).collect(Collectors.toList());
            if (CollectionUtils.isNotEmpty(skuList)) {

                if (log.isDebugEnabled()) {
                    log.debug(this.getClass().getName() + ",suppOrderItemInfoParam:" + JSON.toJSONString(skuList));
                }
                BasicPsQueryService psQueryService = ApplicationContextHandle.getBean(BasicPsQueryService.class);
                SkuInfoQueryRequest skuRequest = new SkuInfoQueryRequest();
                skuRequest.setSkuEcodeList(skuList);
                HashMap<String, PsCProSkuResult> skuInfoList = psQueryService.getSkuInfoByEcode(skuRequest);

                // 如果商品查询不为空
                if (MapUtils.isNotEmpty(skuInfoList)) {
                    if (log.isDebugEnabled()) {
                        log.debug(this.getClass().getName() + ",suppOrderItemInfoResult:" + JSON.toJSONString(skuInfoList));
                    }
                    mergeList.forEach(item -> {

                        PsCProSkuResult skuResult = skuInfoList.get(item.getPsCSkuEcode());
                        if (skuResult != null) {
                            // 补充调拨价(取出厂价)和单位
                            item.setPriceOrder(skuResult.getPriceFactory());
                            item.setUnitId(skuResult.getBasicunit());
                            item.setUnitEcode(skuResult.getBasicunitEcode());
                            item.setUnitEname(skuResult.getBasicunitEname());
                        }
                    });
                }
            }
        }
    }
}
