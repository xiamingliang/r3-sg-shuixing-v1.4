package com.jackrain.nea.sg.out.services;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.mapper.CpCPhyWarehouseMapper;
import com.jackrain.nea.sg.basic.model.table.CpCPhyWarehouse;
import com.jackrain.nea.sg.out.common.SgOutConstantsIF;
import com.jackrain.nea.sg.out.mapper.SgBPhyOutNoticesMapper;
import com.jackrain.nea.sg.out.model.request.WorkbenchOutNoticesQueryRequest;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutNotices;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @description: 工作台-出库通知单服务
 * @author: 郑小龙
 * @date: 2019-05-15 13:42
 */
@Slf4j
@Component
public class WorkbenchOutNoticesQueryService {
    @Autowired
    SgBPhyOutNoticesMapper sgBPhyOutNoticesMapper;//出库通知单
    @Autowired
    CpCPhyWarehouseMapper cpCPhyWarehouseMapper;//实体仓库表


    /**
     * @Description 查询 客服工作台-传WMS失败
     * @author 郑小龙
     * @date 2019-05-15 14:30
     */
    public ValueHolderV14 queryPassWmsFailure(WorkbenchOutNoticesQueryRequest queryRequest) {
        ValueHolderV14 v14 = new ValueHolderV14(ResultCode.SUCCESS, "客服工作台-[传wms失败]接口查询成功");
        try {
            List<Long> shopList = new ArrayList<>();
            if (null != queryRequest) {
                shopList = queryRequest.getShopList();
            }
            //来源订单类型：1 零售发货单 ； 传wms状态： 3 传送失败；
            QueryWrapper<SgBPhyOutNotices> wrapper = new QueryWrapper<>();
            wrapper.eq("isactive", SgConstants.IS_ACTIVE_Y)
                    .eq("source_bill_type", SgConstantsIF.BILL_TYPE_RETAIL)
                    .eq("wms_status", SgOutConstantsIF.WMS_STATUS_PASS_FAILED);
            if (CollectionUtils.isNotEmpty(shopList)) {
                wrapper.in("cp_c_shop_id", shopList);
            }
            int count = sgBPhyOutNoticesMapper.selectCount(wrapper);
            v14.setData(count);
        } catch (Exception e) {
            v14.setCode(ResultCode.FAIL);
            v14.setMessage("客服工作台-[传wms失败]接口查询异常：异常信息" + e.getMessage());
            log.error("客服工作台-传WMS失败 queryPassWmsFailure 异常信息！" + e.getMessage());
            return v14;
        }
        return v14;
    }


    /**
     * @Description 查询 客服工作台-仓库超截单时间未发
     * @author 郑小龙
     * @date 2019-05-15 17:35
     */
    public ValueHolderV14 queryWareSuperCutTimeNoHair(WorkbenchOutNoticesQueryRequest queryRequest) {
        ValueHolderV14 v14 = new ValueHolderV14(ResultCode.SUCCESS, "客服工作台-[仓库超截单时间未发]接口查询成功");
        try {
            List<Long> shopList = new ArrayList<>();
            if (null != queryRequest) {
                shopList = queryRequest.getShopList();
            }

            if (CollectionUtils.isEmpty(shopList)) {
                v14.setData(0);
                return v14;
            }

            //保存 返回来源单号id集合
            List<Long> idList = new ArrayList<>();

            Date d = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(d);
            calendar.add(Calendar.DATE, -90);//当前时间减去90天
            String createTime = sdf.format(calendar.getTime());//
            Timestamp time = Timestamp.valueOf(createTime);

            //出库类型：1 电商出库 ；来源订单类型：1 零售发货单 ；
            //传wms状态：3 传送失败； 创建时间：3个月之内的数据
            //店铺数据；传wms时间不为空
            QueryWrapper<SgBPhyOutNotices> wrapper = new QueryWrapper<>();
            wrapper.eq("isactive", SgConstants.IS_ACTIVE_Y)
                    .eq("out_type", SgOutConstantsIF.OUT_TYPE_ELECTRICITY)
                    .eq("source_bill_type", SgConstantsIF.BILL_TYPE_RETAIL)
                    .eq("wms_status", SgOutConstantsIF.WMS_STATUS_PASS_SUCCESS)
                    .ge("creationdate", time)
                    .in("cp_c_shop_id", shopList)
                    .isNotNull("pass_wms_time");

            //获取出库单数据
            List<SgBPhyOutNotices> sgBPhyOutNoticesList = sgBPhyOutNoticesMapper.selectList(wrapper);
            //获取当前订单所有去重的仓库id
            List<Long> phyIdList = sgBPhyOutNoticesList.stream().map(s -> s.getCpCPhyWarehouseId())
                    .distinct().collect(Collectors.toList());
            if (CollectionUtils.isEmpty(phyIdList)) {
                v14.setData(0);
                return v14;
            }

            //循环仓库
            for (Long phyId : phyIdList) {
                String cutTimeStr = "";
                SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
                //查询实体仓库数据
                CpCPhyWarehouse warehouse = cpCPhyWarehouseMapper.selectById(phyId);
                //获取仓库截单时间
                if (queryRequest.getIsJdPlatform()) {
                    if (null != warehouse.getJdCutTime()) {
                        cutTimeStr = format.format(warehouse.getJdCutTime());//京东仓库截单时间
                    }
                } else {
                    if (null != warehouse.getCutTime()) {
                        cutTimeStr = format.format(warehouse.getCutTime());//非京东仓库截单时间
                    }
                }
                if (StringUtils.isEmpty(cutTimeStr)) {
                    continue;
                }

                //循环判断 出库通知单 推送时间 时间部分和仓库截单时间部分 对比
                //仓库截单时间大于出库通知单推送时间
                for (SgBPhyOutNotices outNotices : sgBPhyOutNoticesList) {
                    if (outNotices.getCpCPhyWarehouseId() == phyId) {
                        Date ctTiem = format.parse(cutTimeStr);
                        String passWmsTimeStr = format.format(outNotices.getPassWmsTime());
                        Date passWmsTime = format.parse(passWmsTimeStr);
                        if (ctTiem.after(passWmsTime)) {
                            idList.add(outNotices.getSourceBillId());
                        }
                    }
                }
            }

            v14.setData(idList.size());
        } catch (Exception e) {
            v14.setCode(ResultCode.FAIL);
            v14.setMessage("客服工作台-[仓库超截单时间未发]接口查询异常：异常信息" + e.getMessage());
            log.error("客服工作台-仓库超截单时间未发 queryWareSuperCutTimeNoHair 异常信息！" + e.getMessage());
            return v14;
        }
        return v14;
    }

}
