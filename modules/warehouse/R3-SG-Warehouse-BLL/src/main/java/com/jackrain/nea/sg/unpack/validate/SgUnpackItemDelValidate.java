package com.jackrain.nea.sg.unpack.validate;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.oc.sale.common.OcSaleConstantsIF;
import com.jackrain.nea.sg.unpack.mapper.SgBTeusUnpackItemMapper;
import com.jackrain.nea.sg.unpack.mapper.SgBTeusUnpackMapper;
import com.jackrain.nea.sg.unpack.model.table.SgBTeusUnpack;
import com.jackrain.nea.sg.unpack.model.table.SgBTeusUnpackItem;
import com.jackrain.nea.tableService.MainTableRecord;
import com.jackrain.nea.tableService.TableServiceContext;
import com.jackrain.nea.tableService.validate.BaseValidator;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.utils.AssertUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author zhoulinsheng
 * @since 2019/12/3 11:13
 * desc: 拆箱单明细删除
 */
@Slf4j
@Component
public class SgUnpackItemDelValidate extends BaseValidator {

    @Override
    public void validate(TableServiceContext context, MainTableRecord row) {
        Long objId = row.getId();
        SgBTeusUnpackMapper sgBTeusUnpackMapper = ApplicationContextHandle.getBean(SgBTeusUnpackMapper.class);
        SgBTeusUnpackItemMapper itemMapper = ApplicationContextHandle.getBean(SgBTeusUnpackItemMapper.class);
        SgBTeusUnpack sgBTeusUnpack = sgBTeusUnpackMapper.selectById(objId);
        AssertUtils.notNull(sgBTeusUnpack, "当前记录不存在！", context.getLocale());

        List<SgBTeusUnpackItem> impItems = itemMapper.selectList(new QueryWrapper<SgBTeusUnpackItem>().lambda().eq(SgBTeusUnpackItem::getSgBTeusUnpackId, objId));
        if (CollectionUtils.isEmpty(impItems)) {
            AssertUtils.logAndThrow("当前记录已不存在！！", context.getLocale());
        }
        Integer status = sgBTeusUnpack.getStatus();
        if (status != null) {
            if (status == OcSaleConstantsIF.SEND_OUT_STATUS_AUDIT) {
                AssertUtils.logAndThrow("当前记录已审核，不允许删除明细！", context.getLocale());
            }
        } else {
            AssertUtils.logAndThrow("当前记录单据状态为空！", context.getLocale());
        }
    }
}
