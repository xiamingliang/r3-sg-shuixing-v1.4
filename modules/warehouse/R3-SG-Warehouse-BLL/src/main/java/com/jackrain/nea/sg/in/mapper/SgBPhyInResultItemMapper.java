package com.jackrain.nea.sg.in.mapper;


import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.in.common.SgInConstants;
import com.jackrain.nea.sg.in.model.table.SgBPhyInResultItem;
import com.jackrain.nea.web.face.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.math.BigDecimal;
import java.util.List;

@Mapper
public interface SgBPhyInResultItemMapper extends ExtentionMapper<SgBPhyInResultItem> {

    /**
     * 分页查询入库结果单明细
     */
    @Select("SELECT * FROM " + SgConstants.SG_B_PHY_IN_RESULT_ITEM + " ORDER BY ID LIMIT #{page} OFFSET #{offset}")
    List<SgBPhyInResultItem> selectListByPage(@Param("page") int page, @Param("offset") int offset);

    @Update("UPDATE " + SgConstants.SG_B_PHY_IN_RESULT_ITEM + " SET QTY_IN=#{qtyIn},AMT_LIST_IN=PRICE_LIST*#{qtyIn}" +
            ",modifierid=#{user.id},modifierename=#{user.ename},modifiername=#{user.name},modifieddate=now() WHERE ID=#{id}")
    int updateItemById(@Param("qtyIn") BigDecimal qtyIn, @Param("user") User user, @Param("id") Long id);

    @Select("SELECT SUM(QTY_IN) totQtyIn,SUM(AMT_LIST_IN) totAmtListIn FROM " + SgConstants.SG_B_PHY_IN_RESULT_ITEM + " WHERE " + SgInConstants.SG_B_PHY_IN_RESULT_ID + "=#{id}")
    JSONObject selectSumById(@Param("id") Long id);
}