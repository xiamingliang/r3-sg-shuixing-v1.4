package com.jackrain.nea.sg.out.mapper;

import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutNoticesTeusItem;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SgBPhyOutNoticesTeusItemMapper extends ExtentionMapper<SgBPhyOutNoticesTeusItem> {
}