package com.jackrain.nea.sg.out.utils;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.config.SgStorageESConfig;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.in.common.SgInConstants;
import com.jackrain.nea.sg.in.mapper.SgBPhyInNoticesItemMapper;
import com.jackrain.nea.sg.in.mapper.SgBPhyInNoticesMapper;
import com.jackrain.nea.sg.in.mapper.SgBPhyInResultItemMapper;
import com.jackrain.nea.sg.in.mapper.SgBPhyInResultMapper;
import com.jackrain.nea.sg.in.model.table.SgBPhyInNotices;
import com.jackrain.nea.sg.in.model.table.SgBPhyInNoticesItem;
import com.jackrain.nea.sg.in.model.table.SgBPhyInResult;
import com.jackrain.nea.sg.in.model.table.SgBPhyInResultItem;
import com.jackrain.nea.sg.out.common.SgOutConstants;
import com.jackrain.nea.sg.out.mapper.SgBPhyOutNoticesItemMapper;
import com.jackrain.nea.sg.out.mapper.SgBPhyOutNoticesMapper;
import com.jackrain.nea.sg.out.mapper.SgBPhyOutResultItemMapper;
import com.jackrain.nea.sg.out.mapper.SgBPhyOutResultMapper;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutNotices;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutNoticesItem;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutResult;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutResultItem;
import com.jackrain.nea.sg.phyadjust.common.SgPhyAdjustConstants;
import com.jackrain.nea.sg.phyadjust.mapper.SgBPhyAdjustItemMapper;
import com.jackrain.nea.sg.phyadjust.mapper.SgBPhyAdjustMapper;
import com.jackrain.nea.sg.phyadjust.model.table.SgBPhyAdjust;
import com.jackrain.nea.sg.phyadjust.model.table.SgBPhyAdjustItem;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author: 舒威
 * @since: 2019/10/27
 * create at : 2019/10/27 19:33
 */
@Slf4j
@Component
public class SgWarehouseESUtils {

    @Autowired
    private SgStorageESConfig esConfig;

    public void pushESByOutNotices(Long id, Boolean isNeedItems, Boolean isDeleteItems, JSONArray deleteItemIds,
                                   SgBPhyOutNoticesMapper noticesMapper, SgBPhyOutNoticesItemMapper noticesItemMapper) {
        if (esConfig.isOpenES()) {
            try {
                log.debug("出库通知单" + id + "开始推送ES!");
                SgBPhyOutNotices outNotices = noticesMapper.selectOne(new QueryWrapper<SgBPhyOutNotices>().lambda().eq(SgBPhyOutNotices::getId, id));
                List<SgBPhyOutNoticesItem> outNoticesItems = isNeedItems ? noticesItemMapper.selectList(new QueryWrapper<SgBPhyOutNoticesItem>().lambda()
                        .eq(SgBPhyOutNoticesItem::getSgBPhyOutNoticesId, id)) : null;
                String index = SgConstants.SG_B_PHY_OUT_NOTICES;
                String type = SgConstants.SG_B_PHY_OUT_NOTICES_ITEM;
                String parentKey = SgOutConstants.OUT_NOTICES_PAREN_FIELD;
                StorageESUtils.pushESBModelByUpdate(outNotices, outNoticesItems, id, deleteItemIds, index, index, type, parentKey,
                        SgBPhyOutNotices.class, SgBPhyOutNoticesItem.class, isDeleteItems);
            } catch (Exception e) {
                e.printStackTrace();
                String messageExtra = AssertUtils.getMessageExtra("出库通知单推送ES异常!", e);
                log.error(messageExtra);
            }
        }
    }

    public void pushESByOutResult(Long id, Boolean isNeedItems, Boolean isDeleteItems, JSONArray deleteItemIds,
                                  SgBPhyOutResultMapper resultMapper, SgBPhyOutResultItemMapper resultItemMapper) {
        if (esConfig.isOpenES()) {
            try {
                log.debug("出库结果单" + id + "开始推送ES!");
                SgBPhyOutResult outResult = resultMapper.selectOne(new QueryWrapper<SgBPhyOutResult>().lambda().eq(SgBPhyOutResult::getId, id));
                List<SgBPhyOutResultItem> outResultItems = isNeedItems ? resultItemMapper.selectList(new QueryWrapper<SgBPhyOutResultItem>().lambda()
                        .eq(SgBPhyOutResultItem::getSgBPhyOutResultId, id)) : null;
                String index = SgConstants.SG_B_PHY_OUT_RESULT;
                String type = SgConstants.SG_B_PHY_OUT_RESULT_ITEM;
                String parentKey = SgOutConstants.OUT_RESULT_PAREN_FIELD;
                StorageESUtils.pushESBModelByUpdate(outResult, outResultItems, id, deleteItemIds, index, index, type, parentKey,
                        SgBPhyOutResult.class, SgBPhyOutResultItem.class, isDeleteItems);
            } catch (Exception e) {
                e.printStackTrace();
                String messageExtra = AssertUtils.getMessageExtra("出库结果单推送ES异常!", e);
                log.error(messageExtra);
            }
        }
    }

    public void pushESByInNotices(Long id, Boolean isNeedItems, Boolean isDeleteItems, JSONArray deleteItemIds,
                                  SgBPhyInNoticesMapper noticesMapper, SgBPhyInNoticesItemMapper noticesItemMapper) {
        if (esConfig.isOpenES()) {
            try {
                log.debug("入库通知单" + id + "开始推送ES!");
                SgBPhyInNotices inNotices = noticesMapper.selectOne(new QueryWrapper<SgBPhyInNotices>().lambda().eq(SgBPhyInNotices::getId, id));
                List<SgBPhyInNoticesItem> inNoticesItems = isNeedItems ? noticesItemMapper.selectList(new QueryWrapper<SgBPhyInNoticesItem>().lambda()
                        .eq(SgBPhyInNoticesItem::getSgBPhyInNoticesId, id)) : null;
                String index = SgConstants.SG_B_PHY_IN_NOTICES;
                String type = SgConstants.SG_B_PHY_IN_NOTICES_ITEM;
                String parentKey = SgInConstants.SG_B_PHY_IN_NOTICES_ID;
                StorageESUtils.pushESBModelByUpdate(inNotices, inNoticesItems, id, deleteItemIds, index, index, type, parentKey,
                        SgBPhyInNotices.class, SgBPhyInNoticesItem.class, isDeleteItems);
            } catch (Exception e) {
                e.printStackTrace();
                String messageExtra = AssertUtils.getMessageExtra("入库通知单推送ES异常!", e);
                log.error(messageExtra);
            }
        }
    }

    public void pushESByInResult(Long id, Boolean isNeedItems, Boolean isDeleteItems, JSONArray deleteItemIds,
                                 SgBPhyInResultMapper resultMapper, SgBPhyInResultItemMapper resultItemMapper) {
        if (esConfig.isOpenES()) {
            try {
                log.debug("入库结果单" + id + "开始推送ES!");
                SgBPhyInResult inResult = resultMapper.selectOne(new QueryWrapper<SgBPhyInResult>().lambda().eq(SgBPhyInResult::getId, id));
                List<SgBPhyInResultItem> inResultItems = isNeedItems ? resultItemMapper.selectList(new QueryWrapper<SgBPhyInResultItem>().lambda()
                        .eq(SgBPhyInResultItem::getSgBPhyInResultId, id)) : null;
                String index = SgConstants.SG_B_PHY_IN_RESULT;
                String type = SgConstants.SG_B_PHY_IN_RESULT_ITEM;
                String parentKey = SgInConstants.SG_B_PHY_IN_RESULT_ID;
                StorageESUtils.pushESBModelByUpdate(inResult, inResultItems, id, deleteItemIds, index, index, type, parentKey,
                        SgBPhyInResult.class, SgBPhyInResultItem.class, isDeleteItems);
            } catch (Exception e) {
                e.printStackTrace();
                String messageExtra = AssertUtils.getMessageExtra("入库结果单推送ES异常!", e);
                log.error(messageExtra);
            }
        }
    }

    public void pushESByPhyAdjust(Long id, Boolean isNeedItems, Boolean isDeleteItems, JSONArray deleteItemIds,
                                  SgBPhyAdjustMapper adjustMapper, SgBPhyAdjustItemMapper adjustItemMapper) {
        if (esConfig.isOpenES()) {
            try {
                log.debug("库存调整单" + id + "开始推送ES!");
                SgBPhyAdjust adjust = adjustMapper.selectOne(new QueryWrapper<SgBPhyAdjust>().lambda().eq(SgBPhyAdjust::getId, id));
                List<SgBPhyAdjustItem> adjustItems = isNeedItems ? adjustItemMapper.selectList(new QueryWrapper<SgBPhyAdjustItem>().lambda()
                        .eq(SgBPhyAdjustItem::getSgBPhyAdjustId, id)) : null;
                String index = SgConstants.SG_B_PHY_ADJUST;
                String type = SgConstants.SG_B_PHY_ADJUST_ITEM;
                String parentKey = SgPhyAdjustConstants.SG_B_PHY_ADJUST_ID;
                StorageESUtils.pushESBModelByUpdate(adjust, adjustItems, id, deleteItemIds, index, index, type, parentKey,
                        SgBPhyAdjust.class, SgBPhyAdjustItem.class, isDeleteItems);
            } catch (Exception e) {
                e.printStackTrace();
                String messageExtra = AssertUtils.getMessageExtra("库存调整单推送ES异常!", e);
                log.error(messageExtra);
            }
        }
    }
}
