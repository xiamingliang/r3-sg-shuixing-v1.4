package com.jackrain.nea.sg.out.common;

/**
 * @author leexh
 * @since 2019/4/23 17:22
 */
public interface SgOutConstants {

    /**
     * 主明细关联字段
     */
    String OUT_NOTICES_PAREN_FIELD = "sg_b_phy_out_notices_id"; // 出库通知单ID
    String OUT_RESULT_PAREN_FIELD = "sg_b_phy_out_result_id"; // 出库结果单ID
    String SOURCE_BILL_ITEM_ID = "source_bill_item_id"; // 来源单据明细ID
    String TABLE_ID = "id"; // 主键ID

    /**
     * 单据编号
     */
    String SQE_SG_B_OUT_NOTICES = "SQE_SG_B_OUT_NOTICES";   // 出库通知单-序号生成器
    String SQE_SG_B_OUT_RESULT = "SQE_SG_B_OUT_RESULT";   // 出库结果单-序号生成器
    String SQE_SG_B_OUT_PICK = "SQE_SG_B_OUT_PICK";   // 出库拣货单-序号生成器
    String SQE_SG_B_IN_PICK = "SQE_SG_B_IN_PICK";   // 出库拣货单-序号生成器
    String SQE_SG_B_OUT_PICK_TEUS_ITEM = "SQE_SG_B_OUT_PICK_TEUS_ITEM";   // 出库拣货单装箱明细-序号生成器
    String SQE_SG_B_IN_PICK_TEUS_ITEM = "SQE_SG_B_IN_PICK_TEUS_ITEM";   // 出库拣货单装箱明细-序号生成器

    /**
     * 来源单据信息
     */
    String SOURCE_BILL_ID = "source_bill_id";       // 来源单据ID
    String SOURCE_BILL_NO = "source_bill_no";       // 来源单据编号
    String SOURCE_BILL_TYPE = "source_bill_type";   // 来源单据类型

    /**
     * 是否传WMS
     */
    int IS_PASS_WMS_N = 0;  // 否
    int IS_PASS_WMS_Y = 1;  // 是

    /**
     * 奇门单据类型
     * <p>
     * JYCK=一般交易出库单;
     * HHCK= 换货出库;
     * BFCK=补发出库;
     * PTCK=普通出库单;
     * DBCK=调拨出库;
     * B2BRK=B2B入库;
     * B2BCK=B2B出库;
     * QTCK=其他出库;
     * SCRK=生产入库;
     * LYRK=领用入库;
     * CCRK=残次品入库;
     * CGRK=采购入库;
     * DBRK= 调拨入库;
     * QTRK=其他入库;
     * XTRK= 销退入库;
     * THRK=退货入库;
     * HHRK= 换货入库;
     * CNJG= 仓内加工单;
     * CGTH=采购退货出库单
     * JITCK=唯品会出库
     */
    String QM_BILL_TYPE_GENERAL_OUT = "JYCK";
    String QM_BILL_TYPE_BFCK_OUT = "BFCK";
    String QM_BILL_TYPE_B2B_OUT = "B2BCK";
    String QM_BILL_TYPE_REF_PUR_OUT = "CGTH";
    String QM_BILL_TYPE_TRANSFER_OUT = "DBCK";
    String QM_BILL_TYPE_PTCK_OUT = "PTCK";
    String QM_BILL_TYPE_OTHER_OUT = "QTCK";
    String QM_BILL_TYPE_VIPSHOP_OUT = "JITCK";
    String QM_DEFAULT_PLATFORM_CODE = "OTHER";  // 奇门默认入参
    String QM_DEFAULT_SENDER_INFO = "0";     // 奇门默认发件人信息
    String QM_DEFAULT_SENDER_NAME = "乔丹官方旗舰店";
    String QM_DEFAULT_SENDER_ZIP_CODE = "0";
    String QM_DEFAULT_SENDER_TEL = "18381025631";
    String QM_DEFAULT_SENDER_MOBILE = "18381025631";
    String QM_DEFAULT_SENDER_PROVINCE = "四川省";
    String QM_DEFAULT_SENDER_CITY = "成都市";
    String QM_DEFAULT_SENDER_AREA = "新都区";
    String QM_DEFAULT_SENDER_DETAIL_ADDRESS = "四川省成都市新都区斑竹园镇新竹大道菜鸟物流园1号库";
    String QM_DEFAULT_SENDER_COMPANYNAME = "伯俊电商";

    /**
     * MQ相关vip_out_result_verify_postback  TAG_OUT_VIP_VERIFY_BACK
     */
    String MQ_FORMAT = "yyyyMMddHHmmssSSS";

    // 出库通知单回执tag
    String TAG_OUT_NOTICES_CREATE_RECEIPT = "sg_to_oms_wms_out_creat_receipt";

    // 出库通知单回传(零售发货单) tag
    String TAG_OUT_RETAIL_VERIFY_BACK = "sg_to_oms_out_result_verify_postback";

    // 出库通知单回传(唯品会) tag
    String TAG_OUT_VIP_VERIFY_BACK = "sg_to_vip_out_result_verify_postback";

    // 出库通知单回传(销售单) tag
    String TAG_OUT_SALE_VERIFY_BACK = "sg_to_oc_sale_out_verify_postback";

    // 出库通知单回传(销售退货单) tag
    String TAG_OUT_SALE_REF_VERIFY_BACK = "sg_to_oc_ref_sale_out_verify_postback";

    // 出库通知单回传(采购退货单) tag
    String TAG_OUT_PURCHASE_REF_VERIFY_BACK = "sg_to_oc_ref_purchase_out_verify_postback";

    // 出库通知单回传(调拨单) tag
    String TAG_OUT_TRANSFER_VERIFY_BACK = "sg_to_sg_transfer_out_verify_postback";

    // 出库通知单作废调用 京东快递取消服务 mq
    String TAG_OUT_NOTICES_JD_CANCEL = "sg_to_out_notices_jd_cancel";

    /**
     * 出库结果单一键扣减库存(零售)tag
     */
    String TAG_RETAIL_TO_NOTICES_STOCK_UPDATE = "sg_to_retail_warehouse_stock_change_status";

    String IS_ACTIVE = "isactive";

    /**
     * 系统参数【实体仓是否允许负库存】 是否勾选
     * 系统参数【WMS撤回失败原因】 各个仓库返回的失败原因
     * 系统参数【零售发货单传WMS失败信息】 各个仓库回执返回的失败原因
     * 系统参数【菜鸟customerid】
     */
    String SYSTEM_WAREHOUSE = "warehouse.allow_negative_storage";
    String SYSTEM_CANCELWMS_FAILED_REASON = "warehouse.cancel_wms_failed_reason";
    String SYSTEM_WMSBACK_FAILED_REASON = "warehouse.wms_back_failed_reason";
    String SYSTEM_WAREHOUSE_CUSTOMERID = "warehouse.customerid";
    /**
     * 批量新增
     */
    int INSERT_PAGE_SIZE = 200;


    /**
     * 付款方式 1在线支付 2货到付款
     */
    int PAY_TYPE_ONLINE = 1;  // 否
    int PAY_TYPE_ARRIVE = 2;  // 是

    /**
     * wms回传生成结果单中间表数据状态 0 初始状态  1回传中   2回传成功  3回传失败
     */
    int WMS_TO_PHY_RESULT_STATUS_WAIT = 0;
    int WMS_TO_PHY_RESULT_STATUS_PASSING = 1;
    int WMS_TO_PHY_RESULT_STATUS_SUCCESS = 2;
    int WMS_TO_PHY_RESULT_STATUS_FAILED = 3;
}
