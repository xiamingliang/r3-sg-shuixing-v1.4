package com.jackrain.nea.sg.inv.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * RPC服务调用
 *
 * @author 舒威
 * @since 2019/4/9
 * create at : 2019/4/9 17:50
 */
@Slf4j
@Component
public class SgInvProfitLossRPCService {

    /**
     * 库存调整单服务-TableServiceCmd
     */
   /* ValueHolder adjTableService(String tableName, JSONObject param, User loginUser, String action) {
        Locale locale = loginUser.getLocale();
        try {
            QuerySessionImpl querySession = new QuerySessionImpl(loginUser);
            DefaultWebEvent event = new DefaultWebEvent(action, new HashMap());
            event.put("param", param);
            querySession.setEvent(event);
            Object o = ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                    TableServiceCmd.class.getName(), "sc", "1.0");
            if (null != o) {
                ValueHolder result = ((TableServiceCmd) o).execute(querySession);
                if (null != result) {
                    int code = (int) result.getData().get("code");
                    String message = (String) result.getData().get("message");
                    if (0 == code) {
                        log.debug(tableName+action+"成功！");
                        return result;
                    } else if (-1 == code) {
                        AssertUtils.logAndThrow(message, locale);
                    }
                } else {
                    AssertUtils.logAndThrow("返回结果为空！", locale);
                }
            } else {
                AssertUtils.logAndThrow("未获取到对应的bean", locale);
            }
        } catch (Exception e) {
            e.printStackTrace();
            AssertUtils.logAndThrow(e.getMessage(), locale);
        }
        return null;
    }*/

    /**
     * 库存调整单-验收
     */
   /* ValueHolder acceptStorageAdj(JSONObject param, User loginUser, String action) {
        Locale locale = loginUser.getLocale();
        try {
            QuerySessionImpl querySession = new QuerySessionImpl(loginUser);
            DefaultWebEvent event = new DefaultWebEvent(action, new HashMap());
            event.put("param", param);
            querySession.setEvent(event);
            Object o = ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                    ScBInvAdjAcceptCmd.class.getName(), "sc", "1.0");
            if (null != o) {
                ValueHolder result = ((ScBInvAdjAcceptCmd) o).execute(querySession);
                if (null != result) {
                    int code = (int) result.getData().get("code");
                    String message = (String) result.getData().get("message");
                    if (0 == code) {
                        log.debug("库存调整单验收成功！");
                        return result;
                    } else if (-1 == code) {
                        AssertUtils.logAndThrow(message, locale);
                    }
                } else {
                    AssertUtils.logAndThrow("返回结果为空！", locale);
                }
            } else {
                AssertUtils.logAndThrow("未获取到对应的bean", locale);
            }
        } catch (Exception e) {
            e.printStackTrace();
            AssertUtils.logAndThrow(e.getMessage(), locale);
        }
        return null;
    }*/

}
