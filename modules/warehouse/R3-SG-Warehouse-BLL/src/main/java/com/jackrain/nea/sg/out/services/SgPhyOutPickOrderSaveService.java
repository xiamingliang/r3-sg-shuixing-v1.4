package com.jackrain.nea.sg.out.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.data.basic.model.request.ProInfoQueryRequest;
import com.jackrain.nea.data.basic.services.BasicPsQueryService;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.ext.sequence.util.SequenceGenUtil;
import com.jackrain.nea.model.util.ModelUtil;
import com.jackrain.nea.ps.api.table.PsCPro;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.out.common.SgOutConstants;
import com.jackrain.nea.sg.out.common.SgOutConstantsIF;
import com.jackrain.nea.sg.out.mapper.*;
import com.jackrain.nea.sg.out.model.request.*;
import com.jackrain.nea.sg.out.model.table.*;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author zhu lin yu
 * @since 2019-11-18
 * create at : 2019-11-18 19:05
 */

@Slf4j
@Component
public class SgPhyOutPickOrderSaveService {


    /**
     * 新增或修改出库拣货单
     */
    public ValueHolderV14<Long> saveSgbBoutPickorder(SgPhyOutPickOrderSaveRequest request) {
        ValueHolderV14<Long> holderV14 = new ValueHolderV14<>(ResultCode.SUCCESS, "新增出库拣货单主表信息成功");

        //参数检验
        if (request == null) {
            throw new NDSException("request is null");
        }
        List<SgPhyOutNoticesSaveRequest> outNoticesList = request.getOutNoticesList();
        AssertUtils.isTrue(CollectionUtils.isNotEmpty(outNoticesList), " 生成出库拣货单主表入参不能为空");
        AssertUtils.isTrue(request.getLoginUser() != null, " 用户信息不能为空");

        //保存
        Long sgBOutPickorderId;
        try {
            SgBOutPickorderMapper outPickorderMapper = ApplicationContextHandle.getBean(SgBOutPickorderMapper.class);

            SgBOutPickorder sgBOutPickorder = new SgBOutPickorder();
            SgPhyOutNoticesSaveRequest saveRequest = outNoticesList.get(0);

            StorageESUtils.setBModelDefalutData(sgBOutPickorder, request.getLoginUser());
            sgBOutPickorderId = ModelUtil.getSequence(SgConstants.SG_B_OUT_PICKORDER);
            sgBOutPickorder.setId(sgBOutPickorderId);
            JSONObject obj = new JSONObject();
            obj.put(SgConstants.SG_B_OUT_PICKORDER.toUpperCase(), sgBOutPickorder);
            String billNo = SequenceGenUtil.generateSquence(SgOutConstants.SQE_SG_B_OUT_PICK, obj, request.getLoginUser().getLocale(), false);
//            String billNo = "测试1";
            sgBOutPickorder.setBillNo(billNo);
            sgBOutPickorder.setBillStatus(SgOutConstantsIF.PICK_ORDER_STATUS_WAIT);
            sgBOutPickorder.setBillDate(new Date());
            sgBOutPickorder.setCpCOrigId(saveRequest.getCpCPhyWarehouseId());
            sgBOutPickorder.setCpCOrigEcode(saveRequest.getCpCPhyWarehouseEcode());
            sgBOutPickorder.setCpCOrigEname(saveRequest.getCpCPhyWarehouseEname());
            sgBOutPickorder.setCpCDestEcode(saveRequest.getCpCCsEcode());
            sgBOutPickorder.setCpCDestEname(saveRequest.getCpCCsEname());
            sgBOutPickorder.setIsactive(SgConstants.IS_ACTIVE_Y);
            outPickorderMapper.insert(sgBOutPickorder);
        } catch (RuntimeException e) {
            log.error(this.getClass().getName() + " 新增出库拣货单主表信息异常");
            throw new NDSException("新增出库拣货单主表信息异常" + e.getMessage());
        }

        holderV14.setData(sgBOutPickorderId);
        return holderV14;
    }


    /**
     * 新增出库拣货单明细
     */
    public ValueHolderV14 saveSgbBoutPickorderItem(SgPhyOutPickOrderSaveRequest request) {
        ValueHolderV14 holderV14 = new ValueHolderV14<>(ResultCode.SUCCESS, "新增出库拣货单明细成功");

        //参数检验
        if (request == null) {
            throw new NDSException("request is null");
        }
        List<SgPhyOutNoticesImpItemSaveRequest> outNoticesImpItemList = request.getOutNoticesImpItemList();
        AssertUtils.isTrue(CollectionUtils.isNotEmpty(outNoticesImpItemList), " 生成出库拣货明细入参不能为空");
        AssertUtils.isTrue(request.getLoginUser() != null, " 用户信息不能为空");
        AssertUtils.isTrue(request.getSgPhyBOutPickorderId() != null, " 主表ID不能为空");
        User loginUser = request.getLoginUser();

        try {
            SgBOutPickorderItemMapper outPickorderItemMapper = ApplicationContextHandle.getBean(SgBOutPickorderItemMapper.class);
            List<SgBOutPickorderItem> outPickorderItems = new ArrayList<>();

            //查询图片
            BasicPsQueryService queryService = ApplicationContextHandle.getBean(BasicPsQueryService.class);
            List<Long> proIdList = outNoticesImpItemList.stream().map(SgPhyOutNoticesImpItemSaveRequest::getPsCProId).collect(Collectors.toList());
            ProInfoQueryRequest proInfoQueryRequest = new ProInfoQueryRequest();
            proInfoQueryRequest.setProIdList(proIdList);
            List<PsCPro> proInfo = queryService.getProInfo(proInfoQueryRequest);
            AssertUtils.isTrue(CollectionUtils.isNotEmpty(proInfo), " 未查到对应的商品信息");
            Map<Long, List<PsCPro>> collect = proInfo.stream().collect(Collectors.groupingBy(PsCPro::getId));

            for (SgPhyOutNoticesImpItemSaveRequest sgBPhyOutNoticesImpItem : outNoticesImpItemList) {
                SgBOutPickorderItem outPickorderItem = new SgBOutPickorderItem();
                BeanUtils.copyProperties(sgBPhyOutNoticesImpItem, outPickorderItem);
                StorageESUtils.setBModelDefalutData(outPickorderItem, loginUser);
                outPickorderItem.setId(ModelUtil.getSequence(SgConstants.SG_B_OUT_PICKORDER_ITEM));
                outPickorderItem.setSgBOutPickorderId(request.getSgPhyBOutPickorderId());
                outPickorderItem.setQtyDiff(sgBPhyOutNoticesImpItem.getQtyDiff());
                outPickorderItem.setModifierename(loginUser.getEname());
                outPickorderItem.setOwnerename(loginUser.getEname());
                outPickorderItem.setQtyNotice(sgBPhyOutNoticesImpItem.getQtyDiff());
                outPickorderItem.setQtyOut(BigDecimal.ZERO);
                outPickorderItem.setImage(collect.get(sgBPhyOutNoticesImpItem.getPsCProId()).get(0).getImage());
                outPickorderItems.add(outPickorderItem);
            }

            //合并相同的条码或箱的数量
            Map<Long, SgBOutPickorderItem> requestTeusMap = Maps.newHashMap();
            Map<Long, SgBOutPickorderItem> requestSkuMap = Maps.newHashMap();
            for (SgBOutPickorderItem item : outPickorderItems) {
                if (item.getPsCTeusId() == null) {
                    Long psCSkuId = item.getPsCSkuId();
                    if (requestSkuMap.containsKey(psCSkuId)) {
                        SgBOutPickorderItem orgItem = requestSkuMap.get(psCSkuId);
                        BigDecimal orgQty = Optional.ofNullable(orgItem.getQtyNotice()).orElse(BigDecimal.ZERO);
                        BigDecimal qty = Optional.ofNullable(item.getQtyNotice()).orElse(BigDecimal.ZERO);
                        BigDecimal totalQty = qty.add(orgQty);
                        orgItem.setQtyNotice(totalQty);
                        orgItem.setQtyDiff(totalQty);
                    } else {
                        requestSkuMap.put(psCSkuId, item);
                    }
                } else {
                    Long psCTeusId = item.getPsCTeusId();
                    if (requestTeusMap.containsKey(psCTeusId)) {
                        SgBOutPickorderItem orgItem = requestTeusMap.get(psCTeusId);
                        BigDecimal orgQty = Optional.ofNullable(orgItem.getQtyNotice()).orElse(BigDecimal.ZERO);
                        BigDecimal qty = Optional.ofNullable(item.getQtyNotice()).orElse(BigDecimal.ZERO);
                        BigDecimal totalQty = qty.add(orgQty);
                        orgItem.setQtyNotice(totalQty);
                        orgItem.setQtyDiff(totalQty);
                    } else {
                        requestTeusMap.put(psCTeusId, item);
                    }
                }

            }

            //合并后的明细
            List<SgBOutPickorderItem> mergeList = new ArrayList<>();
            if (MapUtils.isNotEmpty(requestTeusMap)) {
                List<SgBOutPickorderItem> requestTeusList = new ArrayList<>(requestTeusMap.values());
                mergeList.addAll(requestTeusList);
            }
            if (MapUtils.isNotEmpty(requestSkuMap)) {
                List<SgBOutPickorderItem> requestSkuList = new ArrayList<>(requestSkuMap.values());
                mergeList.addAll(requestSkuList);
            }

            //分批次新增，批量500
            List<List<SgBOutPickorderItem>> partition = Lists.partition(mergeList, SgConstants.SG_COMMON_INSERT_PAGE_SIZE);
            for (List<SgBOutPickorderItem> sgBOutPickorderItems : partition) {
                outPickorderItemMapper.batchInsert(sgBOutPickorderItems);

            }

        } catch (Exception e) {
            log.error(this.getClass().getName() + " 新增出库拣货单明细异常");
            throw new NDSException("新增出库拣货单明细异常" + e.getMessage());
        }

        return holderV14;
    }


    /**
     * 新增出库拣货单来源出库通知单信息
     */
    public ValueHolderV14 saveSgbBoutPickorderNoticeItem(SgPhyOutPickOrderSaveRequest request) {
        ValueHolderV14 holderV14 = new ValueHolderV14<>(ResultCode.SUCCESS, "新增出库拣货单来源出库通知单信息成功");

        //参数检验
        if (request == null) {
            throw new NDSException("request is null");
        }
        List<SgPhyOutNoticesSaveRequest> outNoticesList = request.getOutNoticesList();
        AssertUtils.isTrue(CollectionUtils.isNotEmpty(outNoticesList), " 出库拣货单来源出库通知单信息入参不能为空");
        AssertUtils.isTrue(request.getLoginUser() != null, " 用户信息不能为空");
        AssertUtils.isTrue(request.getSgPhyBOutPickorderId() != null, " 主表ID不能为空");

        //保存
        try {
            SgBOutPickorderNoticeItemMapper outPickorderNoticeItemMapper = ApplicationContextHandle.getBean(SgBOutPickorderNoticeItemMapper.class);
            List<SgBOutPickorderNoticeItem> noticeItemList = new ArrayList<>();

            for (SgPhyOutNoticesSaveRequest outNotices : outNoticesList) {
                SgBOutPickorderNoticeItem outPickorderNoticeItem = new SgBOutPickorderNoticeItem();
                BeanUtils.copyProperties(outNotices, outPickorderNoticeItem);
                StorageESUtils.setBModelDefalutData(outPickorderNoticeItem, request.getLoginUser());
                outPickorderNoticeItem.setId(ModelUtil.getSequence(SgConstants.SG_B_OUT_PICKORDER_NOTICE_ITEM));
                outPickorderNoticeItem.setSgBOutPickorderId(request.getSgPhyBOutPickorderId());
                outPickorderNoticeItem.setNoticeBillNo(outNotices.getBillNo());
                outPickorderNoticeItem.setCpCPhyWarehouseDestEname(outNotices.getCpCCsEname());
                outPickorderNoticeItem.setCpCPhyWarehouseDestEcode(outNotices.getCpCCsEcode());
                noticeItemList.add(outPickorderNoticeItem);
            }
            outPickorderNoticeItemMapper.batchInsert(noticeItemList);
        } catch (Exception e) {
            log.error(this.getClass().getName() + " 保存出库拣货单来源出库通知单信息异常");
            throw new NDSException("保存出库拣货单来源出库通知单信息异常" + e.getMessage());
        }
        return holderV14;
    }

    /**
     * 新增出库拣货单来源出库结果单信息
     */
    public ValueHolderV14 saveSgbBoutPickorderResultItem(SgPhyOutPickOrderSaveRequest request) {
        ValueHolderV14 holderV14 = new ValueHolderV14<>(ResultCode.SUCCESS, "保存成功");

        //参数检验
        if (request == null) {
            throw new NDSException("request is null");
        }
        List<SgPhyOutResultSaveRequest> outResultList = request.getOutResultList();
        AssertUtils.isTrue(CollectionUtils.isNotEmpty(outResultList), " 出库拣货单来源出库结果单信息入参不能为空");
        AssertUtils.isTrue(request.getLoginUser() != null, " 用户信息不能为空");
        AssertUtils.isTrue(request.getSgPhyBOutPickorderId() != null, " 主表ID不能为空");

        try {
            SgBOutPickorderResultItemMapper outPickOrderResultItemMapper = ApplicationContextHandle.getBean(SgBOutPickorderResultItemMapper.class);
            List<SgBOutPickorderResultItem> resultItems = new ArrayList<>();

            for (SgPhyOutResultSaveRequest outResult : outResultList) {
                SgBOutPickorderResultItem outPickOrderResultItem = new SgBOutPickorderResultItem();
                BeanUtils.copyProperties(outResult, outPickOrderResultItem);
                StorageESUtils.setBModelDefalutData(outPickOrderResultItem, request.getLoginUser());
                outPickOrderResultItem.setId(ModelUtil.getSequence(SgConstants.SG_B_OUT_PICKORDER_RESULT_ITEM));
                outPickOrderResultItem.setSgBOutPickorderId(request.getSgPhyBOutPickorderId());
                outPickOrderResultItem.setCpCPhyWarehouseDestEname(outResult.getCpCCsEname());
                outPickOrderResultItem.setCpCPhyWarehouseDestEcode(outResult.getCpCCsEcode());
                resultItems.add(outPickOrderResultItem);
            }
            outPickOrderResultItemMapper.batchInsert(resultItems);
        } catch (Exception e) {
            log.error(this.getClass().getName() + " 保存出库拣货单来源出库通知单信息异常");
            throw new NDSException("保存出库拣货单来源出库通知单信息异常" + e.getMessage());
        }
        return holderV14;
    }


    /**
     * 出库拣货单装箱信息
     */
    public ValueHolderV14 saveSgbBoutPickorderTeusItem(SgPhyOutPickOrderSaveRequest request) {
        ValueHolderV14 holderV14 = new ValueHolderV14<>(ResultCode.SUCCESS, "保存成功");

        //参数检验
        if (request == null) {
            throw new NDSException("request is null");
        }

//        AssertUtils.isTrue(CollectionUtils.isNotEmpty(outPickorderTeusItemList), "出库拣货单装箱信息不能为空");
        AssertUtils.isTrue(request.getLoginUser() != null, " 用户信息不能为空");
        AssertUtils.isTrue(request.getSgPhyBOutPickorderId() != null, " 主表ID不能为空");

        List<SgOutPickorderTeusItemSaveRequest> outPickorderTeusItemList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(request.getOutPickorderTeusItemList())) {
            outPickorderTeusItemList.addAll(request.getOutPickorderTeusItemList());
        }

        //判断出库拣货单是否已经审核
        SgBOutPickorderMapper outPickorderMapper = ApplicationContextHandle.getBean(SgBOutPickorderMapper.class);
        SgBOutPickorder sgBOutPickorder = outPickorderMapper.selectById(request.getSgPhyBOutPickorderId());
        if (sgBOutPickorder == null) {
            throw new NDSException("该出库拣货单已不存在");
        }
        if (sgBOutPickorder.getBillStatus() == SgOutConstantsIF.PICK_ORDER_STATUS_AUDIT) {
            AssertUtils.logAndThrow("单据已已审核，不允许保存");
        }
        if (sgBOutPickorder.getBillStatus() == SgOutConstantsIF.PICK_ORDER_STATUS_VOID) {
            AssertUtils.logAndThrow("单据已作废，不允许保存");
        }


        SgBOutPickorderTeusItemMapper outPickOrderTeusItemMapper = ApplicationContextHandle.getBean(SgBOutPickorderTeusItemMapper.class);
        SgBOutPickorderItemMapper outPickorderItemMapper = ApplicationContextHandle.getBean(SgBOutPickorderItemMapper.class);

        //判断新增的数量是否大于拣货明细数量
        List<SgBOutPickorderItem> sgBOutPickorderItems = outPickorderItemMapper.selectList(new QueryWrapper<SgBOutPickorderItem>().lambda()
                .eq(SgBOutPickorderItem::getSgBOutPickorderId, request.getSgPhyBOutPickorderId()));
        Map<Long, BigDecimal> teusItemQtyMap = new HashMap<>();
        Map<Long, BigDecimal> skuItemQtyMap = new HashMap<>();

        for (SgBOutPickorderItem sgBOutPickorderItem : sgBOutPickorderItems) {
            if (sgBOutPickorderItem.getPsCTeusId() != null) {
                teusItemQtyMap.put(sgBOutPickorderItem.getPsCTeusId(), sgBOutPickorderItem.getQtyNotice());
            } else {
                skuItemQtyMap.put(sgBOutPickorderItem.getPsCSkuId(), sgBOutPickorderItem.getQtyNotice());
            }
        }

        for (SgOutPickorderTeusItemSaveRequest teusItemSaveRequest : outPickorderTeusItemList) {
            Long psCTeusId = teusItemSaveRequest.getPsCTeusId();
            Long psCSkuId = teusItemSaveRequest.getPsCSkuId();
            BigDecimal qtyTeus = teusItemSaveRequest.getQtyTeus();
            if (psCTeusId != null && teusItemQtyMap.containsKey(psCTeusId) && qtyTeus.compareTo(teusItemQtyMap.get(psCTeusId)) > 0) {
                AssertUtils.logAndThrow("装箱明细数量大于拣货明细,箱号为" + teusItemSaveRequest.getPsCTeusEcode());
            } else if (psCSkuId != null && skuItemQtyMap.containsKey(psCSkuId) && qtyTeus.compareTo(skuItemQtyMap.get(psCSkuId)) > 0) {
                AssertUtils.logAndThrow("装箱明细数量大于拣货明细,条码为" + teusItemSaveRequest.getPsCSkuEcode());
            }
        }

        try {
            //删除该拣货单的所有装箱明细
            outPickOrderTeusItemMapper.delete(new UpdateWrapper<SgBOutPickorderTeusItem>().lambda()
                    .eq(SgBOutPickorderTeusItem::getSgBOutPickorderId, request.getSgPhyBOutPickorderId()));

            //批量新增装箱明细
            Map<Long, BigDecimal> teusQtyMap = new HashMap<>();
            Map<Long, BigDecimal> skuQtyMap = new HashMap<>();


            List<SgBOutPickorderTeusItem> sgBOutPickorderTeusItems = new ArrayList<>();
            for (SgOutPickorderTeusItemSaveRequest item : outPickorderTeusItemList) {
                SgBOutPickorderTeusItem sgBOutPickorderTeusItem = new SgBOutPickorderTeusItem();
                BeanUtils.copyProperties(item, sgBOutPickorderTeusItem);
                StorageESUtils.setBModelDefalutData(sgBOutPickorderTeusItem, request.getLoginUser());
                sgBOutPickorderTeusItem.setId(ModelUtil.getSequence(SgConstants.SG_B_OUT_PICKORDER_TEUS_ITEM));
                sgBOutPickorderTeusItem.setSgBOutPickorderId(request.getSgPhyBOutPickorderId());
                sgBOutPickorderTeusItems.add(sgBOutPickorderTeusItem);
                Long psCTeusId = item.getPsCTeusId();
                Long psCSkuId = item.getPsCSkuId();

                if (psCTeusId == null) {
                    if (skuQtyMap.containsKey(psCSkuId)) {
                        BigDecimal qty = skuQtyMap.get(psCSkuId);
                        skuQtyMap.put(psCSkuId, qty.add(item.getQtyTeus()));
                    } else {
                        skuQtyMap.put(item.getPsCSkuId(), item.getQtyTeus());
                    }
                } else {
                    if (skuQtyMap.containsKey(psCTeusId)) {
                        BigDecimal qty = skuQtyMap.get(psCTeusId);
                        skuQtyMap.put(psCTeusId, qty.add(item.getQtyTeus()));
                    } else {
                        teusQtyMap.put(psCTeusId, item.getQtyTeus());
                    }
                }
            }
            if (CollectionUtils.isNotEmpty(sgBOutPickorderTeusItems)) {
                outPickOrderTeusItemMapper.batchInsert(sgBOutPickorderTeusItems);
            }


            for (SgBOutPickorderItem outPickorderItem : sgBOutPickorderItems) {
                SgBOutPickorderItem updateItem = new SgBOutPickorderItem();
                updateItem.setId(outPickorderItem.getId());

                if (outPickorderItem.getPsCTeusId() != null && teusQtyMap.containsKey(outPickorderItem.getPsCTeusId())) {
                    updateItem.setQtyOut(teusQtyMap.get(outPickorderItem.getPsCTeusId()));
                    updateItem.setQtyDiff(outPickorderItem.getQtyNotice().subtract(updateItem.getQtyOut()));
                } else if (outPickorderItem.getPsCSkuId() != null && skuQtyMap.containsKey(outPickorderItem.getPsCSkuId())) {
                    updateItem.setQtyOut(skuQtyMap.get(outPickorderItem.getPsCSkuId()));
                    updateItem.setQtyDiff(outPickorderItem.getQtyNotice().subtract(updateItem.getQtyOut()));
                } else {
                    updateItem.setQtyOut(BigDecimal.ZERO);
                    updateItem.setQtyDiff(outPickorderItem.getQtyNotice());
                }
                outPickorderItemMapper.updateById(updateItem);
            }

        } catch (Exception e) {
            log.error(this.getClass().getName() + " 出库拣货单装箱保存失败");
            throw new NDSException("出库拣货单装箱保存失败" + e.getMessage());
        }
        return holderV14;
    }
}
