package com.jackrain.nea.sg.out.mapper;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.out.common.SgOutConstants;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutNoticesItem;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface SgBPhyOutNoticesItemMapper extends ExtentionMapper<SgBPhyOutNoticesItem> {

    @Select("SELECT\n" +
            "\tSUM (qty) AS qty,\n" +
            "\tSUM (qty_out) AS qty_out,\n" +
            "\tSUM (qty_diff) AS qty_diff,\n" +
            "\tSUM (amt_list) AS amt_list,\n" +
            "\tSUM (amt_cost) AS amt_cost,\n" +
            "SUM (amt_list_out) AS amt_list_out,\n" +
            "SUM (amt_cost_out) AS amt_cost_out,\n" +
            "SUM (amt_list_diff) AS amt_list_diff,\n" +
            "SUM (amt_cost_diff) AS amt_cost_diff\n" +
            "FROM\n" +
            "\tsg_b_phy_out_notices_item\n" +
            "WHERE\n" +
            "\tsg_b_phy_out_notices_id = #{objId}")
    SgBPhyOutNoticesItem selectSumAmt(@Param("objId") Long objId);


    /**
     * 分页查询出库通知单明细
     */
    @Select("SELECT * FROM " + SgConstants.SG_B_PHY_OUT_NOTICES_ITEM + " ORDER BY ID LIMIT #{page} OFFSET #{offset}")
    List<SgBPhyOutNoticesItem> selectListByPage(@Param("page") int page, @Param("offset") int offset);

    /**
     * 按条码id分组查询通知单明细通知数量与出库数量
     */
    @Select("SELECT DISTINCT ps_c_sku_id,ps_c_sku_ecode,SUM(qty) TOT_QTY,SUM(qty_out) TOT_QTY_OUT FROM " + SgConstants.SG_B_PHY_OUT_NOTICES_ITEM
            + " WHERE " + SgOutConstants.OUT_NOTICES_PAREN_FIELD + "=#{objid} GROUP BY ps_c_sku_id,ps_c_sku_ecode\n")
    List<JSONObject> selectSumGroupBySku(@Param("objid") Long objid);

    @Select("SELECT * from sg_b_phy_out_notices_item WHERE SG_B_PHY_OUT_NOTICES_ID in " +
            "(SELECT id FROM sg_b_phy_out_notices WHERE SOURCE_BILL_ID=#{sourceBillId} and SOURCE_BILL_TYPE=#{sourceBillType});")
    List<SgBPhyOutNoticesItem> selectListBySourceBill(@Param("sourceBillId") Long sourceBillId, @Param("sourceBillType") Integer sourceBillType);
}