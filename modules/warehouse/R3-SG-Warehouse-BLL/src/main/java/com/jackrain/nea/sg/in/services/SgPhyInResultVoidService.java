package com.jackrain.nea.sg.in.services;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.redis.config.CusRedisTemplate;
import com.jackrain.nea.redis.util.RedisOpsUtil;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.in.common.SgInConstants;
import com.jackrain.nea.sg.in.mapper.SgBPhyInResultMapper;
import com.jackrain.nea.sg.in.model.table.SgBPhyInResult;
import com.jackrain.nea.sg.out.utils.SgWarehouseESUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.concurrent.TimeUnit;

@Slf4j
@Component
public class SgPhyInResultVoidService {

    @Autowired
    private SgWarehouseESUtils warehouseESUtils;

    /**
     * 入库结果单作废
     *
     * @param ids
     * @return
     */
    public ValueHolderV14 voidSgPhyInResult(JSONArray ids, User user) {
        if (log.isDebugEnabled()) {
            log.debug("Start SgPhyInResultVoidService.voidSgPhyInResult. ReceiveParams:ids="
                    + JSONObject.toJSONString(ids) + ";");
        }
        SgBPhyInResultMapper mapper = ApplicationContextHandle.getBean(SgBPhyInResultMapper.class);
        ValueHolderV14 result = new ValueHolderV14();
        int fail = 0;
        int total = ids.size();
        for (int i = 0; i < ids.size(); i++) {
            Long id = ids.getLongValue(i);
            SgBPhyInResult billResult = mapper.selectById(id);
            CusRedisTemplate<String, String> redisTemplate = RedisOpsUtil.getStrRedisTemplate();
            Long sourceBillId = billResult.getSourceBillId();
            Integer sourceBillType = billResult.getSourceBillType();
            String lockKsy = SgConstants.SG_B_PHY_IN_RESULT + ":" + sourceBillId + ":" + sourceBillType;
            if (redisTemplate.opsForValue().get(lockKsy) != null) {
                fail++;
                continue;
            } else {
                try {
                    Integer status = billResult.getBillStatus();
                    //校验:如果是已审核或者已作废，跳出当前循环，失败条数+1
                    if (status == SgInConstants.BILL_STATUS_CHECKED || status == SgInConstants.BILL_STATUS_VOID) {
                        fail++;
                        continue;
                    } else {
                        redisTemplate.opsForValue().set(lockKsy, "OK", 30, TimeUnit.MINUTES);
                        //设置状态为已作废
                        billResult.setBillStatus(SgInConstants.BILL_STATUS_VOID);
                        billResult.setIsactive(SgConstants.IS_ACTIVE_N);
                        //添加作废人相关信息
                        billResult.setDelerId(user.getId().longValue());
                        billResult.setDelerName(user.getName());
                        billResult.setDelerEname(user.getEname());
                        billResult.setDelTime(new Date());
                        mapper.updateById(billResult);
                        //推送es
                        warehouseESUtils.pushESByInResult(billResult.getId(), false, false, null, mapper, null);
                    }
                } catch (Exception e) {
                    log.error(this.getClass().getName() + ",error" + e.toString());
                    continue;
                } finally {
                    try {
                        redisTemplate.delete(lockKsy);
                    } catch (Exception e) {
                        log.error(this.getClass().getName() + "redis锁释放失败，请联系管理员！lockKsy：" + lockKsy);
                    }
                }
            }
        }
        //获取入库结果单
        result.setCode(ResultCode.SUCCESS);
        result.setMessage("作废成功数:" + (total - fail) + ",作废失败数:" + fail);
        result.setData(ids);
        return result;

    }
}
