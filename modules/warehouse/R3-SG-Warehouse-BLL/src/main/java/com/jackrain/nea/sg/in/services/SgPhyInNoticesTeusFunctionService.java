package com.jackrain.nea.sg.in.services;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.jackrain.nea.core.db.Tools;
import com.jackrain.nea.model.util.ModelUtil;
import com.jackrain.nea.oc.basic.common.OcBasicConstants;
import com.jackrain.nea.oc.basic.model.request.QueryBoxRequest;
import com.jackrain.nea.oc.basic.services.QueryBoxItemsService;
import com.jackrain.nea.psext.model.table.PsCTeusItem;
import com.jackrain.nea.redis.config.CusRedisTemplate;
import com.jackrain.nea.redis.util.RedisOpsUtil;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.in.mapper.SgBPhyInNoticesImpItemMapper;
import com.jackrain.nea.sg.in.mapper.SgBPhyInNoticesTeusItemMapper;
import com.jackrain.nea.sg.in.mapper.SgBPhyInResultItemMapper;
import com.jackrain.nea.sg.in.mapper.SgBPhyInResultTeusItemMapper;
import com.jackrain.nea.sg.in.model.request.SgPhyInNoticesBillSaveRequest;
import com.jackrain.nea.sg.in.model.request.SgPhyInNoticesImpItemSaveRequest;
import com.jackrain.nea.sg.in.model.request.SgPhyInNoticesItemRequest;
import com.jackrain.nea.sg.in.model.request.SgPhyInNoticesTeusItemSaveRequest;
import com.jackrain.nea.sg.in.model.table.*;
import com.jackrain.nea.sg.send.common.SgSendConstantsIF;
import com.jackrain.nea.sg.send.utils.SgStoreUtils;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author zhoulinsheng
 * @since 2019-10-22
 * create at : 2019-10-22 15:07
 */
@Slf4j
@Component
public class SgPhyInNoticesTeusFunctionService {

    /**
     * 入库通知单-录入明细转换成条码明细/箱内明细
     *
     * @param request 录入明细
     */
    public void impRequestConvertMethod(SgPhyInNoticesBillSaveRequest request) {

        if (CollectionUtils.isEmpty(request.getImpItemList()) && StringUtils.isEmpty(request.getRedisKey())) {
            AssertUtils.logAndThrow("录入明细不能为空!");
        } else if (CollectionUtils.isEmpty(request.getImpItemList()) && StringUtils.isNotEmpty(request.getRedisKey())) {
            //从redis中取出大数据量参数,并放入request
            log.debug("redisKey:" + request.getRedisKey());
            CusRedisTemplate<String, String> redisTemplate = RedisOpsUtil.getStrRedisTemplate();
            String result = redisTemplate.opsForValue().get(request.getRedisKey());
            SgPhyInNoticesBillSaveRequest saveRequest = JSONArray.parseObject(result, SgPhyInNoticesBillSaveRequest.class);
            if (saveRequest == null || CollectionUtils.isEmpty(saveRequest.getImpItemList())) {
                AssertUtils.logAndThrow("[" + request.getRedisKey() + "]录入明细不能为空!");
            } else {
                request.setImpItemList(saveRequest.getImpItemList());
                request.setTeusItemList(saveRequest.getTeusItemList());
            }
        }

        HashMap<Long, SgPhyInNoticesImpItemSaveRequest> impItemSaveRequestHashMap = Maps.newHashMap();
        List<SgPhyInNoticesItemRequest> itemList = Lists.newArrayList();

        //录入明细不为空
        for (SgPhyInNoticesImpItemSaveRequest impItem : request.getImpItemList()) {
            if (impItem.getIsTeus().equals(OcBasicConstants.IS_MATCH_SIZE_N)) {
                //散件条码转条码明细
                SgPhyInNoticesItemRequest itemSaveRequest = new SgPhyInNoticesItemRequest();
                BeanUtils.copyProperties(impItem, itemSaveRequest);
                itemSaveRequest.setQtyIn(impItem.getQtyIn());
                itemList.add(itemSaveRequest);
            } else {
                impItemSaveRequestHashMap.put(impItem.getPsCTeusId(), impItem);
            }
        }

        //箱明细不为空
        if (CollectionUtils.isNotEmpty(request.getTeusItemList())) {
            //箱内明细转条码明细
            for (SgPhyInNoticesTeusItemSaveRequest teusItemSaveRequest : request.getTeusItemList()) {
                SgPhyInNoticesItemRequest itemSaveRequest = new SgPhyInNoticesItemRequest();
                SgPhyInNoticesImpItemSaveRequest impItemSaveRequest = impItemSaveRequestHashMap.get(teusItemSaveRequest.getPsCTeusId());
                BeanUtils.copyProperties(teusItemSaveRequest, itemSaveRequest);
                itemSaveRequest.setSgBPhyInNoticesId(teusItemSaveRequest.getSgBPhyInNoticesId());
                itemSaveRequest.setQtyIn(teusItemSaveRequest.getQty());
                itemSaveRequest.setPriceList(impItemSaveRequest.getPriceList());
                itemSaveRequest.setSourceBillItemId(impItemSaveRequest.getSourceBillItemId());
                itemList.add(itemSaveRequest);
            }
        }

        request.setItemList(itemList);
    }


    /**
     * 入库通知单录入明细新增
     *
     * @param objId       通知单id
     * @param impItemList 录入明细
     * @param loginUser   用户
     */
    public void insertInNoticesImpItem(Long objId, List<SgPhyInNoticesImpItemSaveRequest> impItemList, User loginUser) {
        if (CollectionUtils.isNotEmpty(impItemList)) {
            SgBPhyInNoticesImpItemMapper noticesImpItemMapper = ApplicationContextHandle.getBean(SgBPhyInNoticesImpItemMapper.class);
            //2019-08-12 通知单保存时合并相同sku的明细
            Map<Long, SgPhyInNoticesImpItemSaveRequest> skuRequestMap = Maps.newHashMap();
            Map<Long, SgPhyInNoticesImpItemSaveRequest> teusRequestMap = Maps.newHashMap();
            for (SgPhyInNoticesImpItemSaveRequest noticesImpItemRequest : impItemList) {

                if (noticesImpItemRequest.getPsCTeusId() == null) {
                    //散件合并相同sku的明细
                    Long psCSkuId = noticesImpItemRequest.getPsCSkuId();
                    if (skuRequestMap.containsKey(psCSkuId)) {
                        SgPhyInNoticesImpItemSaveRequest orgInNoticesImpItemSaveRequest = skuRequestMap.get(psCSkuId);
                        BigDecimal orgQty = Optional.ofNullable(orgInNoticesImpItemSaveRequest.getQty()).orElse(BigDecimal.ZERO);
                        BigDecimal qty = Optional.ofNullable(noticesImpItemRequest.getQty()).orElse(BigDecimal.ZERO);
                        orgInNoticesImpItemSaveRequest.setQty(qty.add(orgQty));
                    } else {
                        skuRequestMap.put(psCSkuId, noticesImpItemRequest);
                    }

                } else {
                    //合并相同箱
                    Long psCTeusId = noticesImpItemRequest.getPsCTeusId();
                    if (teusRequestMap.containsKey(psCTeusId)) {
                        SgPhyInNoticesImpItemSaveRequest orgInNoticesImpItemSaveRequest = teusRequestMap.get(psCTeusId);
                        BigDecimal orgQty = Optional.ofNullable(orgInNoticesImpItemSaveRequest.getQty()).orElse(BigDecimal.ZERO);
                        BigDecimal qty = Optional.ofNullable(noticesImpItemRequest.getQty()).orElse(BigDecimal.ZERO);
                        orgInNoticesImpItemSaveRequest.setQty(qty.add(orgQty));
                    } else {
                        teusRequestMap.put(psCTeusId, noticesImpItemRequest);
                    }
                }
            }
            //合并后的明细
            List<SgPhyInNoticesImpItemSaveRequest> allImpItemList = new ArrayList<>();
            allImpItemList.addAll(skuRequestMap.values());
            allImpItemList.addAll(teusRequestMap.values());

            List<SgBPhyInNoticesImpItem> impItems = new ArrayList<>();
            allImpItemList.forEach(item -> {
                BigDecimal qty = item.getQty();          // 通知数量
                SgBPhyInNoticesImpItem noticesImpItem = new SgBPhyInNoticesImpItem();
                Long id = Tools.getSequence(SgConstants.SG_B_PHY_IN_NOTICES_IMP_ITEM);
                BeanUtils.copyProperties(item, noticesImpItem);
                noticesImpItem.setId(id);
                noticesImpItem.setSgBPhyInNoticesId(objId);
                noticesImpItem.setQtyDiff(qty);        // 差异数量（通知数量-入库数量）
                noticesImpItem.setQtyIn(BigDecimal.ZERO);        // 入库数量
                noticesImpItem.setOwnerename(loginUser.getEname());
                noticesImpItem.setModifierename(loginUser.getEname());
                StorageESUtils.setBModelDefalutData(noticesImpItem, loginUser);
                impItems.add(noticesImpItem);
            });

            //分批次，批量新增
            SgStoreUtils.batchInsertTeus(impItems, "入库通知单录入明细", SgConstants.SG_COMMON_MAX_INSERT_PAGE_SIZE, noticesImpItemMapper);
        }
    }

    /**
     * 入库通知单箱明细新增
     *
     * @param objId        通知单id
     * @param teusItemList 录入明细
     * @param loginUser    用户
     */
    public void insertInNoticesTeusItem(Long objId, List<SgPhyInNoticesTeusItemSaveRequest> teusItemList, User loginUser) {
        if (CollectionUtils.isNotEmpty(teusItemList)) {
            SgBPhyInNoticesTeusItemMapper noticesTeusItemMapper = ApplicationContextHandle.getBean(SgBPhyInNoticesTeusItemMapper.class);
            List<SgBPhyInNoticesTeusItem> teusItems = new ArrayList<>();

            //封装入库通知单箱明细其他参数
            for (SgPhyInNoticesTeusItemSaveRequest teusItemSaveRequest : teusItemList) {
                SgBPhyInNoticesTeusItem teusItem = new SgBPhyInNoticesTeusItem();
                Long id = Tools.getSequence(SgConstants.SG_B_PHY_IN_NOTICES_TEUS_ITEM);
                BeanUtils.copyProperties(teusItemSaveRequest, teusItem);
                teusItem.setId(id);
                teusItem.setSgBPhyInNoticesId(objId);
                teusItem.setOwnerename(loginUser.getEname());
                teusItem.setModifierename(loginUser.getEname());
                StorageESUtils.setBModelDefalutData(teusItem, loginUser);
                teusItems.add(teusItem);
            }

            //分批次，批量新增
            if (CollectionUtils.isNotEmpty(teusItems)) {
                SgStoreUtils.batchInsertTeus(teusItems, "入库通知单箱明细", SgConstants.SG_COMMON_MAX_INSERT_PAGE_SIZE, noticesTeusItemMapper);
            }
        }
    }

    /**
     * 入库结果单审核修改入库通知单的录入明细
     *
     * @param user
     * @param noticesId
     * @param resultImpItems
     */
    public void updagteSgPhyInNoticesByBox(User user, Long noticesId, List<SgBPhyInResultImpItem> resultImpItems) {
        //分离散码与箱
        List<Long> teusIds = resultImpItems.stream()
                .filter(o -> o.getIsTeus().equals(OcBasicConstants.IS_MATCH_SIZE_Y) && o.getPsCTeusId() != null)
                .map(SgBPhyInResultImpItem::getPsCTeusId).collect(Collectors.toList());
        List<Long> skuIds = resultImpItems.stream()
                .filter(o -> o.getIsTeus().equals(OcBasicConstants.IS_MATCH_SIZE_N))
                .map(SgBPhyInResultImpItem::getPsCSkuId).collect(Collectors.toList());

        SgBPhyInNoticesImpItemMapper noticesImpItemMapper = ApplicationContextHandle.getBean(SgBPhyInNoticesImpItemMapper.class);
        List<SgBPhyInNoticesImpItem> noticesImpItems = noticesImpItemMapper.selectList(new QueryWrapper<SgBPhyInNoticesImpItem>().lambda()
                .eq(SgBPhyInNoticesImpItem::getSgBPhyInNoticesId, noticesId)
                .and(o -> o.in(CollectionUtils.isNotEmpty(teusIds), SgBPhyInNoticesImpItem::getPsCTeusId, teusIds)
                        .or(CollectionUtils.isNotEmpty(skuIds)).in(SgBPhyInNoticesImpItem::getPsCSkuId, skuIds)));
        AssertUtils.isTrue(CollectionUtils.isNotEmpty(noticesImpItems), "入库结果单录入明细在通知单中不存在!");

        Map<Long, SgBPhyInNoticesImpItem> noticesTeusItemMap = noticesImpItems.stream().filter(o -> o.getIsTeus().equals(OcBasicConstants.IS_MATCH_SIZE_Y) && o.getPsCTeusId() != null)
                .collect(Collectors.toMap(SgBPhyInNoticesImpItem::getPsCTeusId, o -> o));
        Map<Long, SgBPhyInNoticesImpItem> noticesItemMap = noticesImpItems.stream().filter(o -> o.getIsTeus().equals(OcBasicConstants.IS_MATCH_SIZE_N))
                .collect(Collectors.toMap(SgBPhyInNoticesImpItem::getPsCSkuId, o -> o));

        resultImpItems.forEach(InResultImpItem -> {
            if (InResultImpItem.getIsTeus().equals(OcBasicConstants.IS_MATCH_SIZE_Y)) {
                SgBPhyInNoticesImpItem noticesImpItem = noticesTeusItemMap.get(InResultImpItem.getPsCTeusId());
                AssertUtils.notNull(noticesImpItem, "结果单录入明细箱号[" + InResultImpItem.getPsCTeusEcode() + "]对应在通知单中不存在!");
                SgBPhyInNoticesImpItem update = new SgBPhyInNoticesImpItem();
                update.setId(noticesImpItem.getId());
                update.setQtyIn(noticesImpItem.getQtyIn().add(InResultImpItem.getQtyIn()));
                update.setQtyDiff(noticesImpItem.getQtyDiff().subtract(InResultImpItem.getQtyIn()));
                StorageESUtils.setBModelDefalutDataByUpdate(update, user);
                noticesImpItemMapper.updateById(update);
            } else {
                SgBPhyInNoticesImpItem noticesItem = noticesItemMap.get(InResultImpItem.getPsCSkuId());
                AssertUtils.notNull(noticesItem, "结果单录入明细sku[" + InResultImpItem.getPsCSkuEcode() + "]对应在通知单中不存在!");
                SgBPhyInNoticesImpItem update = new SgBPhyInNoticesImpItem();
                update.setId(noticesItem.getId());
                update.setQtyIn(noticesItem.getQtyIn().add(InResultImpItem.getQtyIn()));
                update.setQtyDiff(noticesItem.getQtyDiff().subtract(InResultImpItem.getQtyIn()));
                update.setModifierename(user.getEname());
                StorageESUtils.setBModelDefalutDataByUpdate(update, user);
                noticesImpItemMapper.updateById(update);
            }
        });
    }

    public void impItemConvertTeusMethod(List<SgBPhyInResultImpItem> impItems, User user) {
        if (CollectionUtils.isNotEmpty(impItems)) {
            List<SgBPhyInResultTeusItem> teusItemList = Lists.newArrayList();
            HashMap<Long, SgBPhyInResultImpItem> teusMap = Maps.newHashMap();
            HashMap<Long, SgBPhyInResultItem> itemsMap = Maps.newHashMap();

            for (SgBPhyInResultImpItem impitem : impItems) {
                if (impitem.getIsTeus().equals(SgSendConstantsIF.IS_TEUS_Y)) {
                    if (teusMap.containsKey(impitem.getPsCTeusId())) {
                        AssertUtils.logAndThrow("存在重复箱号" + impitem.getPsCTeusEcode() + "录入明细!");
                    } else {
                        teusMap.put(impitem.getPsCTeusId(), impitem);
                    }
                } else {
                    SgBPhyInResultItem item = new SgBPhyInResultItem();
                    BeanUtils.copyProperties(impitem, item);
                    item.setId(ModelUtil.getSequence(SgConstants.SG_B_PHY_IN_RESULT_ITEM));
                    item.setQtyIn(impitem.getQtyIn());
                    item.setAmtListIn(impitem.getQtyIn().multiply(impitem.getPriceList()));
                    itemsMap.put(impitem.getPsCSkuId(), item);
                }
            }

            if (MapUtils.isNotEmpty(teusMap)) {
                //获取箱内明细，并且 箱内明细转换成条码明细
                try {
                    QueryBoxItemsService queryTeusItemsService = ApplicationContextHandle.getBean(QueryBoxItemsService.class);
                    QueryBoxRequest boxRequest = new QueryBoxRequest();
                    List<Long> teusIds = new ArrayList<>(teusMap.keySet());
                    boxRequest.setBoxIds(teusIds);
                    List<PsCTeusItem> psCTeusItems = queryTeusItemsService.queryBoxItemsByBoxInfo(boxRequest);
                    if (CollectionUtils.isNotEmpty(psCTeusItems)) {
                        for (PsCTeusItem psCTeusItem : psCTeusItems) {
                            SgBPhyInResultTeusItem teusItem = new SgBPhyInResultTeusItem();
                            BeanUtils.copyProperties(psCTeusItem, teusItem);
                            //给箱内明细赋值商品信息、逻辑仓
                            SgBPhyInResultImpItem impItem = teusMap.get(psCTeusItem.getPsCTeusId());
                            teusItem.setSgBPhyInResultId(impItem.getSgBPhyInResultId());
                            teusItem.setQty(impItem.getQtyIn().multiply(teusItem.getQty()));
                            teusItem.setPsCProId(impItem.getPsCProId());
                            teusItem.setPsCProEcode(impItem.getPsCProEcode());
                            teusItem.setPsCProEname(impItem.getPsCProEname());
                            teusItem.setId(ModelUtil.getSequence(SgConstants.SG_B_PHY_IN_RESULT_TEUS_ITEM));
                            teusItemList.add(teusItem);

                            //箱内明细转条码明细
                            if (itemsMap.containsKey(teusItem.getPsCSkuId())) {
                                SgBPhyInResultItem inResultItem = itemsMap.get(teusItem.getPsCSkuId());
                                BigDecimal qty = Optional.ofNullable(teusItem.getQty()).orElse(BigDecimal.ZERO);
                                inResultItem.setQtyIn(inResultItem.getQtyIn().add(qty));
                                inResultItem.setAmtListIn(inResultItem.getQtyIn().multiply(inResultItem.getPriceList()));
                            } else {
                                SgBPhyInResultItem inResultItem = new SgBPhyInResultItem();
                                BeanUtils.copyProperties(teusItem, inResultItem);
                                inResultItem.setQtyIn(teusItem.getQty());
                                inResultItem.setPriceList(impItem.getPriceList());
                                inResultItem.setAmtListIn(inResultItem.getQtyIn().multiply(impItem.getPriceList()));
                                inResultItem.setId(ModelUtil.getSequence(SgConstants.SG_B_PHY_IN_RESULT_ITEM));
                                itemsMap.put(teusItem.getPsCSkuId(), inResultItem);
                            }
                        }
                    } else {
                        AssertUtils.logAndThrow("入库结果单根据箱号获取箱明细为空!");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    AssertUtils.logAndThrow("解析箱明细异常!" + e.getMessage());
                }
            }

            //插入箱内明细
            if (CollectionUtils.isNotEmpty(teusItemList)) {
                SgBPhyInResultTeusItemMapper teusItemMapper = ApplicationContextHandle.getBean(SgBPhyInResultTeusItemMapper.class);
                SgStoreUtils.batchInsertTeus(teusItemList, "入库结果单箱内明细", SgConstants.SG_COMMON_INSERT_PAGE_SIZE, teusItemMapper);
            }

            List<SgBPhyInResultItem> itemList = new ArrayList<>(itemsMap.values());

            //插入条码明细
            if (CollectionUtils.isNotEmpty(itemList)) {
                SgBPhyInResultItemMapper inResultItemMapper = ApplicationContextHandle.getBean(SgBPhyInResultItemMapper.class);
                SgStoreUtils.batchInsertTeus(itemList, "入库结果单条码明细", SgConstants.SG_COMMON_MAX_INSERT_PAGE_SIZE, inResultItemMapper);
            }
        }
    }
}
