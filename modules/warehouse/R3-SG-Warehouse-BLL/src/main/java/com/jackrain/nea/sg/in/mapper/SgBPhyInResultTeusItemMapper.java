package com.jackrain.nea.sg.in.mapper;

import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.in.model.table.SgBPhyInResultTeusItem;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SgBPhyInResultTeusItemMapper extends ExtentionMapper<SgBPhyInResultTeusItem> {
}