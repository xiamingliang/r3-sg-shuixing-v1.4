package com.jackrain.nea.sg.out.services;

import com.alibaba.fastjson.JSONArray;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.jackrain.nea.core.db.Tools;
import com.jackrain.nea.model.util.ModelUtil;
import com.jackrain.nea.oc.basic.model.request.QueryBoxRequest;
import com.jackrain.nea.oc.basic.services.QueryBoxItemsService;
import com.jackrain.nea.psext.model.table.PsCTeusItem;
import com.jackrain.nea.redis.config.CusRedisTemplate;
import com.jackrain.nea.redis.util.RedisOpsUtil;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.out.common.SgOutConstants;
import com.jackrain.nea.sg.out.mapper.SgBPhyOutResultImpItemMapper;
import com.jackrain.nea.sg.out.mapper.SgBPhyOutResultItemMapper;
import com.jackrain.nea.sg.out.mapper.SgBPhyOutResultTeusItemMapper;
import com.jackrain.nea.sg.out.model.request.SgPhyOutResultBillSaveRequest;
import com.jackrain.nea.sg.out.model.request.SgPhyOutResultImpItemSaveRequest;
import com.jackrain.nea.sg.out.model.request.SgPhyOutResultTeusItemSaveRequest;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutResultImpItem;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutResultItem;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutResultTeusItem;
import com.jackrain.nea.sg.send.common.SgSendConstantsIF;
import com.jackrain.nea.sg.send.utils.SgStoreUtils;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.*;

/**
 * @author zhu lin yu
 * @since 2019-10-22
 * create at : 2019-10-22 15:07
 */
@Slf4j
@Component
public class SgPhyOutResultConvertService {

    @Autowired
    private QueryBoxItemsService queryBoxItemsService;

    /**
     * 出库结果单-录入明细转换成条码明细/箱内明细
     *
     * @param request 录入明细
     */
    public void sgPhyOutResultRequestConvert(SgPhyOutResultBillSaveRequest request) {
        List<SgPhyOutResultImpItemSaveRequest> impItemList = new ArrayList<>();
        List<SgPhyOutResultTeusItemSaveRequest> teusItemList = new ArrayList<>();

        if (CollectionUtils.isNotEmpty(request.getImpItemList())) {

            //未存redis
            impItemList = request.getImpItemList();
            teusItemList = request.getTeusItemList();

        } else if (StringUtils.isNotEmpty(request.getRedisKey())) {
            //从redis获取录入明细和箱明细
            CusRedisTemplate<String, String> redisTemplate = RedisOpsUtil.getStrRedisTemplate();
            String result = redisTemplate.opsForValue().get(request.getRedisKey());

            if (StringUtils.isNotEmpty(result)) {
                SgPhyOutResultBillSaveRequest saveRequest = JSONArray.parseObject(result, SgPhyOutResultBillSaveRequest.class);
                impItemList.addAll(saveRequest.getImpItemList());
                teusItemList.addAll(saveRequest.getTeusItemList());
            } else {
                AssertUtils.logAndThrow("redis中出库结果单参数不能为空");
            }
        } else {
            AssertUtils.logAndThrow("入参数格式不正确");
        }

        request.setImpItemList(impItemList);
        request.setTeusItemList(teusItemList);
    }


    /**
     * 出库通知单录入明细新增
     *
     * @param objId       通知单id
     * @param impItemList 录入明细
     * @param loginUser   用户
     */
    public void insertOutResultImpItem(Long objId, List<SgPhyOutResultImpItemSaveRequest> impItemList, User loginUser) {
        SgBPhyOutResultImpItemMapper noticesImpItemMapper = ApplicationContextHandle.getBean(SgBPhyOutResultImpItemMapper.class);
        List<SgBPhyOutResultImpItem> outNoticesImpItemList = new ArrayList<>();
        BigDecimal zero = BigDecimal.ZERO;
        String eName = loginUser.getEname();


        //2019-08-12 通知单保存时合并相同sku的明细
        Map<Long, SgPhyOutResultImpItemSaveRequest> skuRequestMap = Maps.newHashMap();
        Map<Long, SgPhyOutResultImpItemSaveRequest> teusRequestMap = Maps.newHashMap();
        for (SgPhyOutResultImpItemSaveRequest ImpItemRequest : impItemList) {

            if (ImpItemRequest.getPsCTeusId() == null) {
                //散件合并相同sku的明细
                Long psCSkuId = ImpItemRequest.getPsCSkuId();
                if (skuRequestMap.containsKey(psCSkuId)) {
                    SgPhyOutResultImpItemSaveRequest orgOutNoticesImpItemSaveRequest = skuRequestMap.get(psCSkuId);
                    BigDecimal orgQty = Optional.ofNullable(orgOutNoticesImpItemSaveRequest.getQtyOut()).orElse(BigDecimal.ZERO);
                    BigDecimal qty = Optional.ofNullable(ImpItemRequest.getQtyOut()).orElse(BigDecimal.ZERO);
                    orgOutNoticesImpItemSaveRequest.setQtyOut(qty.add(orgQty));
                } else {
                    skuRequestMap.put(psCSkuId, ImpItemRequest);
                }

            } else {
                //合并相同箱
                Long psCTeusId = ImpItemRequest.getPsCTeusId();
                if (teusRequestMap.containsKey(psCTeusId)) {
                    SgPhyOutResultImpItemSaveRequest orgOutNoticesImpItemSaveRequest = teusRequestMap.get(psCTeusId);
                    BigDecimal orgQty = Optional.ofNullable(orgOutNoticesImpItemSaveRequest.getQtyOut()).orElse(BigDecimal.ZERO);
                    BigDecimal qty = Optional.ofNullable(ImpItemRequest.getQtyOut()).orElse(BigDecimal.ZERO);
                    orgOutNoticesImpItemSaveRequest.setQtyOut(qty.add(orgQty));
                } else {
                    teusRequestMap.put(psCTeusId, ImpItemRequest);
                }

            }

        }

        //合并后的明细
        List<SgPhyOutResultImpItemSaveRequest> allImpItemList = new ArrayList<>();
        List<SgPhyOutResultImpItemSaveRequest> mergeImpItemSkuList = new ArrayList<>(skuRequestMap.values());
        List<SgPhyOutResultImpItemSaveRequest> mergeImpItemTuesList = new ArrayList<>(teusRequestMap.values());
        allImpItemList.addAll(mergeImpItemSkuList);
        allImpItemList.addAll(mergeImpItemTuesList);

        allImpItemList.forEach(item -> {
            BigDecimal qty = item.getQtyOut();          // 通知数量

            SgBPhyOutResultImpItem noticesImpItem = new SgBPhyOutResultImpItem();
            Long id = Tools.getSequence(SgConstants.SG_B_PHY_OUT_RESULT_IMP_ITEM);

            BeanUtils.copyProperties(item, noticesImpItem);
            noticesImpItem.setId(id);
            noticesImpItem.setSgBPhyOutResultId(objId);
            noticesImpItem.setQtyOut(qty);        // 出库数量
            noticesImpItem.setOwnerename(eName);
            noticesImpItem.setModifierename(eName);
            StorageESUtils.setBModelDefalutData(noticesImpItem, loginUser);

            outNoticesImpItemList.add(noticesImpItem);
        });

        //分批次，批量新增
        List<List<SgBPhyOutResultImpItem>> baseModelPageList
                = StorageESUtils.getBaseModelPageList(outNoticesImpItemList, SgOutConstants.INSERT_PAGE_SIZE);

        int result = 0;
        for (List<SgBPhyOutResultImpItem> pageList : baseModelPageList) {

            if (CollectionUtils.isEmpty(pageList)) {
                continue;
            }

            int count = noticesImpItemMapper.batchInsert(pageList);

            if (count != pageList.size()) {
                AssertUtils.logAndThrow(" 出库通结果录入明细批量新增失败！", loginUser.getLocale());
            }
            result += count;
        }

        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + " 出库结果单录入明细新增结果：" + result);
        }
    }

    /**
     * 出库通知单箱明细新增
     *
     * @param objId        通知单id
     * @param teusItemList 录入明细
     * @param loginUser    用户
     */
    public void insertOutResultTeusItem(Long objId, List<SgPhyOutResultTeusItemSaveRequest> teusItemList, User loginUser) {
        SgBPhyOutResultTeusItemMapper noticesTeusItemMapper = ApplicationContextHandle.getBean(SgBPhyOutResultTeusItemMapper.class);
        List<SgBPhyOutResultTeusItem> outNoticesTeusItemList = new ArrayList<>();
        String eName = loginUser.getEname();

        //封装出库通知单箱明细其他参数
        for (SgPhyOutResultTeusItemSaveRequest teusItemSaveRequest : teusItemList) {

            SgBPhyOutResultTeusItem sgBPhyOutNoticesTeusItem = new SgBPhyOutResultTeusItem();
            Long id = Tools.getSequence(SgConstants.SG_B_PHY_OUT_RESULT_TEUS_ITEM);

            BeanUtils.copyProperties(teusItemSaveRequest, sgBPhyOutNoticesTeusItem);
            sgBPhyOutNoticesTeusItem.setId(id);
            sgBPhyOutNoticesTeusItem.setSgBPhyOutResultId(objId);
            sgBPhyOutNoticesTeusItem.setOwnerename(eName);
            sgBPhyOutNoticesTeusItem.setModifierename(eName);
            StorageESUtils.setBModelDefalutData(sgBPhyOutNoticesTeusItem, loginUser);
            outNoticesTeusItemList.add(sgBPhyOutNoticesTeusItem);
        }

        //分批次，批量新增
        List<List<SgBPhyOutResultTeusItem>> baseModelPageList
                = StorageESUtils.getBaseModelPageList(outNoticesTeusItemList, SgOutConstants.INSERT_PAGE_SIZE);

        int result = 0;
        for (List<SgBPhyOutResultTeusItem> pageList : baseModelPageList) {

            if (CollectionUtils.isEmpty(pageList)) {
                continue;
            }

            int count = noticesTeusItemMapper.batchInsert(pageList);

            if (count != pageList.size()) {
                AssertUtils.logAndThrow(" 出库结果单箱明细批量新增失败！", loginUser.getLocale());
            }
            result += count;
        }

        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + " 出库结果单箱明细新增结果：" + result);
        }

    }

    public List<SgBPhyOutResultItem> impItemConvertTeusMethod(List<SgBPhyOutResultImpItem> impItems, List<SgBPhyOutResultItem> resultItems) {

        if (CollectionUtils.isNotEmpty(impItems)) {
            List<SgBPhyOutResultTeusItem> teusItemList = Lists.newArrayList();
            HashMap<Long, SgBPhyOutResultImpItem> teusMap = Maps.newHashMap();
            HashMap<Long, SgBPhyOutResultItem> itemsMap = Maps.newHashMap();

            for (SgBPhyOutResultImpItem impitem : impItems) {
                if (impitem.getIsTeus().equals(SgSendConstantsIF.IS_TEUS_Y)) {
                    if (teusMap.containsKey(impitem.getPsCTeusId())) {
                        AssertUtils.logAndThrow("存在重复箱号" + impitem.getPsCTeusEcode() + "录入明细!");
                    } else {
                        teusMap.put(impitem.getPsCTeusId(), impitem);
                    }
                } else {
                    SgBPhyOutResultItem item = new SgBPhyOutResultItem();
                    BeanUtils.copyProperties(impitem, item);
                    item.setId(ModelUtil.getSequence(SgConstants.SG_B_PHY_OUT_RESULT_ITEM));
                    item.setQty(impitem.getQtyOut());
                    item.setAmtListOut(impitem.getQtyOut().multiply(impitem.getPriceList()));
                    itemsMap.put(impitem.getPsCSkuId(), item);
                }
            }

            if (MapUtils.isNotEmpty(teusMap)) {
                //获取箱内明细，并且 箱内明细转换成条码明细
                try {
                    QueryBoxItemsService queryTeusItemsService = ApplicationContextHandle.getBean(QueryBoxItemsService.class);
                    QueryBoxRequest boxRequest = new QueryBoxRequest();
                    List<Long> teusIds = new ArrayList<>(teusMap.keySet());
                    boxRequest.setBoxIds(teusIds);
                    List<PsCTeusItem> psCTeusItems = queryTeusItemsService.queryBoxItemsByBoxInfo(boxRequest);
                    if (CollectionUtils.isNotEmpty(psCTeusItems)) {
                        for (PsCTeusItem psCTeusItem : psCTeusItems) {
                            SgBPhyOutResultTeusItem teusItem = new SgBPhyOutResultTeusItem();
                            BeanUtils.copyProperties(psCTeusItem, teusItem);
                            //给箱内明细赋值商品信息、逻辑仓
                            SgBPhyOutResultImpItem impItem = teusMap.get(psCTeusItem.getPsCTeusId());
                            teusItem.setId(ModelUtil.getSequence(SgConstants.SG_B_PHY_OUT_RESULT_TEUS_ITEM));
                            teusItem.setSgBPhyOutResultId(impItem.getSgBPhyOutResultId());
                            teusItem.setQty(impItem.getQtyOut().multiply(teusItem.getQty()));
                            teusItem.setPsCProId(impItem.getPsCProId());
                            teusItem.setPsCProEcode(impItem.getPsCProEcode());
                            teusItem.setPsCProEname(impItem.getPsCProEname());
                            teusItemList.add(teusItem);

                            //箱内明细转条码明细
                            if (itemsMap.containsKey(teusItem.getPsCSkuId())) {
                                SgBPhyOutResultItem outResultItem = itemsMap.get(teusItem.getPsCSkuId());
                                BigDecimal qty = Optional.ofNullable(teusItem.getQty()).orElse(BigDecimal.ZERO);
                                outResultItem.setQty(outResultItem.getQty().add(qty));
                                outResultItem.setAmtListOut(outResultItem.getQty().multiply(outResultItem.getPriceList()));
                            } else {
                                SgBPhyOutResultItem outResultItem = new SgBPhyOutResultItem();
                                BeanUtils.copyProperties(teusItem, outResultItem);
                                outResultItem.setId(ModelUtil.getSequence(SgConstants.SG_B_PHY_OUT_RESULT_ITEM));
                                outResultItem.setQty(teusItem.getQty());
                                outResultItem.setAmtListOut(outResultItem.getQty().multiply(impItem.getPriceList()));
                                outResultItem.setPriceList(impItem.getPriceList());
                                itemsMap.put(teusItem.getPsCSkuId(), outResultItem);
                            }
                        }
                    } else {
                        AssertUtils.logAndThrow("出库结果单根据箱号获取箱明细为空!");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    AssertUtils.logAndThrow("解析箱明细异常!" + e.getMessage());
                }
            }

            List<SgBPhyOutResultItem> itemList = new ArrayList<>(itemsMap.values());
            //插入箱内明细
            if (CollectionUtils.isNotEmpty(teusItemList)) {
                SgBPhyOutResultTeusItemMapper teusItemMapper = ApplicationContextHandle.getBean(SgBPhyOutResultTeusItemMapper.class);
                SgStoreUtils.batchInsertTeus(teusItemList, "出库结果单箱内明细", SgConstants.SG_COMMON_INSERT_PAGE_SIZE, teusItemMapper);
            }

            //插入条码明细
            if (CollectionUtils.isNotEmpty(itemList)) {
                SgBPhyOutResultItemMapper inResultItemMapper = ApplicationContextHandle.getBean(SgBPhyOutResultItemMapper.class);
                SgStoreUtils.batchInsertTeus(itemList, "出库结果单条码明细", SgConstants.SG_COMMON_INSERT_PAGE_SIZE, inResultItemMapper);
                if (resultItems != null) {
                    resultItems = itemList;
                }
            }
        }
        return resultItems;
    }

}
