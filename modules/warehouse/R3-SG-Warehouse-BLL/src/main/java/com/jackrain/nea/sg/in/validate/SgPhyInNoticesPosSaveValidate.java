package com.jackrain.nea.sg.in.validate;

import com.jackrain.nea.sg.in.common.SgInNoticeConstants;
import com.jackrain.nea.sg.in.mapper.SgBPhyInNoticesMapper;
import com.jackrain.nea.sg.in.model.table.SgBPhyInNotices;
import com.jackrain.nea.tableService.MainTableRecord;
import com.jackrain.nea.tableService.TableServiceContext;
import com.jackrain.nea.tableService.validate.BaseValidator;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.utils.AssertUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author zhu lin yu
 * @since 2019/12/10
 * create at : 2019/12/10 5:01 下午
 */
@Slf4j
@Component
public class SgPhyInNoticesPosSaveValidate extends BaseValidator {
    @Override
    public void validate(TableServiceContext context, MainTableRecord currentRow) {

        Long objId = currentRow.getId();
        AssertUtils.notNull(objId, "门店入库单ID为空！", context.getLocale());

        SgBPhyInNoticesMapper packMapper = ApplicationContextHandle.getBean(SgBPhyInNoticesMapper.class);
        SgBPhyInNotices sgBPhyInNotices = packMapper.selectById(objId);
        AssertUtils.notNull(sgBPhyInNotices, "当前记录已不存在！", context.getLocale());

        Integer status = sgBPhyInNotices.getStatus();

        if (SgInNoticeConstants.AUDIT_STATUS_AUDIT == status) {
            AssertUtils.logAndThrow("当前记录已审核，不允许编辑！", context.getLocale());
        } else if (SgInNoticeConstants.AUDIT_STATUS_VOID == status) {
            AssertUtils.logAndThrow("当前记录已作废，不允许编辑！", context.getLocale());
        }

    }
}
