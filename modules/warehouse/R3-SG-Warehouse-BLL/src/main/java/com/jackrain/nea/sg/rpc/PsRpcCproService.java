package com.jackrain.nea.sg.rpc;

import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.psext.model.table.PsCPro;
import com.jackrain.nea.sx.api.PsCproCmd;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


@Component
@Slf4j
public class PsRpcCproService {

    @Reference(version = "1.0", group = "ps-sx")
    private PsCproCmd psCproCmd;

    /**
     * @MethodName: queryCpStoreById
     * @Description: 查询商品档案
     * @Param: [ecode]
     * @Return: com.jackrain.nea.sys.domain.ValueHolderV14<com.jackrain.nea.cpext.model.table.CpCStore>
     * @Author: chenfajun
     * @Date: 2020/6/9
     **/
    public ValueHolderV14<PsCPro> queryCproByEcode(String ecode) {
        ValueHolderV14 vh = new ValueHolderV14<>();
        try {
            List<String> ecodes = new ArrayList<>();
            ecodes.add(ecode);
            ValueHolderV14<List<PsCPro>> psCPros = psCproCmd.getSkuPrice(ecodes);
            if (Objects.isNull(psCPros)) {
                vh.setCode(ResultCode.FAIL);
                return vh;
            }
            vh.setData(psCPros.getData().get(0));
            vh.setCode(ResultCode.SUCCESS);
            return vh;
        } catch (Exception e) {
            e.printStackTrace();
            log.error(this.getClass().getName() + " Rpc查询商品档案异常", e.getMessage());
            vh.setCode(ResultCode.FAIL);
            vh.setMessage(" Rpc查询商品档案异常" + e.getMessage());
            return vh;
        }
    }

}
