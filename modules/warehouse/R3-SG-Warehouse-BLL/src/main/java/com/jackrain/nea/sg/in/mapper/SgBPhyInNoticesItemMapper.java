package com.jackrain.nea.sg.in.mapper;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.in.common.SgInConstants;
import com.jackrain.nea.sg.in.model.table.SgBPhyInNoticesItem;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.UpdateProvider;

import java.math.BigDecimal;
import java.util.List;

@Mapper
public interface SgBPhyInNoticesItemMapper extends ExtentionMapper<SgBPhyInNoticesItem> {


    /**
     * 更新入库通知单明细（入库结果单专用）
     *
     * @param qtyIn        入库数量
     * @param amtListIn    入库吊牌金额
     * @param amtCostIn    入库成交金额
     * @param qtyInDiff    差异数量
     * @param amtListDiff  差异吊牌金额
     * @param amtCostDiff  差异成交金额
     * @param noticesId           id
     * @param defaultqtyIn 没更新之前的入库数量（用来当作锁）
     * @return
     */
    @UpdateProvider(type = SgBPhyInNoticesItemSqlProvider.class,
            method = "updateWarehousingPassByQtyInLock")
    int updateWarehousingPassByQtyInLock(@Param("qtyIn") BigDecimal qtyIn,
                                         @Param("amtListIn") BigDecimal amtListIn,
                                         @Param("amtCostIn") BigDecimal amtCostIn,
                                         @Param("qtyInDiff") BigDecimal qtyInDiff,
                                         @Param("amtListDiff") BigDecimal amtListDiff,
                                         @Param("amtCostDiff") BigDecimal amtCostDiff,
                                         @Param("noticesId") Long noticesId,
                                         @Param("ecode") String ecode,
                                         @Param("defaultqtyIn") BigDecimal defaultqtyIn);

    /**
     * 分页查询入库通知单明细
     */
    @Select("SELECT * FROM " + SgConstants.SG_B_PHY_IN_NOTICES_ITEM + " ORDER BY ID LIMIT #{page} OFFSET #{offset}")
    List<SgBPhyInNoticesItem> selectListByPage(@Param("page") int page, @Param("offset") int offset);

    /**
     * 按条码id分组查询通知单明细通知数量与入库数量
     */
    @Select("SELECT DISTINCT ps_c_sku_id,ps_c_sku_ecode,SUM(qty) TOT_QTY,SUM(qty_in) TOT_QTY_IN FROM " + SgConstants.SG_B_PHY_IN_NOTICES_ITEM
            + " WHERE " + SgInConstants.SG_B_PHY_IN_NOTICES_ID + "=#{objid} GROUP BY ps_c_sku_id,ps_c_sku_ecode\n")
    List<JSONObject> selectSumGroupBySku(@Param("objid") Long objid);

    /**
     * 入库通知单子集动态拼接帮助类
     */
    class SgBPhyInNoticesItemSqlProvider {
        public String updateWarehousingPassByQtyInLock(@Param("qtyIn") BigDecimal qtyIn,
                                                       @Param("amtListIn") BigDecimal amtListIn,
                                                       @Param("amtCostIn") BigDecimal amtCostIn,
                                                       @Param("qtyInDiff") BigDecimal qtyInDiff,
                                                       @Param("amtListDiff") BigDecimal amtListDiff,
                                                       @Param("amtCostDiff") BigDecimal amtCostDiff,
                                                       @Param("noticesId") Long noticesId,
                                                       @Param("ecode") String ecode,
                                                       @Param("defaultqtyIn") BigDecimal defaultqtyIn) {
            StringBuilder sb = new StringBuilder();
            sb.append("  update sg_b_phy_in_notices_item\n" +
                    "    set QTY_IN=#{qtyIn},\n" +
                    "    AMT_LIST_IN=#{amtListIn},\n" +
                    "    AMT_COST_IN=#{amtCostIn},\n" +
                    "    QTY_DIFF=#{qtyInDiff},\n" +
                    "    AMT_LIST_DIFF=#{amtListDiff},\n" +
                    "    AMT_COST_DIFF=#{amtCostDiff}\n " +
                    "   Where PS_C_SKU_ECODE = #{ecode}" +
                    "   AND sg_b_phy_in_notices_id=#{noticesId}");
            if (defaultqtyIn == null) {
                sb.append("    and QTY_IN  is null");
            } else {
                sb.append("  and QTY_IN =#{defaultqtyIn}");
            }
            return sb.toString();
        }
    }
}