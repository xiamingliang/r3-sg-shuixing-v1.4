package com.jackrain.nea.sg.out.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.google.common.collect.Lists;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.model.util.AdParamUtil;
import com.jackrain.nea.r3.mq.exception.SendMqException;
import com.jackrain.nea.r3.mq.util.R3MqSendHelper;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.out.common.SgOutConstants;
import com.jackrain.nea.sg.out.common.SgOutConstantsIF;
import com.jackrain.nea.sg.out.config.SgOutMqConfig;
import com.jackrain.nea.sg.out.mapper.SgBPhyOutNoticesMapper;
import com.jackrain.nea.sg.out.model.request.SgPhyOutNoticesBillWMSBackRequest;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutNotices;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * @author leexh
 * @since 2019/5/6 15:56
 */
@Slf4j
@Component
public class SgPhyOutNoticesSaveWMSBackService {

    @Autowired
    private R3MqSendHelper r3MqSendHelper;

    @Autowired
    private SgOutMqConfig mqConfig;

    @Value("${sg.bill.phy_out_notices_to_wms_max_threads:10}")
    private Integer threadWmsBack;

    public ValueHolderV14 saveWMSBackOutNotices(List<SgPhyOutNoticesBillWMSBackRequest> requests, User user) {

        long start = System.currentTimeMillis();
        ValueHolderV14 v14 = new ValueHolderV14();
        if (CollectionUtils.isEmpty(requests)) {
            AssertUtils.logAndThrow("参数不能为空！");
        }

        AssertUtils.notNull(user, "用户信息不能为空！");
        String eName = user.getEname();

        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ".debug, param:" + JSONObject.toJSONString(requests));
        }

        List<String> billNos = requests.stream().map(SgPhyOutNoticesBillWMSBackRequest::getOrderNo).collect(Collectors.toList());
        SgBPhyOutNoticesMapper outNoticesMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesMapper.class);
        // 根据出库通知单单据编号查询出库通知单
        List<SgBPhyOutNotices> notices = outNoticesMapper.selectList(new QueryWrapper<SgBPhyOutNotices>().lambda().in(SgBPhyOutNotices::getBillNo, billNos));
        Map<String, SgBPhyOutNotices> noticesMap = notices.stream().collect(Collectors.toMap(SgBPhyOutNotices::getBillNo, outNotices -> outNotices));
        // 将回执参数转成  通知单号-request
     /*   Map<String, SgPhyOutNoticesBillWMSBackRequest> requestMap = requests.stream().collect(
                Collectors.toMap(SgPhyOutNoticesBillWMSBackRequest::getOrderNo, request -> request));*/

        List<List<SgPhyOutNoticesBillWMSBackRequest>> requestsList = StorageESUtils.averageAssign(requests, threadWmsBack);
        for (List<SgPhyOutNoticesBillWMSBackRequest> requestList : requestsList) {
            Runnable task = null;
            if (CollectionUtils.isNotEmpty(requestList)) {
                task = () -> {
                    List<JSONObject> bodyList = Lists.newArrayList();
                    List<String> billNoList = Lists.newArrayList();
                    for (int i = 0; i < requestList.size(); i++) {
                        SgPhyOutNoticesBillWMSBackRequest request = requestList.get(i);
                        String billNo = request.getOrderNo();
                        if (StringUtils.isNotEmpty(billNo)) {
                            SgBPhyOutNotices outNotices = noticesMap.get(billNo);
                            if (outNotices != null) {
                                SgBPhyOutNotices updateOutNotices = new SgBPhyOutNotices();
                                StorageESUtils.setBModelDefalutDataByUpdate(updateOutNotices, user);
                                updateOutNotices.setModifierename(eName);
                                updateOutNotices.setId(outNotices.getId());
                                if (request.getCode() == ResultCode.SUCCESS) {
                                    updateOutNotices.setWmsStatus(SgOutConstantsIF.WMS_STATUS_PASS_SUCCESS);
                                    updateOutNotices.setPassWmsTime(new Date());
                                    updateOutNotices.setWmsFailCount(0L);
                                    updateOutNotices.setWmsBillNo(request.getDeliveryOrderId());
                                    outNoticesMapper.update(updateOutNotices, new UpdateWrapper<SgBPhyOutNotices>().lambda()
                                            .eq(SgBPhyOutNotices::getId, outNotices.getId())
                                            .set(SgBPhyOutNotices::getWmsFailReason, null));
                                } else if (request.getCode() == ResultCode.FAIL) {
                                    String backResultMessgae = request.getMessage();
                                    try {
                                        String failedReason = AdParamUtil.getParam(SgOutConstants.SYSTEM_WMSBACK_FAILED_REASON);
                                        log.debug("系统参数零售发货单传WMS失败信息:" + failedReason);
                                        if (StringUtils.isNotEmpty(failedReason)) {
                                            String[] reasons = failedReason.split(";;");
                                            List<String> failedReasons = Lists.newArrayList();
                                            for (String reason : reasons) {
                                                String replace = reason.replace("[", "").replace("]", "");
                                                failedReasons.add(replace);
                                            }
                                            boolean contains = false;
                                            for (String reason : failedReasons) {
                                                if (backResultMessgae.contains(reason)) {
                                                    contains = true;
                                                    break;
                                                }
                                            }
                                            if (contains) { //成功
                                                request.setCode(ResultCode.SUCCESS);
                                                updateOutNotices.setWmsStatus(SgOutConstantsIF.WMS_STATUS_PASS_SUCCESS);
                                                updateOutNotices.setPassWmsTime(new Timestamp(System.currentTimeMillis()));
                                                updateOutNotices.setWmsFailCount(0L);
                                                outNoticesMapper.update(updateOutNotices, new UpdateWrapper<SgBPhyOutNotices>().lambda()
                                                        .eq(SgBPhyOutNotices::getId, outNotices.getId())
                                                        .set(SgBPhyOutNotices::getWmsFailReason, null));
                                            } else {
                                                AssertUtils.logAndThrow("系统参数零售发货单传WMS失败信息不包含该信息,记录失败信息!");
                                            }
                                        } else {
                                            AssertUtils.logAndThrow("系统参数零售发货单传WMS失败信息为空!");
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        updateOutNotices.setWmsStatus(SgOutConstantsIF.WMS_STATUS_PASS_FAILED);
                                        //2019-09-06 截取备注等超长字段  - 失败原因
                                        updateOutNotices.setWmsFailReason(StorageESUtils.strSubString(backResultMessgae, SgConstants.SG_COMMON_STRING_SIZE));
                                        Long wmsFailCount = outNotices.getWmsFailCount();
                                        log.debug("回执状态:失败，出库通知单原失败次数:" + wmsFailCount);
                                        updateOutNotices.setWmsFailCount(wmsFailCount == null ? 1L : wmsFailCount + 1);
                                        outNoticesMapper.updateById(updateOutNotices);
                                    }
                                }

                                //TODO 发送MQ可以改成 批量
                                // 如果不是大货出库，发送MQ通知全渠道订单
                                if (outNotices.getOutType() != SgOutConstantsIF.OUT_TYPE_BIG_GOODS) {
                                    // 来源单据ID和来源单据类型并发送MQ消息给到【更新全渠道订单状态服务】...
                                    JSONObject body = new JSONObject();
                                    body.put("orderId", outNotices.getSourceBillId());
                                    body.put("orderNo", outNotices.getSourceBillNo());
                                    body.put("code", request.getCode());    // 回执结果
                                    body.put("orderType", outNotices.getSourceBillType());
                                    if (request.getCode() == ResultCode.FAIL) {
                                        body.put("wmsFailReason", request.getMessage());
                                    }
                                    bodyList.add(body);
                                    billNoList.add(billNo);
                                }
                            }
                        }
                    }

                    if (CollectionUtils.isNotEmpty(bodyList)) {
                        JSONObject ret = new JSONObject();
                        ret.put("type", 1);
                        ret.put("body", bodyList);
                        if (log.isDebugEnabled()) {
                            log.debug("通知单" + billNoList + ",零售回执消息体:" + ret.toJSONString());
                        }
                        sendMsg(mqConfig.getConfigName(), ret.toJSONString(), mqConfig.getOmsMqTopic(),
                                SgOutConstants.TAG_OUT_NOTICES_CREATE_RECEIPT, UUID.randomUUID().toString().replaceAll("-", ""));
                    }
                };
                new Thread(task).start();
            }
        }

        v14.setCode(ResultCode.SUCCESS);
        v14.setMessage("成功！");
        if (log.isDebugEnabled()) {
            log.debug("Finish SgPhyOutNoticesSaveWMSBackService.saveWMSBackOutNotices. ReturnResult:time consuming:{}ms;"
                    , System.currentTimeMillis() - start);
        }
        return v14;
    }

    /**
     * 发送MQ消息
     *
     * @param configName
     * @param body
     * @param topic
     * @param tag
     * @param msgKey
     * @return
     */
    public void sendMsg(String configName, String body, String topic, String tag, String msgKey) {
        try {
            r3MqSendHelper.sendMessage(configName, body, topic, tag, msgKey);
        } catch (SendMqException e) {
            log.error(this.getClass().getName() + ".error,发送MQ消息给到【更新全渠道订单状态服务】异常：" + e.getMessage(), e);
        }
    }
}
