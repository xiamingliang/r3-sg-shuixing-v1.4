package com.jackrain.nea.sg.out.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.out.mapper.SgBPhyOutNoticesImpItemMapper;
import com.jackrain.nea.sg.out.model.request.SgPhyOutNoticesQueryRequest;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutNoticesImpItem;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author zhu lin yu
 * @since 2019-10-23
 * create at : 2019-10-23 20:03
 */

@Slf4j
@Component
public class SgPhyOutNoticesImpItemQueryService {
    public ValueHolderV14<List<SgBPhyOutNoticesImpItem>> queryOutNoticesItem(SgPhyOutNoticesQueryRequest request) {
        long start = System.currentTimeMillis();
        ValueHolderV14<List<SgBPhyOutNoticesImpItem>> valueHolderV14 = new ValueHolderV14<>();

        if (log.isDebugEnabled()) {
            log.debug("Start SgPhyOutNoticesItemQueryService.queryOutNoticesItem. ReceiveParams:requests:{}"
                    , JSONObject.toJSONString(request));
        }

        if (request == null || request.getPhyOutNoticesId() == null) {
            valueHolderV14.setCode(ResultCode.FAIL);
            valueHolderV14.setMessage("请传入查询参数");
            return valueHolderV14;
        }

        Long phyOutNoticesId = request.getPhyOutNoticesId();

        SgBPhyOutNoticesImpItemMapper sgBPhyOutNoticesImpItemMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesImpItemMapper.class);
        List<SgBPhyOutNoticesImpItem> sgBPhyOutNoticesItems
                = sgBPhyOutNoticesImpItemMapper.selectList(new QueryWrapper<SgBPhyOutNoticesImpItem>().lambda().eq(SgBPhyOutNoticesImpItem::getSgBPhyOutNoticesId, phyOutNoticesId));

        valueHolderV14.setCode(ResultCode.SUCCESS);
        valueHolderV14.setMessage("查询成功");
        valueHolderV14.setData(sgBPhyOutNoticesItems);

        if (log.isDebugEnabled()) {
            log.debug("Finsh SgPhyOutNoticesItemQueryService.queryOutNoticesItem. Result:valueHolderV14:{},spend time:{}ms"
                    , JSONObject.toJSONString(valueHolderV14), System.currentTimeMillis() - start);
        }

        return valueHolderV14;
    }
}
