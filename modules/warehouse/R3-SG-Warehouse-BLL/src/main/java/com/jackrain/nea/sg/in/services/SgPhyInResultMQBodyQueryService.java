package com.jackrain.nea.sg.in.services;

import com.google.common.collect.Lists;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.in.model.request.SgPhyInResultBillSaveRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author: 舒威
 * @since: 2019/9/2
 * create at : 2019/9/2 20:40
 */
@Slf4j
@Component
public class SgPhyInResultMQBodyQueryService {

    public ValueHolderV14<List<SgPhyInResultBillSaveRequest>> querySgPhyInResultMQBody(List<Long> ids, User user) {
        if (log.isDebugEnabled()) {
            log.debug("入库结果单获取mqbody入参:" + ids);
        }
        ValueHolderV14<List<SgPhyInResultBillSaveRequest>> vh = new ValueHolderV14<>(ResultCode.SUCCESS, "success");
        SgPhyInRPCService rpcService = ApplicationContextHandle.getBean(SgPhyInRPCService.class);
        List<SgPhyInResultBillSaveRequest> data = Lists.newArrayList();
        List<Long> error = Lists.newArrayList();
        List<Long> success = Lists.newArrayList();
        for (Long id : ids) {
            try {
                ValueHolderV14<SgPhyInResultBillSaveRequest> mqResult = rpcService.getSgPhyInMQResult(id, null, null, null, null, user);
                if (mqResult.isOK()) {
                    data.add(mqResult.getData());
                    success.add(id);
                } else {
                    AssertUtils.logAndThrow("入库结果单id[" + id + "]查询mqbody失败!" + mqResult.getMessage());
                }
            } catch (Exception e) {
                log.error("入库结果单id[" + id + "]查询mqbody异常!" + e.getMessage());
                error.add(id);
            }
        }
        if (CollectionUtils.isNotEmpty(data)) {
            vh.setData(data);
        } else {
            vh.setCode(ResultCode.FAIL);
            vh.setMessage("查询mqbody失败!失败入库结果单ids:" + error);
        }
        if (log.isDebugEnabled()) {
            log.debug("入库结果单获取mqbody完成!总待补全ids:{},成功ids:{},失败结果单ids:{}", ids, success, error);
        }
        return vh;
    }
}
