package com.jackrain.nea.sg.in.mapper;

import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.in.common.SgInConstants;
import com.jackrain.nea.sg.in.model.table.SgBInPickorder;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface SgBInPickorderMapper extends ExtentionMapper<SgBInPickorder> {

    @Select("SELECT * FROM " + SgConstants.SG_B_IN_PICKORDER + " WHERE STATUS <> " + SgInConstants.BILL_STATUS_VOID + "  AND ID in (${ids})")
    List<SgBInPickorder> selectIdsByStatus(@Param("ids") String ids);
}