package com.jackrain.nea.sg.in.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.jackrain.nea.ac.api.AcBOperateCloseCtrlCmd;
import com.jackrain.nea.ac.model.request.AcBOperateCloseCtrlRequest;
import com.jackrain.nea.ac.model.table.AcBOperateClose;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.ext.sequence.util.SequenceGenUtil;
import com.jackrain.nea.redis.config.CusRedisTemplate;
import com.jackrain.nea.redis.util.RedisOpsUtil;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.basic.utils.StorageUtils;
import com.jackrain.nea.sg.in.mapper.*;
import com.jackrain.nea.sg.in.model.request.SgBPhyInResultSaveRequest;
import com.jackrain.nea.sg.in.model.request.SgPhyInPickOrderAuditRequest;
import com.jackrain.nea.sg.in.model.request.SgPhyInResultBillSaveRequest;
import com.jackrain.nea.sg.in.model.request.SgPhyInResultImpItemSaveRequest;
import com.jackrain.nea.sg.in.model.result.ReturnSgPhyInResult;
import com.jackrain.nea.sg.in.model.table.*;
import com.jackrain.nea.sg.in.utils.InUtil;
import com.jackrain.nea.sg.out.common.SgOutConstants;
import com.jackrain.nea.sg.out.common.SgOutConstantsIF;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @author zhu lin yu
 * @since 2019-11-20
 * create at : 2019-11-20 16:53
 */
@Slf4j
@Component
public class SgPhyInPickOrderAuditService {

    /**
     * 入库拣货单审核
     */
    @Transactional(rollbackFor = Exception.class)
    public ValueHolderV14 auditInPickOrder(SgPhyInPickOrderAuditRequest request, SgBInPickorder pickOrder) {
        ValueHolderV14<List<ReturnSgPhyInResult>> holderV14 = new ValueHolderV14<>();

        if (log.isDebugEnabled()) {
            log.debug("Start SgPhyInPickOrderAuditService.auditInPickOrder. ReceiveParams:params:{};"
                    + JSONObject.toJSONString(request));
        }

        //参数检验
        if (request == null) {
            throw new NDSException("request is null");
        }

        Long inPickOrderId = request.getInPickOrderId();
        AssertUtils.isTrue(inPickOrderId != null, "入库拣货单审核入参不能为空");
        AssertUtils.isTrue(request.getLoginUser() != null, "用户不能为空");
        User loginUser = request.getLoginUser();

        SgBInPickorderMapper inPickOrderMapper = ApplicationContextHandle.getBean(SgBInPickorderMapper.class);
        if (pickOrder == null) {
            //判断入库拣货单是否已经审核
            pickOrder = inPickOrderMapper.selectById(inPickOrderId);
        }

        if (pickOrder == null) {
            throw new NDSException("该入库拣货单已不存在");
        }
        if (pickOrder.getBillStatus() == SgOutConstantsIF.PICK_ORDER_STATUS_AUDIT) {
            AssertUtils.logAndThrow("单据已已审核，不允许重复审核");
        }
        if (pickOrder.getBillStatus() == SgOutConstantsIF.PICK_ORDER_STATUS_VOID) {
            AssertUtils.logAndThrow("单据已作废，不允许审核");
        }

        // 拣货明细查询
        SgBInPickorderItemMapper inPickOrderItemMapper = ApplicationContextHandle.getBean(SgBInPickorderItemMapper.class);
        List<SgBInPickorderItem> pickOrderItems = inPickOrderItemMapper.selectList(new QueryWrapper<SgBInPickorderItem>().lambda()
                .eq(SgBInPickorderItem::getSgBInPickorderId, inPickOrderId));
        AssertUtils.cannot(CollectionUtils.isEmpty(pickOrderItems), "拣货明细为空，不允许审核！", loginUser.getLocale());

        // 2020-04-04：关账卡控
        checkClose(loginUser.getLocale(), pickOrder.getCpCDestEcode());

        // 2020-04-04：调拨入库单独有逻辑
        if (request.getIsTransferIn()) {
            checkStoreAndDiffQty(loginUser.getLocale(), pickOrder.getSourceBillNo(), pickOrderItems);
        }

        Map<Long, BigDecimal> teusQtyMap = new HashMap<>(16);
        Map<Long, BigDecimal> skuQtyMap = new HashMap<>(16);

        //判断拣货明细表中是否存在任意行记录入库数量大于通知数量
        int flag = 0;
        for (SgBInPickorderItem item : pickOrderItems) {
            if (item.getQtyIn().compareTo(BigDecimal.ZERO) > 0) {
                flag++;
            }

            if (item.getQtyIn().compareTo(item.getQtyNotice()) > 0) {
                AssertUtils.logAndThrow("入库数量大于总数量，不允许审核！");
            } else if (item.getPsCTeusId() == null) {
                skuQtyMap.put(item.getPsCSkuId(), item.getQtyIn());
            } else if (item.getPsCTeusId() != null) {
                teusQtyMap.put(item.getPsCTeusId(), item.getQtyIn());
            }
        }

        AssertUtils.isTrue(flag > 0, "单据入库数量为空，不允许审核！");


        //查询来源入库通知单
        SgBInPickorderNoticeItemMapper outPickOrderNoticeItemMapper = ApplicationContextHandle.getBean(SgBInPickorderNoticeItemMapper.class);
        List<SgBInPickorderNoticeItem> noticeItemList = outPickOrderNoticeItemMapper.selectList(new QueryWrapper<SgBInPickorderNoticeItem>().lambda()
                .eq(SgBInPickorderNoticeItem::getSgBInPickorderId, inPickOrderId));
        AssertUtils.isTrue(CollectionUtils.isNotEmpty(noticeItemList), "审核失败，来源入库通知单为空");
        List<String> noticeBillNoList = noticeItemList.stream().map(SgBInPickorderNoticeItem::getNoticeBillNo).collect(Collectors.toList());

        //查询入库通知单
        SgBPhyInNoticesMapper inNoticesMapper = ApplicationContextHandle.getBean(SgBPhyInNoticesMapper.class);
        List<SgBPhyInNotices> sgBPhyInNoticesList = inNoticesMapper.selectList(new QueryWrapper<SgBPhyInNotices>().lambda()
                .in(SgBPhyInNotices::getBillNo, noticeBillNoList)
                .orderByAsc(SgBPhyInNotices::getCreationdate));


        //循环来源入库通知单,调用入库结果单审核接口
        SgPhyInResultSaveAndAuditService saveAndAuditservice = ApplicationContextHandle.getBean(SgPhyInResultSaveAndAuditService.class);
        SgBPhyInNoticesImpItemMapper inNoticesImpItemMapper = ApplicationContextHandle.getBean(SgBPhyInNoticesImpItemMapper.class);

        List<SgPhyInResultBillSaveRequest> sgPhyInResultBillSaveRequests = new ArrayList<>();
        for (SgBPhyInNotices sgBPhyInNotices : sgBPhyInNoticesList) {
            List<SgBPhyInNoticesImpItem> inNoticesImpItemList = inNoticesImpItemMapper.selectList(new QueryWrapper<SgBPhyInNoticesImpItem>().lambda()
                    .eq(SgBPhyInNoticesImpItem::getSgBPhyInNoticesId, sgBPhyInNotices.getId())
                    .gt(SgBPhyInNoticesImpItem::getQtyDiff, BigDecimal.ZERO));

            /**
             * 分配出库数量：
             * 当入库数量=通知数量时，所有入库通知单的所有明细中出库数量=通知数量；
             * 当入库数量＜通知数量时，根据来源入库通知单的创建时间的先后顺序优先分配，时间越早的优先分配；
             */
            List<SgPhyInResultImpItemSaveRequest> inResultImpItemSaveRequests = new ArrayList<>();
            int allQtyFlag = 0;
            //标记明细出库数量为0的条数
            int allZeroFlag = 0;
            for (SgBPhyInNoticesImpItem impItem : inNoticesImpItemList) {
                if (impItem.getPsCTeusId() == null) {
                    if (skuQtyMap.containsKey(impItem.getPsCSkuId())) {
                        BigDecimal totalQty = skuQtyMap.get(impItem.getPsCSkuId());
                        if (totalQty != null) {
                            SgPhyInResultImpItemSaveRequest inResultImpItemSaveRequest = new SgPhyInResultImpItemSaveRequest();
                            BeanUtils.copyProperties(impItem, inResultImpItemSaveRequest);

                            if (totalQty.compareTo(impItem.getQtyDiff()) >= 0) {
                                inResultImpItemSaveRequest.setQtyIn(impItem.getQtyDiff());
                                skuQtyMap.put(impItem.getPsCSkuId(), totalQty.subtract(impItem.getQtyDiff()));
                                inResultImpItemSaveRequests.add(inResultImpItemSaveRequest);
                                allQtyFlag++;
                            } else {
                                if (totalQty.compareTo(BigDecimal.ZERO) == 0) {
                                    allZeroFlag++;
                                }
                                inResultImpItemSaveRequest.setQtyIn(totalQty);
                                skuQtyMap.remove(impItem.getPsCSkuId());
                                inResultImpItemSaveRequests.add(inResultImpItemSaveRequest);
                            }
                        }
                    } else {
                        SgPhyInResultImpItemSaveRequest inResultImpItemSaveRequest = new SgPhyInResultImpItemSaveRequest();
                        BeanUtils.copyProperties(impItem, inResultImpItemSaveRequest);
                        inResultImpItemSaveRequest.setQtyIn(BigDecimal.ZERO);
                        inResultImpItemSaveRequests.add(inResultImpItemSaveRequest);
                        allZeroFlag++;
                    }

                } else if (impItem.getPsCTeusId() != null) {
                    if (teusQtyMap.containsKey(impItem.getPsCTeusId())) {
                        BigDecimal totalQty = teusQtyMap.get(impItem.getPsCTeusId());
                        if (totalQty != null) {
                            SgPhyInResultImpItemSaveRequest inResultImpItemSaveRequest = new SgPhyInResultImpItemSaveRequest();
                            BeanUtils.copyProperties(impItem, inResultImpItemSaveRequest);

                            if (totalQty.compareTo(impItem.getQtyDiff()) > 0) {
                                inResultImpItemSaveRequest.setQtyIn(impItem.getQtyDiff());
                                teusQtyMap.put(impItem.getPsCTeusId(), totalQty.subtract(impItem.getQtyDiff()));
                                inResultImpItemSaveRequests.add(inResultImpItemSaveRequest);
                                allQtyFlag++;
                            } else {
                                if (totalQty.compareTo(BigDecimal.ZERO) == 0) {
                                    allZeroFlag++;
                                }
                                inResultImpItemSaveRequest.setQtyIn(totalQty);
                                teusQtyMap.remove(impItem.getPsCTeusId());
                                inResultImpItemSaveRequests.add(inResultImpItemSaveRequest);
                            }
                        }
                    } else {
                        SgPhyInResultImpItemSaveRequest inResultImpItemSaveRequest = new SgPhyInResultImpItemSaveRequest();
                        BeanUtils.copyProperties(impItem, inResultImpItemSaveRequest);
                        inResultImpItemSaveRequest.setQtyIn(BigDecimal.ZERO);
                        inResultImpItemSaveRequests.add(inResultImpItemSaveRequest);
                        allZeroFlag++;
                    }

                }
            }

            SgPhyInResultBillSaveRequest inResultBillSaveRequest = new SgPhyInResultBillSaveRequest();
            SgBPhyInResultSaveRequest inResultSaveRequest = new SgBPhyInResultSaveRequest();
            BeanUtils.copyProperties(sgBPhyInNotices, inResultSaveRequest);
            inResultSaveRequest.setId(-1L);
            inResultSaveRequest.setSgBPhyInNoticesId(sgBPhyInNotices.getId());
            inResultSaveRequest.setSgBPhyInNoticesBillno(sgBPhyInNotices.getBillNo());
            inResultSaveRequest.setTotQtyIn(sgBPhyInNotices.getTotQtyDiff());
            inResultSaveRequest.setInTime(new Date());
            if (allQtyFlag == inNoticesImpItemList.size()) {
                inResultSaveRequest.setIsLast(SgConstants.IS_LAST_YES);
            } else {
                inResultSaveRequest.setIsLast(SgConstants.IS_LAST_NO);
            }
            inResultBillSaveRequest.setImpItemList(inResultImpItemSaveRequests);
            inResultBillSaveRequest.setSgBPhyInResultSaveRequest(inResultSaveRequest);
            inResultBillSaveRequest.setLoginUser(request.getLoginUser());
            inResultBillSaveRequest.setObjId(-1L);

            inResultBillSaveRequest.setPackOrderId(inPickOrderId);

            //明细数量全为0时不生成结果单
            if (allZeroFlag != inNoticesImpItemList.size()) {
                sgPhyInResultBillSaveRequests.add(inResultBillSaveRequest);
            } else {
                SgBPhyInNotices updatePhyInNotice = new SgBPhyInNotices();
                updatePhyInNotice.setId(sgBPhyInNotices.getId());
                updatePhyInNotice.setPickStatus(SgOutConstantsIF.PICK_STATUS_WAIT);
                inNoticesMapper.updateById(updatePhyInNotice);
            }
        }

        CusRedisTemplate<String, String> redisTemplate = RedisOpsUtil.getObjRedisTemplate();
        String lockKsy = SgConstants.SG_B_IN_PICKORDER + ":" + inPickOrderId;
        Boolean ifAbsent = redisTemplate.opsForValue().setIfAbsent(lockKsy, "OK");

        if (ifAbsent != null && ifAbsent) {
            redisTemplate.opsForValue().set(lockKsy, "OK", 30, TimeUnit.MINUTES);
        } else {
            String msg = "当前单据正在被操作！请稍后重试..";
            log.debug(this.getClass().getName() + ".debug," + msg);
            AssertUtils.logAndThrow(msg, loginUser.getLocale());
        }

        try {
            //调用入库结果单保存并审核服务
            holderV14 = saveAndAuditservice.saveAndAuditBillWithTrans(sgPhyInResultBillSaveRequests);

            List<ReturnSgPhyInResult> data = holderV14.getData();
            if (CollectionUtils.isNotEmpty(data)) {
                for (ReturnSgPhyInResult datum : data) {
                    AssertUtils.isTrue(datum.getCode() == ResultCode.SUCCESS, datum.getMessage());
                }
            }


            //生成装箱明细表的拣货箱箱号
            SgBInPickorderTeusItemMapper teusItemMapper = ApplicationContextHandle.getBean(SgBInPickorderTeusItemMapper.class);
            List<SgBInPickorderTeusItem> sgBInPickorderTeusItems = teusItemMapper.selectList(new QueryWrapper<SgBInPickorderTeusItem>().lambda()
                    .eq(SgBInPickorderTeusItem::getSgBInPickorderId, inPickOrderId));
            List<String> pickOutTeusIdList = sgBInPickorderTeusItems.stream().map(SgBInPickorderTeusItem::getPickInTeusId).collect(Collectors.toList());

            if (CollectionUtils.isNotEmpty(pickOutTeusIdList)) {
                JSONObject obj = new JSONObject();
                obj.put(SgConstants.SG_B_IN_PICKORDER_TEUS_ITEM.toUpperCase(), pickOrder);
                for (String pickOutTeusId : pickOutTeusIdList) {
                    SgBInPickorderTeusItem updateItem = new SgBInPickorderTeusItem();
                    String billNo = SequenceGenUtil.generateSquence(SgOutConstants.SQE_SG_B_IN_PICK_TEUS_ITEM, obj, request.getLoginUser().getLocale(), false);
//                    String billNo = "测试2";
                    updateItem.setPickInBillNo(billNo);
                    teusItemMapper.update(updateItem, new UpdateWrapper<SgBInPickorderTeusItem>().lambda()
                            .eq(SgBInPickorderTeusItem::getPickInTeusId, pickOutTeusId)
                            .eq(SgBInPickorderTeusItem::getSgBInPickorderId, inPickOrderId));
                }
            }

            // 更新入库拣货单主表信息
            SgBInPickorder updatePickOrder = new SgBInPickorder();
            updatePickOrder.setBillStatus(SgOutConstantsIF.PICK_ORDER_STATUS_AUDIT);
            updatePickOrder.setStatus(SgOutConstantsIF.PICK_ORDER_STATUS_AUDIT);
            updatePickOrder.setModifiername(loginUser.getName());
            updatePickOrder.setModifieddate(new Date());
            updatePickOrder.setId(pickOrder.getId());
            updatePickOrder.setStatusId(loginUser.getId());
            updatePickOrder.setStatusName(loginUser.getName());
            updatePickOrder.setStatusEname(loginUser.getEname());
            updatePickOrder.setStatusTime(new Date());
            StorageESUtils.setBModelDefalutDataByUpdate(updatePickOrder, loginUser);
            inPickOrderMapper.updateById(updatePickOrder);

        } catch (Exception e) {
            log.error(this.getClass().getName() + " 入库拣货单调用入库结果单审核错误,{}", e.getMessage(), e);
            AssertUtils.logAndThrow(e.getMessage());
        } finally {
            try {
                redisTemplate.delete(lockKsy);
            } catch (Exception e) {
                log.error(this.getClass().getName() + " redis锁释放失败，请联系管理员！lockKsy：" + lockKsy);
                AssertUtils.logAndThrow("redis锁释放失败，请联系管理员！");
            }
        }

        holderV14.setCode(ResultCode.SUCCESS);
        holderV14.setMessage("审核成功");
        return holderV14;
    }

    /**
     * 关账控制
     *
     * @param locale   国际化
     * @param destCode 收货店仓编码
     */
    private void checkClose(Locale locale, String destCode) {
        //2020.02.25 关账卡控
        AcBOperateCloseCtrlCmd acBOperateCloseCtrlCmd = (AcBOperateCloseCtrlCmd)
                ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                        AcBOperateCloseCtrlCmd.class.getName(), "ac", "1.0");
        AcBOperateCloseCtrlRequest acBOperateCloseCtrlRequest = new AcBOperateCloseCtrlRequest();
        acBOperateCloseCtrlRequest.setEcode(destCode);
        acBOperateCloseCtrlRequest.setCloseDate(new Date());
        ValueHolderV14<AcBOperateClose> acBOperateCloseValueHolderV14 = null;
        try {
            acBOperateCloseValueHolderV14 = acBOperateCloseCtrlCmd.queryAcBOperateCloseCtrl(acBOperateCloseCtrlRequest);
        } catch (Exception e) {
            log.error("acBOperateCloseCtrlCmd.queryAcBOperateCloseCtrl. ReceiveParams:params:{};"
                    + StorageUtils.getExceptionMsg(e));
            AssertUtils.logAndThrow("调用查询关账服务异常:" + StorageUtils.getExceptionMsg(e), locale);
        }
        if (!acBOperateCloseValueHolderV14.isOK()) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String preDay = sdf.format(acBOperateCloseValueHolderV14.getData().getCloseDate());
            AssertUtils.logAndThrow("此营运中心关账日期为" + preDay + "，不允许再操作单据！", locale);
        }
    }

    /**
     * 如果收发货门店的经营类型为集团直营，当差异数量不为0时，
     * 则提示：“入库数量不等于调拨数量，不允许审核！”
     *
     * @param locale         国际化
     * @param transferBillNo 调拨单单据编号
     * @param itemList       明细
     */
    private void checkStoreAndDiffQty(Locale locale, String transferBillNo, List<SgBInPickorderItem> itemList) {

        // 如果入库数量不等于调拨数量，不允许审核
        List<SgBInPickorderItem> pickOrderItems = itemList.stream().filter(item ->
                item.getQtyDiff().compareTo(BigDecimal.ZERO) > 0).collect(Collectors.toList());
        if (CollectionUtils.isNotEmpty(pickOrderItems)) {

            // 如果收发货门店的经营类型为集团直营
            InUtil inUtil = ApplicationContextHandle.getBean(InUtil.class);
            boolean isDirectly = inUtil.checkStoreIsDirectly(locale, transferBillNo);
            if (isDirectly) {
                AssertUtils.logAndThrow("入库数量不等于调拨数量，不允许审核！", locale);
            }
        }
    }

}
