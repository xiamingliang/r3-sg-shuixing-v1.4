package com.jackrain.nea.sg.in.services;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.in.common.SgInNoticeConstants;
import com.jackrain.nea.sg.in.mapper.SgBPhyInNoticesMapper;
import com.jackrain.nea.sg.in.model.request.SgPhyInResultBillAuditRequest;
import com.jackrain.nea.sg.in.model.request.SgPhyInResultBillSaveRequest;
import com.jackrain.nea.sg.in.model.result.ReturnSgPhyInResult;
import com.jackrain.nea.sg.in.model.table.SgBPhyInNotices;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Slf4j
@Component
public class SgPhyInResultSaveAndAuditService {


    @Autowired
    private SgPhyInResultSaveService sgPhyInResultSaveService;

    @Autowired
    private SgPhyInResultAuditService sgPhyInResultAuditService;


    @Transactional(rollbackFor = Exception.class)
    public ValueHolderV14<List<ReturnSgPhyInResult>> saveAndAuditBillWithTrans(List<SgPhyInResultBillSaveRequest> billList) {
        SgPhyInResultSaveAndAuditService saveAndAuditService = ApplicationContextHandle.getBean(SgPhyInResultSaveAndAuditService.class);
        ValueHolderV14<List<ReturnSgPhyInResult>> result = saveAndAuditService.saveAndAuditBill(billList);
        if (!result.isOK()) {
            AssertUtils.logAndThrow(result.getMessage());
        }
        return result;
    }

    /**
     * 新增入库结果单并审核结果单
     *
     * @param billList
     * @return
     */
    public ValueHolderV14<List<ReturnSgPhyInResult>> saveAndAuditBill(List<SgPhyInResultBillSaveRequest> billList) {
        if (log.isDebugEnabled()) {
            log.debug("Start SgPhyInResultSaveAndAuditService.saveAndAuditBill. ReceiveParams:billList=" + JSONObject.toJSONString(billList) + ";");
        }
        SgBPhyInNoticesMapper noticesMapper = ApplicationContextHandle.getBean(SgBPhyInNoticesMapper.class);
        if (CollectionUtils.isEmpty(billList)) {
            throw new NDSException(Resources.getMessage("传入参数为空!"));
        }
        JSONArray ids = new JSONArray();
        String errMsg = "";
        for (SgPhyInResultBillSaveRequest bill : billList) {
            //判断通知单是否存在
            Long noticesId = bill.getSgBPhyInResultSaveRequest().getSgBPhyInNoticesId();
            SgBPhyInNotices notices = noticesMapper.selectById(noticesId);
            if (notices == null) {
                errMsg = "入库通知单不存在!";
                break;
            }
            //判断是否为待入库或者部分入库
            Integer billStatus = notices.getBillStatus();
            if (billStatus == SgInNoticeConstants.BILL_STATUS_IN_PENDING || billStatus == SgInNoticeConstants.BILL_STATUS_IN_PART) {
                ValueHolderV14 result = sgPhyInResultSaveService.saveSgBPhyInResult(bill);
                if (result.isOK()) {
                    ids.add(result.getData());
                }
            } else {
                errMsg = "入库通知单状态不是待入库或部分入库，不允许新增结果单!";
                break;
            }
        }
        if (CollectionUtils.isEmpty(ids)) {
            throw new NDSException(Resources.getMessage("新增入库结果单失败!" + errMsg));
        }
        SgPhyInResultBillAuditRequest billAuditRequest = new SgPhyInResultBillAuditRequest();
        billAuditRequest.setIds(ids);
        billAuditRequest.setLoginUser(billList.get(0).getLoginUser());
        billAuditRequest.setPickOrderId(billList.get(0).getPackOrderId());
        Boolean isOneClickLirary = Optional.ofNullable(billList.get(0).getIsOneClickLibrary()).orElse(false);
        billAuditRequest.setIsOneClickLibrary(isOneClickLirary);
        ValueHolderV14<List<ReturnSgPhyInResult>> result = sgPhyInResultAuditService.auditSgPhyInResult(billAuditRequest);
        if (log.isDebugEnabled()) {
            log.debug("Finish SgPhyInResultSaveAndAuditService.saveAndAuditBill. ReturnResults:"
                    + result.toJSONObject() + ";");
        }
        return result;
    }
}
