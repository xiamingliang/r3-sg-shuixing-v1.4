package com.jackrain.nea.sg.out.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.out.mapper.SgBPhyOutNoticesMapper;
import com.jackrain.nea.sg.out.model.request.SgPhyOutNoticesQueryRequest;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutNotices;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author zhu lin yu
 * @since 2019-10-23
 * create at : 2019-10-23 19:52
 */
@Slf4j
@Component
public class SgPhyOutNoticesMultiConditionQueryService {

    public ValueHolderV14<List<SgBPhyOutNotices>> queryOutNotices(SgPhyOutNoticesQueryRequest request) {
        long start = System.currentTimeMillis();
        ValueHolderV14<List<SgBPhyOutNotices>> valueHolderV14 = new ValueHolderV14<>();

        if (log.isDebugEnabled()) {
            log.debug("Start SgPhyOutNoticesMultiConditionQueryService.queryOutNotices. ReceiveParams:requests:{}"
                    , JSONObject.toJSONString(request));
        }

        if (request.getSourceBillType() == null && CollectionUtils.isEmpty(request.getBillStatusList())
                && request.getCpCPhyWarehouseId() == null && request.getStartTime() == null
                && request.getEndTime() == null && StringUtils.isEmpty(request.getBillNo())) {
            valueHolderV14.setCode(ResultCode.FAIL);
            valueHolderV14.setMessage("请传入查询参数");
            return valueHolderV14;
        }
        PageHelper.startPage(request.getPageNum(), request.getPageSize());

        //根据条件查询出库通知单
        SgBPhyOutNoticesMapper sgBPhyOutNoticesMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesMapper.class);
        List<SgBPhyOutNotices> sgBPhyOutNoticesList = sgBPhyOutNoticesMapper.selectList(new QueryWrapper<SgBPhyOutNotices>().lambda()
                .eq(request.getSourceBillType() != null,SgBPhyOutNotices::getSourceBillType,request.getSourceBillType())
                .in(CollectionUtils.isNotEmpty(request.getBillStatusList()),SgBPhyOutNotices::getBillStatus,request.getBillStatusList())
                .eq(request.getCpCPhyWarehouseId() != null,SgBPhyOutNotices::getCpCPhyWarehouseId,request.getCpCPhyWarehouseId())
                .like(StringUtils.isNotEmpty(request.getBillNo()),SgBPhyOutNotices::getBillNo,request.getBillNo())
                .gt(request.getStartTime() != null && request.getEndTime() == null,SgBPhyOutNotices::getModifieddate, request.getStartTime())
                .between(request.getStartTime() != null && request.getEndTime() != null,SgBPhyOutNotices::getModifieddate, request.getStartTime(), request.getEndTime())
        .orderByDesc(SgBPhyOutNotices::getModifieddate));

        PageInfo<SgBPhyOutNotices> resultPage = new PageInfo<>(sgBPhyOutNoticesList);
        List<SgBPhyOutNotices> list = resultPage.getList();
        valueHolderV14.setCode(ResultCode.SUCCESS);
        valueHolderV14.setMessage("查询成功");
        valueHolderV14.setData(list);

        if (log.isDebugEnabled()) {
            log.debug("Finsh SgPhyOutNoticesMultiConditionQueryService.queryOutNotices. Result:valueHolderV14:{},spend time:{}ms"
                    , JSONObject.toJSONString(valueHolderV14), System.currentTimeMillis() - start);
        }

        return valueHolderV14;
    }
}
