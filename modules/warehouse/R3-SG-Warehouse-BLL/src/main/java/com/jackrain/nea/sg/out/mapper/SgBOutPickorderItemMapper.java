package com.jackrain.nea.sg.out.mapper;

import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.out.model.table.SgBOutPickorderItem;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SgBOutPickorderItemMapper extends ExtentionMapper<SgBOutPickorderItem> {
}