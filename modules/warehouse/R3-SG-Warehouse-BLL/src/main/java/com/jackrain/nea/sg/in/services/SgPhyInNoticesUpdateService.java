package com.jackrain.nea.sg.in.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.in.common.SgInNoticeConstants;
import com.jackrain.nea.sg.in.mapper.SgBPhyInNoticesMapper;
import com.jackrain.nea.sg.in.model.request.SgPhyInNoticesBillUpdateRequest;
import com.jackrain.nea.sg.in.model.table.SgBPhyInNotices;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author: 舒威
 * @since: 2019/11/20
 * create at : 2019/11/20 10:19
 */
@Slf4j
@Component
public class SgPhyInNoticesUpdateService {

    @Autowired
    private SgBPhyInNoticesMapper noticesMapper;

    public ValueHolderV14 updateSgBPhyInNoticesByPick(SgPhyInNoticesBillUpdateRequest request) {
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ". ReceiveParams:request:{};", JSONObject.toJSONString(request));
        }
        ValueHolderV14 vh = new ValueHolderV14(ResultCode.SUCCESS, "success");
        AssertUtils.notNull(request, "参数不能为空!");
        AssertUtils.notNull(request.getLoginUser(), "用户不能为空!");
        AssertUtils.notEmpty(request.getNoticesBillNos(), "通知单号不能为空!");
        SgBPhyInNotices notices=new SgBPhyInNotices();
        notices.setPickStatus(SgInNoticeConstants.PICK_STATUS_WAIT);
        notices.setModifierename(request.getLoginUser().getEname());
        StorageESUtils.setBModelDefalutDataByUpdate(notices,request.getLoginUser());
        int i = noticesMapper.update(notices, new UpdateWrapper<SgBPhyInNotices>().lambda().in(SgBPhyInNotices::getBillNo, request.getNoticesBillNos()));
        log.info("成功更新通知单为未拣货条数:" + i + "条！");
        return vh;
    }
}
