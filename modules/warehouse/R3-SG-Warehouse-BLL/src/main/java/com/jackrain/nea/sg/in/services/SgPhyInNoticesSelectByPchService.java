package com.jackrain.nea.sg.in.services;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.Feature;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Lists;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.common.R3ParamConstants;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.in.mapper.SgBPhyInNoticesMapper;
import com.jackrain.nea.sg.in.model.table.SgBPhyInNotices;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferMapper;
import com.jackrain.nea.sg.transfer.model.table.ScBTransfer;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;

@Slf4j
@Component
public class SgPhyInNoticesSelectByPchService {
    /**
     * 根据批次查询入库通知单
     * @param hashMap
     * @return
     */
    public ValueHolder getSgPhyInNoticesByPch(HashMap hashMap) {
        log.info(this.getClass().getName()+":入参hashMap="+hashMap);
        ValueHolder result=new ValueHolder();
        result.put(R3ParamConstants.CODE,ResultCode.SUCCESS);
        result.put(R3ParamConstants.MESSAGE,"查询成功!");
        result.put(R3ParamConstants.DATA,"");
        ScBTransferMapper transferMapper=ApplicationContextHandle.getBean(ScBTransferMapper.class);
        SgBPhyInNoticesMapper noticesMapper=ApplicationContextHandle.getBean(SgBPhyInNoticesMapper.class);

        JSONObject param = JSON.parseObject(JSON.toJSONStringWithDateFormat(hashMap.get("param"), "yyyy-MM-dd HH-mm-ss", SerializerFeature.WriteMapNullValue), Feature.OrderedField);
       if(param==null){
           result.put(R3ParamConstants.CODE,ResultCode.FAIL);
           result.put(R3ParamConstants.MESSAGE,"请输入参数!");
           return result;
       }
       if(!param.containsKey("value")){
           result.put(R3ParamConstants.CODE,ResultCode.FAIL);
           result.put(R3ParamConstants.MESSAGE,"请输入正确参数!");
           return result;
       }
       JSONObject obj=param.getJSONObject("value");
       if(obj!=null){
           if(!obj.containsKey("val")){
               result.put(R3ParamConstants.CODE,ResultCode.FAIL);
               result.put(R3ParamConstants.MESSAGE,"请输入正确参数!");
               return result;
           }
       }
       String pch=obj.getString("val");
       if (pch==null){
           result.put(R3ParamConstants.CODE,ResultCode.FAIL);
           result.put(R3ParamConstants.MESSAGE,"批次号为空!");
           return result;
       }
       //先查询调拨单,获取来源单据 编号
        QueryWrapper queryWrapper = new QueryWrapper<ScBTransfer>();
        queryWrapper.eq("batch_no", pch);
        queryWrapper.eq("isactive", SgConstants.IS_ACTIVE_Y);
        List<ScBTransfer> transferList=transferMapper.selectList(queryWrapper);
        if(CollectionUtils.isEmpty(transferList)){
            result.put(R3ParamConstants.CODE,ResultCode.FAIL);
            result.put(R3ParamConstants.MESSAGE,"该批次单据不存在!");
            return result;
        }
        List<String> sourceBillNoList= Lists.newArrayList();
        for(ScBTransfer transfer:transferList){
            sourceBillNoList.add(transfer.getBillNo());
        }
        QueryWrapper queryNoticesWrapper = new QueryWrapper<SgBPhyInNotices>();
        queryNoticesWrapper.in("source_bill_no",sourceBillNoList);
        queryNoticesWrapper.eq("isactive", SgConstants.IS_ACTIVE_Y);
        List<SgBPhyInNotices> noticesList=noticesMapper.selectList(queryNoticesWrapper);
        if(CollectionUtils.isEmpty(noticesList)){
            result.put(R3ParamConstants.CODE,ResultCode.FAIL);
            result.put(R3ParamConstants.MESSAGE,"入库通知单不存在!");
            return result;
        }
        List<String>  list=Lists.newArrayList();
        noticesList.forEach(notices->{
            list.add(notices.getBillNo());
        });
        String billNoStr=StringUtils.join(list,",");
        result.put(R3ParamConstants.CODE,ResultCode.SUCCESS);
        result.put(R3ParamConstants.MESSAGE,"查询成功!");
        result.put(R3ParamConstants.DATA,billNoStr);
        log.info(this.getClass().getName()+":出参result="+result);
        return result;
    }
}
