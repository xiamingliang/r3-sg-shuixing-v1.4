package com.jackrain.nea.sg.inv.services;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.redis.config.CusRedisTemplate;
import com.jackrain.nea.redis.util.RedisOpsUtil;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.mapper.SgBAdjustPropMapper;
import com.jackrain.nea.sg.basic.model.table.SgBAdjustProp;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.inv.constants.SgInvConstants;
import com.jackrain.nea.sg.inv.mapper.ScBInventoryImpItemMapper;
import com.jackrain.nea.sg.inv.mapper.ScBInventoryMapper;
import com.jackrain.nea.sg.inv.model.request.ScInvProfitLossMarginQueryCmdRequest;
import com.jackrain.nea.sg.inv.model.result.ScInvProfitLossGenarateQueryResult;
import com.jackrain.nea.sg.inv.model.table.ScBInventory;
import com.jackrain.nea.sg.inv.model.table.ScBInventoryImpItem;
import com.jackrain.nea.sg.inv.model.table.ScBPrePlUnfshBill;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.DateUtil;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @author 舒威
 * @since 2019/12/4
 * create at : 2019/12/4 17:42
 */
@Slf4j
@Component
public class ScInvProfitLossGenarateService {

    @Autowired
    private ScInvProfitLossTotalService totalService;

    @Autowired
    private ScInvProfitLossUnfinishQueryService unfinishQueryService;

    @Autowired
    private ScInvProfitLossGenarateAsyncService asyncService;

    @Autowired
    private ScBInventoryMapper inventoryMapper;

    @Autowired
    private ScBInventoryImpItemMapper inventoryItemMapper;

    /**
     * 生成盈亏按钮渲染信息
     */
    public ValueHolderV14<ScInvProfitLossGenarateQueryResult> queryProfitLossGenarateInfo(ScInvProfitLossMarginQueryCmdRequest model) {
        //合计盈亏数量
        ValueHolderV14<ScInvProfitLossGenarateQueryResult> result = totalService.queryProfitLossTotalInventory(model);
        if (log.isDebugEnabled()) {
            log.debug("Finish ScInvProfitLossGenarateService.queryProfitLossTotalInventory. Return:results:{}"
                    , JSONObject.toJSONString(result));
        }
        if (result.isOK()) {
            ScInvProfitLossGenarateQueryResult queryResult = result.getData();
            //查询调整性质
            SgBAdjustPropMapper adjustPropMapper = ApplicationContextHandle.getBean(SgBAdjustPropMapper.class);
            List<SgBAdjustProp> adjustProps = adjustPropMapper.selectList(new QueryWrapper<>());
            queryResult.setAdjustProps(adjustProps);
            //盘点性质
            queryResult.setInventoryProps(Lists.newArrayList(SgInvConstants.INVENTORY_PROPS_PROFIT,
                    SgInvConstants.INVENTORY_PROPS_LOSS, SgInvConstants.INVENTORY_PROPS_DIFF));
            result.setData(queryResult);
        }
        return result;
    }

    /**
     * 生成盈亏
     */
    public ValueHolderV14 genarateProfitLoss(ScInvProfitLossMarginQueryCmdRequest model) {
        if (log.isDebugEnabled()) {
            log.debug("Start ScInvProfitLossGenarateService.genarateProfitLoss. Receive:request:{}", JSONObject.toJSONString(model));
        }
        ValueHolderV14 result = new ValueHolderV14(ResultCode.SUCCESS, "success");
        User user = model.getLoginUser();
        AssertUtils.notNull(user, "当前用户未登录！");
        Locale locale = user.getLocale();
        AssertUtils.notNull(model.getWarehouseId(), "请选择盘点店仓！", locale);
        if (model.getGenarateInfo() == null || model.getGenarateInfo().getAdjProp() == null || model.getGenarateInfo().getInventoryProp() == null) {
            AssertUtils.logAndThrow("调整性质或盘点性质不能为空！", locale);
        }

        CusRedisTemplate<String, String> redisTemplate = RedisOpsUtil.getObjRedisTemplate();
        String locksKy = "SgProfitLossGenarate:" + model.getWarehouseId() + ":" +
                model.getGenarateInfo().getAdjProp() + ":" +
                model.getGenarateInfo().getInventoryProp() + ":" +
                DateUtil.format(model.getInventoryDate(), "yyyyMMdd");

        if (log.isDebugEnabled()) {
            log.debug("SgProfitLossGenarate. LocksKey:{};", locksKy);
        }

        Boolean blnCanInit = redisTemplate.opsForValue().setIfAbsent(locksKy, "init");
        if (blnCanInit != null && blnCanInit) {
            redisTemplate.expire(locksKy, 60, TimeUnit.SECONDS);
        } else {
            result.setCode(ResultCode.FAIL);
            result.setMessage("正在生成盈亏中，请稍后重试！");
            return result;
        }

        //是否存在未完成单据
        //默认查询1000条盘点、库存调整类型的未盈亏盘点单
        try {
            List<String> unfshBillType = Lists.newArrayList(SgInvConstants.SC_B_INVENTORY, SgConstants.SG_B_PHY_ADJUST);
            model.getGenarateInfo().setUnfshBillType(unfshBillType);
            model.getPage().setPageSize(SgConstants.SG_NORMAL_MAX_QUERY_PAGE_SIZE);
            ValueHolderV14<PageInfo<ScBPrePlUnfshBill>> unfinishPage = unfinishQueryService.queryUnfinishProfitLoss(model);
            if (unfinishPage.isOK()) {
                List<ScBPrePlUnfshBill> unfshBills = unfinishPage.getData().getList();
                if (CollectionUtils.isNotEmpty(unfshBills)) {
                    List<String> unfishBillNos = unfshBills.stream().map(ScBPrePlUnfshBill::getBillNo).collect(Collectors.toList());
                    AssertUtils.logAndThrow("存在未完成单据盘点单或者库存调整单，请先处理!未完成单据" + unfishBillNos);
                }
            }
            //查询未盈亏盘点单号
            HashMap<String, String> inventoryMap = queryInventoryBillNos(model);

            //生成盈亏单、库存调整单
            asyncService.genarateProfitLoss(model, inventoryMap);
        } catch (Exception e) {
            AssertUtils.logAndThrow(e.getMessage());
        } finally {
            redisTemplate.delete(locksKy);
        }
        return result;
    }

    private HashMap<String, String> queryInventoryBillNos(ScInvProfitLossMarginQueryCmdRequest model) {
        //查询未盈亏盘点单
        List<ScBInventory> inventoryList = inventoryMapper.selectList(new QueryWrapper<ScBInventory>().lambda()
                .eq(model.getWarehouseId() != null, ScBInventory::getCpCPhyWarehouseId, model.getWarehouseId())
                .eq(model.getInventoryType() != null, ScBInventory::getInventoryType, model.getInventoryType())
                .eq(model.getInventoryDate() != null, ScBInventory::getInventoryDate, model.getInventoryDate())
                .eq(ScBInventory::getPolStatus, SgInvConstants.PAND_UNPOL)
                .eq(ScBInventory::getIsactive, SgConstants.IS_ACTIVE_Y));
        AssertUtils.notEmpty(inventoryList, "所选盘点日期不存在盘点单！");

        JSONArray errArr = new JSONArray();
        for (int i = 0; i < inventoryList.size(); i++) {
            ScBInventory inventory = inventoryList.get(i);
            Integer cnt = inventoryItemMapper.selectCount(new QueryWrapper<ScBInventoryImpItem>().lambda()
                    .eq(ScBInventoryImpItem::getScBInventoryId, inventory.getId()));
            if (cnt != null && cnt < 1) {
                errArr.add(inventory.getBillNo());
            }
        }

        if (CollectionUtils.isNotEmpty(errArr)) {
            String join = StringUtils.join(errArr, ",");
            AssertUtils.logAndThrow("盘点单[" + join + "],明细为空，不允许预盈亏！");
        }

        List<String> billNoList = inventoryList.stream().map(ScBInventory::getBillNo).collect(Collectors.toList());
        String billNos = StringUtils.join(billNoList, ",");
        HashMap<String, String> retMap = Maps.newHashMap();
        retMap.put("billNos", billNos);
        retMap.put("warehouseEcode", inventoryList.get(0).getCpCPhyWarehouseEcode());
        retMap.put("warehouseEname", inventoryList.get(0).getCpCPhyWarehouseEname());
        return retMap;
    }

}