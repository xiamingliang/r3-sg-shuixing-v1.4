package com.jackrain.nea.sg.phyadjust.services;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Lists;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.cpext.model.table.CpCustomer;
import com.jackrain.nea.ext.sequence.util.SequenceGenUtil;
import com.jackrain.nea.model.util.ModelUtil;
import com.jackrain.nea.oc.basic.common.OcBasicConstants;
import com.jackrain.nea.psext.model.table.PsCPro;
import com.jackrain.nea.sg.basic.common.R3ParamConstants;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.config.SgStorageBoxConfig;
import com.jackrain.nea.sg.basic.mapper.CpCPhyWarehouseMapper;
import com.jackrain.nea.sg.basic.mapper.CpStoreMapper;
import com.jackrain.nea.sg.basic.mapper.SgBAdjustPropMapper;
import com.jackrain.nea.sg.basic.model.result.SgR3BaseResult;
import com.jackrain.nea.sg.basic.model.table.CpCPhyWarehouse;
import com.jackrain.nea.sg.basic.model.table.CpCStore;
import com.jackrain.nea.sg.basic.model.table.SgBAdjustProp;
import com.jackrain.nea.sg.basic.utils.R3ParamUtil;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.out.utils.SgWarehouseESUtils;
import com.jackrain.nea.sg.phyadjust.common.SgPhyAdjustConstants;
import com.jackrain.nea.sg.phyadjust.mapper.SgBPhyAdjustImpItemMapper;
import com.jackrain.nea.sg.phyadjust.mapper.SgBPhyAdjustItemMapper;
import com.jackrain.nea.sg.phyadjust.mapper.SgBPhyAdjustMapper;
import com.jackrain.nea.sg.phyadjust.model.request.SgPhyAdjustBillSaveRequest;
import com.jackrain.nea.sg.phyadjust.model.request.SgPhyAdjustImpItemRequest;
import com.jackrain.nea.sg.phyadjust.model.request.SgPhyAdjustItemSaveRequest;
import com.jackrain.nea.sg.phyadjust.model.request.SgPhyAdjustSaveRequest;
import com.jackrain.nea.sg.phyadjust.model.result.FillSkuResult;
import com.jackrain.nea.sg.phyadjust.model.table.SgBPhyAdjust;
import com.jackrain.nea.sg.phyadjust.model.table.SgBPhyAdjustImpItem;
import com.jackrain.nea.sg.phyadjust.model.table.SgBPhyAdjustItem;
import com.jackrain.nea.sg.phyadjust.utils.FillSkuInfoUtil;
import com.jackrain.nea.sg.rpc.CpRpcCustomerService;
import com.jackrain.nea.sg.rpc.CpRpcStoreService;
import com.jackrain.nea.sg.rpc.PsRpcCproService;
import com.jackrain.nea.sg.send.utils.SgStoreUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.utils.AssertUtils;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.query.QuerySession;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author csy
 * Date: 2019/4/29
 * Description: 库存调整单-实体仓-保存
 */
@Slf4j
@Component
public class SgPhyAdjustSaveService {

    @Autowired
    private SgBPhyAdjustMapper mapper;

    @Autowired
    private SgBPhyAdjustItemMapper itemMapper;

    @Autowired
    private SgBPhyAdjustImpItemMapper impItemMapper;

    @Autowired
    private SgStorageBoxConfig storageBoxConfig;

    @Autowired
    private SgWarehouseESUtils warehouseESUtils;


    @Autowired
    private CpRpcCustomerService cpRpcCustomerQuery;

    @Autowired
    private CpRpcStoreService cpRpcStoreQuery;

    @Autowired
    private PsRpcCproService psRpcCproQuery;

    /**
     * r3框架入参
     *
     * @param session session
     */
    @Transactional(rollbackFor = Exception.class)
    ValueHolder save(QuerySession session) {
        SgPhyAdjustBillSaveRequest request = R3ParamUtil.parseSaveObject(session, SgPhyAdjustBillSaveRequest.class);
        return R3ParamUtil.convertV14WithResult(save(request, true));
    }


    /**
     * 标准入参
     *
     * @param request request
     * @param isR3    是否是r3框架 页面传参
     */
    @Transactional(rollbackFor = Exception.class)
    public ValueHolderV14<SgR3BaseResult> save(SgPhyAdjustBillSaveRequest request, boolean isR3) {
        if (log.isDebugEnabled()) {
            log.debug("Start SgPhyAdjustSaveService.save. ReceiveParams:SgPhyAdjustBillSaveRequest="
                    + JSONObject.toJSONString(request) + ";");
        }
        //明细 是否全部回滚
        boolean isRollBackAll = !isR3;
        //======参数校验======
        isR3 = checkParam(request, isR3);
        //====保存===
        Long objId = request.getObjId();
        User user = request.getLoginUser();
        Locale locale = user.getLocale();
        //页面插入时
        boolean isInsert = objId == null || objId < 0;
        //接口插入时
        if (!isR3 && isInsert) {
            Integer total = mapper.selectCount(new QueryWrapper<SgBPhyAdjust>().lambda()
                    .eq(SgBPhyAdjust::getSourceBillType, request.getPhyAdjust().getSourceBillType())
                    .eq(SgBPhyAdjust::getSourceBillId, request.getPhyAdjust().getSourceBillId())
                    .eq(request.getPhyAdjust().getSgBAdjustPropId() != null, SgBPhyAdjust::getSgBAdjustPropId, request.getPhyAdjust().getSgBAdjustPropId())
                    .eq(SgBPhyAdjust::getIsactive, SgConstants.IS_ACTIVE_Y));
            isInsert = total == 0;
            AssertUtils.cannot(total > 1, "存在重复的单据", locale);
        }
        if (isInsert) {
            return insert(user, locale, request, isR3, isRollBackAll);
        } else {
            return update(objId, user, locale, request, isR3, isRollBackAll);
        }
    }


    /**
     * 主表插入(明细只有插入)
     *
     * @param isR3    是否r3框架入参
     * @param user    用户
     * @param locale  国际化
     * @param request 封装库存调整单对象
     */
    private ValueHolderV14<SgR3BaseResult> insert(User user, Locale locale, SgPhyAdjustBillSaveRequest request, boolean isR3, boolean isRollBackAll) {

        SgBPhyAdjust adjust = new SgBPhyAdjust();
        BeanUtils.copyProperties(request.getPhyAdjust(), adjust);
        if (StringUtils.isEmpty(adjust.getCpCPhyWarehouseEcode())) {
            CpCPhyWarehouseMapper warehouseMapper = ApplicationContextHandle.getBean(CpCPhyWarehouseMapper.class);
            CpCPhyWarehouse warehouse = warehouseMapper.selectById(adjust.getCpCPhyWarehouseId());
            AssertUtils.notNull(warehouse, "不存在的仓库信息!", locale);
            adjust.setCpCPhyWarehouseEcode(warehouse.getEcode());
            adjust.setCpCPhyWarehouseEname(warehouse.getEname());
        }

        //获取主表id
        Long id = ModelUtil.getSequence(SgConstants.SG_B_PHY_ADJUST);
        //插入库存调整单明细数据
        JSONArray errArr = null;

        if (storageBoxConfig.getBoxEnable()) {
            //库存调整单录入明细对象
            List<SgBPhyAdjustImpItem> impItems = createInsertItemBox(request.getImpItems(), user, id,adjust);
            // 压缩明细 去重插入
            if (CollectionUtils.isNotEmpty(impItems)) {
                mergeInsertBox(impItems, id, user, isRollBackAll);
            }
            BigDecimal adjustFee = impItems.stream().map(SgBPhyAdjustImpItem::getAdjustFee).reduce(BigDecimal.ZERO, BigDecimal::add);
            adjust.setAdjustFee(adjustFee);
        } else {
            //库存调整单明细插入
            List<SgBPhyAdjustItem> items = createInsertItem(request.getItems(), user, id);
            // 压缩明细 去重插入
            if (CollectionUtils.isNotEmpty(items)) {
                errArr = mergeInsert(items, null, user, isRollBackAll);
            }
            //推送ES
            List<SgBPhyAdjustItem> adjustItems = itemMapper.selectList(new QueryWrapper<SgBPhyAdjustItem>()
                    .eq(SgPhyAdjustConstants.SG_B_PHY_ADJUST_ID, id));
            String index = SgConstants.SG_B_PHY_ADJUST;
            String type = SgConstants.SG_B_PHY_ADJUST_ITEM;
            String parentKey = SgPhyAdjustConstants.SG_B_PHY_ADJUST_ID;
            StorageESUtils.pushESBModelByUpdate(adjust, adjustItems, adjust.getId(), null, index, index, type, parentKey, SgBPhyAdjust.class, SgBPhyAdjustItem.class, false);

            //totQty 汇总
            BigDecimal totQty = request.getItems().stream().map(SgPhyAdjustItemSaveRequest::getQty).reduce(BigDecimal.ZERO, BigDecimal::add);
            adjust.setTotQty(totQty);
        }

        if (adjust.getBillDate() == null) {
            adjust.setBillDate(new Date());
        }

        //单据编号
        if (StringUtils.isEmpty(adjust.getBillNo())) {
            JSONObject fix = new JSONObject();
            fix.put(SgConstants.SG_B_PHY_ADJUST, adjust);
           adjust.setBillNo(SequenceGenUtil.generateSquence(SgPhyAdjustConstants.PHY_ADJUST_SEQ, fix, user.getLocale(), false));
            //adjust.setBillNo("cehsi123456");
        }

        adjust.setId(id);
        adjust.setBillStatus(SgPhyAdjustConstants.STATUS_N);
        StorageESUtils.setBModelDefalutData(adjust, user);
        adjust.setOwnerename(user.getEname());
        adjust.setModifierename(user.getEname());
        mapper.insert(adjust);

        //推送es
        warehouseESUtils.pushESByPhyAdjust(id, true, false, null, mapper, itemMapper);

        //构造返回结果
        ValueHolderV14<SgR3BaseResult> holderV14 = new ValueHolderV14<>(ResultCode.SUCCESS, SgConstants.MESSAGE_STATUS_SUCCESS);
        SgR3BaseResult result = new SgR3BaseResult();
        JSONObject resultData = new JSONObject();
        resultData.put(R3ParamConstants.OBJID, id);
        resultData.put(R3ParamConstants.TABLENAME, SgConstants.SG_B_PHY_ADJUST);
        result.setDataJo(resultData);
        holderV14.setData(result);
        if (CollectionUtils.isNotEmpty(errArr)) {
            SgR3BaseResult baseResult = new SgR3BaseResult();
            baseResult.setDataArr(errArr);
            holderV14.setData(baseResult);
            holderV14.setCode(ResultCode.FAIL);
            holderV14.setMessage("调整单新增失败:" + errArr.size() + "条条码信息有误!");
        }
        return holderV14;
    }


    /**
     * 主表更新(明细插入/更新)
     *
     * @param isR3    是否r3框架入参
     * @param user    用户
     * @param locale  国际化
     * @param request 封装库存调整单对象
     * @param objId   主表id
     */
    private ValueHolderV14<SgR3BaseResult> update(Long objId, User user, Locale locale, SgPhyAdjustBillSaveRequest request, boolean isR3, boolean isRollBackAll) {

        SgPhyAdjustSaveRequest adjRequest = request.getPhyAdjust();
        SgBPhyAdjust origAdj = isR3 ? mapper.selectById(objId) :
                mapper.selectOne(new QueryWrapper<SgBPhyAdjust>().lambda()
                        .eq(SgBPhyAdjust::getSourceBillId, adjRequest.getSourceBillId())
                        .eq(SgBPhyAdjust::getSourceBillType, adjRequest.getSourceBillType())
                        .eq(adjRequest.getSgBAdjustPropId() != null, SgBPhyAdjust::getSgBAdjustPropId, adjRequest.getSgBAdjustPropId())
                        .eq(SgBPhyAdjust::getIsactive, SgConstants.IS_ACTIVE_Y));
        statusCheck(origAdj, locale);
        Long id = origAdj.getId();
        //=========更新明细=====
        JSONArray errArr = storageBoxConfig.getBoxEnable() ?
                updateSubcordsBox(request, id, user, isR3, isRollBackAll) :
                updateSubcords(request, id, user, isR3, isRollBackAll);
        //=========更新主表=====
        SgBPhyAdjust adjust = new SgBPhyAdjust();
        if (adjRequest != null) {
            BeanUtils.copyProperties(adjRequest, adjust);
        }
        //=========开启箱功能时 保存时不需合计总数量=====
        if (!storageBoxConfig.getBoxEnable()) {
            BigDecimal totQty = itemMapper.selectSumQtyByAdjId(id);
            adjust.setTotQty(totQty);
        }else {
            BigDecimal totFee = impItemMapper.selectSumFeeByAdjId(id);
            adjust.setAdjustFee(totFee);
        }
        adjust.setId(id);
        StorageESUtils.setBModelDefalutDataByUpdate(adjust, user);
        adjust.setModifierename(user.getEname());
        mapper.updateById(adjust);
        //推送es
        warehouseESUtils.pushESByPhyAdjust(id, true, false, null, mapper, itemMapper);

        ValueHolderV14<SgR3BaseResult> holderV14 = new ValueHolderV14<>(ResultCode.SUCCESS, SgConstants.MESSAGE_STATUS_SUCCESS);
        if (CollectionUtils.isNotEmpty(errArr)) {
            SgR3BaseResult baseResult = new SgR3BaseResult();
            baseResult.setDataArr(errArr);
            holderV14.setData(baseResult);
            holderV14.setCode(ResultCode.FAIL);
            holderV14.setMessage(errArr.size() + "条条码信息有误!");
        }
        return holderV14;
    }

    /**
     * 参数校验
     */
    private boolean checkParam(SgPhyAdjustBillSaveRequest request, boolean isR3) {
        AssertUtils.notNull(request, "请求参数为空!");
        User user = request.getLoginUser();
        AssertUtils.notNull(user, "用户未登陆！");
        Locale locale = user.getLocale();

        SgPhyAdjustSaveRequest phyAdjust = request.getPhyAdjust();
        if (phyAdjust != null && phyAdjust.getSourceBillId() == null) {
            isR3 = true;
        }
        if (!isR3) {
            AssertUtils.notNull(phyAdjust, "主表信息不能为空!", locale);
            AssertUtils.notNull(phyAdjust.getSourceBillId(), "参数为空-来源单据id", locale);
            AssertUtils.notNull(phyAdjust.getSourceBillType(), "参数为空-来源单据类型", locale);
            if (storageBoxConfig.getBoxEnable()) {
                //若只有条码明细，没有录入明细，需将条码明细转换成散码copy一份至录入明细，oms用
                List<SgPhyAdjustItemSaveRequest> items = request.getItems();
                List<SgPhyAdjustImpItemRequest> impItems = request.getImpItems();
                if (CollectionUtils.isEmpty(impItems) && CollectionUtils.isNotEmpty(items)) {
                    List<SgPhyAdjustImpItemRequest> impItemSaveRequests = Lists.newArrayList();
                    for (SgPhyAdjustItemSaveRequest item : items) {
                        SgPhyAdjustImpItemRequest impItemSaveRequest = new SgPhyAdjustImpItemRequest();
                        BeanUtils.copyProperties(item, impItemSaveRequest);
                        impItemSaveRequest.setIsTeus(OcBasicConstants.IS_MATCH_SIZE_N);
                        impItemSaveRequests.add(impItemSaveRequest);
                    }
                    request.setImpItems(impItemSaveRequests);
                }
                List<SgPhyAdjustImpItemRequest> itemLists = request.getImpItems().stream()
                        .filter(o -> o.getSourceBillItemId() == null).collect(Collectors.toList());
                if (CollectionUtils.isNotEmpty(itemLists)) {
                    AssertUtils.logAndThrow("存在来源单据明细id为空!", locale);
                }
            } else {
                List<SgPhyAdjustItemSaveRequest> itemLists = request.getItems().stream()
                        .filter(o -> o.getSourceBillItemId() == null).collect(Collectors.toList());
                if (CollectionUtils.isNotEmpty(itemLists)) {
                    AssertUtils.logAndThrow("存在来源单据明细id为空!", locale);
                }
            }
        } else {
            AssertUtils.notNull(request.getObjId(), "参数为空-objId", locale);
        }
        Long objId = request.getObjId();
        if (objId != null && objId < 0) {
            AssertUtils.notNull(phyAdjust, "主表信息不能为空!", locale);
            AssertUtils.notNull(phyAdjust.getCpCPhyWarehouseId(), "参数为空-实体仓id为空!", locale);
        }
        //2019-07-30  库存调整单增加逻辑仓（可填）
        if (phyAdjust != null && phyAdjust.getCpCPhyWarehouseId() != null && phyAdjust.getCpCStoreId() != null) {
            CpCPhyWarehouseMapper warehouseMapper = ApplicationContextHandle.getBean(CpCPhyWarehouseMapper.class);
            CpStoreMapper cpStoreMapper = ApplicationContextHandle.getBean(CpStoreMapper.class);

            CpCPhyWarehouse warehouse = warehouseMapper.selectByStoreId(phyAdjust.getCpCStoreId());
            if (!(warehouse != null && warehouse.getId().compareTo(phyAdjust.getCpCPhyWarehouseId()) == 0)) {
                AssertUtils.logAndThrow("当前逻辑仓不属于当前实体仓", locale);
            }
            if (StringUtils.isEmpty(phyAdjust.getCpCStoreEcode())) {
                Long storeId = phyAdjust.getCpCStoreId();
                List<CpCStore> cpCStores = cpStoreMapper.selectList(new QueryWrapper<CpCStore>().lambda()
                        .select(CpCStore::getCpCStoreEcode, CpCStore::getCpCStoreEname)
                        .eq(CpCStore::getId, storeId)
                        .eq(CpCStore::getIsactive, SgConstants.IS_ACTIVE_Y));
                if (CollectionUtils.isEmpty(cpCStores)) {
                    AssertUtils.logAndThrow("无效的逻辑仓:" + storeId, locale);
                }
                request.getPhyAdjust().setCpCStoreEcode(cpCStores.get(0).getCpCStoreEcode());
                request.getPhyAdjust().setCpCStoreEname(cpCStores.get(0).getCpCStoreEname());
            }
        }
        if (phyAdjust != null && phyAdjust.getSgBAdjustPropId() != null) {
            SgBAdjustPropMapper propMapper = ApplicationContextHandle.getBean(SgBAdjustPropMapper.class);
            int count = propMapper.selectCount(new QueryWrapper<SgBAdjustProp>().lambda()
                    .eq(SgBAdjustProp::getId, phyAdjust.getSgBAdjustPropId()));
            AssertUtils.cannot(count == 0, "调整性质表中不存在该调整性质!", locale);
        }

        if (storageBoxConfig.getBoxEnable() && CollectionUtils.isEmpty(request.getImpItems())) {
            if (isR3) {
                List<SgPhyAdjustImpItemRequest> impItems = Lists.newArrayList();
                request.setImpItems(impItems);
            } else {
                AssertUtils.logAndThrow("录入明细不能为空！");
            }
        }
        if (storageBoxConfig.getBoxEnable() && isR3) {
            //r3页面保存 录入明细不是箱的 不会有is_teus标识 需要手动赋值 (新增时)
            for (SgPhyAdjustImpItemRequest impItemRequest : request.getImpItems()) {
                if ((impItemRequest.getId() == null || impItemRequest.getId() < 0) && impItemRequest.getIsTeus() == null) {
                    impItemRequest.setIsTeus(OcBasicConstants.IS_MATCH_SIZE_N);
                }
            }
        }
        return isR3;
    }


    /**
     * 构建 插入的明细
     *
     * @param user             用户
     * @param id               主表id
     * @param itemSaveRequests 传入的明细request
     */
    private List<SgBPhyAdjustItem> createInsertItem(List<SgPhyAdjustItemSaveRequest> itemSaveRequests, User user, Long id) {
        List<SgBPhyAdjustItem> items = Lists.newArrayList();
        if (CollectionUtils.isNotEmpty(itemSaveRequests)) {
            for (SgPhyAdjustItemSaveRequest itemRequest : itemSaveRequests) {
                SgBPhyAdjustItem item = new SgBPhyAdjustItem();
                BeanUtils.copyProperties(itemRequest, item);
                BigDecimal qty = Optional.ofNullable(item.getQty()).orElse(BigDecimal.ONE);
                itemRequest.setQty(qty);
                item.setQty(qty);
                item.setSgBPhyAdjustId(id);
                StorageESUtils.setBModelDefalutData(item, user);
                item.setModifierename(user.getEname());
                item.setOwnerename(user.getEname());
                item.setId(ModelUtil.getSequence(SgConstants.SG_B_PHY_ADJUST_ITEM));
                item.setAmtList(item.getQty().multiply(item.getPriceList() == null ? BigDecimal.ZERO : item.getPriceList()));
                items.add(item);
            }
        }
        return items;
    }

    /**
     * 构建 插入的明细
     *
     * @param user                用户
     * @param id                  主表id
     * @param impItemSaveRequests 传入的明细request
     */
    private List<SgBPhyAdjustImpItem> createInsertItemBox(List<SgPhyAdjustImpItemRequest> impItemSaveRequests, User user, Long id,SgBPhyAdjust adjust) {
        //获取价格类型
        Long priceType = getPriceType(adjust,user);
        List<SgBPhyAdjustImpItem> impItems = Lists.newArrayList();
        if (CollectionUtils.isNotEmpty(impItemSaveRequests)) {
            for (SgPhyAdjustImpItemRequest itemRequest : impItemSaveRequests) {
                SgBPhyAdjustImpItem impItem = new SgBPhyAdjustImpItem();
                BeanUtils.copyProperties(itemRequest, impItem);
                impItem.setSgBPhyAdjustId(id);
                BigDecimal qty = Optional.ofNullable(itemRequest.getQty()).orElse(BigDecimal.ONE);
                impItem.setQty(qty);
                //获取商品调整价
                PsCPro psCPro = queryCproByEcode(impItem.getPsCSkuEcode());
                if(SgConstants.PRICE_TYPE_AGENT.equals(priceType)){
                    BigDecimal adjust_fee = Optional.ofNullable(psCPro.getPriceAgent()).orElse(BigDecimal.ONE);
                    impItem.setPrice(adjust_fee);
                    impItem.setAdjustFee(adjust_fee.multiply(impItem.getQty()));
                    itemRequest.setAdjustFee(adjust_fee.multiply(impItem.getQty()));
                }else if(SgConstants.PRICE_TYPE_FACTORY.equals(priceType)){
                    BigDecimal adjust_fee = Optional.ofNullable(psCPro.getPriceFactory()).orElse(BigDecimal.ONE);
                    impItem.setPrice(adjust_fee);
                    impItem.setAdjustFee(adjust_fee.multiply(impItem.getQty()));
                    itemRequest.setAdjustFee(adjust_fee.multiply(impItem.getQty()));
                }
                StorageESUtils.setBModelDefalutData(impItem, user);
                impItem.setModifierename(user.getEname());
                impItem.setOwnerename(user.getEname());
                impItem.setId(ModelUtil.getSequence(SgConstants.SG_B_PHY_ADJUST_ITEM));
                impItems.add(impItem);
            }
        }
        return impItems;
    }

    /**
     * 原单 校验
     */
    private void statusCheck(SgBPhyAdjust origAdj, Locale locale) {
        AssertUtils.notNull(origAdj, "当前记录已经不存在!", locale);
        AssertUtils.notNull(origAdj.getIsactive(), "数据错误-单据作废状态不存在!", locale);
        AssertUtils.cannot(origAdj.getIsactive().equalsIgnoreCase(SgConstants.IS_ACTIVE_N), "当前单据已作废，不允许编辑", locale);
        AssertUtils.cannot(origAdj.getBillStatus() == SgPhyAdjustConstants.ADJ_AUDIT_STATUS_Y, "当前单据已审核，不允许编辑", locale);
        AssertUtils.cannot(origAdj.getBillStatus() == SgPhyAdjustConstants.ADJ_AUDIT_STATUS_V, "当前单据已作废，不允许编辑", locale);
    }


    /**
     * 压缩明细 去重插入
     *
     * @param objId         主表id
     * @param user          用户
     * @param items         明细
     * @param isRollBackAll 查询 明细不存在是否全部回滚
     * @return 不存在的 条码jsonObj
     */
    private JSONArray mergeInsert(List<SgBPhyAdjustItem> items, Long objId, User user, boolean isRollBackAll) {
        JSONArray errArr = null;
        //2019-08-30 库存调整明细导入支持(单)国标码导入
        //根据国标码补充条码信息
        FillSkuInfoUtil util = ApplicationContextHandle.getBean(FillSkuInfoUtil.class);
        util.fillPhyAdjItemsGbcode(items, user.getLocale(), isRollBackAll);

        //先内部压缩
        HashMap<String, SgBPhyAdjustItem> itemMap = new HashMap<>(16);
        items.forEach(item -> {
            String skuECode = item.getPsCSkuEcode();
            AssertUtils.isTrue(StringUtils.isNotEmpty(skuECode), "明细条码编码不存在!", user.getLocale());
            SgBPhyAdjustItem existItem = itemMap.get(skuECode);
            if (existItem == null) {
                itemMap.put(skuECode, item);
            } else {
                existItem.setQty(existItem.getQty().add(item.getQty()));
                existItem.setAmtList(existItem.getAmtList().add(item.getAmtList()));
            }
        });

        //查询库存调整单是否有原明细
        List<SgBPhyAdjustItem> orgPhyAdjustitems = objId == null ? Lists.newArrayList() :
                itemMapper.selectList(new QueryWrapper<SgBPhyAdjustItem>().lambda().eq(SgBPhyAdjustItem::getSgBPhyAdjustId, objId));
        Map<Long, SgBPhyAdjustItem> adjustMap = orgPhyAdjustitems.stream().collect(Collectors.toMap(SgBPhyAdjustItem::getPsCSkuId, o -> o));

        List<SgBPhyAdjustItem> insertBatchItems = new ArrayList<>();
        for (SgBPhyAdjustItem item : itemMap.values()) {
            SgBPhyAdjustItem existItem = adjustMap.get(item.getPsCSkuId());
            if (existItem != null) {
                existItem.setQty(existItem.getQty().add(item.getQty()));
                BigDecimal priceList = existItem.getPriceList() == null ? BigDecimal.ZERO : existItem.getPriceList();
                existItem.setAmtList(existItem.getQty().multiply(priceList));
                StorageESUtils.setBModelDefalutDataByUpdate(existItem, user);
                existItem.setModifierename(user.getEname());
                itemMapper.updateById(existItem);
            } else {
                insertBatchItems.add(item);
            }
        }

        if (CollectionUtils.isNotEmpty(insertBatchItems)) {
            //明细补充
            if (insertBatchItems.get(0).getPsCSpec1Id() == null) {
                ValueHolderV14<FillSkuResult> holderV14 = util.fillPhyAdjItems(insertBatchItems, user.getLocale(), isRollBackAll);
                insertBatchItems = holderV14.getData().getItemList();
                errArr = holderV14.getData().getErrArr();
            }
            //批量新增明细 500/次
            if (CollectionUtils.isNotEmpty(insertBatchItems)) {
                SgStoreUtils.batchInsertTeus(insertBatchItems, "库存调整单明细", SgConstants.SG_COMMON_INSERT_PAGE_SIZE, itemMapper);
            }
        }
        return errArr;
    }


    /**
     * 压缩明细 去重插入 - 支持箱
     *
     * @param items         录入明细
     * @param objId         主表id
     * @param user          用户
     * @param isRollBackAll 是否回滚
     */
    private JSONArray mergeInsertBox(List<SgBPhyAdjustImpItem> items, Long objId, User user, boolean isRollBackAll) {

        JSONArray errArr = null;
        //先内部压缩
        HashMap<String, SgBPhyAdjustImpItem> itemMap = new HashMap<>(16);
        HashMap<String, SgBPhyAdjustImpItem> teusMap = new HashMap<>(16);
        for (SgBPhyAdjustImpItem item : items) {
            if (item.getIsTeus().equals(OcBasicConstants.IS_MATCH_SIZE_N)) {
                String skuECode = item.getPsCSkuEcode();
                AssertUtils.isTrue(StringUtils.isNotEmpty(skuECode), "明细条码编码不存在!", user.getLocale());
                SgBPhyAdjustImpItem existItem = itemMap.get(skuECode);
                if (existItem == null) {
                    itemMap.put(skuECode, item);
                } else {
                    existItem.setQty(existItem.getQty().add(item.getQty()));
                    existItem.setPrice(existItem.getPrice().add(item.getPrice()));
                    existItem.setAdjustFee(existItem.getAdjustFee().add(item.getAdjustFee()));
                }
            } else if (item.getIsTeus().equals(OcBasicConstants.IS_MATCH_SIZE_Y)) {
                String teusEcode = item.getPsCTeusEcode();
                AssertUtils.isTrue(StringUtils.isNotEmpty(teusEcode), "箱号为空!", user.getLocale());
                SgBPhyAdjustImpItem existItem = teusMap.get(teusEcode);
                if (existItem == null) {
                    itemMap.put(teusEcode, item);
                } else {
                    existItem.setQty(existItem.getQty().add(item.getQty()));
                }
            }
        }

        List<SgBPhyAdjustImpItem> impItems = Lists.newArrayList();
        impItems.addAll(itemMap.values());
        impItems.addAll(teusMap.values());

        //查询库存调整单是否有原明细
        SgBPhyAdjustImpItemMapper impItemMapper = ApplicationContextHandle.getBean(SgBPhyAdjustImpItemMapper.class);
        List<SgBPhyAdjustImpItem> orgImpItems = objId == null ? Lists.newArrayList() :
                impItemMapper.selectList(new QueryWrapper<SgBPhyAdjustImpItem>().lambda().eq(SgBPhyAdjustImpItem::getSgBPhyAdjustId, objId));
        Map<String, SgBPhyAdjustImpItem> teusImpMap = orgImpItems.stream()
                .filter(o -> o.getIsTeus().equals(OcBasicConstants.IS_MATCH_SIZE_Y))
                .collect(Collectors.toMap(SgBPhyAdjustImpItem::getPsCTeusEcode, o -> o));
        Map<String, SgBPhyAdjustImpItem> skuImpMap = orgImpItems.stream()
                .filter(o -> o.getIsTeus().equals(OcBasicConstants.IS_MATCH_SIZE_N))
                .collect(Collectors.toMap(SgBPhyAdjustImpItem::getPsCSkuEcode, o -> o));

        List<SgBPhyAdjustImpItem> insertBatchItems = new ArrayList<>();
        for (SgBPhyAdjustImpItem item : impItems) {
            SgBPhyAdjustImpItem existItem = item.getIsTeus().equals(OcBasicConstants.IS_MATCH_SIZE_Y) ?
                    teusImpMap.get(item.getPsCTeusEcode()) : skuImpMap.get(item.getPsCSkuEcode());
            if (existItem != null) {
                existItem.setQty(existItem.getQty().add(item.getQty()));
                if(item.getIsTeus().equals(OcBasicConstants.IS_MATCH_SIZE_N)){
                    existItem.setPrice(existItem.getPrice().add(item.getPrice()));
                    existItem.setAdjustFee(existItem.getAdjustFee().add(item.getAdjustFee()));
                }
                BigDecimal priceList = existItem.getPriceList() == null ? BigDecimal.ZERO : existItem.getPriceList();
                existItem.setPriceList(priceList);
                StorageESUtils.setBModelDefalutDataByUpdate(existItem, user);
                existItem.setModifierename(user.getEname());
                impItemMapper.updateById(existItem);
            } else {
                insertBatchItems.add(item);
            }
        }

        if (CollectionUtils.isNotEmpty(insertBatchItems)) {
            //明细补充
            if (insertBatchItems.get(0).getPsCSpec1Id() == null) {
                FillSkuInfoUtil util = ApplicationContextHandle.getBean(FillSkuInfoUtil.class);
                ValueHolderV14<FillSkuResult> holderV14 = util.fillPhyAdjItemsBox(insertBatchItems, user.getLocale(), isRollBackAll);
                insertBatchItems = holderV14.getData().getImpItems();
                errArr = holderV14.getData().getErrArr();
            }
            //批量新增明细 500/次
            if (CollectionUtils.isNotEmpty(insertBatchItems)) {
                SgStoreUtils.batchInsertTeus(insertBatchItems, "库存调整单录入明细", SgConstants.SG_COMMON_INSERT_PAGE_SIZE, impItemMapper);
            } else {
                StringBuilder sb = new StringBuilder("新增库存调整单失败,明细不能为空!");
                sb.append(System.lineSeparator());
                if (CollectionUtils.isNotEmpty(errArr)) {
                    for (int i = 0; i < errArr.size(); i++) {
                        JSONObject errObj = errArr.getJSONObject(i);
                        String messagae = errObj.getString("message");
                        sb.append(messagae);
                        sb.append(System.lineSeparator());
                    }
                }
                AssertUtils.logAndThrow(sb.toString());
            }
        }
        return errArr;
    }


    private JSONArray updateSubcords(SgPhyAdjustBillSaveRequest request, Long id, User user, boolean isR3, boolean isRollBackAll) {
        List<SgBPhyAdjustItem> items;
        List<SgPhyAdjustItemSaveRequest> insertList = Lists.newArrayList();
        //TODO 此处待校验明细id/来源单据id是否为空
        List<SgPhyAdjustItemSaveRequest> itemSaveRequests = request.getItems();
        List<Long> queryCondition = isR3 ?
                itemSaveRequests.stream().filter(o -> o.getId() != null && o.getId() > 0)
                        .map(SgPhyAdjustItemSaveRequest::getId).collect(Collectors.toList()) :
                itemSaveRequests.stream().filter(o -> o.getSourceBillItemId() != null)
                        .map(SgPhyAdjustItemSaveRequest::getSourceBillItemId).collect(Collectors.toList());

        if (CollectionUtils.isNotEmpty(queryCondition)) {
            Map<Long, SgBPhyAdjustItem> exsistMap = isR3 ?
                    itemMapper.selectList(new QueryWrapper<SgBPhyAdjustItem>().lambda()
                            .in(SgBPhyAdjustItem::getId, queryCondition))
                            .stream().collect(Collectors.toMap(SgBPhyAdjustItem::getId, o -> o)) :
                    itemMapper.selectList(new QueryWrapper<SgBPhyAdjustItem>().lambda()
                            .eq(SgBPhyAdjustItem::getSgBPhyAdjustId, id)
                            .in(SgBPhyAdjustItem::getSourceBillItemId, queryCondition))
                            .stream().collect(Collectors.toMap(SgBPhyAdjustItem::getSourceBillItemId, o -> o));
            for (SgPhyAdjustItemSaveRequest itemSaveRequest : itemSaveRequests) {
                SgBPhyAdjustItem existItem = exsistMap.get(isR3 ? itemSaveRequest.getId() : itemSaveRequest.getSourceBillItemId());
                if (existItem != null) {
                    //TODO 目前覆盖 是否需要增量修改？
                    SgBPhyAdjustItem update = new SgBPhyAdjustItem();
                    BeanUtils.copyProperties(itemSaveRequest, update);
                    StorageESUtils.setBModelDefalutDataByUpdate(update, user);
                    update.setModifierename(user.getEname());
                    if (update.getQty() != null) {
                        update.setAmtList(update.getQty().multiply(existItem.getPriceList() == null ? BigDecimal.ZERO : existItem.getPriceList()));
                    }
                    itemMapper.updateById(update);
                } else {
                    insertList.add(itemSaveRequest);
                }
            }
        }

        //构建待新增明细
        items = createInsertItem(insertList, user, id);

        JSONArray errArr = null;
        if (CollectionUtils.isNotEmpty(items)) {
            errArr = mergeInsert(items, id, user, isRollBackAll);
        }
        return errArr;
    }

    private JSONArray updateSubcordsBox(SgPhyAdjustBillSaveRequest request, Long id, User user, boolean isR3, boolean isRollBackAll) {
        List<SgBPhyAdjustImpItem> impItems;
        //TODO 此处待校验明细id/来源单据id是否为空
        List<SgPhyAdjustImpItemRequest> itemSaveRequests = request.getImpItems();
        List<SgPhyAdjustImpItemRequest> insertList = itemSaveRequests.stream().filter(o -> o.getId() == null || o.getId() == -1L)
                .collect(Collectors.toList());
        List<Long> queryCondition = isR3 ?
                itemSaveRequests.stream().filter(o -> o.getId() != null && o.getId() > 0)
                        .map(SgPhyAdjustImpItemRequest::getId).collect(Collectors.toList()) :
                itemSaveRequests.stream().filter(o -> o.getSourceBillItemId() != null)
                        .map(SgPhyAdjustImpItemRequest::getSourceBillItemId).collect(Collectors.toList());

        if (CollectionUtils.isNotEmpty(queryCondition)) {
            SgBPhyAdjustImpItemMapper impItemMapper = ApplicationContextHandle.getBean(SgBPhyAdjustImpItemMapper.class);
            Map<Long, SgBPhyAdjustImpItem> exsistMap = isR3 ?
                    impItemMapper.selectList(new QueryWrapper<SgBPhyAdjustImpItem>().lambda()
                            .in(SgBPhyAdjustImpItem::getId, queryCondition))
                            .stream().collect(Collectors.toMap(SgBPhyAdjustImpItem::getId, o -> o)) :
                    impItemMapper.selectList(new QueryWrapper<SgBPhyAdjustImpItem>().lambda()
                            .eq(SgBPhyAdjustImpItem::getSgBPhyAdjustId, id)
                            .in(SgBPhyAdjustImpItem::getSourceBillItemId, queryCondition))
                            .stream().collect(Collectors.toMap(SgBPhyAdjustImpItem::getSourceBillItemId, o -> o));
            for (SgPhyAdjustImpItemRequest itemSaveRequest : itemSaveRequests) {
                SgBPhyAdjustImpItem existItem = exsistMap.get(isR3 ? itemSaveRequest.getId() : itemSaveRequest.getSourceBillItemId());
                if (existItem != null) {
                    //TODO 目前覆盖 是否需要增量修改？
                    SgBPhyAdjustImpItem update = new SgBPhyAdjustImpItem();
                    BeanUtils.copyProperties(itemSaveRequest, update);
                    BigDecimal qty = Optional.ofNullable(itemSaveRequest.getQty()).orElse(BigDecimal.ONE);
                    update.setQty(qty);
                    update.setAdjustFee(existItem.getAdjustFee().multiply(update.getQty()));
                    StorageESUtils.setBModelDefalutDataByUpdate(update, user);
                    update.setModifierename(user.getEname());
                    impItemMapper.updateById(update);
                } else {
                    insertList.add(itemSaveRequest);
                }
            }
        }
        SgBPhyAdjust adjust = mapper.selectById(id);
        //构建待新增明细
        impItems = createInsertItemBox(insertList, user, id,adjust);
        BigDecimal adjustFee = impItems.stream().map(SgBPhyAdjustImpItem::getAdjustFee).reduce(BigDecimal.ZERO, BigDecimal::add);
        adjust.setAdjustFee(adjustFee);
        mapper.updateById(adjust);

        JSONArray errArr = null;
        if (CollectionUtils.isNotEmpty(impItems)) {
            errArr = mergeInsertBox(impItems, id, user, isRollBackAll);
        }
        return errArr;
    }

    /**
     * @MethodName: getPriceType
     * @Description: 获取价格类型
     * @Param: []
     * @Return: java.lang.Long
     * @Author: chenfajun
     * @Date: 2020/6/9
    **/
    private Long getPriceType(SgBPhyAdjust adjust,User user){
        //逻辑店仓ID
        Long cpCStoreId = adjust.getCpCStoreId();
        //所属经销商 通过逻辑仓档案获取
        com.jackrain.nea.cpext.model.table.CpCStore cpCStore = queryCpCWareHouse(cpCStoreId);
        // 如查没有会根据实体仓ID查找主仓
        if(null == cpCStore){
            Long cpCPhyWarehouseId = adjust.getCpCPhyWarehouseId();
            cpCStore = queryMainStoreByWareHouseId(cpCPhyWarehouseId);
            AssertUtils.notNull(cpCStore, "不存在的店仓信息!", user.getLocale());
        }
        //获取经销商
        CpCustomer cpCustomer = queryCustomer(cpCStore.getCpCCustomerId());
        AssertUtils.notNull(cpCustomer, "不存在的经销商信息!", user.getLocale());
        //经销商价格类型
        Long priceType = cpCustomer.getPriceType();
        AssertUtils.notNull(priceType, "不存在的经销商价格类型!", user.getLocale());
        return priceType;
    }


    /**
     * 获取经销商档案
     */
    private CpCustomer queryCustomer(Long id) {
        try {
            ValueHolderV14<CpCustomer> vh = cpRpcCustomerQuery.queryCustomerById(id);
            if (vh.isOK()) {
                CpCustomer data = vh.getData();
                return data;
            }
        } catch (Exception e) {
            log.error(this.getClass().getName() + "经销商查询异常,id " + id, e);
        }
        return null;

    }

    /**
     * 获取经销商档案
     */
    private PsCPro queryCproByEcode(String ecode) {
        try {
            ValueHolderV14<PsCPro> vh = psRpcCproQuery.queryCproByEcode(ecode);
            if (vh.isOK()) {
                PsCPro data = vh.getData();
                return data;
            }
        } catch (Exception e) {
            log.error(this.getClass().getName() + "商品档案查询异常,ecode " + ecode, e);
        }
        return null;

    }

    /**
     * 查询逻辑仓
     */
    private com.jackrain.nea.cpext.model.table.CpCStore queryCpCWareHouse(Long id) {
        try {
            ValueHolderV14<com.jackrain.nea.cpext.model.table.CpCStore> vh = cpRpcStoreQuery.queryCpStoreById(id);
            if (vh.isOK()) {
                return vh.getData();
            }
        } catch (Exception e) {
            log.error(this.getClass().getName() + "逻辑仓查询异常,id " + id, e);
        }
        return null;
    }

    /**
     * 查询主逻辑仓
     */
    private com.jackrain.nea.cpext.model.table.CpCStore queryMainStoreByWareHouseId(Long id) {
        try {
            ValueHolderV14<com.jackrain.nea.cpext.model.table.CpCStore> vh = cpRpcStoreQuery.queryMainStoreByWareHouseId(id);
            if (vh.isOK()) {
                return vh.getData();
            }
        } catch (Exception e) {
            log.error(this.getClass().getName() + "逻辑仓查询异常,id " + id, e);
        }
        return null;
    }
}
