package com.jackrain.nea.sg.in.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Lists;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.common.SystemUserResource;
import com.jackrain.nea.sg.basic.config.SgStorageBoxConfig;
import com.jackrain.nea.sg.in.mapper.SgBPhyInNoticesImpItemMapper;
import com.jackrain.nea.sg.in.mapper.SgBPhyInNoticesItemMapper;
import com.jackrain.nea.sg.in.mapper.SgBPhyInNoticesMapper;
import com.jackrain.nea.sg.in.model.request.SgBPhyInResultItemSaveRequest;
import com.jackrain.nea.sg.in.model.request.SgBPhyInResultSaveRequest;
import com.jackrain.nea.sg.in.model.request.SgPhyInResultBillSaveRequest;
import com.jackrain.nea.sg.in.model.request.SgPhyInResultImpItemSaveRequest;
import com.jackrain.nea.sg.in.model.result.ReturnSgPhyInResult;
import com.jackrain.nea.sg.in.model.table.SgBPhyInNotices;
import com.jackrain.nea.sg.in.model.table.SgBPhyInNoticesImpItem;
import com.jackrain.nea.sg.in.model.table.SgBPhyInNoticesItem;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Slf4j
@Component
public class SgPhyInNoticesSendMQService {


    @Autowired
    private SgPhyInResultSaveAndAuditService inResultSaveAndAuditService;

    @Autowired
    private SgStorageBoxConfig storageBoxConfig;

    /**
     * 通过MQ获取通知单，包装成入库结果单，调入库结果单新增服务
     *
     * @author jiang.cj
     * @since 2019-05-07
     * create at : 2019-05-07
     */
    public ValueHolderV14 saveSgBPhyInResult(List<Long> billIds) throws NDSException {

        if (log.isDebugEnabled()) {
            log.debug("Start SgPhyInNoticesSendMQService.saveSgBPhyInResult. ReceiveParams:billIds="
                    + JSONObject.toJSONString(billIds) + ";");
        }
        if (CollectionUtils.isEmpty(billIds)) {
            throw new NDSException(Resources.getMessage("传入参数为空!"));
        }

        List<SgPhyInResultBillSaveRequest> billList = getBatchNoticesBill(billIds, null, false);
        ValueHolderV14 result = inResultSaveAndAuditService.saveAndAuditBill(billList);
        return result;
    }

    /**
     * 获取当前MQ发送的所有通知单
     *
     * @param billIds
     * @param loginUser
     * @param isOneClickLibrary
     * @return
     */
    public List<SgPhyInResultBillSaveRequest> getBatchNoticesBill(List<Long> billIds, User loginUser, boolean isOneClickLibrary) {
        List<SgPhyInResultBillSaveRequest> allList = new ArrayList<>();
        //分批获取入库通知单(每次500条)
        int listSize = billIds.size();
        int pageSize = SgConstants.SG_COMMON_INSERT_PAGE_SIZE;
        int page = listSize / pageSize;

        if (listSize % pageSize != 0) {
            page++;
        }

        List<List<Long>> idsList = Lists.newArrayList();
        for (int i = 0; i < page; i++) {
            List<Long> pageList = billIds.subList(i * pageSize,
                    (((i + 1) * pageSize > listSize ? listSize : pageSize * (i + 1))));
            idsList.add(pageList);
        }

        if (CollectionUtils.isNotEmpty(idsList)) {
            for (List<Long> ids : idsList) {
                //获取本批所有的通知单(已组装好的数据)
                List<SgPhyInResultBillSaveRequest> list = getAssemblyParameters(ids, loginUser, isOneClickLibrary);
                allList.addAll(list);
            }
        }

        return allList;
    }

    /**
     * 获取入库通知单和明细并组装好数据
     *
     * @param tempList
     * @param loginUser
     * @param isOneClickLibrary
     * @return
     */
    public List<SgPhyInResultBillSaveRequest> getAssemblyParameters(List<Long> tempList, User loginUser, boolean isOneClickLibrary) {
        SgBPhyInNoticesMapper noticesMapper = ApplicationContextHandle.getBean(SgBPhyInNoticesMapper.class);
        List<SgPhyInResultBillSaveRequest> billList = Lists.newArrayList();
        //获取入库通知单主表数据
        List<SgBPhyInNotices> noticesList = noticesMapper.selectList(new QueryWrapper<SgBPhyInNotices>().lambda().in(SgBPhyInNotices::getId, tempList));

        User user = Optional.ofNullable(loginUser).orElse(SystemUserResource.getRootUser());

        noticesList.forEach(notices -> {
            SgPhyInResultBillSaveRequest resultBillSaveRequest = new SgPhyInResultBillSaveRequest();
            SgBPhyInResultSaveRequest resultSaveRequest = new SgBPhyInResultSaveRequest();
            resultSaveRequest.setId(-1l);
            resultSaveRequest.setCpCPhyWarehouseId(notices.getCpCPhyWarehouseId());
            resultSaveRequest.setCpCPhyWarehouseEcode(notices.getCpCPhyWarehouseEcode());
            resultSaveRequest.setCpCPhyWarehouseEname(notices.getCpCPhyWarehouseEname());
            resultSaveRequest.setBillNo(notices.getBillNo());
            resultSaveRequest.setSgBPhyInNoticesId(notices.getId());
            resultSaveRequest.setGoodsOwner(notices.getGoodsOwner());
            resultSaveRequest.setInTime(notices.getInTime());
            resultSaveRequest.setInId(user.getId().longValue());
            resultSaveRequest.setInEcode("");
            resultSaveRequest.setInEname(user.getEname());
            resultSaveRequest.setInName(user.getName());
            Date inTime = notices.getInTime() != null ? notices.getInTime() : new Date();
            resultSaveRequest.setInTime(inTime);
            resultSaveRequest.setInType(notices.getInType());
            resultSaveRequest.setCpCCustomerWarehouseId(notices.getCpCCustomerWarehouseId());
            resultSaveRequest.setCpCCsEcode(notices.getCpCCsEcode());
            resultSaveRequest.setCpCCsEname(notices.getCpCCsEname());
            resultSaveRequest.setCpCSupplierId(notices.getCpCSupplierId());
            resultSaveRequest.setSourceBillType(notices.getSourceBillType());
            resultSaveRequest.setSourceBillId(notices.getSourceBillId());
            resultSaveRequest.setSourceBillNo(notices.getSourceBillNo());
            resultSaveRequest.setSendName(notices.getSendName());
            resultSaveRequest.setSendMobile(notices.getSendMobile());
            resultSaveRequest.setSendPhone(notices.getSendPhone());
            resultSaveRequest.setCpCRegionProvinceId(notices.getCpCRegionProvinceId());
            resultSaveRequest.setCpCRegionProvinceEcode(notices.getCpCRegionProvinceEcode());
            resultSaveRequest.setCpCRegionProvinceEname(notices.getCpCRegionProvinceEname());
            resultSaveRequest.setCpCRegionCityId(notices.getCpCRegionCityId());
            resultSaveRequest.setCpCRegionCityEcode(notices.getCpCRegionCityEcode());
            resultSaveRequest.setCpCRegionCityEname(notices.getCpCRegionCityEname());
            resultSaveRequest.setCpCRegionAreaId(notices.getCpCRegionAreaId());
            resultSaveRequest.setCpCRegionAreaEcode(notices.getCpCRegionAreaEcode());
            resultSaveRequest.setCpCRegionAreaEname(notices.getCpCRegionAreaEname());
            resultSaveRequest.setSendAddress(notices.getSendAddress());
            resultSaveRequest.setSendZip(notices.getSendZip());
            resultSaveRequest.setCpCLogisticsId(notices.getCpCLogisticsId());
            resultSaveRequest.setCpCLogisticsEcode(notices.getCpCLogisticsEcode());
            resultSaveRequest.setCpCLogisticsEname(notices.getCpCLogisticsEname());
            resultSaveRequest.setLogisticNumber(notices.getLogisticNumber());
            resultSaveRequest.setCpCShopId(notices.getCpCShopId());
            resultSaveRequest.setCpCShopTitle(notices.getCpCShopTitle());
            resultSaveRequest.setSourcecode(notices.getSourcecode());
            resultSaveRequest.setSellerRemark(notices.getSellerRemark());
            resultSaveRequest.setBuyerRemark(notices.getBuyerRemark());
            resultSaveRequest.setTotQtyIn(notices.getTotQtyIn());
            resultSaveRequest.setTotAmtListIn(notices.getTotAmtListIn());
            resultSaveRequest.setSgBPhyInNoticesBillno(notices.getBillNo());
            resultSaveRequest.setRemark(notices.getRemark());
            resultSaveRequest.setIsLast(SgConstants.IS_LAST_YES);
            //设置主表数据
            resultBillSaveRequest.setSgBPhyInResultSaveRequest(resultSaveRequest);
            //设置明细数据
            getItemList(notices.getId(), isOneClickLibrary, resultBillSaveRequest);

            resultBillSaveRequest.setLoginUser(user);
            billList.add(resultBillSaveRequest);
        });
        return billList;
    }

    /**
     * 获取明细数据
     *
     * @param id                    通知单id
     * @param isOneClickLibrary     是否一件出入库
     * @param resultBillSaveRequest 是否开启箱功能
     */
    private void getItemList(Long id, boolean isOneClickLibrary, SgPhyInResultBillSaveRequest resultBillSaveRequest) {
        if (storageBoxConfig.getBoxEnable()) {
            SgBPhyInNoticesImpItemMapper noticesImpItemMapper = ApplicationContextHandle.getBean(SgBPhyInNoticesImpItemMapper.class);
            List<SgBPhyInNoticesImpItem> impItemList = noticesImpItemMapper.selectList(new QueryWrapper<SgBPhyInNoticesImpItem>().lambda()
                    .eq(SgBPhyInNoticesImpItem::getSgBPhyInNoticesId, id));
            List<SgPhyInResultImpItemSaveRequest> impList = Lists.newArrayList();
            impItemList.forEach(item -> {
                SgPhyInResultImpItemSaveRequest impItemSaveRequest = new SgPhyInResultImpItemSaveRequest();
                BeanUtils.copyProperties(item, impItemSaveRequest);
                impItemSaveRequest.setId(-1l);
                //TODO 录入明细一键出入库 入库数量待确认
                BigDecimal qtyIn = item.getQty();
                qtyIn = Optional.ofNullable(qtyIn).orElse(BigDecimal.ZERO);
                impItemSaveRequest.setQtyIn(qtyIn);
                impItemSaveRequest.setPriceList(item.getPriceList());
                impList.add(impItemSaveRequest);
            });
            resultBillSaveRequest.setImpItemList(impList);
        } else {
            SgBPhyInNoticesItemMapper noticesItemMapper = ApplicationContextHandle.getBean(SgBPhyInNoticesItemMapper.class);
            List<SgBPhyInNoticesItem> itemList = noticesItemMapper.selectList(new QueryWrapper<SgBPhyInNoticesItem>().lambda()
                    .eq(SgBPhyInNoticesItem::getSgBPhyInNoticesId, id));
            List<SgBPhyInResultItemSaveRequest> list = Lists.newArrayList();
            itemList.forEach(item -> {
                SgBPhyInResultItemSaveRequest itemRequest = new SgBPhyInResultItemSaveRequest();
                itemRequest.setId(-1l);
                itemRequest.setPsCSkuId(item.getPsCSkuId());
                itemRequest.setPsCSkuEcode(item.getPsCSkuEcode());
                itemRequest.setPsCProId(item.getPsCProId());
                itemRequest.setPsCProEcode(item.getPsCProEcode());
                itemRequest.setPsCProEname(item.getPsCProEname());
                itemRequest.setPsCSpec1Id(item.getPsCSpec1Id());
                itemRequest.setPsCSpec1Ecode(item.getPsCSpec1Ecode());
                itemRequest.setPsCSpec1Ename(item.getPsCSpec1Ename());
                itemRequest.setPsCSpec2Id(item.getPsCSpec2Id());
                itemRequest.setPsCSpec2Ecode(item.getPsCSpec2Ecode());
                itemRequest.setPsCSpec2Ename(item.getPsCSpec2Ename());
                itemRequest.setAmtListIn(item.getAmtListIn());
                BigDecimal qtyIn = isOneClickLibrary ? item.getQty() : item.getReserveDecimal01();
                qtyIn = Optional.ofNullable(qtyIn).orElse(BigDecimal.ZERO);
                itemRequest.setQtyIn(qtyIn);
                itemRequest.setPriceList(item.getPriceList());
                itemRequest.setSgBPhyInNoticesItemId(item.getId());
                itemRequest.setGbcode(item.getGbcode());
                list.add(itemRequest);
            });
            resultBillSaveRequest.setItemList(list);
        }
    }

    /**
     * MQ消费成功后，更新一下状态标记MQ消费成功
     *
     * @param result
     */
    public void updateNoticesByMq(ReturnSgPhyInResult result) {
        SgBPhyInNoticesMapper noticesmapper = ApplicationContextHandle.getBean(SgBPhyInNoticesMapper.class);
        User user = SystemUserResource.getRootUser();
        SgBPhyInNotices notices = new SgBPhyInNotices();
        notices.setId(result.getObjid());
        notices.setIsMqConsume(SgConstants.IS_YES);
        notices.setModifierid(user.getId().longValue());
        notices.setModifiername(user.getName());
        notices.setModifierename(user.getEname());
        notices.setModifieddate(new Date());
        noticesmapper.updateById(notices);
    }
}
