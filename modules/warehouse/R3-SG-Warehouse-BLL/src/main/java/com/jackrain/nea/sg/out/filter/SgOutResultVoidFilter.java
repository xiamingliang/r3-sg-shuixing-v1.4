package com.jackrain.nea.sg.out.filter;

import com.jackrain.nea.redis.config.CusRedisTemplate;
import com.jackrain.nea.redis.util.RedisOpsUtil;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.out.common.SgOutConstantsIF;
import com.jackrain.nea.sg.out.mapper.SgBPhyOutResultMapper;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutResult;
import com.jackrain.nea.tableService.MainTableRecord;
import com.jackrain.nea.tableService.TableServiceContext;
import com.jackrain.nea.tableService.filter.BaseFilter;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * @author leexh
 * @since 2019/5/31 14:08
 * desc: 出库结果单作废
 */
@Slf4j
@Component
public class SgOutResultVoidFilter extends BaseFilter {

    @Override
    public void before(TableServiceContext context, MainTableRecord row) {

        Long objId = row.getId();
        SgBPhyOutResultMapper resultMapper = ApplicationContextHandle.getBean(SgBPhyOutResultMapper.class);
        SgBPhyOutResult outResult = resultMapper.selectById(objId);
        AssertUtils.notNull(outResult, "当前记录已不存在！", context.getLocale());

        Long sourceBillId = outResult.getSourceBillId();
        Integer sourceBillType = outResult.getSourceBillType();
        String lockKsy = SgConstants.SG_B_PHY_OUT_RESULT + ":" + sourceBillId + ":" + sourceBillType;
        CusRedisTemplate<String, String> redisTemplate = RedisOpsUtil.getObjRedisTemplate();
        if (redisTemplate.opsForValue().get(lockKsy) == null) {
            redisTemplate.opsForValue().set(lockKsy, "OK", 30, TimeUnit.MINUTES);

            // 判断单据状态
            Integer billStatus = outResult.getBillStatus();
            if (billStatus != null && billStatus == SgOutConstantsIF.OUT_RESULT_STATUS_AUDIT) {
                AssertUtils.logAndThrow("当前记录已审核，不允许作废！");
            }

            if (billStatus != null && billStatus == SgOutConstantsIF.OUT_RESULT_STATUS_VOID) {
                AssertUtils.logAndThrow("当前记录已作废，不允许重复作废！");
            }
            row.getCommitData().put("BILL_STATUS", SgOutConstantsIF.OUT_RESULT_STATUS_VOID);
        } else {
            String msg = "当前单据审核中，请稍后重试！来源单据ID：" + sourceBillId + "来源单据类型：" + sourceBillType;
            log.debug(this.getClass().getName() + ".debug," + msg);
            AssertUtils.logAndThrow(msg, context.getLocale());
        }
    }

    @Override
    public void after(TableServiceContext context, MainTableRecord row) {

        Long sourceBillId = row.getOrignalData().getLong("SOURCE_BILL_ID");
        Integer sourceBillType = row.getOrignalData().getInteger("SOURCE_BILL_TYPE");
        CusRedisTemplate<String, String> redisTemplate = RedisOpsUtil.getStrRedisTemplate();
        String lockKsy = SgConstants.SG_B_PHY_OUT_RESULT + ":" + sourceBillId + ":" + sourceBillType;
        try {
            if(redisTemplate.opsForValue().get(lockKsy) != null){
                redisTemplate.delete(lockKsy);
            }
        } catch (Exception e) {
            log.error(this.getClass().getName() + "redis锁释放失败，请联系管理员！lockKsy：" + lockKsy);
            AssertUtils.logAndThrow("redis锁释放失败，请联系管理员！");
        }
    }
}
