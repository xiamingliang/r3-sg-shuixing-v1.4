package com.jackrain.nea.sg.unpack.common;

/**
 * @author: 周琳胜
 * @since: 2019/11/27
 * create at : 2019/11/27 14:47
 */
public interface SgUnpackConstants {

    /**
     * 单据状态
     * 1=未审核
     * 2=已审核
     * 3=已作废
     */
    int BILL_STATUS_NOT_AUDIT = 1;
    int BILL_STATUS_AUDIT = 2;
    int BILL_STATUS_VOID = 3;

    String SG_B_TEUS_UNPACK = "sg_b_teus_unpack";  // 拆箱单
    String SG_B_TEUS_UNPACK_ITEM = "sg_b_teus_unpack_item"; // 拆箱明细表
    String SG_B_TEUS_UNPACK_SKU_ITEM = "sg_b_teus_unpack_sku_item"; // 拆箱箱内条码明细

    /**
     * 箱状态（箱定义）
     * 1=待入库
     * 2=已入库未拆箱
     * 3=已拆箱
     */
    int BOX_STATUS_WAIT_IN = 1;
    int BOX_STATUS_IN = 2;
    int BOX_STATUS_SPLIT = 3;

    /**
     * 是否箱
     */
    Integer IS_TEUS_N = 0;
    Integer IS_TEUS_Y = 1;

    /**
     * 是否传WMS
     */
    Integer IS_TOWNS_N = 0;
    Integer IS_TOWNS_Y = 1;

    /**
     * 传WMS失败次数
     */
    Integer FAIL_COUNT = 5;
    Integer COUNT_ZERO = 0;

    /**
     * 传WMS状态
     */
    Integer WMS_STATUS_WAIT_PASS = 0;       // 未传WMS
    Integer WMS_STATUS_PASSING = 1;         // 传WMS中
    Integer WMS_STATUS_PASS_SUCCESS = 2;    // 传WMS成功
    Integer WMS_STATUS_PASS_FAILED = 3;     // 传WMS失败


}
