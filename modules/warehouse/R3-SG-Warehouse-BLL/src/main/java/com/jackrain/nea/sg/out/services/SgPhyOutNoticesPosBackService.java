package com.jackrain.nea.sg.out.services;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Lists;
import com.jackrain.nea.constants.ResultCode;
// start erp.zhang.xiwen.20191213 暂时不用
//import com.jackrain.nea.oc.oms.api.OmsOrderGoBackCmd;
// end erp.zhang.xiwen.20191213 暂时不用
import com.jackrain.nea.sg.basic.common.R3ParamConstants;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.model.result.SgR3BaseResult;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.out.common.SgOutConstantsIF;
import com.jackrain.nea.sg.out.mapper.SgBPhyOutNoticesMapper;
import com.jackrain.nea.sg.out.model.request.SgPhyOutNoticesPosRequest;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutNotices;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 全渠道发货单退回服务
 *
 * @author 舒威
 * @since 2019/12/11
 * create at : 2019/12/11 14:33
 */
@Slf4j
@Component
public class SgPhyOutNoticesPosBackService {

    @Autowired
    private SgBPhyOutNoticesMapper noticesMapper;

    // start erp.zhang.xiwen.20191213 暂时不用
/*
    @Reference(group = "oms-fi", version = "1.0")
    private OmsOrderGoBackCmd omsOrderGoBackCmd;*/
// end erp.zhang.xiwen.20191213 暂时不用

    public ValueHolderV14<SgR3BaseResult> backPosOutNotices(SgPhyOutNoticesPosRequest request) {
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ".debug, param:" + JSONObject.toJSONString(request));
        }
        ValueHolderV14<SgR3BaseResult> v14 = new ValueHolderV14<>();
        AssertUtils.notNull(request, "参数不能为空！");

        checkParams(request);
        User loginUser = request.getLoginUser();

        JSONArray errorArr = new JSONArray();
        Integer accSuccess = 0, accFailed = 0;

        Map<Long, SgBPhyOutNotices> noticesMap = noticesMapper.selectList(new QueryWrapper<SgBPhyOutNotices>().lambda()
                .in(SgBPhyOutNotices::getId, request.getIds()))
                .stream().collect(Collectors.toMap(SgBPhyOutNotices::getId, o -> o));

        List<Long> sourceBillIds = Lists.newArrayList();
        for (int i = 0; i < request.getIds().size(); i++) {
            Long objId = request.getIds().getLong(i);
            try {
                SgBPhyOutNotices outNotices = noticesMap.get(objId);
                AssertUtils.notNull(outNotices, "当前记录已不存在！");
                AssertUtils.isTrue(outNotices.getBillStatus() != SgOutConstantsIF.OUT_NOTICES_STATUS_VOID, "单据状态为已作废，不允许退回！");
                AssertUtils.isTrue(outNotices.getBillStatus() != SgOutConstantsIF.OUT_NOTICES_STATUS_ALL, "单据状态为全部出库，不允许退回！");
                AssertUtils.isTrue(outNotices.getBillStatus() != SgOutConstantsIF.OUT_NOTICES_STATUS_PART, "单据状态为部分出库，不允许退回！");
                sourceBillIds.add(outNotices.getSourceBillId());
                //accSuccess++;
            } catch (Exception e) {
                accFailed = errorRecord(objId, e.getMessage(), errorArr, accFailed);
            }
        }

        String orderGoBackErr = "";
        if (CollectionUtils.isNotEmpty(sourceBillIds)) {
            try {
                // start erp.zhang.xiwen.20191213 暂时不用
            /*    ValueHolderV14 orderGoBack = omsOrderGoBackCmd.orderGoBack(sourceBillIds, loginUser);
                if (log.isDebugEnabled()) {
                    log.debug("OmsOrderGoBackCmd. Return:{};", JSONObject.toJSONString(orderGoBack));
                }
                if (orderGoBack.isOK()) {
                    accSuccess = accSuccess + sourceBillIds.size();
                } else {
                    AssertUtils.logAndThrow(orderGoBack.getMessage());
                }*/
                // end erp.zhang.xiwen.20191213 暂时不用
            } catch (Exception e) {
                e.printStackTrace();
                accFailed = accFailed + sourceBillIds.size();
                orderGoBackErr = "订单" + sourceBillIds + "退回失败！" + e.getMessage();
                log.error(orderGoBackErr);
                orderGoBackErr = StorageESUtils.strSubString(orderGoBackErr, SgConstants.SG_COMMON_MAX_QUERY_PAGE_SIZE);
            }
        }

        if (errorArr.size() > 0 || accFailed > 0) {
            SgR3BaseResult baseResult = new SgR3BaseResult();
            baseResult.setDataArr(errorArr);
            v14.setCode(ResultCode.FAIL);
            String msg = "退回成功记录数：" + accSuccess + "，退回失败记录数：" + accFailed;
            if (StringUtils.isNotEmpty(orderGoBackErr)) {
                msg = msg + "," + orderGoBackErr;
            }
            v14.setMessage(msg);
            v14.setData(baseResult);
        } else {
            v14.setCode(ResultCode.SUCCESS);
            v14.setMessage("退回成功记录数：" + accSuccess);
        }
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ".debug, return:" + JSONObject.toJSONString(v14));
        }
        return v14;
    }

    public void checkParams(SgPhyOutNoticesPosRequest request) {
        User loginUser = request.getLoginUser();
        AssertUtils.notNull(loginUser, "请传入用户信息！");
        if (CollectionUtils.isEmpty(request.getIds())) {
            AssertUtils.logAndThrow("请选择需要退回的数据！", loginUser.getLocale());
        }
    }

    public Integer errorRecord(Long mainId, String message, JSONArray errorArr, Integer accFailed) {
        JSONObject errorDate = new JSONObject();
        errorDate.put(R3ParamConstants.CODE, ResultCode.FAIL);
        errorDate.put(R3ParamConstants.OBJID, mainId);
        errorDate.put(R3ParamConstants.MESSAGE, message);
        errorArr.add(errorDate);
        return ++accFailed;
    }
}
