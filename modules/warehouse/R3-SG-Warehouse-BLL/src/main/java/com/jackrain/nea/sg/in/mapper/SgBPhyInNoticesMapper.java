package com.jackrain.nea.sg.in.mapper;

import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.in.common.SgInNoticeConstants;
import com.jackrain.nea.sg.in.model.table.SgBPhyInNotices;
import com.jackrain.nea.web.face.User;
import org.apache.ibatis.annotations.*;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


@Mapper
public interface SgBPhyInNoticesMapper extends ExtentionMapper<SgBPhyInNotices> {


    /**
     * 更新入库通知单（入库结果单专用）
     *
     * @param qtyIn           入库数量
     * @param amtListIn       入库吊牌金额
     * @param amtCostIn       入库成交金额
     * @param qtyInDiff       差异数量
     * @param amtListDiff     差异吊牌金额
     * @param amtCostDiff     差异成交金额
     * @param inTime          入库时间
     * @param id              id
     * @param defaultTotQtyIn 没更新之前的入库数量（用来当作锁）
     * @param pickStatus
     * @return
     */
    @UpdateProvider(type = SgBPhyInNoticesSqlProvider.class, method = "updateWarehousingPassByTotInLok")
    int updateWarehousingPassByTotInLok(@Param("qtyIn") BigDecimal qtyIn,
                                        @Param("amtListIn") BigDecimal amtListIn,
                                        @Param("amtCostIn") BigDecimal amtCostIn,
                                        @Param("qtyInDiff") BigDecimal qtyInDiff,
                                        @Param("amtListDiff") BigDecimal amtListDiff,
                                        @Param("amtCostDiff") BigDecimal amtCostDiff,
                                        @Param("inTime") Date inTime,
                                        @Param("billStatus") Integer billStatus,
                                        @Param("id") Long id, @Param("defaultTotQtyIn") BigDecimal defaultTotQtyIn, @Param("pickStatus") Integer pickStatus);


    /**
     * 分页查询入库通知单
     */
    @Select("SELECT * FROM " + SgConstants.SG_B_PHY_IN_NOTICES + " ORDER BY ID LIMIT #{page} OFFSET #{offset}")
    List<SgBPhyInNotices> selectListByPage(@Param("page") int page, @Param("offset") int offset);

    class SgBPhyInNoticesSqlProvider {
        public String updateWarehousingPassByTotInLok(@Param("qtyIn") BigDecimal qtyIn,
                                                      @Param("amtListIn") BigDecimal amtListIn,
                                                      @Param("amtCostIn") BigDecimal amtCostIn,
                                                      @Param("qtyInDiff") BigDecimal qtyInDiff,
                                                      @Param("amtListDiff") BigDecimal amtListDiff,
                                                      @Param("amtCostDiff") BigDecimal amtCostDiff,
                                                      @Param("inTime") Date inTime,
                                                      @Param("billStatus") Integer billStatus,
                                                      @Param("id") Long id, @Param("defaultTotQtyIn") BigDecimal defaultTotQtyIn,
                                                      @Param("pickStatus") Integer pickStatus) {
            StringBuilder sb = new StringBuilder();
            sb.append("  update sg_b_phy_in_notices\n" +
                    "    set\n" +
                    "    TOT_QTY_IN=#{qtyIn},\n" +
                    "    TOT_AMT_LIST_IN=#{amtListIn},\n" +
                    "    TOT_AMT_COST_IN=#{amtCostIn},\n" +
                    "    TOT_QTY_DIFF=#{qtyInDiff},\n" +
                    "    TOT_AMT_LIST_DIFF=#{amtListDiff},\n" +
                    "    TOT_AMT_COST_DIFF=#{amtCostDiff} ," +
                    "    PICK_STATUS=#{pickStatus} ," +
                    "    in_time=#{inTime} ," +
                    "    BILL_STATUS=#{billStatus} " +
                    "       Where id = #{id} ");
            if (defaultTotQtyIn == null) {
                sb.append("    and TOT_QTY_IN  is null");
            } else {
                sb.append("  and TOT_QTY_IN =#{defaultTotQtyIn}");
            }
            return sb.toString();
        }
    }
}