package com.jackrain.nea.sg.out.mapper;

import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutResultItem;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface SgBPhyOutResultItemMapper extends ExtentionMapper<SgBPhyOutResultItem> {

    @Select("SELECT SUM(qty) AS qty, SUM(price_list) AS price_list, SUM(amt_list_out) AS amt_list_out FROM sg_b_phy_out_result_item WHERE sg_b_phy_out_result_id = #{objId}")
    SgBPhyOutResultItem selectSumAmt(@Param("objId") Long objId);

    /**
     * 分页查询出库结果单明细
     */
    @Select("SELECT * FROM " + SgConstants.SG_B_PHY_OUT_RESULT_ITEM + " ORDER BY ID LIMIT #{page} OFFSET #{offset}")
    List<SgBPhyOutResultItem> selectListByPage(@Param("page") int page, @Param("offset") int offset);

}