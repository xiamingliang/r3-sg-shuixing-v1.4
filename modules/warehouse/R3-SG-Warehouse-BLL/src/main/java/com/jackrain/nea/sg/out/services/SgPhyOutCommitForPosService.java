package com.jackrain.nea.sg.out.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.model.util.ModelUtil;
import com.jackrain.nea.oc.basic.common.OcBasicConstants;
import com.jackrain.nea.r3.mq.util.R3MqSendHelper;
import com.jackrain.nea.redis.config.CusRedisTemplate;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.config.SgStorageBoxConfig;
import com.jackrain.nea.sg.basic.config.SgStorageMqConfig;
import com.jackrain.nea.sg.basic.mapper.CpStoreMapper;
import com.jackrain.nea.sg.basic.model.result.SgStorageUpdateResult;
import com.jackrain.nea.sg.basic.model.table.CpCStore;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.out.common.SgOutConstants;
import com.jackrain.nea.sg.out.common.SgOutConstantsIF;
import com.jackrain.nea.sg.out.model.request.*;
import com.jackrain.nea.sg.send.common.SgSendConstants;
import com.jackrain.nea.sg.send.common.SgSendConstantsIF;
import com.jackrain.nea.sg.send.mapper.SgBSendImpItemMapper;
import com.jackrain.nea.sg.send.mapper.SgBSendItemMapper;
import com.jackrain.nea.sg.send.mapper.SgBSendMapper;
import com.jackrain.nea.sg.send.model.request.SgSendBillSaveRequest;
import com.jackrain.nea.sg.send.model.request.SgSendImpItemSaveRequest;
import com.jackrain.nea.sg.send.model.request.SgSendItemSaveRequest;
import com.jackrain.nea.sg.send.model.request.SgSendSaveRequest;
import com.jackrain.nea.sg.send.model.table.SgBSend;
import com.jackrain.nea.sg.send.model.table.SgBSendImpItem;
import com.jackrain.nea.sg.send.model.table.SgBSendItem;
import com.jackrain.nea.sg.send.services.SgSendStorageService;
import com.jackrain.nea.sg.send.utils.SgStoreESUtils;
import com.jackrain.nea.sg.send.utils.SgStoreUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * @author 舒威
 * @since 2019/11/21
 * create at : 2019/11/21 14:44
 */
@Slf4j
@Component
public class SgPhyOutCommitForPosService {

    @Autowired
    private SgStorageBoxConfig boxConfig;

    @Autowired
    private R3MqSendHelper r3MqSendHelper;

    @Autowired
    private SgStorageMqConfig storageMqConfig;

    public ValueHolderV14 saveAndAuditOutBills(SgPhyOutCommitForPosRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("Start SgPhyOutCommitForPosService.saveAndAuditOutBills ReceiveParams:{};" + JSONObject.toJSONString(request));
        }

        ValueHolderV14 v14 = new ValueHolderV14(ResultCode.SUCCESS, "生成库存单据成功!");
        JSONObject paramObj = new JSONObject();
        paramObj.put("billno", request.getBillNo());
        paramObj.put("id", request.getId());
        try {
            Map<Long, CpCStore> negativeStock = checkParam(request);

            String billNo = request.getBillNo();
            CusRedisTemplate<String, String> redisTemplate = SgStoreUtils.initRedisTemplate();
            String lockKsy = SgConstants.SG_B_SEND + ":" + billNo;
            Boolean ifAbsent = redisTemplate.opsForValue().setIfAbsent(lockKsy, "OK");
            if (ifAbsent != null && ifAbsent) {
                redisTemplate.expire(lockKsy, 30, TimeUnit.MINUTES);
            } else {
                throw new NDSException("零售单" + billNo + "扣减库存中，请稍后重试!");
            }

            saveBills(request, negativeStock, lockKsy);
            paramObj.put("code", ResultCode.SUCCESS);
            paramObj.put("message", "success");
        } catch (Exception e) {
            e.printStackTrace();
            String errMsg = "一键扣减库存失败！" + e.getMessage();
            log.error("零售单" + request.getBillNo() + errMsg);
            paramObj.put("code", ResultCode.FAIL);
            paramObj.put("message", StorageESUtils.strSubString(errMsg, SgConstants.SG_COMMON_STRING_SIZE));
            v14.setCode(ResultCode.FAIL);
            v14.setMessage(StorageESUtils.strSubString(errMsg, SgConstants.SG_COMMON_REMARK_SIZE));
        }

        try {
            if (log.isDebugEnabled()) {
                log.debug("Retail BillNo:{},Start MQ;", request.getBillNo());
            }
            String message = r3MqSendHelper.sendMessage(storageMqConfig.getChannelSynchConfigName(), paramObj, storageMqConfig.getRetailSynchTopic(),
                    SgOutConstants.TAG_RETAIL_TO_NOTICES_STOCK_UPDATE, UUID.randomUUID().toString().replaceAll("-", ""));
            if (log.isDebugEnabled()) {
                log.debug("RetailBillNo:{},MsgId:{};", request.getBillNo(), message);
            }
            v14.setData(message);
        } catch (Exception e) {
            e.printStackTrace();
            log.error(this.getClass().getName() + ".error,发送MQ消息给到【更新零售单更新库存状态】异常：" + e.getMessage(), e);
        }

        if (log.isDebugEnabled()) {
            log.debug("Finish SgPhyOutCommitForPosService.saveAndAuditOutBills Return:{};", JSONObject.toJSONString(v14));
        }
        return v14;
    }

//    @Async("esAsync")
    public void saveBills(SgPhyOutCommitForPosRequest request, Map<Long, CpCStore> negativeStock, String lockKsy) {
        try {
            SgPhyOutNoticesSaveRPCService noticesSaveRPCService = ApplicationContextHandle.getBean(SgPhyOutNoticesSaveRPCService.class);
            SgPhyOutNoticesBillSaveRequest noticesBillSaveRequest = packageOutNoticesBillRequest(request);

            List<SgPhyOutNoticesSaveRPCRequest> requests = Lists.newArrayList();
            SgPhyOutNoticesSaveRPCRequest rpcRequest = new SgPhyOutNoticesSaveRPCRequest();
            rpcRequest.setBillSaveRequest(noticesBillSaveRequest);
            rpcRequest.setSgSendFlag(Boolean.TRUE);
            rpcRequest.setNegativeStock(negativeStock);
            rpcRequest.setCpCStoreId(request.getCpCStoreId());
            rpcRequest.setCpCStoreEcode(request.getCpCStoreEcode());

            rpcRequest.setCpCStoreEname(request.getCpCStoreEname());
            requests.add(rpcRequest);
            ValueHolderV14 v14 = noticesSaveRPCService.batchSaveOutBills(requests);
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getTypeName() + ".batchSaveOutBills:{};" + JSONObject.toJSONString(v14));
            }
            if (!v14.isOK()) {
                AssertUtils.logAndThrow(v14.getMessage());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            CusRedisTemplate<String, String> redisTemplate = SgStoreUtils.initRedisTemplate();
            redisTemplate.delete(lockKsy);
        }

    }


    private SgPhyOutNoticesBillSaveRequest packageOutNoticesBillRequest(SgPhyOutCommitForPosRequest posRequest) {
        SgPhyOutNoticesBillSaveRequest request = new SgPhyOutNoticesBillSaveRequest();
        request.setObjId(-1L);
        SgPhyOutNoticesSaveRequest outNoticesSaveRequest = new SgPhyOutNoticesSaveRequest();
        outNoticesSaveRequest.setCpCPhyWarehouseId(posRequest.getCpCPhyWarehouseId());
        outNoticesSaveRequest.setCpCPhyWarehouseEcode(posRequest.getCpCPhyWarehouseEcode());
        outNoticesSaveRequest.setCpCPhyWarehouseEname(posRequest.getCpCPhyWarehouseEname());
        outNoticesSaveRequest.setRemark(posRequest.getRemark());

        // 收货人信息
        outNoticesSaveRequest.setReceiverName(posRequest.getReceiverName());
        outNoticesSaveRequest.setReceiverMobile(posRequest.getReceiverMobile());
        outNoticesSaveRequest.setReceiverPhone(posRequest.getReceiverPhone());
        outNoticesSaveRequest.setReceiverAddress(posRequest.getReceiverAddress());
        outNoticesSaveRequest.setReceiverZip(posRequest.getReceiverZip());
        // 物流公司信息
        outNoticesSaveRequest.setCpCLogisticsId(posRequest.getCpCLogisticsId());
        outNoticesSaveRequest.setCpCLogisticsEcode(posRequest.getCpCLogisticsEcode());
        outNoticesSaveRequest.setCpCLogisticsEname(posRequest.getCpCLogisticsEname());
        outNoticesSaveRequest.setLogisticNumber(posRequest.getLogisticNumber());

        // 省市区信息
        outNoticesSaveRequest.setCpCRegionProvinceId(posRequest.getCpCRegionProvinceId());
        outNoticesSaveRequest.setCpCRegionProvinceEcode(posRequest.getCpCRegionProvinceEcode());
        outNoticesSaveRequest.setCpCRegionProvinceEname(posRequest.getCpCRegionProvinceEname());
        outNoticesSaveRequest.setCpCRegionCityId(posRequest.getCpCRegionCityId());
        outNoticesSaveRequest.setCpCRegionCityEcode(posRequest.getCpCRegionCityEcode());
        outNoticesSaveRequest.setCpCRegionCityEname(posRequest.getCpCRegionCityEname());
        outNoticesSaveRequest.setCpCRegionAreaId(posRequest.getCpCRegionAreaId());
        outNoticesSaveRequest.setCpCRegionAreaEcode(posRequest.getCpCRegionAreaEcode());
        outNoticesSaveRequest.setCpCRegionAreaEname(posRequest.getCpCRegionAreaEname());

        outNoticesSaveRequest.setCpCSupplierId(posRequest.getCpCSupplierId());
        outNoticesSaveRequest.setCpCCsEcode(posRequest.getCpCCsEcode());
        outNoticesSaveRequest.setCpCCsEname(posRequest.getCpCCsEname());
        //TODO 出库类型？
        outNoticesSaveRequest.setOutType(SgOutConstantsIF.OUT_TYPE_RETAIL);
        outNoticesSaveRequest.setSourceBillType(SgConstantsIF.BILL_TYPE_RETAIL);
        outNoticesSaveRequest.setSourceBillId(posRequest.getId());
        outNoticesSaveRequest.setSourceBillNo(posRequest.getBillNo());
        outNoticesSaveRequest.setBillDate(posRequest.getBillDate());

        request.setOutNoticesRequest(outNoticesSaveRequest);
        request.setLoginUser(posRequest.getLoginUser());
        request.setIsPos(Boolean.TRUE);

        // 箱功能开启
        if (boxConfig.getBoxEnable()) {
            List<SgPhyOutNoticesImpItemSaveRequest> impItemList = Lists.newArrayList();
            for (SgPhyOutCommitItemForPosRequest itemForPosRequest : posRequest.getItems()) {
                SgPhyOutNoticesImpItemSaveRequest itemSaveRequest = new SgPhyOutNoticesImpItemSaveRequest();
                BeanUtils.copyProperties(itemForPosRequest, itemSaveRequest);
                itemSaveRequest.setSourceBillItemId(itemForPosRequest.getId());
                itemSaveRequest.setIsTeus(OcBasicConstants.IS_MATCH_SIZE_N);
                impItemList.add(itemSaveRequest);
            }
            request.setImpItemList(impItemList);
        } else {
            List<SgPhyOutNoticesItemSaveRequest> itemList = Lists.newArrayList();
            for (SgPhyOutCommitItemForPosRequest itemForPosRequest : posRequest.getItems()) {
                SgPhyOutNoticesItemSaveRequest itemSaveRequest = new SgPhyOutNoticesItemSaveRequest();
                BeanUtils.copyProperties(itemForPosRequest, itemSaveRequest);
                itemSaveRequest.setSourceBillItemId(itemForPosRequest.getId());
                itemList.add(itemSaveRequest);
            }
            request.setOutNoticesItemRequests(itemList);
        }
        return request;
    }


    private Map<Long, CpCStore> checkParam(SgPhyOutCommitForPosRequest request) {
        AssertUtils.notNull(request.getLoginUser(), "用户为未登录!");
        AssertUtils.isTrue(StringUtils.isNotEmpty(request.getBillNo()), "零售单号不能为空!");
        AssertUtils.notNull(request.getCpCStoreId(), "门店id不能为空!");
        AssertUtils.notEmpty(request.getItems(), "商品信息不能为空!");

        for (SgPhyOutCommitItemForPosRequest itemForPosRequest : request.getItems()) {
            // 【条码ID】【条码编码】【商品id】【商品编码】【商品名称】【规格id】【规格编码】【规格名称】
            if (itemForPosRequest.getPsCSkuId() == null
                    || StringUtils.isEmpty(itemForPosRequest.getPsCSkuEcode())
                    || itemForPosRequest.getPsCProId() == null
                    || StringUtils.isEmpty(itemForPosRequest.getPsCProEname())
                    || StringUtils.isEmpty(itemForPosRequest.getPsCProEcode())
                    || itemForPosRequest.getPsCSpec1Id() == null
                    || StringUtils.isEmpty(itemForPosRequest.getPsCSpec1Ename())
                    || StringUtils.isEmpty(itemForPosRequest.getPsCSpec1Ecode())
                    || itemForPosRequest.getPsCSpec2Id() == null
                    || StringUtils.isEmpty(itemForPosRequest.getPsCSpec2Ename())
                    || StringUtils.isEmpty(itemForPosRequest.getPsCSpec2Ecode())
                    || itemForPosRequest.getPriceList() == null) {
                AssertUtils.logAndThrow("商品信息条码/商品/规格信息/价格金额不能为空");
            }
        }

        //补充实体仓信息
        CpStoreMapper storeMapper = ApplicationContextHandle.getBean(CpStoreMapper.class);
        CpCStore cpCStore = storeMapper.selectOne(new QueryWrapper<CpCStore>().lambda().eq(CpCStore::getId, request.getCpCStoreId()));
        AssertUtils.notNull(cpCStore, "门店在逻辑仓档案中不存在!");
        request.setCpCPhyWarehouseId(cpCStore.getCpCPhyWarehouseId());
        request.setCpCPhyWarehouseEcode(cpCStore.getCpCPhyWarehouseEcode());
        request.setCpCPhyWarehouseEname(cpCStore.getCpCPhyWarehouseEname());
        if (StringUtils.isEmpty(request.getCpCStoreEcode()) || StringUtils.isEmpty(request.getCpCStoreEname())) {
            request.setCpCStoreEcode(cpCStore.getCpCStoreEcode());
            request.setCpCStoreEname(cpCStore.getCpCStoreEname());
        }
        if (ObjectUtils.isEmpty(request.getBillDate())) {
            request.setBillDate(new Date());
        }

        //负库存控制map
        Map<Long, CpCStore> negativeStock = Maps.newHashMap();
        negativeStock.put(cpCStore.getId(), cpCStore);
        return negativeStock;
    }
}
