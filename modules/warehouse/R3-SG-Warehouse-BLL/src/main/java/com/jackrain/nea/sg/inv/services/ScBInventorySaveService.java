package com.jackrain.nea.sg.inv.services;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.Feature;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.google.common.collect.Lists;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.data.basic.model.request.SkuInfoQueryRequest;
import com.jackrain.nea.data.basic.services.BasicPsQueryService;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.ext.sequence.util.SequenceGenUtil;
import com.jackrain.nea.model.util.ModelUtil;
import com.jackrain.nea.oc.basic.common.OcBasicConstants;
import com.jackrain.nea.ps.api.result.PsCProSkuResult;
import com.jackrain.nea.psext.api.utils.JsonUtils;
import com.jackrain.nea.psext.model.table.PsCTeus;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.inv.constants.SgInvConstants;
import com.jackrain.nea.sg.inv.mapper.ScBInventoryImpItemMapper;
import com.jackrain.nea.sg.inv.mapper.ScBInventoryMapper;
import com.jackrain.nea.sg.inv.model.request.ScBInventoryRequest;
import com.jackrain.nea.sg.inv.model.table.ScBInventory;
import com.jackrain.nea.sg.inv.model.table.ScBInventoryImpItem;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.utils.AssertUtils;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.query.DefaultWebEvent;
import com.jackrain.nea.web.query.QuerySession;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author: 周琳胜
 * @since: 2019/11/13
 * create at : 2019/11/13 11:20
 */
@Slf4j
@Component
public class ScBInventorySaveService {

    public ValueHolder save(QuerySession session) {
        ValueHolder valueHolder = new ValueHolder();
        DefaultWebEvent event = session.getEvent();
        JSONObject param = JSON.parseObject(JSON.toJSONStringWithDateFormat(event.getParameterValue("param"),
                "yyyy-MM-dd HH:mm:ss", SerializerFeature.WriteMapNullValue), Feature.OrderedField);
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + "入参{}", param.toJSONString());
        }
        User user = session.getUser();
        Long id = param.getLong("objid");
        JSONObject fixColumn = param.getJSONObject("fixcolumn");

        if (id == -1) {
            // 新增
            valueHolder = insertMain(fixColumn, user);
        } else {
            // 修改
            if (update(id,fixColumn, user)){
                //推送ES
                String index = SgInvConstants.SC_B_INVENTORY;
                String type = SgInvConstants.SC_B_INVENTORY_IMP_ITEM;
                ScBInventoryMapper scBInventoryMapper = ApplicationContextHandle.getBean(ScBInventoryMapper.class);
                ScBInventoryImpItemMapper impItemMapper = ApplicationContextHandle.getBean(ScBInventoryImpItemMapper.class);
                ScBInventory inventory = scBInventoryMapper.selectById(id);
                List<ScBInventoryImpItem> items = impItemMapper.selectList(new QueryWrapper<ScBInventoryImpItem>().lambda().eq(ScBInventoryImpItem::getScBInventoryId, id));
                if (log.isDebugEnabled()) {
                    log.debug(this.getClass().getName() + ",新增推ES:"
                            + JSON.toJSONString(inventory), JSON.toJSONString(items));
                }
                StorageESUtils.pushESBModelByUpdate(inventory, items, id,
                        null, index, index, type, SgInvConstants.SC_B_INVENTORY_ID, ScBInventory.class, ScBInventoryImpItem.class, false);

                valueHolder.put("code", ResultCode.SUCCESS);
                valueHolder.put("message", "成功！");
            }
        }

        return valueHolder;
    }

    private ValueHolder insertMain(JSONObject object, User user) {
        ScBInventoryMapper scBInventoryMapper = ApplicationContextHandle.getBean(ScBInventoryMapper.class);
        ValueHolder valueHolder = new ValueHolder();
        // 盘点店仓
        Long cPhyWarehouseId = object.getLong("CP_C_PHY_WAREHOUSE_ID");
        // 盘点类型
        Integer inventoryType = object.getInteger("INVENTORY_TYPE");
        // 盘点日期
        Date inventoryDate = object.getDate("INVENTORY_DATE");
        AssertUtils.notNull(cPhyWarehouseId, Resources.getMessage("盘点店仓不允许为空！", user.getLocale()));
        AssertUtils.notNull(inventoryType, Resources.getMessage("盘点类型不允许为空！", user.getLocale()));
        AssertUtils.notNull(inventoryDate, Resources.getMessage("盘点日期不允许为空！", user.getLocale()));

        if (inventoryType == SgInvConstants.PAND_TYPE_QP) {
            // 盘点日期】=当前新增的【盘点日期】并且【盈亏状态】=【已盈亏】并且【盘点类型】=【全盘】
            Integer countYk = scBInventoryMapper.selectCount(new QueryWrapper<ScBInventory>().lambda()
                    .eq(ScBInventory::getInventoryDate, inventoryDate)
                    .eq(ScBInventory::getPolStatus, SgInvConstants.PAND_POL)
                    .eq(ScBInventory::getInventoryType, SgInvConstants.PAND_TYPE_QP));
            AssertUtils.cannot(countYk > 0,Resources.getMessage("盘点日期存在已盈亏的全盘类型盘点单，不允许新增盘点单！", user.getLocale()));

            // 【盘点日期】=当前新增的【盘点日期】并且【盈亏状态】=【未盈亏】，【盘点类型】=【抽盘】、【盘点店仓】=当前新增的【盘点店仓】
            Integer countNotYk = scBInventoryMapper.selectCount(new QueryWrapper<ScBInventory>().lambda()
                    .eq(ScBInventory::getInventoryDate, inventoryDate)
                    .eq(ScBInventory::getPolStatus, SgInvConstants.PAND_UNPOL)
                    .eq(ScBInventory::getInventoryType, SgInvConstants.PAND_TYPE_CP)
                    .eq(ScBInventory::getCpCPhyWarehouseId, cPhyWarehouseId));
            AssertUtils.cannot(countNotYk > 0,Resources.getMessage("存在未盈亏的抽盘类型的盘点单，不允许新增！", user.getLocale()));
        }
        if (inventoryType == SgInvConstants.PAND_TYPE_CP) {
            // 【盘点日期】=当前新增的【盘点日期】并且【盈亏状态】=【未盈亏】，【盘点类型】=【全盘】、【盘点店仓】=当前新增的【盘点店仓】
            Integer count = scBInventoryMapper.selectCount(new QueryWrapper<ScBInventory>().lambda()
                    .eq(ScBInventory::getInventoryDate, inventoryDate)
                    .eq(ScBInventory::getPolStatus, SgInvConstants.PAND_UNPOL)
                    .eq(ScBInventory::getInventoryType, SgInvConstants.PAND_TYPE_QP)
                    .eq(ScBInventory::getCpCPhyWarehouseId, cPhyWarehouseId));
            AssertUtils.cannot(count > 0,Resources.getMessage("存在未盈亏的全盘类型的盘点单，不允许新增！", user.getLocale()));
        }

        ScBInventory scBInventory = new ScBInventory();
        Long id = ModelUtil.getSequence(SgInvConstants.SC_B_INVENTORY);
        scBInventory.setId(id);
        scBInventory.setCpCPhyWarehouseId(cPhyWarehouseId);
        scBInventory.setCpCPhyWarehouseEcode(object.getString("CP_C_PHY_WAREHOUSE_ECODE"));
        scBInventory.setCpCPhyWarehouseEname(object.getString("CP_C_PHY_WAREHOUSE_ENAME"));
        scBInventory.setInventoryType(inventoryType);
        scBInventory.setInventoryDate(inventoryDate);
        scBInventory.setPolStatus(SgInvConstants.PAND_UNPOL);
        scBInventory.setQtyTotPack(BigDecimal.ZERO);
        scBInventory.setQtyTotParts(BigDecimal.ZERO);
        StorageESUtils.setBModelDefalutData(scBInventory, user);
        scBInventory.setOwnerename(user.getEname());
        scBInventory.setModifierename(user.getEname());
        // 单据编号
        JSONObject fix = new JSONObject();
        fix.put(SgInvConstants.SC_B_INVENTORY.toUpperCase(), scBInventory);
        String billNo = SequenceGenUtil.generateSquence(SgInvConstants.SEQ_SC_B_INVENTORY, fix, user.getLocale(), false);
//        String billNo = "ceshidanju";
        scBInventory.setBillNo(billNo);
        int insert = scBInventoryMapper.insert(scBInventory);
        if (insert > 0) {
            JSONObject data = new JSONObject();
            data.put("objid", id);
            data.put("tablename", SgInvConstants.SC_B_INVENTORY.toUpperCase());
            valueHolder.put("code", ResultCode.SUCCESS);
            valueHolder.put("message", "成功！");
            valueHolder.put("data", data);
            //推送ES
            String index = SgInvConstants.SC_B_INVENTORY;
            String type = SgInvConstants.SC_B_INVENTORY_IMP_ITEM;
            ScBInventory inventory = scBInventoryMapper.selectById(id);
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + ",新增推ES:"
                        + JSON.toJSONString(inventory));
            }
            StorageESUtils.pushESBModelByUpdate(inventory, null, id,
                    null, index, index, type, null, ScBInventory.class, ScBInventoryImpItem.class, false);
        } else {
            valueHolder.put("code", ResultCode.FAIL);
            valueHolder.put("message", "新增盘点单失败！");
        }
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + "mainInsertResult:{}", JSONObject.toJSONString(valueHolder));
        }
        return valueHolder;
    }

    private Boolean update(Long objId,JSONObject object, User user) {
        Boolean result;
        ScBInventoryMapper scBInventoryMapper = ApplicationContextHandle.getBean(ScBInventoryMapper.class);
        ScBInventoryImpItemMapper impItemMapper = ApplicationContextHandle.getBean(ScBInventoryImpItemMapper.class);
        ScBInventoryRequest scBInventoryRequest = JsonUtils.jsonParseClass(object, ScBInventoryRequest.class);
        if (scBInventoryRequest == null) {
            throw new NDSException("数据异常！");
        }
        List<ScBInventoryImpItem> resultList = Lists.newArrayList();
        List<ScBInventoryImpItem> insertList = Lists.newArrayList();
        ScBInventory scBInventory = scBInventoryMapper.selectOne(new QueryWrapper<ScBInventory>().lambda().eq(ScBInventory::getId, objId));
        AssertUtils.notNull(scBInventory, "当前记录已不存在，请刷新页面！", user.getLocale());
        Integer polStatus = scBInventory.getPolStatus();
        AssertUtils.cannot(polStatus.equals(SgInvConstants.PAND_POL), Resources.getMessage("当前记录已盈亏，不允许编辑！", user.getLocale()));
        AssertUtils.cannot(polStatus.equals(SgInvConstants.PAND_POL_VOID), Resources.getMessage("当前记录已作废，不允许编辑！", user.getLocale()));
        // 录入明细
        List<ScBInventoryImpItem> scBInventoryImpItems = scBInventoryRequest.getScBInventoryImpItems();
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + "是否更新明细{}", JSONObject.toJSONString(scBInventoryImpItems));
        }
        if (CollectionUtils.isNotEmpty(scBInventoryImpItems)) {
            List<Long> queryCondition = scBInventoryImpItems.stream().filter(o -> o.getId() != null)
                    .map(ScBInventoryImpItem::getId).collect(Collectors.toList());
            if (CollectionUtils.isNotEmpty(queryCondition)) {
                Map<Long, ScBInventoryImpItem> exsistMap =
                        impItemMapper.selectList(new QueryWrapper<ScBInventoryImpItem>().lambda()
                                .in(ScBInventoryImpItem::getId, queryCondition))
                                .stream().collect(Collectors.toMap(ScBInventoryImpItem::getId, o -> o));

                for (ScBInventoryImpItem inventoryImpItem : scBInventoryImpItems) {
                    ScBInventoryImpItem existItem = exsistMap.get(inventoryImpItem.getId());
                    if (existItem != null) {
                        // 已存在的明细直接更新数量
                        ScBInventoryImpItem update = new ScBInventoryImpItem();
                        update.setId(existItem.getId());
                        update.setQty(inventoryImpItem.getQty());
                        StorageESUtils.setBModelDefalutDataByUpdate(update, user);
                        update.setModifierename(user.getEname());
                        impItemMapper.updateById(update);
                    } else {
                        resultList.add(inventoryImpItem);
                    }
                }
            }
            if (CollectionUtils.isNotEmpty(resultList)) {
                List<String> teusEcode = resultList.stream()
                        .filter(o -> StringUtils.isNotEmpty(o.getPsCTeusEcode()) )
                        .map(ScBInventoryImpItem::getPsCTeusEcode).collect(Collectors.toList());
                List<Long> skuIds = resultList.stream()
                        .filter(o -> o.getPsCSkuId() != null )
                        .map(ScBInventoryImpItem::getPsCSkuId).collect(Collectors.toList());


                List<ScBInventoryImpItem> items = impItemMapper.selectList(new QueryWrapper<ScBInventoryImpItem>().lambda()
                        .eq(ScBInventoryImpItem::getScBInventoryId, objId)
                        .and(o -> o.in(CollectionUtils.isNotEmpty(teusEcode), ScBInventoryImpItem::getPsCTeusEcode, teusEcode)
                                .or(CollectionUtils.isNotEmpty(skuIds)).in(ScBInventoryImpItem::getPsCSkuId, skuIds)));
                Map<String, ScBInventoryImpItem> teusMap = items.stream().filter(o -> StringUtils.isNotEmpty(o.getPsCTeusEcode()))
                        .collect(Collectors.toMap(ScBInventoryImpItem::getPsCTeusEcode, o -> o));
                Map<Long, ScBInventoryImpItem> skusMap = items.stream().filter(o -> o.getPsCSkuId() != null)
                        .collect(Collectors.toMap(ScBInventoryImpItem::getPsCSkuId, o -> o));

                resultList.forEach(item ->{
                    // 判断箱map
                    if (StringUtils.isNotEmpty(item.getPsCTeusEcode())) {
                        ScBInventoryImpItem impItem = teusMap.get(item.getPsCTeusEcode());
                        if (impItem != null) {
                            ScBInventoryImpItem update = new ScBInventoryImpItem();
                            update.setId(impItem.getId());
                            BigDecimal qty = item.getId() < 0? item.getQty().add(impItem.getQty()) : item.getQty();
                            update.setQty(qty);
                            StorageESUtils.setBModelDefalutDataByUpdate(update, user);
                            update.setModifierename(user.getEname());
                            impItemMapper.updateById(update);
                        } else {
                            item.setId(ModelUtil.getSequence(SgInvConstants.SC_B_INVENTORY_IMP_ITEM));
                            StorageESUtils.setBModelDefalutData(item, user);
                            item.setScBInventoryId(objId);
                            item.setOwnerename(user.getEname());
                            item.setModifierename(user.getEname());
                            item.setIsTeus(OcBasicConstants.IS_MATCH_SIZE_Y);
                            insertList.add(item);
                        }
                    } else if (item.getPsCSkuId() != null) {
                        // 判断散码map
                        ScBInventoryImpItem impItem = skusMap.get(item.getPsCSkuId());
                        if (impItem != null) {
                            ScBInventoryImpItem update = new ScBInventoryImpItem();
                            StorageESUtils.setBModelDefalutDataByUpdate(update, user);
                            if (log.isDebugEnabled()) {
                                log.debug(this.getClass().getName() + "updateItem:{}", item);
                            }
                            BigDecimal qty = item.getId() < 0? item.getQty().add(impItem.getQty()) : item.getQty();
                            update.setQty(qty);
                            update.setId(impItem.getId());
                            update.setModifierename(user.getEname());
                            impItemMapper.updateById(update);
                        } else {
                            item.setId(ModelUtil.getSequence(SgInvConstants.SC_B_INVENTORY_IMP_ITEM));
                            item.setScBInventoryId(objId);
                            item.setIsTeus(OcBasicConstants.IS_MATCH_SIZE_N);
                            StorageESUtils.setBModelDefalutData(item, user);
                            item.setOwnerename(user.getEname());
                            item.setModifierename(user.getEname());
                            insertList.add(item);
                        }
                    }
                });
                // 新增明细时 信息补充
                if (CollectionUtils.isNotEmpty(insertList)) {
                    List<ScBInventoryImpItem> resultInsert = addBoxInfo(insertList, user);
                    impItemMapper.batchInsert(resultInsert);
                }

            }
        }

        // 更新主表
        result = updataScbInventory(impItemMapper, scBInventoryMapper, objId, user);
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + "updateResult:{}", result);
        }
        return result;
    }

    /**
     * 更新主表
     * @param impItemMapper
     * @param scBInventoryMapper
     * @param objid
     * @param user
     * @return
     */
    private Boolean updataScbInventory(ScBInventoryImpItemMapper impItemMapper, ScBInventoryMapper scBInventoryMapper, Long objid, User user) {
        // 更新主表数据
        List<ScBInventoryImpItem> unTeusImpItems = impItemMapper.selectList(new LambdaQueryWrapper<ScBInventoryImpItem>()
                .select(ScBInventoryImpItem::getQty)
                .eq(ScBInventoryImpItem::getIsTeus, OcBasicConstants.IS_MATCH_SIZE_N)
                .eq(ScBInventoryImpItem::getScBInventoryId, objid));
        List<ScBInventoryImpItem> teusImpItems = impItemMapper.selectList(new LambdaQueryWrapper<ScBInventoryImpItem>()
                .select(ScBInventoryImpItem::getQty)
                .eq(ScBInventoryImpItem::getIsTeus, OcBasicConstants.IS_MATCH_SIZE_Y)
                .eq(ScBInventoryImpItem::getScBInventoryId, objid));
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + "updataScbInventory:{}", JSONObject.toJSONString(unTeusImpItems), JSONObject.toJSONString(teusImpItems));
        }
        // 计算实盘数
        BigDecimal countUnTeus = unTeusImpItems.stream()
                .map(ScBInventoryImpItem::getQty).reduce(BigDecimal.ZERO, (a, b) -> a.add(b));
        BigDecimal countTeus = teusImpItems.stream()
                .map(ScBInventoryImpItem::getQty).reduce(BigDecimal.ZERO, (a, b) -> a.add(b));
        ScBInventory scBInventory = new ScBInventory();
        scBInventory.setId(objid);
        scBInventory.setQtyTotPack(countTeus);
        scBInventory.setQtyTotParts(countUnTeus);
        scBInventory.setModifierid(user.getId().longValue());
        scBInventory.setModifierename(user.getEname());
        scBInventory.setModifiername(user.getName());
        scBInventory.setModifieddate(new Date());
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + "updataParam:{}", JSONObject.toJSONString(scBInventory));
        }
        int result = scBInventoryMapper.updateById(scBInventory);
        return result > 0 ? true : false;
    }

    /**
     * 补充新增明细信息
     * @param itemList
     * @param user
     * @return
     */
    private List<ScBInventoryImpItem> addBoxInfo(List<ScBInventoryImpItem> itemList, User user) {
        BasicPsQueryService queryService = ApplicationContextHandle.getBean(BasicPsQueryService.class);
        List<ScBInventoryImpItem> insertList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(itemList)) {
            List<String> teusEcode = itemList.stream()
                    .filter(o -> StringUtils.isNotEmpty(o.getPsCTeusEcode()))
                    .map(ScBInventoryImpItem::getPsCTeusEcode).collect(Collectors.toList());
            List<String> skuEcode = itemList.stream()
                    .filter(o -> StringUtils.isNotEmpty(o.getPsCSkuEcode() ))
                    .map(ScBInventoryImpItem::getPsCSkuEcode).collect(Collectors.toList());

            HashMap<String, PsCTeus> boxHashMap = new HashMap<>();
            HashMap<String, PsCProSkuResult> queryResult = new HashMap<>();
            SkuInfoQueryRequest request = new SkuInfoQueryRequest();
            if (CollectionUtils.isNotEmpty(teusEcode) || CollectionUtils.isNotEmpty(skuEcode) ) {
                try {
                    if (skuEcode.size() > 0) {
                        request.setSkuEcodeList(skuEcode);
                        queryResult = queryService.getSkuInfoByEcode(request);
                    }
                    if (teusEcode.size() > 0){
                        boxHashMap = queryService.queryTeusInfoByEcode(teusEcode);
                    }
                } catch (Exception e) {
                    AssertUtils.logAndThrow("商品信息获取失败！" + e.getMessage(), user.getLocale());
                }
                for (ScBInventoryImpItem bInventoryImpItem : itemList) {
                    String psCSkuEcode = bInventoryImpItem.getPsCSkuEcode();
                    String psCTeusEcode = bInventoryImpItem.getPsCTeusEcode();
                    if (StringUtils.isNotEmpty(psCSkuEcode)) {
                        PsCProSkuResult skuResult = queryResult.get(psCSkuEcode);
                        AssertUtils.notNull(skuResult, "条码:" + psCSkuEcode + "不存在", user.getLocale());
                        AssertUtils.cannot("N".equalsIgnoreCase(skuResult.getIsactive()), "条码:" + psCSkuEcode + "未启用", user.getLocale());
                        bInventoryImpItem.setPsCSpec1Id(skuResult.getPsCSpec1objId());
                        bInventoryImpItem.setPsCSpec1Ecode(skuResult.getClrsEcode());
                        bInventoryImpItem.setPsCSpec1Ename(skuResult.getClrsEname());
                        bInventoryImpItem.setPsCSpec2Id(skuResult.getPsCSpec2objId());
                        bInventoryImpItem.setPsCSpec2Ename(skuResult.getSizesEname());
                        bInventoryImpItem.setPsCSpec2Ecode(skuResult.getClrsEcode());
                        bInventoryImpItem.setPsCProId(skuResult.getPsCProId());
                        bInventoryImpItem.setPsCProEcode(skuResult.getPsCProEcode());
                        bInventoryImpItem.setPsCProEname(skuResult.getPsCProEname());
                        bInventoryImpItem.setPriceList(Optional.ofNullable(skuResult.getPricelist()).orElse(BigDecimal.ZERO));
                        insertList.add(bInventoryImpItem);
                    } else if (StringUtils.isNotEmpty(psCTeusEcode)) {
                        PsCTeus psCTeus = boxHashMap.get(psCTeusEcode);
                        AssertUtils.notNull(psCTeus, "箱[" + psCTeusEcode + "]不存在", user.getLocale());
                        bInventoryImpItem.setPsCMatchsizeId(psCTeus.getPsCMatchsizeId());
                        bInventoryImpItem.setPsCMatchsizeEcode(psCTeus.getPsCMatchsizeEcode());
                        bInventoryImpItem.setPsCMatchsizeEname(psCTeus.getPsCMatchsizeEname());
                        bInventoryImpItem.setPsCProId(psCTeus.getPsCProId());
                        bInventoryImpItem.setPsCProEcode(psCTeus.getPsCProEcode());
                        bInventoryImpItem.setPsCProEname(psCTeus.getPsCProEname());
                        bInventoryImpItem.setPsCSpec1Id(psCTeus.getPsCSpec1Id());
                        bInventoryImpItem.setPsCSpec1Ecode(psCTeus.getPsCSpec1Ecode());
                        bInventoryImpItem.setPsCSpec1Ename(psCTeus.getPsCSpec1Ename());
                        bInventoryImpItem.setPsCTeusId(psCTeus.getId());
                        bInventoryImpItem.setPriceList(Optional.ofNullable(psCTeus.getPriceList()).orElse(BigDecimal.ZERO));
                        insertList.add(bInventoryImpItem);
                    }
                }
            }
        }
        return insertList;
    }
}
