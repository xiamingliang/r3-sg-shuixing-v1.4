package com.jackrain.nea.sg.unpack.service;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.unpack.common.SgUnpackConstants;
import com.jackrain.nea.sg.unpack.mapper.SgBTeusUnpackItemMapper;
import com.jackrain.nea.sg.unpack.mapper.SgBTeusUnpackMapper;
import com.jackrain.nea.sg.unpack.model.request.SgUnpackForWmsRequest;
import com.jackrain.nea.sg.unpack.model.result.SgUnpackForWmsResult;
import com.jackrain.nea.sg.unpack.model.table.SgBTeusUnpack;
import com.jackrain.nea.sg.unpack.model.table.SgBTeusUnpackItem;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author: 周琳胜
 * @since: 2019/12/4
 * create at : 2019/12/4 11:32
 */
@Slf4j
@Component
public class SgUnpackQueryForWmsService {

    public ValueHolderV14<List<SgUnpackForWmsResult>> querySgUnpack(SgUnpackForWmsRequest request) {
        ValueHolderV14<List<SgUnpackForWmsResult>> v14 = new ValueHolderV14<>();
        List<SgUnpackForWmsResult> resultList = new ArrayList<>();
        if (log.isDebugEnabled()) {
            log.debug("Start SgUnpackQueryForWmsService.querySgUnpack. ReceiveParams:requests:{}"
                    , JSONObject.toJSONString(request));
        }
        Integer isTowms = request.getIsTowms();
        if (isTowms == null) {
            v14.setCode(ResultCode.FAIL);
            v14.setMessage("是否传WMS状态为空！");
            return v14;
        }
        Integer status = request.getStatus();
        if (status == null) {
            v14.setCode(ResultCode.FAIL);
            v14.setMessage("单据状态为空！");
            return v14;
        }
        List<Integer> wmsStatus = request.getWmsStatus();
        if (CollectionUtils.isEmpty(wmsStatus)) {
            v14.setCode(ResultCode.FAIL);
            v14.setMessage("传WMS状态为空！");
            return v14;
        }

        PageHelper.startPage(request.getPageNum(), request.getPageSize());

        SgBTeusUnpackMapper sgBTeusUnpackMapper = ApplicationContextHandle.getBean(SgBTeusUnpackMapper.class);
        LambdaQueryWrapper<SgBTeusUnpack> queryWrapper = new QueryWrapper<SgBTeusUnpack>().lambda()
                .eq(SgBTeusUnpack::getIsTowms, isTowms)
                .in(SgBTeusUnpack::getWmsStatus, wmsStatus)
                .le(SgBTeusUnpack::getWmsFailCount, SgUnpackConstants.FAIL_COUNT)
                .eq(SgBTeusUnpack::getStatus, SgUnpackConstants.BILL_STATUS_NOT_AUDIT);
        List<SgBTeusUnpack> sgBTeusUnpackList = sgBTeusUnpackMapper.selectList(queryWrapper);
        List<Long> longs = sgBTeusUnpackList.stream().map(SgBTeusUnpack::getId).collect(Collectors.toList());

        SgBTeusUnpackItemMapper itemMapper = ApplicationContextHandle.getBean(SgBTeusUnpackItemMapper.class);

        List<SgBTeusUnpackItem> sgBTeusUnpackItems = itemMapper.selectList(new LambdaQueryWrapper<SgBTeusUnpackItem>().in(SgBTeusUnpackItem::getSgBTeusUnpackId, longs));

        Map<Long, List<SgBTeusUnpackItem>> exsistMap = sgBTeusUnpackItems.stream().collect(Collectors.groupingBy(a -> a.getSgBTeusUnpackId()));

        sgBTeusUnpackList.forEach(x -> {
            SgUnpackForWmsResult sgUnpackForWmsResult = new SgUnpackForWmsResult();
            sgUnpackForWmsResult.setBillDate(x.getBillDate());
            sgUnpackForWmsResult.setCpCPhyWarehouseEcode(x.getCpCPhyWarehouseEcode());
            sgUnpackForWmsResult.setEcode(x.getEcode());
            List<SgBTeusUnpackItem> itemList = exsistMap.get(x.getId());
            List<String> stringList = itemList.stream().map(SgBTeusUnpackItem::getPsCTeusEcode).collect(Collectors.toList());
            sgUnpackForWmsResult.setTeusEcodeList(stringList);
            resultList.add(sgUnpackForWmsResult);
        });

        if (CollectionUtils.isEmpty(resultList)) {
            v14.setCode(ResultCode.FAIL);
            v14.setMessage("未查到对应的有效记录！");
            return v14;
        }

        PageInfo<SgUnpackForWmsResult> pageInfo = new PageInfo<>(resultList);
        List<SgUnpackForWmsResult> list = pageInfo.getList();
        v14.setData(list);

        if (log.isDebugEnabled()) {
            log.debug("Finsh SgUnpackQueryForWmsService.querySgUnpack. Result:valueHolderV14:{}"
                    , JSONObject.toJSONString(v14));
        }
        v14.setCode(ResultCode.SUCCESS);
        v14.setMessage("成功！");
        return v14;
    }

}
