package com.jackrain.nea.sg.task;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.es.util.ElasticSearchUtil;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.in.common.SgInConstants;
import com.jackrain.nea.sg.in.mapper.SgBPhyInNoticesItemMapper;
import com.jackrain.nea.sg.in.mapper.SgBPhyInNoticesMapper;
import com.jackrain.nea.sg.in.mapper.SgBPhyInResultItemMapper;
import com.jackrain.nea.sg.in.mapper.SgBPhyInResultMapper;
import com.jackrain.nea.sg.in.model.table.SgBPhyInNotices;
import com.jackrain.nea.sg.in.model.table.SgBPhyInNoticesItem;
import com.jackrain.nea.sg.in.model.table.SgBPhyInResult;
import com.jackrain.nea.sg.in.model.table.SgBPhyInResultItem;
import com.jackrain.nea.sg.out.common.SgOutConstants;
import com.jackrain.nea.sg.out.mapper.SgBPhyOutNoticesItemMapper;
import com.jackrain.nea.sg.out.mapper.SgBPhyOutNoticesMapper;
import com.jackrain.nea.sg.out.mapper.SgBPhyOutResultItemMapper;
import com.jackrain.nea.sg.out.mapper.SgBPhyOutResultMapper;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutNotices;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutNoticesItem;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutResult;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutResultItem;
import com.jackrain.nea.sg.phyadjust.common.SgPhyAdjustConstants;
import com.jackrain.nea.sg.phyadjust.mapper.SgBPhyAdjustItemMapper;
import com.jackrain.nea.sg.phyadjust.mapper.SgBPhyAdjustMapper;
import com.jackrain.nea.sg.phyadjust.model.table.SgBPhyAdjust;
import com.jackrain.nea.sg.phyadjust.model.table.SgBPhyAdjustItem;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

/**
 * @author 舒威
 * @since 2019/7/16
 * create at : 2019/7/16 9:41
 */
@Slf4j
@Component
public class SgWarehousePushESTaskService {

    @Value("${sg.bill.update_es_data_max_threads}")
    private Integer threadsNum;

    /**
     * 刷新实体仓相关业务es
     */
    public ValueHolderV14 pushStoreES(JSONObject params) {
        if (log.isDebugEnabled()) {
            log.debug("Start Batch Push ES Warehouse. ReceiveParams:params:{};", JSONObject.toJSONString(params));
        }

        long startTime = System.currentTimeMillis();
        ValueHolderV14 result = new ValueHolderV14(ResultCode.SUCCESS, "message");
        if (params != null && StringUtils.isNotEmpty(params.getString("table"))) {
            String table = params.getString("table");
            Boolean isDelete = Optional.ofNullable(params.getBoolean("isDelete")).orElse(false);
            log.debug("isDelete:" + isDelete);
            switch (table) {
                case SgConstants.SG_B_PHY_IN_NOTICES:
                    pushESInNotices(isDelete);
                    break;
                case SgConstants.SG_B_PHY_IN_RESULT:
                    pushESInResults(isDelete);
                    break;
                case SgConstants.SG_B_PHY_OUT_NOTICES:
                    pushESOutNotices(isDelete);
                    break;
                case SgConstants.SG_B_PHY_OUT_RESULT:
                    pushESOutResults(isDelete);
                    break;
                case SgConstants.SG_B_PHY_ADJUST:
                    pushESPhyAdjust(isDelete);
                    break;
                default:
                    log.debug(table + "暂不支持推送es!");
            }
        } else {
            pushESInNotices(false);
            pushESInResults(false);
            pushESOutNotices(false);
            pushESOutResults(false);
            pushESPhyAdjust(false);
        }

        if (log.isDebugEnabled()) {
            log.debug("Finish  Batch Push ES Warehouse: spend time:{}ms;", System.currentTimeMillis() - startTime);
        }

        return result;
    }

    /**
     * 刷新库存调整单es
     *
     * @param isDelete
     */
    private void pushESPhyAdjust(Boolean isDelete) {
        if (log.isDebugEnabled()) {
            log.debug("Start Batch Push ES PhyAdjust");
        }

        long startTime = System.currentTimeMillis();
        SgBPhyAdjustMapper adjustMapper = ApplicationContextHandle.getBean(SgBPhyAdjustMapper.class);
        SgBPhyAdjustItemMapper adjustItemMapper = ApplicationContextHandle.getBean(SgBPhyAdjustItemMapper.class);
        String index = SgConstants.SG_B_PHY_ADJUST;
        String childType = SgConstants.SG_B_PHY_ADJUST_ITEM;

        if (isDelete) {
            try {
                ElasticSearchUtil.indexDelete(index);
            } catch (Exception e) {
                e.printStackTrace();
                log.error(index + "index删除失败!");
            }
        }

        int pageSize = threadsNum * 500;
        //获取主表分页数
        int cnt = adjustMapper.selectCount(null);
        int page = cnt / pageSize;
        if (cnt % pageSize != 0) {
            page++;
        }

        for (int i = 0; i < page; i++) {
            //批量推送主表
            List<SgBPhyAdjust> adjusts = adjustMapper.selectListByPage(pageSize, i * pageSize);
            StorageESUtils.batchPushES(adjusts, threadsNum, index,
                    null, null, SgBPhyAdjust.class, SgBPhyAdjustItem.class);
        }

        //获取明细分页数
        int itemCnt = adjustItemMapper.selectCount(null);
        int itemPage = itemCnt / pageSize;
        if (itemCnt % pageSize != 0) {
            itemPage++;
        }

        for (int i = 0; i < itemPage; i++) {
            //批量推送明细表
            List<SgBPhyAdjustItem> senditems = adjustItemMapper.selectListByPage(pageSize, i * pageSize);
            StorageESUtils.batchPushES(senditems, threadsNum, index, childType,
                    SgPhyAdjustConstants.SG_B_PHY_ADJUST_ID.toUpperCase(), SgBPhyAdjust.class, SgBPhyAdjustItem.class);
        }

        if (log.isDebugEnabled()) {
            log.debug("Finish  Batch Push ES PhyAdjust: spend time:{}ms;", System.currentTimeMillis() - startTime);
        }
    }


    /**
     * 刷新出库结果单es
     *
     * @param isDelete
     */
    private void pushESOutResults(Boolean isDelete) {
        if (log.isDebugEnabled()) {
            log.debug("Start Batch Push ES OutResults");
        }

        long startTime = System.currentTimeMillis();
        SgBPhyOutResultMapper outResultMapper = ApplicationContextHandle.getBean(SgBPhyOutResultMapper.class);
        SgBPhyOutResultItemMapper outResultItemMapper = ApplicationContextHandle.getBean(SgBPhyOutResultItemMapper.class);
        String index = SgConstants.SG_B_PHY_OUT_RESULT;
        String childType = SgConstants.SG_B_PHY_OUT_RESULT_ITEM;

        if (isDelete) {
            try {
                ElasticSearchUtil.indexDelete(index);
            } catch (Exception e) {
                e.printStackTrace();
                log.error(index + "index删除失败!");
            }
        }

        int pageSize = threadsNum * 500;
        //获取主表分页数
        int cnt = outResultMapper.selectCount(null);
        int page = cnt / pageSize;
        if (cnt % pageSize != 0) {
            page++;
        }

        for (int i = 0; i < page; i++) {
            //批量推送主表
            List<SgBPhyOutResult> adjusts = outResultMapper.selectListByPage(pageSize, i * pageSize);
            StorageESUtils.batchPushES(adjusts, threadsNum, index,
                    null, null, SgBPhyOutResult.class, SgBPhyOutResultItem.class);
        }

        //获取明细分页数
        int itemCnt = outResultItemMapper.selectCount(null);
        int itemPage = itemCnt / pageSize;
        if (itemCnt % pageSize != 0) {
            itemPage++;
        }

        for (int i = 0; i < itemPage; i++) {
            //批量推送明细表
            List<SgBPhyOutResultItem> senditems = outResultItemMapper.selectListByPage(pageSize, i * pageSize);
            StorageESUtils.batchPushES(senditems, threadsNum, index, childType,
                    SgOutConstants.OUT_RESULT_PAREN_FIELD.toUpperCase(), SgBPhyOutResult.class, SgBPhyOutResultItem.class);
        }

        if (log.isDebugEnabled()) {
            log.debug("Finish  Batch Push ES OutResults: spend time:{}ms;", System.currentTimeMillis() - startTime);
        }
    }

    /**
     * 刷新出库通知单es
     *
     * @param isDelete
     */
    private void pushESOutNotices(Boolean isDelete) {
        if (log.isDebugEnabled()) {
            log.debug("Start Batch Push ES OutNotices");
        }

        long startTime = System.currentTimeMillis();
        SgBPhyOutNoticesMapper noticesMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesMapper.class);
        SgBPhyOutNoticesItemMapper noticesItemMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesItemMapper.class);
        String index = SgConstants.SG_B_PHY_OUT_NOTICES;
        String childType = SgConstants.SG_B_PHY_OUT_NOTICES_ITEM;

        if (isDelete) {
            try {
                ElasticSearchUtil.indexDelete(index);
            } catch (Exception e) {
                e.printStackTrace();
                log.error(index + "index删除失败!");
            }
        }

        int pageSize = threadsNum * 500;
        //获取主表分页数
        int cnt = noticesMapper.selectCount(null);
        int page = cnt / pageSize;
        if (cnt % pageSize != 0) {
            page++;
        }

        for (int i = 0; i < page; i++) {
            //批量推送主表
            List<SgBPhyOutNotices> notices = noticesMapper.selectListByPage(pageSize, i * pageSize);
            StorageESUtils.batchPushES(notices, threadsNum, index,
                    null, null, SgBPhyOutNotices.class, SgBPhyOutNoticesItem.class);
        }

        //获取明细分页数
        int itemCnt = noticesItemMapper.selectCount(null);
        int itemPage = itemCnt / pageSize;
        if (itemCnt % pageSize != 0) {
            itemPage++;
        }

        for (int i = 0; i < itemPage; i++) {
            //批量推送明细表
            List<SgBPhyOutNoticesItem> outNoticesItems = noticesItemMapper.selectListByPage(pageSize, i * pageSize);
            StorageESUtils.batchPushES(outNoticesItems, threadsNum, index, childType,
                    SgOutConstants.OUT_NOTICES_PAREN_FIELD.toUpperCase(), SgBPhyOutNotices.class, SgBPhyOutNoticesItem.class);
        }

        if (log.isDebugEnabled()) {
            log.debug("Finish  Batch Push ES OutNotices: spend time:{}ms;", System.currentTimeMillis() - startTime);
        }
    }

    /**
     * 刷新入库结果单es
     *
     * @param isDelete
     */
    private void pushESInResults(Boolean isDelete) {
        if (log.isDebugEnabled()) {
            log.debug("Start Batch Push ES InResults");
        }

        long startTime = System.currentTimeMillis();
        SgBPhyInResultMapper inResultMapper = ApplicationContextHandle.getBean(SgBPhyInResultMapper.class);
        SgBPhyInResultItemMapper inResultItemMapper = ApplicationContextHandle.getBean(SgBPhyInResultItemMapper.class);
        String index = SgConstants.SG_B_PHY_IN_RESULT;
        String childType = SgConstants.SG_B_PHY_IN_RESULT_ITEM;

        if (isDelete) {
            try {
                ElasticSearchUtil.indexDelete(index);
            } catch (Exception e) {
                e.printStackTrace();
                log.error(index + "index删除失败!");
            }
        }

        int pageSize = threadsNum * 500;
        //获取主表分页数
        int cnt = inResultMapper.selectCount(null);
        int page = cnt / pageSize;
        if (cnt % pageSize != 0) {
            page++;
        }

        for (int i = 0; i < page; i++) {
            //批量推送主表
            List<SgBPhyInResult> receives = inResultMapper.selectListByPage(pageSize, i * pageSize);
            StorageESUtils.batchPushES(receives, threadsNum, index,
                    null, null, SgBPhyInResult.class, SgBPhyInResultItem.class);
        }

        //获取明细分页数
        int itemCnt = inResultItemMapper.selectCount(null);
        int itemPage = itemCnt / pageSize;
        if (itemCnt % pageSize != 0) {
            itemPage++;
        }

        for (int i = 0; i < itemPage; i++) {
            //批量推送明细表
            List<SgBPhyInResultItem> receiveitems = inResultItemMapper.selectListByPage(pageSize, i * pageSize);
            StorageESUtils.batchPushES(receiveitems, threadsNum, index, childType,
                    SgInConstants.SG_B_PHY_IN_RESULT_ID.toUpperCase(), SgBPhyInResult.class, SgBPhyInResultItem.class);
        }

        if (log.isDebugEnabled()) {
            log.debug("Finish  Batch Push ES InResults: spend time:{}ms;", System.currentTimeMillis() - startTime);
        }
    }

    /**
     * 刷新入库通知单单es
     *
     * @param isDelete
     */
    private void pushESInNotices(Boolean isDelete) {
        if (log.isDebugEnabled()) {
            log.debug("Start Batch Push ES InNotices");
        }

        long startTime = System.currentTimeMillis();
        SgBPhyInNoticesMapper inNoticesMapper = ApplicationContextHandle.getBean(SgBPhyInNoticesMapper.class);
        SgBPhyInNoticesItemMapper inNoticesItemMapper = ApplicationContextHandle.getBean(SgBPhyInNoticesItemMapper.class);
        String index = SgConstants.SG_B_PHY_IN_NOTICES;
        String childType = SgConstants.SG_B_PHY_IN_NOTICES_ITEM;

        if (isDelete) {
            try {
                ElasticSearchUtil.indexDelete(index);
            } catch (Exception e) {
                e.printStackTrace();
                log.error(index + "index删除失败!");
            }
        }

        int pageSize = threadsNum * 500;
        //获取主表分页数
        int cnt = inNoticesMapper.selectCount(null);
        int page = cnt / pageSize;
        if (cnt % pageSize != 0) {
            page++;
        }

        for (int i = 0; i < page; i++) {
            //批量推送主表
            List<SgBPhyInNotices> notices = inNoticesMapper.selectListByPage(pageSize, i * pageSize);
            StorageESUtils.batchPushES(notices, threadsNum, index,
                    null, null, SgBPhyInNotices.class, SgBPhyInNoticesItem.class);
        }

        //获取明细分页数
        int itemCnt = inNoticesItemMapper.selectCount(null);
        int itemPage = itemCnt / pageSize;
        if (itemCnt % pageSize != 0) {
            itemPage++;
        }

        for (int i = 0; i < itemPage; i++) {
            //批量推送明细表
            List<SgBPhyInNoticesItem> inNoticesItems = inNoticesItemMapper.selectListByPage(pageSize, i * pageSize);
            StorageESUtils.batchPushES(inNoticesItems, threadsNum, index, childType,
                    SgInConstants.SG_B_PHY_IN_NOTICES_ID.toUpperCase(), SgBPhyInNotices.class, SgBPhyInNoticesItem.class);
        }

        if (log.isDebugEnabled()) {
            log.debug("Finish  Batch Push ES InNotices: spend time:{}ms;", System.currentTimeMillis() - startTime);
        }
    }

}
