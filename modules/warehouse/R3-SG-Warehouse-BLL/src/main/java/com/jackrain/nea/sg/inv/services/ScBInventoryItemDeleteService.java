package com.jackrain.nea.sg.inv.services;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.Feature;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.oc.basic.common.OcBasicConstants;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.inv.constants.SgInvConstants;
import com.jackrain.nea.sg.inv.mapper.ScBInventoryImpItemMapper;
import com.jackrain.nea.sg.inv.mapper.ScBInventoryMapper;
import com.jackrain.nea.sg.inv.model.table.ScBInventory;
import com.jackrain.nea.sg.inv.model.table.ScBInventoryImpItem;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.utils.AssertUtils;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.query.DefaultWebEvent;
import com.jackrain.nea.web.query.QuerySession;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @author: 周琳胜
 * @since: 2019/11/13
 * create at : 2019/11/13 11:20
 */
@Slf4j
@Component
public class ScBInventoryItemDeleteService {

    public ValueHolder delete(QuerySession session) {
        ValueHolder vh = new ValueHolder();
        DefaultWebEvent event = session.getEvent();
        User user = session.getUser();
        JSONObject param = JSON.parseObject(JSON.toJSONStringWithDateFormat(event.getParameterValue("param"),
                "yyyy-MM-dd HH:mm:ss", SerializerFeature.WriteMapNullValue), Feature.OrderedField);
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + "入参{}", param.toJSONString());
        }
        Long objid = param.getLong("objid"); //盘点单ID
        JSONObject data = JSON.parseObject((String) event.getData().get("DATA"));
//        JSONObject data = param.getJSONObject("data");
        JSONArray itemIds = data.getJSONArray(SgInvConstants.SC_B_INVENTORY_IMP_ITEM.toUpperCase());
        String jsonStr = JSONObject.toJSONString(itemIds);
        // 明细id转list
        List<Long> itemList = JSONObject.parseArray(jsonStr,  Long.class);

        ScBInventoryMapper scBInventoryMapper = ApplicationContextHandle.getBean(ScBInventoryMapper.class);
        ScBInventoryImpItemMapper impItemMapper = ApplicationContextHandle.getBean(ScBInventoryImpItemMapper.class);

        if (checkParam(objid, itemList, user, scBInventoryMapper, impItemMapper)) {
            // 批量删除明细
            int i = impItemMapper.deleteBatchIds(itemList);
            //推送ES
            String index = SgInvConstants.SC_B_INVENTORY;
            String type = SgInvConstants.SC_B_INVENTORY_IMP_ITEM;
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + ",删除推ES:"
                        +  itemIds);
            }
            StorageESUtils.pushESBModelByUpdate(null, null, objid,
                    itemIds, index, index, type, null, ScBInventory.class, ScBInventoryImpItem.class, true);
            // 更新主表
            if (updataScbInventory(impItemMapper, scBInventoryMapper, objid, user)) {
                vh.put("code", ResultCode.SUCCESS);
                vh.put("message", "删除成功！");
                return vh;
            }

        }
        return vh;
    }

    /**
     *  校验参数
     * @param objid
     * @param itemIds
     * @param user
     * @param scBInventoryMapper
     * @param impItemMapper
     * @return
     */
    private Boolean checkParam(Long objid, List<Long> itemIds, User user, ScBInventoryMapper scBInventoryMapper, ScBInventoryImpItemMapper impItemMapper) {
        AssertUtils.cannot(CollectionUtils.sizeIsEmpty(itemIds), Resources.getMessage("请选择要删除的明细！", user.getLocale()), user.getLocale());
        ScBInventory scBInventory = scBInventoryMapper.selectOne(new QueryWrapper<ScBInventory>().lambda().eq(ScBInventory::getId, objid));
        AssertUtils.notNull(scBInventory, Resources.getMessage("当前记录已不存在！", user.getLocale()));
        Integer polStatus = scBInventory.getPolStatus();
        AssertUtils.cannot(SgInvConstants.PAND_POL.equals(polStatus), Resources.getMessage("当前记录已盈亏，不允许删除明细！", user.getLocale()));
        AssertUtils.cannot(SgInvConstants.PAND_POL_VOID.equals(polStatus), Resources.getMessage("当前记录已作废，不允许删除明细！", user.getLocale()));

        List<ScBInventoryImpItem> scBInventoryImpItems = impItemMapper.selectBatchIds(itemIds);
        AssertUtils.cannot(CollectionUtils.isEmpty(scBInventoryImpItems) || scBInventoryImpItems.size() < itemIds.size(),
                Resources.getMessage("当前明细记录已不存在！", user.getLocale()));
        return true;
    }

    /**
     * 更新主表
     * @param impItemMapper
     * @param scBInventoryMapper
     * @param objid
     * @param user
     * @return
     */
    private Boolean updataScbInventory(ScBInventoryImpItemMapper impItemMapper, ScBInventoryMapper scBInventoryMapper, Long objid, User user) {
        // 更新主表数据
        List<ScBInventoryImpItem> teusImpItems = impItemMapper.selectList(new LambdaQueryWrapper<ScBInventoryImpItem>()
                .select(ScBInventoryImpItem::getQty)
                .eq(ScBInventoryImpItem::getIsTeus, OcBasicConstants.IS_MATCH_SIZE_Y)
                .eq(ScBInventoryImpItem::getScBInventoryId, objid));
        List<ScBInventoryImpItem> unTeusImpItems = impItemMapper.selectList(new LambdaQueryWrapper<ScBInventoryImpItem>()
                .select(ScBInventoryImpItem::getQty)
                .eq(ScBInventoryImpItem::getIsTeus, OcBasicConstants.IS_MATCH_SIZE_N)
                .eq(ScBInventoryImpItem::getScBInventoryId, objid));
        // 计算总盘数
        // 计算实盘数
        BigDecimal countUnTeus = unTeusImpItems.stream()
                .map(ScBInventoryImpItem::getQty).reduce(BigDecimal.ZERO, (a, b) -> a.add(b));
        BigDecimal countTeus = teusImpItems.stream()
                .map(ScBInventoryImpItem::getQty).reduce(BigDecimal.ZERO, (a, b) -> a.add(b));
        ScBInventory scBInventory = new ScBInventory();
        scBInventory.setId(objid);
        scBInventory.setQtyTotPack(countTeus);
        scBInventory.setQtyTotParts(countUnTeus);
        scBInventory.setModifierid(user.getId().longValue());
        scBInventory.setModifierename(user.getEname());
        scBInventory.setModifiername(user.getName());
        scBInventory.setModifieddate(new Date());
        int result = scBInventoryMapper.updateById(scBInventory);
        return result > 0 ? true : false;
    }
}
