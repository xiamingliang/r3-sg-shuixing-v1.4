package com.jackrain.nea.sg.in.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Maps;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.in.common.SgInConstants;
import com.jackrain.nea.sg.in.common.SgInNoticeConstants;
import com.jackrain.nea.sg.in.mapper.SgBPhyInNoticesImpItemMapper;
import com.jackrain.nea.sg.in.mapper.SgBPhyInNoticesItemMapper;
import com.jackrain.nea.sg.in.mapper.SgBPhyInNoticesMapper;
import com.jackrain.nea.sg.in.mapper.SgBPhyInNoticesTeusItemMapper;
import com.jackrain.nea.sg.in.mapper.SgBPhyInResultMapper;
import com.jackrain.nea.sg.in.model.request.SgBPhyInResultItemSaveRequest;
import com.jackrain.nea.sg.in.model.request.SgBPhyInResultSaveRequest;
import com.jackrain.nea.sg.in.model.request.SgPhyInCommitForPDADetailRequest;
import com.jackrain.nea.sg.in.model.request.SgPhyInCommitForPDARequest;
import com.jackrain.nea.sg.in.model.request.SgPhyInResultBillSaveRequest;
import com.jackrain.nea.sg.in.model.request.SgPhyInResultImpItemSaveRequest;
import com.jackrain.nea.sg.in.model.request.SgPhyInResultTeusItemSaveRequest;
import com.jackrain.nea.sg.in.model.table.SgBPhyInNotices;
import com.jackrain.nea.sg.in.model.table.SgBPhyInNoticesImpItem;
import com.jackrain.nea.sg.in.model.table.SgBPhyInNoticesItem;
import com.jackrain.nea.sg.in.model.table.SgBPhyInNoticesTeusItem;
import com.jackrain.nea.sg.in.model.table.SgBPhyInResult;
import com.jackrain.nea.sg.transfer.common.SgTransferConstantsIF;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author zhoulinsheng
 * @since 2019-10-29
 * create at : 2019-10-29 14:15
 */

@Slf4j
@Component
public class SgPhyInCommitForPDAService {

    /**
     * @return
     */
    public ValueHolderV14 saveAndAuditInResult(SgPhyInCommitForPDARequest request) {
        if (log.isDebugEnabled()) {
            log.debug("Start SgPhyInCommitForPDAService.saveAndAuditInResult. ReceiveParams:requests:{}"
                    , JSONObject.toJSONString(request));
        }
        SgPhyInResultBillSaveRequest sgPhyInResultBillSaveRequest = new SgPhyInResultBillSaveRequest();
        SgBPhyInResultMapper resultMapper = ApplicationContextHandle.getBean(SgBPhyInResultMapper.class);
        SgBPhyInNoticesMapper noticesMapper = ApplicationContextHandle.getBean(SgBPhyInNoticesMapper.class);
        List<Long> teusId = new ArrayList<>();
        List<String> skuEcodeList = new ArrayList<>();

        //参数校验
        AssertUtils.isTrue(request != null, "参数不能为空！");

        User loginUser = request.getLoginUser();
        AssertUtils.notNull(loginUser, "用户信息不能为空！");

        Long sgBPhyInNoticesId = request.getSgBPhyInNoticesId();
        AssertUtils.isTrue(sgBPhyInNoticesId != null, "关联的入库通知单ID不能为空！", loginUser.getLocale());

        SgBPhyInNotices InNotices = noticesMapper.selectOne(new QueryWrapper<SgBPhyInNotices>().lambda()
                .eq(SgBPhyInNotices::getId, sgBPhyInNoticesId)
                .eq(SgBPhyInNotices::getIsactive, SgConstants.IS_ACTIVE_Y));
        AssertUtils.notNull(InNotices, "关联的入库通知单不存在！", loginUser.getLocale());
        Integer billStatus = InNotices.getBillStatus();

        if (billStatus != null && billStatus != SgInNoticeConstants.BILL_STATUS_IN_PENDING && billStatus != SgInNoticeConstants.BILL_STATUS_IN_PART) {
            AssertUtils.logAndThrow("通知单已全部入库,不允许新增结果单!", loginUser.getLocale());
        }
        List<SgBPhyInResult> InResults = resultMapper.selectList(new QueryWrapper<SgBPhyInResult>().lambda()
                .eq(SgBPhyInResult::getSgBPhyInNoticesId, sgBPhyInNoticesId)
                .eq(SgBPhyInResult::getIsLast, SgInConstants.IN_IS_LAST_Y)
                .eq(SgBPhyInResult::getIsactive, SgConstants.IS_ACTIVE_Y));
        AssertUtils.isTrue(CollectionUtils.isEmpty(InResults), "已存在此通知单关联的最后一次入库的入库结果单，不允许保存!", loginUser.getLocale());

        //拆分是散码还是箱,并获取数量
        HashMap<Long, BigDecimal> teusQtyMap = new HashMap<>();
        HashMap<String, BigDecimal> skuQtyMap = new HashMap<>();

        List<SgPhyInCommitForPDADetailRequest> requests = request.getDetailRequests();
        AssertUtils.notNull(requests, "参数不能为空！", loginUser.getLocale());
        for (SgPhyInCommitForPDADetailRequest detailRequest : requests) {
            if (SgTransferConstantsIF.IS_TEUS_Y == detailRequest.getIsTeus()) {
                teusId.add(detailRequest.getPsCTeusId());
                teusQtyMap.put(detailRequest.getPsCTeusId(), detailRequest.getQty());
            } else {
                skuEcodeList.add(detailRequest.getSkuEcode());
                skuQtyMap.put(detailRequest.getSkuEcode(), detailRequest.getQty());
            }
        }


        /**复制入库通知单主表信息到入库结果单*/
        SgBPhyInResultSaveRequest InResultRequest = new SgBPhyInResultSaveRequest();
        BeanUtils.copyProperties(InNotices, InResultRequest);
        InResultRequest.setSgBPhyInNoticesId(InNotices.getId());
        InResultRequest.setSgBPhyInNoticesBillno(InNotices.getBillNo());
        InResultRequest.setInId(loginUser.getId().longValue());
        InResultRequest.setIsLast(request.getIsLast());
        InResultRequest.setInName(loginUser.getName());
        InResultRequest.setInEname(loginUser.getEname());
        InResultRequest.setInTime(new Timestamp(System.currentTimeMillis()));
        InResultRequest.setId(-1L);
//        InResultRequest.setTotQtyIn();
        sgPhyInResultBillSaveRequest.setSgBPhyInResultSaveRequest(InResultRequest);


        /**复制入库通知单录入明细信息到入库结果单*/
        HashMap<Long, BigDecimal> priceListMap = new HashMap<>();
        HashMap<Long, BigDecimal> origTeusQtyMap = new HashMap<>();
        SgBPhyInNoticesImpItemMapper impItemMapper = ApplicationContextHandle.getBean(SgBPhyInNoticesImpItemMapper.class);

        List<SgBPhyInNoticesImpItem> impItems = impItemMapper.selectList(
                new QueryWrapper<SgBPhyInNoticesImpItem>().lambda()
                        .eq(SgBPhyInNoticesImpItem::getSgBPhyInNoticesId, sgBPhyInNoticesId)
                        .and(x -> x.in(SgBPhyInNoticesImpItem::getPsCTeusId, teusId).or().in(SgBPhyInNoticesImpItem::getPsCSkuEcode, skuEcodeList)));
        AssertUtils.isTrue(CollectionUtils.isNotEmpty(impItems), "关联的入库通知单录入明细为空，不允许新增入库结果单！", loginUser.getLocale());

        List<SgPhyInResultImpItemSaveRequest> InResultImpItemRequests = new ArrayList<>();
        impItems.forEach(item -> {
            SgPhyInResultImpItemSaveRequest resultItem = new SgPhyInResultImpItemSaveRequest();
            BeanUtils.copyProperties(item, resultItem);
            if (item.getPsCTeusId() != null) {
                priceListMap.put(item.getPsCTeusId(), item.getPriceList());
                origTeusQtyMap.put(item.getPsCTeusId(), item.getQty());

                //录入明细的箱数量
                resultItem.setQtyIn(teusQtyMap.get(item.getPsCTeusId()));
            } else {
                //录入明细的散码数量
                resultItem.setQtyIn(skuQtyMap.get(item.getPsCSkuEcode()));
            }
            resultItem.setId(-1L);
            InResultImpItemRequests.add(resultItem);
        });
        sgPhyInResultBillSaveRequest.setImpItemList(InResultImpItemRequests);

        /**复制入库通知单箱明细信息到入库结果单*/
        List<SgBPhyInResultItemSaveRequest> InResultItemRequests = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(teusId)) {
            SgBPhyInNoticesTeusItemMapper teusItemMapper = ApplicationContextHandle.getBean(SgBPhyInNoticesTeusItemMapper.class);
            List<SgBPhyInNoticesTeusItem> teusItems = teusItemMapper.selectList(
                    new QueryWrapper<SgBPhyInNoticesTeusItem>().lambda()
                            .eq(SgBPhyInNoticesTeusItem::getSgBPhyInNoticesId, sgBPhyInNoticesId)
                            .in(SgBPhyInNoticesTeusItem::getPsCTeusId, teusId));
            AssertUtils.notNull(teusItems, "未查到对应的入库通知单箱内明细！", loginUser.getLocale());
            List<SgPhyInResultTeusItemSaveRequest> InResultTeusItemRequests = new ArrayList<>();
            teusItems.forEach(item -> {
                //入库通知单箱明细转入库结果单箱明细
                SgPhyInResultTeusItemSaveRequest resultItem = new SgPhyInResultTeusItemSaveRequest();
                BeanUtils.copyProperties(item, resultItem);

                //通过箱内(条码数量/箱数) x 扫描录入的箱数 = 扫描录入的箱内条码数
                BigDecimal skuQty = item.getQty().divide(origTeusQtyMap.get(item.getPsCTeusId()),BigDecimal.ROUND_HALF_UP)
                        .multiply(teusQtyMap.get(item.getPsCTeusId()));
                resultItem.setQty(skuQty);
                resultItem.setId(-1L);
                InResultTeusItemRequests.add(resultItem);

                //入库通知单箱明细转入库结果单条码明细
                SgBPhyInResultItemSaveRequest itemSaveRequest = new SgBPhyInResultItemSaveRequest();
                BeanUtils.copyProperties(item, itemSaveRequest);
                itemSaveRequest.setPriceList(priceListMap.get(item.getPsCTeusId()));
                itemSaveRequest.setId(-1L);
                InResultItemRequests.add(itemSaveRequest);
            });

            sgPhyInResultBillSaveRequest.setTeusItemList(InResultTeusItemRequests);
        }


        /** 复制入库通知单明细信息到入库结果单*/
        SgBPhyInNoticesItemMapper noticesItemMapper = ApplicationContextHandle.getBean(SgBPhyInNoticesItemMapper.class);
        List<SgBPhyInNoticesItem> noticesItems = noticesItemMapper.selectList(
                new QueryWrapper<SgBPhyInNoticesItem>().lambda()
                        .eq(SgBPhyInNoticesItem::getSgBPhyInNoticesId, sgBPhyInNoticesId)
                        .in(SgBPhyInNoticesItem::getPsCSkuEcode, skuEcodeList));
        AssertUtils.isTrue(CollectionUtils.isNotEmpty(noticesItems), "关联的入库通知单明细为空，不允许新增入库结果单！", loginUser.getLocale());

        noticesItems.forEach(item -> {
            SgBPhyInResultItemSaveRequest resultItem = new SgBPhyInResultItemSaveRequest();
            BeanUtils.copyProperties(item, resultItem);
            resultItem.setSgBPhyInNoticesItemId(item.getId());
            resultItem.setQtyIn(item.getQtyDiff());
            resultItem.setAmtListIn(item.getPriceList().multiply(item.getQtyDiff()));
            resultItem.setId(-1L);
            InResultItemRequests.add(resultItem);
        });


        //合并相同sku的条码明细
        Map<Long, SgBPhyInResultItemSaveRequest> skuRequestMap = Maps.newHashMap();
        for (SgBPhyInResultItemSaveRequest itemSaveRequest : InResultItemRequests) {

            Long psCSkuId = itemSaveRequest.getPsCSkuId();
            if (skuRequestMap.containsKey(psCSkuId)) {
                SgBPhyInResultItemSaveRequest orgItemSaveRequest = skuRequestMap.get(psCSkuId);
                BigDecimal orgQty = Optional.ofNullable(orgItemSaveRequest.getQtyIn()).orElse(BigDecimal.ZERO);
                BigDecimal qty = Optional.ofNullable(itemSaveRequest.getQtyIn()).orElse(BigDecimal.ZERO);
                orgItemSaveRequest.setQtyIn(qty.add(orgQty));
            } else {
                skuRequestMap.put(psCSkuId, itemSaveRequest);
            }

        }

        //调用新增并审核入库结果单服务
        sgPhyInResultBillSaveRequest.setItemList(new ArrayList<>(skuRequestMap.values()));
        sgPhyInResultBillSaveRequest.setLoginUser(loginUser);
        sgPhyInResultBillSaveRequest.setObjId(-1L);
        SgPhyInResultSaveAndAuditService saveAndAuditService = ApplicationContextHandle.getBean(SgPhyInResultSaveAndAuditService.class);
        List<SgPhyInResultBillSaveRequest> inResultBillSaveRequests = new ArrayList<>();
        inResultBillSaveRequests.add(sgPhyInResultBillSaveRequest);
        return saveAndAuditService.saveAndAuditBill(inResultBillSaveRequests);
    }
}