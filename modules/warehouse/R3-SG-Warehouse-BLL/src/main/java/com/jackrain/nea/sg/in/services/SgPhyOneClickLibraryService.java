package com.jackrain.nea.sg.in.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.redis.config.CusRedisTemplate;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.in.common.SgInConstants;
import com.jackrain.nea.sg.in.mapper.SgBPhyInNoticesMapper;
import com.jackrain.nea.sg.in.model.request.SgPhyInResultBillSaveRequest;
import com.jackrain.nea.sg.in.model.request.SgPhyOneClickLibraryRequest;
import com.jackrain.nea.sg.in.model.result.ReturnSgPhyInResult;
import com.jackrain.nea.sg.in.model.table.SgBPhyInNotices;
import com.jackrain.nea.sg.send.utils.SgStoreUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @author: 舒威
 * @since: 2019/9/9
 * create at : 2019/9/9 9:58
 */
@Slf4j
@Component
public class SgPhyOneClickLibraryService {

    public ValueHolderV14 oneClickLibrary(SgPhyOneClickLibraryRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("Start SgPhyOneClickLibraryService.oneClickLibrary. ReceiveParams:request={};", JSONObject.toJSONString(request));
        }
        long start = System.currentTimeMillis();

        if (request == null || request.getSourceBillId() == null || request.getSourceBillType() == null || StringUtils.isEmpty(request.getLockKey())) {
            AssertUtils.logAndThrow("参数不能为空!");
        }

        if (request.getLoginUser() == null) {
            AssertUtils.logAndThrow("请先登录!");
        }

        ValueHolderV14 result = library(request);
        if (log.isDebugEnabled()) {
            log.debug("Finish SgPhyOneClickLibraryService.oneClickLibrary. ReceiveParams:result={},spend time={};",
                    JSONObject.toJSONString(result), System.currentTimeMillis() - start);
        }
        return result;
    }

    public ValueHolderV14 library(SgPhyOneClickLibraryRequest request) {
        ValueHolderV14 result = new ValueHolderV14();
        CusRedisTemplate<String, String> redisTemplate = SgStoreUtils.initRedisTemplate();
        String lockKsy = request.getLockKey();
        Long sourceBillId = request.getSourceBillId();
        Integer sourceBillType = request.getSourceBillType();
        User loginUser = request.getLoginUser();
        if (redisTemplate.opsForValue().get(lockKsy) == null) {
            try {
                redisTemplate.opsForValue().set(lockKsy, "OK", 60, TimeUnit.MINUTES);
                SgBPhyInNoticesMapper noticesMapper = ApplicationContextHandle.getBean(SgBPhyInNoticesMapper.class);
                List<SgBPhyInNotices> notices = noticesMapper.selectList(new QueryWrapper<SgBPhyInNotices>().lambda()
                        .eq(SgBPhyInNotices::getSourceBillId, sourceBillId)
                        .eq(SgBPhyInNotices::getSourceBillType, sourceBillType)
                        .eq(SgBPhyInNotices::getIsactive, SgConstants.IS_ACTIVE_Y));
                if (CollectionUtils.isEmpty(notices)) {
                    AssertUtils.logAndThrow("不存在来源单据id[" + sourceBillId + "],来源单据类型[" + sourceBillType + "]的入库通知单!");
                }
                List<Long> ids = notices.stream().map(SgBPhyInNotices::getId).collect(Collectors.toList());
                //新增并审核结果单
                SgPhyOneClickLibraryService clickLibraryService = ApplicationContextHandle.getBean(SgPhyOneClickLibraryService.class);
                clickLibraryService.saveAndAuditInResult(ids, result, loginUser);
            } catch (Exception e) {
                e.printStackTrace();
                result.setCode(ResultCode.FAIL);
                result.setMessage(e.getMessage());
            } finally {
                try {
                    redisTemplate.delete(lockKsy);
                } catch (Exception e) {
                    e.printStackTrace();
                    log.error("一键入库释放锁失败,lockKsy:" + lockKsy + "，请联系管理员！");
                    result.setCode(ResultCode.FAIL);
                    result.setMessage("一键入库释放锁失败,lockKsy:" + lockKsy + "，请联系管理员！" + e.getMessage());
                }
            }
        }
        return result;
    }

    @Transactional(rollbackFor = Exception.class)
    public void saveAndAuditInResult(List<Long> ids, ValueHolderV14 result, User loginUser) {
        SgPhyInNoticesSendMQService mqService = ApplicationContextHandle.getBean(SgPhyInNoticesSendMQService.class);
        List<SgPhyInResultBillSaveRequest> billList = mqService.getBatchNoticesBill(ids,loginUser,true);
        SgPhyInResultSaveAndAuditService resultService = ApplicationContextHandle.getBean(SgPhyInResultSaveAndAuditService.class);
        billList.forEach(o -> o.setIsOneClickLibrary(true));
        ValueHolderV14 resultRet = resultService.saveAndAuditBill(billList);
        if (log.isDebugEnabled()) {
            log.debug("一键入库新增并审核入库结果单出参:{}", JSONObject.toJSONString(resultRet));
        }
        if (resultRet.isOK()) {
            result.setCode(ResultCode.SUCCESS);
            result.setMessage("一键入库成功");
            List<ReturnSgPhyInResult> data = (List<ReturnSgPhyInResult>) resultRet.getData();
            result.setData(data);
            for (ReturnSgPhyInResult datum : data) {
                if (datum.getCode() == ResultCode.FAIL) {
                    AssertUtils.logAndThrow("新增并审核入库结果单失败!" + datum.getMessage());
                }
            }
        } else {
            AssertUtils.logAndThrow("新增并审核入库结果单失败!" + resultRet.getMessage());
        }
    }
}
