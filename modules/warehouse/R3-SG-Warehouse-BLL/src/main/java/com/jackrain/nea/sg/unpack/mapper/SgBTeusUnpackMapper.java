package com.jackrain.nea.sg.unpack.mapper;

import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.unpack.model.table.SgBTeusUnpack;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SgBTeusUnpackMapper extends ExtentionMapper<SgBTeusUnpack> {
}