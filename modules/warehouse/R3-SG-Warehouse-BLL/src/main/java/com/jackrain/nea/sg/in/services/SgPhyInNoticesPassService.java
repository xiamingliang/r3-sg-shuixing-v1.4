package com.jackrain.nea.sg.in.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.model.result.SgR3BaseResult;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.in.common.SgInNoticeConstants;
import com.jackrain.nea.sg.in.mapper.SgBPhyInNoticesItemMapper;
import com.jackrain.nea.sg.in.mapper.SgBPhyInNoticesMapper;
import com.jackrain.nea.sg.in.model.request.SgPhyInNoticesBillPassRequest;
import com.jackrain.nea.sg.in.model.request.SgPhyInNoticesItemRequest;
import com.jackrain.nea.sg.in.model.table.SgBPhyInNotices;
import com.jackrain.nea.sg.in.model.table.SgBPhyInNoticesItem;
import com.jackrain.nea.sg.out.common.SgOutConstantsIF;
import com.jackrain.nea.sg.out.utils.SgWarehouseESUtils;
import com.jackrain.nea.sg.receive.services.SgReceiveSaveService;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * @Author: ChenChen
 * @Description:库审核通过后回写入库通知单Service
 * @Date: Create in 15:10 2019/4/23
 */
@Slf4j
@Component
public class SgPhyInNoticesPassService {


    @Autowired
    private SgReceiveSaveService sgReceiveSaveService;

    @Autowired
    private SgWarehouseESUtils warehouseESUtils;

    /**
     * 入库审核通过后回写入库通知单方法
     *
     * @param noticesBillPassRequest
     * @return
     */
    public ValueHolderV14 updateInPass(SgPhyInNoticesBillPassRequest noticesBillPassRequest) {
        ValueHolderV14<SgR3BaseResult> result = new ValueHolderV14<>();
        try {
            SgPhyInNoticesPassService obj = ApplicationContextHandle.getBean(SgPhyInNoticesPassService.class);
            result = obj.updateNoticeMain(noticesBillPassRequest);
        } catch (Exception e) {
            log.error(this.getClass().getName() + ".error:" + e, e);
            result.setCode(ResultCode.FAIL);
            result.setMessage(e.getMessage());
        }
        return result;
    }


    /**
     * 更新通知单主表
     */
    @Transactional(rollbackFor = Exception.class)
    public ValueHolderV14 updateNoticeMain(SgPhyInNoticesBillPassRequest noticesBillPassRequest) {
        if (log.isDebugEnabled()) {
            log.debug("Start SgPhyInNoticesPassService.updateInPass. ReceiveParams:SgPhyInNoticesBillPassRequest="
                    + JSONObject.toJSONString(noticesBillPassRequest) + ";");
        }
        String resultParms = checkParams(noticesBillPassRequest);
        if (!StringUtils.isEmpty(resultParms)) {
            ValueHolderV14 result = new ValueHolderV14();
            result.setMessage(resultParms);
            result.setCode(ResultCode.FAIL);
            return result;
        }

        ValueHolderV14 result = new ValueHolderV14();
        SgBPhyInNoticesMapper inNoticesMapper = ApplicationContextHandle.getBean(SgBPhyInNoticesMapper.class);
        SgBPhyInNotices notices = inNoticesMapper.selectById(noticesBillPassRequest.getId());
        Integer billStatus = notices.getBillStatus();
        if (billStatus != null) {
            if (billStatus == SgInNoticeConstants.BILL_STATUS_IN_VOID || StringUtils.equals(notices.getIsactive(), SgConstants.IS_ACTIVE_N)) {
                AssertUtils.logAndThrow("当前结果单所关联的通知单已作废，不允许操作！通知单单据编号：" + notices.getBillNo());
            }
            if (billStatus == SgInNoticeConstants.BILL_STATUS_IN_ALL) {
                AssertUtils.logAndThrow("当前结果单所关联的通知单已全部入库，不允许审核！");
            }
        }
        SgBPhyInNotices updateNotice = new SgBPhyInNotices();
        updateNotice.setTotQtyIn(notices.getTotQtyIn() == null ? BigDecimal.ZERO : notices.getTotQtyIn());
        updateNotice.setTotAmtListIn(notices.getTotAmtListIn() == null ? BigDecimal.ZERO : notices.getTotAmtListIn());
        updateNotice.setTotAmtCostIn(notices.getTotAmtCostIn() == null ? BigDecimal.ZERO : notices.getTotAmtCostIn());
        //更新子表并算出总入库数量，总入库吊牌金额，总入库成交金额
        updateNoticeSubRecords(noticesBillPassRequest.getItemList(), noticesBillPassRequest.getLoginUser(), updateNotice);
        updateNotice.setId(notices.getId());
        updateNotice.setInTime(noticesBillPassRequest.getInTime() == null ? new Date() : noticesBillPassRequest.getInTime());
        //总差异数量=总数量-总入库数量
        updateNotice.setTotQtyDiff(notices.getTotQty().subtract(updateNotice.getTotQtyIn()));
        //总差异吊牌金额=总吊牌金额-总入库吊牌金额
        updateNotice.setTotAmtListDiff(notices.getTotAmtList().subtract(updateNotice.getTotAmtListIn()));
        //总差异成交金额= 总成交金额-总入库成交金额
        if (updateNotice.getTotAmtCostIn().compareTo(BigDecimal.ZERO) > 0) {
            updateNotice.setTotAmtCostDiff(notices.getTotAmtCost().subtract(updateNotice.getTotAmtCostIn()));
        }

        SgBPhyInNoticesItemMapper noticesItemMapper = ApplicationContextHandle.getBean(SgBPhyInNoticesItemMapper.class);

        //check入库数量不能大于通知数量
        //2019-08-30 调拨差异(多入)跳过check
        if (notices.getReserveBigint01() == null || notices.getReserveBigint01().compareTo(SgInNoticeConstants.BILL_TYPE_TRANSFER_DIFF) != 0) {
            List<JSONObject> sumGroupBySku = noticesItemMapper.selectSumGroupBySku(notices.getId());
            if (CollectionUtils.isNotEmpty(sumGroupBySku)) {
                if (log.isDebugEnabled()) {
                    log.debug("入库通知明细按条码分组查询结果" + JSONObject.toJSONString(sumGroupBySku));
                }
                for (JSONObject var : sumGroupBySku) {
                    String ps_c_sku_ecode = var.getString("ps_c_sku_ecode");
                    BigDecimal tot_qty = Optional.ofNullable(var.getBigDecimal("tot_qty")).orElse(BigDecimal.ZERO);
                    BigDecimal tot_qty_in = Optional.ofNullable(var.getBigDecimal("tot_qty_in")).orElse(BigDecimal.ZERO);
                    if (tot_qty_in.compareTo(tot_qty) > 0) {
                        AssertUtils.logAndThrow("条码" + ps_c_sku_ecode + "入库数量不能大于通知数量！");
                    }
                }
            }
        }
        updateNotice.setPickStatus(SgOutConstantsIF.PICK_STATUS_ING);
        Long pickOrderId = noticesBillPassRequest.getPickOrderId();
        if (noticesBillPassRequest.getIsLast() != null && noticesBillPassRequest.getIsLast()) {
            updateNotice.setBillStatus(SgInNoticeConstants.BILL_STATUS_IN_ALL);
            if (pickOrderId != null) {
                updateNotice.setPickStatus(SgOutConstantsIF.PICK_STATUS_SUCCESS);
            }
        } else {
            if (updateNotice.getTotQtyDiff().compareTo(BigDecimal.ZERO) <= 0) {
                //全部入库
                updateNotice.setBillStatus(SgInNoticeConstants.BILL_STATUS_IN_ALL);
                if (pickOrderId != null) {
                    updateNotice.setPickStatus(SgOutConstantsIF.PICK_STATUS_SUCCESS);
                }
            } else {
                //部分入库
                updateNotice.setBillStatus(SgInNoticeConstants.BILL_STATUS_IN_PART);
                if (pickOrderId != null) {
                    updateNotice.setPickStatus(SgOutConstantsIF.PICK_STATUS_WAIT);
                }
            }
        }

        //更新入库通知单入库数量，入库金额，差异金额
        int updateResult = inNoticesMapper.updateWarehousingPassByTotInLok(updateNotice.getTotQtyIn()
                , updateNotice.getTotAmtListIn(), updateNotice.getTotAmtCostIn()
                , updateNotice.getTotQtyDiff(), updateNotice.getTotAmtListDiff(), updateNotice.getTotAmtCostDiff()
                , new Date(), updateNotice.getBillStatus(), updateNotice.getId(), notices.getTotQtyIn(),updateNotice.getPickStatus());
       /*   StorageUtils.setBModelDefalutDataByUpdate(notices, loginUser);

              int updateResult = sgBPhyInNoticesMapper.updateById(updateNotice);*/
        if (updateResult <= 0) {
            AssertUtils.logAndThrow("更新入库通知单失败！", noticesBillPassRequest.getLoginUser().getLocale());
        } else {
            //推送es
            warehouseESUtils.pushESByInNotices(updateNotice.getId(), true, false, null, inNoticesMapper, noticesItemMapper);
        }
        result.setCode(ResultCode.SUCCESS);
        result.setMessage("success");
        return result;
    }


    /**
     * 更新通知单组表
     *
     * @param itemList 需要传入：id(明细id),QtyIn(总入库数量),
     * @param user     当前登录用户
     */
    private void updateNoticeSubRecords(List<SgPhyInNoticesItemRequest> itemList, User user, SgBPhyInNotices updateNotice) {
        SgBPhyInNoticesItemMapper noticesItemMapper = ApplicationContextHandle.getBean(SgBPhyInNoticesItemMapper.class);


        BigDecimal totQtyIn = BigDecimal.ZERO;
        BigDecimal totAmtListIn = BigDecimal.ZERO;
        BigDecimal totAmtCostIn = BigDecimal.ZERO;
        for (SgPhyInNoticesItemRequest item : itemList) {
            SgBPhyInNoticesItem update = new SgBPhyInNoticesItem();
            Long noticesItemId = item.getId();
            String ecode = item.getPsCSkuEcode();
            update.setId(noticesItemId);
            update.setPsCSkuEcode(ecode);
            SgBPhyInNoticesItem noticesItem = noticesItemMapper.selectOne(new QueryWrapper<SgBPhyInNoticesItem>().lambda()
                    .eq(SgBPhyInNoticesItem::getPsCSkuEcode, ecode)
                    .eq(SgBPhyInNoticesItem::getSgBPhyInNoticesId, item.getSgBPhyInNoticesId()));
            if (noticesItem == null) {
                AssertUtils.logAndThrow("入库通知单明细不存在！", user.getLocale());
                return;
            }

            BigDecimal qtyIn = item.getQtyIn();
            //通知单明细 - 通知數量、入库数量、吊牌价、成交价、成交金额、吊牌金额
            BigDecimal noticesQty = Optional.ofNullable(noticesItem.getQty()).orElse(BigDecimal.ZERO);
            BigDecimal noticesQtyIn = Optional.ofNullable(noticesItem.getQtyIn()).orElse(BigDecimal.ZERO);
            BigDecimal noticesPriceList = Optional.ofNullable(noticesItem.getPriceList()).orElse(BigDecimal.ZERO);
            BigDecimal noticesPriceCost = Optional.ofNullable(noticesItem.getPriceCost()).orElse(BigDecimal.ZERO);
            BigDecimal noticesAmtCost = Optional.ofNullable(noticesItem.getAmtCost()).orElse(BigDecimal.ZERO);
            BigDecimal noticesAmtList = Optional.ofNullable(noticesItem.getAmtList()).orElse(BigDecimal.ZERO);
            //总入库数量、总入库吊牌金额、总入库成交金额
            totQtyIn = totQtyIn.add(qtyIn);
            totAmtListIn = totAmtListIn.add(qtyIn.multiply(noticesPriceList));
            totAmtCostIn = totAmtCostIn.add(qtyIn.multiply(noticesPriceCost));

            //更新入库数量(累加原数量)
            qtyIn = noticesQtyIn.add(qtyIn);
            update.setQtyIn(qtyIn);

            //入库成交金额
            BigDecimal amtCostIn = qtyIn.multiply(noticesPriceCost);
            update.setAmtCostIn(amtCostIn);

            //差异成交金额= 成交金额-入库成交金额
            BigDecimal amtCostDiff = noticesAmtCost.subtract(amtCostIn);
            update.setAmtCostDiff(amtCostDiff);

            //入库吊牌金额
            BigDecimal amtListIn = qtyIn.multiply(noticesPriceList);
            update.setAmtListIn(amtListIn);

            //差异数量=数量-入库数量
            BigDecimal qtyDiff = noticesQty.subtract(qtyIn);
            update.setQtyDiff(qtyDiff);

            //差异吊牌金额=吊牌金额-入库吊牌金额
            BigDecimal amtListDiff = noticesAmtList.subtract(amtListIn);
            update.setAmtListDiff(amtListDiff);

           /* if (qtyDiff.compareTo(BigDecimal.ZERO) < 0 || amtListDiff.compareTo(BigDecimal.ZERO) < 0) {
                AssertUtils.logAndThrow("更新入库库存失败！", user.getLocale());
                return;
            }*/
            int updateResult = noticesItemMapper.updateWarehousingPassByQtyInLock(qtyIn, amtListIn,
                    amtCostIn, qtyDiff, amtListDiff, amtCostDiff, item.getSgBPhyInNoticesId(),ecode, noticesQtyIn);
            if (updateResult <= 0) {
                AssertUtils.logAndThrow("更新入库通知单明细失败！", user.getLocale());
            }
        }
        updateNotice.setTotQtyIn(updateNotice.getTotQtyIn().add(totQtyIn));
        updateNotice.setTotAmtListIn(updateNotice.getTotAmtListIn().add(totAmtListIn));
        updateNotice.setTotAmtCostIn(updateNotice.getTotAmtCostIn().add(totAmtCostIn));
    }


    /**
     * 参数校验
     */
    private String checkParams(SgPhyInNoticesBillPassRequest noticesBillPassRequest) {
        StringBuilder sb = new StringBuilder();
        Long id = noticesBillPassRequest.getId();
        if (id == null || id <= 0) {
            sb.append("id不能为空！");
        }
        if (noticesBillPassRequest.getLoginUser() == null) {
            sb.append("用户未登录！");
        }
        for (SgPhyInNoticesItemRequest itemRequest : noticesBillPassRequest.getItemList()) {

            if (itemRequest.getQtyIn() == null) {
                sb.append("入库数量不能为空！");
            }
        }
        return sb.toString();

    }
}
