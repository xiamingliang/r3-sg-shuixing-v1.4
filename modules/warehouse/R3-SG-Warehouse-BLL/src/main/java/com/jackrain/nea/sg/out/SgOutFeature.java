package com.jackrain.nea.sg.out;

import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.out.filter.SgOutResultVoidFilter;
import com.jackrain.nea.sg.out.validate.SgPhyOutNoticesPosSaveValidate;
import com.jackrain.nea.tableService.Feature;
import com.jackrain.nea.tableService.constants.Constants;
import com.jackrain.nea.tableService.feature.FeatureAnnotation;
import com.jackrain.nea.util.ApplicationContextHandle;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author leexh
 * @since 2019/5/27 18:02
 */
@FeatureAnnotation(value = "SgOutFeature", description = "出库Feature", isSystem = true)
public class SgOutFeature extends Feature {

    @Autowired
    private SgPhyOutNoticesPosSaveValidate noticesPosSaveValidate;

    @Override
    protected void initialization() {

        // 出库结果单保存
//        SgOutResultSaveFilter outSaveFilter = ApplicationContextHandle.getBean(SgOutResultSaveFilter.class);
//        addFilter(outSaveFilter, (tableName, actionName) -> tableName.equalsIgnoreCase(SgConstants.SG_B_PHY_OUT_RESULT) &&
//                (actionName.equals(Constants.ACTION_SAVE) || actionName.equals(Constants.ACTION_ADD)));


        // 出库结果单作废
        SgOutResultVoidFilter outResultVoidFilter = ApplicationContextHandle.getBean(SgOutResultVoidFilter.class);
        addFilter(outResultVoidFilter, (tableName, actionName) -> tableName.equalsIgnoreCase(SgConstants.SG_B_PHY_OUT_RESULT)
                && (actionName.equals(Constants.ACTION_VOID)));


        addValidator(noticesPosSaveValidate, (tableName, actionName)
                -> (tableName.equalsIgnoreCase(SgConstants.SG_B_PHY_OUT_NOTICES_POS))
                && (actionName.equalsIgnoreCase(Constants.ACTION_SAVE) || actionName.equals(Constants.ACTION_ADD)));
    }
}
