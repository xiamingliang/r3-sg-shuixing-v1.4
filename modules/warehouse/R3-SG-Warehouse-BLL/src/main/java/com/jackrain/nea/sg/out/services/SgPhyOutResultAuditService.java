package com.jackrain.nea.sg.out.services;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Lists;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.model.util.AdParamUtil;
import com.jackrain.nea.r3.mq.util.R3MqSendHelper;
import com.jackrain.nea.redis.config.CusRedisTemplate;
import com.jackrain.nea.redis.util.RedisOpsUtil;
import com.jackrain.nea.sg.basic.api.SgPhyStorageQueryCmd;
import com.jackrain.nea.sg.basic.common.R3ParamConstants;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.config.SgStorageBoxConfig;
import com.jackrain.nea.sg.basic.model.request.SgPhyStorageQueryRequest;
import com.jackrain.nea.sg.basic.model.result.SgR3BaseResult;
import com.jackrain.nea.sg.basic.model.table.SgBPhyStorage;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.basic.utils.StorageMQAsyncUtils;
import com.jackrain.nea.sg.out.common.SgOutConstants;
import com.jackrain.nea.sg.out.common.SgOutConstantsIF;
import com.jackrain.nea.sg.out.config.SgOutMqConfig;
import com.jackrain.nea.sg.out.mapper.SgBPhyOutResultImpItemMapper;
import com.jackrain.nea.sg.out.mapper.SgBPhyOutResultItemMapper;
import com.jackrain.nea.sg.out.mapper.SgBPhyOutResultMapper;
import com.jackrain.nea.sg.out.mapper.SgBPhyOutResultTeusItemMapper;
import com.jackrain.nea.sg.out.model.request.SgOutResultMQRequest;
import com.jackrain.nea.sg.out.model.request.SgPhyOutPickOrderSaveRequest;
import com.jackrain.nea.sg.out.model.request.SgPhyOutResultBillAuditRequest;
import com.jackrain.nea.sg.out.model.request.SgPhyOutResultSaveRequest;
import com.jackrain.nea.sg.out.model.result.SgOutResultQueryResult;
import com.jackrain.nea.sg.out.model.result.SgOutResultSendMsgResult;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutResult;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutResultImpItem;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutResultItem;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutResultTeusItem;
import com.jackrain.nea.sg.out.utils.OutNoticeUtils;
import com.jackrain.nea.sg.out.utils.SgWarehouseESUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.DateUtil;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @author leexh
 * @since 2019/4/25 14:10
 */
@Slf4j
@Component
public class SgPhyOutResultAuditService {

    @Autowired
    private R3MqSendHelper r3MqSendHelper;

    @Autowired
    private SgOutMqConfig mqConfig;

    @Autowired
    private SgStorageBoxConfig storageBoxConfig;

    public ValueHolderV14<SgR3BaseResult> auditSgPhyOutResult(SgPhyOutResultBillAuditRequest request) {
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ".debug, param:" + JSONObject.toJSONString(request));
        }

        ValueHolderV14<SgR3BaseResult> v14 = new ValueHolderV14<>();
        AssertUtils.notNull(request, "参数不能为空！");

        User loginUser = request.getLoginUser();
        AssertUtils.notNull(loginUser, "请传入用户信息！");

        JSONArray ids = new JSONArray();
        if (CollectionUtils.isEmpty(request.getIds())) {
            // 接口调用
            AssertUtils.notEmpty(request.getOutResultRequests(), "参数不能为空！");
            for (SgPhyOutResultSaveRequest outResult : request.getOutResultRequests()) {
                AssertUtils.notNull(outResult.getId(), "出库结果单ID不能为空！", loginUser.getLocale());
                ids.add(outResult.getId());
            }
        } else {
            ids = request.getIds();
        }

        JSONArray errorArr = new JSONArray();
        Integer accSuccess = 0, accFailed = 0;
        for (int i = 0; i < ids.size(); i++) {
            Long objId = ids.getLong(i);
            try {
                SgPhyOutResultAuditService service = ApplicationContextHandle.getBean(SgPhyOutResultAuditService.class);
                service.auditSingleSgOutResult(objId, loginUser, null, null, null);
                accSuccess++;
            } catch (Exception e) {
                accFailed = errorRecord(objId, e.getMessage(), errorArr, accFailed);
            }
        }

        if (errorArr.size() > 0) {
            SgR3BaseResult baseResult = new SgR3BaseResult();
            baseResult.setDataArr(errorArr);
            v14.setCode(ResultCode.FAIL);
            v14.setMessage("审核成功记录数：" + accSuccess + "，审核失败记录数：" + accFailed);
            v14.setData(baseResult);
        } else {
            v14.setCode(ResultCode.SUCCESS);
            v14.setMessage("审核成功记录数：" + accSuccess);
        }
        return v14;
    }

    /**
     * 单个出库结果单审核
     *
     * @param objId                出库结果单ID
     * @param user                 用户信息
     * @param isOneClickOutLibrary 是否一键出入库
     */
    @Transactional(rollbackFor = Exception.class)
    public void auditSingleSgOutResult(Long objId, User user, Date outTime, Boolean isOneClickOutLibrary, Long pickOrderId) {
        if (log.isDebugEnabled()) {
            log.debug("Start auditSingleSgOutResult:objid={},outTime={},isOneClickOutLibrary={},pickOrderId={};",
                    objId, outTime, isOneClickOutLibrary, pickOrderId);
        }

        long startTime = System.currentTimeMillis();
        Boolean boxEnable = storageBoxConfig.getBoxEnable();

        if (boxEnable) {
            SgBPhyOutResultImpItemMapper mapper = ApplicationContextHandle.getBean(SgBPhyOutResultImpItemMapper.class);
            SgPhyOutResultConvertService service = ApplicationContextHandle.getBean(SgPhyOutResultConvertService.class);
            List<SgBPhyOutResultImpItem> sgBPhyOutResultImpItems = mapper.selectList(new QueryWrapper<SgBPhyOutResultImpItem>().lambda().eq(SgBPhyOutResultImpItem::getSgBPhyOutResultId, objId));
            service.impItemConvertTeusMethod(sgBPhyOutResultImpItems, null);
        }

        SgBPhyOutResultMapper resultMapper = ApplicationContextHandle.getBean(SgBPhyOutResultMapper.class);
        SgBPhyOutResultItemMapper resultItemMapper = ApplicationContextHandle.getBean(SgBPhyOutResultItemMapper.class);

        /*1.参数校验*/
        SgOutResultQueryResult checkResult = checkParams(objId, resultMapper, resultItemMapper);
        SgBPhyOutResult outResult = checkResult.getOutResult();
        List<SgBPhyOutResultItem> resultItems = checkResult.getOutResultItems();

        /*2.添加redis锁 30min*/
        Long sourceBillId = outResult.getSourceBillId();
        Integer sourceBillType = outResult.getSourceBillType();
        CusRedisTemplate<String, String> redisTemplate = RedisOpsUtil.getStrRedisTemplate();
        String lockKsy = SgConstants.SG_B_PHY_OUT_RESULT + ":" + sourceBillId + ":" + sourceBillType;
        Boolean blnCanInit = redisTemplate.opsForValue().setIfAbsent(lockKsy, "ok");
        if (blnCanInit != null && blnCanInit) {
            redisTemplate.expire(lockKsy, 30, TimeUnit.MINUTES);
        } else {
            String msg = "当前单据审核中，请稍后重试！来源单据ID：" + sourceBillId + "来源单据类型：" + sourceBillType;
            AssertUtils.logAndThrow(msg);
        }

        try {
            /* 3.更新出库结果单状态、出库人、修改人 */
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            long userId = user.getId().longValue();
            BigDecimal totQtyOut = resultItems.stream().map(SgBPhyOutResultItem::getQty).reduce(BigDecimal.ZERO, BigDecimal::add);
            BigDecimal totAmtListOut = resultItems.stream().map(SgBPhyOutResultItem::getAmtListOut).reduce(BigDecimal.ZERO, BigDecimal::add);
            outResult.setTotQtyOut(totQtyOut);
            outResult.setTotAmtListOut(totAmtListOut);
            outResult.setBillStatus(SgOutConstantsIF.OUT_RESULT_STATUS_AUDIT);
            outResult.setOutId(userId);
            outResult.setOutName(user.getName());
            outResult.setOutEname(user.getEname());
            outResult.setOutTime(outTime != null ? outTime : timestamp);
            StorageESUtils.setBModelDefalutDataByUpdate(outResult, user);
            outResult.setModifierename(user.getEname());
            resultMapper.updateById(outResult);

            /*
             * 4.调库存相关服务：
             * 判断实体仓库存表的【实物库存】是否大于等于0
             * 若否，则判断系统参数【实体仓是否允许负库存】是否勾选
             * 若为否，则结束服务，返回错误信息“实体仓库存不允许为负”
             * 若为是，则继续执行下逻辑。
             * 若是，则继续执行下逻辑
             */
            // 2020-05-29添加逻辑：差异类型的单据允许实体仓负库存
            OutNoticeUtils noticeUtils = ApplicationContextHandle.getBean(OutNoticeUtils.class);
            boolean isDiffDeal = noticeUtils.allowNegative(outResult.getSgBPhyOutNoticesId());

            String sysParam = AdParamUtil.getParam(SgOutConstants.SYSTEM_WAREHOUSE);
            log.debug("来源单据id[" + sourceBillId + "],出库结果单审核是否允许负库存系统参数:" + sysParam);

            // 2020-04-21 23:31修改：pos过来允许负库存
            if (SgConstantsIF.BILL_TYPE_RETAIL == sourceBillType || SgConstantsIF.BILL_TYPE_RETAIL_REF == sourceBillType || isDiffDeal) {
                sysParam = "true";
            }

            if (!isDiffDeal) {
                queryPhysicalStorage(user, outResult, resultItems, sysParam);
            }

            /**
             * 开启箱功能
             */
            List<SgBPhyOutResultTeusItem> resultTeusItems = new ArrayList<>();
            List<SgBPhyOutResultImpItem> resultImpItems = new ArrayList<>();
            if (boxEnable) {
                SgPhyOutNoticesConvertService service = ApplicationContextHandle.getBean(SgPhyOutNoticesConvertService.class);
                // 2020-05-29添加逻辑：差异类型的单据跳过负库存控制
                if (!isDiffDeal) {
                    // 5.1 判断箱的实体库存是否大于等于出库数量
                    SgBPhyOutResultImpItemMapper ImpItemMapper = ApplicationContextHandle.getBean(SgBPhyOutResultImpItemMapper.class);
                    resultImpItems = ImpItemMapper.selectList(
                            new QueryWrapper<SgBPhyOutResultImpItem>().lambda().eq(SgBPhyOutResultImpItem::getSgBPhyOutResultId, objId));

                    if (CollectionUtils.isEmpty(resultImpItems)) {
                        AssertUtils.logAndThrow("出库结果单录入明细为空，不允许操作！", user.getLocale());
                    } else {
                        service.queryTeusPhyStorage(user, outResult, resultImpItems, sysParam);
                    }
                }

                SgBPhyOutResultTeusItemMapper teusItemMapper = ApplicationContextHandle.getBean(SgBPhyOutResultTeusItemMapper.class);
                resultTeusItems = teusItemMapper.selectList(
                        new QueryWrapper<SgBPhyOutResultTeusItem>().lambda().eq(SgBPhyOutResultTeusItem::getSgBPhyOutResultId, objId));
                // 5.2 更新出库通知单录入明细
                service.updateOutNoticesImpItemByResult(user, outResult.getSgBPhyOutNoticesId(), outTime, resultImpItems);
            }

            /* 6 更新出库通知单主表和条码明细 */
            SgPhyOutNoticesSaveService noticesSaveService = ApplicationContextHandle.getBean(SgPhyOutNoticesSaveService.class);
            JSONObject isLastObj = noticesSaveService.updateOutNoticesByResult(user, outResult.getSgBPhyOutNoticesId(),
                    outResult.getIsLast(), resultItems, outTime, outResult, pickOrderId);

            /*
             * 2019-05-30添加逻辑：如果是最后一次出库审核，
             *  则判断除本单外有没有未审核的出库单
             */
            Integer isLast = isLastObj.getInteger("isLast");
            if (isLast == SgOutConstantsIF.OUT_IS_LAST_Y) {
                List<SgBPhyOutResult> outResults = resultMapper.selectList(new QueryWrapper<SgBPhyOutResult>().lambda()
                        .eq(SgBPhyOutResult::getSgBPhyOutNoticesId, outResult.getSgBPhyOutNoticesId())
                        .eq(SgBPhyOutResult::getBillStatus, SgOutConstantsIF.OUT_RESULT_STATUS_WAIT)
                        .eq(SgBPhyOutResult::getIsactive, SgConstants.IS_ACTIVE_Y));
                if (!CollectionUtils.isEmpty(outResults)) {
                    AssertUtils.logAndThrow("当前出库结果单为最后一次出库，存在未审核的出库结果单，不允许审核！");
                }
            }

            /* 6.同步调用逻辑发货单出库服务 */
            SgPhyOutRPCService rpcService = ApplicationContextHandle.getBean(SgPhyOutRPCService.class);
            ValueHolderV14 sendResult;
            if (boxEnable) {
                sendResult = rpcService.sgBSendOut(outResult, resultItems, resultImpItems, resultTeusItems, user, false, isDiffDeal);
            } else {
                sendResult = rpcService.sgBSendOut(outResult, resultItems, null, null, user, false, isDiffDeal);
            }
            if (!sendResult.isOK()) {
                throw new NDSException(Resources.getMessage(sendResult.getMessage(), user.getLocale()));
            }

            /*7.调库存相关服务*/
            ValueHolderV14 storageResult;
            if (boxEnable) {
                storageResult = rpcService.updateSgPhyStorage(outResult, resultItems, resultImpItems, resultTeusItems, user, sysParam);
            } else {
                storageResult = rpcService.updateSgPhyStorage(outResult, resultItems, null, null, user, sysParam);
            }

            if (!storageResult.isOK()) {
                throw new NDSException(Resources.getMessage(storageResult.getMessage(), user.getLocale()));
            }

            //拣货单生成的出库结果单审核时，回写拣货单的来源出库结果单表
            if (pickOrderId != null) {
                SgPhyOutPickOrderSaveService saveService = ApplicationContextHandle.getBean(SgPhyOutPickOrderSaveService.class);
                SgPhyOutPickOrderSaveRequest sgPhyOutPickOrderSaveRequest = new SgPhyOutPickOrderSaveRequest();
                ArrayList<SgPhyOutResultSaveRequest> sgPhyOutResultSaveRequests = new ArrayList<>();
                SgPhyOutResultSaveRequest saveRequest = new SgPhyOutResultSaveRequest();
                BeanUtils.copyProperties(outResult, saveRequest);
                sgPhyOutResultSaveRequests.add(saveRequest);
                sgPhyOutPickOrderSaveRequest.setOutResultList(sgPhyOutResultSaveRequests);
                sgPhyOutPickOrderSaveRequest.setLoginUser(user);
                sgPhyOutPickOrderSaveRequest.setSgPhyBOutPickorderId(pickOrderId);
                saveService.saveSgbBoutPickorderResultItem(sgPhyOutPickOrderSaveRequest);
            }

            /*8.mq通知上游单据*/
            try {
                log.info("来源单据id[" + sourceBillId + "],出库结果单审核开始发送MQ通知对应单据!");
                //分销单据mody-model先修改传结果单id
                if (sourceBillType == SgConstantsIF.BILL_TYPE_SALE ||
                        sourceBillType == SgConstantsIF.BILL_TYPE_SALE_REF ||
                        sourceBillType == SgConstantsIF.BILL_TYPE_PUR_REF ||
                        sourceBillType == SgConstantsIF.BILL_TYPE_TRANSFER) {
                    SgOutResultMQRequest mqRequest = new SgOutResultMQRequest();
                    mqRequest.setId(objId);
                    mqRequest.setLoginUser(user);
                    Boolean flag = Optional.ofNullable(isOneClickOutLibrary).orElse(false);
                    mqRequest.setIsOneClickOutLibrary(flag);
                    String topic = mqConfig.getMqTopic();
                    String configName = mqConfig.getConfigName();
                    String msgKey = outResult.getBillNo() + DateUtil.getDateTime(SgOutConstants.MQ_FORMAT);
                    String body = JSONObject.toJSONString(mqRequest);
                    sendMsgToBill(sourceBillType, topic, configName, msgKey, body);
                } else {
                    ValueHolderV14<SgOutResultSendMsgResult> result = rpcService.getSgPhyOutMQResult(null, outResult, resultItems, resultImpItems, resultTeusItems, isLastObj, user);
                    if (result.isOK()) {
                        String topic = mqConfig.getMqTopic();
                        String configName = mqConfig.getConfigName();
                        String msgKey = outResult.getBillNo() + DateUtil.getDateTime(SgOutConstants.MQ_FORMAT);
                        String body = "";
                        SgOutResultSendMsgResult mqRequest = result.getData();
                        if (sourceBillType == SgConstantsIF.BILL_TYPE_RETAIL) {
                            List<SgOutResultSendMsgResult> mqRequests = Lists.newArrayList();
                            mqRequests.add(mqRequest);
                            body = JSONObject.toJSONString(mqRequests);
                        } else {
                            body = JSONObject.toJSONString(mqRequest);
                        }
                        sendMsgToBill(sourceBillType, topic, configName, msgKey, body);
                    } else {
                        log.error("来源单据id[" + sourceBillId + "],获取mqbody失败!" + result.getMessage());
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                log.error("来源单据id[" + sourceBillId + "],出库结果单审核发送MQ通知对应单据异常:" + e.getMessage(), e);
            }

            //推送es
            SgWarehouseESUtils warehouseESUtils = ApplicationContextHandle.getBean(SgWarehouseESUtils.class);
            warehouseESUtils.pushESByOutResult(objId, false, false, null, resultMapper, null);
            if (log.isDebugEnabled()) {
                log.debug("出库结果单id[" + objId + "]审核完成!spend time:" + (System.currentTimeMillis() - startTime));
            }
        } catch (Exception e) {
            AssertUtils.logAndThrowExtra(e, user.getLocale());
        } finally {
            try {
                redisTemplate.delete(lockKsy);
            } catch (Exception e) {
                log.error(this.getClass().getName() + "redis锁释放失败，请联系管理员！lockKsy：" + lockKsy);
                AssertUtils.logAndThrow("redis锁释放失败，请联系管理员！");
            }
        }
    }

    /**
     * mq通知对应单据
     */
    public void sendMsgToBill(Integer sourceBillType, String topic, String configName, String msgKey, String body) throws Exception {
        StorageMQAsyncUtils asyncUtils = ApplicationContextHandle.getBean(StorageMQAsyncUtils.class);
        switch (sourceBillType) {
            case SgConstantsIF.BILL_TYPE_RETAIL:
                r3MqSendHelper.sendMessage(configName, body, mqConfig.getOmsMqTopic(), SgOutConstants.TAG_OUT_RETAIL_VERIFY_BACK, msgKey);
                break;
            case SgConstantsIF.BILL_TYPE_VIPSHOP:
                r3MqSendHelper.sendMessage(configName, body, mqConfig.getVipMqTopic(), SgOutConstants.TAG_OUT_VIP_VERIFY_BACK, msgKey);
                break;
            case SgConstantsIF.BILL_TYPE_SALE:
                asyncUtils.sendDelayMessage(configName, body, topic, SgOutConstants.TAG_OUT_SALE_VERIFY_BACK, msgKey, 3000L, r3MqSendHelper);
                break;
            case SgConstantsIF.BILL_TYPE_SALE_REF:
                asyncUtils.sendDelayMessage(configName, body, topic, SgOutConstants.TAG_OUT_SALE_REF_VERIFY_BACK, msgKey, 3000L, r3MqSendHelper);
                break;
            case SgConstantsIF.BILL_TYPE_PUR_REF:
                r3MqSendHelper.sendMessage(configName, body, topic, SgOutConstants.TAG_OUT_PURCHASE_REF_VERIFY_BACK, msgKey);
                break;
            case SgConstantsIF.BILL_TYPE_TRANSFER:
                asyncUtils.sendDelayMessage(configName, body, topic, SgOutConstants.TAG_OUT_TRANSFER_VERIFY_BACK, msgKey, 3000L, r3MqSendHelper);
                break;
            default:
                throw new NDSException(Resources.getMessage("SourceBillType Exception:" + sourceBillType));
        }
    }

    /**
     * 判断实体仓库存表的【实物库存】是否大于等于0
     *
     * @param loginUser   用户信息
     * @param outResult   出库结果单
     * @param resultItems 出库结果单明细
     */
    public void queryPhysicalStorage(User loginUser, SgBPhyOutResult outResult, List<SgBPhyOutResultItem> resultItems, String sysParam) {
        SgPhyStorageQueryCmd storageQueryCmd = (SgPhyStorageQueryCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(),
                SgPhyStorageQueryCmd.class.getName(),
                SgConstantsIF.GROUP, SgConstantsIF.VERSION);

        List<Long> wareHouseIds = new ArrayList<>();
        wareHouseIds.add(outResult.getCpCPhyWarehouseId());

        List<SgBPhyStorage> storageList = new ArrayList<>();
        List<String> skuList = resultItems.stream().map(SgBPhyOutResultItem::getPsCSkuEcode).collect(Collectors.toList());
        SgPhyStorageQueryRequest request = new SgPhyStorageQueryRequest();
        request.setPhyWarehouseIds(wareHouseIds);

        int itemNum = skuList.size();
        int pageSize = SgConstants.SG_COMMON_INSERT_PAGE_SIZE;
        int page = itemNum / pageSize;
        if (itemNum % pageSize != 0) page++;
        for (int i = 0; i < page; i++) {
            int startIndex = i * pageSize;
            int endIndex = (i + 1) * pageSize;
            List<String> skuCodes = skuList.subList(startIndex, endIndex < itemNum ? endIndex : itemNum);
            request.setSkuEcodes(skuCodes);
            ValueHolderV14<List<SgBPhyStorage>> v14 = storageQueryCmd.queryPhyStorage(request, loginUser);
            AssertUtils.notNull(v14, "库存查询未知错误！", loginUser.getLocale());
            if (v14.getCode() == ResultCode.FAIL) {
                AssertUtils.logAndThrow("库存查询异常：" + v14.getMessage(), loginUser.getLocale());
            }
            storageList.addAll(v14.getData());
        }

        HashMap<String, BigDecimal> storageSkuMap = new HashMap<>();
        storageList.forEach(storage -> {
            storageSkuMap.put(storage.getPsCSkuEcode(), storage.getQtyStorage());
        });

        /*
         * 2019-06-03添加逻辑：
         *  1.库存查询不存在的sku默认数量为0；
         *  2.判断出库数量是否大于实物库存；
         *  3.系统参数是否允许负库存
         */
        JSONArray errorSku = new JSONArray();
        resultItems.forEach(resultItem -> {
            BigDecimal qtyOut = resultItem.getQty();
            String skuCode = resultItem.getPsCSkuEcode();
            BigDecimal qtyStorage = storageSkuMap.get(skuCode);
            if (qtyStorage == null) {
                qtyStorage = BigDecimal.ZERO;
            }

            // 如果实物库存小于出库数量，则判断是否允许负库存
            if (qtyOut.compareTo(qtyStorage) > 0) {
                if (StringUtils.isEmpty(sysParam) || !Boolean.valueOf(sysParam)) {
                    JSONObject skuInfo = new JSONObject();
                    skuInfo.put("条码", skuCode);
                    skuInfo.put("出库数量", qtyOut);
                    skuInfo.put("库存数量", qtyStorage);
                    errorSku.add(skuInfo);
                }
            }
        });

        if (errorSku.size() > 0) {
            String errorMsg = "实体仓库存不允许为负:" + errorSku.toString();
            log.error(this.getClass().getName() + ",queryPhysicalStorage:" + errorMsg);
            AssertUtils.logAndThrow(errorMsg, loginUser.getLocale());
        }
    }


    /**
     * 参数校验
     */
    public SgOutResultQueryResult checkParams(Long objId, SgBPhyOutResultMapper resultMapper, SgBPhyOutResultItemMapper resultItemMapper) {
        SgOutResultQueryResult checkResult = new SgOutResultQueryResult();
        SgBPhyOutResult outResult = resultMapper.selectById(objId);
        AssertUtils.notNull(outResult, "当前记录已不存在！");

        List<SgBPhyOutResultItem> resultItems = resultItemMapper.selectList(new QueryWrapper<SgBPhyOutResultItem>().lambda()
                .eq(SgBPhyOutResultItem::getSgBPhyOutResultId, objId));
        if (CollectionUtils.isEmpty(resultItems)) {
            AssertUtils.logAndThrow("出库结果单明细为空，不允许操作！");
        }

        Integer billStatus = outResult.getBillStatus();
        if (billStatus != null && billStatus != SgOutConstantsIF.OUT_RESULT_STATUS_WAIT) {
            AssertUtils.logAndThrow("当前出库结果单不是待审核状态，不允许审核！");
        }

        checkResult.setOutResult(outResult);
        checkResult.setOutResultItems(resultItems);
        return checkResult;
    }

    /**
     * 错误信息收集
     *
     * @param mainId    记录ID
     * @param message   错误信息
     * @param errorArr  错误集合
     * @param accFailed 错误数量
     * @return 错误记录数
     */
    public Integer errorRecord(Long mainId, String message, JSONArray errorArr, Integer accFailed) {
        JSONObject errorDate = new JSONObject();
        errorDate.put(R3ParamConstants.CODE, ResultCode.FAIL);
        errorDate.put(R3ParamConstants.OBJID, mainId);
        errorDate.put(R3ParamConstants.MESSAGE, message);
        errorArr.add(errorDate);
        return ++accFailed;
    }
}
