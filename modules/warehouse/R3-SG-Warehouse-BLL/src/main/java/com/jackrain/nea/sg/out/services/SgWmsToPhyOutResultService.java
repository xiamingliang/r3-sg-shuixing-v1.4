package com.jackrain.nea.sg.out.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.out.common.SgOutConstants;
import com.jackrain.nea.sg.out.mapper.SgBWmsToPhyOutResultMapper;
import com.jackrain.nea.sg.out.model.request.SgPhyOutResultBillSaveRequest;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutResult;
import com.jackrain.nea.sg.out.model.table.SgBWmsToPhyOutResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.impl.UserImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author: 舒威
 * @since: 2019/10/15
 * create at : 2019/10/15 21:15
 */
@Slf4j
@Component
public class SgWmsToPhyOutResultService {

    @Autowired
    private SgPhyOutResultSaveWMSService saveWMSService;

    @Autowired
    private SgPhyOutResultAuditWMSService auditWMSService;

    @Autowired
    private SgBWmsToPhyOutResultMapper wmsToPhyOutResultMapper;

    @Value("${sg.bill.wms_to_phy_out_result_max_threads:10}")
    private Integer threadWmsToResult;

    @Value("${sg.bill.wms_to_phy_out_result_max_query_limit:200}")
    private Integer queryCnt;

    public ValueHolderV14 wmsToPhyOutResult(JSONObject param) {
        ValueHolderV14 vh = new ValueHolderV14(ResultCode.SUCCESS, "success");
        List<Integer> status = Lists.newArrayList(SgOutConstants.WMS_TO_PHY_RESULT_STATUS_WAIT, SgOutConstants.WMS_TO_PHY_RESULT_STATUS_FAILED);
        String statusStr = StringUtils.join(status, ",");
        List<SgBWmsToPhyOutResult> wmsToPhyOutResults = wmsToPhyOutResultMapper.selectListByWms(statusStr, queryCnt);
        if (CollectionUtils.isNotEmpty(wmsToPhyOutResults)) {
            log.debug(this.getClass().getName() + ",回传定时任务拉到数据{}条;", wmsToPhyOutResults.size());
            List<Long> ids = wmsToPhyOutResults.stream().map(SgBWmsToPhyOutResult::getId).collect(Collectors.toList());
            SgBWmsToPhyOutResult wmsToPhyOutResult = new SgBWmsToPhyOutResult();
            wmsToPhyOutResult.setStatus(SgOutConstants.WMS_TO_PHY_RESULT_STATUS_PASSING);
            int update = wmsToPhyOutResultMapper.update(wmsToPhyOutResult, new QueryWrapper<SgBWmsToPhyOutResult>().lambda().in(SgBWmsToPhyOutResult::getId, ids));
            if (update != ids.size()) {
                vh.setCode(ResultCode.FAIL);
                vh.setMessage("更新传回传中失败!");
                return vh;
            }
        }
        List<List<SgBWmsToPhyOutResult>> lists = StorageESUtils.averageAssign(wmsToPhyOutResults, threadWmsToResult);
        if (CollectionUtils.isNotEmpty(lists)) {
            for (List<SgBWmsToPhyOutResult> wmsToPhyOutResultList : lists) {
                Runnable task = null;
                task = () -> {
                    if (CollectionUtils.isNotEmpty(wmsToPhyOutResultList)) {
                        //通知单号-中间表数据
                        HashMap<String, SgBWmsToPhyOutResult> map = Maps.newHashMap();
                        List<String> outResultStrList = Lists.newArrayList();

                        for (SgBWmsToPhyOutResult wmsToPhyOutResult : wmsToPhyOutResultList) {
                            map.put(wmsToPhyOutResult.getNoticesBillNo(), wmsToPhyOutResult);
                            outResultStrList.add(wmsToPhyOutResult.getOutResultStr());
                        }

                        List<SgPhyOutResultBillSaveRequest> batchSaveRequest = Lists.newArrayList();
                        for (String outResultStr : outResultStrList) {
                            batchSaveRequest.add(JSONObject.parseObject(outResultStr, SgPhyOutResultBillSaveRequest.class));
                        }

                        HashMap<String, String> err = Maps.newHashMap();
                        //新增成功结果单 id- 通知单billno
                        HashMap<Long, String> resultMap = Maps.newHashMap();

                        //批量新增结果单
                        if (CollectionUtils.isNotEmpty(batchSaveRequest)) {
                            try {
                                long start = System.currentTimeMillis();
                                batchSaveRequest.forEach(o -> o.setLoginUser(getUser()));
                                ValueHolderV14<JSONObject> batchSaveResult = saveWMSService.saveSgPhyOutResultByWms(batchSaveRequest, getUser());
                                log.debug("批量新增结果单完成!vh:{},耗时;{}ms;", JSONObject.toJSONString(batchSaveResult), System.currentTimeMillis() - start);

                                JSONObject data = batchSaveResult.getData();
                                err = (HashMap<String, String>) data.get("error");
                                if (batchSaveResult.isOK()) {
                                    HashMap<Long, JSONObject> dataMap = (HashMap<Long, JSONObject>) data.get("data");
                                    for (JSONObject val : dataMap.values()) {
                                        SgBPhyOutResult result = (SgBPhyOutResult) val.get("result");
                                        resultMap.put(result.getId(), result.getSgBPhyOutNoticesBillno());
                                    }
                                }
                            } catch (Exception e) {
                                String messageExtra = AssertUtils.getMessageExtra("批量新增结果单异常!", e);
                                log.error(messageExtra);
                                String flag = StorageESUtils.strSubString(messageExtra, SgConstants.SG_COMMON_ITEM_INSERT_PAGE_SIZE);
                                //更新失败记录-中间表
                                List<String> billnoStr = Lists.newArrayList();
                                for (String s : map.keySet()) {
                                    billnoStr.add("'" + s + "'");
                                }
                                String join = StringUtils.join(billnoStr, ",");
                                wmsToPhyOutResultMapper.batchUpdateFailedRecord(flag, join);
                            }
                        } else {
                            log.error("中间表參數转换异常!");
                        }

                        //更新成功记录-中间表
                        if (CollectionUtils.isNotEmpty(resultMap.values())) {
                            SgBWmsToPhyOutResult update = new SgBWmsToPhyOutResult();
                            update.setStatus(SgOutConstants.WMS_TO_PHY_RESULT_STATUS_SUCCESS);
                            StorageESUtils.setBModelDefalutDataByUpdate(update, getUser());
                            wmsToPhyOutResultMapper.update(update, new UpdateWrapper<SgBWmsToPhyOutResult>().lambda().in(SgBWmsToPhyOutResult::getNoticesBillNo, resultMap.values()));
                        }

                        //更新失败记录-中间表
                        if (MapUtils.isNotEmpty(err)) {
                            Iterator<String> iterator = err.keySet().iterator();
                            while (iterator.hasNext()) {
                                String billno = iterator.next();
                                String errMsg = err.get(billno);

                                SgBWmsToPhyOutResult wmsToPhyOutResult = map.get(billno);
                                SgBWmsToPhyOutResult update = new SgBWmsToPhyOutResult();
                                update.setStatus(SgOutConstants.WMS_TO_PHY_RESULT_STATUS_FAILED);
                                Integer wmsFailedCount = Optional.ofNullable(wmsToPhyOutResult.getWmsFailedCount()).orElse(0);
                                wmsFailedCount = wmsFailedCount + 1;
                                update.setWmsFailedCount(wmsFailedCount);
                                update.setWmsFailedReason(StorageESUtils.strSubString(errMsg, SgConstants.SG_COMMON_ITEM_INSERT_PAGE_SIZE));
                                StorageESUtils.setBModelDefalutDataByUpdate(update, getUser());
                                wmsToPhyOutResultMapper.update(update,
                                        new UpdateWrapper<SgBWmsToPhyOutResult>().lambda().eq(SgBWmsToPhyOutResult::getNoticesBillNo, billno));
                            }
                        }

                        //审核结果单 - 批量
                        if (CollectionUtils.isNotEmpty(resultMap.keySet())) {
                            long start = System.currentTimeMillis();

                            //审核失败记录
                            HashMap<Long, String> auditErrMap = Maps.newHashMap();

                            //审核成功记录
                            List<Long> auditSuccess = Lists.newArrayList();

                            try {
                                Set<Long> set = resultMap.keySet();
                                List<Long> ids = new ArrayList<>(set);
                                ValueHolderV14<JSONObject> auditWmsResult = auditWMSService.auditSgPhyOutResultByWms(ids, getUser());
                                JSONObject data = auditWmsResult.getData();
                                auditErrMap = (HashMap<Long, String>) data.get("errMap");
                                auditSuccess = (List<Long>) data.get("auditData");
                            } catch (Exception e) {
                                e.printStackTrace();
                                String messageExtra = AssertUtils.getMessageExtra("批量审核结果单异常!", e);
                                log.error(messageExtra);
                                String flag = StorageESUtils.strSubString(messageExtra, SgConstants.SG_COMMON_ITEM_INSERT_PAGE_SIZE);
                                //更新失败记录-中间表
                                List<String> billnoStr = Lists.newArrayList();
                                for (String s : map.keySet()) {
                                    billnoStr.add("'" + s + "'");
                                }
                                String join = StringUtils.join(billnoStr, ",");
                                wmsToPhyOutResultMapper.batchUpdateAutoAuditRecord(flag, join, SgConstants.IS_ACTIVE_N);
                            }

                            //更新成功记录-中间表
                            if (CollectionUtils.isNotEmpty(auditSuccess)) {
                                List<String> auditNoticesNo = Lists.newArrayList();
                                for (Long success : auditSuccess) {
                                    auditNoticesNo.add(resultMap.get(success));
                                }
                                SgBWmsToPhyOutResult update = new SgBWmsToPhyOutResult();
                                update.setIsAutoAudit(SgConstants.IS_ACTIVE_Y);
                                StorageESUtils.setBModelDefalutDataByUpdate(update, getUser());
                                wmsToPhyOutResultMapper.update(update, new UpdateWrapper<SgBWmsToPhyOutResult>().lambda().in(SgBWmsToPhyOutResult::getNoticesBillNo, auditNoticesNo));
                            }

                            //更新审核失败记录-中间表
                            if (MapUtils.isNotEmpty(auditErrMap)) {
                                Iterator<Long> iterator = auditErrMap.keySet().iterator();
                                while (iterator.hasNext()) {
                                    Long resultId = iterator.next();
                                    String errMsg = auditErrMap.get(resultId);
                                    String noticesBillno = resultMap.get(resultId);
                                    SgBWmsToPhyOutResult wmsToPhyOutResult = map.get(noticesBillno);
                                    SgBWmsToPhyOutResult update = new SgBWmsToPhyOutResult();
                                    update.setIsAutoAudit(SgConstants.IS_ACTIVE_N);
                                    Integer autoAuditCnt = Optional.ofNullable(wmsToPhyOutResult.getAutoAuditCnt()).orElse(0);
                                    update.setAutoAuditCnt(autoAuditCnt + 1);
                                    update.setAutoAuditFailedReason(StorageESUtils.strSubString(errMsg, SgConstants.SG_COMMON_ITEM_INSERT_PAGE_SIZE));
                                    StorageESUtils.setBModelDefalutDataByUpdate(update, getUser());
                                    wmsToPhyOutResultMapper.update(update,
                                            new UpdateWrapper<SgBWmsToPhyOutResult>().lambda().eq(SgBWmsToPhyOutResult::getNoticesBillNo, noticesBillno));
                                }
                            }

                            log.debug("批量审核结果单完成!耗时;{}ms;", System.currentTimeMillis() - start);
                        }
                    }
                };
                new Thread(task).start();
            }
        }
        return vh;
    }

    public UserImpl getUser() {
        UserImpl user = new UserImpl();
        user.setClientId(37);
        user.setOrgId(27);
        user.setId(666);
        user.setName("WMS");
        user.setEname("WMS");
        return user;
    }
}
