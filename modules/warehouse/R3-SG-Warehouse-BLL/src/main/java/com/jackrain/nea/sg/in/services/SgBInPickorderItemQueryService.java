package com.jackrain.nea.sg.in.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.in.mapper.*;
import com.jackrain.nea.sg.in.model.request.SgBInPickorderRequest;
import com.jackrain.nea.sg.in.model.result.SgBInPickorderResult;
import com.jackrain.nea.sg.in.model.table.*;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Locale;

/**
 * @author: heliu
 * @since: 2019/11/18
 * create at : 2019/11/18 12:46
 */
@Slf4j
@Component
public class SgBInPickorderItemQueryService {

    /**
     * 定制接口查询入库拣货单明细和装箱明细
     *
     * @param request
     * @return
     */
    public ValueHolderV14<SgBInPickorderResult> querySgBInPickorderItem(SgBInPickorderRequest request) {

        if (log.isDebugEnabled()) {
            log.debug("Finsh SgBInPickorderItemQueryService.querySgBInPickorderItem. Result:valueHolderV14:{},spend time:{}ms"
                    , JSONObject.toJSONString(request));
        }
        ValueHolderV14<SgBInPickorderResult> valueHolderV14 = new ValueHolderV14<>();
        SgBInPickorderResult sgBInPickorderResult = new SgBInPickorderResult();
        SgBInPickorderMapper sgBInPickorderMapper = ApplicationContextHandle.getBean(SgBInPickorderMapper.class);
        SgBInPickorderItemMapper sgBInPickorderItemMapper = ApplicationContextHandle.getBean(SgBInPickorderItemMapper.class);
        SgBInPickorderTeusItemMapper sgBInPickorderTeusItemMapper = ApplicationContextHandle.getBean(SgBInPickorderTeusItemMapper.class);
        SgBInPickorderNoticeItemMapper sgBInPickorderNoticeItemMapper = ApplicationContextHandle.getBean(SgBInPickorderNoticeItemMapper.class);
        SgBInPickorderResultItemMapper sgBInPickorderResultItemMapper = ApplicationContextHandle.getBean(SgBInPickorderResultItemMapper.class);

        //参数校验
        AssertUtils.isTrue(request != null, "入库拣货单参数不能为空！");
        User user = request.getLoginUser();
        AssertUtils.notNull(user, "入库拣货单用户信息不能为空！");
        Locale locale = user.getLocale();
        AssertUtils.notNull(request.getId(), "入库拣货单Id不能为空！", locale);

        //查询入库拣货单
        SgBInPickorder sgBInPickorder = sgBInPickorderMapper.selectOne(new QueryWrapper<SgBInPickorder>().lambda()
                .eq(SgBInPickorder::getId, request.getId())
                .eq(SgBInPickorder::getIsactive, SgConstants.IS_ACTIVE_Y));
        AssertUtils.notNull(sgBInPickorder, "入库拣货单不存在！", locale);
        sgBInPickorderResult.setSgBInPickorder(sgBInPickorder);

        //查询拣货明细
        List<SgBInPickorderItem> sgBInPickorderItemList = sgBInPickorderItemMapper.selectList(new QueryWrapper<SgBInPickorderItem>().lambda()
                .eq(SgBInPickorderItem::getSgBInPickorderId, request.getId())
                .eq(SgBInPickorderItem::getIsactive, SgConstants.IS_ACTIVE_Y));
        sgBInPickorderResult.setSgBInPickorderItemList(sgBInPickorderItemList);

        //查询来源通知单明细
        List<SgBInPickorderNoticeItem> sgBInPickorderNoticeItemList = sgBInPickorderNoticeItemMapper.selectList(new QueryWrapper<SgBInPickorderNoticeItem>().lambda()
                .eq(SgBInPickorderNoticeItem::getSgBInPickorderId, request.getId())
                .eq(SgBInPickorderNoticeItem::getIsactive, SgConstants.IS_ACTIVE_Y));
        sgBInPickorderResult.setSgBInPickorderNoticeItemList(sgBInPickorderNoticeItemList);

        //查询入库结果单明细
        List<SgBInPickorderResultItem> sgBInPickorderResultItemList = sgBInPickorderResultItemMapper.selectList(new QueryWrapper<SgBInPickorderResultItem>().lambda()
                .eq(SgBInPickorderResultItem::getSgBInPickorderId, request.getId())
                .eq(SgBInPickorderResultItem::getIsactive, SgConstants.IS_ACTIVE_Y));
        sgBInPickorderResult.setSgBInPickorderResultItemList(sgBInPickorderResultItemList);

        //查询装箱明细
        List<SgBInPickorderTeusItem> sgBInPickorderTeusItemList = sgBInPickorderTeusItemMapper.selectList(new QueryWrapper<SgBInPickorderTeusItem>().lambda()
                .eq(SgBInPickorderTeusItem::getSgBInPickorderId, request.getId())
                .eq(SgBInPickorderTeusItem::getIsactive, SgConstants.IS_ACTIVE_Y));
        sgBInPickorderResult.setSgBInPickorderTeusItemList(sgBInPickorderTeusItemList);

        valueHolderV14.setCode(ResultCode.SUCCESS);
        valueHolderV14.setMessage("查询成功");
        valueHolderV14.setData(sgBInPickorderResult);

        if (log.isDebugEnabled()) {
            log.debug("Finsh SgBInPickorderItemQueryService.querySgBInPickorderItem. Result:valueHolderV14:{},spend time:{}ms"
                    , valueHolderV14.toJSONObject());
        }
        return valueHolderV14;
    }


    public SgBInPickorder queryPickOrderBySourceBillNo(String sourceBillNo) {
        SgBInPickorderMapper mapper = ApplicationContextHandle.getBean(SgBInPickorderMapper.class);
        return mapper.selectOne(new QueryWrapper<SgBInPickorder>().lambda().eq(SgBInPickorder::getSourceBillNo, sourceBillNo));
    }
}