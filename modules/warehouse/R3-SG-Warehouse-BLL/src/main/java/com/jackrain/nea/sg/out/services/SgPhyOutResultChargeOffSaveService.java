package com.jackrain.nea.sg.out.services;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.sg.basic.common.R3ParamConstants;
import com.jackrain.nea.sg.basic.model.result.SgR3BaseResult;
import com.jackrain.nea.sg.out.mapper.SgBPhyOutResultItemMapper;
import com.jackrain.nea.sg.out.mapper.SgBPhyOutResultMapper;
import com.jackrain.nea.sg.out.model.request.SgPhyOutResultBillSaveRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author 舒威
 * @since 2019/12/12
 * create at : 2019/12/12 21:02
 */
@Slf4j
@Component
public class SgPhyOutResultChargeOffSaveService {

    @Autowired
    private SgPhyOutResultSaveService outResultSaveService;

    @Autowired
    private SgPhyOutResultSaveAndAuditService outResultSaveAndAuditService;

    @Autowired
    private SgBPhyOutResultMapper resultMapper;

    @Autowired
    private SgBPhyOutResultItemMapper resultItemMapper;

    public ValueHolderV14<SgR3BaseResult> saveSgPhyOutResult(SgPhyOutResultBillSaveRequest request) {
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ".debug, param:" + JSONObject.toJSONString(request));
        }
        outResultSaveService.checkParams(request);
        ValueHolderV14<SgR3BaseResult> saveResult = request.getObjId() < 0 ?
                outResultSaveService.insertSgOutResult(request, resultMapper, resultItemMapper) :
                outResultSaveService.updateSgOutResult(request, resultMapper, resultItemMapper);
        if (saveResult.isOK()) {
            SgR3BaseResult data = saveResult.getData();
            Long objId = data.getDataJo().getLong(R3ParamConstants.OBJID);
            // 新增多包裹明细
            outResultSaveAndAuditService.addOutDelivery(request.getLoginUser(), request.getOutDeliverySaveRequests(), objId, request.getLoginUser().getLocale());
        }
        return saveResult;
    }
}
