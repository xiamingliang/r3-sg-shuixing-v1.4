package com.jackrain.nea.sg.unpack.filter;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.model.util.ModelUtil;
import com.jackrain.nea.oc.basic.model.request.QueryBoxRequest;
import com.jackrain.nea.oc.basic.services.QueryBoxItemsService;
import com.jackrain.nea.psext.api.PsCTeusSaveCmd;
import com.jackrain.nea.psext.model.table.PsCTeusItem;
import com.jackrain.nea.psext.request.PsTeusUpdateRequest;
import com.jackrain.nea.sg.basic.utils.StorageUtils;
import com.jackrain.nea.sg.phyadjust.common.SgPhyAdjustConstants;
import com.jackrain.nea.sg.phyadjust.common.SgPhyAdjustConstantsIF;
import com.jackrain.nea.sg.phyadjust.model.request.SgPhyAdjustBillSaveRequest;
import com.jackrain.nea.sg.phyadjust.model.request.SgPhyAdjustImpItemRequest;
import com.jackrain.nea.sg.phyadjust.model.request.SgPhyAdjustSaveRequest;
import com.jackrain.nea.sg.phyadjust.model.request.SgPhyAdjustStoreRequest;
import com.jackrain.nea.sg.phyadjust.services.SgPhyAdjSaveAndAuditService;
import com.jackrain.nea.sg.unpack.common.SgUnpackConstants;
import com.jackrain.nea.sg.unpack.mapper.SgBTeusUnpackItemMapper;
import com.jackrain.nea.sg.unpack.mapper.SgBTeusUnpackMapper;
import com.jackrain.nea.sg.unpack.mapper.SgBTeusUnpackSkuItemMapper;
import com.jackrain.nea.sg.unpack.model.table.SgBTeusUnpack;
import com.jackrain.nea.sg.unpack.model.table.SgBTeusUnpackItem;
import com.jackrain.nea.sg.unpack.model.table.SgBTeusUnpackSkuItem;
import com.jackrain.nea.tableService.MainTableRecord;
import com.jackrain.nea.tableService.TableServiceContext;
import com.jackrain.nea.tableService.filter.BaseFilter;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author zhoulisnheng
 * @since 2019/11/27 18:04
 * desc: 拆箱单保存
 */
@Slf4j
@Component
public class SgUnpackAuditFilter extends BaseFilter {

    @Override
    public void before(TableServiceContext context, MainTableRecord row) {

        /**
         * d)	根据【明细表】的“箱号”字段：更新对应箱号在【箱定义】上的箱状态=已拆箱
         */
        Long objId = row.getId();
        SgBTeusUnpackMapper unpackMapper = ApplicationContextHandle.getBean(SgBTeusUnpackMapper.class);
        SgBTeusUnpack sgBTeusUnpack = unpackMapper.selectById(objId);
        SgBTeusUnpackItemMapper unpackItemMapper = ApplicationContextHandle.getBean(SgBTeusUnpackItemMapper.class);

        List<SgBTeusUnpackItem> unpackItemList = unpackItemMapper.selectList(new QueryWrapper<SgBTeusUnpackItem>().lambda().eq(SgBTeusUnpackItem::getSgBTeusUnpackId, objId));
        List<Long> teusIdList = unpackItemList.stream()
                .filter(o -> o.getPsCTeusId() != null)
                .map(SgBTeusUnpackItem::getPsCTeusId).collect(Collectors.toList());

        JSONObject commitData = row.getCommitData();

        /**
         * e)	根据【明细表】数据，将【箱号】对应的箱定义明细写入到本单的【箱内条码明细】中
         */
        QueryBoxItemsService queryBoxItemsService = ApplicationContextHandle.getBean(QueryBoxItemsService.class);
        QueryBoxRequest queryBoxRequest = new QueryBoxRequest();
        queryBoxRequest.setBoxIds(teusIdList);
        List<PsCTeusItem> psCTeusItems = queryBoxItemsService.queryBoxItemsByBoxInfo(queryBoxRequest);
        List<SgBTeusUnpackSkuItem> teusUnpackSkuItemList = new ArrayList<>();
        psCTeusItems.forEach(x -> {
            SgBTeusUnpackSkuItem skuItem = new SgBTeusUnpackSkuItem();
            BeanUtils.copyProperties(x, skuItem);
            skuItem.setSgBTeusUnpackId(objId);
            skuItem.setId(ModelUtil.getSequence(SgUnpackConstants.SG_B_TEUS_UNPACK_SKU_ITEM));
            teusUnpackSkuItemList.add(skuItem);
        });

        // 批量新增
        SgBTeusUnpackSkuItemMapper skuItemMapper = ApplicationContextHandle.getBean(SgBTeusUnpackSkuItemMapper.class);
        skuItemMapper.batchInsert(teusUnpackSkuItemList);

        /**
         * f)	生成一张已审核的库存调整单
         */
        SgPhyAdjSaveAndAuditService saveAndAuditService = ApplicationContextHandle.getBean(SgPhyAdjSaveAndAuditService.class);
        SgPhyAdjustBillSaveRequest request = packparam(sgBTeusUnpack, unpackItemList, teusUnpackSkuItemList,commitData);
        request.setLoginUser(context.getUser());
        if (log.isDebugEnabled()) {
            log.debug("SgUnpack.saveAndAuditService.request:{}"
                    , JSONObject.toJSONString(request));
        }
        saveAndAuditService.saveAndAudit(request);

        // 更改箱状态放在调整单之后
        PsCTeusSaveCmd psCTeusSaveCmd = (PsCTeusSaveCmd)
                ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                        PsCTeusSaveCmd.class.getName(), "ps-ext", "1.0");
        PsTeusUpdateRequest teusUpdateRequest = new PsTeusUpdateRequest();
        teusUpdateRequest.setBoxStatus(SgUnpackConstants.BOX_STATUS_SPLIT);
        teusUpdateRequest.setIds(teusIdList);
        psCTeusSaveCmd.batchUpdateBoxForPurIn(context.getUser(), teusUpdateRequest);
    }


    /**
     *  封装参数
     * @param sgBTeusUnpack 拆箱
     * @param unpackItemList  明细表
     * @param teusUnpackSkuItemList  箱内明细
     * @param commitData
     * @return
     */
    private SgPhyAdjustBillSaveRequest packparam(SgBTeusUnpack sgBTeusUnpack, List<SgBTeusUnpackItem> unpackItemList,
                                                 List<SgBTeusUnpackSkuItem> teusUnpackSkuItemList, JSONObject commitData) {

        Long storeId = commitData.getLong("CP_C_STORE_ID");
        String storeEcode = commitData.getString("CP_C_STORE_ECODE");
        String storeEname = commitData.getString("CP_C_STORE_ENAME");

        SgPhyAdjustBillSaveRequest billSaveRequest = new SgPhyAdjustBillSaveRequest();
        // 主表信息
        SgPhyAdjustSaveRequest sgPhyAdjustSaveRequest = new SgPhyAdjustSaveRequest();
        sgPhyAdjustSaveRequest.setCpCPhyWarehouseId(sgBTeusUnpack.getCpCPhyWarehouseId());
        sgPhyAdjustSaveRequest.setCpCPhyWarehouseEname(sgBTeusUnpack.getCpCPhyWarehouseEname());
        sgPhyAdjustSaveRequest.setCpCPhyWarehouseEcode(sgBTeusUnpack.getCpCPhyWarehouseEcode());
//        sgPhyAdjustSaveRequest.setCpCStoreId(sgBTeusUnpack.getCpCStoreId());
//        sgPhyAdjustSaveRequest.setCpCStoreEcode(sgBTeusUnpack.getCpCStoreEcode());
//        sgPhyAdjustSaveRequest.setCpCStoreEname(sgBTeusUnpack.getCpCStoreEname());
        sgPhyAdjustSaveRequest.setBillType(SgPhyAdjustConstantsIF.BILL_TYPE_NORMAL);
        sgPhyAdjustSaveRequest.setSgBAdjustPropId(SgPhyAdjustConstants.ADJUST_PROP_NORMAL_ADJUST);
        sgPhyAdjustSaveRequest.setSourceBillType(SgPhyAdjustConstantsIF.SOURCE_BILL_TYPE_UNPACK);
        sgPhyAdjustSaveRequest.setSourceBillId(sgBTeusUnpack.getId());
        sgPhyAdjustSaveRequest.setSourceBillNo(sgBTeusUnpack.getEcode());
        sgPhyAdjustSaveRequest.setRemark("由拆箱单" + sgBTeusUnpack.getEcode()+ "审核生成！");

        // set逻辑仓相关字段
        SgPhyAdjustStoreRequest sgPhyAdjustStoreRequest = new SgPhyAdjustStoreRequest();
        sgPhyAdjustStoreRequest.setCpCStoreId(storeId);
        sgPhyAdjustStoreRequest.setCpCStoreEname(storeEname);
        sgPhyAdjustStoreRequest.setCpCStoreEcode(storeEcode);
        billSaveRequest.setStoreRequest(sgPhyAdjustStoreRequest);

        // 封装录入明细
        List<SgPhyAdjustImpItemRequest> impItemRequestList = new ArrayList<>();
        // 明细表
        unpackItemList.forEach( x -> {
            SgPhyAdjustImpItemRequest impItemRequest = new SgPhyAdjustImpItemRequest();
            impItemRequest.setId(-1L);
            impItemRequest.setQty(x.getQty().negate());
            impItemRequest.setIsTeus(SgUnpackConstants.IS_TEUS_Y);
            impItemRequest.setSourceBillItemId(x.getId());
            impItemRequest.setPsCTeusId(x.getPsCTeusId());
            impItemRequest.setPsCTeusEcode(x.getPsCTeusEcode());
            impItemRequestList.add(impItemRequest);
        });
        // 箱内明细表
        teusUnpackSkuItemList.forEach( x -> {
            SgPhyAdjustImpItemRequest impItemRequest = new SgPhyAdjustImpItemRequest();
            impItemRequest.setId(-1L);
            impItemRequest.setIsTeus(SgUnpackConstants.IS_TEUS_N);
            impItemRequest.setPsCTeusId(null);
            impItemRequest.setSourceBillItemId(x.getId());
            impItemRequest.setPsCSkuId(x.getPsCSkuId());
            impItemRequest.setPsCSkuEcode(x.getPsCSkuEcode());
            impItemRequestList.add(impItemRequest);
        });
        billSaveRequest.setPhyAdjust(sgPhyAdjustSaveRequest);
        billSaveRequest.setImpItems(impItemRequestList);
        billSaveRequest.setObjId(-1L);
        return billSaveRequest;
    }
}
