package com.jackrain.nea.sg.in.services;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.model.util.AdParamUtil;
import com.jackrain.nea.oc.basic.common.OcBasicConstants;
import com.jackrain.nea.sg.basic.api.SgPhyStorageBillUpdateCmd;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.config.SgStorageBoxConfig;
import com.jackrain.nea.sg.basic.model.request.*;
import com.jackrain.nea.sg.in.mapper.*;
import com.jackrain.nea.sg.in.model.request.*;
import com.jackrain.nea.sg.in.model.table.*;
import com.jackrain.nea.sg.receive.api.SgReceiveCmd;
import com.jackrain.nea.sg.receive.model.request.SgReceiveBillSubmitRequest;
import com.jackrain.nea.sg.receive.model.result.SgReceiveBillSubmitResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.assertj.core.util.Lists;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author: 舒威
 * @since: 2019/8/29
 * create at : 2019/8/29 16:18
 */
@Slf4j
@Component
public class SgPhyInRPCService {

    @Autowired
    private SgStorageBoxConfig storageBoxConfig;

    /**
     * 逻辑收货单入库
     *
     * @param inResult      入库结果单
     * @param inResultitems 入库结果单明细
     * @param user          用户
     */
    public ValueHolderV14<SgReceiveBillSubmitResult> sgBReceiveIn(SgBPhyInResult inResult, List<SgBPhyInResultItem> inResultitems, User user,
                                       List<SgBPhyInResultImpItem> impItemList, List<SgBPhyInResultTeusItem> teusItemList) {
        long start = System.currentTimeMillis();
        SgReceiveCmd SgReceiveCmd = (SgReceiveCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                SgReceiveCmd.class.getName(), SgConstantsIF.GROUP, SgConstantsIF.VERSION);
        SgReceiveBillSubmitRequest submitRequest = new SgReceiveBillSubmitRequest();
        submitRequest.setPhyInResult(inResult);
        submitRequest.setPhyInResultItems(inResultitems);
        submitRequest.setSourceBillId(inResult.getSourceBillId());
        submitRequest.setSourceBillType(inResult.getSourceBillType());
        submitRequest.setLoginUser(user);
        if (storageBoxConfig.getBoxEnable()) {
            submitRequest.setPhyInResultImpItems(impItemList);
            submitRequest.setPhyInResultTeusItems(teusItemList);
        }
        ValueHolderV14<SgReceiveBillSubmitResult> vh = SgReceiveCmd.submitSgBReceive(submitRequest);
        if (log.isDebugEnabled()) {
            log.debug("来源单据id[" + inResult.getSourceBillId() + "],入库结果单审核调用逻辑收货单入库服务:spend time:{}ms,result:;",
                    System.currentTimeMillis() - start, JSONObject.toJSONString(vh));
        }
        return vh;
    }


    /**
     * 修改实体仓库存
     *
     * @param inResult      入库结果单
     * @param inResultitems 入库结果单明细
     * @param user          用户
     */
    public ValueHolderV14 updateSgPhyStorage(SgBPhyInResult inResult, List<SgBPhyInResultItem> inResultitems, User user,
                                             List<SgBPhyInResultImpItem> impItemList, List<SgBPhyInResultTeusItem> teusItemList) {
        Boolean boxEnable = storageBoxConfig.getBoxEnable();

        long start = System.currentTimeMillis();
        SgPhyStorageBillUpdateCmd sgStorageBillUpdateCmd = (SgPhyStorageBillUpdateCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(),
                SgPhyStorageBillUpdateCmd.class.getName(),
                SgConstantsIF.GROUP, SgConstantsIF.VERSION);
        SgPhyStorageSingleUpdateRequest request = new SgPhyStorageSingleUpdateRequest();
        SgPhyStorageUpdateBillRequest billRequest = new SgPhyStorageUpdateBillRequest();

        //单据信息
        billRequest.setBillId(inResult.getId());
        billRequest.setBillNo(inResult.getBillNo());
        //2019-06-24添加:来源单据ID,来源单据编号
        billRequest.setSourceBillId(inResult.getSourceBillId());
        billRequest.setSourceBillNo(inResult.getSourceBillNo());
        billRequest.setBillDate(inResult.getBillDate());
        billRequest.setBillType(inResult.getSourceBillType());
        billRequest.setChangeDate(new Timestamp(System.currentTimeMillis()));

        Map<String, BigDecimal> teusSkuQtyMap = new HashMap<>();
        List<SgPhyTeusStorageUpdateBillItemRequest> teusList = new ArrayList<>();
        if (boxEnable) {
            //箱信息
            for (SgBPhyInResultImpItem impItem : impItemList) {
                if (impItem.getIsTeus().equals(OcBasicConstants.IS_MATCH_SIZE_Y)) {
                    //封装箱入参
                    SgPhyTeusStorageUpdateBillItemRequest teusBillItemRequest = new SgPhyTeusStorageUpdateBillItemRequest();
                    BeanUtils.copyProperties(impItem, teusBillItemRequest);
                    teusBillItemRequest.setBillItemId(impItem.getId());
                    teusBillItemRequest.setQtyChange(impItem.getQtyIn());
                    teusBillItemRequest.setCpCPhyWarehouseId(inResult.getCpCPhyWarehouseId());
                    teusBillItemRequest.setCpCPhyWarehouseEcode(inResult.getCpCPhyWarehouseEcode());
                    teusBillItemRequest.setCpCPhyWarehouseEname(inResult.getCpCPhyWarehouseEname());
                    teusList.add(teusBillItemRequest);
                }
            }
            billRequest.setTeusList(teusList);
            //合并箱内明细数量
            teusSkuQtyMap = teusItemList.stream().collect(Collectors.groupingBy(SgBPhyInResultTeusItem::getPsCSkuEcode,
                    Collectors.reducing(BigDecimal.ZERO, SgBPhyInResultTeusItem::getQty, BigDecimal::add)));
        }

        //明细信息
        List<SgPhyStorageUpdateBillItemRequest> billItemList = Lists.newArrayList();
        for (SgBPhyInResultItem resultItem : inResultitems) {
            SgPhyStorageUpdateBillItemRequest item = new SgPhyStorageUpdateBillItemRequest();
            BeanUtils.copyProperties(resultItem, item);
            item.setBillItemId(resultItem.getId());
            item.setCpCPhyWarehouseId(inResult.getCpCPhyWarehouseId());
            item.setCpCPhyWarehouseEcode(inResult.getCpCPhyWarehouseEcode());
            item.setCpCPhyWarehouseEname(inResult.getCpCPhyWarehouseEname());
            item.setQtyChange(resultItem.getQtyIn());
            if (boxEnable) {
                BigDecimal qtyTeusChange = Optional.ofNullable(teusSkuQtyMap.get(resultItem.getPsCSkuEcode())).orElse(BigDecimal.ZERO);
                item.setQtyTeusChange(qtyTeusChange);
            }
            billItemList.add(item);
        }

        billRequest.setItemList(billItemList);
        SgStorageUpdateControlRequest controlRequest = new SgStorageUpdateControlRequest();
        String isControlModel = AdParamUtil.getParam("warehouse.allow_negative_storage");
        controlRequest.setNegativeStorage(Boolean.valueOf(isControlModel));
        String messageKey = SgConstants.MSG_TAG_PHY_IN_RESULT + ":" + inResult.getBillNo();
        request.setControlModel(controlRequest);
        request.setMessageKey(messageKey);
        request.setBill(billRequest);
        request.setLoginUser(user);
        ValueHolderV14 vh = sgStorageBillUpdateCmd.updateStorageBillWithTrans(request);
        if (log.isDebugEnabled()) {
            log.debug("来源单据id[" + inResult.getSourceBillId() + "],入库结果单审核更新实体仓库存:spend time:{}ms,result:;"
                    , System.currentTimeMillis() - start, JSONObject.toJSONString(vh));
        }
        return vh;
    }


    /**
     * 获取入库结果单审核回写mq
     *
     * @param inResultId      入库结果单id
     * @param inResult        入库结果单
     * @param inResultitems   入库结果单明细
     * @param resultImpItems
     * @param resultTeusItems
     * @param user            用户
     */
    public ValueHolderV14<SgPhyInResultBillSaveRequest> getSgPhyInMQResult(Long inResultId, SgBPhyInResult inResult, List<SgBPhyInResultItem> inResultitems, List<SgBPhyInResultImpItem> resultImpItems, List<SgBPhyInResultTeusItem> resultTeusItems, User user) {
        ValueHolderV14<SgPhyInResultBillSaveRequest> vh = new ValueHolderV14<>(ResultCode.SUCCESS, "success");
        if (inResultId == null && (inResult == null || CollectionUtils.isEmpty(inResultitems))) {
            vh.setCode(ResultCode.FAIL);
            vh.setMessage("获取结果单mqbody失败,结果单信息不能为空!");
            return vh;
        }

        SgBPhyInResult result = inResult;
        List<SgBPhyInResultItem> items = inResultitems;

        if (result == null) {
            SgBPhyInResultMapper inResultMapper = ApplicationContextHandle.getBean(SgBPhyInResultMapper.class);
            result = inResultMapper.selectById(inResultId);
        }
        if (CollectionUtils.isEmpty(inResultitems)) {
            SgBPhyInResultItemMapper inResultItemMapper = ApplicationContextHandle.getBean(SgBPhyInResultItemMapper.class);
            items = inResultItemMapper.selectList(new QueryWrapper<SgBPhyInResultItem>().lambda()
                    .eq(SgBPhyInResultItem::getSgBPhyInResultId, inResultId));
        }

        List<SgPhyInResultImpItemSaveRequest> impItemList=Lists.newArrayList();
        if (storageBoxConfig.getBoxEnable() && CollectionUtils.isEmpty(resultImpItems)) {
            SgBPhyInResultImpItemMapper inResultImpItemMapper = ApplicationContextHandle.getBean(SgBPhyInResultImpItemMapper.class);
            resultImpItems = inResultImpItemMapper.selectList(new QueryWrapper<SgBPhyInResultImpItem>().lambda()
                    .eq(SgBPhyInResultImpItem::getSgBPhyInResultId, inResultId));
            impItemList = JSONArray.parseArray(JSONArray.toJSONString(resultImpItems), SgPhyInResultImpItemSaveRequest.class);
        }

        List<SgPhyInResultTeusItemSaveRequest> teusItemList=Lists.newArrayList();
        if (storageBoxConfig.getBoxEnable() && CollectionUtils.isEmpty(resultTeusItems)) {
            SgBPhyInResultTeusItemMapper inResultTeusItemMapper = ApplicationContextHandle.getBean(SgBPhyInResultTeusItemMapper.class);
            resultTeusItems = inResultTeusItemMapper.selectList(new QueryWrapper<SgBPhyInResultTeusItem>().lambda()
                    .eq(SgBPhyInResultTeusItem::getSgBPhyInResultId, inResultId));
            teusItemList = JSONArray.parseArray(JSONArray.toJSONString(resultTeusItems), SgPhyInResultTeusItemSaveRequest.class);
        }

        SgBPhyInResultSaveRequest billRequest = new SgBPhyInResultSaveRequest();
        List<SgBPhyInResultItemSaveRequest> itemList = Lists.newArrayList();
        List<SgBPhyInResultDefectItemSaveRequest> defectItemList = Lists.newArrayList();

        billRequest.setId(result.getId());
        billRequest.setBillNo(result.getBillNo());
        billRequest.setCpCPhyWarehouseId(result.getCpCPhyWarehouseId());
        billRequest.setCpCPhyWarehouseEcode(result.getCpCPhyWarehouseEcode());
        billRequest.setCpCPhyWarehouseEname(result.getCpCPhyWarehouseEname());
        billRequest.setSourceBillType(result.getSourceBillType());
        billRequest.setSourceBillId(result.getSourceBillId());
        billRequest.setSourceBillNo(result.getSourceBillNo());
        billRequest.setIsLast(result.getIsLast());
        billRequest.setId(result.getInId());
        billRequest.setInName(result.getInName());
        //2019-06-25添加inEname参数
        billRequest.setInEname(result.getInEname());
        billRequest.setInTime(result.getInTime());
        billRequest.setInType(result.getInType());
        //判断是否真的最后一次入库  判断通知单差异数量是否为0
        Boolean isAll = true;
        SgBPhyInNoticesMapper noticesMapper = ApplicationContextHandle.getBean(SgBPhyInNoticesMapper.class);
        SgBPhyInNotices notices = noticesMapper.selectById(result.getSgBPhyInNoticesId());
        if (notices == null || notices.getTotQtyDiff().compareTo(BigDecimal.ZERO) != 0) {
            isAll = false;
        }
        billRequest.setIsAll(isAll);

        for (SgBPhyInResultItem item : items) {
            SgBPhyInResultItemSaveRequest itemBillRequest = new SgBPhyInResultItemSaveRequest();
            itemBillRequest.setId(item.getId());
            itemBillRequest.setSgBPhyInResultId(result.getId());
            itemBillRequest.setPsCSkuId(item.getPsCSkuId());
            itemBillRequest.setPsCSkuEcode(item.getPsCSkuEcode());
            itemBillRequest.setPsCProId(item.getPsCProId());
            itemBillRequest.setPsCProEcode(item.getPsCProEcode());
            itemBillRequest.setPsCProEname(item.getPsCProEname());
            itemBillRequest.setPsCSpec1Id(item.getPsCSpec1Id());
            itemBillRequest.setPsCSpec1Ecode(item.getPsCSpec1Ecode());
            itemBillRequest.setPsCSpec1Ename(item.getPsCSpec1Ename());
            itemBillRequest.setPsCSpec2Id(item.getPsCSpec2Id());
            itemBillRequest.setPsCSpec2Ecode(item.getPsCSpec2Ecode());
            itemBillRequest.setPsCSpec2Ename(item.getPsCSpec2Ename());
            itemBillRequest.setQtyIn(item.getQtyIn());
            itemList.add(itemBillRequest);
        }

        //2019-07-12 新增残次品明细
        SgBPhyInResultDefectItemMapper defectItemMapper = ApplicationContextHandle.getBean(SgBPhyInResultDefectItemMapper.class);
        List<SgBPhyInResultDefectItem> defectItems = defectItemMapper.selectList(new QueryWrapper<SgBPhyInResultDefectItem>().lambda()
                .eq(SgBPhyInResultDefectItem::getSgBPhyInResultId, result.getId()));
        defectItems.forEach(defectItem -> {
            SgBPhyInResultDefectItemSaveRequest itemBillRequest = new SgBPhyInResultDefectItemSaveRequest();
            BeanUtils.copyProperties(defectItem, itemBillRequest);
            defectItemList.add(itemBillRequest);
        });

        SgPhyInResultBillSaveRequest billSaveRequest = new SgPhyInResultBillSaveRequest();
        billSaveRequest.setSgBPhyInResultSaveRequest(billRequest);
        billSaveRequest.setItemList(itemList);
        billSaveRequest.setDefectItemList(defectItemList);
        billSaveRequest.setImpItemList(impItemList);
        billSaveRequest.setTeusItemList(teusItemList);
        billSaveRequest.setLoginUser(user);
        vh.setData(billSaveRequest);
        return vh;
    }
}
