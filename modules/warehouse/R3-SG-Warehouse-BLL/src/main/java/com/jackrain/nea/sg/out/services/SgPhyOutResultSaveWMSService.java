package com.jackrain.nea.sg.out.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.core.db.Tools;
import com.jackrain.nea.oc.basic.common.OcBasicConstants;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.config.SgStorageBoxConfig;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.basic.utils.StorageUtils;
import com.jackrain.nea.sg.out.common.SgOutConstants;
import com.jackrain.nea.sg.out.common.SgOutConstantsIF;
import com.jackrain.nea.sg.out.mapper.*;
import com.jackrain.nea.sg.out.model.request.*;
import com.jackrain.nea.sg.out.model.table.*;
import com.jackrain.nea.sg.out.utils.SgWarehouseESUtils;
import com.jackrain.nea.sg.send.utils.SgStoreUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 批量新增出库结果单-WMS回传用
 *
 * @author: 舒威
 * @since: 2019/10/16
 * create at : 2019/10/16 21:36
 */
@Slf4j
@Component
public class SgPhyOutResultSaveWMSService {

    @Autowired
    private SgBPhyOutResultMapper resultMapper;

    @Autowired
    private SgBPhyOutResultItemMapper resultItemMapper;

    @Autowired
    private SgBOutDeliveryMapper deliveryMapper;

    @Autowired
    private SgWarehouseESUtils warehouseESUtils;

    @Autowired
    private SgStorageBoxConfig storageBoxConfig;


    public ValueHolderV14<JSONObject> saveSgPhyOutResultByWms(List<SgPhyOutResultBillSaveRequest> requests, User user) {
        ValueHolderV14<JSONObject> vh = new ValueHolderV14<>(ResultCode.SUCCESS, "success");
        JSONObject dataObj = new JSONObject();
        HashMap<String, String> err = Maps.newHashMap();
        ValueHolderV14<HashMap<String, Object>> checkResult = checkParams(requests);
        if (!checkResult.isOK()) {
            HashMap<String, Object> data = checkResult.getData();
            err = (HashMap<String, String>) data.get("error");
            vh.setCode(ResultCode.FAIL);
            vh.setMessage("批量新增出库通知单失败!");
        } else {
            HashMap<String, Object> data = checkResult.getData();
            //check通过数据
            HashMap<String, SgPhyOutResultBillSaveRequest> requestMap = (HashMap<String, SgPhyOutResultBillSaveRequest>) data.get("data");
            List<SgPhyOutResultBillSaveRequest> requestList = Lists.newArrayList(requestMap.values());
            err = (HashMap<String, String>) data.get("error");
            //获取批量新增的数据
            ValueHolderV14<HashMap<SgBPhyOutResult, JSONObject>> paramsResult = getBatchSaveParams(requestList, err, user);
            if (paramsResult.isOK()) {
                HashMap<SgBPhyOutResult, JSONObject> paramsMap = paramsResult.getData();
                //批量新增
                ValueHolderV14<HashMap<Long, JSONObject>> batchSaveResult = batchSaveOutResult(paramsMap, err);
                if (!batchSaveResult.isOK()) {
                    vh.setCode(ResultCode.FAIL);
                    vh.setMessage("批量新增出库结果单失败!");
                } else {
                    vh.setMessage("批量新增出库通知单成功!");
                    dataObj.put("data", batchSaveResult.getData());
                }
            } else {
                vh.setCode(ResultCode.FAIL);
                vh.setMessage("批量新增出库通知单失败:获取结果单新增数据失败!");
            }
        }
        dataObj.put("error", err);
        vh.setData(dataObj);
        return vh;
    }

    public ValueHolderV14<HashMap<Long, JSONObject>> batchSaveOutResult(HashMap<SgBPhyOutResult, JSONObject> paramsMap, HashMap<String, String> err) {

        SgPhyOutResultSaveWMSService bean = ApplicationContextHandle.getBean(SgPhyOutResultSaveWMSService.class);
        ValueHolderV14<HashMap<Long, JSONObject>> v14 = new ValueHolderV14<>(ResultCode.SUCCESS, "success");
        HashMap<Long, JSONObject> data = Maps.newHashMap();
        boolean flag = false;
        Iterator<Map.Entry<SgBPhyOutResult, JSONObject>> iterator = paramsMap.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<SgBPhyOutResult, JSONObject> next = iterator.next();
            SgBPhyOutResult result = next.getKey();
            JSONObject value = next.getValue();
            List<SgBPhyOutResultItem> resultItems = (List<SgBPhyOutResultItem>) value.get("items");
            List<SgBPhyOutResultImpItem> resultImpItems = (List<SgBPhyOutResultImpItem>) value.get("impItems");
            List<SgBOutDelivery> deliveries = (List<SgBOutDelivery>) value.get("deliveries");
            try {
                bean.saveOutResult(result, resultItems,resultImpItems, deliveries);
                flag = true;
                JSONObject dataObj = new JSONObject();
                dataObj.put("result", result);
                dataObj.put("items", resultItems);
                dataObj.put("deliveries", deliveries);
                data.put(result.getSourceBillId(), dataObj);
                //推送es
                warehouseESUtils.pushESByOutResult(result.getId(), true, false, null, resultMapper, resultItemMapper);
            } catch (Exception e) {
                String messageExtra = AssertUtils.getMessageExtra("来源单据id[" + result.getSourceBillId() + "],出库结果单新增失败！", e);
                log.error(messageExtra);
                err.put(result.getSgBPhyOutNoticesBillno(), messageExtra);
            }
        }
        if (!flag) {
            v14.setCode(ResultCode.FAIL);
            v14.setMessage("批量新增出库结果单失败!");
        } else {
            v14.setData(data);
        }
        return v14;
    }

    @Transactional(rollbackFor = Exception.class)
    public void saveOutResult(SgBPhyOutResult result, List<SgBPhyOutResultItem> resultItems, List<SgBPhyOutResultImpItem> resultImpItems, List<SgBOutDelivery> deliveries) {
        String billno = result.getSgBPhyOutNoticesBillno();
        int count = resultMapper.insert(result);
        if (count < 1) {
            AssertUtils.logAndThrow("通知单" + billno + ",出库结果单新增失败！");
        }
        if (storageBoxConfig.getBoxEnable()) {
            batchSaveOutResultImpItems(resultImpItems, billno);
        } else {
            batchSaveOutResultItems(resultItems, resultItemMapper, billno);
        }
        batchSaveOutResultDeliverys(deliveries, deliveryMapper, billno);
    }

    public void batchSaveOutResultImpItems(List<SgBPhyOutResultImpItem> resultImpItems, String billno) {
        List<List<SgBPhyOutResultImpItem>> baseModelPageList = StorageESUtils.getBaseModelPageList(
                resultImpItems, SgOutConstants.INSERT_PAGE_SIZE);

        SgBPhyOutResultImpItemMapper impItemMapper = ApplicationContextHandle.getBean(SgBPhyOutResultImpItemMapper.class);
        for (List<SgBPhyOutResultImpItem> pageList : baseModelPageList) {
            if (CollectionUtils.isEmpty(pageList)) {
                continue;
            }
            int count = impItemMapper.batchInsert(pageList);
            if (count != pageList.size()) {
                AssertUtils.logAndThrow("通知单" + billno + ",出库结果单录入明细批量新增失败！");
            }
        }
    }

    public void batchSaveOutResultDeliverys(List<SgBOutDelivery> deliveries, SgBOutDeliveryMapper deliveryMapper, String billno) {
        List<List<SgBOutDelivery>> baseModelPageList = StorageUtils.getBaseModelPageList(
                deliveries, SgOutConstants.INSERT_PAGE_SIZE);

        for (List<SgBOutDelivery> pageList : baseModelPageList) {
            if (CollectionUtils.isEmpty(pageList)) {
                continue;
            }
            int count = deliveryMapper.batchInsert(pageList);
            if (count != pageList.size()) {
                AssertUtils.logAndThrow("通知单" + billno + ",出库结果单多包裹明细批量新增失败！");
            }
        }
    }

    public void batchSaveOutResultItems(List<SgBPhyOutResultItem> resultItems, SgBPhyOutResultItemMapper resultItemMapper, String billno) {
        List<List<SgBPhyOutResultItem>> baseModelPageList = StorageESUtils.getBaseModelPageList(
                resultItems, SgOutConstants.INSERT_PAGE_SIZE);

        for (List<SgBPhyOutResultItem> pageList : baseModelPageList) {
            if (CollectionUtils.isEmpty(pageList)) {
                continue;
            }
            int count = resultItemMapper.batchInsert(pageList);
            if (count != pageList.size()) {
                AssertUtils.logAndThrow("通知单" + billno + ",出库结果单明细批量新增失败！");
            }
        }
    }

    public ValueHolderV14<HashMap<SgBPhyOutResult, JSONObject>> getBatchSaveParams(List<SgPhyOutResultBillSaveRequest> requestList, HashMap<String, String> err, User user) {
        ValueHolderV14<HashMap<SgBPhyOutResult, JSONObject>> vh = new ValueHolderV14<>(ResultCode.SUCCESS, "success");
        HashMap<SgBPhyOutResult, JSONObject> data = Maps.newHashMap();
        for (SgPhyOutResultBillSaveRequest request : requestList) {
            SgPhyOutResultSaveRequest outResultRequest = request.getOutResultRequest();
            String billno = outResultRequest.getSgBPhyOutNoticesBillno();
            try {
                SgBPhyOutResult outResult = new SgBPhyOutResult();
                BeanUtils.copyProperties(outResultRequest, outResult);
                Long objId = Tools.getSequence(SgConstants.SG_B_PHY_OUT_RESULT);
                outResult.setId(objId);
                // 获取单据编号
                String billNo = SgStoreUtils.getBillNo(SgOutConstants.SQE_SG_B_OUT_RESULT, SgConstants.SG_B_PHY_OUT_NOTICES.toUpperCase(), outResult, user.getLocale());
                outResult.setBillNo(billNo);
                outResult.setBillDate(Optional.ofNullable(outResult.getBillDate()).orElse(new Date()));
                outResult.setIsactive(SgConstants.IS_ACTIVE_Y);    // 未作废
                outResult.setBillStatus(SgOutConstantsIF.OUT_NOTICES_STATUS_WAIT);   // 待出库
                StorageESUtils.setBModelDefalutData(outResult, user);
                outResult.setOwnerename(user.getEname());
                outResult.setModifierename(user.getEname());

                JSONObject dataObj = new JSONObject();
                //获取结果单明细
                BigDecimal totQtyOut = BigDecimal.ZERO;
                BigDecimal totAmtListOut = BigDecimal.ZERO;
                if (storageBoxConfig.getBoxEnable()) {
                    List<SgBPhyOutResultImpItem> resultItems = Lists.newArrayList();
                    for (SgPhyOutResultImpItemSaveRequest impItemSaveRequest : request.getImpItemList()) {
                        SgBPhyOutResultImpItem resultImpItem = new SgBPhyOutResultImpItem();
                        BeanUtils.copyProperties(impItemSaveRequest, resultImpItem);
                        resultImpItem.setId(Tools.getSequence(SgConstants.SG_B_PHY_OUT_RESULT_IMP_ITEM));
                        resultImpItem.setSgBPhyOutResultId(objId);
                        resultImpItem.setIsTeus(OcBasicConstants.IS_MATCH_SIZE_N);
                        StorageESUtils.setBModelDefalutData(resultImpItem, user);
                        resultImpItem.setOwnerename(user.getEname());
                        resultImpItem.setModifierename(user.getEname());
                        resultItems.add(resultImpItem);

                        totQtyOut.add(resultImpItem.getQtyOut());
                    }
                    dataObj.put("impItems", resultItems);
                } else {
                    List<SgBPhyOutResultItem> resultItems = Lists.newArrayList();
                    for (SgPhyOutResultItemSaveRequest itemSaveRequest : request.getOutResultItemRequests()) {
                        SgBPhyOutResultItem outResultItem = new SgBPhyOutResultItem();
                        BeanUtils.copyProperties(itemSaveRequest, outResultItem);
                        outResultItem.setId(Tools.getSequence(SgConstants.SG_B_PHY_OUT_RESULT_ITEM));
                        outResultItem.setSgBPhyOutResultId(objId);
                        outResultItem.setAmtListOut(itemSaveRequest.getPriceList().multiply(itemSaveRequest.getQty()));
                        StorageESUtils.setBModelDefalutData(outResultItem, user);
                        outResultItem.setOwnerename(user.getEname());
                        outResultItem.setModifierename(user.getEname());
                        resultItems.add(outResultItem);

                        totQtyOut.add(itemSaveRequest.getQty());
                        totAmtListOut.add(outResultItem.getAmtListOut());
                    }
                    dataObj.put("items", resultItems);
                }

                outResult.setTotQtyOut(totQtyOut);
                outResult.setTotAmtListOut(totAmtListOut);

                //获取多包裹明细
                List<SgBOutDelivery> deliveries = new ArrayList<>();
                if (CollectionUtils.isNotEmpty(request.getOutDeliverySaveRequests())) {
                    for (SgPhyOutDeliverySaveRequest delivery : request.getOutDeliverySaveRequests()) {
                        SgBOutDelivery outDelivery = new SgBOutDelivery();
                        BeanUtils.copyProperties(delivery, outDelivery);
                        outDelivery.setId(Tools.getSequence(SgConstants.SG_B_OUT_DELIVERY));
                        outDelivery.setSgBPhyOutResultId(objId);
                        StorageUtils.setBModelDefalutData(outDelivery, user);
                        outDelivery.setOwnerename(user.getEname());
                        outDelivery.setModifierename(user.getEname());
                        deliveries.add(outDelivery);
                    }
                }

                dataObj.put("deliveries", deliveries);
                data.put(outResult, dataObj);
            } catch (Exception e) {
                String messageExtra = AssertUtils.getMessageExtra("通知单" + billno + ",组装结果单新增数据异常!", e);
                log.error(messageExtra);
                err.put(billno, messageExtra);
            }
        }
        if (MapUtils.isEmpty(data)) {
            vh.setCode(ResultCode.FAIL);
        } else {
            vh.setData(data);
        }
        return vh;
    }

    public ValueHolderV14<HashMap<String, Object>> checkParams(List<SgPhyOutResultBillSaveRequest> requests) {
        ValueHolderV14<HashMap<String, Object>> vh = new ValueHolderV14<>(ResultCode.SUCCESS, "success");
        HashMap<String, Object> data = Maps.newHashMap();
        HashMap<String, String> err = Maps.newHashMap();
        HashMap<String, SgPhyOutResultBillSaveRequest> var = Maps.newHashMap();
        HashMap<String, SgPhyOutResultBillSaveRequest> var2 = Maps.newHashMap();
        //通知单id-结果单Request
        HashMap<String, SgPhyOutResultBillSaveRequest> billNoticesResultMap = Maps.newHashMap();

        t:
        for (SgPhyOutResultBillSaveRequest request : requests) {
            SgPhyOutResultSaveRequest result = request.getOutResultRequest();
            String noticesBillno = result.getSgBPhyOutNoticesBillno();
            if (request.getLoginUser() == null ||
                    result.getSgBPhyOutNoticesId() == null ||
                    result.getSourceBillId() == null ||
                    StringUtils.isEmpty(noticesBillno) ||
                    result.getSourceBillNo() == null ||
                    result.getSourceBillType() == null ||
                    result.getCpCPhyWarehouseId() == null ||
                    result.getCpCPhyWarehouseEcode() == null ||
                    result.getCpCPhyWarehouseEname() == null) {
                err.put(noticesBillno, "通知单" + noticesBillno + ",新增出库结果单异常!出库结果单信息缺失:通知单、来源单据信息、实体仓不能为空!");
                continue;
            }

            if (result.getOutTime() == null) {
                result.setOutTime(new Date());
            }

            if (storageBoxConfig.getBoxEnable()) {
                if (CollectionUtils.isEmpty(request.getImpItemList())) {
                    err.put(noticesBillno, "通知单" + noticesBillno + ",新增出库结果单异常!录入明细信息不能为空!");
                    continue;
                }
                for (SgPhyOutResultImpItemSaveRequest item : request.getImpItemList()) {
                    if (item.getPsCSkuId() == null ||
                            item.getPsCSkuEcode() == null ||
                            item.getPsCProId() == null ||
                            item.getPsCProEcode() == null ||
                            item.getPsCProEname() == null ||
                            item.getPsCSpec1Id() == null ||
                            item.getPsCSpec1Ecode() == null ||
                            item.getPsCSpec1Ename() == null ||
                            item.getPsCSpec2Id() == null ||
                            item.getPsCSpec2Ecode() == null ||
                            item.getPsCSpec2Ename() == null ||
                            item.getQtyOut() == null) {
                        err.put(noticesBillno, "通知单" + noticesBillno + ",新增出库结果单异常!出库结果单信息缺失:所关联的出库通知单单明细id、条码、商品、规格、出库数量不能为空!");
                        continue t;
                    }
                }
            } else {
                if (CollectionUtils.isEmpty(request.getOutResultItemRequests())) {
                    err.put(noticesBillno, "通知单" + noticesBillno + ",新增出库结果单异常!明细信息不能为空!");
                    continue;
                }
                for (SgPhyOutResultItemSaveRequest item : request.getOutResultItemRequests()) {
                    if (item.getSgBPhyOutNoticesItemId() == null ||
                            item.getPsCSkuId() == null ||
                            item.getPsCSkuEcode() == null ||
                            item.getPsCProId() == null ||
                            item.getPsCProEcode() == null ||
                            item.getPsCProEname() == null ||
                            item.getPsCSpec1Id() == null ||
                            item.getPsCSpec1Ecode() == null ||
                            item.getPsCSpec1Ename() == null ||
                            item.getPsCSpec2Id() == null ||
                            item.getPsCSpec2Ecode() == null ||
                            item.getPsCSpec2Ename() == null ||
                            item.getQty() == null) {
                        err.put(noticesBillno, "通知单" + noticesBillno + ",新增出库结果单异常!出库结果单信息缺失:所关联的出库通知单单明细id、条码、商品、规格、出库数量不能为空!");
                        continue t;
                    }
                }
            }
            billNoticesResultMap.put(noticesBillno, request);
        }

        Set<String> checkedNos = Sets.newHashSet();
        Set<String> noticesBillNos = billNoticesResultMap.keySet();
        SgBPhyOutNoticesMapper noticesMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesMapper.class);
        if (CollectionUtils.isNotEmpty(noticesBillNos)) {
            List<SgBPhyOutNotices> notices = noticesMapper.selectList(new QueryWrapper<SgBPhyOutNotices>().lambda().in(SgBPhyOutNotices::getBillNo, noticesBillNos));
            Map<String, SgBPhyOutNotices> noticesMap = notices.stream().collect(Collectors.toMap(SgBPhyOutNotices::getBillNo, notice -> notice));
            for (String billno : noticesBillNos) {
                SgPhyOutResultBillSaveRequest billSaveRequest = billNoticesResultMap.get(billno);
                SgBPhyOutNotices notices1 = noticesMap.get(billno);
                if (notices1 == null) {
                    err.put(billno, "通知单" + billno + ",新增出库结果单异常!出库结果单所关联的通知单不存在!");
                } else {
                    if (!(notices1.getBillStatus() == SgOutConstantsIF.OUT_NOTICES_STATUS_WAIT ||
                            notices1.getBillStatus() == SgOutConstantsIF.OUT_NOTICES_STATUS_PART)) {
                        err.put(billno, "通知单" + billno + ",出库通知单不是待出库或者部分出库，不允许新增出库结果单！");
                    } else {
                        //记录校验通过的
                        checkedNos.add(billno);
                        var.put(billno, billSaveRequest);
                    }
                }
            }

            if (CollectionUtils.isNotEmpty(checkedNos)) {
                SgBPhyOutResultMapper resultMapper = ApplicationContextHandle.getBean(SgBPhyOutResultMapper.class);
                //新增出库结果单，判断如果已经存在此通知单关联的是否最后一次出库为是的未作废的出库结果单，则不允许保存
                List<SgBPhyOutResult> outResults = resultMapper.selectList(new QueryWrapper<SgBPhyOutResult>().lambda()
                        .in(SgBPhyOutResult::getSgBPhyOutNoticesBillno, checkedNos)
                        .eq(SgBPhyOutResult::getIsLast, SgOutConstantsIF.OUT_IS_LAST_Y)
                        .eq(SgBPhyOutResult::getIsactive, SgConstants.IS_ACTIVE_Y));
                //通知单id-结果单
                Map<String, List<SgBPhyOutResult>> resultsMap = Maps.newHashMap();
                if (CollectionUtils.isNotEmpty(outResults)) {
                    for (SgBPhyOutResult outResult : outResults) {
                        String billno = outResult.getSgBPhyOutNoticesBillno();
                        if (resultsMap.containsKey(billno)) {
                            List<SgBPhyOutResult> resultList = resultsMap.get(billno);
                            resultList.add(outResult);
                        } else {
                            List<SgBPhyOutResult> resultList = Lists.newArrayList();
                            resultList.add(outResult);
                            resultsMap.put(billno, resultList);
                        }
                    }
                }

                if (MapUtils.isNotEmpty(resultsMap)) {
                    for (String billno : checkedNos) {
                        List<SgBPhyOutResult> results = resultsMap.get(billno);
                        if (CollectionUtils.isNotEmpty(results)) {
                            err.put(billno, "通知单" + billno + ",已存在此通知单关联的最后一次出库的出库结果单，不允许保存!");
                        } else {
                            //记录校验通过的
                            SgPhyOutResultBillSaveRequest billSaveRequest = billNoticesResultMap.get(billno);
                            var2.put(billno, billSaveRequest);
                        }
                    }
                }
            }
        }

        if (MapUtils.isNotEmpty(var) || MapUtils.isNotEmpty(var2)) {
            data.put("data", MapUtils.isNotEmpty(var2) ? var2 : var);
            data.put("error", err);
            vh.setData(data);
        } else {
            vh.setCode(ResultCode.FAIL);
            vh.setMessage("参数check异常!");
            data.put("error", err);
            vh.setData(data);
        }
        log.debug("check结束:{};", JSONObject.toJSONString(vh));
        return vh;
    }
}
