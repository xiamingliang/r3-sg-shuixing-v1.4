package com.jackrain.nea.sg.inv.mapper;

import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.inv.constants.SgInvConstants;
import com.jackrain.nea.sg.inv.model.table.ScBInventory;
import com.jackrain.nea.web.face.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;


@Mapper
public interface ScBInventoryMapper extends ExtentionMapper<ScBInventory> {

    @Update("UPDATE " + SgInvConstants.SC_B_INVENTORY + " SET POL_STATUS=#{polStatus}" +
            ",poler_id=#{user.id},poler_ename=#{user.ename},poler_name=#{user.name},poler_time=now()" +
            ",modifierid=#{user.id},modifierename=#{user.ename},modifiername=#{user.name},modifieddate=now() WHERE BILL_NO IN (${billNos})")
    int updateInventoryByProfitLoss(@Param("polStatus") Integer polStatus, @Param("user") User user, @Param("billNos") String billNos);
}