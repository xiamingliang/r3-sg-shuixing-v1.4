package com.jackrain.nea.sg.out.services;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Lists;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.common.R3ParamConstants;
import com.jackrain.nea.sg.basic.model.result.SgR3BaseResult;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.out.common.SgOutConstantsIF;
import com.jackrain.nea.sg.out.mapper.SgBPhyOutNoticesMapper;
import com.jackrain.nea.sg.out.model.request.SgPhyOutNoticesPosRequest;
import com.jackrain.nea.sg.out.model.request.SgPhyOutResultBillSaveRequest;
import com.jackrain.nea.sg.out.model.request.SgPhyOutResultSaveRequest;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutNotices;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 全渠道发货单发货服务
 *
 * @author 舒威
 * @since 2019/12/11
 * create at : 2019/12/11 11:41
 */
@Slf4j
@Component
public class SgPhyOutNoticesPosSendService {

    @Autowired
    private SgBPhyOutNoticesMapper noticesMapper;

    @Autowired
    private SgPhyOutNoticesSaveRPCService outNoticesSaveRPCService;

    @Autowired
    private SgPhyOutResultSaveAndAuditService saveAndAuditService;

    @Transactional(rollbackFor = Exception.class)
    public ValueHolderV14<SgR3BaseResult> sendPosOutNotices(SgPhyOutNoticesPosRequest request) {
        ValueHolderV14<SgR3BaseResult> v14 = new ValueHolderV14<>();
        AssertUtils.notNull(request, "参数不能为空！");
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ".debug, param:" + JSONObject.toJSONString(request));
        }

        checkParams(request);
        User loginUser = request.getLoginUser();

        JSONArray errorArr = new JSONArray();
        Integer accSuccess = 0, accFailed = 0;

        Map<Long, SgBPhyOutNotices> noticesMap = noticesMapper.selectList(new QueryWrapper<SgBPhyOutNotices>().lambda()
                .in(SgBPhyOutNotices::getId, request.getIds()))
                .stream().collect(Collectors.toMap(SgBPhyOutNotices::getId, o -> o));

        for (int i = 0; i < request.getIds().size(); i++) {
            Long objId = request.getIds().getLong(i);
            try {
                SgBPhyOutNotices outNotices = noticesMap.get(objId);
                AssertUtils.notNull(outNotices, "当前记录已不存在！");
                AssertUtils.isTrue(outNotices.getBillStatus() != SgOutConstantsIF.OUT_NOTICES_STATUS_VOID, "单据状态为已作废，不允许发货！");
                AssertUtils.isTrue(outNotices.getBillStatus() != SgOutConstantsIF.OUT_NOTICES_STATUS_ALL, "单据状态为全部出库，不允许发货！");
                List<Long> ids = Lists.newArrayList();
                ids.add(objId);
                List<SgPhyOutResultBillSaveRequest> resultBillSaveRequests = outNoticesSaveRPCService.getAssemblyParameters(ids, loginUser, false, true,false);
                SgPhyOutResultBillSaveRequest saveRequest = resultBillSaveRequests.get(0);
                saveRequest.setIsCreateDelivers(true);
                ValueHolderV14 outResultAndAudit = saveAndAuditService.saveOutResultAndAudit(saveRequest);
                if (!outResultAndAudit.isOK()) {
                    AssertUtils.logAndThrow("发货失败!" + outResultAndAudit.getMessage());
                }
                accSuccess++;
            } catch (Exception e) {
                accFailed = errorRecord(objId, e.getMessage(), errorArr, accFailed);
            }
        }

        if (errorArr.size() > 0) {
            SgR3BaseResult baseResult = new SgR3BaseResult();
            baseResult.setDataArr(errorArr);
            v14.setCode(ResultCode.FAIL);
            v14.setMessage("发货成功记录数：" + accSuccess + "，发货失败记录数：" + accFailed);
            v14.setData(baseResult);
        } else {
            v14.setCode(ResultCode.SUCCESS);
            v14.setMessage("发货成功记录数：" + accSuccess);
        }
        return v14;
    }

    public void checkParams(SgPhyOutNoticesPosRequest request) {
        User loginUser = request.getLoginUser();
        AssertUtils.notNull(loginUser, "请传入用户信息！");
        if (CollectionUtils.isEmpty(request.getIds())) {
            AssertUtils.logAndThrow("请选择需要发货的数据！", loginUser.getLocale());
        }
    }

    public Integer errorRecord(Long mainId, String message, JSONArray errorArr, Integer accFailed) {
        JSONObject errorDate = new JSONObject();
        errorDate.put(R3ParamConstants.CODE, ResultCode.FAIL);
        errorDate.put(R3ParamConstants.OBJID, mainId);
        errorDate.put(R3ParamConstants.MESSAGE, message);
        errorArr.add(errorDate);
        return ++accFailed;
    }
}
