package com.jackrain.nea.sg.phyadjust.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.basic.common.R3ParamConstants;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.model.result.SgR3BaseResult;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.phyadjust.mapper.SgBPhyAdjustMapper;
import com.jackrain.nea.sg.phyadjust.model.request.SgPhyAdjustBillSaveRequest;
import com.jackrain.nea.sg.phyadjust.model.table.SgBPhyAdjust;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Locale;


/**
 * @author csy
 */

@Slf4j
@Component
public class SgPhyAdjSaveAndAuditService {

    @Transactional(rollbackFor = Exception.class)
    public ValueHolderV14 saveAndAudit(SgPhyAdjustBillSaveRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("Start SgPhyAdjSaveAndAuditService.saveAndAudit. ReceiveParams:request:{};", JSONObject.toJSONString(request));
        }

        if (request == null || request.getLoginUser() == null) {
            throw new NDSException(Resources.getMessage("user is null,login first!"));
        }

        Long startTime = System.currentTimeMillis();
        User user = request.getLoginUser();
        Locale locale = user.getLocale();
        SgPhyAdjustSaveService saveService = ApplicationContextHandle.getBean(SgPhyAdjustSaveService.class);

        ValueHolderV14<SgR3BaseResult> saveResult = saveService.save(request, false);
        AssertUtils.notNull(saveResult, "调整单保存返回结果为空!", locale);
        AssertUtils.cannot(saveResult.getCode() == ResultCode.FAIL, "调整单新增失败-" + saveResult.getMessage(), locale);
        AssertUtils.cannot(saveResult.getData() == null ||
                        saveResult.getData().getDataJo() == null || saveResult.getData().getDataJo().getLong(R3ParamConstants.OBJID) == null
                , "调整单返回结果有误-无objId返回", locale);
        Long objId = saveResult.getData().getDataJo().getLong(R3ParamConstants.OBJID);
        AssertUtils.cannot(objId < 0, "调整单返回结果有误-objId:" + objId, locale);
        SgPhyAdjustAuditService adjAuditService = ApplicationContextHandle.getBean(SgPhyAdjustAuditService.class);
        ValueHolder auditResult = adjAuditService.audit(objId, user, request.getStoreRequest());
        AssertUtils.notNull(auditResult, "调整单审核返回结果为空!", locale);
        AssertUtils.cannot((Integer) auditResult.get("code") == ResultCode.FAIL, "调整单审核失败-" + saveResult.getMessage(), locale);

        ValueHolderV14 ret = new ValueHolderV14(ResultCode.SUCCESS, "调整单新增审核成功");
        //2019-09-11 产品需求返回新增成功的库存调整单单据编号
        SgBPhyAdjustMapper adjustMapper = ApplicationContextHandle.getBean(SgBPhyAdjustMapper.class);
        List<SgBPhyAdjust> adjusts = adjustMapper.selectList(new QueryWrapper<SgBPhyAdjust>().lambda().
                eq(SgBPhyAdjust::getId, objId).
                eq(SgBPhyAdjust::getIsactive, SgConstants.IS_ACTIVE_Y));
        if (CollectionUtils.isNotEmpty(adjusts)) {
            ret.setData(adjusts.get(0).getBillNo());
        }
        if (log.isDebugEnabled()) {
            log.debug("Finish SgPhyAdjSaveAndAuditService.saveAndAudit. return:{},spend time:{};",
                    JSONObject.toJSONString(ret), System.currentTimeMillis() - startTime);
        }
        return ret;
    }
}
