package com.jackrain.nea.sg.out.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.ext.sequence.util.SequenceGenUtil;
import com.jackrain.nea.redis.config.CusRedisTemplate;
import com.jackrain.nea.redis.util.RedisOpsUtil;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.out.common.SgOutConstants;
import com.jackrain.nea.sg.out.common.SgOutConstantsIF;
import com.jackrain.nea.sg.out.mapper.*;
import com.jackrain.nea.sg.out.model.request.SgPhyOutPickOrderAuditRequest;
import com.jackrain.nea.sg.out.model.request.SgPhyOutResultBillSaveRequest;
import com.jackrain.nea.sg.out.model.request.SgPhyOutResultImpItemSaveRequest;
import com.jackrain.nea.sg.out.model.request.SgPhyOutResultSaveRequest;
import com.jackrain.nea.sg.out.model.table.*;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @author zhu lin yu
 * @since 2019-11-20
 * create at : 2019-11-20 16:53
 */
@Slf4j
@Component
public class SgPhyOutPickOrderAuditService {

    /**
     * 出库拣货单审核
     */
    @Transactional(rollbackFor = Exception.class)
    public ValueHolderV14 auditOutPickOrder(SgPhyOutPickOrderAuditRequest request) {
        ValueHolderV14 holderV14 = new ValueHolderV14<>();

        if (log.isDebugEnabled()) {
            log.debug("Start SgPhyBOutPickOrderAuditService.auditOutPickOrder. ReceiveParams:params:{};"
                    + JSONObject.toJSONString(request));
        }

        //参数检验
        if (request == null) {
            throw new NDSException("request is null");
        }

        Long outPickOrderId = request.getOutPickOrderId();
        AssertUtils.isTrue(outPickOrderId != null, "出库拣货单审核入参不能为空");
        AssertUtils.isTrue(request.getLoginUser() != null, "用户不能为空");
        User loginUser = request.getLoginUser();

        //判断出库拣货单是否已经审核
        SgBOutPickorderMapper outPickOrderMapper = ApplicationContextHandle.getBean(SgBOutPickorderMapper.class);
        SgBOutPickorder sgBOutPickorder = outPickOrderMapper.selectById(outPickOrderId);
        if (sgBOutPickorder == null) {
            throw new NDSException("该出库拣货单已不存在");
        }
        if (sgBOutPickorder.getBillStatus() == SgOutConstantsIF.PICK_ORDER_STATUS_AUDIT) {
            AssertUtils.logAndThrow("单据已已审核，不允许重复审核");
        }
        if (sgBOutPickorder.getBillStatus() == SgOutConstantsIF.PICK_ORDER_STATUS_VOID) {
            AssertUtils.logAndThrow("单据已作废，不允许审核");
        }

        SgBOutPickorderItemMapper outPickOrderItemMapper = ApplicationContextHandle.getBean(SgBOutPickorderItemMapper.class);
        List<SgBOutPickorderItem> sgBOutPickOrderItems = outPickOrderItemMapper.selectList(new QueryWrapper<SgBOutPickorderItem>().lambda()
                .eq(SgBOutPickorderItem::getSgBOutPickorderId, outPickOrderId));

        Map<Long, BigDecimal> teusQtyMap = new HashMap<>();
        Map<Long, BigDecimal> skuQtyMap = new HashMap<>();

        //判断拣货明细表中是否存在任意行记录出库数量大于通知数量
        int flag = 0;
        for (SgBOutPickorderItem item : sgBOutPickOrderItems) {
            if (item.getQtyOut().compareTo(BigDecimal.ZERO) > 0) {
                flag++;
            }

            if (item.getQtyOut().compareTo(item.getQtyNotice()) > 0) {
                AssertUtils.logAndThrow("出库数量大于总数量，不允许审核！");
            } else if (item.getPsCTeusId() == null) {
                skuQtyMap.put(item.getPsCSkuId(), item.getQtyOut());
            } else if (item.getPsCTeusId() != null) {
                teusQtyMap.put(item.getPsCTeusId(), item.getQtyOut());
            }
        }

        AssertUtils.isTrue(flag > 0, "单据出库数量为空，不允许审核！");


        //查询来源出库通知单
        SgBOutPickorderNoticeItemMapper outPickOrderNoticeItemMapper = ApplicationContextHandle.getBean(SgBOutPickorderNoticeItemMapper.class);
        List<SgBOutPickorderNoticeItem> noticeItemList = outPickOrderNoticeItemMapper.selectList(new QueryWrapper<SgBOutPickorderNoticeItem>().lambda()
                .eq(SgBOutPickorderNoticeItem::getSgBOutPickorderId, outPickOrderId));
        AssertUtils.isTrue(CollectionUtils.isNotEmpty(noticeItemList), "审核失败，来源出库通知单为空");
        List<String> noticeBillNoList = noticeItemList.stream().map(SgBOutPickorderNoticeItem::getNoticeBillNo).collect(Collectors.toList());

        //查询出库通知单
        SgBPhyOutNoticesMapper outNoticesMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesMapper.class);
        List<SgBPhyOutNotices> sgBPhyOutNoticesList = outNoticesMapper.selectList(new QueryWrapper<SgBPhyOutNotices>().lambda()
                .in(SgBPhyOutNotices::getBillNo, noticeBillNoList)
                .orderByAsc(SgBPhyOutNotices::getCreationdate));


        //循环来源出库通知单,调用出库结果单审核接口
        SgPhyOutResultSaveAndAuditService saveAndAuditservice = ApplicationContextHandle.getBean(SgPhyOutResultSaveAndAuditService.class);
        SgBPhyOutNoticesImpItemMapper outNoticesImpItemMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesImpItemMapper.class);

        List<SgPhyOutResultBillSaveRequest> sgPhyOutResultBillSaveRequests = new ArrayList<>();
        for (SgBPhyOutNotices sgBPhyOutNotices : sgBPhyOutNoticesList) {
            List<SgBPhyOutNoticesImpItem> outNoticesImpItemList = outNoticesImpItemMapper.selectList(new QueryWrapper<SgBPhyOutNoticesImpItem>().lambda()
                    .eq(SgBPhyOutNoticesImpItem::getSgBPhyOutNoticesId, sgBPhyOutNotices.getId())
                    .gt(SgBPhyOutNoticesImpItem::getQtyDiff, BigDecimal.ZERO));

            /**
             * 分配出库数量：
             * 当出库数量=通知数量时，所有出库通知单的所有明细中出库数量=通知数量；
             * 当出库数量＜通知数量时，根据来源出库通知单的创建时间的先后顺序优先分配，时间越早的优先分配；
             */
            List<SgPhyOutResultImpItemSaveRequest> outResultImpItemSaveRequests = new ArrayList<>();
            int allQtyFlag = 0;
            //标记明细出库数量为0的条数
            int allZeroFlag = 0;
            for (SgBPhyOutNoticesImpItem impItem : outNoticesImpItemList) {
                //散码
                if (impItem.getPsCTeusId() == null) {
                    if (skuQtyMap.containsKey(impItem.getPsCSkuId())) {
                        BigDecimal totalQty = skuQtyMap.get(impItem.getPsCSkuId());
                        if (totalQty != null) {
                            SgPhyOutResultImpItemSaveRequest outResultImpItemSaveRequest = new SgPhyOutResultImpItemSaveRequest();
                            BeanUtils.copyProperties(impItem, outResultImpItemSaveRequest);
                            if (totalQty.compareTo(impItem.getQtyDiff()) >= 0) {
                                outResultImpItemSaveRequest.setQtyOut(impItem.getQtyDiff());
                                skuQtyMap.put(impItem.getPsCSkuId(), totalQty.subtract(impItem.getQtyDiff()));
                                outResultImpItemSaveRequests.add(outResultImpItemSaveRequest);
                                allQtyFlag++;
                            } else {
                                if (totalQty.compareTo(BigDecimal.ZERO) == 0) {
                                    allZeroFlag++;
                                }
                                outResultImpItemSaveRequest.setQtyOut(totalQty);
                                skuQtyMap.remove(impItem.getPsCSkuId());
                                outResultImpItemSaveRequests.add(outResultImpItemSaveRequest);
                            }
                        }
                    } else {
                        SgPhyOutResultImpItemSaveRequest outResultImpItemSaveRequest = new SgPhyOutResultImpItemSaveRequest();
                        BeanUtils.copyProperties(impItem, outResultImpItemSaveRequest);
                        outResultImpItemSaveRequest.setQtyOut(BigDecimal.ZERO);
                        outResultImpItemSaveRequests.add(outResultImpItemSaveRequest);
                        allZeroFlag++;
                    }
                    //箱
                } else if (impItem.getPsCTeusId() != null) {
                    if (teusQtyMap.containsKey(impItem.getPsCTeusId())) {
                        BigDecimal totalQty = teusQtyMap.get(impItem.getPsCTeusId());
                        if (totalQty != null) {
                            SgPhyOutResultImpItemSaveRequest outResultImpItemSaveRequest = new SgPhyOutResultImpItemSaveRequest();
                            BeanUtils.copyProperties(impItem, outResultImpItemSaveRequest);

                            if (totalQty.compareTo(impItem.getQtyDiff()) > 0) {
                                outResultImpItemSaveRequest.setQtyOut(impItem.getQtyDiff());
                                teusQtyMap.put(impItem.getPsCTeusId(), totalQty.subtract(impItem.getQtyDiff()));
                                outResultImpItemSaveRequests.add(outResultImpItemSaveRequest);
                                allQtyFlag++;
                            } else {
                                if (totalQty.compareTo(BigDecimal.ZERO) == 0) {
                                    allZeroFlag++;
                                }
                                outResultImpItemSaveRequest.setQtyOut(totalQty);
                                teusQtyMap.remove(impItem.getPsCTeusId());
                                outResultImpItemSaveRequests.add(outResultImpItemSaveRequest);
                            }
                        }
                    } else {
                        SgPhyOutResultImpItemSaveRequest outResultImpItemSaveRequest = new SgPhyOutResultImpItemSaveRequest();
                        BeanUtils.copyProperties(impItem, outResultImpItemSaveRequest);
                        outResultImpItemSaveRequest.setQtyOut(BigDecimal.ZERO);
                        outResultImpItemSaveRequests.add(outResultImpItemSaveRequest);
                        allZeroFlag++;
                    }

                }
            }

            SgPhyOutResultBillSaveRequest outResultBillSaveRequest = new SgPhyOutResultBillSaveRequest();
            SgPhyOutResultSaveRequest outResultSaveRequest = new SgPhyOutResultSaveRequest();
            BeanUtils.copyProperties(sgBPhyOutNotices, outResultSaveRequest);
            outResultSaveRequest.setSgBPhyOutNoticesId(sgBPhyOutNotices.getId());
            outResultSaveRequest.setSgBPhyOutNoticesBillno(sgBPhyOutNotices.getBillNo());
            outResultSaveRequest.setTotQtyOut(sgBPhyOutNotices.getTotQtyDiff());
            if (allQtyFlag == outNoticesImpItemList.size()) {
                outResultSaveRequest.setIsLast(SgConstants.IS_LAST_YES);
            } else {
                outResultSaveRequest.setIsLast(SgConstants.IS_LAST_NO);
            }
            outResultBillSaveRequest.setImpItemList(outResultImpItemSaveRequests);
            outResultBillSaveRequest.setOutResultRequest(outResultSaveRequest);
            outResultBillSaveRequest.setLoginUser(request.getLoginUser());
            outResultBillSaveRequest.setObjId(-1L);

            outResultBillSaveRequest.setPackOrderId(outPickOrderId);

            //明细数量全为0时不生成结果单
            if (allZeroFlag != outNoticesImpItemList.size()) {
                sgPhyOutResultBillSaveRequests.add(outResultBillSaveRequest);
            } else {
                SgBPhyOutNotices updatePhyOutNotice = new SgBPhyOutNotices();
                updatePhyOutNotice.setId(sgBPhyOutNotices.getId());
                updatePhyOutNotice.setPickStatus(SgOutConstantsIF.PICK_STATUS_WAIT);
                outNoticesMapper.updateById(updatePhyOutNotice);
            }
        }

        CusRedisTemplate<String, String> redisTemplate = RedisOpsUtil.getObjRedisTemplate();
        String lockKsy = SgConstants.SG_B_OUT_PICKORDER + ":" + outPickOrderId;
        Boolean ifAbsent = redisTemplate.opsForValue().setIfAbsent(lockKsy, "OK");

        if (ifAbsent != null && ifAbsent) {
            redisTemplate.opsForValue().set(lockKsy, "OK", 30, TimeUnit.MINUTES);
        } else {
            String msg = "当前单据正在被操作！请稍后重试..";
            log.debug(this.getClass().getName() + ".debug," + msg);
            AssertUtils.logAndThrow(msg, loginUser.getLocale());
        }

        try {
            //调用出库结果单保存并审核服务
            for (SgPhyOutResultBillSaveRequest sgPhyOutResultBillSaveRequest : sgPhyOutResultBillSaveRequests) {
                holderV14 = saveAndAuditservice.saveOutResultAndAudit(sgPhyOutResultBillSaveRequest);
                AssertUtils.isTrue(holderV14.isOK(), holderV14.getMessage());
            }

            //生成装箱明细表的拣货箱箱号
            SgBOutPickorderTeusItemMapper teusItemMapper = ApplicationContextHandle.getBean(SgBOutPickorderTeusItemMapper.class);
            List<SgBOutPickorderTeusItem> sgBOutPickorderTeusItems = teusItemMapper.selectList(new QueryWrapper<SgBOutPickorderTeusItem>().lambda()
                    .eq(SgBOutPickorderTeusItem::getSgBOutPickorderId, outPickOrderId));
            List<Long> pickOutTeusIdList = sgBOutPickorderTeusItems.stream().map(SgBOutPickorderTeusItem::getPickOutTeusId).collect(Collectors.toList());

            if (CollectionUtils.isNotEmpty(pickOutTeusIdList)) {
                JSONObject obj = new JSONObject();
                obj.put(SgConstants.SG_B_OUT_PICKORDER_TEUS_ITEM.toUpperCase(), sgBOutPickorder);
                for (Long pickOutTeusId : pickOutTeusIdList) {
                    SgBOutPickorderTeusItem updateItem = new SgBOutPickorderTeusItem();
                    String billNo = SequenceGenUtil.generateSquence(SgOutConstants.SQE_SG_B_OUT_PICK_TEUS_ITEM, obj, request.getLoginUser().getLocale(), false);
                    //                String billNo = "测试2";
                    updateItem.setPickOutBillNo(billNo);
                    teusItemMapper.update(updateItem, new UpdateWrapper<SgBOutPickorderTeusItem>().lambda()
                            .eq(SgBOutPickorderTeusItem::getPickOutTeusId, pickOutTeusId)
                            .eq(SgBOutPickorderTeusItem::getSgBOutPickorderId, pickOutTeusId));
                }
            }

            //跟新出库拣货单主表信息
            SgBOutPickorder updateOutPickOrder = new SgBOutPickorder();
            updateOutPickOrder.setBillStatus(SgOutConstantsIF.PICK_ORDER_STATUS_AUDIT);
            updateOutPickOrder.setModifiername(loginUser.getName());
            updateOutPickOrder.setModifieddate(new Date());
            updateOutPickOrder.setId(sgBOutPickorder.getId());
            updateOutPickOrder.setStatusId(loginUser.getId());
            updateOutPickOrder.setStatusName(loginUser.getName());
            updateOutPickOrder.setStatusTime(new Date());
            outPickOrderMapper.updateById(updateOutPickOrder);


        } catch (Exception e) {
            log.error(this.getClass().getName() + " 出库拣货单调用出库结果单审核错误,{}", e.getMessage(), e);
            AssertUtils.logAndThrow("出库拣货单调用出库结果单审核错误");
        } finally {
            try {
                redisTemplate.delete(lockKsy);
            } catch (Exception e) {
                log.error(this.getClass().getName() + " redis锁释放失败，请联系管理员！lockKsy：" + lockKsy);
                AssertUtils.logAndThrow("redis锁释放失败，请联系管理员！");
            }
        }

        holderV14.setCode(ResultCode.SUCCESS);
        holderV14.setMessage("审核成功");
        return holderV14;
    }

}
