package com.jackrain.nea.sg.pack.service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.oc.basic.api.TableServiceCmd;
import com.jackrain.nea.sg.pack.common.SgTeusPackConstants;
import com.jackrain.nea.sg.pack.model.table.SgBTeusPackItem;
import com.jackrain.nea.sg.pack.model.request.SgPackSaveAndAuditRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.query.DefaultWebEvent;
import com.jackrain.nea.web.query.QuerySessionImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * @author: 周琳胜
 * @since: 2019/12/4
 * create at : 2019/12/4 11:32
 */
@Slf4j
@Component
public class SgTeusPackSaveAndAuditService {

    public ValueHolderV14 saveAndAuditSgPack(SgPackSaveAndAuditRequest request) {
        ValueHolderV14 v14 = new ValueHolderV14();
        if (log.isDebugEnabled()) {
            log.debug("Start SgTeusPackSaveAndAuditService.saveAndAuditSgPack. ReceiveParams:requests:{}"
                    , JSONObject.toJSONString(request));
        }
        if (request == null) {
            v14.setCode(ResultCode.FAIL);
            v14.setMessage("参数不能为空！");
            return v14;
        }

        ValueHolderV14 holderV14 = checkParam(request);
        if (!holderV14.isOK()) {
            return holderV14;
        }

        User user = request.getUser();

        // 新增装箱单
        ValueHolder result = saveR3(request);
        if (result.isOK()) {
            JSONObject data = (JSONObject) result.getData().get("data");
            Long objid = data.getLong("objid");
            ValueHolder valueHolder = auditR3(objid, user);
            if (!valueHolder.isOK()) {
                HashMap jo = valueHolder.getData();
                v14.setMessage(jo.get("message").toString());
                v14.setCode(ResultCode.FAIL);
                return v14;
            }
        } else {
            v14.setCode(ResultCode.FAIL);
            HashMap jo = result.getData();
            v14.setMessage(jo.get("message").toString());
            return v14;
        }
        if (log.isDebugEnabled()) {
            log.debug("Finsh SgUnpackSaveAndAuditService.saveAndAuditSgUnpack. Result:valueHolderV14:{}"
                    , JSONObject.toJSONString(v14));
        }
        v14.setCode(ResultCode.SUCCESS);
        v14.setMessage("成功！");
        return v14;
    }

    /**
     * 框架新增 封装参数
     *
     * @param request
     * @return
     */
    private ValueHolder saveR3(SgPackSaveAndAuditRequest request) {
        Date biiiDate = request.getBiiiDate();
        Long cpCPhyWarehouseId = request.getCpCPhyWarehouseId();
        List<SgBTeusPackItem> itemList = request.getItemList();
        User user = request.getUser();
        JSONObject param = new JSONObject();
        JSONObject fixcolumn = new JSONObject();
        JSONObject mainDate = new JSONObject();
        mainDate.put("CP_C_PHY_WAREHOUSE_ID", cpCPhyWarehouseId);
        mainDate.put("BILL_DATE", biiiDate);
        mainDate.put("REMARK", request.getRemark());

        fixcolumn.put(SgTeusPackConstants.SG_B_TEUS_PACK.toUpperCase(), mainDate);
        JSONArray itemArray = new JSONArray();
        itemList.forEach(x -> {
            JSONObject itemDate = new JSONObject();
            itemDate.put("QTY", x.getQty());
            itemDate.put("PS_C_TEUS_ID", x.getPsCTeusId());
            itemDate.put("ID", -1L);
            itemDate.put("PS_C_SKU_ID", x.getPsCSkuId());
            itemArray.add(itemDate);
        });
        fixcolumn.put(SgTeusPackConstants.SG_B_TEUS_PACK_ITEM.toUpperCase(), itemArray);
        param.put("fixcolumn", fixcolumn);
        param.put("objid", -1L);
        param.put("table", SgTeusPackConstants.SG_B_TEUS_PACK.toUpperCase());

        QuerySessionImpl querySession = new QuerySessionImpl(user);
        //默认 走save操作
        DefaultWebEvent event = new DefaultWebEvent(param.getString("eventName") == null ? "dosave" : param.getString("eventName"), new HashMap());
        event.put("param", param);
        querySession.setEvent(event);
        Object o = ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                TableServiceCmd.class.getName(), "sg", "7");
        ValueHolder result = ((TableServiceCmd) o).execute(querySession);
        return result;
    }

    /**
     * 框架审核
     *
     * @param id
     * @param user
     * @return
     */
    private ValueHolder auditR3(Long id, User user) {
        JSONObject param = new JSONObject();
        param.put("objid", id);
        param.put("table", SgTeusPackConstants.SG_B_TEUS_PACK.toUpperCase());

        QuerySessionImpl querySession = new QuerySessionImpl(user);
        //默认 走审核操作
        DefaultWebEvent event = new DefaultWebEvent(param.getString("eventName") == null ? "doSubmit" : param.getString("eventName"), new HashMap());
        event.put("param", param);
        querySession.setEvent(event);
        Object o = ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                TableServiceCmd.class.getName(), "sg", "7");
        ValueHolder result = ((TableServiceCmd) o).execute(querySession);
        return result;
    }

    private ValueHolderV14 checkParam(SgPackSaveAndAuditRequest request) {
        ValueHolderV14 v14 = new ValueHolderV14(ResultCode.SUCCESS, "");

        if (request == null) {
            v14.setCode(ResultCode.FAIL);
            v14.setMessage("参数不能为空！");
            return v14;
        }

        if (request.getBiiiDate() == null) {
            v14.setCode(ResultCode.FAIL);
            v14.setMessage("单据日期不能为空！");
            return v14;
        }

        if (request.getCpCPhyWarehouseId() == null) {
            v14.setCode(ResultCode.FAIL);
            v14.setMessage("实体仓不能为空！");
            return v14;
        }

        if (CollectionUtils.isEmpty(request.getItemList())) {
            v14.setCode(ResultCode.FAIL);
            v14.setMessage("明细不能为空！");
            return v14;
        }

        if (request.getUser() == null) {
            v14.setCode(ResultCode.FAIL);
            v14.setMessage("用户信息不能为空！");
            return v14;
        }

        return v14;
    }
}
