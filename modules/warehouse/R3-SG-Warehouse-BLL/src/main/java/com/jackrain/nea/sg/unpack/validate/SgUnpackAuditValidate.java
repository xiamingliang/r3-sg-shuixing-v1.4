package com.jackrain.nea.sg.unpack.validate;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.data.basic.model.request.StoreInfoQueryRequest;
import com.jackrain.nea.data.basic.services.BasicCpQueryService;
import com.jackrain.nea.data.basic.services.BasicPsQueryService;
import com.jackrain.nea.psext.model.table.PsCTeus;
import com.jackrain.nea.sg.basic.api.SgTeusPhyStorageQueryCmd;
import com.jackrain.nea.sg.basic.api.SgTeusStorageQueryCmd;
import com.jackrain.nea.sg.basic.mapper.CpStoreMapper;
import com.jackrain.nea.sg.basic.model.request.SgTeusPhyStorageQueryRequest;
import com.jackrain.nea.sg.basic.model.request.SgTeusStorageQueryRequest;
import com.jackrain.nea.sg.basic.model.table.CpCStore;
import com.jackrain.nea.sg.basic.model.table.SgBPhyTeusStorage;
import com.jackrain.nea.sg.basic.model.table.SgBTeusStorage;
import com.jackrain.nea.sg.basic.utils.StorageUtils;
import com.jackrain.nea.sg.unpack.common.SgUnpackConstants;
import com.jackrain.nea.sg.unpack.mapper.SgBTeusUnpackItemMapper;
import com.jackrain.nea.sg.unpack.mapper.SgBTeusUnpackMapper;
import com.jackrain.nea.sg.unpack.model.table.SgBTeusUnpack;
import com.jackrain.nea.sg.unpack.model.table.SgBTeusUnpackItem;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.tableService.MainTableRecord;
import com.jackrain.nea.tableService.TableServiceContext;
import com.jackrain.nea.tableService.validate.BaseValidator;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.utils.AssertUtils;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author zhoulinsheng
 * @since 2019/11/27 14:02
 */
@Slf4j
@Component
public class SgUnpackAuditValidate extends BaseValidator {

    @Override
    public void validate(TableServiceContext context, MainTableRecord currentRow) {

        Long objId = currentRow.getId();
        AssertUtils.notNull(objId, "拆箱单ID为空！", context.getLocale());

        SgBTeusUnpackMapper unpackMapper = ApplicationContextHandle.getBean(SgBTeusUnpackMapper.class);
        SgBTeusUnpack sgBTeusUnpack = unpackMapper.selectById(objId);
        AssertUtils.notNull(sgBTeusUnpack, "当前记录已不存在！", context.getLocale());
        // 单据状态
        Integer status = sgBTeusUnpack.getStatus();
        // 当前拆箱单是否存在逻辑仓id  不存在说明是wms回传的
        Long unpackCpCStoreId = sgBTeusUnpack.getCpCStoreId();

        if (SgUnpackConstants.BILL_STATUS_AUDIT == status) {
            AssertUtils.logAndThrow("当前记录已审核，不允许重复审核！", context.getLocale());
        } else if (SgUnpackConstants.BILL_STATUS_VOID == status) {
            AssertUtils.logAndThrow("当前记录已作废，不允许审核！", context.getLocale());
        }
        // 删除明细中拆箱数量为0的记录
        SgBTeusUnpackItemMapper unpackItemMapper = ApplicationContextHandle.getBean(SgBTeusUnpackItemMapper.class);
        unpackItemMapper.delete(new QueryWrapper<SgBTeusUnpackItem>().lambda().eq(SgBTeusUnpackItem::getQty, BigDecimal.ZERO));
        // 判断明细记录是否为0   如果是 则不允许审核
        List<SgBTeusUnpackItem> unpackItemList = unpackItemMapper.selectList(new QueryWrapper<SgBTeusUnpackItem>().lambda().eq(SgBTeusUnpackItem::getSgBTeusUnpackId, objId));
        AssertUtils.isTrue(unpackItemList.size() > 0, "当前记录无明细，不允许审核", context.getLocale());
        Map<String, SgBTeusUnpackItem> itemMapTeusCode = unpackItemList.stream().collect(Collectors.toMap(SgBTeusUnpackItem::getPsCTeusEcode, o -> o));
        BasicPsQueryService queryService = ApplicationContextHandle.getBean(BasicPsQueryService.class);
        List<String> teusEcodeList = unpackItemList.stream()
                .filter(o -> o.getPsCTeusEcode() != null)
                .map(SgBTeusUnpackItem::getPsCTeusEcode).collect(Collectors.toList());
        HashMap<String, PsCTeus> teusHashMap = queryService.queryTeusInfoByEcode(teusEcodeList);
        teusEcodeList.forEach(x ->{
            PsCTeus psCTeus = teusHashMap.get(x);
            AssertUtils.notNull(psCTeus, "箱[" + x + "]不存在", context.getLocale());
            // 箱定义中的箱状态=已拆箱，则提示“当前箱：XXX已拆箱，不允许再次拆箱
            Integer teusStatus = psCTeus.getTeusStatus();
            AssertUtils.cannot(SgUnpackConstants.BOX_STATUS_SPLIT == teusStatus, Resources.getMessage("当前箱：" + x + "已拆箱，不允许再次拆箱！", context.getLocale()));
        });

        /**
         * g)	根据箱号+实体仓对应的逻辑仓在【逻辑仓箱库存】中占用箱数>0，则提示“当前箱：XXX已占用，不允许拆箱！”
         */
        List<Long> storeIds = new ArrayList<>();
        if (unpackCpCStoreId == null) {
            CpStoreMapper cpStoreMapper = ApplicationContextHandle.getBean(CpStoreMapper.class);

            List<CpCStore> cpCStoreList = cpStoreMapper.selectList(new QueryWrapper<CpCStore>().lambda().eq(CpCStore::getCpCPhyWarehouseId, sgBTeusUnpack.getCpCPhyWarehouseId()));
            AssertUtils.isTrue(CollectionUtils.isNotEmpty(cpCStoreList), "根据此实体仓未查到逻辑仓!", context.getLocale());
            storeIds = cpCStoreList.stream().map(CpCStore::getId).collect(Collectors.toList());
        } else {
            storeIds.add(unpackCpCStoreId);
        }

        SgTeusStorageQueryCmd storageQueryCmd = (SgTeusStorageQueryCmd)
                ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                        SgTeusStorageQueryCmd.class.getName(), "sg", "1.0");
        SgTeusStorageQueryRequest sgTeusStorageQueryRequest = new SgTeusStorageQueryRequest();
        sgTeusStorageQueryRequest.setStoreIds(storeIds);
        sgTeusStorageQueryRequest.setTeusEcodes(teusEcodeList);
        ValueHolderV14<List<SgBTeusStorage>> holderV14 = storageQueryCmd.queryTeusStorage(sgTeusStorageQueryRequest, context.getUser());
        if (holderV14.isOK()) {


            List<SgBTeusStorage> teusStorageList = holderV14.getData();
            Map<Long, Map<String, SgBTeusStorage>> map1 = new HashMap<>();

            Map<Long, List<SgBTeusStorage>> map2 = new HashMap<>();
            Set<Long> sets = new HashSet<>();
            for (SgBTeusStorage storage : teusStorageList) {
                if (sets.add(storage.getCpCStoreId())){
                    List<SgBTeusStorage> list = new ArrayList<>();
                    list.add(storage);
                    map2.put(storage.getCpCStoreId(), list);
                    continue;
                }
                List<SgBTeusStorage> sgBTeusStorages = map2.get(storage.getCpCStoreId());
                sgBTeusStorages.add(storage);
            }

            for (Map.Entry<Long , List<SgBTeusStorage>> longListEntry : map2.entrySet()) {
                Map<String, SgBTeusStorage> map3 = new HashMap<>();
                Long key = longListEntry.getKey();
                List<SgBTeusStorage> value = longListEntry.getValue();
                for (SgBTeusStorage sgBTeusStorage : value) {
                    if (map3.keySet().contains(sgBTeusStorage.getPsCTeusEcode())){
                        continue;
                    }
                    map3.put(sgBTeusStorage.getPsCTeusEcode(), sgBTeusStorage);
                }
                map1.put(key, map3);
            }


            if (CollectionUtils.isNotEmpty(teusEcodeList)) {

                Map<String, SgBTeusStorage> storageMap = teusStorageList.stream().filter(o -> StringUtils.isNotEmpty(o.getPsCTeusEcode()))
                        .collect(Collectors.toMap(SgBTeusStorage::getPsCTeusEcode, o -> o));

                teusEcodeList.forEach(x -> {
                    SgBTeusStorage sgBTeusStorage = storageMap.get(x);
                    AssertUtils.notNull(sgBTeusStorage, "当前箱：" + x + "无箱库存记录!", context.getLocale());
                    BigDecimal qtyPreout = sgBTeusStorage.getQtyPreout();
                    AssertUtils.isTrue(!(qtyPreout.compareTo(BigDecimal.ZERO) > 0), "当前箱：" + x + "已占用，不允许拆箱!", context.getLocale());
                });
            }

            /**
             * 判断是否有符合所有箱的逻辑仓
             */
            Long resultStoreId = null;
            Integer count = 0;
            for (Long storeId : storeIds) {
                for (String teusEcode : teusEcodeList) {
                    Map<String, SgBTeusStorage> stringListMap = map1.get(storeId);
                    SgBTeusStorage storage = stringListMap.get(teusEcode);
                            BigDecimal qtyAvailable = storage.getQtyAvailable();
                    SgBTeusUnpackItem sgBTeusUnpackItem = itemMapTeusCode.get(teusEcode);
                    if (qtyAvailable.compareTo(sgBTeusUnpackItem.getQty()) < 0){
                                continue;
                            }
                            count++;
                }
                if (count == teusEcodeList.size()) {
                    resultStoreId = storeId;
                }
            }
            if (resultStoreId != null) {
                BasicCpQueryService cpQueryService = ApplicationContextHandle.getBean(BasicCpQueryService.class);
                StoreInfoQueryRequest storeInfoQueryRequest = new StoreInfoQueryRequest();
                List<Long> ids = new ArrayList<>();
                ids.add(resultStoreId);
                storeInfoQueryRequest.setIds(ids);
                HashMap<Long, com.jackrain.nea.cp.result.CpCStore> storeInfo = new HashMap<>();
                try {
                    storeInfo = cpQueryService.getStoreInfo(storeInfoQueryRequest);
                } catch (Exception e) {
                    AssertUtils.logAndThrow("查询逻辑店仓信息异常！" + e.getMessage(), context.getLocale());
                }

                // 补充实体仓逻辑仓信息 并更新
                com.jackrain.nea.cp.result.CpCStore cpCStore = storeInfo.get(resultStoreId);
                SgBTeusUnpack unpack = new SgBTeusUnpack();

                User user = context.getUser();

                List<SgBTeusUnpackItem> itemList = unpackItemMapper.selectList(new QueryWrapper<SgBTeusUnpackItem>().lambda()
                        .eq(SgBTeusUnpackItem::getSgBTeusUnpackId, objId));
                // 计算总拆箱数

                BigDecimal countUnTeus = itemList.stream()
                        .map(SgBTeusUnpackItem::getQty).reduce(BigDecimal.ZERO, (a, b) -> a.add(b));

                JSONObject commitData = currentRow.getCommitData();
                commitData.put("QTY_TOT_UNPACK", countUnTeus);
                commitData.put("STATUS_ID", Long.valueOf(user.getId()));
                commitData.put("STATUS_NAME", user.getName());
                commitData.put("STATUS_TIME", new Date());
                commitData.put("CP_C_STORE_ID", resultStoreId);
                commitData.put("CP_C_STORE_ECODE", cpCStore.getEcode());
                commitData.put("CP_C_STORE_ENAME", cpCStore.getEname());
            } else {
                AssertUtils.logAndThrow( "未找到符合拆箱数量的逻辑仓！", context.getLocale());
            }
        }

        /**
         *  在【实体箱库存】中店仓+箱号的“在库箱数”<=0记录，则提示“当前箱：XXX不存在于店仓YYY中!
         *  在【实体箱库存】中店仓+箱号不存在记录，则提示“当前箱：XXX不存在于店仓YYY中！
         */
        SgTeusPhyStorageQueryCmd phyStorageQueryCmd = (SgTeusPhyStorageQueryCmd)
                ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                        SgTeusPhyStorageQueryCmd.class.getName(), "sg", "1.0");
        SgTeusPhyStorageQueryRequest phyStorageQueryRequest = new SgTeusPhyStorageQueryRequest();
        List<Long> phyWarehouseIds = new ArrayList<>();
        phyWarehouseIds.add(sgBTeusUnpack.getCpCPhyWarehouseId());
        phyStorageQueryRequest.setPhyWarehouseIds(phyWarehouseIds);
        phyStorageQueryRequest.setTeusEcodes(teusEcodeList);
        ValueHolderV14<List<SgBPhyTeusStorage>> v14 = phyStorageQueryCmd.queryTeusPhyStorage(phyStorageQueryRequest, context.getUser());
        if (v14.isOK()) {
            List<SgBPhyTeusStorage> phyTeusStorageList = v14.getData();
            Map<String, SgBPhyTeusStorage> storageMap = phyTeusStorageList.stream().filter(o -> StringUtils.isNotEmpty(o.getPsCTeusEcode()))
                    .collect(Collectors.toMap(SgBPhyTeusStorage::getPsCTeusEcode, o -> o));
            teusEcodeList.forEach(x -> {
                SgBPhyTeusStorage phyTeusStorage = storageMap.get(x);
                AssertUtils.notNull(phyTeusStorage, "当前箱：" + x + "不存在于店仓" + sgBTeusUnpack.getCpCPhyWarehouseEcode() + "中!", context.getLocale());
                BigDecimal qtyStorage = phyTeusStorage.getQtyStorage();
                AssertUtils.isTrue((qtyStorage.compareTo(BigDecimal.ZERO) > 0), "当前箱：" + x + "不存在于店仓" + sgBTeusUnpack.getCpCPhyWarehouseEcode() + "中!", context.getLocale());
            });
        }


    }





}