package com.jackrain.nea.sg.inv.services;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.inv.mapper.ScBInventoryImpItemMapper;
import com.jackrain.nea.sg.inv.mapper.ScBInventoryMapper;
import com.jackrain.nea.sg.inv.model.request.ScInvProfitLossBasePageRequest;
import com.jackrain.nea.sg.inv.model.request.ScInvProfitLossMarginQueryCmdRequest;
import com.jackrain.nea.sg.inv.model.table.ScBInventory;
import com.jackrain.nea.sg.inv.model.table.ScBInventoryImpItem;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Locale;
import java.util.Optional;


/**
 * @author 舒威
 * @since 2019/3/27
 * create at : 2019/3/27 15:57
 */
@Slf4j
@Component
public class ScInvProfitLossQueryService {

    @Autowired
    private ScBInventoryMapper inventoryMapper;

    @Autowired
    private ScBInventoryImpItemMapper inventoryItemMapper;

    /**
     * 查询未盈亏盘点单
     */
    ValueHolderV14<PageInfo<ScBInventory>> queryProfitLossInventory(ScInvProfitLossMarginQueryCmdRequest model) {
        if (log.isDebugEnabled()) {
            log.debug("Start ScInvProfitLossQueryService.queryProfitLossInventory. Receive:request:{}", JSONObject.toJSONString(model));
        }
        ValueHolderV14<PageInfo<ScBInventory>> v14 = new ValueHolderV14<>(ResultCode.SUCCESS, "success");

        User loginUser = model.getLoginUser();
        AssertUtils.notNull(loginUser, "当前用户未登录！");
        Locale locale = loginUser.getLocale();
        AssertUtils.isTrue(!(model.getStoreId() == null && model.getWarehouseId() == null), "请选择盘点店仓！", locale);
        AssertUtils.notNull(model.getInventoryType(), "请选择盘点类型！", locale);
        AssertUtils.notNull(model.getInventoryDate(), "请选择盘点日期！", locale);
        ScInvProfitLossBasePageRequest page = Optional.ofNullable(model.getPage()).orElse(new ScInvProfitLossBasePageRequest());
        String orderByName = StringUtils.isEmpty(page.getName()) ? "CREATIONDATE" : page.getName();
        String order = page.getIsasc() ? " asc" : " desc";

        PageHelper.startPage(page.getCurrentPage(), page.getPageSize(), orderByName + order);
        List<ScBInventory> inventoryList = inventoryMapper.selectList(new QueryWrapper<ScBInventory>().lambda()
                .eq(model.getStoreId() != null, ScBInventory::getCpCStoreId, model.getStoreId())
                .eq(model.getWarehouseId() != null, ScBInventory::getCpCPhyWarehouseId, model.getWarehouseId())
                .eq(model.getInventoryType() != null, ScBInventory::getInventoryType, model.getInventoryType())
                .eq(model.getInventoryDate() != null, ScBInventory::getInventoryDate, model.getInventoryDate())
                .eq(model.getPolStatus() != null, ScBInventory::getPolStatus, model.getPolStatus())
                .eq(ScBInventory::getIsactive, SgConstants.IS_ACTIVE_Y));
        PageInfo<ScBInventory> pageInfo = new PageInfo<>(inventoryList);
        List<ScBInventory> data = pageInfo.getList();

        JSONArray errArr = new JSONArray();
        for (int i = 0; i < data.size(); i++) {
            ScBInventory inventory = data.get(i);
            Integer cnt = inventoryItemMapper.selectCount(new QueryWrapper<ScBInventoryImpItem>().lambda()
                    .eq(ScBInventoryImpItem::getScBInventoryId, inventory.getId()));
            if (cnt != null && cnt < 1) {
                errArr.add(inventory.getBillNo());
            }
        }

        if (CollectionUtils.isNotEmpty(errArr)) {
            v14.setCode(ResultCode.FAIL);
            String join = StringUtils.join(errArr, ",");
            v14.setMessage("盘点单[" + join + "],明细为空，不允许预盈亏！");
            return v14;
        }

        if (CollectionUtils.isNotEmpty(data)) {
            v14.setData(pageInfo);
        } else {
            v14.setCode(ResultCode.FAIL);
            v14.setMessage("所选盘点日期不存在盘点单！");
            return v14;
        }
        return v14;
    }

}
