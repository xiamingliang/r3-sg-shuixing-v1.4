package com.jackrain.nea.sg.inv.mapper;

import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.inv.model.table.SgBInventoryItem;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.Date;
import java.util.List;

@Mapper
public interface SgBInventoryItemMapper extends ExtentionMapper<SgBInventoryItem> {

    @Delete("DELETE FROM sc_b_inventory_item WHERE id IN (${ids}) AND sc_b_inventory_id = #{mainId}")
    int deleteItemById(@Param("ids") String ids, @Param("mainId") Long mainId);

    @Select("SELECT DISTINCT\n" +
            "\tPS_C_PRO_ECODE\n" +
            "FROM\n" +
            "\tSC_B_INVENTORY_ITEM\n" +
            "WHERE\n" +
            "\tSC_B_INVENTORY_ID IN (\n" +
            "\t\tSELECT\n" +
            "\t\t\tID\n" +
            "\t\tFROM\n" +
            "\t\t\tSC_B_INVENTORY\n" +
            "\t\tWHERE\n" +
            "\t\t\tCP_C_STORE_ID = #{storeId}\n" +
            "\t\tAND INVENTORY_TYPE = #{inventoryType}\n" +
            "\t\tAND INVENTORY_DATE = #{inventoryDate}\n" +
            "\t\tAND POL_STATUS = #{polStatus}\n" +
            "\t\tAND ISACTIVE = 'Y'\n" +
            "\t)")
    List<String> selectProEcodes(@Param("storeId") Long storeId,
                                 @Param("inventoryType") String pandType,
                                 @Param("inventoryDate") Date pandDate,
                                 @Param("polStatus") Integer polStatus);
}