package com.jackrain.nea.sg.inv.mapper;

import com.jackrain.nea.oc.basic.common.OcBasicConstants;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.inv.constants.SgInvConstants;
import org.apache.ibatis.annotations.Param;

import java.util.Date;

/**
 * @author: 舒威
 * @since: 2019/4/3
 * create at : 2019/4/3 15:07
 */
public class ScProfitSqlProvider {

    /**
     * 全盘-生成盈亏
     */
    public String queryProfItemByQPC(@Param("storeId") Long storeId,
                                     @Param("inventoryDate") Date inventoryDate,
                                     @Param("inventoryType") Integer inventoryType,
                                     @Param("flag") Boolean flag) {
        StringBuilder sql = new StringBuilder("SELECT\n" +
                "\tCOALESCE (D.PS_C_SKU_ID,P .PS_C_SKU_ID) \"PS_C_SKU_ID\",\n" +
                "\tCOALESCE (D.PS_C_SKU_ECODE,P .PS_C_SKU_ECODE) \"PS_C_SKU_ECODE\",\n" +
                "\tCOALESCE (D.PS_C_PRO_ID,P .PS_C_PRO_ID) \"PS_C_PRO_ID\",\n" +
                "\tCOALESCE (D.PS_C_PRO_ECODE,P .PS_C_PRO_ECODE) \"PS_C_PRO_ECODE\",\n" +
                "\tCOALESCE (D.PS_C_PRO_ENAME,P .PS_C_PRO_ENAME) \"PS_C_PRO_ENAME\",\n" +
                "\tCOALESCE (D.PS_C_SPEC1_ID,P .PS_C_SPEC1_ID) \"PS_C_SPEC1_ID\",\n" +
                "\tCOALESCE (D.PS_C_SPEC1_ECODE,P .PS_C_SPEC1_ECODE) \"PS_C_SPEC1_ECODE\",\n" +
                "\tCOALESCE (D.PS_C_SPEC1_ENAME,P .PS_C_SPEC1_ENAME) \"PS_C_SPEC1_ENAME\",\n" +
                "\tCOALESCE (D.PS_C_SPEC2_ID,P .PS_C_SPEC2_ID) \"PS_C_SPEC2_ID\",\n" +
                "\tCOALESCE (D.PS_C_SPEC2_ECODE,P .PS_C_SPEC2_ECODE) \"PS_C_SPEC2_ECODE\",\n" +
                "\tCOALESCE (D.PS_C_SPEC2_ENAME,P .PS_C_SPEC2_ENAME) \"PS_C_SPEC2_ENAME\",\n" +
                "\tCOALESCE (P .SUM_QTY, 0) \"SUM_QTY\",\n" +
                "\tCOALESCE (D.QTY_STORAGE, 0) \"QTY_STORAGE\",\n" +
                "\t(COALESCE (P .SUM_QTY, 0) - COALESCE (D.QTY_STORAGE, 0)) \"QTY_DIFF\"");
        sql.append("\tFROM\n" +
                "\t(\n" +
                "\t\tSELECT\n" +
                "\t\t\tRPT_SG_B_PHY_STORAGE_DAILY.CP_C_PHY_WAREHOUSE_ID,\n" +
                "\t\t\tRPT_SG_B_PHY_STORAGE_DAILY.PS_C_SKU_ID,\n" +
                "\t\t\tRPT_SG_B_PHY_STORAGE_DAILY.PS_C_SKU_ECODE,\n" +
                "\t\t\tRPT_SG_B_PHY_STORAGE_DAILY.PS_C_PRO_ID,\n" +
                "\t\t\tRPT_SG_B_PHY_STORAGE_DAILY.PS_C_PRO_ECODE,\n" +
                "\t\t\tRPT_SG_B_PHY_STORAGE_DAILY.PS_C_PRO_ENAME,\n" +
                "\t\t\tRPT_SG_B_PHY_STORAGE_DAILY.PS_C_SPEC1_ID,\n" +
                "\t\t\tRPT_SG_B_PHY_STORAGE_DAILY.PS_C_SPEC1_ECODE,\n" +
                "\t\t\tRPT_SG_B_PHY_STORAGE_DAILY.PS_C_SPEC1_ENAME,\n" +
                "\t\t\tRPT_SG_B_PHY_STORAGE_DAILY.PS_C_SPEC2_ID,\n" +
                "\t\t\tRPT_SG_B_PHY_STORAGE_DAILY.PS_C_SPEC2_ECODE,\n" +
                "\t\t\tRPT_SG_B_PHY_STORAGE_DAILY.PS_C_SPEC2_ENAME,\n" +
                "\t\t\tRPT_SG_B_PHY_STORAGE_DAILY.QTY_STORAGE");
        if (flag) {
            sql.append("\tFROM\n" +
                    "\t\t\tSG_B_PHY_STORAGE RPT_SG_B_PHY_STORAGE_DAILY\n" +
                    "\t\tWHERE\n" +
                    "\t\t\tRPT_SG_B_PHY_STORAGE_DAILY.CP_C_PHY_WAREHOUSE_ID = " + storeId);
        } else {
            sql.append("\tFROM\n" +
                    "\t\t\tRPT_SG_B_PHY_STORAGE_DAILY\n" +
                    "\t\tWHERE\n" +
                    "\t\t\tRPT_SG_B_PHY_STORAGE_DAILY.CP_C_PHY_WAREHOUSE_ID = " + storeId +
                    "\tAND RPT_SG_B_PHY_STORAGE_DAILY.RPT_DATE= '" + inventoryDate + "'");
        }
        sql.append(") D\n" +
                "FULL JOIN (\n" +
                "\tSELECT\n" +
                "\t\tSC_B_INVENTORY.CP_C_PHY_WAREHOUSE_ID,\n" +
                "\t\tSC_B_INVENTORY_IMP_ITEM.PS_C_SKU_ID,\n" +
                "\t\tSC_B_INVENTORY_IMP_ITEM.PS_C_SKU_ECODE,\n" +
                "\t\tSC_B_INVENTORY_IMP_ITEM.PS_C_PRO_ID,\n" +
                "\t\tSC_B_INVENTORY_IMP_ITEM.PS_C_PRO_ECODE,\n" +
                "\t\tSC_B_INVENTORY_IMP_ITEM.PS_C_PRO_ENAME,\n" +
                "\t\tSC_B_INVENTORY_IMP_ITEM.PS_C_SPEC1_ID,\n" +
                "\t\tSC_B_INVENTORY_IMP_ITEM.PS_C_SPEC1_ECODE,\n" +
                "\t\tSC_B_INVENTORY_IMP_ITEM.PS_C_SPEC1_ENAME,\n" +
                "\t\tSC_B_INVENTORY_IMP_ITEM.PS_C_SPEC2_ID,\n" +
                "\t\tSC_B_INVENTORY_IMP_ITEM.PS_C_SPEC2_ECODE,\n" +
                "\t\tSC_B_INVENTORY_IMP_ITEM.PS_C_SPEC2_ENAME,\n" +
                "\t\tSUM (SC_B_INVENTORY_IMP_ITEM.QTY) AS SUM_QTY\n" +
                "\tFROM\n" +
                "\t\tSC_B_INVENTORY\n" +
                "\tINNER JOIN SC_B_INVENTORY_IMP_ITEM ON SC_B_INVENTORY. ID = SC_B_INVENTORY_IMP_ITEM.SC_B_INVENTORY_ID\n" +
                "\tWHERE\n" +
                "\t\tSC_B_INVENTORY.CP_C_PHY_WAREHOUSE_ID = " + storeId +
                "\tAND SC_B_INVENTORY.INVENTORY_DATE = '" + inventoryDate + "'\n" +
                "\tAND SC_B_INVENTORY.INVENTORY_TYPE = " + inventoryType +
                "\tAND SC_B_INVENTORY.ISACTIVE = '" + SgConstants.IS_ACTIVE_Y + "'\n" +
                "\tAND SC_B_INVENTORY.POL_STATUS = '" + SgInvConstants.PAND_UNPOL + "'\n" +
                "\t\t\t\tAND SC_B_INVENTORY_IMP_ITEM.IS_TEUS = '" + OcBasicConstants.IS_MATCH_SIZE_N + "'\n" +
                "\tGROUP BY\n" +
                "\t\tSC_B_INVENTORY.CP_C_PHY_WAREHOUSE_ID,\n" +
                "\t\tSC_B_INVENTORY_IMP_ITEM.PS_C_SKU_ID,\n" +
                "\t\tSC_B_INVENTORY_IMP_ITEM.PS_C_SKU_ECODE,\n" +
                "\t\tSC_B_INVENTORY_IMP_ITEM.PS_C_PRO_ID,\n" +
                "\t\tSC_B_INVENTORY_IMP_ITEM.PS_C_PRO_ECODE,\n" +
                "\t\tSC_B_INVENTORY_IMP_ITEM.PS_C_PRO_ENAME,\n" +
                "\t\tSC_B_INVENTORY_IMP_ITEM.PS_C_SPEC1_ID,\n" +
                "\t\tSC_B_INVENTORY_IMP_ITEM.PS_C_SPEC1_ECODE,\n" +
                "\t\tSC_B_INVENTORY_IMP_ITEM.PS_C_SPEC1_ENAME,\n" +
                "\t\tSC_B_INVENTORY_IMP_ITEM.PS_C_SPEC2_ID,\n" +
                "\t\tSC_B_INVENTORY_IMP_ITEM.PS_C_SPEC2_ECODE,\n" +
                "\t\tSC_B_INVENTORY_IMP_ITEM.PS_C_SPEC2_ENAME\n" +
                ") P ON D.PS_C_SKU_ID = P .PS_C_SKU_ID\n" +
                "AND D.CP_C_PHY_WAREHOUSE_ID = P .CP_C_PHY_WAREHOUSE_ID");
        return sql.toString();
    }

    /**
     * 全盘-查询盈亏明细
     */
    public String queryProfItemByQP(
            @Param("storeId") Long storeId,
            @Param("inventoryDate") Date inventoryDate,
            @Param("inventoryType") Integer inventoryType,
            @Param("startIndex") Integer startIndex,
            @Param("range") Integer range,
            @Param("orderByName") String orderByName,
            @Param("flag") Boolean flag,
            @Param("getCount") boolean getCount,
            @Param("isExport") Boolean isExport) {
        StringBuilder sql = null;
        if (getCount) {
            sql = new StringBuilder("select COUNT(1)");
        } else {
            sql = new StringBuilder("SELECT\n" +
                    "\tCOALESCE (D.PS_C_PRO_ID,P .PS_C_PRO_ID) \"PS_C_PRO_ID\",\n" +
                    "\tCOALESCE (D.PS_C_PRO_ECODE,P .PS_C_PRO_ECODE) \"PS_C_PRO_ECODE\",\n" +
                    "\tCOALESCE (D.PS_C_PRO_ENAME,P .PS_C_PRO_ENAME) \"PS_C_PRO_ENAME\",\n" +
                    "\tCOALESCE (D.SUM_QTY_STORAGE, 0) \"QTY_STORAGE\",\n" +
                    "\tCOALESCE (P .SUM_QTY, 0) \"SUM_QTY\",\n" +
                    "\t(COALESCE (P .SUM_QTY, 0) - COALESCE (D.SUM_QTY_STORAGE, 0)) \"QTY_DIFF\"");
        }
        sql.append("\tFROM\n" +
                "\t(\n" +
                "\t\tSELECT\n" +
                "\t\t\tRPT_SG_B_PHY_STORAGE_DAILY.CP_C_PHY_WAREHOUSE_ID,\n" +
                "\t\t\tRPT_SG_B_PHY_STORAGE_DAILY.PS_C_PRO_ID,\n" +
                "\t\t\tRPT_SG_B_PHY_STORAGE_DAILY.PS_C_PRO_ECODE,\n" +
                "\t\t\tRPT_SG_B_PHY_STORAGE_DAILY.PS_C_PRO_ENAME,\n" +
                "\t\t\tSUM (RPT_SG_B_PHY_STORAGE_DAILY.QTY_STORAGE) AS SUM_QTY_STORAGE");
        if (flag) {
            sql.append("\tFROM\n" +
                    "\t\t\tSG_B_PHY_STORAGE RPT_SG_B_PHY_STORAGE_DAILY\n" +
                    "\t\tWHERE\n" +
                    "\t\t\tRPT_SG_B_PHY_STORAGE_DAILY.CP_C_PHY_WAREHOUSE_ID = " + storeId);
        } else {
            sql.append("\tFROM\n" +
                    "\t\t\tRPT_SG_B_PHY_STORAGE_DAILY\n" +
                    "\t\tWHERE\n" +
                    "\t\t\tRPT_SG_B_PHY_STORAGE_DAILY.CP_C_PHY_WAREHOUSE_ID = " + storeId +
                    "\tAND RPT_SG_B_PHY_STORAGE_DAILY.RPT_DATE= '" + inventoryDate + "'");
        }
        sql.append("GROUP BY\n" +
                "\t\t\tRPT_SG_B_PHY_STORAGE_DAILY.CP_C_PHY_WAREHOUSE_ID,\n" +
                "\t\t\tRPT_SG_B_PHY_STORAGE_DAILY.PS_C_PRO_ID,\n" +
                "\t\t\tRPT_SG_B_PHY_STORAGE_DAILY.PS_C_PRO_ECODE,\n" +
                "\t\t\tRPT_SG_B_PHY_STORAGE_DAILY.PS_C_PRO_ENAME\n" +
                "\t) D\n" +
                "FULL JOIN (\n" +
                "\tSELECT\n" +
                "\t\tSC_B_INVENTORY.CP_C_PHY_WAREHOUSE_ID,\n" +
                "\t\tSC_B_INVENTORY_IMP_ITEM.PS_C_PRO_ID,\n" +
                "\t\tSC_B_INVENTORY_IMP_ITEM.PS_C_PRO_ECODE,\n" +
                "\t\tSC_B_INVENTORY_IMP_ITEM.PS_C_PRO_ENAME,\n" +
                "\t\tSUM (SC_B_INVENTORY_IMP_ITEM.QTY) AS SUM_QTY\n" +
                "\tFROM\n" +
                "\t\tSC_B_INVENTORY\n" +
                "\tINNER JOIN SC_B_INVENTORY_IMP_ITEM ON SC_B_INVENTORY. ID = SC_B_INVENTORY_IMP_ITEM.SC_B_INVENTORY_ID\n" +
                "\tWHERE\n" +
                "\t\tSC_B_INVENTORY.CP_C_PHY_WAREHOUSE_ID = " + storeId +
                "\tAND SC_B_INVENTORY.INVENTORY_DATE = '" + inventoryDate + "'\n" +
                "\tAND SC_B_INVENTORY.INVENTORY_TYPE = " + inventoryType +
                "\tAND SC_B_INVENTORY.ISACTIVE = '" + SgConstants.IS_ACTIVE_Y + "'\n" +
                "\tAND SC_B_INVENTORY.POL_STATUS = '" + SgInvConstants.PAND_UNPOL + "'\n" +
                "\t\t\t\tAND SC_B_INVENTORY_IMP_ITEM.IS_TEUS = '" + OcBasicConstants.IS_MATCH_SIZE_N + "'\n" +
                "\tGROUP BY\n" +
                "\t\tSC_B_INVENTORY.CP_C_PHY_WAREHOUSE_ID,\n" +
                "\t\tSC_B_INVENTORY_IMP_ITEM.PS_C_PRO_ID,\n" +
                "\t\tSC_B_INVENTORY_IMP_ITEM.PS_C_PRO_ECODE,\n" +
                "\t\tSC_B_INVENTORY_IMP_ITEM.PS_C_PRO_ENAME\n" +
                ") P ON D.PS_C_PRO_ID = P .PS_C_PRO_ID\n" +
                "AND D.CP_C_PHY_WAREHOUSE_ID = P .CP_C_PHY_WAREHOUSE_ID");
        if (!getCount) {
            if (isExport) {
                //TODO 一期暂不支持导出
            } else {
                sql.append("\tORDER BY ABS(COALESCE (P .SUM_QTY, 0) - COALESCE (D.SUM_QTY_STORAGE, 0)) DESC");
                sql.append("\tLIMIT " + range + " OFFSET " + startIndex);
            }
        }
        return sql.toString();
    }

    /**
     * 抽盘 -生成盈亏
     */
    public String queryProfItemByCPC(@Param("storeId") Long storeId,
                                     @Param("inventoryDate") Date inventoryDate,
                                     @Param("inventoryType") Integer inventoryType,
                                     @Param("flag") Boolean flag) {
        StringBuilder sql = new StringBuilder("SELECT\n" +
                "\tCOALESCE (D.PS_C_SKU_ID,P .PS_C_SKU_ID) \"PS_C_SKU_ID\",\n" +
                "\tCOALESCE (D.PS_C_SKU_ECODE,P .PS_C_SKU_ECODE) \"PS_C_SKU_ECODE\",\n" +
                "\tCOALESCE (D.PS_C_PRO_ID,P .PS_C_PRO_ID) \"PS_C_PRO_ID\",\n" +
                "\tCOALESCE (D.PS_C_PRO_ECODE,P .PS_C_PRO_ECODE) \"PS_C_PRO_ECODE\",\n" +
                "\tP .PS_C_PRO_ENAME \"PS_C_PRO_ENAME\",\n" +
                "\tCOALESCE (D.PS_C_SPEC1_ID,P .PS_C_SPEC1_ID) \"PS_C_SPEC1_ID\",\n" +
                "\tCOALESCE (D.PS_C_SPEC1_ECODE,P .PS_C_SPEC1_ECODE) \"PS_C_SPEC1_ECODE\",\n" +
                "\tCOALESCE (D.PS_C_SPEC1_ENAME,P .PS_C_SPEC1_ENAME) \"PS_C_SPEC1_ENAME\",\n" +
                "\tCOALESCE (D.PS_C_SPEC2_ID,P .PS_C_SPEC2_ID) \"PS_C_SPEC2_ID\",\n" +
                "\tCOALESCE (D.PS_C_SPEC2_ECODE,P .PS_C_SPEC2_ECODE) \"PS_C_SPEC2_ECODE\",\n" +
                "\tCOALESCE (D.PS_C_SPEC2_ENAME,P .PS_C_SPEC2_ENAME) \"PS_C_SPEC2_ENAME\",\n" +
                "\tCOALESCE(P .SUM_QTY,0) \"SUM_QTY\",\n" +
                "\tCOALESCE (D.QTY_STORAGE, 0) \"QTY_STORAGE\",\n" +
                "\t(COALESCE(P .SUM_QTY,0)-COALESCE (D.QTY_STORAGE, 0)) \"QTY_DIFF\"");
        sql.append("\tFROM\n" +
                "\t(\n" +
                "\t\tSELECT\n" +
                "\t\t\tRPT_SG_B_PHY_STORAGE_DAILY.CP_C_PHY_WAREHOUSE_ID,\n" +
                "\t\t\tRPT_SG_B_PHY_STORAGE_DAILY.PS_C_SKU_ID,\n" +
                "\t\t\tRPT_SG_B_PHY_STORAGE_DAILY.PS_C_SKU_ECODE,\n" +
                "\t\t\tRPT_SG_B_PHY_STORAGE_DAILY.PS_C_PRO_ID,\n" +
                "\t\t\tRPT_SG_B_PHY_STORAGE_DAILY.PS_C_PRO_ECODE,\n" +
                "\t\t\tRPT_SG_B_PHY_STORAGE_DAILY.PS_C_SPEC1_ID,\n" +
                "\t\t\tRPT_SG_B_PHY_STORAGE_DAILY.PS_C_SPEC1_ECODE,\n" +
                "\t\t\tRPT_SG_B_PHY_STORAGE_DAILY.PS_C_SPEC1_ENAME,\n" +
                "\t\t\tRPT_SG_B_PHY_STORAGE_DAILY.PS_C_SPEC2_ID,\n" +
                "\t\t\tRPT_SG_B_PHY_STORAGE_DAILY.PS_C_SPEC2_ECODE,\n" +
                "\t\t\tRPT_SG_B_PHY_STORAGE_DAILY.PS_C_SPEC2_ENAME,\n" +
                "\t\t\tRPT_SG_B_PHY_STORAGE_DAILY.QTY_STORAGE");
        if (flag) {//盘点日期=系统日期
            sql.append("\tFROM\n" +
                    "\t\t\tSG_B_PHY_STORAGE RPT_SG_B_PHY_STORAGE_DAILY\n" +
                    "\t\tWHERE\n" +
                    "\t\t\tEXISTS (\n" +
                    "\t\t\t\tSELECT\n" +
                    "\t\t\t\t\t1\n" +
                    "\t\t\t\tFROM\n" +
                    "\t\t\t\t\tSC_B_INVENTORY\n" +
                    "\t\t\t\tINNER JOIN SC_B_INVENTORY_IMP_ITEM ON SC_B_INVENTORY. ID = SC_B_INVENTORY_IMP_ITEM.SC_B_INVENTORY_ID\n" +
                    "\t\t\t\tWHERE\n" +
                    "\t\t\t\t\tRPT_SG_B_PHY_STORAGE_DAILY.PS_C_PRO_ID = SC_B_INVENTORY_IMP_ITEM.PS_C_PRO_ID\n" +
                    "\t\t\t\tAND RPT_SG_B_PHY_STORAGE_DAILY.CP_C_PHY_WAREHOUSE_ID = SC_B_INVENTORY.CP_C_PHY_WAREHOUSE_ID\n" +
                    "\t\t\t\tAND SC_B_INVENTORY.CP_C_PHY_WAREHOUSE_ID = " + storeId + "\n" +
                    "\t\t\t\tAND SC_B_INVENTORY.INVENTORY_DATE = '" + inventoryDate + "'\n" +
                    "\t\t\t\tAND SC_B_INVENTORY.INVENTORY_TYPE = " + inventoryType +
                    "\t\t\t\tAND SC_B_INVENTORY.ISACTIVE = '" + SgConstants.IS_ACTIVE_Y + "'\n" +
                    "\t\t\t\tAND SC_B_INVENTORY.POL_STATUS = '" + SgInvConstants.PAND_UNPOL + "'\n" +
                    "\t\t\t\tAND SC_B_INVENTORY_IMP_ITEM.IS_TEUS = '" + OcBasicConstants.IS_MATCH_SIZE_N + "'\n" +
                    "\t\t\t)\n" +
                    "\t\tAND RPT_SG_B_PHY_STORAGE_DAILY.CP_C_PHY_WAREHOUSE_ID = " + storeId);
        } else {
            sql.append("\tFROM\n" +
                    "\t\t\tRPT_SG_B_PHY_STORAGE_DAILY\n" +
                    "\t\tWHERE\n" +
                    "\t\t\tEXISTS (\n" +
                    "\t\t\t\tSELECT\n" +
                    "\t\t\t\t\t1\n" +
                    "\t\t\t\tFROM\n" +
                    "\t\t\t\t\tSC_B_INVENTORY\n" +
                    "\t\t\t\tINNER JOIN SC_B_INVENTORY_IMP_ITEM ON SC_B_INVENTORY. ID = SC_B_INVENTORY_IMP_ITEM.SC_B_INVENTORY_ID\n" +
                    "\t\t\t\tWHERE\n" +
                    "\t\t\t\t\tRPT_SG_B_PHY_STORAGE_DAILY.PS_C_PRO_ID = SC_B_INVENTORY_IMP_ITEM.PS_C_PRO_ID\n" +
                    "\t\t\t\tAND RPT_SG_B_PHY_STORAGE_DAILY.CP_C_PHY_WAREHOUSE_ID = SC_B_INVENTORY.CP_C_PHY_WAREHOUSE_ID\n" +
                    "\t\t\t\tAND SC_B_INVENTORY.CP_C_PHY_WAREHOUSE_ID = " + storeId + "\n" +
                    "\t\t\t\tAND SC_B_INVENTORY.INVENTORY_DATE = '" + inventoryDate + "'\n" +
                    "\t\t\t\tAND SC_B_INVENTORY.INVENTORY_TYPE = " + inventoryType +
                    "\t\t\t\tAND SC_B_INVENTORY.ISACTIVE = '" + SgConstants.IS_ACTIVE_Y + "'\n" +
                    "\t\t\t\tAND SC_B_INVENTORY.POL_STATUS = '" + SgInvConstants.PAND_UNPOL + "'\n" +
                    "\t\t\t\tAND SC_B_INVENTORY_IMP_ITEM.IS_TEUS = '" + OcBasicConstants.IS_MATCH_SIZE_N + "'\n" +
                    "\t\t\t)\n" +
                    "\t\tAND RPT_SG_B_PHY_STORAGE_DAILY.CP_C_PHY_WAREHOUSE_ID = " + storeId +
                    "\tAND RPT_SG_B_PHY_STORAGE_DAILY.RPT_DATE= '" + inventoryDate + "'");
        }
        sql.append("\t) D\n" +
                "FULL JOIN (\n" +
                "\tSELECT\n" +
                "\t\tSC_B_INVENTORY.CP_C_PHY_WAREHOUSE_ID,\n" +
                "\t\tSC_B_INVENTORY_IMP_ITEM.PS_C_SKU_ID,\n" +
                "\t\tSC_B_INVENTORY_IMP_ITEM.PS_C_SKU_ECODE,\n" +
                "\t\tSC_B_INVENTORY_IMP_ITEM.PS_C_PRO_ID,\n" +
                "\t\tSC_B_INVENTORY_IMP_ITEM.PS_C_PRO_ECODE,\n" +
                "\t\tSC_B_INVENTORY_IMP_ITEM.PS_C_PRO_ENAME,\n" +
                "\t\tSC_B_INVENTORY_IMP_ITEM.PS_C_SPEC1_ID,\n" +
                "\t\tSC_B_INVENTORY_IMP_ITEM.PS_C_SPEC1_ECODE,\n" +
                "\t\tSC_B_INVENTORY_IMP_ITEM.PS_C_SPEC1_ENAME,\n" +
                "\t\tSC_B_INVENTORY_IMP_ITEM.PS_C_SPEC2_ID,\n" +
                "\t\tSC_B_INVENTORY_IMP_ITEM.PS_C_SPEC2_ECODE,\n" +
                "\t\tSC_B_INVENTORY_IMP_ITEM.PS_C_SPEC2_ENAME,\n" +
                "\t\tSUM (SC_B_INVENTORY_IMP_ITEM.QTY) AS SUM_QTY\n" +
                "\tFROM\n" +
                "\t\tSC_B_INVENTORY\n" +
                "\tINNER JOIN SC_B_INVENTORY_IMP_ITEM ON SC_B_INVENTORY. ID = SC_B_INVENTORY_IMP_ITEM.SC_B_INVENTORY_ID\n" +
                "\tWHERE\n" +
                "\t\tSC_B_INVENTORY.CP_C_PHY_WAREHOUSE_ID =" + storeId +
                "\tAND SC_B_INVENTORY.INVENTORY_DATE = '" + inventoryDate + "'\n" +
                "\tAND SC_B_INVENTORY.INVENTORY_TYPE = " + inventoryType +
                "\tAND SC_B_INVENTORY.ISACTIVE = '" + SgConstants.IS_ACTIVE_Y + "'\n" +
                "\tAND SC_B_INVENTORY.POL_STATUS = '" + SgInvConstants.PAND_UNPOL + "'\n" +
                "\t\t\t\tAND SC_B_INVENTORY_IMP_ITEM.IS_TEUS = '" + OcBasicConstants.IS_MATCH_SIZE_N + "'\n" +
                "\tGROUP BY\n" +
                "\t\tSC_B_INVENTORY.CP_C_PHY_WAREHOUSE_ID,\n" +
                "\t\tSC_B_INVENTORY_IMP_ITEM.PS_C_SKU_ID,\n" +
                "\t\tSC_B_INVENTORY_IMP_ITEM.PS_C_SKU_ECODE,\n" +
                "\t\tSC_B_INVENTORY_IMP_ITEM.PS_C_PRO_ID,\n" +
                "\t\tSC_B_INVENTORY_IMP_ITEM.PS_C_PRO_ECODE,\n" +
                "\t\tSC_B_INVENTORY_IMP_ITEM.PS_C_PRO_ENAME,\n" +
                "\t\tSC_B_INVENTORY_IMP_ITEM.PS_C_SPEC1_ID,\n" +
                "\t\tSC_B_INVENTORY_IMP_ITEM.PS_C_SPEC1_ECODE,\n" +
                "\t\tSC_B_INVENTORY_IMP_ITEM.PS_C_SPEC1_ENAME,\n" +
                "\t\tSC_B_INVENTORY_IMP_ITEM.PS_C_SPEC2_ID,\n" +
                "\t\tSC_B_INVENTORY_IMP_ITEM.PS_C_SPEC2_ECODE,\n" +
                "\t\tSC_B_INVENTORY_IMP_ITEM.PS_C_SPEC2_ENAME\n" +
                ") P ON D.PS_C_SKU_ID = P .PS_C_SKU_ID\n" +
                "AND D.CP_C_PHY_WAREHOUSE_ID = P .CP_C_PHY_WAREHOUSE_ID");
        return sql.toString();
    }

    /**
     * 抽盘-查询盈亏明细
     */
    public String queryProfItemByCP(@Param("storeId") Long storeId,
                                    @Param("inventoryDate") Date inventoryDate,
                                    @Param("inventoryType") Integer inventoryType,
                                    @Param("startIndex") Integer startIndex,
                                    @Param("range") Integer range,
                                    @Param("orderByName") String orderByName,
                                    @Param("flag") Boolean flag,
                                    @Param("getCount") boolean getCount,
                                    @Param("isExport") Boolean isExport) {
        StringBuilder sql = null;
        if (getCount) {
            sql = new StringBuilder("select COUNT(1)");
        } else {
            sql = new StringBuilder("SELECT\n" +
                    "\tP .CP_C_PHY_WAREHOUSE_ID \"CP_C_PHY_WAREHOUSE_ID\",\n" +
                    "\tP .PS_C_PRO_ID \"PS_C_PRO_ID\",\n" +
                    "\tP .PS_C_PRO_ECODE \"PS_C_PRO_ECODE\",\n" +
                    "\tP .PS_C_PRO_ENAME \"PS_C_PRO_ENAME\",\n" +
                    "\tP .SUM_QTY \"SUM_QTY\",\n" +
                    "\tCOALESCE (D.QTY_STORAGE, 0) \"QTY_STORAGE\",\n" +
                    "\t(P .SUM_QTY - COALESCE (D.QTY_STORAGE, 0)) \"QTY_DIFF\"");
        }
        sql.append(" FROM\n" +
                "\t(\n" +
                "\t\tSELECT\n" +
                "\t\t\tRPT_SG_B_PHY_STORAGE_DAILY.CP_C_PHY_WAREHOUSE_ID,\n" +
                "\t\t\tRPT_SG_B_PHY_STORAGE_DAILY.PS_C_PRO_ID,\n" +
                "\t\t\tSUM (RPT_SG_B_PHY_STORAGE_DAILY.QTY_STORAGE) AS QTY_STORAGE\n");
        if (flag) {//盘点日期=系统日期
            sql.append(" \tFROM\n" +
                    "\t\t\tSG_B_PHY_STORAGE RPT_SG_B_PHY_STORAGE_DAILY\n" +
                    "\t\tWHERE\n" +
                    "\t\t\tEXISTS (\n" +
                    "\t\t\t\tSELECT\n" +
                    "\t\t\t\t\t1\n" +
                    "\t\t\t\tFROM\n" +
                    "\t\t\t\t\tSC_B_INVENTORY\n" +
                    "\t\t\t\tINNER JOIN SC_B_INVENTORY_IMP_ITEM ON SC_B_INVENTORY. ID = SC_B_INVENTORY_IMP_ITEM.SC_B_INVENTORY_ID\n" +
                    "\t\t\t\tWHERE\n" +
                    "\t\t\t\t\tRPT_SG_B_PHY_STORAGE_DAILY.PS_C_PRO_ID = SC_B_INVENTORY_IMP_ITEM.PS_C_PRO_ID\n" +
                    "\t\t\t\tAND RPT_SG_B_PHY_STORAGE_DAILY.CP_C_PHY_WAREHOUSE_ID = SC_B_INVENTORY.CP_C_PHY_WAREHOUSE_ID\n" +
                    "\t\t\t\tAND SC_B_INVENTORY.CP_C_PHY_WAREHOUSE_ID = " + storeId + "\n" +
                    "\t\t\t\tAND SC_B_INVENTORY.INVENTORY_DATE = '" + inventoryDate + "'\n" +
                    "\t\t\t\tAND SC_B_INVENTORY.INVENTORY_TYPE = " + inventoryType +
                    "\t\t\t\tAND SC_B_INVENTORY.ISACTIVE = '" + SgConstants.IS_ACTIVE_Y + "'\n" +
                    "\t\t\t\tAND SC_B_INVENTORY.POL_STATUS = '" + SgInvConstants.PAND_UNPOL + "'\n" +
                    "\t\t\t\tAND SC_B_INVENTORY_IMP_ITEM.IS_TEUS = '" + OcBasicConstants.IS_MATCH_SIZE_N + "'\n" +
                    "\t\t\t)\n" +
                    "\t\tAND RPT_SG_B_PHY_STORAGE_DAILY.CP_C_PHY_WAREHOUSE_ID = " + storeId);
        } else {
            sql.append(" \tFROM\n" +
                    "\t\t\tRPT_SG_B_PHY_STORAGE_DAILY\n" +
                    "\t\tWHERE\n" +
                    "\t\t\tEXISTS (\n" +
                    "\t\t\t\tSELECT\n" +
                    "\t\t\t\t\t1\n" +
                    "\t\t\t\tFROM\n" +
                    "\t\t\t\t\tSC_B_INVENTORY\n" +
                    "\t\t\t\tINNER JOIN SC_B_INVENTORY_IMP_ITEM ON SC_B_INVENTORY. ID = SC_B_INVENTORY_IMP_ITEM.SC_B_INVENTORY_ID\n" +
                    "\t\t\t\tWHERE\n" +
                    "\t\t\t\t\tRPT_SG_B_PHY_STORAGE_DAILY.PS_C_PRO_ID = SC_B_INVENTORY_IMP_ITEM.PS_C_PRO_ID\n" +
                    "\t\t\t\tAND RPT_SG_B_PHY_STORAGE_DAILY.CP_C_PHY_WAREHOUSE_ID = SC_B_INVENTORY.CP_C_PHY_WAREHOUSE_ID\n" +
                    "\t\t\t\tAND SC_B_INVENTORY.CP_C_PHY_WAREHOUSE_ID = " + storeId + "\n" +
                    "\t\t\t\tAND SC_B_INVENTORY.INVENTORY_DATE = '" + inventoryDate + "'\n" +
                    "\t\t\t\tAND SC_B_INVENTORY.INVENTORY_TYPE = " + inventoryType +
                    "\t\t\t\tAND SC_B_INVENTORY.ISACTIVE = '" + SgConstants.IS_ACTIVE_Y + "'\n" +
                    "\t\t\t\tAND SC_B_INVENTORY.POL_STATUS = '" + SgInvConstants.PAND_UNPOL + "'\n" +
                    "\t\t\t\tAND SC_B_INVENTORY_IMP_ITEM.IS_TEUS = '" + OcBasicConstants.IS_MATCH_SIZE_N + "'\n" +
                    "\t\t\t)\n" +
                    "\t\tAND RPT_SG_B_PHY_STORAGE_DAILY.CP_C_PHY_WAREHOUSE_ID = " + storeId +
                    "  AND RPT_SG_B_PHY_STORAGE_DAILY.RPT_DATE= '" + inventoryDate + "'");
        }
        sql.append(" GROUP BY\n" +
                "\t\t\tRPT_SG_B_PHY_STORAGE_DAILY.CP_C_PHY_WAREHOUSE_ID,\n" +
                "\t\t\tRPT_SG_B_PHY_STORAGE_DAILY.PS_C_PRO_ID\n" +
                "\t) D\n" +
                "FULL JOIN (\n" +
                "\tSELECT\n" +
                "\t\tSC_B_INVENTORY.CP_C_PHY_WAREHOUSE_ID,\n" +
                "\t\tSC_B_INVENTORY_IMP_ITEM.PS_C_PRO_ID,\n" +
                "\t\tSC_B_INVENTORY_IMP_ITEM.PS_C_PRO_ECODE,\n" +
                "\t\tSC_B_INVENTORY_IMP_ITEM.PS_C_PRO_ENAME,\n" +
                "\t\tSUM (\n" +
                "\t\t\tSC_B_INVENTORY_IMP_ITEM.QTY\n" +
                "\t\t) AS SUM_QTY\n" +
                "\tFROM\n" +
                "\t\tSC_B_INVENTORY\n" +
                "\tINNER JOIN SC_B_INVENTORY_IMP_ITEM ON SC_B_INVENTORY. ID = SC_B_INVENTORY_IMP_ITEM.SC_B_INVENTORY_ID\n" +
                "\tWHERE\n" +
                "\t\tSC_B_INVENTORY.CP_C_PHY_WAREHOUSE_ID = " + storeId + "\n" +
                "\tAND SC_B_INVENTORY.INVENTORY_DATE = '" + inventoryDate + "'\n" +
                "\tAND SC_B_INVENTORY.INVENTORY_TYPE = " + inventoryType +
                "\tAND SC_B_INVENTORY.ISACTIVE = '" + SgConstants.IS_ACTIVE_Y + "'\n" +
                "\tAND SC_B_INVENTORY.POL_STATUS = '" + SgInvConstants.PAND_UNPOL + "'\n" +
                "\t\t\t\tAND SC_B_INVENTORY_IMP_ITEM.IS_TEUS = '" + OcBasicConstants.IS_MATCH_SIZE_N + "'\n" +
                "\tGROUP BY\n" +
                "\t\tSC_B_INVENTORY.CP_C_PHY_WAREHOUSE_ID,\n" +
                "\t\tSC_B_INVENTORY_IMP_ITEM.PS_C_PRO_ID,\n" +
                "\t\tSC_B_INVENTORY_IMP_ITEM.PS_C_PRO_ECODE,\n" +
                "\t\tSC_B_INVENTORY_IMP_ITEM.PS_C_PRO_ENAME\n" +
                ") P ON D.PS_C_PRO_ID = P .PS_C_PRO_ID\n" +
                "AND D.CP_C_PHY_WAREHOUSE_ID = P .CP_C_PHY_WAREHOUSE_ID\n" +
                "WHERE\n" +
                "\tP .SUM_QTY IS NOT NULL ");
        if (!getCount) {
            if (isExport) {
                //TODO 一期暂不支持导出
            } else {
                sql.append("ORDER BY ABS(P .SUM_QTY - COALESCE (D.QTY_STORAGE, 0)) DESC");
                sql.append("  limit " + range + " offset " + startIndex);
            }
        }
        return sql.toString();
    }
}
