package com.jackrain.nea.sg.out.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.github.pagehelper.PageHelper;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.oc.purchase.api.OcPurchaseUpdateCmd;
import com.jackrain.nea.oc.purchase.api.OcRefundPurchaseUpdateCmd;
import com.jackrain.nea.oc.purchase.model.request.OcPurchaseUpdatePickRequest;
import com.jackrain.nea.oc.purchase.model.result.OcPurchaseUpdatePickResult;
import com.jackrain.nea.oc.sale.api.OcRefundSaleUpdateCmd;
import com.jackrain.nea.oc.sale.api.OcSaleUpdateCmd;
import com.jackrain.nea.oc.sale.api.OcSendOutQtyUpdateCmd;
import com.jackrain.nea.oc.sale.model.request.OcSaleUpdatePickRequest;
import com.jackrain.nea.oc.sale.model.result.OcSaleUpdatePickResult;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.out.common.SgOutConstantsIF;
import com.jackrain.nea.sg.out.mapper.SgBPhyOutNoticesImpItemMapper;
import com.jackrain.nea.sg.out.mapper.SgBPhyOutNoticesMapper;
import com.jackrain.nea.sg.out.model.request.SgPhyOutNoticesImpItemSaveRequest;
import com.jackrain.nea.sg.out.model.request.SgPhyOutNoticesSaveRequest;
import com.jackrain.nea.sg.out.model.request.SgPhyOutPickOrderSaveRequest;
import com.jackrain.nea.sg.out.model.request.SgPhyOutPickUpGoodSaveRequest;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutNotices;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutNoticesImpItem;
import com.jackrain.nea.sg.transfer.api.SgTransferUpdatePickCmd;
import com.jackrain.nea.sg.transfer.model.request.OcTransferUpdatePickRequest;
import com.jackrain.nea.sg.transfer.model.result.OcTransferUpdatePickResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author zhu lin yu
 * @since 2019-11-13
 * create at : 2019-11-13 14:06
 */
@Slf4j
@Component
public class SgPhyOutPickOrderGenerateService {

    /**
     * 出库通知单生成拣货单
     *
     * @param request 出库通知单id集合
     */
    @Transactional(rollbackFor = Exception.class)
    public ValueHolderV14<Long> generatePhyOutPickUpGood(SgPhyOutPickUpGoodSaveRequest request) {

        ValueHolderV14<Long> valueHolderV14;

        if (log.isDebugEnabled()) {
            log.debug("Start SgPhyOutPickUpGoodGenerateService.generatePhyOutPickUpGood. ReceiveParams:params:{};"
                    + JSONObject.toJSONString(request));
        }

        //参数校验
        valueHolderV14 = checkParam(request);
        if (!valueHolderV14.isOK()) {
            return valueHolderV14;
        }

        User loginUser = request.getLoginUser();

        //根据出库通知单id查询所有的出库通知单
        List<Long> outNoticeIdList = request.getOutNoticeIdList();
        SgBPhyOutNoticesMapper sgBPhyOutNoticesMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesMapper.class);
        List<SgBPhyOutNotices> sgBPhyOutNoticeList = sgBPhyOutNoticesMapper.selectList(new QueryWrapper<SgBPhyOutNotices>()
                .lambda().in(SgBPhyOutNotices::getId, outNoticeIdList));
        log.info(this.getClass().getName() + " 生成拣货单的出库通知单主表参数,{}", JSONObject.toJSONString(sgBPhyOutNoticeList));
        if (CollectionUtils.isEmpty(sgBPhyOutNoticeList) || outNoticeIdList.size() != sgBPhyOutNoticeList.size()) {
            valueHolderV14.setMessage("出库通知单不存在或数量不符合!");
            return valueHolderV14;
        }

        /**
         * 如果未选择出库通知单明细，则提示：“请选择需要拣货的数据！”；
         * 如果选择的出库通知单中存在单据状态不为待出库或部分出库，则提示：“存在全部出库的记录，不允许拣货！”；
         * 如果选择的出库通知单中存在是否传WMS为是，则提示：“存在传WMS的记录，不允许拣货！”；
         * 如果所选择的出库通知单的拣货状态不为未拣货，则提示：“存在拣货中或拣货完成的记录，不允许拣货！”；
         * 如果选择多张出库通知单，出库通知单实体仓、收货方名称、来源单据类型（非零售出库类型）不一致，则提示：“存在实体仓、收货方名称、来源单据类型不一致的记录，不允许拣货！”
         */
        List<Long> cpCPhyWarehouseList = new ArrayList<>();
        List<String> receiverNameList = new ArrayList<>();
        List<Integer> sourceBillTypeList = new ArrayList<>();

        for (SgBPhyOutNotices sgBPhyOutNotice : sgBPhyOutNoticeList) {

            if (SgOutConstantsIF.OUT_NOTICES_STATUS_WAIT != sgBPhyOutNotice.getBillStatus() && SgOutConstantsIF.OUT_NOTICES_STATUS_PART != sgBPhyOutNotice.getBillStatus()) {
                AssertUtils.logAndThrow(" 存在全部出库的记录，不允许拣货!出库通知单为：" + sgBPhyOutNotice.getBillNo());

            } else if (SgOutConstantsIF.IS_PASS_WMS_Y == sgBPhyOutNotice.getIsPassWms()) {
                AssertUtils.logAndThrow(" 存在传WMS的记录，不允许拣货!出库通知单为：" + sgBPhyOutNotice.getBillNo());

            } else if (SgOutConstantsIF.PICK_STATUS_SUCCESS == sgBPhyOutNotice.getPickStatus()
                    || SgOutConstantsIF.PICK_STATUS_ING == sgBPhyOutNotice.getPickStatus()) {
                AssertUtils.logAndThrow(" 存在拣货中或拣货完成的记录，不允许拣货!出库通知单为：" + sgBPhyOutNotice.getBillNo());

            }

            //判断多张出库通知单，出库通知单实体仓、收货方名称、来源单据类型（非零售出库类型）是否一致
            if (!cpCPhyWarehouseList.contains(sgBPhyOutNotice.getCpCPhyWarehouseId())) {
                cpCPhyWarehouseList.add(sgBPhyOutNotice.getCpCPhyWarehouseId());
            }
            if (!receiverNameList.contains(sgBPhyOutNotice.getCpCCsEcode())) {
                receiverNameList.add(sgBPhyOutNotice.getCpCCsEcode());
            }
            if (SgConstantsIF.BILL_TYPE_RETAIL != sgBPhyOutNotice.getSourceBillType() && !sourceBillTypeList.contains(sgBPhyOutNotice.getSourceBillType())) {
                sourceBillTypeList.add(sgBPhyOutNotice.getSourceBillType());
            }
        }

        if (cpCPhyWarehouseList.size() > 1 || receiverNameList.size() > 1 || sourceBillTypeList.size() > 1) {
            AssertUtils.logAndThrow(" 存在拣货中或拣货完成的记录，不允许拣货!");
        }


        SgBPhyOutNoticesImpItemMapper outNoticesImpItemMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesImpItemMapper.class);
        Integer totalQtty = outNoticesImpItemMapper.selectCount(new QueryWrapper<SgBPhyOutNoticesImpItem>()
                .lambda().in(SgBPhyOutNoticesImpItem::getSgBPhyOutNoticesId, outNoticeIdList)
                .gt(SgBPhyOutNoticesImpItem::getQtyDiff, BigDecimal.ZERO));

        //分页处理
        List<SgBPhyOutNoticesImpItem> allOutNoticesImpItem = new ArrayList<>();
        int pageSize = SgConstants.SG_NORMAL_MAX_QUERY_PAGE_SIZE;
        int listSize = totalQtty;
        int page = listSize / pageSize;

        if (listSize % pageSize != 0) {
            page++;
        }

        //分页批量查询
        for (int i = 0; i < page; i++) {

            PageHelper.startPage(i + 1, pageSize);
            List<SgBPhyOutNoticesImpItem> outNoticesImpItemList = outNoticesImpItemMapper.selectList(new QueryWrapper<SgBPhyOutNoticesImpItem>()
                    .lambda().in(SgBPhyOutNoticesImpItem::getSgBPhyOutNoticesId, outNoticeIdList)
                    .gt(SgBPhyOutNoticesImpItem::getQtyDiff, BigDecimal.ZERO));

            if (CollectionUtils.isNotEmpty(outNoticesImpItemList)) {
                allOutNoticesImpItem.addAll(outNoticesImpItemList);
                log.info(this.getClass().getName() + " 第{}批,生成拣货单的拣货明细参数,{}", i + 1, JSONObject.toJSONString(outNoticesImpItemList));
            }
        }
        /**
         * 生成拣货单
         */
        SgPhyOutPickOrderSaveService saveService = ApplicationContextHandle.getBean(SgPhyOutPickOrderSaveService.class);

        //生成出库拣货单主表信息
        List<SgPhyOutNoticesSaveRequest> sgPhyOutNoticesSaveRequests = new ArrayList<>();
        for (SgBPhyOutNotices sgBPhyOutNotices : sgBPhyOutNoticeList) {
            SgPhyOutNoticesSaveRequest saveRequest = new SgPhyOutNoticesSaveRequest();
            BeanUtils.copyProperties(sgBPhyOutNotices, saveRequest);
            sgPhyOutNoticesSaveRequests.add(saveRequest);
        }

        SgPhyOutPickOrderSaveRequest pickorderRequest = new SgPhyOutPickOrderSaveRequest();
        pickorderRequest.setOutNoticesList(sgPhyOutNoticesSaveRequests);
        pickorderRequest.setLoginUser(loginUser);
        ValueHolderV14<Long> holderV14 = saveService.saveSgbBoutPickorder(pickorderRequest);
        AssertUtils.isTrue(holderV14.isOK(), "新增拣货单主表信息失败");
        Long sgBOutPickorderId = holderV14.getData();

        //生成拣货明细信息

        SgPhyOutPickOrderSaveRequest pickorderItemRequest = new SgPhyOutPickOrderSaveRequest();
        pickorderItemRequest.setSgPhyBOutPickorderId(sgBOutPickorderId);
        List<SgPhyOutNoticesImpItemSaveRequest> requests = new ArrayList<>();
        for (SgBPhyOutNoticesImpItem sgBPhyOutNoticesImpItem : allOutNoticesImpItem) {
            SgPhyOutNoticesImpItemSaveRequest impItemSaveRequest = new SgPhyOutNoticesImpItemSaveRequest();
            BeanUtils.copyProperties(sgBPhyOutNoticesImpItem, impItemSaveRequest);
            requests.add(impItemSaveRequest);
        }
        pickorderItemRequest.setOutNoticesImpItemList(requests);
        pickorderItemRequest.setLoginUser(loginUser);
        saveService.saveSgbBoutPickorderItem(pickorderItemRequest);

        //生成来源出库通知单信息

        SgPhyOutPickOrderSaveRequest pickorderNoticeItemRequest = new SgPhyOutPickOrderSaveRequest();
        pickorderNoticeItemRequest.setSgPhyBOutPickorderId(sgBOutPickorderId);
        pickorderNoticeItemRequest.setOutNoticesList(sgPhyOutNoticesSaveRequests);
        pickorderNoticeItemRequest.setLoginUser(loginUser);
        saveService.saveSgbBoutPickorderNoticeItem(pickorderNoticeItemRequest);

        /**
         * 更新对应的出库通知单的拣货状态为拣货中
         */
        SgBPhyOutNotices updatePhyOutNotice = new SgBPhyOutNotices();
        updatePhyOutNotice.setPickStatus(SgOutConstantsIF.PICK_STATUS_ING);
        sgBPhyOutNoticesMapper.update(updatePhyOutNotice, new UpdateWrapper<SgBPhyOutNotices>()
                .lambda().in(SgBPhyOutNotices::getId, outNoticeIdList));


        /**
         * 根据对应的出库通知单的来源单据编号+来源单据类型（调拨单、采购退货单、销售单、销售退货单）更新来源单据拣货状态为拣货中
         */
        Map<Integer, List<SgBPhyOutNotices>> collect = sgBPhyOutNoticeList.stream().collect(Collectors.groupingBy(SgBPhyOutNotices::getSourceBillType));
        try {
            updateSourceBillPickOrderStatus(collect, SgOutConstantsIF.PICK_STATUS_ING,loginUser);
        } catch (Exception e) {
            log.error(this.getClass().getName() + " 入库拣货单更新上游单据拣货状态失败,拣货单id为{},错误信息:{}", sgBOutPickorderId, e.getMessage());
        }
        valueHolderV14.setCode(ResultCode.SUCCESS);
        valueHolderV14.setMessage("拣货单生成成功");
        valueHolderV14.setData(sgBOutPickorderId);

        if (log.isDebugEnabled()) {
            log.debug("Finish SgPhyOutPickUpGoodGenerateService.generatePhyOutPickUpGood. ResultParams:params:{};"
                    + JSONObject.toJSONString(request));
        }
        return valueHolderV14;
    }


    /**
     * 参数校验
     */
    private ValueHolderV14<Long> checkParam(SgPhyOutPickUpGoodSaveRequest request) {

        ValueHolderV14<Long> valueHolderV14 = new ValueHolderV14<>(ResultCode.FAIL, "");

        if (request == null) {
            valueHolderV14.setMessage("request is null");
            return valueHolderV14;

        } else if (CollectionUtils.isEmpty(request.getOutNoticeIdList())) {
            valueHolderV14.setMessage("请选择需要拣货的数据！");
            return valueHolderV14;
        } else if (request.getLoginUser() == null) {
            valueHolderV14.setMessage("用户信息不能为空！");
            return valueHolderV14;
        }

        valueHolderV14.setCode(ResultCode.SUCCESS);
        return valueHolderV14;
    }

    /**
     * 拣货单更改上游单据状态
     */
    private void updateSourceBillPickOrderStatus(Map<Integer, List<SgBPhyOutNotices>> map, Integer pickUpStatus,User user) {
        int error = 0;

        for (Integer sourceBillTyte : map.keySet()) {
            try {
                switch (sourceBillTyte) {
                    case SgConstantsIF.BILL_TYPE_TRANSFER:
                        //调拨单
                        SgTransferUpdatePickCmd tranUpdateCmd = (SgTransferUpdatePickCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                                SgTransferUpdatePickCmd.class.getName(), "oc", "1.0");
                        OcTransferUpdatePickRequest tranUpdatePickRequest = new OcTransferUpdatePickRequest();
                        List<SgBPhyOutNotices> tranPhyOutNotices = map.get(sourceBillTyte);
                        List<Long> transId = tranPhyOutNotices.stream().map(SgBPhyOutNotices::getSourceBillId).collect(Collectors.toList());
                        tranUpdatePickRequest.setIds(transId);
                        tranUpdatePickRequest.setOutPickStatus(pickUpStatus);
                        tranUpdatePickRequest.setUser(user);
                        ValueHolderV14<List<OcTransferUpdatePickResult>> transferHolderV14 = tranUpdateCmd.batchUpdateTransferPickStatus(tranUpdatePickRequest);
                        if (!transferHolderV14.isOK()) {
                            error++;
                            log.error(" 更新调拨单失败,{}", JSONObject.toJSONString(transferHolderV14));
                        }
                        break;
                    case SgConstantsIF.BILL_TYPE_PUR_REF:
                        //采购退货单
                        OcRefundPurchaseUpdateCmd pubRefUpdateCmd = (OcRefundPurchaseUpdateCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                                OcRefundPurchaseUpdateCmd.class.getName(), "oc", "1.0");
                        OcPurchaseUpdatePickRequest pubRefUpdatePickRequest = new OcPurchaseUpdatePickRequest();
                        List<SgBPhyOutNotices> pubRefOutNotices = map.get(sourceBillTyte);
                        List<Long> pubRefId = pubRefOutNotices.stream().map(SgBPhyOutNotices::getSourceBillId).collect(Collectors.toList());
                        pubRefUpdatePickRequest.setIds(pubRefId);
                        pubRefUpdatePickRequest.setOutPickStatus(pickUpStatus);
                        pubRefUpdatePickRequest.setUser(user);
                        ValueHolderV14<List<OcPurchaseUpdatePickResult>> pubRefHolderV14 = pubRefUpdateCmd.batchUpdateRefPurchasePickStatus(pubRefUpdatePickRequest);
                        if (!pubRefHolderV14.isOK()) {
                            error++;
                            log.error(" 更新采购退货失败,{}" + JSONObject.toJSONString(pubRefHolderV14));
                        }
                        break;
                    case SgConstantsIF.BILL_TYPE_SALE:
                        //销售单
                        OcSaleUpdateCmd saleUpdateCmd = (OcSaleUpdateCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                                OcSaleUpdateCmd.class.getName(), "oc", "1.0");
                        OcSaleUpdatePickRequest salePickRequest = new OcSaleUpdatePickRequest();
                        List<SgBPhyOutNotices> saleOutNotices = map.get(sourceBillTyte);
                        List<Long> saleId = saleOutNotices.stream().map(SgBPhyOutNotices::getSourceBillId).collect(Collectors.toList());
                        salePickRequest.setIds(saleId);
                        salePickRequest.setOutPickStatus(pickUpStatus);
                        salePickRequest.setUser(user);
                        ValueHolderV14<List<OcSaleUpdatePickResult>> saleHolderV14 = saleUpdateCmd.batchUpdateSalePickStatus(salePickRequest);
                        if (!saleHolderV14.isOK()) {
                            error++;
                            log.error(" 更新销售单失败,{}" + JSONObject.toJSONString(saleHolderV14));
                        }
                        break;
                    case SgConstantsIF.BILL_TYPE_SALE_REF:
                        //销售退货单
                        OcRefundSaleUpdateCmd saleRefUpdateCmd = (OcRefundSaleUpdateCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                                OcRefundSaleUpdateCmd.class.getName(), "oc", "1.0");
                        OcSaleUpdatePickRequest ocSaleUpdatePickRequest = new OcSaleUpdatePickRequest();
                        List<SgBPhyOutNotices> saleRefOutNotices = map.get(sourceBillTyte);
                        List<Long> saleRefId = saleRefOutNotices.stream().map(SgBPhyOutNotices::getSourceBillId).collect(Collectors.toList());
                        ocSaleUpdatePickRequest.setIds(saleRefId);
                        ocSaleUpdatePickRequest.setOutPickStatus(pickUpStatus);
                        ocSaleUpdatePickRequest.setUser(user);
                        ValueHolderV14<List<OcSaleUpdatePickResult>> saleRefHolderV14 = saleRefUpdateCmd.batchUpdateRefSalePickStatus(ocSaleUpdatePickRequest);
                        if (!saleRefHolderV14.isOK()) {
                            error++;
                            log.error(" 更新销售退货单失败,{}" + JSONObject.toJSONString(saleRefHolderV14));
                        }
                        break;
                    default:
                        throw new NDSException(Resources.getMessage("SourceBillType Exception:" + sourceBillTyte));
                }
            } catch (NDSException e) {
                error++;
            }
        }

        AssertUtils.isTrue(error == 0, " 出库拣货单更新上游单据拣货状态失败");

    }
}
