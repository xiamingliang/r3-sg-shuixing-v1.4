package com.jackrain.nea.sg.in.mapper;


import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.in.model.table.SgBPhyInResult;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface SgBPhyInResultMapper extends ExtentionMapper<SgBPhyInResult> {

    /**
     * 分页查询入库结果单
     */
    @Select("SELECT * FROM " + SgConstants.SG_B_PHY_IN_RESULT + " ORDER BY ID LIMIT #{page} OFFSET #{offset}")
    List<SgBPhyInResult> selectListByPage(@Param("page") int page, @Param("offset") int offset);

}