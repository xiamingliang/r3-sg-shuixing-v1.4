package com.jackrain.nea.sg.inv.services;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.es.util.ElasticSearchUtil;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.inv.constants.SgInvConstants;
import com.jackrain.nea.sg.inv.mapper.SgBInventoryMapper;
import com.jackrain.nea.sg.inv.model.table.SgBInventory;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Locale;


/**
 * @author 舒威
 * @since 2019/4/1
 * create at : 2019/4/1 18:13
 */
@Slf4j
@Component
public class SgInvProfitLossService {

    /**
     * 查询未盈亏盘点单
     */
    JSONObject queryUnfilledProfitLossByEs(Long storeId, String inventoryType, Long inventoryDate, Integer status,
                                           Boolean orderByAsc, String orderByName, Integer range, Integer startIndex, Locale locale) {
        try {
            status = null == status ? 1 : status;
            startIndex = null == startIndex ? 0 : startIndex;
            orderByAsc = null == orderByAsc ? false : orderByAsc;
            orderByName = StringUtils.isEmpty(orderByName) ? "CREATIONDATE" :
                    ("PAND_DATE".equals(orderByName) ? "INVENTORY_DATE" : orderByName);

            JSONObject whereKeys = new JSONObject();
            whereKeys.put("CP_C_STORE_ID", storeId);
            whereKeys.put("POL_STATUS", status);
            whereKeys.put("ISACTIVE", "Y");
            if (StringUtils.isNotEmpty(inventoryType)) {
                whereKeys.put("INVENTORY_TYPE", inventoryType);
            }
            if (inventoryDate != null) {
                whereKeys.put("INVENTORY_DATE", inventoryDate);
            }
            JSONArray orderKeysArr = new JSONArray();
            JSONObject orderKeys = new JSONObject();
            orderKeys.put("asc", orderByAsc);
            orderKeys.put("name", orderByName);
            orderKeysArr.add(orderKeys);
            JSONObject search = ElasticSearchUtil.search("sc_b_inventory", "sc_b_inventory",
                    whereKeys, null, orderKeysArr, range, startIndex, new String[]{"ID"});
            if (ResultCode.FAIL == search.getInteger("code")) {
                throw new NDSException(Resources.getMessage(search.getString("message"), locale));
            }
            return search;
        } catch (Exception e) {
            e.printStackTrace();
            throw new NDSException(Resources.getMessage(e.getMessage(), locale));
        }
    }

    /**
     * 查询未完成单据
     * <p>
     * 【未完成单据】状态节点判断逻辑：
     * 出库通知单：【发出店仓】=【盘点店仓】、【单据日期】<=【盘点日期】并且【未提交】的单据
     * 库存调整单：【调整店仓】=【盘点店仓】、【库存日期】<=【盘点日期】并且【已审核】且【未验收】的单据
     * 零售单：【零售店仓】=【盘点店仓】、【零售日期】<=【盘点日期】并且【已收银】且【未记账】的单据
     * 订单管理：【结算店仓】=【盘点店仓】、【下单日期】<=【盘点日期】并且【单据状态】为【交易完成】的单据
     * 盘点单：【盘点店仓】=【盘点店仓】、【盘点日期】<【盘点日期】并且【未盈亏】且【未作废】的单据
     * </p>
     */
    JSONObject queryUnfishProfitLossByEs(Long storeId, long time, String inventoryType, String billType, List<String> proEcodes, Locale locale) {
        JSONObject whereKeys = buildWhereKeys(storeId, inventoryType, billType, locale);
        JSONObject filterKeys = buildFilterKeys(time, billType, locale);
        String index = SgInvConstants.OC_B_RETAIL.equals(billType) ?
                SgInvConstants.OC_B_RETAIL : (SgInvConstants.SC_B_INVENTORY.equals(billType) ?
                SgInvConstants.SC_B_INVENTORY : (SgInvConstants.SC_B_OUT.equals(billType) ?
                SgInvConstants.SC_B_OUT : (SgInvConstants.SC_B_STORAGE_ADJUST.equals(billType) ?
                SgInvConstants.SC_B_STORAGE_ADJUST : null)));
        String childType = index + "_item";
        AssertUtils.notBlank(index, billType + "- index获取失败！");

        JSONObject search = new JSONObject();
        if (SgInvConstants.PAND_TYPE_QP.equals(inventoryType)) {
            search = ElasticSearchUtil.search(index, index, whereKeys, filterKeys,
                    null, 500, 0, new String[]{"ID"});
        } else if (SgInvConstants.PAND_TYPE_CP.equals(inventoryType)) {
            JSONArray proEodeArr = JSONArray.parseArray(JSON.toJSONString(proEcodes));
            JSONObject childKeys = new JSONObject();
            childKeys.put("PS_C_PRO_ECODE", proEodeArr);
            search = ElasticSearchUtil.search(index, index, childType, whereKeys,
                    filterKeys, null, childKeys, 500, 0, new String[]{"ID"});
        } else {
            AssertUtils.logAndThrow("盘点类型：" + inventoryType + "异常！", locale);
        }

        if (null == search || ResultCode.FAIL == search.getInteger("code")) {
            AssertUtils.logAndThrow("未完成单据es查询失败！", locale);
        }

        return search;
    }

    /**
     * 未完成单据查询 - filterKeys过滤器构造
     */
    private JSONObject buildFilterKeys(long time, String billType, Locale locale) {
        JSONObject filterKeys = new JSONObject();
        switch (billType) {
            case SgInvConstants.SC_B_OUT:
                filterKeys.put("BILL_DATE", "~" + time);
                break;
            case SgInvConstants.SC_B_STORAGE_ADJUST:
                filterKeys.put("STORAGE_DATE", "~" + time);
                break;
            case SgInvConstants.OC_B_RETAIL:
                filterKeys.put("BILL_DATE", "~" + time);
                break;
            case SgInvConstants.SC_B_INVENTORY:
                time = time - 1;
                filterKeys.put("INVENTORY_DATE", "~" + time);
                break;
            default:
                AssertUtils.logAndThrow("单据类型异常！", locale);
        }
        return filterKeys;
    }

    /**
     * 未完成单据查询 - whereKeys条件筛选器构造
     */
    private JSONObject buildWhereKeys(Long storeId, String inventoryType, String billType, Locale locale) {
        JSONObject whereKeys = new JSONObject();
        switch (billType) {
            case SgInvConstants.SC_B_OUT:
                whereKeys.put("CP_C_ORIG_ID", storeId);
                whereKeys.put("ISACTIVE", "Y");
                whereKeys.put("STATUS", 1);
                break;
            case SgInvConstants.SC_B_STORAGE_ADJUST:
                whereKeys.put("CP_C_STORE_ID", storeId);
                whereKeys.put("ISACTIVE", "Y");
                whereKeys.put("STATUS", 2);
                whereKeys.put("ACCEPT_STATUS", 1);
                break;
            case SgInvConstants.OC_B_RETAIL:
                /*TODO 零售单暂未开发*/
                break;
            case SgInvConstants.SC_B_INVENTORY:
                whereKeys.put("CP_C_STORE_ID", storeId);
                whereKeys.put("ISACTIVE", "Y");
                whereKeys.put("POL_STATUS", 1);
                whereKeys.put("INVENTORY_TYPE", inventoryType);
                break;
            default:
                AssertUtils.logAndThrow("单据类型异常！", locale);
        }
        return whereKeys;
    }

    /**
     * 更新盘点单信息
     */
    @Transactional
    void updateInventory(List<Long> ids, User loginUser) {
        try {
            SgBInventoryMapper inventoryMapper = ApplicationContextHandle.getBean(SgBInventoryMapper.class);
            for (Long id : ids) {
                SgBInventory inventory = new SgBInventory();
                inventory.setId(id);
                inventory.setPolStatus(SgInvConstants.PAND_POL);
                inventory.setModifierid(loginUser.getId().longValue());
                inventory.setModifiername(loginUser.getName());
                inventory.setModifierename(loginUser.getEname());
                inventory.setModifieddate(new Date());
                int cn = inventoryMapper.updateById(inventory);
                if (cn == 0) {
                    throw new NDSException(Resources.getMessage("盘点单-" + id + "更新失败！", loginUser.getLocale()));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new NDSException(Resources.getMessage(e.getMessage(), loginUser.getLocale()));
        }

    }
}
