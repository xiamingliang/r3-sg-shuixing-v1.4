package com.jackrain.nea.sg.phyadjust.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Lists;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.phyadjust.mapper.SgBPhyAdjustItemMapper;
import com.jackrain.nea.sg.phyadjust.mapper.SgBPhyAdjustMapper;
import com.jackrain.nea.sg.phyadjust.model.request.SgPhyAdjustBillQueryRequest;
import com.jackrain.nea.sg.phyadjust.model.result.SgPhyAdjustBillQueryResult;
import com.jackrain.nea.sg.phyadjust.model.table.SgBPhyAdjust;
import com.jackrain.nea.sg.phyadjust.model.table.SgBPhyAdjustItem;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author: 舒威
 * @since: 2019/7/29
 * create at : 2019/7/29 22:23
 */
@Slf4j
@Component
public class SgPhyAdjustQueryService {


    public ValueHolderV14<SgPhyAdjustBillQueryResult> queryPhyAdj(SgPhyAdjustBillQueryRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("SgPhyAdjustQueryService.queryPhyAdj:" + JSONObject.toJSONString(request) + ";");
        }

        Long sourceBillId = request.getSourceBillId();
        String sourceBillNo = request.getSourceBillNo();
        Integer sourceBillType = request.getSourceBillType();
        if (sourceBillId == null && StringUtils.isEmpty(sourceBillNo) && sourceBillType == null) {
            AssertUtils.logAndThrow("来源单据信息不能为空!");
        }

        SgBPhyAdjustMapper adjustMapper = ApplicationContextHandle.getBean(SgBPhyAdjustMapper.class);
        SgBPhyAdjustItemMapper adjustItemMapper = ApplicationContextHandle.getBean(SgBPhyAdjustItemMapper.class);

        SgBPhyAdjust adjust = null;
        if (sourceBillId != null && sourceBillType != null) {
            List<SgBPhyAdjust> sgBPhyAdjusts = adjustMapper.selectList(new QueryWrapper<SgBPhyAdjust>().lambda()
                    .eq(SgBPhyAdjust::getSourceBillId, sourceBillId)
                    .eq(SgBPhyAdjust::getSourceBillType, sourceBillType)
                    .eq(SgBPhyAdjust::getIsactive, SgConstants.IS_ACTIVE_Y));
            if (CollectionUtils.isNotEmpty(sgBPhyAdjusts)) {
                adjust = sgBPhyAdjusts.get(0);
            }
        } else if (sourceBillId == null && StringUtils.isNotEmpty(sourceBillNo) && sourceBillType != null) {
            List<SgBPhyAdjust> sgBPhyAdjusts = adjustMapper.selectList(new QueryWrapper<SgBPhyAdjust>().lambda()
                    .eq(SgBPhyAdjust::getSourceBillType, sourceBillType)
                    .eq(SgBPhyAdjust::getSourceBillNo, sourceBillNo)
                    .eq(SgBPhyAdjust::getIsactive, SgConstants.IS_ACTIVE_Y));
            if (CollectionUtils.isNotEmpty(sgBPhyAdjusts)) {
                adjust = sgBPhyAdjusts.get(0);
            }
        } else {
            AssertUtils.logAndThrow("来源单据编号和类型不能同时为空！");
        }

        List<SgBPhyAdjustItem> items = Lists.newArrayList();
        if (adjust != null) {
            items = adjustItemMapper.selectList(new QueryWrapper<SgBPhyAdjustItem>().lambda()
                    .eq(SgBPhyAdjustItem::getSgBPhyAdjustId, adjust.getId())
                    .eq(SgBPhyAdjustItem::getIsactive, SgConstants.IS_ACTIVE_Y));
        }

        SgPhyAdjustBillQueryResult queryResult = new SgPhyAdjustBillQueryResult();
        queryResult.setAdjust(adjust);
        queryResult.setAdjustItems(items);
        ValueHolderV14<SgPhyAdjustBillQueryResult> v14 = new ValueHolderV14<>();
        v14.setCode(ResultCode.SUCCESS);
        v14.setMessage("success");
        v14.setData(queryResult);
        return v14;
    }

}
