package com.jackrain.nea.sg.in.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.mapper.CpStoreMapper;
import com.jackrain.nea.sg.basic.model.table.CpCStore;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.in.common.SgInNoticeConstants;
import com.jackrain.nea.sg.in.mapper.SgBPhyInNoticesMapper;
import com.jackrain.nea.sg.in.model.table.SgBPhyInNotices;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author: 周琳胜
 * @since: 2019/11/1
 *  查询调拨类型的入库通知单数量
 * create at : 2019/11/1 13:48
 */
@Component
@Slf4j
public class SgPhyInNoticesCountForPDAService {

    public ValueHolderV14 selectTransferTypeNum(Long storeId) {
        ValueHolderV14 vh = new ValueHolderV14();
        //参数校验
        AssertUtils.isTrue(storeId != null, "参数不能为空！");

        CpStoreMapper cpStoreMapper = ApplicationContextHandle.getBean(CpStoreMapper.class);
        // 查询返回指定字段 实体中其他字段值没null
        List<CpCStore> cpCStores = cpStoreMapper.selectList(new LambdaQueryWrapper<CpCStore>().select(CpCStore::getCpCPhyWarehouseId)
                .eq(CpCStore::getId, storeId));
        Long warehouseId = cpCStores.get(0).getCpCPhyWarehouseId();
        AssertUtils.isTrue(warehouseId != null, "未查到对应的实体仓！");

        SgBPhyInNoticesMapper sgBPhyInNoticesMapper = ApplicationContextHandle.getBean(SgBPhyInNoticesMapper.class);
        Integer count = sgBPhyInNoticesMapper.selectCount(new QueryWrapper<SgBPhyInNotices>().lambda()
                .eq(SgBPhyInNotices::getCpCPhyWarehouseId, warehouseId)
                .in(SgBPhyInNotices::getBillStatus, SgInNoticeConstants.BILL_STATUS_IN_PENDING,
                        SgInNoticeConstants.BILL_STATUS_IN_PART));
        JSONObject object = new JSONObject();
        object.put("count", count);
        vh.setCode(ResultCode.SUCCESS);
        vh.setMessage("成功");
        vh.setData(object);
        return vh;
    }

}
