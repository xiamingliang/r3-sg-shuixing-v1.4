package com.jackrain.nea.sg.out.services;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Lists;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.core.db.Tools;
import com.jackrain.nea.sg.basic.common.R3ParamConstants;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.config.SgStorageBoxConfig;
import com.jackrain.nea.sg.basic.model.result.SgR3BaseResult;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.basic.utils.StorageUtils;
import com.jackrain.nea.sg.out.common.SgOutConstantsIF;
import com.jackrain.nea.sg.out.mapper.SgBOutDeliveryMapper;
import com.jackrain.nea.sg.out.mapper.SgBPhyOutNoticesMapper;
import com.jackrain.nea.sg.out.mapper.SgBPhyOutResultImpItemMapper;
import com.jackrain.nea.sg.out.mapper.SgBPhyOutResultItemMapper;
import com.jackrain.nea.sg.out.model.request.SgPhyOutDeliverySaveRequest;
import com.jackrain.nea.sg.out.model.request.SgPhyOutResultBillSaveRequest;
import com.jackrain.nea.sg.out.model.request.SgPhyOutResultSaveRequest;
import com.jackrain.nea.sg.out.model.table.SgBOutDelivery;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutNotices;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutResultImpItem;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutResultItem;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * @author leexh
 * @since 2019/5/7 16:54
 */
@Slf4j
@Component
public class SgPhyOutResultSaveAndAuditService {

    @Autowired
    private SgStorageBoxConfig storageBoxConfig;

    public ValueHolderV14 saveOutResultAndAuditNoTrans(SgPhyOutResultBillSaveRequest request) {

        ValueHolderV14 v14 = new ValueHolderV14();
        User loginUser = request.getLoginUser();

        try {
            AssertUtils.notNull(loginUser, "用户信息不能为空！");
            Locale locale = loginUser.getLocale();
            AssertUtils.notNull(request, "参数不能为空！", loginUser.getLocale());
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + ".debug, param:" + JSONObject.toJSONString(request));
            }

            SgPhyOutResultSaveRequest outResultRequest = request.getOutResultRequest();
            AssertUtils.notNull(outResultRequest, "主表信息不能为空！", locale);
            Long sgBPhyOutNoticesId = outResultRequest.getSgBPhyOutNoticesId();
            AssertUtils.notNull(sgBPhyOutNoticesId, "通知单ID不能为空！", locale);

            SgBPhyOutNoticesMapper noticesMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesMapper.class);
            SgBPhyOutNotices outNotices = noticesMapper.selectById(sgBPhyOutNoticesId);
            AssertUtils.notNull(outNotices, "对应出库通知单不存在！", locale);
            Integer billStatus = outNotices.getBillStatus();

            SgPhyOutResultSaveAndAuditService saveAndAuditService = ApplicationContextHandle.getBean(SgPhyOutResultSaveAndAuditService.class);
            /**
             * 2.	判断状态是否为【待出库】/【部分出库】
             */
            if (billStatus == SgOutConstantsIF.OUT_NOTICES_STATUS_WAIT ||
                    billStatus == SgOutConstantsIF.OUT_NOTICES_STATUS_PART) {
                saveAndAuditService.saveAndAuditOutResultNoTrans(request, loginUser);
            } else {
                AssertUtils.logAndThrow("出库通知单不是待出库或者部分出库，不允许新增出库结果单！", locale);
            }

            v14.setCode(ResultCode.SUCCESS);
            v14.setMessage("回传成功！");
        } catch (Exception e) {
            log.error("新增并审核出库结果单异常!" + e.getMessage(), e);
            v14.setCode(ResultCode.FAIL);
            v14.setMessage("新增并审核出库结果单异常!" + e.getMessage());
        }
        return v14;
    }

    /**
     * 出库结果单 新增和审核事务分割开
     * 2019-05-31修改逻辑：新增不放到审核事务里
     *
     * @param request   入参
     * @param loginUser 用户信息
     */
    public void saveAndAuditOutResultNoTrans(SgPhyOutResultBillSaveRequest request, User loginUser) {
        SgPhyOutResultSaveService resultSaveService = ApplicationContextHandle.getBean(SgPhyOutResultSaveService.class);
        ValueHolderV14<SgR3BaseResult> saveSgPhyOutResult = resultSaveService.saveSgPhyOutResult(request);


        if (saveSgPhyOutResult != null && saveSgPhyOutResult.getCode() == ResultCode.SUCCESS) {
            SgR3BaseResult data = saveSgPhyOutResult.getData();
            Long objId = data.getDataJo().getLong(R3ParamConstants.OBJID);

            // 新增多包裹明细
            addOutDelivery(loginUser, request.getOutDeliverySaveRequests(), objId, request.getLoginUser().getLocale());

            // 新增成功 执行审核逻辑
            SgPhyOutResultAuditService auditService = ApplicationContextHandle.getBean(SgPhyOutResultAuditService.class);
            auditService.auditSingleSgOutResult(objId, loginUser, request.getOutResultRequest().getOutTime(),request.getIsOneClickOutLibrary(),null);
        }
    }


    public ValueHolderV14 saveOutResultAndAudit(SgPhyOutResultBillSaveRequest request) {

        ValueHolderV14 v14 = new ValueHolderV14();
        User loginUser = request.getLoginUser();

        try {
            AssertUtils.notNull(loginUser, "用户信息不能为空！");
            Locale locale = loginUser.getLocale();
            AssertUtils.notNull(request, "参数不能为空！", loginUser.getLocale());
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + ".debug, param:" + JSONObject.toJSONString(request));
            }

            SgPhyOutResultSaveRequest outResultRequest = request.getOutResultRequest();
            AssertUtils.notNull(outResultRequest, "主表信息不能为空！", locale);
            Long sgBPhyOutNoticesId = outResultRequest.getSgBPhyOutNoticesId();
            AssertUtils.notNull(sgBPhyOutNoticesId, "通知单ID不能为空！", locale);

            SgBPhyOutNoticesMapper noticesMapper = ApplicationContextHandle.getBean(SgBPhyOutNoticesMapper.class);
            SgBPhyOutNotices outNotices = noticesMapper.selectById(sgBPhyOutNoticesId);
            AssertUtils.notNull(outNotices, "对应出库通知单不存在！", locale);
//            Integer billStatus = outNotices.getBillStatus();

            SgPhyOutResultSaveAndAuditService saveAndAuditService = ApplicationContextHandle.getBean(SgPhyOutResultSaveAndAuditService.class);
            /**
             * 2.	判断状态是否为【待出库】/【部分出库】
             */
            //start sunjunlei 取消出库不做控制
//            if (billStatus == SgOutConstantsIF.OUT_NOTICES_STATUS_WAIT ||
//                    billStatus == SgOutConstantsIF.OUT_NOTICES_STATUS_PART) {
                saveAndAuditService.saveAndAuditOutResult(request, loginUser);
//            } else {
//                AssertUtils.logAndThrow("出库通知单不是待出库或者部分出库，不允许新增出库结果单！", locale);
//            }
            //end sunjunlei 取消出库不做控制

            v14.setCode(ResultCode.SUCCESS);
            v14.setMessage("回传成功！");
        } catch (Exception e) {
            log.error("新增并审核出库结果单异常!" + e.getMessage(), e);
            v14.setCode(ResultCode.FAIL);
            v14.setMessage("新增并审核出库结果单异常!" + e.getMessage());
        }
        return v14;
    }

    /**
     * 出库结果单 新增和审核同一事务
     * 2019-05-31修改逻辑：新增不放到审核事务里
     *
     * @param request   入参
     * @param loginUser 用户信息
     */
    @Transactional(rollbackFor = Exception.class)
    public void saveAndAuditOutResult(SgPhyOutResultBillSaveRequest request, User loginUser) {
        SgPhyOutResultSaveService resultSaveService = ApplicationContextHandle.getBean(SgPhyOutResultSaveService.class);
        ValueHolderV14<SgR3BaseResult> saveSgPhyOutResult = resultSaveService.saveSgPhyOutResult(request);


        if (saveSgPhyOutResult != null && saveSgPhyOutResult.getCode() == ResultCode.SUCCESS) {
            SgR3BaseResult data = saveSgPhyOutResult.getData();
            Long objId = data.getDataJo().getLong(R3ParamConstants.OBJID);

            //WMS回传新增多包裹明细
            addOutDelivery(loginUser, request.getOutDeliverySaveRequests(), objId, request.getLoginUser().getLocale());

            if (request.getIsCreateDelivers()) {
                //全渠道发货单回传新增多包裹明细
                Long cpCLogisticsId = request.getOutResultRequest().getCpCLogisticsId();
                String cpCLogisticsEcode = request.getOutResultRequest().getCpCLogisticsEcode();
                String cpCLogisticsEname = request.getOutResultRequest().getCpCLogisticsEname();
                String logisticNumber = request.getOutResultRequest().getLogisticNumber();
                Long sourceBillId = request.getOutResultRequest().getSourceBillId();
                List<SgPhyOutDeliverySaveRequest> deliveryList = Lists.newArrayList();
                if (storageBoxConfig.getBoxEnable()) {
                    SgBPhyOutResultImpItemMapper impItemMapper = ApplicationContextHandle.getBean(SgBPhyOutResultImpItemMapper.class);
                    List<SgBPhyOutResultImpItem> resultImpItems = impItemMapper.selectList(new QueryWrapper<SgBPhyOutResultImpItem>().lambda()
                            .eq(SgBPhyOutResultImpItem::getSgBPhyOutResultId, objId));
                    for (SgBPhyOutResultImpItem resultsItem : resultImpItems) {
                        SgPhyOutDeliverySaveRequest deliverySaveRequest = new SgPhyOutDeliverySaveRequest();
                        BeanUtils.copyProperties(resultsItem, deliverySaveRequest);
                        deliverySaveRequest.setCpCLogisticsId(cpCLogisticsId != null ? cpCLogisticsId.toString() : "");
                        deliverySaveRequest.setCpCLogisticsEcode(cpCLogisticsEcode);
                        deliverySaveRequest.setCpCLogisticsEname(cpCLogisticsEname);
                        deliverySaveRequest.setLogisticNumber(logisticNumber);
                        deliverySaveRequest.setOcBOrderId(sourceBillId);
                        deliverySaveRequest.setQty(resultsItem.getQtyOut());
                        deliverySaveRequest.setPsCClrId(resultsItem.getPsCSpec1Id());
                        deliverySaveRequest.setPsCClrEcode(resultsItem.getPsCSpec1Ecode());
                        deliverySaveRequest.setPsCClrEname(resultsItem.getPsCSpec1Ename());
                        deliverySaveRequest.setPsCSizeId(resultsItem.getPsCSpec2Id());
                        deliverySaveRequest.setPsCSizeEcode(resultsItem.getPsCSpec2Ecode());
                        deliverySaveRequest.setPsCSizeEname(resultsItem.getPsCSpec2Ename());
                        deliverySaveRequest.setWeight(BigDecimal.ONE);
                        deliverySaveRequest.setSize(BigDecimal.ONE);
                        deliveryList.add(deliverySaveRequest);
                    }
                }else {
                    SgBPhyOutResultItemMapper resultItemMapper = ApplicationContextHandle.getBean(SgBPhyOutResultItemMapper.class);
                    List<SgBPhyOutResultItem> resultsItems = resultItemMapper.selectList(new QueryWrapper<SgBPhyOutResultItem>().lambda()
                            .eq(SgBPhyOutResultItem::getSgBPhyOutResultId, objId));
                    for (SgBPhyOutResultItem resultsItem : resultsItems) {
                        SgPhyOutDeliverySaveRequest deliverySaveRequest = new SgPhyOutDeliverySaveRequest();
                        BeanUtils.copyProperties(resultsItem, deliverySaveRequest);
                        deliverySaveRequest.setCpCLogisticsId(cpCLogisticsId != null ? cpCLogisticsId.toString() : "");
                        deliverySaveRequest.setCpCLogisticsEcode(cpCLogisticsEcode);
                        deliverySaveRequest.setCpCLogisticsEname(cpCLogisticsEname);
                        deliverySaveRequest.setLogisticNumber(logisticNumber);
                        deliverySaveRequest.setOcBOrderId(sourceBillId);
                        deliverySaveRequest.setPsCClrId(resultsItem.getPsCSpec1Id());
                        deliverySaveRequest.setPsCClrEcode(resultsItem.getPsCSpec1Ecode());
                        deliverySaveRequest.setPsCClrEname(resultsItem.getPsCSpec1Ename());
                        deliverySaveRequest.setPsCSizeId(resultsItem.getPsCSpec2Id());
                        deliverySaveRequest.setPsCSizeEcode(resultsItem.getPsCSpec2Ecode());
                        deliverySaveRequest.setPsCSizeEname(resultsItem.getPsCSpec2Ename());
                        deliverySaveRequest.setWeight(BigDecimal.ONE);
                        deliverySaveRequest.setSize(BigDecimal.ONE);
                        deliveryList.add(deliverySaveRequest);
                    }
                }
                SgPhyOutResultSaveAndAuditService saveAndAuditService = ApplicationContextHandle.getBean(SgPhyOutResultSaveAndAuditService.class);
                saveAndAuditService.addOutDelivery(loginUser, deliveryList, objId, loginUser.getLocale());
            }


            // 新增成功 执行审核逻辑
            SgPhyOutResultAuditService auditService = ApplicationContextHandle.getBean(SgPhyOutResultAuditService.class);
            auditService.auditSingleSgOutResult(objId, loginUser, request.getOutResultRequest().getOutTime(),request.getIsOneClickOutLibrary(),request.getPackOrderId());
        }
    }

    /**
     * 新增多包裹明细
     *
     * @param deliveryRequests 多包裹对象
     * @param outResultId      出库结果单ID
     */
    public void addOutDelivery(User user, List<SgPhyOutDeliverySaveRequest> deliveryRequests,
                               Long outResultId, Locale locale) {

        if (!CollectionUtils.isEmpty(deliveryRequests)) {
            log.debug(this.getClass().getName() + ",addOutDelivery,多包裹入参:" + JSON.toJSONString(deliveryRequests)
                    + ",outResultId:" + outResultId);

            String eName = user.getEname();

            List<SgBOutDelivery> deliveries = new ArrayList<>();
            deliveryRequests.forEach(delivery -> {
                SgBOutDelivery outDelivery = new SgBOutDelivery();
                BeanUtils.copyProperties(delivery, outDelivery);
                outDelivery.setId(Tools.getSequence(SgConstants.SG_B_OUT_DELIVERY));
                outDelivery.setSgBPhyOutResultId(outResultId);

                StorageUtils.setBModelDefalutData(outDelivery, user);
                outDelivery.setOwnerename(eName);
                outDelivery.setModifierename(eName);
                deliveries.add(outDelivery);
            });

            List<List<SgBOutDelivery>> baseModelPageList = StorageUtils.getBaseModelPageList(
                    deliveries, SgConstants.SG_COMMON_INSERT_PAGE_SIZE);

            int result = 0;
            for (List<SgBOutDelivery> pageList : baseModelPageList) {

                if (CollectionUtils.isEmpty(pageList)) {
                    continue;
                }
                SgBOutDeliveryMapper deliveryMapper = ApplicationContextHandle.getBean(SgBOutDeliveryMapper.class);
                int count = deliveryMapper.batchInsert(pageList);
                if (count != pageList.size()) {
                    log.error(this.getClass().getName() + ",多包裹明细批量新增失败！");
                    AssertUtils.logAndThrow(",多包裹明细批量新增失败！", locale);
                }
                result += count;
            }
            log.error(this.getClass().getName() + ",addOutDelivery count:" + result);
        }
    }
}
