package com.jackrain.nea.sg.inv.services;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.Feature;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.cp.request.CUsersInfoQueryCmdRequest;
import com.jackrain.nea.data.basic.services.BasicCpQueryService;
import com.jackrain.nea.sg.basic.mapper.CpStoreMapper;
import com.jackrain.nea.sg.basic.model.table.CpCStore;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.query.DefaultWebEvent;
import com.jackrain.nea.web.query.QuerySession;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * @author: 周琳胜
 * @since: 2019/11/13
 * create at : 2019/11/13 11:20
 */
@Slf4j
@Component
public class ScBInventoryAddLoadService {



    public ValueHolder addLoad(QuerySession session) {

        ValueHolder valueHolder = new ValueHolder();
        DefaultWebEvent event = session.getEvent();
        JSONObject param = JSON.parseObject(JSON.toJSONStringWithDateFormat(event.getParameterValue("param"),
                "yyyy-MM-dd HH:mm:ss", SerializerFeature.WriteMapNullValue), Feature.OrderedField);
        User user = session.getUser();
        JSONObject fixColumn = param.getJSONObject("fixcolumn");
        Long storeId = fixColumn.getLong("STORE_ID");
        if(storeId == null){
            BasicCpQueryService basicCpQueryService = ApplicationContextHandle.getBean(BasicCpQueryService.class);
            CUsersInfoQueryCmdRequest list = new CUsersInfoQueryCmdRequest();
            List<String> name = new ArrayList<>();
            name.add(user.getName());
            list.setNameList(name);
            try {
                HashMap<String, User> resultMap = basicCpQueryService.getUserInfoByName(list);
                User user1 = resultMap.get(user.getName());
                storeId = Long.valueOf(user1.getCpCstoreId() == null? 0 : user1.getCpCstoreId());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + "店仓id:{}", storeId);
        }
        CpStoreMapper cpStoreMapper = ApplicationContextHandle.getBean(CpStoreMapper.class);
        CpCStore cpCStore = cpStoreMapper.selectById(storeId);
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + "店仓信息:{}", cpCStore);
        }
        Long cpCPhyWarehouseId = null;
        String cpCPhyWarehouseEcode = null;
        String cpCPhyWarehouseEname = null;
        if (cpCStore != null) {
            cpCPhyWarehouseId = cpCStore.getCpCPhyWarehouseId();
            cpCPhyWarehouseEcode = cpCStore.getCpCPhyWarehouseEcode();
            cpCPhyWarehouseEname = cpCStore.getCpCPhyWarehouseEname();
        }
        HashMap data = new HashMap();
        data.put("ID", cpCPhyWarehouseId);
        data.put("ECODE", cpCPhyWarehouseEcode);
        data.put("ENAME", cpCPhyWarehouseEname);
        data.put("SYSTEM_DATE", new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
        valueHolder.put("code", ResultCode.SUCCESS);
        valueHolder.put("message", "success");
        valueHolder.put("data", data);
        return valueHolder;

    }

}
