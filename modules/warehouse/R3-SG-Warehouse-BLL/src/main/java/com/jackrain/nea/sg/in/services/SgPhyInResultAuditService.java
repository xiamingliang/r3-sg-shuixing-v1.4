package com.jackrain.nea.sg.in.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.config.PropertiesConf;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.r3.mq.exception.SendMqException;
import com.jackrain.nea.r3.mq.util.R3MqSendHelper;
import com.jackrain.nea.redis.config.CusRedisTemplate;
import com.jackrain.nea.redis.util.RedisOpsUtil;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.config.SgStorageBoxConfig;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.basic.utils.StorageMQAsyncUtils;
import com.jackrain.nea.sg.in.common.SgInConstants;
import com.jackrain.nea.sg.in.mapper.SgBPhyInResultImpItemMapper;
import com.jackrain.nea.sg.in.mapper.SgBPhyInResultItemMapper;
import com.jackrain.nea.sg.in.mapper.SgBPhyInResultMapper;
import com.jackrain.nea.sg.in.mapper.SgBPhyInResultTeusItemMapper;
import com.jackrain.nea.sg.in.model.request.*;
import com.jackrain.nea.sg.in.model.result.ReturnSgPhyInResult;
import com.jackrain.nea.sg.in.model.table.SgBPhyInResult;
import com.jackrain.nea.sg.in.model.table.SgBPhyInResultImpItem;
import com.jackrain.nea.sg.in.model.table.SgBPhyInResultItem;
import com.jackrain.nea.sg.in.model.table.SgBPhyInResultTeusItem;
import com.jackrain.nea.sg.out.config.SgOutMqConfig;
import com.jackrain.nea.sg.out.model.request.SgPhyOutPickOrderSaveRequest;
import com.jackrain.nea.sg.out.model.request.SgPhyOutResultSaveRequest;
import com.jackrain.nea.sg.out.services.SgPhyOutPickOrderSaveService;
import com.jackrain.nea.sg.out.utils.SgWarehouseESUtils;
import com.jackrain.nea.sg.receive.model.result.SgReceiveBillSubmitResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.assertj.core.util.Lists;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;


/**
 * 入库结果单审核业务处理
 *
 * @author jiang.cj
 * @since 2019-4-25
 * create at : 2019-4-25
 */
@Slf4j
@Component
public class SgPhyInResultAuditService {

    @Autowired
    private SgBPhyInResultMapper resultMapper;

    @Autowired
    private SgBPhyInResultItemMapper resultItemMapper;

    @Autowired
    private SgPhyInRPCService rpcService;

    @Autowired
    private SgPhyInNoticesPassService sgPhyInNoticesPassService;

    @Autowired
    private PropertiesConf pconf;

    @Autowired
    private R3MqSendHelper r3MqSendHelper;

    @Autowired
    private SgStorageBoxConfig storageBoxConfig;

    @Autowired
    private SgOutMqConfig mqConfig;

    @Autowired
    private SgWarehouseESUtils warehouseESUtils;

    /**
     * 入库结果单审核
     *
     * @param request
     * @return
     */

    public ValueHolderV14<List<ReturnSgPhyInResult>> auditSgPhyInResult(SgPhyInResultBillAuditRequest request) throws NDSException {
        if (log.isDebugEnabled()) {
            log.debug("Start SgPhyInResultAuditService.auditSgPhyInResult. ReceiveParams:SgPhyInResultBillAuditRequest="
                    + JSONObject.toJSONString(request) + ";");
        }
        SgPhyInResultAuditService obj = ApplicationContextHandle.getBean(SgPhyInResultAuditService.class);
        User user = request.getLoginUser();
        List<Long> idList = JSONObject.parseArray(JSONObject.toJSONString(request.getIds()), Long.class);
        List<ReturnSgPhyInResult> dataList = Lists.newArrayList();
        Boolean isOneClickLirary = Optional.ofNullable(request.getIsOneClickLibrary()).orElse(false);

        for (Long id : idList) {
            //加锁
            CusRedisTemplate<String, String> redisTemplate = RedisOpsUtil.getStrRedisTemplate();
            //获取入库结果单
            SgBPhyInResult billResult = resultMapper.selectById(id);
            Long sourceBillId = billResult.getSourceBillId();
            Integer sourceBillType = billResult.getSourceBillType();
            String lockKsy = SgConstants.SG_B_PHY_IN_RESULT + ":" + sourceBillId + ":" + sourceBillType;
            try {
                if (redisTemplate.opsForValue().get(lockKsy) == null) {
                    //设置锁
                    redisTemplate.opsForValue().set(lockKsy, "OK", 30, TimeUnit.MINUTES);
                    ValueHolderV14 result = obj.aduitSgResultBill(id, user, isOneClickLirary,request.getPickOrderId());
                    dataList.add(setResult(id, result.getCode(), result.getMessage()));
                } else {
                    String message = "当前单据已在审核中，不允许重复操作，来源单据ID:" + sourceBillId + ",来源单据类型" + sourceBillType;
                    dataList.add(setResult(id, ResultCode.FAIL, message));
                    log.error(this.getClass().getName() + ",error" + message);
                }
            } catch (Exception e) {
                dataList.add(setResult(id, ResultCode.FAIL, e.getMessage()));
                log.error(this.getClass().getName() + ",error:" + e.toString());
            } finally {
                try {
                    redisTemplate.delete(lockKsy);
                } catch (Exception e) {
                    log.error(this.getClass().getName() + "redis锁释放失败，请联系管理员！lockKsy：" + lockKsy);
                }
            }
        }
        ValueHolderV14<List<ReturnSgPhyInResult>> vh = new ValueHolderV14<>(ResultCode.SUCCESS, "入库结果单审核执行结束!");
        vh.setData(dataList);
        return vh;
    }

    @Transactional(rollbackFor = Exception.class)
    public ValueHolderV14 aduitSgResultBill(Long id, User user, Boolean isOneClickLirary,Long pickOrderId) throws NDSException, SendMqException {
        if (log.isDebugEnabled()) {
            log.debug("入库结果单id[" + id + "]开始审核!");
        }
        long startTime = System.currentTimeMillis();
        ValueHolderV14 vh = new ValueHolderV14();
        //校验参数
        ValueHolderV14 checkResult = checkParams(id);
        if (!checkResult.isOK()) {
            throw new NDSException(Resources.getMessage(checkResult.getMessage(), user.getLocale()));
        }

        SgBPhyInResult inResult = resultMapper.selectById(id);

        List<SgBPhyInResultImpItem> resultImpItems = new ArrayList<>();
        List<SgBPhyInResultTeusItem> resultTeusItems = new ArrayList<>();

        if (storageBoxConfig.getBoxEnable()) {
            //录入明细 转 条码明细 /箱内明细
            SgPhyInNoticesTeusFunctionService service = ApplicationContextHandle.getBean(SgPhyInNoticesTeusFunctionService.class);
            SgBPhyInResultImpItemMapper impItemMapper = ApplicationContextHandle.getBean(SgBPhyInResultImpItemMapper.class);
            resultImpItems = impItemMapper.selectList(new QueryWrapper<SgBPhyInResultImpItem>().lambda().eq(SgBPhyInResultImpItem::getSgBPhyInResultId, id));
            AssertUtils.isTrue(CollectionUtils.isNotEmpty(resultImpItems), "入库结果单录入明细为空，不允许操作！", user.getLocale());
            service.impItemConvertTeusMethod(resultImpItems, user);

            // 查出箱明细
            SgBPhyInResultTeusItemMapper teusItemMapper = ApplicationContextHandle.getBean(SgBPhyInResultTeusItemMapper.class);
            resultTeusItems = teusItemMapper.selectList(new QueryWrapper<SgBPhyInResultTeusItem>().lambda().
                    eq(SgBPhyInResultTeusItem::getSgBPhyInResultId, id));

            //回写通知单录入明细
            service.updagteSgPhyInNoticesByBox(user, inResult.getSgBPhyInNoticesId(), resultImpItems);
        }

        List<SgBPhyInResultItem> inResultitems = resultItemMapper.selectList(new QueryWrapper<SgBPhyInResultItem>().lambda()
                .eq(SgBPhyInResultItem::getSgBPhyInResultId, id));

        //更新入库结果单
        SgBPhyInResult update = new SgBPhyInResult();
        update.setId(id);
        update.setBillStatus(SgInConstants.BILL_STATUS_CHECKED);
        update.setModifierename(user.getEname());
        StorageESUtils.setBModelDefalutDataByUpdate(update, user);
        if (storageBoxConfig.getBoxEnable()) {
            BigDecimal totQtyIn = inResultitems.stream().map(SgBPhyInResultItem::getQtyIn).reduce(BigDecimal.ZERO, BigDecimal::add);
            BigDecimal totAmtListIn = inResultitems.stream().map(SgBPhyInResultItem::getAmtListIn).reduce(BigDecimal.ZERO, BigDecimal::add);
            update.setTotQtyIn(totQtyIn);
            update.setTotAmtListIn(totAmtListIn);
        }
        resultMapper.updateById(update);

        inResult.setBillStatus(SgInConstants.BILL_STATUS_CHECKED);

        //回写通知单条码明细
        SgPhyInResultAuditService obj = ApplicationContextHandle.getBean(SgPhyInResultAuditService.class);
        ValueHolderV14 noticeResult = obj.updagteSgPhyInNotices(inResult, inResultitems, user,pickOrderId);
        if (!noticeResult.isOK()) {
            throw new NDSException(Resources.getMessage(noticeResult.getMessage(), user.getLocale()));
        }

        //逻辑收货单入库
        ValueHolderV14<SgReceiveBillSubmitResult> receiveResult = rpcService.sgBReceiveIn(inResult, inResultitems, user, resultImpItems, resultTeusItems);
        if (!receiveResult.isOK()) {
            throw new NDSException(Resources.getMessage(receiveResult.getMessage(), user.getLocale()));
        }

        //更新实体仓库
        ValueHolderV14 storageResult = rpcService.updateSgPhyStorage(inResult, inResultitems, user, resultImpItems, resultTeusItems);
        if (!storageResult.isOK()) {
            throw new NDSException(Resources.getMessage(storageResult.getMessage(), user.getLocale()));
        }


        //拣货单生成的出库结果单审核时，回写拣货单的来源出库结果单表
        if (pickOrderId != null) {
            SgPhyInPickOrderSaveService saveService = ApplicationContextHandle.getBean(SgPhyInPickOrderSaveService.class);
            SgPhyInPickOrderSaveRequest sgPhyOutPickOrderSaveRequest = new SgPhyInPickOrderSaveRequest();
            List<SgBPhyInResultSaveRequest> sgPhyOutResultSaveRequests = new ArrayList<>();
            SgBPhyInResultSaveRequest saveRequest = new SgBPhyInResultSaveRequest();
            BeanUtils.copyProperties(inResult,saveRequest);
            sgPhyOutResultSaveRequests.add(saveRequest);
            sgPhyOutPickOrderSaveRequest.setInResultList(sgPhyOutResultSaveRequests);
            sgPhyOutPickOrderSaveRequest.setLoginUser(user);
            sgPhyOutPickOrderSaveRequest.setSgPhyBInPickorderId(pickOrderId);
            saveService.saveSgbBInPickorderResultItem(sgPhyOutPickOrderSaveRequest);
        }

        //mq通知上游单据
        try {
            Integer sourceBillType = inResult.getSourceBillType();
            log.debug("入库结果单ID" + id + ",审核开始发送MQ通知对应单据;");
            log.info("来源单据id[" + inResult.getSourceBillId() + "],入库结果单审核开始发送MQ通知对应单据!");
            if (sourceBillType == SgConstantsIF.BILL_TYPE_SALE ||
                    sourceBillType == SgConstantsIF.BILL_TYPE_SALE_REF ||
                    sourceBillType == SgConstantsIF.BILL_TYPE_PUR ||
                    sourceBillType == SgConstantsIF.BILL_TYPE_TRANSFER) {
                SgInResultMQRequest mqRequest = new SgInResultMQRequest();
                mqRequest.setId(id);
                mqRequest.setLoginUser(user);
                mqRequest.setIsOneClickLibrary(isOneClickLirary);
                String topic = pconf.getProperty(SgInConstants.R3_OMS_SG_OUT_MQ_TOPIC_KEY);
                String configName = SgInConstants.SG_PHY_RESULT_IN_MQ_CONFINGNAME;
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmssSSS");
                String msgKey = inResult.getBillNo() + dateFormat.format(new Date());
                String body = JSONObject.toJSONString(mqRequest);
                sendMsgToBill(sourceBillType, topic, configName, msgKey, body);
            } else {
                ValueHolderV14<SgPhyInResultBillSaveRequest> result = rpcService.getSgPhyInMQResult(null, inResult, inResultitems, resultImpItems, resultTeusItems, user);
                if (result.isOK()) {
                    SgPhyInResultBillSaveRequest mqRequest = result.getData();
                    if (sourceBillType == SgConstantsIF.BILL_TYPE_RETAIL_REF) {
                        SgReceiveBillSubmitResult data = receiveResult.getData();
                        if (data != null) {
                            mqRequest.setCpCStoreId(data.getCpCStoreId());
                            mqRequest.setCpCStoreEcode(data.getCpCStoreEcode());
                            mqRequest.setCpCStoreEname(data.getCpCStoreEname());
                        }
                    }
                    String topic = pconf.getProperty(SgInConstants.R3_OMS_SG_OUT_MQ_TOPIC_KEY);
                    String configName = SgInConstants.SG_PHY_RESULT_IN_MQ_CONFINGNAME;
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmssSSS");
                    String msgKey = inResult.getBillNo() + dateFormat.format(new Date());
                    String body = JSONObject.toJSONString(mqRequest);
                    sendMsgToBill(sourceBillType, topic, configName, msgKey, body);
                } else {
                    log.error("来源单据id[" + inResult.getSourceBillId() + "],获取mqbody失败!" + result.getMessage());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("来源单据id[" + inResult.getSourceBillId() + "],入库结果单审核发送MQ通知对应单据异常:" + e.getMessage(), e);
        }

        vh.setCode(ResultCode.SUCCESS);
        vh.setMessage("success");

        //推送es
        warehouseESUtils.pushESByInResult(id, false, false, null, resultMapper, null);

        if (log.isDebugEnabled()) {
            log.debug("入库结果单id[" + id + "]审核完成!spend time:" + (System.currentTimeMillis() - startTime));
        }
        return vh;
    }

    /***
     * mq通知对应单据
     */
    public void sendMsgToBill(Integer sourceBillType, String topic, String configName, String msgKey, String body) throws Exception {
        StorageMQAsyncUtils asyncUtils = ApplicationContextHandle.getBean(StorageMQAsyncUtils.class);
        switch (sourceBillType) {
            case SgConstantsIF.BILL_TYPE_RETAIL_REF:
                //零售退货单,则异步调用订单中心的【零售退货单更新入库结果服务】
                r3MqSendHelper.sendMessage(SgInConstants.SG_PHY_RESULT_IN_MQ_CONFINGNAME, body, mqConfig.getOmsMqTopic(), SgInConstants.SG_PHY_RESULT_IN_MQ_TAG, msgKey);
                break;
            case SgConstantsIF.BILL_TYPE_SALE:
                //销售单,则异步调用订单中心的【销售单入库服务】
                //2018-08-30 销售单增加延时发消息
                asyncUtils.sendDelayMessage(configName, body, topic, SgInConstants.SG_PHY_IN_RESULT_ADUIT_TAG_SALE, msgKey, 3000L, r3MqSendHelper);
                break;
            case SgConstantsIF.BILL_TYPE_SALE_REF:
                //销售退货单，则异步调用订单中心的【销售退货单入库服务】
                //2018-08-30 销售退货单增加延时发消息
                asyncUtils.sendDelayMessage(configName, body, topic, SgInConstants.SG_PHY_IN_RESULT_ADUIT_TAG_SALE_REF, msgKey, 3000L, r3MqSendHelper);
                break;
            case SgConstantsIF.BILL_TYPE_PUR:
                //采购单，则异步调用订单中心的【采购单入库服务】
                //2019-09-16 考虑到一键出入库，采购单修改为延时发消息
                asyncUtils.sendDelayMessage(configName, body, topic, SgInConstants.SG_PHY_IN_RESULT_ADUIT_TAG_PURCHASE, msgKey, 3000L, r3MqSendHelper);
                break;
            case SgConstantsIF.BILL_TYPE_TRANSFER:
                //调拨单，则异步调用订单中心的【调拨单单入库服务】
                //2018-08-30 调拨单增加延时发消息
                asyncUtils.sendDelayMessage(configName, body, topic, SgInConstants.SG_PHY_IN_RESULT_ADUIT_TAG_TRANSFER, msgKey, 3000L, r3MqSendHelper);
                break;
            default:
                throw new NDSException(Resources.getMessage("SourceBillType Exception:" + sourceBillType));
        }
    }

    /**
     * 更新入库通知单
     */
    public ValueHolderV14 updagteSgPhyInNotices(SgBPhyInResult result, List<SgBPhyInResultItem> list, User user,Long pickOrderId) {
        //入库通知单明细参数
        List<SgPhyInNoticesItemRequest> itemList = Lists.newArrayList();
        list.forEach(item -> {
            SgPhyInNoticesItemRequest noticesItem = new SgPhyInNoticesItemRequest();
            noticesItem.setId(item.getSgBPhyInNoticesItemId());
            noticesItem.setPsCSkuEcode(item.getPsCSkuEcode());
            noticesItem.setSgBPhyInNoticesId(result.getSgBPhyInNoticesId());
            noticesItem.setQtyIn(item.getQtyIn());
            itemList.add(noticesItem);
        });
        //判断是否是最后一次入库
        boolean isLast = result.getIsLast() == 0;
        SgPhyInNoticesBillPassRequest sgPhyInNoticesBillPassRequest = new SgPhyInNoticesBillPassRequest();
        sgPhyInNoticesBillPassRequest.setIsLast(isLast);
        sgPhyInNoticesBillPassRequest.setId(result.getSgBPhyInNoticesId());
        sgPhyInNoticesBillPassRequest.setItemList(itemList);
        sgPhyInNoticesBillPassRequest.setLoginUser(user);
        sgPhyInNoticesBillPassRequest.setInTime(result.getInTime());
        sgPhyInNoticesBillPassRequest.setPickOrderId(pickOrderId);

        ValueHolderV14 vh = sgPhyInNoticesPassService.updateInPass(sgPhyInNoticesBillPassRequest);

        return vh;
    }

    /**
     * 校验参数
     *
     * @param id
     */
    private ValueHolderV14 checkParams(Long id) {
        ValueHolderV14 checkResult = new ValueHolderV14();

        //1判断入库结果单的ID是否已经存在
        SgBPhyInResult resultBill = resultMapper.selectById(id);
        if (resultBill == null) {
            checkResult.setCode(ResultCode.FAIL);
            checkResult.setMessage("当前入库结果单不存在!");
            return checkResult;
        }
        //2 判断单据的状态是否为未审核
        if (SgInConstants.BILL_STATUS_UNCHECKED != resultBill.getBillStatus()) {
            checkResult.setCode(ResultCode.FAIL);
            checkResult.setMessage("当前单据已审核!");
            return checkResult;
        }

        //3 判断入库结果单【最后一次入库】的值
        if (resultBill.getIsLast().equals(SgConstants.IS_LAST_YES)) {
            //如果是，这判断是否存在未审核的入库结果单
            // 如果存在则不允许审核
            List<SgBPhyInResult> list = resultMapper.selectList(new QueryWrapper<SgBPhyInResult>().lambda()
                    .eq(SgBPhyInResult::getSgBPhyInNoticesId, resultBill.getSgBPhyInNoticesId())
                    .ne(SgBPhyInResult::getId, id)
                    .ne(SgBPhyInResult::getBillStatus, SgInConstants.BILL_STATUS_VOID));
            for (SgBPhyInResult inResult : list) {
                Integer status = inResult.getBillStatus();
                if (status == SgInConstants.BILL_STATUS_UNCHECKED) {
                    checkResult.setCode(ResultCode.FAIL);
                    checkResult.setMessage("当前单据存在未审核的结果单!");
                    return checkResult;
                }
            }
        }
        checkResult.setCode(ResultCode.SUCCESS);
        checkResult.setMessage("参数校验成功!");
        checkResult.setData(resultBill);
        return checkResult;
    }

    /**
     * 记录数据执行成功还是失败
     *
     * @param id
     * @return
     */
    private ReturnSgPhyInResult setResult(Long id, int code, String message) {
        ReturnSgPhyInResult result = new ReturnSgPhyInResult();
        result.setObjid(id);
        result.setTablename(SgConstants.SG_B_PHY_IN_RESULT.toUpperCase());
        result.setCode(code);
        result.setMessage(message);
        return result;
    }

}
