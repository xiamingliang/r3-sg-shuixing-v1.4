package com.jackrain.nea.sg.unpack.mapper;

import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.unpack.model.table.SgBTeusUnpackSkuItem;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SgBTeusUnpackSkuItemMapper extends ExtentionMapper<SgBTeusUnpackSkuItem> {
}