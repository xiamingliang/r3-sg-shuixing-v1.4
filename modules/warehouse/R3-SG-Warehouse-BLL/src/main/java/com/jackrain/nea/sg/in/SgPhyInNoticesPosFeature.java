package com.jackrain.nea.sg.in;

import com.jackrain.nea.sg.in.common.SgInNoticeConstants;
import com.jackrain.nea.sg.in.filter.SgPhyInNoticesPosAuditFilter;
import com.jackrain.nea.sg.in.filter.SgPhyInNoticesPosSaveFilter;
import com.jackrain.nea.sg.in.validate.SgPhyInNoticesPosAuditValidate;
import com.jackrain.nea.sg.in.validate.SgPhyInNoticesPosSaveValidate;
import com.jackrain.nea.sg.pack.common.SgTeusPackConstants;
import com.jackrain.nea.tableService.Feature;
import com.jackrain.nea.tableService.constants.Constants;
import com.jackrain.nea.tableService.feature.FeatureAnnotation;
import com.jackrain.nea.util.ApplicationContextHandle;

/**
 * @author zhoulinsheng
 * @since 2019/11/27 14:02
 */
@FeatureAnnotation(value = "SgPhyInNoticesPosFeature", description = "门店入库单Feature", isSystem = true)
public class SgPhyInNoticesPosFeature extends Feature {

    @Override
    protected void initialization() {

        // 保存
        SgPhyInNoticesPosSaveValidate inNoticesPosSaveValidate = ApplicationContextHandle.getBean(SgPhyInNoticesPosSaveValidate.class);
        addValidator(inNoticesPosSaveValidate, (tableName, actionName) -> tableName.equalsIgnoreCase(SgInNoticeConstants.SG_B_PHY_IN_NOTICES_POS)
                && (actionName.equals(Constants.ACTION_SAVE) || actionName.equals(Constants.ACTION_ADD)));

        SgPhyInNoticesPosSaveFilter inNoticesPosSaveFilter = ApplicationContextHandle.getBean(SgPhyInNoticesPosSaveFilter.class);
        addFilter(inNoticesPosSaveFilter, (tableName, actionName) -> tableName.equalsIgnoreCase(SgInNoticeConstants.SG_B_PHY_IN_NOTICES_POS)
                && (actionName.equals(Constants.ACTION_SAVE) || actionName.equals(Constants.ACTION_ADD)));

        // 审核
        SgPhyInNoticesPosAuditValidate inNoticesPosAuditValidate = ApplicationContextHandle.getBean(SgPhyInNoticesPosAuditValidate.class);
        addValidator(inNoticesPosAuditValidate, (tableName, actionName) -> tableName.equalsIgnoreCase(SgInNoticeConstants.SG_B_PHY_IN_NOTICES_POS)
                && (actionName.equals(Constants.ACTION_SUBMIT)));

        SgPhyInNoticesPosAuditFilter inNoticesPosAuditFilter = ApplicationContextHandle.getBean(SgPhyInNoticesPosAuditFilter.class);
        addFilter(inNoticesPosAuditFilter, (tableName, actionName) -> tableName.equalsIgnoreCase(SgInNoticeConstants.SG_B_PHY_IN_NOTICES_POS)
                && (actionName.equals(Constants.ACTION_SUBMIT)));

    }
}
