package com.jackrain.nea.sg.inv.controller;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.sg.inv.api.ScBInventorySaveCmd;
import com.jackrain.nea.sg.inv.api.ScBinventoryAddLoadCmd;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.face.impl.UserImpl;
import com.jackrain.nea.web.query.DefaultWebEvent;
import com.jackrain.nea.web.query.QuerySessionImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;


/**
 * @author 周琳胜
 * @since 2019-11-14
 * create at : 2019-11-14 13:03
 */
@RestController
@Slf4j
@Api(value = "ScBInventoryCtrl", description = "盘点单")
public class ScBInventoryCtrl {



    @ApiOperation(value = "盘点单-保存")
    @PostMapping(path = "/p/cs/sg/ScBInventory/save")
    public JSONObject scBInventorySave(HttpServletRequest request, @RequestParam(value = "param") String param) {
        ValueHolder result=null;
            Object o = ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                    ScBInventorySaveCmd.class.getName(), "sg", "1.0");
//            QuerySessionImpl querySession = new QuerySessionImpl(getUser());
        QuerySessionImpl querySession = new QuerySessionImpl(request);
            JSONObject paramJo = JSON.parseObject(param);
            DefaultWebEvent event = new DefaultWebEvent("scBInventorySave", request, false);
            event.put("param", paramJo);
            querySession.setEvent(event);
            result = ((ScBInventorySaveCmd) o).execute(querySession);

        return result.toJSONObject();
    }

    @ApiOperation(value = "初始化盘点单")
    @PostMapping(path = "/p/cs/binventoryaddload")
    public JSONObject binventoryaddload(HttpServletRequest request, @RequestParam(value = "param", required = false) String param) {
//        QuerySessionImpl querySession = new QuerySessionImpl(getUser());
        QuerySessionImpl querySession = new QuerySessionImpl(request);
        DefaultWebEvent event = new DefaultWebEvent("binventoryaddload", request, false);
        event.put("param", JSON.parseObject(param));
        querySession.setEvent(event);
        Object o = ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                ScBinventoryAddLoadCmd.class.getName(), "sg", "1.0");
        ValueHolder result;

        try {
            result = ((ScBinventoryAddLoadCmd) o).execute(querySession);
            return result.toJSONObject();
        } catch (Exception e) {
            result = new ValueHolder();
            result.put("code", -1);
            result.put("message", e.getMessage());
            return result.toJSONObject();
        }


    }

    /***
     * 获取用户
     * @return
     */
    private User getUser() {
        UserImpl user = new UserImpl();
        user.setClientId(27);
        user.setOrgId(37);
        user.setId(100);
        user.setName("root");
        user.setEname("测试用户");
        return user;
    }

}
