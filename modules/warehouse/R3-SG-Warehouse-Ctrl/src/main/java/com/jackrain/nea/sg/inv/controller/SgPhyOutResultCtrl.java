package com.jackrain.nea.sg.inv.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.model.result.SgR3BaseResult;
import com.jackrain.nea.sg.out.api.*;
import com.jackrain.nea.sg.out.model.request.*;
import com.jackrain.nea.sg.out.model.result.SgOutResultSendMsgResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.face.impl.UserImpl;
import com.jackrain.nea.web.query.DefaultWebEvent;
import com.jackrain.nea.web.query.QuerySessionImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author leexh
 * @since 2019/4/25 11:02
 */
@RestController
@Slf4j
@Api(value = "sg_b_phy_out_result", description = "出库结果单")
public class SgPhyOutResultCtrl {

    @ApiOperation(value = "出库结果单保存")
    @PostMapping(path = "/p/cs/saveSgPhyOutResult")
    public JSONObject saveSgPhyOutResult(HttpServletRequest request, @RequestParam(value = "param") String param) {

        if (log.isDebugEnabled()) {
            log.debug("start SgPhyOutResultCtrl.saveSgPhyOutResult. ReceiveParams=" + param + ";");
        }

        Object o = ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                SgPhyOutResultSaveCmd.class.getName(), "sg", "1.0");

        ValueHolderV14<SgR3BaseResult> result;
        try {
            if (o == null) {
                result = new ValueHolderV14();
                result.setCode(ResultCode.FAIL);
                result.setMessage("未获取到对应的bean");
            } else {

                SgPhyOutResultBillSaveRequest bill = new SgPhyOutResultBillSaveRequest();
                bill.setObjId(-1l);
                bill.setOutResultRequest(getOutResultRequest());
                bill.setOutResultItemRequests(getItems());
//                bill.setLoginUser(this.getUser());
                result = ((SgPhyOutResultSaveCmd) o).saveSgPhyOutResult(bill);
            }
        } catch (Exception e) {
            result = new ValueHolderV14();
            result.setCode(ResultCode.FAIL);
            result.setMessage("error--" + e.getMessage());
            return result.toJSONObject();
        }
        return result.toJSONObject();
    }

    @ApiOperation(value = "出库结果单保存(页面)")
    @PostMapping(path = "/p/cs/saveSgPhyOutResultR3")
    public JSONObject saveSgPhyOutResultR3(HttpServletRequest request, @RequestParam(value = "param") String param) {

        if (log.isDebugEnabled()) {
            log.debug("start SgPhyOutResultCtrl.saveSgPhyOutResultR3. ReceiveParams=" + param + ";");
        }

        Object o = ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                SgPhyOutResultSaveR3Cmd.class.getName(), "sg", "1.0");

        ValueHolder result;
        try {
            if (o == null) {
                result = new ValueHolder();
                result.put("code", ResultCode.FAIL);
                result.put("message", "未获取到对应的bean");
            } else {
                QuerySessionImpl querySession = new QuerySessionImpl(getUser());
                JSONObject paramJo = JSON.parseObject(param);
                DefaultWebEvent event = new DefaultWebEvent("saveSgPhyOutResultR3", request, false);
                event.put("param", paramJo);
                querySession.setEvent(event);
                result = ((SgPhyOutResultSaveR3Cmd) o).execute(querySession);
            }
        } catch (Exception e) {
            result = new ValueHolder();
            result.put("code", ResultCode.FAIL);
            result.put("message", "error--" + e.getMessage());
            return result.toJSONObject();
        }
        return result.toJSONObject();
    }

    @ApiOperation(value = "出库结果单审核")
    @PostMapping(path = "/p/cs/auditSgPhyOutResult")
    public JSONObject auditSgPhyOutResult(HttpServletRequest request, @RequestParam(value = "param") String param) {

        if (log.isDebugEnabled()) {
            log.debug("start SgPhyOutResultCtrl.auditSgPhyOutResult. ReceiveParams=" + param + ";");
        }

        Object o = ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                SgPhyOutResultAuditCmd.class.getName(), "sg", "1.0");

        ValueHolderV14<SgR3BaseResult> result;
        try {
            if (o == null) {
                result = new ValueHolderV14();
                result.setCode(ResultCode.FAIL);
                result.setMessage("未获取到对应的bean");
            } else {

                SgPhyOutResultBillAuditRequest bill = new SgPhyOutResultBillAuditRequest();
                bill.setOutResultRequests(this.getOutResultRequests());
//                bill.setLoginUser(this.getUser());
                result = ((SgPhyOutResultAuditCmd) o).auditSgPhyOutResult(bill);
            }
        } catch (Exception e) {
            result = new ValueHolderV14();
            result.setCode(ResultCode.FAIL);
            result.setMessage("error--" + e.getMessage());
            return result.toJSONObject();
        }
        return result.toJSONObject();
    }

    @ApiOperation(value = "出库结果单审核(页面)")
    @PostMapping(path = "/p/cs/auditSgPhyOutResultR3")
    public JSONObject auditSgPhyOutResultR3(HttpServletRequest request, @RequestParam(value = "param") String param) {

        if (log.isDebugEnabled()) {
            log.debug("start SgPhyOutResultCtrl.auditSgPhyOutResultR3. ReceiveParams=" + param + ";");
        }

        QuerySessionImpl querySession = new QuerySessionImpl(getUser());
//        QuerySessionImpl querySession = new QuerySessionImpl(request);
        JSONObject paramJo = JSON.parseObject(param);
        DefaultWebEvent event = new DefaultWebEvent("auditSgPhyOutResultR3", request, false);
        event.put("param", paramJo);
        querySession.setEvent(event);

        Object o = ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                SgPhyOutResultAuditR3Cmd.class.getName(), "sg", "1.0");

        ValueHolder result;
        try {
            if (o == null) {
                result = new ValueHolder();
                result.put("code", ResultCode.FAIL);
                result.put("message", "未获取到对应的bean");
            } else {
                result = ((SgPhyOutResultAuditR3Cmd) o).execute(querySession);
            }
        } catch (Exception e) {
            result = new ValueHolder();
            result.put("code", ResultCode.FAIL);
            result.put("messafe", "error--" + e.getMessage());
            return result.toJSONObject();
        }
        return result.toJSONObject();
    }


    @ApiOperation(value = "新增并审核出库结果单")
    @PostMapping(path = "/p/cs/saveAndAuditSgPhyOutResult")
    public JSONObject saveAndAuditSgPhyOutResult(@RequestBody SgPhyOutResultBillSaveRequest param) {

        if (log.isDebugEnabled()) {
            log.debug("start SgPhyOutResultCtrl.saveAndAuditSgPhyOutResult. ReceiveParams=" + param + ";");
        }
        SgPhyOutResultSaveAndAuditCmd o = (SgPhyOutResultSaveAndAuditCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                SgPhyOutResultSaveAndAuditCmd.class.getName(), "sg", "1.0");

        ValueHolderV14<SgR3BaseResult> result;
        try {
            param.setLoginUser(this.getUser());
            result = o.saveOutResultAndAudit(param);
        } catch (Exception e) {
            result = new ValueHolderV14<>();
            result.setCode(ResultCode.FAIL);
            result.setMessage("error--" + e.getMessage());
            return result.toJSONObject();
        }
        return result.toJSONObject();
    }

    @ApiOperation(value = "出库结果单审核-查询mqbody")
    @PostMapping(path = "/p/cs/queryOutMQBody")
    public JSONObject queryOutMQBody(HttpServletRequest request, @RequestParam(value = "objid") Long objid) {

        if (log.isDebugEnabled()) {
            log.debug("start SgPhyOutResultCtrl.queryOutMQBody. ReceiveParams=" + objid + ";");
        }
        ValueHolderV14 result;
        try {
            SgPhyOutQueryCmd queryCmd = (SgPhyOutQueryCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                    SgPhyOutQueryCmd.class.getName(), "sg", "1.0");
            List<Long> ids = Lists.newArrayList();
            ids.add(objid);
            result = queryCmd.querySgPhyOutResultMQBody(ids, getUser());
        } catch (Exception e) {
            result = new ValueHolderV14();
            result.setCode(ResultCode.FAIL);
            result.setMessage(e.getMessage());
        }
        return result.toJSONObject();
    }


    @ApiOperation(value = "出库结果单审核-JITX查询mqbody")
    @PostMapping(path = "/p/cs/queryOutJITXMQBody")
    public JSONObject queryOutJITXMQBody(@RequestBody List<Long> ids) {

        if (log.isDebugEnabled()) {
            log.debug("start SgPhyOutResultCtrl.queryOutJITXMQBody. ReceiveParams=" + ids + ";");
        }
        ValueHolderV14<List<SgOutResultSendMsgResult>> result;
        try {
            SgPhyOutQueryCmd queryCmd = (SgPhyOutQueryCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                    SgPhyOutQueryCmd.class.getName(), "sg", "1.0");
            result = queryCmd.querySgPhyOutResultJITXMQBody(ids, getUser());
        } catch (Exception e) {
            result = new ValueHolderV14<>();
            result.setCode(ResultCode.FAIL);
            result.setMessage(e.getMessage());
        }
        return result.toJSONObject();
    }

    @ApiOperation(value = "一键出库")
    @PostMapping(path = "/p/cs/sg/oneClickOutLibrary")
    public JSONObject oneClickOutLibrary(@RequestBody SgPhyOneClickOutLibraryRequest request) {
        User user = getUser();
        request.setLoginUser(user);
        SgPhyOneClickOutLibraryCmd libraryCmd = (SgPhyOneClickOutLibraryCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                SgPhyOneClickOutLibraryCmd.class.getName(), "sg", "1.0");
        ValueHolderV14 result;
        try {
            log.debug("开始一键出库");
            result = libraryCmd.oneClickOutLibrary(request);
        } catch (Exception e) {
            result = new ValueHolderV14();
            result.setCode(ResultCode.FAIL);
            result.setMessage(e.getMessage());
        }
        return result.toJSONObject();
    }


    /**
     * 测试用
     *
     * @return
     */
    public User getUser() {
        UserImpl user = new UserImpl();
        user.setOrgId(27);
        user.setClientId(37);
        user.setId(1000);
        user.setName("宋冬野2s");
        user.setEname("宋冬野se");
        return user;
    }

    public SgPhyOutResultSaveRequest getOutResultRequest() {

        SgPhyOutResultSaveRequest resultSaveRequest = new SgPhyOutResultSaveRequest();
        resultSaveRequest.setSgBPhyOutNoticesId(6l);
        resultSaveRequest.setId(12l);
        return resultSaveRequest;
    }

    public List<SgPhyOutResultSaveRequest> getOutResultRequests() {

        List<SgPhyOutResultSaveRequest> list = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            SgPhyOutResultSaveRequest resultSaveRequest = new SgPhyOutResultSaveRequest();
            resultSaveRequest.setId((long) 100 + i);
            list.add(resultSaveRequest);
        }
        return list;
    }

    public List<SgPhyOutResultItemSaveRequest> getItems() {

        List<SgPhyOutResultItemSaveRequest> list = new ArrayList<>();
        for (int i = 0; i < 2; i++) {
            SgPhyOutResultItemSaveRequest request = new SgPhyOutResultItemSaveRequest();
            request.setQty(new BigDecimal(1 + i));
            request.setPriceList(new BigDecimal(100));
            request.setPsCProId((long) 10 + i);
            request.setPsCSkuId((long) 100 + i);
            request.setPsCSpec1Id((long) 10 + i);
            request.setPsCSpec2Id((long) 20 + i);
            list.add(request);
        }

        return list;

    }
}
