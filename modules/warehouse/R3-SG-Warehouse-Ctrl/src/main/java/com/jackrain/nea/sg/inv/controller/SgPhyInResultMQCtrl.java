package com.jackrain.nea.sg.inv.controller;


import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.in.api.SgPhyInNoticesSendMQCmd;
import com.jackrain.nea.sg.in.api.SgPhyInResultSelectCmd;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.face.impl.UserImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


/**
 * @author jiang.cj
 * @since 2019-05-07
 * create at : 2019-05-07
 */
@RestController
@Slf4j
@Api(value = "SgPhyInResultMQCtrl", description = "MQ调入库结果单")
public class SgPhyInResultMQCtrl {

    @ApiOperation(value = "通过MQ新增入库结果单")
    @PostMapping(path = "/p/cs/sg/saveSgPhyInResultByMQ")
    public ValueHolderV14 saveSgPhyInResultByMQ() {
        ValueHolderV14 result = null;
        Object o = ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                SgPhyInNoticesSendMQCmd.class.getName(), "sg", "1.0");
        try {
            List<Long> list = Lists.newArrayList();
            list.add(4l);
            list.add(12l);
            list.add(13l);
            list.add(16l);
            list.add(17l);
            list.add(18l);
            list.add(19l);
            result = ((SgPhyInNoticesSendMQCmd) o).saveSgBPhyInResultByMq(list);
            return result;
        } catch (Exception e) {
            result.setCode(ResultCode.FAIL);
            result.setMessage(e.toString());
            return result;
        }
    }


    @ApiOperation(value = "入库结果单审核-查询mqbody")
    @PostMapping(path = "/p/cs/queryInMQBody")
    public JSONObject queryInMQBody(HttpServletRequest request, @RequestParam(value = "objid") Long objid) {

        if (log.isDebugEnabled()) {
            log.debug("start SgPhyInResultMQCtrl.queryInMQBody. ReceiveParams=" + objid + ";");
        }
        ValueHolderV14 result;
        try {
            SgPhyInResultSelectCmd queryCmd = (SgPhyInResultSelectCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                    SgPhyInResultSelectCmd.class.getName(), "sg", "1.0");
            List<Long> ids = Lists.newArrayList();
            ids.add(objid);
            result = queryCmd.querySgPhyInResultMQBody(ids, getUser());
        } catch (Exception e) {
            result = new ValueHolderV14();
            result.setCode(ResultCode.FAIL);
            result.setMessage(e.getMessage());
        }
        return result.toJSONObject();
    }

    /**
     * 测试用
     *
     * @return
     */
    public User getUser() {
        UserImpl user = new UserImpl();
        user.setOrgId(27);
        user.setClientId(37);
        user.setId(1000);
        user.setName("宋冬野2s");
        user.setEname("宋冬野se");
        return user;
    }
}
