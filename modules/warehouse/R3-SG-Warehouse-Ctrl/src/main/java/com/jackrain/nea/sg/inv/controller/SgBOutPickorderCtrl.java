package com.jackrain.nea.sg.inv.controller;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.out.api.SgBOutPickorderItemQueryCmd;
import com.jackrain.nea.sg.out.api.SgBOutPickorderVoidCmd;
import com.jackrain.nea.sg.out.model.request.SgBOutPickorderRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.face.impl.UserImpl;
import com.jackrain.nea.web.query.DefaultWebEvent;
import com.jackrain.nea.web.query.QuerySessionImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @author: heliu
 * @since: 2019/11/18
 * create at : 2019/11/18 13:49
 */
@RestController
@Slf4j
@Api(value = "SgBOutPickorderCtrl", description = "出库拣货单测试Ctrl")
public class SgBOutPickorderCtrl {

    @ApiOperation(value = "出库拣货单自定义查询接口")
    @PostMapping(path = "/p/cs/sg/v1/warehouse/sgBOutPickorder/querySgBOutPickorder")
    public JSONObject querySgBOutPickorder(HttpServletRequest request, @RequestParam(value = "objid") Long objid) {

        SgBOutPickorderRequest sgBOutPickorderRequest = new SgBOutPickorderRequest();
        User user = getRootUser();
        sgBOutPickorderRequest.setId(objid);
        sgBOutPickorderRequest.setLoginUser(user);
        SgBOutPickorderItemQueryCmd sgBInPickorderItemQueryCmd = (SgBOutPickorderItemQueryCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                SgBOutPickorderItemQueryCmd.class.getName(), "sg", "1.0");
        ValueHolderV14 result;
        try {
            result = sgBInPickorderItemQueryCmd.querySgBOutPickorderItem(sgBOutPickorderRequest);
        } catch (Exception e) {
            result = new ValueHolderV14();
            result.setCode(ResultCode.FAIL);
            result.setMessage(e.getMessage());
        }
        return result.toJSONObject();
    }

    @ApiOperation(value = "出库拣货单作废")
    @PostMapping(path = "/sg/warehouse/sgBOutPickorder/voidSgBOutPickorder")
    public JSONObject voidSgBOutPickorder(HttpServletRequest request, @RequestBody JSONObject param) {
        //TODO 測試用ctr
        if (log.isDebugEnabled()) {
            log.debug("Strat SgBInPickorderCtrl.voidSgBOutPickorder. ReceiveParams=" + param + ";");
        }
        ValueHolder result;
        User user = getRootUser();
        try {
            QuerySessionImpl querySession = new QuerySessionImpl(user);
            DefaultWebEvent event = new DefaultWebEvent("voidSgBOutPickorder", request, false);
            event.put("param", param);
            querySession.setEvent(event);
            SgBOutPickorderVoidCmd sgBOutPickorderVoidCmd = (SgBOutPickorderVoidCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                    SgBOutPickorderVoidCmd.class.getName(), "sg", "1.0");
            result = sgBOutPickorderVoidCmd.execute(querySession);
        } catch (Exception e) {
            throw new NDSException(Resources.getMessage(e.getMessage(), user.getLocale()));
        }
        if (log.isDebugEnabled()) {
            log.debug("Finish SgBInPickorderCtrl.voidSgBOutPickorder. ReturnResult=" + result);
        }
        return result.toJSONObject();
    }

    private User getRootUser() {
        UserImpl user = new UserImpl();
        user.setId(893);
        user.setName("Fire BALL-V1");
        user.setEname("Pokemon-mapper");
        user.setActive(true);
        user.setClientId(37);
        user.setOrgId(27);
        user.setIsAdmin(2);
        user.setIsDev(2);
        return user;
    }
}