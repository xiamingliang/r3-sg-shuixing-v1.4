package com.jackrain.nea.sg.inv.controller;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.phyadjust.api.*;
import com.jackrain.nea.sg.phyadjust.model.request.SgPhyAdjustBillAuditRequest;
import com.jackrain.nea.sg.phyadjust.model.request.SgPhyAdjustBillQueryRequest;
import com.jackrain.nea.sg.phyadjust.model.request.SgPhyAdjustBillSaveRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.face.impl.UserImpl;
import com.jackrain.nea.web.query.DefaultWebEvent;
import com.jackrain.nea.web.query.QuerySession;
import com.jackrain.nea.web.query.QuerySessionImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

/**
 * @author csy
 */
@RestController
@Slf4j
@Api(value = "SgPhyAdjustCtrl", description = "库存整单测试Ctrl")
public class SgPhyAdjustCtrl {


    @ApiOperation(value = "库存调整单 新增/更新 页面接口")
    @PostMapping(path = "/sg/warehouse/phyAdjust/saveR3")
    public JSONObject saveR3(HttpServletRequest request, @RequestBody JSONObject obj) {
        SgPhyAdjustSaveR3Cmd saveCmd = (SgPhyAdjustSaveR3Cmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                SgPhyAdjustSaveR3Cmd.class.getName(), "sg", "1.0");
        ValueHolder result;
        if (log.isDebugEnabled()) {
            log.debug("Strat SgPhyAdjustCtrl.saveR3. ReceiveParams=" + obj.toJSONString() + ";");
        }
        try {
            QuerySession session = new QuerySessionImpl(getRootUser());
            DefaultWebEvent event = new DefaultWebEvent("audit", new HashMap(16));
            event.put("param", obj);
            session.setEvent(event);
            result = saveCmd.execute(session);
        } catch (Exception e) {
            e.printStackTrace();
            result = new ValueHolder();
            result.put("code", ResultCode.FAIL);
            result.put("message", e.getMessage());
        }
        return result.toJSONObject();
    }


    @ApiOperation(value = "库存调整单 新增/更新")
    @PostMapping(path = "/sg/warehouse/phyAdjust/save")
    public JSONObject save(@RequestBody SgPhyAdjustBillSaveRequest saveRequest) {
        User user = getRootUser();
        saveRequest.setLoginUser(user);
        SgPhyAdjustSaveCmd saveCmd = (SgPhyAdjustSaveCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                SgPhyAdjustSaveCmd.class.getName(), "sg", "1.0");
        ValueHolderV14 result;
        try {
            result = saveCmd.save(saveRequest);
        } catch (Exception e) {
            result = new ValueHolderV14();
            result.setCode(ResultCode.FAIL);
            result.setMessage(e.getMessage());
        }
        return result.toJSONObject();
    }

    @ApiOperation(value = "库存调整单 审核")
    @PostMapping(path = "/sg/warehouse/phyAdjust/audit")
    public JSONObject audit(@RequestBody SgPhyAdjustBillAuditRequest auditRequest) {
        User user = getRootUser();
        auditRequest.setUser(user);
        SgPhyAdjustAuditCmd auditCmd = (SgPhyAdjustAuditCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                SgPhyAdjustAuditCmd.class.getName(), "sg", "1.0");
        ValueHolderV14 result;
        try {
            if (log.isDebugEnabled()) {
                log.debug("开始审核");
            }
            result = auditCmd.audit(auditRequest);
        } catch (Exception e) {
            result = new ValueHolderV14();
            result.setCode(ResultCode.FAIL);
            result.setMessage(e.getMessage());
        }
        return result.toJSONObject();
    }

    @ApiOperation(value = "库存调整单 审核 - 页面")
    @PostMapping(path = "/sg/warehouse/phyAdjust/auditR3")
    public JSONObject auditR3(@RequestParam(name = "objid") Long objid) {
        SgPhyAdjustAuditR3Cmd auditCmd = (SgPhyAdjustAuditR3Cmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                SgPhyAdjustAuditR3Cmd.class.getName(), "sg", "1.0");
        ValueHolder result;
        try {
            log.debug("开始审核");
            QuerySession session = new QuerySessionImpl(getRootUser());
            DefaultWebEvent event = new DefaultWebEvent("audit", new HashMap(16));
            JSONObject param = new JSONObject();
            param.put("objid", objid);
            event.put("param", param);
            session.setEvent(event);
            result = auditCmd.execute(session);
        } catch (Exception e) {
            result = new ValueHolder();
            result.put("code", ResultCode.SUCCESS);
            result.put("message", e.getMessage());
        }
        return result.toJSONObject();
    }


    @ApiOperation(value = "库存调整单 保存并审核")
    @PostMapping(path = "/sg/warehouse/phyAdjust/saveAndAudit")
    public JSONObject saveAndAudit(@RequestBody SgPhyAdjustBillSaveRequest saveRequest) {
        User user = getRootUser();
        saveRequest.setLoginUser(user);
        SgPhyAdjSaveAndAuditCmd auditCmd = (SgPhyAdjSaveAndAuditCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                SgPhyAdjSaveAndAuditCmd.class.getName(), "sg", "1.0");
        ValueHolderV14 result;
        try {
            log.debug("开始保存并审核");
            result = auditCmd.saveAndAuditAdj(saveRequest);
        } catch (Exception e) {
            result = new ValueHolderV14();
            result.setCode(ResultCode.FAIL);
            result.setMessage(e.getMessage());
        }
        return result.toJSONObject();
    }

    @ApiOperation(value = "库存调整单 查询")
    @PostMapping(path = "/sg/warehouse/phyAdjust/query")
    public JSONObject audit(@RequestBody SgPhyAdjustBillQueryRequest queryRequest) {
        SgPhyAdjustQueryCmd queryCmd = (SgPhyAdjustQueryCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                SgPhyAdjustQueryCmd.class.getName(), "sg", "1.0");
        ValueHolderV14 result;
        try {
            log.debug("开始查询");
            result = queryCmd.queryPhyAdj(queryRequest);
        } catch (Exception e) {
            result = new ValueHolderV14();
            result.setCode(ResultCode.FAIL);
            result.setMessage(e.getMessage());
        }
        return result.toJSONObject();
    }


    private User getRootUser() {
        UserImpl user = new UserImpl();
        user.setId(893);
        user.setName("Fire BALL-V1");
        user.setEname("Pokemon-mapper");
        user.setActive(true);
        user.setClientId(37);
        user.setOrgId(27);
        user.setIsAdmin(2);
        user.setIsDev(2);
        return user;
    }
}
