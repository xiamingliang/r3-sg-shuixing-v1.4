package com.jackrain.nea.sg.inv.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.in.api.*;
import com.jackrain.nea.sg.in.model.request.SgPhyInNoticesBillPassRequest;
import com.jackrain.nea.sg.in.model.request.SgPhyInNoticesBillSaveRequest;
import com.jackrain.nea.sg.in.model.request.SgPhyInNoticesItemRequest;
import com.jackrain.nea.sg.in.model.request.SgPhyInNoticesVoidReqeust;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.face.impl.UserImpl;
import com.jackrain.nea.web.query.DefaultWebEvent;
import com.jackrain.nea.web.query.QuerySessionImpl;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author: ChenChen
 * @Description:
 * @Date: Create in 13:18 2019/4/24
 */
@RestController
@Slf4j
public class SgPhyInNoticesCtrl {

    @Reference(version = "1.0", group = "sg")
    private SgPhyInNoticesSaveByReturnCmd sgPhyInNoticesSaveByReturnCmd;
    @Reference(version = "1.0", group = "sg")
    private SgPhyInNoticesPassCmd sgPhyInNoticesPassCmd;

    @Reference(version = "1.0", group = "sg")
    private SgPhyInNoticeVoidCmd sgPhyInNoticeVoidCmd;
    @Reference(version = "1.0", group = "sg")
    private SgPhyInNoticesSaveCmd sgPhyInNoticesSaveCmd;

    @RequestMapping(path = "/p/c/notice/add")
    public ValueHolderV14 noticeAdd(HttpServletRequest request,
                                    @RequestParam(value = "param") String param) {
        List<SgPhyInNoticesBillSaveRequest> requests = JSONObject.parseArray(param, SgPhyInNoticesBillSaveRequest.class);
        for (SgPhyInNoticesBillSaveRequest model : requests) {
            model.setLoginUser(getUser());
        }
        return sgPhyInNoticesSaveCmd.saveSgBPhyInNotices(requests);
    }

    @RequestMapping(path = "/p/c/notice/pass")
    public ValueHolderV14 noticePass(HttpServletRequest request) {
        SgPhyInNoticesBillPassRequest passRequest = new SgPhyInNoticesBillPassRequest();
        List<SgPhyInNoticesItemRequest> itemRequestList = new ArrayList<>();
        SgPhyInNoticesItemRequest itemRequest = new SgPhyInNoticesItemRequest();
        passRequest.setId((long) 18);
        itemRequest.setId((long) 10);
        itemRequest.setQtyIn(BigDecimal.ONE);
        itemRequestList.add(itemRequest);
        /*        itemRequestList.add(itemRequest);*/
        passRequest.setItemList(itemRequestList);
        passRequest.setLoginUser(getUser());
        return sgPhyInNoticesPassCmd.passSgBPhyInNotices(passRequest);
    }

    @RequestMapping(path = "/p/c/notice/void")
    public ValueHolderV14 noticeVoid(HttpServletRequest request) {
        SgPhyInNoticesVoidReqeust reqeust = new SgPhyInNoticesVoidReqeust();
        reqeust.setSourceBillType(1);
        reqeust.setSourceBillId((long) 123);
        reqeust.setLoginUser(getUser());
        return sgPhyInNoticeVoidCmd.voidSgPhyInNotice(reqeust);
    }


    @ApiOperation(value = "入库通知单从wms撤回")
    @PostMapping(path = "/p/cs/recallWMS")
    public JSONObject recallWMS(HttpServletRequest request, @RequestParam(value = "param") String param) {

        if (log.isDebugEnabled()) {
            log.debug("start SgPhyInNoticesCtrl.recallWMS. ReceiveParams=" + param + ";");
        }

        ValueHolder result;
        try {
            QuerySessionImpl querySession = new QuerySessionImpl(getUser());
//        QuerySessionImpl querySession = new QuerySessionImpl(request);
            JSONObject paramJo = JSON.parseObject(param);
            DefaultWebEvent event = new DefaultWebEvent("recallWMS", request, false);
            event.put("param", paramJo);
            querySession.setEvent(event);
            SgPhyInNoticesWMSBackCmd recallWmsCmd = (SgPhyInNoticesWMSBackCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                    SgPhyInNoticesWMSBackCmd.class.getName(), "sg", "1.0");
            result = recallWmsCmd.execute(querySession);
        } catch (Exception e) {
            result = new ValueHolder();
            result.put("code", ResultCode.FAIL);
            result.put("message", "error--" + e);
            return result.toJSONObject();
        }
        return result.toJSONObject();
    }


    private UserImpl getUser() {
        UserImpl user = new UserImpl();
        user.setClientId(27);
        user.setOrgId(37);
        user.setId(100);
        user.setName("root");
        user.setEname("测试用户");
        return user;
    }
}
