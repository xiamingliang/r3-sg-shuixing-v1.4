package com.jackrain.nea.sg.inv.controller;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.in.api.SgPhyInResultAuditCmd;
import com.jackrain.nea.sg.in.api.SgPhyInResultAuditR3Cmd;
import com.jackrain.nea.sg.in.api.SgPhyInResultSaveCmd;
import com.jackrain.nea.sg.in.api.SgPhyOneClickLibraryCmd;
import com.jackrain.nea.sg.in.model.request.*;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.face.impl.UserImpl;
import com.jackrain.nea.web.query.DefaultWebEvent;
import com.jackrain.nea.web.query.QuerySessionImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * @author jiang.cj
 * @since 2019-4-24
 * create at : 2019-4-24 13:03
 */
@RestController
@Slf4j
@Api(value = "SgPhyInResultCtrl", description = "入库结果单")
public class SgPhyInResultCtrl {

    @ApiOperation(value = "入库结果单")
    @PostMapping(path = "/p/cs/sg/saveSgPhyInResult")
    public ValueHolderV14 saveSgPhyInResult() {
        ValueHolderV14 result = null;
        Object o = ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                SgPhyInResultSaveCmd.class.getName(), "sg", "1.0");
        try {
            SgPhyInResultBillSaveRequest model = getQueryTotalModel();
            result = ((SgPhyInResultSaveCmd) o).saveSgBPhyInResult(model);
            return result;
        } catch (Exception e) {
            result.setCode(ResultCode.FAIL);
            result.setMessage(e.toString());
            return result;
        }
    }


    @ApiOperation(value = "入库结果单审核")
    @PostMapping(path = "/p/cs/sg/aduitSgPhyInResult")
    public ValueHolderV14 aduitSgPhyInResult() {
        ValueHolderV14 result = null;
        Object o = ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                SgPhyInResultAuditCmd.class.getName(), "sg", "1.0");
        try {
            SgPhyInResultBillAuditRequest model = getQueryAuditModel();
            result = ((SgPhyInResultAuditCmd) o).auditSgPhyInResult(model);
            return result;
        } catch (Exception e) {
            result.setCode(ResultCode.FAIL);
            result.setMessage(e.toString());
            return result;
        }
    }

    @ApiOperation(value = "入库结果单审核-页面入口")
    @PostMapping(path = "/p/cs/sg/aduitSgPhyInResultR3")
    public JSONObject aduitSgPhyInResultR3(HttpServletRequest request, @RequestParam(value = "param") String param) {
        ValueHolder result = null;
        try {
            Object o = ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                    SgPhyInResultAuditR3Cmd.class.getName(), "sg", "1.0");
            QuerySessionImpl querySession = new QuerySessionImpl(getUser());
            JSONObject paramJo = JSON.parseObject(param);
            DefaultWebEvent event = new DefaultWebEvent("aduitSgPhyInResultR3", request, false);
            event.put("param", paramJo);
            querySession.setEvent(event);
            result = ((SgPhyInResultAuditR3Cmd) o).execute(querySession);
        } catch (Exception e) {
            result.put("code", ResultCode.FAIL);
            result.put("message", e.toString());
        }
        return result.toJSONObject();
    }

    @ApiOperation(value = "一键入库")
    @PostMapping(path = "/p/cs/sg/oneClickLibrary")
    public JSONObject oneClickLibrary(@RequestBody SgPhyOneClickLibraryRequest request) {
        User user = getUser();
        request.setLoginUser(user);
        SgPhyOneClickLibraryCmd libraryCmd = (SgPhyOneClickLibraryCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                SgPhyOneClickLibraryCmd.class.getName(), "sg", "1.0");
        ValueHolderV14 result;
        try {
            if (log.isDebugEnabled()) {
                log.debug("开始一键入库");
            }
            result = libraryCmd.oneClickLibrary(request);
        } catch (Exception e) {
            result = new ValueHolderV14();
            result.setCode(ResultCode.FAIL);
            result.setMessage(e.getMessage());
        }
        return result.toJSONObject();
    }

    private SgPhyInResultBillAuditRequest getQueryAuditModel() {
        SgPhyInResultBillAuditRequest model = new SgPhyInResultBillAuditRequest();
        JSONArray ids = new JSONArray();
        ids.add(7);
        model.setIds(ids);
        model.setLoginUser(getUser());
        return model;
    }

    /**
     * @return
     */
    private SgPhyInResultBillSaveRequest getQueryTotalModel() {
        SgPhyInResultBillSaveRequest mode = new SgPhyInResultBillSaveRequest();
        User user = getUser();
        SgBPhyInResultSaveRequest mainRequest = new SgBPhyInResultSaveRequest();
        mainRequest.setCpCPhyWarehouseId(7L);
        mainRequest.setCpCPhyWarehouseEcode("301904241330U");
        mainRequest.setCpCPhyWarehouseEname("测试店仓U");
        mainRequest.setBillNo("301904241330U");
        mainRequest.setSgBPhyInNoticesId(9l);
        mainRequest.setBillDate(new Date());
        mainRequest.setInTime(new Date());
        mainRequest.setInId(7l);
        mainRequest.setInEcode("301904241330");
        mainRequest.setSourceBillId(9L);
        mainRequest.setSourceBillNo("301904241330");
        //--------------------设置明细------------------------
        List<SgBPhyInResultItemSaveRequest> list = new ArrayList();
        SgBPhyInResultItemSaveRequest item1 = new SgBPhyInResultItemSaveRequest();
        item1.setPsCSkuId(9L);
        item1.setPsCSkuEcode("2019042413301U");
        item1.setPsCProId(9l);
        item1.setPsCProEcode("2019042413302");
        item1.setPsCProEname("测试商品U");
        item1.setPsCSpec1Id(9l);
        item1.setPsCSpec1Ecode("2019042413303");
        item1.setPsCSpec1Ename("规格一");
        item1.setQtyIn(new BigDecimal(6));
        item1.setAmtListIn(new BigDecimal(30));
        //-----------------------------------------
        SgBPhyInResultItemSaveRequest item2 = new SgBPhyInResultItemSaveRequest();
        item2.setPsCSkuId(9L);
        item2.setPsCSkuEcode("201904251949");
        item2.setPsCProId(9l);
        item2.setPsCProEcode("201904251949");
        item2.setPsCProEname("测试商品2U");
        item2.setPsCSpec1Id(9l);
        item2.setPsCSpec1Ecode("201904251949");
        item2.setPsCSpec1Ename("规格一2U");
        item2.setQtyIn(new BigDecimal(2));
        item2.setAmtListIn(new BigDecimal(20));
        list.add(item1);
        list.add(item2);
        mode.setSgBPhyInResultSaveRequest(mainRequest);
        mode.setItemList(list);
        mode.setLoginUser(user);
        return mode;
    }

    /***
     * 获取用户
     * @return
     */
    private User getUser() {
        UserImpl user = new UserImpl();
        user.setClientId(27);
        user.setOrgId(37);
        user.setId(100);
        user.setName("root");
        user.setEname("测试用户");
        return user;
    }

}
