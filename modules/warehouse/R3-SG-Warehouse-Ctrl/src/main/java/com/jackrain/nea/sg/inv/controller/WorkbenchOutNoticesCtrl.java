package com.jackrain.nea.sg.inv.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.out.api.SgPhyOutNoticesSaveR3Cmd;
import com.jackrain.nea.sg.out.api.WorkbenchOutNoticesQueryCmd;
import com.jackrain.nea.sg.out.model.request.WorkbenchOutNoticesPageQueryRequest;
import com.jackrain.nea.sg.out.model.request.WorkbenchOutNoticesPageRequest;
import com.jackrain.nea.sg.out.model.request.WorkbenchOutNoticesQueryRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.query.DefaultWebEvent;
import com.jackrain.nea.web.query.QuerySessionImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.alibaba.dubbo.config.annotation.Reference;


import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * @description:
 * @author: 郑小龙
 * @date: 2019-05-15 19:00
 */
@RestController
@Slf4j
@Api(value = "WorkbenchOutNoticesCtrl", description = "工作台-出库通知单")
public class WorkbenchOutNoticesCtrl {

    @Reference(version = "1.0", group = "sg")
    private WorkbenchOutNoticesQueryCmd workcmd;


    @ApiOperation(value = "出库通知单编辑/保存(页面调用)")
    @PostMapping(path = "/p/cs/queryPassWmsFailure")
    public JSONObject queryPassWmsFailure(HttpServletRequest request, @RequestParam(value = "param") String param) {
        if (log.isDebugEnabled()) {
            log.debug("start WorkbenchOutNoticesCtrl.queryPassWmsFailure.ReceiveParams=" + param + ";");
        }
        Object o = ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                WorkbenchOutNoticesQueryCmd.class.getName(), "sg", "1.0");
        ValueHolderV14 result01 = new ValueHolderV14();
        ValueHolderV14 result02 = new ValueHolderV14();
        try {
            if (o != null) {

                List<Long> idlist =new ArrayList<>();
                idlist.add(16L);
                idlist.add(23L);
                idlist.add(10026L);
                WorkbenchOutNoticesPageQueryRequest pageQueryRequest = new WorkbenchOutNoticesPageQueryRequest();
                WorkbenchOutNoticesQueryRequest queryRequest= new WorkbenchOutNoticesQueryRequest();
                queryRequest.setIsJdPlatform(false);
                queryRequest.setShopList(idlist);
                pageQueryRequest.setQueryRequest(queryRequest);
                result01 = ((WorkbenchOutNoticesQueryCmd) o).queryPassWmsFailure(pageQueryRequest);
                result02 = ((WorkbenchOutNoticesQueryCmd) o).queryWareSuperCutTimeNoHair(pageQueryRequest);
            }
        } catch (Exception e) {
            return result01.toJSONObject();
        }
        return result02.toJSONObject();
    }
}
