package com.jackrain.nea.sg.inv.controller;


import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.sg.in.api.SgPhyInNoticesSelectByPchR3Cmd;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;



/**
 * @author jiang.cj
 * @since 2019-05-07
 * create at : 2019-05-07
 */
@RestController
@Slf4j
@Api(value = "SgPhyInResultSelectCtrl", description = "根据批次号查询通知单")
public class SgPhyInResultSelectCtrl {

    @ApiOperation(value = "根据批次号查询通知单")
    @PostMapping(path = "/p/cs/sg/getSgPhyInResultByPch")
    public ValueHolder getSgPhyInResultByPch() {
        ValueHolder result=new ValueHolder();
        Object o = ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                SgPhyInNoticesSelectByPchR3Cmd.class.getName(), "sg", "1.0");
        try {
            HashMap map=new HashMap();
            JSONObject obj=new JSONObject();
            JSONObject value=new JSONObject();
            value.put("val","201906131109270001Z");
            obj.put("value",value);
            map.put("param",obj);
             result = ((SgPhyInNoticesSelectByPchR3Cmd) o).execute(map);
            return result;
        } catch (Exception e) {
            result.put("code",-1);
            result.put("message",e.toString());
            return result;
        }
    }
}
