package com.jackrain.nea.sg.inv.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.out.api.SgPhyOutPickOrderAuditCmd;
import com.jackrain.nea.sg.out.api.SgPhyOutPickOrderGenerateCmd;
import com.jackrain.nea.sg.out.api.SgPhyOutPickOrderSaveCmd;
import com.jackrain.nea.sg.out.model.request.SgPhyOutPickOrderAuditRequest;
import com.jackrain.nea.sg.out.model.request.SgPhyOutPickOrderSaveRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.face.impl.UserImpl;
import com.jackrain.nea.web.query.DefaultWebEvent;
import com.jackrain.nea.web.query.QuerySessionImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @author zhu lin yu
 * @since 2019-11-22
 * create at : 2019-11-22 09:49
 */
@RestController
@Slf4j
public class SgPhyOutPickOrderCtrl {

    @PostMapping(path = "/p/cs/sg/generateOutPickOrder")
    public JSONObject generateOutPickOrder(HttpServletRequest request, @RequestParam(value = "param", required = true) String param) throws Exception {

        Object o = ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                SgPhyOutPickOrderGenerateCmd.class.getName(), "sg", "1.0");
        QuerySessionImpl querySession = new QuerySessionImpl(request);
        JSONObject params = JSON.parseObject(param);
        DefaultWebEvent event = new DefaultWebEvent("generateOutPickOrder", request, false);
        event.put("param", params);
        querySession.setEvent(event);

        ValueHolderV14 result = ((SgPhyOutPickOrderGenerateCmd) o).execute(querySession);
        return result.toJSONObject();
    }

    @PostMapping(path = "/p/cs/sg/saveOutPickOrder")
    public JSONObject saveOutPickOrder(HttpServletRequest request, @RequestParam(value = "param", required = true) String param) throws Exception {

        Object o = ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                SgPhyOutPickOrderSaveCmd.class.getName(), "sg", "1.0");
        QuerySessionImpl querySession = new QuerySessionImpl(request);

        SgPhyOutPickOrderSaveRequest saveRequest = JSONObject.parseObject(param, SgPhyOutPickOrderSaveRequest.class);
        saveRequest.setLoginUser(querySession.getUser());

        ValueHolderV14 result = ((SgPhyOutPickOrderSaveCmd) o).saveSgBoutPickorderTeusItem(saveRequest);
        return result.toJSONObject();
    }

    @PostMapping(path = "/p/cs/sg/auditOutPickOrder")
    public JSONObject auditOutPickOrder(HttpServletRequest request, @RequestParam(value = "param", required = true) String param) throws Exception {

        Object o = ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                SgPhyOutPickOrderAuditCmd.class.getName(), "sg", "1.0");
        QuerySessionImpl querySession = new QuerySessionImpl(request);

        JSONObject params = JSON.parseObject(param);
        Long id = params.getLong("ID");
        SgPhyOutPickOrderAuditRequest sgPhyBOutPickOrderSaveRequest = new SgPhyOutPickOrderAuditRequest();
        sgPhyBOutPickOrderSaveRequest.setLoginUser(querySession.getUser());
        sgPhyBOutPickOrderSaveRequest.setOutPickOrderId(id);

        ValueHolderV14 result = ((SgPhyOutPickOrderAuditCmd) o).auditOutPickOrder(sgPhyBOutPickOrderSaveRequest);
        return result.toJSONObject();
    }

    private User getRootUser() {
        UserImpl user = new UserImpl();
        user.setId(893);
        user.setName("root");
        user.setEname("root");
        user.setActive(true);
        user.setClientId(37);
        user.setOrgId(27);
        user.setIsAdmin(2);
        user.setIsDev(2);
        return user;
    }
}
