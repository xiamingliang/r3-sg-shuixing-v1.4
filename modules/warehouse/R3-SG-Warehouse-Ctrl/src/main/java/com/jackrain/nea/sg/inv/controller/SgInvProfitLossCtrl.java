package com.jackrain.nea.sg.inv.controller;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.inv.api.ScInvProfitLossGenarateCmd;
import com.jackrain.nea.sg.inv.api.ScInvProfitLossMarginQueryCmd;
import com.jackrain.nea.sg.inv.model.request.ScInvProfitLossMarginQueryCmdRequest;
import com.jackrain.nea.sg.inv.model.request.ScInvProfitLossQueryCmdRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.DateUtil;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.face.impl.UserImpl;
import com.jackrain.nea.web.query.QuerySessionImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;


/**
 * @author 舒威
 * @since 2019/4/1
 * create at : 2019/4/1 18:26
 */
@RestController
@Slf4j
@Api(value = "SgInvProfitLossCtrl", description = "预盈亏")
public class SgInvProfitLossCtrl {

    @ApiOperation(value = "查询最近一张未盈亏盘点单")
    @PostMapping(path = "/p/cs/sg/v1/queryLastProfitLossInventory")
    public JSONObject queryLastProfitLossInventory(HttpServletRequest request, @RequestParam(value = "param", required = true) String param) {

        if (log.isDebugEnabled()) {
            log.debug("start SgInvProfitLossCtrl.queryLastProfitLossInventory. ReceiveParams=" + param + ";");
        }

        Object o = ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                ScInvProfitLossMarginQueryCmd.class.getName(), "sg", "1.0");
        ValueHolderV14 result;
        try {
            QuerySessionImpl querySession = new QuerySessionImpl(request);
            User user = querySession.getUser();
            if (null != o) {
                ScInvProfitLossMarginQueryCmdRequest model = JSONObject.parseObject(param, ScInvProfitLossMarginQueryCmdRequest.class);
                model.setLoginUser(user);
                result = ((ScInvProfitLossMarginQueryCmd) o).queryLastProfitLossInventory(model);
                if (log.isDebugEnabled()) {
                    log.debug("Finish ScInvProfitLossCtrl.bprofitlossinventoryquery. ReturnResult=" + result);
                }
                if (result.isOK()) {
                    return JSONObject.parseObject(JSONObject.toJSONString(result.toJSONObject(),
                            SerializerFeature.WriteMapNullValue));
                } else {
                    return result.toJSONObject();
                }
            } else {
                return vh14ToVh13("未获取到对应的bean").toJSONObject();
            }
        } catch (Exception e) {
            ValueHolderV14 ret = new ValueHolderV14(ResultCode.FAIL, e.getMessage());
            return ret.toJSONObject();
        }
    }


    @ApiOperation(value = "查询未盈亏盘点单")
    @PostMapping(path = "/p/cs/sg/v1/queryProfitLossInventory")
    public JSONObject queryProfitLossInventory(HttpServletRequest request, @RequestParam(value = "param", required = true) String param) {

        if (log.isDebugEnabled()) {
            log.debug("start SgInvProfitLossCtrl.queryProfitLossInventory. ReceiveParams=" + param + ";");
        }

        Object o = ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                ScInvProfitLossMarginQueryCmd.class.getName(), "sg", "1.0");
        ValueHolderV14 result;
        try {
            QuerySessionImpl querySession = new QuerySessionImpl(request);
            User user = querySession.getUser();
            if (null != o) {
                ScInvProfitLossMarginQueryCmdRequest model = JSONObject.parseObject(param, ScInvProfitLossMarginQueryCmdRequest.class);
                model.setLoginUser(user);
                result = ((ScInvProfitLossMarginQueryCmd) o).queryProfitLossInventory(model);
                if (log.isDebugEnabled()) {
                    log.debug("Finish ScInvProfitLossCtrl.bprofitlosspandquery. ReturnResult=" + result);
                }
                return JSONObject.parseObject(JSONObject.toJSONString(result.toJSONObject(),
                        SerializerFeature.WriteMapNullValue));
            } else {
                ValueHolderV14 ret = new ValueHolderV14(ResultCode.FAIL, "未获取到对应的bean");
                return ret.toJSONObject();
            }
        } catch (Exception e) {
            ValueHolderV14 ret = new ValueHolderV14(ResultCode.FAIL, e.getMessage());
            return ret.toJSONObject();
        }
    }


    @ApiOperation(value = "查询未完成单据")
    @PostMapping(path = "/p/cs/sg/v1/queryUnfinishProfitLoss")
    public JSONObject queryUnfinishProfitLoss(HttpServletRequest request, @RequestParam(value = "param", required = true) String param) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgInvProfitLossCtrl.queryUnfinishProfitLoss. ReceiveParams=" + param + ";");
        }

        Object o = ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                ScInvProfitLossMarginQueryCmd.class.getName(), "sg", "1.0");
        ValueHolderV14 result;
        try {
            QuerySessionImpl querySession = new QuerySessionImpl(request);
            User user = querySession.getUser();
            ScInvProfitLossMarginQueryCmdRequest model = JSONObject.parseObject(param, ScInvProfitLossMarginQueryCmdRequest.class);
            model.setLoginUser(user);
            result = ((ScInvProfitLossMarginQueryCmd) o).queryUnfinishProfitLoss(model);
            if (log.isDebugEnabled()) {
                log.debug("Finish SgInvProfitLossCtrl.queryUnfinishProfitLoss. ReturnResult=" + result);
            }
            return JSONObject.parseObject(JSONObject.toJSONString(result.toJSONObject(),
                    SerializerFeature.WriteMapNullValue));
        } catch (Exception e) {
            ValueHolderV14 ret = new ValueHolderV14(ResultCode.FAIL, e.getMessage());
            return ret.toJSONObject();
        }
    }

    @ApiOperation(value = "查询盈亏明细")
    @PostMapping(path = "/p/cs/sg/v1/queryProfitLossItem")
    public JSONObject queryProfitLossItem(HttpServletRequest request, @RequestParam(value = "param", required = true) String param) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgInvProfitLossCtrl.queryProfitLossItem. ReceiveParams=" + param + ";");
        }

        Object o = ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                ScInvProfitLossMarginQueryCmd.class.getName(), "sg", "1.0");
        ValueHolderV14 result;
        try {
            QuerySessionImpl querySession = new QuerySessionImpl(request);
            User user = querySession.getUser();
            ScInvProfitLossMarginQueryCmdRequest model = JSONObject.parseObject(param, ScInvProfitLossMarginQueryCmdRequest.class);
            model.setLoginUser(user);
            result = ((ScInvProfitLossMarginQueryCmd) o).queryProfitLossItem(model);
            if (log.isDebugEnabled()) {
                log.debug("Finish SgInvProfitLossCtrl.queryProfitLossItem. ReturnResult=" + result);
            }
            return JSONObject.parseObject(JSONObject.toJSONString(result.toJSONObject(),
                    SerializerFeature.WriteMapNullValue));
        } catch (Exception e) {
            ValueHolderV14 ret = new ValueHolderV14(ResultCode.FAIL, e.getMessage());
            return ret.toJSONObject();
        }
    }

    @ApiOperation(value = "查询预盈亏")
    @PostMapping(path = "/p/cs/sg/v1/queryProfitLoss")
    public JSONObject queryProfitLoss(HttpServletRequest request, @RequestParam(value = "param", required = true) String param) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgInvProfitLossCtrl.queryProfitLoss. ReceiveParams=" + param + ";");
        }

        Object o = ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                ScInvProfitLossMarginQueryCmd.class.getName(), "sg", "1.0");
        ValueHolderV14 result;
        try {
            QuerySessionImpl querySession = new QuerySessionImpl(request);
            User user = querySession.getUser();
            ScInvProfitLossMarginQueryCmdRequest model = JSONObject.parseObject(param, ScInvProfitLossMarginQueryCmdRequest.class);
            model.setLoginUser(user);
            result = ((ScInvProfitLossMarginQueryCmd) o).queryProfitLoss(model);
            if (log.isDebugEnabled()) {
                log.debug("Finish SgInvProfitLossCtrl.queryProfitLoss. ReturnResult=" + result);
            }
            return JSONObject.parseObject(JSONObject.toJSONString(result.toJSONObject(),
                    SerializerFeature.WriteMapNullValue));
        } catch (Exception e) {
            ValueHolderV14 ret = new ValueHolderV14(ResultCode.FAIL, e.getMessage());
            return ret.toJSONObject();
        }
    }


    @ApiOperation(value = "获取预盈亏数量合计信息")
    @PostMapping(path = "/p/cs/sg/v1/queryProfitLossTotalInventory")
    public JSONObject queryProfitLossTotalInventory(HttpServletRequest request, @RequestParam(value = "param", required = true) String param) {

        if (log.isDebugEnabled()) {
            log.debug("start SgInvProfitLossCtrl.queryProfitLossTotalInventory. ReceiveParams=" + param + ";");
        }

        Object o = ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                ScInvProfitLossGenarateCmd.class.getName(), "sg", "1.0");
        ValueHolderV14 result;
        try {
            if (null != o) {
                QuerySessionImpl querySession = new QuerySessionImpl(request);
                User user = querySession.getUser();
                ScInvProfitLossMarginQueryCmdRequest model = JSONObject.parseObject(param, ScInvProfitLossMarginQueryCmdRequest.class);
                model.setLoginUser(user);
                result = ((ScInvProfitLossGenarateCmd) o).queryProfitLossTotalInventory(model);
                if (log.isDebugEnabled()) {
                    log.debug("Finish SgInvProfitLossCtrl.bprofitlosstotalquery. ReturnResult=" + result);
                }
                return result.toJSONObject();
            } else {
                ValueHolderV14 ret = new ValueHolderV14(ResultCode.FAIL, "未获取到对应的bean");
                return ret.toJSONObject();
            }

        } catch (Exception e) {
            ValueHolderV14 ret = new ValueHolderV14(ResultCode.FAIL, e.getMessage());
            return ret.toJSONObject();
        }
    }

    @ApiOperation(value = "生成盈亏")
    @PostMapping(path = "/p/cs/sg/v1/genarateProfitLoss")
    public JSONObject genarateProfitLoss(HttpServletRequest request, @RequestParam(value = "param", required = true) String param) {

        if (log.isDebugEnabled()) {
            log.debug("start SgInvProfitLossCtrl.genarateProfitLoss. ReceiveParams=" + param + ";");
        }

        Object o = ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                ScInvProfitLossGenarateCmd.class.getName(), "sg", "1.0");
        ValueHolderV14 result;
        try {
            QuerySessionImpl querySession = new QuerySessionImpl(request);
            User user = querySession.getUser();
            if (null != o) {
                ScInvProfitLossMarginQueryCmdRequest model = JSONObject.parseObject(param, ScInvProfitLossMarginQueryCmdRequest.class);
                model.setLoginUser(user);
                result = ((ScInvProfitLossGenarateCmd) o).genarateProfitLoss(model);
                if (log.isDebugEnabled()) {
                    log.debug("Finish SgInvProfitLossCtrl.genarateProfitLoss. ReturnResult=" + result);
                }
                return result.toJSONObject();
            } else {
                ValueHolderV14 ret = new ValueHolderV14(ResultCode.FAIL, "未获取到对应的bean");
                return ret.toJSONObject();
            }

        } catch (Exception e) {
            ValueHolderV14 ret = new ValueHolderV14(ResultCode.FAIL, e.getMessage());
            return ret.toJSONObject();
        }
    }


    @ApiOperation(value = "获取预盈亏数量合计信息")
    @PostMapping(path = "/p/cs/sg/v1/queryProfitLossGenarateInfo")
    public JSONObject queryProfitLossGenarateInfo(HttpServletRequest request, @RequestParam(value = "param", required = true) String param) {

        if (log.isDebugEnabled()) {
            log.debug("start SgInvProfitLossCtrl.queryProfitLossGenarateInfo. ReceiveParams=" + param + ";");
        }

        Object o = ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                ScInvProfitLossGenarateCmd.class.getName(), "sg", "1.0");
        ValueHolderV14 result;
        try {
            QuerySessionImpl querySession = new QuerySessionImpl(request);
            User user = querySession.getUser();
            if (null != o) {
                ScInvProfitLossMarginQueryCmdRequest model = JSONObject.parseObject(param, ScInvProfitLossMarginQueryCmdRequest.class);
                model.setLoginUser(user);
                result = ((ScInvProfitLossGenarateCmd) o).queryProfitLossGenarateInfo(model);
                if (log.isDebugEnabled()) {
                    log.debug("Finish SgInvProfitLossCtrl.queryProfitLossGenarateInfo. ReturnResult=" + result);
                }
                return result.toJSONObject();
            } else {
                ValueHolderV14 ret = new ValueHolderV14(ResultCode.FAIL, "未获取到对应的bean");
                return ret.toJSONObject();
            }

        } catch (Exception e) {
            ValueHolderV14 ret = new ValueHolderV14(ResultCode.FAIL, e.getMessage());
            return ret.toJSONObject();
        }
    }

    /**
     * v14转v13
     */
    private ValueHolder vh14ToVh13(String message) {
        ValueHolder v13 = new ValueHolder();
        v13.put("code", ResultCode.FAIL);
        v13.put("message", message);
        return v13;
    }

    /**
     * 封装查询入参model
     */
    private ScInvProfitLossQueryCmdRequest getQueryTotalModel(JSONObject param) {
        ScInvProfitLossQueryCmdRequest model = new ScInvProfitLossQueryCmdRequest();
        model.setWarehouseId(param.getInteger("CP_C_STORE_ID").longValue());
        model.setInventoryType(param.getInteger("PAND_TYPE"));
        String pand_date = param.getString("PAND_DATE");
        Date date = DateUtil.stringToDate(pand_date);
        model.setInventoryDate(date);
        model.setLoginUser(getUser());
        JSONObject page = param.getJSONObject("page");
        if (null != page) {
            model.setCurrentPage(page.getInteger("currentPage"));
            model.setRange(page.getInteger("pageSize"));
            JSONObject orderBy = page.getJSONObject("orderBy");
            if (null != orderBy) {
                model.setOrderByName(orderBy.getString("name"));
                model.setOrderByAsc(orderBy.getBoolean("isasc"));
            }
        }
        return model;
    }

    /**
     * 测试用
     */
    private User getUser() {
        UserImpl user = new UserImpl();
        user.setStoreId(16);
        user.setClientId(27);
        user.setOrgId(37);
        user.setId(100);
        user.setName("田野");
        user.setEname("草原");
        return user;
    }

}
