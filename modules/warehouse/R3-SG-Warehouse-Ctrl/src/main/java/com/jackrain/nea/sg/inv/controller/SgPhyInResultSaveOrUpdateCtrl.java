package com.jackrain.nea.sg.inv.controller;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.sg.in.api.SgPhyInResultSaveR3Cmd;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.face.impl.UserImpl;
import com.jackrain.nea.web.query.DefaultWebEvent;
import com.jackrain.nea.web.query.QuerySessionImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;



/**
 * @author jiang.cj
 * @since 2019-4-24
 * create at : 2019-4-24 13:03
 */
@RestController
@Slf4j
@Api(value = "SgPhyInResultCtrl", description = "入库结果单")
public class SgPhyInResultSaveOrUpdateCtrl {

    @ApiOperation(value = "入库结果单页面新增修改")
    @PostMapping(path = "/p/cs/sg/saveOrUpdateSgPhyInResult")
    public ValueHolder saveOrUpdateSgPhyInResult(HttpServletRequest request, @RequestParam(value = "param", required = true) String param) throws Exception {
        if (log.isDebugEnabled()) {
            log.debug("start SgPhyInResultSaveOrUpdateCtrl.saveOrUpdateSgPhyInResult. ReceiveParams=" + param + ";");
        }
        Object o = ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                SgPhyInResultSaveR3Cmd.class.getName(), "sg", "1.0");
        QuerySessionImpl querySession = new QuerySessionImpl(getUser());
        JSONObject params = JSON.parseObject(param);
        DefaultWebEvent event = new DefaultWebEvent("saveSgPhyOutResultR3", request, false);
        event.put("param", params);
        querySession.setEvent(event);
        ValueHolder result = ((SgPhyInResultSaveR3Cmd) o).execute(querySession);
        return result;
    }


    /***
     * 获取用户
     * @return
     */
    private User getUser() {
        UserImpl user = new UserImpl();
        user.setClientId(27);
        user.setOrgId(37);
        user.setId(100);
        user.setName("root");
        user.setEname("测试用户");
        return user;
    }

}
