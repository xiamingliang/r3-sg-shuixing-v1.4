package com.jackrain.nea.sg.inv.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.in.api.*;
import com.jackrain.nea.sg.in.model.request.SgBInPickorderRequest;
import com.jackrain.nea.sg.in.model.request.SgPhyInPickOrderAuditRequest;
import com.jackrain.nea.sg.in.model.request.SgPhyInPickOrderSaveRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.face.impl.UserImpl;
import com.jackrain.nea.web.query.DefaultWebEvent;
import com.jackrain.nea.web.query.QuerySessionImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @author: heliu
 * @since: 2019/11/18
 * create at : 2019/11/18 13:43
 */
@RestController
@Slf4j
@Api(value = "SgBInPickorderCtrl", description = "入库拣货单测试Ctrl")
public class SgBInPickorderCtrl {

    @ApiOperation(value = "入库拣货单自定义查询接口")
    @PostMapping(path = "/p/cs/sg/v1/warehouse/sgBInPickorder/querySgBInPickorder")
    public JSONObject querySgBInPickorder(HttpServletRequest request, @RequestParam(value = "objid") Long objid) {
        SgBInPickorderRequest sgBInPickorderRequest = new SgBInPickorderRequest();
        User user = getRootUser();
        sgBInPickorderRequest.setId(objid);
        sgBInPickorderRequest.setLoginUser(user);
        SgBInPickorderItemQueryCmd sgBInPickorderItemQueryCmd = (SgBInPickorderItemQueryCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                SgBInPickorderItemQueryCmd.class.getName(), "sg", "1.0");
        ValueHolderV14 result;
        try {
            result = sgBInPickorderItemQueryCmd.querySgBInPickorderItem(sgBInPickorderRequest);
        } catch (Exception e) {
            result = new ValueHolderV14();
            result.setCode(ResultCode.FAIL);
            result.setMessage(e.getMessage());
        }
        return result.toJSONObject();
    }

    @ApiOperation(value = "入库拣货单作废")
    @PostMapping(path = "/sg/warehouse/sgBInPickorder/voidSgBInPickorder")
    public JSONObject voidSgBInPickorder(HttpServletRequest request, @RequestBody JSONObject param) {
        //TODO 測試用ctr
        if (log.isDebugEnabled()) {
            log.debug("Strat SgBInPickorderCtrl.voidSgBInPickorder. ReceiveParams=" + param + ";");
        }
        ValueHolder result;
        User user = getRootUser();
        try {
            QuerySessionImpl querySession = new QuerySessionImpl(user);
            DefaultWebEvent event = new DefaultWebEvent("voidSgBInPickorder", request, false);
            event.put("param", param);
            querySession.setEvent(event);
            SgBInPickorderVoidCmd sgBInPickorderVoidCmd = (SgBInPickorderVoidCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                    SgBInPickorderVoidCmd.class.getName(), "sg", "1.0");
            result = sgBInPickorderVoidCmd.execute(querySession);
        } catch (Exception e) {
            throw new NDSException(Resources.getMessage(e.getMessage(), user.getLocale()));
        }
        if (log.isDebugEnabled()) {
            log.debug("Finish SgBInPickorderCtrl.voidSgBInPickorder. ReturnResult=" + result);
        }
        return result.toJSONObject();
    }


    @PostMapping(path = "/p/cs/sg/generateInPickOrder")
    public JSONObject generateInPickOrder(HttpServletRequest request, @RequestParam(value = "param", required = true) String param) throws Exception {

        Object o = ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                SgPhyInPickOrderGenerateCmd.class.getName(), "sg", "1.0");
        QuerySessionImpl querySession = new QuerySessionImpl(request);
        JSONObject params = JSON.parseObject(param);
        DefaultWebEvent event = new DefaultWebEvent("generateInPickOrder", request, false);
        event.put("param", params);
        querySession.setEvent(event);

        ValueHolderV14 result = ((SgPhyInPickOrderGenerateCmd) o).execute(querySession);
        return result.toJSONObject();
    }

    @PostMapping(path = "/p/cs/sg/saveInPickOrder")
    public JSONObject saveInPickOrder(HttpServletRequest request, @RequestParam(value = "param", required = true) String param) throws Exception {

        Object o = ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                SgPhyInPickOrderSaveCmd.class.getName(), "sg", "1.0");
        QuerySessionImpl querySession = new QuerySessionImpl(request);

        SgPhyInPickOrderSaveRequest saveRequest = JSONObject.parseObject(param, SgPhyInPickOrderSaveRequest.class);
        saveRequest.setLoginUser(querySession.getUser());

        ValueHolderV14 result = ((SgPhyInPickOrderSaveCmd) o).saveSgbBInPickorderTeusItem(saveRequest);
        return result.toJSONObject();
    }

    @PostMapping(path = "/p/cs/sg/auditInPickOrder")
    public JSONObject auditInPickOrder(HttpServletRequest request, @RequestParam(value = "param", required = true) String param) throws Exception {

        Object o = ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                SgPhyInPickOrderAuditCmd.class.getName(), "sg", "1.0");
        QuerySessionImpl querySession = new QuerySessionImpl(request);

        JSONObject params = JSONObject.parseObject(param);
        Long id = params.getLong("ID");
        SgPhyInPickOrderAuditRequest sgPhyBInPickOrderSaveRequest = new SgPhyInPickOrderAuditRequest();
        sgPhyBInPickOrderSaveRequest.setLoginUser(querySession.getUser());
        sgPhyBInPickOrderSaveRequest.setInPickOrderId(id);

        ValueHolderV14 result = ((SgPhyInPickOrderAuditCmd) o).auditInPickOrder(sgPhyBInPickOrderSaveRequest);
        return result.toJSONObject();
    }

    private User getRootUser() {
        UserImpl user = new UserImpl();
        user.setId(893);
        user.setName("Fire BALL-V1");
        user.setEname("Pokemon-mapper");
        user.setActive(true);
        user.setClientId(37);
        user.setOrgId(27);
        user.setIsAdmin(2);
        user.setIsDev(2);
        return user;
    }
}