package com.jackrain.nea.sg.inv.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.oc.basic.api.TableServiceCmd;
import com.jackrain.nea.sg.out.api.*;
import com.jackrain.nea.sg.out.model.request.*;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.face.impl.UserImpl;
import com.jackrain.nea.web.query.DefaultWebEvent;
import com.jackrain.nea.web.query.QuerySessionImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static com.jackrain.nea.sg.basic.common.SystemUserResource.getRootUser;

/**
 * @author leexh
 * @since 2019/4/23 10:06
 */
@RestController
@Slf4j
@Api(value = "sg_b_phy_out_notices", description = "出库通知单")
public class SgPhyOutNoticesCtrl {


    @ApiOperation(value = "出库通知单编辑/保存(接口调用)")
    @PostMapping(path = "/p/cs/saveSgPhyOutNotices")
    public JSONObject saveSgPhyOutNotices(HttpServletRequest request, @RequestParam(value = "param") String param) {

        if (log.isDebugEnabled()) {
            log.debug("start SgPhyOutNoticesCtrl.saveSgPhyOutNotices. ReceiveParams=" + param + ";");
        }

        Object o = ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                SgPhyOutNoticesSaveCmd.class.getName(), "sg", "1.0");

        ValueHolderV14 result;
        try {
            if (o == null) {
                result = new ValueHolderV14();
                result.setCode(ResultCode.FAIL);
                result.setMessage("未获取到对应的bean");
            } else {

               /* SgPhyOutNoticesBillSaveRequest bill = new SgPhyOutNoticesBillSaveRequest();
                bill.setObjId(-1l);
                bill.setOutNoticesRequest(this.getNotices());
                bill.setOutNoticesItemRequests(this.getNoticesItems());*/
                SgPhyOutNoticesBillSaveRequest bill = JSONObject.parseObject(param, SgPhyOutNoticesBillSaveRequest.class);
                bill.setLoginUser(this.getUser());
                result = ((SgPhyOutNoticesSaveCmd) o).saveSgPhyOutNotices(bill);
            }
        } catch (Exception e) {
            result = new ValueHolderV14();
            result.setCode(ResultCode.FAIL);
            result.setMessage("error--" + e.getMessage());
            return result.toJSONObject();
        }
        return result.toJSONObject();
    }

    @ApiOperation(value = "出库通知单编辑/保存(页面调用)")
    @PostMapping(path = "/p/cs/saveSgPhyOutNoticesR3")
    public JSONObject saveSgPhyOutNoticesR3(HttpServletRequest request, @RequestParam(value = "param") String param) {

        if (log.isDebugEnabled()) {
            log.debug("start SgPhyOutNoticesCtrl.saveSgPhyOutNoticesR3. ReceiveParams=" + param + ";");
        }

        QuerySessionImpl querySession = new QuerySessionImpl(getUser());
//        QuerySessionImpl querySession = new QuerySessionImpl(request);
        JSONObject paramJo = JSON.parseObject(param);
        DefaultWebEvent event = new DefaultWebEvent("saveSgPhyOutNoticesR3", request, false);
        event.put("param", paramJo);
        querySession.setEvent(event);
        Object o = ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                SgPhyOutNoticesSaveR3Cmd.class.getName(), "sg", "1.0");

        ValueHolder result;
        try {
            if (o != null) {
                result = ((SgPhyOutNoticesSaveR3Cmd) o).execute(querySession);
                if (log.isDebugEnabled()) {
                    log.debug("Finish SgInvProfitLossCtrl.saveSgPhyOutNoticesR3. ReturnResult=" + result);
                }

            } else {
                result = new ValueHolder();
                result.put("code", ResultCode.FAIL);
                result.put("message", "未获取到对应的bean");
            }
        } catch (Exception e) {
            result = new ValueHolder();
            result.put("code", ResultCode.FAIL);
            result.put("message", "error--" + e);
            return result.toJSONObject();
        }
        return result.toJSONObject();
    }


    @ApiOperation(value = "出库通知单作废")
    @PostMapping(path = "/p/cs/voidSgPhyOutNotices")
    public JSONObject voidSgPhyOutNotices(HttpServletRequest request, @RequestParam(value = "param") String param) {

        if (log.isDebugEnabled()) {
            log.debug("start SgPhyOutNoticesCtrl.voidSgPhyOutNotices. ReceiveParams=" + param + ";");
        }

        Object o = ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                SgPhyOutNoticesVoidCmd.class.getName(), "sg", "1.0");

        ValueHolderV14 result;
        try {
            if (o == null) {
                result = new ValueHolderV14();
                result.setCode(ResultCode.FAIL);
                result.setMessage("未获取到对应的bean");
            } else {

                SgPhyOutNoticesBillVoidRequest bill = new SgPhyOutNoticesBillVoidRequest();
                bill.setNoticesSaveRequests(getNoticesList());
                bill.setLoginUser(this.getUser());
                result = ((SgPhyOutNoticesVoidCmd) o).voidSgPhyOutNotices(bill);
            }
        } catch (Exception e) {
            result = new ValueHolderV14();
            result.setCode(ResultCode.FAIL);
            result.setMessage("error--" + e.getMessage());
            return result.toJSONObject();
        }
        return result.toJSONObject();
    }

    @ApiOperation(value = "出库通知单明细删除")
    @PostMapping(path = "/p/cs/delSgPhyOutNoticesItem")
    public JSONObject delSgPhyOutNoticesItem(HttpServletRequest request, @RequestParam(value = "param") String param) {

        if (log.isDebugEnabled()) {
            log.debug("start SgPhyOutNoticesCtrl.delSgPhyOutNoticesItem. ReceiveParams=" + param + ";");
        }

        Object o = ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                SgPhyOutNoticesItemDelCmd.class.getName(), "sg", "1.0");

        ValueHolderV14 result;
        try {
            if (o == null) {
                result = new ValueHolderV14();
                result.setCode(ResultCode.FAIL);
                result.setMessage("未获取到对应的bean");
            } else {

                JSONArray ids = new JSONArray();
                ids.add(1l);
                SgPhyOutNoticesItemBillDelRequest bill = new SgPhyOutNoticesItemBillDelRequest();
                bill.setNoticesRequest(getNotices());
                bill.setSourceBillItemIds(ids);
                bill.setLoginUser(this.getUser());
                result = ((SgPhyOutNoticesItemDelCmd) o).delSgPhyOutNoticesItem(bill);
            }
        } catch (Exception e) {
            result = new ValueHolderV14();
            result.setCode(ResultCode.FAIL);
            result.setMessage("error--" + e.getMessage());
            return result.toJSONObject();
        }
        return result.toJSONObject();
    }

    @ApiOperation(value = "作废出库通知单并从WMS撤回")
    @PostMapping(path = "/p/cs/voidOutNoticesAndCancelWms")
    public JSONObject voidOutNoticesAndCancelWms(@RequestBody SgPhyOutNoticesBillVoidRequest request) {

        if (log.isDebugEnabled()) {
            log.debug("start SgPhyOutNoticesCtrl.voidOutNoticesAndCancelWms. ReceiveParams=" + JSONObject.toJSONString(request) + ";");
        }

        ValueHolderV14 result;
        try {
            SgPhyOutNoticesBatchVoidAndWMSCmd voidAndWMSCmd = (SgPhyOutNoticesBatchVoidAndWMSCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                    SgPhyOutNoticesBatchVoidAndWMSCmd.class.getName(), "sg", "1.0");
            request.setLoginUser(getUser());
            result = voidAndWMSCmd.batchVoidSgPhyOutNoticesAndWMS(request);
        } catch (Exception e) {
            result = new ValueHolderV14();
            result.setCode(ResultCode.FAIL);
            result.setMessage("error--" + e.getMessage());
            return result.toJSONObject();
        }
        return result.toJSONObject();
    }

    @ApiOperation(value = "出库通知单传WMS回执服务")
    @PostMapping(path = "/p/cs/saveNoticesWmsBack")
    public JSONObject saveNoticesWmsBack(@RequestBody List<SgPhyOutNoticesBillWMSBackRequest> requests) {

        if (log.isDebugEnabled()) {
            log.debug("start SgPhyOutNoticesCtrl.saveNoticesWmsBack. ReceiveParams=" + JSONObject.toJSONString(requests) + ";");
        }

        ValueHolderV14 result;
        try {
            SgPhyOutNoticesSaveWMSBackCmd saveWMSBackCmd = (SgPhyOutNoticesSaveWMSBackCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                    SgPhyOutNoticesSaveWMSBackCmd.class.getName(), "sg", "1.0");
            result = saveWMSBackCmd.saveWMSBackOutNotices(requests, getUser());
        } catch (Exception e) {
            result = new ValueHolderV14();
            result.setCode(ResultCode.FAIL);
            result.setMessage("error--" + e.getMessage());
            return result.toJSONObject();
        }
        return result.toJSONObject();
    }


    @ApiOperation(value = "pos-一键扣减库存")
    @PostMapping(path = "/p/cs/commitOutBillsForPosService")
    public JSONObject commitOutBillsForPosService(@RequestBody SgPhyOutCommitForPosRequest request) {

        if (log.isDebugEnabled()) {
            log.debug("start SgPhyOutNoticesCtrl.commitOutBillsForPosService. ReceiveParams=" + JSONObject.toJSONString(request) + ";");
        }

        ValueHolderV14 result;
        try {
            SgPhyOutCommitForPosCmd commitForPosCmd = (SgPhyOutCommitForPosCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                    SgPhyOutCommitForPosCmd.class.getName(), "sg", "1.0");
            request.setLoginUser(getUser());
            result = commitForPosCmd.saveAndAuditOutBills(request);
        } catch (Exception e) {
            result = new ValueHolderV14();
            result.setCode(ResultCode.FAIL);
            result.setMessage("error--" + e.getMessage());
            return result.toJSONObject();
        }
        return result.toJSONObject();
    }

    @ApiOperation(value = "全渠道发货单-保存")
    @PostMapping(path = "/p/cs/sg/v1/saveOutNoticesPos")
    public JSONObject saveOutNoticesPos(HttpServletRequest request, @RequestParam(value = "param") String param) {
        //TODO 測試用ctr
        if (log.isDebugEnabled()) {
            log.debug("Strat SgPhyOutNoticesCtrl.saveOutNoticesPos. ReceiveParams=" + param + ";");
        }
        ValueHolder result;
        User user = getRootUser();
        QuerySessionImpl querySession = new QuerySessionImpl(user);
        JSONObject paramJo = JSON.parseObject(param);
        //默认 走save操作
        DefaultWebEvent event = new DefaultWebEvent(paramJo.getString("eventName") == null ? "dosave" : paramJo.getString("eventName"), request, false);
        event.put("param", paramJo);
        querySession.setEvent(event);
        Object o = ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                TableServiceCmd.class.getName(), "sg", "7");
        result = ((TableServiceCmd) o).execute(querySession);
        if (log.isDebugEnabled()) {
            log.debug("Finish SgPhyOutNoticesCtrl.saveOutNoticesPos. ReturnResult=" + result);
        }
        return result.toJSONObject();
    }

    @ApiOperation(value = "全渠道发货单-发货")
    @PostMapping(path = "/p/cs/sg/v1/sendOutNoticesPos")
    public JSONObject sendOutNoticesPos(HttpServletRequest request, @RequestParam(value = "param") String param) {
        if (log.isDebugEnabled()) {
            log.debug("Strat SgPhyOutNoticesCtrl.sendOutNoticesPos. ReceiveParams=" + param + ";");
        }
        QuerySessionImpl querySession = new QuerySessionImpl(getUser());
//        QuerySessionImpl querySession = new QuerySessionImpl(request);
        JSONObject paramJo = JSON.parseObject(param);
        DefaultWebEvent event = new DefaultWebEvent("sendOutNoticesPos", request, false);
        event.put("param", paramJo);
        querySession.setEvent(event);
        Object o = ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                SgPhyOutNoticesPosSendCmd.class.getName(), "sg", "1.0");
        ValueHolder result = ((SgPhyOutNoticesPosSendCmd) o).execute(querySession);
        if (log.isDebugEnabled()) {
            log.debug("Finish SgPhyOutNoticesCtrl.saveOutNoticesPos. ReturnResult=" + result);
        }
        return result.toJSONObject();
    }

    @ApiOperation(value = "全渠道发货单-退回")
    @PostMapping(path = "/p/cs/sg/v1/backOutNoticesPos")
    public JSONObject backOutNoticesPos(HttpServletRequest request, @RequestParam(value = "param") String param) {
        if (log.isDebugEnabled()) {
            log.debug("Strat SgPhyOutNoticesCtrl.backOutNoticesPos. ReceiveParams=" + param + ";");
        }
        QuerySessionImpl querySession = new QuerySessionImpl(getUser());
//        QuerySessionImpl querySession = new QuerySessionImpl(request);
        JSONObject paramJo = JSON.parseObject(param);
        DefaultWebEvent event = new DefaultWebEvent("sendOutNoticesPos", request, false);
        event.put("param", paramJo);
        querySession.setEvent(event);
        Object o = ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                SgPhyOutNoticesPosBackCmd.class.getName(), "sg", "1.0");
        ValueHolder result = ((SgPhyOutNoticesPosBackCmd) o).execute(querySession);
        if (log.isDebugEnabled()) {
            log.debug("Finish SgPhyOutNoticesCtrl.backOutNoticesPos. ReturnResult=" + result);
        }
        return result.toJSONObject();
    }

    @ApiOperation(value = "取消出库")
    @PostMapping(path = "/p/cs/sg/v1/cancelOutNotices")
    public JSONObject cancelOutNotices(HttpServletRequest request, @RequestParam(value = "param") String param) {
        if (log.isDebugEnabled()) {
            log.debug("Strat SgPhyOutNoticesCtrl.cancelOutNotices. ReceiveParams=" + param + ";");
        }
        SgPhyOutNoticesCancelRequest model = JSONObject.parseObject(param, SgPhyOutNoticesCancelRequest.class);
        model.setUser(getUser());
        Object o = ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                SgPhyOutNoticesCancelCmd.class.getName(), "sg", "1.0");
        ValueHolderV14 result = ((SgPhyOutNoticesCancelCmd) o).cancelSgPhyOutNotices(model);
        if (log.isDebugEnabled()) {
            log.debug("Finish SgPhyOutNoticesCtrl.cancelOutNotices. ReturnResult=" + result);
        }
        return result.toJSONObject();
    }

    // 测试-出库通知单
    public SgPhyOutNoticesSaveRequest getNotices() {
        SgPhyOutNoticesSaveRequest request = new SgPhyOutNoticesSaveRequest();
        request.setSourceBillId(1l);
        request.setSourceBillNo("2BILL001");
        request.setSourcecode("2CODE001");
        request.setSourceBillType(2);
        request.setOutType(1);
        request.setCpCPhyWarehouseId(10026l);

        return request;
    }

    public List<SgPhyOutNoticesSaveRequest> getNoticesList() {

        List<SgPhyOutNoticesSaveRequest> list = new ArrayList<>();
        for (int i = 1; i < 5; i++) {
            SgPhyOutNoticesSaveRequest request = new SgPhyOutNoticesSaveRequest();
            request.setSourceBillId((long) i);
            request.setSourceBillNo("2BILL00" + i);
            request.setSourcecode("2CODE00" + i);
            request.setSourceBillType(2);
            request.setBillNo("test001");
//            request.setOutType(1);
            list.add(request);
        }

        return list;
    }

    // 测试-出库通知单明细
    public List<SgPhyOutNoticesItemSaveRequest> getNoticesItems() {

        List<SgPhyOutNoticesItemSaveRequest> list = new ArrayList<>();
        for (int i = 1; i < 4; i++) {
            SgPhyOutNoticesItemSaveRequest request = new SgPhyOutNoticesItemSaveRequest();
            request.setSourceBillItemId((long) i);
            request.setPriceList(new BigDecimal(100 + i));
            request.setPriceCost(new BigDecimal(90 + i));
            request.setQty(new BigDecimal(10 + i));
            request.setPsCProId((long) 10000 + i);
            request.setPsCSkuEcode("SKUAS001" + i);
            request.setPsCSpec1Id((long) 100 + i);
            request.setPsCSpec2Id((long) 200 + i);
            list.add(request);
        }
        return list;
    }


    /**
     * 测试用
     *
     * @return
     */
    public UserImpl getUser() {
        UserImpl user = new UserImpl();
        user.setClientId(27);
        user.setOrgId(37);
        user.setId(1000);
        user.setName("宋冬野2s");
        user.setEname("宋小野A8");
        return user;
    }
}
