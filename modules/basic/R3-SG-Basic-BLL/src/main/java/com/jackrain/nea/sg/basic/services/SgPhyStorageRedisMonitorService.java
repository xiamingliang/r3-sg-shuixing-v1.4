package com.jackrain.nea.sg.basic.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.redis.config.CusRedisTemplate;
import com.jackrain.nea.redis.util.RedisOpsUtil;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.config.SgStorageControlConfig;
import com.jackrain.nea.sg.basic.mapper.CpCPhyWarehouseMapper;
import com.jackrain.nea.sg.basic.mapper.SgBPhyStorageMapper;
import com.jackrain.nea.sg.basic.model.request.SgPhyStorageRedisMonitorRequest;
import com.jackrain.nea.sg.basic.model.result.SgPhyStorageRedisMonitorResult;
import com.jackrain.nea.sg.basic.model.result.SgRedisPhyStorageQueryResult;
import com.jackrain.nea.sg.basic.model.table.CpCPhyWarehouse;
import com.jackrain.nea.sg.basic.model.table.SgBPhyStorage;
import com.jackrain.nea.sg.basic.utils.StorageUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.*;

/**
 * @Description: 通用实体仓库存监控接口(Redis)
 * @Author: chenb
 * @Date: 2019/7/29 11:32
 */
@Component
@Slf4j
public class SgPhyStorageRedisMonitorService {

    @Autowired
    SgStorageControlConfig sgStorageControlConfig;

    @Autowired
    private SgBPhyStorageMapper sgBPhyStorageMapper;

    @Autowired
    private CpCPhyWarehouseMapper cpCPhyWarehouseMapper;

    public ValueHolderV14<SgPhyStorageRedisMonitorResult> monitorRedisPhyStorage(SgPhyStorageRedisMonitorRequest request) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgPhyStorageRedisMonitorService.monitorRedisPhyStorage. ReceiveParams:request={};",
                    JSONObject.toJSONString(request));
        }

        long startTime = System.currentTimeMillis();

        ValueHolderV14<SgPhyStorageRedisMonitorResult> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        Map<String, SgRedisPhyStorageQueryResult> redisPhyStorageMap;
        List<SgBPhyStorage> pgSgBPhyStorageResult;
        List<String> phyWarehouseRedisKeysList;
        Set<Object> phyWarehouseRedisKeys;
        List<Long> skuIds;

        SgRedisPhyStorageQueryResult redisResult;
        long totalSuccessNum = 0;
        long totalErrorNum = 0;

        holder = checkServiceParam(request, request.getLoginUser());

        if (ResultCode.FAIL == holder.getCode()) {
            return holder;
        }

        CusRedisTemplate<String, String> redisTemplate = RedisOpsUtil.getStrRedisTemplate();
        DefaultRedisScript<List> redisScript = new DefaultRedisScript<>();
        redisScript.setLocation(new ClassPathResource("lua/QueryRedisPhyStorage.lua"));
        redisScript.setResultType(List.class);

        User loginUser = request.getLoginUser();

        List<CpCPhyWarehouse> cpCPhyWarehouseList = cpCPhyWarehouseMapper.selectList(
                new QueryWrapper<CpCPhyWarehouse>().lambda()
                        .eq(CpCPhyWarehouse::getIsactive, SgConstants.IS_ACTIVE_Y));

        if (CollectionUtils.isEmpty(cpCPhyWarehouseList)) {

            holder.setMessage(Resources.getMessage("Redis实体仓库存监控执行成功！",
                    loginUser.getLocale(),
                    totalSuccessNum, totalErrorNum));

            return holder;
        }

        for (CpCPhyWarehouse cpCPhyWarehouseItem : cpCPhyWarehouseList) {

            phyWarehouseRedisKeys = StorageUtils.scanRedisKeys(
                    SgConstants.REDIS_KEY_PREFIX_PHY_STORAGE + cpCPhyWarehouseItem.getId().toString() + ":",
                    1000);

            phyWarehouseRedisKeysList = new ArrayList((Collection<?>) phyWarehouseRedisKeys);

            if (CollectionUtils.isEmpty(phyWarehouseRedisKeys)) {

                log.info("SgPhyStorageRedisMonitorService.monitorRedisPhyStorage. info :" +
                                "Redis实体仓库存记录为0。实体仓ID:{}",
                        cpCPhyWarehouseItem.getId());

                continue;
            }

            //批量更新单据明细分页
            int pageSize = sgStorageControlConfig.getMaxQueryLimit();
            int listSize = phyWarehouseRedisKeysList.size();
            int page = listSize / pageSize;

            if (listSize % pageSize != 0) {
                page++;
            }

            //分页批量更新
            for (int i = 0; i < page; i++) {

                List<String> keyList = phyWarehouseRedisKeysList.subList(i * pageSize,
                        (((i + 1) * pageSize > listSize ? listSize : pageSize * (i + 1))));

                skuIds = new ArrayList<>();
                redisPhyStorageMap = new HashMap<>();

                for (String redisKey : keyList) {
                    skuIds.add(Long.valueOf(redisKey.split(":")[3]));
                }

                pgSgBPhyStorageResult = sgBPhyStorageMapper.selectList(
                        new QueryWrapper<SgBPhyStorage>().lambda()
                                .eq(SgBPhyStorage::getCpCPhyWarehouseId, cpCPhyWarehouseItem.getId())
                                .in(!CollectionUtils.isEmpty(skuIds), SgBPhyStorage::getPsCSkuId, skuIds));

                List result = redisTemplate.execute(redisScript, keyList);

                if (!CollectionUtils.isEmpty(result) && Integer.valueOf((String) result.get(0)) == ResultCode.SUCCESS) {

                    List<List> phyStorageResultList = (ArrayList) result.get(1);

                    if (!CollectionUtils.isEmpty(phyStorageResultList)) {

                        for (List phyStorageResult : phyStorageResultList) {

                            if (!CollectionUtils.isEmpty(phyStorageResult)) {

                                SgRedisPhyStorageQueryResult queryResult = new SgRedisPhyStorageQueryResult();

                                queryResult.setQtyStorage(new BigDecimal((String) phyStorageResult.get(0)));

                                redisPhyStorageMap.put(((String) phyStorageResult.get(1)).split(":")[3],
                                        queryResult);

                            }

                        }

                    }

                }

                for (SgBPhyStorage pgSgBPhyStorage : pgSgBPhyStorageResult) {

                    redisResult = redisPhyStorageMap.get(pgSgBPhyStorage.getPsCSkuId().toString());

                    if (redisResult == null) {

                        log.error("SgPhyStorageRedisMonitorService.monitorRedisPhyStorage. error :" +
                                        "Redis实体仓库存记录不存在。实体仓ID:{},商品条码ID:{};",
                                pgSgBPhyStorage.getCpCPhyWarehouseId(), pgSgBPhyStorage.getPsCSkuId());

                        totalErrorNum++;

                    } else if (redisResult.getQtyStorage().compareTo(pgSgBPhyStorage.getQtyStorage()) != 0) {

                        log.error("SgPhyStorageRedisMonitorService.monitorRedisPhyStorage. error :" +
                                        "实体仓编码ID:{},商品条码ID:{},PG在库量:{},Redis在库量:{};",
                                pgSgBPhyStorage.getCpCPhyWarehouseId(), pgSgBPhyStorage.getPsCSkuId(),
                                pgSgBPhyStorage.getQtyStorage(), redisResult.getQtyStorage());

                        totalErrorNum++;

                    } else {

                        totalSuccessNum++;
                    }

                    redisPhyStorageMap.remove(pgSgBPhyStorage.getPsCSkuId().toString());
                }

                if (redisPhyStorageMap.size() > 0) {
                    for (String emptySkuId : redisPhyStorageMap.keySet()) {

                        log.error("SgPhyStorageRedisMonitorService.monitorRedisPhyStorage. error :" +
                                        "PG实体仓库存记录不存在。实体仓ID:{},商品条码ID:{};",
                                cpCPhyWarehouseItem.getId(), emptySkuId);

                        totalErrorNum++;
                    }
                }
            }
        }

        holder.setMessage(Resources.getMessage("Redis实体仓库存监控执行成功！",
                loginUser.getLocale(),
                totalSuccessNum, totalErrorNum));

        if (log.isDebugEnabled()) {
            log.debug("Finish SgPhyStorageRedisMonitorService.monitorRedisPhyStorage. ReturnResult:holder={} spend time:{}ms;",
                    JSONObject.toJSONString(holder), System.currentTimeMillis() - startTime);
        }

        return holder;
    }

    /**
     * @param request
     * @param loginUser
     * @return ValueHolderV14
     */
    private ValueHolderV14 checkServiceParam(SgPhyStorageRedisMonitorRequest request,
                                             User loginUser) {

        ValueHolderV14 holder = new ValueHolderV14(ResultCode.SUCCESS, "");

        int maxQueryLimit = sgStorageControlConfig.getMaxQueryLimit();

        List<Long> SkuIds = request.getSkuIds();

        if (!CollectionUtils.isEmpty(SkuIds) &&
                SkuIds.size() > maxQueryLimit) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("商品条码ID的查询件数超过最大限制！",
                    loginUser.getLocale()));
        }

        return holder;
    }
}
