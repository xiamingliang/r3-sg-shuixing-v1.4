package com.jackrain.nea.sg.basic.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.data.basic.model.request.ProInfoQueryRequest;
import com.jackrain.nea.data.basic.model.request.SkuInfoQueryRequest;
import com.jackrain.nea.data.basic.services.BasicPsQueryService;
import com.jackrain.nea.ps.api.result.PsCProSkuResult;
import com.jackrain.nea.ps.api.table.PsCPro;
import com.jackrain.nea.sg.basic.mapper.SgBStorageMapper;
import com.jackrain.nea.sg.basic.model.result.updateStorageBaicInfoResult;
import com.jackrain.nea.sg.basic.model.table.SgBStorage;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @Description: 定时更新逻辑仓库存表基础信息
 * @Author: chenb
 * @Date: 2019/7/11 20:07
 */
@Slf4j
@Component
public class SgStorageBasicInfoUpdateTaskService {

    @Autowired
    private BasicPsQueryService basicPsQueryService;

    @Autowired
    private SgBStorageMapper sgBStorageMapper;

    public ValueHolderV14<updateStorageBaicInfoResult> updateStorageBaicInfo(JSONObject params) {

        ValueHolderV14<updateStorageBaicInfoResult> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        ProInfoQueryRequest proInfoRequest = new ProInfoQueryRequest();
        List<Long> proIdList = new ArrayList<>();
        List<PsCPro> proInfoResult = new ArrayList<>();
        List<PsCPro> proInfoPageResult = null;

        List<SgBStorage> SgBStorageList = sgBStorageMapper.selectList(
                new QueryWrapper<SgBStorage>().select("distinct PS_C_PRO_ID").lambda().eq(
                        SgBStorage::getPsCBrandId, -1)
                        .or().isNull(SgBStorage::getPriceList));

        if (CollectionUtils.isEmpty(SgBStorageList)) {
            log.info("定时更新逻辑仓库存表基础信息成功！详情：待更新的基础信息为空。");
        }

        for (SgBStorage itemInfo : SgBStorageList) {
            if (itemInfo != null && itemInfo.getPsCProId() != null) {
                proIdList.add(itemInfo.getPsCProId());
            }
        }

        //批量更新单据明细分页
        int pageSize = 500;
        int listSize = proIdList.size();
        int page = listSize / pageSize;

        if (listSize % pageSize != 0) {
            page++;
        }

        //分页批量更新
        for (int i = 0; i < page; i++) {

            List<Long> pageList = proIdList.subList(i * pageSize,
                    (((i + 1) * pageSize > listSize ? listSize : pageSize * (i + 1))));

            proInfoRequest.setProIdList(pageList);

            try {
                //调用批量获取商品信息服务
                proInfoPageResult = basicPsQueryService.getProInfo(proInfoRequest);
            } catch (Exception e) {
                holder.setCode(ResultCode.FAIL);
                holder.setMessage(Resources.getMessage("定时更新逻辑仓库存表基础信息失败！原因：调用批量获取商品信息服务失败。"));
                return holder;
            }

            proInfoResult.addAll(proInfoPageResult);
        }

        long updateResult = 0;

        if (!CollectionUtils.isEmpty(proInfoResult)) {

            for (PsCPro proItem : proInfoResult) {

                if (proItem != null && (proItem.getPsCBrandId() != null || proItem.getPricelist() != null)) {
                    SgBStorage updateInfo = new SgBStorage();
                    updateInfo.setPsCProId(proItem.getId());
                    updateInfo.setPsCBrandId(proItem.getPsCBrandId());
                    updateInfo.setPriceList(proItem.getPricelist());

                    updateResult = updateResult + sgBStorageMapper.updateStorageBasicInfo(updateInfo);
                }
            }
        }
        log.info("定时更新逻辑仓库存表基础信息成功！详情：成功更新品牌/吊牌价件数：" + updateResult);
        long updateStorageGBcode = updateStorageGBcode();

        holder.setMessage(Resources.getMessage("定时更新逻辑仓库存表基础信息成功！详情：成功更新基础信息件数：" + (updateResult + updateStorageGBcode)));
        return holder;

    }

    public long updateStorageGBcode() {
        List<SgBStorage> SgBStorageList = sgBStorageMapper.selectList(
                new QueryWrapper<SgBStorage>().select("distinct PS_C_SKU_ECODE").lambda()
                        .isNull(SgBStorage::getGbcode));

        if (CollectionUtils.isEmpty(SgBStorageList)) {
            log.debug("待更新(国标码)的基础信息为空。");
            return 0;
        }


        List<String> skuEcodes = new ArrayList<>();
        List<PsCProSkuResult> skuInfoResult = new ArrayList<>();

        for (SgBStorage itemInfo : SgBStorageList) {
            if (itemInfo != null && StringUtils.isNotEmpty(itemInfo.getPsCSkuEcode())) {
                skuEcodes.add(itemInfo.getPsCSkuEcode());
            }
        }

        //批量更新单据明细分页
        int pageSize = 500;
        int listSize = skuEcodes.size();
        int page = listSize / pageSize;

        if (listSize % pageSize != 0) {
            page++;
        }

        //分页批量更新
        for (int i = 0; i < page; i++) {
            List<String> pageList = skuEcodes.subList(i * pageSize,
                    (((i + 1) * pageSize > listSize ? listSize : pageSize * (i + 1))));

            SkuInfoQueryRequest gbcodeQueryRequest = new SkuInfoQueryRequest();
            gbcodeQueryRequest.setSkuEcodeList(pageList);

            try {
                //调用批量获取商品信息服务
                HashMap<String, PsCProSkuResult> skuResult = basicPsQueryService.getSkuInfoByEcode(gbcodeQueryRequest);
                if (MapUtils.isNotEmpty(skuResult)) {
                    skuInfoResult.addAll(new ArrayList<>(skuResult.values()));
                } else {
                    AssertUtils.logAndThrow("条码" + pageList + ",查询条码信息为空!");
                }
            } catch (Exception e) {
                log.error("定时更新逻辑仓库存表基础信息失败！原因：{}。", e.getMessage());
            }
        }

        long updateResult = 0;

        if (CollectionUtils.isNotEmpty(skuInfoResult)) {

            for (PsCProSkuResult skuItem : skuInfoResult) {

                if (skuItem != null && skuItem.getGbcode() != null) {
                    SgBStorage updateInfo = new SgBStorage();
                    updateInfo.setPsCSkuId(skuItem.getId());
                    updateInfo.setGbcode(skuItem.getGbcode());

                    updateResult = updateResult + sgBStorageMapper.updateStorageGBcode(updateInfo);
                }
            }
        }

        log.info("定时更新逻辑仓库存表基础信息成功！详情：成功更新国标码件数：" + updateResult);
        return updateResult;
    }

}
