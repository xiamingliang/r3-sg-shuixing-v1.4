package com.jackrain.nea.sg.basic.services;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.config.SgStorageKafkaConfig;
import com.jackrain.nea.sg.basic.model.SgTeusStorageUpdateCommonModel;
import com.jackrain.nea.sg.basic.model.request.SgStorageSingleUpdateRequest;
import com.jackrain.nea.sg.basic.model.result.SgTeusStorageBatchUpdateResult;
import com.jackrain.nea.sg.basic.utils.StorageLogUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;


/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/4/29 18:53
 */
@Component
@Slf4j
public class SgTeusStorageRedisSynchService {

    @Autowired
    private SgStorageKafkaConfig mqConfig;

    @Autowired
    private KafkaProducer<String, String> kafkaSender;

    /**
     * @param request
     * @return
     */
    public ValueHolderV14<SgTeusStorageBatchUpdateResult> synchRedisToStorage(SgStorageSingleUpdateRequest request,
                                                                              List<SgTeusStorageUpdateCommonModel> redisSynchMqItemList) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgTeusStorageRedisSynchService.synchRedisToStorage. ReceiveParams:BillNo:{};"
                    , JSONObject.toJSONString(request.getBill().getBillNo()));
        }

        ValueHolderV14<SgTeusStorageBatchUpdateResult> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        HashMap body = new HashMap<>();
        String sendBody;

        //检查入参
        holder = checkServiceParam(request, request.getLoginUser());
        if (ResultCode.FAIL == holder.getCode()) {
            return holder;
        }

        String billNo = request.getBill().getBillNo();

        if (CollectionUtils.isEmpty(redisSynchMqItemList)) {
            log.warn("SgTeusStorageRedisSynchService.synchRedisToStorage. 同步逻辑仓箱库存消息待发送消息为空！单据编号:{}",
                    billNo);
            return holder;
        }

        try {

            String msgTag;
            long recordTimestamp = System.currentTimeMillis();
            int partition;

            if (!StringUtils.isEmpty(billNo) && billNo.length() >= 2) {
                switch (billNo.substring(0, 2)) {
                    case SgConstants.BILL_PREFIX_ADJUST:
                        msgTag = SgConstants.MSG_TAG_ADJUST;
                        break;
                    case SgConstants.BILL_PREFIX_PHY_ADJUST:
                        msgTag = SgConstants.MSG_TAG_PHY_ADJUST;
                        break;
                    case SgConstants.BILL_PREFIX_SEND:
                        msgTag = SgConstants.MSG_TAG_SEND;
                        break;
                    case SgConstants.BILL_PREFIX_RECEIVE:
                        msgTag = SgConstants.MSG_TAG_RECEIVE;
                        break;
                    case SgConstants.BILL_PREFIX_PHY_IN_RESULT:
                        msgTag = SgConstants.MSG_TAG_PHY_IN_RESULT;
                        break;
                    case SgConstants.BILL_PREFIX_PHY_OUT_RESULT:
                        msgTag = SgConstants.MSG_TAG_PHY_OUT_RESULT;
                        break;
                    default:
                        msgTag = "others";
                }
            } else {
                msgTag = "others";
            }

            String messageKey = SgConstants.MSG_TAG_HEAD_TEUS_STORAGE + msgTag + ":" + billNo + ":" + recordTimestamp;

            for (SgTeusStorageUpdateCommonModel commonModel : redisSynchMqItemList) {

                partition = (int) ((commonModel.getCpCStoreId() + commonModel.getPsCTeusId()) %
                        mqConfig.getTotalnumber());

                commonModel.setMessageKey(messageKey);

                body.put("param", commonModel);
                body.put("user", request.getLoginUser());
                sendBody = JSONObject.toJSONString(body);

                ProducerRecord<String, String> record = new ProducerRecord<String, String>(
                        mqConfig.getStorageUpdateKafkaTopic(),
                        partition,
                        recordTimestamp,
                        messageKey,
                        sendBody);

                kafkaSender.send(record);

                if (log.isDebugEnabled()) {
                    log.debug("SgTeusStorageRedisSynchService.synchRedisToStorage. kafka sendMsg:topic:{}," +
                                    "partition:{},messageKey:{},body:{};"
                            , mqConfig.getStorageUpdateKafkaTopic(), partition, messageKey, sendBody);
                }

            }

        } catch (Exception e) {

            StorageLogUtils.getErrMessage(e, request.getMessageKey(), request.getLoginUser());

            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("Redis同步逻辑仓箱库存消息发送失败！", request.getLoginUser().getLocale(), billNo));

        }

        if (log.isDebugEnabled()) {
            log.debug("Finish SgTeusStorageRedisSynchService.synchRedisToStorage. ReturnResult:billNo:{};"
                    , request.getBill().getBillNo());
        }

        return holder;
    }

    /**
     * @param request
     * @param loginUser
     * @return
     */
    private ValueHolderV14<SgTeusStorageBatchUpdateResult> checkServiceParam(SgStorageSingleUpdateRequest request,
                                                                             User loginUser) {

        ValueHolderV14<SgTeusStorageBatchUpdateResult> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");

        if (request == null) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("请求体不能为空！", loginUser.getLocale()));
            return holder;
        }

        if (request.getBill() == null) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("单据信息不能为空！", loginUser.getLocale()));
        }

        if (CollectionUtils.isEmpty(request.getBill().getTeusList())) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("单据箱明细信息不能为空！", loginUser.getLocale()));
        }

        return holder;

    }

}
