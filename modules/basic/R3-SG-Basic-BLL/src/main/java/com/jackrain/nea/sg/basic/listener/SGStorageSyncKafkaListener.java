package com.jackrain.nea.sg.basic.listener;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.consumer.SgBStorageBatchUpdateKafkaConsumer;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/9/11 18:53
 */
@Component
@Slf4j
@ConditionalOnProperty(
        name = {"r3.kafka.consumer.start"},
        havingValue = "true"
)
public class SGStorageSyncKafkaListener {

    @Autowired
    private SgBStorageBatchUpdateKafkaConsumer sgBStorageBatchUpdateKafkaConsumer;

    /**
     * Kafka监听器
     *
     * @param recordList Kafka消息列表
     */
    @KafkaListener(topics = "${r3.kafka.producer.storage.topic}",
            containerFactory = "kafkaListenerContainerFactory")
    public ValueHolderV14 consume(List<ConsumerRecord<String, String>> recordList) {

        ValueHolderV14 holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");

        log.info("Start SGStorageSyncKafkaListener.consume. ReceiveParams:recordList size={};",
                recordList.size());

        if (CollectionUtils.isEmpty(recordList)) {
            log.info("SGStorageSyncKafkaListener.consume. message is empty！");
            return holder;
        }

        try {
            sgBStorageBatchUpdateKafkaConsumer.consume(recordList);
        } catch (Exception e) {
            log.error("SGStorageSyncKafkaListener.consume. error!!! info={};", e);

            holder.setCode(ResultCode.FAIL);
            holder.setMessage("SGStorageSyncKafkaListener.consume. error!!!");
        }

        log.info("Finish SGStorageSyncKafkaListener.consume. ReturnResult:holder={};",
                JSONObject.toJSONString(holder));

        return holder;

    }
}