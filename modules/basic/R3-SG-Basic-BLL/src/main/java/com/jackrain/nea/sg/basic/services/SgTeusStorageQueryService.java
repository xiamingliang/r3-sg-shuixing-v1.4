package com.jackrain.nea.sg.basic.services;

import com.github.pagehelper.PageInfo;
import com.jackrain.nea.sg.basic.logic.SgTeusStorageQueryLogic;
import com.jackrain.nea.sg.basic.model.request.SgTeusStoragePageQueryRequest;
import com.jackrain.nea.sg.basic.model.request.SgTeusStorageQueryRequest;
import com.jackrain.nea.sg.basic.model.table.SgBTeusStorage;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author zhu lin yu
 * @since 2019-11-01
 * create at : 2019-11-01 10:31
 */
@Component
@Slf4j
public class SgTeusStorageQueryService {

    @Autowired
    private SgTeusStorageQueryLogic sgTeusStorageQueryLogic;


    public ValueHolderV14<List<SgBTeusStorage>> queryTeusStorage(SgTeusStorageQueryRequest request, User loginUser) {

        return sgTeusStorageQueryLogic.queryTeusStorage(request, null, loginUser, Boolean.FALSE);

    }

    public ValueHolderV14<PageInfo<SgBTeusStorage>> queryTeusStoragePage(SgTeusStoragePageQueryRequest request, User loginUser) {

        return sgTeusStorageQueryLogic.queryTeusStorage(request.getQueryRequest(), request.getPageRequest(), loginUser, Boolean.FALSE);

    }

    public ValueHolderV14<List<SgBTeusStorage>> queryTeusStorageExclZero(SgTeusStorageQueryRequest request, User loginUser) {

        return sgTeusStorageQueryLogic.queryTeusStorage(request, null, loginUser, Boolean.TRUE);

    }

}
