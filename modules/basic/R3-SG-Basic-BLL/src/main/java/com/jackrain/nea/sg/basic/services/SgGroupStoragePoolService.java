package com.jackrain.nea.sg.basic.services;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.redis.config.CusRedisTemplate;
import com.jackrain.nea.redis.util.RedisOpsUtil;
import com.jackrain.nea.sg.basic.api.SgGroupStorageCalculateCmd;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.model.request.*;
import com.jackrain.nea.sg.basic.model.result.SgGoupStorageCalculateResult;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;

/**
 * @author 舒威
 * @since 2019/7/8
 * create at : 2019/7/8 17:37
 */
@Slf4j
@Component
public class SgGroupStoragePoolService {

    public ValueHolderV14<SgGoupStorageCalculateResult> addGroupStoragePoll(SgGroupStoragePoolRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("Start SgGroupStoragePoolService.addGroupStoragePoll:request:{};", JSONObject.toJSONString(request));
        }

        long startTime = System.currentTimeMillis();
        ValueHolderV14<SgGoupStorageCalculateResult> vh = new ValueHolderV14<>();
        vh.setCode(ResultCode.SUCCESS);
        vh.setMessage("success");

        User loginUser = request.getLoginUser();
        AssertUtils.notNull(loginUser, "用户未登录！");
        AssertUtils.notEmpty(request.getPoolBillRequests(), "单据明细不能为空!");

        List<SgGroupStorageVirtualSkuRequest> list = Lists.newArrayList();
        CusRedisTemplate<String, String> redisTemplate = RedisOpsUtil.getStrRedisTemplate();
        //记录已转换的虚拟条码 避免重复取
        HashMap<String, Object> varMap = Maps.newHashMap();
        for (SgGroupStoragePoolBillRequest poolBillRequest : request.getPoolBillRequests()) {
            List<SgGroupStoragePoolExcuteRequest> itemList = poolBillRequest.getItemList();
            String changeTime = poolBillRequest.getChangeTime();
            for (SgGroupStoragePoolExcuteRequest poolExcuteRequest : itemList) {
                Long storeId = poolExcuteRequest.getCpCStoreId();
                Long skuId = poolExcuteRequest.getPsCSkuId();
                Long cpCShopId = poolExcuteRequest.getCpCShopId();
                String billNo = poolExcuteRequest.getBillNo();
                if (storeId == null || skuId == null) {
                    log.debug("逻辑仓或条码id为空！");
                    continue;
                }
                String key = SgConstantsIF.COMBINED_SKU + ":" + skuId;
                log.debug("RedisKey:" + key);
                if (redisTemplate.opsForHash().get(key, storeId.toString()) == null) {
                    log.debug(key + "不存在于组合(福袋)商品或未启用");
                } else {
                    String param = (String) redisTemplate.opsForHash().get(key, storeId.toString());
                    list.addAll(skuConvertSkuGroup(param, storeId, redisTemplate, varMap, cpCShopId, billNo, changeTime));
                }
            }
        }

        if (CollectionUtils.isEmpty(list)) {
            vh.setCode(ResultCode.SUCCESS);
            vh.setMessage("不存在于组合(福袋)商品或未启用");
        } else {
            /*调用【组合商品库存计算】服务*/
            SgGroupStorageCalculateCmd calculateCmd = (SgGroupStorageCalculateCmd) ReferenceUtil.refer(
                    ApplicationContextHandle.getApplicationContext(),
                    SgGroupStorageCalculateCmd.class.getName(),
                    SgConstantsIF.GROUP, SgConstantsIF.VERSION);
            SgGroupStorageCalculateRequest updateStorageRequest = new SgGroupStorageCalculateRequest();
            updateStorageRequest.setVirtualSkuList(list);
            updateStorageRequest.setLoginUser(loginUser);
            vh = calculateCmd.calculateGroupStorage(updateStorageRequest);
        }

        if (log.isDebugEnabled()) {
            log.debug("Finish SgCombinedCommodityPoolService.ReturnResults:return:{} spend time:{}ms;",
                    vh, System.currentTimeMillis() - startTime);
        }
        return vh;
    }

    /**
     * 根据实际条码获取虚拟条码
     *
     * @param param         实际条码redis
     * @param storeId       逻辑仓id
     * @param redisTemplate redis
     * @param varMap        记录已转换的虚拟条码 避免重复取
     * @param cpCShopId     渠道id
     * @param billNo        单据编号
     * @return 所有引用到实际条码的虚拟条码集合
     */
    public List<SgGroupStorageVirtualSkuRequest> skuConvertSkuGroup(String param, Long storeId, CusRedisTemplate<String, String> redisTemplate,
                                                                    HashMap<String, Object> varMap, Long cpCShopId, String billNo, String changeTime) {
        List<SgGroupStorageVirtualSkuRequest> list = Lists.newArrayList();
        if (StringUtils.isNotEmpty(param)) {
            JSONObject paramObj = JSONObject.parseObject(param);
            JSONArray skugroup = paramObj.getJSONArray("SKUGROUP");
            if (CollectionUtils.isNotEmpty(skugroup)) {
                for (Object o : skugroup) {
                    JSONObject var = (JSONObject) o;
                    //虚拟条码id
                    Long skugroupId = var.getLong("ID");
                    String key = SgConstantsIF.COMBINED_SKUGROUP + ":" + skugroupId;
                    if (varMap.containsKey(key + ":" + storeId.toString())) {
                        continue;
                    } else {
                        varMap.put(key + ":" + storeId.toString(), null);
                        Object var2 = redisTemplate.opsForHash().get(key, storeId.toString());
                        if (var2 != null) {
                            try {
                                String str = (String) var2;
                                SgGroupStorageVirtualSkuRequest skuGroupRequest =
                                        JSONObject.parseObject(str, SgGroupStorageVirtualSkuRequest.class);
                                skuGroupRequest.setCpCShopId(cpCShopId);
                                skuGroupRequest.setBillNo(billNo);
                                list.add(skuGroupRequest);
                            } catch (Exception e) {
                                e.printStackTrace();
                                log.debug("key:" + key + ",redis参数解析异常！");
                            }

                        }
                    }
                }
            }
        }
        return list;
    }
}
