package com.jackrain.nea.sg.basic.services;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.mapper.CpStoreMapper;
import com.jackrain.nea.sg.basic.model.table.CpCStore;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author: zhu lin yu
 * @since: 2019/4/19
 * create at : 2019/4/19 17:01
 */
@Component
@Slf4j
public class CpStoreService {

    @Autowired
    private CpStoreMapper cpStoreMapper;

    /**
     * 店仓新增
     *
     * @param jsonObject
     */
    @Transactional(rollbackFor = Exception.class)
    public int insertStore(JSONObject jsonObject) {

        CpCStore cpCStore = cpStoreMapper.selectOne(new QueryWrapper<CpCStore>().lambda().eq(CpCStore::getCpCStoreEcode, jsonObject.getString("ECODE").trim()));
        if (cpCStore != null) {
            updateStore(jsonObject);
        }
        //参数封装
        JSONObject jo = new JSONObject();
        if (jsonObject.getLong("ID") != null) {
            jo.put("ID", jsonObject.getLong("ID"));
        }
        jo.put("CP_C_STORE_ECODE", jsonObject.getString("ECODE"));
        jo.put("CP_C_STORE_ENAME", jsonObject.getString("ENAME"));
        jo.put("CP_C_PHY_WAREHOUSE_ID", jsonObject.getLong("CP_C_PHY_WAREHOUSE_ID"));
        jo.put("CP_C_PHY_WAREHOUSE_ECODE", jsonObject.getString("CP_C_PHY_WAREHOUSE_ECODE"));
        jo.put("CP_C_PHY_WAREHOUSE_ENAME", jsonObject.getString("CP_C_PHY_WAREHOUSE_ENAME"));
//        jsonObject1.put("VERSION")
        jo.put("AD_ORG_ID", jsonObject.getLong("AD_ORG_ID"));
        jo.put("ISACTIVE", SgConstants.IS_ACTIVE_Y);
        jo.put("AD_CLIENT_ID", jsonObject.getLong("AD_CLIENT_ID"));
        jo.put("OWNERID", jsonObject.getLong("OWNERID"));
        jo.put("OWNERENAME", jsonObject.getString("OWNERENAME"));
        jo.put("OWNERNAME", jsonObject.getString("OWNERNAME"));
        jo.put("CREATIONDATE", jsonObject.getTimestamp("CREATIONDATE"));
        jo.put("MODIFIERID", jsonObject.getLong("MODIFIERID"));
        jo.put("MODIFIERENAME", jsonObject.getString("MODIFIERENAME"));
        jo.put("MODIFIERNAME", jsonObject.getString("MODIFIERNAME"));
        jo.put("MODIFIEDDATE", jsonObject.getTimestamp("MODIFIEDDATE"));
        jo.put("IS_MAIN_WAREHOUSE", jsonObject.getInteger("IS_MAIN_WAREHOUSE"));
        jo.put("WMS_ACCOUNT", jsonObject.getString("WMS_ACCOUNT"));
        jo.put("ISNEGATIVE", jsonObject.getLong("ISNEGATIVE"));
        jo.put("IS_AUTO_IN", jsonObject.getString("IS_AUTO_IN"));
        jo.put("CONTACTER", jsonObject.getString("CONTACTER"));
        jo.put("MOBIL", jsonObject.getString("MOBIL"));
        jo.put("PHONE", jsonObject.getString("PHONE"));
        jo.put("FAX", jsonObject.getString("FAX"));
        jo.put("EMAIL", jsonObject.getString("EMAIL"));
        jo.put("IS_SHARE", jsonObject.getString("IS_SHARE"));
        jo.put("ECSTORE", jsonObject.getLong("ECSTORE"));
        jo.put("ISOUT", jsonObject.getLong("ISOUT"));
        jo.put("ISRECEPT", jsonObject.getLong("ISRECEPT"));
        jo.put("ISDISCENTER", jsonObject.getLong("ISDISCENTER"));
        jo.put("ACCESS_NO_BILL", jsonObject.getString("ACCESS_NO_BILL"));
        jo.put("RECYCLE", jsonObject.getLong("RECYCLE"));
        jo.put("RETCYCLE", jsonObject.getLong("RETCYCLE"));
        jo.put("DATEINV", jsonObject.getLong("DATEINV"));
        jo.put("CLOSEDATE", jsonObject.getLong("CLOSEDATE"));
        jo.put("PLAN_CHECK_DATE", jsonObject.getLong("PLAN_CHECK_DATE"));
        jo.put("IS_AUTO_OUT", jsonObject.getString("IS_AUTO_OUT"));
        jo.put("STORETYPE", jsonObject.getString("STORETYPE"));
        jo.put("STORE_CONFIRM_TIME", jsonObject.getLong("STORE_CONFIRM_TIME"));

        //店仓新增
        return this.cpStoreMapper.insertStore(jo);
    }

    /**
     * 店仓跟新
     *
     * @param jsonObject
     */
    @Transactional(rollbackFor = Exception.class)
    public int updateStore(JSONObject jsonObject) {

        log.error(this.getClass().getName() + ",getJsonObject:" + JSON.toJSONString(jsonObject));
        //参数封装
        JSONObject jo = new JSONObject();
        if (jsonObject.containsKey("ID")) {
            jo.put("ID", jsonObject.getLong("ID"));
        }
        if (jsonObject.containsKey("CP_C_PHY_WAREHOUSE_ID")) {
            jo.put("CP_C_PHY_WAREHOUSE_ID", jsonObject.getLong("CP_C_PHY_WAREHOUSE_ID"));
        }
        if (jsonObject.containsKey("CP_C_PHY_WAREHOUSE_ECODE")) {
            jo.put("CP_C_PHY_WAREHOUSE_ECODE", jsonObject.getString("CP_C_PHY_WAREHOUSE_ECODE"));
        }
        if (jsonObject.containsKey("CP_C_PHY_WAREHOUSE_ENAME")) {
            jo.put("CP_C_PHY_WAREHOUSE_ENAME", jsonObject.getString("CP_C_PHY_WAREHOUSE_ENAME"));
        }
        if (jsonObject.containsKey("AD_ORG_ID")) {
            jo.put("AD_ORG_ID", jsonObject.getLong("AD_ORG_ID"));
        }
        if (jsonObject.containsKey("ISACTIVE")) {
            jo.put("ISACTIVE", jsonObject.getString("ISACTIVE"));
        }
        if (jsonObject.containsKey("MODIFIERID")) {
            jo.put("MODIFIERID", jsonObject.getLong("MODIFIERID"));
        }
        if (jsonObject.containsKey("MODIFIERENAME")) {
            jo.put("MODIFIERENAME", jsonObject.getString("MODIFIERENAME"));
        }
        if (jsonObject.containsKey("MODIFIERNAME")) {
            jo.put("MODIFIERNAME", jsonObject.getString("MODIFIERNAME"));
        }
        if (jsonObject.containsKey("MODIFIEDDATE")) {
            jo.put("MODIFIEDDATE", jsonObject.getTimestamp("MODIFIEDDATE"));
        }
        if (jsonObject.containsKey("IS_MAIN_WAREHOUSE")) {
            jo.put("IS_MAIN_WAREHOUSE", jsonObject.getInteger("IS_MAIN_WAREHOUSE"));
        }
        if (jsonObject.containsKey("WMS_ACCOUNT")) {
            jo.put("WMS_ACCOUNT", jsonObject.getString("WMS_ACCOUNT"));
        }
        if (jsonObject.containsKey("ISNEGATIVE")) {
            jo.put("ISNEGATIVE", jsonObject.getLong("ISNEGATIVE"));
        }
        if (jsonObject.containsKey("IS_AUTO_IN")) {
            jo.put("IS_AUTO_IN", jsonObject.getString("IS_AUTO_IN"));
        }
        if (jsonObject.containsKey("CONTACTER")) {
            jo.put("CONTACTER", jsonObject.getString("CONTACTER"));
        }
        if (jsonObject.containsKey("MOBIL")) {
            jo.put("MOBIL", jsonObject.getString("MOBIL"));
        }
        if (jsonObject.containsKey("PHONE")) {
            jo.put("PHONE", jsonObject.getString("PHONE"));
        }
        if (jsonObject.containsKey("FAX")) {
            jo.put("FAX", jsonObject.getString("FAX"));
        }
        if (jsonObject.containsKey("EMAIL")) {
            jo.put("EMAIL", jsonObject.getString("EMAIL"));
        }
        if (jsonObject.containsKey("IS_SHARE")) {
            jo.put("IS_SHARE", jsonObject.getString("IS_SHARE"));
        }
        if (jsonObject.containsKey("ECSTORE")) {
            jo.put("ECSTORE", jsonObject.getLong("ECSTORE"));
        }
        if (jsonObject.containsKey("ISOUT")) {
            jo.put("ISOUT", jsonObject.getLong("ISOUT"));
        }
        if (jsonObject.containsKey("ISRECEPT")) {
            jo.put("ISRECEPT", jsonObject.getLong("ISRECEPT"));
        }
        if (jsonObject.containsKey("ISDISCENTER")) {
            jo.put("ISDISCENTER", jsonObject.getLong("ISDISCENTER"));
        }
        if (jsonObject.containsKey("ACCESS_NO_BILL")) {
            jo.put("ACCESS_NO_BILL", jsonObject.getString("ACCESS_NO_BILL"));
        }
        if (jsonObject.containsKey("RECYCLE")) {
            jo.put("RECYCLE", jsonObject.getLong("RECYCLE"));
        }
        if (jsonObject.containsKey("RETCYCLE")) {
            jo.put("RETCYCLE", jsonObject.getLong("RETCYCLE"));
        }
        if (jsonObject.containsKey("DATEINV")) {
            jo.put("DATEINV", jsonObject.getLong("DATEINV"));
        }
        if (jsonObject.containsKey("CLOSEDATE")) {
            jo.put("CLOSEDATE", jsonObject.getLong("CLOSEDATE"));
        }
        if (jsonObject.containsKey("PLAN_CHECK_DATE")) {
            jo.put("PLAN_CHECK_DATE", jsonObject.getLong("PLAN_CHECK_DATE"));
        }
        if (jsonObject.containsKey("IS_AUTO_OUT")) {
            jo.put("IS_AUTO_OUT", jsonObject.getString("IS_AUTO_OUT"));
        }
        if (jsonObject.containsKey("STORETYPE")) {
            jo.put("STORETYPE", jsonObject.getString("STORETYPE"));
        }

        //店仓更新
        return this.cpStoreMapper.updateStore(jo);
    }

    /**
     * 店仓删除
     */
    @Transactional(rollbackFor = Exception.class)
    public int deleteStore(Long id) {
        //店仓删除
        return this.cpStoreMapper.deleteById(id);
    }
}
