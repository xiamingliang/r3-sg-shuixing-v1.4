package com.jackrain.nea.sg.basic.services;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.mapper.RptSgBPhyStorageDailyMapper;
import com.jackrain.nea.sg.basic.model.request.SgPhyStorageDailyGenerateRequest;
import com.jackrain.nea.sg.basic.model.result.SgPhyStorageDailyGenerateResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @Description: 通用实体仓逐日库存生成接口
 * @Author: chenb
 * @Date: 2019/7/29 11:32
 */
@Component
@Slf4j
public class SgPhyStorageDailyGenerateService {

    @Autowired
    private RptSgBPhyStorageDailyMapper rptSgBPhyStorageDailyMapper;

    public ValueHolderV14<SgPhyStorageDailyGenerateResult> generateSgPhyStorageDaily(SgPhyStorageDailyGenerateRequest request) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgPhyStorageDailyGenerateService.generateSgPhyStorageDaily. ReceiveParams:request={};",
                    JSONObject.toJSONString(request));
        }

        long startTime = System.currentTimeMillis();

        ValueHolderV14<SgPhyStorageDailyGenerateResult> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        User loginUser = request.getLoginUser();
        String generateDate = null;

        int result = 0;

        try {
            if (request == null ||
                    (!StringUtils.isEmpty(request.getGenerateDate()) && sdf.parse(request.getGenerateDate()) == null)) {
                holder.setMessage(Resources.getMessage("通用实体仓逐日库存生成成功！ 请求体为空", loginUser.getLocale()));
                return holder;
            }
        } catch (ParseException e) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("通用实体仓逐日库存生成失败！ 指定日期格式错误;", loginUser.getLocale()));
            return holder;
        }

        try {

            if (!StringUtils.isEmpty(request.getGenerateDate())){
                generateDate = request.getGenerateDate().substring(0, 8);
            } else {
                generateDate = sdf.format(DateUtils.addDays(new Date(), -1));
            }

            if (log.isDebugEnabled()) {
                log.debug("SgPhyStorageDailyGenerateService.generateSgPhyStorageDaily. generatePhyStorageDailyFn.start. ReceiveParams:generateDate:{};"
                        , JSONObject.toJSONString(generateDate));
            }

            result = rptSgBPhyStorageDailyMapper.generatePhyStorageDailyFn(generateDate);

            if (log.isDebugEnabled()) {
                log.debug("SgPhyStorageDailyGenerateService.generateSgPhyStorageDaily. generatePhyStorageDailyFn.end. ReturnResult:result:{};"
                        , JSONObject.toJSONString(result));
            }

            if (log.isDebugEnabled()) {
                log.debug("SgPhyStorageDailyGenerateService.generateSgPhyStorageDaily. generatePhyTeusStorageDailyFn.start. ReceiveParams:generateDate:{};"
                        , JSONObject.toJSONString(generateDate));
            }

            result = rptSgBPhyStorageDailyMapper.generatePhyTeusStorageDailyFn(generateDate);

            if (log.isDebugEnabled()) {
                log.debug("SgPhyStorageDailyGenerateService.generateSgPhyStorageDaily. generatePhyTeusStorageDailyFn.end. ReturnResult:result:{};"
                        , JSONObject.toJSONString(result));
            }

        } catch (Exception e) {

            log.error("SgPhyStorageDailyGenerateService.generateSgPhyStorageDaily. error ", e);

            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("通用实体仓逐日库存生成失败！", loginUser.getLocale()));
            return holder;
        }

        if (log.isDebugEnabled()) {
            log.debug("Finish SgPhyStorageDailyGenerateService.generateSgPhyStorageDaily. ReturnResult:holder={} spend time:{}ms;",
                    JSONObject.toJSONString(holder), System.currentTimeMillis() - startTime);
        }

        return holder;

    }


}
