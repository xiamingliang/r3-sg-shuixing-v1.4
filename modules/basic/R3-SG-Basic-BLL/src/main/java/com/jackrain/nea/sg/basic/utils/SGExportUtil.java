package com.jackrain.nea.sg.basic.utils;

import com.aliyun.oss.OSSClient;
import com.jackrain.nea.web.face.User;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.*;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Created with IntelliJ IDEA.
 * User: zyj
 * Date: 2019/7/29
 * Time: 21:13
 * Description:报表导出工具类
 */
@Slf4j
@Component
@Data
public class SGExportUtil {
    private String endpoint;

    private String accessKeyId;

    private String accessKeySecret;

    private String bucketName;

    private String timeout;

    public String saveFileAndPutOss(Workbook hssfWorkbook, String fileName, User user, String fileAddress) {
        // 第六步，将文件存到指定位置
        try {
            // 上传文件。
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            hssfWorkbook.write(baos);
            ByteArrayInputStream swapStream = new ByteArrayInputStream(baos.toByteArray());
            return this.updateSheet(swapStream, fileName, user, fileAddress);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }
    public String updateSheet(ByteArrayInputStream file, String fileName, User loginUser, String fileAddress) {
        String prefixKey = fileAddress;
        // 上传文件。
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
        String name = fileName + loginUser.getName() + sdf.format(new Date());//文件名称
        prefixKey = prefixKey + name + ".xlsx";
        // Endpoint以杭州为例，其它Region请按实际情况填写。
        String endpoint = this.endpoint;
        // 阿里云主账号AccessKey拥有所有API的访问权限，风险很高。强烈建议您创建并使用RAM账号进行API访问或日常运维，请登录 https://ram.console.aliyun.com 创建RAM账号。
        String accessKeyId = this.accessKeyId;
        String accessKeySecret = this.accessKeySecret;
        // 创建OSSClient实例。
        OSSClient ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);
        // 判断文件是否存在。doesObjectExist还有一个参数isOnlyInOSS，如果为true则忽略302重定向或镜像；如果为false，则考虑302重定向或镜像。
        boolean found = ossClient.doesObjectExist(bucketName, prefixKey);
        if (found) {
            try {
                Thread.sleep(10);
            } catch (Exception e) {
            }
            String newfileName = fileName + loginUser.getName() + sdf.format(new Date());
            prefixKey = fileAddress + newfileName + ".xlsx";
            return getUrl(file, prefixKey, ossClient);
        } else {
            return getUrl(file, prefixKey, ossClient);
        }
    }
    private String getUrl(ByteArrayInputStream file, String prefixKey, OSSClient ossClient) {
        // 设置URL过期时间。
        Date expiration = new Date(System.currentTimeMillis() + Long.valueOf(this.getTimeout()));
        // 生成以GET方法访问的签名URL，访客可以直接通过浏览器访问相关内容。
        URL url = ossClient.generatePresignedUrl(bucketName, prefixKey, expiration);
        try {
            ossClient.putObject(this.bucketName, prefixKey, file);
        } finally {
            // 关闭OSSClient。
            ossClient.shutdown();
        }
        return url.toString();
    }
    /**
     * 导出多个sheet
     */
    public XSSFWorkbook executeSheet(XSSFWorkbook wb, String sheetName, String titleName, List<String> columnNames, List<String> keys, List dataList, Boolean sortNeed) {
        // 第一步，创建一个webbook，对应一个Excel文件
        // 第二步，在webbook中添加一个sheet,对应Excel文件中的sheet
        XSSFSheet sheet = wb.createSheet(sheetName);
        //sheet.setDefaultColumnWidth(20); //统一设置列宽
        int firstRowNum = 0;
        if (StringUtils.isNotEmpty(titleName)) {
            // 创建第0行 也就是标题
            XSSFRow row1 = sheet.createRow(firstRowNum);
            row1.setHeightInPoints(50);// 设备标题的高度
            // 第三步创建标题的单元格样式style2以及字体样式headerFont1
            XSSFCellStyle style2 = wb.createCellStyle();
            style2.setAlignment(HorizontalAlignment.CENTER);
            style2.setVerticalAlignment(VerticalAlignment.CENTER);

            style2.setFillForegroundColor((short) 0x29);
            //style2.setFillForegroundColor(HSSFColor.HSSFColorPredefined.LIGHT_TURQUOISE.getIndex());
            style2.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            XSSFFont headerFont1 = wb.createFont(); // 创建字体样式
            headerFont1.setBold(Boolean.TRUE); // 字体加粗
            headerFont1.setFontName("黑体"); // 设置字体类型
            headerFont1.setFontHeightInPoints((short) 15); // 设置字体大小
            style2.setFont(headerFont1); // 为标题样式设置字体样式
            XSSFCell cell1 = row1.createCell(0);// 创建标题第一列
            sheet.addMergedRegion(new CellRangeAddress(0, 0, 0,
                    columnNames.size())); // 合并列标题
            cell1.setCellValue(titleName); // 设置值标题
            cell1.setCellStyle(style2); // 设置标题样式
            firstRowNum++;
        }
        // 创建第1行 也就是表头
        XSSFRow row = sheet.createRow(firstRowNum);
        row.setHeightInPoints(15);// 设置表头高度

        // 第四步，创建表头单元格样式 以及表头的字体样式
        XSSFCell cell = null;
        if (sortNeed) {
            cell = row.createCell(0);
            cell.setCellValue("序号");
        }
        for (int i = 1; i <= columnNames.size(); i++) {
            if (sortNeed) {
                cell = row.createCell(i);
                cell.setCellValue(columnNames.get(i - 1));
            } else {
                cell = row.createCell(i - 1);
                cell.setCellValue(columnNames.get(i - 1));
            }
        }
        int index = 1;
        XSSFCellStyle cellStyle1 = wb.createCellStyle();
        cellStyle1.setAlignment(HorizontalAlignment.LEFT);
        for (int a = 0; a < dataList.size(); a++) {
            int s = sheet.getLastRowNum();
            row = sheet.createRow(sheet.getLastRowNum() + 1);
            // 为数据内容设置特点新单元格样式1 自动换行 上下居中
            XSSFCellStyle autoCellStyle = wb.createCellStyle();
            XSSFCell datacell = null;
            if (sortNeed) {
                datacell = row.createCell(0);
                datacell.setCellValue(String.valueOf(index));
            }
            for (int j = 1; j <= keys.size(); j++) {
                if (sortNeed) {
                    datacell = row.createCell(j);
                } else {
                    datacell = row.createCell(j - 1);
                }
                datacell.setCellStyle(cellStyle1);
                Object cellValue = getFieldValueByName(keys.get(j - 1), dataList.get(a));
                if (Objects.isNull(cellValue)) {
                    cellValue = StringUtils.EMPTY;
                }
                if (cellValue instanceof BigDecimal) {
                    datacell.setCellValue(((BigDecimal) cellValue).doubleValue());
                } else if (cellValue instanceof Double) {
                    datacell.setCellValue(((Double) cellValue).doubleValue());
                } else if (cellValue instanceof Short) {
                    datacell.setCellValue(((Short) cellValue).doubleValue());
                } else if (cellValue instanceof Integer) {
                    datacell.setCellValue(((Integer) cellValue).doubleValue());
                } else if (cellValue instanceof Date) {
                    if (null != cellValue && cellValue != StringUtils.EMPTY) {
                        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        datacell.setCellValue(df.format(cellValue));
                    }
                } else {
                    datacell.setCellValue(String.valueOf(cellValue));
                }
            }
            index++;
        }
        return wb;
    }

    /**
     * 利用反射  根据属性名获取属性值
     */
    private static Object getFieldValueByName(String fieldName, Object o) {
        try {
            if (o instanceof Map) {
                return ((Map) o).get(fieldName);
            } else {
                String firstLetter = fieldName.substring(0, 1).toUpperCase();
                String getter = "get" + firstLetter + fieldName.substring(1);
                Method method = o.getClass().getMethod(getter, new Class[]{});
                Object value = method.invoke(o, new Object[]{});
                return value;
            }

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
