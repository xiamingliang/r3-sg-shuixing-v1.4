package com.jackrain.nea.sg.basic.config;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/4/29 19:28
 */
@Slf4j
@Configuration
@Data
public class SgStorageMqConfig {

    @Value("${r3.mq.consumer.default.listener.topic}")
    private String channelSynchTopic;

    @Value("${r3.mq.consumer.storage.listener.topic}")
    private String storageUpdateTopic;

    @Value("${r3.mq.consumer.topic.storage_to_channel:storage_to_channel}")
    private String channelSynchTag;

    @Value("${r3.oms.sg.to.retail.storage.mq.topic}")
    private String retailSynchTopic;

    @Value("${r3.oms.sg.to.retail.storage.tag:sg_to_retail_warehouse_stock_change}")
    private String retailSynchTag;

    @Value("${r3.mq.default.configName:default}")
    private String channelSynchConfigName;

}
