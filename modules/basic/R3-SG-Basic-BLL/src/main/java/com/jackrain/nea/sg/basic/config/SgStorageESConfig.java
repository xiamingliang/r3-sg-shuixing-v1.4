package com.jackrain.nea.sg.basic.config;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * @Description: 库存中心ES推送
 * @author: 舒威
 * @since: 2019/10/25
 * create at : 2019/10/25 19:45
 */
@Slf4j
@Configuration
@Data
public class SgStorageESConfig {

    @Value("${sg.elasticsearch.enable:false}")
    private boolean isOpenES;
}
