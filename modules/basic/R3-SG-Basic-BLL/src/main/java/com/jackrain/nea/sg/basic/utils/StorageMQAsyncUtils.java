package com.jackrain.nea.sg.basic.utils;

import com.jackrain.nea.r3.mq.util.R3MqSendHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * @author: 舒威
 * @since: 2019/8/27
 * create at : 2019/8/27 20:05
 */
@Slf4j
@Component
public class StorageMQAsyncUtils {

    @Async
    public void sendDelayMessage(String configName, String body, String topic, String tag, String msgKey, Long delayTime, R3MqSendHelper r3MqSendHelper) {
        try {
            Thread.sleep(delayTime);
            String message = r3MqSendHelper.sendMessage(configName, body, topic, tag, msgKey);
            log.info("延时异步发送MQ结果:" + message);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("延时异步发送MQ异常!" + e.getMessage());
        }
    }
}
