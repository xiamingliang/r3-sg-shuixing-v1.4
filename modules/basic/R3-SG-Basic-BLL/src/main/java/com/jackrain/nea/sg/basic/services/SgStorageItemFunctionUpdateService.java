package com.jackrain.nea.sg.basic.services;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.model.util.ModelUtil;
import com.jackrain.nea.redis.config.CusRedisTemplate;
import com.jackrain.nea.redis.util.RedisOpsUtil;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.config.SgStorageControlConfig;
import com.jackrain.nea.sg.basic.logic.SgStorageCheckLogic;
import com.jackrain.nea.sg.basic.logic.SgStorageLogic;
import com.jackrain.nea.sg.basic.mapper.SgBStorageLockLogMapper;
import com.jackrain.nea.sg.basic.mapper.SgBStorageMapper;
import com.jackrain.nea.sg.basic.model.SgStorageUpdateCommonModel;
import com.jackrain.nea.sg.basic.model.SgStorageUpdateControlModel;
import com.jackrain.nea.sg.basic.model.SgStorageUpdateFunctionModel;
import com.jackrain.nea.sg.basic.model.result.SgStorageUpdateCommonResult;
import com.jackrain.nea.sg.basic.model.result.SgStorageUpdateFunctionResult;
import com.jackrain.nea.sg.basic.model.table.SgBStorage;
import com.jackrain.nea.sg.basic.model.table.SgBStorageLockLog;
import com.jackrain.nea.sg.basic.utils.StorageLogUtils;
import com.jackrain.nea.sg.basic.utils.StorageUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/3/20 13:15
 */
@Component
@Slf4j
public class SgStorageItemFunctionUpdateService {

    @Autowired
    private SgBStorageMapper sgBStorageMapper;

    @Autowired
    private SgBStorageLockLogMapper sgBStorageLockLogMapper;

    @Autowired
    private SgStorageLogic sgStorageLogic;

    @Autowired
    private SgStorageControlConfig sgStorageControlConfig;

    /**
     * @param requestList
     * @param controlModel
     * @param loginUser
     * @return ValueHolderV14<SgStorageUpdateCommonResult>
     */
    @Transactional(rollbackFor = Exception.class)
    public ValueHolderV14<SgStorageUpdateCommonResult> updateStorageItemWithTrans(List<SgStorageUpdateCommonModel> requestList,
                                                                                  SgStorageUpdateControlModel controlModel,
                                                                                  User loginUser) {

        ValueHolderV14<SgStorageUpdateCommonResult> holder = updateStorageItem(requestList,
                controlModel, loginUser);

        if (ResultCode.FAIL == holder.getCode()) {
            throw new NDSException(holder.getMessage());
        }

        return holder;

    }

    /**
     * @param requestList
     * @param controlModel
     * @param loginUser
     * @return
     */
    public ValueHolderV14<SgStorageUpdateCommonResult> updateStorageItem(List<SgStorageUpdateCommonModel> requestList,
                                                                         SgStorageUpdateControlModel controlModel,
                                                                         User loginUser) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgStorageItemFunctionUpdateService.updateStorageItem. ReceiveParams:requestList.size:{} controlModel:{};"
                    , requestList.size(), JSONObject.toJSONString(controlModel));
        }

        ValueHolderV14<SgStorageUpdateCommonResult> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        List<SgBStorageLockLog> lockLogList = new ArrayList<>();
        List<SgStorageUpdateCommonModel> outStockItemList = new ArrayList<>();
        List<SgStorageUpdateCommonModel> channelMqItemList = new ArrayList<>();
        List<SgStorageUpdateFunctionModel> functionModelList = new ArrayList<>();
        Map<String, SgStorageUpdateCommonModel> channelMqItemMap = new HashMap<>();
        Map<Long, Long> storeMap = new HashMap<>();
        Map<Long, Long> skuMap = new HashMap<>();
        Map<String, SgStorageUpdateCommonModel> storageNotExistMap = new HashMap<>();
        SgStorageUpdateCommonResult updateResult = new SgStorageUpdateCommonResult();
        SimpleDateFormat sdfMilli = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyyMMdd");
        CusRedisTemplate<String, String> redisTemplate = RedisOpsUtil.getStrRedisTemplate();
        int preoutUpdateResult = SgConstantsIF.PREOUT_RESULT_SUCCESS;
        int resultCount = 0;

        JsonParser jsonParser = new JsonParser();
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd HH:mm:ss")
                .create();

        if (CollectionUtils.isEmpty(requestList)) {
            return holder;
        }

        for (SgStorageUpdateCommonModel request : requestList) {

            holder = SgStorageCheckLogic.checkServiceParam(request, loginUser);

            if (ResultCode.FAIL == holder.getCode()) {
                return holder;
            }

            storeMap.put(request.getCpCStoreId(), request.getCpCStoreId());
            skuMap.put(request.getPsCSkuId(), request.getPsCSkuId());

            storageNotExistMap.put(request.getCpCStoreId() + "_" + request.getPsCSkuId(),
                    request);
            channelMqItemMap.put(request.getLockKey(), request);

            //批量初始化库存处理流水锁
            SgBStorageLockLog lockLog = new SgBStorageLockLog();
            BeanUtils.copyProperties(request, lockLog);
            lockLog.setId(ModelUtil.getSequence(SgConstants.SG_B_STORAGE_LOCK_LOG));
            lockLogList.add(lockLog);

            //设置通用库存更新PG函数需要的数据格式
            SgStorageUpdateFunctionModel functionModel = new SgStorageUpdateFunctionModel();
            BeanUtils.copyProperties(request, functionModel);
            functionModel.setBillDate(sdfDate.format(request.getBillDate()));
            functionModel.setChangeDate(sdfDate.format(request.getChangeDate()));
            functionModel.setCreationdate(sdfMilli.format(request.getCreationdate()));
            functionModel.setModifieddate(sdfMilli.format(request.getModifieddate()));

            if (request.getControlmodel() != null) {
                functionModel.setNegativeavailable(request.getControlmodel().isNegativeAvailable() ? "true" : "false");
                functionModel.setNegativeprein(request.getControlmodel().isNegativePrein() ? "true" : "false");
                functionModel.setNegativepreout(request.getControlmodel().isNegativePreout() ? "true" : "false");
                functionModel.setNegativestorage(request.getControlmodel().isNegativeStorage() ? "true" : "false");
            } else {
                functionModel.setNegativeavailable(controlModel.isNegativeAvailable() ? "true" : "false");
                functionModel.setNegativeprein(controlModel.isNegativePrein() ? "true" : "false");
                functionModel.setNegativepreout(controlModel.isNegativePreout() ? "true" : "false");
                functionModel.setNegativestorage(controlModel.isNegativeStorage() ? "true" : "false");
            }

            if (BigDecimal.ZERO.compareTo(request.getQtyPreoutChange()) != 0) {
                functionModel.setPreoutFtpId(ModelUtil.getSequence(SgConstants.SG_B_STORAGE_PREOUT_FTP));
            }

            if (BigDecimal.ZERO.compareTo(request.getQtyPreinChange()) != 0) {
                functionModel.setPreinFtpId(ModelUtil.getSequence(SgConstants.SG_B_STORAGE_PREIN_FTP));
            }

            if (BigDecimal.ZERO.compareTo(request.getQtyStorageChange()) != 0) {
                functionModel.setStorageFtpId(ModelUtil.getSequence(SgConstants.SG_B_STORAGE_CHANGE_FTP));
            }

            functionModelList.add(functionModel);
        }

        //查询已存在的库存记录
        List<SgBStorage> resultList = sgBStorageMapper.selectList(
                new QueryWrapper<SgBStorage>().lambda().select(SgBStorage::getCpCStoreId, SgBStorage::getPsCSkuId)
                        .in(!CollectionUtils.isEmpty(storeMap.values()), SgBStorage::getCpCStoreId, new ArrayList<>(storeMap.values()))
                        .in(!CollectionUtils.isEmpty(skuMap.values()), SgBStorage::getPsCSkuId, new ArrayList<>(skuMap.values()))
        );

        //除去【批量初始化库存】列表中已存在的记录
        if (!CollectionUtils.isEmpty(resultList)) {
            String key = null;
            for (SgBStorage storageRecode : resultList) {
                key = storageRecode.getCpCStoreId() + "_" + storageRecode.getPsCSkuId();
                if (storageNotExistMap.containsKey(key)) {
                    storageNotExistMap.remove(key);
                }
            }
        }

        //批量初始化库存
        if (storageNotExistMap.size() > 0) {

            try {
                holder = sgStorageLogic.initStorageList(new ArrayList<>(storageNotExistMap.values()), loginUser);
            } catch (Exception e) {
                log.error("SgStorageItemFunctionUpdateService.updateStorageItem. 店仓库存初始化插入失败！messageKey:{}",
                        requestList.get(0).getMessageKey());
                holder.setCode(ResultCode.FAIL);
                holder.setData(updateResult);
                holder.setMessage(Resources.getMessage("店仓库存初始化插入失败！", loginUser.getLocale()));
                return holder;
            }

            if (ResultCode.FAIL == holder.getCode()) {
                throw new NDSException(holder.getMessage());
            }
        }

        List<List<SgBStorageLockLog>> insertPageList =
                StorageUtils.getBaseModelPageList(lockLogList, SgConstants.SG_COMMON_INSERT_PAGE_SIZE);

        //分页批量更新
        for (List<SgBStorageLockLog> pageList : insertPageList) {

            if (CollectionUtils.isEmpty(pageList)) {
                continue;
            }

            //批量初始化库存处理流水锁
            resultCount = sgBStorageLockLogMapper.batchInsert(pageList);

            if (resultCount != pageList.size()) {
                throw new NDSException(Resources.getMessage("店仓库存锁批量初始化插入失败！", loginUser.getLocale()));
            }
        }

        //调用通用库存更新PG函数
        HashMap<String, Object> param = new HashMap<>();
        try {

            param.put("p_item", JSONObject.toJSONString(functionModelList,
                    SerializerFeature.WriteMapNullValue));
            param.put("p_preoutoperatetype", controlModel.getPreoutOperateType());

            if (log.isDebugEnabled()) {
                log.debug("SgStorageItemFunctionUpdateService.updateStorageItem. updateStorageQtyFn.start. ReceiveParams:param:{};"
                        , JSONObject.toJSONString(param));
            }

            sgBStorageMapper.updateStorageQtyFn(param);

            if (log.isDebugEnabled()) {
                log.debug("SgStorageItemFunctionUpdateService.updateStorageItem. updateStorageQtyFn.end. ReturnResult:param:{};"
                        , JSONObject.toJSONString(param));
            }

            if (param.get("p_out_preoutoperatetype") != null &&
                    (Integer) param.get("p_out_preoutoperatetype") != SgConstantsIF.PREOUT_RESULT_SUCCESS) {

                //获取PG函数结果
                JsonArray jsonParam = jsonParser.parse((String) param.get("p_item_error")).getAsJsonArray();
                List<SgStorageUpdateFunctionResult> billList = gson.fromJson(jsonParam,
                        new TypeToken<List<SgStorageUpdateFunctionResult>>() {
                        }.getType());

                for (SgStorageUpdateFunctionResult fnResult : billList) {

                    if (channelMqItemMap.containsKey(fnResult.getLockkey())) {
                        SgStorageUpdateCommonModel outStockItem = channelMqItemMap.get(fnResult.getLockkey());
                        outStockItem.setQtyOutOfStock(outStockItem.getQtyPreoutChange());
                        outStockItemList.add(outStockItem);
                        channelMqItemMap.remove(fnResult.getLockkey());
                    }

                    holder.setMessage(holder.getMessage().concat(fnResult.getMsg() + " "));
                }

                preoutUpdateResult = (Integer) param.get("p_out_preoutoperatetype");

                if (SgConstantsIF.PREOUT_RESULT_OUT_STOCK != preoutUpdateResult) {
                    holder.setCode(ResultCode.FAIL);
                }

            }

            if (channelMqItemMap.size() > 0) {
                channelMqItemList.addAll(new ArrayList<>(channelMqItemMap.values()));

                //库存更新成功后，删除Redis中的流水记录
                if (sgStorageControlConfig.isRedisFunction()) {
                    for (SgStorageUpdateCommonModel commonModel : channelMqItemList) {
                        redisTemplate.opsForHash().delete(commonModel.getRedisBillFtpKey(),
                                commonModel.getBillId() + "_" + commonModel.getBillItemId());
                    }
                }
            }


        } catch (Exception e) {

            String errorMsg = StorageLogUtils.getErrMessage(e, requestList.get(0).getMessageKey(), loginUser);

            holder.setCode(ResultCode.FAIL);
            holder.setData(updateResult);
            holder.setMessage(Resources.getMessage("店仓库存批量更新失败！", loginUser.getLocale()).concat(errorMsg));
            return holder;
        }

        if (ResultCode.FAIL == holder.getCode()) {
            throw new NDSException(holder.getMessage());
        }

        updateResult.setPreoutUpdateResult(preoutUpdateResult);
        updateResult.setOutStockItemList(outStockItemList);
        updateResult.setChannelMqItemList(channelMqItemList);
        holder.setData(updateResult);

        if (log.isDebugEnabled()) {
            log.debug("Finish SgStorageItemFunctionUpdateService.updateStorageItem. ReturnResult:preoutUpdateResult:{};"
                    , JSONObject.toJSONString(holder.getData().getPreoutUpdateResult()));
        }

        return holder;

    }

}