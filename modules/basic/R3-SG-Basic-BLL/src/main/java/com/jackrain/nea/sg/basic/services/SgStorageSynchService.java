package com.jackrain.nea.sg.basic.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.config.PropertiesConf;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.r3.mq.util.R3MqSendHelper;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.config.SgStorageMqConfig;
import com.jackrain.nea.sg.basic.mapper.SgBStorageMapper;
import com.jackrain.nea.sg.basic.model.SgStorageUpdateCommonModel;
import com.jackrain.nea.sg.basic.model.mq.SgStorageChannelSynchBillItemModel;
import com.jackrain.nea.sg.basic.model.mq.SgStorageChannelSynchBillModel;
import com.jackrain.nea.sg.basic.model.mq.SgStrorageRetailSynchModel;
import com.jackrain.nea.sg.basic.model.result.SgStorageBatchUpdateResult;
import com.jackrain.nea.sg.basic.model.table.SgBStorage;
import com.jackrain.nea.sg.basic.utils.StorageLogUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


/**
 * @Description: 通用逻辑仓库存变动同步接口
 * @Author: chenb
 * @Date: 2019/4/29 18:53
 */
@Component
@Slf4j
public class SgStorageSynchService {

    @Autowired
    private SgStorageMqConfig mqConfig;

    @Autowired
    private R3MqSendHelper r3MqSendHelper;

    @Autowired
    private SgBStorageMapper sgBStorageMapper;

    @Autowired
    private PropertiesConf pconf;

    /**
     * @param channelMqItemList
     * @param loginUser
     * @return
     */
    public ValueHolderV14<SgStorageBatchUpdateResult> synchOthersStorage(List<SgStorageUpdateCommonModel> channelMqItemList,
                                                                         User loginUser) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgStorageSynchService.synchOthersStorage. ReceiveParams:channelMqItemList:{};"
                    , JSONObject.toJSONString(channelMqItemList));
        }

        ValueHolderV14<SgStorageBatchUpdateResult> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        SgStorageChannelSynchBillModel billModel = new SgStorageChannelSynchBillModel();
        List<SgStorageChannelSynchBillItemModel> itemList = new ArrayList<>();
        List<SgStrorageRetailSynchModel> retailSynchList = new ArrayList<>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        String currentThreadName = Thread.currentThread().getName();
        List<SgStorageChannelSynchBillModel> billList = new ArrayList<>();
        SgBStorage storageQueryResult;
        HashMap body = new HashMap<>();
        BigDecimal qtyAvailable = BigDecimal.ZERO;

        //判断是否同步渠道，组合商品库存
        boolean isChannelSynch = Boolean.parseBoolean(pconf.getProperty("sg.control.is_synch_channel_storage"));
        boolean isGroupSynch = Boolean.parseBoolean(pconf.getProperty("sg.control.is_synch_group_storage"));
        boolean isRetailSynch = Boolean.parseBoolean(pconf.getProperty("sg.control.is_synch_retail"));

        if (!isChannelSynch && !isGroupSynch && !isRetailSynch) {
            return holder;
        }

        if (CollectionUtils.isEmpty(channelMqItemList)) {
            log.warn("SgStorageSynchService.synchOthersStorage. 同步库存消息待发送消息为空！进程名称:{}",
                    currentThreadName);
            return holder;
        }

        billModel.setChangeTime(sdf.format(new Date()));

        try {

            for (SgStorageUpdateCommonModel itemRequest : channelMqItemList) {

                SgStorageChannelSynchBillItemModel itemModel;

                //注释掉原因：解决一个平台一个条码多连接时同步缺失问题
                if (itemRequest.getCpCshopId() != null) {
                    itemRequest.setCpCshopId(-1L);
                }

                // 可用变化量 = 在库变化量 - 占用变化量
                qtyAvailable = itemRequest.getQtyStorageChange() == null ? BigDecimal.ZERO : itemRequest.getQtyStorageChange();
                qtyAvailable = qtyAvailable.subtract(itemRequest.getQtyPreoutChange() == null ? BigDecimal.ZERO : itemRequest.getQtyPreoutChange());

                //渠道库存·组合商品库存消息体封装
                if (qtyAvailable.compareTo(BigDecimal.ZERO) != 0) {

                    itemModel = new SgStorageChannelSynchBillItemModel();
                    BeanUtils.copyProperties(itemRequest, itemModel);
                    itemModel.setQtyChange(qtyAvailable);

                    itemList.add(itemModel);

                }

                //零售中心库存消息体封装
                if (isRetailSynch &&
                        ((itemRequest.getQtyStorageChange() != null && itemRequest.getQtyStorageChange().compareTo(BigDecimal.ZERO) != 0) ||
                                (itemRequest.getQtyPreoutChange() != null && itemRequest.getQtyPreoutChange().compareTo(BigDecimal.ZERO) != 0))) {
                    SgStrorageRetailSynchModel retailSynchModel = new SgStrorageRetailSynchModel();
                    BeanUtils.copyProperties(itemRequest, retailSynchModel);
                    retailSynchModel.setQtyStorageChange(itemRequest.getQtyStorageChange());
                    retailSynchModel.setQtyPreoutChange(itemRequest.getQtyPreoutChange());
                    retailSynchList.add(retailSynchModel);
                }

            }

            //发送渠道库存·组合商品库存MQ消息
            if (!CollectionUtils.isEmpty(itemList)) {

                //批量更新单据明细分页
                int pageSize = 500;
                int listSize = itemList.size();
                int page = listSize / pageSize;

                if (listSize % pageSize != 0) {
                    page++;
                }

                //分页批量更新
                for (int i = 0; i < page; i++) {

                    List<SgStorageChannelSynchBillItemModel> pageList = itemList.subList(i * pageSize,
                            (((i + 1) * pageSize > listSize ? listSize : pageSize * (i + 1))));

                    billModel.setItemList(pageList);

                    billList = new ArrayList<>();
                    billList.add(billModel);

                    body.put("param", billList);

                    //发送渠道库存MQ消息
                    if (isChannelSynch) {

                        //使用延时消息缓解逻辑仓单据与实体仓单据事务过程问题
                        r3MqSendHelper.sendDelayMessageAsync(mqConfig.getChannelSynchConfigName(),
                                JSONObject.toJSONString(body),
                                mqConfig.getChannelSynchTopic(),
                                mqConfig.getChannelSynchTag(),
                                SgConstants.MSG_KEY_HEAD_STORAGE_TO_CHANNEL.concat(currentThreadName + "_" + i),
                                3000L);
                    }

                    //发送组合商品库存MQ消息
                    if (isGroupSynch) {

                        body.put("user", loginUser);

                        //使用延时消息缓解逻辑仓单据与实体仓单据事务过程问题
                        r3MqSendHelper.sendDelayMessageAsync(mqConfig.getChannelSynchConfigName(),
                                JSONObject.toJSONString(body),
                                mqConfig.getStorageUpdateTopic(),
                                SgConstants.Msg_TAG_GROUP_STORAGE,
                                SgConstants.MSG_KEY_HEAD_STORAGE_TO_GROUP.concat(currentThreadName + "_" + i),
                                3000L);

                    }
                }

            }

            //发送零售中心库存MQ消息
            if (!CollectionUtils.isEmpty(retailSynchList)) {

                if (log.isDebugEnabled()) {
                    log.debug(this.getClass().getName() + ",发送MQ消息通知零售单同步库存:" + retailSynchList.size());
                }

                body = new HashMap<>();

                //批量更新单据明细分页
                int pageSize = 500;
                int listSize = retailSynchList.size();
                int page = listSize / pageSize;

                if (listSize % pageSize != 0) {
                    page++;
                }

                //分页批量更新
                for (int i = 0; i < page; i++) {

                    List<SgStrorageRetailSynchModel> pageList = retailSynchList.subList(i * pageSize,
                            (((i + 1) * pageSize > listSize ? listSize : pageSize * (i + 1))));

                    for (SgStrorageRetailSynchModel syncModel : pageList) {

                        storageQueryResult = sgBStorageMapper.selectOne(
                                new QueryWrapper<SgBStorage>().lambda()
                                        .eq(SgBStorage::getCpCStoreId, syncModel.getCpCStoreId())
                                        .eq(SgBStorage::getPsCSkuId, syncModel.getPsCSkuId())
                        );

                        syncModel.setChangeTime(System.currentTimeMillis());

                        if (storageQueryResult != null) {
                            syncModel.setQtyAvailable(storageQueryResult.getQtyAvailable());
                            syncModel.setQtyStorage(storageQueryResult.getQtyStorage());
                        }

                    }

                    body.put("param", pageList);

                    String sendResult = r3MqSendHelper.sendMessageAsync(mqConfig.getChannelSynchConfigName(),
                            JSONObject.toJSONString(body),
                            mqConfig.getRetailSynchTopic(),
                            mqConfig.getRetailSynchTag(),
                            SgConstants.MSG_KEY_HEAD_STORAGE_TO_RETAIL.concat(currentThreadName + "_" + i));

                    if (log.isDebugEnabled()) {
                        log.debug(this.getClass().getName() + ",发送MQ消息通知零售单同步库存返回msgId:" + sendResult);
                    }
                }
            }

        } catch (Exception e) {

            StorageLogUtils.getErrMessage(e, currentThreadName, loginUser);

            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("同步渠道·组合商品·零售中心库存消息发送失败！", loginUser.getLocale()));

        }

        if (log.isDebugEnabled()) {
            log.debug("Finish SgStorageSynchService.synchOthersStorage. ReturnResult:currentThreadName:{};"
                    , currentThreadName);
        }

        return holder;
    }

}
