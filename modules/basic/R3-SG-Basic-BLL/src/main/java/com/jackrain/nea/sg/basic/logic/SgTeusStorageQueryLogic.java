package com.jackrain.nea.sg.basic.logic;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.cp.result.CpCStore;
import com.jackrain.nea.data.basic.model.request.StoreInfoQueryRequest;
import com.jackrain.nea.data.basic.services.BasicCpQueryService;
import com.jackrain.nea.sg.basic.config.SgStorageControlConfig;
import com.jackrain.nea.sg.basic.mapper.SgBTeusStorageMapper;
import com.jackrain.nea.sg.basic.model.request.SgTeusStoragePageRequest;
import com.jackrain.nea.sg.basic.model.request.SgTeusStorageQueryRequest;
import com.jackrain.nea.sg.basic.model.table.SgBTeusStorage;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author zhu lin yu
 * @since 2019-11-01
 * create at : 2019-11-01 11:21
 */
@Component
@Slf4j
public class SgTeusStorageQueryLogic {

    @Autowired
    SgStorageControlConfig sgStorageControlConfig;

    @Autowired
    private SgBTeusStorageMapper sgBTeusStorageMapper;

    @Autowired
    private BasicCpQueryService basicCpQueryService;

    /**
     * 箱逻辑仓库存查询
     *
     * @return ValueHolderV14
     */
    public ValueHolderV14 queryTeusStorage(SgTeusStorageQueryRequest sgTeusStorageQueryRequest,
                                           SgTeusStoragePageRequest sgTeusStoragePageRequest,
                                           User loginUser,
                                           boolean excludeZero) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgTeusQueryLogic.queryTeusStorage. ReceiveParams:sgTeusStorageQueryRequest={} " +
                            "sgTeusStoragePageRequest={} loginUser={};",
                    JSONObject.toJSONString(sgTeusStorageQueryRequest), JSONObject.toJSONString(sgTeusStoragePageRequest),
                    JSONObject.toJSONString(loginUser));
        }

        ValueHolderV14 holder = new ValueHolderV14(ResultCode.SUCCESS, "");

        try {

            holder = checkServiceParam(sgTeusStorageQueryRequest, loginUser);

            if (ResultCode.FAIL == holder.getCode()) {
                return holder;
            }

            List<Long> storeIds = sgTeusStorageQueryRequest.getStoreIds();
            List<Long> phyWarehouseIds = sgTeusStorageQueryRequest.getPhyWarehouseIds();
            List<String> proEcodes = sgTeusStorageQueryRequest.getProEcodes();
            List<String> teusEcodes = sgTeusStorageQueryRequest.getTeusEcodes();

            // 获取实体仓下的虚拟仓ID
            if (!CollectionUtils.isEmpty(phyWarehouseIds)) {

                storeIds = getStoreIdsByPhyIds(phyWarehouseIds);

                // 检查实体仓下是否存在店仓，防止全量查询
                if (CollectionUtils.isEmpty(storeIds)) {
                    holder.setCode(ResultCode.FAIL);
                    holder.setMessage(Resources.getMessage("实体仓获取店仓信息失败！", loginUser.getLocale()));
                    return holder;
                }
            }

            if (sgTeusStoragePageRequest == null) {

                // 全量查询
                List<SgBTeusStorage> resultList = sgBTeusStorageMapper.selectList(
                        new QueryWrapper<SgBTeusStorage>().lambda()
                                .in(!CollectionUtils.isEmpty(storeIds), SgBTeusStorage::getCpCStoreId, storeIds)
                                .in(!CollectionUtils.isEmpty(proEcodes), SgBTeusStorage::getPsCProEcode, proEcodes)
                                .in(!CollectionUtils.isEmpty(teusEcodes), SgBTeusStorage::getPsCTeusEcode, teusEcodes)
                                .ne(excludeZero, SgBTeusStorage::getQtyStorage, BigDecimal.ZERO)
                );

                holder.setData(resultList);

            } else {

                // 分页查询
                PageHelper.startPage(sgTeusStoragePageRequest.getPageNum(), sgTeusStoragePageRequest.getPageSize());
                List<SgBTeusStorage> resultList = sgBTeusStorageMapper.selectList(
                        new QueryWrapper<SgBTeusStorage>().lambda()
                                .in(!CollectionUtils.isEmpty(storeIds), SgBTeusStorage::getCpCStoreId, storeIds)
                                .in(!CollectionUtils.isEmpty(proEcodes), SgBTeusStorage::getPsCProEcode, proEcodes)
                                .in(!CollectionUtils.isEmpty(teusEcodes), SgBTeusStorage::getPsCTeusEcode, teusEcodes)
                                .ne(excludeZero, SgBTeusStorage::getQtyStorage, BigDecimal.ZERO)
                );
                PageInfo<SgBTeusStorage> resultPage = new PageInfo<>(resultList);

                holder.setData(resultPage);
            }

            holder.setCode(ResultCode.SUCCESS);
            holder.setMessage(Resources.getMessage("查询箱库存成功！", loginUser.getLocale()));

        } catch (Exception ex) {
            log.error("SgTeusStorageQueryLogic.queryTeusStorage Error", ex);
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("查询箱库存信息发生异常！", loginUser.getLocale()));
        }

        return holder;
    }


    /**
     * @return ValueHolderV14
     */
    private ValueHolderV14 checkServiceParam(SgTeusStorageQueryRequest sgTeusStorageQueryRequest,
                                             User loginUser) {

        ValueHolderV14 holder = new ValueHolderV14(ResultCode.SUCCESS, "");

        int maxQueryLimit = sgStorageControlConfig.getMaxQueryLimit();

        List<Long> storeIds = sgTeusStorageQueryRequest.getStoreIds();
        List<Long> phyWarehouseIds = sgTeusStorageQueryRequest.getPhyWarehouseIds();
        List<String> proEcodes = sgTeusStorageQueryRequest.getProEcodes();
        List<String> teusEcodes = sgTeusStorageQueryRequest.getTeusEcodes();

        if (CollectionUtils.isEmpty(storeIds) && CollectionUtils.isEmpty(phyWarehouseIds)) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("店仓或实体仓不能同时为空！", loginUser.getLocale()));
        } else if (!CollectionUtils.isEmpty(storeIds) && !CollectionUtils.isEmpty(phyWarehouseIds)) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("店仓和实体仓不能同时查询！", loginUser.getLocale()));
        } else if (!CollectionUtils.isEmpty(proEcodes) &&
                proEcodes.size() > maxQueryLimit) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("商品编码的查询件数超过最大限制！",
                    loginUser.getLocale()));
        } else if (!CollectionUtils.isEmpty(teusEcodes) &&
                teusEcodes.size() > maxQueryLimit) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("箱编码的查询件数超过最大限制！",
                    loginUser.getLocale()));

        }

        return holder;
    }


    /**
     * @return List<Long>
     */
    private List<Long> getStoreIdsByPhyIds(List<Long> phyWarehouseIds) throws Exception {

        List<Long> storeIds = null;

        // 获取实体仓下的虚拟仓ID
        if (!CollectionUtils.isEmpty(phyWarehouseIds)) {

            StoreInfoQueryRequest storeInfoQueryRequest;
            storeIds = new ArrayList<>();

            for (Long phyId : phyWarehouseIds) {

                storeInfoQueryRequest = new StoreInfoQueryRequest();
                storeInfoQueryRequest.setPhyId(phyId);

                HashMap<Long, List<CpCStore>> storeMap = basicCpQueryService.getStoreInfoByPhyId(storeInfoQueryRequest);

                if (storeMap != null && !CollectionUtils.isEmpty(storeMap.get(phyId))) {

                    for (CpCStore cpCStore : storeMap.get(phyId)) {
                        storeIds.add(cpCStore.getId());
                    }
                }
            }

        }

        return storeIds;
    }
}
