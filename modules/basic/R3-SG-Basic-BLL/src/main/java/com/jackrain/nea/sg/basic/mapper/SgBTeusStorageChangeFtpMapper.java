package com.jackrain.nea.sg.basic.mapper;

import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.basic.model.table.SgBTeusStorageChangeFtp;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SgBTeusStorageChangeFtpMapper extends ExtentionMapper<SgBTeusStorageChangeFtp> {
}