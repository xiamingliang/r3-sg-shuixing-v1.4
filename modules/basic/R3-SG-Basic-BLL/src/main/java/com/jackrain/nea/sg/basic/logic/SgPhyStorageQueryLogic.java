package com.jackrain.nea.sg.basic.logic;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.config.SgStorageControlConfig;
import com.jackrain.nea.sg.basic.mapper.SgBPhyStorageMapper;
import com.jackrain.nea.sg.basic.model.request.SgPhyStorageQueryRequest;
import com.jackrain.nea.sg.basic.model.request.SgStoragePageRequest;
import com.jackrain.nea.sg.basic.model.table.SgBPhyStorage;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/4/26 10:39
 */
@Component
@Slf4j
public class SgPhyStorageQueryLogic {

    @Autowired
    SgStorageControlConfig sgStorageControlConfig;

    @Autowired
    private SgBPhyStorageMapper sgBPhyStorageMapper;

    /**
     * 逻辑仓库存查询
     *
     * @param sgPhyStorageQueryRequest
     * @param sgStoragePageRequest
     * @param loginUser
     * @return
     */
    public ValueHolderV14 queryPhyStorage(SgPhyStorageQueryRequest sgPhyStorageQueryRequest,
                                          SgStoragePageRequest sgStoragePageRequest,
                                          User loginUser) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgPhyStorageQueryLogic.queryPhyStorage. ReceiveParams:sgPhyStorageQueryRequest={} " +
                            "sgStoragePageRequest={} loginUser={};",
                    JSONObject.toJSONString(sgPhyStorageQueryRequest), JSONObject.toJSONString(sgStoragePageRequest),
                    JSONObject.toJSONString(loginUser));
        }

        ValueHolderV14 holder = new ValueHolderV14(ResultCode.SUCCESS, "");

        try {

            holder = checkServiceParam(sgPhyStorageQueryRequest, loginUser);

            if (ResultCode.FAIL == holder.getCode()) {
                return holder;
            }

            List<Long> phyWarehouseIds = sgPhyStorageQueryRequest.getPhyWarehouseIds();
            List<String> proEcodes = sgPhyStorageQueryRequest.getProEcodes();
            List<String> skuEcodes = sgPhyStorageQueryRequest.getSkuEcodes();

            if (sgStoragePageRequest == null) {

                /** 全量查询 **/
                List<SgBPhyStorage> resultList = sgBPhyStorageMapper.selectList(
                        new QueryWrapper<SgBPhyStorage>().lambda()
                                .in(!CollectionUtils.isEmpty(phyWarehouseIds), SgBPhyStorage::getCpCPhyWarehouseId, phyWarehouseIds)
                                .in(!CollectionUtils.isEmpty(proEcodes), SgBPhyStorage::getPsCProEcode, proEcodes)
                                .in(!CollectionUtils.isEmpty(skuEcodes), SgBPhyStorage::getPsCSkuEcode, skuEcodes)
                );

                holder.setData(resultList);

            } else {

                /** 分页查询 **/
                PageHelper.startPage(sgStoragePageRequest.getPageNum(), sgStoragePageRequest.getPageSize());
                List<SgBPhyStorage> resultList = sgBPhyStorageMapper.selectList(
                        new QueryWrapper<SgBPhyStorage>().lambda()
                                .in(!CollectionUtils.isEmpty(phyWarehouseIds), SgBPhyStorage::getCpCPhyWarehouseId, phyWarehouseIds)
                                .in(!CollectionUtils.isEmpty(proEcodes), SgBPhyStorage::getPsCProEcode, proEcodes)
                                .in(!CollectionUtils.isEmpty(skuEcodes), SgBPhyStorage::getPsCSkuEcode, skuEcodes)
                );
                PageInfo<SgBPhyStorage> resultPage = new PageInfo<>(resultList);

                holder.setData(resultPage);
            }

            holder.setCode(ResultCode.SUCCESS);
            holder.setMessage(Resources.getMessage("查询实体仓库存成功！", loginUser.getLocale()));

        } catch (Exception ex) {
            log.error("SgPhyStorageQueryLogic.queryPhyStorage Error", ex);
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("查询实体仓库存信息发生异常！", loginUser.getLocale()));
        }

        return holder;
    }

    /**
     * @param sgPhyStorageQueryRequest
     * @param loginUser
     * @return
     */
    private ValueHolderV14 checkServiceParam(SgPhyStorageQueryRequest sgPhyStorageQueryRequest,
                                             User loginUser) {

        ValueHolderV14 holder = new ValueHolderV14(ResultCode.SUCCESS, "");

        int maxQueryLimit = sgStorageControlConfig.getMaxQueryLimit();

        List<Long> phyWarehouseIds = sgPhyStorageQueryRequest.getPhyWarehouseIds();
        List<String> proEcodes = sgPhyStorageQueryRequest.getProEcodes();
        List<String> skuEcodes = sgPhyStorageQueryRequest.getSkuEcodes();

        if (CollectionUtils.isEmpty(phyWarehouseIds)) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("实体仓不能为空！", loginUser.getLocale()));
        } else if (!CollectionUtils.isEmpty(proEcodes) &&
                proEcodes.size() > maxQueryLimit) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("商品编码的查询件数超过最大限制！",
                    loginUser.getLocale()));
        } else if (!CollectionUtils.isEmpty(skuEcodes) &&
                skuEcodes.size() > maxQueryLimit) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("商品条码的查询件数超过最大限制！",
                    loginUser.getLocale()));

        }

        return holder;
    }
}
