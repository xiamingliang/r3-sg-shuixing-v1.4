package com.jackrain.nea.sg.basic.services;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.config.SgStorageBoxConfig;
import com.jackrain.nea.sg.basic.model.request.SgStorageSingleUpdateRequest;
import com.jackrain.nea.sg.basic.model.request.SgStorageUpdateControlRequest;
import com.jackrain.nea.sg.basic.model.result.SgStorageBatchUpdateResult;
import com.jackrain.nea.sg.basic.model.result.SgTeusStorageBatchUpdateResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/6/1 12:51
 */
@Component
@Slf4j
public class SgStorageRedisBillUpdateService {

    @Autowired
    private SgStorageRedisUpdateService stoUpdService;

    @Autowired
    private SgTeusStorageRedisUpdateService teusStoUpdService;

    @Autowired
    private SgStorageRedisSynchService sgStorageRedisSynchService;

    @Autowired
    private SgTeusStorageRedisSynchService sgTeusStorageRedisSynchService;

    @Autowired
    private SgStorageBoxConfig sgStorageBoxConfig;

    public ValueHolderV14<SgStorageBatchUpdateResult> updateStorageBill(SgStorageSingleUpdateRequest request) {

        ValueHolderV14<SgStorageBatchUpdateResult> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        ValueHolderV14<SgTeusStorageBatchUpdateResult> teusHolder = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        SgStorageBatchUpdateResult updateResult = new SgStorageBatchUpdateResult();
        SgStorageUpdateControlRequest controlModel = request.getControlModel();

        long startTime = System.currentTimeMillis();

        try {

            if (log.isDebugEnabled()) {
                log.debug("Start SgStorageRedisBillUpdateService.updateStorageBill. ReceiveParams:request:{};"
                        , JSONObject.toJSONString(request));
            }

            //调用单个单据库存更新接口
            holder = stoUpdService.updateStorageBill(request);

            updateResult = holder.getData();

            //调用单个单据箱库存更新接口
            if (holder != null && ResultCode.SUCCESS == holder.getCode() && sgStorageBoxConfig.getBoxEnable() &&
                    (request.getBill() != null && !CollectionUtils.isEmpty(request.getBill().getTeusList()))) {

                teusHolder = teusStoUpdService.updateStorageBill(request);

                if (teusHolder != null && teusHolder.getData() != null) {

                    //箱Redis更新流水键与SKU Redis更新流水键进行合并
                    updateResult.getRedisBillFtpKeyList().addAll(teusHolder.getData().getRedisBillFtpKeyList());

                    if (ResultCode.SUCCESS != teusHolder.getCode() || teusHolder.getData().getErrorBillItemQty() > 0) {
                        holder.setCode(ResultCode.FAIL);
                        holder.setMessage(teusHolder.getMessage().concat(" " + holder.getMessage()));
                    }

                    updateResult.setErrorBillItemQty(updateResult.getErrorBillItemQty() + teusHolder.getData().getErrorBillItemQty());
                    updateResult.setOutStockTeusList(teusHolder.getData().getOutStockItemList());

                    if (SgConstantsIF.PREOUT_RESULT_SUCCESS != teusHolder.getData().getPreoutUpdateResult()) {
                        updateResult.setPreoutUpdateResult(teusHolder.getData().getPreoutUpdateResult());
                    }

                }

            }

            if (holder != null && ResultCode.SUCCESS == holder.getCode() && holder.getData() != null) {

                if (!CollectionUtils.isEmpty(holder.getData().getRedisSynchMqItemList())) {

                    sgStorageRedisSynchService.synchRedisToStorage(request,
                            holder.getData().getRedisSynchMqItemList());

                }
            }

            if (teusHolder != null && ResultCode.SUCCESS == holder.getCode() && teusHolder.getData() != null) {

                if (!CollectionUtils.isEmpty(teusHolder.getData().getRedisSynchMqItemList())) {

                    sgTeusStorageRedisSynchService.synchRedisToStorage(request,
                            teusHolder.getData().getRedisSynchMqItemList());

                }
            }


        } catch (Exception e) {

            holder.setCode(ResultCode.FAIL);

            if (controlModel != null) {
                updateResult.setPreoutUpdateResult(controlModel.getPreoutOperateType());
            }

            holder.setData(updateResult);
            holder.setMessage(e.getMessage().concat(" " + holder.getMessage()));

        }

        //更新失败的场景下，流水回滚
        if (holder != null && ResultCode.SUCCESS != holder.getCode()) {
            stoUpdService.rollbackStorageBill(updateResult.getRedisBillFtpKeyList());
        }

        if (log.isDebugEnabled()) {
            log.debug("Finish SgStorageRedisBillUpdateService.updateStorageBill. ReturnResult:errorBillItemQty:{} billNo:{} spend time:{}ms;"
                    , JSONObject.toJSONString(holder.getData() == null ? 0 : holder.getData().getErrorBillItemQty()), request.getBill().getBillNo(), System.currentTimeMillis() - startTime);
        }

        return holder;

    }

}
