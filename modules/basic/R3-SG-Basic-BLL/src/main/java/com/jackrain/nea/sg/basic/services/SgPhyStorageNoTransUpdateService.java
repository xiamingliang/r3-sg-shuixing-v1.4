package com.jackrain.nea.sg.basic.services;

import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.model.SgStorageBillUpdateControlModel;
import com.jackrain.nea.sg.basic.model.request.SgPhyStorageSingleUpdateRequest;
import com.jackrain.nea.sg.basic.model.request.SgStorageUpdateControlRequest;
import com.jackrain.nea.sg.basic.model.result.SgPhyStorageBatchUpdateResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/6/1 12:51
 */
@Component
@Slf4j
public class SgPhyStorageNoTransUpdateService {

    @Autowired
    private SgPhyStorageBillUpdateService service;

    public ValueHolderV14<SgPhyStorageBatchUpdateResult> updatePhyStorageBill(SgPhyStorageSingleUpdateRequest request) {

        ValueHolderV14<SgPhyStorageBatchUpdateResult> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        SgPhyStorageBatchUpdateResult updateResult = new SgPhyStorageBatchUpdateResult();

        User loginUser = request.getLoginUser();
        String messageKey = request.getMessageKey();
        SgStorageUpdateControlRequest controlModel = request.getControlModel();

        SgStorageBillUpdateControlModel billControlModel = new SgStorageBillUpdateControlModel();
        billControlModel.setTransactionLevel(SgConstants.TRANSACTION_LEVEL_CONSUMER);

        try {
            holder = service.updatePhyStorageBill(request, billControlModel);
        } catch (Exception e) {

            log.error("SgPhyStorageNoTransUpdateService.updatePhyStorageBill. error ", e);

            holder.setCode(ResultCode.FAIL);
            holder.setData(updateResult);
            holder.setMessage(e.getMessage().concat(" " + holder.getMessage()));
        }

        return holder;

    }

}
