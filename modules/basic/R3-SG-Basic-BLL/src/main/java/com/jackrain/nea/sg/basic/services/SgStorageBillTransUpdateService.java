package com.jackrain.nea.sg.basic.services;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.model.request.SgStorageSingleUpdateRequest;
import com.jackrain.nea.sg.basic.model.request.SgStorageUpdateControlRequest;
import com.jackrain.nea.sg.basic.model.result.SgStorageBatchUpdateResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/6/1 12:51
 */
@Component
@Slf4j
public class SgStorageBillTransUpdateService {

    @Autowired
    private SgStorageBillUpdateService service;

    @Autowired
    private SgStorageSynchService sgStorageSynchService;

    public ValueHolderV14<SgStorageBatchUpdateResult> updateStorageBill(SgStorageSingleUpdateRequest request) {

        ValueHolderV14<SgStorageBatchUpdateResult> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        SgStorageBatchUpdateResult updateResult = new SgStorageBatchUpdateResult();
        SgStorageUpdateControlRequest controlModel = request.getControlModel();

        long startTime = System.currentTimeMillis();

        try {

            if (log.isDebugEnabled()) {
                log.debug("Start SgStorageBillTransUpdateService.updateStorageBill. ReceiveParams:request:{};"
                        , JSONObject.toJSONString(request.getBill()));
            }

            holder = service.updateStorageBillWithTrans(request);

            if (log.isDebugEnabled()) {
                log.debug("Finish SgStorageBillTransUpdateService.updateStorageBill. ReturnResult:errorBillItemQt:{} billNo:{} spend time:{}ms;"
                        , JSONObject.toJSONString(holder.getData() == null ? 0 : holder.getData().getErrorBillItemQty()), request.getBill().getBillNo(), System.currentTimeMillis() - startTime);
            }

            if (holder != null && holder.getData() != null) {

                if (!CollectionUtils.isEmpty(holder.getData().getChannelMqItemList())) {

                    //考虑库存事务锁快速释放问题，同步渠道库存在事务外调用
                    sgStorageSynchService.synchOthersStorage(holder.getData().getChannelMqItemList(),
                            request.getLoginUser());

                }
            }

        } catch (Exception e) {

            log.error("SgStorageBillTransUpdateService.updateStorageBill. error ", e);

            holder.setCode(ResultCode.FAIL);
            if (controlModel != null) {
                updateResult.setPreoutUpdateResult(controlModel.getPreoutOperateType());
            }
            holder.setData(updateResult);
            holder.setMessage(e.getMessage().concat(" " + holder.getMessage()));

        }

        return holder;

    }

}
