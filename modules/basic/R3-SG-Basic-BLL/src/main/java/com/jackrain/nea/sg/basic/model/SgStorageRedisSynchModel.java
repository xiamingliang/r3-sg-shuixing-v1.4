package com.jackrain.nea.sg.basic.model;

import com.jackrain.nea.sg.basic.model.request.SgStorageSingleUpdateRequest;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class SgStorageRedisSynchModel {


    SgStorageSingleUpdateRequest singleRequest;

    List<SgStorageUpdateCommonModel> sgStorageUpdateCommonList;

    List<SgTeusStorageUpdateCommonModel> sgTeusStorageUpdateCommonList;

    public SgStorageRedisSynchModel() {
        sgStorageUpdateCommonList = new ArrayList<>();
        sgTeusStorageUpdateCommonList = new ArrayList<>();
    }

}
