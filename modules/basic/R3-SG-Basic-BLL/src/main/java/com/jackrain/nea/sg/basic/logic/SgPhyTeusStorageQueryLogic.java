package com.jackrain.nea.sg.basic.logic;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.config.SgStorageControlConfig;
import com.jackrain.nea.sg.basic.mapper.SgBPhyTeusStorageMapper;
import com.jackrain.nea.sg.basic.model.request.SgTeusPhyStorageQueryRequest;
import com.jackrain.nea.sg.basic.model.request.SgTeusStoragePageRequest;
import com.jackrain.nea.sg.basic.model.table.SgBPhyTeusStorage;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author zhu lin yu
 * @since 2019-11-01
 * create at : 2019-11-01 11:21
 */
@Component
@Slf4j
public class SgPhyTeusStorageQueryLogic {
    @Autowired
    private SgStorageControlConfig sgStorageControlConfig;

    @Autowired
    private SgBPhyTeusStorageMapper sgBPhyTeusStorageMapper;

    public ValueHolderV14 queryPhyTeusStorage(SgTeusPhyStorageQueryRequest sgTeusPhyStorageQueryRequest,
                                              SgTeusStoragePageRequest sgTeusStoragePageRequest,
                                              User loginUser) {
        if (log.isDebugEnabled()) {
            log.debug("Start SgPhyTeusQueryLogic.queryPhyTeusStorage. ReceiveParams:sgTeusPhyStorageQueryRequest={} " +
                            "sgTeusStoragePageRequest={} loginUser={};",
                    JSONObject.toJSONString(sgTeusPhyStorageQueryRequest), JSONObject.toJSONString(sgTeusStoragePageRequest),
                    JSONObject.toJSONString(loginUser));
        }

        ValueHolderV14 holder = new ValueHolderV14(ResultCode.SUCCESS, "");

        try {

            holder = checkServiceParam(sgTeusPhyStorageQueryRequest, loginUser);

            if (ResultCode.FAIL == holder.getCode()) {
                return holder;
            }

            List<String> proEcodes = sgTeusPhyStorageQueryRequest.getProEcodes();
            List<String> teusEcodes = sgTeusPhyStorageQueryRequest.getTeusEcodes();
            List<Long> phyWarehouseIds = sgTeusPhyStorageQueryRequest.getPhyWarehouseIds();

            if (sgTeusStoragePageRequest == null) {

                /** 全量查询 **/
                List<SgBPhyTeusStorage> resultList = sgBPhyTeusStorageMapper.selectList(
                        new QueryWrapper<SgBPhyTeusStorage>().lambda()
                                .in(!CollectionUtils.isEmpty(phyWarehouseIds), SgBPhyTeusStorage::getCpCPhyWarehouseId, phyWarehouseIds)
                                .in(!CollectionUtils.isEmpty(proEcodes), SgBPhyTeusStorage::getPsCProEcode, proEcodes)
                                .in(!CollectionUtils.isEmpty(teusEcodes), SgBPhyTeusStorage::getPsCTeusEcode, teusEcodes)
                );

                holder.setData(resultList);

            } else {

                /** 分页查询 **/
                PageHelper.startPage(sgTeusStoragePageRequest.getPageNum(), sgTeusStoragePageRequest.getPageSize());
                List<SgBPhyTeusStorage> resultList = sgBPhyTeusStorageMapper.selectList(
                        new QueryWrapper<SgBPhyTeusStorage>().lambda()
                                .in(!CollectionUtils.isEmpty(phyWarehouseIds), SgBPhyTeusStorage::getCpCPhyWarehouseId, phyWarehouseIds)
                                .in(!CollectionUtils.isEmpty(proEcodes), SgBPhyTeusStorage::getPsCProEcode, proEcodes)
                                .in(!CollectionUtils.isEmpty(teusEcodes), SgBPhyTeusStorage::getPsCTeusEcode, teusEcodes)

                );
                PageInfo<SgBPhyTeusStorage> resultPage = new PageInfo<>(resultList);

                holder.setData(resultPage);
            }

            holder.setCode(ResultCode.SUCCESS);
            holder.setMessage(Resources.getMessage("查询箱库存成功！", loginUser.getLocale()));

        } catch (Exception ex) {
            log.error("SgPhyTeusQueryLogic.queryPhyTeusStorage Error", ex);
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("查询箱库存信息发生异常！", loginUser.getLocale()));
        }

        return holder;
    }

    private ValueHolderV14 checkServiceParam(SgTeusPhyStorageQueryRequest sgTeusPhyStorageQueryRequest,
                                             User loginUser) {

        ValueHolderV14 holder = new ValueHolderV14(ResultCode.SUCCESS, "");

        int maxQueryLimit = sgStorageControlConfig.getMaxQueryLimit();

        List<Long> phyWarehouseIds = sgTeusPhyStorageQueryRequest.getPhyWarehouseIds();
        List<String> proEcodes = sgTeusPhyStorageQueryRequest.getProEcodes();
        List<String> teusEcodes = sgTeusPhyStorageQueryRequest.getTeusEcodes();

        if (CollectionUtils.isEmpty(phyWarehouseIds)) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("实体仓不能为空！", loginUser.getLocale()));
        } else if (CollectionUtils.isNotEmpty(teusEcodes) && teusEcodes.size() > maxQueryLimit) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("箱号查询件数超过最大限制！", loginUser.getLocale()));
        } else if (CollectionUtils.isNotEmpty(proEcodes) && proEcodes.size() > maxQueryLimit) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("款号的查询件数超过最大限制！", loginUser.getLocale()));
        }

        return holder;
    }
}
