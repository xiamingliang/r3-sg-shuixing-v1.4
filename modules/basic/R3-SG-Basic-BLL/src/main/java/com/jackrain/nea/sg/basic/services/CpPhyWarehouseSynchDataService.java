package com.jackrain.nea.sg.basic.services;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.mapper.CpCPhyWarehouseItemMapper;
import com.jackrain.nea.sg.basic.mapper.CpCPhyWarehouseMapper;
import com.jackrain.nea.sg.basic.model.request.CpPhyWarehouseRequest;
import com.jackrain.nea.sg.basic.model.table.CpCPhyWarehouse;
import com.jackrain.nea.sg.basic.model.table.CpCPhyWarehouseItem;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @program: r3-sg
 * @author: Lijp
 * @create: 2019-05-09 14:05
 */
@Component
@Slf4j
public class CpPhyWarehouseSynchDataService {

    @Autowired
    private CpCPhyWarehouseMapper cpCPhyWarehouseMapper;

    @Autowired
    private CpCPhyWarehouseItemMapper cpCPhyWarehouseItemMapper;

    @Transactional(rollbackFor = Exception.class)
    public ValueHolderV14 warehouseSave(CpPhyWarehouseRequest cpPhyWarehouseRequest) {
        ValueHolderV14 result = new ValueHolderV14();
        result = checkWareHouseParam(cpPhyWarehouseRequest);
        if (ResultCode.FAIL == result.getCode()) {
            return result;
        }
        if(null == cpPhyWarehouseRequest || null == cpPhyWarehouseRequest.getCpCPhyWarehouse()){
            result.setCode(ResultCode.FAIL);
            result.setMessage("实体仓信息为空");
            return result;
        }
        //主表新增
        if(null !=  cpPhyWarehouseRequest.getCpCPhyWarehouse()){
            String ecode = cpPhyWarehouseRequest.getCpCPhyWarehouse().getEcode();
            CpCPhyWarehouse warehouse = cpCPhyWarehouseMapper.selectOne(new QueryWrapper<CpCPhyWarehouse>().lambda().eq(CpCPhyWarehouse::getEcode,ecode));
            if(warehouse != null) {
                warehouseAudit(cpPhyWarehouseRequest);
            }
            cpCPhyWarehouseMapper.insert(cpPhyWarehouseRequest.getCpCPhyWarehouse());
        }
        if(CollectionUtils.isNotEmpty(cpPhyWarehouseRequest.getCpCPhyWarehouseItemList())){
            result = checkWareHouseItemParam(cpPhyWarehouseRequest.getCpCPhyWarehouseItemList());
            if (ResultCode.FAIL == result.getCode()) {
                return result;
            }
            cpCPhyWarehouseItemMapper.batchInsert(cpPhyWarehouseRequest.getCpCPhyWarehouseItemList());
        }
        result.setCode(ResultCode.SUCCESS);
        result.setMessage("新增成功");
        return result;
    }

    @Transactional(rollbackFor = Exception.class)
    public ValueHolderV14 warehouseAudit(CpPhyWarehouseRequest cpPhyWarehouseRequest) {
        ValueHolderV14 result = new ValueHolderV14();
        if(null == cpPhyWarehouseRequest ){
            result.setCode(ResultCode.FAIL);
            result.setMessage("实体仓信息为空");
            return result;
        }
        //主表更新
        if(null != cpPhyWarehouseRequest.getCpCPhyWarehouse()){
            cpCPhyWarehouseMapper.updateById(cpPhyWarehouseRequest.getCpCPhyWarehouse());
        }
        if(CollectionUtils.isNotEmpty(cpPhyWarehouseRequest.getCpCPhyWarehouseItemList())){
            //明细在修改界面新增数据
            cpCPhyWarehouseItemMapper.batchInsert(cpPhyWarehouseRequest.getCpCPhyWarehouseItemList());
        }

        if(CollectionUtils.isNotEmpty(cpPhyWarehouseRequest.getUpdateCpCPhyWarehouseItemList())){
            //修改明细数据
            for(CpCPhyWarehouseItem cpCPhyWarehouseItem : cpPhyWarehouseRequest.getUpdateCpCPhyWarehouseItemList() ){
                cpCPhyWarehouseItemMapper.updateById(cpCPhyWarehouseItem);
            }
        }
        result.setCode(ResultCode.SUCCESS);
        result.setMessage("修改成功");
        return result;
    }

    @Transactional(rollbackFor = Exception.class)
    public ValueHolderV14 warehosueDelete(CpPhyWarehouseRequest cpPhyWarehouseRequest) {
        //实体仓目前只允许删除明细
        ValueHolderV14 result = new ValueHolderV14();
        if(CollectionUtils.isNotEmpty(cpPhyWarehouseRequest.getCpCPhyWarehouseItemList())){
            List<Long> itemIds = cpPhyWarehouseRequest.getCpCPhyWarehouseItemList().stream().map(CpCPhyWarehouseItem ::getId)
                    .collect(Collectors.toList());
            cpCPhyWarehouseItemMapper.deleteBatchIds(itemIds);
        }
        result.setCode(ResultCode.SUCCESS);
        result.setMessage("明细删除成功");
        return result;
    }




    /**
     * 主表参数校验
     *
     * @return
     */
    public static ValueHolderV14 checkWareHouseParam(CpPhyWarehouseRequest cpCPhyWarehouseCmdRequest) {
        ValueHolderV14 result = new ValueHolderV14();
        if (null == cpCPhyWarehouseCmdRequest.getCpCPhyWarehouse()) {
            HashMap map = new HashMap();
            map.put("code", ResultCode.FAIL);
            map.put("message", "请传入主表字段信息");
            map.put("data", new HashMap<>());
            result.setData(map);
            return result;
        }
        if (null == cpCPhyWarehouseCmdRequest.getCpCPhyWarehouse().getEcode()
                || StringUtils.isEmpty(cpCPhyWarehouseCmdRequest.getCpCPhyWarehouse().getEname())) {
            HashMap map = new HashMap();
            map.put("code", ResultCode.FAIL);
            map.put("message", "[实体仓库名称/编码不能为空]");
            map.put("data", new HashMap<>());
            result.setData(map);
            return result;
        }
        HashMap map = new HashMap();
        map.put("code", ResultCode.SUCCESS);
        map.put("message", "校验通过");
        map.put("data", new HashMap<>());
        result.setData(map);
        return result;
    }


    /**
     * 子表参数校验
     *
     * @param cpCPhyWarehouseItemList
     * @return
     */
    public static ValueHolderV14 checkWareHouseItemParam(List<CpCPhyWarehouseItem> cpCPhyWarehouseItemList) {
        ValueHolderV14 result = new ValueHolderV14();
        StringBuffer massage = new StringBuffer();
        int count = 1;
        for (CpCPhyWarehouseItem cpCPhyWarehouseItem : cpCPhyWarehouseItemList) {
            StringBuffer errorMassage = new StringBuffer();
            if (null == cpCPhyWarehouseItem.getCpCLogisticsId()) {
                errorMassage.append("[物流公司id不能为空]");
            }
            if (StringUtils.isNotEmpty(errorMassage)) {
                massage.append("第" + count + "条记录:" + errorMassage + "/n");
            }
            count++;
        }
        if (StringUtils.isNotEmpty(massage.toString())) {
            HashMap map = new HashMap();
            map.put("code", ResultCode.FAIL);
            map.put("message", massage.toString());
            map.put("data", new HashMap<>());
            result.setData(map);
            return result;
        }
        HashMap map = new HashMap();
        map.put("code", ResultCode.SUCCESS);
        map.put("message", "校验通过");
        map.put("data", new HashMap<>());
        result.setData(map);
        return result;
    }

}
