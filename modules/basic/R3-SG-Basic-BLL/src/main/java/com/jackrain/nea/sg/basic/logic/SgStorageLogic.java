package com.jackrain.nea.sg.basic.logic;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.model.util.ModelUtil;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.mapper.SgBStorageMapper;
import com.jackrain.nea.sg.basic.model.SgStorageUpdateCommonModel;
import com.jackrain.nea.sg.basic.model.result.SgStorageUpdateCommonResult;
import com.jackrain.nea.sg.basic.model.table.SgBStorage;
import com.jackrain.nea.sg.basic.utils.StorageUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/3/13 14:50
 */
@Component
@Slf4j
public class SgStorageLogic {

    @Autowired
    private SgBStorageMapper sgBStorageMapper;

    /**
     * 初始化库存
     *
     * @param updateModel
     * @param loginUser
     * @return ValueHolderV14<SgStorageUpdateCommonResult>
     */
    public ValueHolderV14<SgStorageUpdateCommonResult> initStorage(
            SgStorageUpdateCommonModel updateModel,
            User loginUser) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgStorageLogic.initStorage. ReceiveParams:request="
                    + JSONObject.toJSONString(updateModel) + ";");
        }

        ValueHolderV14<SgStorageUpdateCommonResult> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        SgStorageUpdateCommonResult commonResult = new SgStorageUpdateCommonResult();

        SgBStorage sgBStorage = new SgBStorage();
        BeanUtils.copyProperties(updateModel, sgBStorage);
        sgBStorage.setId(ModelUtil.getSequence(SgConstants.SG_B_STORAGE));
        sgBStorage.setQtyPreout(BigDecimal.ZERO);
        sgBStorage.setQtyPrein(BigDecimal.ZERO);
        sgBStorage.setQtyStorage(BigDecimal.ZERO);
        sgBStorage.setAmtCost(BigDecimal.ZERO);
        sgBStorage.setQtyAvailable(BigDecimal.ZERO);
        sgBStorage.setQtyPreoutTeus(BigDecimal.ZERO);
        sgBStorage.setQtyPreinTeus(BigDecimal.ZERO);
        sgBStorage.setQtyStorageTeus(BigDecimal.ZERO);
        sgBStorage.setPsCBrandId(-1L);

        int insertResult = sgBStorageMapper.insert(sgBStorage);

        if (insertResult <= 0) {

            log.error("SgStorageLogic.initStorage 店仓库存初始化插入失败！单据编号:{},店仓编码:{},商品条码:{};",
                    updateModel.getBillNo(),
                    updateModel.getCpCStoreEcode(),
                    updateModel.getPsCSkuEcode());

            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("店仓库存初始化插入失败！",
                    loginUser.getLocale(),
                    updateModel.getCpCStoreEcode(),
                    updateModel.getPsCSkuEcode()));
            return holder;
        }

        holder.setData(commonResult);
        holder.setCode(ResultCode.SUCCESS);
        holder.setMessage(Resources.getMessage("店仓库存初始化插入成功！", loginUser.getLocale()));

        return holder;
    }

    /**
     * 批量初始化库存
     *
     * @param updateModelList
     * @param loginUser
     * @return ValueHolderV14<SgStorageUpdateCommonResult>
     */
    public ValueHolderV14<SgStorageUpdateCommonResult> initStorageList(
            List<SgStorageUpdateCommonModel> updateModelList,
            User loginUser) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgStorageLogic.initStorageList. ReceiveParams:updateModelList="
                    + JSONObject.toJSONString(updateModelList) + ";");
        }

        ValueHolderV14<SgStorageUpdateCommonResult> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        SgStorageUpdateCommonResult commonResult = new SgStorageUpdateCommonResult();
        List<SgBStorage> batchInitList = new ArrayList<>();

        for (SgStorageUpdateCommonModel updateModel : updateModelList) {

            SgBStorage sgBStorage = new SgBStorage();
            BeanUtils.copyProperties(updateModel, sgBStorage);
            sgBStorage.setId(ModelUtil.getSequence(SgConstants.SG_B_STORAGE));
            sgBStorage.setQtyPreout(BigDecimal.ZERO);
            sgBStorage.setQtyPrein(BigDecimal.ZERO);
            sgBStorage.setQtyStorage(BigDecimal.ZERO);
            sgBStorage.setAmtCost(BigDecimal.ZERO);
            sgBStorage.setQtyAvailable(BigDecimal.ZERO);
            sgBStorage.setQtyPreoutTeus(BigDecimal.ZERO);
            sgBStorage.setQtyPreinTeus(BigDecimal.ZERO);
            sgBStorage.setQtyStorageTeus(BigDecimal.ZERO);
            sgBStorage.setPsCBrandId(-1L);

            batchInitList.add(sgBStorage);
        }

        List<List<SgBStorage>> insertPageList =
                StorageUtils.getBaseModelPageList(batchInitList, SgConstants.SG_COMMON_INSERT_PAGE_SIZE);

        //分页批量更新
        for (List<SgBStorage> pageList : insertPageList) {

            if (CollectionUtils.isEmpty(pageList)) {
                continue;
            }

            int insertResult = sgBStorageMapper.batchInsert(pageList);

            if (insertResult != pageList.size()) {
                log.error("SgStorageLogic.initStorageList. 店仓批量库存初始化插入失败！");
                holder.setCode(ResultCode.FAIL);
                holder.setMessage(Resources.getMessage("店仓批量库存初始化插入失败！",
                        loginUser.getLocale()));
                return holder;
            }
        }

        holder.setData(commonResult);
        holder.setCode(ResultCode.SUCCESS);
        holder.setMessage(Resources.getMessage("店仓批量库存初始化插入成功！", loginUser.getLocale()));

        return holder;
    }
}
