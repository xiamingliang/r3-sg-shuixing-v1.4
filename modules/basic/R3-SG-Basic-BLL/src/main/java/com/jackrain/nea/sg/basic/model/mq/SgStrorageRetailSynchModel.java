package com.jackrain.nea.sg.basic.model.mq;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class SgStrorageRetailSynchModel implements Serializable {

    /** 变动日期 */
    private Long changeTime;
    /** 单据编号 */
    private String billNo;
    /** 店仓id */
    private Long cpCStoreId;
    /** 条码id */
    private Long psCSkuId;
    /** 条码编码 */
    private String psCSkuEcode;
    /** 条码编码 */
    private String psCProEcode;
    /** 占用变动数量 */
    private BigDecimal qtyPreoutChange;
    /** 在库变动数量 */
    private BigDecimal qtyStorageChange;
    /** 可用库存 */
    private BigDecimal qtyAvailable;
    /** 在库库存 */
    private BigDecimal qtyStorage;
}
