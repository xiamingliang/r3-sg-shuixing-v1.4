package com.jackrain.nea.sg.basic.services;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.redis.config.CusRedisTemplate;
import com.jackrain.nea.redis.util.RedisOpsUtil;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.config.SgStorageControlConfig;
import com.jackrain.nea.sg.basic.model.SgStorageUpdateCommonModel;
import com.jackrain.nea.sg.basic.model.SgStorageUpdateControlModel;
import com.jackrain.nea.sg.basic.model.request.SgStorageSingleUpdateRequest;
import com.jackrain.nea.sg.basic.model.request.SgStorageUpdateBillItemRequest;
import com.jackrain.nea.sg.basic.model.request.SgStorageUpdateBillRequest;
import com.jackrain.nea.sg.basic.model.result.SgStorageBatchUpdateResult;
import com.jackrain.nea.sg.basic.utils.StorageLogUtils;
import com.jackrain.nea.sg.basic.utils.StorageUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.stereotype.Component;
import org.thymeleaf.util.StringUtils;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/7/10 00:00
 */
@Component
@Slf4j
public class SgStorageRedisUpdateService {

    @Autowired
    SgStorageControlConfig sgStorageControlConfig;

    /**
     * 通用单个单据库存更新接口
     *
     * @param request
     * @return
     */
    public ValueHolderV14<SgStorageBatchUpdateResult> updateStorageBill(SgStorageSingleUpdateRequest request) {

        long startTime = System.currentTimeMillis();

        if (log.isDebugEnabled()) {
            log.debug("Start SgStorageRedisUpdateService.updateStorageBill. ReceiveParams:request:{};"
                    , JSONObject.toJSONString(request.getBill()));
        }

        ValueHolderV14<SgStorageBatchUpdateResult> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        SgStorageBatchUpdateResult updateResult = new SgStorageBatchUpdateResult();
        Map<String, SgStorageUpdateCommonModel> redisSynchMqItemMap = new HashMap<>();
        Map<String, SgStorageUpdateCommonModel> outStockItemMap = new HashMap<>();
        List<SgStorageUpdateCommonModel> outStockItemList = new ArrayList<>();
        List<SgStorageUpdateCommonModel> redisSynchMqItemList = new ArrayList<>();
        List<String> batchUpdateRedisKeyList = new ArrayList<>();
        List<String> batchUpdateRedisQttyList = new ArrayList<>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        int preoutUpdateResult = SgConstantsIF.PREOUT_RESULT_SUCCESS;
        //发生错误的明细数
        long errorItemQty = 0;

        CusRedisTemplate<String, String> redisTemplate = RedisOpsUtil.getStrRedisTemplate();
        DefaultRedisScript<List> redisScript = new DefaultRedisScript<>();
        redisScript.setLocation(new ClassPathResource("lua/UpdateRedisStorage.lua"));
        redisScript.setResultType(List.class);

        //检查入参
        holder = checkServiceParam(request, request.getLoginUser());
        if (ResultCode.FAIL == holder.getCode()) {
            return holder;
        }

        User loginUser = request.getLoginUser();
        SgStorageUpdateBillRequest billInfo = request.getBill();

        if (CollectionUtils.isEmpty(billInfo.getItemList())) {
            return holder;
        }

        //Redis更新流水键
        String redisBillFtpKey = SgConstants.REDIS_KEY_PREFIX_STORAGE_FTP +
                sdf.format(new Date()) + "_" + billInfo.getBillNo();

        //初始化库存更新控制对象（默认不允许负库存）
        SgStorageUpdateControlModel controlModel = new SgStorageUpdateControlModel();

        //APOLLO系统配置优先级最低
        if (sgStorageControlConfig != null) {
            BeanUtils.copyProperties(sgStorageControlConfig, controlModel);
        }

        //当消费者设置了控制类里的情况下，优先使用消费者的控制类
        if (request.getControlModel() != null) {
            BeanUtils.copyProperties(request.getControlModel(), controlModel);
        }

        if (log.isDebugEnabled()) {
            log.debug("SgStorageRedisUpdateService.updateStorageBill. billInfo.controlModel:{};"
                    + JSONObject.toJSONString(controlModel));
        }

        for (SgStorageUpdateBillItemRequest itemInfo : billInfo.getItemList()) {

            SgStorageUpdateControlModel detailControlModel = new SgStorageUpdateControlModel();
            SgStorageUpdateCommonModel updateCommonModel = new SgStorageUpdateCommonModel();

            if (log.isDebugEnabled()) {
                log.debug("SgStorageRedisUpdateService.updateStorageBill. SgStorageUpdateBillItemRequest:{};"
                        , JSONObject.toJSONString(itemInfo));
            }

            billInfo.setRedisBillFtpKey(redisBillFtpKey);

            StorageUtils.setBModelDefalutData(updateCommonModel, loginUser);
            BeanUtils.copyProperties(billInfo, updateCommonModel);
            BeanUtils.copyProperties(itemInfo, updateCommonModel);

            //单据取消的情况下，相应数量取反
            if (billInfo.getIsCancel()) {
                updateCommonModel.setQtyPreinChange(updateCommonModel.getQtyPreinChange().negate());
                updateCommonModel.setQtyPreoutChange(updateCommonModel.getQtyPreoutChange().negate());
                updateCommonModel.setQtyStorageChange(updateCommonModel.getQtyStorageChange().negate());
            }

            if (updateCommonModel.getControlmodel() == null) {
                detailControlModel = controlModel;
            } else {
                BeanUtils.copyProperties(updateCommonModel.getControlmodel(), detailControlModel);
            }

            //占用是否允许负库存,在途是否允许负库存,在库是否允许负库存,可用在库是否允许负库存(1：允许 0：不允许)
            String strConrol = (detailControlModel.isNegativePreout() ? "1" : "0") + "#" +
                    (detailControlModel.isNegativePrein() ? "1" : "0") + "#" +
                    (detailControlModel.isNegativeStorage() ? "1" : "0") + "#" +
                    (detailControlModel.isNegativeAvailable() ? "1" : "0");

            //LUA Redis键：sg:storage:逻辑仓ID:SKUID
            batchUpdateRedisKeyList.add(SgConstants.REDIS_KEY_PREFIX_STORAGE + updateCommonModel.getCpCStoreId() +
                    ":" + updateCommonModel.getPsCSkuId());

            //LUA参数:占用数量,在途数量,在库数量,明细ID,库存控制
            batchUpdateRedisQttyList.add(updateCommonModel.getQtyPreoutChange() + "," +
                    updateCommonModel.getQtyPreinChange() + "," +
                    updateCommonModel.getQtyStorageChange() + "," +
                    updateCommonModel.getBillId().toString() + "_" + updateCommonModel.getBillItemId() + "," +
                    strConrol);

            redisSynchMqItemMap.put(updateCommonModel.getBillId().toString() + "_" + updateCommonModel.getBillItemId().toString(),
                    updateCommonModel);
            outStockItemMap.put(updateCommonModel.getBillId().toString() + "_" + updateCommonModel.getBillItemId().toString(),
                    updateCommonModel);

        }

        //批量更新单据明细分页
        int pageSize = controlModel.getBatchUpdateItems();
        int listSize = batchUpdateRedisKeyList.size();
        int page = listSize / pageSize;

        if (listSize % pageSize != 0) {
            page++;
        }

        boolean setDataFlg = false;

        try {

            //分页批量更新
            for (int i = 0; i < page; i++) {

                List<String> keyList = batchUpdateRedisKeyList.subList(i * pageSize,
                        (((i + 1) * pageSize > listSize ? listSize : pageSize * (i + 1))));

                List<String> qtyList = batchUpdateRedisQttyList.subList(i * pageSize,
                        (((i + 1) * pageSize > listSize ? listSize : pageSize * (i + 1))));

                List<String> qtyListItem = new ArrayList<>();
                qtyListItem.addAll(qtyList);
                qtyListItem.add(redisBillFtpKey);
                qtyListItem.add(String.valueOf(controlModel.getPreoutOperateType()));
                qtyListItem.add(billInfo.getIsCancel() ? "1" : "0");

                if (log.isDebugEnabled()) {
                    log.debug("SgStorageRedisUpdateService.updateStorageBill. redisTemplate.execute.start. ReceiveParams:keyList:{}, " +
                                    "qtyList:{};"
                            , JSONObject.toJSONString(keyList), JSONObject.toJSONString(qtyListItem));
                }

                List result = redisTemplate.execute(redisScript, keyList, qtyListItem.toArray(new String[qtyListItem.size()]));

                if (log.isDebugEnabled()) {
                    log.debug("SgStorageRedisUpdateService.updateStorageBill. redisTemplate.execute.end. ReturnResult:result:{};"
                            , JSONObject.toJSONString(result));
                }

                if (SgConstantsIF.PREOUT_OPT_TYPE_OUT_STOCK != controlModel.getPreoutOperateType()) {
                    setDataFlg = true;
                }

                if (!CollectionUtils.isEmpty(result) && Integer.valueOf((String) result.get(0)) != SgConstantsIF.PREOUT_RESULT_SUCCESS) {

                    List<String> billList = (ArrayList) result.get(1);

                    if (!CollectionUtils.isEmpty(billList)) {
                        for (String luaResult : billList) {
                            if (outStockItemMap.containsKey(luaResult)) {
                                outStockItemList.add(outStockItemMap.get(luaResult));
                                redisSynchMqItemMap.remove(luaResult);
                            }
                        }
                    }

                    preoutUpdateResult = Integer.valueOf((String) result.get(0));

                    if (SgConstantsIF.PREOUT_RESULT_OUT_STOCK != preoutUpdateResult) {

                        holder.setCode(ResultCode.FAIL);
                        //报出错的情况下，错误件数为当前批量更新件数
                        errorItemQty = errorItemQty + keyList.size();

                        if (result.get(2) != null && outStockItemMap.containsKey(result.get(2))) {
                            SgStorageUpdateCommonModel negativeModel = outStockItemMap.get(result.get(2));

                            if (log.isDebugEnabled()) {
                                log.debug("SgStorageRedisUpdateService.updateStorageBill. 当前库存变动明细库存不足！！单据编号:{}，" +
                                                "逻辑仓ID:{}，商品条码ID:{}，占用变动数量:{}，在途变动数量:{}，在库变动数量:{}",
                                        negativeModel.getBillNo(), negativeModel.getCpCStoreId(),
                                        negativeModel.getPsCSkuId(), negativeModel.getQtyPreoutChange(),
                                        negativeModel.getQtyPreinChange(), negativeModel.getQtyStorageChange());
                            }

                            holder.setMessage(Resources.getMessage("店仓库存不足！",
                                    loginUser.getLocale(), negativeModel.getCpCStoreEcode(), negativeModel.getPsCSkuEcode()));
                        }

                        break;
                    }

                }

            }

        } catch (Exception e) {

            String errorMsg = StorageLogUtils.getErrMessage(e, request.getMessageKey(), loginUser);
            updateResult.setPreoutUpdateResult(controlModel.getPreoutOperateType());

            List keyList = new ArrayList();
            keyList.add(redisBillFtpKey);

            rollbackStorageBill(keyList);

            holder.setCode(ResultCode.FAIL);
            holder.setData(updateResult);
            holder.setMessage(Resources.getMessage("店仓库存批量更新失败！", loginUser.getLocale()).concat(errorMsg));

            if (log.isDebugEnabled()) {
                log.debug("Finish SgStorageRedisUpdateService.updateStorageBill. error ReturnResult:errorBillItemQty:{} spend time:{}ms;"
                        , JSONObject.toJSONString(holder.getData() == null ? 0 : holder.getData().getErrorBillItemQty()), System.currentTimeMillis() - startTime);
            }

            return holder;
        }

        if (ResultCode.SUCCESS == holder.getCode() && redisSynchMqItemMap.size() > 0) {

            redisSynchMqItemList.addAll(new ArrayList<>(redisSynchMqItemMap.values()));

        } else if (ResultCode.FAIL == holder.getCode() && setDataFlg) {

            List keyList = new ArrayList();
            keyList.add(redisBillFtpKey);

            rollbackStorageBill(keyList);

        }

        updateResult.setRedisBillFtpKeyList(new ArrayList<>(Arrays.asList(redisBillFtpKey)));
        updateResult.setErrorBillItemQty(errorItemQty);
        updateResult.setPreoutUpdateResult(preoutUpdateResult);
        updateResult.setOutStockItemList(outStockItemList);
        updateResult.setRedisSynchMqItemList(redisSynchMqItemList);
        holder.setData(updateResult);

        // 没有错误明细的情况下，设置成功运行结果，
        // 有错误明细的情况下，在发生错误的明细中设置运行结果
        if (errorItemQty < 1) {

            holder.setCode(ResultCode.SUCCESS);
            holder.setMessage(Resources.getMessage("当前单据的店仓库存变动消息处理完毕！",
                    loginUser.getLocale(), holder.getMessage(), billInfo.getBillNo()));

        }

        if (log.isDebugEnabled()) {
            log.debug("Finish SgStorageRedisUpdateService.updateStorageBill. ReturnResult:errorBillItemQty:{} spend time:{}ms;"
                    , JSONObject.toJSONString(holder.getData() == null ? 0 : holder.getData().getErrorBillItemQty()), System.currentTimeMillis() - startTime);
        }

        return holder;
    }

    /**
     * @param request
     * @param loginUser
     * @return
     */
    private ValueHolderV14<SgStorageBatchUpdateResult> checkServiceParam(SgStorageSingleUpdateRequest request,
                                                                         User loginUser) {

        ValueHolderV14<SgStorageBatchUpdateResult> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");

        if (request == null) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("请求体为空！", loginUser.getLocale()));
            return holder;
        }

        if (request.getLoginUser() == null) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("操作用户信息不能为空！", loginUser.getLocale()));
        }

        if (request.getBill() == null || StringUtils.isEmpty(request.getBill().getBillNo())) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("店仓库存更新单据不能为空！", loginUser.getLocale()));
        }

        if (ResultCode.FAIL == holder.getCode() && log.isDebugEnabled()) {
            log.debug("SgStorageRedisUpdateService.checkServiceParam. checkServiceParam error:"
                    + holder.getMessage() + ";");
        }

        return holder;

    }

    public ValueHolderV14<SgStorageBatchUpdateResult> rollbackStorageBill(List<String> redisBillFtpKeyList) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgStorageRedisUpdateService.rollbackStorageBill. ReceiveParams:redisBillFtpKeyList:{};"
                    , JSONObject.toJSONString(redisBillFtpKeyList));
        }

        ValueHolderV14<SgStorageBatchUpdateResult> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");

        if (CollectionUtils.isEmpty(redisBillFtpKeyList)) {
            return holder;
        }

        CusRedisTemplate<String, String> redisTemplate = RedisOpsUtil.getStrRedisTemplate();
        DefaultRedisScript<List> redisScript = new DefaultRedisScript<>();
        redisScript.setLocation(new ClassPathResource("lua/RollbackRedisStorage.lua"));
        redisScript.setResultType(List.class);

        redisTemplate.execute(redisScript, redisBillFtpKeyList, "");

        return holder;
    }

}
