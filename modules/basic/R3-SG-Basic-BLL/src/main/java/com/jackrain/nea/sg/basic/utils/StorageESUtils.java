package com.jackrain.nea.sg.basic.utils;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.google.common.collect.Lists;
import com.jackrain.nea.data.basic.model.request.ProInfoQueryRequest;
import com.jackrain.nea.data.basic.services.BasicPsQueryService;
import com.jackrain.nea.es.util.ElasticSearchUtil;
import com.jackrain.nea.ps.api.result.PsCProResult;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sys.domain.BaseModelES;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author 舒威
 * @since 2019/7/1
 * create at : 2019/7/1 18:32
 */
@Slf4j
public class StorageESUtils {

    public static <T extends BaseModelES> T setBModelDefalutData(T sourceModel, User loginUser) {

        Date systemDate = new Date();
        Long loginUserId = loginUser.getId() == null ? null : loginUser.getId().longValue();

        sourceModel.setAdClientId(Long.valueOf(loginUser.getClientId()));
        sourceModel.setAdOrgId(Long.valueOf(loginUser.getOrgId()));
        sourceModel.setIsactive(SgConstants.IS_ACTIVE_Y);
        sourceModel.setOwnerid(loginUserId);
        sourceModel.setOwnername(loginUser.getName());
        sourceModel.setCreationdate(systemDate);
        sourceModel.setModifierid(loginUserId);
        sourceModel.setModifiername(loginUser.getName());
        sourceModel.setModifieddate(systemDate);

        return sourceModel;
    }


    public static <T extends BaseModelES> T setBModelDefalutDataByUpdate(T sourceModel, User loginUser) {
        Date systemDate = new Date();
        Long loginUserId = loginUser.getId() == null ? null : loginUser.getId().longValue();

        sourceModel.setModifierid(loginUserId);
        sourceModel.setModifiername(loginUser.getName());
        sourceModel.setModifieddate(systemDate);

        return sourceModel;
    }

    public static <T extends BaseModelES> List<List<T>> getBaseModelPageList(List<T> sourceList, int pageSize) {

        List<List<T>> resultList = new ArrayList<>();

        if (CollectionUtils.isEmpty(sourceList)) {
            return resultList;
        }

        int listSize = sourceList.size();
        int page = listSize / pageSize;

        if (listSize % pageSize != 0) {
            page++;
        }

        for (int i = 0; i < page; i++) {

            List<T> pageList = sourceList.subList(i * pageSize,
                    (((i + 1) * pageSize > listSize ? listSize : pageSize * (i + 1))));

            resultList.add(pageList);
        }

        return resultList;
    }

    public static <T extends BaseModelES> void pushESBModelByUpdate(T sourceModel, List sourceModels, Long objid, JSONArray ids,
                                                                    String index, String type, String childType, String parentKey,
                                                                    Class a, Class b, boolean isDeletedItems) {
        try {
            // 判断索引是否存在
            if (!ElasticSearchUtil.indexExists(index)) {
                ElasticSearchUtil.indexCreate(b, a);
            }
            //删除明细记录
            if (isDeletedItems) {
                for (Object id : ids) {
                    ElasticSearchUtil.delDocument(index, childType, id, objid);
                }
            }

            if (sourceModel != null) {
                ElasticSearchUtil.indexDocument(index, type, sourceModel, objid);
            }
            
            if (CollectionUtils.isNotEmpty(sourceModels)) {
                ElasticSearchUtil.indexDocuments(index, childType, sourceModels, parentKey.toUpperCase());
            }

        } catch (Exception e) {
            e.printStackTrace();
            log.error("push es error!" + index + ":" + sourceModel.getId() == null ? "" : sourceModel.getId() + ":" + e.getMessage());
        }
    }

    /**
     * 批量推送ES
     */
    public static <T extends BaseModelES> void singlePushES(List<T> source, JSONArray ids, String index, String childType, String parentKey, Class parent, Class child) {
        // 判断索引是否存在
        try {
            if (!ElasticSearchUtil.indexExists(index)) {
                ElasticSearchUtil.indexCreate(child, parent);
            }
            if (CollectionUtils.isNotEmpty(source)) {
                if (StringUtils.isEmpty(parentKey)) {
                    ElasticSearchUtil.indexDocuments(index, index, source);
                } else {
                    ElasticSearchUtil.indexDocuments(index, childType, source, parentKey);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("push single es error!" + index + ":" + ids + ":" + e.getMessage());
        }
    }

    /**
     * 批量推送ES-多线程
     */
    public static <T extends BaseModelES> void batchPushES(List<T> source, int n, String index, String childType, String parentKey, Class parent, Class child) {
        List<List<T>> sourceList = StorageESUtils.averageAssign(source, n);
        if (CollectionUtils.isNotEmpty(sourceList)) {
            // 判断索引是否存在
            if (!ElasticSearchUtil.indexExists(index)) {
                try {
                    ElasticSearchUtil.indexCreate(child, parent);
                } catch (Exception e) {
                    e.printStackTrace();
                    log.error("es create error!" + index + ":" + e.getMessage());
                }
            }
            for (List<T> sources : sourceList) {
                Runnable task = null;
                if (CollectionUtils.isNotEmpty(sources)) {
                    task = () -> {
                        try {
                            if (StringUtils.isEmpty(parentKey)) {
                                ElasticSearchUtil.indexDocuments(index, index, sources);
                            } else {
                                ElasticSearchUtil.indexDocuments(index, childType, sources, parentKey);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            log.error("push batch es error!" + index + ":" + e.getMessage());
                        }
                    };
                    new Thread(task).start();
                }
            }
        }
    }

    /**
     * 将一个list均分成n个list,主要通过偏移量来实现的
     */
    public static <T> List<List<T>> averageAssign(List<T> source, int n) {
        if (CollectionUtils.isEmpty(source)) return null;
        List<List<T>> result = new ArrayList<List<T>>();
        int remaider = source.size() % n; //(先计算出余数)
        int number = source.size() / n; //然后是商
        int offset = 0;//偏移量
        for (int i = 0; i < n; i++) {
            List<T> value = null;
            if (remaider > 0) {
                value = source.subList(i * number + offset, (i + 1) * number + offset + 1);
                remaider--;
                offset++;
            } else {
                value = source.subList(i * number + offset, (i + 1) * number + offset);
            }
            result.add(value);
        }
        return result;
    }


    /**
     * 分批获取商品信息
     */
    public static List<PsCProResult> getPros(List<String> pros, int pageSize) {
        List<List<String>> resultList = new ArrayList<>();
        int listSize = pros.size();
        int page = listSize / pageSize;

        if (listSize % pageSize != 0) {
            page++;
        }

        for (int i = 0; i < page; i++) {
            List<String> pageList = pros.subList(i * pageSize,
                    (((i + 1) * pageSize > listSize ? listSize : pageSize * (i + 1))));
            resultList.add(pageList);
        }

        List<PsCProResult> littleProInfo = Lists.newArrayList();
        BasicPsQueryService psQueryService = ApplicationContextHandle.getBean(BasicPsQueryService.class);

        for (List<String> proList : resultList) {
            ProInfoQueryRequest queryRequest = new ProInfoQueryRequest();
            queryRequest.setProEcodeList(proList);
            List<PsCProResult> proInfo = psQueryService.getLittleProInfo(queryRequest);
            if (CollectionUtils.isEmpty(proInfo)) {
                AssertUtils.logAndThrow("补充商品信息查询结果为空!");
            }
            littleProInfo.addAll(proInfo);
        }

        if (log.isDebugEnabled()) {
            log.debug("补充商品明细查询结果：" + JSONObject.toJSONString(littleProInfo, SerializerFeature.WriteMapNullValue));
        }

        return littleProInfo;
    }

    /**
     * 超长字符串截取
     */
    public static String strSubString(String remark, int size) {
        if (StringUtils.isNotEmpty(remark)) {
            if (remark.length() > size) {
                remark = remark.substring(0, size);
            }
        }
        return remark;
    }
}
