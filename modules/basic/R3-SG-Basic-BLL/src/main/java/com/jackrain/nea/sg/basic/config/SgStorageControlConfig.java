package com.jackrain.nea.sg.basic.config;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/3/24 13:27
 */
@Slf4j
@Configuration
@Data
public class SgStorageControlConfig {

    /** 占用是否允许负库存 */
    @Value("${sg.control.is_negative_preout:false}")
    private boolean isNegativePreout;

    /** 占用错误处理类别(1.报警（允许负库存）2.报错  3.报缺货) */
    @Value("${sg.control.preout_operate_type:2}")
    private int preoutOperateType;

    /** 在途是否允许负库存 */
    @Value("${sg.control.is_negative_prein:false}")
    private boolean isNegativePrein;

    /** 在库是否允许负库存 */
    @Value("${sg.control.is_negative_storage:false}")
    private boolean isNegativeStorage;

    /** 可用是否允许负库存 */
    @Value("${sg.control.is_negative_available:false}")
    private boolean isNegativeAvailable;

    /** 是否启用 PG函数更新库存 */
    @Value("${sg.control.is_pg_function:true}")
    private boolean isPgFunction;

    /** 是否批量更新单据明细 */
    @Value("${sg.control.is_batch_update_item:true}")
    private boolean isBatchUpdateItem;

    /** 单次批量更新单据明细数量 */
    @Value("${sg.control.batch_update_items:20}")
    private int batchUpdateItems;

    /** 是否同步R3基础数据 */
    @Value("${sg.control.is_sync_r3_basic_data:false}")
    private boolean isSyncR3BasicData;

    /** 是否启用 Redis函数更新库存 */
    @Value("${sg.control.is_redis_function:false}")
    private boolean isRedisFunction;

    /** 库存查询单次最大查询量 */
    @Value("${sg.control.storage_sku_pro_max_query_limit:500}")
    private int maxQueryLimit;


}
