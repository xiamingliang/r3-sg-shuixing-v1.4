package com.jackrain.nea.sg.basic.common.enums;

/**
 * 库存变动类型
 * @Description:
 * @Author: chenb
 * @Date: 2019/3/20 18:09
 */
public enum StorageType {

    ERROR(0, "错误"),
    STORAGE_PREOUT(1, "占用量"),
    STORAGE_PREIN(2, "在途"),
    STORAGE(3, "在库");

    private String desc;
    private Integer value;

    StorageType(Integer value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public static StorageType valueOf(Integer value) {
        switch (value) {
            case 1:
                return STORAGE_PREOUT;
            case 2:
                return STORAGE_PREIN;
            case 3:
                return STORAGE;
            default:
                return ERROR;
        }

    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }
}
