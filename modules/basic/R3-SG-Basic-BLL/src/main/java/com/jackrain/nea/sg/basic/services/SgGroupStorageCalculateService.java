package com.jackrain.nea.sg.basic.services;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.jackrain.nea.config.PropertiesConf;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.model.util.ModelUtil;
import com.jackrain.nea.r3.mq.util.R3MqSendHelper;
import com.jackrain.nea.redis.config.CusRedisTemplate;
import com.jackrain.nea.redis.util.RedisOpsUtil;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.config.SgStorageMqConfig;
import com.jackrain.nea.sg.basic.mapper.SgBGroupStorageMapper;
import com.jackrain.nea.sg.basic.mapper.SgBStorageMapper;
import com.jackrain.nea.sg.basic.model.request.SgGroupStorageCalculateRequest;
import com.jackrain.nea.sg.basic.model.request.SgGroupStorageSkuRequest;
import com.jackrain.nea.sg.basic.model.request.SgGroupStorageVirtualSkuRequest;
import com.jackrain.nea.sg.basic.model.result.SgGoupStorageCalculateResult;
import com.jackrain.nea.sg.basic.model.result.SgGoupStorageGroupSkuResult;
import com.jackrain.nea.sg.basic.model.result.SgGoupStorageSkuResult;
import com.jackrain.nea.sg.basic.model.table.SgBGroupStorage;
import com.jackrain.nea.sg.basic.model.table.SgBStorage;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.basic.utils.StorageUtils;
import com.jackrain.nea.sg.oms.model.request.SgChannelStorageOmsBufferMessageItemRequest;
import com.jackrain.nea.sg.oms.model.request.SgChannelStorageOmsBufferMessageRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.DateUtil;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @author 舒威
 * @since 2019/7/9
 * create at : 2019/7/9 20:28
 */
@Slf4j
@Component
public class SgGroupStorageCalculateService {

    @Autowired
    private SgBStorageMapper storageMapper;

    @Autowired
    private SgBGroupStorageMapper groupStorageMapper;

    @Autowired
    private R3MqSendHelper r3MqSendHelper;

    @Autowired
    private SgStorageMqConfig mqConfig;

    @Autowired
    private PropertiesConf pconf;

    @Transactional(rollbackFor = Exception.class)
    public ValueHolderV14<SgGoupStorageCalculateResult> calculateGroupStorage(SgGroupStorageCalculateRequest request) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgGroupStorageCalculateService.calculateGroupStorage:request:{};", JSONObject.toJSONString(request));
        }

        User user = request.getLoginUser();
        AssertUtils.notNull(user, "用户未登录！");

        ValueHolderV14<SgGoupStorageCalculateResult> vh = new ValueHolderV14<>();
        vh.setCode(ResultCode.SUCCESS);
        vh.setMessage("success");

        SgGoupStorageCalculateResult data = new SgGoupStorageCalculateResult();
        //记录组合商品库存新增个数
        Integer add = 0;
        JSONArray addArr = new JSONArray();
        //记录组合商品库存修改个数
        Integer update = 0;
        JSONArray updateArr = new JSONArray();
        //记录组合商品库存修改失败个数
        Integer fail = 0;
        JSONArray failArr = new JSONArray();
        //记录库存计算结果
        HashMap<String, Object> calculator = Maps.newHashMap();
        List<SgGoupStorageGroupSkuResult> groupSkuResults = Lists.newArrayList();

        //记录虚拟条码id
        List<Long> psCSkuIdList = Lists.newArrayList();
        HashMap<Long, BigDecimal> qtyChangeMap = Maps.newHashMap();
        CusRedisTemplate<String, String> redisTemplate = RedisOpsUtil.getObjRedisTemplate();
        List<SgGroupStorageVirtualSkuRequest> virtualSkuList = request.getVirtualSkuList();
        for (SgGroupStorageVirtualSkuRequest virtualSkuRequest : virtualSkuList) {
            SgGoupStorageGroupSkuResult groupSkuResult = new SgGoupStorageGroupSkuResult();
            //虚拟条码id
            Long skuId = virtualSkuRequest.getId();
            //虚拟条码Ecode
            String skuEcode = virtualSkuRequest.getEcode();
            Long storeId = virtualSkuRequest.getCpCStoreId();
            String poolKey = SgConstantsIF.COMBINED_POOL + ":" + skuId + ":" + storeId;
            try {
                if (null != redisTemplate.opsForValue().get(poolKey)) {
                    fail++;
                    String errMsg = poolKey + "：正在计算组合商品(福袋)库存";
                    log.debug(errMsg);
                    failArr.add(errMsg);
                    continue;
                }
                redisTemplate.opsForValue().set(poolKey, "OK", 5, TimeUnit.MINUTES);
                //每组抽取数量-福袋
                Integer groupExtractNum = virtualSkuRequest.getGroupExtractNum();
                //获取虚拟条码下的实际条码集合
                List<SgGroupStorageSkuRequest> skus = virtualSkuRequest.getSku();
                if (CollectionUtils.isNotEmpty(skus)) {
                    //记录实际条码id-实际条码
                    Map<Long, SgGroupStorageSkuRequest> skuRequestMap = Maps.newHashMap();
                    //实际条码id
                    List<Long> skuIds = Lists.newArrayList();
                    for (SgGroupStorageSkuRequest sku : skus) {
                        skuRequestMap.put(sku.getId(), sku);
                        skuIds.add(sku.getId());
                    }
                    List<SgBStorage> bStorages = storageMapper.selectList(new QueryWrapper<SgBStorage>().lambda()
                            .select(SgBStorage::getPsCSkuId, SgBStorage::getPsCSkuEcode, SgBStorage::getQtyAvailable)
                            .eq(SgBStorage::getCpCStoreId, storeId)
                            .eq(SgBStorage::getIsactive, SgConstants.IS_ACTIVE_Y)
                            .in(SgBStorage::getPsCSkuId, skuIds));
                    if (CollectionUtils.isNotEmpty(bStorages)) {
                        if (log.isDebugEnabled()) {
                            log.debug("逻辑仓" + storeId + ",虚拟条码" + skuId + ",实际条码库存" + JSONObject.toJSONString(bStorages, SerializerFeature.WriteMapNullValue));
                        }
                        List<BigDecimal> minAvaliableList = Lists.newArrayList();
                        List<SgGoupStorageSkuResult> skuResults = Lists.newArrayList();
                        for (SgBStorage storage : bStorages) {
                            SgGroupStorageSkuRequest skuRequest = skuRequestMap.get(storage.getPsCSkuId());
                            BigDecimal num = skuRequest.getNum();
                            //最小库存（组合商品）=可用数量/组合数量
                            BigDecimal qtyAvaliable = storage.getQtyAvailable().divide(num, 0, BigDecimal.ROUND_DOWN);
                            minAvaliableList.add(qtyAvaliable);
                            //记录实际条码库存
                            SgGoupStorageSkuResult skuResult = new SgGoupStorageSkuResult();
                            skuResult.setId(storage.getPsCSkuId());
                            skuResult.setEcode(storage.getPsCSkuEcode());
                            skuResult.setNum(num);
                            skuResult.setQtyAvaliable(storage.getQtyAvailable());
                            skuResults.add(skuResult);
                            //记录单个实际条码可用库存
                            skuRequest.setSingleStorage(qtyAvaliable);
                        }
                        groupSkuResult.setSkuResults(skuResults);
                        BigDecimal min;
                        List<BigDecimal> sortList = minAvaliableList.stream().sorted(Comparator.reverseOrder()).collect(Collectors.toList());
                        min = sortList.get(sortList.size() - 1);
                        if (groupExtractNum != null) {
                            //check福袋商品实际条码 分组编号是否为空
                            List<SgGroupStorageSkuRequest> groupNumIsNull = skus.stream()
                                    .filter(s -> s.getGroupnum() == null).collect(Collectors.toList());
                            if (CollectionUtils.isNotEmpty(groupNumIsNull)) {
                                AssertUtils.logAndThrow("逻辑仓" + storeId + ",虚拟条码" + skuId + ",实际条码存在分组编号为空!");
                            }
                            //福袋商品
                            min = getLuckyProMinStorage(skus, groupExtractNum);
                        }
                        log.debug("逻辑仓" + storeId + ",虚拟条码" + skuId + "计算最小库存" + min);
                        //更新组合商品库存
                        List<SgBGroupStorage> skuGroupStorages = groupStorageMapper.selectList(new QueryWrapper<SgBGroupStorage>().lambda()
                                .eq(SgBGroupStorage::getPsCSkuId, skuId)
                                .eq(SgBGroupStorage::getCpCStoreId, storeId)
                                .eq(SgBGroupStorage::getIsactive, SgConstants.IS_ACTIVE_Y));
                        if (CollectionUtils.isEmpty(skuGroupStorages)) {
                            //新增
                            insertGroupStorage(virtualSkuRequest, min, user);
                            add++;
                            addArr.add(poolKey);
                            //记录组合商品库存变化
                            if (min.compareTo(BigDecimal.ZERO) != 0) {
                                qtyChangeMap.put(skuId, min);
                                groupSkuResult.setQtyChange(min);
                            }
                        } else {
                            BigDecimal qtyStorage = Optional.ofNullable(skuGroupStorages.get(0).getQtyStorage()).orElse(BigDecimal.ZERO);
                            //修改
                            updateGroupStorage(skuGroupStorages.get(0), min, user);
                            update++;
                            updateArr.add(poolKey);
                            //记录组合商品库存变化
                            BigDecimal qtyChange = min.subtract(qtyStorage);
                            if (qtyChange.compareTo(BigDecimal.ZERO) != 0) {
                                qtyChangeMap.put(skuId, qtyChange);
                                groupSkuResult.setQtyChange(qtyChange);
                            }
                        }
                        //记录虚拟条码库存更新成功的id
                        psCSkuIdList.add(skuId);
                        //记录组合商品库存数量
                        if (groupExtractNum != null) {
                            groupSkuResult.setGroupExtractNum(groupExtractNum);
                        }
                        groupSkuResult.setId(skuId);
                        groupSkuResult.setEcode(skuEcode);
                        groupSkuResult.setCpCStoreId(storeId);
                        groupSkuResult.setMinStorage(min);
                        groupSkuResults.add(groupSkuResult);
                    } else {
                        log.debug("逻辑仓" + storeId + ",虚拟条码" + skuId + ",实际条码库存不存在");
                    }
                } else {
                    log.debug("逻辑仓" + storeId + ",虚拟条码" + skuId + "沒有实际条码");
                }
            } catch (Exception e) {
                e.printStackTrace();
                fail++;
                String errMsg = poolKey + "：计算异常" + e.getMessage();
                failArr.add(errMsg);
            } finally {
                try {
                    redisTemplate.delete(poolKey);
                } catch (Exception ex) {
                    ex.printStackTrace();
                    log.error(this.getClass().getName() + poolKey + "释放锁失败，请联系管理员！");
                    AssertUtils.logAndThrowExtra("释放锁失败，请联系管理员！", ex);
                }
            }
        }

        //判断是否同步渠道库存
        boolean isChannelSynch = Boolean.parseBoolean(pconf.getProperty("sg.control.is_synch_channel_storage"));
        if (isChannelSynch && CollectionUtils.isNotEmpty(psCSkuIdList)) {
            /* 同步渠道库存(MQ)*/
            log.debug("虚拟条码库存变化" + qtyChangeMap);
            //渠道id - 虚拟条码List
            Map<Long, SgChannelStorageOmsBufferMessageRequest> channelStorageMap = Maps.newHashMap();
            for (SgGroupStorageVirtualSkuRequest virtualSkuRequest : virtualSkuList) {
                //封装同步渠道库存 model
                Long virtualSkuid = virtualSkuRequest.getId();
                //上游有传渠道id 转为-1  若未传渠道id传null
                Long cpCShopId = virtualSkuRequest.getCpCShopId();
                if (cpCShopId != null) cpCShopId = -1L;
                String billNo = Optional.ofNullable(virtualSkuRequest.getBillNo()).orElse(UUID.randomUUID().toString().replaceAll("-", ""));
                String changeTime = virtualSkuRequest.getChangeTime();
                Date changeDate = StringUtils.isNotEmpty(changeTime) ? DateUtil.stringToDate(changeTime) : new Date();

                BigDecimal qtyChange = qtyChangeMap.get(virtualSkuid);
                //有库存变化的时候 会同步渠道库存
                if (qtyChange != null) {
                    if (channelStorageMap.containsKey(cpCShopId)) {
                        SgChannelStorageOmsBufferMessageRequest channelStorageRequest = channelStorageMap.get(cpCShopId);
                        List<SgChannelStorageOmsBufferMessageItemRequest> itemList = channelStorageRequest.getItemList();
                        SgChannelStorageOmsBufferMessageItemRequest newItem = new SgChannelStorageOmsBufferMessageItemRequest();
                        newItem.setCpCStoreId(virtualSkuRequest.getCpCStoreId());
                        newItem.setPsCSkuId(virtualSkuid);
                        newItem.setQtyChange(qtyChange);
                        newItem.setCpCshopId(cpCShopId);
                        newItem.setBillNo(billNo);
                        itemList.add(newItem);
                    } else {
                        SgChannelStorageOmsBufferMessageRequest channelStorageRequest = new SgChannelStorageOmsBufferMessageRequest();
                        List<SgChannelStorageOmsBufferMessageItemRequest> itemList = Lists.newArrayList();
                        SgChannelStorageOmsBufferMessageItemRequest newItem = new SgChannelStorageOmsBufferMessageItemRequest();
                        newItem.setCpCStoreId(virtualSkuRequest.getCpCStoreId());
                        newItem.setPsCSkuId(virtualSkuid);
                        newItem.setQtyChange(qtyChange);
                        newItem.setCpCshopId(cpCShopId);
                        newItem.setBillNo(billNo);
                        itemList.add(newItem);
                        channelStorageRequest.setItemList(itemList);
                        channelStorageRequest.setChangeTime(changeDate);
                        channelStorageMap.put(cpCShopId, channelStorageRequest);
                    }
                }
            }
            if (MapUtils.isNotEmpty(channelStorageMap)) {
                List<SgChannelStorageOmsBufferMessageRequest> values = Lists.newArrayList(channelStorageMap.values());
                HashMap<String, List<SgChannelStorageOmsBufferMessageRequest>> map = Maps.newHashMap();
                map.put("param", values);
                String body = JSONObject.toJSONString(map);
                String msgKey = SgConstants.MSG_KEY_HEAD_STORAGE_TO_CHANNEL + System.currentTimeMillis();
                String tag = SgConstantsIF.MSG_TAG_GROUP_STORAGE_TO_CHANNEL;
                try {
                    r3MqSendHelper.sendMessage(mqConfig.getChannelSynchConfigName(), body, mqConfig.getChannelSynchTopic(), tag, msgKey);
                } catch (Exception e) {
                    log.error("组合商品同步渠道库存失败" + e.getMessage());
                }
            }
        }

        log.debug("组合商品库存计算新增:" + add + "修改:" + update + "失败:" + fail);

        if (add > 0) {
            calculator.put("addCount", add);
            calculator.put("addArr", addArr);
        }
        if (update > 0) {
            calculator.put("updateCount", update);
            calculator.put("updateArr", updateArr);
        }
        if (fail > 0) {
            calculator.put("failCount", fail);
            calculator.put("failArr", failArr);
        }

        data.setCalculator(calculator);
        data.setGroupSkuResults(groupSkuResults);
        vh.setData(data);

        if (log.isDebugEnabled()) {
            log.debug("Finish SgGroupStorageCalculateService.calculateGroupStorage:result:{};", vh.toJSONObject());
        }
        return vh;
    }

    /**
     * 福袋商品计算最低库存
     *
     * @param skus 虚拟条码下的实际条码
     * @param num  每组抽取数量
     */
    public BigDecimal getLuckyProMinStorage(List<SgGroupStorageSkuRequest> skus, Integer num) {
        if (log.isDebugEnabled()) {
            log.debug("福袋商品计算最低库存skus:" + JSONObject.toJSONString(skus, SerializerFeature.WriteMapNullValue) + ",num:" + num);
        }
        Map<Integer, List<SgGroupStorageSkuRequest>> map = skus.stream().collect(Collectors.groupingBy(sku -> sku.getGroupnum()));
        BigDecimal minStorage = BigDecimal.ZERO;
        if (MapUtils.isNotEmpty(map)) {
            int t = 0;
            Iterator<Integer> iterator = map.keySet().iterator();
            while (iterator.hasNext()) {
                List<SgGroupStorageSkuRequest> v = map.get(iterator.next());
                BigDecimal min = BigDecimal.ZERO;
                if (v.size() >= num) {
                    List<SgGroupStorageSkuRequest> sorted = v.stream()
                            .sorted(Comparator.comparing(SgGroupStorageSkuRequest::getSingleStorage).reversed())
                            .collect(Collectors.toList());
                    for (int i = 0; i < sorted.size(); i = i + 1) {
                        if ((i + 1) % num == 0) {
                            BigDecimal singleStorage = sorted.get(i).getSingleStorage();
                            min = min.add(singleStorage);
                        }
                    }
                }
                if (t == 0 || minStorage.compareTo(min) > 0) {
                    minStorage = min;
                    t++;
                }
            }
        }
        return minStorage;
    }

    /**
     * 插入组合商品库存
     *
     * @param min 最低库存
     * @param sku 虚拟条码
     */
    private void insertGroupStorage(SgGroupStorageVirtualSkuRequest sku, BigDecimal min, User user) {
        SgBGroupStorage groupStorage = new SgBGroupStorage();
        BeanUtils.copyProperties(sku, groupStorage);
        groupStorage.setId(ModelUtil.getSequence(SgConstants.SG_B_GROUP_STORAGE));
        groupStorage.setPsCSkuId(sku.getId());
        groupStorage.setPsCSkuEcode(sku.getEcode());
        groupStorage.setPsCProId(sku.getPsCProId());
        groupStorage.setQtyStorage(min);
        groupStorage.setCpCStoreId(sku.getCpCStoreId());
        StorageUtils.setBModelDefalutData(groupStorage, user);
        groupStorageMapper.insert(groupStorage);
    }

    /**
     * 更新组合商品库存
     *
     * @param groupStorage 组合商品库存实体
     * @param min          最低库存
     * @param user         登录用户
     */
    private void updateGroupStorage(SgBGroupStorage groupStorage, BigDecimal min, User user) {
        groupStorage.setQtyStorage(min);
        StorageUtils.setBModelDefalutDataByUpdate(groupStorage, user);
        groupStorageMapper.updateById(groupStorage);
    }
}
