package com.jackrain.nea.sg.basic.common;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/3/16 22:28
 */
public interface SgConstants {

    /**
     * 中心名
     */
    String CENTER_NAME_SG = "sg";

    /**
     * 表名
     */
    String SG_B_STORAGE = "sg_b_storage";   // 库存查询表
    String SG_B_TEUS_STORAGE = "sg_b_teus_storage";   // 箱库存查询表
    String SG_B_GROUP_STORAGE = "sg_b_group_storage";//组合商品库存表
    String SG_B_STORAGE_CHANGE_FTP = "sg_b_storage_change_ftp"; // 库存变动流水表
    String SG_B_STORAGE_PREIN_FTP = "sg_b_storage_prein_ftp";  // 库存在单变动流水表
    String SG_B_STORAGE_PREOUT_FTP = "sg_b_storage_preout_ftp";  // 库存在途变动流水表
    String SG_B_STORAGE_LOCK_LOG = "sg_b_storage_lock_log"; //库存消息消费流水表
    String SG_B_TEUS_STORAGE_CHANGE_FTP = "sg_b_teus_storage_change_ftp"; // 箱库存变动流水表
    String SG_B_TEUS_STORAGE_PREIN_FTP = "sg_b_teus_storage_prein_ftp";  // 箱库存在单变动流水表
    String SG_B_TEUS_STORAGE_PREOUT_FTP = "sg_b_teus_storage_preout_ftp";  // 箱库存在途变动流水表
    String SG_B_ADJUST = "sg_b_adjust";
    String SG_B_ADJUST_ITEM = "sg_b_adjust_item";
    String SG_B_ADJUST_PROP = "sg_b_adjust_prop";
    String SG_B_CHANNEL_DEF = "sg_b_channel_def";
    String SG_B_CHANNEL_PRODUCT = "sg_b_channel_product";
    String SG_B_CHANNEL_STORAGE = "sg_b_channel_storage";
    String SG_B_CHANNEL_STORAGE_BUFFER = "sg_b_channel_storage_buffer";
    String SG_B_CHANNEL_STORAGE_CHANGE_FTP = "sg_b_channel_storage_change_ftp";
    String SG_B_CHANNEL_STORE = "sg_b_channel_store";
    String SG_B_CHANNEL_SYNSTOCK_Q = "sg_b_channel_synstock_q";
    String SG_B_PHY_ADJUST = "sg_b_phy_adjust";
    String SG_B_PHY_ADJUST_ITEM = "sg_b_phy_adjust_item";
    String SG_B_PHY_IN_NOTICES = "sg_b_phy_in_notices";
    String SG_B_PHY_IN_NOTICES_ITEM = "sg_b_phy_in_notices_item";
    String SG_B_PHY_IN_RESULT = "sg_b_phy_in_result";
    String SG_B_PHY_IN_RESULT_ITEM = "sg_b_phy_in_result_item";
    String SG_B_PHY_IN_RESULT_DEFECT_ITEM = "sg_b_phy_in_result_defect_item";
    String SG_B_PHY_OUT_NOTICES = "sg_b_phy_out_notices";
    String SG_B_PHY_OUT_NOTICES_ITEM = "sg_b_phy_out_notices_item";
    String SG_B_PHY_OUT_NOTICES_TEUS_ITEM = "sg_b_phy_out_notices_teus_item";
    String SG_B_PHY_OUT_NOTICES_IMP_ITEM = "sg_b_phy_out_notices_imp_item";
    String SG_B_PHY_OUT_NOTICES_POS = "sg_b_phy_out_notices_pos";
    String SG_B_PHY_OUT_RESULT = "sg_b_phy_out_result";
    String SG_B_PHY_OUT_RESULT_ITEM = "sg_b_phy_out_result_item";
    String SG_B_PHY_OUT_RESULT_TEUS_ITEM = "sg_b_phy_out_result_teus_item";
    String SG_B_PHY_OUT_RESULT_IMP_ITEM = "sg_b_phy_out_result_imp_item";
    String SG_B_PHY_STORAGE = "sg_b_phy_storage";
    String SG_B_PHY_STORAGE_CHANGE_FTP = "sg_b_phy_storage_change_ftp";
    String SG_B_PHY_TEUS_STORAGE = "sg_b_phy_teus_storage";
    String SG_B_PHY_TEUS_STORAGE_CHANGE_FTP = "sg_b_phy_teus_storage_change_ftp";
    String SG_B_RECEIVE = "sg_b_receive";
    String SG_B_RECEIVE_ITEM = "sg_b_receive_item";
    String SG_B_SEND = "sg_b_send";
    String SG_B_SEND_ITEM = "sg_b_send_item";
    String SG_B_TRANSFER = "sc_b_transfer";
    String SG_B_TRANSFER_ITEM = "sc_b_transfer_item";
    String SG_B_OUT_DELIVERY = "sg_b_out_delivery";   // 多包裹明细表
    String SC_B_TRANSFER_DEFECT_ITEM = "sc_b_transfer_defect_item"; // 残次品明细
    String SC_B_TRANSFER_DIFF = "sc_b_transfer_diff";   // 调拨差异处理
    String SC_B_TRANSFER_DIFF_ITEM = "sc_b_transfer_diff_item"; // 调拨差异处理明细
    String SG_B_WMS_TO_PHY_OUT_RESULT = "sg_b_wms_to_phy_out_result"; //WMS回传出库结果单中间表
    String SG_B_CHANNEL_MANUAL_SYN = "sg_b_channel_manual_syn";
    String SG_B_CHANNEL_MANUAL_SYN_ITEM = "sg_b_channel_manual_syn_item";
    String SC_B_PROFIT = "sc_b_profit";
    String SC_B_PROFIT_IMP_ITEM = "sc_b_profit_imp_item";
    String SC_B_TRANSFER="sc_b_transfer";//调拨单 transfer

    String SG_B_ASSIGN = "sg_b_assign";//铺货单
    String SG_B_ASSIGN_IMP_ITEM = "sg_b_assign_imp_item";//铺货单明细

    //add chenxinxing
    String SG_B_CHK_DIFFE_APPLY = "sg_b_chk_diffe_apply";//盘差单
    String SG_B_CHK_DIFFE_APPLY_ITEM = "sg_b_chk_diffe_apply_item";//盘差单明细

    String SC_B_TRANSFER_IMP_ITEM = "sc_b_transfer_imp_item";   // 调拨录入明细
    String SC_B_TRANSFER_TEUS_ITEM = "sc_b_transfer_teus_item"; // 调拨箱内明细

    String SG_B_PHY_IN_NOTICES_IMP_ITEM = "sg_b_phy_in_notices_imp_item";   // 入库通知单录入明细
    String SG_B_PHY_IN_NOTICES_TEUS_ITEM = "sg_b_phy_in_notices_teus_item";   // 入库通知单箱内明细
    String SG_B_PHY_IN_RESULT_IMP_ITEM = "sg_b_phy_in_result_imp_item";  // 入库结果单录入明细
    String SG_B_PHY_IN_RESULT_TEUS_ITEM = "sg_b_phy_in_result_teus_item";  // 入库结果单箱内明细


    String SG_B_SEND_IMP_ITEM = "sg_b_send_imp_item"; //逻辑发货单录入明细
    String SG_B_SEND_TEUS_ITEM = "sg_b_send_teus_item"; //逻辑发货单箱内明细
    String SG_B_RECEIVE_IMP_ITEM = "sg_b_receive_imp_item";//逻辑收货单录入明细
    String SG_B_RECEIVE_TEUS_ITEM = "sg_b_receive_teus_item";//逻辑收货单箱内明细

    String SG_B_ADJUST_IMP_ITEM = "sg_b_adjust_imp_item";//逻辑调整单录入明细
    String SG_B_ADJUST_TEUS_ITEM = "sg_b_adjust_teus_item";//逻辑调整单箱内明细

    String SG_B_PHY_ADJUST_IMP_ITEM = "sg_b_phy_adjust_imp_item";//库存调整单录入明细
    String SG_B_PHY_ADJUST_TEUS_ITEM = "sg_b_phy_adjust_teus_item";//库存调整单箱内明细

    String SG_B_IN_PICKORDER = "sg_b_in_pickorder";//入库拣货单
    String SG_B_IN_PICKORDER_ITEM = "sg_b_in_pickorder_item";//入库拣货单拣货明细

    String SG_B_OUT_PICKORDER = "sg_b_out_pickorder";//出库拣货单
    String SG_B_OUT_PICKORDER_ITEM = "sg_b_out_pickorder_item";//出库拣货单拣货明细

    String MESSAGE_STATUS_INIT = "init";
    String MESSAGE_STATUS_SUCCESS = "success";

    String SG_B_IN_PICKORDER_NOTICE_ITEM = "sg_b_in_pickorder_notice_item";//入库拣货单-来源入库通知单
    String SG_B_IN_PICKORDER_RESULT_ITEM = "sg_b_in_pickorder_result_item";//入库拣货单-入库结果单明细
    String SG_B_IN_PICKORDER_TEUS_ITEM = "sg_b_in_pickorder_teus_item";//入库拣货单-装箱明细

    String SG_B_OUT_PICKORDER_NOTICE_ITEM = "sg_b_out_pickorder_notice_item";//出库拣货单-来源入库通知单
    String SG_B_OUT_PICKORDER_RESULT_ITEM = "sg_b_out_pickorder_result_item";//出库拣货单-入库结果单明细
    String SG_B_OUT_PICKORDER_TEUS_ITEM = "sg_b_out_pickorder_teus_item";//出库拣货单-装箱明细

    // 调拨入库单（入库拣货单视图）
    String SC_B_TRANSFER_IN = "SC_B_TRANSFER_IN";


    //redis 锁
    /**
     * MQ幂等性防重锁
     **/
    String REDIS_KEY_PREFIX_STO_MQ_MESSAGE = "storage_mq:";
    String REDIS_KEY_PREFIX_PHY_MQ_MESSAGE = "phy_storage_mq:";
    String REDIS_KEY_PREFIX_STORAGE = "sg:storage:";
    String REDIS_KEY_PREFIX_TEUS_STORAGE = "sg:teus_storage:";
    String REDIS_KEY_PREFIX_STORAGE_FTP = "sg:storage_ftp:";
    String REDIS_KEY_PREFIX_TEUS_STORAGE_FTP = "sg:teus_storage_ftp:";
    String REDIS_KEY_PREFIX_PHY_STORAGE = "sg:phystorage:";
    String REDIS_KEY_PREFIX_PHY_TEUS_STORAGE = "sg:teus_phystorage:";
    String REDIS_KEY_PREFIX_PHY_STORAGE_FTP = "sg:phystorage_ftp:";
    String REDIS_KEY_PREFIX_PHY_TEUS_STORAGE_FTP = "sg:teus_phystorage_ftp:";
    String REDIS_SUBKEY_PREOUT = "preout";
    String REDIS_SUBKEY_PREIN = "prein";
    String REDIS_SUBKEY_STORAGE = "storage";
    String REDIS_SUBKEY_AVAILABLE = "available";

    @Deprecated
    int STORAGE_MAX_QUERY_LIMIT = 500;

    /**
     * sg_b_storage XACT锁 区段
     */
    long XACT_LOCK_PREFIX_STORAGE = 10000000000000L;
    /**
     * sg_b_channel_storage XACT锁 区段
     */
    long XACT_LOCK_PREFIX_CHANNEL_STORAGE = 20000000000000L;
    /**
     * sg_b_phy_storage XACT锁 区段
     */
    long XACT_LOCK_PREFIX_PHY_STORAGE = 30000000000000L;

    /**
     * 库存中心事务控制等级
     */
    //消费者事务
    int TRANSACTION_LEVEL_CONSUMER = 1;
    //单据事务
    int TRANSACTION_LEVEL_BILL = 2;
    //明细事务
    int TRANSACTION_LEVEL_ITEM = 3;

    /**
     * 消息队列消息KEY前缀
     */
    String MSG_KEY_HEAD_STORAGE_TO_CHANNEL = "sto_to_chan:";
    String MSG_KEY_HEAD_STORAGE_TO_GROUP = "sto_to_group:";
    String MSG_KEY_HEAD_STORAGE_TO_RETAIL = "sto_to_retail:";

    /**
     * 消息队列消息TAG前缀
     */
    String MSG_TAG_HEAD_STORAGE = "storage_";
    String MSG_TAG_HEAD_PHY_STORAGE = "phy_storage_";
    String MSG_TAG_HEAD_TEUS_STORAGE = "teus_storage_";
    String MSG_TAG_HEAD_PHY_TEUS_STORAGE = "phy_teus_storage_";
    String Msg_TAG_GROUP_STORAGE = "group_storage";

    String MSG_TAG_ADJUST = "adjust";
    String MSG_TAG_PHY_ADJUST = "phy_adjust";
    String MSG_TAG_SEND = "send";
    String MSG_TAG_RECEIVE = "receive";
    String MSG_TAG_PHY_IN_RESULT = "phy_in_result";
    String MSG_TAG_PHY_OUT_RESULT = "phy_out_result";

    String BILL_PREFIX_ADJUST = "LA";
    String BILL_PREFIX_PHY_ADJUST = "PA";
    String BILL_PREFIX_SEND = "SN";
    String BILL_PREFIX_RECEIVE = "RN";
    String BILL_PREFIX_PHY_IN_RESULT = "IR";
    String BILL_PREFIX_PHY_OUT_RESULT = "OR";

    /**
     * 可用
     */
    String IS_ACTIVE_Y = "Y";
    String IS_ACTIVE_N = "N";
    Integer IS_YES = 1;
    Integer IS_NO = 0;

    //是否为最后一次出库
    Integer IS_LAST_YES = 0;
    Integer IS_LAST_NO = 1;

    String DEFAULT_INFO_ZERO = "0";

    int SG_COMMON_INSERT_PAGE_SIZE = 500;
    int SG_COMMON_MAX_INSERT_PAGE_SIZE = 200;
    int SG_COMMON_ITEM_INSERT_PAGE_SIZE = 300;
    int SG_COMMON_MAX_QUERY_PAGE_SIZE = 100;
    int SG_COMMON_STRING_SIZE = 500;
    int SG_COMMON_REMARK_SIZE = 1000;
    int SG_NORMAL_MAX_QUERY_PAGE_SIZE = 1000;

    //库存调整单
    Long PRICE_TYPE_FACTORY = 1L;
    Long PRICE_TYPE_AGENT = 2L;
}
