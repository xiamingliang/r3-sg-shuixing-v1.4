package com.jackrain.nea.sg.basic.logic;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.redis.config.CusRedisTemplate;
import com.jackrain.nea.redis.util.RedisOpsUtil;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.config.SgStorageControlConfig;
import com.jackrain.nea.sg.basic.model.request.SgRedisStorageQueryModel;
import com.jackrain.nea.sg.basic.model.result.SgRedisStorageQueryResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/9/14 14:50
 */
@Component
@Slf4j
public class SgRedisStorageLogic {

    @Autowired
    SgStorageControlConfig sgStorageControlConfig;

    /**
     * Redis逻辑仓库存查询
     *
     * @param sgRedisStorageQueryList
     * @param loginUser
     * @return ValueHolderV14
     */
    public ValueHolderV14<HashMap<String, SgRedisStorageQueryResult>> queryStorage(
            List<SgRedisStorageQueryModel> sgRedisStorageQueryList,
            User loginUser) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgRedisStorageLogic.queryStorage. ReceiveParams:sgRedisStorageQueryList={} loginUser={};",
                    JSONObject.toJSONString(sgRedisStorageQueryList),
                    JSONObject.toJSONString(loginUser));
        }

        ValueHolderV14 holder = new ValueHolderV14(ResultCode.SUCCESS, "");
        List<String> batchUpdateRedisKeyList = new ArrayList<>();
        Map<String, SgRedisStorageQueryResult> resultMap = new HashMap<>();

        try {

            CusRedisTemplate<String, String> redisTemplate = RedisOpsUtil.getStrRedisTemplate();
            DefaultRedisScript<List> redisScript = new DefaultRedisScript<>();
            redisScript.setLocation(new ClassPathResource("lua/QueryRedisStorage.lua"));
            redisScript.setResultType(List.class);


            for (SgRedisStorageQueryModel queryModel : sgRedisStorageQueryList) {

                //LUA Redis键：sg:storage:逻辑仓ID:SKUID
                batchUpdateRedisKeyList.add(SgConstants.REDIS_KEY_PREFIX_STORAGE + queryModel.getCpCStoreId() +
                        ":" + queryModel.getPsCSkuId());
            }

            //批量更新单据明细分页
            int pageSize = sgStorageControlConfig.getMaxQueryLimit();
            int listSize = batchUpdateRedisKeyList.size();
            int page = listSize / pageSize;

            if (listSize % pageSize != 0) {
                page++;
            }

            //分页批量更新
            for (int i = 0; i < page; i++) {

                List<String> keyList = batchUpdateRedisKeyList.subList(i * pageSize,
                        (((i + 1) * pageSize > listSize ? listSize : pageSize * (i + 1))));

                if (log.isDebugEnabled()) {
                    log.debug("SgRedisStorageLogic.queryStorage. redisTemplate.execute.start. ReceiveParams:keyList:{};"
                            , JSONObject.toJSONString(keyList));
                }

                List result = redisTemplate.execute(redisScript, keyList);

                if (log.isDebugEnabled()) {
                    log.debug("SgRedisStorageLogic.queryStorage. redisTemplate.execute.end. ReturnResult:result:{};"
                            , JSONObject.toJSONString(result));
                }

                if (!CollectionUtils.isEmpty(result) && Integer.valueOf((String) result.get(0)) == ResultCode.SUCCESS) {

                    List<List> storageResultList = (ArrayList) result.get(1);

                    if (!CollectionUtils.isEmpty(storageResultList)) {
                        for (List storageResult : storageResultList) {

                            if (!CollectionUtils.isEmpty(storageResult)) {
                                SgRedisStorageQueryResult queryResult = new SgRedisStorageQueryResult();

                                queryResult.setQtyPreout(new BigDecimal((String) storageResult.get(0)));
                                queryResult.setQtyPrein(new BigDecimal((String) storageResult.get(1)));
                                queryResult.setQtyStorage(new BigDecimal((String) storageResult.get(2)));
                                queryResult.setQtyAvailable(new BigDecimal((String) storageResult.get(3)));

                                String[] key = ((String) storageResult.get(4)).split(":");
                                queryResult.setCpCStoreId(Long.valueOf(key[2]));
                                queryResult.setPsCSkuId(Long.valueOf(key[3]));

                                resultMap.put(queryResult.getCpCStoreId() + ":" + queryResult.getPsCSkuId(),
                                        queryResult);

                            }

                        }

                    }

                }
            }

            holder.setData(resultMap);
            holder.setCode(ResultCode.SUCCESS);
            holder.setMessage(Resources.getMessage("查询店仓库存成功！", loginUser.getLocale()));

        } catch (Exception ex) {
            log.error("SgRedisStorageLogic.queryStorage Error", ex);
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("查询店仓库存信息发生异常！", loginUser.getLocale()));
        }

        return holder;
    }

}
