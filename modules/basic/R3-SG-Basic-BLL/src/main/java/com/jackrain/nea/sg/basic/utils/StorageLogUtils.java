package com.jackrain.nea.sg.basic.utils;

import com.jackrain.nea.config.Resources;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/3/30 9:13
 */
@Slf4j
public class StorageLogUtils {

    public static String getErrMessage(Exception e, String messageKey, User loginUser) {

        StackTraceElement element = e.getStackTrace()[0];
        String returnMsg;

        String errorMsg = Resources.getMessage(
                "店仓库存变动消息处理失败！",
                loginUser == null ? null : loginUser.getLocale(), messageKey, e.getMessage(), element.getFileName(), element.getClassName(),
                element.getMethodName(), element.getLineNumber());

        returnMsg = Resources.getMessage(
                "店仓库存变动消息处理失败！_简短",
                loginUser == null ? null : loginUser.getLocale(), messageKey, e.getMessage());

        log.error("失败处理流水号：{} 错误信息:{} 详情：{}",
                messageKey, e.getMessage(), errorMsg);

        return returnMsg;
    }

    public static String getChannelErrMessage(Exception e, String extendMessage) {

        StackTraceElement element = e.getStackTrace()[0];

        String errorMsg = Resources.getMessage(
                "该全渠道库存处理失败！",
                extendMessage, e.getMessage(), element.getFileName(), element.getClassName(),
                element.getMethodName(), element.getLineNumber());

        log.error("关键字:{}失败处理 错误信息:{} 详情：{}",
                extendMessage, e.getMessage(), errorMsg);

        return errorMsg;
    }

    public static String getGroupErrMessage(Exception e, String messageKey, User loginUser) {

        StackTraceElement element = e.getStackTrace()[0];
        String returnMsg;

        String errorMsg = Resources.getMessage(
                "该组合商品库存处理失败！",
                loginUser == null ? null : loginUser.getLocale(), messageKey, e.getMessage(), element.getFileName(), element.getClassName(),
                element.getMethodName(), element.getLineNumber());

        returnMsg = Resources.getMessage(
                "该组合商品库存处理失败！_简短",
                loginUser == null ? null : loginUser.getLocale(), messageKey, e.getMessage());

        log.error("失败处理流水号：{} 错误信息:{} 详情：{}",
                messageKey, e.getMessage(), errorMsg);


        return returnMsg;
    }
}
