package com.jackrain.nea.sg.basic.services;

import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.model.request.SgPhyStorageSingleUpdateRequest;
import com.jackrain.nea.sg.basic.model.result.SgPhyStorageBatchUpdateResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/6/1 12:51
 */
@Component
@Slf4j
public class SgPhyStorageBillTransUpdateService {

    @Autowired
    private SgPhyStorageBillUpdateService service;

    public ValueHolderV14<SgPhyStorageBatchUpdateResult> updatePhyStorageBill(SgPhyStorageSingleUpdateRequest request) {

        ValueHolderV14<SgPhyStorageBatchUpdateResult> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        SgPhyStorageBatchUpdateResult updateResult = new SgPhyStorageBatchUpdateResult();

        try {
            holder = service.updatePhyStorageBillWithTrans(request);
        } catch (Exception e) {

            log.error("SgPhyStorageBillTransUpdateService.updatePhyStorageBill. error ", e);

            holder.setCode(ResultCode.FAIL);
            holder.setData(updateResult);
            holder.setMessage(e.getMessage().concat(" " + holder.getMessage()));

        }

        return holder;

    }

}
