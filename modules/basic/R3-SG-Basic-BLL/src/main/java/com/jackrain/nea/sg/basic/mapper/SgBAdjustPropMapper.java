package com.jackrain.nea.sg.basic.mapper;

import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.basic.model.table.SgBAdjustProp;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SgBAdjustPropMapper extends ExtentionMapper<SgBAdjustProp> {
}