package com.jackrain.nea.sg.basic.model;

import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import lombok.Data;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/3/24 13:27
 */
@Data
public class SgStorageUpdateControlModel {

    /** 占用是否允许负库存 */
    private boolean isNegativePreout;
    /** 占用错误处理类别(1.报警（允许负库存）2.报错  3.报缺货) */
    private int preoutOperateType;
    /** 在途是否允许负库存 */
    private boolean isNegativePrein;
    /** 在库是否允许负库存 */
    private boolean isNegativeStorage;
    /** 可用是否允许负库存 */
    private boolean isNegativeAvailable;
    /** 是否批量更新单据明细 */
    private boolean isBatchUpdateItem;
    /** 单次批量更新单据明细数量 */
    private int batchUpdateItems;
    /** 是否同步R3基础数据 */
    private boolean isSyncR3BasicData;

    public SgStorageUpdateControlModel(){
        this.isNegativePrein = false;
        this.isNegativePreout = false;
        this.isNegativeStorage = false;
        this.isNegativeAvailable = false;
        this.isBatchUpdateItem = true;
        this.batchUpdateItems = 20;
        this.preoutOperateType = SgConstantsIF.PREOUT_OPT_TYPE_ERROR;
        this.isSyncR3BasicData = false;
    }

}
