package com.jackrain.nea.sg.basic.mapper;


import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.basic.model.table.CpCPhyWarehouse;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface CpCPhyWarehouseMapper extends ExtentionMapper<CpCPhyWarehouse> {


    /**
     * 根据逻辑仓id查询实体仓信息
     *
     * @param storeId 逻辑仓id
     * @return 实体仓信息
     */
    // 水星-zhanghang-2019-12-25 start
    @Select("SELECT j.* , p.STORENATURE  FROM cp_c_store P " +
            " LEFT JOIN cp_c_phy_warehouse j ON P.cp_c_phy_warehouse_id = j.ID " +
            " WHERE P.ID = #{storeId} " +
            " AND P.isactive = 'Y' ")
    // 水星-zhanghang-2019-12-25 start
    CpCPhyWarehouse selectByStoreId(@Param("storeId") Long storeId);
}