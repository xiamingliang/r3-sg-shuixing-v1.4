package com.jackrain.nea.sg.basic.model;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/3/17 17:54
 */
@Data
public class SgStorageItemLockKey implements Serializable {

    //MQ幂等性防重锁
    private String messageKey;
    //库存变动类型
    private Integer storageType;
    //单据类型
    private Integer billType;
    //单据编号
    private String billNo;
    //单据日期
    private String billDate;
    //单据明细ID
    private Long billItemId;
    //变动日期
    private String changeDate;
    //店仓id
    private Long cpCStoreId;
    //实体仓id
    private Long cpCPhyWarehouseId;
    //条码id
    private Long psCSkuId;
    //箱id
    private Long psCTeusId;
    //变动数量
    private BigDecimal qtyChange;
    //占用变动数量
    private BigDecimal qtyPreoutChange;
    //在途变动数量
    private BigDecimal qtyPreinChange;
    //在库变动数量
    private BigDecimal qtyStorageChange;
    //是否库存取消操作
    private Boolean isCancel;

}
