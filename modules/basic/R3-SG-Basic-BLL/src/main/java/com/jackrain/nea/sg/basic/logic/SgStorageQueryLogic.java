package com.jackrain.nea.sg.basic.logic;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.cp.result.CpCStore;
import com.jackrain.nea.data.basic.model.request.StoreInfoQueryRequest;
import com.jackrain.nea.data.basic.services.BasicCpQueryService;
import com.jackrain.nea.sg.basic.config.SgStorageControlConfig;
import com.jackrain.nea.sg.basic.mapper.SgBStorageMapper;
import com.jackrain.nea.sg.basic.model.request.SgStoragePageRequest;
import com.jackrain.nea.sg.basic.model.request.SgStorageQueryRequest;
import com.jackrain.nea.sg.basic.model.request.SgSumStorageQueryRequest;
import com.jackrain.nea.sg.basic.model.result.SgSumStorageQueryResult;
import com.jackrain.nea.sg.basic.model.table.SgBStorage;
import com.jackrain.nea.sg.basic.model.table.SgBStorageInclPhy;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


@Component
@Slf4j
public class SgStorageQueryLogic {

    @Autowired
    SgStorageControlConfig sgStorageControlConfig;

    @Autowired
    private SgBStorageMapper sgBStorageMapper;

    @Autowired
    private BasicCpQueryService basicCpQueryService;

    /**
     * 逻辑仓库存查询
     *
     * @param sgStorageQueryRequest
     * @param sgStoragePageRequest
     * @param loginUser
     * @return ValueHolderV14
     */
    public ValueHolderV14 queryStorage(SgStorageQueryRequest sgStorageQueryRequest,
                                       SgStoragePageRequest sgStoragePageRequest,
                                       User loginUser,
                                       boolean excludeZero) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgStorageQueryLogic.queryStorage. ReceiveParams:sgStorageQueryRequest={} " +
                            "sgStoragePageRequest={} loginUser={};",
                    JSONObject.toJSONString(sgStorageQueryRequest), JSONObject.toJSONString(sgStoragePageRequest),
                    JSONObject.toJSONString(loginUser));
        }

        ValueHolderV14 holder = new ValueHolderV14(ResultCode.SUCCESS, "");

        try {

            holder = checkServiceParam(sgStorageQueryRequest, loginUser);

            if (ResultCode.FAIL == holder.getCode()) {
                return holder;
            }

            List<Long> storeIds = sgStorageQueryRequest.getStoreIds();
            List<Long> phyWarehouseIds = sgStorageQueryRequest.getPhyWarehouseIds();
            List<String> proEcodes = sgStorageQueryRequest.getProEcodes();
            List<String> skuEcodes = sgStorageQueryRequest.getSkuEcodes();

            // 获取实体仓下的虚拟仓ID
            if (!CollectionUtils.isEmpty(phyWarehouseIds)) {

                storeIds = getStoreIdsByPhyIds(phyWarehouseIds);

                // 检查实体仓下是否存在店仓，防止全量查询
                if (CollectionUtils.isEmpty(storeIds)) {
                    holder.setCode(ResultCode.FAIL);
                    holder.setMessage(Resources.getMessage("实体仓获取店仓信息失败！", loginUser.getLocale()));
                    return holder;

                }

            }

            if (sgStoragePageRequest == null) {

                /** 全量查询 **/
                List<SgBStorage> resultList = sgBStorageMapper.selectList(
                        new QueryWrapper<SgBStorage>().lambda()
                                .in(!CollectionUtils.isEmpty(storeIds), SgBStorage::getCpCStoreId, storeIds)
                                .in(!CollectionUtils.isEmpty(proEcodes), SgBStorage::getPsCProEcode, proEcodes)
                                .in(!CollectionUtils.isEmpty(skuEcodes), SgBStorage::getPsCSkuEcode, skuEcodes)
                                .ne(excludeZero, SgBStorage::getQtyStorage, BigDecimal.ZERO)
                );

                holder.setData(resultList);

            } else {

                /** 分页查询 **/
                PageHelper.startPage(sgStoragePageRequest.getPageNum(), sgStoragePageRequest.getPageSize());
                List<SgBStorage> resultList = sgBStorageMapper.selectList(
                        new QueryWrapper<SgBStorage>().lambda()
                                .in(!CollectionUtils.isEmpty(storeIds), SgBStorage::getCpCStoreId, storeIds)
                                .in(!CollectionUtils.isEmpty(proEcodes), SgBStorage::getPsCProEcode, proEcodes)
                                .in(!CollectionUtils.isEmpty(skuEcodes), SgBStorage::getPsCSkuEcode, skuEcodes)
                                .ne(excludeZero, SgBStorage::getQtyStorage, BigDecimal.ZERO)
                );
                PageInfo<SgBStorage> resultPage = new PageInfo<>(resultList);

                holder.setData(resultPage);
            }

            holder.setCode(ResultCode.SUCCESS);
            holder.setMessage(Resources.getMessage("查询店仓库存成功！", loginUser.getLocale()));

        } catch (Exception ex) {
            log.error("SgStorageQueryLogic.queryStorage Error", ex);
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("查询店仓库存信息发生异常！", loginUser.getLocale()));
        }

        return holder;
    }

    /**
     * 逻辑库存查询（包含实体仓信息）
     *
     * @param sgStorageQueryRequest
     * @param sgStoragePageRequest
     * @param loginUser
     * @return ValueHolderV14
     */
    public ValueHolderV14 queryStorageInclPhy(SgStorageQueryRequest sgStorageQueryRequest,
                                              SgStoragePageRequest sgStoragePageRequest,
                                              User loginUser) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgStorageQueryLogic.queryStorageInclPhy. ReceiveParams:sgStorageQueryRequest={} " +
                            "sgStoragePageRequest={} loginUser={};",
                    JSONObject.toJSONString(sgStorageQueryRequest), JSONObject.toJSONString(sgStoragePageRequest),
                    JSONObject.toJSONString(loginUser));
        }

        ValueHolderV14 holder = new ValueHolderV14(ResultCode.SUCCESS, "");

        try {

            holder = checkServiceParam(sgStorageQueryRequest, loginUser);

            if (ResultCode.FAIL == holder.getCode()) {
                return holder;
            }

            List<Long> storeIds = sgStorageQueryRequest.getStoreIds();
            List<Long> phyWarehouseIds = sgStorageQueryRequest.getPhyWarehouseIds();
            List<String> proEcodes = sgStorageQueryRequest.getProEcodes();
            List<String> skuEcodes = sgStorageQueryRequest.getSkuEcodes();

            // 获取实体仓下的虚拟仓ID
            if (!CollectionUtils.isEmpty(phyWarehouseIds)) {

                storeIds = getStoreIdsByPhyIds(phyWarehouseIds);

                // 检查实体仓下是否存在店仓，防止全量查询
                if (CollectionUtils.isEmpty(storeIds)) {
                    holder.setCode(ResultCode.FAIL);
                    holder.setMessage(Resources.getMessage("实体仓获取店仓信息失败！", loginUser.getLocale()));
                    return holder;

                }

            }

            if (sgStoragePageRequest == null) {

                /** 全量查询 **/
                List<SgBStorageInclPhy> resultList = sgBStorageMapper.selectStrorageInclPhy(storeIds, proEcodes, skuEcodes);

                holder.setData(resultList);

            } else {

                /** 分页查询 **/
                PageHelper.startPage(sgStoragePageRequest.getPageNum(), sgStoragePageRequest.getPageSize());
                List<SgBStorageInclPhy> resultList = sgBStorageMapper.selectStrorageInclPhy(storeIds, proEcodes, skuEcodes);
                PageInfo<SgBStorageInclPhy> resultPage = new PageInfo<>(resultList);

                holder.setData(resultPage);
            }

            holder.setCode(ResultCode.SUCCESS);
            holder.setMessage(Resources.getMessage("查询店仓库存成功！", loginUser.getLocale()));

        } catch (Exception ex) {
            log.error("SgStorageQueryLogic.queryStorage Error", ex);
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("查询店仓库存信息发生异常！", loginUser.getLocale()));
        }

        return holder;
    }

    /**
     * 实体仓库存查询接口(通过逻辑仓ID)
     *
     * @param sgSumStorageQueryRequest
     * @param sgStoragePageRequest
     * @param loginUser
     * @return ValueHolderV14<List<SgSumStorageQueryResult>>
     */
    public ValueHolderV14<List<SgSumStorageQueryResult>> querySumStorageGrpPhy(SgSumStorageQueryRequest sgSumStorageQueryRequest,
                                                                               SgStoragePageRequest sgStoragePageRequest,
                                                                               User loginUser) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgStorageQueryLogic.querySumStorageGrpPhy. ReceiveParams:SgSumStorageQueryRequest={} " +
                            "SgStoragePageRequest={} loginUser={};",
                    JSONObject.toJSONString(sgSumStorageQueryRequest), JSONObject.toJSONString(sgStoragePageRequest),
                    JSONObject.toJSONString(loginUser));
        }

        ValueHolderV14 holder = new ValueHolderV14(ResultCode.SUCCESS, "");

        try {

            holder = checkServiceParam(sgSumStorageQueryRequest, loginUser);

            if (ResultCode.FAIL == holder.getCode()) {
                return holder;
            }

            List<Long> storeIds = sgSumStorageQueryRequest.getStoreIds();
            List<String> proEcodes = sgSumStorageQueryRequest.getProEcodes();
            List<String> skuEcodes = sgSumStorageQueryRequest.getSkuEcodes();

            if (sgStoragePageRequest == null) {

                /** 全量查询 **/
                List<SgSumStorageQueryResult> resultList = sgBStorageMapper.querySumStorageGrpPhy(storeIds, proEcodes, skuEcodes);

                holder.setData(resultList);

            } else {

                /** 分页查询 **/
                PageHelper.startPage(sgStoragePageRequest.getPageNum(), sgStoragePageRequest.getPageSize());
                List<SgSumStorageQueryResult> resultList = sgBStorageMapper.querySumStorageGrpPhy(storeIds, proEcodes, skuEcodes);
                PageInfo<SgSumStorageQueryResult> resultPage = new PageInfo<>(resultList);

                holder.setData(resultPage);
            }

            holder.setCode(ResultCode.SUCCESS);
            holder.setMessage(Resources.getMessage("查询实体仓库存成功！", loginUser.getLocale()));

        } catch (Exception ex) {
            log.error("SgStorageQueryLogic.querySumStorageGrpPhy Error", ex);
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("查询实体仓库存信息发生异常！", loginUser.getLocale()));
        }

        return holder;
    }

    /**
     * @param sgStorageQueryRequest
     * @param loginUser
     * @return ValueHolderV14
     */
    private ValueHolderV14 checkServiceParam(SgStorageQueryRequest sgStorageQueryRequest,
                                             User loginUser) {

        ValueHolderV14 holder = new ValueHolderV14(ResultCode.SUCCESS, "");

        int maxQueryLimit = sgStorageControlConfig.getMaxQueryLimit();

        List<Long> storeIds = sgStorageQueryRequest.getStoreIds();
        List<Long> phyWarehouseIds = sgStorageQueryRequest.getPhyWarehouseIds();
        List<String> proEcodes = sgStorageQueryRequest.getProEcodes();
        List<String> skuEcodes = sgStorageQueryRequest.getSkuEcodes();

        if (CollectionUtils.isEmpty(storeIds) && CollectionUtils.isEmpty(phyWarehouseIds)) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("店仓或实体仓不能同时为空！", loginUser.getLocale()));
        } else if (!CollectionUtils.isEmpty(storeIds) && !CollectionUtils.isEmpty(phyWarehouseIds)) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("店仓和实体仓不能同时查询！", loginUser.getLocale()));
        } else if (!CollectionUtils.isEmpty(proEcodes) &&
                proEcodes.size() > maxQueryLimit) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("商品编码的查询件数超过最大限制！",
                    loginUser.getLocale()));
        } else if (!CollectionUtils.isEmpty(skuEcodes) &&
                skuEcodes.size() > maxQueryLimit) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("商品条码的查询件数超过最大限制！",
                    loginUser.getLocale()));

        }

        return holder;
    }

    /**
     * @param sgStorageQueryRequest
     * @param loginUser
     * @return ValueHolderV14
     */
    private ValueHolderV14 checkServiceParam(SgSumStorageQueryRequest sgStorageQueryRequest,
                                             User loginUser) {

        ValueHolderV14 holder = new ValueHolderV14(ResultCode.SUCCESS, "");

        int maxQueryLimit = sgStorageControlConfig.getMaxQueryLimit();

        List<Long> storeIds = sgStorageQueryRequest.getStoreIds();
        List<String> proEcodes = sgStorageQueryRequest.getProEcodes();
        List<String> skuEcodes = sgStorageQueryRequest.getSkuEcodes();

        if (CollectionUtils.isEmpty(storeIds)) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("店仓不能为空！", loginUser.getLocale()));
        } else if (!CollectionUtils.isEmpty(proEcodes) &&
                proEcodes.size() > maxQueryLimit) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("商品编码的查询件数超过最大限制！",
                    loginUser.getLocale()));
        } else if (!CollectionUtils.isEmpty(skuEcodes) &&
                skuEcodes.size() > maxQueryLimit) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("商品条码的查询件数超过最大限制！",
                    loginUser.getLocale()));

        }

        return holder;
    }

    /**
     * @param phyWarehouseIds
     * @return List<Long>
     */
    private List<Long> getStoreIdsByPhyIds(List<Long> phyWarehouseIds) throws Exception {

        List<Long> storeIds = null;

        // 获取实体仓下的虚拟仓ID
        if (!CollectionUtils.isEmpty(phyWarehouseIds)) {

            StoreInfoQueryRequest storeInfoQueryRequest = null;
            storeIds = new ArrayList<>();

            for (Long phyId : phyWarehouseIds) {

                storeInfoQueryRequest = new StoreInfoQueryRequest();
                storeInfoQueryRequest.setPhyId(phyId);

                HashMap<Long, List<CpCStore>> storeMap = basicCpQueryService.getStoreInfoByPhyId(storeInfoQueryRequest);

                if (storeMap != null && !CollectionUtils.isEmpty(storeMap.get(phyId))) {

                    for (CpCStore cpCStore : storeMap.get(phyId)) {
                        storeIds.add(cpCStore.getId());
                    }
                }
            }

        }

        return storeIds;
    }


}
