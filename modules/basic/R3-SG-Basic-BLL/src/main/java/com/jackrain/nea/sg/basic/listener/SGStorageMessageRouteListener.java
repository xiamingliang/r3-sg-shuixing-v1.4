package com.jackrain.nea.sg.basic.listener;

import com.alibaba.fastjson.JSONObject;
import com.aliyun.openservices.ons.api.Action;
import com.aliyun.openservices.ons.api.ConsumeContext;
import com.aliyun.openservices.ons.api.Message;
import com.aliyun.openservices.ons.api.MessageListener;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.consumer.SgBPhyStorageBatchUpdateConsumer;
import com.jackrain.nea.sg.basic.consumer.SgBStorageBatchUpdateConsumer;
import com.jackrain.nea.sg.basic.consumer.SgGroupStorageCalculateConsumer;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.thymeleaf.util.StringUtils;

/**
 * @Description:通用MQ库存批量更新监听器
 * @Author: chenb
 * @Date: 2019/5/15 11:02
 */
@Slf4j
@Component
public class SGStorageMessageRouteListener implements MessageListener {

    @Autowired
    private SgBStorageBatchUpdateConsumer sgBStorageBatchUpdateConsumer;

    @Autowired
    private SgBPhyStorageBatchUpdateConsumer sgBPhyStorageBatchUpdateConsumer;

    @Autowired
    private SgGroupStorageCalculateConsumer sgGroupStorageCalculateConsumer;

    @Override
    public Action consume(Message message, ConsumeContext consumeContext) {

        Action consumeResult = Action.ReconsumeLater;

        if (log.isDebugEnabled()) {
            log.debug("Start SGStorageMessageRouteListener.consume. ReceiveParams:message={};",
                    JSONObject.toJSONString(message));
        }

        ValueHolderV14 holder = checkServiceParam(message);

        if (ResultCode.FAIL == holder.getCode()) {
            log.error("SGStorageMessageRouteListener.consume. ".concat(holder.getMessage()));
            return Action.CommitMessage;
        }

        String messageTag = message.getTag();

        //根据TAG调用相应的监听器
        if (!StringUtils.isEmpty(messageTag) &&
                messageTag.startsWith(SgConstants.MSG_TAG_HEAD_STORAGE)) {
            consumeResult = sgBStorageBatchUpdateConsumer.consume(message);
        } else if (!StringUtils.isEmpty(messageTag) &&
                messageTag.startsWith(SgConstants.MSG_TAG_HEAD_PHY_STORAGE)) {
            consumeResult = sgBPhyStorageBatchUpdateConsumer.consume(message);
        } else if (!StringUtils.isEmpty(messageTag) &&
                messageTag.startsWith(SgConstants.Msg_TAG_GROUP_STORAGE)) {
            consumeResult = sgGroupStorageCalculateConsumer.consume(message);
        }

        if (log.isDebugEnabled()) {
            log.debug("Finish SGStorageMessageRouteListener.consume. consumeResult={};",
                    JSONObject.toJSONString(consumeResult));
        }

        return consumeResult;
    }

    /**
     * @param message
     * @return Boolean
     */
    private ValueHolderV14 checkServiceParam(Message message) {

        ValueHolderV14 holder = new ValueHolderV14(ResultCode.SUCCESS, "");

        if (message == null) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("message is empty！"));
        }

        return holder;

    }

}
