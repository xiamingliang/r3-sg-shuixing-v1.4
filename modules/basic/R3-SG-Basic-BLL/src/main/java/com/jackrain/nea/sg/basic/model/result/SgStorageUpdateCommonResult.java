package com.jackrain.nea.sg.basic.model.result;

import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.model.SgStorageUpdateCommonModel;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/3/13 15:53
 */
@Data
public class SgStorageUpdateCommonResult implements Serializable {

    //最近数据库操作结果
    private int countResult;
    //占用结果
    private int preoutUpdateResult;
    //缺货明细详情
    private List<SgStorageUpdateCommonModel> outStockItemList;
    /*
     *渠道库存同步MQ列表
     */
    private List<SgStorageUpdateCommonModel> channelMqItemList;

    public SgStorageUpdateCommonResult() {
        this.countResult = 0;
        this.preoutUpdateResult = SgConstantsIF.PREOUT_RESULT_SUCCESS;
    }

}
