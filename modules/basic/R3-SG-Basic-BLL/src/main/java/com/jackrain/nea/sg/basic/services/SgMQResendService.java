package com.jackrain.nea.sg.basic.services;

import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.r3.mq.util.R3MqSendHelper;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 库存消息重发
 *
 * @author: 舒威
 * @since: 2019/8/19
 * create at : 2019/8/19 16:53
 */
@Slf4j
@Component
public class SgMQResendService {

    @Autowired
    private R3MqSendHelper r3MqSendHelper;

    public ValueHolderV14 sendMQ(String body, String topic, String tag, String msgKey) {
        ValueHolderV14 vh = new ValueHolderV14(ResultCode.SUCCESS, "success");
        try {
            String message = r3MqSendHelper.sendMessage("default", body, topic, tag, msgKey);
            vh.setMessage("msgKey:" + msgKey + "重新发送成功!" + message);
        } catch (Exception e) {
            log.error("msgKey:" + msgKey + "重新发送失败!" + e.getMessage());
            vh.setCode(ResultCode.FAIL);
            vh.setMessage("msgKey:" + msgKey + "重新发送失败!" + e.getMessage());
        }
        return vh;
    }
}
