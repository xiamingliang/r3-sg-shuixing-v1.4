package com.jackrain.nea.sg.basic.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.cp.api.CpStoreSyncTaskCmd;
import com.jackrain.nea.sg.basic.mapper.CpStoreMapper;
import com.jackrain.nea.sg.basic.model.request.CpStoreSyncRequest;
import com.jackrain.nea.sg.basic.model.table.CpCStore;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author zhu lin yu
 * @since 2019/5/31
 * create at : 2019/5/31 15:07
 */
@Slf4j
@Component
public class CpStoreSyncService {

    @Reference(version = "1.0", group = "cp")
    private CpStoreSyncTaskCmd cpStoreSyncTaskCmd;

    public ValueHolderV14 cpStoreSyncInfo() {

        ValueHolderV14<CpStoreSyncRequest> holderV14 = this.cpStoreSyncTaskCmd.cpStoreInfoQuery();
        CpStoreSyncRequest cpStoreSyncRequest = holderV14.getData();

        ValueHolderV14 holder;
        //入参日志记录
        if (log.isDebugEnabled()) {
            log.debug("Start CpStoreSyncService.cpStoreSyncInfo. ReceiveParams:cpStoreSyncRequest="
                    + JSONObject.toJSONString(cpStoreSyncRequest) + ";");
        }

        //检查入参
        holder = checkServiceParam(cpStoreSyncRequest);
        if (ResultCode.FAIL == holder.getCode()) {
            return holder;
        }

        CpStoreMapper cpStoreMapper = ApplicationContextHandle.getBean(CpStoreMapper.class);
        List<CpCStore> cpCStoreList = cpStoreSyncRequest.getCpCStoreList();

        cpCStoreList.forEach(cpCStoreRds -> {
            Integer resultCount = cpStoreMapper.selectCount(new QueryWrapper<CpCStore>().lambda()
                    .eq(CpCStore::getId, cpCStoreRds.getId()));

            //判断该逻辑仓是否已经插入pg库,大于0为已插入，小于0位未插入
            if (resultCount > 0) {
                CpCStore cpCStorePg = cpStoreMapper.selectById(cpCStoreRds.getId());

                //去除逻辑仓档案实体类基础属性字段
                CpCStore cpCStorePgAfter = removeBaseInfo(cpCStorePg);
                CpCStore cpCStoreRdsAfter = removeBaseInfo(cpCStoreRds);

                //判断内容是否相同不相同则执行更改
                if (!cpCStorePgAfter.equals(cpCStoreRdsAfter)) {
                    cpStoreMapper.updateById(cpCStoreRds);
                    if (log.isDebugEnabled()) {
                        log.debug("pg库未更新id为" + cpCStoreRds.getId() + "的逻辑仓档案信息，现更新，数据为"
                                + JSONObject.toJSONString(cpCStoreRds) + ";");
                    }
                }
            } else {
                //逻辑仓库存未同步到pg库，执行新增
                cpStoreMapper.insert(cpCStoreRds);
                if (log.isDebugEnabled()) {
                    log.debug("pg库未插入id为" + cpCStoreRds.getId() + "的逻辑仓档案信息，现插入，数据为"
                            + JSONObject.toJSONString(cpCStoreRds) + ";");
                }
            }
        });

        return holder;
    }

    /**
     * 去除逻辑仓档案实体类基础属性字段
     *
     * @param cpCStore 逻辑仓档案
     * @return 逻辑仓档案
     */
    private CpCStore removeBaseInfo(CpCStore cpCStore) {
        cpCStore.setCreationdate(null);
        cpCStore.setModifieddate(null);
        return cpCStore;
    }

    /**
     * 入参校验
     *
     * @param cpStoreSyncRequest 逻辑仓档案信息集合
     * @return 执行结果
     */
    private ValueHolderV14 checkServiceParam(CpStoreSyncRequest cpStoreSyncRequest) {
        ValueHolderV14 holderV14 = new ValueHolderV14<>(ResultCode.SUCCESS, "");

        if (cpStoreSyncRequest == null) {
            holderV14.setCode(ResultCode.FAIL);
            holderV14.setMessage("请求体不能为空!");
        }

        if (cpStoreSyncRequest != null && CollectionUtils.isEmpty(cpStoreSyncRequest.getCpCStoreList())) {
            holderV14.setCode(ResultCode.FAIL);
            holderV14.setMessage("逻辑仓档案信息为空");
        }
        return holderV14;
    }
}



