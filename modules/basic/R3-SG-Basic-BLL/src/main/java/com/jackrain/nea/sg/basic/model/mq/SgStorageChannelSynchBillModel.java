package com.jackrain.nea.sg.basic.model.mq;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/4/29 19:43
 */
@Data
public class SgStorageChannelSynchBillModel implements Serializable {

    /**
     * 单据编号
     */
    @Deprecated
    private String billNo;
    /**
     * 变动日期
     */
    private String changeTime;
    /**
     * 渠道ID
     */
    @Deprecated
    private Long cpCshopId;
    /**
     * 单据明细
     */
    List<SgStorageChannelSynchBillItemModel> itemList;

}
