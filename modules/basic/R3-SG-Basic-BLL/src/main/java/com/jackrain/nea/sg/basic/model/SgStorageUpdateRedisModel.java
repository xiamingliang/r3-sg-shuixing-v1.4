package com.jackrain.nea.sg.basic.model;

import lombok.Data;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/3/13 15:00
 */
@Data
public class SgStorageUpdateRedisModel {

    /**
     * Redis库存键
     */
    private String stoRedisKey;
    /**
     * 库存更新数量（形式：占用数量,在途数量,在库数量）
     */
    private String qtyChange;

}
