package com.jackrain.nea.sg.basic.common;


/**
 * @author csy
 * <p>
 * r3框架的 参数常量
 * <p>
 * param={"objid":-1,"fixcolumn":{"AAA":{},"AAA_ITEM":[]},"table":"AAA"}
 */
public interface R3ParamConstants {

    String OBJID = "objid";

    String TABLENAME = "tablename";

    String TABLE = "table";

    String FIXCOLUMN = "fixcolumn";

    String PARAM = "param";

    String IDS = "ids";

    String CODE = "code";

    String MESSAGE = "message";

    String DATA = "data";

    /**
     * 删除明细时不传fixcolumn 传 tabitem，踩过的坑
     */
    String TABITEM = "tabitem";
}
