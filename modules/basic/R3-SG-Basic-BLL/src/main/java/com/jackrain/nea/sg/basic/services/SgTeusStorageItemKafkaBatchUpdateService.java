package com.jackrain.nea.sg.basic.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.cp.result.CpCStore;
import com.jackrain.nea.data.basic.model.request.StoreInfoQueryRequest;
import com.jackrain.nea.data.basic.services.BasicCpQueryService;
import com.jackrain.nea.redis.config.CusRedisTemplate;
import com.jackrain.nea.redis.util.RedisOpsUtil;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.config.SgStorageControlConfig;
import com.jackrain.nea.sg.basic.mapper.SgBStorageLockLogMapper;
import com.jackrain.nea.sg.basic.model.SgStorageItemLockKey;
import com.jackrain.nea.sg.basic.model.SgStorageUpdateControlModel;
import com.jackrain.nea.sg.basic.model.SgTeusStorageUpdateCommonModel;
import com.jackrain.nea.sg.basic.model.request.SgStorageUpdateControlRequest;
import com.jackrain.nea.sg.basic.model.result.SgTeusStorageBatchUpdateResult;
import com.jackrain.nea.sg.basic.model.result.SgTeusStorageUpdateCommonResult;
import com.jackrain.nea.sg.basic.model.table.SgBStorageLockLog;
import com.jackrain.nea.sg.basic.utils.StorageLogUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * @Description:通用Kafka实体仓明细箱库存更新接口
 * @Author: chenb
 * @Date: 2019/3/17 16:08
 */
@Component
@Slf4j
public class SgTeusStorageItemKafkaBatchUpdateService {

    @Autowired
    SgStorageControlConfig sgStorageControlConfig;

    @Autowired
    private SgBStorageLockLogMapper sgBStorageLockLogMapper;

    @Autowired
    private SgTeusStorageItemBatchUpdateService sgTeusStorageItemBatchUpdateService;

    @Autowired
    private BasicCpQueryService basicCpQueryService;


    /**
     * 通用Kafka明细逻辑仓库存更新接口
     *
     * @param batchModel
     * @param loginUser
     * @return ValueHolderV14<SgTeusStorageBatchUpdateResult>
     */
    public ValueHolderV14<SgTeusStorageBatchUpdateResult> updateStorageItem(List<SgTeusStorageUpdateCommonModel> batchModel,
                                                                               User loginUser) {

        long startTime = System.currentTimeMillis();

        log.info("Start SgTeusStorageItemKafkaBatchUpdateService.updateStorageItem. ReceiveParams:size:{};",
                JSONObject.toJSONString(batchModel.size()));

        ValueHolderV14<SgTeusStorageBatchUpdateResult> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        ValueHolderV14<SgTeusStorageUpdateCommonResult> itemUpdateResult = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        SgTeusStorageBatchUpdateResult updateResult = new SgTeusStorageBatchUpdateResult();
        List<SgTeusStorageUpdateCommonModel> batchUpdateCommonList;
        Map<String, SgTeusStorageUpdateCommonModel> batchUpdateCommonMap = new HashMap<>();
        HashMap<Long, CpCStore> stoInfoMap = null;
        SgStorageItemLockKey lockKey = new SgStorageItemLockKey();
        String currentThreadName = Thread.currentThread().getName();
        CusRedisTemplate<String, String> redisTemplate = RedisOpsUtil.getStrRedisTemplate();

        //发生错误的明细数
        long errorItemQty = 0;

        //检查入参
        holder = checkServiceParam(batchModel, loginUser);
        if (ResultCode.FAIL == holder.getCode()) {
            return holder;
        }

        //Kafka同步的情况下，负库存控制在Redis中管理，同步PG库时不进行管理
        SgStorageUpdateControlModel controlModel = new SgStorageUpdateControlModel();
        controlModel.setNegativePreout(Boolean.TRUE);
        controlModel.setNegativePrein(Boolean.TRUE);
        controlModel.setNegativeStorage(Boolean.TRUE);
        controlModel.setNegativeAvailable(Boolean.TRUE);
        controlModel.setPreoutOperateType(SgConstantsIF.PREOUT_OPT_TYPE_ERROR);

        SgStorageUpdateControlRequest controlRequest = new SgStorageUpdateControlRequest();
        controlRequest.setNegativePreout(Boolean.TRUE);
        controlRequest.setNegativePrein(Boolean.TRUE);
        controlRequest.setNegativeStorage(Boolean.TRUE);
        controlRequest.setNegativeAvailable(Boolean.TRUE);
        controlRequest.setPreoutOperateType(SgConstantsIF.PREOUT_OPT_TYPE_ERROR);

        /** ########## 获取逻辑仓基础信息 ########## **/
        //同步R3基础数据的情况下，获取组织中心的基础数据
        if (sgStorageControlConfig.isSyncR3BasicData()) {
            //批量获取逻辑仓信息
            StoreInfoQueryRequest storeRequest = new StoreInfoQueryRequest();
            HashMap<Long, Long> stoIdMap = new HashMap<>(16);

            for (SgTeusStorageUpdateCommonModel itemInfo : batchModel) {
                if (itemInfo != null) {
                    stoIdMap.put(itemInfo.getCpCStoreId(), itemInfo.getCpCStoreId());
                }

            }
            storeRequest.setIds(new ArrayList<>(stoIdMap.values()));

            try {

                stoInfoMap = basicCpQueryService.getStoreInfo(storeRequest);

            } catch (Exception e) {

                String errorMsg = StorageLogUtils.getErrMessage(e, currentThreadName, loginUser);

                holder.setCode(ResultCode.FAIL);
                holder.setMessage(errorMsg.concat(" " + holder.getMessage()));

                return holder;
            }
        }

        //单据明细排序（避免库存更新死锁）
        Collections.sort(batchModel);

        for (SgTeusStorageUpdateCommonModel updateCommonModel : batchModel) {

            if (log.isDebugEnabled()) {
                log.debug("SgTeusStorageItemKafkaBatchUpdateService.updateStorageItem. updateCommonModel:{};"
                        , JSONObject.toJSONString(updateCommonModel));
            }

            BeanUtils.copyProperties(updateCommonModel, lockKey);
            lockKey.setBillDate(updateCommonModel.getBillDate().toString());
            lockKey.setChangeDate(updateCommonModel.getChangeDate().toString());

            updateCommonModel.setLockKey(DigestUtils.md5Hex(JSONObject.toJSONString(lockKey)));
            updateCommonModel.setOwnerename(loginUser.getEname());
            updateCommonModel.setModifierename(loginUser.getEname());
            updateCommonModel.setControlmodel(controlRequest);

            try {

                //同步R3基础数据的情况下，获取组织中心的基础数据
                if (controlModel.isSyncR3BasicData()) {

                    if (stoInfoMap != null && stoInfoMap.get(updateCommonModel.getCpCStoreId()) != null) {

                        updateCommonModel.setCpCStoreEcode(stoInfoMap.get(updateCommonModel.getCpCStoreId()).getEcode());
                        updateCommonModel.setCpCStoreEname(stoInfoMap.get(updateCommonModel.getCpCStoreId()).getEname());

                    } else {
                        holder.setCode(ResultCode.FAIL);
                        holder.setMessage(Resources.getMessage("店仓信息获取失败，店仓不存在！",
                                loginUser.getLocale(), updateCommonModel.getCpCStoreEcode()));
                        errorItemQty++;

                        continue;
                    }
                }

                batchUpdateCommonMap.put(updateCommonModel.getLockKey(), updateCommonModel);

            } catch (Exception e) {

                String errorMsg = StorageLogUtils.getErrMessage(e, currentThreadName, loginUser);

                holder.setCode(ResultCode.FAIL);
                holder.setMessage(errorMsg.concat(" " + holder.getMessage()));

                errorItemQty++;

            }

        }

        List<SgBStorageLockLog> lockLogList = sgBStorageLockLogMapper.selectList(
                new QueryWrapper<SgBStorageLockLog>().lambda().select(SgBStorageLockLog::getLockKey)
                        .in(SgBStorageLockLog::getLockKey, batchUpdateCommonMap.keySet()));

        if (!CollectionUtils.isEmpty(lockLogList)) {
            for (SgBStorageLockLog existKey : lockLogList) {
                log.info("SgTeusStorageItemKafkaBatchUpdateService.updateStorageItem. md5LockKey:{} lockKey is already existed. "
                        , existKey);
                batchUpdateCommonMap.remove(existKey.getLockKey());
            }
        }

        batchUpdateCommonList = new ArrayList<>(batchUpdateCommonMap.values());

        //批量更新单据明细的情况下
        if (!CollectionUtils.isEmpty(batchUpdateCommonList)) {

            //批量更新单据明细分页
            int pageSize = sgStorageControlConfig.getBatchUpdateItems();
            int listSize = batchUpdateCommonList.size();
            int page = listSize / pageSize;

            if (listSize % pageSize != 0) {
                page++;
            }

            //分页批量更新
            for (int i = 0; i < page; i++) {

                List<SgTeusStorageUpdateCommonModel> pageList = batchUpdateCommonList.subList(i * pageSize,
                        (((i + 1) * pageSize > listSize ? listSize : pageSize * (i + 1))));
                try {

                    itemUpdateResult = sgTeusStorageItemBatchUpdateService.updateTeusStorageItemWithTrans(
                            pageList, controlModel, loginUser);


                    if (ResultCode.FAIL == itemUpdateResult.getCode()) {

                        holder.setCode(ResultCode.FAIL);
                        holder.setMessage(itemUpdateResult.getMessage().concat(" " + holder.getMessage()));
                        errorItemQty = errorItemQty + pageList.size();

                    } else {

                        //箱库存更新成功后，删除Redis中的流水记录
                        if (!CollectionUtils.isEmpty(pageList)) {
                            if (sgStorageControlConfig.isRedisFunction()) {
                                for (SgTeusStorageUpdateCommonModel commonModel : pageList) {
                                    redisTemplate.opsForHash().delete(commonModel.getRedisBillFtpKey(),
                                            commonModel.getBillId() + "_" + commonModel.getBillItemId());
                                }
                            }
                        }
                    }

                } catch (Exception e) {

                    String errorMsg = StorageLogUtils.getErrMessage(e, currentThreadName, loginUser);

                    holder.setCode(ResultCode.FAIL);
                    holder.setMessage(errorMsg.concat(" " + holder.getMessage()));

                    errorItemQty = errorItemQty + pageList.size();

                }
            }
        }

        updateResult.setErrorBillItemQty(errorItemQty);
        holder.setData(updateResult);

        // 没有错误明细的情况下，设置成功运行结果，
        // 有错误明细的情况下，在发生错误的明细中设置运行结果
        if (errorItemQty < 1) {

            holder.setCode(ResultCode.SUCCESS);
            holder.setMessage(Resources.getMessage("当前单据的店仓箱库存变动消息处理完毕！",
                    loginUser.getLocale(), holder.getMessage(), currentThreadName));

        }

        log.info("Finish SgTeusStorageItemKafkaBatchUpdateService.updateStorageItem. ReturnResult:errorBillItemQty:{} spend time:{}ms;"
                , JSONObject.toJSONString(holder.getData() == null ? 0 : holder.getData().getErrorBillItemQty()), System.currentTimeMillis() - startTime);

        return holder;
    }

    /**
     * @param batchModel
     * @param loginUser
     * @return ValueHolderV14<SgTeusStorageBatchUpdateResult>
     */
    private ValueHolderV14<SgTeusStorageBatchUpdateResult> checkServiceParam(List<SgTeusStorageUpdateCommonModel> batchModel,
                                                                            User loginUser) {

        ValueHolderV14<SgTeusStorageBatchUpdateResult> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");

        if (loginUser == null) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("操作用户信息不能为空！"));
            return holder;
        }

        if (CollectionUtils.isEmpty(batchModel)) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("请求体为空！", loginUser.getLocale()));
        }

        if (ResultCode.FAIL == holder.getCode() && log.isDebugEnabled()) {
            log.error("SgTeusStorageItemKafkaBatchUpdateService.checkServiceParam. checkServiceParam error:"
                    + holder.getMessage() + ";");
        }

        return holder;

    }

}
