package com.jackrain.nea.sg.basic.mapper;

import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.basic.model.table.SgBStoragePreoutFtp;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SgBStoragePreoutFtpMapper extends ExtentionMapper<SgBStoragePreoutFtp> {
}