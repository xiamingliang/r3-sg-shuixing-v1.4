package com.jackrain.nea.sg.basic.model;

import com.jackrain.nea.sg.basic.model.request.SgPhyStorageSingleUpdateRequest;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class SgPhyStorageRedisSynchModel {


    SgPhyStorageSingleUpdateRequest singleRequest;

    List<SgPhyStorageUpdateCommonModel> sgPhyStorageUpdateCommonList;

    List<SgPhyTeusStorageUpdateCommonModel> sgPhyTeusStorageUpdateCommonList;

    public SgPhyStorageRedisSynchModel() {
        sgPhyStorageUpdateCommonList = new ArrayList<>();
        sgPhyTeusStorageUpdateCommonList = new ArrayList<>();
    }

}
