package com.jackrain.nea.sg.basic.model;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/3/13 15:00
 */
@Data
public class SgStorageUpdateFunctionModel implements Comparable<SgStorageUpdateFunctionModel> {

    /**
     * 占用是否允许负库存
     */
    private String negativepreout;
    /**
     * 在途是否允许负库存
     */
    private String negativeprein;
    /**
     * 在库是否允许负库存
     */
    private String negativestorage;
    /**
     * 可用在库是否允许负库存
     */
    private String negativeavailable;
    /**
     * 占用流水ID
     */
    @JSONField(
            name = "preoutftpid"
    )
    private Long preoutFtpId;
    /**
     * 在途流水ID
     */
    @JSONField(
            name = "preinftpid"
    )
    private Long preinFtpId;
    /**
     * 在库流水ID
     */
    @JSONField(
            name = "storageftpid"
    )
    private Long storageFtpId;
    /**
     * 占用变动数量
     */
    @JSONField(
            name = "qtypreoutchange"
    )
    private BigDecimal qtyPreoutChange;
    /**
     * 在途变动数量
     */
    @JSONField(
            name = "qtypreinchange"
    )
    private BigDecimal qtyPreinChange;
    /**
     * 在库变动数量
     */
    @JSONField(
            name = "qtystoragechange"
    )
    private BigDecimal qtyStorageChange;
    /**
     * 箱内占用变动数量
     */
    @JSONField(
            name = "qtypreoutteuschange"
    )
    private BigDecimal qtyPreoutTeusChange;
    /**
     * 箱内在途变动数量
     */
    @JSONField(
            name = "qtypreinteuschange"
    )
    private BigDecimal qtyPreinTeusChange;
    /**
     * 箱内在库变动数量
     */
    @JSONField(
            name = "qtystorageteuschange"
    )
    private BigDecimal qtyStorageTeusChange;
    /**
     * MQ幂等性防重锁
     */
    @JSONField(
            name = "messagekey"
    )
    private String messageKey;
    /**
     * 库存变动类型
     */
    @JSONField(
            name = "storagetype"
    )
    @Deprecated
    private Integer storageType;
    /**
     * 单据类型
     */
    @JSONField(
            name = "billtype"
    )
    private Integer billType;
    /**
     * 业务节点
     */
    @JSONField(
            name = "servicenode"
    )
    private Long serviceNode;
    /**
     * 单据ID
     */
    @JSONField(
            name = "billid"
    )
    private Long billId;
    /**
     * 单据编号
     */
    @JSONField(
            name = "billno"
    )
    private String billNo;
    /**
     * 业务单据ID
     */
    @JSONField(
            name = "sourcebillid"
    )
    private Long sourceBillId;
    /**
     * 业务单据编号
     */
    @JSONField(
            name = "sourcebillno"
    )
    private String sourceBillNo;
    /**
     * 单据日期
     */
    @JSONField(
            name = "billdate"
    )
    private String billDate;
    /**
     * 单据明细ID
     */
    @JSONField(
            name = "billitemid"
    )
    private Long billItemId;
    /**
     * 变动日期
     */
    @JSONField(
            name = "changedate"
    )
    private String changeDate;
    /**
     * 店仓id
     */
    @JSONField(
            name = "cpcstoreid"
    )
    private Long cpCStoreId;
    /**
     * 店仓编码
     */
    @JSONField(
            name = "cpcstoreecode"
    )
    private String cpCStoreEcode;
    /**
     * 店仓名称
     */
    @JSONField(
            name = "cpcstoreename"
    )
    private String cpCStoreEname;
    /**
     * 商品id
     */
    @JSONField(
            name = "pscproid"
    )
    private Long psCProId;
    /**
     * 商品编码
     */
    @JSONField(
            name = "pscproecode"
    )
    private String psCProEcode;
    /**
     * 商品名称
     */
    @JSONField(
            name = "pscproename"
    )
    private String psCProEname;
    /**
     * 条码id
     */
    @JSONField(
            name = "pscskuid"
    )
    private Long psCSkuId;
    /**
     * 条码编码
     */
    @JSONField(
            name = "pscskuecode"
    )
    private String psCSkuEcode;
    /**
     * 国标码
     */
    private String gbcode;
    /**
     * 规格1ID
     */
    @JSONField(
            name = "pscspec1id"
    )
    private Long psCSpec1Id;
    /**
     * 规格1编码
     */
    @JSONField(
            name = "pscspec1ecode"
    )
    private String psCSpec1Ecode;
    /**
     * 规格1名称
     */
    @JSONField(
            name = "pscspec1ename"
    )
    private String psCSpec1Ename;
    /**
     * 规格2ID
     */
    @JSONField(
            name = "pscspec2id"
    )
    private Long psCSpec2Id;
    /**
     * 规格2编码
     */
    @JSONField(
            name = "pscspec2ecode"
    )
    private String psCSpec2Ecode;
    /**
     * 规格2名称
     */
    @JSONField(
            name = "pscspec2ename"
    )
    private String psCSpec2Ename;
    /**
     * 吊牌价
     */
    @JSONField(
            name = "pricelist"
    )
    private BigDecimal priceList;
    /**
     * 成本价
     */
    @JSONField(
            name = "pricecost"
    )
    private BigDecimal priceCost;
    /**
     * 期初数量
     */
    @JSONField(
            name = "qtybegin"
    )
    @Deprecated
    private BigDecimal qtyBegin;
    /**
     * 变动数量
     */
    @JSONField(
            name = "qtychange"
    )
    @Deprecated
    private BigDecimal qtyChange;
    /**
     * 期末数量
     */
    @JSONField(
            name = "qtyend"
    )
    @Deprecated
    private BigDecimal qtyEnd;
    /**
     * 库存锁
     */
    @JSONField(
            name = "lockkey"
    )
    private String lockKey;
    /**
     * Redis更新流水键
     */
    @JSONField(
            name = "redisbillftpkey"
    )
    private String redisBillFtpKey;
    /**
     * 创建人姓名
     */
    private String ownerename;
    /**
     * 修改人姓名
     */
    private String modifierename;

    @JSONField(
            name = "ad_client_id"
    )
    private Long adClientId;

    @JSONField(
            name = "ad_org_id"
    )
    private Long adOrgId;
    /**
     * 单出库通知据ID
     */
    @JSONField(
            name = "phyoutnoticesid"
    )
    private Long phyOutNoticesId;
    /**
     * 出库通知单据编号
     */
    @JSONField(
            name = "phyoutnoticesno"
    )
    private String phyOutNoticesNo;
    /**
     * 入库通知单据ID
     */
    @JSONField(
            name = "phyinnoticesid"
    )
    private Long phyInNoticesId;
    /**
     * 入库通知单据编号
     */
    @JSONField(
            name = "phyinnoticesno"
    )
    private String phyInNoticesNo;

    private String creationdate;

    private String modifieddate;

    private Long ownerid;

    private String ownername;

    private Long modifierid;

    private String modifiername;

    private String isactive;


    @Override
    public int compareTo(SgStorageUpdateFunctionModel o) {
        // 目前排序顺序：3;在库 -> 2;在途 -> 1;占用量
        return o.toString().compareTo(this.toString());
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append(storageType).append("_").append(cpCStoreId).append("_").append(psCSkuId);
        return sb.toString();
    }
}
