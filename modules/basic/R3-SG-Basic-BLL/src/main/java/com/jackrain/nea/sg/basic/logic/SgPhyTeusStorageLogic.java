package com.jackrain.nea.sg.basic.logic;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.model.util.ModelUtil;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.mapper.SgBPhyTeusStorageChangeFtpMapper;
import com.jackrain.nea.sg.basic.mapper.SgBPhyTeusStorageMapper;
import com.jackrain.nea.sg.basic.model.SgPhyTeusStorageUpdateCommonModel;
import com.jackrain.nea.sg.basic.model.SgStorageUpdateControlModel;
import com.jackrain.nea.sg.basic.model.result.SgPhyTeusStorageUpdateCommonResult;
import com.jackrain.nea.sg.basic.model.table.SgBPhyTeusStorage;
import com.jackrain.nea.sg.basic.model.table.SgBPhyTeusStorageChangeFtp;
import com.jackrain.nea.sg.basic.utils.StorageUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/3/13 14:50
 */
@Component
@Slf4j
public class SgPhyTeusStorageLogic {

    @Autowired
    private SgBPhyTeusStorageMapper sgBPhyTeusStorageMapper;

    @Autowired
    private SgBPhyTeusStorageChangeFtpMapper sgBPhyTeusStorageChangeFtpMapper;

    /**
     * 更新箱库存及流水
     *
     * @param updateModel
     * @param controlModel
     * @param loginUser
     * @return ValueHolderV14<SgPhyTeusStorageUpdateCommonResult>
     */
    public ValueHolderV14<SgPhyTeusStorageUpdateCommonResult> updatePhyTeusStorageChangeWithFtp(
            SgPhyTeusStorageUpdateCommonModel updateModel,
            SgStorageUpdateControlModel controlModel,
            User loginUser) {

        ValueHolderV14<SgPhyTeusStorageUpdateCommonResult> holder;

        holder = updatePhyTeusStorageChange(updateModel, controlModel, loginUser);

        if (ResultCode.FAIL == holder.getCode()) {
            return holder;
        }

        BeanUtils.copyProperties(holder.getData(), updateModel);

        holder = insertPhyTeusStorageChangeFtp(updateModel, loginUser);

        return holder;
    }

    /**
     * 更新箱库存
     *
     * @param updateModel
     * @param controlModel
     * @param loginUser
     * @return ValueHolderV14<SgPhyTeusStorageUpdateCommonResult>
     */
    public ValueHolderV14<SgPhyTeusStorageUpdateCommonResult> updatePhyTeusStorageChange(
            SgPhyTeusStorageUpdateCommonModel updateModel,
            SgStorageUpdateControlModel controlModel,
            User loginUser) {

        Date systemDate = new Date();
        ValueHolderV14<SgPhyTeusStorageUpdateCommonResult> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        SgPhyTeusStorageUpdateCommonResult commonResult = new SgPhyTeusStorageUpdateCommonResult();

        updateModel.setModifierid(loginUser.getId() == null ? null : loginUser.getId().longValue());
        updateModel.setModifierename(loginUser.getEname());
        updateModel.setModifiername(loginUser.getName());
        updateModel.setModifieddate(systemDate);

        boolean blncheckNegative = !controlModel.isNegativeStorage() &&
                (updateModel.getQtyChange().compareTo(BigDecimal.ZERO) < 0 ? true : false);

        int updateResult = sgBPhyTeusStorageMapper.updateQtyChange(updateModel,
                blncheckNegative);

        //在库不允许负库存 并且 更新0条记录的情况下
        if (!controlModel.isNegativeStorage() && updateResult <= 0) {

            holder.setCode(ResultCode.FAIL);

            if (log.isDebugEnabled()) {
                log.debug("SgPhyTeusStorageLogic.updatePhyTeusStorageChange. 当前箱库存变动明细库存不足！！" +
                                "单据编号:{}，实体仓ID:{}，箱ID:{}，变动数量:{}",
                        updateModel.getBillNo(), updateModel.getCpCPhyWarehouseId(),
                        updateModel.getPsCTeusId(), updateModel.getQtyChange());
            }

            List<SgPhyTeusStorageUpdateCommonModel> outStockItemList = new ArrayList<>();
            outStockItemList.add(updateModel);
            commonResult.setOutStockItemList(outStockItemList);

            holder.setData(commonResult);
            holder.setMessage(Resources.getMessage("实体仓箱库存不足！",
                    loginUser.getLocale(), updateModel.getCpCPhyWarehouseEcode(), updateModel.getPsCTeusEcode()));
            return holder;

        }

        commonResult.setCountResult(updateResult);

        List<SgBPhyTeusStorage> selectResult = sgBPhyTeusStorageMapper.selectList(
                new QueryWrapper<SgBPhyTeusStorage>().lambda()
                        .eq(SgBPhyTeusStorage::getCpCPhyWarehouseId, updateModel.getCpCPhyWarehouseId())
                        .eq(SgBPhyTeusStorage::getPsCTeusId, updateModel.getPsCTeusId()));

        if (selectResult != null && selectResult.size() > 0) {

            BigDecimal qtyEnd = selectResult.get(0).getQtyStorage();
            //本版本的期初数量暂时通过期末数量为基准计算
            commonResult.setQtyBegin(qtyEnd.subtract(updateModel.getQtyChange()));
            commonResult.setQtyEnd(qtyEnd);
        }

        holder.setData(commonResult);
        holder.setCode(ResultCode.SUCCESS);
        holder.setMessage(Resources.getMessage("实体仓箱在库数量更新成功！", loginUser.getLocale()));

        return holder;
    }

    /**
     * 记录箱库存变动流水
     *
     * @param updateModel
     * @param loginUser
     * @return ValueHolderV14<SgPhyTeusStorageUpdateCommonResult>
     */
    public ValueHolderV14<SgPhyTeusStorageUpdateCommonResult> insertPhyTeusStorageChangeFtp(
            SgPhyTeusStorageUpdateCommonModel updateModel,
            User loginUser) {

        ValueHolderV14<SgPhyTeusStorageUpdateCommonResult> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        SgPhyTeusStorageUpdateCommonResult commonResult = new SgPhyTeusStorageUpdateCommonResult();
        SgBPhyTeusStorageChangeFtp changeUpdateModel = new SgBPhyTeusStorageChangeFtp();

        BeanUtils.copyProperties(updateModel, changeUpdateModel);
        StorageUtils.setBModelDefalutData(changeUpdateModel, loginUser);
        changeUpdateModel.setId(ModelUtil.getSequence(SgConstants.SG_B_PHY_TEUS_STORAGE_CHANGE_FTP));
        changeUpdateModel.setOwnerename(loginUser.getEname());
        changeUpdateModel.setModifierename(loginUser.getEname());
        changeUpdateModel.setRedisBillFtpKey(updateModel.getRedisBillFtpKey());

        int insertResult = sgBPhyTeusStorageChangeFtpMapper.insert(changeUpdateModel);

        if (insertResult <= 0) {
            log.error("SgPhyTeusStorageLogic.insertPhyTeusStorageChangeFtp 实体仓箱库存变动流水插入失败！单据编号:{},实体仓编码:{},箱号:{}",
                    updateModel.getBillNo(),
                    updateModel.getCpCPhyWarehouseEcode(),
                    updateModel.getPsCTeusEcode());
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("实体仓箱库存变动流水插入失败！",
                    loginUser.getLocale(),
                    updateModel.getBillNo(),
                    updateModel.getCpCPhyWarehouseEcode(),
                    updateModel.getPsCTeusEcode()));
            return holder;
        }

        holder.setData(commonResult);
        holder.setCode(ResultCode.SUCCESS);
        holder.setMessage(Resources.getMessage("实体仓箱库存变动流水插入成功！", loginUser.getLocale()));

        return holder;
    }

    /**
     * 批量记录库存变动流水
     *
     * @param updateModelList
     * @param loginUser
     * @return ValueHolderV14<SgPhyTeusStorageUpdateCommonResult>
     */
    public ValueHolderV14<SgPhyTeusStorageUpdateCommonResult> insertPhyTeusStorageChangeFtpList(
            List<SgPhyTeusStorageUpdateCommonModel> updateModelList,
            User loginUser) {

        ValueHolderV14<SgPhyTeusStorageUpdateCommonResult> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        SgPhyTeusStorageUpdateCommonResult commonResult = new SgPhyTeusStorageUpdateCommonResult();
        List<SgBPhyTeusStorageChangeFtp> batchInsertList = new ArrayList<>();

        for (SgPhyTeusStorageUpdateCommonModel updateModel : updateModelList) {

            SgBPhyTeusStorageChangeFtp changeUpdateModel = new SgBPhyTeusStorageChangeFtp();

            BeanUtils.copyProperties(updateModel, changeUpdateModel);
            StorageUtils.setBModelDefalutData(changeUpdateModel, loginUser);
            changeUpdateModel.setId(ModelUtil.getSequence(SgConstants.SG_B_PHY_TEUS_STORAGE_CHANGE_FTP));
            changeUpdateModel.setOwnerename(loginUser.getEname());
            changeUpdateModel.setModifierename(loginUser.getEname());
            changeUpdateModel.setRedisBillFtpKey(updateModel.getRedisBillFtpKey());

            batchInsertList.add(changeUpdateModel);
        }

        List<List<SgBPhyTeusStorageChangeFtp>> insertPageList =
                StorageUtils.getBaseModelPageList(batchInsertList, SgConstants.SG_COMMON_INSERT_PAGE_SIZE);

        for (List<SgBPhyTeusStorageChangeFtp> pageList : insertPageList) {

            if (CollectionUtils.isEmpty(pageList)) {
                continue;
            }

            int insertResult = sgBPhyTeusStorageChangeFtpMapper.batchInsert(pageList);

            if (insertResult != pageList.size()) {
                log.error("SgPhyTeusStorageLogic.insertPhyTeusStorageChangeFtpList 实体仓批量箱库存变动流水插入失败！");
                holder.setCode(ResultCode.FAIL);
                holder.setMessage(Resources.getMessage("实体仓批量箱库存变动流水插入失败！",
                        loginUser.getLocale()));
                return holder;
            }

        }

        holder.setData(commonResult);
        holder.setCode(ResultCode.SUCCESS);
        holder.setMessage(Resources.getMessage("实体仓批量箱库存变动流水插入成功！", loginUser.getLocale()));

        return holder;
    }

    /**
     * 初始化箱库存
     *
     * @param updateModel
     * @param loginUser
     * @return ValueHolderV14<SgPhyTeusStorageUpdateCommonResult>
     */
    public ValueHolderV14<SgPhyTeusStorageUpdateCommonResult> initPhyTeusStorage(
            SgPhyTeusStorageUpdateCommonModel updateModel,
            User loginUser) {

        ValueHolderV14<SgPhyTeusStorageUpdateCommonResult> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        SgPhyTeusStorageUpdateCommonResult commonResult = new SgPhyTeusStorageUpdateCommonResult();

        SgBPhyTeusStorage sgBPhyTeusStorage = new SgBPhyTeusStorage();
        BeanUtils.copyProperties(updateModel, sgBPhyTeusStorage);
        sgBPhyTeusStorage.setId(ModelUtil.getSequence(SgConstants.SG_B_PHY_TEUS_STORAGE));
        sgBPhyTeusStorage.setQtyStorage(BigDecimal.ZERO);

        int insertResult = sgBPhyTeusStorageMapper.insert(sgBPhyTeusStorage);

        if (insertResult <= 0) {
            log.error("SgPhyTeusStorageLogic.initPhyTeusStorage 实体仓箱库存初始化插入失败！实体仓编码:{},箱号:{}",
                    updateModel.getCpCPhyWarehouseEcode(),
                    updateModel.getPsCTeusEcode());
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("实体仓箱库存初始化插入失败！",
                    loginUser.getLocale(),
                    updateModel.getCpCPhyWarehouseEcode(),
                    updateModel.getPsCTeusEcode()));
            return holder;
        }

        holder.setData(commonResult);
        holder.setCode(ResultCode.SUCCESS);
        holder.setMessage(Resources.getMessage("实体仓箱库存初始化插入成功！", loginUser.getLocale()));

        return holder;
    }

    /**
     * 批量初始化箱库存
     *
     * @param updateModelList
     * @param loginUser
     * @return ValueHolderV14<SgPhyTeusStorageUpdateCommonResult>
     */
    public ValueHolderV14<SgPhyTeusStorageUpdateCommonResult> initPhyTeusStorageList(
            List<SgPhyTeusStorageUpdateCommonModel> updateModelList,
            User loginUser) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgPhyTeusStorageLogic.initPhyTeusStorageList. ReceiveParams:updateModelList="
                    + JSONObject.toJSONString(updateModelList) + ";");
        }

        ValueHolderV14<SgPhyTeusStorageUpdateCommonResult> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        SgPhyTeusStorageUpdateCommonResult commonResult = new SgPhyTeusStorageUpdateCommonResult();
        List<SgBPhyTeusStorage> batchInitList = new ArrayList<>();

        for (SgPhyTeusStorageUpdateCommonModel updateModel : updateModelList) {

            SgBPhyTeusStorage sgBPhyTeusStorage = new SgBPhyTeusStorage();
            BeanUtils.copyProperties(updateModel, sgBPhyTeusStorage);
            sgBPhyTeusStorage.setId(ModelUtil.getSequence(SgConstants.SG_B_PHY_TEUS_STORAGE));
            sgBPhyTeusStorage.setQtyStorage(BigDecimal.ZERO);

            batchInitList.add(sgBPhyTeusStorage);
        }

        List<List<SgBPhyTeusStorage>> insertPageList =
                StorageUtils.getBaseModelPageList(batchInitList, SgConstants.SG_COMMON_INSERT_PAGE_SIZE);

        for (List<SgBPhyTeusStorage> pageList : insertPageList) {

            if (CollectionUtils.isEmpty(pageList)) {
                continue;
            }

            int insertResult = sgBPhyTeusStorageMapper.batchInsert(pageList);

            if (insertResult != pageList.size()) {
                log.error("SgPhyTeusStorageLogic.initPhyTeusStorageList 实体仓批量箱库存初始化插入失败！");
                holder.setCode(ResultCode.FAIL);
                holder.setMessage(Resources.getMessage("实体仓批量箱库存初始化插入失败！",
                        loginUser.getLocale()));
                return holder;
            }
        }

        holder.setData(commonResult);
        holder.setCode(ResultCode.SUCCESS);
        holder.setMessage(Resources.getMessage("实体仓批量箱库存初始化插入成功！", loginUser.getLocale()));

        return holder;
    }
}
