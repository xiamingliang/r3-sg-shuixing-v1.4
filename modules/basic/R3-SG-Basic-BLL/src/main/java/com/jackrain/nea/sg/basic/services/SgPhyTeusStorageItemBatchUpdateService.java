package com.jackrain.nea.sg.basic.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.model.util.ModelUtil;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.config.SgStorageControlConfig;
import com.jackrain.nea.sg.basic.logic.SgPhyTeusStorageLogic;
import com.jackrain.nea.sg.basic.logic.SgStorageCheckLogic;
import com.jackrain.nea.sg.basic.mapper.SgBPhyTeusStorageMapper;
import com.jackrain.nea.sg.basic.mapper.SgBStorageLockLogMapper;
import com.jackrain.nea.sg.basic.model.SgPhyTeusStorageUpdateCommonModel;
import com.jackrain.nea.sg.basic.model.SgStorageUpdateControlModel;
import com.jackrain.nea.sg.basic.model.result.SgPhyTeusStorageUpdateCommonResult;
import com.jackrain.nea.sg.basic.model.table.SgBPhyTeusStorage;
import com.jackrain.nea.sg.basic.model.table.SgBStorageLockLog;
import com.jackrain.nea.sg.basic.utils.StorageUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/3/20 13:15
 */
@Component
@Slf4j
public class SgPhyTeusStorageItemBatchUpdateService {

    @Autowired
    private SgBPhyTeusStorageMapper sgBPhyTeusStorageMapper;

    @Autowired
    private SgBStorageLockLogMapper sgBStorageLockLogMapper;

    @Autowired
    private SgPhyTeusStorageLogic sgPhyTeusStorageLogic;

    @Autowired
    private SgStorageControlConfig sgStorageControlConfig;

    /**
     * @param requestList
     * @param controlModel
     * @param loginUser
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public ValueHolderV14<SgPhyTeusStorageUpdateCommonResult> updatePhyTeusStorageItemWithTrans(List<SgPhyTeusStorageUpdateCommonModel> requestList,
                                                                                        SgStorageUpdateControlModel controlModel,
                                                                                        User loginUser) {

        ValueHolderV14<SgPhyTeusStorageUpdateCommonResult> holder = updatePhyTeusStorageItem(requestList,
                controlModel, loginUser);

        if (ResultCode.FAIL == holder.getCode()) {
            throw new NDSException(holder.getMessage());
        }

        return holder;

    }

    /**
     * @param requestList
     * @param controlModel
     * @param loginUser
     * @return
     */
    public ValueHolderV14<SgPhyTeusStorageUpdateCommonResult> updatePhyTeusStorageItem(List<SgPhyTeusStorageUpdateCommonModel> requestList,
                                                                               SgStorageUpdateControlModel controlModel,
                                                                               User loginUser) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgPhyTeusStorageItemBatchUpdateService.updatePhyTeusStorageItem. ReceiveParams:requestList:{} controlModel:{};"
                    , JSONObject.toJSONString(requestList), JSONObject.toJSONString(controlModel));
        }

        ValueHolderV14<SgPhyTeusStorageUpdateCommonResult> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        List<SgBStorageLockLog> lockLogList = new ArrayList<>();
        List<SgPhyTeusStorageUpdateCommonModel> phyStorageChangeFtpList = new ArrayList<>();
        Map<Long, Long> phyWarehouseMap = new HashMap<>();
        Map<Long, Long> teusMap = new HashMap<>();
        Map<String, SgPhyTeusStorageUpdateCommonModel> phyTeusStorageNotExistMap = new HashMap<>();
        SgPhyTeusStorageUpdateCommonResult updateResult = new SgPhyTeusStorageUpdateCommonResult();
        int resultCount = 0;

        if (CollectionUtils.isEmpty(requestList)) {
            return holder;
        }

        for (SgPhyTeusStorageUpdateCommonModel request : requestList) {

            holder = SgStorageCheckLogic.checkServiceParam(request, loginUser);

            if (ResultCode.FAIL == holder.getCode()) {
                return holder;
            }

            phyWarehouseMap.put(request.getCpCPhyWarehouseId(), request.getCpCPhyWarehouseId());
            teusMap.put(request.getPsCTeusId(), request.getPsCTeusId());

            phyTeusStorageNotExistMap.put(request.getCpCPhyWarehouseId() + "_" + request.getPsCTeusId(),
                    request);

            //批量初始化库存处理流水锁
            SgBStorageLockLog lockLog = new SgBStorageLockLog();
            BeanUtils.copyProperties(request, lockLog);
            lockLog.setId(ModelUtil.getSequence(SgConstants.SG_B_STORAGE_LOCK_LOG));
            lockLogList.add(lockLog);

        }

        //查询已存在的库存记录
        List<SgBPhyTeusStorage> resultList = sgBPhyTeusStorageMapper.selectList(
                new QueryWrapper<SgBPhyTeusStorage>().lambda().select(SgBPhyTeusStorage::getCpCPhyWarehouseId, SgBPhyTeusStorage::getPsCTeusId)
                        .in(!CollectionUtils.isEmpty(phyWarehouseMap.values()), SgBPhyTeusStorage::getCpCPhyWarehouseId, new ArrayList<>(phyWarehouseMap.values()))
                        .in(!CollectionUtils.isEmpty(teusMap.values()), SgBPhyTeusStorage::getPsCTeusId, new ArrayList<>(teusMap.values()))
        );

        //除去【批量初始化库存】列表中已存在的记录
        if (!CollectionUtils.isEmpty(resultList)) {
            String key = null;
            for (SgBPhyTeusStorage storageRecode : resultList) {
                key = storageRecode.getCpCPhyWarehouseId() + "_" + storageRecode.getPsCTeusId();
                if (phyTeusStorageNotExistMap.containsKey(key)) {
                    phyTeusStorageNotExistMap.remove(key);
                }
            }
        }

        //批量初始化库存
        if (phyTeusStorageNotExistMap.size() > 0) {

            try {
                holder = sgPhyTeusStorageLogic.initPhyTeusStorageList(new ArrayList<>(phyTeusStorageNotExistMap.values()), loginUser);
            } catch (Exception e) {
                log.error("SgPhyTeusStorageLogic.initPhyTeusStorageList 实体仓批量箱库存初始化插入失败！messageKey:{}",
                        requestList.get(0).getMessageKey());
                //并发初始化库存记录异常时进行等待重试
                holder.setCode(ResultCode.FAIL);
                holder.setData(updateResult);
                holder.setMessage(Resources.getMessage("实体仓箱库存初始化插入失败！", loginUser.getLocale()));
                return holder;
            }

            if (ResultCode.FAIL == holder.getCode()) {
                throw new NDSException(holder.getMessage());
            }
        }

        List<List<SgBStorageLockLog>> insertPageList =
                StorageUtils.getBaseModelPageList(lockLogList, SgConstants.SG_COMMON_INSERT_PAGE_SIZE);

        //分页批量更新
        for (List<SgBStorageLockLog> pageList : insertPageList) {

            if (CollectionUtils.isEmpty(pageList)) {
                continue;
            }

            //批量初始化库存处理流水锁
            resultCount = sgBStorageLockLogMapper.batchInsert(pageList);

            if (resultCount != pageList.size()) {
                throw new NDSException(Resources.getMessage("实体仓箱库存锁批量初始化插入失败！", loginUser.getLocale()));
            }

        }

        //批量更新实体仓库存
        for (SgPhyTeusStorageUpdateCommonModel request : requestList) {

            holder = sgPhyTeusStorageLogic.updatePhyTeusStorageChange(request, controlModel, loginUser);

            //更新期初，期末数量
            if (ResultCode.SUCCESS == holder.getCode()) {
                BeanUtils.copyProperties(holder.getData(), request);
                phyStorageChangeFtpList.add(request);
            }

            if (ResultCode.FAIL == holder.getCode()) {
                throw new NDSException(holder.getMessage());
            }

        }

        //批量更新库存在库流水
        if (!CollectionUtils.isEmpty(phyStorageChangeFtpList)) {
            holder = sgPhyTeusStorageLogic.insertPhyTeusStorageChangeFtpList(phyStorageChangeFtpList, loginUser);

            if (ResultCode.FAIL == holder.getCode()) {
                throw new NDSException(holder.getMessage());
            }
        }

        holder.setData(updateResult);

        if (log.isDebugEnabled()) {
            log.debug("Finish SgPhyTeusStorageItemBatchUpdateService.updatePhyTeusStorageItem. ReturnResult:countResult:{};"
                    , JSONObject.toJSONString(holder.getData().getCountResult()));
        }

        return holder;

    }

}