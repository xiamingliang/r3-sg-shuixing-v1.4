package com.jackrain.nea.sg.basic.services;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.redis.config.CusRedisTemplate;
import com.jackrain.nea.redis.util.RedisOpsUtil;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.model.SgStorageBillUpdateControlModel;
import com.jackrain.nea.sg.basic.model.request.SgPhyStorageBatchUpdateRequest;
import com.jackrain.nea.sg.basic.model.request.SgPhyStorageSingleUpdateRequest;
import com.jackrain.nea.sg.basic.model.request.SgPhyStorageUpdateBillRequest;
import com.jackrain.nea.sg.basic.model.request.SgStorageUpdateControlRequest;
import com.jackrain.nea.sg.basic.model.result.SgPhyStorageBatchUpdateResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.thymeleaf.util.StringUtils;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @Description:通用实体仓库存批量更新接口
 * @Author: chenb
 * @Date: 2019/3/17 14:47
 */
@Component
@Slf4j
public class SgPhyStorageMQBatchUpdateService {

    @Autowired
    private SgPhyStorageBillUpdateService billUpdateService;

    public ValueHolderV14<SgPhyStorageBatchUpdateResult> updatePhyStorageBatch(SgPhyStorageBatchUpdateRequest request) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgPhyStorageMQBatchUpdateService.updatePhyStorageBatch. ReceiveParams:request="
                    + JSONObject.toJSONString(request) + ";");
        }

        ValueHolderV14<SgPhyStorageBatchUpdateResult> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        CusRedisTemplate<String, String> redisTemplate = RedisOpsUtil.getStrRedisTemplate();
        SgPhyStorageBatchUpdateResult updateResult = new SgPhyStorageBatchUpdateResult();
        //开启明细为单位的事务管理
        SgStorageBillUpdateControlModel billControlModel = new SgStorageBillUpdateControlModel();
        billControlModel.setTransactionLevel(SgConstants.TRANSACTION_LEVEL_ITEM);
        String messageKey = request.getMessageKey();

        holder.setData(updateResult);

        //初始化变量
        long totalErrorItemQty = 0;
        long startTime = System.currentTimeMillis();

        //检查入参
        holder = checkServiceParam(request, request.getLoginUser());
        if (ResultCode.FAIL == holder.getCode()) {

            if (log.isDebugEnabled()) {
                log.debug("Finish SgPhyStorageMQBatchUpdateService.updatePhyStorageBatch. ReturnResult:checkerror messageKey:{} spend time:{}ms;"
                        , messageKey, System.currentTimeMillis() - startTime);
            }

            return holder;
        }

        User loginUser = request.getLoginUser();
        SgStorageUpdateControlRequest controlModel = request.getControlModel();
        List<SgPhyStorageUpdateBillRequest> billList = request.getBillList();

        //设置MQ幂等性防重锁（设为【初始化状态】）
        String messageUniqueKey = SgConstants.REDIS_KEY_PREFIX_PHY_MQ_MESSAGE + messageKey;
        Boolean blnCanInit = redisTemplate.opsForValue().setIfAbsent(messageUniqueKey, "init");

        //判断当前MQ消息是否正在处理
        if (!blnCanInit) {

            //redis中当前锁已存在的情况下
            //获取当前锁的状态
            String messageStatus = redisTemplate.opsForValue().get(messageUniqueKey);

            if (messageStatus.equals(SgConstants.MESSAGE_STATUS_INIT)) {

                if (log.isDebugEnabled()) {
                    log.debug("Finish SgPhyStorageMQBatchUpdateService.updatePhyStorageBatch. ReturnResult:running messageKey:{} spend time:{}ms;"
                            , messageKey, System.currentTimeMillis() - startTime);
                }

                //锁状态为初始化的情况下
                holder.setCode(ResultCode.FAIL);
                holder.setMessage(Resources.getMessage("实体仓库存变动消息正在处理中！请耐心等待......", loginUser.getLocale()));
                return holder;
            } else if (messageStatus.equals(SgConstants.MESSAGE_STATUS_SUCCESS)) {

                if (log.isDebugEnabled()) {
                    log.debug("Finish SgPhyStorageMQBatchUpdateService.updatePhyStorageBatch. ReturnResult:finished messageKey:{} spend time:{}ms;"
                            , messageKey, System.currentTimeMillis() - startTime);
                }

                //锁状态为运行成功的情况下
                holder.setCode(ResultCode.SUCCESS);
                holder.setMessage(Resources.getMessage("实体仓库存变动消息已成功处理完毕！", loginUser.getLocale()));
                return holder;
            }

        } else if (blnCanInit) {

            //redis中当前锁不存在的情况下，设置锁的过期时间
            redisTemplate.expire(messageUniqueKey, 30, TimeUnit.MINUTES);

            //循环遍历MQ消息体中单据信息，调用通用单个单据库存更新接口
            for (int billIndex = 0; billIndex < billList.size(); billIndex++) {

                //设置单据MQ幂等性防重锁
                String itemNo = StringUtils.isEmpty(billList.get(billIndex).getBillNo()) ?
                        "billIndex[" + billIndex + "]" : billList.get(billIndex).getBillNo();
                String billUniqueKey = messageUniqueKey + ":" + itemNo;

                SgPhyStorageSingleUpdateRequest bill = new SgPhyStorageSingleUpdateRequest();
                bill.setLoginUser(loginUser);
                bill.setMessageKey(billUniqueKey);
                bill.setControlModel(controlModel);
                bill.setBill(billList.get(billIndex));

                try {
                    //调用通用单个单据库存更新接口
                    ValueHolderV14<SgPhyStorageBatchUpdateResult> result = billUpdateService.updatePhyStorageBill(bill, billControlModel);

                    if (result != null && result.getData() != null) {

                        if (result.getData().getErrorBillItemQty() > 0) {
                            holder.setMessage(result.getMessage().concat(" " + holder.getMessage()));
                        }

                        //合计执行错误的明细数量
                        totalErrorItemQty = totalErrorItemQty + result.getData().getErrorBillItemQty();

                    }

                } catch (Exception e) {

                    StackTraceElement element = e.getStackTrace()[0];

                    String errorMsg = Resources.getMessage(
                            "实体仓库存变动消息处理失败！",
                            loginUser.getLocale(), billUniqueKey, e.getMessage(), element.getFileName(), element.getClassName(),
                            element.getMethodName(), element.getLineNumber());

                    log.error("失败处理流水号：{} 错误信息:{} 详情：{}",
                            billUniqueKey, e.getMessage(), errorMsg);
                    holder.setCode(ResultCode.FAIL);
                    holder.setMessage(errorMsg.concat(" " + holder.getMessage()));

                    //递增执行错误的明细数量（异常时明细错误数量暂时不精准）
                    totalErrorItemQty++;
                }

                /* TODO MQ消息体暂时不存入Redis中 */

            }

            if (totalErrorItemQty > 0) {

                if (log.isDebugEnabled()) {
                    log.debug("Finish SgPhyStorageMQBatchUpdateService.updatePhyStorageBatch. ReturnResult:fail messageKey:{} spend time:{}ms;"
                            , messageKey, System.currentTimeMillis() - startTime);
                }

                holder.setCode(ResultCode.FAIL);
                holder.setMessage(Resources.getMessage("实体仓库存变动消息处理部分失败！", loginUser.getLocale(), totalErrorItemQty)
                        + holder.getMessage());
                redisTemplate.delete(messageUniqueKey);
                return holder;
            } else {
                holder.setCode(ResultCode.SUCCESS);
                holder.setMessage(Resources.getMessage("实体仓库存变动消息已成功处理完毕！", loginUser.getLocale()));
            }

            //执行成功的情况下，设置锁的过期时间
            redisTemplate.opsForValue().set(messageUniqueKey, SgConstants.MESSAGE_STATUS_SUCCESS, 1, TimeUnit.DAYS);

            if (log.isDebugEnabled()) {
                log.debug("Finish SgPhyStorageMQBatchUpdateService.updatePhyStorageBatch. ReturnResult:success messageKey:{} spend time:{}ms;"
                        , messageKey, System.currentTimeMillis() - startTime);
            }


        } else {
            //redis无法正常获取锁的情况下
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("实体仓库存变动消息处理部分失败！原因：系统redis发生错误！", loginUser.getLocale()));
        }

        return holder;
    }

    /**
     * @param request
     * @param loginUser
     * @return
     */
    private ValueHolderV14<SgPhyStorageBatchUpdateResult> checkServiceParam(SgPhyStorageBatchUpdateRequest request,
                                                                            User loginUser) {

        ValueHolderV14<SgPhyStorageBatchUpdateResult> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");

        if (request == null) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("请求体不能为空！", loginUser.getLocale()));
            return holder;
        }

        if (StringUtils.isEmpty(request.getMessageKey())) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("消息Key不能为空！", loginUser.getLocale()));
        }

        if (CollectionUtils.isEmpty(request.getBillList())) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("实体仓库存更新单据不能为空！", loginUser.getLocale()));
        }

        return holder;

    }
}
