package com.jackrain.nea.sg.basic.model.result;

import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.model.SgStorageUpdateCommonModel;
import com.jackrain.nea.sg.basic.model.SgTeusStorageUpdateCommonModel;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/3/17 14:50
 */
@Data
public class SgStorageBatchUpdateResult implements Serializable {

    /*
     * 错误明细件数（包含发生逻辑错误明细+XactLock超过最大执行数的明细）
     */
    private long errorBillItemQty;
    /*
     * 占用更新结果
     */
    private int preoutUpdateResult;
    /*
     * Redis更新流水键
     */
    private List<String> redisBillFtpKeyList;
    /*
     *缺货明细列表
     */
    private List<SgStorageUpdateCommonModel> outStockItemList;
    /*
     *缺货箱明细列表
     */
    private List<SgTeusStorageUpdateCommonModel> outStockTeusList;
    /*
     *渠道库存同步MQ列表
     */
    private List<SgStorageUpdateCommonModel> channelMqItemList;
    /*
     *逻辑仓库存同步MQ列表
     */
    private List<SgStorageUpdateCommonModel> redisSynchMqItemList;

    public SgStorageBatchUpdateResult() {
        this.errorBillItemQty = 0;
        this.preoutUpdateResult = SgConstantsIF.PREOUT_RESULT_SUCCESS;
    }
}
