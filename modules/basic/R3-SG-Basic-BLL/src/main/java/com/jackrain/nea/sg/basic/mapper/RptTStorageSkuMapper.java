package com.jackrain.nea.sg.basic.mapper;

import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.basic.model.result.SgRcStorageResult;
import com.jackrain.nea.sg.basic.model.table.RptTStorageSku;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.mapping.StatementType;

import java.util.HashMap;


@Mapper
public interface RptTStorageSkuMapper extends ExtentionMapper<RptTStorageSku> {
    @Select("{CALL ${schema}.fun_rpt_storage_sku (#{sgRcStorageResult.rptDate,jdbcType=VARCHAR}," +
            "#{sgRcStorageResult.storeCode,jdbcType=VARCHAR},#{sgRcStorageResult.brandId,jdbcType=INTEGER},#{sgRcStorageResult.proyearId,jdbcType=INTEGER}," +
            "#{sgRcStorageResult.proseaId,jdbcType=INTEGER},#{sgRcStorageResult.materieltype,jdbcType=VARCHAR},#{sgRcStorageResult.largeseriesId,jdbcType=INTEGER}," +
            "#{sgRcStorageResult.jacketBottom,jdbcType=VARCHAR},#{sgRcStorageResult.smallseriesId,jdbcType=INTEGER},#{sgRcStorageResult.smallcateId,jdbcType=INTEGER}," +
            "#{sgRcStorageResult.proEcode,jdbcType=VARCHAR},#{sgRcStorageResult.sexId,jdbcType=INTEGER},#{sgRcStorageResult.clrs,jdbcType=VARCHAR}," +
            "#{sgRcStorageResult.skuCode,jdbcType=VARCHAR},#{sgRcStorageResult.gbcode,jdbcType=VARCHAR})}")
    @Options(statementType = StatementType.CALLABLE)
    HashMap generateBatchNo(@Param("schema") String schema,@Param("sgRcStorageResult") SgRcStorageResult sgRcStorageResult);
}