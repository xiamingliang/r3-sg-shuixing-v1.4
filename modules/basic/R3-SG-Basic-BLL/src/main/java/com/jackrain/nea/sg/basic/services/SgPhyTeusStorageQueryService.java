package com.jackrain.nea.sg.basic.services;

import com.jackrain.nea.sg.basic.logic.SgPhyTeusStorageQueryLogic;
import com.jackrain.nea.sg.basic.model.request.SgTeusPhyStorageQueryRequest;
import com.jackrain.nea.sg.basic.model.table.SgBPhyTeusStorage;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author zhu lin yu
 * @since 2019-11-01
 * create at : 2019-11-01 10:31
 */
@Component
@Slf4j
public class SgPhyTeusStorageQueryService {

    @Autowired
    private SgPhyTeusStorageQueryLogic sgPhyTeusStorageQueryLogic;

    public ValueHolderV14<List<SgBPhyTeusStorage>> queryTeusPhyStorage(SgTeusPhyStorageQueryRequest request, User loginUser) {
        return sgPhyTeusStorageQueryLogic.queryPhyTeusStorage(request,null,loginUser);
    }
}
