package com.jackrain.nea.sg.basic.config;

import com.jackrain.nea.model.util.AdParamUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author: 舒威
 * @since: 2019/10/28
 * create at : 2019/10/28 19:48
 */
@Slf4j
@Component
public class SgStorageBoxConfig {
    public Boolean getBoxEnable() {
        return Boolean.valueOf(AdParamUtil.getParam("r3.box.enable"));
    }
}
