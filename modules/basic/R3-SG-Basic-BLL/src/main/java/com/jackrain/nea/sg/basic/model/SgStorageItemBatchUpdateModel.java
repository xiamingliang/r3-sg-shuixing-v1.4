package com.jackrain.nea.sg.basic.model;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/3/17 13:23
 */
@Data
public class SgStorageItemBatchUpdateModel implements Serializable {

    //kafka消息键
    private String MessageKey;

    /**
     * 明细信息
     */
    List<SgStorageUpdateCommonModel> itemList;

}
