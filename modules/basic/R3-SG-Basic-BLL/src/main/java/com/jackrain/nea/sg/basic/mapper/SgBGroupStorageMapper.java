package com.jackrain.nea.sg.basic.mapper;

import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.basic.model.table.SgBGroupStorage;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SgBGroupStorageMapper extends ExtentionMapper<SgBGroupStorage> {
}