package com.jackrain.nea.sg.basic.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.redis.config.CusRedisTemplate;
import com.jackrain.nea.redis.util.RedisOpsUtil;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.config.SgStorageControlConfig;
import com.jackrain.nea.sg.basic.mapper.SgBStorageMapper;
import com.jackrain.nea.sg.basic.model.request.SgStorageRedisInitRequest;
import com.jackrain.nea.sg.basic.model.result.SgStorageRedisInitResult;
import com.jackrain.nea.sg.basic.model.table.SgBStorage;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description: 通用逻辑仓库存初始化接口(Redis)
 * @Author: chenb
 * @Date: 2019/7/29 11:32
 */
@Component
@Slf4j
public class SgStorageRedisInitService {

    @Autowired
    SgStorageControlConfig sgStorageControlConfig;

    @Autowired
    private SgBStorageMapper sgBStorageMapper;

    public ValueHolderV14<SgStorageRedisInitResult> initRedisStorage(SgStorageRedisInitRequest request) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgStorageRedisInitService.initRedisStorage. ReceiveParams:SgStorageRedisInitRequest={};",
                    JSONObject.toJSONString(request));
        }

        ValueHolderV14<SgStorageRedisInitResult> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        long totalSuccessNum = 0;

        holder = checkServiceParam(request, request.getLoginUser());

        if (ResultCode.FAIL == holder.getCode()) {
            return holder;
        }

        User loginUser = request.getLoginUser();

        CusRedisTemplate<String, String> redisTemplate = RedisOpsUtil.getStrRedisTemplate();
        DefaultRedisScript<List> redisScript = new DefaultRedisScript<>();
        redisScript.setLocation(new ClassPathResource("lua/InitRedisStorage.lua"));
        redisScript.setResultType(List.class);
        List<String> keyList;
        List<String> qtyList;

        List<Long> storeIds = request.getStoreIds();

        if (CollectionUtils.isEmpty(storeIds)) {
            storeIds = sgBStorageMapper.selectStorageStoreIds();
        }

        List<Long> proIds = request.getProIds();
        List<Long> skuIds = request.getSkuIds();
        List<String> proEcodes = request.getProEcodes();
        List<String> skuEcodes = request.getSkuEcodes();

        //获取满足条件的逻辑仓库存件数
        int totalQtty = sgBStorageMapper.selectCount(new QueryWrapper<SgBStorage>().lambda()
                .in(!CollectionUtils.isEmpty(storeIds), SgBStorage::getCpCStoreId, storeIds)
                .in(!CollectionUtils.isEmpty(proIds), SgBStorage::getPsCProId, proIds)
                .in(!CollectionUtils.isEmpty(skuIds), SgBStorage::getPsCSkuId, skuIds)
                .in(!CollectionUtils.isEmpty(proEcodes), SgBStorage::getPsCProEcode, proEcodes)
                .in(!CollectionUtils.isEmpty(skuEcodes), SgBStorage::getPsCSkuEcode, skuEcodes)
        );

        //分页处理
        int pageSize = sgStorageControlConfig.getMaxQueryLimit();
        int listSize = totalQtty;
        int page = listSize / pageSize;

        if (listSize % pageSize != 0) {
            page++;
        }

        //分页批量更新
        for (int i = 0; i < page; i++) {

            /** 分页查询 **/
            PageHelper.startPage(i + 1, pageSize);
            List<SgBStorage> resultList = sgBStorageMapper.selectList(
                    new QueryWrapper<SgBStorage>().lambda().select(SgBStorage::getCpCStoreId,
                            SgBStorage::getPsCSkuId, SgBStorage::getQtyPreout, SgBStorage::getQtyPrein,
                            SgBStorage::getQtyStorage)
                            .in(!CollectionUtils.isEmpty(storeIds), SgBStorage::getCpCStoreId, storeIds)
                            .in(!CollectionUtils.isEmpty(proIds), SgBStorage::getPsCProId, proIds)
                            .in(!CollectionUtils.isEmpty(skuIds), SgBStorage::getPsCSkuId, skuIds)
                            .in(!CollectionUtils.isEmpty(proEcodes), SgBStorage::getPsCProEcode, proEcodes)
                            .in(!CollectionUtils.isEmpty(skuEcodes), SgBStorage::getPsCSkuEcode, skuEcodes)
            );

            keyList = new ArrayList();
            qtyList = new ArrayList();

            if (!CollectionUtils.isEmpty(resultList)) {

                for (SgBStorage sgBStorage : resultList) {

                    //LUA Redis键：sg:storage:逻辑仓ID:SKUID
                    keyList.add(SgConstants.REDIS_KEY_PREFIX_STORAGE + sgBStorage.getCpCStoreId() +
                            ":" + sgBStorage.getPsCSkuId());

                    //LUA参数:占用数量,在途数量,在库数量
                    qtyList.add(sgBStorage.getQtyPreout() + "," +
                            sgBStorage.getQtyPrein() + "," +
                            sgBStorage.getQtyStorage());
                }

            }

            if (log.isDebugEnabled()) {
                log.debug("SgStorageRedisInitService.initRedisStorage. redisTemplate.execute.start. ReceiveParams:resultList:{};"
                        , JSONObject.toJSONString(resultList));
            }

            //调用逻辑仓库存初始化LUA脚本
            List result = redisTemplate.execute(redisScript, keyList, qtyList.toArray(new String[qtyList.size()]));

            if (log.isDebugEnabled()) {
                log.debug("SgStorageRedisInitService.initRedisStorage. redisTemplate.execute.end. ReturnResult:result:{};"
                        , JSONObject.toJSONString(result));
            }

            if (!CollectionUtils.isEmpty(result) && (Long) result.get(0) != ResultCode.SUCCESS) {

                log.error("SgStorageRedisInitService.initRedisStorage. Redis逻辑仓库存初始化失败！成功件数:{};"
                        , JSONObject.toJSONString(result.get(1)));

            }

            totalSuccessNum = totalSuccessNum + (Long) result.get(1);

            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }

        holder.setMessage(Resources.getMessage("Redis逻辑仓库存初始化成功！",
                loginUser.getLocale(),
                totalSuccessNum));

        return holder;
    }

    /**
     * @param request
     * @param loginUser
     * @return ValueHolderV14
     */
    private ValueHolderV14 checkServiceParam(SgStorageRedisInitRequest request,
                                             User loginUser) {

        ValueHolderV14 holder = new ValueHolderV14(ResultCode.SUCCESS, "");

        int maxQueryLimit = sgStorageControlConfig.getMaxQueryLimit();

        List<Long> proIds = request.getProIds();
        List<Long> skuIds = request.getSkuIds();
        List<String> proEcodes = request.getProEcodes();
        List<String> skuEcodes = request.getSkuEcodes();

        if (!CollectionUtils.isEmpty(proIds) &&
                proIds.size() > maxQueryLimit) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("商品编码ID的查询件数超过最大限制！",
                    loginUser.getLocale()));
        } else if (!CollectionUtils.isEmpty(skuIds) &&
                skuIds.size() > maxQueryLimit) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("商品条码ID的查询件数超过最大限制！",
                    loginUser.getLocale()));
        } else if (!CollectionUtils.isEmpty(proEcodes) &&
                proEcodes.size() > maxQueryLimit) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("商品编码的查询件数超过最大限制！",
                    loginUser.getLocale()));
        } else if (!CollectionUtils.isEmpty(skuEcodes) &&
                skuEcodes.size() > maxQueryLimit) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("商品条码的查询件数超过最大限制！",
                    loginUser.getLocale()));
        }

        return holder;
    }
}
