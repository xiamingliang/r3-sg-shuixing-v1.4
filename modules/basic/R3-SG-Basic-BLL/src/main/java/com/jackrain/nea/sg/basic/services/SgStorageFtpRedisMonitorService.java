package com.jackrain.nea.sg.basic.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.redis.config.CusRedisTemplate;
import com.jackrain.nea.redis.util.RedisOpsUtil;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.config.SgStorageControlConfig;
import com.jackrain.nea.sg.basic.mapper.SgBStorageChangeFtpMapper;
import com.jackrain.nea.sg.basic.mapper.SgBStoragePreinFtpMapper;
import com.jackrain.nea.sg.basic.mapper.SgBStoragePreoutFtpMapper;
import com.jackrain.nea.sg.basic.model.request.SgStorageFtpRedisMonitorRequest;
import com.jackrain.nea.sg.basic.model.result.SgStorageFtpRedisMonitorResult;
import com.jackrain.nea.sg.basic.model.table.SgBStorageChangeFtp;
import com.jackrain.nea.sg.basic.model.table.SgBStoragePreinFtp;
import com.jackrain.nea.sg.basic.model.table.SgBStoragePreoutFtp;
import com.jackrain.nea.sg.basic.utils.StorageUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Set;

/**
 * @Description: 通用逻辑仓库流水存监控接口(Redis)
 * @Author: chenb
 * @Date: 2019/7/29 11:32
 */
@Component
@Slf4j
public class SgStorageFtpRedisMonitorService {

    @Autowired
    SgStorageControlConfig sgStorageControlConfig;

    @Autowired
    private SgBStorageChangeFtpMapper sgBStorageChangeFtpMapper;

    @Autowired
    private SgBStoragePreoutFtpMapper sgBStoragePreoutFtpMapper;

    @Autowired
    private SgBStoragePreinFtpMapper sgBStoragePreinFtpMapper;


    public ValueHolderV14<SgStorageFtpRedisMonitorResult> monitorRedisStorageFtp(SgStorageFtpRedisMonitorRequest request) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgStorageFtpRedisMonitorService.monitorRedisStorageFtp. ReceiveParams:request={};",
                    JSONObject.toJSONString(request));
        }

        long startTime = System.currentTimeMillis();

        ValueHolderV14<SgStorageFtpRedisMonitorResult> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        CusRedisTemplate<String, String> redisTemplate = RedisOpsUtil.getStrRedisTemplate();
        Set<Object> storageRedisKeys;
        Map<Object, Object> storageFtp;

        long totalSuccessNum = 0;
        long totalErrorNum = 0;

        User loginUser = request.getLoginUser();

        storageRedisKeys = StorageUtils.scanRedisKeys(
                SgConstants.REDIS_KEY_PREFIX_STORAGE_FTP +
                        sdf.format(DateUtils.addHours(new Date(), -1)),
                1000);

        for (Object storeRedisKey : storageRedisKeys) {

            storageFtp = redisTemplate.opsForHash().entries((String) storeRedisKey);

            for (Object storageFtpItem : storageFtp.values()) {

                String[] storageFtpParam = ((String) storageFtpItem).split(",");
                String[] storageFtpKey = storageFtpParam[0].split(":");
                String[] storageFtpIds = storageFtpParam[4].split("_");

                BigDecimal preoutNum = new BigDecimal(storageFtpParam[1]);
                BigDecimal preinNum = new BigDecimal(storageFtpParam[2]);
                BigDecimal storageNum = new BigDecimal(storageFtpParam[3]);

                if (preoutNum.compareTo(BigDecimal.ZERO) != 0) {
                    if (sgBStoragePreoutFtpMapper.selectCount(new QueryWrapper<SgBStoragePreoutFtp>().lambda()
                            .eq(SgBStoragePreoutFtp::getReserveVarchar01, storeRedisKey)
                            .eq(SgBStoragePreoutFtp::getBillId, Integer.valueOf(storageFtpIds[0]))
                            .eq(SgBStoragePreoutFtp::getBillItemId, Integer.valueOf(storageFtpIds[1]))
                            .eq(SgBStoragePreoutFtp::getCpCStoreId, Integer.valueOf(storageFtpKey[2]))
                            .eq(SgBStoragePreoutFtp::getPsCSkuId, Integer.valueOf(storageFtpKey[3]))
                            .eq(SgBStoragePreoutFtp::getQtyChange, preoutNum)) != 1) {

                        log.error("SgStorageFtpRedisMonitorService.monitorRedisStorageFtp. error :" +
                                        "逻辑仓占用库存流水记录缺失。单据ID, 明细ID, 逻辑仓ID:{}, 商品条码ID:{}, 变动数量{};",
                                storageFtpIds[0], storageFtpIds[1], storageFtpKey[2], storageFtpKey[3], preoutNum);

                        totalErrorNum++;
                    } else {
                        totalSuccessNum++;
                    }
                }

                if (preinNum.compareTo(BigDecimal.ZERO) != 0) {
                    if (sgBStoragePreinFtpMapper.selectCount(new QueryWrapper<SgBStoragePreinFtp>().lambda()
                            .eq(SgBStoragePreinFtp::getReserveVarchar01, storeRedisKey)
                            .eq(SgBStoragePreinFtp::getBillId, Integer.valueOf(storageFtpIds[0]))
                            .eq(SgBStoragePreinFtp::getBillItemId, Integer.valueOf(storageFtpIds[1]))
                            .eq(SgBStoragePreinFtp::getCpCStoreId, Integer.valueOf(storageFtpKey[2]))
                            .eq(SgBStoragePreinFtp::getPsCSkuId, Integer.valueOf(storageFtpKey[3]))
                            .eq(SgBStoragePreinFtp::getQtyChange, preinNum)) != 1) {

                        log.error("SgStorageFtpRedisMonitorService.monitorRedisStorageFtp. error :" +
                                        "逻辑仓在途库存流水记录缺失。单据ID, 明细ID, 逻辑仓ID:{}, 商品条码ID:{}, 变动数量{};",
                                storageFtpIds[0], storageFtpIds[1], storageFtpKey[2], storageFtpKey[3], preinNum);

                        totalErrorNum++;

                    } else {
                        totalSuccessNum++;
                    }
                }

                if (storageNum.compareTo(BigDecimal.ZERO) != 0) {
                    if (sgBStorageChangeFtpMapper.selectCount(new QueryWrapper<SgBStorageChangeFtp>().lambda()
                            .eq(SgBStorageChangeFtp::getReserveVarchar01, storeRedisKey)
                            .eq(SgBStorageChangeFtp::getBillId, Integer.valueOf(storageFtpIds[0]))
                            .eq(SgBStorageChangeFtp::getBillItemId, Integer.valueOf(storageFtpIds[1]))
                            .eq(SgBStorageChangeFtp::getCpCStoreId, Integer.valueOf(storageFtpKey[2]))
                            .eq(SgBStorageChangeFtp::getPsCSkuId, Integer.valueOf(storageFtpKey[3]))
                            .eq(SgBStorageChangeFtp::getQtyChange, storageNum)) != 1) {

                        log.error("SgStorageFtpRedisMonitorService.monitorRedisStorageFtp. error :" +
                                        "逻辑仓在库库存流水记录缺失。单据ID, 明细ID, 逻辑仓ID:{}, 商品条码ID:{}, 变动数量{};",
                                storageFtpIds[0], storageFtpIds[1], storageFtpKey[2], storageFtpKey[3], storageNum);

                        totalErrorNum++;
                    } else {
                        totalSuccessNum++;
                    }
                }
            }
        }

        holder.setMessage(Resources.getMessage("Redis逻辑仓库存监控执行成功！",
                loginUser.getLocale(),
                totalSuccessNum, totalErrorNum));

        if (log.isDebugEnabled()) {
            log.debug("Finish SgStorageFtpRedisMonitorService.monitorRedisStorageFtp. ReturnResult:holder={} spend time:{}ms;",
                    JSONObject.toJSONString(holder), System.currentTimeMillis() - startTime);
        }

        return holder;
    }

}
