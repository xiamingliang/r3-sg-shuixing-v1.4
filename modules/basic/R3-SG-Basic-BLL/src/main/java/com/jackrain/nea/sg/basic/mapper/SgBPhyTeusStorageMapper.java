package com.jackrain.nea.sg.basic.mapper;

import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.basic.model.SgPhyTeusStorageUpdateCommonModel;
import com.jackrain.nea.sg.basic.model.table.SgBPhyTeusStorage;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface SgBPhyTeusStorageMapper extends ExtentionMapper<SgBPhyTeusStorage> {

    /**
     * 更新箱在库量
     *
     * @param model 更新信息
     */
    @Update("<script>" +
            "update sg_b_phy_teus_storage  " +
            "set qty_storage = qty_storage + #{model.qtyChange}," +
            "modifieddate=#{model.modifieddate,jdbcType=TIMESTAMP}," +
            "modifiername=#{model.modifiername}, " +
            "modifierename=#{model.modifierename}," +
            "modifierid=#{model.modifierid} " +
            "where ps_c_teus_id = #{model.psCTeusId} and cp_c_phy_warehouse_id = #{model.cpCPhyWarehouseId} " +
            "<if test = 'blncheckNegative == true' >" +
            "and qty_storage + #{model.qtyChange} >= 0" +
            "</if>"  +
            "</script>")
    Integer updateQtyChange(@Param("model") SgPhyTeusStorageUpdateCommonModel model,
                            @Param("blncheckNegative") boolean blncheckNegative);

    /**
     * 获取库存查询表逻辑仓ID
     *
     */
    @Select("select distinct cp_c_phy_warehouse_id from sg_b_phy_teus_storage")
    List<Long> selectPhyStoragePhyWarehouseIds();
}