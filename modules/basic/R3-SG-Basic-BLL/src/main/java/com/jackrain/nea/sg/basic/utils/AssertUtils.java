package com.jackrain.nea.sg.basic.utils;

import com.jackrain.nea.config.Resources;
import com.jackrain.nea.exception.NDSException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Locale;

@Slf4j
public class AssertUtils {

    public static void notNull(Object obj, String msg, Locale locale) {
        if (obj == null) {
            logAndThrow(msg, locale);
        }
    }

    public static void notNull(Object obj, String msg) {
        if (obj == null) {
            logAndThrow(msg);
        }
    }

    public static void notLessThanZero(Long num, String msg, Locale locale) {
        if (num != null && num < 0) {
            logAndThrow(msg, locale);
            return;

        }
    }

    public static void notLessThanZero(Long num, String msg) {
        if (num != null && num < 0) {
            logAndThrow(msg);
            return;
        }
    }

    public static void notLessThanZero(BigDecimal num, String msg) {
        if (num != null && num.compareTo(BigDecimal.ZERO) < 0) {
            logAndThrow(msg);
            return;
        }
    }


    public static void logAndThrow(String msg, Locale locale) {
        NDSException ndsException = new NDSException(Resources.getMessage(msg, locale));
        log.error(msg, ndsException);
        throw ndsException;
    }

    public static void logAndThrow(String msg) {
        NDSException ndsException = new NDSException(Resources.getMessage(msg));
        log.error(msg, ndsException);
        throw ndsException;
    }

    public static void isTrue(boolean b, String msg, Locale locale) {
        if (!b) {
            logAndThrow(msg, locale);
        }
    }

    public static void isTrue(boolean b, String msg) {
        if (!b) {
            logAndThrow(msg);
        }
    }

    public static void cannot(boolean b, String msg, Locale locale) {
        if (b) {
            logAndThrow(msg, locale);
        }
    }

    public static void cannot(boolean b, String msg) {
        if (b) {
            logAndThrow(msg);
        }
    }

    public static void notBlank(String str, String msg) {
        if (StringUtils.isBlank(str)) {
            logAndThrow(msg);
        }
    }

    public static void isNotEmpty(String msg, Collection list) {
        if (list == null || list.isEmpty()) {
            logAndThrow(msg);
        }
    }

    public static void notEmpty(Collection list, String msg) {
        if (CollectionUtils.isEmpty(list)) {
            logAndThrow(msg);
        }
    }

    public static void logAndThrowExtra(Exception e, Locale locale) {
        if (!(e instanceof NDSException)) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            String message = sw.toString().replace("\n", "<br/>").replace("\tat", "");
            logAndThrow(message, locale);
        } else {
            logAndThrow(e.getMessage(), locale);
        }
    }

    public static void logAndThrowExtra(String msg, Exception e) {
        if (!(e instanceof NDSException)) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            String message = sw.toString().replace("\n", "<br/>").replace("\tat", "");
            logAndThrow(msg + message);
        } else {
            logAndThrow(msg + e.getMessage());
        }
    }

    public static void logAndThrowExtra(String msg, Exception e, Locale locale) {
        if (!(e instanceof NDSException)) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            String message = sw.toString().replace("\n", "<br/>").replace("\tat", "");
            logAndThrow(msg + message, locale);
        } else {
            logAndThrow(msg + e.getMessage(), locale);
        }
    }

    public static String getMessageExtra(String msg, Exception e) {
        String message = "";
        if (!(e instanceof NDSException)) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            message = sw.toString().replace("\n", "<br/>").replace("\tat", "");
        } else {
            message = msg + e.getMessage();
        }
        return message;
    }

    public static String getWMSErrMessage(Exception e, String extendMessage) {

        StackTraceElement element = e.getStackTrace()[0];

        String errorMsg = Resources.getMessage(
                "该单据新增通知单并传WMS失败！",
                extendMessage, e.getMessage(), element.getFileName(), element.getClassName(),
                element.getMethodName(), element.getLineNumber());

        log.error("关键字:{}失败处理 错误信息:{} 详情：{}",
                extendMessage, e.getMessage(), errorMsg);

        return errorMsg;
    }
}
