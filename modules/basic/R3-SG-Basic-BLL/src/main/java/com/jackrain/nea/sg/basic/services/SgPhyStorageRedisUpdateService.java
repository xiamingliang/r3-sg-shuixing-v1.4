package com.jackrain.nea.sg.basic.services;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.redis.config.CusRedisTemplate;
import com.jackrain.nea.redis.util.RedisOpsUtil;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.config.SgStorageControlConfig;
import com.jackrain.nea.sg.basic.model.SgPhyStorageUpdateCommonModel;
import com.jackrain.nea.sg.basic.model.SgStorageUpdateControlModel;
import com.jackrain.nea.sg.basic.model.request.SgPhyStorageSingleUpdateRequest;
import com.jackrain.nea.sg.basic.model.request.SgPhyStorageUpdateBillItemRequest;
import com.jackrain.nea.sg.basic.model.request.SgPhyStorageUpdateBillRequest;
import com.jackrain.nea.sg.basic.model.result.SgPhyStorageBatchUpdateResult;
import com.jackrain.nea.sg.basic.utils.StorageLogUtils;
import com.jackrain.nea.sg.basic.utils.StorageUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.stereotype.Component;
import org.thymeleaf.util.StringUtils;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/7/10 00:00
 */
@Component
@Slf4j
public class SgPhyStorageRedisUpdateService {

    @Autowired
    SgStorageControlConfig sgStorageControlConfig;

    /**
     * 通用单个单据库存更新接口
     *
     * @param request
     * @return
     */
    public ValueHolderV14<SgPhyStorageBatchUpdateResult> updatePhyStorageBill(SgPhyStorageSingleUpdateRequest request) {

        long startTime = System.currentTimeMillis();

        if (log.isDebugEnabled()) {
            log.debug("Start SgPhyStorageRedisUpdateService.updatePhyStorageBill. ReceiveParams:request:{};"
                    , JSONObject.toJSONString(request.getBill()));
        }

        ValueHolderV14<SgPhyStorageBatchUpdateResult> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        SgPhyStorageBatchUpdateResult updateResult = new SgPhyStorageBatchUpdateResult();
        Map<String, SgPhyStorageUpdateCommonModel> redisSynchMqItemMap = new HashMap<>();
        Map<String, SgPhyStorageUpdateCommonModel> outStockItemMap = new HashMap<>();
        List<SgPhyStorageUpdateCommonModel> outStockItemList = new ArrayList<>();
        List<SgPhyStorageUpdateCommonModel> redisSynchMqItemList = new ArrayList<>();
        List<String> batchUpdateRedisKeyList = new ArrayList<>();
        List<String> batchUpdateRedisQttyList = new ArrayList<>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        //发生错误的明细数
        long errorItemQty = 0;

        CusRedisTemplate<String, String> redisTemplate = RedisOpsUtil.getStrRedisTemplate();
        DefaultRedisScript<List> redisScript = new DefaultRedisScript<>();
        redisScript.setLocation(new ClassPathResource("lua/UpdateRedisPhyStorage.lua"));
        redisScript.setResultType(List.class);

        //检查入参
        holder = checkServiceParam(request, request.getLoginUser());
        if (ResultCode.FAIL == holder.getCode()) {
            return holder;
        }

        User loginUser = request.getLoginUser();
        SgPhyStorageUpdateBillRequest billInfo = request.getBill();

        if (CollectionUtils.isEmpty(billInfo.getItemList())) {
            return holder;
        }

        //Redis更新流水键
        String redisBillFtpKey = SgConstants.REDIS_KEY_PREFIX_PHY_STORAGE_FTP +
                sdf.format(new Date()) + "_" + billInfo.getBillNo();

        //初始化库存更新控制对象（默认不允许负库存）
        SgStorageUpdateControlModel controlModel = new SgStorageUpdateControlModel();

        //APOLLO系统配置优先级最低
        if (sgStorageControlConfig != null) {
            BeanUtils.copyProperties(sgStorageControlConfig, controlModel);
        }

        //当消费者设置了控制类里的情况下，优先使用消费者的控制类
        if (request.getControlModel() != null) {
            BeanUtils.copyProperties(request.getControlModel(), controlModel);
        }

        if (log.isDebugEnabled()) {
            log.debug("SgPhyStorageRedisUpdateService.updatePhyStorageBill. billInfo.controlModel:{};"
                    + JSONObject.toJSONString(controlModel));
        }

        for (SgPhyStorageUpdateBillItemRequest itemInfo : billInfo.getItemList()) {

            SgStorageUpdateControlModel detailControlModel = new SgStorageUpdateControlModel();
            SgPhyStorageUpdateCommonModel updateCommonModel = new SgPhyStorageUpdateCommonModel();

            if (log.isDebugEnabled()) {
                log.debug("SgPhyStorageRedisUpdateService.updatePhyStorageBill. SgPhyStorageUpdateBillItemRequest:{};"
                        , JSONObject.toJSONString(itemInfo));
            }

            billInfo.setRedisBillFtpKey(redisBillFtpKey);

            StorageUtils.setBModelDefalutData(updateCommonModel, loginUser);
            BeanUtils.copyProperties(billInfo, updateCommonModel);
            BeanUtils.copyProperties(itemInfo, updateCommonModel);

            //单据取消的情况下，相应数量取反
            if (billInfo.getIsCancel()) {
                updateCommonModel.setQtyChange(updateCommonModel.getQtyChange().negate());
            }

            if (updateCommonModel.getControlmodel() == null) {
                detailControlModel = controlModel;
            } else {
                BeanUtils.copyProperties(updateCommonModel.getControlmodel(), detailControlModel);
            }

            //LUA Redis键：sg:phystorage:逻辑仓ID:SKUID
            batchUpdateRedisKeyList.add(SgConstants.REDIS_KEY_PREFIX_PHY_STORAGE +
                    updateCommonModel.getCpCPhyWarehouseId() +
                    ":" + updateCommonModel.getPsCSkuId());

            //LUA参数:在库数量,明细ID,在库是否允许负库存(1：允许 0：不允许)
            batchUpdateRedisQttyList.add(
                    updateCommonModel.getQtyChange() + "," +
                            updateCommonModel.getBillId().toString() + "_" + updateCommonModel.getBillItemId() + "," +
                            (detailControlModel.isNegativeStorage() ? "1" : "0"));

            redisSynchMqItemMap.put(updateCommonModel.getBillId().toString() + "_" + updateCommonModel.getBillItemId().toString(),
                    updateCommonModel);
            outStockItemMap.put(updateCommonModel.getBillId().toString() + "_" + updateCommonModel.getBillItemId().toString(),
                    updateCommonModel);

        }

        //批量更新单据明细分页
        int pageSize = controlModel.getBatchUpdateItems();
        int listSize = batchUpdateRedisKeyList.size();
        int page = listSize / pageSize;

        if (listSize % pageSize != 0) {
            page++;
        }

        boolean setDataFlg = false;

        try {

            //分页批量更新
            for (int i = 0; i < page; i++) {

                List<String> keyList = batchUpdateRedisKeyList.subList(i * pageSize,
                        (((i + 1) * pageSize > listSize ? listSize : pageSize * (i + 1))));

                List<String> qtyList = batchUpdateRedisQttyList.subList(i * pageSize,
                        (((i + 1) * pageSize > listSize ? listSize : pageSize * (i + 1))));

                List<String> qtyListItem = new ArrayList<>();
                qtyListItem.addAll(qtyList);
                qtyListItem.add(redisBillFtpKey);
                qtyListItem.add(billInfo.getIsCancel() ? "1" : "0");

                if (log.isDebugEnabled()) {
                    log.debug("SgPhyStorageRedisUpdateService.updatePhyStorageBill. redisTemplate.execute.start. ReceiveParams:keyList:{}, " +
                                    "qtyList:{};"
                            , JSONObject.toJSONString(keyList), JSONObject.toJSONString(qtyListItem));
                }

                List result = redisTemplate.execute(redisScript, keyList, qtyListItem.toArray(new String[qtyListItem.size()]));

                if (log.isDebugEnabled()) {
                    log.debug("SgPhyStorageRedisUpdateService.updatePhyStorageBill. redisTemplate.execute.end. ReturnResult:result:{};"
                            , JSONObject.toJSONString(result));
                }

                setDataFlg = true;

                if (!CollectionUtils.isEmpty(result) && Integer.valueOf((String) result.get(0)) != ResultCode.SUCCESS) {

                    String negativeStockItem = (String) result.get(1);

                    if (!StringUtils.isEmpty(negativeStockItem)) {

                        if (outStockItemMap.containsKey(negativeStockItem)) {

                            outStockItemList.add(outStockItemMap.get(negativeStockItem));
                            redisSynchMqItemMap.remove(negativeStockItem);

                            SgPhyStorageUpdateCommonModel negativeModel = outStockItemMap.get(negativeStockItem);

                            if (log.isDebugEnabled()) {
                                log.debug("SgPhyStorageRedisUpdateService.updatePhyStorageBill. 当前库存变动明细库存不足！！单据编号:{}，" +
                                                "实体仓ID:{}，商品条码ID:{}，在库变动数量:{}",
                                        negativeModel.getBillNo(), negativeModel.getCpCPhyWarehouseId(),
                                        negativeModel.getPsCSkuId(), negativeModel.getQtyChange());
                            }

                            holder.setMessage(Resources.getMessage("实体仓库存不足！",
                                    loginUser.getLocale(), negativeModel.getCpCPhyWarehouseEcode(), negativeModel.getPsCSkuEcode()));


                        }

                    }

                    holder.setCode(ResultCode.FAIL);
                    errorItemQty = errorItemQty + 1;

                    break;
                }

            }

        } catch (Exception e) {

            String errorMsg = StorageLogUtils.getErrMessage(e, request.getMessageKey(), loginUser);

            List keyList = new ArrayList();
            keyList.add(redisBillFtpKey);
            rollbackPhyStorageBill(keyList);

            holder.setCode(ResultCode.FAIL);
            holder.setData(updateResult);
            holder.setMessage(Resources.getMessage("实体仓库存批量更新失败！", loginUser.getLocale()).concat(errorMsg));

            if (log.isDebugEnabled()) {
                log.debug("Finish SgPhyStorageRedisUpdateService.updatePhyStorageBill. error ReturnResult:errorBillItemQty:{} spend time:{}ms;"
                        , JSONObject.toJSONString(holder.getData() == null ? 0 : holder.getData().getErrorBillItemQty()), System.currentTimeMillis() - startTime);
            }

            return holder;
        }

        if (ResultCode.SUCCESS == holder.getCode() && redisSynchMqItemMap.size() > 0) {

            redisSynchMqItemList.addAll(new ArrayList<>(redisSynchMqItemMap.values()));

        } else if (ResultCode.FAIL == holder.getCode() && setDataFlg) {

            List keyList = new ArrayList();
            keyList.add(redisBillFtpKey);
            rollbackPhyStorageBill(keyList);
        }

        updateResult.setRedisBillFtpKeyList(new ArrayList<>(Arrays.asList(redisBillFtpKey)));
        updateResult.setErrorBillItemQty(errorItemQty);
        updateResult.setOutStockItemList(outStockItemList);
        updateResult.setRedisSynchMqItemList(redisSynchMqItemList);
        holder.setData(updateResult);

        // 没有错误明细的情况下，设置成功运行结果，
        // 有错误明细的情况下，在发生错误的明细中设置运行结果
        if (errorItemQty < 1) {

            holder.setCode(ResultCode.SUCCESS);
            holder.setMessage(Resources.getMessage("当前单据的实体仓库存变动消息处理完毕！",
                    loginUser.getLocale(), holder.getMessage(), billInfo.getBillNo()));

        }

        if (log.isDebugEnabled()) {
            log.debug("Finish SgPhyStorageRedisUpdateService.updatePhyStorageBill. ReturnResult:errorBillItemQty:{} spend time:{}ms;"
                    , JSONObject.toJSONString(holder.getData() == null ? 0 : holder.getData().getErrorBillItemQty()), System.currentTimeMillis() - startTime);
        }

        return holder;
    }

    /**
     * @param request
     * @param loginUser
     * @return
     */
    private ValueHolderV14<SgPhyStorageBatchUpdateResult> checkServiceParam(SgPhyStorageSingleUpdateRequest request,
                                                                            User loginUser) {

        ValueHolderV14<SgPhyStorageBatchUpdateResult> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");

        if (request == null) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("请求体为空！", loginUser.getLocale()));
            return holder;
        }

        if (request.getLoginUser() == null) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("操作用户信息不能为空！", loginUser.getLocale()));
        }

        if (request.getBill() == null || StringUtils.isEmpty(request.getBill().getBillNo())) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("实体仓库存更新单据不能为空！", loginUser.getLocale()));
        }

        if (ResultCode.FAIL == holder.getCode() && log.isDebugEnabled()) {
            log.debug("SgPhyStorageRedisUpdateService.checkServiceParam. checkServiceParam error:"
                    + holder.getMessage() + ";");
        }

        return holder;

    }

    public ValueHolderV14<SgPhyStorageBatchUpdateResult> rollbackPhyStorageBill(List<String> redisBillFtpKeyList) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgPhyStorageRedisUpdateService.rollbackPhyStorageBill. ReceiveParams:redisBillFtpKeyList:{};"
                    , JSONObject.toJSONString(redisBillFtpKeyList));
        }

        ValueHolderV14<SgPhyStorageBatchUpdateResult> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");

        if (CollectionUtils.isEmpty(redisBillFtpKeyList)) {
            return holder;
        }

        CusRedisTemplate<String, String> redisTemplate = RedisOpsUtil.getStrRedisTemplate();
        DefaultRedisScript<List> redisScript = new DefaultRedisScript<>();
        redisScript.setLocation(new ClassPathResource("lua/RollbackRedisPhyStorage.lua"));
        redisScript.setResultType(List.class);

        redisTemplate.execute(redisScript, redisBillFtpKeyList, "");

        return holder;
    }

}
