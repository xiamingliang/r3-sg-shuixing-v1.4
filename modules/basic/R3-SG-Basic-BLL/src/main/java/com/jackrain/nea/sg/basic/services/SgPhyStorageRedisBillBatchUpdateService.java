package com.jackrain.nea.sg.basic.services;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.config.SgStorageBoxConfig;
import com.jackrain.nea.sg.basic.model.SgPhyStorageRedisSynchModel;
import com.jackrain.nea.sg.basic.model.SgPhyStorageUpdateCommonModel;
import com.jackrain.nea.sg.basic.model.SgPhyTeusStorageUpdateCommonModel;
import com.jackrain.nea.sg.basic.model.request.SgPhyStorageBatchUpdateRequest;
import com.jackrain.nea.sg.basic.model.request.SgPhyStorageSingleUpdateRequest;
import com.jackrain.nea.sg.basic.model.result.SgPhyStorageBatchUpdateResult;
import com.jackrain.nea.sg.basic.model.result.SgPhyTeusStorageBatchUpdateResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/6/1 12:51
 */
@Component
@Slf4j
public class SgPhyStorageRedisBillBatchUpdateService {

    @Autowired
    private SgPhyStorageRedisUpdateService stoUpdService;

    @Autowired
    private SgPhyTeusStorageRedisUpdateService teusStoUpdService;

    @Autowired
    private SgPhyStorageRedisSynchService sgPhyStorageRedisSynchService;

    @Autowired
    private SgPhyTeusStorageRedisSynchService sgPhyTeusStorageRedisSynchService;

    @Autowired
    private SgStorageBoxConfig sgStorageBoxConfig;

    public ValueHolderV14<SgPhyStorageBatchUpdateResult> updatePhyStorageBatch(SgPhyStorageBatchUpdateRequest request) {

        ValueHolderV14<SgPhyStorageBatchUpdateResult> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        ValueHolderV14<SgPhyStorageBatchUpdateResult> stoHolder = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        ValueHolderV14<SgPhyTeusStorageBatchUpdateResult> teusHolder = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        SgPhyStorageBatchUpdateResult updateResult = new SgPhyStorageBatchUpdateResult();
        List<SgPhyStorageRedisSynchModel> sgPhyStorageRedisSynchList = new ArrayList<>();
        List<String> redisBillFtpKeyList = new ArrayList<>();
        List<SgPhyStorageUpdateCommonModel> outStockItemList = new ArrayList<>();
        List<SgPhyTeusStorageUpdateCommonModel> outStockTeusList = new ArrayList<>();
        boolean rollbackFlg = false;

        //初始化变量
        long totalErrorItemQty = 0;

        if (request == null || CollectionUtils.isEmpty(request.getBillList())) {
            holder.setData(updateResult);
            return holder;
        }

        if (log.isDebugEnabled()) {
            log.debug("Start SgPhyStorageRedisBillBatchUpdateService.updatePhyStorageBatch. ReceiveParams:billList size:{};"
                    , JSONObject.toJSONString(request.getBillList().size()));
        }

        try {

            //循环遍历MQ消息体中单据信息，调用通用单个单据库存更新接口
            for (int billIndex = 0; billIndex < request.getBillList().size(); billIndex++) {

                SgPhyStorageRedisSynchModel synchModel = new SgPhyStorageRedisSynchModel();

                SgPhyStorageSingleUpdateRequest itemRequest = new SgPhyStorageSingleUpdateRequest();

                itemRequest.setLoginUser(request.getLoginUser());
                itemRequest.setMessageKey(request.getMessageKey());
                itemRequest.setControlModel(request.getControlModel());
                itemRequest.setBill(request.getBillList().get(billIndex));

                synchModel.setSingleRequest(itemRequest);

                stoHolder = stoUpdService.updatePhyStorageBill(itemRequest);

                if (stoHolder != null && ResultCode.SUCCESS != stoHolder.getCode()) {

                    holder.setCode(ResultCode.FAIL);
                    holder.setMessage(stoHolder.getMessage().concat(" " + holder.getMessage()));

                    rollbackFlg = true;

                    break;

                } else if (stoHolder != null && stoHolder.getData() != null) {

                    totalErrorItemQty = totalErrorItemQty + stoHolder.getData().getErrorBillItemQty();

                    synchModel.setSgPhyStorageUpdateCommonList(stoHolder.getData().getRedisSynchMqItemList());

                    if (!CollectionUtils.isEmpty(stoHolder.getData().getOutStockItemList())) {
                        outStockItemList.addAll(stoHolder.getData().getOutStockItemList());
                    }

                    redisBillFtpKeyList.addAll(stoHolder.getData().getRedisBillFtpKeyList());

                }

                //调用单个单据箱库存更新接口
                if (sgStorageBoxConfig.getBoxEnable() &&
                        (itemRequest.getBill() != null && !CollectionUtils.isEmpty(itemRequest.getBill().getTeusList()))) {

                    teusHolder = teusStoUpdService.updatePhyStorageBill(itemRequest);

                    if (teusHolder != null && ResultCode.SUCCESS != teusHolder.getCode()) {

                        holder.setCode(ResultCode.FAIL);
                        holder.setMessage(teusHolder.getMessage().concat(" " + holder.getMessage()));

                        rollbackFlg = true;

                        break;

                    } else if (teusHolder != null && teusHolder.getData() != null) {

                        totalErrorItemQty = totalErrorItemQty + teusHolder.getData().getErrorBillItemQty();

                        synchModel.setSgPhyTeusStorageUpdateCommonList(teusHolder.getData().getRedisSynchMqItemList());

                        if (!CollectionUtils.isEmpty(teusHolder.getData().getOutStockItemList())) {
                            outStockTeusList.addAll(teusHolder.getData().getOutStockItemList());
                        }

                        redisBillFtpKeyList.addAll(teusHolder.getData().getRedisBillFtpKeyList());

                    }

                }

                sgPhyStorageRedisSynchList.add(synchModel);

            }

            if (rollbackFlg) {

                stoUpdService.rollbackPhyStorageBill(redisBillFtpKeyList);

            } else {

                for (SgPhyStorageRedisSynchModel synchModel:sgPhyStorageRedisSynchList) {

                    if (!CollectionUtils.isEmpty(synchModel.getSgPhyStorageUpdateCommonList())) {
                        sgPhyStorageRedisSynchService.synchRedisToPhyStorage(synchModel.getSingleRequest(),
                                synchModel.getSgPhyStorageUpdateCommonList());
                    }

                    if (!CollectionUtils.isEmpty(synchModel.getSgPhyTeusStorageUpdateCommonList())) {
                        sgPhyTeusStorageRedisSynchService.synchRedisToPhyStorage(synchModel.getSingleRequest(),
                                synchModel.getSgPhyTeusStorageUpdateCommonList());
                    }
                }

            }

            updateResult.setErrorBillItemQty(totalErrorItemQty);
            updateResult.setRedisBillFtpKeyList(redisBillFtpKeyList);
            updateResult.setOutStockItemList(outStockItemList);
            updateResult.setOutStockTeusList(outStockTeusList);

            holder.setData(updateResult);

        } catch (Exception e) {

            stoUpdService.rollbackPhyStorageBill(redisBillFtpKeyList);

            holder.setCode(ResultCode.FAIL);
            holder.setData(updateResult);
            holder.setMessage(e.getMessage().concat(" " + holder.getMessage()));

        }

        if (log.isDebugEnabled()) {
            log.debug("Finish SgPhyStorageRedisBillBatchUpdateService.updatePhyStorageBatch. ReturnResult:errorBillItemQty:{};"
                    , holder.getData() == null ? 0 : holder.getData().getErrorBillItemQty());
        }

        return holder;

    }

    public ValueHolderV14<SgPhyStorageBatchUpdateResult> rollbackStorageBatch(List<String> redisBillFtpKeyList) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgPhyStorageRedisBillBatchUpdateService.updatePhyStorageBatch. ReceiveParams:redisBillFtpKeyList:{};"
                    , JSONObject.toJSONString(redisBillFtpKeyList));
        }

        return stoUpdService.rollbackPhyStorageBill(redisBillFtpKeyList);
    }

}
