package com.jackrain.nea.sg.basic.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.redis.config.CusRedisTemplate;
import com.jackrain.nea.redis.util.RedisOpsUtil;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.config.SgStorageControlConfig;
import com.jackrain.nea.sg.basic.mapper.CpStoreMapper;
import com.jackrain.nea.sg.basic.mapper.SgBStorageMapper;
import com.jackrain.nea.sg.basic.model.request.SgStorageRedisMonitorRequest;
import com.jackrain.nea.sg.basic.model.result.SgRedisStorageQueryResult;
import com.jackrain.nea.sg.basic.model.result.SgStorageRedisMonitorResult;
import com.jackrain.nea.sg.basic.model.table.CpCStore;
import com.jackrain.nea.sg.basic.model.table.SgBStorage;
import com.jackrain.nea.sg.basic.utils.StorageUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.*;

/**
 * @Description: 通用逻辑仓库存监控接口(Redis)
 * @Author: chenb
 * @Date: 2019/7/29 11:32
 */
@Component
@Slf4j
public class SgStorageRedisMonitorService {

    @Autowired
    SgStorageControlConfig sgStorageControlConfig;

    @Autowired
    private SgBStorageMapper sgBStorageMapper;

    @Autowired
    private CpStoreMapper cpStoreMapper;

    public ValueHolderV14<SgStorageRedisMonitorResult> monitorRedisStorage(SgStorageRedisMonitorRequest request) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgStorageRedisMonitorService.monitorRedisStorage. ReceiveParams:request={};",
                    JSONObject.toJSONString(request));
        }

        long startTime = System.currentTimeMillis();

        ValueHolderV14<SgStorageRedisMonitorResult> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        Map<String, SgRedisStorageQueryResult> redisStorageMap;
        List<SgBStorage> pgSgBStorageResult;
        List<String> storeRedisKeysList;
        Set<Object> storeRedisKeys;
        List<Long> skuIds;

        SgRedisStorageQueryResult redisResult;
        long totalSuccessNum = 0;
        long totalErrorNum = 0;

        holder = checkServiceParam(request, request.getLoginUser());

        if (ResultCode.FAIL == holder.getCode()) {
            return holder;
        }

        CusRedisTemplate<String, String> redisTemplate = RedisOpsUtil.getStrRedisTemplate();
        DefaultRedisScript<List> redisScript = new DefaultRedisScript<>();
        redisScript.setLocation(new ClassPathResource("lua/QueryRedisStorage.lua"));
        redisScript.setResultType(List.class);

        User loginUser = request.getLoginUser();

        List<CpCStore> cpCStoreList = cpStoreMapper.selectList(
                new QueryWrapper<CpCStore>().lambda()
                        .eq(CpCStore::getIsactive, SgConstants.IS_ACTIVE_Y));

        if (CollectionUtils.isEmpty(cpCStoreList)) {

            holder.setMessage(Resources.getMessage("Redis逻辑仓库存监控执行成功！",
                    loginUser.getLocale(),
                    totalSuccessNum, totalErrorNum));

            return holder;
        }

        for (CpCStore cpCStoreItem : cpCStoreList) {

            storeRedisKeys = StorageUtils.scanRedisKeys(
                    SgConstants.REDIS_KEY_PREFIX_STORAGE + cpCStoreItem.getId().toString() + ":", 1000);

            storeRedisKeysList = new ArrayList((Collection<?>) storeRedisKeys);

            if (CollectionUtils.isEmpty(storeRedisKeys)) {

                log.info("SgStorageRedisMonitorService.monitorRedisStorage. info :" +
                                "Redis逻辑仓库存记录为0。店仓ID:{}",
                        cpCStoreItem.getId());

                continue;
            }

            //批量更新单据明细分页
            int pageSize = sgStorageControlConfig.getMaxQueryLimit();
            int listSize = storeRedisKeysList.size();
            int page = listSize / pageSize;

            if (listSize % pageSize != 0) {
                page++;
            }

            //分页批量更新
            for (int i = 0; i < page; i++) {

                List<String> keyList = storeRedisKeysList.subList(i * pageSize,
                        (((i + 1) * pageSize > listSize ? listSize : pageSize * (i + 1))));

                skuIds = new ArrayList<>();
                redisStorageMap = new HashMap<>();

                for (String redisKey : keyList) {
                    skuIds.add(Long.valueOf(redisKey.split(":")[3]));
                }

                pgSgBStorageResult = sgBStorageMapper.selectList(
                        new QueryWrapper<SgBStorage>().lambda()
                                .eq(SgBStorage::getCpCStoreId, cpCStoreItem.getId())
                                .in(!CollectionUtils.isEmpty(skuIds), SgBStorage::getPsCSkuId, skuIds));

                List result = redisTemplate.execute(redisScript, keyList);

                if (!CollectionUtils.isEmpty(result) && Integer.valueOf((String) result.get(0)) == ResultCode.SUCCESS) {

                    List<List> storageResultList = (ArrayList) result.get(1);

                    if (!CollectionUtils.isEmpty(storageResultList)) {

                        for (List storageResult : storageResultList) {

                            if (!CollectionUtils.isEmpty(storageResult)) {

                                SgRedisStorageQueryResult queryResult = new SgRedisStorageQueryResult();

                                queryResult.setQtyPreout(new BigDecimal((String) storageResult.get(0)));
                                queryResult.setQtyPrein(new BigDecimal((String) storageResult.get(1)));
                                queryResult.setQtyStorage(new BigDecimal((String) storageResult.get(2)));
                                queryResult.setQtyAvailable(new BigDecimal((String) storageResult.get(3)));

                                redisStorageMap.put(((String) storageResult.get(4)).split(":")[3],
                                        queryResult);

                            }

                        }

                    }

                }

                for (SgBStorage pgSgBStorage : pgSgBStorageResult) {

                    redisResult = redisStorageMap.get(pgSgBStorage.getPsCSkuId().toString());

                    if (redisResult == null) {

                        log.error("SgStorageRedisMonitorService.monitorRedisStorage. error :" +
                                        "Redis逻辑仓库存记录不存在。逻辑仓ID:{},商品条码ID:{};",
                                pgSgBStorage.getCpCStoreId(), pgSgBStorage.getPsCSkuId());

                        totalErrorNum++;

                    } else if (redisResult.getQtyPreout().compareTo(pgSgBStorage.getQtyPreout()) != 0 ||
                            redisResult.getQtyPrein().compareTo(pgSgBStorage.getQtyPrein()) != 0 ||
                            redisResult.getQtyStorage().compareTo(pgSgBStorage.getQtyStorage()) != 0 ||
                            redisResult.getQtyAvailable().compareTo(pgSgBStorage.getQtyAvailable()) != 0) {

                        log.error("SgStorageRedisMonitorService.monitorRedisStorage. error :" +
                                        "逻辑仓ID:{},商品条码ID:{},PG占用量:{},PG在途量:{},PG在库量:{},PG可用量:{}," +
                                        "Redis占用量:{},Redis在途量:{},Redis在库量:{},Redis可用量:{};",
                                pgSgBStorage.getCpCStoreId(), pgSgBStorage.getPsCSkuId(),
                                pgSgBStorage.getQtyPreout(), pgSgBStorage.getQtyPrein(),
                                pgSgBStorage.getQtyStorage(), pgSgBStorage.getQtyAvailable(),
                                redisResult.getQtyPreout(), redisResult.getQtyPrein(),
                                redisResult.getQtyStorage(), redisResult.getQtyAvailable());

                        totalErrorNum++;

                    } else {

                        totalSuccessNum++;
                    }

                    redisStorageMap.remove(pgSgBStorage.getPsCSkuId().toString());
                }

                if (redisStorageMap.size() > 0) {
                    for (String emptySkuId : redisStorageMap.keySet()) {

                        log.error("SgStorageRedisMonitorService.monitorRedisStorage. error :" +
                                        "PG逻辑仓库存记录不存在。店仓ID:{},商品条码ID:{};",
                                cpCStoreItem.getId(), emptySkuId);

                        totalErrorNum++;
                    }
                }
            }
        }

        holder.setMessage(Resources.getMessage("Redis逻辑仓库存监控执行成功！",
                loginUser.getLocale(),
                totalSuccessNum, totalErrorNum));

        if (log.isDebugEnabled()) {
            log.debug("Finish SgStorageRedisMonitorService.monitorRedisStorage. ReturnResult:holder={} spend time:{}ms;",
                    JSONObject.toJSONString(holder), System.currentTimeMillis() - startTime);
        }

        return holder;
    }

    /**
     * @param request
     * @param loginUser
     * @return ValueHolderV14
     */
    private ValueHolderV14 checkServiceParam(SgStorageRedisMonitorRequest request,
                                             User loginUser) {

        ValueHolderV14 holder = new ValueHolderV14(ResultCode.SUCCESS, "");

        int maxQueryLimit = sgStorageControlConfig.getMaxQueryLimit();

        List<Long> SkuIds = request.getSkuIds();

        if (!CollectionUtils.isEmpty(SkuIds) &&
                SkuIds.size() > maxQueryLimit) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("商品条码ID的查询件数超过最大限制！",
                    loginUser.getLocale()));
        }

        return holder;
    }
}
