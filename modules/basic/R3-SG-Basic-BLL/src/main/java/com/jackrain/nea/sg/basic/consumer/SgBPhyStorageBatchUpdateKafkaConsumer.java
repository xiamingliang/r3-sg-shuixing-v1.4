package com.jackrain.nea.sg.basic.consumer;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.config.PropertiesConf;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.config.SgStorageKafkaConfig;
import com.jackrain.nea.sg.basic.model.SgPhyStorageUpdateCommonModel;
import com.jackrain.nea.sg.basic.model.SgPhyTeusStorageUpdateCommonModel;
import com.jackrain.nea.sg.basic.model.result.SgPhyStorageBatchUpdateResult;
import com.jackrain.nea.sg.basic.model.result.SgPhyTeusStorageBatchUpdateResult;
import com.jackrain.nea.sg.basic.services.SgPhyStorageItemKafkaBatchUpdateService;
import com.jackrain.nea.sg.basic.services.SgPhyTeusStorageItemKafkaBatchUpdateService;
import com.jackrain.nea.sg.basic.utils.StorageLogUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.face.impl.UserImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @Description:通用kafka实体仓库存批量更新消费者
 * @Author: chenb
 * @Date: 2019/5/15 11:02
 */
@Slf4j
@Component
public class SgBPhyStorageBatchUpdateKafkaConsumer {

    @Autowired
    private SgPhyStorageItemKafkaBatchUpdateService phyUpdService;

    @Autowired
    private SgPhyTeusStorageItemKafkaBatchUpdateService teusPhyUpdService;

    @Autowired
    private KafkaProducer<String, String> kafkaSender;

    @Autowired
    private SgStorageKafkaConfig mqConfig;

    @Autowired
    private PropertiesConf pconf;

    /**
     * 通用kafka实体仓库存批量更新消费者
     *
     * @param recordList
     * @return
     */
    public ValueHolderV14 consume(List<ConsumerRecord<String, String>> recordList) {

        ValueHolderV14<SgPhyStorageBatchUpdateResult> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        ValueHolderV14<SgPhyTeusStorageBatchUpdateResult> teusHolder = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        List<SgPhyStorageUpdateCommonModel> batchModelList = new ArrayList<>();
        List<SgPhyTeusStorageUpdateCommonModel> batchTeusModelList = new ArrayList<>();
        SgPhyStorageUpdateCommonModel commonModel;
        SgPhyTeusStorageUpdateCommonModel teusCommonModel;
        String currentThreadName = Thread.currentThread().getName();
        User loginUser = null;
        HashMap paramMap;
        int partition = -1;

        try {

            for (ConsumerRecord<String, String> record : recordList) {

                paramMap = JSON.parseObject(record.value(), HashMap.class);

                log.info("SgBPhyStorageBatchUpdateKafkaConsumer.consume. ReceiveParams:messageTopic={} " +
                                "messageKey={} partition={} timestamp={} messageBody={};",
                        record.topic(), record.key(), record.partition(), record.timestamp(), JSONObject.toJSONString(paramMap));

                if (partition == -1) {
                    partition = record.partition();
                }

                if (loginUser == null) {
                    loginUser = JSONObject.parseObject(JSONObject.toJSONString(paramMap.get("user")), UserImpl.class);
                }

                if (record.key().startsWith(SgConstants.MSG_TAG_HEAD_PHY_STORAGE)) {
                    commonModel = JSONObject.parseObject(JSONObject.toJSONString(paramMap.get("param")), SgPhyStorageUpdateCommonModel.class);
                    commonModel.setOffset(record.offset());
                    batchModelList.add(commonModel);
                } else if (record.key().startsWith(SgConstants.MSG_TAG_HEAD_PHY_TEUS_STORAGE)) {
                    teusCommonModel = JSONObject.parseObject(JSONObject.toJSONString(paramMap.get("param")), SgPhyTeusStorageUpdateCommonModel.class);
                    teusCommonModel.setOffset(record.offset());
                    batchTeusModelList.add(teusCommonModel);
                }

            }

            if (!CollectionUtils.isEmpty(batchModelList)) {
                holder = phyUpdService.updatePhyStorageItem(batchModelList, loginUser);

                if (ResultCode.FAIL == holder.getCode()) {
                    holder.setCode(holder.getCode());
                    holder.setMessage(holder.getMessage());
                }
            }

            if (!CollectionUtils.isEmpty(batchTeusModelList)) {
                teusHolder = teusPhyUpdService.updatePhyStorageItem(batchTeusModelList, loginUser);

                if (ResultCode.FAIL == teusHolder.getCode()) {
                    holder.setCode(teusHolder.getCode());
                    holder.setMessage(teusHolder.getMessage());
                }
            }

        } catch (Exception e) {
            String returnMsg = StorageLogUtils.getErrMessage(e, currentThreadName, loginUser);
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(returnMsg);
        }

        if (ResultCode.FAIL == holder.getCode()) {

            paramMap = new HashMap();
            long recordTimestamp = System.currentTimeMillis();
            int maxTryTimes = Integer.valueOf(pconf.getProperty("sg.control.xact_retry_times"));

            for (SgPhyStorageUpdateCommonModel commonModelretry : batchModelList) {

                if (commonModelretry.getErrorTimes() > maxTryTimes) {

                    log.error("SgBPhyStorageBatchUpdateKafkaConsumer.consume. messageTopic={},messageBody={} has retried over " + maxTryTimes + " times;",
                            mqConfig.getStorageUpdateKafkaTopic(), JSONObject.toJSONString(commonModelretry));
                    continue;

                } else {

                    log.warn("SgBPhyStorageBatchUpdateKafkaConsumer.consume. messageTopic={},messageBody={} has retried over {} times;",
                            mqConfig.getStorageUpdateKafkaTopic(), JSONObject.toJSONString(commonModelretry), commonModelretry.getErrorTimes());

                    commonModelretry.setErrorTimes(commonModelretry.getErrorTimes() + 1);

                    paramMap.put("param", commonModelretry);
                    paramMap.put("user", loginUser);

                    ProducerRecord<String, String> record = new ProducerRecord<String, String>(
                            mqConfig.getPhyStorageUpdateKafkaTopic(),
                            partition,
                            recordTimestamp,
                            commonModelretry.getMessageKey(),
                            JSONObject.toJSONString(paramMap));

                    kafkaSender.send(record);

                }
            }

        }

        //TODO 箱库存重发待开发

        return holder;

    }
}
