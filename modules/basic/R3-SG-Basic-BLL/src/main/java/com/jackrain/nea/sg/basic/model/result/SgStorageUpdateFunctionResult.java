package com.jackrain.nea.sg.basic.model.result;

import lombok.Data;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/3/13 15:00
 */
@Data
public class SgStorageUpdateFunctionResult {

    /**
     * 报错消息
     */
    private String msg;
    /**
     * 库存锁
     */
    private String lockkey;

}
