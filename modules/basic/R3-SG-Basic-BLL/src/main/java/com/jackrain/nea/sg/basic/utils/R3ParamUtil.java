package com.jackrain.nea.sg.basic.utils;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.Feature;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.jackrain.nea.sg.basic.common.R3ParamConstants;
import com.jackrain.nea.sg.basic.model.request.SgR3BaseRequest;
import com.jackrain.nea.sg.basic.model.result.SgR3BaseResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.query.DefaultWebEvent;
import com.jackrain.nea.web.query.QuerySession;
import org.apache.commons.collections.CollectionUtils;

import java.util.Locale;

/**
 * @author csy
 */
public class R3ParamUtil {


    /**
     * 从jsonObj转换到保存该有的入参
     */
    public static <T extends SgR3BaseRequest> T parseSaveObject(QuerySession session, Class<T> clazz) {
        Locale locale = session.getLocale();
        JSONObject param = R3ParamUtil.getParamJo(session);
        AssertUtils.notNull(param, "参数不能为空!-param", locale);
        Long objId = param.getLong(R3ParamConstants.OBJID);
        JSONObject fixC = param.getJSONObject(R3ParamConstants.FIXCOLUMN);
        AssertUtils.notNull(fixC, "参数不能为空!-" + R3ParamConstants.FIXCOLUMN, locale);
        User user = session.getUser();
        AssertUtils.notNull(user, "用户不能为空!", locale);
        T request = JSONObject.parseObject(fixC.toString(), clazz);
        request.setObjId(objId);
        request.setLoginUser(user);
        return request;
    }

    /**
     * 返回获取param jsonObj
     *
     * @param session session
     */
    public static JSONObject getParamJo(QuerySession session) {
        AssertUtils.notNull(session, "session is null");
        Locale locale = session.getLocale();
        DefaultWebEvent event = session.getEvent();
        AssertUtils.notNull(event, "参数不能为空!-event", locale);
        return JSON.parseObject(JSON.toJSONStringWithDateFormat(event.getParameterValue(R3ParamConstants.PARAM),
                "yyyy-MM-dd HH:mm:ss", SerializerFeature.WriteMapNullValue), Feature.OrderedField);
    }

    /**
     * 将v14的 holder转化成v13的holder
     */
    public static ValueHolder convertV14(ValueHolderV14 holderV14) {
        ValueHolder holder = new ValueHolder();
        if (holderV14 != null) {
            holder.put("code", holderV14.getCode());
            holder.put("message", holderV14.getMessage());
            holder.put("data", holderV14.getData());
        }
        return holder;
    }

    /**
     * 将v14的 holder转化成v13的holder
     * 考虑 date可能即是jsonObj 又是jsonArray的 情况
     */
    public static ValueHolder convertV14WithResult(ValueHolderV14<SgR3BaseResult> holderV14) {
        ValueHolder holder = new ValueHolder();
        if (holderV14 != null) {
            holder.put("code", holderV14.getCode());
            holder.put("message", holderV14.getMessage());
            if (holderV14.getData() != null) {
                if (holderV14.getData().getDataJo() != null) {
                    holder.put("data", holderV14.getData().getDataJo());
                } else if (CollectionUtils.isNotEmpty(holderV14.getData().getDataArr())) {
                    holder.put("data", holderV14.getData().getDataArr());
                }
            }
        }
        return holder;
    }
}
