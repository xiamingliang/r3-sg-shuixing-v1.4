package com.jackrain.nea.sg.basic.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.model.util.ModelUtil;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.logic.SgStorageCheckLogic;
import com.jackrain.nea.sg.basic.logic.SgTeusStorageLogic;
import com.jackrain.nea.sg.basic.mapper.SgBStorageLockLogMapper;
import com.jackrain.nea.sg.basic.mapper.SgBTeusStorageMapper;
import com.jackrain.nea.sg.basic.model.SgStorageUpdateControlModel;
import com.jackrain.nea.sg.basic.model.SgTeusStorageUpdateCommonModel;
import com.jackrain.nea.sg.basic.model.result.SgTeusStorageUpdateCommonResult;
import com.jackrain.nea.sg.basic.model.table.SgBStorageLockLog;
import com.jackrain.nea.sg.basic.model.table.SgBTeusStorage;
import com.jackrain.nea.sg.basic.utils.StorageUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/3/20 13:15
 */
@Component
@Slf4j
public class SgTeusStorageItemBatchUpdateService {

    @Autowired
    private SgBTeusStorageMapper sgBTeusStorageMapper;

    @Autowired
    private SgBStorageLockLogMapper sgBStorageLockLogMapper;

    @Autowired
    private SgTeusStorageLogic sgTeusStorageLogic;

    /**
     * @param requestList
     * @param controlModel
     * @param loginUser
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public ValueHolderV14<SgTeusStorageUpdateCommonResult> updateTeusStorageItemWithTrans(List<SgTeusStorageUpdateCommonModel> requestList,
                                                                                          SgStorageUpdateControlModel controlModel,
                                                                                          User loginUser) {

        ValueHolderV14<SgTeusStorageUpdateCommonResult> holder = updateTeusStorageItem(requestList,
                controlModel, loginUser);

        if (ResultCode.FAIL == holder.getCode()) {
            throw new NDSException(holder.getMessage());
        }

        return holder;

    }

    /**
     * @param requestList
     * @param controlModel
     * @param loginUser
     * @return
     */
    public ValueHolderV14<SgTeusStorageUpdateCommonResult> updateTeusStorageItem(List<SgTeusStorageUpdateCommonModel> requestList,
                                                                               SgStorageUpdateControlModel controlModel,
                                                                               User loginUser) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgTeusStorageItemBatchUpdateService.updateTeusStorageItem. ReceiveParams:requestList:{} controlModel:{};"
                    , JSONObject.toJSONString(requestList), JSONObject.toJSONString(controlModel));
        }

        ValueHolderV14<SgTeusStorageUpdateCommonResult> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        List<SgBStorageLockLog> lockLogList = new ArrayList<>();
        List<SgTeusStorageUpdateCommonModel> teusStorageChangeFtpList = new ArrayList<>();
        Map<Long, Long> storeMap = new HashMap<>();
        Map<Long, Long> teusMap = new HashMap<>();
        Map<String, SgTeusStorageUpdateCommonModel> teusStorageNotExistMap = new HashMap<>();
        SgTeusStorageUpdateCommonResult updateResult = new SgTeusStorageUpdateCommonResult();
        int resultCount = 0;

        if (CollectionUtils.isEmpty(requestList)) {
            return holder;
        }

        for (SgTeusStorageUpdateCommonModel request : requestList) {

            holder = SgStorageCheckLogic.checkServiceParam(request, loginUser);

            if (ResultCode.FAIL == holder.getCode()) {
                return holder;
            }

            storeMap.put(request.getCpCStoreId(), request.getCpCStoreId());
            teusMap.put(request.getPsCTeusId(), request.getPsCTeusId());

            teusStorageNotExistMap.put(request.getCpCStoreId() + "_" + request.getPsCTeusId(),
                    request);

            //批量初始化库存处理流水锁
            SgBStorageLockLog lockLog = new SgBStorageLockLog();
            BeanUtils.copyProperties(request, lockLog);
            lockLog.setId(ModelUtil.getSequence(SgConstants.SG_B_STORAGE_LOCK_LOG));
            lockLogList.add(lockLog);

        }

        //查询已存在的库存记录
        List<SgBTeusStorage> resultList = sgBTeusStorageMapper.selectList(
                new QueryWrapper<SgBTeusStorage>().lambda().select(SgBTeusStorage::getCpCStoreId, SgBTeusStorage::getPsCTeusId)
                        .in(!CollectionUtils.isEmpty(storeMap.values()), SgBTeusStorage::getCpCStoreId, new ArrayList<>(storeMap.values()))
                        .in(!CollectionUtils.isEmpty(teusMap.values()), SgBTeusStorage::getPsCTeusId, new ArrayList<>(teusMap.values()))
        );

        //除去【批量初始化库存】列表中已存在的记录
        if (!CollectionUtils.isEmpty(resultList)) {
            String key = null;
            for (SgBTeusStorage storageRecode : resultList) {
                key = storageRecode.getCpCStoreId() + "_" + storageRecode.getPsCTeusId();
                if (teusStorageNotExistMap.containsKey(key)) {
                    teusStorageNotExistMap.remove(key);
                }
            }
        }

        //批量初始化库存
        if (teusStorageNotExistMap.size() > 0) {

            try {
                holder = sgTeusStorageLogic.initTeusStorageList(new ArrayList<>(teusStorageNotExistMap.values()), loginUser);
            } catch (Exception e) {
                log.error("SgTeusStorageLogic.initTeusStorageList 逻辑仓批量箱库存初始化插入失败！messageKey:{}",
                        requestList.get(0).getMessageKey());
                //并发初始化库存记录异常时进行等待重试
                holder.setCode(ResultCode.FAIL);
                holder.setData(updateResult);
                holder.setMessage(Resources.getMessage("逻辑仓箱库存初始化插入失败！", loginUser.getLocale()));
                return holder;
            }

            if (ResultCode.FAIL == holder.getCode()) {
                throw new NDSException(holder.getMessage());
            }
        }

        List<List<SgBStorageLockLog>> insertPageList =
                StorageUtils.getBaseModelPageList(lockLogList, SgConstants.SG_COMMON_INSERT_PAGE_SIZE);

        //分页批量更新
        for (List<SgBStorageLockLog> pageList : insertPageList) {

            if (CollectionUtils.isEmpty(pageList)) {
                continue;
            }

            //批量初始化库存处理流水锁
            resultCount = sgBStorageLockLogMapper.batchInsert(pageList);

            if (resultCount != pageList.size()) {
                throw new NDSException(Resources.getMessage("逻辑仓箱库存锁批量初始化插入失败！", loginUser.getLocale()));
            }

        }

        //批量更新实体仓库存
        for (SgTeusStorageUpdateCommonModel request : requestList) {

            holder = sgTeusStorageLogic.updateTeusStorageChange(request, controlModel, loginUser);

            //更新期初，期末数量
            if (ResultCode.SUCCESS == holder.getCode()) {
                BeanUtils.copyProperties(holder.getData(), request);
                teusStorageChangeFtpList.add(request);
            }

            if (ResultCode.FAIL == holder.getCode()) {
                throw new NDSException(holder.getMessage());
            }

        }

        //批量更新箱库存在库流水
        if (!CollectionUtils.isEmpty(teusStorageChangeFtpList)) {

            holder = sgTeusStorageLogic.insertTeusStoragePreoutFtpList(teusStorageChangeFtpList, loginUser);

            if (ResultCode.FAIL == holder.getCode()) {
                throw new NDSException(holder.getMessage());
            }

            holder = sgTeusStorageLogic.insertTeusStoragePreinFtpList(teusStorageChangeFtpList, loginUser);

            if (ResultCode.FAIL == holder.getCode()) {
                throw new NDSException(holder.getMessage());
            }

            holder = sgTeusStorageLogic.insertTeusStorageChangeFtpList(teusStorageChangeFtpList, loginUser);

            if (ResultCode.FAIL == holder.getCode()) {
                throw new NDSException(holder.getMessage());
            }
        }

        holder.setData(updateResult);

        if (log.isDebugEnabled()) {
            log.debug("Finish SgTeusStorageItemBatchUpdateService.updateTeusStorageItem. ReturnResult:countResult:{};"
                    , JSONObject.toJSONString(holder.getData().getCountResult()));
        }

        return holder;

    }

}