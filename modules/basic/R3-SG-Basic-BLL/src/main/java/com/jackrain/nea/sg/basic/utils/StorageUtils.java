package com.jackrain.nea.sg.basic.utils;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.redis.config.CusRedisTemplate;
import com.jackrain.nea.redis.util.RedisOpsUtil;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sys.domain.BaseModel;
import com.jackrain.nea.utility.ExceptionUtil;
import com.jackrain.nea.web.face.User;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.Cursor;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.ScanOptions;

import java.util.*;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/3/14 14:30
 */
public class StorageUtils {

    public static <T extends BaseModel> T setBModelDefalutData(T sourceModel, User loginUser) {

        Date systemDate = new Date();
        Long loginUserId = loginUser.getId() == null ? null : loginUser.getId().longValue();

        sourceModel.setAdClientId(Long.valueOf(loginUser.getClientId()));
        sourceModel.setAdOrgId(Long.valueOf(loginUser.getOrgId()));
        sourceModel.setIsactive(SgConstants.IS_ACTIVE_Y);
        sourceModel.setOwnerid(loginUserId);
        sourceModel.setOwnername(loginUser.getName());
        sourceModel.setCreationdate(systemDate);
        sourceModel.setModifierid(loginUserId);
        sourceModel.setModifiername(loginUser.getName());
        sourceModel.setModifieddate(systemDate);

        return sourceModel;
    }


    public static <T extends BaseModel> T setBModelDefalutDataByUpdate(T sourceModel, User loginUser) {
        Date systemDate = new Date();
        Long loginUserId = loginUser.getId() == null ? null : loginUser.getId().longValue();

        sourceModel.setModifierid(loginUserId);
        sourceModel.setModifiername(loginUser.getName());
        sourceModel.setModifieddate(systemDate);

        return sourceModel;
    }

    public static <T extends BaseModel> List<List<T>> getBaseModelPageList(List<T> sourceList, int pageSize) {

        List<List<T>> resultList = new ArrayList<>();

        if (CollectionUtils.isEmpty(sourceList)) {
            return resultList;
        }

        int listSize = sourceList.size();
        int page = listSize / pageSize;

        if (listSize % pageSize != 0) {
            page++;
        }

        for (int i = 0; i < page; i++) {

            List<T> pageList = sourceList.subList(i * pageSize,
                    (((i + 1) * pageSize > listSize ? listSize : pageSize * (i + 1))));

            resultList.add(pageList);
        }

        return resultList;
    }

    public static <T> List<List<T>> getPageList(List<T> sourceList, int pageSize) {

        List<List<T>> resultList = new ArrayList<>();
        if (CollectionUtils.isEmpty(sourceList)) {
            return resultList;
        }

        int listSize = sourceList.size();
        int page = listSize / pageSize;
        if (listSize % pageSize != 0) {
            page++;
        }

        for (int i = 0; i < page; i++) {

            List<T> pageList = sourceList.subList(i * pageSize,
                    (((i + 1) * pageSize > listSize ? listSize : pageSize * (i + 1))));
            resultList.add(pageList);
        }
        return resultList;
    }

    public static Set<Object> scanRedisKeys(String redisKeyPrefix, int countSize) {

        CusRedisTemplate<String, String> redisTemplate = RedisOpsUtil.getStrRedisTemplate();

        Set<Object> result = redisTemplate.execute(new RedisCallback<Set<Object>>() {

            @Override
            public Set<Object> doInRedis(RedisConnection connection) throws DataAccessException {

                Set<Object> binaryKeys = new HashSet<>();

                Cursor<byte[]> cursor = connection.scan(
                        new ScanOptions.ScanOptionsBuilder()
                                .match(redisKeyPrefix + "*")
                                .count(countSize).build());

                while (cursor.hasNext()) {
                    binaryKeys.add(new String(cursor.next()));
                }

                return binaryKeys;
            }
        });

        return result;
    }

    /**
     * 获取异常信息
     *
     * @param e 异常
     * @return 信息
     */
    public static String getExceptionMsg(Exception e) {
        return e instanceof NDSException ? e.getMessage() : ExceptionUtil.getMessage(e);
    }

}
