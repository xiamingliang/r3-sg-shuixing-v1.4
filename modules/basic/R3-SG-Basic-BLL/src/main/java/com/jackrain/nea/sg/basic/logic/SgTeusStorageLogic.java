package com.jackrain.nea.sg.basic.logic;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.model.util.ModelUtil;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.mapper.SgBTeusStorageChangeFtpMapper;
import com.jackrain.nea.sg.basic.mapper.SgBTeusStorageMapper;
import com.jackrain.nea.sg.basic.mapper.SgBTeusStoragePreinFtpMapper;
import com.jackrain.nea.sg.basic.mapper.SgBTeusStoragePreoutFtpMapper;
import com.jackrain.nea.sg.basic.model.SgStorageUpdateControlModel;
import com.jackrain.nea.sg.basic.model.SgTeusStorageUpdateCommonModel;
import com.jackrain.nea.sg.basic.model.result.SgTeusStorageUpdateCommonResult;
import com.jackrain.nea.sg.basic.model.table.SgBTeusStorage;
import com.jackrain.nea.sg.basic.model.table.SgBTeusStorageChangeFtp;
import com.jackrain.nea.sg.basic.model.table.SgBTeusStoragePreinFtp;
import com.jackrain.nea.sg.basic.model.table.SgBTeusStoragePreoutFtp;
import com.jackrain.nea.sg.basic.utils.StorageUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/3/13 14:50
 */
@Component
@Slf4j
public class SgTeusStorageLogic {

    @Autowired
    private SgBTeusStorageMapper sgBTeusStorageMapper;

    @Autowired
    private SgBTeusStoragePreoutFtpMapper sgBTeusStoragePreoutFtpMapper;

    @Autowired
    private SgBTeusStoragePreinFtpMapper sgBTeusStoragePreinFtpMapper;

    @Autowired
    private SgBTeusStorageChangeFtpMapper sgBTeusStorageChangeFtpMapper;


    /**
     * 更新箱库存
     *
     * @param updateModel
     * @param controlModel
     * @param loginUser
     * @return ValueHolderV14<SgTeusStorageUpdateCommonResult>
     */
    public ValueHolderV14<SgTeusStorageUpdateCommonResult> updateTeusStorageChange(
            SgTeusStorageUpdateCommonModel updateModel,
            SgStorageUpdateControlModel controlModel,
            User loginUser) {

        Date systemDate = new Date();
        ValueHolderV14<SgTeusStorageUpdateCommonResult> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        SgTeusStorageUpdateCommonResult commonResult = new SgTeusStorageUpdateCommonResult();

        updateModel.setModifierid(loginUser.getId() == null ? null : loginUser.getId().longValue());
        updateModel.setModifierename(loginUser.getEname());
        updateModel.setModifiername(loginUser.getName());
        updateModel.setModifieddate(systemDate);

        boolean blncheckPreoutNegative = !controlModel.isNegativePreout() &&
                (updateModel.getQtyPreoutChange().compareTo(BigDecimal.ZERO) < 0 ? true : false);
        boolean blncheckPreinNegative = !controlModel.isNegativePrein() &&
                (updateModel.getQtyPreinChange().compareTo(BigDecimal.ZERO) < 0 ? true : false);
        boolean blncheckChangeNegative = !controlModel.isNegativeStorage() &&
                (updateModel.getQtyStorageChange().compareTo(BigDecimal.ZERO) < 0 ? true : false);
        boolean blncheckAvailableNegative = !controlModel.isNegativeAvailable() && (
                (updateModel.getQtyPreoutChange().compareTo(BigDecimal.ZERO) > 0 ||
                        updateModel.getQtyStorageChange().compareTo(BigDecimal.ZERO) < 0) ? true : false);

        int updateResult = sgBTeusStorageMapper.updateQtyChange(updateModel,
                blncheckPreoutNegative, blncheckPreinNegative, blncheckChangeNegative, blncheckAvailableNegative);

        //在库不允许负箱库存 并且 更新0条记录的情况下
        if ((!controlModel.isNegativePreout() ||
            !controlModel.isNegativePrein() ||
            !controlModel.isNegativeStorage() ||
            !controlModel.isNegativeAvailable())
            && updateResult <= 0) {

            holder.setCode(ResultCode.FAIL);

            if (log.isDebugEnabled()) {
                log.debug("SgTeusStorageLogic.updateTeusStorageChange. 当前箱库存变动明细库存不足！！单据编号:{}，逻辑仓ID:{}，" +
                                "箱ID:{}，占用变动数量:{}, 在途变动数量:{}, 在库变动数量:{}",
                        updateModel.getBillNo(), updateModel.getCpCStoreId(),
                        updateModel.getPsCTeusId(), updateModel.getQtyPreoutChange(),
                        updateModel.getQtyPreinChange(), updateModel.getQtyStorageChange());
            }

            List<SgTeusStorageUpdateCommonModel> outStockItemList = new ArrayList<>();
            outStockItemList.add(updateModel);
            commonResult.setOutStockItemList(outStockItemList);

            holder.setData(commonResult);
            holder.setMessage(Resources.getMessage("逻辑仓箱库存不足！",
                    loginUser.getLocale(), updateModel.getCpCStoreEcode(), updateModel.getPsCTeusEcode()));
            return holder;

        }

        commonResult.setCountResult(updateResult);

        List<SgBTeusStorage> selectResult = sgBTeusStorageMapper.selectList(
                new QueryWrapper<SgBTeusStorage>().lambda()
                        .eq(SgBTeusStorage::getCpCStoreId, updateModel.getCpCStoreId())
                        .eq(SgBTeusStorage::getPsCTeusId, updateModel.getPsCTeusId()));

        if (selectResult != null && selectResult.size() > 0) {

            BigDecimal qtyPreoutEnd = selectResult.get(0).getQtyPreout();
            BigDecimal qtyPreinEnd = selectResult.get(0).getQtyPrein();
            BigDecimal qtyStorageEnd = selectResult.get(0).getQtyStorage();
            //本版本的期初数量暂时通过期末数量为基准计算
            commonResult.setQtyPreoutBegin(qtyPreoutEnd.subtract(updateModel.getQtyPreoutChange()));
            commonResult.setQtyPreoutEnd(qtyPreoutEnd);
            commonResult.setQtyPreinBegin(qtyPreinEnd.subtract(updateModel.getQtyPreinChange()));
            commonResult.setQtyPreinEnd(qtyPreinEnd);
            commonResult.setQtyStorageBegin(qtyStorageEnd.subtract(updateModel.getQtyStorageChange()));
            commonResult.setQtyStorageEnd(qtyStorageEnd);
        }


        holder.setData(commonResult);
        holder.setCode(ResultCode.SUCCESS);
        holder.setMessage(Resources.getMessage("逻辑仓箱在库数量更新成功！", loginUser.getLocale()));

        return holder;
    }

    /**
     * 批量记录箱库存占用库存变动流水
     *
     * @param updateModelList
     * @param loginUser
     * @return ValueHolderV14<SgTeusStorageUpdateCommonResult>
     */
    public ValueHolderV14<SgTeusStorageUpdateCommonResult> insertTeusStoragePreoutFtpList(
            List<SgTeusStorageUpdateCommonModel> updateModelList,
            User loginUser) {

        ValueHolderV14<SgTeusStorageUpdateCommonResult> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        SgTeusStorageUpdateCommonResult commonResult = new SgTeusStorageUpdateCommonResult();
        List<SgBTeusStoragePreoutFtp> batchInsertList = new ArrayList<>();

        for (SgTeusStorageUpdateCommonModel updateModel : updateModelList) {

            if (BigDecimal.ZERO.compareTo(updateModel.getQtyPreoutChange()) != 0) {

                SgBTeusStoragePreoutFtp preoutUpdateModel = new SgBTeusStoragePreoutFtp();

                BeanUtils.copyProperties(updateModel, preoutUpdateModel);
                StorageUtils.setBModelDefalutData(preoutUpdateModel, loginUser);
                preoutUpdateModel.setId(ModelUtil.getSequence(SgConstants.SG_B_TEUS_STORAGE_PREOUT_FTP));
                preoutUpdateModel.setQtyChange(updateModel.getQtyPreoutChange());
                preoutUpdateModel.setQtyBegin(updateModel.getQtyPreoutBegin());
                preoutUpdateModel.setQtyEnd(updateModel.getQtyPreoutEnd());
                preoutUpdateModel.setOwnerename(loginUser.getEname());
                preoutUpdateModel.setModifierename(loginUser.getEname());
                preoutUpdateModel.setRedisBillFtpKey(updateModel.getRedisBillFtpKey());

                batchInsertList.add(preoutUpdateModel);
            }

        }

        List<List<SgBTeusStoragePreoutFtp>> insertPageList =
                StorageUtils.getBaseModelPageList(batchInsertList, SgConstants.SG_COMMON_INSERT_PAGE_SIZE);

        for (List<SgBTeusStoragePreoutFtp> pageList : insertPageList) {

            if (CollectionUtils.isEmpty(pageList)) {
                continue;
            }

            int insertResult = sgBTeusStoragePreoutFtpMapper.batchInsert(pageList);

            if (insertResult != pageList.size()) {
                log.error("SgTeusStorageLogic.insertTeusStoragePreoutFtpList 逻辑仓批量箱库存占用变动流水插入失败！");
                holder.setCode(ResultCode.FAIL);
                holder.setMessage(Resources.getMessage("逻辑仓批量箱库存占用变动流水插入失败！",
                        loginUser.getLocale()));
                return holder;
            }

        }

        holder.setData(commonResult);
        holder.setCode(ResultCode.SUCCESS);
        holder.setMessage(Resources.getMessage("逻辑仓批量箱库存占用变动流水插入成功！", loginUser.getLocale()));

        return holder;
    }

    /**
     * 批量记录箱库存在途变动流水
     *
     * @param updateModelList
     * @param loginUser
     * @return ValueHolderV14<SgTeusStorageUpdateCommonResult>
     */
    public ValueHolderV14<SgTeusStorageUpdateCommonResult> insertTeusStoragePreinFtpList(
            List<SgTeusStorageUpdateCommonModel> updateModelList,
            User loginUser) {

        ValueHolderV14<SgTeusStorageUpdateCommonResult> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        SgTeusStorageUpdateCommonResult commonResult = new SgTeusStorageUpdateCommonResult();
        List<SgBTeusStoragePreinFtp> batchInsertList = new ArrayList<>();

        for (SgTeusStorageUpdateCommonModel updateModel : updateModelList) {

            if (BigDecimal.ZERO.compareTo(updateModel.getQtyPreinChange()) != 0) {

                SgBTeusStoragePreinFtp preinUpdateModel = new SgBTeusStoragePreinFtp();

                BeanUtils.copyProperties(updateModel, preinUpdateModel);
                StorageUtils.setBModelDefalutData(preinUpdateModel, loginUser);
                preinUpdateModel.setId(ModelUtil.getSequence(SgConstants.SG_B_TEUS_STORAGE_PREIN_FTP));
                preinUpdateModel.setQtyChange(updateModel.getQtyPreinChange());
                preinUpdateModel.setQtyBegin(updateModel.getQtyPreinBegin());
                preinUpdateModel.setQtyEnd(updateModel.getQtyPreinEnd());
                preinUpdateModel.setOwnerename(loginUser.getEname());
                preinUpdateModel.setModifierename(loginUser.getEname());
                preinUpdateModel.setRedisBillFtpKey(updateModel.getRedisBillFtpKey());

                batchInsertList.add(preinUpdateModel);
            }

        }

        List<List<SgBTeusStoragePreinFtp>> insertPageList =
                StorageUtils.getBaseModelPageList(batchInsertList, SgConstants.SG_COMMON_INSERT_PAGE_SIZE);

        for (List<SgBTeusStoragePreinFtp> pageList : insertPageList) {

            if (CollectionUtils.isEmpty(pageList)) {
                continue;
            }

            int insertResult = sgBTeusStoragePreinFtpMapper.batchInsert(pageList);

            if (insertResult != pageList.size()) {
                log.error("SgTeusStorageLogic.insertTeusStorageChangeFtpList 逻辑仓批量箱库存在途变动流水插入失败！");
                holder.setCode(ResultCode.FAIL);
                holder.setMessage(Resources.getMessage("逻辑仓批量箱库存在途变动流水插入失败！",
                        loginUser.getLocale()));
                return holder;
            }

        }

        holder.setData(commonResult);
        holder.setCode(ResultCode.SUCCESS);
        holder.setMessage(Resources.getMessage("逻辑仓批量箱库存在途变动流水插入成功！", loginUser.getLocale()));

        return holder;
    }

    /**
     * 批量记录箱库存在库变动流水
     *
     * @param updateModelList
     * @param loginUser
     * @return ValueHolderV14<SgTeusStorageUpdateCommonResult>
     */
    public ValueHolderV14<SgTeusStorageUpdateCommonResult> insertTeusStorageChangeFtpList(
            List<SgTeusStorageUpdateCommonModel> updateModelList,
            User loginUser) {

        ValueHolderV14<SgTeusStorageUpdateCommonResult> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        SgTeusStorageUpdateCommonResult commonResult = new SgTeusStorageUpdateCommonResult();
        List<SgBTeusStorageChangeFtp> batchInsertList = new ArrayList<>();

        for (SgTeusStorageUpdateCommonModel updateModel : updateModelList) {

            if (BigDecimal.ZERO.compareTo(updateModel.getQtyStorageChange()) != 0) {

                SgBTeusStorageChangeFtp changeUpdateModel = new SgBTeusStorageChangeFtp();

                BeanUtils.copyProperties(updateModel, changeUpdateModel);
                StorageUtils.setBModelDefalutData(changeUpdateModel, loginUser);
                changeUpdateModel.setId(ModelUtil.getSequence(SgConstants.SG_B_TEUS_STORAGE_CHANGE_FTP));
                changeUpdateModel.setQtyChange(updateModel.getQtyStorageChange());
                changeUpdateModel.setQtyBegin(updateModel.getQtyStorageBegin());
                changeUpdateModel.setQtyEnd(updateModel.getQtyStorageEnd());
                changeUpdateModel.setOwnerename(loginUser.getEname());
                changeUpdateModel.setModifierename(loginUser.getEname());
                changeUpdateModel.setRedisBillFtpKey(updateModel.getRedisBillFtpKey());

                batchInsertList.add(changeUpdateModel);
            }
        }

        List<List<SgBTeusStorageChangeFtp>> insertPageList =
                StorageUtils.getBaseModelPageList(batchInsertList, SgConstants.SG_COMMON_INSERT_PAGE_SIZE);

        for (List<SgBTeusStorageChangeFtp> pageList : insertPageList) {

            if (CollectionUtils.isEmpty(pageList)) {
                continue;
            }

            int insertResult = sgBTeusStorageChangeFtpMapper.batchInsert(pageList);

            if (insertResult != pageList.size()) {
                log.error("SgTeusStorageLogic.insertTeusStorageChangeFtpList 逻辑仓批量箱库存在库变动流水插入失败！");
                holder.setCode(ResultCode.FAIL);
                holder.setMessage(Resources.getMessage("逻辑仓批量箱库存在库变动流水插入失败！",
                        loginUser.getLocale()));
                return holder;
            }

        }

        holder.setData(commonResult);
        holder.setCode(ResultCode.SUCCESS);
        holder.setMessage(Resources.getMessage("逻辑仓批量箱库存在库变动流水插入成功！", loginUser.getLocale()));

        return holder;
    }

    /**
     * 初始化箱库存
     *
     * @param updateModel
     * @param loginUser
     * @return ValueHolderV14<SgTeusStorageUpdateCommonResult>
     */
    public ValueHolderV14<SgTeusStorageUpdateCommonResult> initTeusStorage(
            SgTeusStorageUpdateCommonModel updateModel,
            User loginUser) {

        ValueHolderV14<SgTeusStorageUpdateCommonResult> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        SgTeusStorageUpdateCommonResult commonResult = new SgTeusStorageUpdateCommonResult();

        SgBTeusStorage sgBTeusStorage = new SgBTeusStorage();
        BeanUtils.copyProperties(updateModel, sgBTeusStorage);
        sgBTeusStorage.setId(ModelUtil.getSequence(SgConstants.SG_B_TEUS_STORAGE));
        sgBTeusStorage.setQtyStorage(BigDecimal.ZERO);

        int insertResult = sgBTeusStorageMapper.insert(sgBTeusStorage);

        if (insertResult <= 0) {
            log.error("SgTeusStorageLogic.initTeusStorage 逻辑仓箱库存初始化插入失败！逻辑仓编码:{},箱号:{}",
                    updateModel.getCpCStoreEcode(),
                    updateModel.getPsCTeusEcode());
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("逻辑仓箱库存初始化插入失败！",
                    loginUser.getLocale(),
                    updateModel.getCpCStoreEcode(),
                    updateModel.getPsCTeusEcode()));
            return holder;
        }

        holder.setData(commonResult);
        holder.setCode(ResultCode.SUCCESS);
        holder.setMessage(Resources.getMessage("逻辑仓箱库存初始化插入成功！", loginUser.getLocale()));

        return holder;
    }

    /**
     * 批量初始化箱库存
     *
     * @param updateModelList
     * @param loginUser
     * @return ValueHolderV14<SgTeusStorageUpdateCommonResult>
     */
    public ValueHolderV14<SgTeusStorageUpdateCommonResult> initTeusStorageList(
            List<SgTeusStorageUpdateCommonModel> updateModelList,
            User loginUser) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgTeusStorageLogic.initTeusStorageList. ReceiveParams:updateModelList="
                    + JSONObject.toJSONString(updateModelList) + ";");
        }

        ValueHolderV14<SgTeusStorageUpdateCommonResult> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        SgTeusStorageUpdateCommonResult commonResult = new SgTeusStorageUpdateCommonResult();
        List<SgBTeusStorage> batchInitList = new ArrayList<>();

        for (SgTeusStorageUpdateCommonModel updateModel : updateModelList) {

            SgBTeusStorage sgBTeusStorage = new SgBTeusStorage();
            BeanUtils.copyProperties(updateModel, sgBTeusStorage);
            sgBTeusStorage.setId(ModelUtil.getSequence(SgConstants.SG_B_TEUS_STORAGE));
            sgBTeusStorage.setQtyPreout(BigDecimal.ZERO);
            sgBTeusStorage.setQtyPrein(BigDecimal.ZERO);
            sgBTeusStorage.setQtyStorage(BigDecimal.ZERO);
            sgBTeusStorage.setQtyAvailable(BigDecimal.ZERO);

            batchInitList.add(sgBTeusStorage);
        }

        List<List<SgBTeusStorage>> insertPageList =
                StorageUtils.getBaseModelPageList(batchInitList, SgConstants.SG_COMMON_INSERT_PAGE_SIZE);

        for (List<SgBTeusStorage> pageList : insertPageList) {

            if (CollectionUtils.isEmpty(pageList)) {
                continue;
            }

            int insertResult = sgBTeusStorageMapper.batchInsert(pageList);

            if (insertResult != pageList.size()) {
                log.error("SgTeusStorageLogic.initTeusStorageList 逻辑仓批量箱库存初始化插入失败！");
                holder.setCode(ResultCode.FAIL);
                holder.setMessage(Resources.getMessage("逻辑仓批量箱库存初始化插入失败！",
                        loginUser.getLocale()));
                return holder;
            }
        }

        holder.setData(commonResult);
        holder.setCode(ResultCode.SUCCESS);
        holder.setMessage(Resources.getMessage("逻辑仓批量箱库存初始化插入成功！", loginUser.getLocale()));

        return holder;
    }
}
