package com.jackrain.nea.sg.basic.consumer;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.aliyun.openservices.ons.api.Action;
import com.aliyun.openservices.ons.api.Message;
import com.aliyun.openservices.ons.api.impl.util.MsgConvertUtil;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.config.SgStorageControlConfig;
import com.jackrain.nea.sg.basic.model.request.SgPhyStorageBatchUpdateRequest;
import com.jackrain.nea.sg.basic.model.request.SgPhyStorageUpdateBillRequest;
import com.jackrain.nea.sg.basic.model.request.SgStorageUpdateControlRequest;
import com.jackrain.nea.sg.basic.model.result.SgPhyStorageBatchUpdateResult;
import com.jackrain.nea.sg.basic.services.SgPhyStorageMQBatchUpdateService;
import com.jackrain.nea.sg.basic.utils.StorageLogUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.face.impl.UserImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;

/**
 * @Description:通用MQ实体仓库存批量更新消费者
 * @Author: chenb
 * @Date: 2019/5/15 11:02
 */
@Slf4j
@Component
public class SgBPhyStorageBatchUpdateConsumer {

    @Autowired
    private SgPhyStorageMQBatchUpdateService service;

    @Autowired
    private SgStorageControlConfig sgStorageControlConfig;

    /**
     * 通用MQ实体仓库存批量更新消费者
     * @param message
     * @return
     */
    public Action consume(Message message) {

        SgPhyStorageBatchUpdateRequest request = new SgPhyStorageBatchUpdateRequest();
        Action consumeResult = Action.CommitMessage;

        String messageTopic = message.getTopic();
        String messageKey = message.getKey();
        String messageTag = message.getTag();
        String messageBody = null;

        try {

            messageBody = MsgConvertUtil.objectDeserialize(message.getBody()).toString();

            if (log.isDebugEnabled()) {
                log.debug("Start SgBPhyStorageBatchUpdateConsumer.consume. ReceiveParams:messageTopic={} " +
                                "messageKey={} messageTag={} messageBody={};",
                        messageTopic, messageKey, messageTag, messageBody);
            }

            HashMap paramMap = JSON.parseObject(messageBody, HashMap.class);

            //MQ消息实体封装
            User loginUser = JSONObject.parseObject(JSONObject.toJSONString(paramMap.get("user")), UserImpl.class);
            request.setLoginUser(loginUser);

            request.setMessageKey(messageKey);

            List<SgPhyStorageUpdateBillRequest> billList = JSON.parseArray(JSONObject.toJSONString(paramMap.get("param")),
                    SgPhyStorageUpdateBillRequest.class);
            request.setBillList(billList);

            //Redis库存更新控制的情况下（默认不进行负库存控制）
            if (sgStorageControlConfig.isRedisFunction()) {
                SgStorageUpdateControlRequest controlRequest = new SgStorageUpdateControlRequest();
                controlRequest.setNegativePrein(Boolean.TRUE);
                controlRequest.setNegativePreout(Boolean.TRUE);
                controlRequest.setNegativeStorage(Boolean.TRUE);
                controlRequest.setNegativeAvailable(Boolean.TRUE);
                request.setControlModel(controlRequest);
            }

            //调用通用实体仓库存批量更新接口
            ValueHolderV14<SgPhyStorageBatchUpdateResult> updateResult = service.updatePhyStorageBatch(request);

            if (ResultCode.FAIL == updateResult.getCode()) {
                consumeResult = Action.ReconsumeLater;
            }

        } catch (Exception e) {

            StorageLogUtils.getErrMessage(e, messageKey, null);

            consumeResult = Action.ReconsumeLater;
        }

        return consumeResult;

    }
}
