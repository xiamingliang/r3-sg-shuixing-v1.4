package com.jackrain.nea.sg.basic.services;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.Feature;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Maps;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.psext.api.PsSkuGroupQureyCmd;
import com.jackrain.nea.psext.model.table.PsCSkugroup;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.mapper.SgBGroupStorageMapper;
import com.jackrain.nea.sg.basic.mapper.SgBStorageMapper;
import com.jackrain.nea.sg.basic.model.request.SgGroupQueryRequest;
import com.jackrain.nea.sg.basic.model.request.SgGroupStorageQueryRequest;
import com.jackrain.nea.sg.basic.model.result.SgGroupStorageQueryResult;
import com.jackrain.nea.sg.basic.model.table.SgBGroupStorage;
import com.jackrain.nea.sg.basic.model.table.SgBStorage;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.query.DefaultWebEvent;
import com.jackrain.nea.web.query.QuerySession;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.dubbo.config.annotation.Reference;
import org.assertj.core.util.Lists;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 福袋生成-获取福库存数量、实际条码库存数量
 *
 * @author 舒威
 * @since 2019/7/12
 * create at : 2019/7/12 17:28
 */
@Slf4j
@Component
public class SgGroupStorageQueryService {

    @Autowired
    private SgBStorageMapper storageMapper;

    @Autowired
    private SgBGroupStorageMapper sgBGroupStorageMapper;

    @Autowired
    private SgBGroupStorageMapper groupStorageMapper;

    @Reference(version = "1.0", group = "ps-ext")
    private PsSkuGroupQureyCmd psSkuGroupQureyCmd;

    public ValueHolderV14<List<SgGroupStorageQueryResult>> queryGroupStorage(List<SgGroupStorageQueryRequest> skuGroupList) {
        if (log.isDebugEnabled()) {
            log.debug("Start SgGroupStorageQueryService.ReceiveParams:request:{};", JSONObject.toJSONString(skuGroupList));
        }

        if (CollectionUtils.isEmpty(skuGroupList)) {
            AssertUtils.logAndThrow("虚拟条码不能为空！");
        }

        ValueHolderV14<List<SgGroupStorageQueryResult>> v14 = new ValueHolderV14<>();
        v14.setCode(ResultCode.SUCCESS);
        v14.setMessage("success");
        long startTime = System.currentTimeMillis();

        List<SgGroupStorageQueryResult> results = Lists.newArrayList();
        //遍历虚拟条码-批量
        for (SgGroupStorageQueryRequest skuGroupRequest : skuGroupList) {
            Long id = skuGroupRequest.getId();
            List<Long> storeIds = skuGroupRequest.getStoreIds();
            AssertUtils.isTrue((id != null && CollectionUtils.isNotEmpty(storeIds)), "虚拟条码和逻辑仓不能为空！");
            //按逻辑仓遍历
            for (Long storeId : storeIds) {
                SgGroupStorageQueryResult result = new SgGroupStorageQueryResult();
                HashMap<Long, BigDecimal> sku = Maps.newHashMap();
                //查询虚拟条码库存
                List<SgBGroupStorage> groupStorages = groupStorageMapper.selectList(new QueryWrapper<SgBGroupStorage>().lambda()
                        .eq(SgBGroupStorage::getPsCSkuId, id)
                        .eq(SgBGroupStorage::getCpCStoreId, storeId)
                        .eq(SgBGroupStorage::getIsactive, SgConstants.IS_ACTIVE_Y));
                if (CollectionUtils.isEmpty(groupStorages)) {
                    log.debug("虚拟条码" + id + "," + storeId + "沒有库存记录！");
                    continue;
                } else {
                    result.setId(id);
                    result.setCpCStoreId(storeId);
                    result.setQtyStorage(groupStorages.get(0).getQtyStorage());
                }
                //查询实际条码库存
                List<Long> skuIds = skuGroupRequest.getSkuIds();
                if (CollectionUtils.isEmpty(skuIds)) {
                    log.debug("虚拟条码" + id + "," + storeId + "沒有实际条码！");
                } else {
                    for (Long skuId : skuIds) {
                        List<SgBStorage> storages = storageMapper.selectList(new QueryWrapper<SgBStorage>().lambda()
                                .eq(SgBStorage::getPsCSkuId, skuId)
                                .eq(SgBStorage::getCpCStoreId, storeId)
                                .eq(SgBStorage::getIsactive, SgConstants.IS_ACTIVE_Y));
                        if (CollectionUtils.isEmpty(storages)) {
                            log.debug("实际条码" + skuId + "," + storeId + "沒有库存记录！");
                        } else {
                            sku.put(skuId, storages.get(0).getQtyAvailable());
                        }
                    }
                }
                result.setSku(sku);
                results.add(result);
            }
        }

        v14.setData(results);

        if (log.isDebugEnabled()) {
            log.debug("Finish SgGroupStorageQueryService.ReturnResults:return:{} spend time:{}ms;",
                    v14, System.currentTimeMillis() - startTime);
        }
        return v14;
    }

    /**
     * 查询组合商品库存信息
     *
     * @param session
     * @return
     */
    public ValueHolderV14<List<SgGroupQueryRequest>> queryGroupStorageById(QuerySession session) {
        ValueHolderV14 valueHolderV14 = new ValueHolderV14();
        DefaultWebEvent event = session.getEvent();
        JSONObject param = JSON.parseObject(JSON.toJSONStringWithDateFormat(event.getParameterValue("param"),
                "yyyy-MM-dd HH:mm:ss", SerializerFeature.WriteMapNullValue), Feature.OrderedField);
        if (log.isDebugEnabled()) {
            log.debug("SgCombinedStorageQueryService——param ==>" + param.toJSONString());
        }
        Long objid = param.getLong("objid");
        try {
            if (objid != null && objid > 0) {
                SgBGroupStorage sgBGroupStorage = sgBGroupStorageMapper.selectById(objid);
                if (log.isDebugEnabled()) {
                    log.debug("根据objid【" + objid + "】查询组合商品库存信息：" + sgBGroupStorage.toString());
                }
                //逻辑仓ID
                Long cpCStoreId = sgBGroupStorage.getCpCStoreId();
                List<Long> storeIds = new ArrayList<Long>();
                //商品编码
                String psCProEcode = sgBGroupStorage.getPsCProEcode();
                List<String> proEcodes = new ArrayList<String>();
                //条码
                String psCSkuEcode = sgBGroupStorage.getPsCSkuEcode();
                if (cpCStoreId != null && psCProEcode != null && psCSkuEcode != null) {
                    storeIds.add(cpCStoreId);
                    proEcodes.add(psCProEcode);
                    List<PsCSkugroup> psCSkugroups = psSkuGroupQureyCmd.psSkuGroupQurey(cpCStoreId, psCProEcode, psCSkuEcode);
                    if (log.isDebugEnabled()) {
                        log.debug("逻辑仓【" + sgBGroupStorage.getCpCStoreEname() + "】下实际条码信息" + psCSkugroups);
                    }
                    if (CollectionUtils.isNotEmpty(psCSkugroups)) {
                        List<String> psCSkuEcodes = psCSkugroups.stream().map(PsCSkugroup::getPsCSkuEcode).collect(Collectors.toList());
                        List<String> psCProEcodes = psCSkugroups.stream().map(PsCSkugroup::getPsCProEcode).collect(Collectors.toList());
                        List<SgBStorage> resultList = storageMapper.selectList(
                                new QueryWrapper<SgBStorage>().lambda()
                                        .in(!CollectionUtils.isEmpty(storeIds), SgBStorage::getCpCStoreId, storeIds)
                                        .in(!CollectionUtils.isEmpty(psCProEcodes), SgBStorage::getPsCProEcode, psCProEcodes)
                                        .in(!CollectionUtils.isEmpty(psCSkuEcodes), SgBStorage::getPsCSkuEcode, psCSkuEcodes)
                        );
                        if (log.isDebugEnabled()) {
                            log.debug("组合商品【" + sgBGroupStorage.getPsCProEname() + "】在当前逻辑仓" + cpCStoreId + "下所有实际商品的库存信息" + resultList);
                        }
                        if (CollectionUtils.isNotEmpty(resultList)) {
                            List<SgGroupQueryRequest> sgGroupQueryRequestList = setGroupInfo(resultList);
                            valueHolderV14.setCode(0);
                            valueHolderV14.setMessage("查询成功");
                            valueHolderV14.setData(sgGroupQueryRequestList);
                            return valueHolderV14;
                        }
                    }
                }
            }
            valueHolderV14.setCode(-1);
            valueHolderV14.setMessage("查询信息失败");
        } catch (Exception e) {
            valueHolderV14.setCode(-1);
            valueHolderV14.setMessage("根据逻辑仓ID和商品编码查询组合商品档案信息异常：" + e.getMessage());
            log.error("异常信息："+e.getMessage());
        }
        return valueHolderV14;
    }

    /**
     * 封装前端展示数据
     *
     * @param sgBStorages
     * @return
     */
    public List<SgGroupQueryRequest> setGroupInfo(List<SgBStorage> sgBStorages) {
        List<SgGroupQueryRequest> sgGroupQueryRequestList = new ArrayList<SgGroupQueryRequest>();
        for (SgBStorage sgBStorage : sgBStorages) {
            SgGroupQueryRequest sgGroupQueryRequest = new SgGroupQueryRequest();
            BeanUtils.copyProperties(sgBStorage, sgGroupQueryRequest);
            sgGroupQueryRequestList.add(sgGroupQueryRequest);
        }
        return sgGroupQueryRequestList;
    }
}
