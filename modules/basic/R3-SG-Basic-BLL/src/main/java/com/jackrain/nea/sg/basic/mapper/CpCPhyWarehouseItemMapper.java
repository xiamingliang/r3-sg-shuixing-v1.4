package com.jackrain.nea.sg.basic.mapper;

import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.basic.model.table.CpCPhyWarehouseItem;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface CpCPhyWarehouseItemMapper extends ExtentionMapper<CpCPhyWarehouseItem> {
}