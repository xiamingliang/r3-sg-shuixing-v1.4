package com.jackrain.nea.sg.basic.logic;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.config.SgStorageControlConfig;
import com.jackrain.nea.sg.basic.mapper.SgBStorageMapper;
import com.jackrain.nea.sg.basic.model.request.SgRedisStorageQueryModel;
import com.jackrain.nea.sg.basic.model.request.SgStoreWithPrioritySearchItemRequest;
import com.jackrain.nea.sg.basic.model.request.StStockPriorityRequest;
import com.jackrain.nea.sg.basic.model.result.SgRedisStorageQueryResult;
import com.jackrain.nea.sg.basic.model.result.SgStoreWithPrioritySearchItemResult;
import com.jackrain.nea.sg.basic.model.result.SgStoreWithPrioritySearchResult;
import com.jackrain.nea.sg.basic.model.table.SgBStorage;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/4/23 15:22
 */
@Component
@Slf4j
public class SgStorageQtyCalculateLogic {

    @Autowired
    private SgBStorageMapper sgBStorageMapper;

    @Autowired
    private SgRedisStorageLogic sgRedisStorageLogic;

    @Autowired
    private SgStorageControlConfig sgStorageControlConfig;

    /**
     * 根据优先级获取库存占用计划
     *
     * @param itemRequest
     * @param loginUser
     * @return ValueHolderV14<SgStoreWithPrioritySearchResult>
     */
    public ValueHolderV14<SgStoreWithPrioritySearchResult> getSgStoreOccupyPlan(
            SgStoreWithPrioritySearchItemRequest itemRequest,
            User loginUser) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgStorageQtyCalculateLogic.getSgStoreOccupyPlan. ReceiveParams:itemRequest {} loginUser {};"
                    , JSONObject.toJSONString(itemRequest), JSONObject.toJSONString(loginUser));
        }

        ValueHolderV14<SgStoreWithPrioritySearchResult> holder = new ValueHolderV14<>();
        SgStoreWithPrioritySearchResult result = new SgStoreWithPrioritySearchResult();
        List<SgStoreWithPrioritySearchItemResult> itemResultList = new ArrayList<>();
        List<StStockPriorityRequest> priorityList = itemRequest.getPriorityList();
        SgStoreWithPrioritySearchItemResult outOfStock = null;
        HashMap<String, SgRedisStorageQueryResult> redisQueryResult = null;
        String redisKey;

        //根据虚拟仓优先级列表顺序排序
        Collections.sort(priorityList);

        //剩余未占用数量
        BigDecimal remainChangeQty = itemRequest.getQtyChange();

        boolean isRedisFunction = sgStorageControlConfig.isRedisFunction();

        if (isRedisFunction) {

            List<SgRedisStorageQueryModel> sgRedisStorageQueryList = new ArrayList<>();

            for (StStockPriorityRequest stockPriority : priorityList) {

                SgRedisStorageQueryModel queryModel = new SgRedisStorageQueryModel();
                queryModel.setCpCStoreId(stockPriority.getSupplyStoreId());
                queryModel.setPsCSkuId(itemRequest.getPsCSkuId());

                sgRedisStorageQueryList.add(queryModel);

            }

            ValueHolderV14<HashMap<String, SgRedisStorageQueryResult>> queryResult =
                    sgRedisStorageLogic.queryStorage(sgRedisStorageQueryList, loginUser);

            redisQueryResult = queryResult.getData();

        }

        //遍历逻辑仓优先级
        for (StStockPriorityRequest stockPriority : priorityList) {

            if (remainChangeQty.compareTo(BigDecimal.ZERO) > 0) {

                SgBStorage updateRecord;
                redisKey = stockPriority.getSupplyStoreId() + ":" + itemRequest.getPsCSkuId();

                if (redisQueryResult != null &&
                        redisQueryResult.containsKey(redisKey)) {

                    updateRecord = new SgBStorage();
                    BeanUtils.copyProperties(redisQueryResult.get(redisKey), updateRecord);

                } else {

                     updateRecord = sgBStorageMapper.selectOne(
                            new QueryWrapper<SgBStorage>().lambda()
                                    .eq(SgBStorage::getCpCStoreId, stockPriority.getSupplyStoreId())
                                    .eq(SgBStorage::getPsCSkuId, itemRequest.getPsCSkuId()));

                }

                if (log.isDebugEnabled()) {
                    log.debug("SgStorageQtyCalculateLogic.getSgStoreOccupyPlan. ReturnResult:updateRecord {};"
                            , JSONObject.toJSONString(updateRecord));
                }

                // 允许负库存 或 可用库存 > 0
                if (updateRecord != null && updateRecord.getQtyAvailable() != null &&
                        (stockPriority.isNegativeAvailable() ||
                                updateRecord.getQtyAvailable().compareTo(BigDecimal.ZERO) > 0)
                        ) {

                    SgStoreWithPrioritySearchItemResult itemResult = new SgStoreWithPrioritySearchItemResult();
                    itemResult.setCpCStoreId(stockPriority.getSupplyStoreId());
                    itemResult.setCpCStoreEcode(stockPriority.getSupplyStoreEcode());
                    itemResult.setCpCStoreEname(stockPriority.getSupplyStoreEname());
                    itemResult.setPsCSkuId(itemRequest.getPsCSkuId());
                    itemResult.setRank(stockPriority.getPriority());
                    itemResult.setSourceItemId(itemRequest.getSourceItemId());

                    //判断当前店仓的可用库存是否充足 或 允许负库存
                    if (stockPriority.isNegativeAvailable() ||
                            updateRecord.getQtyAvailable().compareTo(remainChangeQty) >= 0) {
                        //可用充足的情况下，使用单据全量数据进行占用
                        itemResult.setQtyPreout(remainChangeQty);
                        remainChangeQty = remainChangeQty.subtract(remainChangeQty);
                    } else {
                        //可用不足的情况下，使用库存可用量进行占用
                        itemResult.setQtyPreout(updateRecord.getQtyAvailable());
                        remainChangeQty = remainChangeQty.subtract(updateRecord.getQtyAvailable());
                    }

                    if (outOfStock == null) {
                        outOfStock = new SgStoreWithPrioritySearchItemResult();
                        BeanUtils.copyProperties(itemResult, outOfStock);
                    }

                    itemResultList.add(itemResult);

                    if (remainChangeQty.compareTo(BigDecimal.ZERO) <= 0) {
                        break;
                    }

                } else if (updateRecord == null && stockPriority.isNegativeAvailable()) {

                    SgStoreWithPrioritySearchItemResult itemResult = new SgStoreWithPrioritySearchItemResult();
                    itemResult.setCpCStoreId(stockPriority.getSupplyStoreId());
                    itemResult.setCpCStoreEcode(stockPriority.getSupplyStoreEcode());
                    itemResult.setCpCStoreEname(stockPriority.getSupplyStoreEname());
                    itemResult.setPsCSkuId(itemRequest.getPsCSkuId());
                    itemResult.setRank(stockPriority.getPriority());
                    itemResult.setSourceItemId(itemRequest.getSourceItemId());
                    itemResult.setQtyPreout(remainChangeQty);
                    itemResultList.add(itemResult);

                    remainChangeQty = remainChangeQty.subtract(remainChangeQty);

                    break;

                }
            }

        }

        //剩余未占用数量>0的情况下，标记缺货
        if (remainChangeQty.compareTo(BigDecimal.ZERO) > 0) {
            List<SgStoreWithPrioritySearchItemResult> outStockItemList = new ArrayList<>();

            if (outOfStock == null) {

                outOfStock = new SgStoreWithPrioritySearchItemResult();

                if (!CollectionUtils.isEmpty(priorityList)) {
                    outOfStock.setCpCStoreId(priorityList.get(0).getSupplyStoreId());
                    outOfStock.setCpCStoreEcode(priorityList.get(0).getSupplyStoreEcode());
                    outOfStock.setCpCStoreEname(priorityList.get(0).getSupplyStoreEname());
                    outOfStock.setRank(priorityList.get(0).getPriority());
                }

                outOfStock.setPsCSkuId(itemRequest.getPsCSkuId());
                outOfStock.setSourceItemId(itemRequest.getSourceItemId());
                outOfStock.setQtyPreout(itemRequest.getQtyChange());

            }

            outOfStock.setQtyOutOfStock(remainChangeQty);
            outStockItemList.add(outOfStock);
            result.setOutStockItemList(outStockItemList);
            result.setPreoutUpdateResult(SgConstantsIF.PREOUT_RESULT_OUT_STOCK);
        }

        result.setItemResultList(itemResultList);
        holder.setData(result);

        if (log.isDebugEnabled()) {
            log.debug("Finish SgStorageQtyCalculateLogic.getSgStoreOccupyPlan. ReturnResult:param:{} remainChangeQty:{};"
                    , JSONObject.toJSONString(result), remainChangeQty);
        }

        holder.setCode(ResultCode.SUCCESS);
        holder.setMessage(Resources.getMessage("根据优先级获取库存占用计划成功！", loginUser.getLocale()));

        return holder;

    }

}
