package com.jackrain.nea.sg.basic.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.r3.mq.exception.SendMqException;
import com.jackrain.nea.r3.mq.util.R3MqSendHelper;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.config.SgStorageMqConfig;
import com.jackrain.nea.sg.basic.mapper.SgBGroupStorageMapper;
import com.jackrain.nea.sg.basic.model.request.SgGroupStorageCleanRequest;
import com.jackrain.nea.sg.basic.model.request.SgGroupStorageVirtualSkuRequest;
import com.jackrain.nea.sg.basic.model.table.SgBGroupStorage;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.basic.utils.StorageUtils;
import com.jackrain.nea.sg.oms.model.request.SgChannelStorageOmsBufferMessageItemRequest;
import com.jackrain.nea.sg.oms.model.request.SgChannelStorageOmsBufferMessageRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * @author: 舒威
 * @since: 2019/7/14
 * create at : 2019/7/14 15:20
 */
@Slf4j
@Component
public class SgGroupStorageCleanService {

    @Autowired
    private SgBGroupStorageMapper groupStorageMapper;

    @Autowired
    private R3MqSendHelper r3MqSendHelper;

    @Autowired
    private SgStorageMqConfig mqConfig;

    public ValueHolderV14<Integer> cleanGroupStorage(SgGroupStorageCleanRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("Start SgGroupStorageCleanService.ReceiveParams:request:{};", JSONObject.toJSONString(request));
        }

        User loginUser = request.getLoginUser();
        AssertUtils.notNull(loginUser, "用户未登录！");

        List<SgGroupStorageVirtualSkuRequest> virtualSkuRequests = request.getVirtualSkuRequests();
        if (CollectionUtils.isEmpty(virtualSkuRequests)) {
            AssertUtils.logAndThrow("虚拟条码不能为空！");
        }

        ValueHolderV14<Integer> v14 = new ValueHolderV14<>();
        v14.setCode(ResultCode.SUCCESS);
        v14.setMessage("success");
        long startTime = System.currentTimeMillis();
        int updateCount = 0;

        List<SgChannelStorageOmsBufferMessageItemRequest> itemList = Lists.newArrayList();
        for (SgGroupStorageVirtualSkuRequest cleanRequest : virtualSkuRequests) {
            Long id = cleanRequest.getId();
            Long cpCStoreId = cleanRequest.getCpCStoreId();
            AssertUtils.isTrue(id != null && cpCStoreId != null, "虚拟条码和逻辑仓不能为空！");

            List<SgBGroupStorage> groupStorages = groupStorageMapper.selectList(new QueryWrapper<SgBGroupStorage>().lambda()
                    .eq(SgBGroupStorage::getPsCSkuId, id)
                    .eq(SgBGroupStorage::getCpCStoreId, cpCStoreId)
                    .eq(SgBGroupStorage::getIsactive, SgConstants.IS_ACTIVE_Y));
            if (CollectionUtils.isNotEmpty(groupStorages)) {
                BigDecimal qtyChange = groupStorages.get(0).getQtyStorage();
                SgBGroupStorage groupStorage = new SgBGroupStorage();
                groupStorage.setQtyStorage(BigDecimal.ZERO);
                groupStorage.setIsactive(SgConstants.IS_ACTIVE_N);
                groupStorage.setModifierename(loginUser.getEname());
                StorageUtils.setBModelDefalutDataByUpdate(groupStorage, loginUser);
                int update = groupStorageMapper.update(groupStorage, new UpdateWrapper<SgBGroupStorage>().lambda()
                        .eq(SgBGroupStorage::getPsCSkuId, id)
                        .eq(SgBGroupStorage::getCpCStoreId, cpCStoreId)
                        .eq(SgBGroupStorage::getIsactive, SgConstants.IS_ACTIVE_Y));
                if (update > 0) {
                    updateCount += update;
                    SgChannelStorageOmsBufferMessageItemRequest newItem = new SgChannelStorageOmsBufferMessageItemRequest();
                    newItem.setCpCStoreId(cpCStoreId);
                    newItem.setPsCSkuId(id);
                    newItem.setQtyChange(qtyChange);
                    itemList.add(newItem);
                }
            }
        }

        //同步渠道库存
        if (CollectionUtils.isNotEmpty(itemList)) {
            List<SgChannelStorageOmsBufferMessageRequest> list = Lists.newArrayList();
            SgChannelStorageOmsBufferMessageRequest channelStorageRequest = new SgChannelStorageOmsBufferMessageRequest();
            channelStorageRequest.setItemList(itemList);
            channelStorageRequest.setChangeTime(new Date());
            channelStorageRequest.setCpCshopId(-1L);
            channelStorageRequest.setBillNo("组合商品库存" + RandomStringUtils.randomAlphanumeric(6));
            list.add(channelStorageRequest);
            HashMap<String, List<SgChannelStorageOmsBufferMessageRequest>> map = Maps.newHashMap();
            map.put("param", list);
            String body = JSONObject.toJSONString(map);
            String msgKey = SgConstants.MSG_KEY_HEAD_STORAGE_TO_CHANNEL + System.currentTimeMillis();
            String tag = SgConstantsIF.MSG_TAG_GROUP_STORAGE_TO_CHANNEL;
            log.debug("GroupStorageCalculate Body:" + body + ",MsgKey:" + msgKey + ",Tag:" + tag + " ;");
            try {
                r3MqSendHelper.sendMessage(mqConfig.getChannelSynchConfigName(), body, mqConfig.getChannelSynchTopic(), tag, msgKey);
            } catch (SendMqException e) {
                e.printStackTrace();
            }

        }

        v14.setData(updateCount);

        if (log.isDebugEnabled()) {
            log.debug("Finish SgGroupStorageCleanService.ReturnResults:return:{} spend time:{}ms;",
                    v14.toJSONObject(), System.currentTimeMillis() - startTime);
        }
        return v14;
    }
}
