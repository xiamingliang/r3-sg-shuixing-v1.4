package com.jackrain.nea.sg.basic.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.data.basic.services.BasicCpQueryService;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.config.SgStorageBoxConfig;
import com.jackrain.nea.sg.basic.logic.SgStorageQtyCalculateLogic;
import com.jackrain.nea.sg.basic.mapper.CpStoreMapper;
import com.jackrain.nea.sg.basic.model.request.SgQtyStorageByWareCalcItemRequest;
import com.jackrain.nea.sg.basic.model.request.SgQtyStorageByWareCalcRequest;
import com.jackrain.nea.sg.basic.model.request.SgStoreWithPrioritySearchItemRequest;
import com.jackrain.nea.sg.basic.model.request.StStockPriorityRequest;
import com.jackrain.nea.sg.basic.model.result.SgQtyStorageByWareCalcItemResult;
import com.jackrain.nea.sg.basic.model.result.SgQtyStorageByWareCalcResult;
import com.jackrain.nea.sg.basic.model.result.SgStoreWithPrioritySearchItemResult;
import com.jackrain.nea.sg.basic.model.result.SgStoreWithPrioritySearchResult;
import com.jackrain.nea.sg.basic.model.table.CpCStore;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @Description: 逻辑仓库存计算相关类
 * @Author: chenb
 * @Date: 2019/4/23 12:45
 */
@Component
@Slf4j
public class SgStorageQtyCalculateService {

    @Autowired
    private CpStoreMapper cpStoreMapper;

    @Autowired
    private SgStorageQtyCalculateLogic sgStorageQtyCalculateLogic;

    @Autowired
    private BasicCpQueryService basicCpQueryService;

    @Autowired
    private SgStorageBoxConfig sgStorageBoxConfig;

    @Autowired
    private SgTeusStorageQtyCalculateService sgTeusStorageQtyCalculateService;

    /**
     * 根据实体仓计算其下的各逻辑仓的调整数量
     * 默认调整主逻辑仓
     *
     * @param request
     * @return ValueHolderV14<SgQtyStorageByWareCalcResult>
     */
    public ValueHolderV14<SgQtyStorageByWareCalcResult> calcSgQtyStorageByWare(SgQtyStorageByWareCalcRequest request) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgStorageQtyCalculateService.calcSgQtyStorageByWare. ReceiveParams:request="
                    + JSONObject.toJSONString(request) + ";");
        }

        ValueHolderV14<SgQtyStorageByWareCalcResult> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        ValueHolderV14<SgStoreWithPrioritySearchResult> searchResult;
        SgQtyStorageByWareCalcResult calcResult = new SgQtyStorageByWareCalcResult();
        List<SgStoreWithPrioritySearchItemResult> searchResultList;
        List<SgQtyStorageByWareCalcItemResult> itemResultList = new ArrayList<>();
        List<StStockPriorityRequest> priorityList = new ArrayList<>();
        SgQtyStorageByWareCalcItemResult itemResult;
        String errorSkuEcode = "";

        User loginUser = request.getLoginUser();

        //参数检查
        holder = checkServiceParam(request, loginUser);
        if (ResultCode.FAIL == holder.getCode()) {
            return holder;
        }

        //根据实体仓获取逻辑仓信息，按主仓，逻辑仓ID降序
        List<CpCStore> storeList = new ArrayList<>();

        //判断是否指定逻辑仓
        if (request.getSupplyStoreId() != null) {

            CpCStore supplyStore = new CpCStore();
            supplyStore.setId(request.getSupplyStoreId());
            supplyStore.setCpCStoreEcode(request.getSupplyStoreEcode());
            supplyStore.setCpCStoreEname(request.getSupplyStoreEname());
            storeList.add(supplyStore);

        } else {

            if (SgConstantsIF.TRADE_MARK_CC.equals(request.getTradeMark())) {

                storeList.addAll(cpStoreMapper.selectList(new QueryWrapper<CpCStore>().lambda()
                        .eq(CpCStore::getCpCPhyWarehouseId, request.getSupplyPhyWarehouseId())
                        .eq(CpCStore::getStoretype, SgConstantsIF.STORE_TYPE_CC)
                        .orderByDesc(CpCStore::getId)));

                if (storeList.size() < 1) {
                    holder.setCode(ResultCode.FAIL);
                    holder.setMessage(Resources.getMessage("次品仓不存在，保存失败！", loginUser.getLocale(),
                            request.getSupplyPhyWarehouseEcode()));
                    return holder;
                }

            } else {

                storeList.addAll(cpStoreMapper.selectList(new QueryWrapper<CpCStore>().lambda()
                        .eq(CpCStore::getCpCPhyWarehouseId, request.getSupplyPhyWarehouseId())
                        .ne(CpCStore::getStoretype, SgConstantsIF.STORE_TYPE_CC)
                        .orderByDesc(CpCStore::getIsMainWarehouse, CpCStore::getId)));

            }

        }

        if (CollectionUtils.isEmpty(storeList)) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("根据实体仓获取逻辑仓信息失败！",
                    loginUser.getLocale(), request.getSupplyPhyWarehouseEcode()));
            return holder;
        }

        //遍历逻辑仓列表生成逻辑仓优先级列表
        for (int i = 0; i < storeList.size(); i++) {

            StStockPriorityRequest priorityRequest = new StStockPriorityRequest();
            priorityRequest.setSupplyStoreId(storeList.get(i).getId());
            priorityRequest.setSupplyStoreEcode(storeList.get(i).getCpCStoreEcode());
            priorityRequest.setSupplyStoreEname(storeList.get(i).getCpCStoreEname());

            if (request.getIsNegativeAvailable() != null) {
                priorityRequest.setNegativeAvailable(request.getIsNegativeAvailable());
            }

            //主仓->逻辑仓ID降序
            priorityRequest.setPriority(i);
            priorityList.add(priorityRequest);

        }

        if (log.isDebugEnabled()) {
            log.debug("SgStorageQtyCalculateService.calcSgQtyStorageByWare. ReturnResult:priorityList:{};"
                    , JSONObject.toJSONString(priorityList));
        }

        //遍历明细信息试算在库数量变动计划
        for (SgQtyStorageByWareCalcItemRequest itemRequest : request.getItemList()) {

            if (itemRequest.getQtyChange().compareTo(BigDecimal.ZERO) < 0) {

                //变动数量为负的情况下
                SgStoreWithPrioritySearchItemRequest searchRequest = new SgStoreWithPrioritySearchItemRequest();
                searchRequest.setPsCSkuId(itemRequest.getPsCSkuId());
                //调用【根据优先级获取库存占用计划】的数量必须是正数，需取绝对值
                searchRequest.setQtyChange(itemRequest.getQtyChange().abs());
                searchRequest.setPriorityList(priorityList);

                //调用【根据优先级获取库存占用计划】试算在库数量变动计划
                searchResult = sgStorageQtyCalculateLogic.getSgStoreOccupyPlan(searchRequest,
                        loginUser);

                searchResultList = searchResult.getData().getItemResultList();

                if (!CollectionUtils.isEmpty(searchResultList)) {

                    for (SgStoreWithPrioritySearchItemResult searchItem : searchResultList) {
                        itemResult = new SgQtyStorageByWareCalcItemResult();
                        itemResult.setCpCStoreId(searchItem.getCpCStoreId());
                        itemResult.setCpCStoreEcode(searchItem.getCpCStoreEcode());
                        itemResult.setCpCStoreEname(searchItem.getCpCStoreEname());
                        itemResult.setPsCSkuId(searchItem.getPsCSkuId());
                        //【根据优先级获取库存占用计划】返回的数量是正数，库存调整单需取反操作
                        itemResult.setQtyStorage(searchItem.getQtyPreout().negate());
                        itemResultList.add(itemResult);
                    }

                }

                if (SgConstantsIF.PREOUT_RESULT_SUCCESS != searchResult.getData().getPreoutUpdateResult()) {
                    calcResult.setChangeUpdateResult(searchResult.getData().getPreoutUpdateResult());
                    errorSkuEcode = errorSkuEcode + itemRequest.getPsCSkuEcode() + " ";
                }

            } else {
                //变动数量为正的情况下，直接调整为主逻辑仓或ID最大的逻辑仓
                itemResult = new SgQtyStorageByWareCalcItemResult();

                itemResult.setCpCStoreId(storeList.get(0).getId());
                itemResult.setCpCStoreEcode(storeList.get(0).getCpCStoreEcode());
                itemResult.setCpCStoreEname(storeList.get(0).getCpCStoreEname());
                itemResult.setPsCSkuId(itemRequest.getPsCSkuId());
                itemResult.setQtyStorage(itemRequest.getQtyChange());

                itemResultList.add(itemResult);
            }

        }

        if (SgConstantsIF.PREOUT_RESULT_SUCCESS != calcResult.getChangeUpdateResult()) {

            if (SgConstantsIF.TRADE_MARK_CC.equals(request.getTradeMark())) {
                holder.setMessage(Resources.getMessage("次品仓对应的店仓库存不足！", loginUser.getLocale(),
                        request.getSupplyPhyWarehouseEcode(),
                        errorSkuEcode));
            } else {
                holder.setMessage(Resources.getMessage("实体仓对应的店仓库存不足！", loginUser.getLocale(),
                        request.getSupplyPhyWarehouseEcode(),
                        errorSkuEcode));
            }

        }

        //根据实体仓计算其下的各逻辑仓的调整数量
        if (sgStorageBoxConfig.getBoxEnable() && !CollectionUtils.isEmpty(request.getTeusList())) {

            ValueHolderV14<SgQtyStorageByWareCalcResult> teusHolder = sgTeusStorageQtyCalculateService.calcSgQtyStorageByWare(request);

            if (ResultCode.SUCCESS != teusHolder.getCode()){
                holder.setCode(teusHolder.getCode());
            }

            holder.setMessage(teusHolder.getMessage().concat(" " + holder.getMessage()));

            if (teusHolder.getData() != null) {

                if (SgConstantsIF.PREOUT_RESULT_SUCCESS != teusHolder.getData().getChangeUpdateResult()) {
                    calcResult.setChangeUpdateResult(teusHolder.getData().getChangeUpdateResult());
                }

                calcResult.setTeusResultList(teusHolder.getData().getTeusResultList());
            }

        }

        calcResult.setItemResultList(itemResultList);
        holder.setData(calcResult);

        if (log.isDebugEnabled()) {
            log.debug("Finish SgStorageQtyCalculateService.calcSgQtyStorageByWare. ReturnResult:holder:{};"
                    , JSONObject.toJSONString(holder));
        }

        return holder;

    }

    /**
     * @param request
     * @param loginUser
     * @return
     */
    private ValueHolderV14<SgQtyStorageByWareCalcResult> checkServiceParam(SgQtyStorageByWareCalcRequest request,
                                                                           User loginUser) {

        ValueHolderV14<SgQtyStorageByWareCalcResult> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");

        if (request == null) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("请求体为空！", loginUser.getLocale()));
            return holder;
        }

        if (request.getLoginUser() == null) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("操作用户信息不能为空！", loginUser.getLocale())
                    .concat(" " + holder.getMessage()));
        }

        if (request.getSupplyPhyWarehouseId() == null) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("实体仓信息不能为空！", loginUser.getLocale())
                    .concat(" " + holder.getMessage()));
        }

        if (CollectionUtils.isEmpty(request.getItemList())) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("明细信息不能为空！", loginUser.getLocale())
                    .concat(" " + holder.getMessage()));
        }

        for (SgQtyStorageByWareCalcItemRequest itemRequest : request.getItemList()) {

            if (itemRequest.getPsCSkuId() == null) {
                holder.setCode(ResultCode.FAIL);
                holder.setMessage(Resources.getMessage("条码信息不能为空！", loginUser.getLocale())
                        .concat(" " + holder.getMessage()));
            }

            if (itemRequest.getQtyChange() == null || itemRequest.getQtyChange().compareTo(BigDecimal.ZERO) == 0) {
                holder.setCode(ResultCode.FAIL);
                holder.setMessage(Resources.getMessage("条码数量不能为空！", loginUser.getLocale())
                        .concat(" " + holder.getMessage()));
            }

        }

        return holder;

    }
}
