package com.jackrain.nea.sg.basic.services;

import com.jackrain.nea.sg.basic.logic.SgPhyStorageQueryLogic;
import com.jackrain.nea.sg.basic.model.request.SgPhyStorageQueryRequest;
import com.jackrain.nea.sg.basic.model.table.SgBPhyStorage;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/4/26 10:35
 */
@Component
@Slf4j
public class SgPhyStorageQueryService {

    @Autowired
    private SgPhyStorageQueryLogic sgPhyStorageQueryLogic;

    public ValueHolderV14<List<SgBPhyStorage>> queryPhyStorage(SgPhyStorageQueryRequest request, User loginUser) {

        return sgPhyStorageQueryLogic.queryPhyStorage(request, null, loginUser);

    }
}
