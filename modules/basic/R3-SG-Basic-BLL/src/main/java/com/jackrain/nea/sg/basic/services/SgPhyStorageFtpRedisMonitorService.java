package com.jackrain.nea.sg.basic.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.redis.config.CusRedisTemplate;
import com.jackrain.nea.redis.util.RedisOpsUtil;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.config.SgStorageControlConfig;
import com.jackrain.nea.sg.basic.mapper.SgBPhyStorageChangeFtpMapper;
import com.jackrain.nea.sg.basic.model.request.SgPhyStorageFtpRedisMonitorRequest;
import com.jackrain.nea.sg.basic.model.result.SgPhyStorageFtpRedisMonitorResult;
import com.jackrain.nea.sg.basic.model.table.SgBPhyStorageChangeFtp;
import com.jackrain.nea.sg.basic.utils.StorageUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Set;

/**
 * @Description: 通用实体仓库流水存监控接口(Redis)
 * @Author: chenb
 * @Date: 2019/7/29 11:32
 */
@Component
@Slf4j
public class SgPhyStorageFtpRedisMonitorService {

    @Autowired
    SgStorageControlConfig sgStorageControlConfig;

    @Autowired
    private SgBPhyStorageChangeFtpMapper sgBPhyStorageChangeFtpMapper;


    public ValueHolderV14<SgPhyStorageFtpRedisMonitorResult> monitorRedisPhyStorageFtp(SgPhyStorageFtpRedisMonitorRequest request) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgPhyStorageFtpRedisMonitorService.monitorRedisPhyStorageFtp. ReceiveParams:request={};",
                    JSONObject.toJSONString(request));
        }

        long startTime = System.currentTimeMillis();

        ValueHolderV14<SgPhyStorageFtpRedisMonitorResult> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        CusRedisTemplate<String, String> redisTemplate = RedisOpsUtil.getStrRedisTemplate();
        Set<Object> phyStorageRedisKeys;
        Map<Object, Object> phystorageFtp;

        long totalSuccessNum = 0;
        long totalErrorNum = 0;

        User loginUser = request.getLoginUser();

        phyStorageRedisKeys = StorageUtils.scanRedisKeys(
                SgConstants.REDIS_KEY_PREFIX_PHY_STORAGE_FTP +
                        sdf.format(DateUtils.addHours(new Date(), -1)),
                1000);

        for (Object storeRedisKey : phyStorageRedisKeys) {

            phystorageFtp = redisTemplate.opsForHash().entries((String) storeRedisKey);

            for (Object phystorageFtpItem : phystorageFtp.values()) {

                String[] phystorageFtpParam = ((String) phystorageFtpItem).split(",");
                String[] phystorageFtpKey = phystorageFtpParam[0].split(":");
                String[] phystorageFtpIds = phystorageFtpParam[2].split("_");

                BigDecimal phystorageNum = new BigDecimal(phystorageFtpParam[1]);

                if (phystorageNum.compareTo(BigDecimal.ZERO) != 0) {
                    if (sgBPhyStorageChangeFtpMapper.selectCount(new QueryWrapper<SgBPhyStorageChangeFtp>().lambda()
                            .eq(SgBPhyStorageChangeFtp::getReserveVarchar01, storeRedisKey)
                            .eq(SgBPhyStorageChangeFtp::getBillId, Integer.valueOf(phystorageFtpIds[0]))
                            .eq(SgBPhyStorageChangeFtp::getBillItemId, Integer.valueOf(phystorageFtpIds[1]))
                            .eq(SgBPhyStorageChangeFtp::getCpCPhyWarehouseId, Integer.valueOf(phystorageFtpKey[2]))
                            .eq(SgBPhyStorageChangeFtp::getPsCSkuId, Integer.valueOf(phystorageFtpKey[3]))
                            .eq(SgBPhyStorageChangeFtp::getQtyChange, phystorageNum)) != 1) {

                        log.error("SgPhyStorageFtpRedisMonitorService.monitorRedisPhyStorageFtp. error :" +
                                        "实体仓在库库存流水记录缺失。单据ID, 明细ID, 实体仓ID:{}, 商品条码ID:{}, 变动数量{};",
                                phystorageFtpIds[0], phystorageFtpIds[1], phystorageFtpKey[2], phystorageFtpKey[3], phystorageNum);

                        totalErrorNum++;
                    } else {
                        totalSuccessNum++;
                    }
                }
            }
        }

        holder.setMessage(Resources.getMessage("Redis实体仓库存监控执行成功！",
                loginUser.getLocale(),
                totalSuccessNum, totalErrorNum));

        if (log.isDebugEnabled()) {
            log.debug("Finish SgPhyStorageFtpRedisMonitorService.monitorRedisPhyStorageFtp. ReturnResult:holder={} spend time:{}ms;",
                    JSONObject.toJSONString(holder), System.currentTimeMillis() - startTime);
        }

        return holder;
    }

}
