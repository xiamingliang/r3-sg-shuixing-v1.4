package com.jackrain.nea.sg.basic.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.redis.config.CusRedisTemplate;
import com.jackrain.nea.redis.util.RedisOpsUtil;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.config.SgStorageControlConfig;
import com.jackrain.nea.sg.basic.mapper.SgBPhyTeusStorageMapper;
import com.jackrain.nea.sg.basic.model.request.SgPhyTeusStorageRedisInitRequest;
import com.jackrain.nea.sg.basic.model.result.SgPhyTeusStorageRedisInitResult;
import com.jackrain.nea.sg.basic.model.table.SgBPhyTeusStorage;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description: 通用实体仓箱库存初始化接口(Redis)
 * @Author: chenb
 * @Date: 2019/7/29 11:32
 */
@Component
@Slf4j
public class SgPhyTeusStorageRedisInitService {

    @Autowired
    SgStorageControlConfig sgStorageControlConfig;

    @Autowired
    private SgBPhyTeusStorageMapper sgBPhyTeusStorageMapper;

    public ValueHolderV14<SgPhyTeusStorageRedisInitResult> initRedisPhyStorage(SgPhyTeusStorageRedisInitRequest request) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgPhyTeusStorageRedisInitService.initRedisPhyStorage. ReceiveParams:SgPhyTeusStorageRedisInitRequest={};",
                    JSONObject.toJSONString(request));
        }

        ValueHolderV14<SgPhyTeusStorageRedisInitResult> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        long totalSuccessNum = 0;

        holder = checkServiceParam(request, request.getLoginUser());

        if (ResultCode.FAIL == holder.getCode()) {
            return holder;
        }

        User loginUser = request.getLoginUser();

        CusRedisTemplate<String, String> redisTemplate = RedisOpsUtil.getStrRedisTemplate();
        DefaultRedisScript<List> redisScript = new DefaultRedisScript<>();
        redisScript.setLocation(new ClassPathResource("lua/InitRedisPhyTeusStorage.lua"));
        redisScript.setResultType(List.class);
        List<String> keyList;
        List<String> qtyList;

        List<Long> phyWarehouseIds = request.getPhyWarehouseIds();

        if (CollectionUtils.isEmpty(phyWarehouseIds)) {
            phyWarehouseIds = sgBPhyTeusStorageMapper.selectPhyStoragePhyWarehouseIds();
        }

        List<Long> proIds = request.getProIds();
        List<Long> teusIds = request.getTeusIds();
        List<String> proEcodes = request.getProEcodes();
        List<String> teusEcodes = request.getTeusEcodes();

        //获取满足条件的逻辑仓库存件数
        int totalQtty = sgBPhyTeusStorageMapper.selectCount(new QueryWrapper<SgBPhyTeusStorage>().lambda()
                .in(!CollectionUtils.isEmpty(phyWarehouseIds), SgBPhyTeusStorage::getCpCPhyWarehouseId, phyWarehouseIds)
                .in(!CollectionUtils.isEmpty(proIds), SgBPhyTeusStorage::getPsCProId, proIds)
                .in(!CollectionUtils.isEmpty(teusIds), SgBPhyTeusStorage::getPsCTeusId, teusIds)
                .in(!CollectionUtils.isEmpty(proEcodes), SgBPhyTeusStorage::getPsCProEcode, proEcodes)
                .in(!CollectionUtils.isEmpty(teusEcodes), SgBPhyTeusStorage::getPsCTeusEcode, teusEcodes)
        );

        //分页处理
        int pageSize = sgStorageControlConfig.getMaxQueryLimit();
        int listSize = totalQtty;
        int page = listSize / pageSize;

        if (listSize % pageSize != 0) {
            page++;
        }

        //分页批量更新
        for (int i = 0; i < page; i++) {

            /** 分页查询 **/
            PageHelper.startPage(i + 1, pageSize);
            List<SgBPhyTeusStorage> resultList = sgBPhyTeusStorageMapper.selectList(
                    new QueryWrapper<SgBPhyTeusStorage>().lambda().select(SgBPhyTeusStorage::getCpCPhyWarehouseId,
                            SgBPhyTeusStorage::getPsCTeusId, SgBPhyTeusStorage::getQtyStorage)
                            .in(!CollectionUtils.isEmpty(phyWarehouseIds), SgBPhyTeusStorage::getCpCPhyWarehouseId, phyWarehouseIds)
                            .in(!CollectionUtils.isEmpty(proIds), SgBPhyTeusStorage::getPsCProId, proIds)
                            .in(!CollectionUtils.isEmpty(teusIds), SgBPhyTeusStorage::getPsCTeusId, teusIds)
                            .in(!CollectionUtils.isEmpty(proEcodes), SgBPhyTeusStorage::getPsCProEcode, proEcodes)
                            .in(!CollectionUtils.isEmpty(teusEcodes), SgBPhyTeusStorage::getPsCTeusEcode, teusEcodes)
            );

            keyList = new ArrayList();
            qtyList = new ArrayList();

            if (!CollectionUtils.isEmpty(resultList)) {

                for (SgBPhyTeusStorage sgBPhyStorage : resultList) {

                    //LUA Redis键：sg:storage:逻辑仓ID:SKUID
                    keyList.add(SgConstants.REDIS_KEY_PREFIX_PHY_TEUS_STORAGE + sgBPhyStorage.getCpCPhyWarehouseId() +
                            ":" + sgBPhyStorage.getPsCTeusId());

                    //LUA参数:占用数量,在途数量,在库数量
                    qtyList.add(sgBPhyStorage.getQtyStorage().toString());
                }

            }

            if (log.isDebugEnabled()) {
                log.debug("SgPhyTeusStorageRedisInitService.initRedisPhyStorage. redisTemplate.execute.start. ReceiveParams:resultList:{};"
                        , JSONObject.toJSONString(resultList));
            }

            //调用逻辑仓库存初始化LUA脚本
            List result = redisTemplate.execute(redisScript, keyList, qtyList.toArray(new String[qtyList.size()]));

            if (log.isDebugEnabled()) {
                log.debug("SgPhyTeusStorageRedisInitService.initRedisPhyStorage. redisTemplate.execute.end. ReturnResult:result:{};"
                        , JSONObject.toJSONString(result));
            }

            if (!CollectionUtils.isEmpty(result) && (Long) result.get(0) != ResultCode.SUCCESS) {

                log.error("SgPhyTeusStorageRedisInitService.initRedisPhyStorage. Redis实体仓库存初始化失败！成功件数:{};"
                        , JSONObject.toJSONString(result.get(1)));

            }

            totalSuccessNum = totalSuccessNum + (Long) result.get(1);

            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }

        holder.setMessage(Resources.getMessage("Redis实体仓箱库存初始化成功！",
                loginUser.getLocale(),
                totalSuccessNum));

        return holder;
    }

    /**
     * @param request
     * @param loginUser
     * @return ValueHolderV14
     */
    private ValueHolderV14 checkServiceParam(SgPhyTeusStorageRedisInitRequest request,
                                             User loginUser) {

        ValueHolderV14 holder = new ValueHolderV14(ResultCode.SUCCESS, "");

        int maxQueryLimit = sgStorageControlConfig.getMaxQueryLimit();

        List<Long> proIds = request.getProIds();
        List<Long> teusIds = request.getTeusIds();
        List<String> proEcodes = request.getProEcodes();
        List<String> teusEcodes = request.getTeusEcodes();

        if (!CollectionUtils.isEmpty(proIds) &&
                proIds.size() > maxQueryLimit) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("商品编码ID的查询件数超过最大限制！",
                    loginUser.getLocale()));
        } else if (!CollectionUtils.isEmpty(teusIds) &&
                teusIds.size() > maxQueryLimit) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("箱ID的查询件数超过最大限制！",
                    loginUser.getLocale()));
        } else if (!CollectionUtils.isEmpty(proEcodes) &&
                proEcodes.size() > maxQueryLimit) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("商品编码的查询件数超过最大限制！",
                    loginUser.getLocale()));
        } else if (!CollectionUtils.isEmpty(teusEcodes) &&
                teusEcodes.size() > maxQueryLimit) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("箱号的查询件数超过最大限制！",
                    loginUser.getLocale()));
        }

        return holder;
    }
}
