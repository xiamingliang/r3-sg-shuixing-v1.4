package com.jackrain.nea.sg.basic.model.result;

import com.jackrain.nea.sg.basic.model.SgPhyTeusStorageUpdateCommonModel;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/3/13 15:53
 */
@Data
public class SgPhyTeusStorageUpdateCommonResult implements Serializable {

    //最近数据库操作结果
    private int countResult;
    //期初数量
    private BigDecimal qtyBegin;
    //期末数量
    private BigDecimal qtyEnd;
    //缺货明细详情
    private List<SgPhyTeusStorageUpdateCommonModel> outStockItemList;

    public SgPhyTeusStorageUpdateCommonResult() {
        this.countResult = 0;
    }

}
