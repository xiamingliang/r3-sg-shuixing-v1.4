package com.jackrain.nea.sg.basic.services;

import com.github.pagehelper.PageInfo;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.logic.SgRedisStorageLogic;
import com.jackrain.nea.sg.basic.logic.SgStorageQueryLogic;
import com.jackrain.nea.sg.basic.model.request.SgRedisStorageQueryRequest;
import com.jackrain.nea.sg.basic.model.request.SgStoragePageQueryRequest;
import com.jackrain.nea.sg.basic.model.request.SgStorageQueryRequest;
import com.jackrain.nea.sg.basic.model.request.SgSumStoragePageQueryRequest;
import com.jackrain.nea.sg.basic.model.result.SgRedisStorageQueryResult;
import com.jackrain.nea.sg.basic.model.result.SgSumStorageQueryResult;
import com.jackrain.nea.sg.basic.model.table.SgBStorage;
import com.jackrain.nea.sg.basic.model.table.SgBStorageInclPhy;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/3/7 16:38
 */
@Component
@Slf4j
public class SgStorageQueryService {

    @Autowired
    private SgStorageQueryLogic sgStorageQueryLogic;

    @Autowired
    private SgRedisStorageLogic sgRedisStorageLogic;

    public ValueHolderV14<List<SgBStorage>> queryStorage(SgStorageQueryRequest request, User loginUser) {

        return sgStorageQueryLogic.queryStorage(request, null, loginUser, Boolean.FALSE);

    }

    public ValueHolderV14<PageInfo<SgBStorage>> queryStoragePage(SgStoragePageQueryRequest request, User loginUser) {

        return sgStorageQueryLogic.queryStorage(request.getQueryRequest(), request.getPageRequest(), loginUser, Boolean.FALSE);

    }

    public ValueHolderV14<List<SgBStorage>> queryStorageExclZero(SgStorageQueryRequest request, User loginUser) {

        return sgStorageQueryLogic.queryStorage(request, null, loginUser, Boolean.TRUE);

    }

    public ValueHolderV14<List<SgBStorageInclPhy>> queryStorageInclPhy(SgStorageQueryRequest request, User loginUser) {

        return sgStorageQueryLogic.queryStorageInclPhy(request, null, loginUser);

    }

    public ValueHolderV14<List<SgSumStorageQueryResult>> querySumStorageGrpPhy(SgSumStoragePageQueryRequest request, User loginUser) {

        return sgStorageQueryLogic.querySumStorageGrpPhy(request.getQueryRequest(), request.getPageRequest(), loginUser);

    }

    public ValueHolderV14<List<SgRedisStorageQueryResult>> queryRedisStorage(SgRedisStorageQueryRequest request, User loginUser) {

        ValueHolderV14<List<SgRedisStorageQueryResult>> holder = new ValueHolderV14(ResultCode.SUCCESS, "");

        ValueHolderV14<HashMap<String, SgRedisStorageQueryResult>> queryResultMap =
                sgRedisStorageLogic.queryStorage(request.getQueryModels(), loginUser);

        Collection<SgRedisStorageQueryResult> valueCollection = queryResultMap.getData().values();

        List<SgRedisStorageQueryResult> queryResultList = new ArrayList<>(valueCollection);

        holder.setCode(queryResultMap.getCode());
        holder.setMessage(queryResultMap.getMessage());
        holder.setData(queryResultList);

        return holder;
    }

}
