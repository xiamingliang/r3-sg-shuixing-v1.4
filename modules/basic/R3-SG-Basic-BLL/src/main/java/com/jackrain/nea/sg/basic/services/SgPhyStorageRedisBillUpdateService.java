package com.jackrain.nea.sg.basic.services;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.config.SgStorageBoxConfig;
import com.jackrain.nea.sg.basic.model.request.SgPhyStorageSingleUpdateRequest;
import com.jackrain.nea.sg.basic.model.result.SgPhyStorageBatchUpdateResult;
import com.jackrain.nea.sg.basic.model.result.SgPhyTeusStorageBatchUpdateResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/6/1 12:51
 */
@Component
@Slf4j
public class SgPhyStorageRedisBillUpdateService {

    @Autowired
    private SgPhyStorageRedisUpdateService stoUpdService;

    @Autowired
    private SgPhyTeusStorageRedisUpdateService teusStoUpdService;

    @Autowired
    private SgPhyStorageRedisSynchService sgPhyStorageRedisSynchService;

    @Autowired
    private SgPhyTeusStorageRedisSynchService sgPhyTeusStorageRedisSynchService;

    @Autowired
    private SgStorageBoxConfig sgStorageBoxConfig;

    public ValueHolderV14<SgPhyStorageBatchUpdateResult> updatePhyStorageBill(SgPhyStorageSingleUpdateRequest request) {

        ValueHolderV14<SgPhyStorageBatchUpdateResult> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        ValueHolderV14<SgPhyTeusStorageBatchUpdateResult> teusHolder = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        SgPhyStorageBatchUpdateResult updateResult = new SgPhyStorageBatchUpdateResult();

        long startTime = System.currentTimeMillis();

        try {

            if (log.isDebugEnabled()) {
                log.debug("Start SgPhyStorageRedisBillUpdateService.updatePhyStorageBill. ReceiveParams:request:{};"
                        , JSONObject.toJSONString(request));
            }

            holder = stoUpdService.updatePhyStorageBill(request);

            updateResult = holder.getData();

            if (holder != null && ResultCode.SUCCESS == holder.getCode() && sgStorageBoxConfig.getBoxEnable() &&
                    (request.getBill() != null && !CollectionUtils.isEmpty(request.getBill().getTeusList()))) {

                teusHolder = teusStoUpdService.updatePhyStorageBill(request);

                if (teusHolder != null && teusHolder.getData() != null) {

                    //箱Redis更新流水键与SKU Redis更新流水键进行合并
                    updateResult.getRedisBillFtpKeyList().addAll(teusHolder.getData().getRedisBillFtpKeyList());

                    if (ResultCode.SUCCESS != teusHolder.getCode() || teusHolder.getData().getErrorBillItemQty() > 0) {
                        holder.setCode(ResultCode.FAIL);
                        holder.setMessage(teusHolder.getMessage().concat(" " + holder.getMessage()));
                    }

                    updateResult.setErrorBillItemQty(updateResult.getErrorBillItemQty() + teusHolder.getData().getErrorBillItemQty());

                }

            }

            if (holder != null && ResultCode.SUCCESS == holder.getCode() && holder.getData() != null) {

                if (!CollectionUtils.isEmpty(holder.getData().getRedisSynchMqItemList())) {

                    sgPhyStorageRedisSynchService.synchRedisToPhyStorage(request,
                            holder.getData().getRedisSynchMqItemList());

                }
            }

            if (teusHolder != null && ResultCode.SUCCESS == holder.getCode() && teusHolder.getData() != null) {

                if (!CollectionUtils.isEmpty(teusHolder.getData().getRedisSynchMqItemList())) {

                    sgPhyTeusStorageRedisSynchService.synchRedisToPhyStorage(request,
                            teusHolder.getData().getRedisSynchMqItemList());

                }
            }

        } catch (Exception e) {

            holder.setCode(ResultCode.FAIL);

            holder.setData(updateResult);
            holder.setMessage(e.getMessage().concat(" " + holder.getMessage()));

        }

        //更新失败的场景下，流水回滚
        if (holder != null && ResultCode.SUCCESS != holder.getCode()) {
            stoUpdService.rollbackPhyStorageBill(updateResult.getRedisBillFtpKeyList());
        }

        if (log.isDebugEnabled()) {
            log.debug("Finish SgPhyStorageRedisBillUpdateService.updatePhyStorageBill. ReturnResult:errorBillItemQty:{} billNo:{} spend time:{}ms;"
                    , JSONObject.toJSONString(holder.getData() == null ? 0 : holder.getData().getErrorBillItemQty()), request.getBill().getBillNo(), System.currentTimeMillis() - startTime);
        }

        return holder;

    }

}
