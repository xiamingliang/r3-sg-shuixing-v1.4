package com.jackrain.nea.sg.basic.model;

import com.jackrain.nea.sg.basic.model.request.SgStorageUpdateControlRequest;
import com.jackrain.nea.sys.domain.BaseModel;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/3/13 15:00
 */
@Data
public class SgPhyTeusStorageUpdateCommonModel extends BaseModel implements Comparable<SgPhyTeusStorageUpdateCommonModel> {


    /**
     * MQ幂等性防重锁
     */
    private String messageKey;
    /**
     * 库存变动类型
     */
    private Integer storageType;
    /**
     * 单据类型
     */
    private Integer billType;
    /**
     * 业务节点
     */
    private Long serviceNode;
    /**
     * 单据ID
     */
    private Long billId;
    /**
     * 单据编号
     */
    private String billNo;
    /**
     * 业务单据ID
     */
    private Long sourceBillId;
    /**
     * 业务单据编号
     */
    private String sourceBillNo;
    /**
     * 单据日期
     */
    private Date billDate;
    /**
     * 单据明细ID
     */
    private Long billItemId;
    /**
     * 变动日期
     */
    private Date changeDate;
    /**
     * 实体仓id
     */
    private Long cpCPhyWarehouseId;
    /**
     * 实体仓编码
     */
    private String cpCPhyWarehouseEcode;
    /**
     * 实体仓名称
     */
    private String cpCPhyWarehouseEname;
    /**
     * 商品id
     */
    private Long psCProId;
    /**
     * 商品编码
     */
    private String psCProEcode;
    /**
     * 商品名称
     */
    private String psCProEname;
    /**
     * 箱id
     */
    private Long psCTeusId;
    /**
     * 箱号
     */
    private String psCTeusEcode;
    /**
     * 配码id
     */
    private Long psCMatchsizeId;
    /**
     * 配码编码
     */
    private String psCMatchsizeEcode;
    /**
     * 配码名称
     */
    private String psCMatchsizeEname;
    /**
     * 规格1ID
     */
    private Long psCSpec1Id;
    /**
     * 规格1编码
     */
    private String psCSpec1Ecode;
    /**
     * 规格1名称
     */
    private String psCSpec1Ename;
    /**
     * 规格2ID
     */
    private Long psCSpec2Id;
    /**
     * 规格2编码
     */
    private String psCSpec2Ecode;
    /**
     * 规格2名称
     */
    private String psCSpec2Ename;
    /**
     * 期初数量
     */
    private BigDecimal qtyBegin;
    /**
     * 变动数量
     */
    private BigDecimal qtyChange;
    /**
     * 期末数量
     */
    private BigDecimal qtyEnd;
    /**
     * 库存锁
     */
    private String lockKey;
    /**
     * 创建人姓名
     */
    private String ownerename;
    /**
     * 修改人姓名
     */
    private String modifierename;
    /**
     * 明细控制类
     */
    private SgStorageUpdateControlRequest controlmodel;
    /**
     * Redis更新流水键
     */
    private String redisBillFtpKey;
    /**
     * 错误次数
     */
    private int errorTimes;
    /**
     * offset
     */
    private long offset;

    public SgPhyTeusStorageUpdateCommonModel() {
        errorTimes = 0;
        offset = 0;
    }

    @Override
    public int compareTo(SgPhyTeusStorageUpdateCommonModel o) {
        return o.toString().compareTo(this.toString());
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append(cpCPhyWarehouseId).append("_").append(psCTeusId).append("_").append(offset);
        return sb.toString();
    }
}
