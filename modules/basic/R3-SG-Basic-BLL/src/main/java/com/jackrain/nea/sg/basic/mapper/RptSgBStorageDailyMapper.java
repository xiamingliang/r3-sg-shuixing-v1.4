package com.jackrain.nea.sg.basic.mapper;

import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.basic.model.table.RptSgBPhyStorageDaily;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.mapping.StatementType;

@Mapper
public interface RptSgBStorageDailyMapper extends ExtentionMapper<RptSgBPhyStorageDaily> {

    /**
     * 调用逐日逻辑仓库存生成函数f_storage_daily
     *
     * @param v_date 逐日库存生成日期
     */
    @Select("{CALL f_storage_daily(#{v_date,mode=IN,jdbcType=VARCHAR})}")
    @Options(statementType = StatementType.CALLABLE)
    int generateStorageDailyFn(String v_date);

    /**
     * 调用逐日逻辑仓箱库存生成函数f_teus_storage_daily
     *
     * @param v_date 逐日库存生成日期
     */
    @Select("{CALL f_teus_storage_daily(#{v_date,mode=IN,jdbcType=VARCHAR})}")
    @Options(statementType = StatementType.CALLABLE)
    int generateTeusStorageDailyFn(String v_date);

}