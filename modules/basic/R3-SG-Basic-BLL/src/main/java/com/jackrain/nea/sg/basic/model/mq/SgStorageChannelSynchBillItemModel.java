package com.jackrain.nea.sg.basic.model.mq;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/4/29 19:43
 */
@Data
public class SgStorageChannelSynchBillItemModel implements Serializable {

    /** 单据编号 */
    private String billNo;
    /** 渠道ID */
    private Long cpCshopId;
    /** 店仓id */
    private Long cpCStoreId;
    /** 条码id */
    private Long psCSkuId;
    /** 变动数量 */
    private BigDecimal qtyChange;

}
