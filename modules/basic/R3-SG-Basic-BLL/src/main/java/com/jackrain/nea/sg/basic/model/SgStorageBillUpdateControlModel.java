package com.jackrain.nea.sg.basic.model;

import com.jackrain.nea.sg.basic.common.SgConstants;
import lombok.Data;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/5/31 14:12
 */
@Data
public class SgStorageBillUpdateControlModel {
    /**
     * 是否开启新事务
     */
    private int TransactionLevel;

    public SgStorageBillUpdateControlModel() {
        this.TransactionLevel = SgConstants.TRANSACTION_LEVEL_BILL;
    }
}
