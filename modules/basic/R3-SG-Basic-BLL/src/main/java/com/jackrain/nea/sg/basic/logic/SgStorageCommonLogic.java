package com.jackrain.nea.sg.basic.logic;

import com.jackrain.nea.ps.api.result.PsCProSkuResult;
import com.jackrain.nea.sg.basic.model.SgPhyStorageUpdateCommonModel;
import com.jackrain.nea.sg.basic.model.SgStorageUpdateCommonModel;

import java.util.HashMap;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/4/1 15:24
 */
public class SgStorageCommonLogic {

    /**
     *
     * @param sourceModel
     * @param skuInfoMap
     * @return
     */
    public static <T extends SgStorageUpdateCommonModel> Boolean setSkuInfo(
            T sourceModel,
            HashMap<Long, PsCProSkuResult> skuInfoMap) {

        //从MAP中获取商品条码信息
        PsCProSkuResult skuResult = null;

        if (skuInfoMap != null ) {
            skuResult = skuInfoMap.get(sourceModel.getPsCSkuId());
        } else {
            return Boolean.FALSE;
        }

        if (skuResult != null) {
            sourceModel.setPsCProId(skuResult.getPsCProId());
            sourceModel.setPsCProEcode(skuResult.getPsCProEcode());
            sourceModel.setPsCProEname(skuResult.getPsCProEname());
            sourceModel.setPsCSkuEcode(skuResult.getSkuEcode());
            sourceModel.setPsCSpec1Id(skuResult.getPsCSpec1objId());
            sourceModel.setPsCSpec1Ecode(skuResult.getClrsEcode());
            sourceModel.setPsCSpec1Ename(skuResult.getClrsEname());
            sourceModel.setPsCSpec2Id(skuResult.getPsCSpec2objId());
            sourceModel.setPsCSpec2Ecode(skuResult.getSizesEcode());
            sourceModel.setPsCSpec2Ename(skuResult.getSizesEname());
            sourceModel.setPriceList(skuResult.getPricelist());
        } else {
            return Boolean.FALSE;
        }

        return Boolean.TRUE;

    }

    /**
     *
     * @param sourceModel
     * @param skuInfoMap
     * @return
     */
    public static <T extends SgPhyStorageUpdateCommonModel> Boolean setSkuInfo(
            T sourceModel,
            HashMap<Long, PsCProSkuResult> skuInfoMap) {

        //从MAP中获取商品条码信息
        PsCProSkuResult skuResult = null;

        if (skuInfoMap != null ) {
            skuResult = skuInfoMap.get(sourceModel.getPsCSkuId());
        } else {
            return Boolean.FALSE;
        }

        if (skuResult != null) {
            sourceModel.setPsCProId(skuResult.getPsCProId());
            sourceModel.setPsCProEcode(skuResult.getPsCProEcode());
            sourceModel.setPsCProEname(skuResult.getPsCProEname());
            sourceModel.setPsCSkuEcode(skuResult.getSkuEcode());
            sourceModel.setPsCSpec1Id(skuResult.getPsCSpec1objId());
            sourceModel.setPsCSpec1Ecode(skuResult.getClrsEcode());
            sourceModel.setPsCSpec1Ename(skuResult.getClrsEname());
            sourceModel.setPsCSpec2Id(skuResult.getPsCSpec2objId());
            sourceModel.setPsCSpec2Ecode(skuResult.getSizesEcode());
            sourceModel.setPsCSpec2Ename(skuResult.getSizesEname());
            sourceModel.setPriceList(skuResult.getPricelist());
        } else {
            return Boolean.FALSE;
        }

        return Boolean.TRUE;

    }
}
