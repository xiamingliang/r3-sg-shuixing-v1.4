package com.jackrain.nea.sg.basic.model.result;

import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.model.SgTeusStorageUpdateCommonModel;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/10/30 15:53
 */
@Data
public class SgTeusStorageUpdateCommonResult implements Serializable {

    //最近数据库操作结果
    private int countResult;
    //占用结果
    private int preoutUpdateResult;
    //期初占用数量
    private BigDecimal qtyPreoutBegin;
    //期末占用数量
    private BigDecimal qtyPreoutEnd;
    //期初在途数量
    private BigDecimal qtyPreinBegin;
    //期末在途数量
    private BigDecimal qtyPreinEnd;
    //期初在库数量
    private BigDecimal qtyStorageBegin;
    //期末在库数量
    private BigDecimal qtyStorageEnd;
    //缺货明细详情
    private List<SgTeusStorageUpdateCommonModel> outStockItemList;

    public SgTeusStorageUpdateCommonResult() {
        this.countResult = 0;
        this.preoutUpdateResult = SgConstantsIF.PREOUT_RESULT_SUCCESS;
    }

}
