package com.jackrain.nea.sg.basic.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.cp.result.CpCStore;
import com.jackrain.nea.data.basic.model.request.SkuInfoQueryRequest;
import com.jackrain.nea.data.basic.model.request.StoreInfoQueryRequest;
import com.jackrain.nea.data.basic.services.BasicCpQueryService;
import com.jackrain.nea.data.basic.services.BasicPsQueryService;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.ps.api.result.PsCProSkuResult;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.config.SgStorageControlConfig;
import com.jackrain.nea.sg.basic.logic.SgStorageCommonLogic;
import com.jackrain.nea.sg.basic.mapper.SgBStorageLockLogMapper;
import com.jackrain.nea.sg.basic.model.SgStorageBillUpdateControlModel;
import com.jackrain.nea.sg.basic.model.SgStorageItemLockKey;
import com.jackrain.nea.sg.basic.model.SgStorageUpdateCommonModel;
import com.jackrain.nea.sg.basic.model.SgStorageUpdateControlModel;
import com.jackrain.nea.sg.basic.model.request.SgStorageSingleUpdateRequest;
import com.jackrain.nea.sg.basic.model.request.SgStorageUpdateBillItemRequest;
import com.jackrain.nea.sg.basic.model.request.SgStorageUpdateBillRequest;
import com.jackrain.nea.sg.basic.model.request.SgStorageUpdateControlRequest;
import com.jackrain.nea.sg.basic.model.result.SgStorageBatchUpdateResult;
import com.jackrain.nea.sg.basic.model.result.SgStorageUpdateCommonResult;
import com.jackrain.nea.sg.basic.model.table.SgBStorageLockLog;
import com.jackrain.nea.sg.basic.utils.StorageLogUtils;
import com.jackrain.nea.sg.basic.utils.StorageUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.util.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * @Description:通用单个单据库存更新接口
 * @Author: chenb
 * @Date: 2019/3/17 16:08
 */
@Component
@Slf4j
public class SgStorageBillUpdateService {

    @Autowired
    SgStorageControlConfig sgStorageControlConfig;

    @Autowired
    private SgBStorageLockLogMapper sgBStorageLockLogMapper;

    @Autowired
    private SgStorageItemFunctionUpdateService sgStorageItemFunctionUpdateService;

    @Autowired
    private BasicPsQueryService basicPsQueryService;

    @Autowired
    private BasicCpQueryService basicCpQueryService;

    /**
     * 通用单个单据库存更新接口（开启新事务）
     *
     * @param request
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public ValueHolderV14<SgStorageBatchUpdateResult> updateStorageBillWithTrans(SgStorageSingleUpdateRequest request) {

        long startTime = System.currentTimeMillis();

        SgStorageBillUpdateControlModel billControlModel = new SgStorageBillUpdateControlModel();
        billControlModel.setTransactionLevel(SgConstants.TRANSACTION_LEVEL_BILL);

        if (log.isDebugEnabled()) {
            log.debug("Start SgStorageBillUpdateService.updateStorageBillWithTrans. ReceiveParams:request:{};"
                    , JSONObject.toJSONString(request.getBill()));
        }

        ValueHolderV14<SgStorageBatchUpdateResult> holder = updateStorageBill(request,
                billControlModel);

        if (log.isDebugEnabled()) {
            log.debug("Finish SgStorageBillUpdateService.updateStorageBillWithTrans. ReturnResult:errorBillItemQty:{} billNo:{} spend time:{}ms;"
                    , JSONObject.toJSONString(holder.getData() == null ? 0 : holder.getData().getErrorBillItemQty()), request.getBill().getBillNo(), System.currentTimeMillis() - startTime);
        }

        if (ResultCode.FAIL == holder.getCode()) {
            throw new NDSException(holder.getMessage());
        }

        return holder;
    }

    /**
     * 用单个单据库存更新接口
     *
     * @param request
     * @param billControlModel
     * @return
     */
    public ValueHolderV14<SgStorageBatchUpdateResult> updateStorageBill(SgStorageSingleUpdateRequest request,
                                                                        SgStorageBillUpdateControlModel billControlModel) {

        long startTime = System.currentTimeMillis();

        if (log.isDebugEnabled()) {
            log.debug("Start SgStorageBillUpdateService.updateStorageBill. ReceiveParams:request:{};"
                    , JSONObject.toJSONString(request.getBill()));
        }

        ValueHolderV14<SgStorageBatchUpdateResult> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        ValueHolderV14<SgStorageUpdateCommonResult> itemUpdateResult = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        SgStorageBatchUpdateResult updateResult = new SgStorageBatchUpdateResult();
        List<SgStorageUpdateCommonModel> batchUpdateCommonList = new ArrayList<>();
        List<SgStorageUpdateCommonModel> outStockItemList = new ArrayList<>();
        List<SgStorageUpdateCommonModel> channelMqItemList = new ArrayList<>();
        HashMap<Long, CpCStore> storeInfoMap = null;
        HashMap<Long, PsCProSkuResult> skuInfoMap = null;
        SgStorageItemLockKey lockKey = new SgStorageItemLockKey();
        int preoutUpdateResult = SgConstantsIF.PREOUT_RESULT_SUCCESS;
        //发生错误的明细数
        long errorItemQty = 0;

        //检查入参
        holder = checkServiceParam(request, request.getLoginUser());
        if (ResultCode.FAIL == holder.getCode()) {
            return holder;
        }

        User loginUser = request.getLoginUser();
        String messageKey = request.getMessageKey();
        SgStorageUpdateBillRequest billInfo = request.getBill();

        //设置单据MQ幂等性防重锁
        if (StringUtils.isEmpty(messageKey)) {
            messageKey = SgConstants.REDIS_KEY_PREFIX_STO_MQ_MESSAGE +
                    billInfo.getBillNo() + ":" + billInfo.getBillNo();
        }

        //初始化库存更新控制对象（默认不允许负库存）
        SgStorageUpdateControlModel controlModel = new SgStorageUpdateControlModel();

        //APOLLO系统配置优先级最低
        if (sgStorageControlConfig != null) {
            BeanUtils.copyProperties(sgStorageControlConfig, controlModel);
        }

        //当消费者设置了控制类里的情况下，优先使用消费者的控制类
        if (request.getControlModel() != null) {
            BeanUtils.copyProperties(request.getControlModel(), controlModel);
        }

        if (log.isDebugEnabled()) {
            log.debug("SgStorageBillUpdateService.updateStorageBill. billInfo.controlModel:{};"
                    + JSONObject.toJSONString(controlModel));
        }

        /** ########## 获取逻辑仓，商品条码基础信息 ########## **/
        //同步R3基础数据的情况下，获取组织中心，商品中心的基础数据
        if (controlModel.isSyncR3BasicData()) {
            //批量获取商品条码，店仓信息
            SkuInfoQueryRequest skuRequest = new SkuInfoQueryRequest();
            StoreInfoQueryRequest storeRequest = new StoreInfoQueryRequest();
            HashMap<Long, Long> skuIdMap = new HashMap<>(16);
            HashMap<Long, Long> storeIdMap = new HashMap<>(16);

            for (SgStorageUpdateBillItemRequest itemInfo : billInfo.getItemList()) {
                if (itemInfo != null) {
                    storeIdMap.put(itemInfo.getCpCStoreId(), itemInfo.getCpCStoreId());
                    skuIdMap.put(itemInfo.getPsCSkuId(), itemInfo.getPsCSkuId());
                }

            }
            storeRequest.setIds(new ArrayList<>(storeIdMap.values()));
            skuRequest.setSkuIdList(new ArrayList<>(skuIdMap.values()));

            try {

                storeInfoMap = basicCpQueryService.getStoreInfo(storeRequest);
                skuInfoMap = basicPsQueryService.getSkuInfo(skuRequest);

            } catch (Exception e) {

                String errorMsg = StorageLogUtils.getErrMessage(e, messageKey, loginUser);

                holder.setCode(ResultCode.FAIL);
                holder.setMessage(errorMsg.concat(" " + holder.getMessage()));

                return holder;
            }
        }

        //单据明细排序（避免库存更新死锁）
        Collections.sort(billInfo.getItemList());

        for (SgStorageUpdateBillItemRequest itemInfo : billInfo.getItemList()) {

            SgStorageUpdateCommonModel updateCommonModel = new SgStorageUpdateCommonModel();

            if (log.isDebugEnabled()) {
                log.debug("SgStorageBillUpdateService.updateStorageBill. SgStorageUpdateBillItemRequest:{};"
                        , JSONObject.toJSONString(itemInfo));
            }

            BeanUtils.copyProperties(billInfo, lockKey);
            BeanUtils.copyProperties(itemInfo, lockKey);
            lockKey.setMessageKey(messageKey);

            String md5LockKey = DigestUtils.md5Hex(JSONObject.toJSONString(lockKey));

            int lockKeyCount = sgBStorageLockLogMapper.selectCount(
                    new QueryWrapper<SgBStorageLockLog>().lambda().eq(SgBStorageLockLog::getLockKey, md5LockKey));

            if (lockKeyCount > 0) {
                log.warn("SgStorageBillUpdateService.updateStorageBill. billno:{} lockKey is already existed. md5LockKey:{};"
                        , billInfo.getBillNo(), md5LockKey);
                continue;
            }

            StorageUtils.setBModelDefalutData(updateCommonModel, loginUser);
            updateCommonModel.setMessageKey(messageKey);
            updateCommonModel.setLockKey(md5LockKey);
            updateCommonModel.setOwnerename(loginUser.getEname());
            updateCommonModel.setModifierename(loginUser.getEname());

            try {

                BeanUtils.copyProperties(billInfo, updateCommonModel);
                BeanUtils.copyProperties(itemInfo, updateCommonModel);

                //单据取消的情况下，相应数量取反
                if (billInfo.getIsCancel()) {
                    updateCommonModel.setQtyPreinChange(updateCommonModel.getQtyPreinChange().negate());
                    updateCommonModel.setQtyPreoutChange(updateCommonModel.getQtyPreoutChange().negate());
                    updateCommonModel.setQtyStorageChange(updateCommonModel.getQtyStorageChange().negate());
                }

                //设置明细的控制属性
                if (itemInfo.getControlmodel() == null) {
                    SgStorageUpdateControlRequest itemControlmodel = new SgStorageUpdateControlRequest();
                    BeanUtils.copyProperties(controlModel, itemControlmodel);
                    updateCommonModel.setControlmodel(itemControlmodel);
                }

                if (log.isDebugEnabled()) {
                    log.debug("SgStorageBillUpdateService.updateStorageBill. itemInfo.controlModel:{};"
                            + JSONObject.toJSONString(controlModel));
                }

                //同步R3基础数据的情况下，获取组织中心，商品中心的基础数据
                if (controlModel.isSyncR3BasicData()) {

                    if (storeInfoMap != null && storeInfoMap.get(itemInfo.getCpCStoreId()) != null) {

                        updateCommonModel.setCpCStoreEcode(storeInfoMap.get(itemInfo.getCpCStoreId()).getEcode());
                        updateCommonModel.setCpCStoreEname(storeInfoMap.get(itemInfo.getCpCStoreId()).getEname());

                    } else {
                        holder.setCode(ResultCode.FAIL);
                        holder.setMessage(Resources.getMessage("店仓信息获取失败，店仓不存在！",
                                loginUser.getLocale(), itemInfo.getCpCStoreId()));
                        errorItemQty++;

                        continue;
                    }

                    //批量设定商品条码信息
                    Boolean skuFlg = SgStorageCommonLogic.setSkuInfo(updateCommonModel, skuInfoMap);

                    if (!skuFlg) {
                        holder.setCode(ResultCode.FAIL);
                        holder.setMessage(Resources.getMessage("条码信息获取失败，条码不存在！",
                                loginUser.getLocale(), updateCommonModel.getPsCSkuId())
                                .concat(" " + holder.getMessage()));

                        errorItemQty++;

                        continue;
                    }
                }

                batchUpdateCommonList.add(updateCommonModel);


            } catch (Exception e) {

                String errorMsg = StorageLogUtils.getErrMessage(e, messageKey, loginUser);

                holder.setCode(ResultCode.FAIL);
                preoutUpdateResult = controlModel.getPreoutOperateType();
                holder.setMessage(errorMsg.concat(" " + holder.getMessage()));

                errorItemQty++;

            }

        }

        //批量更新单据明细的情况下
        if (!CollectionUtils.isEmpty(batchUpdateCommonList)) {

            //批量更新单据明细分页
            int pageSize = controlModel.getBatchUpdateItems();
            int listSize = batchUpdateCommonList.size();
            int page = listSize / pageSize;

            if (listSize % pageSize != 0) {
                page++;
            }

            //分页批量更新
            for (int i = 0; i < page; i++) {

                List<SgStorageUpdateCommonModel> pageList = batchUpdateCommonList.subList(i * pageSize,
                        (((i + 1) * pageSize > listSize ? listSize : pageSize * (i + 1))));

                try {

                    if (SgConstants.TRANSACTION_LEVEL_ITEM == billControlModel.getTransactionLevel()) {
                        itemUpdateResult = sgStorageItemFunctionUpdateService.updateStorageItemWithTrans(pageList,
                                controlModel, loginUser);
                    } else {
                        itemUpdateResult = sgStorageItemFunctionUpdateService.updateStorageItem(pageList,
                                controlModel, loginUser);
                    }

                    if (ResultCode.FAIL == itemUpdateResult.getCode()) {

                        holder.setCode(ResultCode.FAIL);
                        holder.setMessage(itemUpdateResult.getMessage().concat(" " + holder.getMessage()));
                        errorItemQty = errorItemQty + pageList.size();

                    } else {

                        if (SgConstantsIF.PREOUT_RESULT_SUCCESS != itemUpdateResult.getData().getPreoutUpdateResult()) {
                            preoutUpdateResult = itemUpdateResult.getData().getPreoutUpdateResult();
                            outStockItemList.addAll(itemUpdateResult.getData().getOutStockItemList());
                            holder.setMessage(itemUpdateResult.getMessage().concat(" " + holder.getMessage()));
                        }

                        //存入渠道库存同步MQ列表
                        channelMqItemList.addAll(itemUpdateResult.getData().getChannelMqItemList());
                    }

                } catch (Exception e) {

                    String errorMsg = StorageLogUtils.getErrMessage(e, messageKey, loginUser);

                    holder.setCode(ResultCode.FAIL);
                    preoutUpdateResult = controlModel.getPreoutOperateType();
                    holder.setMessage(errorMsg.concat(" " + holder.getMessage()));

                    errorItemQty = errorItemQty + pageList.size();

                }
            }
        }

        updateResult.setErrorBillItemQty(errorItemQty);
        updateResult.setPreoutUpdateResult(preoutUpdateResult);
        updateResult.setOutStockItemList(outStockItemList);
        updateResult.setChannelMqItemList(channelMqItemList);
        holder.setData(updateResult);

        // 没有错误明细的情况下，设置成功运行结果，
        // 有错误明细的情况下，在发生错误的明细中设置运行结果
        if (errorItemQty < 1) {

            holder.setCode(ResultCode.SUCCESS);
            holder.setMessage(Resources.getMessage("当前单据的店仓库存变动消息处理完毕！",
                    loginUser.getLocale(), holder.getMessage(), billInfo.getBillNo()));

        }

        if (log.isDebugEnabled()) {
            log.debug("Finish SgStorageBillUpdateService.updateStorageBill. ReturnResult:errorBillItemQty:{} spend time:{}ms;"
                    , JSONObject.toJSONString(holder.getData() == null ? 0 : holder.getData().getErrorBillItemQty()), System.currentTimeMillis() - startTime);
        }

        return holder;
    }

    /**
     * @param request
     * @param loginUser
     * @return
     */
    private ValueHolderV14<SgStorageBatchUpdateResult> checkServiceParam(SgStorageSingleUpdateRequest request,
                                                                         User loginUser) {

        ValueHolderV14<SgStorageBatchUpdateResult> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");

        if (request == null) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("请求体为空！", loginUser.getLocale()));
            return holder;
        }

        if (request.getLoginUser() == null) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("操作用户信息不能为空！", loginUser.getLocale()));
        }

        if (request.getBill() == null || StringUtils.isEmpty(request.getBill().getBillNo())) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("店仓库存更新单据不能为空！", loginUser.getLocale()));
        }

        if (ResultCode.FAIL == holder.getCode() && log.isDebugEnabled()) {
            log.debug("SgStorageBillUpdateService.checkServiceParam. checkServiceParam error:"
                    + holder.getMessage() + ";");
        }

        return holder;

    }


}
