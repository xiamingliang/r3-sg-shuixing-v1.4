package com.jackrain.nea.sg.basic.config;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/4/29 19:28
 */
@Slf4j
@Configuration
@Data
public class SgStorageKafkaConfig {

    @Value("${r3.kafka.producer.storage.topic}")
    private String storageUpdateKafkaTopic;

    @Value("${r3.kafka.producer.phystorage.topic}")
    private String phyStorageUpdateKafkaTopic;

    @Value("${r3.kafak.partition.totalnumber:6}")
    private int totalnumber;


}
