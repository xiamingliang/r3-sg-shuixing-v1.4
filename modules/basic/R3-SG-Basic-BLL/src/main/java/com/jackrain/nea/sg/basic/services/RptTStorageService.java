package com.jackrain.nea.sg.basic.services;

import com.google.common.collect.Lists;
import com.jackrain.nea.config.PropertiesConf;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.cp.result.CpCStore;
import com.jackrain.nea.data.basic.model.request.StoreInfoQueryRequest;
import com.jackrain.nea.data.basic.services.BasicCpQueryService;
import com.jackrain.nea.sg.basic.mapper.RptTStorageMapper;
import com.jackrain.nea.sg.basic.mapper.RptTStorageSkuMapper;
import com.jackrain.nea.sg.basic.model.result.SgRcStorageQueryResult;
import com.jackrain.nea.sg.basic.model.result.SgRcStorageResult;
import com.jackrain.nea.sg.basic.utils.SGExportUtil;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * Created with IntelliJ IDEA.
 * User: zyj
 * Date: 2019/7/21
 * Time: 17:53
 * Description: 库存报表处理类
 */
@Component
@Slf4j
public class RptTStorageService {
    @Autowired
    private RptTStorageSkuMapper rptTStorageSkuMapper;

    @Autowired
    private RptTStorageMapper rptTStorageMapper;

    @Autowired
    private SGExportUtil sgExportUtil;

    @Value("${r3.oss.endpoint}")
    private String endpoint;

    @Value("${r3.oss.accessKey}")
    private String accessKeyId;

    @Value("${r3.oss.secretKey}")
    private String accessKeySecret;

    @Value("${r3.oss.bucketName}")
    private String bucketName;

    @Value("${r3.oss.timeout}")
    private String timeout;

    @Autowired
    private PropertiesConf propertiesConf;

    @Autowired
    private BasicCpQueryService basicCpQueryService;

    /**
      * @author:zyj
      * @date:2019/7/31
      * @description:获取逻辑仓信息
      */
    private SgRcStorageResult getCpCStoreCode(SgRcStorageResult sgRcStorageResult) throws Exception {
        if(sgRcStorageResult.getCpCStoreId() != null){
            StoreInfoQueryRequest storeInfoQueryRequest = new StoreInfoQueryRequest();
            List<Long> idList = new ArrayList<>();
            idList.add(sgRcStorageResult.getCpCStoreId());
            storeInfoQueryRequest.setIds(idList);
            HashMap storeMap = null;
            storeMap = basicCpQueryService.getStoreInfo(storeInfoQueryRequest);
            CpCStore cpCStore = (CpCStore) storeMap.get(sgRcStorageResult.getCpCStoreId());
            sgRcStorageResult.setStoreCode(cpCStore.getEcode());
        }
        return sgRcStorageResult;
    }
    /**
      * @author:zyj
      * @date:2019/7/21
      * @description:商品期末库存生成批次号入口
      */
    public ValueHolderV14 generateBatchNoForStorageSku(String schema,SgRcStorageResult sgRcStorageResult){
        ValueHolderV14 valueHolder14 = new ValueHolderV14();
        HashMap map = new HashMap();
        try{
            getCpCStoreCode(sgRcStorageResult);
            map =  rptTStorageSkuMapper.generateBatchNo(schema,sgRcStorageResult);
        }catch (Exception e){
            valueHolder14.setCode(ResultCode.FAIL);
            valueHolder14.setMessage(e.getMessage());
            valueHolder14.setData(map);
            return valueHolder14;
        }
        valueHolder14.setCode(ResultCode.SUCCESS);
        valueHolder14.setData(map);
        return valueHolder14;
    }
    /**
     * @author:zyj
     * @date:2019/7/21
     * @description:逐日库存生成批次号入口
     */
    public ValueHolderV14 generateBatchNoForStorage(String schema,SgRcStorageResult sgRcStorageResult){
        ValueHolderV14 valueHolder14 = new ValueHolderV14();
        HashMap map = new HashMap();
        try{
            getCpCStoreCode(sgRcStorageResult);
            map =  rptTStorageMapper.generateStorageBatchNo(schema,sgRcStorageResult);
        }catch (Exception e){
            valueHolder14.setCode(ResultCode.FAIL);
            valueHolder14.setMessage(e.getMessage());
            valueHolder14.setData(map);
            return valueHolder14;
        }
        valueHolder14.setCode(ResultCode.SUCCESS);
        valueHolder14.setData(map);
        return valueHolder14;
    }
    /**
      * @author:zyj
      * @date:2019/7/25
      * @description:逐日库存报表查询
      */
    public List<SgRcStorageQueryResult> queryDataForStorage(SgRcStorageResult sgRcStorageResult){
        try {
            getCpCStoreCode(sgRcStorageResult);
        } catch (Exception e) {
            log.error(this.getClass().getName()+" 获取逻辑仓信息异常！！！",e);
            return new ArrayList<>();
        }
        List<SgRcStorageQueryResult> list =  rptTStorageMapper.queryDataForStorage(sgRcStorageResult);
        return list;
    }
    /**
      * @author:zyj
      * @date:2019/7/29
      * @description:物流逐日报表导出
      */
    public ValueHolderV14 exportStorage(SgRcStorageResult sgRcStorageResult,User user){
        ValueHolderV14 vh = new ValueHolderV14(ResultCode.SUCCESS, "物流逐日报表导出成功！");
        try {
            getCpCStoreCode(sgRcStorageResult);
        } catch (Exception e) {
            log.error(this.getClass().getName()+" 获取逻辑仓信息异常！！！",e);
            vh.setCode(ResultCode.FAIL);
            vh.setMessage(e.getMessage());
            return vh;
        }
        List<SgRcStorageQueryResult> mainExcelList = getRPStorageData(sgRcStorageResult).getData();
        sgExportUtil.setEndpoint(this.endpoint);
        sgExportUtil.setAccessKeyId(this.accessKeyId);
        sgExportUtil.setAccessKeySecret(this.accessKeySecret);
        sgExportUtil.setBucketName(this.bucketName);
        if(StringUtils.isEmpty(timeout)){
            //如果获取不到apllo配置参数，设置默认过期时间为30分钟
            timeout = "1800000";
        }
        sgExportUtil.setTimeout(this.timeout);

        /**
         *  拼接Excel主表sheet表头字段和列表
         * */
        String mainNames[] = {"日期", "仓库名称", "鞋子", "服装", "配件", "袜子", "纸质"};
        String orderKeys[] = {"rptDate", "cpCStoreEname", "shot", "cloth", "part", "stock", "paper"};
        List<String> mainName = Lists.newArrayList(mainNames);
        List<String> mainKey = Lists.newArrayList(orderKeys);
        XSSFWorkbook hssfWorkbook = new XSSFWorkbook();
        sgExportUtil.executeSheet(hssfWorkbook, "逐日库存报表信息", "", mainName, mainKey, mainExcelList, false);
        String rootUrl =propertiesConf.getProperty("sg.rpt.storage_export_upload_base");
        if(log.isDebugEnabled()){
            log.debug(this.getClass().getName()+" 报表导出文件地址:"+rootUrl);
        }
        String putMsg = sgExportUtil.saveFileAndPutOss(hssfWorkbook, "逐日库存报表导出", user, rootUrl);
        if(StringUtils.isEmpty(putMsg)){
            vh.setCode(ResultCode.FAIL);
            vh.setMessage("逐日库存报表导出失败");
        }
        vh.setData(putMsg);
        return vh;
    }
    public ValueHolderV14<List<SgRcStorageQueryResult>> getRPStorageData(SgRcStorageResult sgRcStorageResult) {
        ValueHolderV14 vh = new ValueHolderV14();
        //查询符合的主表
        List<SgRcStorageQueryResult> mainExcelList = rptTStorageMapper.exportDataForStorage(sgRcStorageResult);
        vh.setData(mainExcelList);
        return vh;
    }

    /**
     * @author:zyj
     * @date:2019/7/30
     * @description:逐日库存报表统计查询
     */
    public Integer countForStorage(SgRcStorageResult sgRcStorageResult){
        try {
            getCpCStoreCode(sgRcStorageResult);
        } catch (Exception e) {
            log.error(this.getClass().getName()+" 获取逻辑仓信息异常！！！",e);
            return null;
        }
        Integer totalSize =  rptTStorageMapper.countForStorage(sgRcStorageResult);
        return totalSize;
    }
}
