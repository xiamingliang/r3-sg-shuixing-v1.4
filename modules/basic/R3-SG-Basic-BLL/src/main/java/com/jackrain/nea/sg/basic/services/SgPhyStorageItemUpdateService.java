package com.jackrain.nea.sg.basic.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.model.util.ModelUtil;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.logic.SgPhyStorageLogic;
import com.jackrain.nea.sg.basic.logic.SgStorageCheckLogic;
import com.jackrain.nea.sg.basic.mapper.SgBPhyStorageMapper;
import com.jackrain.nea.sg.basic.mapper.SgBStorageLockLogMapper;
import com.jackrain.nea.sg.basic.model.SgPhyStorageUpdateCommonModel;
import com.jackrain.nea.sg.basic.model.SgStorageUpdateControlModel;
import com.jackrain.nea.sg.basic.model.result.SgPhyStorageUpdateCommonResult;
import com.jackrain.nea.sg.basic.model.table.SgBPhyStorage;
import com.jackrain.nea.sg.basic.model.table.SgBStorageLockLog;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;


/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/3/20 13:15
 */
@Component
@Slf4j
public class SgPhyStorageItemUpdateService {

    @Autowired
    private SgBPhyStorageMapper sgBPhyStorageMapper;

    @Autowired
    private SgBStorageLockLogMapper sgBStorageLockLogMapper;

    @Autowired
    private SgPhyStorageLogic sgPhyStorageLogic;

    /**
     * 通用单个明细库存更新接口(开启新事务)
     * @param request
     * @param controlModel
     * @param loginUser
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public ValueHolderV14<SgPhyStorageUpdateCommonResult> updatePhyStorageItemWithTrans(SgPhyStorageUpdateCommonModel request,
                                                                                  SgStorageUpdateControlModel controlModel,
                                                                                  User loginUser) {

        long startTime = System.currentTimeMillis();

        if (log.isDebugEnabled()) {
            log.debug("Start SgPhyStorageItemUpdateService.updateStorageItemWithTrans. ReceiveParams:request:{} controlModel:{};"
                    , JSONObject.toJSONString(request), JSONObject.toJSONString(controlModel));
        }

        ValueHolderV14<SgPhyStorageUpdateCommonResult> holder = updatePhyStorageItem(request,controlModel,loginUser);

        if (log.isDebugEnabled()) {
            log.debug("Finish SgPhyStorageItemUpdateService.updatePhyStorageBillWithTrans. ReturnResult:billNo:{} spend time:{}ms;"
                    , request.getBillNo(), System.currentTimeMillis() - startTime);
        }

        if (ResultCode.FAIL == holder.getCode()) {
            throw new NDSException(holder.getMessage());
        }

        return holder;

    }

    /**
     * 通用单个明细库存更新接口
     * @param request
     * @param controlModel
     * @param loginUser
     * @return
     */
    public ValueHolderV14<SgPhyStorageUpdateCommonResult> updatePhyStorageItem(SgPhyStorageUpdateCommonModel request,
                                                                               SgStorageUpdateControlModel controlModel,
                                                                               User loginUser) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgPhyStorageItemUpdateService.updatePhyStorageItem. ReceiveParams:request:{} controlModel:{};"
                    , JSONObject.toJSONString(request), JSONObject.toJSONString(controlModel));
        }

        ValueHolderV14<SgPhyStorageUpdateCommonResult> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        SgPhyStorageUpdateCommonResult commonResult = new SgPhyStorageUpdateCommonResult();
        int resultCount = 0;

        holder = SgStorageCheckLogic.checkServiceParam(request, loginUser);

        if (ResultCode.FAIL == holder.getCode()) {
            return holder;
        }

        SgBPhyStorage updateRecord = sgBPhyStorageMapper.selectOne(
                new QueryWrapper<SgBPhyStorage>().lambda().select(SgBPhyStorage::getId)
                        .eq(SgBPhyStorage::getCpCPhyWarehouseId, request.getCpCPhyWarehouseId())
                        .eq(SgBPhyStorage::getPsCSkuId, request.getPsCSkuId()));

        if (updateRecord == null) {

            try {

                holder = sgPhyStorageLogic.initPhyStorage(request, loginUser);

                if (ResultCode.FAIL == holder.getCode()) {
                    throw new NDSException(holder.getMessage());
                }

            } catch (Exception e) {

                log.error("SgPhyStorageItemUpdateService.updatePhyStorageItem. 实体仓库存初始化插入失败！" +
                                "实体仓编码:{},商品条码:{}",
                        request.getCpCPhyWarehouseEcode(), request.getPsCSkuEcode());

                //并发初始化库存记录异常时进行等待重试
                holder.setCode(ResultCode.FAIL);
                holder.setData(commonResult);
                holder.setMessage(Resources.getMessage("当前记录正在更新中......！", loginUser.getLocale()));
                return holder;
            }
        }

        SgBStorageLockLog lockLog = new SgBStorageLockLog();
        BeanUtils.copyProperties(request, lockLog);
        lockLog.setId(ModelUtil.getSequence(SgConstants.SG_B_STORAGE_LOCK_LOG));

        resultCount = sgBStorageLockLogMapper.insert(lockLog);

        if (resultCount < 1) {
            throw new NDSException(Resources.getMessage("实体仓库存锁初始化插入失败！", loginUser.getLocale(),
                    request.getCpCPhyWarehouseEcode(), request.getPsCSkuEcode()));
        }

        holder = sgPhyStorageLogic.updatePhyStorageChangeWithFtp(request, controlModel, loginUser);

        if (ResultCode.FAIL == holder.getCode()) {
            throw new NDSException(holder.getMessage());
        }

        return holder;

    }

}