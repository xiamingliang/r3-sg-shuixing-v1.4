package com.jackrain.nea.sg.basic.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.cp.result.CpCStore;
import com.jackrain.nea.data.basic.model.request.SkuInfoQueryRequest;
import com.jackrain.nea.data.basic.model.request.StoreInfoQueryRequest;
import com.jackrain.nea.data.basic.services.BasicCpQueryService;
import com.jackrain.nea.data.basic.services.BasicPsQueryService;
import com.jackrain.nea.ps.api.result.PsCProSkuResult;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.config.SgStorageControlConfig;
import com.jackrain.nea.sg.basic.logic.SgStorageCommonLogic;
import com.jackrain.nea.sg.basic.mapper.SgBStorageLockLogMapper;
import com.jackrain.nea.sg.basic.model.SgStorageItemBatchUpdateModel;
import com.jackrain.nea.sg.basic.model.SgStorageItemLockKey;
import com.jackrain.nea.sg.basic.model.SgStorageUpdateCommonModel;
import com.jackrain.nea.sg.basic.model.SgStorageUpdateControlModel;
import com.jackrain.nea.sg.basic.model.result.SgStorageBatchUpdateResult;
import com.jackrain.nea.sg.basic.model.result.SgStorageUpdateCommonResult;
import com.jackrain.nea.sg.basic.model.table.SgBStorageLockLog;
import com.jackrain.nea.sg.basic.utils.StorageLogUtils;
import com.jackrain.nea.sg.basic.utils.StorageUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * @Description:通用明细库存更新接口
 * @Author: chenb
 * @Date: 2019/3/17 16:08
 */
@Component
@Slf4j
public class SgStorageItemBatchUpdateService {

    @Autowired
    SgStorageControlConfig sgStorageControlConfig;

    @Autowired
    private SgBStorageLockLogMapper sgBStorageLockLogMapper;

    @Autowired
    private SgStorageItemFunctionUpdateService sgStorageItemFunctionUpdateService;

    @Autowired
    private BasicPsQueryService basicPsQueryService;

    @Autowired
    private BasicCpQueryService basicCpQueryService;


    /*
     * 通用明细库存更新接口
     *
     * @param request
     * @param billControlModel
     * @return
     */
    public ValueHolderV14<SgStorageBatchUpdateResult> updateStorageItem(SgStorageItemBatchUpdateModel batchModel,
                                                                        User loginUser) {

        long startTime = System.currentTimeMillis();

        if (log.isDebugEnabled()) {
            log.debug("Start SgStorageItemBatchUpdateService.updateStorageItem. ReceiveParams:messageKey:{};"
                    , JSONObject.toJSONString(batchModel.getMessageKey()));
        }

        ValueHolderV14<SgStorageBatchUpdateResult> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        ValueHolderV14<SgStorageUpdateCommonResult> itemUpdateResult = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        SgStorageBatchUpdateResult updateResult = new SgStorageBatchUpdateResult();
        List<SgStorageUpdateCommonModel> batchUpdateCommonList = new ArrayList<>();
        List<SgStorageUpdateCommonModel> outStockItemList = new ArrayList<>();
        List<SgStorageUpdateCommonModel> channelMqItemList = new ArrayList<>();
        HashMap<Long, CpCStore> storeInfoMap = null;
        HashMap<Long, PsCProSkuResult> skuInfoMap = null;
        SgStorageItemLockKey lockKey = new SgStorageItemLockKey();
        int preoutUpdateResult = SgConstantsIF.PREOUT_RESULT_SUCCESS;

        //发生错误的明细数
        long errorItemQty = 0;

        //检查入参
        holder = checkServiceParam(batchModel, loginUser);
        if (ResultCode.FAIL == holder.getCode()) {
            return holder;
        }

        SgStorageUpdateControlModel controlModel = new SgStorageUpdateControlModel();
        controlModel.setPreoutOperateType(SgConstantsIF.PREOUT_OPT_TYPE_OUT_STOCK);

        /** ########## 获取逻辑仓，商品条码基础信息 ########## **/
        //同步R3基础数据的情况下，获取组织中心，商品中心的基础数据
        if (sgStorageControlConfig.isSyncR3BasicData()) {
            //批量获取商品条码，店仓信息
            SkuInfoQueryRequest skuRequest = new SkuInfoQueryRequest();
            StoreInfoQueryRequest storeRequest = new StoreInfoQueryRequest();
            HashMap<Long, Long> skuIdMap = new HashMap<>(16);
            HashMap<Long, Long> storeIdMap = new HashMap<>(16);

            for (SgStorageUpdateCommonModel itemInfo : batchModel.getItemList()) {
                if (itemInfo != null) {
                    storeIdMap.put(itemInfo.getCpCStoreId(), itemInfo.getCpCStoreId());
                    skuIdMap.put(itemInfo.getPsCSkuId(), itemInfo.getPsCSkuId());
                }

            }
            storeRequest.setIds(new ArrayList<>(storeIdMap.values()));
            skuRequest.setSkuIdList(new ArrayList<>(skuIdMap.values()));

            try {

                storeInfoMap = basicCpQueryService.getStoreInfo(storeRequest);
                skuInfoMap = basicPsQueryService.getSkuInfo(skuRequest);

            } catch (Exception e) {

                String errorMsg = StorageLogUtils.getErrMessage(e, batchModel.getMessageKey(), loginUser);

                holder.setCode(ResultCode.FAIL);
                holder.setMessage(errorMsg.concat(" " + holder.getMessage()));

                return holder;
            }
        }

        //单据明细排序（避免库存更新死锁）
        Collections.sort(batchModel.getItemList());

        for (SgStorageUpdateCommonModel updateCommonModel : batchModel.getItemList()) {

            if (log.isDebugEnabled()) {
                log.debug("SgStorageItemBatchUpdateService.updateStorageBill. updateCommonModel:{};"
                        , JSONObject.toJSONString(updateCommonModel));
            }

            BeanUtils.copyProperties(updateCommonModel, lockKey);
            lockKey.setMessageKey(batchModel.getMessageKey());

            String md5LockKey = DigestUtils.md5Hex(JSONObject.toJSONString(lockKey));

            int lockKeyCount = sgBStorageLockLogMapper.selectCount(
                    new QueryWrapper<SgBStorageLockLog>().lambda().eq(SgBStorageLockLog::getLockKey, md5LockKey));

            if (lockKeyCount > 0) {
                log.warn("SgStorageItemBatchUpdateService.updateStorageBill. billno:{} messageKey:{} lockKey is already existed. " +
                                "md5LockKey:{};"
                        , updateCommonModel.getBillNo(), batchModel.getMessageKey(), md5LockKey);
                continue;
            }

            StorageUtils.setBModelDefalutData(updateCommonModel, loginUser);
            updateCommonModel.setMessageKey(batchModel.getMessageKey());
            updateCommonModel.setLockKey(md5LockKey);
            updateCommonModel.setOwnerename(loginUser.getEname());
            updateCommonModel.setModifierename(loginUser.getEname());

            try {

                //同步R3基础数据的情况下，获取组织中心，商品中心的基础数据
                if (sgStorageControlConfig.isSyncR3BasicData()) {

                    if (storeInfoMap != null && storeInfoMap.get(updateCommonModel.getCpCStoreId()) != null) {

                        updateCommonModel.setCpCStoreEcode(storeInfoMap.get(updateCommonModel.getCpCStoreId()).getEcode());
                        updateCommonModel.setCpCStoreEname(storeInfoMap.get(updateCommonModel.getCpCStoreId()).getEname());

                    } else {
                        holder.setCode(ResultCode.FAIL);
                        holder.setMessage(Resources.getMessage("店仓信息获取失败，店仓不存在！",
                                loginUser.getLocale(), updateCommonModel.getCpCStoreId()));
                        errorItemQty++;

                        continue;
                    }

                    //批量设定商品条码信息
                    Boolean skuFlg = SgStorageCommonLogic.setSkuInfo(updateCommonModel, skuInfoMap);

                    if (!skuFlg) {
                        holder.setCode(ResultCode.FAIL);
                        holder.setMessage(Resources.getMessage("条码信息获取失败，条码不存在！",
                                loginUser.getLocale(), updateCommonModel.getPsCSkuId())
                                .concat(" " + holder.getMessage()));

                        errorItemQty++;

                        continue;
                    }
                }

                batchUpdateCommonList.add(updateCommonModel);


            } catch (Exception e) {

                String errorMsg = StorageLogUtils.getErrMessage(e, batchModel.getMessageKey(), loginUser);

                holder.setCode(ResultCode.FAIL);
                holder.setMessage(errorMsg.concat(" " + holder.getMessage()));

                errorItemQty++;

            }

        }

        //批量更新单据明细的情况下
        if (!CollectionUtils.isEmpty(batchUpdateCommonList)) {

            //批量更新单据明细分页
            int pageSize = sgStorageControlConfig.getBatchUpdateItems();
            int listSize = batchUpdateCommonList.size();
            int page = listSize / pageSize;

            if (listSize % pageSize != 0) {
                page++;
            }

            //分页批量更新
            for (int i = 0; i < page; i++) {

                List<SgStorageUpdateCommonModel> pageList = batchUpdateCommonList.subList(i * pageSize,
                        (((i + 1) * pageSize > listSize ? listSize : pageSize * (i + 1))));

                try {

                    itemUpdateResult = sgStorageItemFunctionUpdateService.updateStorageItemWithTrans(pageList,
                            controlModel, loginUser);

                    if (ResultCode.FAIL == itemUpdateResult.getCode()) {

                        holder.setCode(ResultCode.FAIL);
                        holder.setMessage(itemUpdateResult.getMessage().concat(" " + holder.getMessage()));
                        errorItemQty = errorItemQty + pageList.size();

                    } else {

                        if (SgConstantsIF.PREOUT_RESULT_SUCCESS != itemUpdateResult.getData().getPreoutUpdateResult()) {
                            preoutUpdateResult = itemUpdateResult.getData().getPreoutUpdateResult();
                            outStockItemList.addAll(itemUpdateResult.getData().getOutStockItemList());
                            holder.setMessage(itemUpdateResult.getMessage().concat(" " + holder.getMessage()));
                        }

                        //存入渠道库存同步MQ列表
                        channelMqItemList.addAll(itemUpdateResult.getData().getChannelMqItemList());
                    }

                } catch (Exception e) {

                    String errorMsg = StorageLogUtils.getErrMessage(e, batchModel.getMessageKey(), loginUser);

                    holder.setCode(ResultCode.FAIL);
                    preoutUpdateResult = controlModel.getPreoutOperateType();
                    holder.setMessage(errorMsg.concat(" " + holder.getMessage()));

                    errorItemQty = errorItemQty + pageList.size();

                }
            }
        }

        updateResult.setErrorBillItemQty(errorItemQty);
        updateResult.setPreoutUpdateResult(preoutUpdateResult);
        updateResult.setOutStockItemList(outStockItemList);
        updateResult.setChannelMqItemList(channelMqItemList);
        holder.setData(updateResult);

        // 没有错误明细的情况下，设置成功运行结果，
        // 有错误明细的情况下，在发生错误的明细中设置运行结果
        if (errorItemQty < 1) {

            holder.setCode(ResultCode.SUCCESS);
            holder.setMessage(Resources.getMessage("当前单据的店仓库存变动消息处理完毕！",
                    loginUser.getLocale(), holder.getMessage(), batchModel.getMessageKey()));

        }

        if (log.isDebugEnabled()) {
            log.debug("Finish SgStorageItemBatchUpdateService.updateStorageItem. ReturnResult:errorBillItemQty:{} spend time:{}ms;"
                    , JSONObject.toJSONString(holder.getData() == null ? 0 : holder.getData().getErrorBillItemQty()), System.currentTimeMillis() - startTime);
        }

        return holder;
    }

    /**
     * @param batchModel
     * @param loginUser
     * @return
     */
    private ValueHolderV14<SgStorageBatchUpdateResult> checkServiceParam(SgStorageItemBatchUpdateModel batchModel,
                                                                         User loginUser) {

        ValueHolderV14<SgStorageBatchUpdateResult> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");

        if (loginUser == null) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("操作用户信息不能为空！"));
            return holder;
        }

        if (CollectionUtils.isEmpty(batchModel.getItemList())) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("请求体为空！", loginUser.getLocale()));
        }

        if (ResultCode.FAIL == holder.getCode() && log.isDebugEnabled()) {
            log.error("SgStorageItemBatchUpdateService.checkServiceParam. checkServiceParam error:"
                    + holder.getMessage() + ";");
        }

        return holder;

    }


}
