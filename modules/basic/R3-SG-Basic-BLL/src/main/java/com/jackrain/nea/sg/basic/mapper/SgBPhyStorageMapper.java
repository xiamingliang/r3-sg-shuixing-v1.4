package com.jackrain.nea.sg.basic.mapper;

import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.basic.model.SgPhyStorageUpdateCommonModel;
import com.jackrain.nea.sg.basic.model.table.SgBPhyStorage;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface SgBPhyStorageMapper extends ExtentionMapper<SgBPhyStorage> {

    /**
     * 获取实体仓库存查询表XACT锁
     *
     * @param id 库存查询表ID
     */
    @Select("select count(1) from sg_b_phy_storage where id = #{id} and pg_try_advisory_xact_lock(#{prefix}+id)")
    Integer selectPhyStorageXactLock(@Param("id") Long id, @Param("prefix") Long prefix);

    /**
     * 更新在库量
     *
     * @param model 更新信息
     */
    @Update("<script>" +
            "update sg_b_phy_storage  " +
            "set qty_storage = qty_storage + #{model.qtyChange}," +
            "qty_storage_teus = qty_storage_teus + #{model.qtyTeusChange}," +
            "amt_list = (qty_storage + #{model.qtyChange}) * COALESCE(price_list,0)," +
            "reserve_bigint01 = case when qty_storage + #{model.qtyChange} <![CDATA[<=]]> 0 then 0 else 1 end," +
            "modifieddate=#{model.modifieddate,jdbcType=TIMESTAMP}," +
            "modifiername=#{model.modifiername}, " +
            "modifierename=#{model.modifierename}," +
            "modifierid=#{model.modifierid} " +
            "where ps_c_sku_id = #{model.psCSkuId} and cp_c_phy_warehouse_id = #{model.cpCPhyWarehouseId} " +
            "<if test = 'blncheckNegative == true' >" +
            "and qty_storage + #{model.qtyChange} >= 0" +
            "</if>"  +
            "</script>")
    Integer updateQtyChange(@Param("model") SgPhyStorageUpdateCommonModel model,
                            @Param("blncheckNegative") boolean blncheckNegative);

    /**
     * 更新实体仓库存表基础信息
     *
     * @param model 更新信息
     */
    @Update("<script>" +
            "update sg_b_phy_storage set " +
            "<if test='model.psCBrandId!=null'> ps_c_brand_id = #{model.psCBrandId} </if>" +
            "<if test='model.psCBrandId!=null and model.priceList!=null'>,</if>" +
            "<if test='model.priceList!=null'> price_list = #{model.priceList} </if>" +
            "where ps_c_pro_id = #{model.psCProId} " +
            "<if test='model.psCBrandId!=null and model.priceList!=null'> AND ( ps_c_brand_id = -1 OR price_list IS NULL ) </if>" +
            "<if test='model.psCBrandId==null and model.priceList!=null'> AND price_list IS NULL </if>" +
            "<if test='model.psCBrandId!=null and model.priceList==null'> AND ps_c_brand_id = -1 </if>" +
            "</script>")
    Integer updateStorageBasicInfo(@Param("model") SgBPhyStorage model);


    /**
     * 更新实体仓库存表国标码
     *
     * @param model 更新信息
     */
    @Update("update sg_b_phy_storage " +
            "set gbcode = #{model.gbcode} " +
            "where ps_c_sku_id = #{model.psCSkuId} ")
    Integer updateStorageGBcode(@Param("model") SgBPhyStorage model);

    /**
     * 获取库存查询表逻辑仓ID
     *
     */
    @Select("select distinct cp_c_phy_warehouse_id from sg_b_phy_storage")
    List<Long> selectPhyStoragePhyWarehouseIds();
}