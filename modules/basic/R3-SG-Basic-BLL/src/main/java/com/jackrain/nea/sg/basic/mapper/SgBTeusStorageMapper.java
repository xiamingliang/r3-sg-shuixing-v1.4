package com.jackrain.nea.sg.basic.mapper;

import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.basic.model.SgTeusStorageUpdateCommonModel;
import com.jackrain.nea.sg.basic.model.table.SgBTeusStorage;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface SgBTeusStorageMapper extends ExtentionMapper<SgBTeusStorage> {

    /**
     * 更新占用量
     *
     * @param model 更新信息
     */
    @Update("<script>" +
            "update sg_b_teus_storage  " +
            "set qty_preout = qty_preout + #{model.qtyPreoutChange}," +
            "qty_prein = qty_prein + #{model.qtyPreinChange}," +
            "qty_storage = qty_storage + #{model.qtyStorageChange}," +
            "qty_available = qty_storage - qty_preout + #{model.qtyStorageChange} - #{model.qtyPreoutChange}," +
            "modifieddate=#{model.modifieddate,jdbcType=TIMESTAMP}," +
            "modifiername=#{model.modifiername}, " +
            "modifierename=#{model.modifierename}," +
            "modifierid=#{model.modifierid} " +
            "where ps_c_teus_id = #{model.psCTeusId} and cp_c_store_id = #{model.cpCStoreId} " +
            "<if test = 'blncheckPreoutNegative == true' >" +
            "and qty_preout + #{model.qtyPreoutChange} >= 0" +
            "</if>" +
            "<if test = 'blncheckPreinNegative == true' >" +
            "and qty_prein + #{model.qtyPreinChange} >= 0" +
            "</if>" +
            "<if test = 'blncheckChangeNegative == true' >" +
            "and qty_storage + #{model.qtyStorageChange} >= 0" +
            "</if>" +
            "<if test = 'blncheckAvailableNegative == true' >" +
            "and qty_available + #{model.qtyStorageChange} - #{model.qtyPreoutChange} >= 0" +
            "</if>" +
            "</script>")
    Integer updateQtyChange(@Param("model") SgTeusStorageUpdateCommonModel model,
                            @Param("blncheckPreoutNegative") boolean blncheckPreoutNegative,
                            @Param("blncheckPreinNegative") boolean blncheckPreinNegative,
                            @Param("blncheckChangeNegative") boolean blncheckChangeNegative,
                            @Param("blncheckAvailableNegative") boolean blncheckAvailableNegative);


    /**
     * 获取库存查询表逻辑仓ID
     */
    @Select("select distinct cp_c_store_id from sg_b_teus_storage")
    List<Long> selectTeusStorageStoreIds();

}