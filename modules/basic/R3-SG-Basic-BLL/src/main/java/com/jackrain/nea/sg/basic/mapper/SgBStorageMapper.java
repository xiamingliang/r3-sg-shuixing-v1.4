package com.jackrain.nea.sg.basic.mapper;

import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.basic.model.SgStorageUpdateCommonModel;
import com.jackrain.nea.sg.basic.model.request.SgStorageUpdateControlRequest;
import com.jackrain.nea.sg.basic.model.result.SgSumStorageQueryResult;
import com.jackrain.nea.sg.basic.model.table.SgBStorage;
import com.jackrain.nea.sg.basic.model.table.SgBStorageInclPhy;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.StatementType;

import java.util.List;
import java.util.Map;

@Mapper
public interface SgBStorageMapper extends ExtentionMapper<SgBStorage> {


    /**
     * 查询逻辑仓库存（包含实体仓信息）
     *
     * @param storeIds
     * @param proEcodes
     * @param skuEcodes
     * @return
     */
    @Select("<script>" +
            " select sg_b_storage.*, " +
            " cp_c_store.cp_c_phy_warehouse_id," +
            " cp_c_store.cp_c_phy_warehouse_ecode," +
            " cp_c_store.cp_c_phy_warehouse_ename " +
            " from sg_b_storage left join cp_c_store on sg_b_storage.cp_c_store_id = cp_c_store.id " +
            " where sg_b_storage.cp_c_store_id in " +
            " <foreach collection='storeIds' item='storeId' open='(' separator=',' close=')'> #{storeId} </foreach>" +
            " <if test='proEcodes != null and !proEcodes.isEmpty()'>" +
            " and sg_b_storage.ps_c_pro_ecode in " +
            " <foreach collection='proEcodes' item='proEcode' open='(' separator=',' close=')'> #{proEcode} </foreach>" +
            " </if>" +
            " <if test='skuEcodes != null and !skuEcodes.isEmpty()'>" +
            " and sg_b_storage.ps_c_sku_ecode in " +
            " <foreach collection='skuEcodes' item='skuEcode' open='(' separator=',' close=')'> #{skuEcode} </foreach>" +
            " </if>" +
            "</script>")
    List<SgBStorageInclPhy> selectStrorageInclPhy(@Param("storeIds") List<Long> storeIds,
                                                  @Param("proEcodes") List<String> proEcodes,
                                                  @Param("skuEcodes") List<String> skuEcodes);

    /**
     * 查询逻辑仓库存（包含实体仓信息）
     *
     * @param storeIds
     * @param proEcodes
     * @param skuEcodes
     * @return
     */
    @Select("<script>" +
            " select sg_b_storage.ps_c_sku_id as psCSkuId," +
            " sg_b_storage.ps_c_sku_ecode as psCSkuEcode," +
            " sg_b_storage.ps_c_pro_id as psCProId," +
            " sg_b_storage.ps_c_pro_ecode as psCProEcode," +
            " sg_b_storage.ps_c_pro_ename as psCProEname," +
            " sum(sg_b_storage.qty_preout) as qtyPreout," +
            " sum(sg_b_storage.qty_prein) as qtyPrein," +
            " sum(sg_b_storage.qty_storage) as qtyStorage," +
            " sum(sg_b_storage.amt_cost) as amtCost," +
            " sum(sg_b_storage.qty_available) as qtyAvailable," +
            " cp_c_store.cp_c_phy_warehouse_id as cpCPhyWarehouseId," +
            " cp_c_store.cp_c_phy_warehouse_ecode as cpCPhyWarehouseEcode," +
            " cp_c_store.cp_c_phy_warehouse_ename as cpCPhyWarehouseEname " +
            " from sg_b_storage inner join cp_c_store on sg_b_storage.cp_c_store_id = cp_c_store.id " +
            " where sg_b_storage.qty_available > 0 " +
            " and sg_b_storage.cp_c_store_id in " +
            " <foreach collection='storeIds' item='storeId' open='(' separator=',' close=')'> #{storeId} </foreach>" +
            " <if test='proEcodes != null and !proEcodes.isEmpty()'>" +
            " and sg_b_storage.ps_c_pro_ecode in " +
            " <foreach collection='proEcodes' item='proEcode' open='(' separator=',' close=')'> #{proEcode} </foreach>" +
            " </if>" +
            " <if test='skuEcodes != null and !skuEcodes.isEmpty()'>" +
            " and sg_b_storage.ps_c_sku_ecode in " +
            " <foreach collection='skuEcodes' item='skuEcode' open='(' separator=',' close=')'> #{skuEcode} </foreach>" +
            " </if>" +
            " group by cp_c_store.cp_c_phy_warehouse_id,cp_c_store.cp_c_phy_warehouse_ecode,cp_c_store.cp_c_phy_warehouse_ename," +
            " sg_b_storage.ps_c_sku_id,sg_b_storage.ps_c_sku_ecode," +
            " sg_b_storage.ps_c_pro_id,sg_b_storage.ps_c_pro_ecode,sg_b_storage.ps_c_pro_ename " +
            "</script>")
    List<SgSumStorageQueryResult> querySumStorageGrpPhy(@Param("storeIds") List<Long> storeIds,
                                                        @Param("proEcodes") List<String> proEcodes,
                                                        @Param("skuEcodes") List<String> skuEcodes);

    /**
     * 获取库存查询表XACT锁
     *
     * @param id 库存查询表ID
     */
    @Select("select count(1) from sg_b_storage where id = #{id} and pg_try_advisory_xact_lock(#{prefix}+id)")
    Integer selectStorageXactLock(@Param("id") Long id, @Param("prefix") Long prefix);

    /**
     * 调用更新逻辑仓库存函数fn_sg_storage_occu
     *
     * @param param 更新信息
     */
    @Select("{CALL fn_sg_storage_occu(#{p_item,mode=IN,jdbcType=VARCHAR}," +
            "#{p_preoutoperatetype,mode=IN,jdbcType=INTEGER}," +
            "#{p_item_error,mode=OUT,jdbcType=VARCHAR}," +
            "#{p_out_preoutoperatetype,mode=OUT,jdbcType=INTEGER})}")
    @Options(statementType = StatementType.CALLABLE)
    void updateStorageQtyFn(Map<String, Object> param);


    /**
     * 更新占用量
     *
     * @param model 更新信息
     */
    @Update("<script>" +
            "update sg_b_storage  " +
            "set qty_preout = qty_preout + #{model.qtyChange}," +
            "qty_available = qty_storage - qty_preout - #{model.qtyChange}," +
            "modifieddate=#{model.modifieddate,jdbcType=TIMESTAMP}," +
            "modifiername=#{model.modifiername}, " +
            "modifierename=#{model.modifierename}," +
            "modifierid=#{model.modifierid} " +
            "where ps_c_sku_id = #{model.psCSkuId} and cp_c_store_id = #{model.cpCStoreId} " +
            "<when test = 'control.isNegativePreout == false'>" +
            "and qty_available - #{model.qtyChange} >= 0" +
            "</when>" +
            "</script>")
    Integer updateQtyPreout(@Param("model") SgStorageUpdateCommonModel model,
                            @Param("control") SgStorageUpdateControlRequest controlModel);

    /**
     * 更新在途量
     *
     * @param model 更新信息
     */
    @Update("update sg_b_storage  " +
            "set qty_prein = qty_prein + #{model.qtyChange}," +
            "modifieddate=#{model.modifieddate,jdbcType=TIMESTAMP}," +
            "modifiername=#{model.modifiername}, " +
            "modifierename=#{model.modifierename}," +
            "modifierid=#{model.modifierid} " +
            "where ps_c_sku_id = #{model.psCSkuId} and cp_c_store_id = #{model.cpCStoreId} ")
    Integer updateQtyPrein(@Param("model") SgStorageUpdateCommonModel model);

    /**
     * 更新在库量
     *
     * @param model 更新信息
     */
    @Update("update sg_b_storage  " +
            "set qty_storage = qty_storage + #{model.qtyChange}," +
            "qty_available = qty_storage - qty_preout + #{model.qtyChange}," +
            "modifieddate=#{model.modifieddate,jdbcType=TIMESTAMP}," +
            "modifiername=#{model.modifiername}, " +
            "modifierename=#{model.modifierename}," +
            "modifierid=#{model.modifierid} " +
            "where ps_c_sku_id = #{model.psCSkuId} and cp_c_store_id = #{model.cpCStoreId} ")
    Integer updateQtyChange(@Param("model") SgStorageUpdateCommonModel model);

    /**
     * 更新逻辑仓库存表基础信息
     *
     * @param model 更新信息
     */
    @Update("<script>" +
            "update sg_b_storage set " +
            "<if test='model.psCBrandId!=null'> ps_c_brand_id = #{model.psCBrandId} </if>" +
            "<if test='model.psCBrandId!=null and model.priceList!=null'>,</if>" +
            "<if test='model.priceList!=null'> price_list = #{model.priceList} </if>" +
            "where ps_c_pro_id = #{model.psCProId} " +
            "<if test='model.psCBrandId!=null and model.priceList!=null'> AND ( ps_c_brand_id = -1 OR price_list IS NULL ) </if>" +
            "<if test='model.psCBrandId==null and model.priceList!=null'> AND price_list IS NULL </if>" +
            "<if test='model.psCBrandId!=null and model.priceList==null'> AND ps_c_brand_id = -1 </if>" +
            "</script>")
    Integer updateStorageBasicInfo(@Param("model") SgBStorage model);

    /**
     * 更新逻辑仓库存表国标码
     *
     * @param model 更新信息
     */
    @Update("update sg_b_storage " +
            "set gbcode = #{model.gbcode} " +
            "where ps_c_sku_id = #{model.psCSkuId} ")
    Integer updateStorageGBcode(@Param("model") SgBStorage model);

	/**
     * 获取库存查询表逻辑仓ID
     *
     */
    @Select("select distinct cp_c_store_id from sg_b_storage")
    List<Long> selectStorageStoreIds();
}