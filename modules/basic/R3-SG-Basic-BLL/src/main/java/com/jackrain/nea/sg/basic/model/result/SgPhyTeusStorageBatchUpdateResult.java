package com.jackrain.nea.sg.basic.model.result;

import com.jackrain.nea.sg.basic.model.SgPhyTeusStorageUpdateCommonModel;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/3/17 14:50
 */
@Data
public class SgPhyTeusStorageBatchUpdateResult implements Serializable {

    /*
     * 错误明细件数（包含发生逻辑错误明细+XactLock超过最大执行数的明细）
     */
    private long errorBillItemQty = 0;
    /*
     * Redis更新流水键
     */
    private List<String> redisBillFtpKeyList;
    /*
     *缺货明细列表
     */
    private List<SgPhyTeusStorageUpdateCommonModel> outStockItemList;
    /*
     *逻辑仓库存同步MQ列表
     */
    private List<SgPhyTeusStorageUpdateCommonModel> redisSynchMqItemList;


    public SgPhyTeusStorageBatchUpdateResult() {
        this.errorBillItemQty = 0;
    }
}
