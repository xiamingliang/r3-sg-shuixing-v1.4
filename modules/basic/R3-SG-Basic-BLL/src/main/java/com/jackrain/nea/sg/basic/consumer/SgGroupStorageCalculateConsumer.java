package com.jackrain.nea.sg.basic.consumer;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.aliyun.openservices.ons.api.Action;
import com.aliyun.openservices.ons.api.Message;
import com.aliyun.openservices.ons.api.impl.util.MsgConvertUtil;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.model.request.SgGroupStoragePoolBillRequest;
import com.jackrain.nea.sg.basic.model.request.SgGroupStoragePoolRequest;
import com.jackrain.nea.sg.basic.model.result.SgGoupStorageCalculateResult;
import com.jackrain.nea.sg.basic.services.SgGroupStoragePoolService;
import com.jackrain.nea.sg.basic.utils.StorageLogUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.face.impl.UserImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;

/**
 * 组合商品库存计算消费者
 *
 * @author 舒威
 * @since 2019/7/20
 * create at : 2019/7/20 18:38
 */
@Slf4j
@Component
public class SgGroupStorageCalculateConsumer {

    @Autowired
    private SgGroupStoragePoolService service;

    public Action consume(Message message) {

        Action consumeResult = Action.CommitMessage;

        String messageTopic = message.getTopic();
        String messageKey = message.getKey();
        String messageTag = message.getTag();
        String messageBody = null;

        try {

            messageBody = MsgConvertUtil.objectDeserialize(message.getBody()).toString();

            if (log.isDebugEnabled()) {
                log.debug("Start SgGroupStorageCalculateConsumer.consume. ReceiveParams:messageTopic={} " +
                                "messageKey={} messageTag={} messageBody={};",
                        messageTopic, messageKey, messageTag, messageBody);
            }

            HashMap paramMap = JSON.parseObject(messageBody, HashMap.class);

            //MQ消息实体封装

            User loginUser = JSONObject.parseObject(JSONObject.toJSONString(paramMap.get("user")), UserImpl.class);
            SgGroupStoragePoolRequest request = new SgGroupStoragePoolRequest();
            request.setLoginUser(loginUser);

            List<SgGroupStoragePoolBillRequest> billList = JSON.parseArray(JSONObject.toJSONString(paramMap.get("param")),
                    SgGroupStoragePoolBillRequest.class);
            request.setPoolBillRequests(billList);

            ValueHolderV14<SgGoupStorageCalculateResult> updateResult = service.addGroupStoragePoll(request);

            if (ResultCode.FAIL == updateResult.getCode()) {
                consumeResult = Action.ReconsumeLater;
            }

        } catch (Exception e) {

            StorageLogUtils.getGroupErrMessage(e, messageKey, null);

            consumeResult = Action.ReconsumeLater;
        }

        return consumeResult;

    }
}
