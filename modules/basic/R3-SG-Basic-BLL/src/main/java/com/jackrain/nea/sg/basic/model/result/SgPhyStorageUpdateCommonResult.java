package com.jackrain.nea.sg.basic.model.result;

import com.jackrain.nea.sg.basic.model.SgPhyStorageUpdateCommonModel;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/3/13 15:53
 */
@Data
public class SgPhyStorageUpdateCommonResult implements Serializable {

    //最近数据库操作结果
    private int countResult;
    //期初数量
    private BigDecimal qtyBegin;
    //期末数量
    private BigDecimal qtyEnd;
    //箱期初数量
    private BigDecimal qtyChangeTeusBegin;
    //箱期末数量
    private BigDecimal qtyChangeTeusEnd;
    //缺货明细详情
    private List<SgPhyStorageUpdateCommonModel> outStockItemList;

    public SgPhyStorageUpdateCommonResult() {
        this.countResult = 0;
    }

}
