package com.jackrain.nea.sg.basic.logic;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.model.util.ModelUtil;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.mapper.SgBPhyStorageChangeFtpMapper;
import com.jackrain.nea.sg.basic.mapper.SgBPhyStorageMapper;
import com.jackrain.nea.sg.basic.model.SgPhyStorageUpdateCommonModel;
import com.jackrain.nea.sg.basic.model.SgStorageUpdateControlModel;
import com.jackrain.nea.sg.basic.model.result.SgPhyStorageUpdateCommonResult;
import com.jackrain.nea.sg.basic.model.table.SgBPhyStorage;
import com.jackrain.nea.sg.basic.model.table.SgBPhyStorageChangeFtp;
import com.jackrain.nea.sg.basic.utils.StorageUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/3/13 14:50
 */
@Component
@Slf4j
public class SgPhyStorageLogic {

    @Autowired
    private SgBPhyStorageMapper sgBPhyStorageMapper;

    @Autowired
    private SgBPhyStorageChangeFtpMapper sgBPhyStorageChangeFtpMapper;

    /**
     * 更新库存及流水
     *
     * @param updateModel
     * @param controlModel
     * @param loginUser
     * @return ValueHolderV14<SgPhyStorageUpdateCommonResult>
     */
    public ValueHolderV14<SgPhyStorageUpdateCommonResult> updatePhyStorageChangeWithFtp(
            SgPhyStorageUpdateCommonModel updateModel,
            SgStorageUpdateControlModel controlModel,
            User loginUser) {

        ValueHolderV14<SgPhyStorageUpdateCommonResult> holder;

        holder = updatePhyStorageChange(updateModel, controlModel, loginUser);

        if (ResultCode.FAIL == holder.getCode()) {
            return holder;
        }

        BeanUtils.copyProperties(holder.getData(), updateModel);

        holder = insertPhyStorageChangeFtp(updateModel, loginUser);

        return holder;
    }

    /**
     * 更新库存
     *
     * @param updateModel
     * @param controlModel
     * @param loginUser
     * @return ValueHolderV14<SgPhyStorageUpdateCommonResult>
     */
    public ValueHolderV14<SgPhyStorageUpdateCommonResult> updatePhyStorageChange(
            SgPhyStorageUpdateCommonModel updateModel,
            SgStorageUpdateControlModel controlModel,
            User loginUser) {

        Date systemDate = new Date();
        ValueHolderV14<SgPhyStorageUpdateCommonResult> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        SgPhyStorageUpdateCommonResult commonResult = new SgPhyStorageUpdateCommonResult();

        updateModel.setModifierid(loginUser.getId() == null ? null : loginUser.getId().longValue());
        updateModel.setModifierename(loginUser.getEname());
        updateModel.setModifiername(loginUser.getName());
        updateModel.setModifieddate(systemDate);

        boolean blncheckNegative = !controlModel.isNegativeStorage() &&
                (updateModel.getQtyChange().compareTo(BigDecimal.ZERO) < 0 ? true : false);

        int updateResult = sgBPhyStorageMapper.updateQtyChange(updateModel,
                blncheckNegative);

        //在库不允许负库存 并且 更新0条记录的情况下
        if (!controlModel.isNegativeStorage() && updateResult <= 0) {

            holder.setCode(ResultCode.FAIL);

            if (log.isDebugEnabled()) {
                log.debug("SgPhyStorageLogic.updatePhyStorageChange. 当前库存变动明细库存不足！！单据编号:{}，实体仓ID:{}，商品条码ID:{}，变动数量:{}",
                        updateModel.getBillNo(), updateModel.getCpCPhyWarehouseId(),
                        updateModel.getPsCSkuId(), updateModel.getQtyChange());
            }

            List<SgPhyStorageUpdateCommonModel> outStockItemList = new ArrayList<>();
            outStockItemList.add(updateModel);
            commonResult.setOutStockItemList(outStockItemList);

            holder.setData(commonResult);
            holder.setMessage(Resources.getMessage("实体仓库存不足！",
                    loginUser.getLocale(), updateModel.getCpCPhyWarehouseEcode(), updateModel.getPsCSkuEcode()));
            return holder;

        }

        commonResult.setCountResult(updateResult);

        List<SgBPhyStorage> selectResult = sgBPhyStorageMapper.selectList(
                new QueryWrapper<SgBPhyStorage>().lambda()
                        .eq(SgBPhyStorage::getCpCPhyWarehouseId, updateModel.getCpCPhyWarehouseId())
                        .eq(SgBPhyStorage::getPsCSkuId, updateModel.getPsCSkuId()));

        if (selectResult != null && selectResult.size() > 0) {

            //本版本的期初数量暂时通过期末数量为基准计算

            BigDecimal qtyEnd = selectResult.get(0).getQtyStorage();
            commonResult.setQtyBegin(qtyEnd.subtract(updateModel.getQtyChange()));
            commonResult.setQtyEnd(qtyEnd);

            if (selectResult.get(0).getQtyStorageTeus() == null) {
                selectResult.get(0).setQtyStorageTeus(BigDecimal.ZERO);
            }

            BigDecimal qtyChangeTeusEnd = selectResult.get(0).getQtyStorageTeus();
            commonResult.setQtyChangeTeusBegin(qtyChangeTeusEnd.subtract(updateModel.getQtyTeusChange()));
            commonResult.setQtyChangeTeusEnd(qtyChangeTeusEnd);

        }

        holder.setData(commonResult);
        holder.setCode(ResultCode.SUCCESS);
        holder.setMessage(Resources.getMessage("实体仓在库数量更新成功！", loginUser.getLocale()));

        return holder;
    }

    /**
     * 记录库存变动流水
     *
     * @param updateModel
     * @param loginUser
     * @return ValueHolderV14<SgPhyStorageUpdateCommonResult>
     */
    public ValueHolderV14<SgPhyStorageUpdateCommonResult> insertPhyStorageChangeFtp(
            SgPhyStorageUpdateCommonModel updateModel,
            User loginUser) {

        ValueHolderV14<SgPhyStorageUpdateCommonResult> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        SgPhyStorageUpdateCommonResult commonResult = new SgPhyStorageUpdateCommonResult();
        SgBPhyStorageChangeFtp changeUpdateModel = new SgBPhyStorageChangeFtp();

        BeanUtils.copyProperties(updateModel, changeUpdateModel);
        StorageUtils.setBModelDefalutData(changeUpdateModel, loginUser);
        changeUpdateModel.setId(ModelUtil.getSequence(SgConstants.SG_B_PHY_STORAGE_CHANGE_FTP));
        changeUpdateModel.setOwnerename(loginUser.getEname());
        changeUpdateModel.setModifierename(loginUser.getEname());
        changeUpdateModel.setQtyChangeTeus(updateModel.getQtyTeusChange());
        //设置Redis更新流水键
        changeUpdateModel.setReserveVarchar01(updateModel.getRedisBillFtpKey());

        int insertResult = sgBPhyStorageChangeFtpMapper.insert(changeUpdateModel);

        if (insertResult <= 0) {
            log.error("SgPhyStorageLogic.insertPhyStorageChangeFtp 实体仓库存变动流水插入失败！单据编号:{},实体仓编码:{},商品条码:{}",
                    updateModel.getBillNo(),
                    updateModel.getCpCPhyWarehouseEcode(),
                    updateModel.getPsCSkuEcode());
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("实体仓库存变动流水插入失败！",
                    loginUser.getLocale(),
                    updateModel.getBillNo(),
                    updateModel.getCpCPhyWarehouseEcode(),
                    updateModel.getPsCSkuEcode()));
            return holder;
        }

        holder.setData(commonResult);
        holder.setCode(ResultCode.SUCCESS);
        holder.setMessage(Resources.getMessage("实体仓库存变动流水插入成功！", loginUser.getLocale()));

        return holder;
    }

    /**
     * 批量记录库存变动流水
     *
     * @param updateModelList
     * @param loginUser
     * @return ValueHolderV14<SgPhyStorageUpdateCommonResult>
     */
    public ValueHolderV14<SgPhyStorageUpdateCommonResult> insertPhyStorageChangeFtpList(
            List<SgPhyStorageUpdateCommonModel> updateModelList,
            User loginUser) {

        ValueHolderV14<SgPhyStorageUpdateCommonResult> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        SgPhyStorageUpdateCommonResult commonResult = new SgPhyStorageUpdateCommonResult();
        List<SgBPhyStorageChangeFtp> batchInsertList = new ArrayList<>();

        for (SgPhyStorageUpdateCommonModel updateModel : updateModelList) {

            SgBPhyStorageChangeFtp changeUpdateModel = new SgBPhyStorageChangeFtp();

            BeanUtils.copyProperties(updateModel, changeUpdateModel);
            StorageUtils.setBModelDefalutData(changeUpdateModel, loginUser);
            changeUpdateModel.setId(ModelUtil.getSequence(SgConstants.SG_B_PHY_STORAGE_CHANGE_FTP));
            changeUpdateModel.setOwnerename(loginUser.getEname());
            changeUpdateModel.setModifierename(loginUser.getEname());
            changeUpdateModel.setQtyChangeTeus(updateModel.getQtyTeusChange());
            //设置Redis更新流水键
            changeUpdateModel.setReserveVarchar01(updateModel.getRedisBillFtpKey());

            batchInsertList.add(changeUpdateModel);
        }

        List<List<SgBPhyStorageChangeFtp>> insertPageList =
                StorageUtils.getBaseModelPageList(batchInsertList, SgConstants.SG_COMMON_INSERT_PAGE_SIZE);

        for (List<SgBPhyStorageChangeFtp> pageList : insertPageList) {

            if (CollectionUtils.isEmpty(pageList)) {
                continue;
            }

            int insertResult = sgBPhyStorageChangeFtpMapper.batchInsert(pageList);

            if (insertResult != pageList.size()) {
                log.error("SgPhyStorageLogic.insertPhyStorageChangeFtpList 实体仓批量库存变动流水插入失败！");
                holder.setCode(ResultCode.FAIL);
                holder.setMessage(Resources.getMessage("实体仓批量库存变动流水插入失败！",
                        loginUser.getLocale()));
                return holder;
            }

        }

        holder.setData(commonResult);
        holder.setCode(ResultCode.SUCCESS);
        holder.setMessage(Resources.getMessage("实体仓批量库存变动流水插入成功！", loginUser.getLocale()));

        return holder;
    }

    /**
     * 初始化库存
     *
     * @param updateModel
     * @param loginUser
     * @return ValueHolderV14<SgPhyStorageUpdateCommonResult>
     */
    public ValueHolderV14<SgPhyStorageUpdateCommonResult> initPhyStorage(
            SgPhyStorageUpdateCommonModel updateModel,
            User loginUser) {

        ValueHolderV14<SgPhyStorageUpdateCommonResult> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        SgPhyStorageUpdateCommonResult commonResult = new SgPhyStorageUpdateCommonResult();

        SgBPhyStorage sgBPhyStorage = new SgBPhyStorage();
        BeanUtils.copyProperties(updateModel, sgBPhyStorage);
        sgBPhyStorage.setId(ModelUtil.getSequence(SgConstants.SG_B_PHY_STORAGE));
        sgBPhyStorage.setQtyStorage(BigDecimal.ZERO);
        sgBPhyStorage.setQtyStorageTeus(BigDecimal.ZERO);
        sgBPhyStorage.setPsCBrandId(-1L);

        int insertResult = sgBPhyStorageMapper.insert(sgBPhyStorage);

        if (insertResult <= 0) {
            log.error("SgPhyStorageLogic.initPhyStorage 实体仓库存初始化插入失败！实体仓编码:{},商品条码:{}",
                    updateModel.getCpCPhyWarehouseEcode(),
                    updateModel.getPsCSkuEcode());
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("实体仓库存初始化插入失败！",
                    loginUser.getLocale(),
                    updateModel.getCpCPhyWarehouseEcode(),
                    updateModel.getPsCSkuEcode()));
            return holder;
        }

        holder.setData(commonResult);
        holder.setCode(ResultCode.SUCCESS);
        holder.setMessage(Resources.getMessage("实体仓库存初始化插入成功！", loginUser.getLocale()));

        return holder;
    }

    /**
     * 批量初始化库存
     *
     * @param updateModelList
     * @param loginUser
     * @return ValueHolderV14<SgPhyStorageUpdateCommonResult>
     */
    public ValueHolderV14<SgPhyStorageUpdateCommonResult> initPhyStorageList(
            List<SgPhyStorageUpdateCommonModel> updateModelList,
            User loginUser) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgPhyStorageLogic.initPhyStorageList. ReceiveParams:updateModelList="
                    + JSONObject.toJSONString(updateModelList) + ";");
        }

        ValueHolderV14<SgPhyStorageUpdateCommonResult> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        SgPhyStorageUpdateCommonResult commonResult = new SgPhyStorageUpdateCommonResult();
        List<SgBPhyStorage> batchInitList = new ArrayList<>();

        for (SgPhyStorageUpdateCommonModel updateModel : updateModelList) {

            SgBPhyStorage sgBPhyStorage = new SgBPhyStorage();
            BeanUtils.copyProperties(updateModel, sgBPhyStorage);
            sgBPhyStorage.setId(ModelUtil.getSequence(SgConstants.SG_B_PHY_STORAGE));
            sgBPhyStorage.setQtyStorage(BigDecimal.ZERO);
            sgBPhyStorage.setQtyStorageTeus(BigDecimal.ZERO);
            sgBPhyStorage.setPsCBrandId(-1L);

            batchInitList.add(sgBPhyStorage);
        }

        List<List<SgBPhyStorage>> insertPageList =
                StorageUtils.getBaseModelPageList(batchInitList, SgConstants.SG_COMMON_INSERT_PAGE_SIZE);

        for (List<SgBPhyStorage> pageList : insertPageList) {

            if (CollectionUtils.isEmpty(pageList)) {
                continue;
            }

            int insertResult = sgBPhyStorageMapper.batchInsert(pageList);

            if (insertResult != pageList.size()) {
                log.error("SgPhyStorageLogic.initPhyStorageList 实体仓批量库存初始化插入失败！");
                holder.setCode(ResultCode.FAIL);
                holder.setMessage(Resources.getMessage("实体仓批量库存初始化插入失败！",
                        loginUser.getLocale()));
                return holder;
            }
        }

        holder.setData(commonResult);
        holder.setCode(ResultCode.SUCCESS);
        holder.setMessage(Resources.getMessage("实体仓批量库存初始化插入成功！", loginUser.getLocale()));

        return holder;
    }
}
