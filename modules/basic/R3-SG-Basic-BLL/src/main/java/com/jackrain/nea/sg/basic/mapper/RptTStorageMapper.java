package com.jackrain.nea.sg.basic.mapper;

import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.basic.model.result.SgRcStorageQueryResult;
import com.jackrain.nea.sg.basic.model.result.SgRcStorageResult;
import com.jackrain.nea.sg.basic.model.table.RptTStorage;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.mapping.StatementType;

import java.util.HashMap;
import java.util.List;

@Mapper
public interface RptTStorageMapper extends ExtentionMapper<RptTStorage> {
    @Select("{CALL ${schema}.fun_rpt_storage(#{sgRcStorageResult.startDate,jdbcType=VARCHAR}," +
            "#{sgRcStorageResult.endDate,jdbcType=VARCHAR},#{sgRcStorageResult.storeCode,jdbcType=VARCHAR},#{sgRcStorageResult.brandId,jdbcType=INTEGER}," +
            "#{sgRcStorageResult.proEcode,jdbcType=VARCHAR},#{sgRcStorageResult.skuCode,jdbcType=VARCHAR},#{sgRcStorageResult.gbcode,jdbcType=VARCHAR})}")
    @Options(statementType = StatementType.CALLABLE)
    HashMap generateStorageBatchNo(@Param("schema")String schema, @Param("sgRcStorageResult")SgRcStorageResult sgRcStorageResult);

    @Select("<script> select * from (select RPT_DATE,CP_C_STORE_ENAME," +
            "sum(case logistics_storage_type_ecode when '0001' then QTY_END else 0 end) as SHOT ," +
            "sum(case logistics_storage_type_ecode when '0002' then QTY_END else 0 end) as CLOTH ," +
            "sum(case logistics_storage_type_ecode when '0003' then QTY_END else 0 end) as PART ," +
            "sum(case logistics_storage_type_ecode when '0004' then QTY_END else 0 end) as STOCK ," +
            "sum(case logistics_storage_type_ecode when '0005' then QTY_END else 0 end) as PAPER " +
            " from v_rpt_t_storage where 1=1 " +
            "<if test = 'sgRcStorageResult.sdate !=null'> and rpt_date &gt;= #{sgRcStorageResult.sdate} </if> " +
            "<if test = 'sgRcStorageResult.edate != null'> and rpt_date &lt;= #{sgRcStorageResult.edate} </if> " +
            "<if test = 'sgRcStorageResult.cpCStoreId != null'> and CP_C_STORE_ID = #{sgRcStorageResult.cpCStoreId} </if>" +
            "<if test = 'sgRcStorageResult.version != null and !sgRcStorageResult.version.isEmpty()'> and VERSION =#{sgRcStorageResult.version} </if> " +
            "group by rpt_date,cp_c_store_ename,version " +
            " order by cp_c_store_ename,rpt_date asc) a " +
            "limit #{sgRcStorageResult.pageNum} offset #{sgRcStorageResult.offect} </script>")
    List<SgRcStorageQueryResult> queryDataForStorage(@Param("sgRcStorageResult") SgRcStorageResult sgRcStorageResult);


    @Select("<script> select RPT_DATE,CP_C_STORE_ENAME," +
            "sum(case logistics_storage_type_ecode when '0001' then QTY_END else 0 end) as SHOT ," +
            "sum(case logistics_storage_type_ecode when '0002' then QTY_END else 0 end) as CLOTH ," +
            "sum(case logistics_storage_type_ecode when '0003' then QTY_END else 0 end) as PART ," +
            "sum(case logistics_storage_type_ecode when '0004' then QTY_END else 0 end) as STOCK ," +
            "sum(case logistics_storage_type_ecode when '0005' then QTY_END else 0 end) as PAPER " +
            " from v_rpt_t_storage where 1=1 " +
            "<if test = 'sgRcStorageResult.sdate !=null'> and rpt_date  &gt;= #{sgRcStorageResult.sdate} </if> " +
            "<if test = 'sgRcStorageResult.edate != null'> and rpt_date &lt;= #{sgRcStorageResult.edate} </if> " +
            "<if test = 'sgRcStorageResult.cpCStoreId != null'> and CP_C_STORE_ID = #{sgRcStorageResult.cpCStoreId} </if>" +
            "<if test = 'sgRcStorageResult.version != null and !sgRcStorageResult.version.isEmpty()'> and VERSION =#{sgRcStorageResult.version} </if> " +
            " group by rpt_date,cp_c_store_ename,version order by cp_c_store_ename,rpt_date asc </script>")
    List<SgRcStorageQueryResult> exportDataForStorage(@Param("sgRcStorageResult") SgRcStorageResult sgRcStorageResult);

    @Select("<script> select count(1) from (select rpt_date,cp_c_store_ename, " +
            "sum(case logistics_storage_type_ecode when '0001' then QTY_END else 0 end) as SHOT ," +
            "sum(case logistics_storage_type_ecode when '0002' then QTY_END else 0 end) as CLOTH ," +
            "sum(case logistics_storage_type_ecode when '0003' then QTY_END else 0 end) as PART ," +
            "sum(case logistics_storage_type_ecode when '0004' then QTY_END else 0 end) as STOCK ," +
            "sum(case logistics_storage_type_ecode when '0005' then QTY_END else 0 end) as PAPER " +
            " from v_rpt_t_storage where 1=1 " +
            "<if test = 'sgRcStorageResult.sdate !=null'> and rpt_date &gt;= #{sgRcStorageResult.sdate} </if> " +
            "<if test = 'sgRcStorageResult.edate != null'> and rpt_date &lt;= #{sgRcStorageResult.edate} </if> " +
            "<if test = 'sgRcStorageResult.cpCStoreId != null'> and CP_C_STORE_ID = #{sgRcStorageResult.cpCStoreId} </if>" +
            "<if test = 'sgRcStorageResult.version != null and !sgRcStorageResult.version.isEmpty()'> and VERSION =#{sgRcStorageResult.version} </if> " +
            " group by rpt_date,cp_c_store_ename,version order by cp_c_store_ename,rpt_date asc ) a </script>")
    Integer countForStorage(@Param("sgRcStorageResult") SgRcStorageResult sgRcStorageResult);
}