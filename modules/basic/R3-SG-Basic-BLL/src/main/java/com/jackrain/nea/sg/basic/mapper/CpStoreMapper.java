package com.jackrain.nea.sg.basic.mapper;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.basic.model.table.CpCStore;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.jdbc.SQL;

import java.util.Set;

/**
 * @author: zhu lin yu
 * @since: 2019/4/19
 * create at : 2019/4/19 17:18
 */
@Mapper
public interface CpStoreMapper extends ExtentionMapper<CpCStore> {


    class CpStoreProvider {

        public String update(final JSONObject jsonObject) {
            return new SQL() {
                {
                    Set<String> keySet = jsonObject.keySet();
                    UPDATE("CP_C_STORE");
                    for (int i = 0; i < jsonObject.size(); i++) {
                        String key = (String) keySet.toArray()[i];
                        if ("ID".equals(key)) {
                            WHERE("ID=#{" + key + "}");
                        } else {
                            SET(key + "= #{" + key + "}");
                        }
                    }
                }
            }.toString();
        }

        public String insert(final JSONObject jsonObject) {
            return new SQL() {
                {
                    Set<String> keySet = jsonObject.keySet();
                    INSERT_INTO("CP_C_STORE");
                    for (int i = 0; i < jsonObject.size(); i++) {
                        String key = (String) keySet.toArray()[i];
                        VALUES(key, "#{" + key + "}");
                    }
                }
            }.toString();
        }
    }

    @InsertProvider(type = CpStoreProvider.class, method = "insert")
    int insertStore(JSONObject jsonObject);

    @UpdateProvider(type = CpStoreProvider.class, method = "update")
    int updateStore(JSONObject jsonObject);
}
