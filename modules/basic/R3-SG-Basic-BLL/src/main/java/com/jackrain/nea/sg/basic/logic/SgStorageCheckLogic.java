package com.jackrain.nea.sg.basic.logic;

import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.model.SgPhyStorageUpdateCommonModel;
import com.jackrain.nea.sg.basic.model.SgPhyTeusStorageUpdateCommonModel;
import com.jackrain.nea.sg.basic.model.SgStorageUpdateCommonModel;
import com.jackrain.nea.sg.basic.model.SgTeusStorageUpdateCommonModel;
import com.jackrain.nea.sg.basic.model.result.SgPhyStorageUpdateCommonResult;
import com.jackrain.nea.sg.basic.model.result.SgPhyTeusStorageUpdateCommonResult;
import com.jackrain.nea.sg.basic.model.result.SgStorageUpdateCommonResult;
import com.jackrain.nea.sg.basic.model.result.SgTeusStorageUpdateCommonResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;

/**
 * @Description: 库存中心通用检查逻辑
 * @Author: chenb
 * @Date: 2019/4/1 15:03
 */
public class SgStorageCheckLogic {

    /**
     * @param request
     * @param loginUser
     * @return
     */
    public static ValueHolderV14<SgStorageUpdateCommonResult> checkServiceParam(
            SgStorageUpdateCommonModel request,
            User loginUser) {
        ValueHolderV14<SgStorageUpdateCommonResult> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");

        if (request.getCpCStoreId() == null) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("店仓不能为空！", loginUser.getLocale())
                    .concat(" " + holder.getMessage()));
        }

        if (request.getPsCSkuId() == null) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("商品条码不能为空！", loginUser.getLocale())
                    .concat(" " + holder.getMessage()));
        }

        return holder;

    }

    /**
     * @param request
     * @param loginUser
     * @return
     */
    public static ValueHolderV14<SgTeusStorageUpdateCommonResult> checkServiceParam(
            SgTeusStorageUpdateCommonModel request,
            User loginUser) {
        ValueHolderV14<SgTeusStorageUpdateCommonResult> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");

        if (request.getCpCStoreId() == null) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("店仓不能为空！", loginUser.getLocale())
                    .concat(" " + holder.getMessage()));
        }

        if (request.getPsCTeusId() == null) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("箱号不能为空！", loginUser.getLocale())
                    .concat(" " + holder.getMessage()));
        }

        return holder;

    }

    /**
     * @param request
     * @param loginUser
     * @return
     */
    public static ValueHolderV14<SgPhyStorageUpdateCommonResult> checkServiceParam(
            SgPhyStorageUpdateCommonModel request,
            User loginUser) {
        ValueHolderV14<SgPhyStorageUpdateCommonResult> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");

        if (request.getCpCPhyWarehouseId() == null) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("实体仓不能为空！", loginUser.getLocale())
                    .concat(" " + holder.getMessage()));
        }

        if (request.getPsCSkuId() == null) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("商品条码不能为空！", loginUser.getLocale())
                    .concat(" " + holder.getMessage()));
        }

        return holder;

    }

    /**
     * @param request
     * @param loginUser
     * @return
     */
    public static ValueHolderV14<SgPhyTeusStorageUpdateCommonResult> checkServiceParam(
            SgPhyTeusStorageUpdateCommonModel request,
            User loginUser) {
        ValueHolderV14<SgPhyTeusStorageUpdateCommonResult> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");

        if (request.getCpCPhyWarehouseId() == null) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("实体仓不能为空！", loginUser.getLocale())
                    .concat(" " + holder.getMessage()));
        }

        if (request.getPsCTeusId() == null) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("箱号不能为空！", loginUser.getLocale())
                    .concat(" " + holder.getMessage()));
        }

        return holder;

    }
}
