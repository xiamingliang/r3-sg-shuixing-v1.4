package com.jackrain.nea.sg.basic.services;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.config.SgStorageBoxConfig;
import com.jackrain.nea.sg.basic.model.SgStorageRedisSynchModel;
import com.jackrain.nea.sg.basic.model.SgStorageUpdateCommonModel;
import com.jackrain.nea.sg.basic.model.SgTeusStorageUpdateCommonModel;
import com.jackrain.nea.sg.basic.model.request.SgStorageBatchUpdateRequest;
import com.jackrain.nea.sg.basic.model.request.SgStorageSingleUpdateRequest;
import com.jackrain.nea.sg.basic.model.result.SgStorageBatchUpdateResult;
import com.jackrain.nea.sg.basic.model.result.SgTeusStorageBatchUpdateResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/6/1 12:51
 */
@Component
@Slf4j
public class SgStorageRedisBillBatchUpdateService {

    @Autowired
    private SgStorageRedisUpdateService stoUpdService;

    @Autowired
    private SgTeusStorageRedisUpdateService teusStoUpdService;

    @Autowired
    private SgStorageRedisSynchService sgStorageRedisSynchService;

    @Autowired
    private SgTeusStorageRedisSynchService sgTeusStorageRedisSynchService;

    @Autowired
    private SgStorageBoxConfig sgStorageBoxConfig;

    public ValueHolderV14<SgStorageBatchUpdateResult> updateStorageBatch(SgStorageBatchUpdateRequest request) {

        ValueHolderV14<SgStorageBatchUpdateResult> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        ValueHolderV14<SgStorageBatchUpdateResult> stoHolder = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        ValueHolderV14<SgTeusStorageBatchUpdateResult> teusHolder = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        SgStorageBatchUpdateResult updateResult = new SgStorageBatchUpdateResult();
        List<SgStorageRedisSynchModel> sgStorageRedisSynchList = new ArrayList<>();
        List<String> redisBillFtpKeyList = new ArrayList<>();
        List<SgStorageUpdateCommonModel> outStockItemList = new ArrayList<>();
        List<SgTeusStorageUpdateCommonModel> outStockTeusList = new ArrayList<>();
        boolean rollbackFlg = false;

        //初始化变量
        long totalErrorItemQty = 0;
        int preoutUpdateResult = SgConstantsIF.PREOUT_RESULT_SUCCESS;

        if (request == null || CollectionUtils.isEmpty(request.getBillList())) {
            holder.setData(updateResult);
            return holder;
        }

        if (log.isDebugEnabled()) {
            log.debug("Start SgStorageRedisBillBatchUpdateService.updateStorageBatch. ReceiveParams:billList size:{};"
                    , JSONObject.toJSONString(request.getBillList().size()));
        }

        try {

            //循环遍历MQ消息体中单据信息，调用通用单个单据库存更新接口
            for (int billIndex = 0; billIndex < request.getBillList().size(); billIndex++) {

                SgStorageRedisSynchModel synchModel = new SgStorageRedisSynchModel();

                SgStorageSingleUpdateRequest itemRequest = new SgStorageSingleUpdateRequest();

                itemRequest.setLoginUser(request.getLoginUser());
                itemRequest.setMessageKey(request.getMessageKey());
                itemRequest.setControlModel(request.getControlModel());
                itemRequest.setBill(request.getBillList().get(billIndex));

                synchModel.setSingleRequest(itemRequest);

                stoHolder = stoUpdService.updateStorageBill(itemRequest);

                if (stoHolder != null && ResultCode.SUCCESS != stoHolder.getCode()) {

                    holder.setCode(ResultCode.FAIL);
                    holder.setMessage(stoHolder.getMessage().concat(" " + holder.getMessage()));

                    preoutUpdateResult = SgConstantsIF.PREOUT_RESULT_ERROR;
                    rollbackFlg = true;

                    break;

                } else if (stoHolder != null && stoHolder.getData() != null) {

                    totalErrorItemQty = totalErrorItemQty + stoHolder.getData().getErrorBillItemQty();

                    synchModel.setSgStorageUpdateCommonList(stoHolder.getData().getRedisSynchMqItemList());

                    if (!CollectionUtils.isEmpty(stoHolder.getData().getOutStockItemList())) {
                        outStockItemList.addAll(stoHolder.getData().getOutStockItemList());
                    }

                    if (SgConstantsIF.PREOUT_RESULT_SUCCESS != stoHolder.getData().getPreoutUpdateResult()) {
                        preoutUpdateResult = stoHolder.getData().getPreoutUpdateResult();
                    }

                    redisBillFtpKeyList.addAll(stoHolder.getData().getRedisBillFtpKeyList());

                }

                //调用单个单据箱库存更新接口
                if (sgStorageBoxConfig.getBoxEnable() &&
                        (itemRequest.getBill() != null && !CollectionUtils.isEmpty(itemRequest.getBill().getTeusList()))) {

                    teusHolder = teusStoUpdService.updateStorageBill(itemRequest);

                    if (teusHolder != null && ResultCode.SUCCESS != teusHolder.getCode()) {

                        holder.setCode(ResultCode.FAIL);
                        holder.setMessage(teusHolder.getMessage().concat(" " + holder.getMessage()));

                        preoutUpdateResult = SgConstantsIF.PREOUT_RESULT_ERROR;
                        rollbackFlg = true;

                        break;

                    } else if (teusHolder != null && teusHolder.getData() != null) {

                        totalErrorItemQty = totalErrorItemQty + teusHolder.getData().getErrorBillItemQty();

                        synchModel.setSgTeusStorageUpdateCommonList(teusHolder.getData().getRedisSynchMqItemList());

                        if (!CollectionUtils.isEmpty(teusHolder.getData().getOutStockItemList())) {
                            outStockTeusList.addAll(teusHolder.getData().getOutStockItemList());
                        }

                        if (SgConstantsIF.PREOUT_RESULT_SUCCESS != teusHolder.getData().getPreoutUpdateResult()) {
                            preoutUpdateResult = teusHolder.getData().getPreoutUpdateResult();
                        }

                        redisBillFtpKeyList.addAll(teusHolder.getData().getRedisBillFtpKeyList());

                    }

                }

                sgStorageRedisSynchList.add(synchModel);

            }

            if (rollbackFlg) {

                stoUpdService.rollbackStorageBill(redisBillFtpKeyList);

            } else {

                for (SgStorageRedisSynchModel synchModel:sgStorageRedisSynchList) {

                    if (!CollectionUtils.isEmpty(synchModel.getSgStorageUpdateCommonList())) {
                        sgStorageRedisSynchService.synchRedisToStorage(synchModel.getSingleRequest(),
                                synchModel.getSgStorageUpdateCommonList());
                    }

                    if (!CollectionUtils.isEmpty(synchModel.getSgTeusStorageUpdateCommonList())) {
                        sgTeusStorageRedisSynchService.synchRedisToStorage(synchModel.getSingleRequest(),
                                synchModel.getSgTeusStorageUpdateCommonList());
                    }
                }

            }

            updateResult.setErrorBillItemQty(totalErrorItemQty);
            updateResult.setPreoutUpdateResult(preoutUpdateResult);
            updateResult.setRedisBillFtpKeyList(redisBillFtpKeyList);
            updateResult.setOutStockItemList(outStockItemList);
            updateResult.setOutStockTeusList(outStockTeusList);

            holder.setData(updateResult);

        } catch (Exception e) {

            stoUpdService.rollbackStorageBill(redisBillFtpKeyList);

            updateResult.setPreoutUpdateResult(SgConstantsIF.PREOUT_RESULT_ERROR);

            holder.setCode(ResultCode.FAIL);
            holder.setData(updateResult);
            holder.setMessage(e.getMessage().concat(" " + holder.getMessage()));

        }

        if (log.isDebugEnabled()) {
            log.debug("Finish SgStorageRedisBillBatchUpdateService.updateStorageBatch. ReturnResult:errorBillItemQty:{} " +
                            "preoutUpdateResult:{};"
                    , holder.getData() == null ? 0 : holder.getData().getErrorBillItemQty(), holder.getData().getPreoutUpdateResult());
        }

        return holder;

    }

    public ValueHolderV14<SgStorageBatchUpdateResult> rollbackStorageBatch(List<String> redisBillFtpKeyList) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgStorageRedisBillBatchUpdateService.rollbackStorageBatch. ReceiveParams:redisBillFtpKeyList:{};"
                    , JSONObject.toJSONString(redisBillFtpKeyList));
        }

        return stoUpdService.rollbackStorageBill(redisBillFtpKeyList);
    }

}
