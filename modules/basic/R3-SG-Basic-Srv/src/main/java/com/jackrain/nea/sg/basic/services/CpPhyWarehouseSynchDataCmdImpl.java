package com.jackrain.nea.sg.basic.services;

import com.jackrain.nea.sg.basic.api.CpPhyWarehouseSynchDataCmd;
import com.jackrain.nea.sg.basic.model.request.CpPhyWarehouseRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @program: r3-sg
 * @author: Lijp
 * @create: 2019-05-09 14:01
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class CpPhyWarehouseSynchDataCmdImpl implements CpPhyWarehouseSynchDataCmd {

    @Autowired
    private CpPhyWarehouseSynchDataService cpPhyWarehouseSynchDataService;

    @Override
    public ValueHolderV14 SaveWarehouse(CpPhyWarehouseRequest cpPhyWarehouseRequest) {
        return cpPhyWarehouseSynchDataService.warehouseSave(cpPhyWarehouseRequest);
    }

    @Override
    public ValueHolderV14 AuditWarehouse(CpPhyWarehouseRequest cpPhyWarehouseRequest) {
        return cpPhyWarehouseSynchDataService.warehouseAudit(cpPhyWarehouseRequest);
    }

    @Override
    public ValueHolderV14 DeleteWarehosueItem(CpPhyWarehouseRequest cpPhyWarehouseRequest) {
        return cpPhyWarehouseSynchDataService.warehosueDelete(cpPhyWarehouseRequest);
    }
}
