package com.jackrain.nea.sg.basic.services;

import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.api.SgPhyStorageNoTransUpdateCmd;
import com.jackrain.nea.sg.basic.config.SgStorageControlConfig;
import com.jackrain.nea.sg.basic.model.request.SgPhyStorageSingleUpdateRequest;
import com.jackrain.nea.sg.basic.model.result.SgPhyStorageBatchUpdateResult;
import com.jackrain.nea.sg.basic.model.result.SgPhyStorageUpdateResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/3/17 11:02
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg", timeout = 10 * 60 * 1000)
public class SgPhyStorageNoTransUpdateCmdImpl implements SgPhyStorageNoTransUpdateCmd {

    @Autowired
    private SgPhyStorageNoTransUpdateService service;

    @Autowired
    private SgPhyStorageRedisBillUpdateService redisService;

    @Autowired
    private SgStorageControlConfig sgStorageControlConfig;

    @Override
    public ValueHolderV14<SgPhyStorageUpdateResult> updateStorageBillNoTrans(SgPhyStorageSingleUpdateRequest request) {

        ValueHolderV14<SgPhyStorageUpdateResult> result = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        SgPhyStorageUpdateResult updateResult = new SgPhyStorageUpdateResult();

        ValueHolderV14<SgPhyStorageBatchUpdateResult> batchUpdateResult;

        if (sgStorageControlConfig.isRedisFunction()) {
            batchUpdateResult = redisService.updatePhyStorageBill(request);
        } else {
            batchUpdateResult = service.updatePhyStorageBill(request);
        }

        if (batchUpdateResult != null && batchUpdateResult.getData() != null) {
            updateResult.setErrorBillItemQty(batchUpdateResult.getData().getErrorBillItemQty());
        }

        result.setData(updateResult);

        if (batchUpdateResult != null) {
            result.setCode(batchUpdateResult.getCode());
            result.setMessage(batchUpdateResult.getMessage());
        }

        return result;

    }
}
