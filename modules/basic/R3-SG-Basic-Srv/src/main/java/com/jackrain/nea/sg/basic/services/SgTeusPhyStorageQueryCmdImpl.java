package com.jackrain.nea.sg.basic.services;

import com.jackrain.nea.sg.basic.api.SgTeusPhyStorageQueryCmd;
import com.jackrain.nea.sg.basic.model.request.SgTeusPhyStorageQueryRequest;
import com.jackrain.nea.sg.basic.model.table.SgBPhyTeusStorage;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author zhu lin yu
 * @since 2019-11-01
 * create at : 2019-11-01 09:56
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgTeusPhyStorageQueryCmdImpl implements SgTeusPhyStorageQueryCmd {

    @Override
    public ValueHolderV14<List<SgBPhyTeusStorage>> queryTeusPhyStorage(SgTeusPhyStorageQueryRequest request, User loginUser) {
        SgPhyTeusStorageQueryService service = ApplicationContextHandle.getBean(SgPhyTeusStorageQueryService.class);
        return service.queryTeusPhyStorage(request, loginUser);
    }
}
