package com.jackrain.nea.sg.basic.services;

import com.jackrain.nea.sg.basic.api.SgStorageQtyCalculateCmd;
import com.jackrain.nea.sg.basic.model.request.SgQtyStorageByWareCalcRequest;
import com.jackrain.nea.sg.basic.model.result.SgQtyStorageByWareCalcResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgStorageQtyCalculateCmdImpl implements SgStorageQtyCalculateCmd {

    @Autowired
    private SgStorageQtyCalculateService service;

    /**
     * @Description: 根据实体仓计算其下的各逻辑仓的调整数量
     * @Author: chenb
     * @Date: 2019/3/7 16:05
     * @Param: SgQtyStorageByWareCalcRequest 计算逻辑仓在库数量请求信息
     * @Return: ValueHolder
     */
    @Override
    public ValueHolderV14<SgQtyStorageByWareCalcResult> calcSgQtyStorageByWare(SgQtyStorageByWareCalcRequest request) {
        return service.calcSgQtyStorageByWare(request);
    }

}
