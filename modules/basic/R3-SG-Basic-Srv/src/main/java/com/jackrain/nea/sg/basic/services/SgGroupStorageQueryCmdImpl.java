package com.jackrain.nea.sg.basic.services;

import com.jackrain.nea.sg.basic.api.SgGroupStorageQueryCmd;
import com.jackrain.nea.sg.basic.model.request.SgGroupQueryRequest;
import com.jackrain.nea.sg.basic.model.request.SgGroupStorageQueryRequest;
import com.jackrain.nea.sg.basic.model.result.SgGroupStorageQueryResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.query.QuerySession;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author 舒威
 * @since 2019/7/12
 * create at : 2019/7/12 17:27
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgGroupStorageQueryCmdImpl implements SgGroupStorageQueryCmd {

    @Autowired
    private SgGroupStorageQueryService queryService;

    @Override
    public ValueHolderV14<List<SgGroupStorageQueryResult>> queryGroupStorage(List<SgGroupStorageQueryRequest> skuGroupList) {
        return queryService.queryGroupStorage(skuGroupList);
    }

    @Override
    public ValueHolderV14<List<SgGroupQueryRequest>> queryGroupStorageById(QuerySession session) {
        return queryService.queryGroupStorageById(session);
    }
}
