package com.jackrain.nea.sg.basic.task;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.sg.basic.services.CpStoreSyncService;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.task.IR3Task;
import com.jackrain.nea.task.RunTaskResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author zhu lin yu
 * @since 2019/5/31
 * create at : 2019/5/31 20:32
 */
@Slf4j
@Component
public class CpStoreSyncInfoTask implements IR3Task {

    @Autowired
    private CpStoreSyncService cpStoreSyncService;

    @Override
    public RunTaskResult execute(JSONObject params) {
        log.info("Start CpStoreSyncInfoTask.execute,params=" + JSONObject.toJSONString(params) + ";");
        RunTaskResult runTaskResult=new RunTaskResult();
        ValueHolderV14 result = cpStoreSyncService.cpStoreSyncInfo();
        if(result.isOK()){
            runTaskResult.setSuccess(true);
            runTaskResult.setMessage(result.getMessage());
        }else {
            log.error(this.getClass().getName()+",error:"+result.getMessage());
            runTaskResult.setSuccess(false);
            runTaskResult.setMessage(result.getMessage());
        }
        return runTaskResult;
    }
}
