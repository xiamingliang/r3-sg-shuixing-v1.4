package com.jackrain.nea.sg.basic.services;

import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.api.SgStorageBillUpdateCmd;
import com.jackrain.nea.sg.basic.config.SgStorageControlConfig;
import com.jackrain.nea.sg.basic.model.request.SgStorageSingleUpdateRequest;
import com.jackrain.nea.sg.basic.model.result.SgStorageBatchUpdateResult;
import com.jackrain.nea.sg.basic.model.result.SgStorageUpdateResult;
import com.jackrain.nea.sg.basic.utils.SgBasicResultUtil;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/3/17 11:02
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg", timeout = 10 * 60 * 1000)
public class SgStorageBillUpdateCmdImpl implements SgStorageBillUpdateCmd {

    @Autowired
    private SgStorageBillTransUpdateService service;

    @Autowired
    private SgStorageRedisBillUpdateService redisService;

    @Autowired
    private SgStorageControlConfig sgStorageControlConfig;

    @Override
    public ValueHolderV14<SgStorageUpdateResult> updateStorageBillWithTrans(SgStorageSingleUpdateRequest request) {

        ValueHolderV14<SgStorageUpdateResult> result = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        SgStorageUpdateResult updateResult = new SgStorageUpdateResult();
        ValueHolderV14<SgStorageBatchUpdateResult> batchUpdateResult;

        if (sgStorageControlConfig.isRedisFunction()) {
            batchUpdateResult = redisService.updateStorageBill(request);
        } else {
            batchUpdateResult = service.updateStorageBill(request);
        }

        if (batchUpdateResult != null && batchUpdateResult.getData() != null) {
            updateResult = SgBasicResultUtil.convertSgUpdateResult(batchUpdateResult.getData());
        }

        result.setData(updateResult);

        if (batchUpdateResult != null) {
            result.setCode(batchUpdateResult.getCode());
            result.setMessage(batchUpdateResult.getMessage());
        }

        return result;

    }
}
