package com.jackrain.nea.sg.basic.services;


import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.api.SgPhyStorageBatchUpdateCmd;
import com.jackrain.nea.sg.basic.config.SgStorageControlConfig;
import com.jackrain.nea.sg.basic.model.request.SgPhyStorageBatchUpdateRequest;
import com.jackrain.nea.sg.basic.model.result.SgPhyStorageBatchUpdateResult;
import com.jackrain.nea.sg.basic.model.result.SgPhyStorageUpdateResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/3/17 11:02
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgPhyStorageBatchUpdateCmdImpl implements SgPhyStorageBatchUpdateCmd {

    @Autowired
    private SgPhyStorageMQBatchUpdateService service;

    @Autowired
    private SgStorageControlConfig sgStorageControlConfig;

    @Autowired
    private SgPhyStorageRedisBillBatchUpdateService redisBatchService;

    @Override
    public ValueHolderV14<SgPhyStorageUpdateResult> updateStorageBatch(SgPhyStorageBatchUpdateRequest request) {

        ValueHolderV14<SgPhyStorageUpdateResult> holder = new ValueHolderV14<>(ResultCode.SUCCESS,"");
        ValueHolderV14<SgPhyStorageBatchUpdateResult> batchUpdateResult = null;

        SgPhyStorageUpdateResult updateResult = new SgPhyStorageUpdateResult();

        if (sgStorageControlConfig.isRedisFunction()) {

            //调用通用Redis库存批量更新接口
            batchUpdateResult = redisBatchService.updatePhyStorageBatch(request);

        } else {

            batchUpdateResult = service.updatePhyStorageBatch(request);

        }

        if (batchUpdateResult != null && batchUpdateResult.getData() != null) {
            updateResult.setRedisBillFtpKeyList(batchUpdateResult.getData().getRedisBillFtpKeyList());
            updateResult.setErrorBillItemQty(batchUpdateResult.getData().getErrorBillItemQty());
        }

        holder.setData(updateResult);
        holder.setCode(batchUpdateResult.getCode());
        holder.setMessage(batchUpdateResult.getMessage());

        return holder;

    }

}
