package com.jackrain.nea.sg.basic.services;

import com.jackrain.nea.sg.basic.api.SgGroupStorageCleanCmd;
import com.jackrain.nea.sg.basic.model.request.SgGroupStorageCleanRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author: 舒威
 * @since: 2019/7/14
 * create at : 2019/7/14 15:19
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgGroupStorageCleanCmdImpl implements SgGroupStorageCleanCmd {

    @Autowired
    private SgGroupStorageCleanService cleanService;

    @Override
    public ValueHolderV14<Integer> cleanGroupStorage(SgGroupStorageCleanRequest request) {
        return cleanService.cleanGroupStorage(request);
    }
}
