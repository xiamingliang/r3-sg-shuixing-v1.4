package com.jackrain.nea.sg.basic.services;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.api.CpStoreCmd;
import com.jackrain.nea.sg.basic.model.request.CpStoreSaveRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author: zhu lin yu
 * @since: 2019/4/19
 * create at : 2019/4/19 16:49
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class CpStoreCmdImpl implements CpStoreCmd {

    @Autowired
    private CpStoreService cpStoreService;

    /**
     * 店仓新增
     *
     * @param cpStoreSaveRequest
     */
    @Override
    public ValueHolderV14 insertStore(CpStoreSaveRequest cpStoreSaveRequest) {
        ValueHolderV14 cpStoreResultValueHolderV14 = new ValueHolderV14<>();
        JSONObject jsonObject = cpStoreSaveRequest.getData();
        try {
            this.cpStoreService.insertStore(jsonObject);
        } catch (Exception e) {
            cpStoreResultValueHolderV14.setCode(ResultCode.FAIL);
            return cpStoreResultValueHolderV14;
        }
        cpStoreResultValueHolderV14.setCode(ResultCode.SUCCESS);
        return cpStoreResultValueHolderV14;

    }

    /**
     * 店仓跟新
     *
     * @param cpStoreSaveRequest
     */
    @Override
    public ValueHolderV14 updateStore(CpStoreSaveRequest cpStoreSaveRequest) {
        ValueHolderV14 cpStoreResultValueHolderV14 = new ValueHolderV14<>();
        JSONObject jsonObject = cpStoreSaveRequest.getData();
        try {
            this.cpStoreService.updateStore(jsonObject);
        } catch (Exception e) {
            cpStoreResultValueHolderV14.setCode(ResultCode.FAIL);
            return cpStoreResultValueHolderV14;
        }
        cpStoreResultValueHolderV14.setCode(ResultCode.SUCCESS);
        return cpStoreResultValueHolderV14;
    }

    /**
     * 店仓删除
     *
     * @param id
     */
    @Override
    public ValueHolderV14 deleteStore(Long id) {
        ValueHolderV14 cpStoreResultValueHolderV14 = new ValueHolderV14<>();
        try {
            this.cpStoreService.deleteStore(id);
        } catch (Exception e) {
            cpStoreResultValueHolderV14.setCode(ResultCode.FAIL);
            return cpStoreResultValueHolderV14;
        }
        cpStoreResultValueHolderV14.setCode(ResultCode.SUCCESS);
        return cpStoreResultValueHolderV14;
    }
}
