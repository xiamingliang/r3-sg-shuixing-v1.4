package com.jackrain.nea.sg.basic.utils;

import com.jackrain.nea.sg.basic.model.SgStorageUpdateCommonModel;
import com.jackrain.nea.sg.basic.model.result.SgStorageBatchUpdateResult;
import com.jackrain.nea.sg.basic.model.result.SgStorageOutStockResult;
import com.jackrain.nea.sg.basic.model.result.SgStorageUpdateResult;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/6/1 13:30
 */
public class SgBasicResultUtil {

    /**
     * @param updateResult
     * @return
     */
    public static SgStorageUpdateResult convertSgUpdateResult(SgStorageBatchUpdateResult updateResult) {

        SgStorageUpdateResult result = new SgStorageUpdateResult();
        List<SgStorageOutStockResult> outStockItemList = new ArrayList<>();

        if (updateResult != null) {
            result.setErrorBillItemQty(updateResult.getErrorBillItemQty());
            result.setPreoutUpdateResult(updateResult.getPreoutUpdateResult());

            //解析缺货明细列表
            if (!CollectionUtils.isEmpty(updateResult.getOutStockItemList())) {

                for (SgStorageUpdateCommonModel commonModel : updateResult.getOutStockItemList()) {
                    SgStorageOutStockResult outResult = new SgStorageOutStockResult();
                    BeanUtils.copyProperties(commonModel, outResult);
                    outStockItemList.add(outResult);
                }

                result.setOutStockItemList(outStockItemList);
            }

        }

        return result;
    }
}
