package com.jackrain.nea.sg.basic.services;

import com.jackrain.nea.sg.basic.api.SgGroupStorageCalculateCmd;
import com.jackrain.nea.sg.basic.model.request.SgGroupStorageCalculateRequest;
import com.jackrain.nea.sg.basic.model.request.SgGroupStorageSkuRequest;
import com.jackrain.nea.sg.basic.model.result.SgGoupStorageCalculateResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author 舒威
 * @since 2019/7/9
 * create at : 2019/7/9 20:27
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgGroupStorageCalculateCmdImpl implements SgGroupStorageCalculateCmd {

    @Autowired
    private SgGroupStorageCalculateService calculateGroupStorage;

    @Override
    public ValueHolderV14<SgGoupStorageCalculateResult> calculateGroupStorage(SgGroupStorageCalculateRequest request) {
        return calculateGroupStorage.calculateGroupStorage(request);
    }

    @Override
    public BigDecimal calculateLuckyProMinStorage(List<SgGroupStorageSkuRequest> skus, Integer num) {
        return calculateGroupStorage.getLuckyProMinStorage(skus, num);
    }
}
