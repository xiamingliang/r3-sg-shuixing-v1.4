package com.jackrain.nea.sg.basic.services;

import com.github.pagehelper.PageInfo;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.api.SgTeusStorageQueryCmd;
import com.jackrain.nea.sg.basic.model.request.*;
import com.jackrain.nea.sg.basic.model.table.SgBTeusStorage;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zhu lin yu
 * @since 2019-11-01
 * create at : 2019-11-01 09:56
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgTeusStorageQueryCmdImpl implements SgTeusStorageQueryCmd {

    @Autowired
    private SgTeusStorageQueryService service;

    /**
     * 箱库存查询接口
     */
    @Override
    public ValueHolderV14<List<SgBTeusStorage>> queryTeusStorage(SgTeusStorageQueryRequest request, User loginUser) {
        return service.queryTeusStorage(request, loginUser);
    }

    /**
     * 箱库存查询接口(在库数量不等于0)
     */
    @Override
    public ValueHolderV14<List<SgBTeusStorage>> queryTeusStorageExclZero(SgTeusStorageQueryRequest request, User loginUser) {
        return service.queryTeusStorageExclZero(request, loginUser);
    }

    /**
     * 实体仓库存查询接口
     */
    @Override
    public ValueHolderV14<List<SgBTeusStorage>> queryTeusPhyWarehouseStorage(SgTeusPhyStorageQueryRequest request, User loginUser) {

        ValueHolderV14 holder = new ValueHolderV14();
        SgTeusStorageQueryRequest basticRequest = new SgTeusStorageQueryRequest();

        if (request == null) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage("request is null!");
            return holder;
        }

        BeanUtils.copyProperties(request, basticRequest);

        basticRequest.setPhyWarehouseIds(request.getPhyWarehouseIds());
        return service.queryTeusStorage(basticRequest, loginUser);
    }

    /**
     *单虚拟仓库存查询接口
     */
    @Override
    public ValueHolderV14<List<SgBTeusStorage>> queryStoreTeusStorage(SgTeusStoreStorageQueryRequest request, User loginUser) {

        ValueHolderV14 holder = new ValueHolderV14();
        SgTeusStorageQueryRequest basticRequest = new SgTeusStorageQueryRequest();

        if (request == null) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage("request is null!");
            return holder;
        }

        BeanUtils.copyProperties(request, basticRequest);

        List<Long> storeIds = new ArrayList<>();
        storeIds.add(request.getStoreId());
        basticRequest.setStoreIds(storeIds);
        return service.queryTeusStorage(basticRequest, loginUser);
    }

    /**
     * 箱库存分页查询接口
     */
    @Override
    public ValueHolderV14<PageInfo<SgBTeusStorage>> queryTeusStoragePage(SgTeusStoragePageQueryRequest request, User loginUser) {
        return service.queryTeusStoragePage(request, loginUser);
    }

    /**
     * 单实体仓箱库存分页查询接口
     */
    @Override
    public ValueHolderV14<PageInfo<SgBTeusStorage>> queryTeusPhyWarehouseStoragePage(SgTeusPhyStoragePageQueryRequest request, User loginUser) {

        ValueHolderV14 holder = new ValueHolderV14();
        SgTeusStoragePageQueryRequest basticRequest = new SgTeusStoragePageQueryRequest();
        SgTeusStorageQueryRequest queryRequest = new SgTeusStorageQueryRequest();

        if (request == null || request.getSgTeusPhyStorageQueryRequest() == null) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage("request is null!");
            return holder;
        }

        queryRequest.setPhyWarehouseIds(request.getSgTeusPhyStorageQueryRequest().getPhyWarehouseIds());
        queryRequest.setProEcodes(request.getSgTeusPhyStorageQueryRequest().getProEcodes());
        queryRequest.setTeusEcodes(request.getSgTeusPhyStorageQueryRequest().getTeusEcodes());

        basticRequest.setQueryRequest(queryRequest);
        basticRequest.setPageRequest(request.getSgTeusStoragePageRequest());

        return service.queryTeusStoragePage(basticRequest, loginUser);
    }

    /**
     *单虚拟仓箱库存分页查询接口
     */
    @Override
    public ValueHolderV14<PageInfo<SgBTeusStorage>> queryTeusStoreStoragePage(SgTeusStoreStoragePageQueryRequest request, User loginUser) {

        ValueHolderV14 holder = new ValueHolderV14();
        SgTeusStoragePageQueryRequest basticRequest = new SgTeusStoragePageQueryRequest();
        SgTeusStorageQueryRequest queryRequest = new SgTeusStorageQueryRequest();

        if (request == null || request.getSgStoragePageRequest() == null) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage("request is null!");
            return holder;
        }

        queryRequest.setProEcodes(request.getSgTeusStoreStorageQueryRequest().getProEcodes());
        queryRequest.setTeusEcodes(request.getSgTeusStoreStorageQueryRequest().getTeusEcodes());

        List<Long> storeIds = new ArrayList<>();
        storeIds.add(request.getSgTeusStoreStorageQueryRequest().getStoreId());
        queryRequest.setStoreIds(storeIds);

        basticRequest.setQueryRequest(queryRequest);
        basticRequest.setPageRequest(request.getSgStoragePageRequest());

        return service.queryTeusStoragePage(basticRequest, loginUser);
    }

}
