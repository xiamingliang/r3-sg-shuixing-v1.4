package com.jackrain.nea.sg.basic.services;

import com.jackrain.nea.sg.basic.api.SgPhyStorageQueryCmd;
import com.jackrain.nea.sg.basic.model.request.SgPhyStorageQueryRequest;
import com.jackrain.nea.sg.basic.model.table.SgBPhyStorage;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/4/26 9:23
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgPhyStorageQueryCmdImpl implements SgPhyStorageQueryCmd {

    @Autowired
    private SgPhyStorageQueryService service;

    /**
     * @Description: 实体仓库存查询接口
     * @Author: chenb
     * @Date: 2019/3/7 16:05
     * @Param: SgPhyStorageQueryRequest 查询库存相关关键字信息
     * @Return: ValueHolderV14<List < SgBPhyStorage>>
     */
    @Override
    public ValueHolderV14<List<SgBPhyStorage>> queryPhyStorage(SgPhyStorageQueryRequest request, User loginUser) {
        return service.queryPhyStorage(request, loginUser);
    }
}
