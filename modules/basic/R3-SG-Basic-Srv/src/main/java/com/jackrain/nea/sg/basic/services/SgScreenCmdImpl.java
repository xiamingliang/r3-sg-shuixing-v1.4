package com.jackrain.nea.sg.basic.services;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.Feature;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.core.schema.Column;
import com.jackrain.nea.core.schema.Table;
import com.jackrain.nea.core.schema.TableManager;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.basic.api.SgScreenCmd;
import com.jackrain.nea.sys.CommandAdapter;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.mapper.ScreenvalueMapper;
import com.jackrain.nea.web.query.DefaultWebEvent;
import com.jackrain.nea.web.query.QuerySession;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;

/**
 * 获取筛选条件
 *
 * @author nathan
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgScreenCmdImpl extends CommandAdapter implements SgScreenCmd {
    @Override
    public ValueHolder execute(QuerySession querySession) throws NDSException {
        ValueHolder valueHolder = new ValueHolder();
        TableManager tableManager = querySession.getTableManager();
        DefaultWebEvent event = querySession.getEvent();
        JSONObject param = JSON.parseObject(JSON.toJSONStringWithDateFormat(event.getParameterValue("param"), "yyyy-MM-dd HH:mm:ss", SerializerFeature.WriteMapNullValue), Feature.OrderedField);
        ScreenvalueMapper screenvalueMapper = ApplicationContextHandle.getBean(ScreenvalueMapper.class);
        String tableName = param.getString("TABLENAME");
        if (null == tableName) {
            int id = param.getInteger("refcolid");
            Column column = tableManager.getColumn(id);
            Table table = column.getReferenceTable();
            tableName = table.getName();
        }
        JSONObject properties = tableManager.getTable(tableName).getJSONProps();
        JSONArray resultArray = new JSONArray();
        if (null != properties && properties.containsKey("multiFilter")) {
            JSONArray multiFilter = properties.getJSONArray("multiFilter");
            for (int i = 0; i < multiFilter.size(); i++) {
                Column column = tableManager.getTable(tableName).getColumn(multiFilter.getString(i));
                if (null == column) {
                    throw new NDSException(Resources.getMessage("未找到此字段：") + multiFilter.getString(i));
                }
                JSONObject resultJson = new JSONObject(true);
                resultJson.put("NAME", column.getDescription(querySession.getLocale()));
                String akName = column.getName();
                resultJson.put("AKNAME", akName);
                String refTableName = column.getReferenceTable().getName();
                String realTableName = column.getReferenceTable().getName();
                //获取实际表名
                if (column.getReferenceTable().isView()) {
                    realTableName = column.getReferenceTable().getRealTableName();
                }
                JSONObject jsonObject = new JSONObject(true);
                jsonObject.put("COLUMN", column.getReferenceTable().getAlternateKey().getName());
                StringBuilder filter = new StringBuilder();
                if (null != tableManager.getTable(refTableName).getFilter()) {
                    filter.append(tableManager.getTable(refTableName).getFilter());
                    if (null != column.getFilter()) {
                        filter.append(" and " + column.getFilter());
                    }
                } else if (null != column.getFilter()) {
                    filter.append(column.getFilter());
                }
                jsonObject.put("FILTER", filter);
                jsonObject.put("TABLENAME", realTableName);
                jsonObject.put("REFTABLENAME", refTableName);
                List<HashMap> result = screenvalueMapper.queryScreenValue(jsonObject);
                resultJson.put("VALUE", result);
                resultArray.add(resultJson);
            }
        }
        valueHolder.put("code", 0);
        valueHolder.put("data", resultArray);
        return valueHolder;
    }
}
