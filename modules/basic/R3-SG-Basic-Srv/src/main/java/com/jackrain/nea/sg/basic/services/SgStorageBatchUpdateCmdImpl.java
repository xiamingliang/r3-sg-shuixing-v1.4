package com.jackrain.nea.sg.basic.services;

import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.api.SgStorageBatchUpdateCmd;
import com.jackrain.nea.sg.basic.config.SgStorageControlConfig;
import com.jackrain.nea.sg.basic.model.request.SgStorageBatchUpdateRequest;
import com.jackrain.nea.sg.basic.model.result.SgStorageBatchUpdateResult;
import com.jackrain.nea.sg.basic.model.result.SgStorageUpdateResult;
import com.jackrain.nea.sg.basic.utils.SgBasicResultUtil;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/3/17 11:02
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg", timeout = 10 * 60 * 1000)
public class SgStorageBatchUpdateCmdImpl implements SgStorageBatchUpdateCmd {

    @Autowired
    private SgStorageMQBatchUpdateService service;

    @Autowired
    private SgStorageControlConfig sgStorageControlConfig;

    @Autowired
    private SgStorageRedisBillBatchUpdateService redisBatchService;

    @Override
    public ValueHolderV14<SgStorageUpdateResult> updateStorageBatch(SgStorageBatchUpdateRequest request) {

        ValueHolderV14<SgStorageUpdateResult> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        ValueHolderV14<SgStorageBatchUpdateResult> batchUpdateResult = null;

        SgStorageUpdateResult updateResult = new SgStorageUpdateResult();

        if (sgStorageControlConfig.isRedisFunction()) {

            //调用通用Redis库存批量更新接口
            batchUpdateResult = redisBatchService.updateStorageBatch(request);

        } else {

            //调用通用库存批量更新接口
            batchUpdateResult = service.updateStorageBatch(request);
        }

        if (batchUpdateResult != null && batchUpdateResult.getData() != null) {
            updateResult = SgBasicResultUtil.convertSgUpdateResult(batchUpdateResult.getData());
        }

        holder.setData(updateResult);
        holder.setCode(batchUpdateResult.getCode());
        holder.setMessage(batchUpdateResult.getMessage());


        return holder;

    }

}
