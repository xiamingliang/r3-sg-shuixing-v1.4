package com.jackrain.nea.sg.basic.services;

import com.jackrain.nea.sg.basic.api.SgMQResendCmd;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author: 舒威
 * @since: 2019/8/19
 * create at : 2019/8/19 17:11
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgMQResendCmdImpl implements SgMQResendCmd {

    @Autowired
    private SgMQResendService mqResendService;

    @Override
    public ValueHolderV14 sendMQ(String body, String topic, String tag, String msgKey) {
        return  mqResendService.sendMQ(body, topic, tag, msgKey);
    }
}
