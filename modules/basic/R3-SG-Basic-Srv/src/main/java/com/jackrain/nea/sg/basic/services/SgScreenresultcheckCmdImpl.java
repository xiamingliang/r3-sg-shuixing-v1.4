package com.jackrain.nea.sg.basic.services;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.Feature;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.core.schema.Table;
import com.jackrain.nea.core.schema.TableManager;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.ext.permission.DataPermissionsType;
import com.jackrain.nea.ext.permission.Permissions;
import com.jackrain.nea.sg.basic.api.SgScreenresultcheckCmd;
import com.jackrain.nea.sys.CommandAdapter;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.mapper.ScreenvalueMapper;
import com.jackrain.nea.web.query.DefaultWebEvent;
import com.jackrain.nea.web.query.QuerySession;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.*;

/**
 * 查看选中结果
 *
 * @author nathan
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgScreenresultcheckCmdImpl extends CommandAdapter implements SgScreenresultcheckCmd {

    private static final String STORENAME = "CP_C_STORE";

    @Override
    public ValueHolder execute(QuerySession querySession) throws NDSException {
        ValueHolder valueHolder = new ValueHolder();
        TableManager tableManager = querySession.getTableManager();
        DefaultWebEvent event = querySession.getEvent();
        JSONObject param = JSON.parseObject(JSON.toJSONStringWithDateFormat(event.getParameterValue("param"), "yyyy-MM-dd HH:mm:ss", SerializerFeature.WriteMapNullValue), Feature.OrderedField);
        ScreenvalueMapper screenvalueMapper = ApplicationContextHandle.getBean(ScreenvalueMapper.class);
        //获取条件condition
        JSONArray condition = param.getJSONArray("CONDITION");
        //获取表名
        String tableName = param.getString("TABLENAME");
        //获取全局搜索值
        String global = param.getString("GLOBAL");
        //获取IN
        JSONArray inArray = param.getJSONArray("IN");
        //获取NOTIN
        JSONArray notInArray = param.getJSONArray("NOTIN");
        //获取页数
        String pageNum = param.getString("PAGENUM");
        //获取页大小
        String pageSize = param.getString("PAGESIZE");
        //获取exclude
        JSONArray exclude = param.getJSONArray("EXCLUDE");
        Table table = tableManager.getTable(tableName);
        String realTable;
        if (table.isView()) {
            realTable = table.getRealTableName();
        } else {
            realTable = tableName;
        }
        String filter = table.getFilter();
        JSONObject properties = table.getJSONProps();
        if (null == properties) {
            throw new NDSException(Resources.getMessage("此表没有配置！", querySession.getLocale()));
        } else if (null != properties && (null == properties.get("multiIndex") || null == properties.get("multiColumn"))) {
            throw new NDSException(Resources.getMessage("此表没有配置！", querySession.getLocale()));
        }
        JSONArray multiIndex = properties.getJSONArray("multiIndex");
        String column = properties.getString("multiColumn");
        JSONArray orderby = properties.getJSONArray("orderby");
        String[] columns = column.split(",");
        JSONObject columnName = new JSONObject();
        if (null != columns && columns.length > 0) {
            for (int i = 0; i < columns.length; i++) {
                columnName.put(columns[i], table.getColumn(columns[i]).getDescription(querySession.getLocale()));
            }
        }

        String id = null;
        if (STORENAME.equals(realTable.toUpperCase())){
            if (!querySession.getUser().isAdmin()) {
                List<Long> storeIds = Permissions.getReadableDatas(DataPermissionsType.MainStore, querySession.getUser().getId());
                id = org.apache.commons.lang.StringUtils.join(storeIds, ",");
            }
        }

        if (STORENAME.equals(realTable.toUpperCase()) && StringUtils.isEmpty(id) && !querySession.getUser().isAdmin()){
            PageInfo<HashMap> pageInfo = new PageInfo<HashMap>(new ArrayList<>());
            valueHolder.put("data",pageInfo);
            valueHolder.put("ids",new ArrayList<>());
        }else {
            if (!StringUtils.isEmpty(pageNum) && !StringUtils.isEmpty(pageSize)) {
                PageHelper.startPage(Integer.valueOf(pageNum), Integer.valueOf(pageSize));
            }

            List<HashMap> queryScreenCheckResultOne = screenvalueMapper.queryScreenCheckResultOne(column, tableName, condition, exclude, inArray, notInArray, filter, realTable, global, multiIndex,id,orderby);
            PageInfo<HashMap> pageInfoOne = new PageInfo<HashMap>(queryScreenCheckResultOne);
            if (null != queryScreenCheckResultOne && queryScreenCheckResultOne.size() > 0) {
                valueHolder.put("data", pageInfoOne);
                List<Long> ids = screenvalueMapper.queryScreenCheckResultIdOne(tableName, condition, exclude, inArray, notInArray, filter, realTable, global, multiIndex,id,orderby);
                valueHolder.put("ids", ids);
            } else {
                if (!StringUtils.isEmpty(pageNum) && !StringUtils.isEmpty(pageSize)) {
                    PageHelper.startPage(Integer.valueOf(pageNum), Integer.valueOf(pageSize));
                }
                List<HashMap> queryScreenCheckResult = screenvalueMapper.queryScreenCheckResult(column, tableName, condition, exclude, inArray, notInArray, filter, realTable, global, multiIndex,id,orderby);
                List<HashMap> list=new ArrayList<HashMap>();
                //2019-07-19添加
                for(HashMap map:queryScreenCheckResult){
                    HashMap checkMap=new HashMap();
                    Iterator<Map.Entry<String, String>> entries = map.entrySet().iterator();
                    while (entries.hasNext()) {
                        Map.Entry<String, String> entry = entries.next();
                        checkMap.put(entry.getKey().toUpperCase(),entry.getValue());
                    }
                    list.add(checkMap);
                }
                PageInfo<HashMap> pageInfo = new PageInfo<HashMap>(list);
                valueHolder.put("data", pageInfo);
                List<Long> ids = screenvalueMapper.queryScreenCheckResultId(tableName, condition, exclude, inArray, notInArray, filter, realTable, global, multiIndex,id,orderby);
                valueHolder.put("ids", ids);
            }
        }
        valueHolder.put("code", 0);
        valueHolder.put("header", JSON.toJSONString(columnName, new SerializerFeature[]{SerializerFeature.WriteMapNullValue}));
        return valueHolder;
    }
}
