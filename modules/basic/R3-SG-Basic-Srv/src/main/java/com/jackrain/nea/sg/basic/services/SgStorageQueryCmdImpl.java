package com.jackrain.nea.sg.basic.services;

import com.github.pagehelper.PageInfo;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.api.SgStorageQueryCmd;
import com.jackrain.nea.sg.basic.model.request.*;
import com.jackrain.nea.sg.basic.model.result.SgRedisStorageQueryResult;
import com.jackrain.nea.sg.basic.model.result.SgSumStorageQueryResult;
import com.jackrain.nea.sg.basic.model.table.SgBStorage;
import com.jackrain.nea.sg.basic.model.table.SgBStorageInclPhy;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/3/7 16:35
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgStorageQueryCmdImpl implements SgStorageQueryCmd {

    @Autowired
    private SgStorageQueryService service;

    /**
     * @Description: 商品库存查询接口
     * @Author: chenb
     * @Date: 2019/3/7 16:05
     * @Param: sgStorageQueryRequest 查询库存相关关键字信息
     * @Return: ValueHolderV14<List<SgBStorage>>
     */
    @Override
    public ValueHolderV14<List<SgBStorage>> queryStorage(SgStorageQueryRequest request, User loginUser) {
        return service.queryStorage(request, loginUser);
    }

    /**
     * @Description: 商品库存查询接口(在库数量不等于0)
     * @Author: chenb
     * @Date: 2019/8/20 16:05
     * @Param: sgStorageQueryRequest 查询库存相关关键字信息
     * @Return: ValueHolderV14<List<SgBStorage>>
     */
    @Override
    public ValueHolderV14<List<SgBStorage>> queryStorageExclZero(SgStorageQueryRequest request, User loginUser) {
        return service.queryStorageExclZero(request, loginUser);
    }

    /**
     * @Description: 实体仓库存查询接口
     * @Author: chenb
     * @Date: 2019/3/7 16:05
     * @Param: sgStorageQueryRequest 查询库存请求信息
     * @Return: ValueHolder
     */
    @Override
    public ValueHolderV14<List<SgBStorage>> queryPhyWarehouseStorage(SgPhyStorageQueryRequest request, User loginUser) {

        ValueHolderV14 holder = new ValueHolderV14();
        SgStorageQueryRequest basticRequest = new SgStorageQueryRequest();

        if (request == null) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage("request is null!");
            return holder;
        }

        BeanUtils.copyProperties(request, basticRequest);

        basticRequest.setPhyWarehouseIds(request.getPhyWarehouseIds());
        return service.queryStorage(basticRequest, loginUser);
    }

    /**
     * @Description: 单虚拟仓库存查询接口
     * @Author: chenb
     * @Date: 2019/3/7 16:05
     * @Param: sgStorageQueryRequest 查询库存请求信息
     * @Return: ValueHolder
     */
    @Override
    public ValueHolderV14<List<SgBStorage>> queryStoreStorage(SgStoreStorageQueryRequest request, User loginUser) {

        ValueHolderV14 holder = new ValueHolderV14();
        SgStorageQueryRequest basticRequest = new SgStorageQueryRequest();

        if (request == null) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage("request is null!");
            return holder;
        }

        BeanUtils.copyProperties(request, basticRequest);

        List<Long> storeIds = new ArrayList<>();
        storeIds.add(request.getStoreId());
        basticRequest.setStoreIds(storeIds);
        return service.queryStorage(basticRequest, loginUser);
    }

    /**
     * @Description: 商品库存分页查询接口
     * @Author: chenb
     * @Date: 2019/3/7 16:05
     * @Param: sgStorageQueryRequest 查询库存相关关键字信息
     * @Return: ValueHolderV14<List < HashMap>>
     */
    @Override
    public ValueHolderV14<PageInfo<SgBStorage>> queryStoragePage(SgStoragePageQueryRequest request, User loginUser) {
        return service.queryStoragePage(request, loginUser);
    }

    /**
     * @Description: 单实体仓库存分页查询接口
     * @Author: chenb
     * @Date: 2019/3/7 16:05
     * @Param: sgStorageQueryRequest 查询库存请求信息
     * @Return: ValueHolder
     */
    @Override
    public ValueHolderV14<PageInfo<SgBStorage>> queryPhyWarehouseStoragePage(SgPhyStoragePageQueryRequest request, User loginUser) {

        ValueHolderV14 holder = new ValueHolderV14();
        SgStoragePageQueryRequest basticRequest = new SgStoragePageQueryRequest();
        SgStorageQueryRequest queryRequest = new SgStorageQueryRequest();

        if (request == null || request.getQueryRequest() == null) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage("request is null!");
            return holder;
        }

        queryRequest.setPhyWarehouseIds(request.getQueryRequest().getPhyWarehouseIds());
        queryRequest.setProEcodes(request.getQueryRequest().getProEcodes());
        queryRequest.setSkuEcodes(request.getQueryRequest().getSkuEcodes());

        basticRequest.setQueryRequest(queryRequest);
        basticRequest.setPageRequest(request.getPageRequest());

        return service.queryStoragePage(basticRequest, loginUser);
    }

    /**
     * @Description: 单虚拟仓库存分页查询接口
     * @Author: chenb
     * @Date: 2019/3/7 16:05
     * @Param: sgStorageQueryRequest 查询库存请求信息
     * @Return: ValueHolder
     */
    @Override
    public ValueHolderV14<PageInfo<SgBStorage>> queryStoreStoragePage(SgStoreStoragePageQueryRequest request, User loginUser) {

        ValueHolderV14 holder = new ValueHolderV14();
        SgStoragePageQueryRequest basticRequest = new SgStoragePageQueryRequest();
        SgStorageQueryRequest queryRequest = new SgStorageQueryRequest();

        if (request == null || request.getQueryRequest() == null) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage("request is null!");
            return holder;
        }

        queryRequest.setProEcodes(request.getQueryRequest().getProEcodes());
        queryRequest.setSkuEcodes(request.getQueryRequest().getSkuEcodes());

        List<Long> storeIds = new ArrayList<>();
        storeIds.add(request.getQueryRequest().getStoreId());
        queryRequest.setStoreIds(storeIds);

        basticRequest.setQueryRequest(queryRequest);
        basticRequest.setPageRequest(request.getPageRequest());

        return service.queryStoragePage(basticRequest, loginUser);
    }

    /**
     * @Description: 商品库存查询接口（包含实体仓信息）
     * @Author: chenb
     * @Date: 2019/3/7 16:05
     * @Param: sgStorageQueryRequest 查询库存相关关键字信息
     * @Return: ValueHolderV14<List < HashMap>>
     */
    @Override
    public ValueHolderV14<List<SgBStorageInclPhy>> queryStorageInclPhy(SgStorageQueryRequest request, User loginUser) {
        return service.queryStorageInclPhy(request, loginUser);
    }

    /**
     * @Description: 实体仓库存查询接口(通过逻辑仓ID)
     * @Author: chenb
     * @Date: 2019/3/7 16:05
     * @Param: sgStorageQueryRequest 查询库存请求信息
     * @Return: ValueHolder
     */
    @Override
    public ValueHolderV14<List<SgSumStorageQueryResult>> querySumStorageGrpPhy(SgSumStoragePageQueryRequest request, User loginUser){
        return service.querySumStorageGrpPhy(request, loginUser);
    }

    /**
     * @Description: Redis仓库存查询接口
     * @Author: chenb
     * @Date: 2019/9/14 16:05
     * @Param: SgRedisStorageQueryRequest 查询库存请求信息
     * @Return: ValueHolder
     */
    @Override
    public ValueHolderV14<List<SgRedisStorageQueryResult>> queryRedisStorage(SgRedisStorageQueryRequest request, User loginUser) {
        return service.queryRedisStorage(request, loginUser);
    }


}
