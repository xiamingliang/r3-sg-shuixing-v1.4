package com.jackrain.nea.sg.basic.services;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.Feature;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.core.schema.Table;
import com.jackrain.nea.core.schema.TableManager;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.ext.permission.DataPermissionsType;
import com.jackrain.nea.ext.permission.Permissions;
import com.jackrain.nea.sg.basic.api.SgScreenResultCheckIdQueryCmd;
import com.jackrain.nea.sys.CommandAdapter;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.mapper.ScreenvalueMapper;
import com.jackrain.nea.web.query.DefaultWebEvent;
import com.jackrain.nea.web.query.QuerySession;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;

/**
 * 查看选中结果(返回结果ID)
 *
 * @author Yvan (copy to nathon)
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgScreenResultCheckIdQueryCmdImpl extends CommandAdapter implements SgScreenResultCheckIdQueryCmd {

    private static final String STORENAME = "CP_C_STORE";

    @Override
    public ValueHolder execute(QuerySession querySession) throws NDSException {
        ValueHolder valueHolder = new ValueHolder();
        TableManager tableManager = querySession.getTableManager();
        DefaultWebEvent event = querySession.getEvent();
        JSONObject param = JSON.parseObject(JSON.toJSONStringWithDateFormat(event.getParameterValue("param"), "yyyy-MM-dd HH:mm:ss", SerializerFeature.WriteMapNullValue), Feature.OrderedField);
        ScreenvalueMapper screenvalueMapper = ApplicationContextHandle.getBean(ScreenvalueMapper.class);
        //获取条件condition
        JSONArray condition = param.getJSONArray("CONDITION");
        //获取表名
        String tableName = param.getString("TABLENAME");
        //获取全局搜索值
        String global = param.getString("GLOBAL");
        //获取IN
        JSONArray inArray = param.getJSONArray("IN");
        //获取NOTIN
        JSONArray notInArray = param.getJSONArray("NOTIN");
        //获取exclude
        JSONArray exclude = param.getJSONArray("EXCLUDE");

        Table table = tableManager.getTable(tableName);
        String realTable;
        if (table.isView()) {
            realTable = table.getRealTableName();
        } else {
            realTable = tableName;
        }
        String filter = table.getFilter();
        JSONObject properties = table.getJSONProps();
        if (null == properties) {
            throw new NDSException(Resources.getMessage("此表没有配置！", querySession.getLocale()));
        } else if (null != properties && (null == properties.get("multiIndex") || null == properties.get("multiColumn"))) {
            throw new NDSException(Resources.getMessage("此表没有配置！", querySession.getLocale()));
        }

        JSONArray multiIndex = properties.getJSONArray("multiIndex");
        JSONArray orderby = properties.getJSONArray("orderby");
        String column = "ID";

        String id = null;
        if (STORENAME.equals(realTable.toUpperCase())){
            if (!querySession.getUser().isAdmin()) {
                List<Long> storeIds = Permissions.getReadableDatas(DataPermissionsType.MainStore, querySession.getUser().getId());
                id = org.apache.commons.lang.StringUtils.join(storeIds, ",");
            }
        }

        List<HashMap> queryScreenCheckResultOne = screenvalueMapper.queryScreenCheckResultOne(column, tableName, condition, exclude, inArray, notInArray, filter, realTable, global, multiIndex,id,orderby);
        JSONArray idArray = new JSONArray();
        if(queryScreenCheckResultOne != null && queryScreenCheckResultOne.size() > 0){
            for(HashMap hashMap : queryScreenCheckResultOne){
                idArray.add(hashMap.get("ID"));
            }
        }

        valueHolder.put("code", 0);
        valueHolder.put("message", "查询成功！");
        valueHolder.put("data", idArray);
        return valueHolder;
    }
}
