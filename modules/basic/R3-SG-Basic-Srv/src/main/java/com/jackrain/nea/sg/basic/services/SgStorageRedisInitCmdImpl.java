package com.jackrain.nea.sg.basic.services;

import com.jackrain.nea.sg.basic.api.SgStorageRedisInitCmd;
import com.jackrain.nea.sg.basic.model.request.SgStorageRedisInitRequest;
import com.jackrain.nea.sg.basic.model.result.SgStorageRedisInitResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Description: 通用逻辑仓库存初始化接口(Redis)
 * @Author: chenb
 * @Date: 2019/7/10 00:00
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg", timeout = 10 * 60 * 1000)
public class SgStorageRedisInitCmdImpl implements SgStorageRedisInitCmd {

    @Autowired
    private SgStorageRedisInitService service;

    @Override
    public ValueHolderV14<SgStorageRedisInitResult> initRedisStorage(SgStorageRedisInitRequest request) {

        ValueHolderV14<SgStorageRedisInitResult> result = service.initRedisStorage(request);

        return result;

    }
}
