package com.jackrain.nea.sg.basic.services;

import com.jackrain.nea.sg.basic.api.SgPhyStorageRedisMonitorCmd;
import com.jackrain.nea.sg.basic.model.request.SgPhyStorageRedisMonitorRequest;
import com.jackrain.nea.sg.basic.model.result.SgPhyStorageRedisMonitorResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Description: 通用实体仓库存监控接口(Redis)
 * @Author: chenb
 * @Date: 2019/7/10 00:00
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg", timeout = 10 * 60 * 1000)
public class SgPhyStorageRedisMonitorCmdImpl implements SgPhyStorageRedisMonitorCmd {

    @Autowired
    private SgPhyStorageRedisMonitorService service;

    @Override
    public ValueHolderV14<SgPhyStorageRedisMonitorResult> monitorRedisPhyStorage(SgPhyStorageRedisMonitorRequest request) {

        ValueHolderV14<SgPhyStorageRedisMonitorResult> result = service.monitorRedisPhyStorage(request);

        return result;

    }
}
