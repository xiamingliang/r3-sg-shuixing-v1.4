package com.jackrain.nea.sg.basic.services;

import com.jackrain.nea.sg.basic.api.VRptTStorageExportCmd;
import com.jackrain.nea.sg.basic.model.result.SgRcStorageResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * User: zyj
 * Date: 2019/7/29
 * Time: 21:06
 * Description: 导出报表
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class RptTStorageExportCmdImpl implements VRptTStorageExportCmd{

    @Autowired
    private RptTStorageService rptTStorageService;
    @Override
    public ValueHolderV14 exportStorage(SgRcStorageResult sgRcStorageResult, User user) {
        return rptTStorageService.exportStorage(sgRcStorageResult,user);
    }
}
