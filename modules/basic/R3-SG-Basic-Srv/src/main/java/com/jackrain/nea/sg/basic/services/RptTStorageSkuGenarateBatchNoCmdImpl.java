package com.jackrain.nea.sg.basic.services;

import com.jackrain.nea.config.PropertiesConf;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.basic.api.VRptTStorageSkuGenarateBatchNoCmd;
import com.jackrain.nea.sg.basic.model.result.SgRcStorageQueryResult;
import com.jackrain.nea.sg.basic.model.result.SgRcStorageResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: zyj
 * Date: 2019/7/21
 * Time: 17:48
 * Description: 商品期末库存
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class RptTStorageSkuGenarateBatchNoCmdImpl  implements VRptTStorageSkuGenarateBatchNoCmd{
    @Autowired
    private RptTStorageService rptTStorageService;

    @Autowired
    private PropertiesConf propertiesConf;


    @Override
    public ValueHolderV14 queryBatchNoForStorageSku(SgRcStorageResult sgRcStorageResult) throws NDSException {
        String schema = this.propertiesConf.getProperty("sg.control.rc.database_schema");
        if(log.isDebugEnabled()){
            log.debug(this.getClass().getName()+" 数据库schema"+schema);
        }
        return rptTStorageService.generateBatchNoForStorageSku(schema ,sgRcStorageResult);
    }

    @Override
    public ValueHolderV14 queryBatchNoForStorage(SgRcStorageResult sgRcStorageResult) {
        String schema = this.propertiesConf.getProperty("sg.control.rc.database_schema");
        if(log.isDebugEnabled()){
            log.debug(this.getClass().getName()+" 数据库schema"+schema);
        }
        return rptTStorageService.generateBatchNoForStorage(schema ,sgRcStorageResult);
    }

    @Override
    public List<SgRcStorageQueryResult> queryDataForStorage(SgRcStorageResult sgRcStorageResult) {
        return rptTStorageService.queryDataForStorage(sgRcStorageResult);
    }

    @Override
    public Integer countForStorage(SgRcStorageResult sgRcStorageResult) {
        return rptTStorageService.countForStorage(sgRcStorageResult);
    }
}
