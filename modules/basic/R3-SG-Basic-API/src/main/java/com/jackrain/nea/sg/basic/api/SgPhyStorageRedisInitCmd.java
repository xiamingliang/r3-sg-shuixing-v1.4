package com.jackrain.nea.sg.basic.api;

import com.jackrain.nea.sg.basic.model.request.SgPhyStorageRedisInitRequest;
import com.jackrain.nea.sg.basic.model.result.SgPhyStorageRedisInitResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/7/10 00:00
 */
public interface SgPhyStorageRedisInitCmd {

    /**
     * @Description: Redis实体仓库存初始化接口
     * @Author: chenb
     * @Date: 2019/3/7 16:05
     * @Param: SgStorageRedisInitRequest Redis实体仓库存初始化请求信息
     * @Return: ValueHolder
     */
    ValueHolderV14<SgPhyStorageRedisInitResult> initRedisPhyStorage(SgPhyStorageRedisInitRequest request);

}
