package com.jackrain.nea.sg.basic.api;

import com.jackrain.nea.sg.basic.model.request.SgTeusStorageRedisInitRequest;
import com.jackrain.nea.sg.basic.model.result.SgTeusStorageRedisInitResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/7/10 00:00
 */
public interface SgTeusStorageRedisInitCmd {

    /**
     * @Description: Redis箱库存初始化接口
     * @Author: chenb
     * @Date: 2019/3/7 16:05
     * @Param: SgTeusStorageRedisInitRequest Redis箱库存初始化请求信息
     * @Return: ValueHolder
     */
    ValueHolderV14<SgTeusStorageRedisInitResult> initRedisStorage(SgTeusStorageRedisInitRequest request);

}
