package com.jackrain.nea.sg.basic.model.table;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.jackrain.nea.sys.domain.BaseModel;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;

@TableName(value = "sg_b_teus_storage_change_ftp")
@Data
public class SgBTeusStorageChangeFtp extends BaseModel {
    @JSONField(name = "ID")
    @TableField(fill = FieldFill.INSERT)
    @TableId(value = "id", type = IdType.INPUT)
    private Long id;

    @JSONField(name = "BILL_TYPE")
    private Integer billType;

    @JSONField(name = "BILL_DATE")
    private Date billDate;

    @JSONField(name = "CHANGE_DATE")
    private Date changeDate;

    @JSONField(name = "SOURCE_BILL_ID")
    private Long sourceBillId;

    @JSONField(name = "SOURCE_BILL_NO")
    private String sourceBillNo;

    @JSONField(name = "BILL_ID")
    private Long billId;

    @JSONField(name = "BILL_NO")
    private String billNo;

    @JSONField(name = "BILL_ITEM_ID")
    private Long billItemId;

    @JSONField(name = "SERVICE_NODE")
    private Long serviceNode;

    @JSONField(name = "CP_C_STORE_ID")
    private Long cpCStoreId;

    @JSONField(name = "CP_C_STORE_ECODE")
    private String cpCStoreEcode;

    @JSONField(name = "CP_C_STORE_ENAME")
    private String cpCStoreEname;

    @JSONField(name = "PS_C_TEUS_ID")
    private Long psCTeusId;

    @JSONField(name = "PS_C_TEUS_ECODE")
    private String psCTeusEcode;

    @JSONField(name = "PS_C_PRO_ID")
    private Long psCProId;

    @JSONField(name = "PS_C_PRO_ECODE")
    private String psCProEcode;

    @JSONField(name = "PS_C_PRO_ENAME")
    private String psCProEname;

    @JSONField(name = "PS_C_SPEC1_ID")
    private Long psCSpec1Id;

    @JSONField(name = "PS_C_SPEC1_ECODE")
    private String psCSpec1Ecode;

    @JSONField(name = "PS_C_SPEC1_ENAME")
    private String psCSpec1Ename;

    @JSONField(name = "PS_C_MATCHSIZE_ID")
    private Long psCMatchsizeId;

    @JSONField(name = "PS_C_MATCHSIZE_ECODE")
    private String psCMatchsizeEcode;

    @JSONField(name = "PS_C_MATCHSIZE_ENAME")
    private String psCMatchsizeEname;

    @JSONField(name = "QTY_BEGIN")
    private BigDecimal qtyBegin;

    @JSONField(name = "QTY_CHANGE")
    private BigDecimal qtyChange;

    @JSONField(name = "QTY_END")
    private BigDecimal qtyEnd;

    @JSONField(name = "REMARK")
    private String remark;

    @JSONField(name = "VERSION")
    private Long version;

    @JSONField(name = "OWNERENAME")
    private String ownerename;

    @JSONField(name = "MODIFIERENAME")
    private String modifierename;

    @JSONField(name = "REDIS_BILL_FTP_KEY")
    private String redisBillFtpKey;
}