package com.jackrain.nea.sg.basic.model.request;



import com.jackrain.nea.sg.basic.model.table.CpCPhyWarehouse;
import com.jackrain.nea.sg.basic.model.table.CpCPhyWarehouseItem;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @program: r3-sg
 * @author: Lijp
 * @create: 2019-05-09 09:06
 */
@Data
public class CpPhyWarehouseRequest implements Serializable {

    private long objId;

    private CpCPhyWarehouse cpCPhyWarehouse;

    /**
     * 实体仓明细新增
     */
    private List<CpCPhyWarehouseItem> cpCPhyWarehouseItemList;

    /**
     * 实体仓明细修改
     */
    private List<CpCPhyWarehouseItem> updateCpCPhyWarehouseItemList;
}
