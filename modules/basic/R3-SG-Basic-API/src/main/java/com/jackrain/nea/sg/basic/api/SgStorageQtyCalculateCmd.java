package com.jackrain.nea.sg.basic.api;

import com.jackrain.nea.sg.basic.model.request.SgQtyStorageByWareCalcRequest;
import com.jackrain.nea.sg.basic.model.result.SgQtyStorageByWareCalcResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;

public interface SgStorageQtyCalculateCmd {

    /**
     * @Description: 计算逻辑仓在库数量接口(通过实体仓ID)
     * @Author: chenb
     * @Date: 2019/3/7 16:05
     * @Param: SgQtyStorageByWareCalcRequest 计算逻辑仓在库数量请求信息
     * @Return: ValueHolder
     */
    ValueHolderV14<SgQtyStorageByWareCalcResult> calcSgQtyStorageByWare(SgQtyStorageByWareCalcRequest request);

}
