package com.jackrain.nea.sg.basic.model.request;

import com.jackrain.nea.web.face.User;
import lombok.Data;

import java.io.Serializable;

@Data
public class SgR3BaseRequest implements Serializable {

    /**
     * 传入参数
     * objId 用于判断新增/修改
     * objId < 0 新增
     * objId = null 接口修改
     * objId > 0 页面修改
     */
    private Long objId;


    /**
     * 登录用户
     */
    private User loginUser;
}
