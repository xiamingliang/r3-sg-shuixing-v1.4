package com.jackrain.nea.sg.basic.model.result;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/3/13 15:00
 */
@Data
public class SgRedisPhyStorageQueryResult implements Serializable {

    /**
     * 实体仓id
     */
    @JSONField(name = "CP_C_PHY_WAREHOUSE_ID")
    private Long cpCPhyWarehouseId;
    /**
     * 条码id
     */
    @JSONField(name = "PS_C_SKU_ID")
    private Long psCSkuId;
    /**
     * 在库数量
     */
    @JSONField(name = "QTY_STORAGE")
    private BigDecimal qtyStorage;


}
