package com.jackrain.nea.sg.basic.model.request;

import com.jackrain.nea.web.face.User;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author 舒威
 * @since 2019/7/8
 * create at : 2019/7/8 17:15
 */
@Data
public class SgGroupStoragePoolRequest implements Serializable {

    private List<SgGroupStoragePoolBillRequest> poolBillRequests;

    private User loginUser;

}
