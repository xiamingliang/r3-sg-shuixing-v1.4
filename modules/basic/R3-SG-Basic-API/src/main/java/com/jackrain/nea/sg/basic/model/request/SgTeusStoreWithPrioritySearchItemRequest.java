package com.jackrain.nea.sg.basic.model.request;

import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Data
public class SgTeusStoreWithPrioritySearchItemRequest implements Serializable {

    /**
     * 虚拟仓优先级列表
     */
    private List<StStockPriorityRequest> priorityList;
    /**
     * 箱id
     */
    private Long psCTeusId;
    /**
     * 变动数量
     */
    private BigDecimal qtyChange;
    /**
     * 明细操作类型
     */
    private Integer operateType;
    /**
     * 来源单据明细编号
     */
    private Long sourceItemId;

    public SgTeusStoreWithPrioritySearchItemRequest() {
        this.operateType = SgConstantsIF.OPERATE_TYPE_OCCUPY;
    }

}
