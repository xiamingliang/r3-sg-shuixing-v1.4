package com.jackrain.nea.sg.basic.model.table;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.*;
import com.jackrain.nea.sys.domain.BaseModel;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;


@TableName(value = "cp_c_phy_warehouse")
@Data
public class CpCPhyWarehouse extends BaseModel {
    @JSONField(name = "ID")
    @TableField(fill = FieldFill.INSERT)
    @TableId(value = "id", type = IdType.INPUT)
    private Long id;

    @JSONField(name = "ECODE")
    private String ecode;

    @JSONField(name = "ENAME")
    private String ename;

    @TableField(fill = FieldFill.UPDATE)
    @JSONField(name = "WMS_WAREHOUSE_CODE")
    private String wmsWarehouseCode;

    @TableField(fill = FieldFill.UPDATE)
    @JSONField(name = "OWNER_CODE")
    private String ownerCode;

    @TableField(fill = FieldFill.UPDATE)
    @JSONField(name = "WMS_ACCOUNT")
    private String wmsAccount;

    @JSONField(name = "IS_SAP")
    private Integer isSap;

    @TableField(fill = FieldFill.UPDATE)
    @JSONField(name = "WMS_URL")
    private String wmsUrl;

    @TableField(fill = FieldFill.UPDATE)
    @JSONField(name = "REMARK")
    private String remark;

    @TableField(fill = FieldFill.UPDATE)
    @JSONField(name = "CONTACT_NAME")
    private String contactName;

    @TableField(fill = FieldFill.UPDATE)
    @JSONField(name = "MOBILEPHONE_NUM")
    private String mobilephoneNum;

    @TableField(fill = FieldFill.UPDATE)
    @JSONField(name = "PHONE_NUM")
    private String phoneNum;

    @TableField(fill = FieldFill.UPDATE)
    @JSONField(name = "SELLER_PROVINCE_ID")
    private Long sellerProvinceId;

    @TableField(fill = FieldFill.UPDATE)
    @JSONField(name = "SELLER_CITY_ID")
    private Long sellerCityId;

    @TableField(fill = FieldFill.UPDATE)
    @JSONField(name = "SELLER_AREA_ID")
    private Long sellerAreaId;

    @TableField(fill = FieldFill.UPDATE)
    @JSONField(name = "SELLER_ZIP")
    private String sellerZip;

    @TableField(fill = FieldFill.UPDATE)
    @JSONField(name = "SEND_ADDRESS")
    private String sendAddress;

    @JSONField(name = "CP_C_CUSTOMER_ID")
    private Long cpCCustomerId;

    @JSONField(name = "CP_C_CUSTOMER_ECODE")
    private String cpCCustomerEcode;

    @JSONField(name = "CP_C_CUSTOMER_ENAME")
    private String cpCCustomerEname;

    @JSONField(name = "CP_C_STORE_ID")
    private Long cpCStoreId;

    @JSONField(name = "CP_C_STORE_ECODE")
    private String cpCStoreEcode;

    @JSONField(name = "CP_C_STORE_ENAME")
    private String cpCStoreEname;

    @TableField(fill = FieldFill.UPDATE)
    @JSONField(name = "QTY_PKG_MAX")
    private BigDecimal qtyPkgMax;

    @JSONField(name = "WMS_CONTROL_WAREHOUSE")
    private Integer wmsControlWarehouse;

    @JSONField(name = "OWNERENAME")
    private String ownerename;

    @JSONField(name = "MODIFIERENAME")
    private String modifierename;

    @TableField(fill = FieldFill.UPDATE)
    @JSONField(name = "CUT_TIME")
    private Date cutTime;

    @TableField(fill = FieldFill.UPDATE)
    @JSONField(name = "JD_CUT_TIME")
    private Date jdCutTime;

    @TableField(fill = FieldFill.UPDATE)
    @JSONField(name = "JIT_WAREHOUSE_ECODE")
    private String jitWarehouseEcode;

    @TableField(fill = FieldFill.UPDATE)
    @JSONField(name = "RETURN_PHY_WAREHOUSE_ID")
    private Long returnPhyWarehouseId;

    @TableField(fill = FieldFill.UPDATE)
    @JSONField(name = "RETURN_PHY_WAREHOUSE_ECODE")
    private String returnPhyWarehouseEcode;

    @TableField(fill = FieldFill.UPDATE)
    @JSONField(name = "RETURN_PHY_WAREHOUSE_ENAME")
    private String returnPhyWarehouseEname;

    @TableField(fill = FieldFill.UPDATE)
    @JSONField(name = "JIT_WAREHOUSE_ID")
    private Long jitWarehouseId;

    @TableField(fill = FieldFill.UPDATE)
    @JSONField(name = "CP_C_STORE_ID_LOST")
    private Long CpCStoreIdLost;

    @TableField(fill = FieldFill.UPDATE)
    @JSONField(name = "IS_PASS_PRO")
    private Integer isPassPro;

    // 水星-zhanghang-2019-12-13 start

    @TableField(fill = FieldFill.UPDATE)
    @JSONField(name = "IS_CHANGE_STORAGE")
    private Integer isChangeStorage;
    // 水星-zhanghang-2019-12-13 end

    // 水星-zhanghang-2019-12-25 start

    @TableField(exist = false)
    @JSONField(name = "STORENATURE")
    private String storenature;
    // 水星-zhanghang-2019-12-25 end

    /**
     * 千百度注释
     */
    /*@TableField(fill = FieldFill.UPDATE)
    @JSONField(name = "CP_C_STORE_CC_ID")
    private Long cpCStoreCcId;*/
}