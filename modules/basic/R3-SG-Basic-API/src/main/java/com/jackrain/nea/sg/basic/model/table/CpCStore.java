package com.jackrain.nea.sg.basic.model.table;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.*;
import com.jackrain.nea.sys.domain.BaseModel;
import lombok.Data;
import org.springframework.stereotype.Component;

@TableName(value = "cp_c_store")
@Data
@Component("SG_CpCStore")
public class CpCStore extends BaseModel {
    @JSONField(name = "ID")
    @TableField(fill = FieldFill.INSERT)
    @TableId(value = "id", type = IdType.INPUT)
    private Long id;

    @JSONField(name = "CP_C_STORE_ECODE")
    private String cpCStoreEcode;

    @JSONField(name = "CP_C_STORE_ENAME")
    private String cpCStoreEname;

    @JSONField(name = "CP_C_PHY_WAREHOUSE_ID")
    private Long cpCPhyWarehouseId;

    @JSONField(name = "CP_C_PHY_WAREHOUSE_ECODE")
    private String cpCPhyWarehouseEcode;

    @JSONField(name = "CP_C_PHY_WAREHOUSE_ENAME")
    private String cpCPhyWarehouseEname;

    @JSONField(name = "VERSION")
    private Long version;

    @JSONField(name = "OWNERENAME")
    private String ownerename;

    @JSONField(name = "MODIFIERENAME")
    private String modifierename;

    @JSONField(name = "IS_MAIN_WAREHOUSE")
    private Integer isMainWarehouse;

    @JSONField(name = "IS_WMS")
    private Integer isWms;

    @JSONField(name = "WMS_ACCOUNT")
    private String wmsAccount;

    @JSONField(name = "ISNEGATIVE")
    private Long isnegative;

    @JSONField(name = "IS_AUTO_IN")
    private String isAutoIn;

    @JSONField(name = "IS_AUTO_OUT")
    private String isAutoOut;

    @JSONField(name = "CONTACTER")
    private String contacter;

    @JSONField(name = "MOBIL")
    private String mobil;

    @JSONField(name = "PHONE")
    private String phone;

    @JSONField(name = "FAX")
    private String fax;

    @JSONField(name = "EMAIL")
    private String email;

    @JSONField(name = "IS_SHARE")
    private String isShare;

    @JSONField(name = "ECSTORE")
    private Long ecstore;

    @JSONField(name = "ISOUT")
    private Long isout;

    @JSONField(name = "ISRECEPT")
    private Long isrecept;

    @JSONField(name = "ISDISCENTER")
    private Long isdiscenter;

    @JSONField(name = "ACCESS_NO_BILL")
    private String accessNoBill;

    @JSONField(name = "RECYCLE")
    private Long recycle;

    @JSONField(name = "RETCYCLE")
    private Long retcycle;

    @JSONField(name = "DATEINV")
    private Long dateinv;

    @JSONField(name = "CLOSEDATE")
    private Long closedate;

    @JSONField(name = "PLAN_CHECK_DATE")
    private Long planCheckDate;

    @JSONField(name = "STORETYPE")
    private String storetype;

    @JSONField(name = "STORE_CONFIRM_TIME")
    private Long storeConfirmTime;
}