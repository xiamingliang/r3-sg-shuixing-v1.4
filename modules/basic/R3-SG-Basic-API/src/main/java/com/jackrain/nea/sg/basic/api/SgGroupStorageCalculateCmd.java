package com.jackrain.nea.sg.basic.api;

import com.jackrain.nea.sg.basic.model.request.SgGroupStorageCalculateRequest;
import com.jackrain.nea.sg.basic.model.request.SgGroupStorageSkuRequest;
import com.jackrain.nea.sg.basic.model.result.SgGoupStorageCalculateResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author 舒威
 * @since 2019/7/8
 * create at : 2019/7/8 18:28
 */
public interface SgGroupStorageCalculateCmd {

    ValueHolderV14<SgGoupStorageCalculateResult> calculateGroupStorage(SgGroupStorageCalculateRequest request);

    BigDecimal calculateLuckyProMinStorage(List<SgGroupStorageSkuRequest> skus, Integer num);
}
