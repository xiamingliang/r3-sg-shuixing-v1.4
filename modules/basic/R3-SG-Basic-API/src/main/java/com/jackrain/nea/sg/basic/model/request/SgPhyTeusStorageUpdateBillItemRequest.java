package com.jackrain.nea.sg.basic.model.request;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/10/23 13:31
 */
@Data
public class SgPhyTeusStorageUpdateBillItemRequest extends SgPhyTeusStorageUpdateBillItemExt
        implements Serializable, Comparable<SgPhyTeusStorageUpdateBillItemRequest> {

    /**
     * 明细控制类
     */
    private SgStorageUpdateControlRequest controlmodel;

    @Override
    public int compareTo(SgPhyTeusStorageUpdateBillItemRequest o) {
        return o.toString().compareTo(this.toString());
    }

    @Override
    public String toString(){
        StringBuffer sb = new StringBuffer();
        sb.append(super.getCpCPhyWarehouseId())
                .append("_")
                .append(super.getPsCTeusId());
        return sb.toString();
    }
}
