package com.jackrain.nea.sg.basic.model.request;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/3/19 19:36
 */
@Data
public class SgStoreStoragePageQueryRequest implements Serializable {

    private SgStoreStorageQueryRequest queryRequest;

    private SgStoragePageRequest pageRequest;

}