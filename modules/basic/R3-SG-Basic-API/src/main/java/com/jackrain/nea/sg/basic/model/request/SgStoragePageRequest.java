package com.jackrain.nea.sg.basic.model.request;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/3/19 18:54
 */
@Data
public class SgStoragePageRequest implements Serializable {

    private int pageNum;

    private int pageSize;

    private String sortKeys;

    public SgStoragePageRequest(){
        this.pageNum = 1;
        this.pageSize = 10;
    }
}