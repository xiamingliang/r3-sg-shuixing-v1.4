package com.jackrain.nea.sg.basic.model.result;

import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class SgTeusStoreWithPrioritySearchResult implements Serializable {

    /**
     * 占用更新结果
     */
    private int preoutUpdateResult;
    /**
     * 是否是新增逻辑发货单
     */
    private boolean isAddBillFlg;

    /**
     * 寻仓结果明细信息
     */
    private List<SgTeusStoreWithPrioritySearchItemResult> itemResultList;

    /**
     *缺货明细列表
     */
    private List<SgTeusStoreWithPrioritySearchItemResult> outStockItemList;

    public SgTeusStoreWithPrioritySearchResult() {
        this.isAddBillFlg = false;
        this.preoutUpdateResult = SgConstantsIF.PREOUT_RESULT_SUCCESS;
    }
}
