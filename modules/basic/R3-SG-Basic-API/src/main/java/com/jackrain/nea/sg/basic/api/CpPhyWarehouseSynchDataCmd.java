package com.jackrain.nea.sg.basic.api;

import com.jackrain.nea.sg.basic.model.request.CpPhyWarehouseRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 * @program: r3-sg
 * @author: Lijp
 * @create: 2019-05-08 20:39
 */
public interface CpPhyWarehouseSynchDataCmd {

    ValueHolderV14 SaveWarehouse(CpPhyWarehouseRequest cpPhyWarehouseRequest);

    ValueHolderV14 AuditWarehouse(CpPhyWarehouseRequest cpPhyWarehouseRequest);

    ValueHolderV14 DeleteWarehosueItem(CpPhyWarehouseRequest cpPhyWarehouseRequest);

}