package com.jackrain.nea.sg.basic.model.request;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author: 汪聿森
 * @Date: Created in 2019-10-11 13:53
 * @Description : 组合商品库存
 */
@Data
public class SgGroupQueryRequest implements Serializable {
    //商品编码
    @JSONField(name = "PS_C_PRO_ECODE")
    private String psCProEcode;
    //条码编码
    @JSONField(name = "PS_C_SKU_ECODE")
    private String psCSkuEcode;
    //可用库存
    @JSONField(name = "QTY_AVAILABLE")
    private BigDecimal qtyAvailable;
    //在库库存
    @JSONField(name = "QTY_STORAGE")
    private BigDecimal qtyStorage;
    //国标码
    @JSONField(name = "GBCODE")
    private String gbcode;
    //商品名称
    @JSONField(name = "PS_C_PRO_ENAME")
    private String psCProEname;
    //颜色
    @JSONField(name = "PS_C_SPEC1_ENAME")
    private String psCSpec1Ename;
    //尺码
    @JSONField(name = "PS_C_SPEC2_ENAME")
    private String psCSpec2Ename;
    //逻辑仓
    @JSONField(name = "CP_C_STORE_ENAME")
    private String cpCStoreEname;
}
