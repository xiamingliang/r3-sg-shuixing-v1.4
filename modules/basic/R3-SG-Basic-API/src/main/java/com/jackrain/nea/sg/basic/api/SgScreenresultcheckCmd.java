package com.jackrain.nea.sg.basic.api;

import com.jackrain.nea.sys.Command;

/**
 * 查看选中结果
 * @author nathan
 */
public interface SgScreenresultcheckCmd extends Command {
}
