package com.jackrain.nea.sg.basic.api;

import com.jackrain.nea.sg.basic.model.result.SgRcStorageQueryResult;
import com.jackrain.nea.sg.basic.model.result.SgRcStorageResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: zyj
 * Date: 2019/7/21
 * Time: 16:40
 * Description: 商品期末库存
 */
public interface VRptTStorageSkuGenarateBatchNoCmd  {

    ValueHolderV14 queryBatchNoForStorageSku(SgRcStorageResult sgRcStorageResult);

    ValueHolderV14 queryBatchNoForStorage(SgRcStorageResult sgRcStorageResult);

    List<SgRcStorageQueryResult> queryDataForStorage(SgRcStorageResult sgRcStorageResult);

    Integer countForStorage(SgRcStorageResult sgRcStorageResult);

}
