package com.jackrain.nea.sg.basic.model.request;

import com.jackrain.nea.web.face.User;
import lombok.Data;

import java.io.Serializable;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/3/17 16:01
 */
@Data
public class SgStorageUpdateBaseRequest implements Serializable {
    //用户信息
    private User loginUser;
    //MQ幂等性防重锁
    private String MessageKey;
    //库存更新控制
    private SgStorageUpdateControlRequest controlModel;
}
