package com.jackrain.nea.sg.basic.model.request;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/4/25 12:32
 */
@Data
public class SgSumStorageQueryRequest implements Serializable {

    private List<Long> storeIds;

    private List<String> proEcodes;

    private List<String> skuEcodes;
}
