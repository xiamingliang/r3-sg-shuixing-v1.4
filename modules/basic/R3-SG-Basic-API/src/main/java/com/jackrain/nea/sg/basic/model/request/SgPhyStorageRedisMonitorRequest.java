package com.jackrain.nea.sg.basic.model.request;

import com.jackrain.nea.web.face.User;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/3/17 13:23
 */
@Data
public class SgPhyStorageRedisMonitorRequest implements Serializable {

    /**
     * 实体仓ID列表
     */
    private List<Long> phyWarehouseIds;

    private List<Long> skuIds;

    //用户信息
    private User loginUser;

}
