package com.jackrain.nea.sg.basic.model.request;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/3/19 18:54
 */
@Data
public class SgStoragePageQueryRequest implements Serializable {

    private SgStorageQueryRequest queryRequest;

    private SgStoragePageRequest pageRequest;

}
