package com.jackrain.nea.sg.basic.model.result;

import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/3/14 19:00
 */
@Data
public class SgStorageUpdateResult implements Serializable {

    /*
     * 错误明细件数
     */
    private long errorBillItemQty = 0;
    /*
     * 占用更新结果
     */
    private int preoutUpdateResult;
    /*
     * Redis更新流水键
     */
    private List<String> redisBillFtpKeyList;
    /*
     *缺货明细列表
     */
    private List<SgStorageOutStockResult> outStockItemList;

    public SgStorageUpdateResult() {
        this.errorBillItemQty = 0;
        this.preoutUpdateResult = SgConstantsIF.PREOUT_RESULT_SUCCESS;
    }
}
