package com.jackrain.nea.sg.basic.model.request;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;

import java.io.Serializable;

/**
 * @author: zhu lin yu
 * @since: 2019/4/25
 * create at : 2019/4/25 12:25
 */
@Data
public class CpStoreSaveRequest implements Serializable {
    private JSONObject data;
}
