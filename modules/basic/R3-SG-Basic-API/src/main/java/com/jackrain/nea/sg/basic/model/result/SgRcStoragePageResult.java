package com.jackrain.nea.sg.basic.model.result;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: zyj
 * Date: 2019/7/30
 * Time: 20:05
 * Description: 分页库存报表返回
 */
@Data
public class SgRcStoragePageResult implements Serializable{
    private List<SgRcStorageQueryResult> sgRcStorageQueryResultList;

    private Integer totalSize;
}
