package com.jackrain.nea.sg.basic.model.request;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class SgQtyTeusStorageByWareCalcItemRequest implements Serializable {

    /**
     * 箱id
     */
    private Long psCTeusId;
    /**
     * 箱号
     */
    private String psCTeusEcode;
    /**
     * 变动数量
     */
    private BigDecimal qtyChange;

}
