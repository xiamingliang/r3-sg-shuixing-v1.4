package com.jackrain.nea.sg.basic.model.request;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/3/17 13:23
 */
@Data
public class SgStorageSingleUpdateRequest extends SgStorageUpdateBaseRequest implements Serializable {

    //单据信息
    private SgStorageUpdateBillRequest bill;

}
