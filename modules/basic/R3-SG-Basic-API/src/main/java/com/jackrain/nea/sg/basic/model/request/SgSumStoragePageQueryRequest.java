package com.jackrain.nea.sg.basic.model.request;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/4/25 12:59
 */
@Data
public class SgSumStoragePageQueryRequest implements Serializable {

    private SgSumStorageQueryRequest queryRequest;

    private SgStoragePageRequest pageRequest;

}