package com.jackrain.nea.sg.basic.model.result;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author: 舒威
 * @since: 2019/7/24
 * create at : 2019/7/24 21:42
 */
@Data
public class SgGoupStorageSkuResult implements Serializable{

    private Long id;

    private String ecode;

    private BigDecimal num;

    private BigDecimal qtyAvaliable;
}
