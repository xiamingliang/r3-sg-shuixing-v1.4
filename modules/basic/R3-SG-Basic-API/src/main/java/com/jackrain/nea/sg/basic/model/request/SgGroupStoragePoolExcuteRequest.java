package com.jackrain.nea.sg.basic.model.request;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author 舒威
 * @since 2019/7/8
 * create at : 2019/7/8 17:15
 */
@Data
public class SgGroupStoragePoolExcuteRequest implements Serializable {

    private String billNo;

    private Long cpCShopId;

    private Long cpCStoreId;

    private Long psCSkuId;

    private BigDecimal qtyChange;

}
