package com.jackrain.nea.sg.basic.model.result;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/3/14 19:00
 */
@Data
public class SgPhyStorageRedisMonitorResult implements Serializable {

    /*
     * 错误初始化件数
     */
    private long errorItemQty;

    public SgPhyStorageRedisMonitorResult() {
        this.errorItemQty = 0;

    }
}
