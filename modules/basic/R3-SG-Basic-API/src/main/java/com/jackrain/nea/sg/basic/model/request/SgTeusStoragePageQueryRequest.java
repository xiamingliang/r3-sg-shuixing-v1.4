package com.jackrain.nea.sg.basic.model.request;

import lombok.Data;

import java.io.Serializable;

/**
 * @author zhu lin yu
 * @since 2019-11-01
 * create at : 2019-11-01 10:24
 */
@Data
public class SgTeusStoragePageQueryRequest implements Serializable {

    private SgTeusStorageQueryRequest queryRequest;

    private SgTeusStoragePageRequest pageRequest;
}
