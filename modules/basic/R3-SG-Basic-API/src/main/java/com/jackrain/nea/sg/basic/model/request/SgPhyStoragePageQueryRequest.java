package com.jackrain.nea.sg.basic.model.request;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/3/19 18:54
 */
@Data
public class SgPhyStoragePageQueryRequest implements Serializable {

    private SgPhyStorageQueryRequest queryRequest;

    private SgStoragePageRequest pageRequest;

}
