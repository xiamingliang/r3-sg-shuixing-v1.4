package com.jackrain.nea.sg.basic.model.table;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.jackrain.nea.es.annotations.Document;
import com.jackrain.nea.es.annotations.Field;
import com.jackrain.nea.es.constans.FieldType;
import com.jackrain.nea.sys.domain.BaseModel;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@TableName(value = "v_rpt_t_storage_sku")
@Data
@Document(index = "v_rpt_t_storage_sku",type = "v_rpt_t_storage_sku")
public class RptTStorageSku extends BaseModel {
    @JSONField(name = "ID")
    @Field(type = FieldType.Long)
    private Long id;

    @JSONField(name = "VERSION")
    @Field(type = FieldType.Keyword)
    private String version;

    @JSONField(name = "RPT_DATE")
    @Field(type = FieldType.Date)
    private Date rptDate;

    @JSONField(name = "CP_C_STORE_ID")
    @Field(type = FieldType.Long)
    private Long cpCStoreId;

    @JSONField(name = "CP_C_STORE_ECODE")
    @Field(type = FieldType.Keyword)
    private String cpCStoreEcode;

    @JSONField(name = "CP_C_STORE_ENAME")
    @Field(type = FieldType.Keyword)
    private String cpCStoreEname;

    @JSONField(name = "PS_C_BRAND_ID")
    @Field(type = FieldType.Long)
    private Long psCBrandId;

    @JSONField(name = "PROYEAR")
    @Field(type = FieldType.Keyword)
    private String proyear;

    @JSONField(name = "PROSEA")
    @Field(type = FieldType.Keyword)
    private String prosea;

    @JSONField(name = "MATERIELTYPE")
    @Field(type = FieldType.Keyword)
    private String materieltype;

    @JSONField(name = "PS_C_PRO_ECODE")
    @Field(type = FieldType.Keyword)
    private String psCProEcode;

    @JSONField(name = "PS_C_PRO_ENAME")
    @Field(type = FieldType.Keyword)
    private String psCProEname;

    @JSONField(name = "PS_C_SKU_ECODE")
    @Field(type = FieldType.Keyword)
    private String psCSkuEcode;

    @JSONField(name = "LARGESERIES")
    @Field(type = FieldType.Keyword)
    private String largeseries;

    @JSONField(name = "JACKET_BOTTOM")
    @Field(type = FieldType.Keyword)
    private String jacketBottom;

    @JSONField(name = "SMALLSERIES")
    @Field(type = FieldType.Keyword)
    private String smallseries;

    @JSONField(name = "SMALLCATE")
    @Field(type = FieldType.Keyword)
    private String smallcate;

    @JSONField(name = "SEX")
    @Field(type = FieldType.Keyword)
    private String sex;

    @JSONField(name = "CLRS")
    @Field(type = FieldType.Keyword)
    private String clrs;

    @JSONField(name = "SIZES")
    @Field(type = FieldType.Keyword)
    private String sizes;

    @JSONField(name = "VIPCOM_CN")
    @Field(type = FieldType.Keyword)
    private String vipcomCn;

    @JSONField(name = "PRICECOSTLIST")
    @Field(type = FieldType.Double)
    private BigDecimal pricecostlist;

    @JSONField(name = "GBCODE")
    @Field(type = FieldType.Keyword)
    private String gbcode;

    @JSONField(name = "PROYEAR_ID")
    @Field(type = FieldType.Long)
    private Long proyearId;

    @JSONField(name = "PROSEA_ID")
    @Field(type = FieldType.Long)
    private Long proseaId;

    @JSONField(name = "MATERIELTYPE_ECODE")
    @Field(type = FieldType.Keyword)
    private String materieltypeEcode;

    @JSONField(name = "LARGESERIES_ID")
    @Field(type = FieldType.Long)
    private Long largeseriesId;

    @JSONField(name = "JACKET_BOTTOM_ECODE")
    @Field(type = FieldType.Keyword)
    private String jacketBottomEcode;

    @JSONField(name = "SMALLSERIES_ID")
    @Field(type = FieldType.Long)
    private Long smallseriesId;

    @JSONField(name = "SMALLCATE_ID")
    @Field(type = FieldType.Long)
    private Long smallcateId;

    @JSONField(name = "SEX_ID")
    @Field(type = FieldType.Long)
    private Long sexId;

    @JSONField(name = "QTY_END")
    @Field(type = FieldType.Double)
    private BigDecimal qtyEnd;

    @JSONField(name = "QTY_AVAILABLE")
    @Field(type = FieldType.Double)
    private BigDecimal qtyAvailable;

    @JSONField(name = "QTY_STORAGE")
    @Field(type = FieldType.Double)
    private BigDecimal qtyStorage;

    @JSONField(name = "OWNERENAME")
    @Field(type = FieldType.Keyword)
    private String ownerename;

    @JSONField(name = "MODIFIERENAME")
    @Field(type = FieldType.Keyword)
    private String modifierename;

}