package com.jackrain.nea.sg.basic.common;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/3/16 22:28
 */
public interface SgConstantsIF {

    //组别
    String GROUP = "sg";
    //版本
    String VERSION = "1.0";

    //业务节点
    long SERVICE_NODE_PUR_SUBMIT = 1;//采购单审核
    long SERVICE_NODE_PUR_UNSUBMIT = 2;//采购单取消审核
    long SERVICE_NODE_PUR_IN_SUBMIT = 3;//采购单入库审核
    long SERVICE_NODE_PUR_IN = 4;//采购入库
    long SERVICE_NODE_REF_PUR_SUBMIT = 5;//采购退货单审核
    long SERVICE_NODE_REF_PUR_UNSUBMIT = 6;//采购退货单取消审核
    long SERVICE_NODE_REF_PUR_OUT_SUBMIT = 7;//采购退货单出库审核
    long SERVICE_NODE_REF_PUR_OUT = 8;//采购退货出库
    long SERVICE_NODE_SALE_SUBMIT = 9;//销售单审核
    long SERVICE_NODE_SALE_UNSUBMIT = 10;//销售单取消审核
    long SERVICE_NODE_SALE_IN_SUBMIT = 11;//销售单入库审核
    long SERVICE_NODE_SALE_OUT_SUBMIT = 12;//销售单出库审核
    long SERVICE_NODE_SALE_IN = 13;//销售入库
    long SERVICE_NODE_SALE_OUT = 14;//销售出库
    long SERVICE_NODE_REF_SALE_SUBMIT = 15;//销售退货单审核
    long SERVICE_NODE_REF_SALE_UNSUBMIT = 16;//销售退货单取消审核
    long SERVICE_NODE_REF_SALE_IN_SUBMIT = 17;//销售退货单入库审核
    long SERVICE_NODE_REF_SALE_OUT_SUBMIT = 18;//销售退货单出库审核
    long SERVICE_NODE_REF_SALE_IN = 19;//销售退货入库
    long SERVICE_NODE_REF_SALE_OUT = 20;//销售退货出库
    long SERVICE_NODE_TRANSFER_SUBMIT = 21;//调拨单审核
    long SERVICE_NODE_TRANSFER_UNSUBMIT = 22;//调拨单取消审核
    long SERVICE_NODE_TRANSFER_IN_SUBMIT = 23;//调拨单入库审核
    long SERVICE_NODE_TRANSFER_OUT_SUBMIT = 24;//调拨单出库审核
    long SERVICE_NODE_TRANSFER_IN = 25;//调拨入库
    long SERVICE_NODE_TRANSFER_OUT = 26;//调拨出库
    long SERVICE_NODE_RETAIL = 27;//零售
    long SERVICE_NODE_INVENTORY = 28;//盘点损益
    long SERVICE_NODE_OTHERS_INOUT = 29;//其他出入库
    long SERVICE_NODE_TRANSFER_LOCK = 30;//调拨单锁单
    long SERVICE_NODE_TRANSFER_UNLOCK = 31;//调拨单取消锁单
    long SERVICE_NODE_SALE_LOCK = 32;//销售单锁单
    long SERVICE_NODE_SALE_UNLOCK = 33;//销售单取消锁单
    long SERVICE_NODE_TRANSFER_DIFF_DEAL = 34;//调拨单差异处理
    long SERVICE_NODE_SALE_DIFF_DEAL = 35;//销售单单差异处理
    long SERVICE_NODE_SALE_REF_DIFF_DEAL = 36;//销售退货单差异处理

    long SERVICE_NODE_ADJUST_SUBMIT = 37;//逻辑调整单审核
    long SERVICE_NODE_TRANSFER_CANCEL_OUT = 38;//调拨单取消出库
    //start sunjunlei
    long SERVICE_NODE_SALE_CANCEL_OUT = 39;//销售单取消出库
    //end sunjunlei

    //调拨入库单作废
    long SERVICE_NODE_TRANSFER_IN_VOID = 40;
    long SERVICE_NODE_CHKDIFFE_LOCK = 41;//盘差单锁单
    long SERVICE_NODE_CHKDIFFE_UNLOCK = 42;//盘差单取消锁单
    //退货申请单取消出库
    long SERVICE_NODE_REFUND_SALE_CANCEL_OUT = 44;//退货申请单取消出库

    long SERVICE_NODE_RETAIL_POS_SUBMIT = 43;//零售单审核

    //来源单据类型
    int BILL_TYPE_RETAIL = 1;//零售发货单
    int BILL_TYPE_RETAIL_REF = 2;//零售退货单
    int BILL_TYPE_SALE = 3;//销售单
    int BILL_TYPE_SALE_REF = 4;//销售退货单
    int BILL_TYPE_PUR = 5;//采购单
    int BILL_TYPE_PUR_REF = 6;//采购退货单
    int BILL_TYPE_TRANSFER = 7;//调拨单
    int BILL_TYPE_VIPSHOP = 8;//唯品会单
    int BILL_TYPE_ADJUST = 9;//库存调整单
    int BILL_TYPE_VIPSHOP_TIME = 10;//唯品会时效订单
    int BILL_TYPE_DIRECT = 12;//直发单
    int BILL_TYPE_RETAIL_POS = 11;//零售单
    int BILL_TYPE_CHKDIFFE = 13;//盘差单

    int STORAGE_TYPE_PREOUT = 1;//占用量
    int STORAGE_TYPE_PREIN = 2;//在途
    int STORAGE_TYPE_STORAGE = 3;//在库

    //占用错误处理类别
    //报警（允许负库存）
    int PREOUT_OPT_TYPE_WARNING = 1;
    //报错
    int PREOUT_OPT_TYPE_ERROR = 2;
    //报缺货
    int PREOUT_OPT_TYPE_OUT_STOCK = 3;

    //占用更新结果
    //正常（成功）
    int PREOUT_RESULT_SUCCESS = 0;
    //异常（报警）
    int PREOUT_RESULT_WARNING = 1;
    //异常（报错）
    int PREOUT_RESULT_ERROR = 2;
    //异常（缺货）
    int PREOUT_RESULT_OUT_STOCK = 3;

    /**
     * 单据明细修改方式(1.全量 2.增量)
     */
    Integer ITEM_UPDATE_TYPE_ALL = 1;
    Integer ITEM_UPDATE_TYPE_INC = 2;

    int OPERATE_TYPE_OCCUPY = 1;//占用
    int OPERATE_TYPE_RELEASE = 2;//释放

    /**
     * 矩阵查询类型（1.逻辑仓 2.实体仓）
     */
    int MATRIX_QUERY_TYPE_STORAGE = 1;
    int MATRIX_QUERY_TYPE_PHY_STORAGE = 2;

    String COMBINED_POOL = "COMBINED:POOL";
    String COMBINED_SKUGROUP = "COMBINED:SKUGROUP";
    String COMBINED_SKU = "COMBINED:SKU";

    /**
     * 商品标识类型（ZP 正品 CC 次品）
     */
    String TRADE_MARK_CC = "CC";
    String TRADE_MARK_ZP = "ZP";

    /**
     * 店仓属性（01 正品仓 02 次品仓）
     */
    String STORE_TYPE_ZP = "01";
    String STORE_TYPE_CC = "02";

    /**
     * 组合商品同步渠道库存tag
     */
    String MSG_TAG_GROUP_STORAGE_TO_CHANNEL = "group_storage_to_channel";

}
