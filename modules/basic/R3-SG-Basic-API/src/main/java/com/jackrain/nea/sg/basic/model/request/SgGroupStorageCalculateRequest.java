package com.jackrain.nea.sg.basic.model.request;

import com.jackrain.nea.web.face.User;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author 舒威
 * @since 2019/7/11
 * create at : 2019/7/11 11:06
 */
@Data
public class SgGroupStorageCalculateRequest implements Serializable {

    private List<SgGroupStorageVirtualSkuRequest> virtualSkuList;

    private User loginUser;
}
