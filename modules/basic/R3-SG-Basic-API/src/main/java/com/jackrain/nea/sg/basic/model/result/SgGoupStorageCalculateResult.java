package com.jackrain.nea.sg.basic.model.result;

import lombok.Data;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

/**
 * @author 舒威
 * @since 2019/7/11
 * create at : 2019/7/11 10:51
 */
@Data
public class SgGoupStorageCalculateResult implements Serializable {

    /**
     * 库存计算结果
     */
    private HashMap<String, Object> calculator;

    private List<SgGoupStorageGroupSkuResult> groupSkuResults;
}
