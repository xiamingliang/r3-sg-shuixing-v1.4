package com.jackrain.nea.sg.basic.model.table;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.*;
import com.jackrain.nea.sys.domain.BaseModel;
import lombok.Data;

@TableName(value = "cp_c_phy_warehouse_item")
@Data
public class CpCPhyWarehouseItem extends BaseModel {
    @JSONField(name = "ID")
    @TableField(fill = FieldFill.INSERT)
    @TableId(value = "id", type = IdType.INPUT)
    private Long id;

    @TableField(fill = FieldFill.UPDATE)
    @JSONField(name = "CP_C_LOGISTICS_ID")
    private Long cpCLogisticsId;

    @TableField(fill = FieldFill.UPDATE)
    @JSONField(name = "CP_C_LOGISTICS_ECODE")
    private String cpCLogisticsEcode;

    @TableField(fill = FieldFill.UPDATE)
    @JSONField(name = "CP_C_LOGISTICS_ENAME")
    private String cpCLogisticsEname;

    @TableField(fill = FieldFill.UPDATE)
    @JSONField(name = "IS_ENABLE_EWAYBILL")
    private String isEnableEwaybill;

    @TableField(fill = FieldFill.UPDATE)
    @JSONField(name = "BRANCH_CODE")
    private String branchCode;

    @TableField(fill = FieldFill.UPDATE)
    @JSONField(name = "SETTLEMENT_CODE")
    private String settlementCode;

    @TableField(fill = FieldFill.UPDATE)
    @JSONField(name = "LOGISTICS_TYPE")
    private String logisticsType;

    @TableField(fill = FieldFill.UPDATE)
    @JSONField(name = "EXPRESS_PAY_TYPE")
    private Long expressPayType;

    @TableField(fill = FieldFill.UPDATE)
    @JSONField(name = "EXPRESS_TYPE")
    private Long expressType;

    @JSONField(name = "CP_C_PHY_WAREHOUSE_ID")
    private Long cpCPhyWarehouseId;

    @JSONField(name = "CP_C_PHY_WAREHOUSE_ECODE")
    private String cpCPhyWarehouseEcode;

    @JSONField(name = "CP_C_PHY_WAREHOUSE_ENAME")
    private String cpCPhyWarehouseEname;

    @JSONField(name = "OWNERENAME")
    private String ownerename;

    @JSONField(name = "MODIFIERENAME")
    private String modifierename;
}