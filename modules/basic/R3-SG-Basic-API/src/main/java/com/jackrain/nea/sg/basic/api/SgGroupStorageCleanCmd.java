package com.jackrain.nea.sg.basic.api;

import com.jackrain.nea.sg.basic.model.request.SgGroupStorageCleanRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 * @author: 舒威
 * @since: 2019/7/14
 * create at : 2019/7/14 15:17
 */
public interface SgGroupStorageCleanCmd {

    /**
     * 组合商品停用-清空库存服务
     * return : 清空成功记录数
     */
    ValueHolderV14<Integer> cleanGroupStorage(SgGroupStorageCleanRequest request);
}
