package com.jackrain.nea.sg.basic.model.request;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class SgQtyStorageByWareCalcItemRequest implements Serializable {

    /**
     * 条码id
     */
    private Long psCSkuId;
    /**
     * 条码编码
     */
    private String psCSkuEcode;
    /**
     * 变动数量
     */
    private BigDecimal qtyChange;

}
