package com.jackrain.nea.sg.basic.api;

import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 * @author: 舒威
 * @since: 2019/8/19
 * create at : 2019/8/19 17:10
 */
public interface SgMQResendCmd {


    ValueHolderV14 sendMQ(String body, String topic, String tag, String msgKey);
}
