package com.jackrain.nea.sg.basic.api;

import com.jackrain.nea.sg.basic.model.request.SgGroupQueryRequest;
import com.jackrain.nea.sg.basic.model.request.SgGroupStorageQueryRequest;
import com.jackrain.nea.sg.basic.model.result.SgGroupStorageQueryResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.query.QuerySession;

import java.util.List;

/**
 * @author 舒威
 * @since 2019/7/11
 * create at : 2019/7/11 19:39
 */
public interface SgGroupStorageQueryCmd {

    /**
     * 福袋生成-获取福库存数量、实际条码库存数量
     */
    ValueHolderV14<List<SgGroupStorageQueryResult>> queryGroupStorage(List<SgGroupStorageQueryRequest> skuGroupList);

    /**
     * 组合商品库存根据id查询对应逻辑仓下所有实际商品的库存信息
     */
    ValueHolderV14<List<SgGroupQueryRequest>> queryGroupStorageById(QuerySession session);
}
