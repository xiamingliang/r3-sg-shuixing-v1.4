package com.jackrain.nea.sg.basic.model.result;

import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class SgQtyStorageByWareCalcResult implements Serializable {

    /**
     * 变动更新结果
     */
    private int changeUpdateResult;
    /**
     * 调整结果明细信息
     */
    List<SgQtyStorageByWareCalcItemResult> itemResultList;
    /**
     * 调整结果箱明细信息
     */
    List<SgQtyTeusStorageByWareCalcItemResult> teusResultList;

    public SgQtyStorageByWareCalcResult(){
        this.changeUpdateResult = SgConstantsIF.PREOUT_RESULT_SUCCESS;
    }
}
