package com.jackrain.nea.sg.basic.model.request;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/3/17 13:30
 */
@Data
public class SgStorageUpdateBillRequest extends SgStorageUpdateBillExt implements Serializable {

    /**
     * 明细信息
     */
    List<SgStorageUpdateBillItemRequest> itemList;

    /**
     * 箱明细信息
     */
    List<SgTeusStorageUpdateBillItemRequest> teusList;

}
