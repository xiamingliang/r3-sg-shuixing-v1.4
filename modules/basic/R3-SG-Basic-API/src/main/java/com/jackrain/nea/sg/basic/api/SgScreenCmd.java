package com.jackrain.nea.sg.basic.api;

import com.jackrain.nea.sys.Command;

/**
 * 获取筛选条件
 * @author nathan
 */
public interface SgScreenCmd extends Command {
}
