package com.jackrain.nea.sg.basic.model.request;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/4/17 16:27
 */

import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import lombok.Data;

import java.io.Serializable;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/3/24 13:27
 */
@Data
public class SgStorageUpdateControlRequest implements Serializable {

    //占用是否允许负库存
    private boolean isNegativePreout;
    //占用错误处理类别(1.报警（允许负库存）2.报错  3.报缺货)
    private int preoutOperateType;
    //在途是否允许负库存
    private boolean isNegativePrein;
    //在库是否允许负库存
    private boolean isNegativeStorage;
    //可用在库是否允许负库存
    private boolean isNegativeAvailable;

    public SgStorageUpdateControlRequest() {
        this.isNegativePrein = false;
        this.isNegativePreout = false;
        this.isNegativeStorage = false;
        this.isNegativeAvailable = false;
        this.preoutOperateType = SgConstantsIF.PREOUT_OPT_TYPE_ERROR;
    }

}
