package com.jackrain.nea.sg.basic.model.request;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/3/17 13:31
 */
@Data
public class SgStorageUpdateBillItemRequest extends SgStorageUpdateBillItemExt
        implements Serializable, Comparable<SgStorageUpdateBillItemRequest> {

    /**
     * 明细控制类
     */
    private SgStorageUpdateControlRequest controlmodel;

    @Override
    public int compareTo(SgStorageUpdateBillItemRequest o) {
        //目前排序顺序：3;在库 -> 2;在途 -> 1;占用量
        return o.toString().compareTo(this.toString());
    }

    @Override
    public String toString(){
        StringBuffer sb = new StringBuffer();
        sb.append(super.getStorageType())
                .append("_")
                .append(super.getCpCStoreId())
                .append("_")
                .append(super.getPsCSkuId());
        return sb.toString();
    }
}
