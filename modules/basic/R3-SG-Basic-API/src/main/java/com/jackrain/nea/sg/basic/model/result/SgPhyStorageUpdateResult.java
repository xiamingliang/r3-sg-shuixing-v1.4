package com.jackrain.nea.sg.basic.model.result;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/3/14 19:00
 */
@Data
public class SgPhyStorageUpdateResult implements Serializable {

    /*
     * 错误明细件数
     */
    private long errorBillItemQty = 0;
    /*
     * Redis更新流水键
     */
    private List<String> redisBillFtpKeyList;

    public SgPhyStorageUpdateResult() {
        this.errorBillItemQty = 0;
    }
}
