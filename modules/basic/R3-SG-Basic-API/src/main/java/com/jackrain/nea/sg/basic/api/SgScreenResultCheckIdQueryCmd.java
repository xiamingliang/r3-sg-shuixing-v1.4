package com.jackrain.nea.sg.basic.api;

import com.jackrain.nea.sys.Command;

/**
 * 查看选中结果(返回结果ID)
 *
 * @author Yvan (copy to nathon)
 */
public interface SgScreenResultCheckIdQueryCmd extends Command {
}
