package com.jackrain.nea.sg.basic.model.request;

import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Data
public class SgStoreWithPrioritySearchItemRequest implements Serializable {

    /**
     * 虚拟仓优先级列表
     */
    private List<StStockPriorityRequest> priorityList;
    /**
     * 条码id
     */
    private Long psCSkuId;
    /**
     * 变动数量
     */
    private BigDecimal qtyChange;
    /**
     * 明细操作类型
     */
    private Integer operateType;
    /**
     * 来源单据明细编号
     */
    private Long sourceItemId;

    public SgStoreWithPrioritySearchItemRequest() {
        this.operateType = SgConstantsIF.OPERATE_TYPE_OCCUPY;
    }

}
