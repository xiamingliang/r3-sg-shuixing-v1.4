package com.jackrain.nea.sg.basic.model.request;

import com.jackrain.nea.web.face.User;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/3/17 13:23
 */
@Data
public class SgStorageRedisInitRequest implements Serializable {

    /**
     * 逻辑仓ID列表
     */
    private List<Long> storeIds;

    private List<Long> proIds;

    private List<Long> skuIds;

    private List<String> proEcodes;

    private List<String> skuEcodes;

    //用户信息
    private User loginUser;

}
