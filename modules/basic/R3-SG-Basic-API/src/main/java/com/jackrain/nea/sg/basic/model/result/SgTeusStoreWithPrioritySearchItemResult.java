package com.jackrain.nea.sg.basic.model.result;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class SgTeusStoreWithPrioritySearchItemResult implements Serializable {

    /**
     * 店仓id
     */
    private Long cpCStoreId;
    /**
     * 逻辑仓编码
     */
    private String cpCStoreEcode;
    /**
     * 逻辑仓名称
     */
    private String cpCStoreEname;
    /**
     * 箱id
     */
    private Long psCTeusId;
    /**
     * 占用数量
     */
    private BigDecimal qtyPreout;
    /**
     * 缺货数量
     */
    private BigDecimal qtyOutOfStock;
    /**
     * 优先级
     */
    private Integer rank;
    /**
     * 来源单据明细编号
     */
    private Long sourceItemId;

}
