package com.jackrain.nea.sg.basic.api;

import com.jackrain.nea.sg.basic.model.request.SgPhyStorageBatchUpdateRequest;
import com.jackrain.nea.sg.basic.model.result.SgPhyStorageUpdateResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/4/26 20:40
 */
public interface SgPhyStorageBatchUpdateCmd {

    /**
     *
     * @param request
     * @return
     */
    ValueHolderV14<SgPhyStorageUpdateResult> updateStorageBatch(SgPhyStorageBatchUpdateRequest request);

}
