package com.jackrain.nea.sg.basic.model.request;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/4/26 20:43
 */
@Data
public class SgPhyStorageUpdateBillExt extends SgPhyStorageUpdateBillBase implements Serializable {

    /**
     * Redis更新流水键
     */
    private String redisBillFtpKey;

}
