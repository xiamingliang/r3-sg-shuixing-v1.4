package com.jackrain.nea.sg.basic.model.request;

import com.jackrain.nea.web.face.User;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author: 舒威
 * @since: 2019/7/14
 * create at : 2019/7/14 17:32
 */
@Data
public class SgGroupStorageCleanRequest implements Serializable {

    private List<SgGroupStorageVirtualSkuRequest> virtualSkuRequests;

    private User loginUser;

}
