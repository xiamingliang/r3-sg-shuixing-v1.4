package com.jackrain.nea.sg.basic.api;

import com.jackrain.nea.sg.basic.model.request.SgPhyStorageQueryRequest;
import com.jackrain.nea.sg.basic.model.table.SgBPhyStorage;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;

import java.util.List;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/4/25 20:42
 */
public interface SgPhyStorageQueryCmd {

    /**
     * @Description: 实体仓库存查询接口
     * @Author: chenb
     * @Date: 2019/3/7 16:05
     * @Param: sgStorageQueryRequest 查询实体仓库存请求信息
     * @Return: ValueHolder
     */
    ValueHolderV14<List<SgBPhyStorage>> queryPhyStorage(SgPhyStorageQueryRequest request, User loginUser);
}
