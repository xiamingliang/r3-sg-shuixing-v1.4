package com.jackrain.nea.sg.basic.model.result;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class SgQtyStorageByWareCalcItemResult implements Serializable {

    /**
     * 店仓id
     */
    private Long cpCStoreId;
    /**
     * 逻辑仓编码
     */
    private String cpCStoreEcode;
    /**
     * 逻辑仓名称
     */
    private String cpCStoreEname;
    /**
     * 条码id
     */
    private Long psCSkuId;
    /**
     * 在库数量
     */
    private BigDecimal qtyStorage;

}
