package com.jackrain.nea.sg.basic.model.request;

import lombok.Data;

import java.io.Serializable;

/**
 * @author zhu lin yu
 * @since 2019-11-01
 * create at : 2019-11-01 10:26
 */
@Data
public class SgTeusStoragePageRequest implements Serializable {

    private int pageNum;

    private int pageSize;

    private String sortKeys;

    public SgTeusStoragePageRequest() {
        this.pageNum = 1;
        this.pageSize = 20;
    }
}
