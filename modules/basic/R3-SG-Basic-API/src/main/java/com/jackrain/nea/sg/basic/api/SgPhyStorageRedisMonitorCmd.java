package com.jackrain.nea.sg.basic.api;

import com.jackrain.nea.sg.basic.model.request.SgPhyStorageRedisMonitorRequest;
import com.jackrain.nea.sg.basic.model.result.SgPhyStorageRedisMonitorResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/7/10 00:00
 */
public interface SgPhyStorageRedisMonitorCmd {

    /**
     * @Description: Redis实体仓库存监控接口
     * @Author: chenb
     * @Date: 2019/3/7 16:05
     * @Param: SgStorageRedisMonitorRequest Redis实体仓库存监控请求信息
     * @Return: ValueHolder
     */
    ValueHolderV14<SgPhyStorageRedisMonitorResult> monitorRedisPhyStorage(SgPhyStorageRedisMonitorRequest request);

}
