package com.jackrain.nea.sg.basic.model.request;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/3/17 13:30
 */
@Data
public class SgStorageUpdateBillItemExt extends SgStorageUpdateBillItemBase implements Serializable {

    /**
     * 逻辑仓编码
     */
    private String cpCStoreEcode;
    /**
     * 逻辑仓名称
     */
    private String cpCStoreEname;
    /**
     * 商品id
     */
    private Long psCProId;
    /**
     * 商品编码
     */
    private String psCProEcode;
    /**
     * 商品名称
     */
    private String psCProEname;
    /**
     * 条码编码
     */
    private String psCSkuEcode;
    /**
     * 规格1ID
     */
    private Long psCSpec1Id;
    /**
     * 规格1编码
     */
    private String psCSpec1Ecode;
    /**
     * 规格1名称
     */
    private String psCSpec1Ename;
    /**
     * 规格2ID
     */
    private Long psCSpec2Id;
    /**
     * 规格2编码
     */
    private String psCSpec2Ecode;
    /**
     * 规格2名称
     */
    private String psCSpec2Ename;
    /**
     * 吊牌价
     */
    private BigDecimal priceList;
    /**
     * 成本价
     */
    private BigDecimal priceCost;
    /**
     * 国标码
     */
    private String gbcode;
    /**
     * 箱内占用变动数量
     */
    private BigDecimal qtyPreoutTeusChange;
    /**
     * 箱内在途变动数量
     */
    private BigDecimal qtyPreinTeusChange;
    /**
     * 箱内在库变动数量
     */
    private BigDecimal qtyStorageTeusChange;

    public SgStorageUpdateBillItemExt() {
        this.qtyPreoutTeusChange = BigDecimal.ZERO;
        this.qtyPreinTeusChange = BigDecimal.ZERO;
        this.qtyStorageTeusChange = BigDecimal.ZERO;
    }

}
