package com.jackrain.nea.sg.basic.model.request;

import lombok.Data;

import java.io.Serializable;

/**
 * @author zhu lin yu
 * @since 2019-11-04
 * create at : 2019-11-04 17:22
 */
@Data
public class SgTeusStoreStoragePageQueryRequest implements Serializable {

    private SgTeusStoreStorageQueryRequest sgTeusStoreStorageQueryRequest;

    private SgTeusStoragePageRequest sgStoragePageRequest;
}
