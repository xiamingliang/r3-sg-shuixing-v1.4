package com.jackrain.nea.sg.basic.api;

import com.jackrain.nea.sg.basic.model.request.SgStorageSingleUpdateRequest;
import com.jackrain.nea.sg.basic.model.result.SgStorageUpdateResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/3/11 18:59
 */
public interface SgStorageNoTransUpdateCmd {

    /**
     * @Description: 单张单据库存更新接口(沿用消费者事务)
     * @Author: chenb
     * @Date: 2019/3/7 16:05
     * @Param: SgPreoutStorageUpdateRequest 在单库存更新请求信息
     * @Return: ValueHolder
     */
    ValueHolderV14<SgStorageUpdateResult> updateStorageBillNoTrans(SgStorageSingleUpdateRequest request);

}
