package com.jackrain.nea.sg.basic.model.table;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

@Data
public class SgBStorageInclPhy extends SgBStorage {

    @JSONField(name = "CP_C_PHY_WAREHOUSE_ID")
    private Long cpCPhyWarehouseId;

    @JSONField(name = "CP_C_PHY_WAREHOUSE_ECODE")
    private String cpCPhyWarehouseEcode;

    @JSONField(name = "CP_C_PHY_WAREHOUSE_ENAME")
    private String cpCPhyWarehouseEname;

}
