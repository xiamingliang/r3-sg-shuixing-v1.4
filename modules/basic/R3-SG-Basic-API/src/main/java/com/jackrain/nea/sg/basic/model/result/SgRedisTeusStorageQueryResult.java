package com.jackrain.nea.sg.basic.model.result;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/3/13 15:00
 */
@Data
public class SgRedisTeusStorageQueryResult implements Serializable {

    /**
     * 店仓id
     */
    @JSONField(name = "CP_C_STORE_ID")
    private Long cpCStoreId;
    /**
     * 箱id
     */
    @JSONField(name = "PS_C_TEUS_ID")
    private Long psCTeusId;
    /**
     * 占用数量
     */
    @JSONField(name = "QTY_PREOUT")
    private BigDecimal qtyPreout;
    /**
     * 在途数量
     */
    @JSONField(name = "QTY_PREIN")
    private BigDecimal qtyPrein;
    /**
     * 在库数量
     */
    @JSONField(name = "QTY_STORAGE")
    private BigDecimal qtyStorage;
    /**
     * 可用数量
     */
    @JSONField(name = "QTY_AVAILABLE")
    private BigDecimal qtyAvailable;


}
