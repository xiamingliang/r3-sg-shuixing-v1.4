package com.jackrain.nea.sg.basic.model.request;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/3/17 13:23
 */
@Data
public class SgStorageBatchUpdateRequest extends SgStorageUpdateBaseRequest implements Serializable {

    //单据信息列表
    private List<SgStorageUpdateBillRequest> billList;

}
