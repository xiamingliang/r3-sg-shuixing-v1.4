package com.jackrain.nea.sg.basic.model.request;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/3/17 13:30
 */
@Data
public class SgPhyTeusStorageUpdateBillItemExt extends SgPhyTeusStorageUpdateBillItemBase implements Serializable {

    /**
     * 实体仓编码
     */
    private String cpCPhyWarehouseEcode;
    /**
     * 实体仓名称
     */
    private String cpCPhyWarehouseEname;
    /**
     * 商品id
     */
    private Long psCProId;
    /**
     * 商品编码
     */
    private String psCProEcode;
    /**
     * 商品名称
     */
    private String psCProEname;
    /**
     * 箱号
     */
    private String psCTeusEcode;
    /**
     * 配码id
     */
    private Long psCMatchsizeId;
    /**
     * 配码编码
     */
    private String psCMatchsizeEcode;
    /**
     * 配码名称
     */
    private String psCMatchsizeEname;
    /**
     * 规格1ID
     */
    private Long psCSpec1Id;
    /**
     * 规格1编码
     */
    private String psCSpec1Ecode;
    /**
     * 规格1名称
     */
    private String psCSpec1Ename;
    /**
     * 规格2ID
     */
    private Long psCSpec2Id;
    /**
     * 规格2编码
     */
    private String psCSpec2Ecode;
    /**
     * 规格2名称
     */
    private String psCSpec2Ename;

}
