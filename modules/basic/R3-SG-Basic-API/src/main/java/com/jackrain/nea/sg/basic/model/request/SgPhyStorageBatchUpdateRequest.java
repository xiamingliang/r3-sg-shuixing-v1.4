package com.jackrain.nea.sg.basic.model.request;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/4/26 20:41
 */
@Data
public class SgPhyStorageBatchUpdateRequest extends SgStorageUpdateBaseRequest implements Serializable {

    //单据信息列表
    private List<SgPhyStorageUpdateBillRequest> billList;
}
