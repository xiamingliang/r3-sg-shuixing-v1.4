package com.jackrain.nea.sg.basic.model.request;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author 舒威
 * @since 2019/7/20
 * create at : 2019/7/20 20:12
 */
@Data
public class SgGroupStoragePoolBillRequest implements Serializable {

    private String changeTime;

    private List<SgGroupStoragePoolExcuteRequest> itemList;
}
