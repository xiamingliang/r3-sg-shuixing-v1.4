package com.jackrain.nea.sg.basic.model.request;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/4/26 20:47
 */
@Data
public class SgPhyStorageUpdateBillItemRequest extends SgPhyStorageUpdateBillItemExt
        implements Serializable, Comparable<SgPhyStorageUpdateBillItemRequest> {

    @Override
    public int compareTo(SgPhyStorageUpdateBillItemRequest o) {
        return o.toString().compareTo(this.toString());
    }

    @Override
    public String toString(){
        StringBuffer sb = new StringBuffer();
        sb.append(super.getCpCPhyWarehouseId())
                .append("_")
                .append(super.getPsCSkuId());
        return sb.toString();
    }
}
