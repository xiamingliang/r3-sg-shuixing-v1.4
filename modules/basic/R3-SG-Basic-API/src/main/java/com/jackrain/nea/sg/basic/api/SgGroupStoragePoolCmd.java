package com.jackrain.nea.sg.basic.api;

import com.jackrain.nea.sg.basic.model.request.SgGroupStoragePoolRequest;
import com.jackrain.nea.sg.basic.model.result.SgGoupStorageCalculateResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 * @author 舒威
 * @since 2019/7/8
 * create at : 2019/7/8 17:12
 */
public interface SgGroupStoragePoolCmd {

    ValueHolderV14<SgGoupStorageCalculateResult> addGroupStoragePoll(SgGroupStoragePoolRequest request);
}
