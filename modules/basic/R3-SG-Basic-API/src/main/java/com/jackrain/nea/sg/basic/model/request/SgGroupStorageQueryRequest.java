package com.jackrain.nea.sg.basic.model.request;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author: 舒威
 * @since: 2019/7/14
 * create at : 2019/7/14 16:55
 */
@Data
public class SgGroupStorageQueryRequest implements Serializable{

    /**
     * 虚拟条码id
     */
    private Long id;

    /**
     * 逻辑仓id
     */
    private List<Long> storeIds;

    /**
     * 实际条码id
     */
    private List<Long> skuIds;
}
