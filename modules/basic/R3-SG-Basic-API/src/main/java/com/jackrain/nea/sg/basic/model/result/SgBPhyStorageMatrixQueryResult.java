package com.jackrain.nea.sg.basic.model.result;

import com.alibaba.fastjson.annotation.JSONField;
import com.jackrain.nea.sg.basic.model.table.SgBPhyStorage;
import lombok.Data;

import java.io.Serializable;

/**
 * @author csy
 * Date: 2019/5/31
 * Description:
 */
@Data
public class SgBPhyStorageMatrixQueryResult extends SgBPhyStorage implements Serializable {

    @JSONField(name = "PS_C_CLR_ID")
    private Long psCClrId;


    @JSONField(name = "PS_C_CLR_ECODE")
    private String psCClrEcode;


    @JSONField(name = "PS_C_CLR_ENAME")
    private String psCClrEname;


    @JSONField(name = "PS_C_SIZE_ID")
    private Long psCSizeId;


    @JSONField(name = "PS_C_SIZE_ECODE")
    private String psCSizeEcode;

    @JSONField(name = "PS_C_SIZE_ENAME")
    private String psCSizeEname;

}
