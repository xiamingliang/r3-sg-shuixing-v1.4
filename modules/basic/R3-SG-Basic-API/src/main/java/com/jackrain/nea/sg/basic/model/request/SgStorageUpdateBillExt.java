package com.jackrain.nea.sg.basic.model.request;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/3/17 13:30
 */
@Data
public class SgStorageUpdateBillExt extends SgStorageUpdateBillBase implements Serializable {

    /**
     * 单出库通知据ID
     */
    private Long phyOutNoticesId;
    /**
     * 出库通知单据编号
     */
    private String phyOutNoticesNo;
    /**
     * 入库通知单据ID
     */
    private Long phyInNoticesId;
    /**
     * 入库通知单据编号
     */
    private String phyInNoticesNo;
    /**
     * Redis更新流水键
     */
    private String redisBillFtpKey;
}
