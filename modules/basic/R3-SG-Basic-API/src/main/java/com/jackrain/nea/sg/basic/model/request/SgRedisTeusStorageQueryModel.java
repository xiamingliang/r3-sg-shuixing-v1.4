package com.jackrain.nea.sg.basic.model.request;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/3/13 15:00
 */
@Data
public class SgRedisTeusStorageQueryModel implements Serializable {

    /**
     * 店仓id
     */
    private Long cpCStoreId;
    /**
     * 箱id
     */
    private Long psCTeusId;


}
