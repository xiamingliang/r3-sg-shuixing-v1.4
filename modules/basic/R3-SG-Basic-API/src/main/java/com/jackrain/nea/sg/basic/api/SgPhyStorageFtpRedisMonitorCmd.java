package com.jackrain.nea.sg.basic.api;

import com.jackrain.nea.sg.basic.model.request.SgPhyStorageFtpRedisMonitorRequest;
import com.jackrain.nea.sg.basic.model.result.SgPhyStorageFtpRedisMonitorResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/7/10 00:00
 */
public interface SgPhyStorageFtpRedisMonitorCmd {

    /**
     * @Description: Redis实体仓库存流水监控接口
     * @Author: chenb
     * @Date: 2019/3/7 16:05
     * @Param: SgStorageFtpRedisMonitorRequest Redis实体仓库存流水监控请求信息
     * @Return: ValueHolder
     */
    ValueHolderV14<SgPhyStorageFtpRedisMonitorResult> monitorRedisPhyStorageFtp(SgPhyStorageFtpRedisMonitorRequest request);

}
