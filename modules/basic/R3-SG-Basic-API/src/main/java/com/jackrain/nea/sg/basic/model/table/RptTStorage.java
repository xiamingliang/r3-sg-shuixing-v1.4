package com.jackrain.nea.sg.basic.model.table;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.jackrain.nea.es.annotations.Document;
import com.jackrain.nea.es.annotations.Field;
import com.jackrain.nea.es.constans.FieldType;
import com.jackrain.nea.sys.domain.BaseModel;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@TableName(value = "v_rpt_t_storage")
@Data
@Document(index = "v_rpt_t_storage",type = "v_rpt_t_storage")
public class RptTStorage extends BaseModel {
    @JSONField(name = "ID")
    @Field(type = FieldType.Long)
    private Long id;

    @JSONField(name = "VERSION")
    @Field(type = FieldType.Keyword)
    private String version;

    @JSONField(name = "RPT_DATE")
    @Field(type = FieldType.Date)
    private Date rptDate;

    @JSONField(name = "CP_C_STORE_ID")
    @Field(type = FieldType.Long)
    private Long cpCStoreId;

    @JSONField(name = "CP_C_STORE_ECODE")
    @Field(type = FieldType.Keyword)
    private String cpCStoreEcode;

    @JSONField(name = "CP_C_STORE_ENAME")
    @Field(type = FieldType.Keyword)
    private String cpCStoreEname;

    @JSONField(name = "LOGISTICS_STORAGE_TYPE")
    @Field(type = FieldType.Integer)
    private Integer logisticsStorageType;

    @JSONField(name = "LOGISTICS_STORAGE_TYPE_ECODE")
    @Field(type = FieldType.Keyword)
    private String logisticsStorageTypeEcode;

    @JSONField(name = "LOGISTICS_STORAGE_TYPE_ENAME")
    @Field(type = FieldType.Keyword)
    private String logisticsStorageTypeEname;

    @JSONField(name = "QTY_END")
    @Field(type = FieldType.Double)
    private BigDecimal qtyEnd;

    @JSONField(name = "OWNERENAME")
    @Field(type = FieldType.Keyword)
    private String ownerename;

    @JSONField(name = "MODIFIERENAME")
    @Field(type = FieldType.Keyword)
    private String modifierename;
}