package com.jackrain.nea.sg.basic.model.request;

import com.jackrain.nea.web.face.User;
import lombok.Data;

import java.io.Serializable;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/3/17 13:23
 */
@Data
public class SgPhyStorageFtpRedisMonitorRequest implements Serializable {

    //用户信息
    private User loginUser;

}
