package com.jackrain.nea.sg.basic.model.request;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/4/26 20:48
 */
@Data
public class SgPhyStorageUpdateBillItemBase implements Serializable {

    /** 单据明细ID */
    private Long billItemId;
    /** 实体仓id */
    private Long cpCPhyWarehouseId;
    /** 条码id */
    private Long psCSkuId;
    /** 变动数量 */
    private BigDecimal qtyChange;

    public SgPhyStorageUpdateBillItemBase(){
        this.qtyChange = BigDecimal.ZERO;
    }
}
