package com.jackrain.nea.sg.basic.model.result;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: zyj
 * Date: 2019/7/29
 * Time: 20:21
 * Description: 逐日库存查询
 */
@Data
public class SgRcStorageQueryResult implements Serializable{

    @JSONField(name = "RPT_DATE")
    private String rptDate;

    @JSONField(name = "CP_C_STORE_ENAME")
    private String cpCStoreEname;

    //鞋子
    @JSONField(name = "SHOT")
    private Integer shot;

    //服装
    @JSONField(name = "CLOTH")
    private Integer cloth;

    //配件
    @JSONField(name = "PART")
    private Integer part;

    //袜子
    @JSONField(name = "STOCK")
    private Integer stock;

    //纸质
    @JSONField(name = "PAPER")
    private Integer paper;


}
