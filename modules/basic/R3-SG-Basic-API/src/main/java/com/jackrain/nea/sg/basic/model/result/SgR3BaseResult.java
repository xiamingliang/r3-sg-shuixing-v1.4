package com.jackrain.nea.sg.basic.model.result;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.util.ValueHolder;
import lombok.Data;

import java.io.Serializable;

/**
 * r3页面返回result
 */
@Data
public class SgR3BaseResult implements Serializable {

    private JSONObject dataJo;

    private JSONArray dataArr;

    public final static String SAVE = "save";

    public final static String VOID = "void";

    public final static String SUBMIT = "submit";

    public final static String UN_SUBMIT = "un_submit";

    /*TODO 这里实现转换为R3页面需要的结构*/
    public ValueHolder isOk(String action, String message, JSONObject extraData, JSONObject data) {
        ValueHolder vh = new ValueHolder();
        switch (action) {
            case VOID:
            case SUBMIT:
            case UN_SUBMIT:
                vh.put("code", ResultCode.SUCCESS);
                vh.put("message", message);
                vh.put("extraData", extraData);
                return vh;
            case SAVE:
                vh.put("code", ResultCode.SUCCESS);
                vh.put("message", message);
                vh.put("extraData", extraData);
                vh.put("data", data);
                return vh;
            default:
                throw new NDSException(Resources.getMessage("未知的动作"));
        }
    }

    public ValueHolder isFail(String action, String message, JSONArray data) {
        ValueHolder vh = new ValueHolder();
        switch (action) {
            case VOID:
            case SUBMIT:
            case UN_SUBMIT:
                vh.put("code", ResultCode.FAIL);
                vh.put("message", message);
                return vh;
            case SAVE:
                vh.put("code", ResultCode.FAIL);
                vh.put("message", message);
                vh.put("data", data);
                return vh;
            default:
                return null;
        }
    }
}
