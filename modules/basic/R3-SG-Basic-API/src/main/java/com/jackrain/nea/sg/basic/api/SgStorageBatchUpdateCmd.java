package com.jackrain.nea.sg.basic.api;

import com.jackrain.nea.sg.basic.model.request.SgStorageBatchUpdateRequest;
import com.jackrain.nea.sg.basic.model.result.SgStorageUpdateResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/3/20 21:06
 */
public interface SgStorageBatchUpdateCmd {

    /**
     *
     * @param request
     * @return
     */
    ValueHolderV14<SgStorageUpdateResult> updateStorageBatch(SgStorageBatchUpdateRequest request);

}
