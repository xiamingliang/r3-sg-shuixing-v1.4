package com.jackrain.nea.sg.basic.model.request;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author zhu lin yu
 * @since 2019-11-01
 * create at : 2019-11-01 10:20
 */
@Data
public class SgTeusStorageQueryRequest implements Serializable {

    private List<Long> storeIds;

    private List<Long> phyWarehouseIds;

    private List<String> teusEcodes;

    private List<String> proEcodes;
}
