package com.jackrain.nea.sg.basic.api;

import com.github.pagehelper.PageInfo;
import com.jackrain.nea.sg.basic.model.request.*;
import com.jackrain.nea.sg.basic.model.result.SgRedisStorageQueryResult;
import com.jackrain.nea.sg.basic.model.result.SgSumStorageQueryResult;
import com.jackrain.nea.sg.basic.model.table.SgBStorage;
import com.jackrain.nea.sg.basic.model.table.SgBStorageInclPhy;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;

import java.util.List;

public interface SgStorageQueryCmd {

    /**
     * @Description: 库存查询接口
     * @Author: chenb
     * @Date: 2019/3/7 16:05
     * @Param: sgStorageQueryRequest 查询库存请求信息
     * @Return: ValueHolder
     */
    ValueHolderV14<List<SgBStorage>> queryStorage(SgStorageQueryRequest request, User loginUser);

    /**
     * @Description: 库存查询接口(在库数量不等于0)
     * @Author: chenb
     * @Date: 2019/8/20 16:05
     * @Param: sgStorageQueryRequest 查询库存请求信息
     * @Return: ValueHolder
     */
    ValueHolderV14<List<SgBStorage>> queryStorageExclZero(SgStorageQueryRequest request, User loginUser);

    /**
     * @Description: 单实体仓库存查询接口
     * @Author: chenb
     * @Date: 2019/3/7 16:05
     * @Param: sgStorageQueryRequest 查询库存请求信息
     * @Return: ValueHolder
     */
    ValueHolderV14<List<SgBStorage>> queryPhyWarehouseStorage(SgPhyStorageQueryRequest request, User loginUser);

    /**
     * @Description: 单虚拟仓库存查询接口
     * @Author: chenb
     * @Date: 2019/3/7 16:05
     * @Param: sgStorageQueryRequest 查询库存请求信息
     * @Return: ValueHolder
     */
    ValueHolderV14<List<SgBStorage>> queryStoreStorage(SgStoreStorageQueryRequest request, User loginUser);

    /**
     * @Description: 库存分页查询接口
     * @Author: chenb
     * @Date: 2019/3/7 16:05
     * @Param: sgStorageQueryRequest 查询库存请求信息
     * @Return: ValueHolder
     */
    ValueHolderV14<PageInfo<SgBStorage>> queryStoragePage(SgStoragePageQueryRequest request, User loginUser);

    /**
     * @Description: 单实体仓库存分页查询接口
     * @Author: chenb
     * @Date: 2019/3/7 16:05
     * @Param: sgStorageQueryRequest 查询库存请求信息
     * @Return: ValueHolder
     */
    ValueHolderV14<PageInfo<SgBStorage>> queryPhyWarehouseStoragePage(SgPhyStoragePageQueryRequest request, User loginUser);

    /**
     * @Description: 单虚拟仓库存分页查询接口
     * @Author: chenb
     * @Date: 2019/3/7 16:05
     * @Param: sgStorageQueryRequest 查询库存请求信息
     * @Return: ValueHolder
     */
    ValueHolderV14<PageInfo<SgBStorage>> queryStoreStoragePage(SgStoreStoragePageQueryRequest request, User loginUser);

    /**
     * @Description: 库存查询接口(包含实体仓信息)
     * @Author: chenb
     * @Date: 2019/3/7 16:05
     * @Param: sgStorageQueryRequest 查询库存请求信息
     * @Return: ValueHolder
     */
    ValueHolderV14<List<SgBStorageInclPhy>> queryStorageInclPhy(SgStorageQueryRequest request, User loginUser);

    /**
     * @Description: 实体仓库存分页查询接口(通过逻辑仓ID)
     * @Author: chenb
     * @Date: 2019/3/7 16:05
     * @Param: sgStorageQueryRequest 查询库存请求信息
     * @Return: ValueHolder
     */
    ValueHolderV14<List<SgSumStorageQueryResult>> querySumStorageGrpPhy(SgSumStoragePageQueryRequest request, User loginUser);

    /**
     * @Description: Redis仓库存查询接口
     * @Author: chenb
     * @Date: 2019/9/14 16:05
     * @Param: SgRedisStorageQueryRequest 查询库存请求信息
     * @Return: ValueHolder
     */
    ValueHolderV14<List<SgRedisStorageQueryResult>> queryRedisStorage(SgRedisStorageQueryRequest request, User loginUser);
}
