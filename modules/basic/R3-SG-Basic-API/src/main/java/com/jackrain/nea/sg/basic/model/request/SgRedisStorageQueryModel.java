package com.jackrain.nea.sg.basic.model.request;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/3/13 15:00
 */
@Data
public class SgRedisStorageQueryModel implements Serializable {

    /**
     * 店仓id
     */
    private Long cpCStoreId;
    /**
     * 条码id
     */
    private Long psCSkuId;


}
