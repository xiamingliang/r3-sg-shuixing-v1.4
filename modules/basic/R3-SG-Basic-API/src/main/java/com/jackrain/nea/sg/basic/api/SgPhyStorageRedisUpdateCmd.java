package com.jackrain.nea.sg.basic.api;

import com.jackrain.nea.sg.basic.model.request.SgPhyStorageSingleUpdateRequest;
import com.jackrain.nea.sg.basic.model.result.SgPhyStorageUpdateResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/7/10 00:00
 */
public interface SgPhyStorageRedisUpdateCmd {

    /**
     * @Description: 单张单据Redis实体仓库存更新接口
     * @Author: chenb
     * @Date: 2019/3/7 16:05
     * @Param: SgStorageSingleUpdateRequest 在单库存更新请求信息
     * @Return: ValueHolder
     */
    ValueHolderV14<SgPhyStorageUpdateResult> updatePhyStorageBillRedis(SgPhyStorageSingleUpdateRequest request);

}
