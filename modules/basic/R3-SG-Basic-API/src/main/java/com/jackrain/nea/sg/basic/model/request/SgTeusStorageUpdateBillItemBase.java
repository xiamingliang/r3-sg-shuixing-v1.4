package com.jackrain.nea.sg.basic.model.request;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/3/17 13:30
 */
@Data
public class SgTeusStorageUpdateBillItemBase implements Serializable {

    /** 库存变动类型 */
    private Integer storageType;
    /** 单据明细ID */
    private Long billItemId;
    /** 店仓id */
    private Long cpCStoreId;
    /** 箱id */
    private Long psCTeusId;
    /** 占用变动数量 */
    private BigDecimal qtyPreoutChange;
    /** 在途变动数量 */
    private BigDecimal qtyPreinChange;
    /** 在库变动数量 */
    private BigDecimal qtyStorageChange;

    public SgTeusStorageUpdateBillItemBase(){
        this.qtyPreoutChange = BigDecimal.ZERO;
        this.qtyPreinChange = BigDecimal.ZERO;
        this.qtyStorageChange = BigDecimal.ZERO;
    }

}
