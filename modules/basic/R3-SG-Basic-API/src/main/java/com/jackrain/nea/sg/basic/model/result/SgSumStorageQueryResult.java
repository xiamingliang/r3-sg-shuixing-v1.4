package com.jackrain.nea.sg.basic.model.result;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/4/25 12:46
 */
@Data
public class SgSumStorageQueryResult implements Serializable {

    @JSONField(name = "CP_C_PHY_WAREHOUSE_ID")
    private Long cpCPhyWarehouseId;

    @JSONField(name = "CP_C_PHY_WAREHOUSE_ECODE")
    private String cpCPhyWarehouseEcode;

    @JSONField(name = "CP_C_PHY_WAREHOUSE_ENAME")
    private String cpCPhyWarehouseEname;

    @JSONField(name = "PS_C_SKU_ID")
    private Long psCSkuId;

    @JSONField(name = "PS_C_SKU_ECODE")
    private String psCSkuEcode;

    @JSONField(name = "PS_C_PRO_ID")
    private Long psCProId;

    @JSONField(name = "PS_C_PRO_ECODE")
    private String psCProEcode;

    @JSONField(name = "PS_C_PRO_ENAME")
    private String psCProEname;

    @JSONField(name = "QTY_PREOUT")
    private BigDecimal qtyPreout;

    @JSONField(name = "QTY_PREIN")
    private BigDecimal qtyPrein;

    @JSONField(name = "QTY_STORAGE")
    private BigDecimal qtyStorage;

    @JSONField(name = "AMT_COST")
    private BigDecimal amtCost;

    @JSONField(name = "QTY_AVAILABLE")
    private BigDecimal qtyAvailable;

}
