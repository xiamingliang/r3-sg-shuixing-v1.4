package com.jackrain.nea.sg.basic.api;

import com.jackrain.nea.sg.basic.model.request.CpStoreSaveRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 * @author: zhu lin yu
 * @since: 2019/4/19
 * create at : 2019/4/19 16:51
 */
public interface CpStoreCmd {

    /**
     * 店仓新增
     * @param cpStoreSaveRequest
     * @return
     */
    ValueHolderV14 insertStore(CpStoreSaveRequest cpStoreSaveRequest);

    /**
     * 店仓修改
     * @param cpStoreSaveRequest
     * @return
     */
    ValueHolderV14 updateStore(CpStoreSaveRequest cpStoreSaveRequest);

    /**
     * 店仓删除
     * @param id
     * @return
     */
    ValueHolderV14 deleteStore(Long id);

}
