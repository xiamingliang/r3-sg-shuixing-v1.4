package com.jackrain.nea.sg.basic.model.request;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/3/30 11:26
 */
@Data
public class StStockPriorityRequest implements Serializable, Comparable<StStockPriorityRequest>{

    //逻辑仓ID
    private Long supplyStoreId;
    //逻辑仓编码
    private String supplyStoreEcode;
    //逻辑仓名称
    private String supplyStoreEname;
    //可用在库是否允许负库存
    private boolean isNegativeAvailable;

    //优先级
    private Integer priority;

    public StStockPriorityRequest() {
        this.isNegativeAvailable = false;
        //默认优先级99
        this.priority = 99;
    }

    @Override
    public int compareTo(StStockPriorityRequest o) {

        int result = 0;

        //根据优先级升序,店仓ID降序排序
        int priorDiff = this.priority - o.getPriority();

        if (priorDiff != 0) {
            result = priorDiff;
        } else if (o.getSupplyStoreId() != null && this.supplyStoreId != null) {
            result = o.getSupplyStoreId().compareTo(this.supplyStoreId);
        }

        return result;

    }
}
