package com.jackrain.nea.sg.basic.model.result;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashMap;

/**
 * @author: 舒威
 * @since: 2019/7/14
 * create at : 2019/7/14 18:01
 */
@Data
public class SgGroupStorageQueryResult implements Serializable {

    /**
     * 虚拟条码id
     */
    private Long id;

    /**
     * 虚拟条码库存
     */
    private BigDecimal qtyStorage;

    /**
     * 逻辑仓
     */
    private Long cpCStoreId;

    /**
     * 实际条码库存
     */
    private HashMap<Long, BigDecimal> sku;
}
