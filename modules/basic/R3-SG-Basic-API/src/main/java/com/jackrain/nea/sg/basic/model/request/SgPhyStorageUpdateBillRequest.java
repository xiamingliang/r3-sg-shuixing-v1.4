package com.jackrain.nea.sg.basic.model.request;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/4/26 20:42
 */
@Data
public class SgPhyStorageUpdateBillRequest extends SgPhyStorageUpdateBillExt implements Serializable {

    //明细信息
    List<SgPhyStorageUpdateBillItemRequest> itemList;

    /**
     * 箱明细信息
     */
    List<SgPhyTeusStorageUpdateBillItemRequest> teusList;

}
