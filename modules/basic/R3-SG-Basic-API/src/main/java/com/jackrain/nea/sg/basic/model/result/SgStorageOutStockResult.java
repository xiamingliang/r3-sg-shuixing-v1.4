package com.jackrain.nea.sg.basic.model.result;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/3/13 15:00
 */
@Data
public class SgStorageOutStockResult implements Serializable {

    /**
     * 库存变动类型
     */
    private Integer storageType;
    /**
     * 单据类型
     */
    private Integer billType;
    /**
     * 业务节点
     */
    private Long serviceNode;
    /**
     * 单据ID
     */
    private Long billId;
    /**
     * 单据编号
     */
    private String billNo;
    /**
     * 业务单据ID
     */
    private Long sourceBillId;
    /**
     * 业务单据编号
     */
    private String sourceBillNo;
    /**
     * 单据日期
     */
    private Date billDate;
    /**
     * 单据明细ID
     */
    private Long billItemId;
    /**
     * 变动日期
     */
    private Date changeDate;
    /**
     * 店仓id
     */
    private Long cpCStoreId;
    /**
     * 店仓编码
     */
    private String cpCStoreEcode;
    /**
     * 店仓名称
     */
    private String cpCStoreEname;
    /**
     * 商品id
     */
    private Long psCProId;
    /**
     * 商品编码
     */
    private String psCProEcode;
    /**
     * 商品名称
     */
    private String psCProEname;
    /**
     * 条码id
     */
    private Long psCSkuId;
    /**
     * 条码编码
     */
    private String psCSkuEcode;
    /**
     * 规格1ID
     */
    private Long psCSpec1Id;
    /**
     * 规格1编码
     */
    private String psCSpec1Ecode;
    /**
     * 规格1名称
     */
    private String psCSpec1Ename;
    /**
     * 规格2ID
     */
    private Long psCSpec2Id;
    /**
     * 规格2编码
     */
    private String psCSpec2Ecode;
    /**
     * 规格2名称
     */
    private String psCSpec2Ename;
    /**
     * 吊牌价
     */
    private BigDecimal priceList;
    /**
     * 成本价
     */
    private BigDecimal priceCost;
    /**
     * 占用缺货数量
     */
    private BigDecimal qtyOutOfStock;
    /**
     * 变动数量
     */
    private BigDecimal qtyChange;
    /**
     * 占用变动数量
     */
    private BigDecimal qtyPreoutChange;
    /**
     * 在途变动数量
     */
    private BigDecimal qtyPreinChange;
    /**
     * 在库变动数量
     */
    private BigDecimal qtyStorageChange;

}
