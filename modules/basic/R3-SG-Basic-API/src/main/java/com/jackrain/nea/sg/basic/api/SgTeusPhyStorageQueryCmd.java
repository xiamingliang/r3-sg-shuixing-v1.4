package com.jackrain.nea.sg.basic.api;

import com.jackrain.nea.sg.basic.model.request.SgTeusPhyStorageQueryRequest;
import com.jackrain.nea.sg.basic.model.table.SgBPhyTeusStorage;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;

import java.util.List;

/**
 * @author zhu lin yu
 * @since 2019-11-01
 * create at : 2019-11-01 09:55
 */
public interface SgTeusPhyStorageQueryCmd {

    /**
     * 箱库存实体库存查询
     * @param request   查询条件
     * @param loginUser 用户
     * @return 箱库存实体库存
     */
    ValueHolderV14<List<SgBPhyTeusStorage>> queryTeusPhyStorage(SgTeusPhyStorageQueryRequest request, User loginUser);
}
