package com.jackrain.nea.sg.basic.model.request;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/4/26 20:44
 */
@Data
public class SgPhyStorageUpdateBillBase implements Serializable {

    //单据类型
    private Integer billType;
    //单据ID
    private Long billId;
    //单据编号
    private String billNo;
    /**
     * 业务单据ID
     */
    private Long sourceBillId;
    /**
     * 业务单据编号
     */
    private String sourceBillNo;
    //单据日期
    private Date billDate;
    //变动日期
    private Date changeDate;
    //是否库存取消操作
    private Boolean isCancel;

    public SgPhyStorageUpdateBillBase(){
        //库存取消操作默认为否
        this.isCancel = false;
    }

}
