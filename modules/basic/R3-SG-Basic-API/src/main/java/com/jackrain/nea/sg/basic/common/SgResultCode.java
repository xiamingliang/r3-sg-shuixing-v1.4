package com.jackrain.nea.sg.basic.common;

import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 * 基础错误编码:00001~00005
 */
public class SgResultCode extends ResultCode {
    public static String KEY_CODE = "code";
    public static String KEY_MESSAGE = "message";
    public static String KEY_DATA = "data";
    /**
     * 校验结果,执行成功返回true
     *
     * @param valueHolderV14
     * @return
     */
    public static boolean isSuccess(ValueHolderV14 valueHolderV14) {
        if (valueHolderV14 != null && valueHolderV14.getCode() == SUCCESS) {
            return true;
        }
        return false;
    }

    /**
     * 校验结果,执行失败返回true
     *
     * @param valueHolderV14
     * @return
     */
    public static boolean isFail(ValueHolderV14 valueHolderV14) {
        return !isSuccess(valueHolderV14);
    }
}
