package com.jackrain.nea.sg.basic.model.result;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/3/14 19:00
 */
@Data
public class SgStorageFtpRedisMonitorResult implements Serializable {

    /*
     * 错误初始化件数
     */
    private long errorItemQty;

    public SgStorageFtpRedisMonitorResult() {
        this.errorItemQty = 0;

    }
}
