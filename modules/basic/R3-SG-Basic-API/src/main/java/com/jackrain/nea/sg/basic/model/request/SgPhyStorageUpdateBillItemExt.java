package com.jackrain.nea.sg.basic.model.request;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/4/26 20:48
 */
@Data
public class SgPhyStorageUpdateBillItemExt extends SgPhyStorageUpdateBillItemBase implements Serializable {

    /**
     * 实体仓编码
     */
    private String cpCPhyWarehouseEcode;
    /**
     * 实体仓名称
     */
    private String cpCPhyWarehouseEname;
    /**
     * 商品id
     */
    private Long psCProId;
    /**
     * 商品编码
     */
    private String psCProEcode;
    /**
     * 商品名称
     */
    private String psCProEname;
    /**
     * 条码编码
     */
    private String psCSkuEcode;
    /**
     * 规格1ID
     */
    private Long psCSpec1Id;
    /**
     * 规格1编码
     */
    private String psCSpec1Ecode;
    /**
     * 规格1名称
     */
    private String psCSpec1Ename;
    /**
     * 规格2ID
     */
    private Long psCSpec2Id;
    /**
     * 规格2编码
     */
    private String psCSpec2Ecode;
    /**
     * 规格2名称
     */
    private String psCSpec2Ename;
    /**
     * 吊牌价
     */
    private BigDecimal priceList;
    /**
     * 成本价
     */
    private BigDecimal priceCost;
    /**
     * 国标码
     */
    private String gbcode;
    /**
     * 箱内变动数量
     */
    private BigDecimal qtyTeusChange;

    public SgPhyStorageUpdateBillItemExt() {
        this.qtyTeusChange = BigDecimal.ZERO;
    }
}
