package com.jackrain.nea.sg.basic.model.result;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author: 舒威
 * @since: 2019/7/24
 * create at : 2019/7/24 21:42
 */
@Data
public class SgGoupStorageGroupSkuResult implements Serializable {

    private Long id;

    private String ecode;

    private Long cpCStoreId;

    private Integer groupExtractNum;

    private BigDecimal minStorage;

    private BigDecimal qtyChange;

    private List<SgGoupStorageSkuResult> skuResults;

    public SgGoupStorageGroupSkuResult() {
        this.qtyChange=BigDecimal.ZERO;
    }
}
