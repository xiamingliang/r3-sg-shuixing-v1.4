package com.jackrain.nea.sg.basic.model.result;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: zyj
 * Date: 2019/7/26
 * Time: 10:23
 * Description: 库存报表
 */
@Data
public class SgRcStorageResult implements Serializable{

    @JSONField(name = "ID_LIST")
    private List<Long> idList;

    @JSONField(name = "RPT_DATE")
    private String rptDate;

    @JSONField(name = "CP_C_STORE_ECODE")
    private String storeCode;

    @JSONField(name = "CP_C_STORE_ID")
    private Long cpCStoreId;

    @JSONField(name = "CP_C_STORE_ENAME")
    private String cpCStoreEname;

    @JSONField(name = "PS_C_BRAND_ID")
    private Integer brandId;

    @JSONField(name = "PROYEAR")
    private String proyear;

    @JSONField(name = "PROSEA")
    private Integer prosea;

    @JSONField(name = "MATERIELTYPE")
    private String materieltype;

    @JSONField(name = "LARGESERIES")
    private Integer largeseries;

    @JSONField(name = "JACKET_BOTTOM")
    private String jacketBottom;

    @JSONField(name = "SMALLSERIES")
    private Integer smallseries;

    @JSONField(name = "SMALLCATE")
    private Integer smallcate;

    @JSONField(name = "PS_C_PRO_ECODE")
    private String proEcode;

    @JSONField(name = "SEX")
    private Integer sex;

    @JSONField(name = "CLRS")
    private String clrs;

    @JSONField(name = "PS_C_SKU_ECODE")
    private String skuCode;

    @JSONField(name = "GBCODE")
    private String gbcode;

    @JSONField(name = "START_DATE")
    private String startDate;

    @JSONField(name = "END_DATE")
    private String endDate;

    @JSONField(name = "LOGISTICS_STORAGE_TYPE")
    private Integer logisticsStorageType;

    private Date sdate;

    private Date edate;

    @JSONField(name = "PAGE_NUM")
    private Integer pageNum;

    @JSONField(name = "PAGE_NO")
    private Integer pageNo;

    //位移量
    private Integer offect;


    /**
     * 年份
     */
    @JSONField(name = "PROYEAR_ID")
    private Integer proyearId;
    /**
     * 季节名称
     */
    @JSONField(name = "PROSEA_ID")
    private Integer proseaId;
    /**
     * 大系列
     */
    @JSONField(name = "LARGESERIES_ID")
    private Integer largeseriesId;
    /**
     * 小系列
     */
    @JSONField(name = "SMALLSERIES_ID")
    private Integer smallseriesId;
    /**
     * 小分类
     */
    @JSONField(name = "SMALLCATE_ID")
    private Integer smallcateId;
    /**
     * 性别
     */
    @JSONField(name = "SEX_ID")
    private Integer sexId;

    @JSONField(name = "VERSION")
    private String version;
}
