package com.jackrain.nea.sg.basic.model.request;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author 舒威
 * @since 2019/7/9
 * create at : 2019/7/9 17:19
 */
@Data
public class SgGroupStorageVirtualSkuRequest implements Serializable {

    /**
     * 虚拟条码id
     */
    private Long id;

    /**
     * 虚拟条码ecode
     */
    private String ecode;

    /**
     * 渠道id
     */
    private Long cpCShopId;

    private String changeTime;

    /**
     * 单据编号
     */
    private String billNo;

    /**
     * 每組抽取数量-福袋商品
     */
    private Integer groupExtractNum;

    /**
     * 组合商品id
     */
    private Long psCProId;

    /**
     * 组合商品编码
     */
    private String psCProEcode;

    /**
     * 组合商品名称
     */
    private String psCProEname;

    /**
     * 逻辑仓id
     */
    private Long cpCStoreId;

    /**
     * 逻辑仓编码
     */
    private String cpCStoreEcode;

    /**
     * 逻辑仓名称
     */
    private String cpCStoreEname;

    /**
     * 实际条码
     */
    private List<SgGroupStorageSkuRequest> sku;

}
