package com.jackrain.nea.sg.basic.model.table;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.*;
import com.jackrain.nea.sys.domain.BaseModel;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@TableName(value = "sg_b_storage_prein_ftp")
@Data
public class SgBStoragePreinFtp extends BaseModel {
    @JSONField(name = "ID")
    @TableField(fill = FieldFill.INSERT)
    @TableId(value = "id", type = IdType.INPUT)
    private Long id;

    @JSONField(name = "BILL_TYPE")
    private Integer billType;

    @JSONField(name = "BILL_ID")
    private Long billId;

    @JSONField(name = "BILL_NO")
    private String billNo;

    @JSONField(name = "BILL_DATE")
    private Date billDate;

    @JSONField(name = "BILL_ITEM_ID")
    private Long billItemId;

    @JSONField(name = "CHANGE_DATE")
    private Date changeDate;

    @JSONField(name = "CP_C_STORE_ID")
    private Long cpCStoreId;

    @JSONField(name = "CP_C_STORE_ECODE")
    private String cpCStoreEcode;

    @JSONField(name = "CP_C_STORE_ENAME")
    private String cpCStoreEname;

    @JSONField(name = "PS_C_PRO_ID")
    private Long psCProId;

    @JSONField(name = "PS_C_PRO_ECODE")
    private String psCProEcode;

    @JSONField(name = "PS_C_PRO_ENAME")
    private String psCProEname;

    @JSONField(name = "PS_C_SKU_ID")
    private Long psCSkuId;

    @JSONField(name = "PS_C_SKU_ECODE")
    private String psCSkuEcode;

    @JSONField(name = "QTY_BEGIN")
    private BigDecimal qtyBegin;

    @JSONField(name = "QTY_END")
    private BigDecimal qtyEnd;

    @JSONField(name = "QTY_CHANGE")
    private BigDecimal qtyChange;

    @JSONField(name = "REMARK")
    private String remark;

    @JSONField(name = "LOCK_KEY")
    private String lockKey;

    @JSONField(name = "VERSION")
    private Long version;

    @JSONField(name = "OWNERENAME")
    private String ownerename;

    @JSONField(name = "MODIFIERENAME")
    private String modifierename;

    @JSONField(name = "SERVICE_NODE")
    private Long serviceNode;

    @JSONField(name = "RESERVE_BIGINT01")
    private Long reserveBigint01;

    @JSONField(name = "RESERVE_BIGINT02")
    private Long reserveBigint02;

    @JSONField(name = "RESERVE_BIGINT03")
    private Long reserveBigint03;

    @JSONField(name = "RESERVE_BIGINT04")
    private Long reserveBigint04;

    @JSONField(name = "RESERVE_BIGINT05")
    private Long reserveBigint05;

    @JSONField(name = "RESERVE_BIGINT06")
    private Long reserveBigint06;

    @JSONField(name = "RESERVE_BIGINT07")
    private Long reserveBigint07;

    @JSONField(name = "RESERVE_BIGINT08")
    private Long reserveBigint08;

    @JSONField(name = "RESERVE_BIGINT09")
    private Long reserveBigint09;

    @JSONField(name = "RESERVE_BIGINT10")
    private Long reserveBigint10;

    @JSONField(name = "RESERVE_VARCHAR01")
    private String reserveVarchar01;

    @JSONField(name = "RESERVE_VARCHAR02")
    private String reserveVarchar02;

    @JSONField(name = "RESERVE_VARCHAR03")
    private String reserveVarchar03;

    @JSONField(name = "RESERVE_VARCHAR04")
    private String reserveVarchar04;

    @JSONField(name = "RESERVE_VARCHAR05")
    private String reserveVarchar05;

    @JSONField(name = "RESERVE_VARCHAR06")
    private String reserveVarchar06;

    @JSONField(name = "RESERVE_VARCHAR07")
    private String reserveVarchar07;

    @JSONField(name = "RESERVE_VARCHAR08")
    private String reserveVarchar08;

    @JSONField(name = "RESERVE_VARCHAR09")
    private String reserveVarchar09;

    @JSONField(name = "RESERVE_VARCHAR10")
    private String reserveVarchar10;

    @JSONField(name = "RESERVE_DECIMAL01")
    private BigDecimal reserveDecimal01;

    @JSONField(name = "RESERVE_DECIMAL02")
    private BigDecimal reserveDecimal02;

    @JSONField(name = "RESERVE_DECIMAL03")
    private BigDecimal reserveDecimal03;

    @JSONField(name = "RESERVE_DECIMAL04")
    private BigDecimal reserveDecimal04;

    @JSONField(name = "RESERVE_DECIMAL05")
    private BigDecimal reserveDecimal05;

    @JSONField(name = "RESERVE_DECIMAL06")
    private BigDecimal reserveDecimal06;

    @JSONField(name = "RESERVE_DECIMAL07")
    private BigDecimal reserveDecimal07;

    @JSONField(name = "RESERVE_DECIMAL08")
    private BigDecimal reserveDecimal08;

    @JSONField(name = "RESERVE_DECIMAL09")
    private BigDecimal reserveDecimal09;

    @JSONField(name = "RESERVE_DECIMAL10")
    private BigDecimal reserveDecimal10;

    @JSONField(name = "SOURCE_BILL_NO")
    private String sourceBillNo;

    @JSONField(name = "SOURCE_BILL_ID")
    private Long sourceBillId;

    @JSONField(name = "GBCODE")
    private String gbcode;

    @JSONField(name = "PS_C_BRAND_ID")
    private Long psCBrandId;
}