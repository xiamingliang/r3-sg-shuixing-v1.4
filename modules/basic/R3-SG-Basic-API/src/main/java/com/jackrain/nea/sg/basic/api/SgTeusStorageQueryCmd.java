package com.jackrain.nea.sg.basic.api;

import com.github.pagehelper.PageInfo;
import com.jackrain.nea.sg.basic.model.request.*;
import com.jackrain.nea.sg.basic.model.table.SgBTeusStorage;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;

import java.util.List;

/**
 * @author zhu lin yu
 * @since 2019-11-01
 * create at : 2019-11-01 09:55
 */
public interface SgTeusStorageQueryCmd {

    /**
     * 箱库存查询接口
     */
    ValueHolderV14<List<SgBTeusStorage>> queryTeusStorage(SgTeusStorageQueryRequest request, User loginUser);

    /**
     * 箱库存查询接口(在库数量不等于0)
     */
    ValueHolderV14<List<SgBTeusStorage>> queryTeusStorageExclZero(SgTeusStorageQueryRequest request, User loginUser);

    /**
     * 单实体仓箱库存查询接口
     */
    ValueHolderV14<List<SgBTeusStorage>> queryTeusPhyWarehouseStorage(SgTeusPhyStorageQueryRequest request, User loginUser);

    /**
     * 单虚拟仓箱库存查询接口
     */
    ValueHolderV14<List<SgBTeusStorage>> queryStoreTeusStorage(SgTeusStoreStorageQueryRequest request, User loginUser);

    /**
     * 箱库存分页查询接口
     */
    ValueHolderV14<PageInfo<SgBTeusStorage>> queryTeusStoragePage(SgTeusStoragePageQueryRequest request, User loginUser);

    /**
     * 单实体仓箱库存分页查询接口
     */
    ValueHolderV14<PageInfo<SgBTeusStorage>> queryTeusPhyWarehouseStoragePage(SgTeusPhyStoragePageQueryRequest request, User loginUser);

    /**
     * 单虚拟仓箱库存分页查询接口
     */
    ValueHolderV14<PageInfo<SgBTeusStorage>> queryTeusStoreStoragePage(SgTeusStoreStoragePageQueryRequest request, User loginUser);

}
