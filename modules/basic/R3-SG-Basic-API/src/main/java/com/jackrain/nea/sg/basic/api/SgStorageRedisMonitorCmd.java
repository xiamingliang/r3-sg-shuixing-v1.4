package com.jackrain.nea.sg.basic.api;

import com.jackrain.nea.sg.basic.model.request.SgStorageRedisMonitorRequest;
import com.jackrain.nea.sg.basic.model.result.SgStorageRedisMonitorResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/7/10 00:00
 */
public interface SgStorageRedisMonitorCmd {

    /**
     * @Description: Redis逻辑仓库存监控接口
     * @Author: chenb
     * @Date: 2019/3/7 16:05
     * @Param: SgStorageRedisMonitorRequest Redis逻辑仓库存监控请求信息
     * @Return: ValueHolder
     */
    ValueHolderV14<SgStorageRedisMonitorResult> monitorRedisStorage(SgStorageRedisMonitorRequest request);

}
