package com.jackrain.nea.sg.basic.common;

import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.face.impl.UserImpl;

/**
 * 系统用户资源定义(完善后删除该文件)
 */
@Deprecated
public final class SystemUserResource {

    public static final Long AD_CLIENT_ID = 37L;

    public static final Long AD_ORG_ID = 27L;

    public static final String ROOT_USER_NAME = "root";

    public static final Long ROOT_USER_ID = 893L;

    private SystemUserResource() {

    }

    /**
     * 返回默认的系统Root用户
     *
     * @return 默认的系统Root用户
     */
    public static User getRootUser() {
        User user = new UserImpl();
        ((UserImpl) user).setId(ROOT_USER_ID.intValue());
        ((UserImpl) user).setName(ROOT_USER_NAME);
        ((UserImpl) user).setEname(ROOT_USER_NAME);
        ((UserImpl) user).setTruename(ROOT_USER_NAME);
        ((UserImpl) user).setClientId(AD_CLIENT_ID.intValue());
        ((UserImpl) user).setOrgId(AD_ORG_ID.intValue());
        return user;
    }
}
