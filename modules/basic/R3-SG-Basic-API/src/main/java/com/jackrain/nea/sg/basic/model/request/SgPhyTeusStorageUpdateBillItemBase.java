package com.jackrain.nea.sg.basic.model.request;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/3/17 13:30
 */
@Data
public class SgPhyTeusStorageUpdateBillItemBase implements Serializable {

    /** 库存变动类型 */
    private Integer storageType;
    /** 单据明细ID */
    private Long billItemId;
    /** 实体仓id */
    private Long cpCPhyWarehouseId;
    /** 箱id */
    private Long psCTeusId;
    /** 变动数量 */
    private BigDecimal qtyChange;

    public SgPhyTeusStorageUpdateBillItemBase(){
        this.qtyChange = BigDecimal.ZERO;
    }

}
