package com.jackrain.nea.sg.basic.api;

import com.jackrain.nea.sg.basic.model.request.SgPhyTeusStorageRedisInitRequest;
import com.jackrain.nea.sg.basic.model.result.SgPhyTeusStorageRedisInitResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/7/10 00:00
 */
public interface SgPhyTeusStorageRedisInitCmd {

    /**
     * @Description: Redis实体箱仓库存初始化接口
     * @Author: chenb
     * @Date: 2019/3/7 16:05
     * @Param: SgStorageRedisInitRequest Redis实体箱仓库存初始化请求信息
     * @Return: ValueHolder
     */
    ValueHolderV14<SgPhyTeusStorageRedisInitResult> initRedisPhyStorage(SgPhyTeusStorageRedisInitRequest request);

}
