package com.jackrain.nea.sg.basic.model.request;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/3/19 18:54
 */
@Data
public class SgStorageQueryRequest implements Serializable {

    private List<Long> storeIds;

    private List<Long> phyWarehouseIds;

    private List<String> proEcodes;

    private List<String> skuEcodes;

}
