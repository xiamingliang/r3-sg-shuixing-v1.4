package com.jackrain.nea.sg.basic.model.request;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/3/19 19:36
 */
@Data
public class SgTeusStoreStorageQueryRequest implements Serializable {

    private Long storeId;

    private List<String> proEcodes;

    private List<String> teusEcodes;
}