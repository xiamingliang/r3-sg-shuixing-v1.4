package com.jackrain.nea.sg.basic.model.request;

import com.jackrain.nea.web.face.User;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Description:根据实体仓计算其下的各逻辑仓的调整数量请求
 * @Author: chenb
 * @Date: 2019/4/26 20:47
 */
@Data
public class SgQtyStorageByWareCalcRequest implements Serializable {

    /**
     * 实体仓ID
     */
    private Long supplyPhyWarehouseId;
    /**
     * 实体仓编码
     */
    private String supplyPhyWarehouseEcode;
    /**
     * 逻辑仓ID
     */
    private Long supplyStoreId;
    /**
     * 逻辑仓编码
     */
    private String supplyStoreEcode;
    /**
     * 逻辑仓编码
     */
    private String supplyStoreEname;
    /**
     * 商品标识
     */
    private String tradeMark;
    /**
     * 可用在库是否允许负库存
     */
    private Boolean isNegativeAvailable;

    /**
     * 用户信息
     */
    private User loginUser;

    /**
     * 调整明细列表
     */
    List<SgQtyStorageByWareCalcItemRequest> itemList;

    /**
     * 调整箱明细列表
     */
    List<SgQtyTeusStorageByWareCalcItemRequest> teusList;


}
