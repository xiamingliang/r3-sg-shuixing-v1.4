package com.jackrain.nea.sg.basic.api;

import com.jackrain.nea.sg.basic.model.request.SgStorageRedisInitRequest;
import com.jackrain.nea.sg.basic.model.result.SgStorageRedisInitResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/7/10 00:00
 */
public interface SgStorageRedisInitCmd {

    /**
     * @Description: Redis库存初始化接口
     * @Author: chenb
     * @Date: 2019/3/7 16:05
     * @Param: SgStorageRedisInitRequest Redis库存初始化请求信息
     * @Return: ValueHolder
     */
    ValueHolderV14<SgStorageRedisInitResult> initRedisStorage(SgStorageRedisInitRequest request);

}
