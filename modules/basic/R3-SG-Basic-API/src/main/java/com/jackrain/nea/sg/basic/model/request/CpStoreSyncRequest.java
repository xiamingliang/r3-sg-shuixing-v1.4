package com.jackrain.nea.sg.basic.model.request;

import com.jackrain.nea.sg.basic.model.table.CpCStore;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author zhu lin yu
 * @since 2019/5/31
 * create at : 2019/5/31 15:03
 */
@Data
public class CpStoreSyncRequest implements Serializable {
    private List<CpCStore> cpCStoreList;
}
