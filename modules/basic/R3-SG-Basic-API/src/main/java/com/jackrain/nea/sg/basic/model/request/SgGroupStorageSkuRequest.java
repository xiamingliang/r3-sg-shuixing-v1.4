package com.jackrain.nea.sg.basic.model.request;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author 舒威
 * @since 2019/7/9
 * create at : 2019/7/9 20:24
 */
@Data
public class SgGroupStorageSkuRequest implements Serializable {

    /**
     * 实际条码id
     */
    private Long id;

    /**
     * 实际条码ecode
     */
    private String ecode;

    /**
     * 组合数量
     */
    private BigDecimal num;

    /**
     * 分组编号-福袋
     */
    private Integer groupnum;

    /**
     * 单个实际条码可用库存-初始化0
     */
    private BigDecimal singleStorage;

    public SgGroupStorageSkuRequest() {
        this.singleStorage = BigDecimal.ZERO;
    }
}
