package com.jackrain.nea.sg.basic.api;

import com.jackrain.nea.sg.basic.model.result.SgRcStorageResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;

/**
 * Created with IntelliJ IDEA.
 * User: zyj
 * Date: 2019/7/29
 * Time: 21:04
 * Description: 逐日库存报表导出
 */
public interface VRptTStorageExportCmd {
    /**
     * 逐日库存报表导出
     * @return
     */
    public ValueHolderV14 exportStorage(SgRcStorageResult sgRcStorageResult,  User user);
}
