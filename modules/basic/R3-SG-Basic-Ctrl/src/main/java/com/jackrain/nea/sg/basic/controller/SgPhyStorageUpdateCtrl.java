package com.jackrain.nea.sg.basic.controller;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.sg.basic.api.*;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.model.request.*;
import com.jackrain.nea.sg.basic.model.result.*;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.face.impl.UserImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author: chenbin
 * @since: 2019-01-10
 * create at : 2019-01-10 14:13
 */
@RestController
@Slf4j
@Api(value = "SgPhyStorageUpdateController", description = "实体仓库存更新接口")

public class SgPhyStorageUpdateCtrl {


    @ApiOperation(value = "通用实体仓库存更新接口(明细事务)")
    @RequestMapping(path = "/p/cs/sg/phystorage/SgPhyStorageBatchUpdateCmd", method = {RequestMethod.POST})
    public JSONObject SgPhyStorageBatchUpdateCmd(HttpServletRequest request,
                                              @RequestParam(value = "messageKey", required = true) String messageKey,
                                              @RequestParam(value = "param", required = true) String param) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgPhyStorageUpdateCtrl.SgPhyStorageBatchUpdateCmd. ReceiveParams:messageKey:{},param:{}",
                    messageKey,param);
        }

        SgPhyStorageBatchUpdateRequest updateRequest = new SgPhyStorageBatchUpdateRequest();
        JsonParser jsonParser = new JsonParser();
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd HH:mm:ss")
                .create();

        SgPhyStorageBatchUpdateCmd sgStorageBatchUpdateCmd = (SgPhyStorageBatchUpdateCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(),
                SgPhyStorageBatchUpdateCmd.class.getName(),
                SgConstantsIF.GROUP, SgConstantsIF.VERSION);

        //获取MQ消息
        JsonArray jsonParam = jsonParser.parse(param).getAsJsonArray();
        User loginUser = getRootUser();

        //MQ消息实体封装
        updateRequest.setLoginUser(loginUser);
        updateRequest.setMessageKey(messageKey);
        List<SgPhyStorageUpdateBillRequest> billList = gson.fromJson(jsonParam,
                new TypeToken<List<SgPhyStorageUpdateBillRequest>>() {
                }.getType());
        updateRequest.setBillList(billList);

        //调用通用库存批量更新接口
        ValueHolderV14<SgPhyStorageUpdateResult> callAPIResult = sgStorageBatchUpdateCmd.updateStorageBatch(updateRequest);

        JSONObject result = JSONObject.parseObject(JSONObject.toJSONString(callAPIResult.toJSONObject(),
                SerializerFeature.WriteMapNullValue));

        if (log.isDebugEnabled()) {
            log.debug("Finish SgPhyStorageUpdateCtrl.SgPhyStorageBatchUpdateCmd. ReturnResult=" + result);
        }

        return result;
    }

    @ApiOperation(value = "单个单据通用实体仓库存更新接口(带单据事务)")
    @RequestMapping(path = "/p/cs/sg/phystorage/SgPhyStorageBillTransUpdateCmd", method = {RequestMethod.POST})
    public JSONObject SgPhyStorageBillTransUpdateCmd(HttpServletRequest request,
                                                  @RequestParam(value = "messageKey", required = true) String messageKey,
                                                  @RequestParam(value = "param", required = true) String param) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgPhyStorageUpdateCtrl.SgPhyStorageBillTransUpdateCmd. ReceiveParams:messageKey:{},param:{}",
                    messageKey,param);
        }

        JsonParser jsonParser = new JsonParser();
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd HH:mm:ss")
                .create();

        //QuerySessionImpl querySession = new QuerySessionImpl(request);
        //User loginUser = querySession.getUser();
        User loginUser = getRootUser();

        SgPhyStorageBillUpdateCmd sgStorageBillUpdateCmd = (SgPhyStorageBillUpdateCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(),
                SgPhyStorageBillUpdateCmd.class.getName(),
                SgConstantsIF.GROUP, SgConstantsIF.VERSION);

        //获取MQ消息
        JsonObject jsonParam = jsonParser.parse(param).getAsJsonObject();

        SgPhyStorageSingleUpdateRequest updateRequest = gson.fromJson(jsonParam,
                new TypeToken<SgPhyStorageSingleUpdateRequest>() {
                }.getType());

        updateRequest.setMessageKey(messageKey);
        updateRequest.setLoginUser(loginUser);

        ValueHolderV14<SgPhyStorageUpdateResult> callAPIResult =
                sgStorageBillUpdateCmd.updateStorageBillWithTrans(updateRequest);

        JSONObject result = JSONObject.parseObject(JSONObject.toJSONString(callAPIResult.toJSONObject(),
                SerializerFeature.WriteMapNullValue));

        if (log.isDebugEnabled()) {
            log.debug("Finish SgPhyStorageUpdateCtrl.SgPhyStorageBillTransUpdateCmd. ReturnResult=" + result);
        }

        return result;
    }

    @ApiOperation(value = "单个单据通用实体仓库存更新接口(消费者事务)")
    @RequestMapping(path = "/p/cs/sg/phystorage/SgPhyStorageNoTransUpdateCmd", method = {RequestMethod.POST})
    public JSONObject SgPhyStorageNoTransUpdateCmd(HttpServletRequest request,
                                                @RequestParam(value = "messageKey", required = true) String messageKey,
                                                @RequestParam(value = "param", required = true) String param) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgPhyStorageUpdateCtrl.SgPhyStorageNoTransUpdateCmd. ReceiveParams:messageKey:{},param:{}",
                    messageKey,param);
        }

        JsonParser jsonParser = new JsonParser();
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd HH:mm:ss")
                .create();

        //QuerySessionImpl querySession = new QuerySessionImpl(request);
        //User loginUser = querySession.getUser();
        User loginUser = getRootUser();

        SgPhyStorageNoTransUpdateCmd sgStorageNoTransUpdateCmd = (SgPhyStorageNoTransUpdateCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(),
                SgPhyStorageNoTransUpdateCmd.class.getName(),
                SgConstantsIF.GROUP, SgConstantsIF.VERSION);

        //获取MQ消息
        JsonObject jsonParam = jsonParser.parse(param).getAsJsonObject();

        SgPhyStorageSingleUpdateRequest updateRequest = gson.fromJson(jsonParam,
                new TypeToken<SgPhyStorageSingleUpdateRequest>() {
                }.getType());

        updateRequest.setMessageKey(messageKey);
        updateRequest.setLoginUser(loginUser);

        ValueHolderV14<SgPhyStorageUpdateResult> callAPIResult =
                sgStorageNoTransUpdateCmd.updateStorageBillNoTrans(updateRequest);

        JSONObject result = JSONObject.parseObject(JSONObject.toJSONString(callAPIResult.toJSONObject(),
                SerializerFeature.WriteMapNullValue));

        if (log.isDebugEnabled()) {
            log.debug("Finish SgPhyStorageUpdateCtrl.SgPhyStorageNoTransUpdateCmd. ReturnResult=" + result);
        }

        return result;
    }

    @ApiOperation(value = "单个单据通用实体仓库存更新接口(Redis)")
    @RequestMapping(path = "/p/cs/sg/phystorage/SgPhyStorageRedisUpdateCmd", method = {RequestMethod.POST})
    public JSONObject SgPhyStorageRedisUpdateCmd(HttpServletRequest request,
                                              @RequestParam(value = "param", required = true) String param) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgPhyStorageUpdateCtrl.SgPhyStorageRedisUpdateCmd. ReceiveParams:param:{}",
                    param);
        }

        JsonParser jsonParser = new JsonParser();
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd HH:mm:ss")
                .create();

        //QuerySessionImpl querySession = new QuerySessionImpl(request);
        //User loginUser = querySession.getUser();
        User loginUser = getRootUser();

        SgPhyStorageRedisUpdateCmd sgPhyStorageRedisUpdateCmd = (SgPhyStorageRedisUpdateCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(),
                SgPhyStorageRedisUpdateCmd.class.getName(),
                SgConstantsIF.GROUP, SgConstantsIF.VERSION);

        //获取MQ消息
        JsonObject jsonParam = jsonParser.parse(param).getAsJsonObject();

        SgPhyStorageSingleUpdateRequest updateRequest = gson.fromJson(jsonParam,
                new TypeToken<SgPhyStorageSingleUpdateRequest>() {
                }.getType());

        updateRequest.setLoginUser(loginUser);

        ValueHolderV14<SgPhyStorageUpdateResult> callAPIResult =
                sgPhyStorageRedisUpdateCmd.updatePhyStorageBillRedis(updateRequest);

        JSONObject result = JSONObject.parseObject(JSONObject.toJSONString(callAPIResult.toJSONObject(),
                SerializerFeature.WriteMapNullValue));

        if (log.isDebugEnabled()) {
            log.debug("Finish SgPhyStorageUpdateCtrl.SgPhyStorageRedisUpdateCmd. ReturnResult=" + result);
        }

        return result;
    }

    @ApiOperation(value = "通用实体仓库存初始化接口(Redis)")
    @RequestMapping(path = "/p/cs/sg/phystorage/initCommonRedisPhyStorage", method = {RequestMethod.POST})
    public JSONObject initCommonRedisPhyStorage(HttpServletRequest request,
                                         @RequestParam(value = "phyWarehouseIds", required = false) String phyWarehouseIds,
                                         @RequestParam(value = "proIds", required = false) String proIds,
                                         @RequestParam(value = "skuIds", required = false) String skuIds,
                                         @RequestParam(value = "proEcodes", required = false) String proEcodes,
                                         @RequestParam(value = "skuEcodes", required = false) String skuEcodes) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgPhyStorageUpdateCtrl.initCommonRedisPhyStorage. ReceiveParams:phyWarehouseIds:{}," +
                    "proIds:{}, skuIds:{}, proEcodes:{}, skuEcodes:{}", phyWarehouseIds, proIds, skuIds, proEcodes, skuEcodes);
        }

        //QuerySessionImpl querySession = new QuerySessionImpl(request);
        //User loginUser = querySession.getUser();
        User loginUser = getRootUser();

        SgPhyStorageRedisInitCmd sgPhyStorageRedisInitCmd = (SgPhyStorageRedisInitCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(),
                SgPhyStorageRedisInitCmd.class.getName(),
                SgConstantsIF.GROUP, SgConstantsIF.VERSION);

        SgPhyStorageRedisInitRequest initRequest = new SgPhyStorageRedisInitRequest();

        initRequest.setLoginUser(loginUser);

        initRequest.setPhyWarehouseIds(JSONObject.parseArray(phyWarehouseIds, Long.class));
        initRequest.setProIds(JSONObject.parseArray(proIds, Long.class));
        initRequest.setSkuIds(JSONObject.parseArray(skuIds, Long.class));
        initRequest.setProEcodes(JSONObject.parseArray(proEcodes, String.class));
        initRequest.setSkuEcodes(JSONObject.parseArray(skuEcodes, String.class));

        ValueHolderV14<SgPhyStorageRedisInitResult> callAPIResult = sgPhyStorageRedisInitCmd.initRedisPhyStorage(initRequest);

        JSONObject result = JSONObject.parseObject(JSONObject.toJSONString(callAPIResult.toJSONObject(),
                SerializerFeature.WriteMapNullValue));

        if (log.isDebugEnabled()) {
            log.debug("Finish SgPhyStorageUpdateCtrl.initCommonRedisStorage. ReturnResult:{}", result);
        }

        return result;
    }

    @ApiOperation(value = "通用实体仓库存监控接口")
    @RequestMapping(path = "/p/cs/sg/phystorage/monitorCommonRedisPhyStorage", method = {RequestMethod.POST})
    public JSONObject monitorCommonRedisPhyStorage(HttpServletRequest request,
                                                @RequestParam(value = "phyWarehouseIds", required = false) String phyWarehouseIds,
                                                @RequestParam(value = "skuIds", required = false) String skuIds) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgPhyStorageUpdateCtrl.monitorCommonRedisPhyStorage. ReceiveParams:storeIds:{}, skuIds:{}",
                    phyWarehouseIds, skuIds);
        }

        //QuerySessionImpl querySession = new QuerySessionImpl(request);
        //User loginUser = querySession.getUser();
        User loginUser = getRootUser();

        SgPhyStorageRedisMonitorCmd sgPhyStorageRedisMonitorCmd = (SgPhyStorageRedisMonitorCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(),
                SgPhyStorageRedisMonitorCmd.class.getName(),
                SgConstantsIF.GROUP, SgConstantsIF.VERSION);

        SgPhyStorageRedisMonitorRequest initRequest = new SgPhyStorageRedisMonitorRequest();

        initRequest.setLoginUser(loginUser);

        initRequest.setPhyWarehouseIds(JSONObject.parseArray(phyWarehouseIds, Long.class));
        initRequest.setSkuIds(JSONObject.parseArray(skuIds, Long.class));

        ValueHolderV14<SgPhyStorageRedisMonitorResult> callAPIResult =
                sgPhyStorageRedisMonitorCmd.monitorRedisPhyStorage(initRequest);

        JSONObject result = JSONObject.parseObject(JSONObject.toJSONString(callAPIResult.toJSONObject(),
                SerializerFeature.WriteMapNullValue));

        if (log.isDebugEnabled()) {
            log.debug("Finish SgPhyStorageUpdateCtrl.monitorCommonRedisPhyStorage. ReturnResult:{}", result);
        }

        return result;
    }

    @ApiOperation(value = "通用实体仓库存流水监控接口")
    @RequestMapping(path = "/p/cs/sg/phystorage/monitorCommonRedisPhyStorageFtp", method = {RequestMethod.POST})
    public JSONObject monitorCommonRedisPhyStorageFtp(HttpServletRequest request) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgPhyStorageUpdateCtrl.monitorCommonRedisPhyStorageFtp. ");
        }

        //QuerySessionImpl querySession = new QuerySessionImpl(request);
        //User loginUser = querySession.getUser();
        User loginUser = getRootUser();

        SgPhyStorageFtpRedisMonitorCmd sgPhyStorageFtpRedisMonitorCmd = (SgPhyStorageFtpRedisMonitorCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(),
                SgPhyStorageFtpRedisMonitorCmd.class.getName(),
                SgConstantsIF.GROUP, SgConstantsIF.VERSION);

        SgPhyStorageFtpRedisMonitorRequest monitorRequest = new SgPhyStorageFtpRedisMonitorRequest();

        monitorRequest.setLoginUser(loginUser);

        ValueHolderV14<SgPhyStorageFtpRedisMonitorResult> callAPIResult =
                sgPhyStorageFtpRedisMonitorCmd.monitorRedisPhyStorageFtp(monitorRequest);

        JSONObject result = JSONObject.parseObject(JSONObject.toJSONString(callAPIResult.toJSONObject(),
                SerializerFeature.WriteMapNullValue));

        if (log.isDebugEnabled()) {
            log.debug("Finish SgPhyStorageUpdateCtrl.monitorCommonRedisPhyStorageFtp. ReturnResult:{}", result);
        }

        return result;
    }

    @ApiOperation(value = "通用实体仓箱库存初始化接口(Redis)")
    @RequestMapping(path = "/p/cs/sg/phystorage/initCommonRedisPhyTeusStorage", method = {RequestMethod.POST})
    public JSONObject initCommonRedisPhyTeusStorage(HttpServletRequest request,
                                                @RequestParam(value = "phyWarehouseIds", required = false) String phyWarehouseIds,
                                                @RequestParam(value = "proIds", required = false) String proIds,
                                                @RequestParam(value = "teusIds", required = false) String teusIds,
                                                @RequestParam(value = "proEcodes", required = false) String proEcodes,
                                                @RequestParam(value = "teusEcodes", required = false) String teusEcodes) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgPhyStorageUpdateCtrl.initCommonRedisPhyTeusStorage. ReceiveParams:phyWarehouseIds:{}," +
                    "proIds:{}, teusIds:{}, proEcodes:{}, teusEcodes:{}", phyWarehouseIds, proIds, teusIds, proEcodes, teusEcodes);
        }

        //QuerySessionImpl querySession = new QuerySessionImpl(request);
        //User loginUser = querySession.getUser();
        User loginUser = getRootUser();

        SgPhyTeusStorageRedisInitCmd sgPhyStorageRedisInitCmd = (SgPhyTeusStorageRedisInitCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(),
                SgPhyTeusStorageRedisInitCmd.class.getName(),
                SgConstantsIF.GROUP, SgConstantsIF.VERSION);

        SgPhyTeusStorageRedisInitRequest initRequest = new SgPhyTeusStorageRedisInitRequest();

        initRequest.setLoginUser(loginUser);

        initRequest.setPhyWarehouseIds(JSONObject.parseArray(phyWarehouseIds, Long.class));
        initRequest.setProIds(JSONObject.parseArray(proIds, Long.class));
        initRequest.setTeusIds(JSONObject.parseArray(teusIds, Long.class));
        initRequest.setProEcodes(JSONObject.parseArray(proEcodes, String.class));
        initRequest.setTeusEcodes(JSONObject.parseArray(teusEcodes, String.class));

        ValueHolderV14<SgPhyTeusStorageRedisInitResult> callAPIResult = sgPhyStorageRedisInitCmd.initRedisPhyStorage(initRequest);

        JSONObject result = JSONObject.parseObject(JSONObject.toJSONString(callAPIResult.toJSONObject(),
                SerializerFeature.WriteMapNullValue));

        if (log.isDebugEnabled()) {
            log.debug("Finish SgPhyStorageUpdateCtrl.initCommonRedisTeusStorage. ReturnResult:{}", result);
        }

        return result;
    }


    //TODO NEED DELETE
    private User getRootUser() {
        UserImpl user = new UserImpl();
        user.setId(893);
        user.setName("root");
        user.setEname("root");
        user.setActive(true);
        user.setClientId(37);
        user.setOrgId(27);
        user.setIsAdmin(2);
        user.setIsDev(2);
        return user;
    }
}
