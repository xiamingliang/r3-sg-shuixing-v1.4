package com.jackrain.nea.sg.basic.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.google.common.collect.Lists;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.redis.config.CusRedisTemplate;
import com.jackrain.nea.redis.util.RedisOpsUtil;
import com.jackrain.nea.sg.basic.api.SgGroupStorageCalculateCmd;
import com.jackrain.nea.sg.basic.api.SgGroupStorageCleanCmd;
import com.jackrain.nea.sg.basic.api.SgGroupStoragePoolCmd;
import com.jackrain.nea.sg.basic.api.SgGroupStorageQueryCmd;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.model.request.*;
import com.jackrain.nea.sg.basic.model.result.SgGoupStorageCalculateResult;
import com.jackrain.nea.sg.basic.model.result.SgGroupStorageQueryResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.face.impl.UserImpl;
import com.jackrain.nea.web.query.DefaultWebEvent;
import com.jackrain.nea.web.query.QuerySessionImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.MapUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @author: 舒威
 * @since: 2019/7/14
 * create at : 2019/7/14 15:49
 */
@RestController
@Slf4j
@Api(value = "SgGroupStorageCtrl", description = "组合商品库存计算接口")
public class SgGroupStorageCtrl {

    @ApiOperation(value = "福袋启用-库存池")
    @PostMapping(path = "/p/cs/sg/combinedCommodityPool")
    public JSONObject groupStoragePool(@RequestBody SgGroupStoragePoolRequest param) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgGroupStorageCtrl.groupStoragePool. ReceiveParams:param:{}, ", param);
        }

        SgGroupStoragePoolCmd groupStoragePoolCmd = (SgGroupStoragePoolCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(),
                SgGroupStoragePoolCmd.class.getName(),
                SgConstantsIF.GROUP, SgConstantsIF.VERSION);

        param.setLoginUser(getRootUser());
        ValueHolderV14<SgGoupStorageCalculateResult> result = groupStoragePoolCmd.addGroupStoragePoll(param);
        JSONObject var = JSONObject.parseObject(JSONObject.toJSONString(result.toJSONObject(),
                SerializerFeature.WriteMapNullValue));
        if (log.isDebugEnabled()) {
            log.debug("Finish SgGroupStorageCtrl.groupStoragePool. ReturnResult=" + result);
        }

        return var;
    }

    @ApiOperation(value = "福袋启用-库存计算服务")
    @PostMapping(path = "/p/cs/sg/updateCombinedCommodity")
    public JSONObject updateGroupStorage(@RequestBody List<SgGroupStorageVirtualSkuRequest> requests) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgGroupStorageCtrl.updateGroupStorage. ReceiveParams:param:{}, ", requests);
        }

        SgGroupStorageCalculateCmd calculateCmd = (SgGroupStorageCalculateCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(),
                SgGroupStorageCalculateCmd.class.getName(),
                SgConstantsIF.GROUP, SgConstantsIF.VERSION);

        SgGroupStorageCalculateRequest model = new SgGroupStorageCalculateRequest();
        model.setVirtualSkuList(requests);
        model.setLoginUser(getRootUser());
        ValueHolderV14<SgGoupStorageCalculateResult> result = calculateCmd.calculateGroupStorage(model);
        JSONObject var = JSONObject.parseObject(JSONObject.toJSONString(result.toJSONObject(),
                SerializerFeature.WriteMapNullValue));
        if (log.isDebugEnabled()) {
            log.debug("Finish SgGroupStorageCtrl.updateGroupStorage. ReturnResult=" + result);
        }

        return var;
    }

    @ApiOperation(value = "组合福袋库存计算-虚拟条码推送Redis(自测用)")
    @PostMapping(path = "/p/cs/sg/pushGroupRedis")
    public String pushGroupRedis(@RequestBody JSONArray param) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgGroupStorageCtrl.pushGroupRedis. ReceiveParams:param:{}, ", param);
        }

        CusRedisTemplate<String, String> redisTemplate = RedisOpsUtil.getStrRedisTemplate();
        for (Object o : param) {
            JSONObject sku_group = (JSONObject) o;
            Long skuId = sku_group.getLong("ID");
            String storeId = sku_group.getString("CP_C_STORE_ID");
            String key = SgConstantsIF.COMBINED_SKUGROUP + ":" + skuId;
            redisTemplate.opsForHash().put(key, storeId, sku_group.toJSONString());
        }
        return "success";
    }

    @ApiOperation(value = "组合福袋库存计算-实际条码推送Redis(自测用)")
    @PostMapping(path = "/p/cs/sg/pushRedis")
    public String pushRedis(@RequestBody JSONArray param) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgGroupStorageCtrl.pushRedis. ReceiveParams:param:{}, ", param);
        }

        CusRedisTemplate<String, String> redisTemplate = RedisOpsUtil.getStrRedisTemplate();
        for (Object o : param) {
            JSONObject sku_group = (JSONObject) o;
            Long skuId = sku_group.getLong("ID");
            Long storeId = sku_group.getLong("CP_C_STORE_ID");
            String key = SgConstantsIF.COMBINED_SKU + ":" + skuId;
            redisTemplate.opsForHash().put(key, storeId.toString(), sku_group.toJSONString());
        }
        return "success";
    }

    @ApiOperation(value = "福袋生成-获取福袋库存数量、实际条码库存数量")
    @PostMapping(path = "/p/cs/sg/queryCombinedCommodity")
    public JSONObject queryGroupStorage(HttpServletRequest request, @RequestParam(value = "param", required = false) String param) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgGroupStorageCtrl.queryGroupStorage. ReceiveParams:param:{}, ", param);
        }

        SgGroupStorageQueryCmd storageQueryCmd = (SgGroupStorageQueryCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(),
                SgGroupStorageQueryCmd.class.getName(),
                SgConstantsIF.GROUP, SgConstantsIF.VERSION);

        List<SgGroupStorageQueryRequest> requests = JSONArray.parseArray(param, SgGroupStorageQueryRequest.class);
        ValueHolderV14<List<SgGroupStorageQueryResult>> result = storageQueryCmd.queryGroupStorage(requests);
        JSONObject var = JSONObject.parseObject(JSONObject.toJSONString(result.toJSONObject(),
                SerializerFeature.WriteMapNullValue));
        if (log.isDebugEnabled()) {
            log.debug("Finish SgGroupStorageCtrl.queryGroupStorage. ReturnResult=" + result);
        }

        return var;
    }

    @ApiOperation(value = "组合商品停用-清空库存")
    @PostMapping(path = "/p/cs/sg/cleanCombinedCommodity")
    public JSONObject cleanGroupStorage(@RequestBody SgGroupStorageCleanRequest request) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgGroupStorageCtrl.cleanGroupStorage. ReceiveParams:param:{}, ", JSONObject.toJSONString(request));
        }

        //QuerySessionImpl querySession = new QuerySessionImpl(request);
        //User loginUser = querySession.getUser();
        User loginUser = getRootUser();
        request.setLoginUser(loginUser);

        SgGroupStorageCleanCmd storageCleanCmd = (SgGroupStorageCleanCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(),
                SgGroupStorageCleanCmd.class.getName(),
                SgConstantsIF.GROUP, SgConstantsIF.VERSION);

        ValueHolderV14<Integer> result = storageCleanCmd.cleanGroupStorage(request);

        if (log.isDebugEnabled()) {
            log.debug("Finish SgGroupStorageCtrl.cleanGroupStorage. ReturnResult=" + result.toJSONObject());
        }

        return result.toJSONObject();
    }


    @ApiOperation(value = "福袋库存计算")
    @PostMapping(path = "/p/cs/sg/calculateLuckyProMinStorage")
    public BigDecimal calculateLuckyProMinStorage(@RequestBody Map map) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgGroupStorageCtrl.calculateLuckyProMinStorage. ReceiveParams:param:{}, ", map);
        }

        SgGroupStorageCalculateCmd calculateCmd = (SgGroupStorageCalculateCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(),
                SgGroupStorageCalculateCmd.class.getName(),
                SgConstantsIF.GROUP, SgConstantsIF.VERSION);
        List<SgGroupStorageSkuRequest> skus = Lists.newArrayList();
        Integer num = null;
        if (MapUtils.isNotEmpty(map)) {
            skus = JSONArray.parseArray(JSONObject.toJSONString(map.get("sku")), SgGroupStorageSkuRequest.class);
            num = (Integer) map.get("num");
        }
        BigDecimal storage = calculateCmd.calculateLuckyProMinStorage(skus, num);
        if (log.isDebugEnabled()) {
            log.debug("Finish SgGroupStorageCtrl.calculateLuckyProMinStorage. ReturnResult=" + storage);
        }
        return storage;
    }

    @ApiOperation(value = "组合商品库存查询")
    @PostMapping(path = "/p/cs/sg/storage/queryGroupStorage")
    public JSONObject queryCombinedGoods(HttpServletRequest request, @RequestBody JSONObject param) {
        if (log.isDebugEnabled()) {
            log.debug("Start SgGroupStorageCtrl.queryGroupStorage. ReceiveParams:param=" + param + ";");
        }
        User user = (User) request.getSession().getAttribute("user");
        QuerySessionImpl querySession = new QuerySessionImpl(user);
        DefaultWebEvent event = new DefaultWebEvent("queryGroupStorage", request, false);
        event.put("param", param);
        querySession.setEvent(event);
        SgGroupStorageQueryCmd sgGroupStorageQueryCmd = (SgGroupStorageQueryCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(),
                SgGroupStorageQueryCmd.class.getName(),
                SgConstantsIF.GROUP, SgConstantsIF.VERSION);
        ValueHolderV14<List<SgGroupQueryRequest>> callAPIResult = sgGroupStorageQueryCmd.queryGroupStorageById(querySession);

        JSONObject result = JSONObject.parseObject(JSONObject.toJSONString(callAPIResult.toJSONObject(),
                SerializerFeature.WriteMapNullValue));

        if (log.isDebugEnabled()) {
            log.debug("Finish SgGroupStorageCtrl.queryGroupStorage. ReturnResult=" + result);
        }
        return result;
    }
    private User getRootUser() {
        UserImpl user = new UserImpl();
        user.setId(893);
        user.setName("root");
        user.setEname("root");
        user.setActive(true);
        user.setClientId(37);
        user.setOrgId(27);
        user.setIsAdmin(2);
        user.setIsDev(2);
        return user;
    }
}
