package com.jackrain.nea.sg.basic.controller;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.sg.basic.api.SgPhyStorageQueryCmd;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.model.request.SgPhyStorageQueryRequest;
import com.jackrain.nea.sg.basic.model.table.SgBPhyStorage;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.face.impl.UserImpl;
import com.jackrain.nea.web.query.QuerySessionImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/4/26 13:14
 */
@RestController
@Slf4j
@Api(value = "SgPhyStorageQueryController", description = "实体仓库存查询接口")
public class SgPhyStorageQueryCtrl {

    @ApiOperation(value = "商品编码实体仓库存查询接口")
    @RequestMapping(path = "/p/cs/sg/phystorage/queryPhyStorageByPro", method = {RequestMethod.POST})
    public JSONObject queryPhyStorageByPro(HttpServletRequest request,
                                        @RequestParam(value = "phyWarehouseId", required = true) Long phyWarehouseId,
                                        @RequestParam(value = "proEcode", required = true) String proEcode) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgPhyStorageQueryCtrl.queryPhyStorageByPro. ReceiveParams:phyWarehouseId=" + phyWarehouseId +
                    " proEcode=" + proEcode + ";");
        }

        QuerySessionImpl querySession = new QuerySessionImpl(request);
        User loginUser = querySession.getUser();
        //User loginUser = getRootUser();

        SgPhyStorageQueryCmd sgPhyStorageQueryCmd = (SgPhyStorageQueryCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(),
                SgPhyStorageQueryCmd.class.getName(),
                SgConstantsIF.GROUP, SgConstantsIF.VERSION);

        SgPhyStorageQueryRequest queryRequest = new SgPhyStorageQueryRequest();
        List<Long> phyWarehouseIds = new ArrayList<>();
        List<String> proEcodes = new ArrayList<>();
        phyWarehouseIds.add(phyWarehouseId);
        proEcodes.add(proEcode);
        queryRequest.setPhyWarehouseIds(phyWarehouseIds);
        queryRequest.setProEcodes(proEcodes);

        ValueHolderV14<List<SgBPhyStorage>> callAPIResult = sgPhyStorageQueryCmd.queryPhyStorage(queryRequest, loginUser);

        JSONObject result = JSONObject.parseObject(JSONObject.toJSONString(callAPIResult.toJSONObject(),
                SerializerFeature.WriteMapNullValue));

        if (log.isDebugEnabled()) {
            log.debug("Finish SgPhyStorageQueryCtrl.queryPhyStorageByPro. ReturnResult=" + result);
        }

        return result;
    }

    @ApiOperation(value = "实体仓库存查询通用接口")
    @RequestMapping(path = "/p/cs/sg/phystorage/queryCommonPhyStorage", method = {RequestMethod.POST})
    public JSONObject queryCommonPhyStorage(HttpServletRequest request,
                                             @RequestParam(value = "phyWarehouseIds", required = false) String phyWarehouseIds,
                                             @RequestParam(value = "proEcodes", required = false) String proEcodes,
                                             @RequestParam(value = "skuEcodes", required = false) String skuEcodes) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgPhyStorageQueryCtrl.queryCommonPhyStorage. ReceiveParams:phyWarehouseIds:{}, " +
                    "proEcodes:{}, skuEcodes:{}", phyWarehouseIds, proEcodes, skuEcodes);
        }

        //QuerySessionImpl querySession = new QuerySessionImpl(request);
        //User loginUser = querySession.getUser();
        User loginUser = getRootUser();

        SgPhyStorageQueryCmd sgPhyStorageQueryCmd = (SgPhyStorageQueryCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(),
                SgPhyStorageQueryCmd.class.getName(),
                SgConstantsIF.GROUP, SgConstantsIF.VERSION);

        SgPhyStorageQueryRequest queryRequest = new SgPhyStorageQueryRequest();

        queryRequest.setPhyWarehouseIds(JSONObject.parseArray(phyWarehouseIds, Long.class));
        queryRequest.setProEcodes(JSONObject.parseArray(proEcodes, String.class));
        queryRequest.setSkuEcodes(JSONObject.parseArray(skuEcodes, String.class));

        ValueHolderV14<List<SgBPhyStorage>> callAPIResult = sgPhyStorageQueryCmd.queryPhyStorage(queryRequest, loginUser);

        JSONObject result = JSONObject.parseObject(JSONObject.toJSONString(callAPIResult.toJSONObject(),
                SerializerFeature.WriteMapNullValue));

        if (log.isDebugEnabled()) {
            log.debug("Finish SgPhyStorageQueryCtrl.queryCommonPhyStorage. ReturnResult=" + result);
        }

        return result;
    }

    //TODO NEED DELETE
    private User getRootUser() {
        UserImpl user = new UserImpl();
        user.setId(893);
        user.setName("root");
        user.setEname("root");
        user.setActive(true);
        user.setClientId(37);
        user.setOrgId(27);
        user.setIsAdmin(2);
        user.setIsDev(2);
        return user;
    }
}
