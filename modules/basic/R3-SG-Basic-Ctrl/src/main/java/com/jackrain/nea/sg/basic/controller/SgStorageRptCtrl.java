package com.jackrain.nea.sg.basic.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.sg.basic.api.VRptTStorageExportCmd;
import com.jackrain.nea.sg.basic.api.VRptTStorageSkuGenarateBatchNoCmd;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.model.result.SgRcStoragePageResult;
import com.jackrain.nea.sg.basic.model.result.SgRcStorageQueryResult;
import com.jackrain.nea.sg.basic.model.result.SgRcStorageResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.query.QuerySessionImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: zyj
 * Date: 2019/7/23
 * Time: 19:11
 * Description: 库存报表查询
 */
@RestController
@Slf4j
@Api(value = "SgStorageQueryController", description = "库存查询接口")
public class SgStorageRptCtrl {
    private SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");


    @ApiOperation(value = "商品期末库存批次号查询接口")
    @RequestMapping(path = "/p/cs/storage/report/queryStorageSku", method = {RequestMethod.POST})
    public ValueHolderV14 queryPhyWarehouseStorage(HttpServletRequest request,
                                                   @RequestBody SgRcStorageResult sgRcStorageResult
                                                    ) {
        if(log.isDebugEnabled()){
            log.debug(this.getClass().getName()+" 商品库存批次号查询"+JSONObject.toJSONString(sgRcStorageResult));
        }
        VRptTStorageSkuGenarateBatchNoCmd vRptTStorageSkuGenarateBatchNoCmd = (VRptTStorageSkuGenarateBatchNoCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(),
                VRptTStorageSkuGenarateBatchNoCmd.class.getName(),
                SgConstantsIF.GROUP, SgConstantsIF.VERSION);
        ValueHolderV14 valueHolderV14 = vRptTStorageSkuGenarateBatchNoCmd.queryBatchNoForStorageSku(sgRcStorageResult);
        return valueHolderV14;
    }
    @ApiOperation(value = "逐日库存批次号查询接口")
    @RequestMapping(path = "/p/cs/storage/report/queryDayByDayStorage", method = {RequestMethod.POST})
    public ValueHolderV14 queryDayByDayStorage(HttpServletRequest request,
                                               @RequestBody SgRcStorageResult sgRcStorageResult) {
        if(log.isDebugEnabled()){
            log.debug(this.getClass().getName()+" 逐日库存批次号查询"+JSONObject.toJSONString(sgRcStorageResult));
        }
        VRptTStorageSkuGenarateBatchNoCmd vRptTStorageSkuGenarateBatchNoCmd = (VRptTStorageSkuGenarateBatchNoCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(),
                VRptTStorageSkuGenarateBatchNoCmd.class.getName(),
                SgConstantsIF.GROUP, SgConstantsIF.VERSION);
        if (StringUtils.isNotEmpty(sgRcStorageResult.getStartDate())) {
            String startDate = sgRcStorageResult.getStartDate();
            sgRcStorageResult.setStartDate(startDate.replace("-", ""));
        }
        if (StringUtils.isNotEmpty(sgRcStorageResult.getEndDate())) {
            String endDate = sgRcStorageResult.getEndDate();
            sgRcStorageResult.setEndDate(endDate.replace("-", ""));
        }
        ValueHolderV14 valueHolderV14 = vRptTStorageSkuGenarateBatchNoCmd.queryBatchNoForStorage(sgRcStorageResult);
        return valueHolderV14;
    }

    @ApiOperation(value = "逐日库存查询接口")
    @RequestMapping(path = "/p/cs/storage/report/queryDataStorage", method = {RequestMethod.POST})
    public ValueHolderV14 queryDataStorage(HttpServletRequest request,
                                           @RequestBody SgRcStorageResult sgRcStorageResult) {
        ValueHolderV14 valueHolderV14 = new ValueHolderV14();
        if(log.isDebugEnabled()){
            log.debug(this.getClass().getName()+" 逐日库存查询"+JSONObject.toJSONString(sgRcStorageResult));
        }
        VRptTStorageSkuGenarateBatchNoCmd vRptTStorageSkuGenarateBatchNoCmd = (VRptTStorageSkuGenarateBatchNoCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(),
                VRptTStorageSkuGenarateBatchNoCmd.class.getName(),
                SgConstantsIF.GROUP, SgConstantsIF.VERSION);
        try{
            if(StringUtils.isNotEmpty(sgRcStorageResult.getStartDate())){
                sgRcStorageResult.setSdate(sf.parse(sgRcStorageResult.getStartDate()));
            }
            if(StringUtils.isNotEmpty(sgRcStorageResult.getEndDate())){
                sgRcStorageResult.setEdate(sf.parse(sgRcStorageResult.getEndDate()));
            }
        }catch (ParseException e) {
            log.error(this.getClass().getName()+" 日期格式转化出错！！！",e);
        }
        //分页
        sgRcStorageResult.setOffect((sgRcStorageResult.getPageNo()-1)*sgRcStorageResult.getPageNum());
        //获取总数
        Integer totalSize = vRptTStorageSkuGenarateBatchNoCmd.countForStorage(sgRcStorageResult);
        if(log.isDebugEnabled()){
            log.debug(this.getClass().getName()+" 逐日库存查询返回条数："+totalSize);
        }
        if(totalSize == null){
            SgRcStoragePageResult sgRcStoragePageResult = new SgRcStoragePageResult();
            sgRcStoragePageResult.setSgRcStorageQueryResultList(new ArrayList<>());
            sgRcStoragePageResult.setTotalSize(0);
            valueHolderV14.setData(sgRcStoragePageResult);
            return valueHolderV14;
        }
        SgRcStoragePageResult sgRcStoragePageResult = new SgRcStoragePageResult();
        sgRcStoragePageResult.setTotalSize(totalSize);
        List<SgRcStorageQueryResult> sgRcStorageQueryResultList = vRptTStorageSkuGenarateBatchNoCmd.queryDataForStorage(sgRcStorageResult);
        if(CollectionUtils.isNotEmpty(sgRcStorageQueryResultList)){
            if(log.isDebugEnabled()){
                log.debug(this.getClass().getName()+" 库存逐日查询:"+ JSONArray.toJSONString(sgRcStorageQueryResultList));
            }
        }
        sgRcStoragePageResult.setSgRcStorageQueryResultList(sgRcStorageQueryResultList);
        valueHolderV14.setData(sgRcStoragePageResult);
        return valueHolderV14;
    }

    @ApiOperation(value = "逐日库存报表导出接口")
    @RequestMapping(path = "/p/cs/storage/report/exportDataForStorage", method = {RequestMethod.POST})
    public ValueHolderV14 exportDataForStorage(HttpServletRequest request,
                                           @RequestBody SgRcStorageResult sgRcStorageResult) {

        QuerySessionImpl querySession = new QuerySessionImpl(request);
        User loginUser = querySession.getUser();
        if(log.isDebugEnabled()){
            log.debug(this.getClass().getName()+" 逐日库存报表导出"+JSONObject.toJSONString(sgRcStorageResult));
        }
        VRptTStorageExportCmd vRptTStorageExportCmd = (VRptTStorageExportCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(),
                VRptTStorageExportCmd.class.getName(),
                SgConstantsIF.GROUP, SgConstantsIF.VERSION);
        try{
            if(StringUtils.isNotEmpty(sgRcStorageResult.getStartDate())){
                sgRcStorageResult.setSdate(sf.parse(sgRcStorageResult.getStartDate()));
            }
            if(StringUtils.isNotEmpty(sgRcStorageResult.getEndDate())){
                sgRcStorageResult.setEdate(sf.parse(sgRcStorageResult.getEndDate()));
            }
        }catch (ParseException e) {
            log.error(this.getClass().getName()+" 日期格式转化出错！！！",e);
        }
        ValueHolderV14 valueHolderV14 = vRptTStorageExportCmd.exportStorage(sgRcStorageResult, loginUser);
        return valueHolderV14;
    }
}
