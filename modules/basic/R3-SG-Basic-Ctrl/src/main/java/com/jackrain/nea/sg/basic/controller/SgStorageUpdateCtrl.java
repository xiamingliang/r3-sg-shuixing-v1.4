package com.jackrain.nea.sg.basic.controller;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.sg.basic.api.*;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.model.request.*;
import com.jackrain.nea.sg.basic.model.result.*;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.face.impl.UserImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author: chenbin
 * @since: 2019-01-10
 * create at : 2019-01-10 14:13
 */
@RestController
@Slf4j
@Api(value = "SgStorageUpdateController", description = "逻辑仓库存更新接口")
public class SgStorageUpdateCtrl {


    @ApiOperation(value = "通用逻辑仓库存更新接口(明细事务)")
    @RequestMapping(path = "/p/cs/sg/storage/SgStorageBatchUpdateCmd", method = {RequestMethod.POST})
    public JSONObject SgStorageBatchUpdateCmd(HttpServletRequest request,
                                              @RequestParam(value = "messageKey", required = true) String messageKey,
                                              @RequestParam(value = "param", required = true) String param) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgStorageUpdateCtrl.SgStorageBatchUpdateCmd. ReceiveParams:messageKey:{},param:{}",
                    messageKey, param);
        }

        SgStorageBatchUpdateRequest updateRequest = new SgStorageBatchUpdateRequest();
        JsonParser jsonParser = new JsonParser();
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd HH:mm:ss")
                .create();

        SgStorageBatchUpdateCmd sgStorageBatchUpdateCmd = (SgStorageBatchUpdateCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(),
                SgStorageBatchUpdateCmd.class.getName(),
                SgConstantsIF.GROUP, SgConstantsIF.VERSION);

        //获取MQ消息
        JsonArray jsonParam = jsonParser.parse(param).getAsJsonArray();
        User loginUser = getRootUser();

        //MQ消息实体封装
        updateRequest.setLoginUser(loginUser);
        updateRequest.setMessageKey(messageKey);
        List<SgStorageUpdateBillRequest> billList = gson.fromJson(jsonParam,
                new TypeToken<List<SgStorageUpdateBillRequest>>() {
                }.getType());
        updateRequest.setBillList(billList);

        //调用通用库存批量更新接口
        ValueHolderV14<SgStorageUpdateResult> callAPIResult = sgStorageBatchUpdateCmd.updateStorageBatch(updateRequest);

        JSONObject result = JSONObject.parseObject(JSONObject.toJSONString(callAPIResult.toJSONObject(),
                SerializerFeature.WriteMapNullValue));

        if (log.isDebugEnabled()) {
            log.debug("Finish SgStorageUpdateCtrl.SgStorageBatchUpdateCmd. ReturnResult=" + result);
        }

        return result;
    }

    @ApiOperation(value = "单个单据通用逻辑仓库存更新接口(带单据事务)")
    @RequestMapping(path = "/p/cs/sg/storage/SgStorageBillTransUpdateCmd", method = {RequestMethod.POST})
    public JSONObject SgStorageBillTransUpdateCmd(HttpServletRequest request,
                                                  @RequestParam(value = "messageKey", required = true) String messageKey,
                                                  @RequestParam(value = "param", required = true) String param) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgStorageUpdateCtrl.SgStorageBillTransUpdateCmd. ReceiveParams:messageKey:{},param:{}",
                    messageKey, param);
        }

        JsonParser jsonParser = new JsonParser();
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd HH:mm:ss")
                .create();

        //QuerySessionImpl querySession = new QuerySessionImpl(request);
        //User loginUser = querySession.getUser();
        User loginUser = getRootUser();

        SgStorageBillUpdateCmd sgStorageBillUpdateCmd = (SgStorageBillUpdateCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(),
                SgStorageBillUpdateCmd.class.getName(),
                SgConstantsIF.GROUP, SgConstantsIF.VERSION);

        //获取MQ消息
        JsonObject jsonParam = jsonParser.parse(param).getAsJsonObject();

        SgStorageSingleUpdateRequest updateRequest = gson.fromJson(jsonParam,
                new TypeToken<SgStorageSingleUpdateRequest>() {
                }.getType());

        updateRequest.setMessageKey(messageKey);
        updateRequest.setLoginUser(loginUser);

        ValueHolderV14<SgStorageUpdateResult> callAPIResult =
                sgStorageBillUpdateCmd.updateStorageBillWithTrans(updateRequest);

        JSONObject result = JSONObject.parseObject(JSONObject.toJSONString(callAPIResult.toJSONObject(),
                SerializerFeature.WriteMapNullValue));

        if (log.isDebugEnabled()) {
            log.debug("Finish SgStorageUpdateCtrl.SgStorageBillTransUpdateCmd. ReturnResult=" + result);
        }

        return result;
    }

    @ApiOperation(value = "单个单据通用逻辑仓库存更新接口(消费者事务)")
    @RequestMapping(path = "/p/cs/sg/storage/SgStorageNoTransUpdateCmd", method = {RequestMethod.POST})
    public JSONObject SgStorageNoTransUpdateCmd(HttpServletRequest request,
                                                @RequestParam(value = "messageKey", required = true) String messageKey,
                                                @RequestParam(value = "param", required = true) String param) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgStorageUpdateCtrl.SgStorageNoTransUpdateCmd. ReceiveParams:messageKey:{},param:{}",
                    messageKey, param);
        }

        JsonParser jsonParser = new JsonParser();
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd HH:mm:ss")
                .create();

        //QuerySessionImpl querySession = new QuerySessionImpl(request);
        //User loginUser = querySession.getUser();
        User loginUser = getRootUser();

        SgStorageNoTransUpdateCmd sgStorageNoTransUpdateCmd = (SgStorageNoTransUpdateCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(),
                SgStorageNoTransUpdateCmd.class.getName(),
                SgConstantsIF.GROUP, SgConstantsIF.VERSION);

        //获取MQ消息
        JsonObject jsonParam = jsonParser.parse(param).getAsJsonObject();

        SgStorageSingleUpdateRequest updateRequest = gson.fromJson(jsonParam,
                new TypeToken<SgStorageSingleUpdateRequest>() {
                }.getType());

        updateRequest.setMessageKey(messageKey);
        updateRequest.setLoginUser(loginUser);

        ValueHolderV14<SgStorageUpdateResult> callAPIResult =
                sgStorageNoTransUpdateCmd.updateStorageBillNoTrans(updateRequest);

        JSONObject result = JSONObject.parseObject(JSONObject.toJSONString(callAPIResult.toJSONObject(),
                SerializerFeature.WriteMapNullValue));

        if (log.isDebugEnabled()) {
            log.debug("Finish SgStorageUpdateCtrl.SgStorageNoTransUpdateCmd. ReturnResult=" + result);
        }

        return result;
    }

    @ApiOperation(value = "单个单据通用逻辑仓库存更新接口(Redis)")
    @RequestMapping(path = "/p/cs/sg/storage/SgStorageRedisUpdateCmd", method = {RequestMethod.POST})
    public JSONObject SgStorageRedisUpdateCmd(HttpServletRequest request,
                                              @RequestParam(value = "param", required = true) String param) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgStorageUpdateCtrl.SgStorageRedisUpdateCmd. ReceiveParams:param:{}",
                    param);
        }

        JsonParser jsonParser = new JsonParser();
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd HH:mm:ss")
                .create();

        //QuerySessionImpl querySession = new QuerySessionImpl(request);
        //User loginUser = querySession.getUser();
        User loginUser = getRootUser();

        SgStorageRedisUpdateCmd sgStorageRedisUpdateCmd = (SgStorageRedisUpdateCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(),
                SgStorageRedisUpdateCmd.class.getName(),
                SgConstantsIF.GROUP, SgConstantsIF.VERSION);

        //获取MQ消息
        JsonObject jsonParam = jsonParser.parse(param).getAsJsonObject();

        SgStorageSingleUpdateRequest updateRequest = gson.fromJson(jsonParam,
                new TypeToken<SgStorageSingleUpdateRequest>() {
                }.getType());

        updateRequest.setLoginUser(loginUser);

        ValueHolderV14<SgStorageUpdateResult> callAPIResult =
                sgStorageRedisUpdateCmd.updateStorageBillRedis(updateRequest);

        JSONObject result = JSONObject.parseObject(JSONObject.toJSONString(callAPIResult.toJSONObject(),
                SerializerFeature.WriteMapNullValue));

        if (log.isDebugEnabled()) {
            log.debug("Finish SgStorageUpdateCtrl.SgStorageRedisUpdateCmd. ReturnResult=" + result);
        }

        return result;
    }

    @ApiOperation(value = "通用逻辑仓库存初始化接口(Redis)")
    @RequestMapping(path = "/p/cs/sg/storage/initCommonRedisStorage", method = {RequestMethod.POST})
    public JSONObject initCommonRedisStorage(HttpServletRequest request,
                                        @RequestParam(value = "storeIds", required = false) String storeIds,
                                        @RequestParam(value = "proIds", required = false) String proIds,
                                        @RequestParam(value = "skuIds", required = false) String skuIds,
                                        @RequestParam(value = "proEcodes", required = false) String proEcodes,
                                        @RequestParam(value = "skuEcodes", required = false) String skuEcodes) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgStorageUpdateCtrl.initCommonRedisStorage. ReceiveParams:storeIds:{}," +
                    "proIds:{}, skuIds:{}, proEcodes:{}, skuEcodes:{}", storeIds, proIds, skuIds, proEcodes, skuEcodes);
        }

        //QuerySessionImpl querySession = new QuerySessionImpl(request);
        //User loginUser = querySession.getUser();
        User loginUser = getRootUser();

        SgStorageRedisInitCmd sgStorageRedisInitCmd = (SgStorageRedisInitCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(),
                SgStorageRedisInitCmd.class.getName(),
                SgConstantsIF.GROUP, SgConstantsIF.VERSION);

        SgStorageRedisInitRequest initRequest = new SgStorageRedisInitRequest();

        initRequest.setLoginUser(loginUser);

        initRequest.setStoreIds(JSONObject.parseArray(storeIds, Long.class));
        initRequest.setProIds(JSONObject.parseArray(proIds, Long.class));
        initRequest.setSkuIds(JSONObject.parseArray(skuIds, Long.class));
        initRequest.setProEcodes(JSONObject.parseArray(proEcodes, String.class));
        initRequest.setSkuEcodes(JSONObject.parseArray(skuEcodes, String.class));

        ValueHolderV14<SgStorageRedisInitResult> callAPIResult = sgStorageRedisInitCmd.initRedisStorage(initRequest);

        JSONObject result = JSONObject.parseObject(JSONObject.toJSONString(callAPIResult.toJSONObject(),
                SerializerFeature.WriteMapNullValue));

        if (log.isDebugEnabled()) {
            log.debug("Finish SgStorageUpdateCtrl.initCommonRedisStorage. ReturnResult:{}", result);
        }

        return result;
    }

    @ApiOperation(value = "通用逻辑仓库存监控接口")
    @RequestMapping(path = "/p/cs/sg/storage/monitorCommonRedisStorage", method = {RequestMethod.POST})
    public JSONObject monitorCommonRedisStorage(HttpServletRequest request,
                                                @RequestParam(value = "storeIds", required = false) String storeIds,
                                                @RequestParam(value = "skuIds", required = false) String skuIds) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgStorageUpdateCtrl.monitorCommonRedisStorage. ReceiveParams:storeIds:{}, skuIds:{}",
                    storeIds, skuIds);
        }

        //QuerySessionImpl querySession = new QuerySessionImpl(request);
        //User loginUser = querySession.getUser();
        User loginUser = getRootUser();

        SgStorageRedisMonitorCmd sgStorageRedisMonitorCmd = (SgStorageRedisMonitorCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(),
                SgStorageRedisMonitorCmd.class.getName(),
                SgConstantsIF.GROUP, SgConstantsIF.VERSION);

        SgStorageRedisMonitorRequest monitorRequest = new SgStorageRedisMonitorRequest();

        monitorRequest.setLoginUser(loginUser);

        monitorRequest.setStoreIds(JSONObject.parseArray(storeIds, Long.class));
        monitorRequest.setSkuIds(JSONObject.parseArray(skuIds, Long.class));

        ValueHolderV14<SgStorageRedisMonitorResult> callAPIResult = sgStorageRedisMonitorCmd.monitorRedisStorage(monitorRequest);

        JSONObject result = JSONObject.parseObject(JSONObject.toJSONString(callAPIResult.toJSONObject(),
                SerializerFeature.WriteMapNullValue));

        if (log.isDebugEnabled()) {
            log.debug("Finish SgStorageUpdateCtrl.monitorCommonRedisStorage. ReturnResult:{}", result);
        }

        return result;
    }

    @ApiOperation(value = "通用逻辑仓库存流水监控接口")
    @RequestMapping(path = "/p/cs/sg/storage/monitorCommonRedisStorageFtp", method = {RequestMethod.POST})
    public JSONObject monitorCommonRedisStorageFtp(HttpServletRequest request) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgStorageUpdateCtrl.monitorCommonRedisStorageFtp. ");
        }

        //QuerySessionImpl querySession = new QuerySessionImpl(request);
        //User loginUser = querySession.getUser();
        User loginUser = getRootUser();

        SgStorageFtpRedisMonitorCmd sgStorageFtpRedisMonitorCmd = (SgStorageFtpRedisMonitorCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(),
                SgStorageFtpRedisMonitorCmd.class.getName(),
                SgConstantsIF.GROUP, SgConstantsIF.VERSION);

        SgStorageFtpRedisMonitorRequest monitorRequest = new SgStorageFtpRedisMonitorRequest();

        monitorRequest.setLoginUser(loginUser);

        ValueHolderV14<SgStorageFtpRedisMonitorResult> callAPIResult =
                sgStorageFtpRedisMonitorCmd.monitorRedisStorageFtp(monitorRequest);

        JSONObject result = JSONObject.parseObject(JSONObject.toJSONString(callAPIResult.toJSONObject(),
                SerializerFeature.WriteMapNullValue));

        if (log.isDebugEnabled()) {
            log.debug("Finish SgStorageUpdateCtrl.monitorCommonRedisStorageFtp. ReturnResult:{}", result);
        }

        return result;
    }

    @ApiOperation(value = "通用逻辑仓箱库存初始化接口(Redis)")
    @RequestMapping(path = "/p/cs/sg/storage/initCommonRedisTeusStorage", method = {RequestMethod.POST})
    public JSONObject initCommonRedisTeusStorage(HttpServletRequest request,
                                        @RequestParam(value = "storeIds", required = false) String storeIds,
                                        @RequestParam(value = "proIds", required = false) String proIds,
                                        @RequestParam(value = "teusIds", required = false) String teusIds,
                                        @RequestParam(value = "proEcodes", required = false) String proEcodes,
                                        @RequestParam(value = "teusEcodes", required = false) String teusEcodes) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgStorageUpdateCtrl.initCommonRedisTeusStorage. ReceiveParams:storeIds:{}," +
                    "proIds:{}, teusIds:{}, proEcodes:{}, teusEcodes:{}", storeIds, proIds, teusIds, proEcodes, teusEcodes);
        }

        //QuerySessionImpl querySession = new QuerySessionImpl(request);
        //User loginUser = querySession.getUser();
        User loginUser = getRootUser();

        SgTeusStorageRedisInitCmd sgStorageRedisInitCmd = (SgTeusStorageRedisInitCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(),
                SgTeusStorageRedisInitCmd.class.getName(),
                SgConstantsIF.GROUP, SgConstantsIF.VERSION);

        SgTeusStorageRedisInitRequest initRequest = new SgTeusStorageRedisInitRequest();

        initRequest.setLoginUser(loginUser);

        initRequest.setStoreIds(JSONObject.parseArray(storeIds, Long.class));
        initRequest.setProIds(JSONObject.parseArray(proIds, Long.class));
        initRequest.setTeusIds(JSONObject.parseArray(teusIds, Long.class));
        initRequest.setProEcodes(JSONObject.parseArray(proEcodes, String.class));
        initRequest.setTeusEcodes(JSONObject.parseArray(teusEcodes, String.class));

        ValueHolderV14<SgTeusStorageRedisInitResult> callAPIResult = sgStorageRedisInitCmd.initRedisStorage(initRequest);

        JSONObject result = JSONObject.parseObject(JSONObject.toJSONString(callAPIResult.toJSONObject(),
                SerializerFeature.WriteMapNullValue));

        if (log.isDebugEnabled()) {
            log.debug("Finish SgStorageUpdateCtrl.initCommonRedisTeusStorage. ReturnResult:{}", result);
        }

        return result;
    }

    //TODO NEED DELETE
    private User getRootUser() {
        UserImpl user = new UserImpl();
        user.setId(893);
        user.setName("root");
        user.setEname("root");
        user.setActive(true);
        user.setClientId(37);
        user.setOrgId(27);
        user.setIsAdmin(2);
        user.setIsDev(2);
        return user;
    }
}
