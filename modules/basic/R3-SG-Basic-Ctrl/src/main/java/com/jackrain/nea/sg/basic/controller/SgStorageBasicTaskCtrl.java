package com.jackrain.nea.sg.basic.controller;

import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.face.impl.UserImpl;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RestController;

//import com.jackrain.nea.sg.basic.task.SgStorageBasicInfoUpdateTask;
@RestController
@Slf4j
@Api(value = "SgStorageBasicTaskController", description = "库存定时任务接口")
public class SgStorageBasicTaskCtrl {


    /**
    @Autowired
    SgStorageBasicInfoUpdateTask sgStorageBasicInfoUpdateTask;

    @ApiOperation(value = "库存表基础信息补偿定时任务接口")
    @RequestMapping(path = "/p/cs/sg/storage/runSgStorageBasicTask", method = {RequestMethod.POST})
    public JSONObject runSgStorageBasicTask(HttpServletRequest request) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgStorageBasicTaskCtrl.runSgStorageBasicTask;");
        }

        //QuerySessionImpl querySession = new QuerySessionImpl(request);
        //User loginUser = querySession.getUser();
        User loginUser = getRootUser();

        RunTaskResult callAPIResult = sgStorageBasicInfoUpdateTask.execute(new JSONObject());

        JSONObject result = JSONObject.parseObject(JSONObject.toJSONString(callAPIResult));

        if (log.isDebugEnabled()) {
            log.debug("Finish SgStorageBasicTaskCtrl.runSgStorageBasicTask. ReturnResult=" + result);
        }

        return result;
    }*/

    //TODO NEED DELETE
    private User getRootUser() {
        UserImpl user = new UserImpl();
        user.setId(893);
        user.setName("root");
        user.setEname("root");
        user.setActive(true);
        user.setClientId(37);
        user.setOrgId(27);
        user.setIsAdmin(2);
        user.setIsDev(2);
        return user;
    }
}
