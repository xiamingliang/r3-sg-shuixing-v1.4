package com.jackrain.nea.sg.basic.controller;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.github.pagehelper.PageInfo;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.api.SgPhyStorageQueryCmd;
import com.jackrain.nea.sg.basic.api.SgStorageQueryCmd;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.model.request.*;
import com.jackrain.nea.sg.basic.model.result.SgBPhyStorageMatrixQueryResult;
import com.jackrain.nea.sg.basic.model.result.SgBStorageMatrixQueryResult;
import com.jackrain.nea.sg.basic.model.result.SgRedisStorageQueryResult;
import com.jackrain.nea.sg.basic.model.result.SgSumStorageQueryResult;
import com.jackrain.nea.sg.basic.model.table.SgBPhyStorage;
import com.jackrain.nea.sg.basic.model.table.SgBStorage;
import com.jackrain.nea.sg.basic.model.table.SgBStorageInclPhy;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.face.impl.UserImpl;
import com.jackrain.nea.web.query.QuerySessionImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * @author: chenbin
 * @since: 2019-01-10
 * create at : 2019-01-10 14:13
 */
@RestController
@Slf4j
@Api(value = "SgStorageQueryController", description = "逻辑仓库存查询接口")
public class SgStorageQueryCtrl {

    @ApiOperation(value = "商品编码逻辑仓库存查询接口")
    @RequestMapping(path = "/p/cs/sg/storage/queryStorageByPro", method = {RequestMethod.POST})
    public JSONObject queryStorageByPro(HttpServletRequest request,
                                        @RequestParam(value = "storeId", required = true) Long storeId,
                                        @RequestParam(value = "proEcode", required = true) String proEcode) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgStorageQueryCtrl.querySgByPro. ReceiveParams:storeId=" + storeId +
                    " proEcode=" + proEcode + ";");
        }

        QuerySessionImpl querySession = new QuerySessionImpl(request);
        User loginUser = querySession.getUser();
        //User loginUser = getRootUser();

        SgStorageQueryCmd sgStorageQueryCmd = (SgStorageQueryCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(),
                SgStorageQueryCmd.class.getName(),
                SgConstantsIF.GROUP, SgConstantsIF.VERSION);

        SgStorageQueryRequest queryRequest = new SgStorageQueryRequest();
        List<Long> storeIds = new ArrayList<>();
        List<String> proEcodes = new ArrayList<>();
        storeIds.add(storeId);
        proEcodes.add(proEcode);
        queryRequest.setStoreIds(storeIds);
        queryRequest.setProEcodes(proEcodes);

        ValueHolderV14<List<SgBStorage>> callAPIResult = sgStorageQueryCmd.queryStorage(queryRequest, loginUser);

        JSONObject result = JSONObject.parseObject(JSONObject.toJSONString(callAPIResult.toJSONObject(),
                SerializerFeature.WriteMapNullValue));

        if (log.isDebugEnabled()) {
            log.debug("Finish SgStorageQueryCtrl.queryStorageByPro. ReturnResult=" + result);
        }

        return result;
    }

    @ApiOperation(value = "逻辑仓库存查询通用接口（实体仓）")
    @RequestMapping(path = "/p/cs/sg/storage/queryPhyWarehouseStorage", method = {RequestMethod.POST})
    public JSONObject queryPhyWarehouseStorage(HttpServletRequest request,
                                               @RequestParam(value = "phyWarehouseIds", required = true) String phyWarehouseIds,
                                               @RequestParam(value = "proEcodes", required = false) String proEcodes,
                                               @RequestParam(value = "skuEcodes", required = false) String skuEcodes) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgStorageQueryCtrl.queryPhyWarehouseStorage. ReceiveParams:phyWarehouseIds:{}, " +
                    "proEcodes:{}, skuEcodes:{}", phyWarehouseIds, proEcodes, skuEcodes);
        }

        //QuerySessionImpl querySession = new QuerySessionImpl(request);
        //User loginUser = querySession.getUser();
        User loginUser = getRootUser();

        SgStorageQueryCmd sgStorageQueryCmd = (SgStorageQueryCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(),
                SgStorageQueryCmd.class.getName(),
                SgConstantsIF.GROUP, SgConstantsIF.VERSION);

        SgPhyStorageQueryRequest queryRequest = new SgPhyStorageQueryRequest();

        queryRequest.setPhyWarehouseIds(JSONObject.parseArray(phyWarehouseIds, Long.class));
        queryRequest.setProEcodes(JSONObject.parseArray(proEcodes, String.class));
        queryRequest.setSkuEcodes(JSONObject.parseArray(skuEcodes, String.class));

        ValueHolderV14<List<SgBStorage>> callAPIResult = sgStorageQueryCmd.
                queryPhyWarehouseStorage(queryRequest, loginUser);

        JSONObject result = JSONObject.parseObject(JSONObject.toJSONString(callAPIResult.toJSONObject(),
                SerializerFeature.WriteMapNullValue));

        if (log.isDebugEnabled()) {
            log.debug("Finish SgStorageQueryCtrl.queryPhyWarehouseStorage. ReturnResult=" + result);
        }

        return result;
    }

    @ApiOperation(value = "逻辑仓库存查询通用接口（逻辑仓）")
    @RequestMapping(path = "/p/cs/sg/storage/queryStoreStorage", method = {RequestMethod.POST})
    public JSONObject queryStoreStorage(HttpServletRequest request,
                                        @RequestParam(value = "StoreId", required = true) Long StoreId,
                                        @RequestParam(value = "proEcodes", required = false) String proEcodes,
                                        @RequestParam(value = "skuEcodes", required = false) String skuEcodes) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgStorageQueryCtrl.queryStoreStorage. ReceiveParams:StoreId:{}, " +
                    "proEcodes:{}, skuEcodes:{}", StoreId, proEcodes, skuEcodes);
        }

        //QuerySessionImpl querySession = new QuerySessionImpl(request);
        //User loginUser = querySession.getUser();
        User loginUser = getRootUser();

        SgStorageQueryCmd sgStorageQueryCmd = (SgStorageQueryCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(),
                SgStorageQueryCmd.class.getName(),
                SgConstantsIF.GROUP, SgConstantsIF.VERSION);

        SgStoreStorageQueryRequest queryRequest = new SgStoreStorageQueryRequest();

        queryRequest.setStoreId(StoreId);
        queryRequest.setProEcodes(JSONObject.parseArray(proEcodes, String.class));
        queryRequest.setSkuEcodes(JSONObject.parseArray(skuEcodes, String.class));

        ValueHolderV14<List<SgBStorage>> callAPIResult = sgStorageQueryCmd.
                queryStoreStorage(queryRequest, loginUser);

        JSONObject result = JSONObject.parseObject(JSONObject.toJSONString(callAPIResult.toJSONObject(),
                SerializerFeature.WriteMapNullValue));

        if (log.isDebugEnabled()) {
            log.debug("Finish SgStorageQueryCtrl.queryStoreStorage. ReturnResult=" + result);
        }

        return result;
    }

    @ApiOperation(value = "逻辑仓库存查询通用接口")
    @RequestMapping(path = "/p/cs/sg/storage/queryCommonStorage", method = {RequestMethod.POST})
    public JSONObject queryCommonStorage(HttpServletRequest request,
                                         @RequestParam(value = "storeIds", required = false) String storeIds,
                                         @RequestParam(value = "phyWarehouseIds", required = false) String phyWarehouseIds,
                                         @RequestParam(value = "proEcodes", required = false) String proEcodes,
                                         @RequestParam(value = "skuEcodes", required = false) String skuEcodes) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgStorageQueryCtrl.queryCommonStorage. ReceiveParams:storeIds:{}," +
                    "phyWarehouseIds:{}, proEcodes:{}, skuEcodes:{}", storeIds, phyWarehouseIds, proEcodes, skuEcodes);
        }

        //QuerySessionImpl querySession = new QuerySessionImpl(request);
        //User loginUser = querySession.getUser();
        User loginUser = getRootUser();

        SgStorageQueryCmd sgStorageQueryCmd = (SgStorageQueryCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(),
                SgStorageQueryCmd.class.getName(),
                SgConstantsIF.GROUP, SgConstantsIF.VERSION);

        SgStorageQueryRequest queryRequest = new SgStorageQueryRequest();

        queryRequest.setStoreIds(JSONObject.parseArray(storeIds, Long.class));
        queryRequest.setPhyWarehouseIds(JSONObject.parseArray(phyWarehouseIds, Long.class));
        queryRequest.setProEcodes(JSONObject.parseArray(proEcodes, String.class));
        queryRequest.setSkuEcodes(JSONObject.parseArray(skuEcodes, String.class));

        ValueHolderV14<List<SgBStorage>> callAPIResult = sgStorageQueryCmd.queryStorage(queryRequest, loginUser);

        JSONObject result = JSONObject.parseObject(JSONObject.toJSONString(callAPIResult.toJSONObject(),
                SerializerFeature.WriteMapNullValue));

        if (log.isDebugEnabled()) {
            log.debug("Finish SgStorageQueryCtrl.queryCommonStorage. ReturnResult=" + result);
        }

        return result;
    }

    @ApiOperation(value = "逻辑仓库存查询通用接口(在库数量大于0)")
    @RequestMapping(path = "/p/cs/sg/storage/queryCommonStorageExclZero", method = {RequestMethod.POST})
    public JSONObject queryCommonStorageExclZero(HttpServletRequest request,
                                                 @RequestParam(value = "storeIds", required = false) String storeIds,
                                                 @RequestParam(value = "phyWarehouseIds", required = false) String phyWarehouseIds,
                                                 @RequestParam(value = "proEcodes", required = false) String proEcodes,
                                                 @RequestParam(value = "skuEcodes", required = false) String skuEcodes) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgStorageQueryCtrl.queryCommonStorageExclZero. ReceiveParams:storeIds:{}," +
                    "phyWarehouseIds:{}, proEcodes:{}, skuEcodes:{}", storeIds, phyWarehouseIds, proEcodes, skuEcodes);
        }

        //QuerySessionImpl querySession = new QuerySessionImpl(request);
        //User loginUser = querySession.getUser();
        User loginUser = getRootUser();

        SgStorageQueryCmd sgStorageQueryCmd = (SgStorageQueryCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(),
                SgStorageQueryCmd.class.getName(),
                SgConstantsIF.GROUP, SgConstantsIF.VERSION);

        SgStorageQueryRequest queryRequest = new SgStorageQueryRequest();

        queryRequest.setStoreIds(JSONObject.parseArray(storeIds, Long.class));
        queryRequest.setPhyWarehouseIds(JSONObject.parseArray(phyWarehouseIds, Long.class));
        queryRequest.setProEcodes(JSONObject.parseArray(proEcodes, String.class));
        queryRequest.setSkuEcodes(JSONObject.parseArray(skuEcodes, String.class));

        ValueHolderV14<List<SgBStorage>> callAPIResult = sgStorageQueryCmd.queryStorageExclZero(queryRequest, loginUser);

        JSONObject result = JSONObject.parseObject(JSONObject.toJSONString(callAPIResult.toJSONObject(),
                SerializerFeature.WriteMapNullValue));

        if (log.isDebugEnabled()) {
            log.debug("Finish SgStorageQueryCtrl.queryCommonStorageExclZero. ReturnResult=" + result);
        }

        return result;
    }

    @ApiOperation(value = "逻辑仓库存分页查询通用接口")
    @RequestMapping(path = "/p/cs/sg/storage/queryCommonStoragePage", method = {RequestMethod.POST})
    public JSONObject queryCommonStoragePage(HttpServletRequest request,
                                             @RequestParam(value = "storeIds", required = false) String storeIds,
                                             @RequestParam(value = "phyWarehouseIds", required = false) String phyWarehouseIds,
                                             @RequestParam(value = "proEcodes", required = false) String proEcodes,
                                             @RequestParam(value = "skuEcodes", required = false) String skuEcodes,
                                             @RequestParam(value = "pageNum", required = false) int pageNum,
                                             @RequestParam(value = "pageSize", required = false) int pageSize) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgStorageQueryCtrl.queryCommonStoragePage. ReceiveParams:storeIds:{}," +
                            "phyWarehouseIds:{}, proEcodes:{}, skuEcodes:{}, pageNum:{}, pageSize{}",
                    storeIds, phyWarehouseIds, proEcodes, skuEcodes, pageNum, pageSize);
        }

        //QuerySessionImpl querySession = new QuerySessionImpl(request);
        //User loginUser = querySession.getUser();
        User loginUser = getRootUser();

        SgStorageQueryCmd sgStorageQueryCmd = (SgStorageQueryCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(),
                SgStorageQueryCmd.class.getName(),
                SgConstantsIF.GROUP, SgConstantsIF.VERSION);

        SgStorageQueryRequest queryRequest = new SgStorageQueryRequest();
        SgStoragePageQueryRequest pageQueryRequest = new SgStoragePageQueryRequest();
        SgStoragePageRequest pageRequest = new SgStoragePageRequest();

        queryRequest.setStoreIds(JSONObject.parseArray(storeIds, Long.class));
        queryRequest.setPhyWarehouseIds(JSONObject.parseArray(phyWarehouseIds, Long.class));
        queryRequest.setProEcodes(JSONObject.parseArray(proEcodes, String.class));
        queryRequest.setSkuEcodes(JSONObject.parseArray(skuEcodes, String.class));
        pageQueryRequest.setQueryRequest(queryRequest);

        pageRequest.setPageNum(pageNum);
        pageRequest.setPageSize(pageSize);

        pageQueryRequest.setPageRequest(pageRequest);

        ValueHolderV14<PageInfo<SgBStorage>> callAPIResult = sgStorageQueryCmd.queryStoragePage(pageQueryRequest, loginUser);

        JSONObject result = JSONObject.parseObject(JSONObject.toJSONString(callAPIResult.toJSONObject(),
                SerializerFeature.WriteMapNullValue));

        if (log.isDebugEnabled()) {
            log.debug("Finish SgStoragePageQueryCtrl.queryCommonStoragePage. ReturnResult=" + result);
        }

        return result;
    }

    @ApiOperation(value = "逻辑仓库存查询通用接口(包含实体仓信息)")
    @RequestMapping(path = "/p/cs/sg/storage/queryCommonStorageInclPhy", method = {RequestMethod.POST})
    public JSONObject queryCommonStorageInclPhy(HttpServletRequest request,
                                                @RequestParam(value = "storeIds", required = false) String storeIds,
                                                @RequestParam(value = "phyWarehouseIds", required = false) String phyWarehouseIds,
                                                @RequestParam(value = "proEcodes", required = false) String proEcodes,
                                                @RequestParam(value = "skuEcodes", required = false) String skuEcodes) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgStorageQueryCtrl.queryCommonStorageInclPhy. ReceiveParams:storeIds:{}," +
                    "phyWarehouseIds:{}, proEcodes:{}, skuEcodes:{}", storeIds, phyWarehouseIds, proEcodes, skuEcodes);
        }

        //QuerySessionImpl querySession = new QuerySessionImpl(request);
        //User loginUser = querySession.getUser();
        User loginUser = getRootUser();

        SgStorageQueryCmd sgStorageQueryCmd = (SgStorageQueryCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(),
                SgStorageQueryCmd.class.getName(),
                SgConstantsIF.GROUP, SgConstantsIF.VERSION);

        SgStorageQueryRequest queryRequest = new SgStorageQueryRequest();

        queryRequest.setStoreIds(JSONObject.parseArray(storeIds, Long.class));
        queryRequest.setPhyWarehouseIds(JSONObject.parseArray(phyWarehouseIds, Long.class));
        queryRequest.setProEcodes(JSONObject.parseArray(proEcodes, String.class));
        queryRequest.setSkuEcodes(JSONObject.parseArray(skuEcodes, String.class));

        ValueHolderV14<List<SgBStorageInclPhy>> callAPIResult = sgStorageQueryCmd.queryStorageInclPhy(queryRequest, loginUser);

        JSONObject result = JSONObject.parseObject(JSONObject.toJSONString(callAPIResult.toJSONObject(),
                SerializerFeature.WriteMapNullValue));

        if (log.isDebugEnabled()) {
            log.debug("Finish SgStorageQueryCtrl.queryCommonStorageInclPhy. ReturnResult=" + result);
        }

        return result;
    }

    @ApiOperation(value = "逻辑仓库存查询通用接口(按照实体仓合计)")
    @RequestMapping(path = "/p/cs/sg/storage/querySumStorageGrpPhy", method = {RequestMethod.POST})
    public JSONObject querySumStorageGrpPhy(HttpServletRequest request,
                                            @RequestParam(value = "storeIds", required = false) String storeIds,
                                            @RequestParam(value = "proEcodes", required = false) String proEcodes,
                                            @RequestParam(value = "skuEcodes", required = false) String skuEcodes) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgStorageQueryCtrl.querySumStorageGrpPhy. ReceiveParams:storeIds:{}," +
                    "proEcodes:{}, skuEcodes:{}", storeIds, proEcodes, skuEcodes);
        }

        //QuerySessionImpl querySession = new QuerySessionImpl(request);
        //User loginUser = querySession.getUser();
        User loginUser = getRootUser();

        SgStorageQueryCmd sgStorageQueryCmd = (SgStorageQueryCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(),
                SgStorageQueryCmd.class.getName(),
                SgConstantsIF.GROUP, SgConstantsIF.VERSION);

        SgSumStorageQueryRequest queryRequest = new SgSumStorageQueryRequest();

        queryRequest.setStoreIds(JSONObject.parseArray(storeIds, Long.class));
        queryRequest.setProEcodes(JSONObject.parseArray(proEcodes, String.class));
        queryRequest.setSkuEcodes(JSONObject.parseArray(skuEcodes, String.class));

        SgSumStoragePageQueryRequest pageRequest = new SgSumStoragePageQueryRequest();
        pageRequest.setQueryRequest(queryRequest);

        ValueHolderV14<List<SgSumStorageQueryResult>> callAPIResult = sgStorageQueryCmd.querySumStorageGrpPhy(pageRequest, loginUser);

        JSONObject result = JSONObject.parseObject(JSONObject.toJSONString(callAPIResult.toJSONObject(),
                SerializerFeature.WriteMapNullValue));

        if (log.isDebugEnabled()) {
            log.debug("Finish SgStorageQueryCtrl.querySumStorageGrpPhy. ReturnResult=" + result);
        }

        return result;
    }

    @ApiOperation(value = "通用商品编码库存查询接口（商品矩阵专用）")
    @RequestMapping(path = "/p/cs/sg/storage/queryCommonStorageByPro", method = {RequestMethod.POST})
    public JSONObject queryCommonStorageByPro(HttpServletRequest request,
                                              @RequestParam(value = "storeId", required = false) Long storeId,
                                              @RequestParam(value = "phyWarehouseId", required = false) Long phyWarehouseId,
                                              @RequestParam(value = "proEcode", required = true) String proEcode) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgStorageQueryCtrl.queryCommonStorageByPro. ReceiveParams:storeId=" + storeId +
                    " phyWarehouseId=" + phyWarehouseId + " proEcode=" + proEcode + ";");
        }

        List<String> proEcodes = new ArrayList<>();
        JSONObject result;

        //QuerySessionImpl querySession = new QuerySessionImpl(request);
        //User loginUser = querySession.getUser();
        User loginUser = getRootUser();

        if (storeId != null) {
            //逻辑仓库存查询
            SgStorageQueryCmd sgStorageQueryCmd = (SgStorageQueryCmd) ReferenceUtil.refer(
                    ApplicationContextHandle.getApplicationContext(),
                    SgStorageQueryCmd.class.getName(),
                    SgConstantsIF.GROUP, SgConstantsIF.VERSION);

            SgStorageQueryRequest queryRequest = new SgStorageQueryRequest();
            List<Long> storeIds = new ArrayList<>();
            storeIds.add(storeId);
            proEcodes.add(proEcode);
            queryRequest.setStoreIds(storeIds);
            queryRequest.setProEcodes(proEcodes);

            ValueHolderV14<List<SgBStorage>> callAPIResult = sgStorageQueryCmd.queryStorage(queryRequest, loginUser);


            List<SgBStorageMatrixQueryResult> results = new ArrayList<>();
            if (CollectionUtils.isNotEmpty(callAPIResult.getData())) {
                callAPIResult.getData().forEach(sgBPhyStorage -> {
                    SgBStorageMatrixQueryResult matrixQueryResult = new SgBStorageMatrixQueryResult();
                    BeanUtils.copyProperties(sgBPhyStorage, matrixQueryResult);
                    matrixQueryResult.setPsCClrId(sgBPhyStorage.getPsCSpec1Id());
                    matrixQueryResult.setPsCClrEcode(sgBPhyStorage.getPsCSpec1Ecode());
                    matrixQueryResult.setPsCClrEname(sgBPhyStorage.getPsCSpec1Ename());
                    matrixQueryResult.setPsCSizeId(sgBPhyStorage.getPsCSpec2Id());
                    matrixQueryResult.setPsCSizeEcode(sgBPhyStorage.getPsCSpec2Ecode());
                    matrixQueryResult.setPsCSizeEname(sgBPhyStorage.getPsCSpec2Ename());
                    results.add(matrixQueryResult);
                });
            }
            ValueHolderV14<List<SgBStorageMatrixQueryResult>> holderV14 = new ValueHolderV14<>();
            holderV14.setCode(ResultCode.SUCCESS);
            holderV14.setData(results);


            result = JSONObject.parseObject(JSONObject.toJSONString(holderV14.toJSONObject(),
                    SerializerFeature.WriteMapNullValue));

            result.put("matrixType", SgConstantsIF.MATRIX_QUERY_TYPE_STORAGE);

        } else {

            //实体仓库存查询
            SgPhyStorageQueryCmd sgPhyStorageQueryCmd = (SgPhyStorageQueryCmd) ReferenceUtil.refer(
                    ApplicationContextHandle.getApplicationContext(),
                    SgPhyStorageQueryCmd.class.getName(),
                    SgConstantsIF.GROUP, SgConstantsIF.VERSION);

            SgPhyStorageQueryRequest queryRequest = new SgPhyStorageQueryRequest();
            List<Long> phyWarehouseIds = new ArrayList<>();
            phyWarehouseIds.add(phyWarehouseId);
            proEcodes.add(proEcode);
            queryRequest.setPhyWarehouseIds(phyWarehouseIds);
            queryRequest.setProEcodes(proEcodes);

            ValueHolderV14<List<SgBPhyStorage>> callAPIResult = sgPhyStorageQueryCmd.queryPhyStorage(queryRequest, loginUser);

            List<SgBPhyStorageMatrixQueryResult> results = new ArrayList<>();
            if (CollectionUtils.isNotEmpty(callAPIResult.getData())) {
                callAPIResult.getData().forEach(sgBPhyStorage -> {
                    SgBPhyStorageMatrixQueryResult matrixQueryResult = new SgBPhyStorageMatrixQueryResult();
                    BeanUtils.copyProperties(sgBPhyStorage, matrixQueryResult);
                    matrixQueryResult.setPsCClrId(sgBPhyStorage.getPsCSpec1Id());
                    matrixQueryResult.setPsCClrEcode(sgBPhyStorage.getPsCSpec1Ecode());
                    matrixQueryResult.setPsCClrEname(sgBPhyStorage.getPsCSpec1Ename());
                    matrixQueryResult.setPsCSizeId(sgBPhyStorage.getPsCSpec2Id());
                    matrixQueryResult.setPsCSizeEcode(sgBPhyStorage.getPsCSpec2Ecode());
                    matrixQueryResult.setPsCSizeEname(sgBPhyStorage.getPsCSpec2Ename());
                    results.add(matrixQueryResult);
                });
            }
            ValueHolderV14<List<SgBPhyStorageMatrixQueryResult>> holderV14 = new ValueHolderV14<>();
            holderV14.setCode(ResultCode.SUCCESS);
            holderV14.setData(results);
            result = JSONObject.parseObject(JSONObject.toJSONString(holderV14.toJSONObject(),
                    SerializerFeature.WriteMapNullValue));

            result.put("matrixType", SgConstantsIF.MATRIX_QUERY_TYPE_PHY_STORAGE);
        }

        if (log.isDebugEnabled()) {
            log.debug("Finish SgStorageQueryCtrl.queryCommonStorageByPro. ReturnResult=" + result);
        }

        return result;
    }

    @ApiOperation(value = "商品条码Redis逻辑仓库存查询接口")
    @RequestMapping(path = "/p/cs/sg/storage/queryRedisStorageBySkuId", method = {RequestMethod.POST})
    public JSONObject queryRedisStorageBySkuId(HttpServletRequest request,
                                               @RequestParam(value = "param", required = true) String param) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgStorageQueryCtrl.queryRedisStorageBySkuId. ReceiveParams:param={};", param);
        }

        //QuerySessionImpl querySession = new QuerySessionImpl(request);
        //User loginUser = querySession.getUser();
        User loginUser = getRootUser();

        JsonParser jsonParser = new JsonParser();
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd HH:mm:ss")
                .create();

        SgStorageQueryCmd sgStorageQueryCmd = (SgStorageQueryCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(),
                SgStorageQueryCmd.class.getName(),
                SgConstantsIF.GROUP, SgConstantsIF.VERSION);

        //获取MQ消息
        JsonObject jsonParam = jsonParser.parse(param).getAsJsonObject();

        SgRedisStorageQueryRequest queryRequest = gson.fromJson(jsonParam,
                new TypeToken<SgRedisStorageQueryRequest>() {
                }.getType());

        ValueHolderV14<List<SgRedisStorageQueryResult>> callAPIResult = sgStorageQueryCmd.queryRedisStorage(queryRequest, loginUser);

        JSONObject result = JSONObject.parseObject(JSONObject.toJSONString(callAPIResult.toJSONObject(),
                SerializerFeature.WriteMapNullValue));

        if (log.isDebugEnabled()) {
            log.debug("Finish SgStorageQueryCtrl.queryRedisStorageBySkuId. ReturnResult=" + result);
        }

        return result;
    }

    //TODO NEED DELETE
    private User getRootUser() {
        UserImpl user = new UserImpl();
        user.setId(893);
        user.setName("root");
        user.setEname("root");
        user.setActive(true);
        user.setClientId(37);
        user.setOrgId(27);
        user.setIsAdmin(2);
        user.setIsDev(2);
        return user;
    }
}
