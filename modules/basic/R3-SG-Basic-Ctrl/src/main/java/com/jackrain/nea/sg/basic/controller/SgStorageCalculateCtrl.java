package com.jackrain.nea.sg.basic.controller;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.sg.basic.api.SgStorageQtyCalculateCmd;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.model.request.SgQtyStorageByWareCalcRequest;
import com.jackrain.nea.sg.basic.model.result.SgQtyStorageByWareCalcResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.face.impl.UserImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@Slf4j
@Api(value = "SgStorageCalculateController", description = "库存计算接口")
public class SgStorageCalculateCtrl {

    @ApiOperation(value = "计算逻辑仓在库数量接口(通过实体仓ID)接口")
    @RequestMapping(path = "/p/cs/sg/storage/calcSgQtyStorageByWare", method = {RequestMethod.POST})
    public JSONObject calcSgQtyStorageByWare(HttpServletRequest request,
                                             @RequestParam(value = "param", required = true) String param) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgStorageCalculateCtrl.calcSgQtyStorageByWare;");
        }

        JsonParser jsonParser = new JsonParser();
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd HH:mm:ss")
                .create();

        //QuerySessionImpl querySession = new QuerySessionImpl(request);
        //User loginUser = querySession.getUser();
        User loginUser = getRootUser();

        SgStorageQtyCalculateCmd sgStorageQtyCalculateCmd = (SgStorageQtyCalculateCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(),
                SgStorageQtyCalculateCmd.class.getName(),
                SgConstantsIF.GROUP, SgConstantsIF.VERSION);

        //获取MQ消息
        JsonObject jsonParam = jsonParser.parse(param).getAsJsonObject();

        SgQtyStorageByWareCalcRequest calcRequest = gson.fromJson(jsonParam,
                new TypeToken<SgQtyStorageByWareCalcRequest>() {
                }.getType());

        calcRequest.setLoginUser(loginUser);

        ValueHolderV14<SgQtyStorageByWareCalcResult> callAPIResult = sgStorageQtyCalculateCmd.calcSgQtyStorageByWare(calcRequest);

        JSONObject result = JSONObject.parseObject(JSONObject.toJSONString(callAPIResult.toJSONObject(),
                SerializerFeature.WriteMapNullValue));

        if (log.isDebugEnabled()) {
            log.debug("Finish SgStorageCalculateCtrl.calcSgQtyStorageByWare. ReturnResult=" + result);
        }

        return result;
    }

    //TODO NEED DELETE
    private User getRootUser() {
        UserImpl user = new UserImpl();
        user.setId(893);
        user.setName("root");
        user.setEname("root");
        user.setActive(true);
        user.setClientId(37);
        user.setOrgId(27);
        user.setIsAdmin(2);
        user.setIsDev(2);
        return user;
    }
}
