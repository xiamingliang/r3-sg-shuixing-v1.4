package com.jackrain.nea.sg.basic.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.api.SgMQResendCmd;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @author: 舒威
 * @since: 2019/8/19
 * create at : 2019/8/19 17:05
 */
@RestController
@Slf4j
@Api(value = "SgMQResendCtrl", description = "库存MQ消息重发")
public class SgMQResendCtrl {

    @ApiOperation(value = "库存消息补发")
    @PostMapping(path = "/p/sg/sendMQ")
    public JSONObject sendMQ(HttpServletRequest request, @RequestParam(value = "param") String param) {

        if (log.isDebugEnabled()) {
            log.debug("start SgMQResendCtrl.sendMQ. ReceiveParams=" + param + ";");
        }

        ValueHolderV14 result = new ValueHolderV14(ResultCode.SUCCESS, "success");
        try {
            SgMQResendCmd mqResendCmd = (SgMQResendCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                    SgMQResendCmd.class.getName(), "sg", "1.0");
            JSONObject paramJo = JSON.parseObject(param);
            JSONObject body = paramJo.getJSONObject("body");
            String topic = paramJo.getString("topic");
            String tag = paramJo.getString("tag");
            String msgKey = paramJo.getString("msgKey");
            if (body == null || body.isEmpty()
                    || StringUtils.isEmpty(topic)
                    || StringUtils.isEmpty(tag)
                    || StringUtils.isEmpty(msgKey)) {
                result.setCode(ResultCode.FAIL);
                result.setMessage("body,topic,tag,msgKey不能为空!");
            } else {
                result = mqResendCmd.sendMQ(body.toJSONString(), topic, tag, msgKey);
            }
        } catch (Exception e) {
            result.setCode(ResultCode.FAIL);
            result.setMessage("error--" + e.getMessage());
        }

        if (log.isDebugEnabled()) {
            log.debug("Finish SgMQResendCtrl.sendMQ. ReturnResult=" + JSONObject.toJSONString(result));
        }

        return result.toJSONObject();
    }
}
