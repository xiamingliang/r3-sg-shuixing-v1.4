package com.jackrain.nea.sg.basic.task;

import com.alibaba.fastjson.JSONObject;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.jackrain.nea.sg.basic.model.request.SgStorageRedisMonitorRequest;
import com.jackrain.nea.sg.basic.services.SgStorageRedisMonitorService;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.task.IR3Task;
import com.jackrain.nea.task.RunTaskResult;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.face.impl.UserImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author chenbin
 * @since 2019/9/13
 * create at : 2019/9/13 20:32
 * sample : {"param":"{}","className":"com.jackrain.nea.sg.basic.task.SgStorageRedisMonitorTask","type":"localJob"}
 */
@Slf4j
@Component
public class SgStorageRedisMonitorTask implements IR3Task {

    @Autowired
    private SgStorageRedisMonitorService service;

    @Override
    public RunTaskResult execute(JSONObject params) {

        log.info("Start SgStorageRedisMonitorTask.execute. ReceiveParams:params:{};",
                JSONObject.toJSONString(params));

        RunTaskResult runTaskResult = new RunTaskResult();
        SgStorageRedisMonitorRequest request;

        JsonParser jsonParser = new JsonParser();
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd HH:mm:ss")
                .create();

        if (params.isEmpty()) {
            request = new SgStorageRedisMonitorRequest();
        } else {
            JsonObject jsonParam = jsonParser.parse(params.toString()).getAsJsonObject();

            request = gson.fromJson(jsonParam,
                    new TypeToken<SgStorageRedisMonitorRequest>() {
                    }.getType());
        }

        if(request.getLoginUser() == null) {
            request.setLoginUser(getRootUser());
        }

        ValueHolderV14 holder = service.monitorRedisStorage(request);

        if (holder.isOK()) {
            runTaskResult.setSuccess(Boolean.TRUE);
            runTaskResult.setMessage(holder.getMessage());
        } else {
            log.error("SgStorageRedisMonitorTask.execute. error:" + holder.getMessage());
            runTaskResult.setSuccess(Boolean.FALSE);
            runTaskResult.setMessage(holder.getMessage());
        }

        log.info("Finish SgStorageRedisMonitorTask.execute. ReturnResult:success holder:{};"
                , holder);

        return runTaskResult;
    }

    //TODO NEED DELETE
    private User getRootUser() {
        UserImpl user = new UserImpl();
        user.setId(893);
        user.setName("root");
        user.setEname("root");
        user.setActive(true);
        user.setClientId(37);
        user.setOrgId(27);
        user.setIsAdmin(2);
        user.setIsDev(2);
        return user;
    }
}
