package com.jackrain.nea.sg.basic.task;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.sg.basic.services.SgMQResendService;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.task.IR3Task;
import com.jackrain.nea.task.RunTaskResult;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 库存消息重发
 *
 * @author: 舒威
 * @since: 2019/8/19
 * create at : 2019/8/19 16:42
 */
@Slf4j
@Component
public class SgMQResendTask implements IR3Task {

    @Autowired
    private SgMQResendService mqResendService;

    @Override
    public RunTaskResult execute(JSONObject params) {
        log.info("Start SgMQResendTask.execute. ReceiveParams:params:{};",
                JSONObject.toJSONString(params));

        RunTaskResult runTaskResult = new RunTaskResult();
        runTaskResult.setSuccess(Boolean.TRUE);
        if (params != null) {
            JSONObject body = params.getJSONObject("body");
            String topic = params.getString("topic");
            String tag = params.getString("tag");
            String msgKey = params.getString("msgKey");
            if (body == null || body.isEmpty()
                    || StringUtils.isEmpty(topic)
                    || StringUtils.isEmpty(tag)
                    || StringUtils.isEmpty(msgKey)) {
                runTaskResult.setSuccess(Boolean.FALSE);
                runTaskResult.setMessage("body,topic,tag,msgKey不能为空!");
            } else {
                ValueHolderV14 sendMQ = mqResendService.sendMQ(body.toJSONString(), topic, tag, msgKey);
                if (sendMQ.isOK()) {
                    runTaskResult.setMessage(sendMQ.getMessage());
                } else {
                    runTaskResult.setSuccess(Boolean.FALSE);
                    runTaskResult.setMessage(sendMQ.getMessage());
                }
            }
        } else {
            runTaskResult.setSuccess(Boolean.FALSE);
            runTaskResult.setMessage("param不能为空!");
        }

        log.info("Finish SgMQResendTask.execute. ReturnResults:results:{};",
                JSONObject.toJSONString(runTaskResult));

        return runTaskResult;
    }
}
