package com.jackrain.nea.sg.basic.task;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.redis.config.CusRedisTemplate;
import com.jackrain.nea.redis.util.RedisOpsUtil;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.model.request.SgGroupStorageCalculateRequest;
import com.jackrain.nea.sg.basic.model.request.SgGroupStorageVirtualSkuRequest;
import com.jackrain.nea.sg.basic.services.SgGroupStorageCalculateService;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.task.IR3Task;
import com.jackrain.nea.task.RunTaskResult;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.face.impl.UserImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.curator.shaded.com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author 舒威
 * @since 2019/7/19
 * create at : 2019/7/19 11:28
 */
@Slf4j
@Component
public class SgGroupStorageCalculateTask implements IR3Task {

    @Value("${sg.group.calc_group_stroage_max_threads}")
    private Integer threadsNum;


    /**
     * 组合福袋商品库存计算定时任务
     */
    @Override
    public RunTaskResult execute(JSONObject params) {
        log.info("Start SgGroupStorageCalculateTask.execute. ReceiveParams:params:{};",
                JSONObject.toJSONString(params));

        RunTaskResult runTaskResult = new RunTaskResult();
        runTaskResult.setSuccess(Boolean.TRUE);

        CusRedisTemplate<String, String> redisTemplate = RedisOpsUtil.getStrRedisTemplate();
        String fuzzyKey = SgConstantsIF.COMBINED_SKUGROUP + ":*";
        Set<String> keys = redisTemplate.keys(fuzzyKey);
        if (CollectionUtils.isNotEmpty(keys)) {
            //记录所有的虚拟条码记录
            List<SgGroupStorageVirtualSkuRequest> virtualSkuList = Lists.newArrayList();
            for (String key : keys) {
                Map entries = redisTemplate.opsForHash().entries(key);
                if (MapUtils.isNotEmpty(entries)) {
                    for (Object o : entries.values()) {
                        if (o != null) {
                            try {
                                String str = (String) o;
                                SgGroupStorageVirtualSkuRequest virtualSkuRequest =
                                        JSONObject.parseObject(str, SgGroupStorageVirtualSkuRequest.class);
                                virtualSkuList.add(virtualSkuRequest);
                            } catch (Exception e) {
                                e.printStackTrace();
                                if (log.isDebugEnabled()) {
                                    log.debug("key:" + key + ",redis参数解析异常！");
                                }
                            }
                        }
                    }
                }
            }

            if (CollectionUtils.isNotEmpty(virtualSkuList)) {
                List<List<SgGroupStorageVirtualSkuRequest>> lists = StorageESUtils.averageAssign(virtualSkuList, threadsNum);
                if (CollectionUtils.isNotEmpty(lists)) {
                    SgGroupStorageCalculateService calculateService = ApplicationContextHandle.getBean(SgGroupStorageCalculateService.class);
                    for (List<SgGroupStorageVirtualSkuRequest> list : lists) {
                        Runnable task;
                        if (CollectionUtils.isNotEmpty(list)) {
                            task = () -> {
                                SgGroupStorageCalculateRequest request = new SgGroupStorageCalculateRequest();
                                request.setVirtualSkuList(list);
                                request.setLoginUser(getRootUser());
                                calculateService.calculateGroupStorage(request);
                            };
                            new Thread(task).start();
                        }
                    }
                }
            }

        }
        return runTaskResult;
    }

    private User getRootUser() {
        User user = new UserImpl();
        ((UserImpl) user).setId(893);
        ((UserImpl) user).setName("root");
        ((UserImpl) user).setEname("系统管理员");
        ((UserImpl) user).setTruename("系统管理员");
        ((UserImpl) user).setClientId(37);
        ((UserImpl) user).setOrgId(27);
        return user;
    }
}
