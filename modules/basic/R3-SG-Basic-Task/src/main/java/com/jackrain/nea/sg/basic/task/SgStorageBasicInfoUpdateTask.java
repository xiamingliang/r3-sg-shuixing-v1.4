package com.jackrain.nea.sg.basic.task;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.services.SgPhyStorageBasicInfoUpdateTaskService;
import com.jackrain.nea.sg.basic.services.SgStorageBasicInfoUpdateTaskService;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.task.IR3Task;
import com.jackrain.nea.task.RunTaskResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/7/11 18:59
 */
@Slf4j
@Component
public class SgStorageBasicInfoUpdateTask implements IR3Task {

    @Autowired
    private SgStorageBasicInfoUpdateTaskService sgStorageBasicInfoUpdateTaskkService;

    @Autowired
    private SgPhyStorageBasicInfoUpdateTaskService sgPhyStorageBasicInfoUpdateTaskkService;

    /**
     * 库存表基础信息补偿定时任务
     *
     * @param params
     * @return
     */
    @Override
    public RunTaskResult execute(JSONObject params) {

        log.info("Start SgStorageBasicInfoUpdateTask.execute. ReceiveParams:params:{};",
                JSONObject.toJSONString(params));

        RunTaskResult runTaskResult = new RunTaskResult();
        runTaskResult.setSuccess(Boolean.TRUE);
        ValueHolderV14 result = null;

        result = sgStorageBasicInfoUpdateTaskkService.updateStorageBaicInfo(params);

        if (ResultCode.FAIL == result.getCode()) {
            log.error("SgStorageBasicInfoUpdateTask.execute.定时更新逻辑仓库存表基础信息执行失败！详情：", result.getMessage());
            runTaskResult.setSuccess(Boolean.FALSE);
        }

        log.info("SgStorageBasicInfoUpdateTask.execute. ReturnResult:result:{};",
                JSONObject.toJSONString(result));

        result = sgPhyStorageBasicInfoUpdateTaskkService.updateStorageBaicInfo(params);

        if (ResultCode.FAIL == result.getCode()) {
            log.error("SgStorageBasicInfoUpdateTask.execute.定时更新实体仓库存表基础信息执行失败！详情：", result.getMessage());
            runTaskResult.setSuccess(Boolean.FALSE);
        }

        log.info("SgStorageBasicInfoUpdateTask.execute. ReturnResult:result:{};",
                JSONObject.toJSONString(result));

        runTaskResult.setMessage(Resources.getMessage("库存表基础信息补偿定时任务执行完成！"));

        log.info("Finish SgStorageBasicInfoUpdateTask.execute. ReturnResult:runTaskResult:{};",
                JSONObject.toJSONString(runTaskResult));

        return runTaskResult;

    }
}
