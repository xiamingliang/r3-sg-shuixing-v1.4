package com.jackrain.nea.sg.itface.processor.erp;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.r3.mq.processor.AbstractMqProcessor;
import com.jackrain.nea.r3.mq.processor.MqProcessResult;
import com.jackrain.nea.sg.transfer.erp.SgTransferOutCallBack;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.utils.BillType;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

/**
 * @author leexh
 * @since 2020/4/2 19:28
 * desc: 调拨出库erp回传
 */
@Slf4j
@Component
public class SgTransferOutForErpProcessor extends AbstractMqProcessor {

    @Override
    public boolean checkCanExecuteProcess(String messageTopic, String messageKey, String messageBody, String messageTag) {
        log.info(this.getClass().getName() + ",接收调拨出库erp回传消息:" +
                        "messageTopic{}," +
                        "messageTag{}," +
                        "messageKey{}," +
                        "messageBody{}",
                new Object[]{messageTopic, messageTag, messageKey, messageBody});
        return StringUtils.equalsIgnoreCase(messageTag, BillType.TRANSFER_OUT_SYNC.getBillTag());
    }


    @Override
    public void initialMqOrderProcessor(ApplicationContext applicationContext) {
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ",进入调拨出库erp回传服务");
        }
    }

    @Override
    public MqProcessResult startProcess(String messageTopic, String messageKey, String messageBody, String messageTag) {

        // 获取消息体
        JSONObject param = JSON.parseObject(messageBody);
        if (param == null) {
            return new MqProcessResult(false, Resources.getMessage("messageBody解析为null"));
        }

        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ",调拨出库erp回传消息体:" + JSON.toJSONString(param));
        }

        boolean success = param.getBooleanValue("SUCCESS");
        if (success) {
            // 调拨单ID
            Long objId = param.getLong("SOURCE_BILL_NO");
            // erp调拨单号
            JSONObject erpBackContent = param.getJSONObject("ERP_BACK_CONTENT");
            String erpBillNo = erpBackContent.getString("BILL_NO");

            SgTransferOutCallBack callBack = ApplicationContextHandle.getBean(SgTransferOutCallBack.class);
            callBack.updateTransferErpBillNo(objId, erpBillNo,true);
        }

        return new MqProcessResult(false, Resources.getMessage("消费成功！"));
    }

}
