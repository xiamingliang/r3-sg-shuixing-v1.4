package com.jackrain.nea.sg.itface.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.data.basic.services.BasicPsQueryService;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.ip.api.qimen.QimenEntryorderCreateCmd;
import com.jackrain.nea.ip.model.qimen.QimenEntryorderCreateModel;
import com.jackrain.nea.ip.model.result.QimenAsyncMqResult;
import com.jackrain.nea.model.util.AdParamUtil;
import com.jackrain.nea.psext.request.PsToWmsRequest;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.common.SystemUserResource;
import com.jackrain.nea.sg.basic.mapper.CpCPhyWarehouseMapper;
import com.jackrain.nea.sg.basic.model.table.CpCPhyWarehouse;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.in.common.SgInNoticeConstants;
import com.jackrain.nea.sg.in.mapper.SgBPhyInNoticesMapper;
import com.jackrain.nea.sg.in.model.result.SgBPhyInNoticesBoxResult;
import com.jackrain.nea.sg.in.model.result.SgBPhyInNoticesResult;
import com.jackrain.nea.sg.in.model.table.SgBPhyInNotices;
import com.jackrain.nea.sg.in.model.table.SgBPhyInNoticesImpItem;
import com.jackrain.nea.sg.in.model.table.SgBPhyInNoticesItem;
import com.jackrain.nea.sg.in.services.SgPhyInNoticesSelectService;
import com.jackrain.nea.sg.itface.config.AddressforWmsConfig;
import com.jackrain.nea.sg.itface.out.common.MQConstantsIF;
import com.jackrain.nea.sg.itface.out.common.SgInterfaceConstants;
import com.jackrain.nea.sg.out.common.SgOutConstants;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Component
public class SgPhyInNoticeSendToWmsTaskService {


    @Autowired
    private SgPhyInNoticesSelectService sgPhyInNoticesSelectService;

    @Autowired
    private BasicPsQueryService basicPsQueryService;
    @Autowired
    private AddressforWmsConfig addressforWmsConfig;

    public ValueHolderV14 sgPhyInNoticeSendToWmsTask(JSONObject params) throws NDSException {
        if (log.isDebugEnabled()) {
            log.debug("Start SgPhyInNoticeSendToWmsTaskService.sgPhyInNoticeSendToWmsTask. ReceiveParams:params="
                    + JSONObject.toJSONString(params) + ";");
        }
        ValueHolderV14 result = new ValueHolderV14();
        SgBPhyInNoticesMapper sgBPhyInNoticesMapper = ApplicationContextHandle.getBean(SgBPhyInNoticesMapper.class);
        User user = SystemUserResource.getRootUser();
        //定时任务获取符合条件的入库通知单数据


        ValueHolderV14<List<SgBPhyInNoticesResult>> vh = sgPhyInNoticesSelectService.selectByBigCrgo();

        if (!vh.isOK()) {
            throw new NDSException(Resources.getMessage("获取入库通知单失败!"));
        }
        List<SgBPhyInNoticesResult> noticesBillList = vh.getData();
        if (CollectionUtils.isEmpty(noticesBillList)) {
            throw new NDSException(Resources.getMessage("当前入库通知单不存在!"));
        }

        //2019-09-18 过滤通知单明细数量都为0的单据
        List<SgBPhyInNoticesResult> billList = Lists.newArrayList();
        HashMap<String, SgBPhyInNotices> noticesMap = Maps.newHashMap();
        for (SgBPhyInNoticesResult noticesResult : noticesBillList) {
            List<SgBPhyInNoticesItem> noticesItems = noticesResult.getItemList().stream()
                    .filter(var -> var.getQty().compareTo(BigDecimal.ZERO) != 0)
                    .collect(Collectors.toList());
            if (CollectionUtils.isNotEmpty(noticesItems)) {
                billList.add(noticesResult);
                SgBPhyInNotices orgNotices = noticesResult.getNotices();
                noticesMap.put(orgNotices.getBillNo(), orgNotices);

                SgBPhyInNotices notices = new SgBPhyInNotices();
                notices.setId(orgNotices.getId());
                notices.setWmsStatus((long) SgInNoticeConstants.WMS_UPLOAD_STATUTS_UPLOADING);
                StorageESUtils.setBModelDefalutDataByUpdate(notices, user);
                notices.setModifierename(user.getEname());
                sgBPhyInNoticesMapper.updateById(notices);
            } else {
                SgBPhyInNotices notices = new SgBPhyInNotices();
                SgBPhyInNotices orgNotices = noticesResult.getNotices();
                notices.setId(orgNotices.getId());
                notices.setWmsStatus((long) SgInNoticeConstants.WMS_UPLOAD_STATUTS_FAIL);
                notices.setWmsFailReason("通知数量不允许全部为0!");
                Long wmsFailCount = Optional.ofNullable(orgNotices.getWmsFailCount()).orElse(0L);
                wmsFailCount++;
                notices.setWmsFailCount(wmsFailCount);
                StorageESUtils.setBModelDefalutDataByUpdate(notices, user);
                notices.setModifierename(user.getEname());
                sgBPhyInNoticesMapper.updateById(notices);
            }
        }

        if (CollectionUtils.isNotEmpty(billList)) {
            ValueHolderV14 qimenResult = createQimenEntryOrder(billList, user);
            List<QimenAsyncMqResult> list = (List<QimenAsyncMqResult>) qimenResult.getData();
            list.forEach(qimenAsyncMqResult -> {
                if (!Boolean.valueOf(qimenAsyncMqResult.getIsSend())) {
                    SgBPhyInNotices notices = new SgBPhyInNotices();
                    notices.setWmsStatus((long) SgInNoticeConstants.WMS_UPLOAD_STATUTS_FAIL);
                    notices.setWmsFailReason(qimenAsyncMqResult.getMessage());
                    StorageESUtils.setBModelDefalutDataByUpdate(notices, user);
                    notices.setModifierename(user.getEname());

                    SgBPhyInNotices orgNotices = noticesMap.get(qimenAsyncMqResult.getBillNo());
                    Long wmsFailedCount = orgNotices == null ? 1L : orgNotices.getWmsFailCount() + 1;
                    notices.setWmsFailCount(wmsFailedCount);
                    sgBPhyInNoticesMapper.update(notices, new UpdateWrapper<SgBPhyInNotices>().lambda()
                            .eq(SgBPhyInNotices::getBillNo, qimenAsyncMqResult.getBillNo())
                            .eq(SgBPhyInNotices::getIsactive, SgConstants.IS_ACTIVE_Y));
                }
            });
        }

        result.setCode(ResultCode.SUCCESS);
        result.setMessage("入库通知单上传WMS成功!");
        return result;
    }
    public ValueHolderV14 sgPhyInNoticeSendToWmsTaskBox(JSONObject params) throws NDSException {
        if (log.isDebugEnabled()) {
            log.debug("Start SgPhyInNoticeSendToWmsTaskService.sgPhyInNoticeSendToWmsTask. ReceiveParams:params="
                    + JSONObject.toJSONString(params) + ";");
        }
        ValueHolderV14 result = new ValueHolderV14();
        SgBPhyInNoticesMapper sgBPhyInNoticesMapper = ApplicationContextHandle.getBean(SgBPhyInNoticesMapper.class);
        User user = SystemUserResource.getRootUser();
        //定时任务获取符合条件的入库通知单数据

       ValueHolderV14<List<SgBPhyInNoticesBoxResult>> vh=sgPhyInNoticesSelectService.selectBigCrgoBox();

        if (!vh.isOK()) {
            throw new NDSException(Resources.getMessage("获取入库通知单失败!"));
        }
        List<SgBPhyInNoticesBoxResult> noticesBillList = vh.getData();
        if (CollectionUtils.isEmpty(noticesBillList)) {
            throw new NDSException(Resources.getMessage("当前入库通知单不存在!"));
        }

        ValueHolderV14 qimenResult = createQimenEntryOrderBox(noticesBillList, user);
        if (!qimenResult.isOK()) {
            throw new NDSException(Resources.getMessage("入库通知单传WMS失败!"));
        }

        List<QimenAsyncMqResult> list = (List<QimenAsyncMqResult>) qimenResult.getData();

        list.forEach(qimenAsyncMqResult -> {
            UpdateWrapper queryWrapper = new UpdateWrapper<SgBPhyInNotices>();
            queryWrapper.eq("bill_no", qimenAsyncMqResult.getBillNo());
            SgBPhyInNotices notices = new SgBPhyInNotices();
            notices.setModifierid(user.getId().longValue());
            notices.setModifiername(user.getName());
            notices.setModifierename(user.getEname());
            notices.setModifieddate(new Date());
            notices.setPassWmsTime(new Date());
            //如果成功!将入库通知单【传WMS状态】改为传WMS中,失败原因置空
            if ("true".equalsIgnoreCase(qimenAsyncMqResult.getIsSend())) {
                notices.setWmsStatus(new Long(SgInNoticeConstants.WMS_UPLOAD_STATUTS_UPLOADING));
                notices.setWmsFailReason("");
                sgBPhyInNoticesMapper.update(notices, queryWrapper);
            } else {
                //如果失败!将入库通知单【传WMS状态】改为未传WMS
                notices.setWmsStatus(new Long(SgInNoticeConstants.WMS_UPLOAD_STATUTS_NO));
                notices.setWmsFailReason(qimenAsyncMqResult.getMessage());
                sgBPhyInNoticesMapper.update(notices, queryWrapper);
            }
        });

        result.setCode(ResultCode.SUCCESS);
        result.setMessage("入库通知单上传WMS成功!");
        return result;
    }

    /**
     * RPC调用接口心
     *
     * @param noticesBillList
     * @return
     */
    private ValueHolderV14 createQimenEntryOrder(List<SgBPhyInNoticesResult> noticesBillList, User user) {

        QimenEntryorderCreateCmd qimenEntryorderCreateCmd = (QimenEntryorderCreateCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(),
                QimenEntryorderCreateCmd.class.getName(),
                "ip",
                "1.4.0");
        List<QimenEntryorderCreateModel> qimenEntryorderList = Lists.newArrayList();

        noticesBillList.forEach(noticesRequest -> {
            SgBPhyInNotices notice = noticesRequest.getNotices();
            QimenEntryorderCreateModel qimenEntryorderCreateModel = new QimenEntryorderCreateModel();
            QimenEntryorderCreateModel.EntryOrder order = new QimenEntryorderCreateModel.EntryOrder();

            //获取收货实体仓账户
            CpCPhyWarehouseMapper warehouseMapper = ApplicationContextHandle.getBean(CpCPhyWarehouseMapper.class);
            CpCPhyWarehouse cpCPhyWarehouse = warehouseMapper.selectById(notice.getCpCPhyWarehouseId());
            String customerId = cpCPhyWarehouse != null ? cpCPhyWarehouse.getWmsAccount() : SgConstants.DEFAULT_INFO_ZERO;
            String cpCPhyWarehouseEcode = cpCPhyWarehouse != null ? cpCPhyWarehouse.getWmsWarehouseCode() : SgConstants.DEFAULT_INFO_ZERO;
                    //单据信息
            order.setEntryOrderCode(notice.getBillNo());
            order.setOwnerCode(notice.getGoodsOwner());
            order.setWarehouseCode(cpCPhyWarehouseEcode);
            order.setOrderType(getOrderType(notice.getSourceBillType(), customerId, notice.getCpCCsEcode()));
            order.setSupplierCode(notice.getCpCCsEcode());
            order.setSupplierName(notice.getCpCCsEname());
            order.setRemark(notice.getRemark());

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            //add by nrl 2019-10-28
            order.setExpectStartTime(sdf.format(notice.getReceiveTime()));//预期到货时间
            order.setOperatorName(SgInNoticeConstants.QM_BILL_USER_ECODE);//操作员名称，SG_B_PHY_IN_NOTICES. MODIFIERID关联查询USERS.ECODE
            //发货人信息
            QimenEntryorderCreateModel.SenderInfo senderInfo = new QimenEntryorderCreateModel.SenderInfo();
            senderInfo.setName(notice.getSendName());
           // senderInfo.setZipCode(MQConstantsIF.DEFAULT_ZIP_CODE);
          //  senderInfo.setTel(jsonAddress.getString("mobile"));
            senderInfo.setMobile(addressforWmsConfig.getMobile());
            senderInfo.setProvince(addressforWmsConfig.getProvince());
            senderInfo.setCity(addressforWmsConfig.getCity());
           // senderInfo.setArea(jsonAddress.getString("area"));
            senderInfo.setDetailAddress(addressforWmsConfig.getDetailAddress());
            log.debug("InNoticeBillNosenderInfo" + notice.getBillNo() + "," + senderInfo);
            //收货人信息
            QimenEntryorderCreateModel.ReceiverInfo receiverInfo = new QimenEntryorderCreateModel.ReceiverInfo();
            receiverInfo.setName(SgConstants.DEFAULT_INFO_ZERO);
            receiverInfo.setMobile(SgConstants.DEFAULT_INFO_ZERO);
            receiverInfo.setProvince(SgConstants.DEFAULT_INFO_ZERO);
            receiverInfo.setCity(SgConstants.DEFAULT_INFO_ZERO);
            receiverInfo.setDetailAddress(SgConstants.DEFAULT_INFO_ZERO);
            //2019-07-11增加传参itemId
            List<SgBPhyInNoticesItem> itemList = noticesRequest.getItemList();
            HashMap<String, String> psToWmsInfo = Maps.newHashMap();
            if (CollectionUtils.isNotEmpty(itemList)) {
                List<PsToWmsRequest> list = Lists.newArrayList();
                for (SgBPhyInNoticesItem phyInNoticesItem : itemList) {
                    PsToWmsRequest psCTowms = new PsToWmsRequest();
                    psCTowms.setCustomerId(customerId);
                    psCTowms.setSku(phyInNoticesItem.getPsCSkuEcode());
                    list.add(psCTowms);
                }
                try {
                    psToWmsInfo = basicPsQueryService.getPsToWmsInfo(list);
                    log.debug("psToWmsInfo:" + psToWmsInfo);
                } catch (Exception e) {
                    log.error("取WMS商品推送表中的itemid异常!" + e.getMessage());
                }
            }

            //设置发货人信息和收货人信息
            order.setSenderInfo(senderInfo);
            order.setReceiverInfo(receiverInfo);
            qimenEntryorderCreateModel.setCustomerId(customerId);
            qimenEntryorderCreateModel.setEntryOrder(order);
            qimenEntryorderCreateModel.setOrderLines(getItemList(notice, itemList, psToWmsInfo));

            qimenEntryorderList.add(qimenEntryorderCreateModel);
        });
        log.debug("PassQimenEntryorderList" + JSONObject.toJSONString(qimenEntryorderList));
        ValueHolderV14 result = qimenEntryorderCreateCmd.asyncCreateQimenEntryOrder(qimenEntryorderList, user, "sg");
        log.debug("Return SgPhyInNoticeSendToWmsTaskService.createQimenEntryOrder. returnParams:params="
                + JSONObject.toJSONString(result) + ";");
        return result;
    }
    /**
     * RPC调用接口心
     *
     * @param noticesBillList
     * @return
     */
    private ValueHolderV14 createQimenEntryOrderBox(List<SgBPhyInNoticesBoxResult> noticesBillList, User user) {
        QimenEntryorderCreateCmd qimenEntryorderCreateCmd = (QimenEntryorderCreateCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(),
                QimenEntryorderCreateCmd.class.getName(),
                "ip",
                "1.4.0");
        CpCPhyWarehouseMapper warehouseMapper = ApplicationContextHandle.getBean(CpCPhyWarehouseMapper.class);
        List<QimenEntryorderCreateModel> qimenEntryorderList = Lists.newArrayList();
        noticesBillList.forEach(noticesRequest -> {
            SgBPhyInNotices notice = noticesRequest.getNotices();
            QimenEntryorderCreateModel qimenEntryorderCreateModel = new QimenEntryorderCreateModel();
            QimenEntryorderCreateModel.EntryOrder order = new QimenEntryorderCreateModel.EntryOrder();

            //获取实体仓账户
            CpCPhyWarehouse cpCPhyWarehouse = warehouseMapper.selectById(notice.getCpCPhyWarehouseId());
            String customerId = cpCPhyWarehouse != null ? cpCPhyWarehouse.getWmsAccount() : SgConstants.DEFAULT_INFO_ZERO;
            String cpCPhyWarehouseEcode = cpCPhyWarehouse != null ? cpCPhyWarehouse.getWmsWarehouseCode() : SgConstants.DEFAULT_INFO_ZERO;
            String cpWarehousecode=cpCPhyWarehouse != null ?cpCPhyWarehouse.getEcode():SgConstants.DEFAULT_INFO_ZERO;

            //单据信息
            order.setEntryOrderCode(notice.getBillNo());
            order.setOwnerCode(notice.getGoodsOwner());
            order.setWarehouseCode(cpCPhyWarehouseEcode);
            order.setOrderType(getOrderType(notice.getSourceBillType(),customerId,notice.getCpCCsEcode()));
            order.setSupplierCode(notice.getCpCCsEcode());
            order.setSupplierName(notice.getCpCCsEname());
            order.setRemark(notice.getRemark());

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            //add by nrl 2019-10-28
            order.setExpectStartTime(notice.getReceiveTime()==null?"":sdf.format(notice.getReceiveTime()));//预期到货时间
            order.setOperatorName(SgInNoticeConstants.QM_BILL_USER_ECODE);//操作员名称，SG_B_PHY_IN_NOTICES. MODIFIERID关联查询USERS.ECODE
            //发货人信息
            QimenEntryorderCreateModel.SenderInfo senderInfo = new QimenEntryorderCreateModel.SenderInfo();
            senderInfo.setCompany(MQConstantsIF.DEFAULT_NAME);
            //senderInfo.setName(MQConstantsIF.DEFAULT_NAME);
            senderInfo.setName(notice.getSendName());
            senderInfo.setZipCode(MQConstantsIF.DEFAULT_ZIP_CODE);
            senderInfo.setTel(MQConstantsIF.DEFAULT_TEL);
            senderInfo.setMobile(MQConstantsIF.DEFAULT_MOBILE);
            senderInfo.setProvince(MQConstantsIF.DEFAULT_PROVINCE);
            senderInfo.setCity(MQConstantsIF.DEFAULT_CITY);
            senderInfo.setArea(notice.getCpCRegionAreaEcode());
            senderInfo.setDetailAddress(MQConstantsIF.DEFAULT_DETAIL_ADDRESS);
            log.debug("InNoticeBillNosenderInfo" + notice.getBillNo() + "," + senderInfo);
            //收货人信息
            QimenEntryorderCreateModel.ReceiverInfo receiverInfo = new QimenEntryorderCreateModel.ReceiverInfo();
            receiverInfo.setName(addressforWmsConfig.getName());
            receiverInfo.setMobile(addressforWmsConfig.getMobile());
            receiverInfo.setProvince(addressforWmsConfig.getProvince());
            receiverInfo.setCity(addressforWmsConfig.getCity());
            receiverInfo.setDetailAddress(addressforWmsConfig.getDetailAddress());
            //2019-07-11增加传参itemId
            List<SgBPhyInNoticesImpItem> itemList = noticesRequest.getItemList();
            HashMap<String, String> psToWmsInfo = Maps.newHashMap();
            if (CollectionUtils.isNotEmpty(itemList)) {
                List<PsToWmsRequest> list = Lists.newArrayList();
                for (SgBPhyInNoticesImpItem phyInNoticesItem : itemList) {
                    PsToWmsRequest psCTowms = new PsToWmsRequest();
                    psCTowms.setCustomerId(customerId);
                    psCTowms.setSku(phyInNoticesItem.getPsCSkuEcode());
                    list.add(psCTowms);
                }
                try {
                    psToWmsInfo = basicPsQueryService.getPsToWmsInfo(list);
                    log.debug("psToWmsInfo:" + psToWmsInfo);
                } catch (Exception e) {
                    log.error("取WMS商品推送表中的itemid异常!" + e.getMessage());
                }
            }

            //设置发货人信息和收货人信息
            order.setSenderInfo(senderInfo);
            order.setReceiverInfo(receiverInfo);
            qimenEntryorderCreateModel.setCustomerId(customerId);
            qimenEntryorderCreateModel.setEntryOrder(order);
            qimenEntryorderCreateModel.setOrderLines(getItemListBox(notice, itemList, psToWmsInfo));
            Map<Object, Object> extendProps = Maps.newHashMap();
            extendProps.put("storageplace",cpWarehousecode);//实体仓编码
            extendProps.put("partsQty",notice.getTotQty());//总数量
            qimenEntryorderCreateModel.setExtendProps(extendProps);
            qimenEntryorderList.add(qimenEntryorderCreateModel);
        });
        log.debug("PassQimenEntryorderList" + JSONObject.toJSONString(qimenEntryorderList));
        ValueHolderV14 result = qimenEntryorderCreateCmd.asyncCreateQimenEntryOrder(qimenEntryorderList, user, "sg");
        log.debug("Return SgPhyInNoticeSendToWmsTaskService.createQimenEntryOrder. returnParams:params="
                + JSONObject.toJSONString(result) + ";");
        return result;
    }

    /**
     * 设置明细数据
     *
     * @param notice
     * @param itemList
     * @param psToWmsInfo
     * @return
     */
    private List<QimenEntryorderCreateModel.OrderLine> getItemList(SgBPhyInNotices notice, List<SgBPhyInNoticesItem> itemList, HashMap<String, String> psToWmsInfo) {
        List<QimenEntryorderCreateModel.OrderLine> OrderLineList = Lists.newArrayList();
        for (SgBPhyInNoticesItem item : itemList) {
            QimenEntryorderCreateModel.OrderLine orderLine = new QimenEntryorderCreateModel.OrderLine();
            orderLine.setOwnerCode(notice.getGoodsOwner());
            orderLine.setItemCode(item.getPsCSkuEcode());
            BigDecimal qty = Optional.ofNullable(item.getQty()).orElse(BigDecimal.ZERO);
            if (qty.compareTo(BigDecimal.ZERO) == 0) {
                continue;
            }
            orderLine.setPlanQty(qty.longValue());
            orderLine.setInventoryType(SgConstantsIF.TRADE_MARK_ZP);
            String itemId = psToWmsInfo.get(item.getPsCSkuEcode());
            orderLine.setItemId(itemId);
            OrderLineList.add(orderLine);
        }
        return OrderLineList;
    }
    /**
     * 设置明细数据
     *
     * @param notice
     * @param itemList
     * @param psToWmsInfo
     * @return
     */
    private List<QimenEntryorderCreateModel.OrderLine> getItemListBox(SgBPhyInNotices notice, List<SgBPhyInNoticesImpItem> itemList, HashMap<String, String> psToWmsInfo) {
        List<QimenEntryorderCreateModel.OrderLine> OrderLineList = Lists.newArrayList();
        itemList.forEach(item -> {
            QimenEntryorderCreateModel.OrderLine orderLine = new QimenEntryorderCreateModel.OrderLine();
            orderLine.setOwnerCode(notice.getGoodsOwner());
            orderLine.setItemCode(item.getPsCSkuEcode());
            BigDecimal qty = Optional.ofNullable(item.getQty()).orElse(BigDecimal.ZERO);
            if (qty.compareTo(BigDecimal.ZERO) == 0) {
                return;
            }
            orderLine.setPlanQty(qty.longValue());
            orderLine.setInventoryType(SgConstantsIF.TRADE_MARK_ZP);
            orderLine.setProduceCode(item.getPsCTeusEcode());//箱号
            orderLine.setOrderLineNo(item.getId().toString());
            String itemId = psToWmsInfo.get(item.getPsCSkuEcode());
            orderLine.setItemId(itemId);
            OrderLineList.add(orderLine);
        });
        return OrderLineList;
    }


    /**
     * 获取奇门需要的订单类型
     *
     * @param sourceBillType
     * @param destCustomerId     收货方customerid
     * @param origWareHouseEcode 发货实体仓ecode
     * @return
     */
    private String getOrderType(Integer sourceBillType, String destCustomerId, String origWareHouseEcode) {
        String orderType = SgInterfaceConstants.QIMEN_ORDER_TYPE_B2BRK;
        switch (sourceBillType) {
            case SgConstantsIF.BILL_TYPE_SALE_REF:
                orderType = SgInterfaceConstants.QIMEN_ORDER_TYPE_B2BRK;
                break;
            case SgConstantsIF.BILL_TYPE_PUR:
                orderType = SgInterfaceConstants.QIMEN_ORDER_TYPE_CGRK;
                break;
            case SgConstantsIF.BILL_TYPE_TRANSFER:
                orderType = getOrderTypeForTransfer(destCustomerId, origWareHouseEcode);
                //orderType = SgInterfaceConstants.QIMEN_ORDER_TYPE_DBRK;
                break;
        }
        return orderType;
    }

    /**
     * 特殊处理调拨单类型，获取奇门所需的订单类型
     *
     * @param destCustomerId     收货方customerid
     * @param origWareHouseEcode 发货实体仓Ecode
     * @return
     * @description: 判断  系统参数：菜鸟仓customerid是否为空，若为空 orderType传 DBRK ，若不为空，则判断
     * 【收货店仓对应customerid 是否为 系统参数维护值， 发货店仓对应的customerid 不为 系统参数维护值】，
     * 若判断结果为是 orderType传CGRK， 判断结果为否 传DBRK
     */
    private String getOrderTypeForTransfer(String destCustomerId, String origWareHouseEcode) {
        String orderType = SgInterfaceConstants.QIMEN_ORDER_TYPE_DBRK;
        try {
            //系统参数-菜鸟customerid
            String sysCustoemerId = AdParamUtil.getParam(SgOutConstants.SYSTEM_WAREHOUSE_CUSTOMERID);
            log.debug("大货入库传WMS获取菜鸟仓customerid:" + sysCustoemerId + ",发货实体仓ecode:" + origWareHouseEcode);
            if (StringUtils.isNotEmpty(sysCustoemerId) &&
                    StringUtils.isNotEmpty(destCustomerId) &&
                    sysCustoemerId.equals(destCustomerId)) {
                if (StringUtils.isNotEmpty(origWareHouseEcode)) {
                    //获取发货实体仓customreid
                    CpCPhyWarehouseMapper warehouseMapper = ApplicationContextHandle.getBean(CpCPhyWarehouseMapper.class);
                    List<CpCPhyWarehouse> warehouses = warehouseMapper.selectList(new QueryWrapper<CpCPhyWarehouse>().lambda()
                            .eq(CpCPhyWarehouse::getEcode, origWareHouseEcode)
                            .eq(CpCPhyWarehouse::getIsactive, SgConstants.IS_ACTIVE_Y));
                    if (CollectionUtils.isNotEmpty(warehouses)) {
                        String origCustomerId = warehouses.get(0).getWmsAccount();
                        log.debug("大货入库传WMS获取发货实体仓customerid" + origCustomerId);
                        if (StringUtils.isNotEmpty(origCustomerId)) {
                            //发货方customerid不等于菜鸟仓customerid
                            if (!sysCustoemerId.equals(origCustomerId)) {
                                orderType = SgInterfaceConstants.QIMEN_ORDER_TYPE_CGRK;
                            }
                        } else {
                            //发货方对应的实体仓档案中customerid为空
                            orderType = SgInterfaceConstants.QIMEN_ORDER_TYPE_CGRK;
                        }
                    } else {
                        //发货方在实体仓档案中没有记录
                        orderType = SgInterfaceConstants.QIMEN_ORDER_TYPE_CGRK;
                    }
                } else {
                    //通知单发货方ecode为空
                    orderType = SgInterfaceConstants.QIMEN_ORDER_TYPE_CGRK;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("系统参数菜鸟仓customerid获取异常!" + e.getMessage());
        }
        return orderType;
    }
}
