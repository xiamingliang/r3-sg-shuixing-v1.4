package com.jackrain.nea.sg.itface.listener;

import com.aliyun.openservices.ons.api.Action;
import com.aliyun.openservices.ons.api.ConsumeContext;
import com.aliyun.openservices.ons.api.Message;
import com.aliyun.openservices.ons.api.MessageListener;
import com.jackrain.nea.sg.itface.consumer.SgBChannelStorageBufferConsumer;
import com.jackrain.nea.sg.itface.out.common.MQConstantsIF;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author caopengflying
 * @time 2019/4/22 22:00
 */
@Slf4j
@Component
public class SGMessageRouteListener implements MessageListener {

    @Autowired
    private SgBChannelStorageBufferConsumer sgBChannelStorageBufferConsumer;


    @Override
    public Action consume(Message message, ConsumeContext consumeContext) {
        String messageTag = message.getTag();
        //渠道库存计算池mq消费tag
        if (MQConstantsIF.SGB_CHANNEL_STORAGE_BUFFER_CONSUMER_TAGS.contains(messageTag)) {
            return sgBChannelStorageBufferConsumer.consume(message);
        }
        return Action.ReconsumeLater;
    }

}
