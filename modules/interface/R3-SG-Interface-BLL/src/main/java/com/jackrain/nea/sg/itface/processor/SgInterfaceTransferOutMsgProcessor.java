package com.jackrain.nea.sg.itface.processor;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.r3.mq.exception.ProcessMqException;
import com.jackrain.nea.r3.mq.processor.AbstractMqProcessor;
import com.jackrain.nea.r3.mq.processor.MqProcessResult;
import com.jackrain.nea.sg.basic.utils.StorageUtils;
import com.jackrain.nea.sg.itface.out.common.MQConstantsIF;
import com.jackrain.nea.sg.out.model.request.SgOutResultMQRequest;
import com.jackrain.nea.sg.transfer.services.SgTransferOutResultService;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.face.impl.UserImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

/**
 * @author csy
 * create at : 2019/5/21 17:58
 */
@Slf4j
@Component
public class SgInterfaceTransferOutMsgProcessor extends AbstractMqProcessor {
    @Override
    public MqProcessResult startProcess(String messageTopic, String messageKey, String messageBody, String messageTag) throws ProcessMqException {
        try {
            SgOutResultMQRequest sendMsgRequest = JSON.parseObject(messageBody, SgOutResultMQRequest.class);
            if (sendMsgRequest == null) {
                return new MqProcessResult(false, Resources.getMessage("messageBody解析为null"));
            }
            JSONObject jo = JSON.parseObject(messageBody);
            User user = JSON.parseObject(jo.getString("loginUser"), UserImpl.class);
            sendMsgRequest.setLoginUser(user);

            SgTransferOutResultService service = ApplicationContextHandle.getBean(SgTransferOutResultService.class);
            ValueHolderV14 v14 = service.writeBackOutResult(sendMsgRequest, false);
            if (v14.getCode() == ResultCode.SUCCESS) {
                return new MqProcessResult(false, Resources.getMessage(v14.getMessage()));
            } else {
                return new MqProcessResult(true, Resources.getMessage(v14.getMessage()));
            }

        } catch (Exception e) {
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + ",startProcess error:" + StorageUtils.getExceptionMsg(e));
            }
            return new MqProcessResult(true, Resources.getMessage("处理内容非法：" + StorageUtils.getExceptionMsg(e)));
        }
    }

    @Override
    public boolean checkCanExecuteProcess(String messageTopic, String messageKey, String messageBody, String messageTag) {
        log.info(this.getClass().getName() + ".debug,接收出库结果单消息：" +
                        "messageTopic{}," +
                        "messageTag{}," +
                        "messageKey{}," +
                        "messageBody{}",
                new Object[]{messageTopic, messageTag, messageKey, messageBody});
        return StringUtils.equalsIgnoreCase(messageTag, MQConstantsIF.TAG_OUT_TRANSFER_VERIFY_BACK);
    }

    @Override
    public void initialMqOrderProcessor(ApplicationContext applicationContext) {
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ",start");
        }
    }
}
