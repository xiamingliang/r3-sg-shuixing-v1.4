package com.jackrain.nea.sg.itface.consumer;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.sg.basic.common.SgResultCode;
import com.jackrain.nea.sg.out.model.request.SgPhyOutNoticesBillWMSBackRequest;
import com.jackrain.nea.sg.out.services.SgPhyOutNoticesSaveWMSBackService;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author caopengflying
 * @time 2019/5/6 17:52
 * @descript 新增出库通知单并传WMS服务 - 回执
 */
@Slf4j
@Component
public class WMSCallDeliveryOrderConsumer {
    @Autowired
    private SgPhyOutNoticesSaveWMSBackService sgPhyOutNoticesSaveWMSBackService;

    /**
     *
     * @param messageBody
     *         {
     *           "method": "taobao.qimen.deliveryorder.create",
     *           "data": [{
     *             "code": 0,
     *             "message": "",
     *             "orderNo": "test001"
     *           }]
     *         }
     * @return
     */
    public Boolean consume(String messageBody){
        try {
            List<SgPhyOutNoticesBillWMSBackRequest> requests =
                    JSON.parseArray(JSONObject.parseObject(messageBody).getJSONArray("data").toJSONString(),
                    SgPhyOutNoticesBillWMSBackRequest.class);

            ValueHolderV14 holder = sgPhyOutNoticesSaveWMSBackService.saveWMSBackOutNotices(requests,
                    WMSUserFactory.initWmsUser(null, null));
            if (SgResultCode.isFail(holder)){
                log.error(this.getClass().getName() + " 新增出库通知单并传WMS服务失败", holder.getMessage());
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(this.getClass().getName() + " 新增出库通知单并传WMS服务异常", e);
            return false;
        }
        return true;
    }
}
