package com.jackrain.nea.sg.itface.processor;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.r3.mq.exception.ProcessMqException;
import com.jackrain.nea.r3.mq.processor.AbstractMqProcessor;
import com.jackrain.nea.r3.mq.processor.MqProcessResult;
import com.jackrain.nea.sg.itface.out.common.MQConstantsIF;
import com.jackrain.nea.sg.out.model.request.SgPhyOutNoticesBillSaveRequest;
import com.jackrain.nea.sg.out.model.request.SgPhyOutNoticesSaveRequest;
import com.jackrain.nea.sg.out.model.result.SgOutNoticesAndWMSMqResult;
import com.jackrain.nea.sg.out.model.result.SgOutNoticesResult;
import com.jackrain.nea.sg.out.services.SgPhyOutNoticesBatchSaveAndWMSService;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.face.impl.UserImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author: 舒威
 * @since: 2019/8/5
 * create at : 2019/8/5 14:12
 */
@Slf4j
@Component
public class SgInterfaceOutNoticesAndWMSProcessor extends AbstractMqProcessor {

    @Override
    public MqProcessResult startProcess(String messageTopic, String messageKey, String messageBody, String messageTag) throws ProcessMqException {
        try {
            long start = System.currentTimeMillis();
            SgOutNoticesAndWMSMqResult sendMsgRequest = JSON.parseObject(messageBody, SgOutNoticesAndWMSMqResult.class);

            if (sendMsgRequest == null) {
                return new MqProcessResult(false, Resources.getMessage("messageBody解析为null"));
            }

            List<SgPhyOutNoticesBillSaveRequest> noticesBills = sendMsgRequest.getNoticesBills();
            if (CollectionUtils.isEmpty(noticesBills)) {
                return new MqProcessResult(false, Resources.getMessage("出库通知单为null"));
            }

            List<SgPhyOutNoticesBillSaveRequest> filters = noticesBills.stream().filter(s -> s.getOutNoticesRequest() == null).collect(Collectors.toList());
            if (CollectionUtils.isNotEmpty(filters)) {
                return new MqProcessResult(false, Resources.getMessage("存在出库通知单主表信息为null"));
            }

            List<SgPhyOutNoticesSaveRequest> notices = noticesBills.stream().map(SgPhyOutNoticesBillSaveRequest::getOutNoticesRequest).collect(Collectors.toList());
            Set<Long> set = notices.stream().map(SgPhyOutNoticesSaveRequest::getSourceBillId).collect(Collectors.toSet());
            if (CollectionUtils.isEmpty(set)) {
                return new MqProcessResult(false, Resources.getMessage("出库通知单来源单据id为null"));
            }

            List<Long> sourceBillIds = new ArrayList<>(set);

            if (log.isDebugEnabled()) {
                log.debug("MQ新增出库通知单并传wms来源单据id集合" + sourceBillIds);
            }

            JSONObject jo = JSON.parseObject(messageBody);
            User user = JSON.parseObject(jo.getString("loginUser"), UserImpl.class);
            noticesBills.forEach(var -> var.setLoginUser(user));

            SgPhyOutNoticesBatchSaveAndWMSService noticesBatchSaveAndWMSService = ApplicationContextHandle.getBean(SgPhyOutNoticesBatchSaveAndWMSService.class);
            ValueHolderV14<List<SgOutNoticesResult>> result = noticesBatchSaveAndWMSService.batchSaveOutNoticesAndWMS(noticesBills,sourceBillIds, user);
            List<SgOutNoticesResult> data = result.getData();
            if (log.isDebugEnabled()) {
                log.debug("MQ新增出库通知单并传wms服务出参:{},总耗时:{}ms;", JSONObject.toJSONString(data), System.currentTimeMillis() - start);
            }
            if (result.isOK()) {
                return new MqProcessResult(false, Resources.getMessage(result.getMessage()));
            } else {
                return new MqProcessResult(true, Resources.getMessage(result.getMessage()));
            }
        } catch (Exception e) {
            log.error(this.getClass().getName() + ".error startProcess", e);
            return new MqProcessResult(true, Resources.getMessage("处理内容非法：" + e.getMessage()));
        }
    }

    @Override
    public boolean checkCanExecuteProcess(String messageTopic, String messageKey, String messageBody, String messageTag) {
        log.info(this.getClass().getName() + ".debug,接收新增出库通知单并传WMS服务消息：" +
                        "messageTopic{}," +
                        "messageTag{}," +
                        "messageKey{}," +
                        "messageBody{}" +
                        "messageBody大小{}",
                new Object[]{messageTopic, messageTag, messageKey, messageBody, messageBody.getBytes().length});
        return StringUtils.equalsIgnoreCase(messageTag, MQConstantsIF.OMS_TO_SG_OUT_NOTICES_TO_WMS);
    }

    @Override
    public void initialMqOrderProcessor(ApplicationContext applicationContext) {
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ".start");
        }
    }
}
