package com.jackrain.nea.sg.itface.config;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.model.util.AdParamUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @ Author     ：nrl
 * @ Date       ：Created in 14:25 2019/10/31
 * @ Description：${description}
 * @ Modified By：
 * @Version: $version$
 */
@Slf4j
@Component
public class AddressforWmsConfig {
    JSONObject jsonAddress;


    public String getName() {
        jsonAddress= JSON.parseObject(AdParamUtil.getParam("AddressForWMS"));
        return jsonAddress.getString("name");
    }

    private String name;

    public String getMobile() {
        jsonAddress= JSON.parseObject(AdParamUtil.getParam("AddressForWMS"));
        return jsonAddress.getString("mobile");
    }

    private String mobile;

    public String getProvince() {
        jsonAddress= JSON.parseObject(AdParamUtil.getParam("AddressForWMS"));
        return jsonAddress.getString("province");
    }

    private String province;

    public String getCity() {
        jsonAddress= JSON.parseObject(AdParamUtil.getParam("AddressForWMS"));
        return jsonAddress.getString("city");
    }

    private String city;

    public String getDetailAddress() {
        jsonAddress= JSON.parseObject(AdParamUtil.getParam("AddressForWMS"));
        return jsonAddress.getString("detailAddress");
    }

    private String detailAddress;
}