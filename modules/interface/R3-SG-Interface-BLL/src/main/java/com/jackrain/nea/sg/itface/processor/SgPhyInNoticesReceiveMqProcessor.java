package com.jackrain.nea.sg.itface.processor;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.r3.mq.exception.ProcessMqException;
import com.jackrain.nea.r3.mq.processor.AbstractMqProcessor;
import com.jackrain.nea.r3.mq.processor.MqProcessResult;
import com.jackrain.nea.sg.in.model.result.ReturnSgPhyInResult;
import com.jackrain.nea.sg.in.services.SgPhyInNoticesSendMQService;
import com.jackrain.nea.sg.itface.out.common.MQConstantsIF;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * 入库通知单发mq
 * 生成入库结果单
 */
@Slf4j
@Component
public class SgPhyInNoticesReceiveMqProcessor extends AbstractMqProcessor {

    @Autowired
    private SgPhyInNoticesSendMQService sgPhyInNoticesSendMQService;

    @Override
    public MqProcessResult startProcess(String messageTopic, String messageKey, String messageBody, String messageTag) throws ProcessMqException {
        log.debug("SgPhyInNoticesReceiveMqProcessor.startProcess");
        try {
            List<Long> billIdList = JSONObject.parseArray(messageBody,Long.class);
            if(CollectionUtils.isEmpty(billIdList)){
                return new MqProcessResult(false, Resources.getMessage("messageBody解析为null"));
            }
            //新增并审核入库结果单
            ValueHolderV14 result = sgPhyInNoticesSendMQService.saveSgBPhyInResult(billIdList);
            List<ReturnSgPhyInResult> list= (List<ReturnSgPhyInResult>) result.getData();

            list.forEach(returnResult->{
                sgPhyInNoticesSendMQService.updateNoticesByMq(returnResult);
            });
            if (result.getCode() == ResultCode.SUCCESS) {
                return new MqProcessResult(false, Resources.getMessage(result.getMessage()));
            } else {
                return new MqProcessResult(true, Resources.getMessage(result.getMessage()));
            }

        } catch (Exception e) {
            log.error(this.getClass().getName() + ".error startProcess", e);
            return new MqProcessResult(true, Resources.getMessage("处理内容非法：" + e.getMessage()));
        }
    }


    @Override
    public boolean checkCanExecuteProcess(String messageTopic, String messageKey, String messageBody, String messageTag) {
        log.info(this.getClass().getName() + ".debug,接收入库通知单消息：" +
                        "messageTopic{}," +
                        "messageTag{}," +
                        "messageKey{}," +
                        "messageBody{}",
                new Object[]{messageTopic, messageTag, messageKey, messageBody});
        return StringUtils.equalsIgnoreCase(messageTag, MQConstantsIF.SGB_CHANNEL_SG_IN_CONSUMER_TAGS);
    }

    @Override
    public void initialMqOrderProcessor(ApplicationContext applicationContext) {
        if (log.isDebugEnabled()) {
            log.debug( "SgPhyInNoticesReceiveMqProcessor.Start");
        }
    }
}
