package com.jackrain.nea.sg.itface.processor.erp;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.r3.mq.processor.AbstractMqProcessor;
import com.jackrain.nea.r3.mq.processor.MqProcessResult;
import com.jackrain.nea.sg.transfer.erp.SgTransferOutCallBack;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.utils.BillType;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

/**
 * @description 调拨单退货出库
 * @author Micheal
 * @version 1.0
 * @date 2020/4/8 16:22
 */
@Slf4j
@Component
public class SgTransferOutStockErpProcessor extends AbstractMqProcessor {

    @Override
    public boolean checkCanExecuteProcess(String messageTopic, String messageKey, String messageBody, String messageTag) {
        log.info(this.getClass().getName() + ",接收退货调拨调拨出库erp回传消息:" +
                        "messageTopic{}," +
                        "messageTag{}," +
                        "messageKey{}," +
                        "messageBody{}",
                new Object[]{messageTopic, messageTag, messageKey, messageBody});
        return StringUtils.equalsIgnoreCase(messageTag, BillType.REFUND_TRANSFER_OUT_SYNC.getBillTag());
    }


    @Override
    public void initialMqOrderProcessor(ApplicationContext applicationContext) {
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ",进入调拨出库erp回传服务");
        }
    }

    @Override
    public MqProcessResult startProcess(String messageTopic, String messageKey, String messageBody, String messageTag) {

        // 获取消息体
        JSONObject param = JSON.parseObject(messageBody);
        if (param == null) {
            return new MqProcessResult(false, Resources.getMessage("messageBody解析为null"));
        }

        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ",调退货调拨拨出库erp回传消息体:" + JSON.toJSONString(param));
        }

        boolean success = param.getBooleanValue("SUCCESS");
        if (success) {
            // 调拨单ID
            Long objId = param.getLong("SOURCE_BILL_NO");
            // 销退单号（erp）
            JSONObject erpBackContent = param.getJSONObject("ERP_BACK_CONTENT");
            String erpBillNo = erpBackContent.getString("BILL_NO");
            SgTransferOutCallBack callBack = ApplicationContextHandle.getBean(SgTransferOutCallBack.class);
            callBack.updateTransferErpBillNo(objId, erpBillNo,false);
        }

        return new MqProcessResult(false, Resources.getMessage("消费成功！"));
    }

}
