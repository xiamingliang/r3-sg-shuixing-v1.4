package com.jackrain.nea.sg.itface.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.google.common.collect.Maps;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.data.basic.services.BasicPsQueryService;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.ip.api.qimen.QmStockoutCreateCmd;
import com.jackrain.nea.ip.model.qimen.QmStockoutCreateModel;
import com.jackrain.nea.ip.model.result.QimenAsyncMqResult;
import com.jackrain.nea.model.util.AdParamUtil;
import com.jackrain.nea.psext.request.PsToWmsRequest;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.common.SystemUserResource;
import com.jackrain.nea.sg.basic.config.SgStorageBoxConfig;
import com.jackrain.nea.sg.basic.mapper.CpCPhyWarehouseMapper;
import com.jackrain.nea.sg.basic.model.table.CpCPhyWarehouse;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.itface.config.AddressforWmsConfig;
import com.jackrain.nea.sg.itface.mapper.SgBigPhyOutNoticesToWmsMapper;
import com.jackrain.nea.sg.out.common.SgOutConstants;
import com.jackrain.nea.sg.out.common.SgOutConstantsIF;
import com.jackrain.nea.sg.out.mapper.SgBPhyOutNoticesImpItemMapper;
import com.jackrain.nea.sg.out.mapper.SgBPhyOutNoticesItemMapper;
import com.jackrain.nea.sg.out.mapper.SgBPhyOutNoticesTeusItemMapper;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutNotices;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutNoticesImpItem;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutNoticesItem;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutNoticesTeusItem;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.DateUtil;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author zhu lin yu
 * @since 2019/5/6
 * create at : 2019/5/6 15:12
 */
@Slf4j
@Component
public class SgBigPhyOutNoticesToWmsService {

    @Value("${lts.TimedReturnOrderTask.range}")
    private Integer range;

    @Autowired
    private SgBPhyOutNoticesItemMapper sgBPhyOutNoticesItemMapper;

    @Autowired
    private SgBigPhyOutNoticesToWmsMapper sgBigPhyOutNoticesToWmsMapper;

    @Autowired
    private CpCPhyWarehouseMapper CpCPhyWarehouseMapper;

    @Autowired
    private BasicPsQueryService basicPsQueryService;

    @Autowired
    private SgBPhyOutNoticesImpItemMapper sgBPhyOutNoticesImpItemMapper;

    @Autowired
    private SgBPhyOutNoticesTeusItemMapper sgBPhyOutNoticesTeusItemMapper;

    @Autowired
    private SgStorageBoxConfig storageBoxConfig;

    @Autowired
    private AddressforWmsConfig addressforWmsConfig;

    /**
     * 大货出库传wms
     *
     * @return
     */
    public ValueHolderV14 timerQueryBigPhyOutNoticesTask() {

        User rootUser = SystemUserResource.getRootUser();

        ValueHolderV14 valueHolder = new ValueHolderV14();

        if (log.isDebugEnabled()) {
            log.debug("=============timerQueryBigPhyOutNoticesTask beginning=====================");
        }

        //查询出库通知单主表信息
        List<SgBPhyOutNotices> sgBPhyOutNoticeList = this.sgBigPhyOutNoticesToWmsMapper.selectPhyOutNoticesInfo(range);

        //记录是否查询到出库通知单信息
        if (CollectionUtils.isEmpty(sgBPhyOutNoticeList)) {
            throw new NDSException(Resources.getMessage("未查到出库通知单"));
        }

        //封装出库创建接口参数
        List<QmStockoutCreateModel> stockoutCreateModelList = new ArrayList<>();
        HashMap<String, SgBPhyOutNotices> noticesMap = Maps.newHashMap();

        for (SgBPhyOutNotices model : sgBPhyOutNoticeList) {
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + " 大货出库通知单信息{}", JSONObject.toJSONString(model));
            }
            noticesMap.put(model.getBillNo(), model);
            //将出库通知单更新为传WMS中
            SgBPhyOutNotices notices = new SgBPhyOutNotices();
            notices.setId(model.getId());
            notices.setWmsStatus(SgOutConstantsIF.WMS_STATUS_PASSING);
            notices.setModifierename(rootUser.getEname());
            StorageESUtils.setBModelDefalutDataByUpdate(notices, rootUser);
            sgBigPhyOutNoticesToWmsMapper.updateById(notices);

            QmStockoutCreateModel stockoutCreateModel = new QmStockoutCreateModel();

            //封装CustomerId
            CpCPhyWarehouse cpCPhyWarehouse = this.CpCPhyWarehouseMapper.selectById(model.getCpCPhyWarehouseId());
            stockoutCreateModel.setCustomerId(cpCPhyWarehouse.getWmsAccount());

            //封装出库通知单主表信息
            QmStockoutCreateModel.DeliveryOrder deliveryOrder = new QmStockoutCreateModel.DeliveryOrder();
            deliveryOrder.setDeliveryOrderCode(model.getBillNo());
            Integer sourceType = model.getSourceBillType();
            String sourceBillType = changeNumToWord(sourceType, cpCPhyWarehouse.getWmsAccount(), model.getCpCCsEcode());
            deliveryOrder.setOrderType(sourceBillType);
            deliveryOrder.setWarehouseCode(cpCPhyWarehouse.getWmsWarehouseCode());
            deliveryOrder.setCreateTime(DateUtil.getDateTime());
            deliveryOrder.setScheduleDate(DateUtil.format(model.getPreTime(), "yyyy-MM-dd"));
            deliveryOrder.setSupplierCode(model.getCpCCsEcode());
            deliveryOrder.setSupplierName(model.getCpCCsEname());
            deliveryOrder.setSourcePlatformCode(SgOutConstants.QM_DEFAULT_PLATFORM_CODE);
            deliveryOrder.setLogisticsCode(model.getCpCLogisticsEcode());
            deliveryOrder.setLogisticsName(model.getCpCLogisticsEname());
            deliveryOrder.setRemark(model.getRemark());

            //封装deliveryOrder中的senderInfo
            QmStockoutCreateModel.SenderInfo senderInfo = new QmStockoutCreateModel.SenderInfo();
            //发货信息

            senderInfo.setName(addressforWmsConfig.getName());
            //  senderInfo.setZipCode(MQConstantsIF.DEFAULT_ZIP_CODE);
            //  senderInfo.setTel(MQConstantsIF.DEFAULT_TEL);
            senderInfo.setMobile(addressforWmsConfig.getMobile());
            senderInfo.setProvince(addressforWmsConfig.getProvince());
            senderInfo.setCity(addressforWmsConfig.getCity());
            //senderInfo.setArea(addressforWmsConfig.get);
            senderInfo.setDetailAddress(addressforWmsConfig.getDetailAddress());

            deliveryOrder.setSenderInfo(senderInfo);

            //封装deliveryOrder中的receiverInfo
            QmStockoutCreateModel.ReceiverInfo receiverInfo = new QmStockoutCreateModel.ReceiverInfo();
            receiverInfo.setName(model.getReceiverName() == null ? SgConstants.DEFAULT_INFO_ZERO : model.getReceiverName());
            receiverInfo.setZipCode(model.getReceiverZip() == null ? SgConstants.DEFAULT_INFO_ZERO : model.getReceiverZip());
            receiverInfo.setTel(model.getReceiverPhone() == null ? SgConstants.DEFAULT_INFO_ZERO : model.getReceiverPhone());
            receiverInfo.setMobile(model.getReceiverMobile() == null ? SgConstants.DEFAULT_INFO_ZERO : model.getReceiverMobile());
            receiverInfo.setProvince(model.getCpCRegionProvinceEname() == null ? SgConstants.DEFAULT_INFO_ZERO : model.getCpCRegionProvinceEname());
            receiverInfo.setCity(model.getCpCRegionCityEname() == null ? SgConstants.DEFAULT_INFO_ZERO : model.getCpCRegionCityEname());
            receiverInfo.setArea(model.getCpCRegionAreaEname() == null ? SgConstants.DEFAULT_INFO_ZERO : model.getCpCRegionAreaEname());
            receiverInfo.setDetailAddress(model.getReceiverAddress() == null ? "" : model.getReceiverAddress());

            deliveryOrder.setReceiverInfo(receiverInfo);
            stockoutCreateModel.setDeliveryOrder(deliveryOrder);

            //查询出库通知单明细表信息
            if (storageBoxConfig.getBoxEnable()) {
                stockoutCreateModel.setOrderLines(getNoticeItemBox(model, model.getGoodsOwner(), cpCPhyWarehouse.getWmsAccount()));
            } else {
                QueryWrapper<SgBPhyOutNoticesItem> queryItem = new QueryWrapper<>();
                queryItem.eq(SgOutConstants.OUT_NOTICES_PAREN_FIELD, model.getId());
                List<SgBPhyOutNoticesItem> phyOutNoticesItemList = this.sgBPhyOutNoticesItemMapper.selectList(queryItem);

                //封装List<QmStockoutCreateModel.OrderLine>
                String goodsOwner = model.getGoodsOwner();
                List<QmStockoutCreateModel.OrderLine> orderLines = new ArrayList<>();

                phyOutNoticesItemList.forEach(noticesItem -> {
                    QmStockoutCreateModel.OrderLine orderLine = new QmStockoutCreateModel.OrderLine();
                    orderLine.setOwnerCode(goodsOwner);
                    orderLine.setItemCode(noticesItem.getPsCSkuEcode());
                    if (model.getSgBOutPropId() != null && model.getSgBOutPropId() == 2) {
                        orderLine.setInventoryType(SgConstantsIF.TRADE_MARK_CC);
                    } else {
                        orderLine.setInventoryType(SgConstantsIF.TRADE_MARK_ZP);
                    }
                    List<PsToWmsRequest> psToWmsRequestList = new ArrayList<>();

                    PsToWmsRequest psToWmsRequest = new PsToWmsRequest();
                    psToWmsRequest.setSku(noticesItem.getPsCSkuEcode());
                    psToWmsRequest.setCustomerId(cpCPhyWarehouse.getWmsAccount());
                    psToWmsRequestList.add(psToWmsRequest);

                    try {
                        HashMap<String, String> psToWmsInfo = basicPsQueryService.getPsToWmsInfo(psToWmsRequestList);
                        orderLine.setItemId(psToWmsInfo.get(noticesItem.getPsCSkuEcode()));
                    } catch (Exception e) {
                        log.error("调用basicPsQueryService.getPsToWmsInfo失败");
                        orderLine.setItemId(null);
                    }

                    orderLine.setPlanQty(noticesItem.getQty() == null ? "" : noticesItem.getQty().setScale(0, BigDecimal.ROUND_HALF_UP).toString());
                    orderLines.add(orderLine);
                });
                stockoutCreateModel.setOrderLines(orderLines);
            }
            //封装扩展属性
            Map<Object, Object> extendProps = new HashMap<>();
            extendProps.put("brand", model.getPsCBrandEname());
            if (SgConstantsIF.BILL_TYPE_VIPSHOP == model.getSourceBillType()) {
                extendProps.put("vop", 1);
            } else {
                extendProps.put("vop", 0);
            }
            extendProps.put("arrival_time", DateUtil.format(model.getDateinExpect(), "yyyy-MM-dd HH:mm:ss"));
            extendProps.put("storage_no", model.getOutBillNo());
            extendProps.put("ck_desc", model.getCpCCsEname());
            extendProps.put("wpwhco", model.getCpCCsEcode());
            extendProps.put("pick_no", model.getSourcecode());
            extendProps.put("po_no", model.getPoNo());
            extendProps.put("storageplace", cpCPhyWarehouse.getEcode());

            stockoutCreateModel.setExtendProps(extendProps);
            stockoutCreateModelList.add(stockoutCreateModel);
        }

        try {
            //调用出库单创建接口
            QmStockoutCreateCmd qmStockoutCreateCmd = (QmStockoutCreateCmd) ReferenceUtil.refer(
                    ApplicationContextHandle.getApplicationContext(), QmStockoutCreateCmd.class.getName(), "ip", "1.4.0");

            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + ".debug, param:" + JSONObject.toJSONString(stockoutCreateModelList));
            }

            //判断是否调用接口成功，成功更改状态为传WMS中，失败记录原因
            ValueHolderV14<List<QimenAsyncMqResult>> listValueHolderV14 = qmStockoutCreateCmd.stockoutCreate(stockoutCreateModelList, SgConstantsIF.GROUP, rootUser);
            List<QimenAsyncMqResult> qimenAsyncMqResultList = listValueHolderV14.getData();

            if (log.isDebugEnabled()) {
                log.debug(listValueHolderV14.getMessage() + "返回数据长度" + qimenAsyncMqResultList.size());
            }
            if (CollectionUtils.isNotEmpty(qimenAsyncMqResultList)) {
                qimenAsyncMqResultList.forEach(qimenAsyncMqResult -> {
                    //云枢纽返回失败，更新传WMS状态为传WMS失败，失败次数+1，记录失败原因。
                    if (!Boolean.valueOf(qimenAsyncMqResult.getIsSend())) {
                        SgBPhyOutNotices notices = new SgBPhyOutNotices();
                        notices.setWmsStatus(SgOutConstantsIF.WMS_STATUS_PASS_FAILED);
                        notices.setWmsFailReason(qimenAsyncMqResult.getMessage());
                        StorageESUtils.setBModelDefalutDataByUpdate(notices, rootUser);
                        notices.setModifierename(rootUser.getEname());
                        //获取wms失败次数
                        String billNo = qimenAsyncMqResult.getBillNo();
                        SgBPhyOutNotices orgNotices = noticesMap.get(billNo);
                        Long wmsFailedCount = orgNotices == null ? 1L : orgNotices.getWmsFailCount() + 1;
                        notices.setWmsFailCount(wmsFailedCount);
                        this.sgBigPhyOutNoticesToWmsMapper.update(notices, new UpdateWrapper<SgBPhyOutNotices>().lambda()
                                .eq(SgBPhyOutNotices::getBillNo, billNo)
                                .eq(SgBPhyOutNotices::getIsactive, SgConstants.IS_ACTIVE_Y));
                    }
                });
            }

        } catch (Exception e) {
            log.error(this.getClass().getName() + ".error:" + e.getMessage(), e);
            valueHolder.setCode(ResultCode.FAIL);
            valueHolder.setMessage(e.getMessage());
        }
        return valueHolder;
    }

    /**
     * 获取入库通知单明细及箱的信息
     *
     * @param notices
     */
    private List<QmStockoutCreateModel.OrderLine> getNoticeItemBox(SgBPhyOutNotices notices, String goodsOwner, String wmsAccount) {


        List<QmStockoutCreateModel.OrderLine> orderLines = new ArrayList<>();
        //录入明细
        QueryWrapper<SgBPhyOutNoticesImpItem> queryItem = new QueryWrapper<>();
        queryItem.eq(SgOutConstants.OUT_NOTICES_PAREN_FIELD, notices.getId());
        List<SgBPhyOutNoticesImpItem> phyOutNoticesItemList = this.sgBPhyOutNoticesImpItemMapper.selectList(queryItem);

        //箱内明细
        QueryWrapper<SgBPhyOutNoticesTeusItem> queryTenusitem = new QueryWrapper();
        queryTenusitem.eq(SgOutConstants.OUT_NOTICES_PAREN_FIELD, notices.getId());
        List<SgBPhyOutNoticesTeusItem> sgBPhyOutNoticesTeusItemList = this.sgBPhyOutNoticesTeusItemMapper.selectList(queryTenusitem);
        for (SgBPhyOutNoticesImpItem impItem : phyOutNoticesItemList) {
            if (impItem.getPsCTeusId() == null || impItem.getPsCTeusId() == 0L)//散件
            {
                QmStockoutCreateModel.OrderLine orderLine = new QmStockoutCreateModel.OrderLine();
                orderLine.setOwnerCode(goodsOwner);
                orderLine.setItemCode(impItem.getPsCSkuEcode());
                if (notices.getSgBOutPropId() != null && notices.getSgBOutPropId() == 2) {
                    orderLine.setInventoryType(SgConstantsIF.TRADE_MARK_CC);
                } else {
                    orderLine.setInventoryType(SgConstantsIF.TRADE_MARK_ZP);
                }
                String itemId = getItemId(wmsAccount, impItem.getPsCSkuEcode());
                orderLine.setItemId(itemId);
                orderLine.setPlanQty(impItem.getQty() == null ? "" : impItem.getQty().setScale(0, BigDecimal.ROUND_HALF_UP).toString());
                orderLines.add(orderLine);

            } else //箱
            {
                List<SgBPhyOutNoticesTeusItem> teusItems = sgBPhyOutNoticesTeusItemList.stream().filter(item -> item.getPsCTeusId().equals(impItem.getPsCTeusId())).collect(Collectors.toList());
                for (SgBPhyOutNoticesTeusItem teusItem2 : teusItems) {
                    QmStockoutCreateModel.OrderLine orderLine = new QmStockoutCreateModel.OrderLine();
                    orderLine.setOwnerCode(goodsOwner);
                    orderLine.setItemCode(teusItem2.getPsCSkuEcode());
                    if (notices.getSgBOutPropId() != null && notices.getSgBOutPropId() == 2) {
                        orderLine.setInventoryType(SgConstantsIF.TRADE_MARK_CC);
                    } else {
                        orderLine.setInventoryType(SgConstantsIF.TRADE_MARK_ZP);
                    }
                    String itemId = getItemId(wmsAccount, teusItem2.getPsCSkuEcode());
                    orderLine.setItemId(itemId);
                    orderLine.setPlanQty(teusItem2.getQty() == null ? "" : teusItem2.getQty().setScale(0, BigDecimal.ROUND_HALF_UP).toString());
                    orderLine.setProductCode(teusItem2.getPsCTeusEcode());//箱号
                    orderLines.add(orderLine);

                }
            }
        }
        return orderLines;


    }

    /**
     * 获取商品id
     *
     * @param wmsAccount
     * @param sku
     * @return
     */
    private String getItemId(String wmsAccount, String sku) {

        List<PsToWmsRequest> psToWmsRequestList = new ArrayList<>();

        PsToWmsRequest psToWmsRequest = new PsToWmsRequest();
        psToWmsRequest.setSku(sku);
        psToWmsRequest.setCustomerId(wmsAccount);
        psToWmsRequestList.add(psToWmsRequest);

        try {
            HashMap<String, String> psToWmsInfo = basicPsQueryService.getPsToWmsInfo(psToWmsRequestList);
            return psToWmsInfo.get(sku);
        } catch (Exception e) {
            log.error("调用basicPsQueryService.getPsToWmsInfo失败");
            return "";
        }
    }

    /**
     * 单据类型，数字转文字
     *
     * @param sourceType         单据类型（数字）
     * @param origCustomerId
     * @param destWareHouseEcode
     * @return 单据类型（文字）
     */
    private String changeNumToWord(Integer sourceType, String origCustomerId, String destWareHouseEcode) {
        String orderType = SgOutConstants.QM_BILL_TYPE_OTHER_OUT;
        switch (sourceType) {
            case SgConstantsIF.BILL_TYPE_SALE:
                orderType = SgOutConstants.QM_BILL_TYPE_B2B_OUT;
                break;
            case SgConstantsIF.BILL_TYPE_PUR_REF:
                orderType = SgOutConstants.QM_BILL_TYPE_REF_PUR_OUT;
                break;
            case SgConstantsIF.BILL_TYPE_TRANSFER:
                orderType = getOrderTypeForTransfer(origCustomerId, destWareHouseEcode);
                break;
            case SgConstantsIF.BILL_TYPE_VIPSHOP:
                orderType = SgOutConstants.QM_BILL_TYPE_VIPSHOP_OUT;
                break;
        }
        return orderType;
    }

    /**
     * 特殊处理调拨单类型，获取奇门所需的订单类型
     *
     * @param origCustomerId     发货方customerid
     * @param destWareHouseEcode 收货实体仓Ecode
     * @description: 判断 如果发货实体仓对应的customerid是系统参数菜鸟仓customerid，
     * 收货店仓对应的customerid不是系统参数菜鸟仓customerid的值，则传PTCK；否则则传DBCK。
     */
    private String getOrderTypeForTransfer(String origCustomerId, String destWareHouseEcode) {
        String orderType = SgOutConstants.QM_BILL_TYPE_TRANSFER_OUT;
        try {
            //系统参数-菜鸟customerid
            String sysCustoemerId = AdParamUtil.getParam(SgOutConstants.SYSTEM_WAREHOUSE_CUSTOMERID);
            log.debug("大货出库传WMS获取菜鸟仓customerid:" + sysCustoemerId + ",收货实体仓ecode:" + destWareHouseEcode);
            if (StringUtils.isNotEmpty(sysCustoemerId) &&
                    StringUtils.isNotEmpty(origCustomerId) &&
                    sysCustoemerId.equals(origCustomerId)) {
                if (StringUtils.isNotEmpty(destWareHouseEcode)) {
                    //获取收货实体仓customreid
                    CpCPhyWarehouseMapper warehouseMapper = ApplicationContextHandle.getBean(CpCPhyWarehouseMapper.class);
                    List<CpCPhyWarehouse> warehouses = warehouseMapper.selectList(new QueryWrapper<CpCPhyWarehouse>().lambda()
                            .eq(CpCPhyWarehouse::getEcode, destWareHouseEcode)
                            .eq(CpCPhyWarehouse::getIsactive, SgConstants.IS_ACTIVE_Y));
                    if (CollectionUtils.isNotEmpty(warehouses)) {
                        String destCustomerId = warehouses.get(0).getWmsAccount();
                        log.debug("大货出库传WMS获取收货实体仓customerid" + destCustomerId);
                        if (StringUtils.isNotEmpty(destCustomerId)) {
                            //收货方customerid不等于菜鸟仓customerid
                            if (!sysCustoemerId.equals(destCustomerId)) {
                                orderType = SgOutConstants.QM_BILL_TYPE_PTCK_OUT;
                            }
                        } else {
                            //收货方对应的实体仓档案中customerid为空
                            orderType = SgOutConstants.QM_BILL_TYPE_PTCK_OUT;
                        }
                    } else {
                        //收货方在实体仓档案中没有记录
                        orderType = SgOutConstants.QM_BILL_TYPE_PTCK_OUT;
                    }
                } else {
                    //通知单收货方ecode为空
                    orderType = SgOutConstants.QM_BILL_TYPE_PTCK_OUT;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("系统参数菜鸟仓customerid获取异常!" + e.getMessage());
        }
        return orderType;
    }
}
