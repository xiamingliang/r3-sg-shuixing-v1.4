package com.jackrain.nea.sg.itface.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.itface.mapper.SgBigPhyOutNoticesToWmsBackMapper;
import com.jackrain.nea.sg.out.model.request.SgPhyOutNoticesBillWMSBackRequest;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutNotices;
import com.jackrain.nea.sg.out.services.SgPhyOutNoticesSaveWMSBackService;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zhu lin yu
 * @since 2019/5/7
 * create at : 2019/5/7 10:43
 */
@Slf4j
@Component
public class SgBigPhyOutNoticesToWmsBackService {
    @Autowired
    private SgBigPhyOutNoticesToWmsBackMapper sgBigPhyOutNoticesToWmsBackMapper;

    @Autowired
    private SgPhyOutNoticesSaveWMSBackService sgPhyOutNoticesSaveWMSBackService;

    public ValueHolderV14 bigPhyOutNoticeToWmsBack(List<SgPhyOutNoticesBillWMSBackRequest> requests, User user) {

        //判断参数是否存在
        if (CollectionUtils.isEmpty(requests)) {
            AssertUtils.logAndThrow("参数不能为空！");
        }
        AssertUtils.notNull(user, "用户信息不能为空！");

        //校验并封装参数
        List<SgPhyOutNoticesBillWMSBackRequest> sgPhyOutNoticesBillWMSBackRequests = new ArrayList<>();
        for (SgPhyOutNoticesBillWMSBackRequest request : requests) {
            String billNo = request.getOrderNo();

            // 根据出库通知单单据编号查询出库通知单是否存在
            QueryWrapper<SgBPhyOutNotices> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("bill_no", billNo);
            SgBPhyOutNotices outNotices = sgBigPhyOutNoticesToWmsBackMapper.selectOne(queryWrapper);
            if (outNotices != null) {

                //判断出库通知单单据状态是否为待出库
                int billStatus = this.sgBigPhyOutNoticesToWmsBackMapper.queryBillStatusByBillNo(billNo);
                if (1 == billStatus) {
                    //设置状态1为大货出库
                    request.setFlag(1);
                    sgPhyOutNoticesBillWMSBackRequests.add(request);
                } else {
                    if (log.isDebugEnabled()) {
                        log.debug(this.getClass().getName() + ".debug, 单据状态不为待出库:" + JSONObject.toJSONString(request));
                    }
                }
            }

        }

        //调用服务，更新状态
        return this.sgPhyOutNoticesSaveWMSBackService.saveWMSBackOutNotices(sgPhyOutNoticesBillWMSBackRequests, user);
    }
}
