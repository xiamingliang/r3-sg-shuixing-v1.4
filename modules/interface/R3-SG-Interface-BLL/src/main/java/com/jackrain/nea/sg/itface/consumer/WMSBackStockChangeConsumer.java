package com.jackrain.nea.sg.itface.consumer;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Lists;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.data.basic.model.request.SkuInfoQueryRequest;
import com.jackrain.nea.data.basic.services.BasicPsQueryService;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.oc.basic.common.OcBasicConstants;
import com.jackrain.nea.ps.api.result.PsCProSkuResult;
import com.jackrain.nea.sg.basic.common.SgResultCode;
import com.jackrain.nea.sg.basic.mapper.CpCPhyWarehouseMapper;
import com.jackrain.nea.sg.basic.model.table.CpCPhyWarehouse;
import com.jackrain.nea.sg.phyadjust.api.SgPhyAdjSaveAndAuditCmd;
import com.jackrain.nea.sg.phyadjust.common.SgPhyAdjustConstants;
import com.jackrain.nea.sg.phyadjust.common.SgPhyAdjustConstantsIF;
import com.jackrain.nea.sg.phyadjust.model.request.SgPhyAdjustBillSaveRequest;
import com.jackrain.nea.sg.phyadjust.model.request.SgPhyAdjustImpItemRequest;
import com.jackrain.nea.sg.phyadjust.model.request.SgPhyAdjustItemSaveRequest;
import com.jackrain.nea.sg.phyadjust.model.request.SgPhyAdjustSaveRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * @author caopengflying
 * @time 2019/5/7 9:31
 * @descript 库存调整结果WMS回传服务 - 回传
 */
@Slf4j
@Component
public class WMSBackStockChangeConsumer {
    @Reference(group = "sg", version = "1.0")
    private SgPhyAdjSaveAndAuditCmd sgPhyAdjSaveAndAuditCmd;

    @Autowired
    CpCPhyWarehouseMapper warehouseMapper;

    @Autowired
    BasicPsQueryService basicPsQueryService;

    /**
     * @param message
     * @return
     * @deprecated 该方法已经被弃用，wms取消发送该method的mq消息
     */
    @Deprecated
    public Boolean consume(String message) {
        SgPhyAdjustBillSaveRequest sgPhyAdjustBillSaveRequest = parseStockChangeMessage2SgPhyAdjust(message);
        try {
            ValueHolderV14 valueHolderV14 = sgPhyAdjSaveAndAuditCmd.saveAndAuditAdj(sgPhyAdjustBillSaveRequest);
            if (SgResultCode.isFail(valueHolderV14)) {
                log.error("WMSBackStockChangeConsumer.consume ResultMessage={}", new Object[]{valueHolderV14.getMessage()});
                return false;
            }
        } catch (NDSException e) {
            e.printStackTrace();
            log.error("WMSBackStockChangeConsumer.consume exception{}", new Object[]{e});
            return false;
        }
        return true;
    }

    /**
     * wms调整单回传
     *
     * @param message
     * @return
     */
    public Boolean consumeNew(String message) {
        ValueHolderV14<SgPhyAdjustBillSaveRequest> v14 = parseStockChangeMessage2PhyAdjustNew(message);
        if (v14.getCode() == ResultCode.FAIL) {
            return false;
        }
        SgPhyAdjustBillSaveRequest sgPhyAdjustBillSaveRequest = v14.getData();
        try {
            ValueHolderV14 valueHolderV14 = sgPhyAdjSaveAndAuditCmd.saveAndAuditAdj(sgPhyAdjustBillSaveRequest);
            if (SgResultCode.isFail(valueHolderV14)) {
                log.error("WMSBackStockChangeConsumer.consume ResultMessage={}", new Object[]{valueHolderV14.getMessage()});
                return false;
            }
        } catch (NDSException e) {
            e.printStackTrace();
            log.error("WMSBackStockChangeConsumer.consume exception{}", new Object[]{e});
            return false;
        }
        return true;
    }

    /**
     * 库存调整结果WMS回传服务 - 回传mq消息转化为库存调整单
     *
     * @param message
     * @return
     */
    private SgPhyAdjustBillSaveRequest parseStockChangeMessage2SgPhyAdjust(String message) {
        JSONObject result = JSONObject.parseObject(message);
        JSONObject request = result.getJSONObject("request").getJSONObject("items").getJSONObject("item");
        SgPhyAdjustBillSaveRequest sgPhyAdjustBillSaveRequest = new SgPhyAdjustBillSaveRequest();
        //主表
        SgPhyAdjustSaveRequest sgBPhyAdjust = new SgPhyAdjustSaveRequest();
        sgBPhyAdjust.setCpCPhyWarehouseEcode(request.getString("warehouseCode"));
        sgBPhyAdjust.setBillType(SgPhyAdjustConstantsIF.BILL_TYPE_NORMAL);
        sgBPhyAdjust.setSourceBillType(SgPhyAdjustConstantsIF.SOURCE_BILL_TYPE_PAND);
        sgBPhyAdjust.setSourceBillId(request.getLong("checkOrderCode"));
        sgBPhyAdjust.setSourceBillNo(request.getString("checkOrderCode"));
        sgBPhyAdjust.setWmsBillNo(request.getString("checkOrderCode"));
        sgPhyAdjustBillSaveRequest.setPhyAdjust(sgBPhyAdjust);
        //子表
        List<SgPhyAdjustItemSaveRequest> itemSaveRequestList = Lists.newArrayList();
        SgPhyAdjustItemSaveRequest sgPhyAdjustItemSaveRequest = new SgPhyAdjustItemSaveRequest();
        sgPhyAdjustItemSaveRequest.setQty(request.getBigDecimal("quantity"));
        sgPhyAdjustItemSaveRequest.setPsCSkuEcode(request.getString("itemCode"));
        itemSaveRequestList.add(sgPhyAdjustItemSaveRequest);
        sgPhyAdjustBillSaveRequest.setItems(itemSaveRequestList);
        sgPhyAdjustBillSaveRequest.setLoginUser(WMSUserFactory.initWmsUser(WMSUserFactory.DEFAULT_ORG_ID,
                WMSUserFactory.DEFAULT_CLIENT_ID));
        return sgPhyAdjustBillSaveRequest;
    }

    /**
     * 库存调整结果WMS回传服务 - 回传mq消息转化为库存调整单
     *
     * @param message
     * @return
     */
    private ValueHolderV14<SgPhyAdjustBillSaveRequest> parseStockChangeMessage2PhyAdjustNew(String message) {
        ValueHolderV14 ret = new ValueHolderV14();
        JSONObject result = JSONObject.parseObject(message).getJSONObject("request");
        SgPhyAdjustBillSaveRequest sgPhyAdjustBillSaveRequest = new SgPhyAdjustBillSaveRequest();
        String firtItemStr = result.getJSONArray("items").get(0).toString();
        String ordercode = JSONObject.parseObject(firtItemStr).getJSONObject("item").getString("orderCode");
        String warehousecode = result.getJSONObject("extendProps").getString("storageplace");
        CpCPhyWarehouse cpCPhyWarehouse = warehouseMapper.selectList(new QueryWrapper<CpCPhyWarehouse>().lambda().eq(CpCPhyWarehouse::getEcode, warehousecode)).stream().findFirst().get();
        if (cpCPhyWarehouse == null) {
            ret.setCode(-1);
            ret.setMessage("未找到对应的实体仓库，仓库编码" + warehousecode);
            log.error("wms库存调整单同步失败，未找到对应的实体仓库，仓库编码" + warehousecode);
            return ret;
        }

        //主表
        SgPhyAdjustSaveRequest sgPhyAdjustSaveRequest = new SgPhyAdjustSaveRequest();
        sgPhyAdjustSaveRequest.setSourceBillType(SgPhyAdjustConstantsIF.SOURCE_BILL_TYPE_PAND);//盈亏单
        sgPhyAdjustSaveRequest.setBillType(SgPhyAdjustConstantsIF.BILL_TYPE_NORMAL);//正常调整
        sgPhyAdjustSaveRequest.setSgBAdjustPropId(SgPhyAdjustConstants.ADJUST_PROP_PROFITLOSS_ADJUST);//损益调整
        sgPhyAdjustSaveRequest.setSourceBillNo(ordercode);
        sgPhyAdjustSaveRequest.setWmsBillNo(ordercode);
        //sgPhyAdjustSaveRequest.setSourceBillId(ordercode);

        sgPhyAdjustSaveRequest.setCpCPhyWarehouseEcode(warehousecode);//仓库编码
        sgPhyAdjustSaveRequest.setCpCPhyWarehouseId(cpCPhyWarehouse.getId());
        sgPhyAdjustSaveRequest.setCpCPhyWarehouseEname(cpCPhyWarehouse.getEname());

        //子表
        JSONArray itemArray = result.getJSONArray("items");
        List<SgPhyAdjustImpItemRequest> itemSaveRequestList = Lists.newArrayList();
        for (Object itemObj : itemArray) {
            SgPhyAdjustImpItemRequest sgPhyAdjustItemSaveRequest = new SgPhyAdjustImpItemRequest();
            JSONObject itemjson = JSONObject.parseObject(itemObj.toString()).getJSONObject("item");
            String sku = itemjson.getString("itemCode");
            sgPhyAdjustItemSaveRequest.setQty(itemjson.getBigDecimal("quantity"));
            sgPhyAdjustItemSaveRequest.setPsCSkuEcode(sku);
            SkuInfoQueryRequest request = new SkuInfoQueryRequest();
            request.setSkuEcodeList(Arrays.asList(sku));
            try {
                HashMap<String, PsCProSkuResult> psCProSkuResultHashMap = basicPsQueryService.getSkuInfoByEcode(request);
                PsCProSkuResult proSkuResult = psCProSkuResultHashMap.get(sku);
                sgPhyAdjustItemSaveRequest.setPsCSkuId(proSkuResult.getId());
                sgPhyAdjustItemSaveRequest.setPsCProId(proSkuResult.getPsCProId());
                sgPhyAdjustItemSaveRequest.setPsCProEcode(proSkuResult.getPsCProEcode());
                sgPhyAdjustItemSaveRequest.setPsCProEname(proSkuResult.getPsCProEname());
                sgPhyAdjustItemSaveRequest.setPriceList(proSkuResult.getPricelist());
                sgPhyAdjustItemSaveRequest.setPsCSpec1Id(proSkuResult.getPsCSpec1objId());
                sgPhyAdjustItemSaveRequest.setPsCSpec1Ecode(proSkuResult.getClrsEcode());
                sgPhyAdjustItemSaveRequest.setPsCSpec1Ename(proSkuResult.getClrsEname());
                sgPhyAdjustItemSaveRequest.setPsCSpec2Id(proSkuResult.getPsCSpec2objId());
                sgPhyAdjustItemSaveRequest.setPsCSpec2Ecode(proSkuResult.getSizesEcode());
                sgPhyAdjustItemSaveRequest.setPsCSpec2Ename(proSkuResult.getSizesEname());
                sgPhyAdjustItemSaveRequest.setIsTeus(OcBasicConstants.IS_MATCH_SIZE_N);
            } catch (Exception e) {
                log.error("获取商品" + sku + "信息异常" + e.getMessage());
                e.printStackTrace();
            }


            itemSaveRequestList.add(sgPhyAdjustItemSaveRequest);
        }
        sgPhyAdjustBillSaveRequest.setImpItems(itemSaveRequestList);
        sgPhyAdjustBillSaveRequest.setPhyAdjust(sgPhyAdjustSaveRequest);
        sgPhyAdjustBillSaveRequest.setObjId(-1L);
        sgPhyAdjustBillSaveRequest.setLoginUser(WMSUserFactory.initWmsUser(WMSUserFactory.DEFAULT_ORG_ID,
                WMSUserFactory.DEFAULT_CLIENT_ID));
        ret.setCode(ResultCode.SUCCESS);
        ret.setData(sgPhyAdjustBillSaveRequest);
        return ret;

    }
}
