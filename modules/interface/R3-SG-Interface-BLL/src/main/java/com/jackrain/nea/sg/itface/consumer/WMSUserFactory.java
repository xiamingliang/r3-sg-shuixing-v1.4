package com.jackrain.nea.sg.itface.consumer;


import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.face.impl.UserImpl;

/**
 * @author caopengflying
 * @time 2019/5/7 15:47
 */
public class WMSUserFactory {
    public static Integer DEFAULT_ORG_ID = 27;//默认组织ID
    public static Integer DEFAULT_CLIENT_ID = 37;//默认的公司ID
    public static User initWmsUser(Integer orgId, Integer clientId) {
        User user = new UserImpl();
        ((UserImpl) user).setEname("WMS");
        ((UserImpl) user).setName("WMS");
        ((UserImpl) user).setId(666);
        //所属公司
        if (null != clientId) {
            ((UserImpl) user).setClientId(clientId);
        }
        //所属组织
        if (null != orgId) {
            ((UserImpl) user).setOrgId(orgId);
        }
        return user;
    }
}
