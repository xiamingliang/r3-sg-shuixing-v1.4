package com.jackrain.nea.sg.itface.consumer;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.sg.basic.common.SgResultCode;
import com.jackrain.nea.sg.in.model.request.NoticesCallBackRequest;
import com.jackrain.nea.sg.in.services.SgPhyInNoticesCallBackService;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author caopengflying
 * @time 2019/5/7 9:42
 * @descript 大货入库通知单传WMS - 回执
 */
@Slf4j
@Component
public class WMSCallEntryOrderConsumer {
    @Autowired
    private SgPhyInNoticesCallBackService sgPhyInNoticesCallBackService;

    public Boolean consume(String messageBody) {
        try {
            String data = JSONObject.parseObject(messageBody).getJSONArray("data").toJSONString();
            List<NoticesCallBackRequest> noticesCallBackRequests = JSON.parseArray(data, NoticesCallBackRequest.class);
            ValueHolderV14<NoticesCallBackRequest> holder =
                    sgPhyInNoticesCallBackService.callBackSgPhyInNotices(noticesCallBackRequests,
                            WMSUserFactory.initWmsUser(null,null));
            if (SgResultCode.isFail(holder)) {
                log.error(this.getClass().getName() + " 大货入库通知单传WMS回执失败", holder.getMessage());
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(this.getClass().getName() + " 大货入库通知单传WMS回执异常", e);
            return false;
        }
        return true;
    }

}
