package com.jackrain.nea.sg.itface.mapper;

import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutNotices;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author zhu lin yu
 * @since 2019/5/6
 * create at : 2019/5/6 15:32
 */
@Mapper
public interface SgBigPhyOutNoticesToWmsMapper extends ExtentionMapper<SgBPhyOutNotices> {

    @Select("SELECT * FROM SG_B_PHY_OUT_NOTICES WHERE OUT_TYPE = 2 AND BILL_STATUS = 1 AND IS_PASS_WMS = 1 AND (WMS_STATUS = 0 OR (WMS_STATUS = 3 AND WMS_FAIL_COUNT < 6)) ORDER BY MODIFIEDDATE LIMIT #{range}")
    List<SgBPhyOutNotices> selectPhyOutNoticesInfo(@Param("range") Integer range);
}
