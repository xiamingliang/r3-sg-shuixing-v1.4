package com.jackrain.nea.sg.itface.consumer;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.google.common.collect.Lists;
import com.jackrain.nea.cpext.api.CpLogisticsSelectServiceCmd;
import com.jackrain.nea.cpext.model.table.CpLogistics;
import com.jackrain.nea.model.util.ModelUtil;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.config.SgStorageBoxConfig;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.out.common.SgOutConstants;
import com.jackrain.nea.sg.out.common.SgOutConstantsIF;
import com.jackrain.nea.sg.out.mapper.SgBWmsToPhyOutResultMapper;
import com.jackrain.nea.sg.out.model.request.SgPhyOutDeliverySaveRequest;
import com.jackrain.nea.sg.out.model.request.SgPhyOutResultBillSaveRequest;
import com.jackrain.nea.sg.out.model.request.SgPhyOutResultImpItemSaveRequest;
import com.jackrain.nea.sg.out.model.request.SgPhyOutResultItemSaveRequest;
import com.jackrain.nea.sg.out.model.table.SgBWmsToPhyOutResult;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author caopengflying
 * @time 2019/5/6 17:51
 * @descript (云枢纽回传库存中心)零售发货单 - 回传
 */
@Slf4j
@Component
public class WMSBackDeliveryOrderConsumer {

    @Autowired
    private WMSBackStockOutConsumer stockOutConsumer;

    @Autowired
    private SgBWmsToPhyOutResultMapper wmsToPhyOutResultMapper;

    @Autowired
    private SgStorageBoxConfig storageBoxConfig;
    @Reference(group = "cp-ext", version = "1.0")
    private CpLogisticsSelectServiceCmd cpLogisticsSelectServiceCmd;


    public Boolean consume(String message) {
        try {
            SgPhyOutResultBillSaveRequest sgPhyOutResultBillSaveRequest = stockOutConsumer.parseStockOutMessage2SgPhyOutResultBillSaveRequest(message);
            if (null == sgPhyOutResultBillSaveRequest) {
                log.error(this.getClass().getName() +
                        " 查询出库通知单失败", message);
                return false;
            }
            parseDeliveryOrderMessageToSgPhyOutRequest(message, sgPhyOutResultBillSaveRequest);
            String noticesBillNo = sgPhyOutResultBillSaveRequest.getOutResultRequest().getSgBPhyOutNoticesBillno();

            String body = JSONObject.toJSONString(sgPhyOutResultBillSaveRequest);
            List<SgBWmsToPhyOutResult> wmsToPhyOutResults = wmsToPhyOutResultMapper.selectList(new QueryWrapper<SgBWmsToPhyOutResult>().lambda()
                    .eq(SgBWmsToPhyOutResult::getNoticesBillNo, noticesBillNo)
                    .eq(SgBWmsToPhyOutResult::getIsactive, SgConstants.IS_ACTIVE_Y));
            if (CollectionUtils.isNotEmpty(wmsToPhyOutResults)) {
                Long wmsBackCount = Optional.ofNullable(wmsToPhyOutResults.get(0).getWmsBackCount()).orElse(1L);
                wmsBackCount = wmsBackCount + 1;
                SgBWmsToPhyOutResult wmsToPhyOutResult = new SgBWmsToPhyOutResult();
                wmsToPhyOutResult.setOutResultStr(body);
                wmsToPhyOutResult.setWmsBackCount(wmsBackCount);
                StorageESUtils.setBModelDefalutData(wmsToPhyOutResult, sgPhyOutResultBillSaveRequest.getLoginUser());
                int update = wmsToPhyOutResultMapper.update(wmsToPhyOutResult, new UpdateWrapper<SgBWmsToPhyOutResult>().lambda()
                        .eq(SgBWmsToPhyOutResult::getNoticesBillNo, noticesBillNo)
                        .eq(SgBWmsToPhyOutResult::getIsactive, SgConstants.IS_ACTIVE_Y));
                if (update < 1) {
                    AssertUtils.logAndThrow(sgPhyOutResultBillSaveRequest.getOutResultRequest().getSourceBillId() + "WMS回传记录中间表失败!");
                }
            } else {
                SgBWmsToPhyOutResult wmsToPhyOutResult = new SgBWmsToPhyOutResult();
                wmsToPhyOutResult.setId(ModelUtil.getSequence(SgConstants.SG_B_WMS_TO_PHY_OUT_RESULT));
                wmsToPhyOutResult.setStatus(SgOutConstants.WMS_TO_PHY_RESULT_STATUS_WAIT);
                wmsToPhyOutResult.setOutType(SgOutConstantsIF.OUT_TYPE_ELECTRICITY);
                wmsToPhyOutResult.setOutResultStr(body);
                wmsToPhyOutResult.setNoticesBillNo(noticesBillNo);
                wmsToPhyOutResult.setWmsBackCount(1L);
                wmsToPhyOutResult.setWmsFailedCount(0);
                wmsToPhyOutResult.setIsactive(SgConstants.IS_ACTIVE_Y);
                StorageESUtils.setBModelDefalutData(wmsToPhyOutResult, sgPhyOutResultBillSaveRequest.getLoginUser());
                int insert = wmsToPhyOutResultMapper.insert(wmsToPhyOutResult);
                if (insert < 1) {
                    AssertUtils.logAndThrow(sgPhyOutResultBillSaveRequest.getOutResultRequest().getSourceBillId() + "WMS回传记录中间表失败!");
                }
            }
           /* if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + " 新增并审核出库结果单入参 " + JSONObject.toJSONString(sgPhyOutResultBillSaveRequest));
            }
            ValueHolderV14 holder = sgPhyOutResultSaveAndAuditService.saveOutResultAndAuditNoTrans(sgPhyOutResultBillSaveRequest);
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + " 新增并审核出库结果单返回 " + JSONObject.toJSONString(holder));
            }
            if (SgResultCode.isFail(holder)) {
                log.error(this.getClass().getName() +
                        " 保存出库结果单失败", holder.getMessage());
                return false;
            }*/
        } catch (Exception e) {
            e.printStackTrace();
            log.error("出库通知单回传异常!", e);
            return false;
        }
        return true;
    }

    /**
     * 在出库结果单中加入wms编号和理论重量
     * 增加多包裹情况
     *
     * @param message
     * @param sgPhyOutResultBillSaveRequest
     */
    private void parseDeliveryOrderMessageToSgPhyOutRequest(String message, SgPhyOutResultBillSaveRequest sgPhyOutResultBillSaveRequest) {
        List<SgPhyOutDeliverySaveRequest> outDeliverySaveRequestList = Lists.newArrayList();
        JSONObject request = JSONObject.parseObject(message).getJSONObject("request");
        JSONObject deliveryOrderObject = request.getJSONObject(
                "deliveryOrder");
        JSONArray packages = request.getJSONArray("packages");
        BigDecimal theoreticalWeight = BigDecimal.ZERO;
        for (Object aPackage : packages) {
            JSONObject packageObject = ((JSONObject) aPackage).getJSONObject("package");
            //理论重量
            BigDecimal bigDecimal = (packageObject.getBigDecimal("theoreticalWeight"));
            if (null != bigDecimal) {
                theoreticalWeight = theoreticalWeight.add(bigDecimal);
            }
            //物流公司Code
            String logisticCode = packageObject.getString("logisticsCode");
            String logisticName = packageObject.getString("logisticsName");
            String logisticId = "";
            //2019-08-12增加逻辑: 取logisticsCode从菜鸟映射表中取出对应物流公司档案的cp_c_logistics_ecode
            if (log.isDebugEnabled()) {
                log.debug("回传logisticCode参数:" + logisticCode);
            }
            if (StringUtils.isNotEmpty(logisticCode)) {
                String logisticEcodeTransform = cpLogisticsSelectServiceCmd.logisticEcodeTransform(logisticCode);
                if (log.isDebugEnabled()) {
                    log.debug("logisticCode菜鸟映射结果:" + logisticEcodeTransform);
                }
                if (StringUtils.isNotEmpty(logisticEcodeTransform)) {
                    logisticCode = logisticEcodeTransform;
                }
            }
            //物流公司ID
            CpLogistics cpLogistics = cpLogisticsSelectServiceCmd.queryCpLogisticByEcode(logisticCode);
            if (null != cpLogistics) {
                logisticId = cpLogistics.getId().toString();
                //物流公司名称
                logisticName = cpLogistics.getEname();
            }
            //物流单号
            String expressCode = packageObject.getString("expressCode");
            //重量
            BigDecimal weight = packageObject.getBigDecimal("weight");
            //体积
            BigDecimal volume = packageObject.getBigDecimal("volume");
            JSONArray items = packageObject.getJSONArray("items");
            for (Object item : items) {
                JSONObject itemObject = ((JSONObject) item).getJSONObject("item");
                String itemCode = itemObject.getString("itemCode");
                BigDecimal quantity = itemObject.getBigDecimal("quantity");
                //判断箱
                if (storageBoxConfig.getBoxEnable()) {
                    List<SgPhyOutResultImpItemSaveRequest> collect = sgPhyOutResultBillSaveRequest.getImpItemList().stream().filter(sgPhyOutResultImpItemSaveRequest -> sgPhyOutResultImpItemSaveRequest.getPsCSkuEcode().equals(itemCode)).collect(Collectors.toList());
                    if (CollectionUtils.isNotEmpty(collect)) {
                        SgPhyOutResultImpItemSaveRequest sgPhyOutResultItemSaveRequest = collect.get(0);
                        SgPhyOutDeliverySaveRequest sgPhyOutDeliverySaveRequest =
                                parseOutResultImpItem2DeliverySave(sgPhyOutResultItemSaveRequest, logisticCode, logisticName, logisticId, expressCode, weight, quantity, volume, sgPhyOutResultBillSaveRequest.getOutResultRequest().getSourceBillId());
                        outDeliverySaveRequestList.add(sgPhyOutDeliverySaveRequest);
                    }
                }
                else{
                    List<SgPhyOutResultItemSaveRequest> collect = sgPhyOutResultBillSaveRequest.getOutResultItemRequests().stream().filter(sgPhyOutResultItemSaveRequest -> sgPhyOutResultItemSaveRequest.getPsCSkuEcode().equals(itemCode)).collect(Collectors.toList());
                    if (CollectionUtils.isNotEmpty(collect)) {
                        SgPhyOutResultItemSaveRequest sgPhyOutResultItemSaveRequest = collect.get(0);
                        SgPhyOutDeliverySaveRequest sgPhyOutDeliverySaveRequest =
                                parseOutResultItem2DeliverySave(sgPhyOutResultItemSaveRequest, logisticCode, logisticName, logisticId, expressCode, weight, quantity, volume, sgPhyOutResultBillSaveRequest.getOutResultRequest().getSourceBillId());
                        outDeliverySaveRequestList.add(sgPhyOutDeliverySaveRequest);
                    }
                }
            }
        }
        String deliveryOrderId = deliveryOrderObject.getString("deliveryOrderId");
        sgPhyOutResultBillSaveRequest.getOutResultRequest().setWmsBillNo(deliveryOrderId);
        sgPhyOutResultBillSaveRequest.getOutResultRequest().setTheoreticalWeight(theoreticalWeight);
        sgPhyOutResultBillSaveRequest.setOutDeliverySaveRequests(outDeliverySaveRequestList);
    }

    /**
     * 出库通知单明细转为零售包裹信息
     *
     * @param sgPhyOutResultItemSaveRequest 出库通知单明细
     * @param logisticCode                  物流公司编码
     * @param logisticName                  物流公司名称
     * @param logisticId                    物流公司id
     * @param expressCode                   物流单号
     * @param weight                        重量
     * @param quantity                      数量
     * @param volume                        体积
     * @param sourceBillId                  来源单据id
     * @return
     */
    private SgPhyOutDeliverySaveRequest parseOutResultItem2DeliverySave(SgPhyOutResultItemSaveRequest sgPhyOutResultItemSaveRequest, String logisticCode, String logisticName, String logisticId, String expressCode, BigDecimal weight, BigDecimal quantity, BigDecimal volume, Long sourceBillId) {
        SgPhyOutDeliverySaveRequest sgPhyOutDeliverySaveRequest = new SgPhyOutDeliverySaveRequest();
        BeanUtils.copyProperties(sgPhyOutResultItemSaveRequest, sgPhyOutDeliverySaveRequest);
        sgPhyOutDeliverySaveRequest.setCpCLogisticsEcode(logisticCode);
        sgPhyOutDeliverySaveRequest.setCpCLogisticsEname(logisticName);
        sgPhyOutDeliverySaveRequest.setCpCLogisticsId(logisticId);
        sgPhyOutDeliverySaveRequest.setLogisticNumber(expressCode);
        sgPhyOutDeliverySaveRequest.setWeight(weight);
        sgPhyOutDeliverySaveRequest.setQty(quantity);
        sgPhyOutDeliverySaveRequest.setSize(volume);
        //出库通知单中来源单据ID
        sgPhyOutDeliverySaveRequest.setOcBOrderId(sourceBillId);
        //尺寸编码、名称、id
        sgPhyOutDeliverySaveRequest.setPsCSizeEcode(sgPhyOutResultItemSaveRequest.getPsCSpec2Ecode());
        sgPhyOutDeliverySaveRequest.setPsCSizeEname(sgPhyOutResultItemSaveRequest.getPsCSpec2Ename());
        sgPhyOutDeliverySaveRequest.setPsCSizeId(sgPhyOutResultItemSaveRequest.getPsCSpec2Id());
        //颜色编码、名称、id
        sgPhyOutDeliverySaveRequest.setPsCClrId(sgPhyOutResultItemSaveRequest.getPsCSpec1Id());
        sgPhyOutDeliverySaveRequest.setPsCClrEcode(sgPhyOutResultItemSaveRequest.getPsCSpec1Ecode());
        sgPhyOutDeliverySaveRequest.setPsCClrEname(sgPhyOutResultItemSaveRequest.getPsCSpec1Ename());
        return sgPhyOutDeliverySaveRequest;
    }

    /**
     * 出库通知单明细转为零售包裹信息(从录入明细获取) add by nrl
     *
     * @param sgPhyOutResultImpItemSaveRequest 出库通知单明细
     * @param logisticCode                  物流公司编码
     * @param logisticName                  物流公司名称
     * @param logisticId                    物流公司id
     * @param expressCode                   物流单号
     * @param weight                        重量
     * @param quantity                      数量
     * @param volume                        体积
     * @param sourceBillId                  来源单据id
     * @return
     */
    private SgPhyOutDeliverySaveRequest parseOutResultImpItem2DeliverySave(SgPhyOutResultImpItemSaveRequest sgPhyOutResultImpItemSaveRequest, String logisticCode, String logisticName, String logisticId, String expressCode, BigDecimal weight, BigDecimal quantity, BigDecimal volume, Long sourceBillId) {
        SgPhyOutDeliverySaveRequest sgPhyOutDeliverySaveRequest = new SgPhyOutDeliverySaveRequest();
        BeanUtils.copyProperties(sgPhyOutResultImpItemSaveRequest, sgPhyOutDeliverySaveRequest);
        sgPhyOutDeliverySaveRequest.setCpCLogisticsEcode(logisticCode);
        sgPhyOutDeliverySaveRequest.setCpCLogisticsEname(logisticName);
        sgPhyOutDeliverySaveRequest.setCpCLogisticsId(logisticId);
        sgPhyOutDeliverySaveRequest.setLogisticNumber(expressCode);
        sgPhyOutDeliverySaveRequest.setWeight(weight);
        sgPhyOutDeliverySaveRequest.setQty(quantity);
        sgPhyOutDeliverySaveRequest.setSize(volume);
        //出库通知单中来源单据ID
        sgPhyOutDeliverySaveRequest.setOcBOrderId(sourceBillId);
        //尺寸编码、名称、id
        sgPhyOutDeliverySaveRequest.setPsCSizeEcode(sgPhyOutResultImpItemSaveRequest.getPsCSpec2Ecode());
        sgPhyOutDeliverySaveRequest.setPsCSizeEname(sgPhyOutResultImpItemSaveRequest.getPsCSpec2Ename());
        sgPhyOutDeliverySaveRequest.setPsCSizeId(sgPhyOutResultImpItemSaveRequest.getPsCSpec2Id());
        //颜色编码、名称、id
        sgPhyOutDeliverySaveRequest.setPsCClrId(sgPhyOutResultImpItemSaveRequest.getPsCSpec1Id());
        sgPhyOutDeliverySaveRequest.setPsCClrEcode(sgPhyOutResultImpItemSaveRequest.getPsCSpec1Ecode());
        sgPhyOutDeliverySaveRequest.setPsCClrEname(sgPhyOutResultImpItemSaveRequest.getPsCSpec1Ename());
        return sgPhyOutDeliverySaveRequest;
    }
}
