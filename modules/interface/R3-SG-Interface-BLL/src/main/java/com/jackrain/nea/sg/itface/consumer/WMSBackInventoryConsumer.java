package com.jackrain.nea.sg.itface.consumer;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.cpext.api.CpcPhyWareHouseQueryCmd;
import com.jackrain.nea.cpext.model.request.CpcPhyWareHouseQueryRequest;
import com.jackrain.nea.cpext.model.result.CpcPhyWareHouseQueryResult;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.ps.api.ProSkuListCmd;
import com.jackrain.nea.ps.api.request.ProSkuListCmdRequest;
import com.jackrain.nea.ps.api.result.ProSkuResult;
import com.jackrain.nea.ps.api.result.PsCProSkuResult;
import com.jackrain.nea.sg.basic.common.R3ParamConstants;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.common.SgResultCode;
import com.jackrain.nea.sg.basic.model.result.SgR3BaseResult;
import com.jackrain.nea.sg.phyadjust.api.SgPhyAdjustAuditR3Cmd;
import com.jackrain.nea.sg.phyadjust.api.SgPhyAdjustQueryCmd;
import com.jackrain.nea.sg.phyadjust.api.SgPhyAdjustSaveCmd;
import com.jackrain.nea.sg.phyadjust.common.SgPhyAdjustConstantsIF;
import com.jackrain.nea.sg.phyadjust.model.request.SgPhyAdjustBillQueryRequest;
import com.jackrain.nea.sg.phyadjust.model.request.SgPhyAdjustBillSaveRequest;
import com.jackrain.nea.sg.phyadjust.model.request.SgPhyAdjustItemSaveRequest;
import com.jackrain.nea.sg.phyadjust.model.request.SgPhyAdjustSaveRequest;
import com.jackrain.nea.sg.phyadjust.model.result.SgPhyAdjustBillQueryResult;
import com.jackrain.nea.sg.phyadjust.services.SgPhyAdjSaveAndAuditService;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.Tools;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.query.DefaultWebEvent;
import com.jackrain.nea.web.query.QuerySession;
import com.jackrain.nea.web.query.QuerySessionImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author caopengflying
 * @time 2019/5/7 9:36
 * @descript 库存盘点结果WMS回传服务 - 回传
 */
@Slf4j
@Component
public class WMSBackInventoryConsumer {
    @Autowired
    private SgPhyAdjSaveAndAuditService sgPhyAdjSaveAndAuditService;
    @Reference(group = "ps", version = "1.0")
    private ProSkuListCmd proSkuListCmd;
    @Reference(group = "cp-ext", version = "1.0")
    private CpcPhyWareHouseQueryCmd cpcPhyWareHouseQueryCmd;

    @Reference(group = "sg", version = "1.0")
    private SgPhyAdjustSaveCmd sgPhyAdjustSaveCmd;

    @Reference(group = "sg", version = "1.0")
    private SgPhyAdjustAuditR3Cmd sgPhyAdjustAuditR3Cmd;

    @Reference(group = "sg", version = "1.0")
    private SgPhyAdjustQueryCmd sgPhyAdjustQueryCmd;

    public Boolean consume(String message) {
        try {
            List<SgPhyAdjustBillSaveRequest> sgPhyAdjustBillSaveRequestList = parseInventoryMessage2SgPhyAdjust(message);
            if (null == sgPhyAdjustBillSaveRequestList || CollectionUtils.isEmpty(sgPhyAdjustBillSaveRequestList)) {
                log.error(this.getClass().getName() + " 转化WMS盘点结果单失败", message);
                return false;
            }
            for (SgPhyAdjustBillSaveRequest sgPhyAdjustBillSaveRequest : sgPhyAdjustBillSaveRequestList) {
                //必传字段，默认-1
                sgPhyAdjustBillSaveRequest.setObjId(-1L);
                if (log.isDebugEnabled()) {
                    log.debug(this.getClass().getName() + " 新增调整单入参 " + JSONObject.toJSONString(sgPhyAdjustBillSaveRequest));
                }

                ValueHolderV14<SgR3BaseResult> holderV14 = sgPhyAdjustSaveCmd.save(sgPhyAdjustBillSaveRequest);
                if (log.isDebugEnabled()) {
                    log.debug(this.getClass().getName() + " 新增调整单结果 " + JSONObject.toJSONString(holderV14));
                }
                if (SgResultCode.isFail(holderV14)) {
                    log.error(this.getClass().getName() + " 新增调整单失败", message);
                    return false;
                }

                Long objId = holderV14.getData().getDataJo().getLong(R3ParamConstants.OBJID);
                QuerySession session = new QuerySessionImpl(sgPhyAdjustBillSaveRequest.getLoginUser());
                DefaultWebEvent event = new DefaultWebEvent("audit", new HashMap(16));
                JSONObject param = new JSONObject();
                param.put("objid", objId);
                event.put("param", param);
                session.setEvent(event);
                ValueHolder execute = null;
                try {
                    execute = sgPhyAdjustAuditR3Cmd.execute(session);
                } catch (NDSException e) {
                    log.error(this.getClass().getName() + " 审核调整单失败", e);
                    return false;
                }
            }
        } catch (NDSException e) {
            e.printStackTrace();
            log.error(this.getClass().getName() + " 新增并审核调整单异常", e);
            return false;
        }
        return true;
    }

    /**
     * 奇云库存盘点结果WMS回传服务数据转化为库存调整单
     *
     * @param message
     * @return
     */
    private List<SgPhyAdjustBillSaveRequest> parseInventoryMessage2SgPhyAdjust(String message) {
        JSONObject result = JSONObject.parseObject(message);
        JSONObject request = result.getJSONObject("request");
        List<SgPhyAdjustBillSaveRequest> sgPhyAdjustBillSaveRequestList = Lists.newArrayList();
        //主表
        SgPhyAdjustSaveRequest sgBPhyAdjust = parseInventory(request);
        if (null == sgBPhyAdjust) {
            log.error(this.getClass().getName() + " 转换库存调整单主表失败 " + JSONObject.toJSONString(request));
            return null;
        }
        //子表
        List<SgPhyAdjustItemSaveRequest> itemSaveRequestList = parseInventoryItem(request.getJSONArray("items"));
        if (null == itemSaveRequestList || CollectionUtils.isEmpty(itemSaveRequestList)) {
            log.error(this.getClass().getName() + " 转换库存调整单子表失败 " + JSONObject.toJSONString(request));
            return null;
        }
/*        List<SgPhyAdjustItemSaveRequest> SgPhyAdjustItemSaveRequestMore =
                itemSaveRequestList.stream().filter(sgPhyAdjustItemSaveRequest -> sgPhyAdjustItemSaveRequest.getQty().compareTo(BigDecimal.ZERO) >= 0).collect(Collectors.toList());
        List<SgPhyAdjustItemSaveRequest> SgPhyAdjustItemSaveRequestLess =
                itemSaveRequestList.stream().filter(sgPhyAdjustItemSaveRequest -> sgPhyAdjustItemSaveRequest.getQty().compareTo(BigDecimal.ZERO) < 0).collect(Collectors.toList());
        if (CollectionUtils.isNotEmpty(SgPhyAdjustItemSaveRequestMore)){
            //正数调整单
            SgPhyAdjustBillSaveRequest sgPhyAdjustBillSaveRequestMore = new SgPhyAdjustBillSaveRequest();
            sgPhyAdjustBillSaveRequestMore.setPhyAdjust(sgBPhyAdjust);
            sgPhyAdjustBillSaveRequestMore.setItems(SgPhyAdjustItemSaveRequestMore);
            sgPhyAdjustBillSaveRequestMore.setLoginUser(WMSUserFactory.initWmsUser(WMSUserFactory.DEFAULT_ORG_ID,
                    WMSUserFactory.DEFAULT_CLIENT_ID));
            sgPhyAdjustBillSaveRequestList.add(sgPhyAdjustBillSaveRequestMore);
        }*/
        if (CollectionUtils.isNotEmpty(itemSaveRequestList)) {
            //调整单
            SgPhyAdjustBillSaveRequest sgPhyAdjustBillSaveRequest = new SgPhyAdjustBillSaveRequest();
            sgPhyAdjustBillSaveRequest.setPhyAdjust(sgBPhyAdjust);
            sgPhyAdjustBillSaveRequest.setItems(itemSaveRequestList);
            sgPhyAdjustBillSaveRequest.setLoginUser(WMSUserFactory.initWmsUser(WMSUserFactory.DEFAULT_ORG_ID,
                    WMSUserFactory.DEFAULT_CLIENT_ID));
            sgPhyAdjustBillSaveRequestList.add(sgPhyAdjustBillSaveRequest);
        }
        return sgPhyAdjustBillSaveRequestList;
    }

    /**
     * 转换wms message 为库存调整单从表
     *
     * @param items
     * @return
     */
    private List<SgPhyAdjustItemSaveRequest> parseInventoryItem(JSONArray items) {
        List<SgPhyAdjustItemSaveRequest> itemSaveRequestList = Lists.newArrayList();
        for (int i = 0; i < items.size(); i++) {
            JSONObject itemRequest = ((JSONObject) items.get(i)).getJSONObject("item");
            SgPhyAdjustItemSaveRequest sgPhyAdjustItemSaveRequest = new SgPhyAdjustItemSaveRequest();
            //接口字段quantity传的值
            sgPhyAdjustItemSaveRequest.setQty(itemRequest.getBigDecimal("quantity"));
            //接口字段itemCode传的值
            sgPhyAdjustItemSaveRequest.setPsCSkuEcode(itemRequest.getString("itemCode"));
            PsCProSkuResult psCProSkuResult = selectProdSkuInfoBySku(Lists.newArrayList(itemRequest.getString("itemCode")));
            if (null == psCProSkuResult) {
                log.error(this.getClass().getName() + " 查询商品sku失败 " + JSONObject.toJSONString(itemRequest));
                return null;
            }
            //条码ID ps_c_sku_id
            sgPhyAdjustItemSaveRequest.setPsCSkuId(psCProSkuResult.getId());
            //商品id ps_c_pro_id
            sgPhyAdjustItemSaveRequest.setPsCProId(psCProSkuResult.getPsCProId());
            //商品编码 ps_c_pro_ecode
            sgPhyAdjustItemSaveRequest.setPsCProEcode(psCProSkuResult.getPsCProEcode());
            //商品名称 ps_c_pro_ename
            sgPhyAdjustItemSaveRequest.setPsCProEname(psCProSkuResult.getPsCProEname());
            //规格1id ps_c_spec1_id
            sgPhyAdjustItemSaveRequest.setPsCSpec1Id(psCProSkuResult.getPsCSpec1objId());
            //规格1编码 ps_c_spec1_ecode
            sgPhyAdjustItemSaveRequest.setPsCSpec1Ecode(psCProSkuResult.getClrsEcode());
            //规格1名称 ps_c_spec1_ename
            sgPhyAdjustItemSaveRequest.setPsCSpec1Ename(psCProSkuResult.getClrsEname());
            //规格2id ps_c_spec2_id
            sgPhyAdjustItemSaveRequest.setPsCSpec2Id(psCProSkuResult.getPsCSpec2objId());
            //规格2编码 ps_c_spec2_ecode
            sgPhyAdjustItemSaveRequest.setPsCSpec2Ecode(psCProSkuResult.getSizesEcode());
            //规格2名称 ps_c_spec2_ename
            sgPhyAdjustItemSaveRequest.setPsCSpec2Ename(psCProSkuResult.getSizesEname());
            //吊牌价 price_list
            sgPhyAdjustItemSaveRequest.setPriceList(psCProSkuResult.getPricelist());
            //国标码
            sgPhyAdjustItemSaveRequest.setGbcode(psCProSkuResult.getGbcode());
            //默认设置为-1
            sgPhyAdjustItemSaveRequest.setSourceBillItemId(-1L);
            itemSaveRequestList.add(sgPhyAdjustItemSaveRequest);
        }
        return itemSaveRequestList;
    }

    /**
     * 转换wmsmessage为库存调整单主表
     *
     * @param request
     * @return
     */
    private SgPhyAdjustSaveRequest parseInventory(JSONObject request) {
        String inventoryType = ((JSONObject) request.getJSONArray("items").get(0)).getJSONObject("item").getString("inventoryType");
        SgPhyAdjustSaveRequest sgBPhyAdjust = new SgPhyAdjustSaveRequest();

        //单据类型
        sgBPhyAdjust.setBillType(SgPhyAdjustConstantsIF.BILL_TYPE_NORMAL);
        sgBPhyAdjust.setSgBAdjustPropId(8L);
        String adjustType = request.getString("orderType");
        //来源单据类型
        if (StringUtils.isEmpty(adjustType) || "CHECK".equals(adjustType) || "PD".equals(adjustType)) {
            sgBPhyAdjust.setSourceBillType(SgPhyAdjustConstantsIF.SOURCE_BILL_TYPE_PAND);
        } else if ("ADJUST".equals(adjustType)) {
            sgBPhyAdjust.setSourceBillType(SgPhyAdjustConstantsIF.SOURCE_BILL_TYPE_ZPCC);
        }

        //查询调整单是否存在
        log.info(this.getClass().getName() + " 查询调整单是否存在,来源单据类型:{},来源单据编号:{}"
                , sgBPhyAdjust.getBillType(), request.getString("checkOrderId"));
        SgPhyAdjustBillQueryRequest sgPhyAdjustBillQueryRequest = new SgPhyAdjustBillQueryRequest();
        sgPhyAdjustBillQueryRequest.setSourceBillNo(request.getString("checkOrderId"));
        sgPhyAdjustBillQueryRequest.setSourceBillType(sgBPhyAdjust.getSourceBillType());

        ValueHolderV14<SgPhyAdjustBillQueryResult> ValueHolderV14 = sgPhyAdjustQueryCmd.queryPhyAdj(sgPhyAdjustBillQueryRequest);
        if (ResultCode.FAIL == ValueHolderV14.getCode() || ValueHolderV14.getData().getAdjust() != null) {
            log.error(this.getClass().getName() + " 该调整单已经存在");
            return null;
        }
        //商品标记
        if (SgConstantsIF.TRADE_MARK_ZP.equals(inventoryType)) {
            sgBPhyAdjust.setTradeMark(SgConstantsIF.TRADE_MARK_ZP);
        } else if (SgConstantsIF.TRADE_MARK_CC.equals(inventoryType)) {
            sgBPhyAdjust.setTradeMark(SgConstantsIF.TRADE_MARK_CC);
        }
        CpcPhyWareHouseQueryRequest cpcPhyWareHouseQueryRequest = new CpcPhyWareHouseQueryRequest();
        cpcPhyWareHouseQueryRequest.setWmsWarehouseCode(Lists.newArrayList(request.getString("warehouseCode")));
        ValueHolderV14<CpcPhyWareHouseQueryResult> execute = cpcPhyWareHouseQueryCmd.execute(cpcPhyWareHouseQueryRequest);
        if (SgResultCode.isFail(execute) || CollectionUtils.isEmpty(execute.getData().getCpCPhyWarehouseList())) {
            log.error(this.getClass().getName() + " 查询逻辑仓失败！入参",
                    request.getString("warehouseCode") + "出参" + JSONObject.toJSONString(execute));
            return null;
        }
        execute.getData().getCpCPhyWarehouseList().get(0).getEname();
        //实体仓ID
        sgBPhyAdjust.setCpCPhyWarehouseId(execute.getData().getCpCPhyWarehouseList().get(0).getId());
        //实体仓名称
        sgBPhyAdjust.setCpCPhyWarehouseEname(execute.getData().getCpCPhyWarehouseList().get(0).getEname());
        //实体仓Code
        sgBPhyAdjust.setCpCPhyWarehouseEcode(execute.getData().getCpCPhyWarehouseList().get(0).getEcode());
        sgBPhyAdjust.setSourceBillNo(request.getString("checkOrderId"));
        sgBPhyAdjust.setWmsBillNo(request.getString("checkOrderId"));
        return sgBPhyAdjust;
    }

    /**
     * 调用SKUCMD查询商品数据
     *
     * @param skuList SKU编码列表
     * @return 查询结果
     */
    public PsCProSkuResult selectProdSkuInfoBySku(List<String> skuList) {
        ProSkuListCmdRequest listCmdRequest = new ProSkuListCmdRequest();
        listCmdRequest.setEcodes(skuList);
        listCmdRequest.setSkuids(new ArrayList<>());
        ValueHolder holder = proSkuListCmd.execute(listCmdRequest);
        int code = Tools.getInt(holder.get("code"), -1);
        if (code == 0) {
            if (holder.getData().containsKey("data")) {
                return ((ProSkuResult) holder.get("data")).getProSkuList().get(0);
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
}
