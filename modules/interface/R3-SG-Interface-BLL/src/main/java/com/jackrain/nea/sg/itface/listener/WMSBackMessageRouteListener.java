package com.jackrain.nea.sg.itface.listener;

import com.alibaba.fastjson.JSONObject;
import com.aliyun.openservices.ons.api.Action;
import com.aliyun.openservices.ons.api.ConsumeContext;
import com.aliyun.openservices.ons.api.Message;
import com.aliyun.openservices.ons.api.MessageListener;
import com.aliyun.openservices.ons.api.impl.util.MsgConvertUtil;
import com.jackrain.nea.sg.itface.consumer.*;
import com.jackrain.nea.sg.itface.out.common.MQConstantsIF;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author caopengflying
 * @time 2019/5/6 10:22
 * @descript 奇云回传
 */
@Slf4j
@Component
public class WMSBackMessageRouteListener implements MessageListener {
    @Autowired
    private WMSBackStockChangeConsumer wmsBackStockChangeConsumer;
    @Autowired
    private WMSBackStockOutConsumer wmsBackStockOutConsumer;
    @Autowired
    private WMSBackDeliveryOrderConsumer wmsBackDeliveryOrderConsumer;
    @Autowired
    private WMSBackInventoryConsumer wmsBackInventoryConsumer;
    @Autowired
    private WMSBackEntryOrderConsumer wmsBackEntryOrderConsumer;

    @Override
    public Action consume(Message message, ConsumeContext consumeContext) {
        try {
            String messageBody = MsgConvertUtil.objectDeserialize(message.getBody()).toString();
            log.info(this.getClass().getName() + " 接收wms回传mq消息 messageBody{},messageKey{}," +
                            "messageId{}," +
                            "messageTopic{}",
                    new Object[]{messageBody, message.getKey(), message.getMsgID(), message.getTopic()});
            JSONObject result = JSONObject.parseObject(messageBody);
            String method = result.getString("method");
            Boolean flag = false;
            if (MQConstantsIF.WMS_BACK_DELIVERY_ORDER_METHOD.equals(method)) {
                //(云枢纽回传库存中心)零售发货单 - 回传
                flag = wmsBackDeliveryOrderConsumer.consume(messageBody);
            } else if (MQConstantsIF.WMS_BACK_ENTRY_ORDER_METHOD.equals(method)) {
                //大货入库WMS回传服务 - 回传
                flag = wmsBackEntryOrderConsumer.consume(messageBody);
            } else if (MQConstantsIF.WMS_BACK_STOCK_OUT_METHOD.equals(method)) {
                //大货出库WMS回传服务 - 回传
                flag = wmsBackStockOutConsumer.consume(messageBody);
            } else if (MQConstantsIF.WMS_BACK_STOCK_CHANGE_METHOD.equals(method)) {
                //库存调整结果WMS回传服务 - 回传
                flag = wmsBackStockChangeConsumer.consumeNew(messageBody);
            } else if (MQConstantsIF.WMS_BACK_INVENTORY_METHOD.equals(method)) {
                //库存盘点结果WMS回传服务 - 回传
                flag = wmsBackInventoryConsumer.consume(messageBody);
            }

            if (flag)
                return Action.CommitMessage;
            else
                return Action.ReconsumeLater;
            
        } catch (Exception e) {
            e.printStackTrace();
            return Action.ReconsumeLater;
        }
    }
}
