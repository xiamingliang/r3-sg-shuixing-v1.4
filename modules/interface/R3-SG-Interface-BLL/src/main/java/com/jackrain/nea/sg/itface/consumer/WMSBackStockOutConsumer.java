package com.jackrain.nea.sg.itface.consumer;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.util.StringUtil;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.jackrain.nea.cpext.api.CpLogisticsSelectServiceCmd;
import com.jackrain.nea.cpext.model.table.CpLogistics;
import com.jackrain.nea.sg.basic.common.SgResultCode;
import com.jackrain.nea.sg.basic.config.SgStorageBoxConfig;
import com.jackrain.nea.sg.itface.out.common.MQConstantsIF;
import com.jackrain.nea.sg.out.model.request.*;
import com.jackrain.nea.sg.out.model.result.SgOutNoticesByBillNoResult;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutNotices;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutNoticesImpItem;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutNoticesItem;
import com.jackrain.nea.sg.out.services.SgPhyOutNoticesSaveService;
import com.jackrain.nea.sg.out.services.SgPhyOutResultSaveAndAuditService;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author caopengflying
 * @time 2019/5/7 9:26
 * @descript 大货出库WMS回传服务 - 回传
 */
@Slf4j
@Component
public class WMSBackStockOutConsumer {
    @Autowired
    private SgPhyOutNoticesSaveService sgPhyOutNoticesSaveService;
    @Autowired
    private SgPhyOutResultSaveAndAuditService sgPhyOutResultSaveAndAuditService;
    @Reference(group = "cp-ext", version = "1.0")
    private CpLogisticsSelectServiceCmd cpLogisticsSelectServiceCmd;

    @Autowired
    private SgStorageBoxConfig storageBoxConfig;

    public Boolean consume(String message) {
        try {
            SgPhyOutResultBillSaveRequest sgPhyOutResultBillSaveRequest =
                    parseStockOutMessage2SgPhyOutResultBillSaveRequest(message);
            if (null == sgPhyOutResultBillSaveRequest) {
                log.error(this.getClass().getName() +
                        " 查询出库通知单失败", message);
                return false;
            }
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + " 新增并审核出库结果通知单入参 " + JSONObject.toJSONString(sgPhyOutResultBillSaveRequest));
            }
            ValueHolderV14 holder = sgPhyOutResultSaveAndAuditService.saveOutResultAndAudit(sgPhyOutResultBillSaveRequest);
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + " 新增并审核出库结果通知单结果 " + JSONObject.toJSONString(holder));
            }
            if (SgResultCode.isFail(holder)) {
                log.error(this.getClass().getName() + " 新增并审核出库通知单失败", holder.getMessage());
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(this.getClass().getName() + " 新增并审核出库通知单异常", e);
            return false;
        }
        return true;
    }

    /**
     * 将出库通知单信息与WMS消息组合成出库结果单
     *
     * @param message
     * @return
     */
    public SgPhyOutResultBillSaveRequest parseStockOutMessage2SgPhyOutResultBillSaveRequest(String message) {
        JSONObject request = JSONObject.parseObject(message).getJSONObject("request");
        JSONObject deliveryOrderObject = request.getJSONObject("deliveryOrder");
        JSONObject packageObject = ((JSONObject) request.getJSONArray("packages").get(0)).getJSONObject("package");
        String deliveryOrderCode = deliveryOrderObject.getString("deliveryOrderCode");
        log.debug("[" + deliveryOrderCode + "],出库通知单开始回传!");
        List<SgPhyOutNoticesSaveRequest> sgPhyOutNoticesSaveRequestList = Lists.newArrayList();
        SgPhyOutNoticesSaveRequest sgPhyOutNoticesSaveRequest = new SgPhyOutNoticesSaveRequest();
        sgPhyOutNoticesSaveRequest.setBillNo(deliveryOrderCode);
        sgPhyOutNoticesSaveRequestList.add(sgPhyOutNoticesSaveRequest);
        ValueHolderV14<List<SgOutNoticesByBillNoResult>> holder =
                sgPhyOutNoticesSaveService.queryOutNoticesByBillNo(sgPhyOutNoticesSaveRequestList);
        List<SgOutNoticesByBillNoResult> data = holder.getData();
        if (SgResultCode.isFail(holder) || CollectionUtils.isEmpty(data)) {
            return null;
        }
        SgPhyOutResultBillSaveRequest sgPhyOutResultBillSaveRequest = new SgPhyOutResultBillSaveRequest();
        SgOutNoticesByBillNoResult sgOutNoticesByBillNoResult = data.get(0);
        SgBPhyOutNotices outNotices = sgOutNoticesByBillNoResult.getOutNotices();
        //主表转换
        SgPhyOutResultSaveRequest sgPhyOutResultSaveRequest = parseOutNoticeToOutResult(outNotices,
                deliveryOrderObject, packageObject);

        JSONArray orderLines = request.getJSONArray("orderLines");
        String logisticCode;
        String logisticName;
        String expressCode;
        //将零售发货回传与大货出库回传进行区分
        if (MQConstantsIF.WMS_BACK_DELIVERY_ORDER_METHOD.equals(JSONObject.parseObject(message).getString("method"))) {
            logisticCode = packageObject.getString("logisticsCode");
            logisticName = packageObject.getString("logisticsName");
            expressCode = packageObject.getString("expressCode");
            //2019-08-12增加逻辑: 取logisticsCode从菜鸟映射表中取出对应物流公司档案的cp_c_logistics_ecode
            if (log.isDebugEnabled()) {
                log.debug("回传logisticCode参数:" + logisticCode);
            }
            if (StringUtils.isNotEmpty(logisticCode)) {
                String logisticEcodeTransform = cpLogisticsSelectServiceCmd.logisticEcodeTransform(logisticCode);
                if (log.isDebugEnabled()) {
                    log.debug("logisticCode菜鸟映射结果:" + logisticEcodeTransform);
                }
                if (StringUtils.isNotEmpty(logisticEcodeTransform)) {
                    logisticCode = logisticEcodeTransform;
                }
            }

            //平台店铺ID
            sgPhyOutResultSaveRequest.setCpCShopId(outNotices.getCpCShopId());
            //平台店铺标题
            sgPhyOutResultSaveRequest.setCpCShopTitle(outNotices.getCpCShopTitle());
            //平台编号
            sgPhyOutResultSaveRequest.setSourcecode(outNotices.getSourcecode());
            //客服备注
            sgPhyOutResultSaveRequest.setSellerRemark(outNotices.getSellerRemark());
            //买家备注
            sgPhyOutResultSaveRequest.setBuyerRemark(outNotices.getBuyerRemark());
        } else {
            logisticCode = deliveryOrderObject.getString("logisticsCode");
            logisticName = deliveryOrderObject.getString("logisticsName");
            expressCode = deliveryOrderObject.getString("expressCode");
        }
        //物流公司ID
        CpLogistics cpLogistics = cpLogisticsSelectServiceCmd.queryCpLogisticByEcode(logisticCode);
        if (null != cpLogistics) {
            sgPhyOutResultSaveRequest.setCpCLogisticsId(cpLogistics.getId());
            //物流公司名称
            sgPhyOutResultSaveRequest.setCpCLogisticsEname(cpLogistics.getEname());
        }
        //物流公司Code
        sgPhyOutResultSaveRequest.setCpCLogisticsEcode(logisticCode);        //物流单号
        sgPhyOutResultSaveRequest.setLogisticNumber(expressCode);
        if (null == sgPhyOutResultSaveRequest) {
            return null;
        }
        sgPhyOutResultBillSaveRequest.setOutResultRequest(sgPhyOutResultSaveRequest);
        //从表转换
        if(storageBoxConfig.getBoxEnable()) {// alter by nrl 添加箱功能
            List<SgBPhyOutNoticesImpItem> impitems=sgOutNoticesByBillNoResult.getImpItems();
            List<SgPhyOutResultImpItemSaveRequest> outResultItemSaveRequestList = parseOutNoticeImplItemToOutResultItem(impitems,
                    orderLines);
            if (null == outResultItemSaveRequestList) {
                return null;
            }
            sgPhyOutResultBillSaveRequest.setImpItemList(outResultItemSaveRequestList);
        }
        else{
            List<SgBPhyOutNoticesItem> items = sgOutNoticesByBillNoResult.getItems();
            List<SgPhyOutResultItemSaveRequest> outResultItemSaveRequestList = parseOutNoticeItemToOutResultItem(items,
                    orderLines);
            if (null == outResultItemSaveRequestList) {
                return null;
            }
            sgPhyOutResultBillSaveRequest.setOutResultItemRequests(outResultItemSaveRequestList);
        }
        //出库人信息
        User user = WMSUserFactory.initWmsUser(outNotices.getAdOrgId().intValue(), outNotices.getAdClientId().intValue());
        sgPhyOutResultBillSaveRequest.setLoginUser(user);
        //默认设置ObjId为-1
        sgPhyOutResultBillSaveRequest.setObjId(-1L);
        return sgPhyOutResultBillSaveRequest;
    }

    /**
     * 出库通知单从表+message消息转化为出库结果单从表
     *
     * @param items
     * @param orderLines
     * @return
     */
    private List<SgPhyOutResultItemSaveRequest> parseOutNoticeItemToOutResultItem(List<SgBPhyOutNoticesItem> items, JSONArray orderLines) {
        HashMap<String, SgPhyOutResultItemSaveRequest> map = Maps.newHashMap();
        List<SgPhyOutResultItemSaveRequest> outResultItemSaveRequestList = Lists.newArrayList();
        for (int i = 0; i < orderLines.size(); i++) {
            BigDecimal actualQty = ((JSONObject) orderLines.get(i)).getJSONObject("orderLine").getBigDecimal("actualQty");
            String itemCode = ((JSONObject) orderLines.get(i)).getJSONObject("orderLine").getString("itemCode");
            List<SgBPhyOutNoticesItem> sgBPhyOutNoticesItems =
                    items.stream().filter(s -> s.getPsCSkuEcode().equals(itemCode)).collect(Collectors.toList());
            if (CollectionUtils.isEmpty(sgBPhyOutNoticesItems)) {
                log.error(this.getClass().getName() + " 出库通知单中不包含该sku！[" + itemCode + "]" + JSONObject.toJSONString(items));
                return null;
            }
            SgBPhyOutNoticesItem noticesItem = sgBPhyOutNoticesItems.get(0);
            if (map.containsKey(itemCode)) {
                SgPhyOutResultItemSaveRequest outResultItem = map.get(itemCode);
                BigDecimal qty = Optional.ofNullable(outResultItem.getQty()).orElse(BigDecimal.ZERO);
                qty = qty.add(actualQty);
                outResultItem.setQty(qty);
            } else {
                SgPhyOutResultItemSaveRequest outResultItem = new SgPhyOutResultItemSaveRequest();
                BeanUtils.copyProperties(noticesItem, outResultItem);
                //关联出库通知单明细ID
                outResultItem.setSgBPhyOutNoticesItemId(noticesItem.getId());
                //出库数量
                outResultItem.setQty(actualQty);
                map.put(itemCode, outResultItem);
            }
        }
        if (MapUtils.isNotEmpty(map) && CollectionUtils.isNotEmpty(map.values()))
            outResultItemSaveRequestList.addAll(map.values());
        return outResultItemSaveRequestList;
    }

    /**
     * 出库通知单从表+message消息转化为出库结果单从表(录入明细转换)
     *  add by nrl 箱功能匹配
     * @param items
     * @param orderLines
     * @return
     */
    private List<SgPhyOutResultImpItemSaveRequest> parseOutNoticeImplItemToOutResultItem(List<SgBPhyOutNoticesImpItem> items, JSONArray orderLines) {
        HashMap<String, SgPhyOutResultImpItemSaveRequest> map = Maps.newHashMap();
        List<SgPhyOutResultImpItemSaveRequest> outResultItemSaveRequestList = Lists.newArrayList();

        for (int i = 0; i < orderLines.size(); i++) {
            BigDecimal actualQty = ((JSONObject) orderLines.get(i)).getJSONObject("orderLine").getBigDecimal("actualQty");
            String itemCode = ((JSONObject) orderLines.get(i)).getJSONObject("orderLine").getString("itemCode");
            String boxCode=((JSONObject) orderLines.get(i)).getJSONObject("orderLine").getString("produceCode");
            String matchCode=StringUtil.isEmpty(boxCode)?itemCode:boxCode;
            BigDecimal matchNum=StringUtil.isEmpty(boxCode)?actualQty:BigDecimal.ONE;
            boolean isBox=StringUtil.isEmpty(boxCode)?false:true;

            List<SgBPhyOutNoticesImpItem> sgBPhyOutNoticesItems =
                    items.stream().filter(s -> s.getPsCSkuEcode().equals(matchCode)).collect(Collectors.toList());
            if (CollectionUtils.isEmpty(sgBPhyOutNoticesItems)) {
                log.error(this.getClass().getName() + " 出库通知单中不包含该明细！[" + matchCode + "]" + JSONObject.toJSONString(items));
                return null;
            }
            SgBPhyOutNoticesImpItem noticesItem = sgBPhyOutNoticesItems.get(0);

            if (map.containsKey(matchCode)) {
                if(!isBox) {
                    SgPhyOutResultImpItemSaveRequest outResultItem = map.get(itemCode);
                    BigDecimal qty = Optional.ofNullable(outResultItem.getQtyOut()).orElse(BigDecimal.ZERO);
                    qty = qty.add(matchNum);
                    outResultItem.setQtyOut(qty);
                }
            } else {
                SgPhyOutResultImpItemSaveRequest outResultItem = new SgPhyOutResultImpItemSaveRequest();
                BeanUtils.copyProperties(noticesItem, outResultItem);
                //出库数量
                outResultItem.setQtyOut(matchNum);
                map.put(itemCode, outResultItem);
            }
        }
        if (MapUtils.isNotEmpty(map) && CollectionUtils.isNotEmpty(map.values()))
            outResultItemSaveRequestList.addAll(map.values());
        return outResultItemSaveRequestList;
    }


    /**
     * 出库通知单主表+message消息转化为出库结果单主表
     *
     * @param outNotices
     * @param deliveryOrderObject
     * @param packageObject
     * @return
     */
    private SgPhyOutResultSaveRequest parseOutNoticeToOutResult(SgBPhyOutNotices outNotices, JSONObject deliveryOrderObject, JSONObject packageObject) {
        SgPhyOutResultSaveRequest sgPhyOutResultSaveRequest = new SgPhyOutResultSaveRequest();
        //出库时间
        sgPhyOutResultSaveRequest.setOutTime(deliveryOrderObject.getDate("orderConfirmTime"));
        //关联出库通知单编号
        sgPhyOutResultSaveRequest.setSgBPhyOutNoticesBillno(deliveryOrderObject.getString("deliveryOrderCode"));
        //实体仓ID
        sgPhyOutResultSaveRequest.setCpCPhyWarehouseId(outNotices.getCpCPhyWarehouseId());
        //实体仓code
        sgPhyOutResultSaveRequest.setCpCPhyWarehouseEcode(outNotices.getCpCPhyWarehouseEcode());
        //实体仓名称
        sgPhyOutResultSaveRequest.setCpCPhyWarehouseEname(outNotices.getCpCPhyWarehouseEname());
        //货主编号
        sgPhyOutResultSaveRequest.setGoodsOwner(outNotices.getGoodsOwner());
        //收货方名称
        sgPhyOutResultSaveRequest.setCpCCsEname(outNotices.getCpCCsEname());
        //收货方code
        sgPhyOutResultSaveRequest.setCpCCsEcode(outNotices.getCpCCsEcode());
        //经销商实体仓ID
        sgPhyOutResultSaveRequest.setCpCCustomerWarehouseId(outNotices.getCpCCustomerWarehouseId());
        //供应商ID
        sgPhyOutResultSaveRequest.setCpCSupplierId(outNotices.getCpCSupplierId());
        //出库类型
        sgPhyOutResultSaveRequest.setOutType(outNotices.getOutType());
        //来源单据类型
        sgPhyOutResultSaveRequest.setSourceBillType(outNotices.getSourceBillType());
        //来源单据ID
        sgPhyOutResultSaveRequest.setSourceBillId(outNotices.getSourceBillId());
        //来源单据编号
        sgPhyOutResultSaveRequest.setSourceBillNo(outNotices.getSourceBillNo());
        //关联出库通知单ID
        sgPhyOutResultSaveRequest.setSgBPhyOutNoticesId(outNotices.getId());
        //关联出库通知单编号
        sgPhyOutResultSaveRequest.setSgBPhyOutNoticesBillno(outNotices.getBillNo());
        //sg_b_phy_in_notices_billno
        //收货人
        sgPhyOutResultSaveRequest.setReceiverName(outNotices.getReceiverName());
        //收货人手机
        sgPhyOutResultSaveRequest.setReceiverPhone(outNotices.getReceiverPhone());
        //收货人电话
        sgPhyOutResultSaveRequest.setReceiverMobile(outNotices.getReceiverMobile());
        //省id
        sgPhyOutResultSaveRequest.setCpCRegionProvinceId(outNotices.getCpCRegionProvinceId());
        //省编码
        sgPhyOutResultSaveRequest.setCpCRegionProvinceEcode(outNotices.getCpCRegionProvinceEcode());
        //省名称
        sgPhyOutResultSaveRequest.setCpCRegionProvinceEname(outNotices.getCpCRegionProvinceEname());
        //市id
        sgPhyOutResultSaveRequest.setCpCRegionCityId(outNotices.getCpCRegionCityId());
        //市编码
        sgPhyOutResultSaveRequest.setCpCRegionCityEcode(outNotices.getCpCRegionCityEcode());
        //市名称
        sgPhyOutResultSaveRequest.setCpCRegionCityEname(outNotices.getCpCRegionCityEname());
        //区id
        sgPhyOutResultSaveRequest.setCpCRegionAreaId(outNotices.getCpCRegionAreaId());
        //区编码
        sgPhyOutResultSaveRequest.setCpCRegionAreaEcode(outNotices.getCpCRegionAreaEcode());
        //区名称
        sgPhyOutResultSaveRequest.setCpCRegionAreaEname(outNotices.getCpCRegionAreaEname());
        //收货人详细地址
        sgPhyOutResultSaveRequest.setReceiverAddress(outNotices.getReceiverAddress());
        //收货人邮编
        sgPhyOutResultSaveRequest.setReceiverZip(outNotices.getReceiverZip());

        //备注
        sgPhyOutResultSaveRequest.setRemark(outNotices.getRemark());
        //新增确认类型
        sgPhyOutResultSaveRequest.setIsLast(deliveryOrderObject.getInteger("confirmType"));
        return sgPhyOutResultSaveRequest;
    }
}
