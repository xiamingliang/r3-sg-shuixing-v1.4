package com.jackrain.nea.sg.itface.consumer;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.sg.basic.common.SgResultCode;
import com.jackrain.nea.sg.out.model.request.SgPhyOutNoticesBillWMSBackRequest;
import com.jackrain.nea.sg.out.services.SgPhyOutNoticesSaveWMSBackService;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author caopengflying
 * @time 2019/5/7 9:41
 * @descript 大货出库通知单传WMS - 回执
 */
@Slf4j
@Component
public class WMSCallStockOutConsumer {
    @Autowired
    private SgPhyOutNoticesSaveWMSBackService sgPhyOutNoticesSaveWMSBackService;
    public Boolean consume(String messageBody){
        try {
            String datas = JSONObject.parseObject(messageBody).getJSONArray("data").toJSONString();
            List<SgPhyOutNoticesBillWMSBackRequest> sgPhyOutNoticesBillWMSBackRequests = JSON.parseArray(datas,
                    SgPhyOutNoticesBillWMSBackRequest.class);
            for (SgPhyOutNoticesBillWMSBackRequest sgPhyOutNoticesBillWMSBackRequest : sgPhyOutNoticesBillWMSBackRequests) {
                sgPhyOutNoticesBillWMSBackRequest.setFlag(1);
            }
            ValueHolderV14 holder =
                    sgPhyOutNoticesSaveWMSBackService.saveWMSBackOutNotices(sgPhyOutNoticesBillWMSBackRequests,
                    WMSUserFactory.initWmsUser(null, null));
            if (SgResultCode.isFail(holder)){
                log.error(this.getClass().getName() + " 大货出库通知单传WMS失败", holder.getMessage());

                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(this.getClass().getName() + " 大货出库通知单传WMS回执异常", e);
            return false;
        }
        return true;
    }

}
