package com.jackrain.nea.sg.itface.listener;

import com.alibaba.fastjson.JSONObject;
import com.aliyun.openservices.ons.api.Action;
import com.aliyun.openservices.ons.api.ConsumeContext;
import com.aliyun.openservices.ons.api.Message;
import com.aliyun.openservices.ons.api.MessageListener;
import com.aliyun.openservices.ons.api.impl.util.MsgConvertUtil;
import com.jackrain.nea.sg.itface.consumer.WMSCallDeliveryOrderConsumer;
import com.jackrain.nea.sg.itface.consumer.WMSCallEntryOrderConsumer;
import com.jackrain.nea.sg.itface.consumer.WMSCallStockOutConsumer;
import com.jackrain.nea.sg.itface.out.common.MQConstantsIF;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author caopengflying
 * @time 2019/5/6 10:23
 */
@Slf4j
@Component
public class WMSCallMessageRouteListener implements MessageListener {
    @Autowired
    private WMSCallDeliveryOrderConsumer wmsCallDeliveryOrderConsumer;
    @Autowired
    private WMSCallStockOutConsumer wmsCallStockOutConsumer;
    @Autowired
    private WMSCallEntryOrderConsumer wmsCallEntryOrderConsumer;

    @Override
    public Action consume(Message message, ConsumeContext consumeContext) {
        try {
            String messageBody = MsgConvertUtil.objectDeserialize(message.getBody()).toString();
            log.info(this.getClass().getName() + " 接收wms回执mq消息 messageBody{},messageKey{}," +
                            "messageId{}," +
                            "messageTopic{}",
                    new Object[]{messageBody, message.getKey(), message.getMsgID(), message.getTopic()});
            JSONObject result = JSONObject.parseObject(messageBody);
            String method = (String) result.get("method");
            Boolean flag = false;
            if (MQConstantsIF.WMS_CALL_ENTRY_ORDER_METHOD.equals(method)) {
                //大货入库通知单传WMS - 回执
                flag = wmsCallEntryOrderConsumer.consume(messageBody);
            } else if (MQConstantsIF.WMS_CALL_STOCK_OUT_METHOD.equals(method)||MQConstantsIF.WMS_CALL_DIFF_ADJUST_METHOD.equals(method)) {
                //大货出库通知单传WMS - 回执||库存差异调整传WMS-回执
                flag = wmsCallStockOutConsumer.consume(messageBody);
            } else if (MQConstantsIF.WMS_CALL_DELIVERY_ORDER_METHOD.equals(method)) {
                //新增出库通知单并传WMS服务 - 回执
                flag = wmsCallDeliveryOrderConsumer.consume(messageBody);
            }

            if (flag)
                return Action.CommitMessage;
            else
                return Action.ReconsumeLater;

        } catch (Exception e) {
            e.printStackTrace();
            return Action.ReconsumeLater;
        }
    }
}
