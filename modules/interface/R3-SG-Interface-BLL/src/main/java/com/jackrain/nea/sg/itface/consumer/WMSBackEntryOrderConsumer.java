package com.jackrain.nea.sg.itface.consumer;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.common.SgResultCode;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.in.model.request.SgBPhyInResultDefectItemSaveRequest;
import com.jackrain.nea.sg.in.model.request.SgBPhyInResultItemSaveRequest;
import com.jackrain.nea.sg.in.model.request.SgBPhyInResultSaveRequest;
import com.jackrain.nea.sg.in.model.request.SgPhyInResultBillSaveRequest;
import com.jackrain.nea.sg.in.model.result.SgBPhyInNoticesResult;
import com.jackrain.nea.sg.in.model.table.SgBPhyInNotices;
import com.jackrain.nea.sg.in.model.table.SgBPhyInNoticesItem;
import com.jackrain.nea.sg.in.services.SgPhyInNoticesSelectService;
import com.jackrain.nea.sg.in.services.SgPhyInResultSaveAndAuditService;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author caopengflying
 * @time 2019/5/7 9:28
 * @descript 大货入库WMS回传服务 - 回传
 */

@Slf4j
@Component
public class WMSBackEntryOrderConsumer {
    @Autowired
    private SgPhyInResultSaveAndAuditService sgPhyInResultSaveAndAuditService;
    @Autowired
    private SgPhyInNoticesSelectService sgPhyInNoticesSelectService;

    public Boolean consume(String message) {
        try {
            JSONObject result = JSONObject.parseObject(message).getJSONObject("request");
            JSONObject entryOrder = result.getJSONObject("entryOrder");
            log.info(this.getClass().getName() + " 查询入库通知单入参", entryOrder.getString("entryOrderCode"));
            ValueHolderV14<SgBPhyInNoticesResult> holder =
                    sgPhyInNoticesSelectService.selectSgBPhyInNoticesByBillNo(entryOrder.getString("entryOrderCode"));
            if (SgResultCode.isFail(holder)) {
                log.error(this.getClass().getName() + " 查询入库通知单失败 " + JSONObject.toJSONString(holder));
                return false;
            }
            SgBPhyInNoticesResult data = holder.getData();
            SgPhyInResultBillSaveRequest sgPhyInResultBillSaveRequest =
                    parseEntryOrderMessage2SgPhyInResultBillSaveRequest(data, result);
            SgPhyInResultSaveAndAuditService saveAndAuditService = ApplicationContextHandle.getBean(SgPhyInResultSaveAndAuditService.class);
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + " 新增并审核入库通知结果单入参 " + JSONObject.toJSONString(sgPhyInResultBillSaveRequest));
            }
            ValueHolderV14 inResultHolder = saveAndAuditService.saveAndAuditBillWithTrans(Lists.newArrayList(sgPhyInResultBillSaveRequest));
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + " 新增并审核入库通知结果单结果 " + JSONObject.toJSONString(inResultHolder));
            }
            if (!inResultHolder.isOK()) {
                AssertUtils.logAndThrow(inResultHolder.getMessage());
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(this.getClass().getName() + " 新增并审核入库通知结果单异常", e);
            return false;
        }
        return true;
    }

    /**
     * 将入库通知单回传信息结合通知单封装新增并审核入库结果单
     *
     * @param sgBPhyInNoticesResult
     * @param result
     * @return
     */
    private SgPhyInResultBillSaveRequest parseEntryOrderMessage2SgPhyInResultBillSaveRequest(SgBPhyInNoticesResult sgBPhyInNoticesResult,
                                                                                             JSONObject result) {
        SgPhyInResultBillSaveRequest sgPhyInResultBillSaveRequest = new SgPhyInResultBillSaveRequest();
        JSONObject entryOrder = result.getJSONObject("entryOrder");
        //主表
        SgBPhyInNotices notices = sgBPhyInNoticesResult.getNotices();
        SgBPhyInResultSaveRequest sgBPhyInResultSaveRequest = parseNoticeToPhyInResult(notices, entryOrder);
        if (null == sgBPhyInResultSaveRequest) {
            return null;
        }
        sgPhyInResultBillSaveRequest.setSgBPhyInResultSaveRequest(sgBPhyInResultSaveRequest);
        //从表
        List<SgBPhyInNoticesItem> itemList = sgBPhyInNoticesResult.getItemList();
        JSONArray orderLines = result.getJSONArray("orderLines");
        JSONArray defectItemOrderLines = new JSONArray();
        for (int i = 0; i < orderLines.size(); i++) {
            String inventoryType = ((JSONObject) orderLines.get(i)).getJSONObject("orderLine").getString("inventoryType");
            if (SgConstantsIF.TRADE_MARK_CC.equalsIgnoreCase(inventoryType)) {
                JSONObject orderLine = ((JSONObject) orderLines.get(i));
                defectItemOrderLines.add(orderLine);
            }
        }
        List<SgBPhyInResultDefectItemSaveRequest> sgBPhyInResultDefectItemSaveRequestList =
                parseNoticeDefectItemToPhyInResultItem(itemList, defectItemOrderLines);

        List<SgBPhyInResultItemSaveRequest> sgBPhyInResultItemSaveRequestList =
                parseNoticeItemToPhyInResultItem(itemList, orderLines);
        if (null == sgBPhyInResultItemSaveRequestList) {
            return null;
        }
        sgPhyInResultBillSaveRequest.setItemList(sgBPhyInResultItemSaveRequestList);
        sgPhyInResultBillSaveRequest.setDefectItemList(sgBPhyInResultDefectItemSaveRequestList);

        //设置登录人员
        sgPhyInResultBillSaveRequest.setLoginUser(WMSUserFactory.initWmsUser(notices.getAdOrgId().intValue(), notices.getAdClientId().intValue()));
        return sgPhyInResultBillSaveRequest;
    }

    private List<SgBPhyInResultDefectItemSaveRequest> parseNoticeDefectItemToPhyInResultItem(List<SgBPhyInNoticesItem> itemList, JSONArray defectItemOrderLines) {
        List<SgBPhyInResultDefectItemSaveRequest> sgBPhyInResultDefectItemSaveRequestList = Lists.newArrayList();
        for (int i = 0; i < defectItemOrderLines.size(); i++) {
            BigDecimal actualQty = ((JSONObject) defectItemOrderLines.get(i)).getJSONObject("orderLine").getBigDecimal("actualQty");
            String itemCode = ((JSONObject) defectItemOrderLines.get(i)).getJSONObject("orderLine").getString("itemCode");
            List<SgBPhyInNoticesItem> sgBPhyInNoticesItems =
                    itemList.stream().filter(s -> s.getPsCSkuEcode().equals(itemCode)).collect(Collectors.toList());
            if (CollectionUtils.isEmpty(sgBPhyInNoticesItems)) {
                log.error(this.getClass().getName() + " 入库通知单不包含sku[" + itemCode + "]" + JSONObject.toJSONString(itemList));
                return null;
            }
            SgBPhyInResultDefectItemSaveRequest sgBPhyInResultDefectItemSaveRequest = new SgBPhyInResultDefectItemSaveRequest();
            SgBPhyInNoticesItem sgBPhyInNoticesItem = sgBPhyInNoticesItems.get(0);
            //关联入库通知单明细ID
            //sg_b_phy_in_notices_item_id
            sgBPhyInResultDefectItemSaveRequest.setSgBPhyInNoticesItemId(sgBPhyInNoticesItem.getId());
            //条码ID ps_c_sku_id
            sgBPhyInResultDefectItemSaveRequest.setPsCSkuId(sgBPhyInNoticesItem.getPsCSkuId());
            //条码编码 ps_c_sku_ecode
            sgBPhyInResultDefectItemSaveRequest.setPsCSkuEcode(sgBPhyInNoticesItem.getPsCSkuEcode());
            //商品id ps_c_pro_id
            sgBPhyInResultDefectItemSaveRequest.setPsCProId(sgBPhyInNoticesItem.getPsCProId());
            //商品编码 ps_c_pro_ecode
            sgBPhyInResultDefectItemSaveRequest.setPsCProEcode(sgBPhyInNoticesItem.getPsCProEcode());
            //商品名称 ps_c_pro_ename
            sgBPhyInResultDefectItemSaveRequest.setPsCProEname(sgBPhyInNoticesItem.getPsCProEname());
            //规格1id ps_c_spec1_id
            sgBPhyInResultDefectItemSaveRequest.setPsCSpec1Id(sgBPhyInNoticesItem.getPsCSpec1Id());
            //规格1编码 ps_c_spec1_ecode
            sgBPhyInResultDefectItemSaveRequest.setPsCSpec1Ecode(sgBPhyInNoticesItem.getPsCSpec1Ecode());
            //规格1名称 ps_c_spec1_ename
            sgBPhyInResultDefectItemSaveRequest.setPsCSpec1Ename(sgBPhyInNoticesItem.getPsCSpec1Ename());
            //规格2id ps_c_spec2_id
            sgBPhyInResultDefectItemSaveRequest.setPsCSpec2Id(sgBPhyInNoticesItem.getPsCSpec2Id());
            //规格2编码 ps_c_spec2_ecode
            sgBPhyInResultDefectItemSaveRequest.setPsCSpec2Ecode(sgBPhyInNoticesItem.getPsCSpec2Ecode());
            //规格2名称 ps_c_spec2_ename
            sgBPhyInResultDefectItemSaveRequest.setPsCSpec2Ename(sgBPhyInNoticesItem.getPsCSpec2Ename());
            //入库数量actualQty
            sgBPhyInResultDefectItemSaveRequest.setQtyIn(actualQty);
            //吊牌价 price_list
            sgBPhyInResultDefectItemSaveRequest.setPriceList(sgBPhyInNoticesItem.getPriceList());
            //国标码
            sgBPhyInResultDefectItemSaveRequest.setGbcode(sgBPhyInNoticesItem.getGbcode());
            sgBPhyInResultDefectItemSaveRequestList.add(sgBPhyInResultDefectItemSaveRequest);
        }
        List<String> getPsCSkuEcodeList = sgBPhyInResultDefectItemSaveRequestList.stream().map(SgBPhyInResultDefectItemSaveRequest::getPsCSkuEcode).distinct().collect(Collectors.toList());
        Map<String, List<SgBPhyInResultDefectItemSaveRequest>> resultMap = sgBPhyInResultDefectItemSaveRequestList.stream().collect(Collectors.groupingBy(SgBPhyInResultDefectItemSaveRequest::getPsCSkuEcode));


        List<SgBPhyInResultDefectItemSaveRequest> sgBPhyInResultDefectItemSaveRequestReturn = Lists.newArrayList();
        for (String getPsCSkuEcode : getPsCSkuEcodeList) {
            List<SgBPhyInResultDefectItemSaveRequest> sgBPhyInResultDefectItemSaveRequests = resultMap.get(getPsCSkuEcode);
            if (CollectionUtils.isNotEmpty(sgBPhyInResultDefectItemSaveRequests)) {
                BigDecimal allQty = sgBPhyInResultDefectItemSaveRequests.stream()
                        .map(SgBPhyInResultDefectItemSaveRequest::getQtyIn).reduce(BigDecimal::add).orElse(BigDecimal.ZERO);
                SgBPhyInResultDefectItemSaveRequest sgBPhyInResultDefectItemSaveRequest = sgBPhyInResultDefectItemSaveRequests.get(0);
                sgBPhyInResultDefectItemSaveRequest.setQtyIn(allQty);
                sgBPhyInResultDefectItemSaveRequestReturn.add(sgBPhyInResultDefectItemSaveRequest);
            }
        }
        return sgBPhyInResultDefectItemSaveRequestReturn;
    }

    /**
     * 通知单从表+wms消息转化为入库结果单从表
     *
     * @param itemList
     * @param orderLines
     * @return
     */
    private List<SgBPhyInResultItemSaveRequest> parseNoticeItemToPhyInResultItem(List<SgBPhyInNoticesItem> itemList, JSONArray orderLines) {
        List<SgBPhyInResultItemSaveRequest> sgBPhyInResultItemSaveRequestList = Lists.newArrayList();
        for (int i = 0; i < orderLines.size(); i++) {
            BigDecimal actualQty = ((JSONObject) orderLines.get(i)).getJSONObject("orderLine").getBigDecimal("actualQty");
            String itemCode = ((JSONObject) orderLines.get(i)).getJSONObject("orderLine").getString("itemCode");
            List<SgBPhyInNoticesItem> sgBPhyInNoticesItems =
                    itemList.stream().filter(s -> s.getPsCSkuEcode().equals(itemCode)).collect(Collectors.toList());
            if (CollectionUtils.isEmpty(sgBPhyInNoticesItems)) {
                log.error(this.getClass().getName() + " 入库通知单不包含sku[" + itemCode + "]" + JSONObject.toJSONString(itemList));
                return null;
            }
            SgBPhyInResultItemSaveRequest sgBPhyInResultItemSaveRequest = new SgBPhyInResultItemSaveRequest();
            SgBPhyInNoticesItem sgBPhyInNoticesItem = sgBPhyInNoticesItems.get(0);
            //关联入库通知单明细ID
            //sg_b_phy_in_notices_item_id
            sgBPhyInResultItemSaveRequest.setSgBPhyInNoticesItemId(sgBPhyInNoticesItem.getId());
            //条码ID ps_c_sku_id
            sgBPhyInResultItemSaveRequest.setPsCSkuId(sgBPhyInNoticesItem.getPsCSkuId());
            //条码编码 ps_c_sku_ecode
            sgBPhyInResultItemSaveRequest.setPsCSkuEcode(sgBPhyInNoticesItem.getPsCSkuEcode());
            //商品id ps_c_pro_id
            sgBPhyInResultItemSaveRequest.setPsCProId(sgBPhyInNoticesItem.getPsCProId());
            //商品编码 ps_c_pro_ecode
            sgBPhyInResultItemSaveRequest.setPsCProEcode(sgBPhyInNoticesItem.getPsCProEcode());
            //商品名称 ps_c_pro_ename
            sgBPhyInResultItemSaveRequest.setPsCProEname(sgBPhyInNoticesItem.getPsCProEname());
            //规格1id ps_c_spec1_id
            sgBPhyInResultItemSaveRequest.setPsCSpec1Id(sgBPhyInNoticesItem.getPsCSpec1Id());
            //规格1编码 ps_c_spec1_ecode
            sgBPhyInResultItemSaveRequest.setPsCSpec1Ecode(sgBPhyInNoticesItem.getPsCSpec1Ecode());
            //规格1名称 ps_c_spec1_ename
            sgBPhyInResultItemSaveRequest.setPsCSpec1Ename(sgBPhyInNoticesItem.getPsCSpec1Ename());
            //规格2id ps_c_spec2_id
            sgBPhyInResultItemSaveRequest.setPsCSpec2Id(sgBPhyInNoticesItem.getPsCSpec2Id());
            //规格2编码 ps_c_spec2_ecode
            sgBPhyInResultItemSaveRequest.setPsCSpec2Ecode(sgBPhyInNoticesItem.getPsCSpec2Ecode());
            //规格2名称 ps_c_spec2_ename
            sgBPhyInResultItemSaveRequest.setPsCSpec2Ename(sgBPhyInNoticesItem.getPsCSpec2Ename());
            //入库数量actualQty
            sgBPhyInResultItemSaveRequest.setQtyIn(actualQty);
            //吊牌价 price_list
            sgBPhyInResultItemSaveRequest.setPriceList(sgBPhyInNoticesItem.getPriceList());
            //国标码
            sgBPhyInResultItemSaveRequest.setGbcode(sgBPhyInNoticesItem.getGbcode());
            sgBPhyInResultItemSaveRequestList.add(sgBPhyInResultItemSaveRequest);
        }
        return sgBPhyInResultItemSaveRequestList;
    }

    /**
     * 通知单主表+wms消息 转换成入库结果单主表
     *
     * @param notices
     * @param entryOrder
     * @return
     */
    private SgBPhyInResultSaveRequest parseNoticeToPhyInResult(SgBPhyInNotices notices, JSONObject entryOrder) {
        SgBPhyInResultSaveRequest sgBPhyInResultSaveRequest = new SgBPhyInResultSaveRequest();
        //WMS单据编号
        sgBPhyInResultSaveRequest.setWmsBillNo(entryOrder.getString("entryOrderId"));
        //备注
        sgBPhyInResultSaveRequest.setRemark(entryOrder.getString("remark"));
        //入库通知单编号
        sgBPhyInResultSaveRequest.setSgBPhyInNoticesBillno(entryOrder.getString("entryOrderCode"));
        //入库时间
        Date operateTime = entryOrder.getDate("operateTime");
        sgBPhyInResultSaveRequest.setInTime(operateTime == null ? new Date() : operateTime);
        //实体仓CODE
        sgBPhyInResultSaveRequest.setCpCPhyWarehouseEcode(notices.getCpCPhyWarehouseEcode());
        //实体仓名称
        sgBPhyInResultSaveRequest.setCpCPhyWarehouseEname(notices.getCpCPhyWarehouseEname());
        //实体ID
        sgBPhyInResultSaveRequest.setCpCPhyWarehouseId(notices.getCpCPhyWarehouseId());
        //货主编号
        sgBPhyInResultSaveRequest.setGoodsOwner(notices.getGoodsOwner());
        //入库通知单ID
        sgBPhyInResultSaveRequest.setSgBPhyInNoticesId(notices.getId());
        //收货方名称
        sgBPhyInResultSaveRequest.setCpCCsEname(notices.getCpCCsEname());
        //收货方code
        sgBPhyInResultSaveRequest.setCpCCsEcode(notices.getCpCCsEcode());
        //经销商实体仓ID
        sgBPhyInResultSaveRequest.setCpCCustomerWarehouseId(notices.getCpCCustomerWarehouseId());
        //供应商ID
        sgBPhyInResultSaveRequest.setCpCSupplierId(notices.getCpCSupplierId());
        //入库类型
        sgBPhyInResultSaveRequest.setInType(notices.getInType());
        //来源单据类型
        sgBPhyInResultSaveRequest.setSourceBillType(notices.getSourceBillType());
        //来源单据ID
        sgBPhyInResultSaveRequest.setSourceBillId(notices.getSourceBillId());
        //来源单据编号
        sgBPhyInResultSaveRequest.setSourceBillNo(notices.getSourceBillNo());
        //发货人
        sgBPhyInResultSaveRequest.setSendName(notices.getSendName());
        //发货人手机
        sgBPhyInResultSaveRequest.setSendMobile(notices.getSendMobile());
        //发货人电话
        sgBPhyInResultSaveRequest.setSendPhone(notices.getSendPhone());
        //发货人省ID
        sgBPhyInResultSaveRequest.setCpCRegionProvinceId(notices.getCpCRegionProvinceId());
        //省编码
        sgBPhyInResultSaveRequest.setCpCRegionProvinceEcode(notices.getCpCRegionProvinceEcode());
        //省名称
        sgBPhyInResultSaveRequest.setCpCRegionProvinceEname(notices.getCpCRegionProvinceEname());
        //市ID
        sgBPhyInResultSaveRequest.setCpCRegionCityId(notices.getCpCRegionCityId());
        //市编码
        sgBPhyInResultSaveRequest.setCpCRegionCityEcode(notices.getCpCRegionCityEcode());
        //市名称
        sgBPhyInResultSaveRequest.setCpCRegionCityEname(notices.getCpCRegionCityEname());
        //区ID
        sgBPhyInResultSaveRequest.setCpCRegionAreaId(notices.getCpCRegionAreaId());
        //区编码
        sgBPhyInResultSaveRequest.setCpCRegionAreaEcode(notices.getCpCRegionAreaEcode());
        //区名称
        sgBPhyInResultSaveRequest.setCpCRegionAreaEname(notices.getCpCRegionAreaEname());
        //发货人详细地址
        sgBPhyInResultSaveRequest.setSendAddress(notices.getSendAddress());
        //发货人邮编
        sgBPhyInResultSaveRequest.setSendZip(notices.getSendZip());
        //物流公司ID
        sgBPhyInResultSaveRequest.setCpCLogisticsId(notices.getCpCLogisticsId());
        //物流公司Code
        sgBPhyInResultSaveRequest.setCpCLogisticsEcode(notices.getCpCLogisticsEcode());
        //物流公司名称
        sgBPhyInResultSaveRequest.setCpCLogisticsEname(notices.getCpCLogisticsEname());
        //物流单号
        sgBPhyInResultSaveRequest.setLogisticNumber(notices.getLogisticNumber());
        //新增确认类型
        sgBPhyInResultSaveRequest.setIsLast(entryOrder.getInteger("confirmType"));
        return sgBPhyInResultSaveRequest;
    }
}
