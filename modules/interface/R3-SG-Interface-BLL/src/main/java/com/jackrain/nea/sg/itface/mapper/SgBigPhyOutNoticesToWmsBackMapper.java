package com.jackrain.nea.sg.itface.mapper;

import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutNotices;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * @author zhu lin yu
 * @since 2019/5/7
 * create at : 2019/5/7 13:32
 */
@Mapper
public interface SgBigPhyOutNoticesToWmsBackMapper extends ExtentionMapper<SgBPhyOutNotices> {

    @Select("SELECT BILL_STATUS FROM SG_B_PHY_OUT_NOTICES WHERE BILL_NO = #{billNo}")
    int queryBillStatusByBillNo(@Param("billNo") String billNo);
}
