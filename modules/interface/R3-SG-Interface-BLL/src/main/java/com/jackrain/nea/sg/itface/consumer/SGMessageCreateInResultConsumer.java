package com.jackrain.nea.sg.itface.consumer;

import com.alibaba.fastjson.JSONObject;
import com.aliyun.openservices.ons.api.Action;
import com.aliyun.openservices.ons.api.Message;
import com.aliyun.openservices.ons.api.impl.util.MsgConvertUtil;
import com.jackrain.nea.sg.in.model.result.ReturnSgPhyInResult;
import com.jackrain.nea.sg.in.services.SgPhyInNoticesSendMQService;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
public class SGMessageCreateInResultConsumer {

    @Autowired
    private SgPhyInNoticesSendMQService sgPhyInNoticesSendMQService;

    public Action consume(Message message) {
        String messageTopic = message.getTopic();
        String messageKey = message.getKey();
        String messageTag = message.getTag();
        try {
            String messageBody = MsgConvertUtil.objectDeserialize(message.getBody()).toString();
            List<Long> billIdList = JSONObject.parseArray(messageBody,Long.class);
            ValueHolderV14 result = sgPhyInNoticesSendMQService.saveSgBPhyInResult(billIdList);
            List<ReturnSgPhyInResult> list= (List<ReturnSgPhyInResult>) result.getData();
            //更新入库通知单状态，告诉那些消费成功(更新消费成功的状态)
            list.forEach(returnResult->{
                sgPhyInNoticesSendMQService.updateNoticesByMq(returnResult);
            });
            return Action.CommitMessage;
        } catch (Exception e) {
            e.printStackTrace();
            log.error(this.getClass().getName() + "Error, Resume MQ. ResultMessage={};Topic={};Body={};" +
                    "Key={};Tag={}", new Object[]{messageTopic, messageKey,
                    messageTag});
            return Action.ReconsumeLater;
        }
    }
}
