package com.jackrain.nea.sg.itface.consumer;

import com.aliyun.openservices.ons.api.Action;
import com.aliyun.openservices.ons.api.Message;
import com.aliyun.openservices.ons.api.impl.util.MsgConvertUtil;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.oms.common.OmsResultCode;
import com.jackrain.nea.sg.oms.model.request.SgChannelStorageOmsBufferRequest;
import com.jackrain.nea.sg.oms.model.tableExtend.SgBChannelStorageBufferExtend;
import com.jackrain.nea.sg.oms.services.SgChannelStorageOmsBufferService;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author caopengflying
 * @time 2019/5/6 17:39
 * @descript 渠道库存计算池消费MQ
 */
@Slf4j
@Component
public class SgBChannelStorageBufferConsumer {
    @Autowired
    private SgChannelStorageOmsBufferService sgChannelStorageOmsBufferService;

    public Action consume(Message message) {
        String messageTopic = message.getTopic();
        String messageKey = message.getKey();
        String messageTag = message.getTag();
        try {
            String messageBody = MsgConvertUtil.objectDeserialize(message.getBody()).toString();
            log.info(this.getClass().getName() + " 渠道库存计算池接收MQ消息 messageBody{}," +
                            "messageTopic{}," +
                            "messageKey," +
                            "messageTag{}",
                    new Object[]{messageBody, messageTopic, messageKey, messageTag});
            Action var19 = Action.CommitMessage;
            List<SgChannelStorageOmsBufferRequest> sgCSOBufferRequestList = null;
            try {
                sgCSOBufferRequestList = SgBChannelStorageBufferExtend.parseMessageToSgChannelStorageOmsBufferRequest(messageBody);
            } catch (Exception e) {
                e.printStackTrace();
                log.error(this.getClass().getName() + " 解析MQ消息失败" +
                        "ResultMessage={};" +
                        "Topic={};" +
                        "Body={};" +
                        "Key={};Tag={}", new Object[]{e, messageTopic, messageKey, messageBody, messageTag});
                return var19;
            }
            ValueHolderV14<Integer> channelStorageOmsBuffer = sgChannelStorageOmsBufferService.createChannelStorageOmsBuffer(sgCSOBufferRequestList,messageTag);
            //如果插入失败则MQ重新消费
            if (OmsResultCode.isFail(channelStorageOmsBuffer)) {
                log.error(this.getClass().getName() + " 新增库存计算池失败" +
                        "ResultMessage={};" +
                        "Topic={};" +
                        "Body={};" +
                        "Key={};Tag={}", new Object[]{channelStorageOmsBuffer.getMessage(), messageTopic, messageKey,
                        messageBody, messageTag});
                var19 = Action.ReconsumeLater;
            }
            return var19;
        } catch (Exception var16) {
            var16.printStackTrace();
            log.error(this.getClass().getName() + " 新增去渠道库存计算池失败", var16);
            throw new NDSException(var16);
        }
    }
}
