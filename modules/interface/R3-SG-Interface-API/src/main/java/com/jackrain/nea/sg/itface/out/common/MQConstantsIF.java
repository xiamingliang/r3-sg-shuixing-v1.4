package com.jackrain.nea.sg.itface.out.common;

import com.google.common.collect.Lists;

import java.util.List;

/**
 * @author caopengflying
 * @time 2019/5/7 13:46
 */
public interface MQConstantsIF {
    /*------------------------method静态常量维护----------------*/
    //库存盘点结果WMS回传服务 - 回传
    String WMS_BACK_INVENTORY_METHOD = "inventory.report";
    //(云枢纽回传库存中心)零售发货单 - 回传
    String WMS_BACK_DELIVERY_ORDER_METHOD = "deliveryorder.confirm";
    //大货入库WMS回传服务 - 回传
    String WMS_BACK_ENTRY_ORDER_METHOD = "entryorder.confirm";
    //库存调整结果WMS回传服务 - 回传
    String WMS_BACK_STOCK_CHANGE_METHOD = "stockchange.report";
    //大货出库WMS回传服务 - 回传
    String WMS_BACK_STOCK_OUT_METHOD = "stockout.confirm";
    //新增出库通知单并传WMS服务 - 回执
    String WMS_CALL_DELIVERY_ORDER_METHOD = "taobao.qimen.deliveryorder.create";
    //大货入库通知单传WMS - 回执
    String WMS_CALL_ENTRY_ORDER_METHOD = "taobao.qimen.entryorder.create";
    //大货出库通知单传WMS - 回执
    String WMS_CALL_STOCK_OUT_METHOD = "taobao.qimen.stockout.create";
    //库存差异调整传WMS - 回执
    String WMS_CALL_DIFF_ADJUST_METHOD="taobao.qimen.entryorder.create.special";


    /*------------------------tag静态常量维护----------------*/
    //渠道库存计算池消费MQ TAG
    List<String> SGB_CHANNEL_STORAGE_BUFFER_CONSUMER_TAGS = Lists.newArrayList("storage_to_channel","group_storage_to_channel");


    /**
     * 调拨单出库 mq tag
     */
    String TAG_OUT_TRANSFER_VERIFY_BACK = "sg_to_sg_transfer_out_verify_postback";


    /**
     * 调拨单入库 mq tag
     */
    String TAG_IN_TRANSFER_VERIFY_BACK = "sg_to_sg_transfer_in_verify_postback";

    //入库通知单生成入库结果单MQ TAG
    String SGB_CHANNEL_SG_IN_CONSUMER_TAGS = "IN_NOTICES_TO_IN_RESULT";


    /**
     * 出库通知单 调用 电子面单服务 mq
     */
    String SGB_CHANNEL_SG_OUT_NOTICES_FACE_TAGS = "sg_to_out_notices_face";

    /**
     * 新增出库通知单并传WMS mq
     */
    String OMS_TO_SG_OUT_NOTICES_TO_WMS = "oms_to_sg_out_notices_to_wms";

    String DEFAULT_NAME = "千百度";
    String DEFAULT_MOBILE = "862584791598";
    String DEFAULT_TEL = "862584791598";
    String DEFAULT_PROVINCE = "江苏省";
    String DEFAULT_CITY = "南京市";
    String DEFAULT_AREA = "秦淮区";
    String DEFAULT_ZIP_CODE = "361000";
    String DEFAULT_DETAIL_ADDRESS = "江苏省南京市秦淮区新街口汉中路1号南京国际贸易中心31层";
}
