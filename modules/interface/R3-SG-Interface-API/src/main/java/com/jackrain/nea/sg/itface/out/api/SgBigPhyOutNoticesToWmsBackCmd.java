package com.jackrain.nea.sg.itface.out.api;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.out.model.request.SgPhyOutNoticesBillWMSBackRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;

import java.util.List;

/**
 * @author zhu lin yu
 * @since 2019/5/7
 * create at : 2019/5/7 10:22
 */
public interface SgBigPhyOutNoticesToWmsBackCmd {
    ValueHolderV14 bigPhyOutNoticeToWmsBack(List<SgPhyOutNoticesBillWMSBackRequest> requests, User user) throws NDSException;
}
