package com.jackrain.nea.sg.itface.out.common;


public interface SgInterfaceConstants {

    /**
     * 转奇门订单类型
     */
   String QIMEN_ORDER_TYPE_B2BRK="B2BRK";

    String QIMEN_ORDER_TYPE_CGRK="CGRK";

    String QIMEN_ORDER_TYPE_DBRK="DBRK";


}
