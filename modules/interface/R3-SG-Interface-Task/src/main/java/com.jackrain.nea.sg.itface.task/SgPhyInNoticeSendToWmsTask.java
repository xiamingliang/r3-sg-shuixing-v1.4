package com.jackrain.nea.sg.itface.task;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.sg.basic.config.SgStorageBoxConfig;
import com.jackrain.nea.sg.itface.services.SgPhyInNoticeSendToWmsTaskService;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.task.IR3Task;
import com.jackrain.nea.task.RunTaskResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author jiang.cj
 * @since 2019-05-08
 * create at : 2019-05-08
 */
@Slf4j
@Component
public class SgPhyInNoticeSendToWmsTask implements IR3Task {

    @Autowired
    private SgPhyInNoticeSendToWmsTaskService sgPhyInNoticeSendToWmsTaskService;
    @Autowired
    private SgStorageBoxConfig storageBoxConfig;

    /**
     * 大货入库通知单传wms定时任务
     * @param params
     * @return
     */
    @Override
    public RunTaskResult execute(JSONObject params) {
        log.info("Start SgPhyInNoticeSendToWmsTask.execute,params=" + JSONObject.toJSONString(params) + ";");
        RunTaskResult runTaskResult=new RunTaskResult();
        ValueHolderV14 result=new ValueHolderV14();
        if(storageBoxConfig.getBoxEnable()) {
            result=sgPhyInNoticeSendToWmsTaskService.sgPhyInNoticeSendToWmsTaskBox(params);
        }
        else
        {
            result=sgPhyInNoticeSendToWmsTaskService.sgPhyInNoticeSendToWmsTask(params);
        }
        if(result.isOK()){
            runTaskResult.setSuccess(true);
            runTaskResult.setMessage(result.getMessage());
        }else {
            log.error(this.getClass().getName()+",error:"+result.getMessage());
            runTaskResult.setSuccess(false);
            runTaskResult.setMessage(result.getMessage());
        }
        return runTaskResult;
    }
}
