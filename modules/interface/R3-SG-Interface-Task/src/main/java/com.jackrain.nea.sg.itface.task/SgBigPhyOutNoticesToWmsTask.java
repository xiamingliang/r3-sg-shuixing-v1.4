package com.jackrain.nea.sg.itface.task;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.itface.services.SgBigPhyOutNoticesToWmsService;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.task.IR3Task;
import com.jackrain.nea.task.RunTaskResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author: zhu lin yu
 * @since: 2019/5/6
 * create at : 2019/5/6 15:09
 */
@Slf4j
@Component
public class SgBigPhyOutNoticesToWmsTask implements IR3Task {

    @Autowired
    private SgBigPhyOutNoticesToWmsService sgBigPhyOutNoticesToWmsService;

    @Override
    public RunTaskResult execute(JSONObject params) {
        System.out.println("=============SgBigPhyOutNoticesToWmsTask beginning=====================");
        RunTaskResult result = new RunTaskResult();
        ValueHolderV14 holderV14 =this.sgBigPhyOutNoticesToWmsService.timerQueryBigPhyOutNoticesTask();
        if (holderV14.getCode() == ResultCode.FAIL) {
            log.error(holderV14.getMessage());
            result.setSuccess(false);
            result.setMessage(holderV14.getMessage());
        } else {
            result.setSuccess(true);
        }
        return result;
    }

}
