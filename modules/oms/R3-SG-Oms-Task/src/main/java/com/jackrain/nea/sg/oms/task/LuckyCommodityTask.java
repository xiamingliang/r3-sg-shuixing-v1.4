package com.jackrain.nea.sg.oms.task;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.oms.services.CombinedCommodityService;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.task.IR3Task;
import com.jackrain.nea.task.RunTaskResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Author: chenchen
 * @Description:福袋商品最低库存计算
 * @Date: Create in 14:04 2019/4/11
 */
@Slf4j
@Component
public class LuckyCommodityTask implements IR3Task {


    @Autowired
    private CombinedCommodityService combinedCommodityService;


    @Override
    public RunTaskResult execute(JSONObject params) {
        RunTaskResult result = new RunTaskResult();
        ValueHolderV14 holderV14 = combinedCommodityService.updateCombinedCommodity(2);
        if (holderV14.getCode() == ResultCode.FAIL) {
            log.error(holderV14.getMessage());
            result.setSuccess(false);
            result.setMessage(holderV14.getMessage());
        } else {
            result.setSuccess(true);
        }
        return result;

    }
}
