package com.jackrain.nea.sg.oms.task;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.sg.oms.services.SgChannelStorageOmsCalcService;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.task.IR3Task;
import com.jackrain.nea.task.RunTaskResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Author: chenss
 * @Description:全渠道库存计算任务
 * @Date: Create in 14:04 2019/4/11
 */
@Slf4j
@Component
public class ChannelStorageCalcTask implements IR3Task {



    @Autowired
    private SgChannelStorageOmsCalcService calcService;



    @Override
    public RunTaskResult execute(JSONObject params) {
        log.info("=============ChannelStorageCalcTask beginning=====================");
        RunTaskResult result = new RunTaskResult();
        ValueHolderV14<Boolean> holder = calcService.executeTask();
        result.setSuccess(holder.getData());
        result.setMessage(holder.getMessage());
        return result;
    }
}
