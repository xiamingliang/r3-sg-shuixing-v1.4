package com.jackrain.nea.sg.oms.task;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.oms.services.SgChannelStorageOmsSynchService;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.task.IR3Task;
import com.jackrain.nea.task.RunTaskResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author zhu lin yu
 * @since 2019/7/27
 * create at : 2019/7/27 11:12
 */
@Slf4j
@Component
public class SNSyncChannelStorageAll implements IR3Task {
    @Autowired
    private SgChannelStorageOmsSynchService sgChannelStorageOmsSynchService;

    @Override
    public RunTaskResult execute(JSONObject params) {
        RunTaskResult result = new RunTaskResult();
        ValueHolderV14 holderV14 = sgChannelStorageOmsSynchService.snSyncChannelStorageAll();
        if (ResultCode.SUCCESS == holderV14.getCode()) {
            result.setSuccess(true);
        }else{
            result.setSuccess(false);
            result.setMessage(holderV14.getMessage());
        }
        return result;
    }
}
