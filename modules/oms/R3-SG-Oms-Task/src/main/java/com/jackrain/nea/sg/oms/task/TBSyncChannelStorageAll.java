package com.jackrain.nea.sg.oms.task;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.oms.model.result.SgStorageOmsSynchronousResult;
import com.jackrain.nea.sg.oms.services.SgChannelStorageOmsSynchService;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.task.IR3Task;
import com.jackrain.nea.task.RunTaskResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class TBSyncChannelStorageAll implements IR3Task {

    @Autowired
    private SgChannelStorageOmsSynchService sgChannelStorageOmsSynchService;

    @Override
    public RunTaskResult execute(JSONObject params) {
        RunTaskResult result = new RunTaskResult();
        ValueHolderV14<SgStorageOmsSynchronousResult> holderV14 = sgChannelStorageOmsSynchService.tbSyncChannelStorageAll();
        if (ResultCode.SUCCESS == holderV14.getCode()) {
            result.setSuccess(true);
        }else{
            result.setSuccess(false);
            result.setMessage(holderV14.getMessage());
        }
        return result;
    }
}
