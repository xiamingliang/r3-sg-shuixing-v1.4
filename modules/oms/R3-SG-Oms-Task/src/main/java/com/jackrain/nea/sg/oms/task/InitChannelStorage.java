package com.jackrain.nea.sg.oms.task;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.oms.model.result.SgStorageOmsSynchronousResult;
import com.jackrain.nea.sg.oms.services.SgChannelStorageOmsSynchService;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.task.IR3Task;
import com.jackrain.nea.task.RunTaskResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Slf4j
@Component
public class InitChannelStorage implements IR3Task {

    /**
     * 初始化渠道库存
     */
    @Autowired
    private SgChannelStorageOmsSynchService sgChannelStorageOmsSynchService;

    @Override
    public RunTaskResult execute(JSONObject params) {
        log.info(this.getClass().getName() + " 开始初始化渠道库存！");
        RunTaskResult result = new RunTaskResult();
        String platformIdsStr = params.getString("platformIds");
        List<String> platformIds = Arrays.asList(platformIdsStr.split(","));
        ValueHolderV14<SgStorageOmsSynchronousResult> holderV14 = sgChannelStorageOmsSynchService.initChannelStorage(platformIds);
        if (ResultCode.SUCCESS == holderV14.getCode()) {
            result.setSuccess(true);
        }else{
            result.setSuccess(false);
            result.setMessage(holderV14.getMessage());
        }
        return result;
    }
}
