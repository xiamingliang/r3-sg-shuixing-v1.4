package com.jackrain.nea.sg.oms.controller;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.sg.basic.model.request.SgStoreWithPrioritySearchItemRequest;
import com.jackrain.nea.sg.basic.model.request.StStockPriorityRequest;
import com.jackrain.nea.sg.basic.model.result.SgStoreWithPrioritySearchResult;
import com.jackrain.nea.sg.oms.api.SgStorageOmsQueryCmd;
import com.jackrain.nea.sg.oms.constants.OmsConstants;
import com.jackrain.nea.sg.oms.model.request.SgStoreWithPrioritySearchRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.face.impl.UserImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author: 易邵峰
 * @since: 2019-01-10
 * create at : 2019-01-10 14:13
 */
@RestController
@Slf4j
@Api(value = "SgStorageOmsQueryController", description = "全渠道库存PG查询接口")
public class SgStorageOmsQueryCtrl {

    @ApiOperation(value = "逻辑仓寻仓生成占用库存信息接口")
    @RequestMapping(path = "/p/cs/storage/searchSgStoreWithPriority", method = {RequestMethod.POST})
    public JSONObject searchSgStoreWithPriority(HttpServletRequest request) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgStorageOmsQueryCtrl.searchSgStoreWithPriority. ReceiveParams:");
        }

        //QuerySessionImpl querySession = new QuerySessionImpl(request);
        //User loginUser = querySession.getUser();
        User loginUser = getRootUser();

        SgStorageOmsQueryCmd sgStorageOmsQueryCmd = (SgStorageOmsQueryCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(),
                SgStorageOmsQueryCmd.class.getName(),
                OmsConstants.GROUP, OmsConstants.VERSION);

        SgStoreWithPrioritySearchRequest omsRequest = new SgStoreWithPrioritySearchRequest();
        List<SgStoreWithPrioritySearchItemRequest> itemList = new ArrayList<>();


        SgStoreWithPrioritySearchItemRequest itemRequest = new SgStoreWithPrioritySearchItemRequest();
        itemRequest.setQtyChange(new BigDecimal("8"));
        itemRequest.setPsCSkuId(421L);
        itemRequest.setSourceItemId(208L);
        List<StStockPriorityRequest> priorityList = new ArrayList<>();
        StStockPriorityRequest priority1 = new StStockPriorityRequest();
        priority1.setSupplyStoreId(55L);
        priority1.setSupplyStoreEcode("42ecode");
        priority1.setSupplyStoreEname("42ename");
        priority1.setPriority(1);
        StStockPriorityRequest priority2 = new StStockPriorityRequest();
        priority2.setSupplyStoreId(43L);
        priority2.setSupplyStoreEcode("43ecode");
        priority2.setSupplyStoreEname("43ename");
        priority2.setPriority(2);
//        StStockPriorityRequest priority3 = new StStockPriorityRequest();
//        priority3.setSupplyStoreId(43L);
//        priority3.setSupplyStoreEcode("43ecode");
//        priority3.setSupplyStoreEname("43ename");
//        priority3.setPriority(2);
//        StStockPriorityRequest priority4 = new StStockPriorityRequest();
//        priority4.setSupplyStoreId(44L);
//        priority4.setSupplyStoreEcode("44ecode");
//        priority4.setSupplyStoreEname("44ename");
//        priority4.setPriority(3);
        priorityList.add(priority1);
        priorityList.add(priority2);
//        priorityList.add(priority3);
//        priorityList.add(priority4);
        omsRequest.setPriorityList(priorityList);
        omsRequest.setLoginUser(loginUser);
        omsRequest.setBillNo("OM19052000000030");
        itemList.add(itemRequest);
        omsRequest.setItemList(itemList);

        ValueHolderV14<SgStoreWithPrioritySearchResult> callAPIResult = sgStorageOmsQueryCmd.searchSgStoreWithPriority(omsRequest);

        JSONObject result = JSONObject.parseObject(JSONObject.toJSONString(callAPIResult.toJSONObject(),
                SerializerFeature.WriteMapNullValue));

        if (log.isDebugEnabled()) {
            log.debug("Finish SgStorageOmsQueryCtrl.searchSgStoreWithPriority. ReturnResult=" + result);
        }

        return result;
    }

    //TODO NEED DELETE
    private User getRootUser() {
        UserImpl user = new UserImpl();
        user.setId(893);
        user.setName("Fire Blast");
        user.setEname("Pokemon-mapper");
        user.setActive(true);
        user.setClientId(37);
        user.setOrgId(27);
        user.setIsAdmin(2);
        user.setIsDev(2);
        return user;
    }

}
