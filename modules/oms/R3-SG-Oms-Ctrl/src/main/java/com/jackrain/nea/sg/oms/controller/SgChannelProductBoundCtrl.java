package com.jackrain.nea.sg.oms.controller;

import com.alibaba.fastjson.JSON;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.sg.oms.api.SgChannelProductBoundCmd;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.query.DefaultWebEvent;
import com.jackrain.nea.web.query.QuerySessionImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @author zhu lin yu
 * @since 2019-11-08
 * create at : 2019-11-08 18:04
 */
@RestController
@Slf4j
public class SgChannelProductBoundCtrl {

    @PostMapping(path = "/p/cs/sg/sgChannelProductBound")
    public ValueHolder saveOrUpdateSgPhyInResult(HttpServletRequest request, @RequestParam(value = "param", required = true) String param) throws Exception {

        QuerySessionImpl querySession = new QuerySessionImpl(request);
        DefaultWebEvent event = new DefaultWebEvent("saveOrUpdateSgPhyInResult", request, false);
        event.put("param", JSON.parseObject(param));
        querySession.setEvent(event);
        Object o = ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                SgChannelProductBoundCmd.class.getName(), "sg", "1.0");
        return ((SgChannelProductBoundCmd) o).execute(querySession);
    }
}

