package com.jackrain.nea.sg.oms.controller;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.sg.oms.api.SgChannelStorageOmsInitCmd;
import com.jackrain.nea.sg.oms.constants.OmsConstants;
import com.jackrain.nea.sg.oms.model.request.SgChannelStorageOmsManualSynchRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zhu lin yu
 * @since 2019/8/27
 * create at : 2019/8/27 14:02
 */
@RestController
@Slf4j
@Api(value = "SgChannelProductCalcAndSyncController", description = "全渠道商品计算并同步接口")
public class SgChannelProductCalcAndSyncCtrl {

    @ApiOperation(value = "手工计算渠道商品库存并同步")
    @RequestMapping(path = "/p/cs/storage/manualCalcAndSynchChannelProduct", method = {RequestMethod.POST})
    public JSONObject manualCalcAndSyncChannelProduct(@RequestBody SgChannelStorageOmsManualSynchRequest request) {
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + " 手工计算渠道商品库存并同步Ctrl入参 " + JSONObject.toJSONString(request));
        }
        SgChannelStorageOmsInitCmd sgChannelStorageOmsInitCmd = (SgChannelStorageOmsInitCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(),
                SgChannelStorageOmsInitCmd.class.getName(),
                OmsConstants.GROUP, OmsConstants.VERSION);
        //设置操作方式为按查询条件同步
        request.setOperate(SgChannelStorageOmsManualSynchRequest.QUERY_BY_CONDITION);
        ValueHolderV14<Boolean> holderV14 = sgChannelStorageOmsInitCmd.manualCalcAndSyncChannelProduct(request);
        JSONObject result = JSONObject.parseObject(JSONObject.toJSONString(holderV14.toJSONObject(),
                SerializerFeature.WriteMapNullValue));
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + " 手工计算渠道商品库存并同步结果" + result.toJSONString());
        }
        return result;
    }
}
