package com.jackrain.nea.sg.oms.controller;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.sg.oms.api.SgChannelStorageOmsInitCmd;
import com.jackrain.nea.sg.oms.api.SgChannelStorageOmsSynchCmd;
import com.jackrain.nea.sg.oms.constants.OmsConstants;
import com.jackrain.nea.sg.oms.model.request.SgChannelStorageOmsManualSynchRequest;
import com.jackrain.nea.sg.oms.model.request.SgStorageOmsSynchronousRequest;
import com.jackrain.nea.sg.oms.model.result.SgStorageOmsSynchronousResult;
import com.jackrain.nea.sg.oms.model.table.SgBChannelStorage;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;

@RestController
@Slf4j
@Api(value = "SgChannelStorageSyncController", description = "全渠道库存同步接口")
public class SgChannelStorageSyncCtrl {


    @ApiOperation(value = "全渠道库存更新测试接口")
    @RequestMapping(path = "/p/cs/storage/SgChannelStorageOmsSynchCmd/synchronousStock", method = {RequestMethod.POST})
    public JSONObject synchronousStock(HttpServletRequest request) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgChannelStorageSyncCtrl.synchronousStock. ReceiveParams:");
        }

        SgChannelStorageOmsSynchCmd sgChannelStorageOmsSynchCmd = (SgChannelStorageOmsSynchCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(),
                SgChannelStorageOmsSynchCmd.class.getName(),
                OmsConstants.GROUP, OmsConstants.VERSION);
        SgStorageOmsSynchronousRequest syncRequest = new SgStorageOmsSynchronousRequest();
        syncRequest.setSyncType(1);
        System.out.println("================增量同步==============");
        SgBChannelStorage sg = new SgBChannelStorage();
        sg.setQty(BigDecimal.valueOf(1000));
        sg.setCpCShopId(2L);
        sg.setCpCShopId(20190424L);
        sg.setCpCShopTitle("天猫旗舰店");
        sg.setSkuId("测试增量同步");
        sg.setPsCSkuId(007L);
        sg.setCpCPlatformId(2);
        sg.setCpCShopSellerNick("aaaa");
        sg.setNumiid(123 + "");
        sg.setSkuId("213123a");
        sg.setPsCSkuEcode("123");
        sg.setProperties("12sddsfsdf");
        sg.setQty(BigDecimal.valueOf(100));
        syncRequest.setSgBChannelStorage(sg);
        ValueHolderV14<SgStorageOmsSynchronousResult> holderV14 = sgChannelStorageOmsSynchCmd.synchronousStock(syncRequest);
        JSONObject result = JSONObject.parseObject(JSONObject.toJSONString(holderV14.toJSONObject(),
                SerializerFeature.WriteMapNullValue));

        if (log.isDebugEnabled()) {
            log.debug("Finish SgChannelStorageSyncCtrl.synchronousStock. ReturnResult=" + result);
        }
        return result;
    }


    @ApiOperation(value = "=======全渠道库存全量更新测试接口=======")
    @RequestMapping(path = "/p/cs/storage/SgChannelStorageOmsSynchCmd/synchronousStockAll", method = {RequestMethod.POST})
    public JSONObject synchronousStockAll(HttpServletRequest request) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgChannelStorageSyncCtrl.synchronousStock. ReceiveParams:");
        }
        SgChannelStorageOmsSynchCmd sgChannelStorageOmsSynchCmd = (SgChannelStorageOmsSynchCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(),
                SgChannelStorageOmsSynchCmd.class.getName(),
                OmsConstants.GROUP, OmsConstants.VERSION);
        SgStorageOmsSynchronousRequest syncRequest = new SgStorageOmsSynchronousRequest();
        syncRequest.setSyncType(2);
        System.out.println("================全量同步==============");
        SgBChannelStorage sg = new SgBChannelStorage();
        sg.setCpCShopId(20190424L);
        syncRequest.setCpCShopId(20190424L);
        syncRequest.setSgBChannelStorage(sg);
        ValueHolderV14<SgStorageOmsSynchronousResult> holderV14 = sgChannelStorageOmsSynchCmd.synchronousStock(syncRequest);
        JSONObject result = JSONObject.parseObject(JSONObject.toJSONString(holderV14.toJSONObject(),
                SerializerFeature.WriteMapNullValue));

        if (log.isDebugEnabled()) {
            log.debug("Finish SgChannelStorageSyncCtrl.synchronousStock. ReturnResult=" + result);
        }
        return result;
    }

    @ApiOperation(value = "手工同步渠道库存")
    @RequestMapping(path = "/p/cs/storage/manualSynchChannelStorageByQuery", method = {RequestMethod.POST})
    public JSONObject manualSynchChannelStorageByQuery(@RequestBody SgChannelStorageOmsManualSynchRequest request) {
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + " 手动同步渠道库存Ctrl入参 " + JSONObject.toJSONString(request));
        }
        SgChannelStorageOmsInitCmd sgChannelStorageOmsInitCmd = (SgChannelStorageOmsInitCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(),
                SgChannelStorageOmsInitCmd.class.getName(),
                OmsConstants.GROUP, OmsConstants.VERSION);
        //设置操作方式为按查询条件同步
        request.setOperate(SgChannelStorageOmsManualSynchRequest.QUERY_BY_CONDITION);
        ValueHolderV14<Boolean> holderV14 = sgChannelStorageOmsInitCmd.manualSynchChannelStorage(request);
        JSONObject result = JSONObject.parseObject(JSONObject.toJSONString(holderV14.toJSONObject(),
                SerializerFeature.WriteMapNullValue));
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + " 手工同步渠道库存结果" + result.toJSONString());
        }
        return result;
    }

}
