package com.jackrain.nea.sg.oms.controller;

import com.jackrain.nea.sg.oms.api.InitChannelStorageCmd;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @Author: chenchen
 * @Description:
 * @Date: Create in 14:49 2019/4/15
 */
@RestController
@Slf4j
public class TbSyncChannelStorageCtrl {

    @Reference(version = "1.0", group = "sg")
    private InitChannelStorageCmd initChannelStorageCmd;

    @RequestMapping(path = "p/cs/b/initChannelStorage")
    public ValueHolderV14 initChannelStorage(HttpServletRequest request) {
        return initChannelStorageCmd.initChannelStorage();

    }

    @RequestMapping(path = "p/cs/b/tbSyncChannelStorage")
    public ValueHolderV14 tbSyncChannelStorage(HttpServletRequest request) {
        return initChannelStorageCmd.tbSyncChannelStorage();
    }

    @RequestMapping(path = "p/cs/b/jdSyncChannelStorage")
    public ValueHolderV14 jdSyncChannelStorage(HttpServletRequest request) {
        return initChannelStorageCmd.jdSyncChannelStorage();
    }

    @RequestMapping(path = "p/cs/b/compensationAll")
    public ValueHolderV14 compensationAll(HttpServletRequest request) {
        return initChannelStorageCmd.compensationAll();
    }
}
