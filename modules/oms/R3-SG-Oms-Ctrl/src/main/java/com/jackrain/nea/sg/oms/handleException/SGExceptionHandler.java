package com.jackrain.nea.sg.oms.handleException;

import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import org.springframework.core.annotation.Order;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author caopengflying
 * @time 2019/5/14 0:24
 */
@RestControllerAdvice
@Order(999)
public class SGExceptionHandler {
    @ExceptionHandler(Exception.class)
    public ValueHolderV14 handleException(Exception e){
        ValueHolderV14 holder = new ValueHolderV14();
        holder.setCode(ResultCode.FAIL);
        holder.setMessage(e.getMessage());
        return holder;
    }
}
