package com.jackrain.nea.sg.oms.services;

import com.jackrain.nea.sg.oms.api.InitChannelStorageCmd;
import com.jackrain.nea.sg.out.common.SgOutConstantsIF;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class TbSyncChannelStorageCmdImpl implements InitChannelStorageCmd {

    @Autowired
    private SgChannelStorageOmsSynchService sgChannelStorageOmsSynchService;


    @Override
    public ValueHolderV14 initChannelStorage() {
        ArrayList<String> strings = new ArrayList<>();
        strings.add(SgOutConstantsIF.PLATFORMTYPE_TAOBAO);
        return sgChannelStorageOmsSynchService.initChannelStorage(strings);
    }

    @Override
    public ValueHolderV14 tbSyncChannelStorage() {
        return sgChannelStorageOmsSynchService.tbSyncChannelStorageAll();
    }

    @Override
    public ValueHolderV14 jdSyncChannelStorage() {
        return sgChannelStorageOmsSynchService.jdSyncChannelStorageAll();
    }

    @Override
    public ValueHolderV14 compensationAll() {
        return sgChannelStorageOmsSynchService.compensationAll();
    }
}
