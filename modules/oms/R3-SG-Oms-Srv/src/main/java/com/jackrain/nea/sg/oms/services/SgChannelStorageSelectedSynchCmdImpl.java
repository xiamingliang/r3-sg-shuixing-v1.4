package com.jackrain.nea.sg.oms.services;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.Feature;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.google.common.collect.Maps;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.oms.api.SgChannelStorageSelectedSynchCmd;
import com.jackrain.nea.sg.oms.common.OmsResultCode;
import com.jackrain.nea.sg.oms.model.request.SgChannelStorageOmsManualSynchRequest;
import com.jackrain.nea.sys.CommandAdapter;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.query.DefaultWebEvent;
import com.jackrain.nea.web.query.QuerySession;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author caopengflying
 * @time 2019/6/19 11:04
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgChannelStorageSelectedSynchCmdImpl extends CommandAdapter implements SgChannelStorageSelectedSynchCmd {
    @Autowired
    private SgChannelStorageOmsService sgChannelStorageOmsService;

    @Override
    public ValueHolder execute(QuerySession session) throws NDSException {
        ValueHolder holderV14 = new ValueHolder();
        DefaultWebEvent event = session.getEvent();
        JSONObject param = JSON.parseObject(JSON.toJSONStringWithDateFormat(event.getParameterValue("param"),
                "yyyy-MM-dd HH:mm:ss", SerializerFeature.WriteMapNullValue), Feature.OrderedField);
        SgChannelStorageOmsManualSynchRequest sgChannelStorageOmsManualSynchRequest =
                new SgChannelStorageOmsManualSynchRequest();
        if (CollectionUtils.isNotEmpty(param.getJSONArray("ids"))) {
            List<Integer> ids = param.getJSONArray("ids").stream().map(id -> {
                return Integer.parseInt(id.toString());
            }).collect(Collectors.toList());
            sgChannelStorageOmsManualSynchRequest.setIdList(ids);
        }
        ValueHolderV14 holder =
                sgChannelStorageOmsService.manualSynchChannelStorage(sgChannelStorageOmsManualSynchRequest);
        HashMap resultMap = Maps.newHashMap();
        resultMap.put(OmsResultCode.KEY_CODE, holder.getCode());
        resultMap.put(OmsResultCode.KEY_MESSAGE, holder.getMessage());
        if (OmsResultCode.isFail(holder)) {
            resultMap.put(OmsResultCode.KEY_DATA, false);
        } else {
            resultMap.put(OmsResultCode.KEY_DATA, true);
        }
        holderV14.setData(resultMap);
        return holderV14;
    }
}
