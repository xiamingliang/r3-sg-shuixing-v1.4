package com.jackrain.nea.sg.oms.services;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.oms.api.SgChannelStorgeManualSyncCallBackCmd;
import com.jackrain.nea.sg.oms.api.SgChannelStorgeOmsCallbackAllCmd;
import com.jackrain.nea.sg.oms.model.request.SgChannelStorgeOmsCallbackAllRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgChannelStorageManualSyncCallbackCmdImpl implements SgChannelStorgeManualSyncCallBackCmd {
    @Autowired
    SgChannelStorageManualSyncCallBackService sgChannelStorageManualSyncCallBackService;

    @Override
    public ValueHolderV14<Integer> callbackAllRPC(SgChannelStorgeOmsCallbackAllRequest request) {
        try{
            return sgChannelStorageManualSyncCallBackService.callbackAllRPC(request);
        }catch (Exception e){
            log.error(this.getClass().getName() +" 调用渠道库存手工导入同步RPC回执失败，入参为{}", JSONObject.toJSONString(request),e);
            return new ValueHolderV14<>(ResultCode.SUCCESS,"调用渠道库存手工导入同步RPC回执失败!");
        }
    }
}
