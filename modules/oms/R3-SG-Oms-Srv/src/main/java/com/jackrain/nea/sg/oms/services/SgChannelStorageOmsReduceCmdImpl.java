package com.jackrain.nea.sg.oms.services;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.oms.api.SgChannelStorageOmsReduceCmd;
import com.jackrain.nea.sg.oms.model.request.SgChannelStorageOmsReduceRequest;
import com.jackrain.nea.sg.oms.model.result.SgChannelStorageOmsReduceResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Description:
 * @Author: chenss
 * @Date: 2019/4/22 13:15
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgChannelStorageOmsReduceCmdImpl implements SgChannelStorageOmsReduceCmd {

    @Autowired
    private SgChannelStorageOmsReduceService service;

    @Override
    public ValueHolderV14<SgChannelStorageOmsReduceResult> reductChannelStorageOms(SgChannelStorageOmsReduceRequest request,String messageTag) throws NDSException {
        return service.reductChannelStorageOms(request,messageTag);
    }
}
