package com.jackrain.nea.sg.oms.services;

import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.oms.api.SgChannelStorgeOmsCallbackAllCmd;
import com.jackrain.nea.sg.oms.model.request.SgChannelStorgeOmsCallbackAllRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgChannelStorageOmsCallbackAllCmdImpl implements SgChannelStorgeOmsCallbackAllCmd {
    @Autowired
    SgChannelStorageOmsCallbackAllService sgChannelStorageOmsCallbackAllService;

    @Override
    public ValueHolderV14<Integer> callbackAllRPC(SgChannelStorgeOmsCallbackAllRequest request) {
        try{
            return sgChannelStorageOmsCallbackAllService.callbackAllRPC(request);
        }catch (Exception e){
            e.printStackTrace();
            return new ValueHolderV14<>(ResultCode.SUCCESS,"调用全量库存同步RPC失败!");
        }
    }
}
