package com.jackrain.nea.sg.oms.services;

import com.jackrain.nea.sg.oms.api.SgChannelStorageOmsInitCmd;
import com.jackrain.nea.sg.oms.model.request.SgChannelStorageOmsInitRequest;
import com.jackrain.nea.sg.oms.model.request.SgChannelStorageOmsManualSynchRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Description:
 * @Author: chenss
 * @Date: 2019/4/22 13:15
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgChannelStorageOmsInitCmdImpl implements SgChannelStorageOmsInitCmd {
    @Autowired
    private SgChannelStorageOmsInitService sgChannelStorageOmsInitService;
    @Autowired
    private SgChannelStorageOmsService sgChannelStorageOmsService;

    @Override
    public ValueHolderV14<Integer> initChannelStorage(SgChannelStorageOmsInitRequest request) {
        return sgChannelStorageOmsInitService.initChannelStorage(request);
    }

    @Override
    public ValueHolderV14<Boolean> manualSynchChannelStorage(SgChannelStorageOmsManualSynchRequest request) {
        return sgChannelStorageOmsService.manualSynchChannelStorage(request);
    }

    @Override
    public ValueHolderV14<Boolean> manualCalcAndSyncChannelProduct(SgChannelStorageOmsManualSynchRequest request) {
        return sgChannelStorageOmsService.manualCalcAndSyncChannelProduct(request);
    }


}
