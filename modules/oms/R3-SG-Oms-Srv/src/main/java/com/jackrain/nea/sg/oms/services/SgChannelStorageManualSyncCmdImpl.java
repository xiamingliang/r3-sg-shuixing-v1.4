package com.jackrain.nea.sg.oms.services;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.basic.utils.R3ParamUtil;
import com.jackrain.nea.sg.oms.api.SgChannelProductCalcAndSyncCmd;
import com.jackrain.nea.sg.oms.api.SgChannelStorageManualSyncCmd;
import com.jackrain.nea.sys.CommandAdapter;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.query.QuerySession;
import lombok.extern.slf4j.Slf4j;
import org.apache.catalina.core.ApplicationContext;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author zhu lin yu
 * @since 2019-10-16
 * create at : 2019-10-16 14:57
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgChannelStorageManualSyncCmdImpl extends CommandAdapter implements SgChannelStorageManualSyncCmd {
    @Autowired
    private SgChannelStorageManualSyncService sgChannelStorageManualSyncService;

    @Override
    public ValueHolder execute(QuerySession session) throws NDSException {
        User user = session.getUser();
        JSONObject param = R3ParamUtil.getParamJo(session);
        JSONArray ids = param.getJSONArray("ids");
        return this.sgChannelStorageManualSyncService.manualSync(ids, user);
    }
}
