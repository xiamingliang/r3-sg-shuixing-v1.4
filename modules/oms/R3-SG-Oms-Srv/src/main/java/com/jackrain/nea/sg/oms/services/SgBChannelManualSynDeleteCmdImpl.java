package com.jackrain.nea.sg.oms.services;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.oms.api.SgBChannelManualSynDeleteCmd;
import com.jackrain.nea.sys.CommandAdapter;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.query.QuerySession;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Author chenjinjun
 * @Description 手工导入店铺库存删除实现类
 * @Date  2019-10-21
**/
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgBChannelManualSynDeleteCmdImpl extends CommandAdapter implements SgBChannelManualSynDeleteCmd {
    @Autowired
    private SgBChannelManualSynDeleteService sgBChannelManualSynDeleteService;

    @Override
    public ValueHolder execute(QuerySession session) throws NDSException {
        return sgBChannelManualSynDeleteService.execute(session);
    }
}
