package com.jackrain.nea.sg.oms.services;

import com.jackrain.nea.sg.oms.api.SgChannelStoreChangeCmd;
import com.jackrain.nea.sg.oms.model.request.SgChannelStoreChangeRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgChannelStoreChangeCmdImpl implements SgChannelStoreChangeCmd {

    @Autowired
    SgChannelStoreChangeService sgChannelStoreChangeService;

    @Override
    public ValueHolderV14<Integer> saveSbGChannelStore(SgChannelStoreChangeRequest request) {
        return sgChannelStoreChangeService.saveSbGChannelStore(request);
    }

    @Override
    public ValueHolderV14<Integer> deleteSbGChannelStore(SgChannelStoreChangeRequest request) {
        return sgChannelStoreChangeService.deleteSbGChannelStore(request);
    }
    @Override
    public ValueHolderV14<Integer> changeCpCStoreTitleById(SgChannelStoreChangeRequest request) {
        return sgChannelStoreChangeService.changeCpCStoreTitleById(request);
    }

}
