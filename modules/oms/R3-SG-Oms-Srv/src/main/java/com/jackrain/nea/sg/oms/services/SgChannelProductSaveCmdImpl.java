package com.jackrain.nea.sg.oms.services;

import com.jackrain.nea.sg.oms.api.SgChannelProductSaveCmd;
import com.jackrain.nea.sg.oms.model.request.SgChannelProductSaveRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author zhu lin yu
 * @since 2019/5/14
 * create at : 2019/5/14 10:26
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgChannelProductSaveCmdImpl implements SgChannelProductSaveCmd {
    @Autowired
    private SgChannelProductSaveService sgChannelProductSaveService;

    @Override
    public ValueHolderV14 saveChannelProduct(SgChannelProductSaveRequest sgChannelProductSaveRequest) {
        return this.sgChannelProductSaveService.saveChannelProduct(sgChannelProductSaveRequest);
    }
}
