package com.jackrain.nea.sg.oms.services;

import com.jackrain.nea.sg.oms.api.SgChannelStorageQueryCmd;
import com.jackrain.nea.sg.oms.model.request.SgChannelStorageQueryRequest;
import com.jackrain.nea.sg.oms.model.table.SgBChannelStorage;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author zhu lin yu
 * @since 2019-11-11
 * create at : 2019-11-11 20:52
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgChannelStorageQueryCmdImpl implements SgChannelStorageQueryCmd {
    @Override
    public ValueHolderV14<List<SgBChannelStorage>> queryChannelStorage(List<SgChannelStorageQueryRequest> requestList) {
        SgChannelStorageQueryCmdService service= ApplicationContextHandle.getBean(SgChannelStorageQueryCmdService.class);
        return service.queryChannelStorage(requestList);
    }
}
