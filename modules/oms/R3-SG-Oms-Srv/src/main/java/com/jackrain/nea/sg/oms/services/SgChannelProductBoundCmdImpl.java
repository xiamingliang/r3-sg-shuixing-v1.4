package com.jackrain.nea.sg.oms.services;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.Feature;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.basic.utils.R3ParamUtil;
import com.jackrain.nea.sg.oms.api.SgChannelProductBoundCmd;
import com.jackrain.nea.sg.oms.model.request.SgChannelProductBoundRequest;
import com.jackrain.nea.sys.CommandAdapter;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.query.DefaultWebEvent;
import com.jackrain.nea.web.query.QuerySession;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

/**
 * @author zhu lin yu
 * @since 2019-11-08
 * create at : 2019-11-08 10:58
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgChannelProductBoundCmdImpl extends CommandAdapter implements SgChannelProductBoundCmd {

    @Override
    public ValueHolder execute(QuerySession session) throws NDSException {
        SgChannelProductBoundService service = ApplicationContextHandle.getBean(SgChannelProductBoundService.class);

        DefaultWebEvent event = session.getEvent();
        User user = session.getUser();

        JSONObject param = JSON.parseObject(JSON.toJSONStringWithDateFormat(event.getParameterValue("param"),
                "yyyy-MM-dd HH:mm:ss", SerializerFeature.WriteMapNullValue), Feature.OrderedField);
        SgChannelProductBoundRequest sgChannelProductBoundRequest = JSONObject.toJavaObject(param, SgChannelProductBoundRequest.class);
        sgChannelProductBoundRequest.setLoginUser(user);

        return R3ParamUtil.convertV14WithResult(service.channelProductBound(sgChannelProductBoundRequest));
    }
}
