package com.jackrain.nea.sg.oms.services;

import com.jackrain.nea.sg.oms.api.SgChannelStorgeOmsCallbackCmd;
import com.jackrain.nea.sg.oms.model.request.SgChannelStorgeOmsCallbackRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgChannelStorageOmsCallbackCmdImpl implements SgChannelStorgeOmsCallbackCmd {
    @Autowired
    SgChannelStorageOmsCallbackService sgChannelStorageOmsCallbackService;

    @Override
    public ValueHolderV14<Integer> callbackRPC(SgChannelStorgeOmsCallbackRequest request) {
        return sgChannelStorageOmsCallbackService.callbackRPC(request);
    }





}
