package com.jackrain.nea.sg.oms.services;

import com.jackrain.nea.sg.oms.api.SgChannelStorageDefChangeCmd;
import com.jackrain.nea.sg.oms.model.request.SgChannelStorageDefChangeRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgChannelStorageDefChangeCmdImpl implements SgChannelStorageDefChangeCmd {

    @Autowired
    SgChannelStorageDefChangeService sgChannelStorageDefChangeService;

    @Override
    public ValueHolderV14<Integer> changeSbGChannelDef(SgChannelStorageDefChangeRequest request) {
        return sgChannelStorageDefChangeService.changeSbGChannelDef(request);
    }

    @Override
    public ValueHolderV14<Integer> addSbGChannelDef(SgChannelStorageDefChangeRequest request) {
        return sgChannelStorageDefChangeService.addSbGChannelDef(request);
    }

}
