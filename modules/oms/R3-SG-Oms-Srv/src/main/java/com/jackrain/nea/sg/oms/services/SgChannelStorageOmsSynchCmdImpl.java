package com.jackrain.nea.sg.oms.services;

import com.jackrain.nea.sg.oms.api.SgChannelStorageOmsSynchCmd;
import com.jackrain.nea.sg.oms.model.request.SgStorageOmsSynchronousRequest;
import com.jackrain.nea.sg.oms.model.result.SgStorageOmsSynchronousResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgChannelStorageOmsSynchCmdImpl implements SgChannelStorageOmsSynchCmd {

    @Autowired
    SgChannelStorageOmsSynchService sgChannelStorageOmsSynchService;

    @Override
    public ValueHolderV14<SgStorageOmsSynchronousResult> synchronousStock(SgStorageOmsSynchronousRequest request) {
        return sgChannelStorageOmsSynchService.synchronousStock(request);
    }



}
