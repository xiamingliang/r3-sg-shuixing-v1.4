package com.jackrain.nea.sg.oms.services;

import com.jackrain.nea.sg.oms.api.SgChannelProductQueryCmd;
import com.jackrain.nea.sg.oms.model.request.SgChannelProductQueryRequest;
import com.jackrain.nea.sg.oms.model.table.SgBChannelProduct;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;

/**
 * @author zhu lin yu
 * @since 2019/7/16
 * create at : 2019/7/16 19:48
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgChannelProductQueryCmdImpl implements SgChannelProductQueryCmd {

    @Autowired
    private SgChannelProductQueryService sgChannelProductQueryService;

    @Override
    public ValueHolderV14<List<SgBChannelProduct>> queryChannelProduct(SgChannelProductQueryRequest sgChannelProductQueryRequest) {
        return this.sgChannelProductQueryService.queryChannelProduct(sgChannelProductQueryRequest);
    }

    @Override
    public List<HashMap<String, Object>> queryChannelProductByPageLayout(HashMap map) {
        return sgChannelProductQueryService.queryChannelProductByPageLayout(map);
    }
}
