package com.jackrain.nea.sg.oms.services;

import com.jackrain.nea.sg.basic.model.result.SgStoreWithPrioritySearchResult;
import com.jackrain.nea.sg.oms.api.SgStorageOmsQueryCmd;
import com.jackrain.nea.sg.oms.model.request.SgStoreWithPrioritySearchRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgStorageOmsQueryCmdImpl implements SgStorageOmsQueryCmd {

    @Autowired
    private SgStorageOmsQueryService sgStorageOmsQueryService;

    @Override
    public ValueHolderV14<SgStoreWithPrioritySearchResult> searchSgStoreWithPriority(SgStoreWithPrioritySearchRequest request) {
        return sgStorageOmsQueryService.searchSgStoreWithPriority(request);
    }

}
