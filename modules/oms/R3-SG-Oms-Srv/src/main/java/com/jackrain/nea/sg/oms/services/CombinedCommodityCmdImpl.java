package com.jackrain.nea.sg.oms.services;

import com.jackrain.nea.sg.oms.api.CombinedCommodityCmd;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Author: chenchen
 * @Description:组合商品和福袋商品服务类
 * @Date: Create in 15:28 2019/4/10
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class CombinedCommodityCmdImpl implements CombinedCommodityCmd {

    @Autowired
    CombinedCommodityService combinedCommodityService;

    @Override
    public ValueHolderV14 updateCombinedCommodityCmd(Integer wareType) {
        return combinedCommodityService.updateCombinedCommodity(wareType);
    }
}
