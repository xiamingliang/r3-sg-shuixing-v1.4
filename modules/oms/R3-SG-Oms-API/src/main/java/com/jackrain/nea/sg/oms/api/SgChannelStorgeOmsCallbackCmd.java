package com.jackrain.nea.sg.oms.api;

import com.jackrain.nea.sg.oms.model.request.SgChannelStorgeOmsCallbackRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 * 库存回调RPC服务
 */
public interface SgChannelStorgeOmsCallbackCmd {
    /**
     * 增量库存回调
     * @param request
     * @return
     */
    ValueHolderV14<Integer> callbackRPC(SgChannelStorgeOmsCallbackRequest request);
}
