package com.jackrain.nea.sg.oms.model.tableExtend;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.sg.oms.model.table.SgBChannelSynstockQ;

public class SgBChannelSynstockQExtend extends SgBChannelSynstockQ {
    public static final String SYNSTATUS = "syn_status";
    public static final String ID = "id";

    /**
     * 增量调用RPC服务
     */
    public static final String GROUPNAME = "ip";
    public static final String VERSION = "1.4.0";
    /**
     * 同步结果
     */
    public static class SynStatusEnum{
        // 同步成功
        public static final Integer SUCCESS = 4;
        // 同步失败
        public static final Integer FAIL = 3;
        // 线下超时
        public static final Integer TIMEOUT = 2;
        // 初始化状态
        public static final Integer INIT = 0;

    }

    /**
     * 同步方式
     */
       public static class SyncTypeEnum{
        // 增量
        public static final Integer ADD = 1;
        // 全量
        public static final Integer ALL = 2;
        // 手工
        public static final Integer HAND = 3;
        // 全量补偿
        public static final Integer RETRY = 4;
    }

    /**
     * platFrom 平台来源
     *
     */
    public static class PlatFormCode{
        // 淘宝全量
        public static final Integer TBQL = 2;
        // 淘宝分销
        public static final Integer TBFX = 3;
        // 京东全量
        public static final Integer JDQL = 4;
        // 唯品会JIT
        public static final Integer WPHJIT = 19;
        //唯品会OXO
        public static final Integer WPHOXO = 36;
    }


    /**
     * 查询类生成工具
     *
     * @return
     */
    public QueryWrapper<SgBChannelSynstockQ> createQueryWrapper(){
        QueryWrapper<SgBChannelSynstockQ> queryWrapper = new QueryWrapper<SgBChannelSynstockQ>();
        if(this.getSynStatus() != null ){
            queryWrapper.eq(SYNSTATUS,this.getSynStatus());
        }
        if(this.getId() != null ){
            queryWrapper.eq(ID,this.getId());
        }
        return queryWrapper;
    }
}
