package com.jackrain.nea.sg.oms.model.request;

import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @author caopengflying
 * @time 2019/4/25 15:45
 */
@Data
public class SgChannelStorageOmsBufferMessageRequest {
    private Date changeTime;
    @Deprecated
    private String billNo;
    @Deprecated
    private Long cpCshopId;

    private List<SgChannelStorageOmsBufferMessageItemRequest> itemList;
}
