package com.jackrain.nea.sg.oms.api;

import com.jackrain.nea.sg.oms.model.request.SgChannelStoreChangeRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 * 渠道逻辑仓关系表变动
 */
public interface SgChannelStoreChangeCmd {

    ValueHolderV14<Integer> saveSbGChannelStore(SgChannelStoreChangeRequest request);

    ValueHolderV14<Integer> deleteSbGChannelStore(SgChannelStoreChangeRequest request);

    ValueHolderV14<Integer> changeCpCStoreTitleById(SgChannelStoreChangeRequest request);


}
