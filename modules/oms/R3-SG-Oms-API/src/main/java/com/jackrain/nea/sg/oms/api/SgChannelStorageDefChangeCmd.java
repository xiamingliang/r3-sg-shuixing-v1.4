package com.jackrain.nea.sg.oms.api;

import com.jackrain.nea.sg.oms.model.request.SgChannelStorageDefChangeRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 * 新增或更新渠道信息表服务
 */
public interface SgChannelStorageDefChangeCmd {

    ValueHolderV14<Integer> changeSbGChannelDef(SgChannelStorageDefChangeRequest request);

    ValueHolderV14<Integer> addSbGChannelDef(SgChannelStorageDefChangeRequest request);

}
