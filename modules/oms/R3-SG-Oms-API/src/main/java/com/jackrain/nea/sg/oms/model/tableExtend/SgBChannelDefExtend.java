package com.jackrain.nea.sg.oms.model.tableExtend;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.sg.oms.model.table.SgBChannelDef;

public class SgBChannelDefExtend extends SgBChannelDef {
    /**
     * 字段及对应枚举
     */
    public static final String CPCSHOPID = "CP_C_SHOP_ID";//渠道ID

    public static final String PT_SKU_ID = "PT_SKU_ID";//渠道条码ID
    public static final String PT_STATUS= "ESTATUS";//店铺策略状态
    public static final Integer PT_STATUS_CONFIRMED= 2;//已审核


    public static final String STOCK_SCALE = "STOCK_SCALE";//同步比例

    public static final String LOW_STOCK = "LOW_STOCK";//安全库存
    public static final String ST_C_PRODUCT_STRATEGY_ID = "ST_C_PRODUCT_STRATEGY_ID";//主表id
    public static final String CP_C_PLATFORM_ID = "CP_C_PLATFORM_ID";//来源平台id(字段)
    public static final String SYSTEM_PLATFORM_ID = "1";//系统平台ID(值)

    public static final String SG_B_CHANNEL_DEF_ES_INDEX = "st_c_product_strategy"; //渠道商品特殊比例主表
    public static final String SG_B_CHANNEL_DEF_ES_TYPE = "st_c_product_strategy_item"; //渠道商品特殊比例主表

    //开始时间
    public static final String BEGIN_TIME = "BEGIN_TIME";
    //结束时间
    public static final String END_TIME = "END_TIME";

    /**
     * 查询类生成工具
     *
     * @return
     */
    public QueryWrapper<SgBChannelDef> createQueryWrapper(){
        QueryWrapper<SgBChannelDef> queryWrapper = new QueryWrapper<SgBChannelDef>();
        if(this.getCpCShopId() != null){
            // 平台店铺id
            queryWrapper.eq(CPCSHOPID, this.getCpCShopId());
        }
        return queryWrapper;
    }

}
