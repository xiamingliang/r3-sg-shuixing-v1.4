package com.jackrain.nea.sg.oms.model.request;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Description:
 * @Author: chenss
 * @Date: 2019/4/22 13:15
 */
@Data
@ToString
public class SgChannelStorageOmsReduceItemRequest implements Serializable {
    /**
     * 逻辑仓ID
     */
    private Long cpCStoreId;
    /**
     * skuId
     */
    private Long psCSkuId;

    /**
     * 库存变化量
     */
    private BigDecimal qtyStorage;

    /**
     * 来源渠道（店铺）ID
     */
    private Long sourceCpCShopId;

    /**
     * 来源单据号
     */
    private String sourceNo;
}
