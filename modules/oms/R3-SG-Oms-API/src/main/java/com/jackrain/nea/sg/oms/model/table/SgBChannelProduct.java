package com.jackrain.nea.sg.oms.model.table;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.*;
import com.jackrain.nea.es.annotations.Document;
import com.jackrain.nea.es.annotations.Field;
import com.jackrain.nea.es.constans.FieldType;
import com.jackrain.nea.sys.domain.BaseModel;
import lombok.Data;

import java.math.BigDecimal;

@TableName(value = "sg_b_channel_product")
@Data
@Document(index = "sg_b_channel_product",type = "sg_b_channel_product")
public class SgBChannelProduct extends BaseModel {
    @JSONField(name = "ID")
    @TableField(fill = FieldFill.INSERT)
    @TableId(value = "id", type = IdType.INPUT)
    @Field(type = FieldType.Long)
    private Long id;

    @JSONField(name = "CP_C_SHOP_ID")
    @Field(type = FieldType.Long)
    private Long cpCShopId;

    @JSONField(name = "CP_C_SHOP_TITLE")
    @Field(type = FieldType.Keyword)
    private String cpCShopTitle;

    @JSONField(name = "CP_C_SHOP_SELLER_NICK")
    @Field(type = FieldType.Keyword)
    private String cpCShopSellerNick;

    @JSONField(name = "CP_C_PLATFORM_ID")
    @Field(type = FieldType.Integer)
    private Integer cpCPlatformId;

    @JSONField(name = "SKU_ID")
    @Field(type = FieldType.Keyword)
    private String skuId;

    @JSONField(name = "NUMIID")
    @Field(type = FieldType.Keyword)
    private String numiid;

    @JSONField(name = "SKU_SPEC")
    @Field(type = FieldType.Keyword)
    private String skuSpec;

    @JSONField(name = "PIC_URL")
    @Field(type = FieldType.Keyword)
    private String picUrl;

    @JSONField(name = "PRICE")
    @Field(type = FieldType.Double)
    private BigDecimal price;

    @JSONField(name = "STATUS")
    @Field(type = FieldType.Integer)
    private Integer status;

    @JSONField(name = "QTY_STORAGE")
    @Field(type = FieldType.Double)
    private BigDecimal qtyStorage;

    @JSONField(name = "ATTRIBUTE")
    @Field(type = FieldType.Keyword)
    private String attribute;

    @JSONField(name = "PS_C_SKU_ID")
    @Field(type = FieldType.Long)
    private Long psCSkuId;

    @JSONField(name = "PS_C_SKU_ECODE")
    @Field(type = FieldType.Keyword)
    private String psCSkuEcode;

    @JSONField(name = "PS_C_PRO_ID")
    @Field(type = FieldType.Long)
    private Long psCProId;

    @JSONField(name = "PS_C_PRO_ECODE")
    @Field(type = FieldType.Keyword)
    private String psCProEcode;

    @JSONField(name = "PS_C_PRO_ENAME")
    @Field(type = FieldType.Keyword)
    private String psCProEname;

    @JSONField(name = "PS_C_SPEC1_ID")
    @Field(type = FieldType.Long)
    private Long psCSpec1Id;

    @JSONField(name = "PS_C_SPEC1_ECODE")
    @Field(type = FieldType.Keyword)
    private String psCSpec1Ecode;

    @JSONField(name = "PS_C_SPEC1_ENAME")
    @Field(type = FieldType.Keyword)
    private String psCSpec1Ename;

    @JSONField(name = "PS_C_SPEC2_ID")
    @Field(type = FieldType.Long)
    private Long psCSpec2Id;

    @JSONField(name = "PS_C_SPEC2_ECODE")
    @Field(type = FieldType.Keyword)
    private String psCSpec2Ecode;

    @JSONField(name = "PS_C_SPEC2_ENAME")
    @Field(type = FieldType.Keyword)
    private String psCSpec2Ename;

    @JSONField(name = "OWNERENAME")
    @Field(type = FieldType.Keyword)
    private String ownerename;

    @JSONField(name = "MODIFIERENAME")
    @Field(type = FieldType.Keyword)
    private String modifierename;

    @JSONField(name = "CP_C_PLATFORM_ECODE")
    @Field(type = FieldType.Keyword)
    private String cpCPlatformEcode;

    @JSONField(name = "CP_C_PLATFORM_ENAME")
    @Field(type = FieldType.Keyword)
    private String cpCPlatformEname;

    @JSONField(name = "GBCODE")
    @Field(type = FieldType.Keyword)
    private String gbcode;

    @JSONField(name = "WARE_TYPE")
    @Field(type = FieldType.Long)
    private Integer wareType;
}