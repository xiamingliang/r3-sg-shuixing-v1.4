package com.jackrain.nea.sg.oms.model.request;

import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.Data;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author caopengflying
 * @time 2019/4/24 19:08
 */
@Data
@ToString
public class SgChannelStorageOmsBufferRequest {
    private Date changeDate;
    /**
     * 逻辑仓ID
     */
    private Long cpCStoreId;

    /**
     * 库存变化量
     */
    private BigDecimal changeNum;
    /**
     * sku
     */
    private Long psCSkuId;
    /**
     * 来源渠道（店铺）ID
     */
    private Long sourceCpCShopId;
    /**
     * 来源单据号
     */
    private String sourceNo;
    /**
     * messageKey
     */
    private String messageKey;

    /**
     * 渠道库存计算池MQ消息数据校验
     * @return
     */
    public ValueHolderV14<Integer> checkCreate() {
        ValueHolderV14<Integer> holder = new ValueHolderV14<Integer>(ResultCode.SUCCESS,"");
        StringBuffer errorMsg = new StringBuffer();
        if (null == this.cpCStoreId){
            errorMsg.append("[逻辑仓ID为空]");
        }
        if (null == changeNum){
            errorMsg.append("[库存变化量为空]");
        }
        if (null == psCSkuId){
            errorMsg.append("[来源商品SKU为空]");
        }
        /*
        来源渠道ID，在分销情况下可能没有
        if (null == sourceCpCShopId){
            errorMsg.append("[来源平台渠道ID为空]");
        }*/
        if (null == sourceNo){
            errorMsg.append("[来源单据号为空]");
        }
        if (StringUtils.isNotEmpty(errorMsg.toString())){
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(errorMsg.toString());
        }
        return holder;
    }
}
