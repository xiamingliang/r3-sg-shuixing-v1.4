package com.jackrain.nea.sg.oms.model.request;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class SgChannelStoreChangeRequest implements Serializable {

    private Long cpCShopId;//平台店铺ID

    private String cpCShopTitle;//平台店铺标题

    private Long cpCStoreId;//逻辑仓ID

    private String cpCStoreEcode;//逻辑仓编码

    private String cpCStoreEname;//逻辑仓名称

    private Integer priority;//供货优先级

    private BigDecimal rate;//比例

    private BigDecimal lowStock;//低库存数

    private Integer isSend;//是否发货
}
