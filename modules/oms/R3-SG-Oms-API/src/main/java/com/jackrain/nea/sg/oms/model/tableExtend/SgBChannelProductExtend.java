package com.jackrain.nea.sg.oms.model.tableExtend;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.oms.model.table.SgBChannelProduct;
import lombok.Data;
import lombok.ToString;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

@Data
@ToString
public class SgBChannelProductExtend extends SgBChannelProduct {
    public static final String CPCSHOPID = "CP_C_SHOP_ID";
    public static final String PSCSKUID = "PS_C_SKU_ID";
    public static final String SKUID = "SKU_ID";
    /**
     * 辅助字段
     */
    private BaseExtend baseExtend;// 基础类

    private List<Long> cpCShopIdList;// 平台店铺idList

    private List<Long> psCSkuIdList;// skuIdList

    private List<String> skuIdList;//渠道条码ID list
    /**
     * 查询类生成工具
     *
     * @return
     */
    public QueryWrapper<SgBChannelProduct> createQueryWrapper() {
        QueryWrapper<SgBChannelProduct> queryWrapper = new QueryWrapper<SgBChannelProduct>();
        if (this.getCpCShopId() != null) {
            // 平台店铺id
            queryWrapper.eq(CPCSHOPID, this.getCpCShopId());
        }
        if(CollectionUtils.isNotEmpty(cpCShopIdList)){
            // 平台店铺idList
            queryWrapper.in(CPCSHOPID,cpCShopIdList);
        }
        if (this.getPsCSkuId() != null) {
            // sku
            queryWrapper.eq(PSCSKUID, this.getPsCSkuId());
        }
        if(CollectionUtils.isNotEmpty(psCSkuIdList)){
            // sku
            queryWrapper.in(PSCSKUID, psCSkuIdList);
        }
        if(StringUtils.isNotEmpty(this.getSkuId())){
            // 渠道条码
            queryWrapper.eq(SKUID, this.getSkuId());
        }
        if(CollectionUtils.isNotEmpty(skuIdList)){
            // 渠道条码ID list
            queryWrapper.in(SKUID, skuIdList);
        }
        if (queryWrapper.isEmptyOfWhere()){
            throw new NDSException("不允许查询整个渠道商品表");
        }
        return queryWrapper;
    }


}
