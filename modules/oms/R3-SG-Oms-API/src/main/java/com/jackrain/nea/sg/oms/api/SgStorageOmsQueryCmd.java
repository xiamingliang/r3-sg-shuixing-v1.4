package com.jackrain.nea.sg.oms.api;

import com.jackrain.nea.sg.basic.model.result.SgStoreWithPrioritySearchResult;
import com.jackrain.nea.sg.oms.model.request.SgStoreWithPrioritySearchRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/4/2 13:15
 */
public interface SgStorageOmsQueryCmd {

    ValueHolderV14<SgStoreWithPrioritySearchResult> searchSgStoreWithPriority(SgStoreWithPrioritySearchRequest request);

}
