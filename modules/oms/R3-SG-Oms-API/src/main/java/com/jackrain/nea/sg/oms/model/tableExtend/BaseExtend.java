package com.jackrain.nea.sg.oms.model.tableExtend;


import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString
public class BaseExtend {
    /**
     * id范围
     */
    private List<Long> idList;

    /**
     * 状态范围
     */
    private List<Integer> statusList;

}
