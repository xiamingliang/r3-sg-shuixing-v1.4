package com.jackrain.nea.sg.oms.api;

import com.jackrain.nea.sg.oms.model.request.SgChannelStorageQueryRequest;
import com.jackrain.nea.sg.oms.model.table.SgBChannelStorage;
import com.jackrain.nea.sys.domain.ValueHolderV14;

import java.util.List;

/**
 * @author zhu lin yu
 * @since 2019-11-11
 * create at : 2019-11-11 20:44
 */
public interface SgChannelStorageQueryCmd {
    ValueHolderV14<List<SgBChannelStorage>> queryChannelStorage(List<SgChannelStorageQueryRequest> requestList);
}
