package com.jackrain.nea.sg.oms.model.request;

import com.jackrain.nea.sg.basic.model.request.SgR3BaseRequest;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author zhu lin yu
 * @since 2019-11-11
 * create at : 2019-11-11 20:48
 */

@Data
public class SgChannelStorageQueryRequest extends SgR3BaseRequest implements Serializable {
    private Long shopId;

    private List<String> skuEcodeList;
}
