package com.jackrain.nea.sg.oms.model.request;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
public class SgChannelStorgeOmsCallbackRequest implements Serializable {
    //id
    private Long id;
    //错误信息
    private String errorMsg;
    //同步状态
    private Integer synStatus;

}
