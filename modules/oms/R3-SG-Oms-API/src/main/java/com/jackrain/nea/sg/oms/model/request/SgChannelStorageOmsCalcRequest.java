package com.jackrain.nea.sg.oms.model.request;

import lombok.Data;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

/**
 * @Description:
 * @Author: chenss
 * @Date: 2019/4/22 13:15
 */
@Data
@ToString
public class SgChannelStorageOmsCalcRequest implements Serializable {
    /**
     * 平台店铺id
     */
    private Long cpCShopId;
    /**
     * 平台店铺标题
     */
    private String cpCShopTitle;
    /**
     * 平台（渠道）条码ID
     */
    private String skuId;
    /**
     * 平台（渠道）条码编码
     */
    private String psCSkukuEcode;
    /**
     * 来源渠道（店铺）ID
     */
    private Long sourceCpCShopId;
    /**
     * 来源单据号
     */
    private String sourceNo;

    /**
     * 任务关键字段
     */
    private String messageKey;

    private Integer wareType;

    public Boolean check() {
        if (cpCShopId == null || StringUtils.isEmpty(skuId)) {
            return true;
        }
        return false;
    }


}
