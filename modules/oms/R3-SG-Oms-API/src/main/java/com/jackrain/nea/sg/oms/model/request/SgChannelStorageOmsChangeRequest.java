package com.jackrain.nea.sg.oms.model.request;

import lombok.Data;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@ToString
public class SgChannelStorageOmsChangeRequest implements Serializable {
    /**
     * 渠道id
     */
    private Long cpCShopId;
    /**
     * 渠道条码id
     */
    private String skuId;
    /**
     * 库存变化量
     */
    private BigDecimal qtyStorage;

    /**
     * 来源渠道（店铺）ID
     */
    private Long sourceCpCShopId;
    /**
     * 来源单据号
     */
    private String sourceNo;

    private String skuSpec;

    /**
     * 参数校验
     */
    public Boolean check() {
        if (cpCShopId == null || StringUtils.isEmpty(skuId) || qtyStorage == null) {
            return true;
        }
        return false;
    }

    /**
     * 获取锁关键字
     * @return
     */
    public String getKey() {
        return cpCShopId + ":" + skuId;
    }

}
