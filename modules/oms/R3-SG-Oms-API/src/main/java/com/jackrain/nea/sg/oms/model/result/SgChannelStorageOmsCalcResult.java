package com.jackrain.nea.sg.oms.model.result;

import com.jackrain.nea.sg.oms.model.request.SgChannelStorageOmsCalcRequest;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * @Description:
 * @Author: chenss
 * @Date: 2019/4/22 13:15
 */
@Data
@ToString
public class SgChannelStorageOmsCalcResult extends SgChannelStorageOmsCalcRequest implements Serializable {
    /**
     * 描述信息
     */
    private String descriptMessage;

}
