package com.jackrain.nea.sg.oms.model.request;

import com.jackrain.nea.sg.oms.model.table.SgBChannelStorage;
import com.jackrain.nea.web.face.User;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@ToString
public class SgStorageOmsSynchronousRequest implements Serializable {
    //渠道id
    private Long cpCShopId;
    //渠道条码id
    private String skuId;
    //同步库存量
    private BigDecimal qtyStorage;
    //同步类型
    private Integer syncType;
    //渠道库存
    private SgBChannelStorage sgBChannelStorage;
    //登录人信息
    private User loginUser;
    //来源来源单据号
    private String sourceNo;
    //平台id
    private String cpCPlatformId;

    private String skuSpec;
}
