package com.jackrain.nea.sg.oms.api;

import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 * @Author: chenchen
 * @Description:
 * @Date: Create in 15:27 2019/4/10
 */
public interface CombinedCommodityCmd {
    /**
     * 更新组合商品和福袋商品库存
     *
     * @param groupType 1 p普通 2 福袋
     * @return
     */
    ValueHolderV14 updateCombinedCommodityCmd(Integer groupType);
}
