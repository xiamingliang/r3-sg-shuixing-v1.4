package com.jackrain.nea.sg.oms.api;

import com.jackrain.nea.sg.oms.model.request.SgChannelProductSaveRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 * @author zhu lin yu
 * @since 2019/5/14
 * create at : 2019/5/14 10:16
 */
public interface SgChannelProductSaveCmd {

    ValueHolderV14 saveChannelProduct(SgChannelProductSaveRequest sgChannelProductSaveRequest);

}
