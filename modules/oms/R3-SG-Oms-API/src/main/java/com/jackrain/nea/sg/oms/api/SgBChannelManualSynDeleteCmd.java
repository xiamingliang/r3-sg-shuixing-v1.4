package com.jackrain.nea.sg.oms.api;

import com.jackrain.nea.sys.Command;

/**
 * @Author chenjinjun
 * @Description 手工导入店铺库存删除接口
 * @Date  2019-10-21
**/
public interface SgBChannelManualSynDeleteCmd extends Command {
}
