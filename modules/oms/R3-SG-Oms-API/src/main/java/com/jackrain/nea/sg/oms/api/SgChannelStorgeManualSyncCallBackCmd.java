package com.jackrain.nea.sg.oms.api;

import com.jackrain.nea.sg.oms.model.request.SgChannelStorgeOmsCallbackAllRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 * 库存回调RPC服务
 */
public interface SgChannelStorgeManualSyncCallBackCmd {

    /**
     * 全量库存回调
     * @param request
     * @return
     */
    ValueHolderV14<Integer> callbackAllRPC(SgChannelStorgeOmsCallbackAllRequest request);
}
