package com.jackrain.nea.sg.oms.model.table;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.*;
import com.jackrain.nea.es.annotations.Document;
import com.jackrain.nea.es.annotations.Field;
import com.jackrain.nea.es.constans.FieldType;
import com.jackrain.nea.sys.domain.BaseModel;
import lombok.Data;

import java.math.BigDecimal;

@TableName(value = "sg_b_channel_synstock_q")
@Data
@Document(index = "sg_b_channel_synstock_q",type = "sg_b_channel_synstock_q")
public class SgBChannelSynstockQ extends BaseModel {
    @JSONField(name = "ID")
    @TableField(fill = FieldFill.INSERT)
    @TableId(value = "id", type = IdType.INPUT)
    @Field(type = FieldType.Long)
    private Long id;

    @JSONField(name = "BATCHNO")
    @Field(type = FieldType.Keyword)
    private String batchno;

    @JSONField(name = "SELLER_NICK")
    @Field(type = FieldType.Keyword)
    private String sellerNick;

    @JSONField(name = "NUMIID")
    @Field(type = FieldType.Keyword)
    private String numiid;

    @JSONField(name = "IS_ERROR")
    @Field(type = FieldType.Integer)
    private Integer isError;

    @JSONField(name = "ERROR_MSG")
    @Field(type = FieldType.Keyword)
    private String errorMsg;

    @JSONField(name = "SYN_STATUS")
    @Field(type = FieldType.Integer)
    private Integer synStatus;

    @JSONField(name = "QTY_STORAGE")
    @Field(type = FieldType.Double)
    private BigDecimal qtyStorage;

    @JSONField(name = "SKU_ID")
    @Field(type = FieldType.Keyword)
    private String skuId;

    @JSONField(name = "PS_C_SKU_ID")
    @Field(type = FieldType.Long)
    private Long psCSkuId;

    @JSONField(name = "PS_C_SKU_ECODE")
    @Field(type = FieldType.Keyword)
    private String psCSkuEcode;

    @JSONField(name = "CP_C_SHOP_ID")
    @Field(type = FieldType.Long)
    private Long cpCShopId;

    @JSONField(name = "CP_C_SHOP_TITLE")
    @Field(type = FieldType.Keyword)
    private String cpCShopTitle;

    @JSONField(name = "PROPERTIES")
    @Field(type = FieldType.Keyword)
    private String properties;

    @JSONField(name = "MESSAGE_KEY")
    @Field(type = FieldType.Keyword)
    private String messageKey;

    @JSONField(name = "OWNERENAME")
    @Field(type = FieldType.Keyword)
    private String ownerename;

    @JSONField(name = "MODIFIERENAME")
    @Field(type = FieldType.Keyword)
    private String modifierename;

    @JSONField(name = "CP_C_PLATFORM_ID")
    @Field(type = FieldType.Long)
    private Long cpCPlatformId;

    @JSONField(name = "CP_C_PLATFORM_ECODE")
    @Field(type = FieldType.Keyword)
    private String cpCPlatformEcode;

    @JSONField(name = "CP_C_PLATFORM_ENAME")
    @Field(type = FieldType.Keyword)
    private String cpCPlatformEname;

    @JSONField(name = "SOURCE_NO")
    @Field(type = FieldType.Keyword)
    private String sourceNo;

    @JSONField(name = "GBCODE")
    @Field(type = FieldType.Keyword)
    private String gbcode;
}