package com.jackrain.nea.sg.oms.model.request;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
public class SgChannelStorgeOmsCallbackAllRequest implements Serializable {

    private Long shopStock;
    private Integer status;
    private String msg;
    private Integer isagainstock;
    private Integer platForm;
    private Long id;

}
