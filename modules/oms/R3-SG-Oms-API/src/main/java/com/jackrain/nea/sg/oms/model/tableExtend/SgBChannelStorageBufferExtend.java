package com.jackrain.nea.sg.oms.model.tableExtend;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.oms.common.OmsConstantsIF;
import com.jackrain.nea.sg.oms.model.request.SgChannelStorageOmsBufferMessageItemRequest;
import com.jackrain.nea.sg.oms.model.request.SgChannelStorageOmsBufferMessageRequest;
import com.jackrain.nea.sg.oms.model.request.SgChannelStorageOmsBufferRequest;
import com.jackrain.nea.sg.oms.model.table.SgBChannelProduct;
import com.jackrain.nea.sg.oms.model.table.SgBChannelStorageBuffer;
import lombok.Data;
import lombok.ToString;
import org.apache.commons.collections.CollectionUtils;
import org.assertj.core.util.Lists;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@Data
@ToString
public class SgBChannelStorageBufferExtend extends SgBChannelStorageBuffer {
    /**
     * 状态及对应枚举
     */
    public static final String SGB_CHANNEL_STORAGE_BUFFER_TAG = "storage_to_channel";
    public static final String SGB_CHANNEL_STORAGE_BUFFER_Topic = "BJ_DEV_R3_SG_CHANNEL_STORAGE_BUFFER";
    public static final String ID = "ID";
    public static final String DEALSTATUS = "DEAL_STATUS";// 状态 1未执行 2执行中 3执行成功 4待补偿 5执行失败(无需补偿)
    public static final Long DEFAULT_CLIENT_ID = 37L;// 默认公司ID
    public static final Long DEFAULT_ORG_ID = 27L;// 默认组织ID
    /**
     * 辅助字段
     */
    private BaseExtend baseExtend;// 基础类
    private Integer listSize;// 返回的记录数
    private Integer updateDealStatus;// 更新后的状态

    /**
     * 将渠道商品转化为库存计算池模型
     *
     * @return
     */
    public static List<SgBChannelStorageBuffer> parseChannelProductToCSBuffer(List<SgBChannelProduct> sgBChannelProductList,String messageTag) {
        List<SgBChannelStorageBuffer> sgBChannelStorageBufferList = sgBChannelProductList.stream().map(sg -> {
            SgBChannelStorageBuffer sgBChannelStorageBuffer = new SgBChannelStorageBuffer();
            sgBChannelStorageBuffer.setCpCShopId(sg.getCpCShopId());
            sgBChannelStorageBuffer.setCpCShopTitle(sg.getCpCShopTitle());
            sgBChannelStorageBuffer.setSkuId(sg.getSkuId());
            sgBChannelStorageBuffer.setDealStatus(DealStatusEnum.UN_DEAL.getCode());
            if (SgConstantsIF.MSG_TAG_GROUP_STORAGE_TO_CHANNEL.equals(messageTag)) {
                sgBChannelStorageBuffer.setWareType(OmsConstantsIF.WARE_TYPE_GROUP_PRO);
            } else {
                sgBChannelStorageBuffer.setWareType(OmsConstantsIF.WARE_TYPE_NORMAL_PRO);
            }
            return sgBChannelStorageBuffer;
        }).collect(Collectors.toList());
        return sgBChannelStorageBufferList;
    }

    /**
     * 将消息转化为计算缓存池模型
     *
     * @param messageBody {
     *                    "param": [{
     *                          "changeTime": "2019-03-21 09:01:54",
     *                          "billNo": "FH0119031809424A",
     *                          "cpCshopId": 88820180725484418,
     *                          "itemList": [{
     *                              "cpCStoreId": 1,
     *                              "psCSkuId": 11,
     *                              "qtyChange": 5.0
     *                               },
     *                               {
     *                              "cpCStoreId": 1,
     *                              "psCSkuId": 11,
     *                              "qtyChange": 5.0
     *                              }]
     *                          }]
     *                    }
     * @return
     */
    public static List<SgChannelStorageOmsBufferRequest> parseMessageToSgChannelStorageOmsBufferRequest(String messageBody) {
        Object param = JSON.parseObject(messageBody, HashMap.class).get("param");
        if (null == param) {
            return null;
        }
        List<SgChannelStorageOmsBufferMessageRequest> sgChannelStorageOmsBufferMessageRequestList = null;
        List<SgChannelStorageOmsBufferRequest> sgChannelStorageOmsBufferRequestList = Lists.newArrayList();
        try {
            sgChannelStorageOmsBufferMessageRequestList = JSON.parseArray(JSONObject.toJSONString(param), SgChannelStorageOmsBufferMessageRequest.class);
            if (null == sgChannelStorageOmsBufferMessageRequestList) {
                return null;
            }
            for (SgChannelStorageOmsBufferMessageRequest sgcsoBuffer : sgChannelStorageOmsBufferMessageRequestList) {
                for (SgChannelStorageOmsBufferMessageItemRequest item : sgcsoBuffer.getItemList()) {
                    SgChannelStorageOmsBufferRequest sgChannelStorageOmsBufferRequest = new SgChannelStorageOmsBufferRequest();
                    sgChannelStorageOmsBufferRequest.setChangeDate(sgcsoBuffer.getChangeTime());
                    sgChannelStorageOmsBufferRequest.setSourceNo(item.getBillNo());
                    sgChannelStorageOmsBufferRequest.setSourceCpCShopId(item.getCpCshopId());
                    sgChannelStorageOmsBufferRequest.setCpCStoreId(item.getCpCStoreId());
                    sgChannelStorageOmsBufferRequest.setChangeNum(item.getQtyChange());
                    sgChannelStorageOmsBufferRequest.setPsCSkuId(item.getPsCSkuId());
                    sgChannelStorageOmsBufferRequestList.add(sgChannelStorageOmsBufferRequest);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sgChannelStorageOmsBufferRequestList;
    }

    /**
     * 查询类生成工具
     *
     * @return
     */
    public QueryWrapper<SgBChannelStorageBuffer> createQueryWrapper() {
        QueryWrapper<SgBChannelStorageBuffer> queryWrapper = new QueryWrapper<SgBChannelStorageBuffer>();
        if (this.getDealStatus() != null) {
            // 状态
            queryWrapper.eq(DEALSTATUS, this.getDealStatus());
        }
        if (this.baseExtend != null) {
            if (CollectionUtils.isNotEmpty(this.baseExtend.getStatusList())) {
                // 状态范围
                queryWrapper.in(DEALSTATUS, this.baseExtend.getStatusList());
            }
            if (CollectionUtils.isNotEmpty(this.baseExtend.getIdList())) {
                // id范围
                queryWrapper.in(ID, this.baseExtend.getIdList());
            }
        }
        return queryWrapper;
    }

    public enum DealStatusEnum {
        UN_DEAL(1, "未执行"),
        DEALING(2, "执行中"),
        DEAL_SUCCESS(3, "执行成功"),
        DEAL_REDEAL(4, "待补偿"),
        DEAL_FAIL(5, "执行失败(无需补偿)");

        private Integer code;
        private String name;

        DealStatusEnum(Integer code, String name) {
            this.code = code;
            this.name = name;
        }

        public Integer getCode() {
            return code;
        }

        public String getName() {
            return name;
        }
    }
}
