package com.jackrain.nea.sg.oms.model.result;

import lombok.Data;

/**
 * @Author: chenchen
 * @Description:库存按照店仓和可用数量分组
 * @Date: Create in 16:23 2019/4/8
 */
@Data
public class SgBStorageResult {

    private Long storeId;
    private String storeName;
    private String storeEcode;
    private String qtyAvailableList;
}
