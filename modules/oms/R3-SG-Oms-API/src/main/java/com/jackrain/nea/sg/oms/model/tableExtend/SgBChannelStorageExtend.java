package com.jackrain.nea.sg.oms.model.tableExtend;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.oms.model.table.SgBChannelDef;
import com.jackrain.nea.sg.oms.model.table.SgBChannelProduct;
import com.jackrain.nea.sg.oms.model.table.SgBChannelStorage;
import lombok.Data;
import lombok.ToString;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.Date;
import java.util.List;

@Data
@ToString
public class SgBChannelStorageExtend extends SgBChannelStorage {
    /**
     * 新增处理字段
     * 'ID''平台店铺ID''平台店铺标题''可售库存''平台商品ID''平台条码ID''平台类型'
     * '条码id''条码编码''商品id''商品编码'商品名称''规格1id''规格1编码''规格1名称'
     * '规格2id''规格2编码''规格2名称''创建时间'
     */
    /**
     * 修改处理字段
     * 可售库存'修改时间'
     */
    /**
     * 状态及对应枚举
     */
    //平台店铺ID
    public static final String CPCSHOPID = "CP_C_SHOP_ID";
    //平台条码ID
    public static final String SKUID = "SKU_ID";
    public static final String ID = "ID";
    //平台类型
    public static final String CP_C_PLATFORM_ID = "CP_C_PLATFORM_ID";
    //商品编码
    public static final String PS_C_PRO_ECODE = "PS_C_PRO_ECODE";
    //平台商品ID
    public static final String NUMIID = "NUMIID";
    //条码编码
    public static final String PS_C_SKU_ECODE = "PS_C_SKU_ECODE";


    private List<Integer> idList;
    //平台类型 集合
    private List<Integer> cpCPlatformIdList;
    //平台店铺ID集合
    private List<Integer>cpCShopIdList;

    /**
     * 辅助字段
     */
    private BaseExtend baseExtend;// 基础类
    /**
     * 查询类生成工具
     *
     * @return
     */
    public QueryWrapper<SgBChannelStorage> createQueryWrapper() {
        QueryWrapper<SgBChannelStorage> queryWrapper = new QueryWrapper<SgBChannelStorage>();
        if (this.getCpCShopId() != null) {
            // 平台店铺id
            queryWrapper.eq(CPCSHOPID, this.getCpCShopId());
        }
        if (CollectionUtils.isNotEmpty(this.getCpCShopIdList())){
            // 平台店铺id集合
            queryWrapper.in(CPCSHOPID, this.getCpCShopIdList());
        }
        if (StringUtils.isNotEmpty(this.getSkuId())) {
            // 渠道条码ID
            queryWrapper.eq(SKUID, this.getSkuId());
        }
        if (CollectionUtils.isNotEmpty(this.getIdList())){
            queryWrapper.in(ID,this.getIdList());
        }
        if (null != this.getCpCPlatformId()){
            queryWrapper.eq(CP_C_PLATFORM_ID, this.getCpCPlatformId());
        }
        if (CollectionUtils.isNotEmpty(cpCPlatformIdList)){
            queryWrapper.in(CP_C_PLATFORM_ID, this.getCpCPlatformIdList());
        }
        if (StringUtils.isNotEmpty(this.getNumiid())){
            queryWrapper.eq(NUMIID, this.getNumiid());
        }
        if (StringUtils.isNotEmpty(this.getPsCProEcode())){
            queryWrapper.eq(PS_C_PRO_ECODE, this.getPsCProEcode());
        }
        if (StringUtils.isNotEmpty(this.getPsCSkuEcode())){
            queryWrapper.eq(PS_C_SKU_ECODE, this.getPsCSkuEcode());
        }
        if (queryWrapper.isEmptyOfWhere()){
            throw new NDSException("请至少输入一个查询条件");
        }
        return queryWrapper;
    }

    public QueryWrapper<SgBChannelProduct> createChannelProductQueryWrapper() {
        QueryWrapper<SgBChannelProduct> queryWrapper = new QueryWrapper<>();
        if (this.getCpCShopId() != null) {
            // 平台店铺id
            queryWrapper.eq(CPCSHOPID, this.getCpCShopId());
        }
        if (CollectionUtils.isNotEmpty(this.getCpCShopIdList())){
            // 平台店铺id集合
            queryWrapper.in(CPCSHOPID, this.getCpCShopIdList());
        }
        if (StringUtils.isNotEmpty(this.getSkuId())) {
            // 渠道条码ID
            queryWrapper.eq(SKUID, this.getSkuId());
        }
        if (CollectionUtils.isNotEmpty(this.getIdList())){
            queryWrapper.in(ID,this.getIdList());
        }
        if (null != this.getCpCPlatformId()){
            queryWrapper.eq(CP_C_PLATFORM_ID, this.getCpCPlatformId());
        }
        if (CollectionUtils.isNotEmpty(cpCPlatformIdList)){
            queryWrapper.in(CP_C_PLATFORM_ID, this.getCpCPlatformIdList());
        }
        if (StringUtils.isNotEmpty(this.getNumiid())){
            queryWrapper.eq(NUMIID, this.getNumiid());
        }
        if (StringUtils.isNotEmpty(this.getPsCProEcode())){
            queryWrapper.eq(PS_C_PRO_ECODE, this.getPsCProEcode());
        }
        if (StringUtils.isNotEmpty(this.getPsCSkuEcode())){
            queryWrapper.eq(PS_C_SKU_ECODE, this.getPsCSkuEcode());
        }
        if (queryWrapper.isEmptyOfWhere()){
            throw new NDSException("请至少输入一个查询条件");
        }
        return queryWrapper;
    }

    /**
     * 构造SgBChannelStorage
     *
     * @param sgBChannelDef
     * @param sgBChannelProduct
     */
    public static void complete(SgBChannelStorage sgBChannelStorage,
                                SgBChannelDef sgBChannelDef,
                                SgBChannelProduct sgBChannelProduct) {
        sgBChannelStorage.setCpCShopId(sgBChannelProduct.getCpCShopId());
        sgBChannelStorage.setCpCShopTitle(sgBChannelProduct.getCpCShopTitle());
        sgBChannelStorage.setCpCShopSellerNick(sgBChannelProduct.getCpCShopSellerNick());
        sgBChannelStorage.setNumiid(sgBChannelProduct.getNumiid());
        sgBChannelStorage.setSkuId(sgBChannelProduct.getSkuId());
        sgBChannelStorage.setCpCPlatformId(sgBChannelProduct.getCpCPlatformId());
        sgBChannelStorage.setPsCSkuId(sgBChannelProduct.getPsCSkuId());
        sgBChannelStorage.setPsCSkuEcode(sgBChannelProduct.getPsCSkuEcode());
        sgBChannelStorage.setPsCProId(sgBChannelProduct.getPsCProId());
        sgBChannelStorage.setPsCProEcode(sgBChannelProduct.getPsCProEcode());
        sgBChannelStorage.setPsCProEname(sgBChannelProduct.getPsCProEname());
        sgBChannelStorage.setPsCSpec1Id(sgBChannelProduct.getPsCSpec1Id());
        sgBChannelStorage.setPsCSpec1Ecode(sgBChannelProduct.getPsCSpec1Ecode());
        sgBChannelStorage.setPsCSpec1Ename(sgBChannelProduct.getPsCSpec1Ename());
        sgBChannelStorage.setPsCSpec2Id(sgBChannelProduct.getPsCSpec2Id());
        sgBChannelStorage.setPsCSpec2Ecode(sgBChannelProduct.getPsCSpec2Ecode());
        sgBChannelStorage.setPsCSpec2Ename(sgBChannelProduct.getPsCSpec2Ename());
        sgBChannelStorage.setGbcode(sgBChannelProduct.getGbcode());
        sgBChannelStorage.setCreationdate(new Date());
    }

}
