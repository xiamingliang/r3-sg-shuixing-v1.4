package com.jackrain.nea.sg.oms.common;

import java.math.BigDecimal;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/4/1 15:37
 */
public interface OmsConstantsIF {

    /**
     * 百分比
     */
    BigDecimal PERCENT = new BigDecimal("100");

    /**
     * 是否
     */
    int YES = 1;
    int NO = -1;

    /**
     * redis锁尝试次数
     */
    Integer maxRedisLockCount = 3;

    /**
     * redis锁定时间
     */
    Long redisLockTime = 30l;

    /**
     * MQ幂等性防重锁
     */
    String REDIS_KEY_PREFIX_CHANNEL_STORAGE = "channel_storage_change:";

    /**
     * 组合商品标记
     */
    Integer WARE_TYPE_GROUP_PRO = 1;

    /**
     * 普通商品标记
     */
    Integer WARE_TYPE_NORMAL_PRO = 0;

    /**
     * 拼多多苏宁平台回调tag
     */
    String SG_CHANNEL_STORAGE_OMS_CALLBACK_TAG = "qddev";

    String SHOP_CHANNEL_TYPE_DIRECTLY = "1";    //直营
    String SHOP_CHANNEL_TYPE_DISTRIBUTION = "2";  //分销
    String SHOP_CHANNEL_TYPE_OWN_PRO = "-1";    //分销店铺的经销商自有商品
    String SHOP_CHANNEL_TYPE_NO_DISTRIBUTION_PRO = "-2";  //分销店铺的非分销代销商品

    int IS_SYNC_FLAG_LOCK_SKU = -1;    //无需同步，存在锁条码策略
    int IS_SYNC_FLAG_LOCK_STORE = -2;  //无需同步，存在锁库策略
    int IS_SYNC_FLAG_NO_CHANNEL_INFO = -3;  //无需同步，无渠道信息
    int IS_SYNC_FLAG_CHANNEL_NOSYNC = -4;  //无需同步，渠道设置不同步
    int IS_SYNC_FLAG_SYNCABLE = 1; //可同步


    //库存同步方式
    String AUTO_SNYC = "AUTO";
    String MANUAL_SNYC = "MANUAL";   //手工导入同步
}
