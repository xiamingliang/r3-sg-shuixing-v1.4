package com.jackrain.nea.sg.oms.model.request;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.io.Serializable;

@Data
public class StandplatRequest implements Serializable {
    @JSONField(name = "ID")
    private Long ID;

    @JSONField(name = "TID")
    private String tid;

    @JSONField(name = "IS_SUCCESS")
    private boolean isSuccess;

    @JSONField(name = "MESSAGE")
    private String message;
}