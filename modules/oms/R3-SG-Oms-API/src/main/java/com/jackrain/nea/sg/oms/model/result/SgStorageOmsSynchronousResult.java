package com.jackrain.nea.sg.oms.model.result;

import lombok.Data;

import java.io.Serializable;

@Data
public class SgStorageOmsSynchronousResult implements Serializable {
    public SgStorageOmsSynchronousResult(){
        whetherSynchronous = false;
    }
    //是否同步
    private Boolean whetherSynchronous;
}
