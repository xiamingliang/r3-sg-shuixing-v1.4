package com.jackrain.nea.sg.oms.api;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.oms.model.request.SgChannelStorageOmsReduceRequest;
import com.jackrain.nea.sg.oms.model.result.SgChannelStorageOmsReduceResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 * @Description:
 * @Author: chenss
 * @Date: 2019/4/22 13:15
 */
public interface SgChannelStorageOmsReduceCmd {

    /**
     * 扣减渠道库存
     * @param request
     * @return
     */
    ValueHolderV14<SgChannelStorageOmsReduceResult> reductChannelStorageOms(SgChannelStorageOmsReduceRequest request,String messageTag) throws NDSException;

}
