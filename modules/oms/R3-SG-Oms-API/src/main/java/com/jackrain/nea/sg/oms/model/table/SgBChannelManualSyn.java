package com.jackrain.nea.sg.oms.model.table;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.jackrain.nea.sys.domain.BaseModel;
import java.util.Date;
import lombok.Data;

@TableName(value = "sg_b_channel_manual_syn")
@Data
public class SgBChannelManualSyn extends BaseModel {
    @JSONField(name = "ID")
    @TableField(fill = FieldFill.INSERT)
    @TableId(value = "id", type = IdType.INPUT)
    private Long id;

    @JSONField(name = "CP_C_SHOP_ID")
    private Long cpCShopId;

    @JSONField(name = "DELER_ID")
    private Long delerId;

    @JSONField(name = "DELER_ENAME")
    private String delerEname;

    @JSONField(name = "DELER_NAME")
    private String delerName;

    @JSONField(name = "DEL_TIME")
    private Date delTime;

    @JSONField(name = "VERSION_NO")
    private Long versionNo;

    @JSONField(name = "OWNERENAME")
    private String ownerename;

    @JSONField(name = "MODIFIERENAME")
    private String modifierename;

    @JSONField(name = "REMARK")
    private String remark;
}