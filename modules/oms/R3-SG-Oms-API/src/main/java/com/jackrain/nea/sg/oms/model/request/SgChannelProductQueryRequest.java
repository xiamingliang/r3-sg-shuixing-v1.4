package com.jackrain.nea.sg.oms.model.request;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author zhu lin yu
 * @since 2019/7/16
 * create at : 2019/7/16 19:46
 */
@Data
public class SgChannelProductQueryRequest implements Serializable {
    private List<Long> sgBChannelProductIdList;

    private List<Long> psCSkuIdList;
}
