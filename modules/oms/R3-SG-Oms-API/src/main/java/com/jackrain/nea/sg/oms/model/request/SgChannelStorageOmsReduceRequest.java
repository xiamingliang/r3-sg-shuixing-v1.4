package com.jackrain.nea.sg.oms.model.request;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Description:
 * @Author: chenss
 * @Date: 2019/4/22 13:15
 */
@Data
public class SgChannelStorageOmsReduceRequest implements Serializable {
    /**
     *  批量请求
     */
    private List<SgChannelStorageOmsReduceItemRequest> itemList;

    /**
     * 内部变量,
     */
    protected List<Long> singleCpCShopIdList;

}
