package com.jackrain.nea.sg.oms.api;

import com.jackrain.nea.sys.Command;

/**
 * @author zhu lin yu
 * @since 2019-11-08
 * create at : 2019-11-08 10:56
 */
public interface SgChannelProductBoundCmd extends Command {
}
