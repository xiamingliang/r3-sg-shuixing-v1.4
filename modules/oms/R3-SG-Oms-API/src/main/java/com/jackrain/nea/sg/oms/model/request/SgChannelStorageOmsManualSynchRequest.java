package com.jackrain.nea.sg.oms.model.request;

import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.Data;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.List;

/**
 * @author caopengflying
 * @time 2019/6/19 9:24
 */
@Data
public class SgChannelStorageOmsManualSynchRequest implements Serializable {
    public static Integer QUERY_BY_CONDITION = 1;
    private Integer operate;
    private List<Integer> idList;
    //渠道ID
    private Integer cpCShopId;
    //平台店铺ID集合
    private List<Integer>cpCShopIdList;
    //平台编码
    private String numiid;
    //平台条码ID
    private String skuId;
    //平台类型
    private Integer cpCPlatformId;
    //平台类型 集合
    private List<Integer> cpCPlatformIdList;
    //商品编码
    private String psCProEcode;
    //条码编码
    private String psCSkuEcode;



    /**
     * 校验手工同步渠道库存入参
     *
     * @return
     */
    public ValueHolderV14<Boolean> checkManualSynch() {
        ValueHolderV14<Boolean> holder = new ValueHolderV14<>(ResultCode.SUCCESS,"");
        StringBuffer errorMsg = new StringBuffer();
        if (CollectionUtils.isEmpty(idList)) {
            errorMsg.append("请至少选择一条数据");
        }
        if (StringUtils.isNotEmpty(errorMsg)){
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(errorMsg.toString());
            holder.setData(false);
        }
        return holder;
    }
}
