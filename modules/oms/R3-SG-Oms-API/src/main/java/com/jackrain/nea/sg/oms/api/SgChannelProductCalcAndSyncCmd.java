package com.jackrain.nea.sg.oms.api;

import com.jackrain.nea.sys.Command;

/**
 * @author zhu lin yu
 * @since 2019/8/28
 * create at : 2019/8/28 13:56
 */
public interface SgChannelProductCalcAndSyncCmd extends Command {
}
