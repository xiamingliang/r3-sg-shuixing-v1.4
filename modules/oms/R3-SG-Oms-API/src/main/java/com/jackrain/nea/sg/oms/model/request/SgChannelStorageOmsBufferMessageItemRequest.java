package com.jackrain.nea.sg.oms.model.request;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author caopengflying
 * @time 2019/4/25 15:45
 */
@Data
public class SgChannelStorageOmsBufferMessageItemRequest {
    /** 单据编号 */
    private String billNo;
    /** 渠道ID */
    private Long cpCshopId;
    private Long cpCStoreId;
    private Long psCSkuId;
    private BigDecimal qtyChange;
}
