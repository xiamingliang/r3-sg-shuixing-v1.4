package com.jackrain.nea.sg.oms.model.request;

import com.jackrain.nea.sg.oms.model.table.SgBChannelProduct;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author zhu lin yu
 * @since 2019/5/14
 * create at : 2019/5/14 10:23
 */
@Data
public class SgChannelProductSaveRequest implements Serializable {
    private List<SgBChannelProduct> sgBChannelProductList;
}
