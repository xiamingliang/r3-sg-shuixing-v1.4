package com.jackrain.nea.sg.oms.model.tableExtend;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.sg.oms.model.table.SgBChannelStorage;
import com.jackrain.nea.sg.oms.model.table.SgBChannelStorageChangeFtp;
import org.springframework.beans.BeanUtils;

import java.util.Date;

public class SgBChannelStorageChangeFtpExtend extends SgBChannelStorageChangeFtp {
    /**
     * 新增处理字段
     * 'ID''平台店铺ID''平台店铺标题''期初可售数''变化数量''期末可售数'
     * '条码id''条码编码''商品id''商品编码''商品名称''创建时间'
     */
    /**
     * 查询类生成工具
     * @return
     */
    public QueryWrapper<SgBChannelStorageChangeFtp> createQueryWrapper(){
        QueryWrapper<SgBChannelStorageChangeFtp> queryWrapper = new QueryWrapper<SgBChannelStorageChangeFtp>();

        return queryWrapper;
    }

    /**
     * 构造SgBChannelStorage
     *
     * @param sgBChannelStorageChangeFtp
     * @param sgBChannelStorage
     */
    public static void complete(SgBChannelStorageChangeFtp sgBChannelStorageChangeFtp,
                                SgBChannelStorage sgBChannelStorage) {
        BeanUtils.copyProperties(sgBChannelStorage,sgBChannelStorageChangeFtp);
        sgBChannelStorageChangeFtp.setCreationdate(new Date());
        sgBChannelStorageChangeFtp.setModifieddate(null);
    }

}
