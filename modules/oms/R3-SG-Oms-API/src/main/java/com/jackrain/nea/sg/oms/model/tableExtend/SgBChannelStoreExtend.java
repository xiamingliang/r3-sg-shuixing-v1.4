package com.jackrain.nea.sg.oms.model.tableExtend;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.sg.oms.model.table.SgBChannelStore;
import lombok.Data;
import lombok.ToString;
import org.apache.commons.collections.CollectionUtils;

import java.util.List;

@Data
@ToString
public class SgBChannelStoreExtend extends SgBChannelStore {
    public static final String CPCSHOPID = "CP_C_SHOP_ID";
    public static final String CPCSTOREID = "CP_C_STORE_ID";
    public static final String STOCKSCALE = "STOCK_SCALE";
    /**
     * 辅助字段
     */
    private BaseExtend baseExtend;// 基础类

    private List<Long> cpCStoreIdList;

    /**
     * 查询类生成工具
     *
     * @return
     */
    public QueryWrapper<SgBChannelStore> createQueryWrapper() {
        QueryWrapper<SgBChannelStore> queryWrapper = new QueryWrapper<SgBChannelStore>();
        if (this.getCpCShopId() != null) {
            // 平台店铺
            queryWrapper.eq(CPCSHOPID, this.getCpCShopId());
        }
        if (CollectionUtils.isNotEmpty(cpCStoreIdList)) {
            // 逻辑仓
            queryWrapper.in(CPCSTOREID, cpCStoreIdList);
        }
        if (this.getCpCStoreId() != null) {
            // 逻辑仓
            queryWrapper.eq(CPCSTOREID, this.getCpCStoreId());
        }
        if (CollectionUtils.isNotEmpty(this.getCpCStoreIdList())){
            queryWrapper.in(CPCSTOREID, this.getCpCStoreIdList());
        }
        return queryWrapper;
    }

}
