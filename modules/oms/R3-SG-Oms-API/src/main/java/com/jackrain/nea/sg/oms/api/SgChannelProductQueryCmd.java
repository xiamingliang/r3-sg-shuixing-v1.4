package com.jackrain.nea.sg.oms.api;

import com.jackrain.nea.sg.oms.model.request.SgChannelProductQueryRequest;
import com.jackrain.nea.sg.oms.model.table.SgBChannelProduct;
import com.jackrain.nea.sys.domain.ValueHolderV14;

import java.util.HashMap;
import java.util.List;

/**
 * @author zhu lin yu
 * @since 2019/7/16
 * create at : 2019/7/16 19:45
 */
public interface SgChannelProductQueryCmd {

    /**
     * 渠道商品查询
     * @param sgChannelProductQueryRequest
     * @return
     */
    ValueHolderV14<List<SgBChannelProduct>> queryChannelProduct(SgChannelProductQueryRequest sgChannelProductQueryRequest);

    /**
     * 页面弹出框渠道商品导入查询
     * @param map
     * @return
     */
    List<HashMap<String, Object>> queryChannelProductByPageLayout(HashMap map);
}
