package com.jackrain.nea.sg.oms.model.result;

import com.jackrain.nea.sg.oms.model.request.SgChannelStorageOmsReduceItemRequest;
import com.jackrain.nea.sg.oms.model.table.SgBChannelStorageBuffer;
import lombok.Data;
import org.assertj.core.util.Lists;

import java.io.Serializable;
import java.util.List;

/**
 * @Description:
 * @Author: chenss
 * @Date: 2019/4/22 13:15
 */
@Data
public class SgChannelStorageOmsReduceResult implements Serializable {
    public SgChannelStorageOmsReduceResult() {
        sgBChannelStorageBufferList = Lists.newArrayList();
        successList = Lists.newArrayList();
    }

    /**
     * 渠道库存计算信息
     */
    List<SgBChannelStorageBuffer> sgBChannelStorageBufferList;

    /**
     * 执行成功的明细记录
     */
    List<SgChannelStorageOmsReduceItemRequest> successList;
}
