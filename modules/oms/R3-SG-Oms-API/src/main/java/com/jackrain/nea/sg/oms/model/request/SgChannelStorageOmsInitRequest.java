package com.jackrain.nea.sg.oms.model.request;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Description:
 * @Author: chenss
 * @Date: 2019/4/22 13:15
 */
@Data
public class SgChannelStorageOmsInitRequest implements Serializable {
    /**
     * 渠道ID list , sku list, 渠道条码ID list
     */
    private List<Long> cpCShopIdList;
    private List<Long> psCSkuIdList;// skuIdList
    private List<String> skuIdList;//渠道条码ID list

}
