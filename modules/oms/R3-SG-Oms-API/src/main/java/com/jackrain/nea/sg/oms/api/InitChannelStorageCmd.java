package com.jackrain.nea.sg.oms.api;

import com.jackrain.nea.sys.domain.ValueHolderV14;

public interface InitChannelStorageCmd {
    ValueHolderV14 initChannelStorage();
    ValueHolderV14 tbSyncChannelStorage();
    ValueHolderV14 jdSyncChannelStorage();
    ValueHolderV14 compensationAll();
}
