package com.jackrain.nea.sg.oms.model.tableExtend;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.sg.oms.model.table.SgBChannelDef;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class SgBChannelStorageDefExtend extends SgBChannelDef {
    public static final String CPCSHOPID = "CP_C_SHOP_ID";
    /**
     * 辅助字段
     */
    private BaseExtend baseExtend;// 基础类

    /**
     * 查询类生成工具
     *
     * @return
     */
    public QueryWrapper<SgBChannelDef> createQueryWrapper() {
        QueryWrapper<SgBChannelDef> queryWrapper = new QueryWrapper<SgBChannelDef>();
        if (this.getCpCShopId() != null) {
            // 平台店铺
            queryWrapper.eq(CPCSHOPID, this.getCpCShopId());
        }
        return queryWrapper;
    }


}
