package com.jackrain.nea.sg.oms.model.table;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.*;
import com.jackrain.nea.es.annotations.Document;
import com.jackrain.nea.es.annotations.Field;
import com.jackrain.nea.es.constans.FieldType;
import com.jackrain.nea.sys.domain.BaseModel;
import lombok.Data;

import java.math.BigDecimal;

@TableName(value = "sg_b_channel_store")
@Data
@Document(index = "sg_b_channel_store",type = "sg_b_channel_store")
public class SgBChannelStore extends BaseModel {
    @JSONField(name = "ID")
    @TableField(fill = FieldFill.INSERT)
    @TableId(value = "id", type = IdType.INPUT)
    @Field(type = FieldType.Long)
    private Long id;

    @JSONField(name = "CP_C_SHOP_ID")
    @Field(type = FieldType.Long)
    private Long cpCShopId;

    @JSONField(name = "CP_C_SHOP_TITLE")
    @Field(type = FieldType.Keyword)
    private String cpCShopTitle;

    @JSONField(name = "CP_C_STORE_ID")
    @Field(type = FieldType.Long)
    private Long cpCStoreId;

    @JSONField(name = "CP_C_STORE_ECODE")
    @Field(type = FieldType.Keyword)
    private String cpCStoreEcode;

    @JSONField(name = "CP_C_STORE_ENAME")
    @Field(type = FieldType.Keyword)
    private String cpCStoreEname;

    @JSONField(name = "PRIORITY")
    @Field(type = FieldType.Integer)
    private Integer priority;

    @JSONField(name = "STOCK_SCALE")
    @Field(type = FieldType.Double)
    private BigDecimal stockScale;

    @JSONField(name = "QTY_SEND")
    @Field(type = FieldType.Double)
    private BigDecimal qtySend;

    @JSONField(name = "LOW_STOCK")
    @Field(type = FieldType.Double)
    private BigDecimal lowStock;

    @JSONField(name = "IS_SEND")
    @Field(type = FieldType.Integer)
    private Integer isSend;

    @JSONField(name = "PRODUCT_GROUP")
    @Field(type = FieldType.Keyword)
    private String productGroup;

    @JSONField(name = "OWNERENAME")
    @Field(type = FieldType.Keyword)
    private String ownerename;

    @JSONField(name = "MODIFIERENAME")
    @Field(type = FieldType.Keyword)
    private String modifierename;
}