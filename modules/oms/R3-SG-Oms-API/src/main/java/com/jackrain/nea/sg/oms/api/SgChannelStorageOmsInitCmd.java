package com.jackrain.nea.sg.oms.api;

import com.jackrain.nea.sg.oms.model.request.SgChannelStorageOmsInitRequest;
import com.jackrain.nea.sg.oms.model.request.SgChannelStorageOmsManualSynchRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;

public interface SgChannelStorageOmsInitCmd {

    /**
     * 初始化渠道库存
     * 上线初期, 渠道库存为空时, 渠道商品表新增或更新数据时, 渠道和逻辑仓供货策略发生改变时, 或者人工重刷时
     * 参数  ： 渠道ID list , sku list, 渠道条码ID list
     */

    ValueHolderV14<Integer> initChannelStorage(SgChannelStorageOmsInitRequest request);

    /**
     * 手工同步渠道库存
     * @param request
     * @return
     */
    ValueHolderV14<Boolean> manualSynchChannelStorage(SgChannelStorageOmsManualSynchRequest request);

    /**
     * 手工计算渠道商品库存并同步
     * @param request
     * @return
     */
    ValueHolderV14<Boolean> manualCalcAndSyncChannelProduct(SgChannelStorageOmsManualSynchRequest request);
}

