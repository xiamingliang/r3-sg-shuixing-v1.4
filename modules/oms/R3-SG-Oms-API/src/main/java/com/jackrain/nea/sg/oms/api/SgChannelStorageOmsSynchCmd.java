package com.jackrain.nea.sg.oms.api;

import com.jackrain.nea.sg.oms.model.request.SgStorageOmsSynchronousRequest;
import com.jackrain.nea.sg.oms.model.result.SgStorageOmsSynchronousResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;

public interface SgChannelStorageOmsSynchCmd {

    /**
     *  渠道库存同步
     *
     * */
    ValueHolderV14<SgStorageOmsSynchronousResult> synchronousStock(SgStorageOmsSynchronousRequest request);


}

