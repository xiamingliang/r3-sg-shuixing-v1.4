package com.jackrain.nea.sg.oms.model.request;

import com.jackrain.nea.sg.basic.model.request.SgStoreWithPrioritySearchItemRequest;
import com.jackrain.nea.sg.basic.model.request.StStockPriorityRequest;
import com.jackrain.nea.web.face.User;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class SgStoreWithPrioritySearchRequest implements Serializable {

    /**
     * 来源单据表
     */
    private String table;
    /**
     * 单据编号
     */
    private String billNo;

    /**
     * 单据明细修改方式(1.全量 2.增量)
     */
    private Integer itemUpdateType;

    /**
     * 虚拟仓优先级列表
     */
    private List<StStockPriorityRequest> priorityList;

    /**
     * 寻仓明细信息
     */
    private List<SgStoreWithPrioritySearchItemRequest> itemList;

    //用户信息
    private User loginUser;

}
