package com.jackrain.nea.sg.oms.model.request;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Data
public class SgChannelStorageDefChangeRequest implements Serializable {

    private Long Id;//平台店铺ID

    private Long platForm;//平台类型

    private Long cpCCustomerId;//经销商

    private String cpCCustomerEcode;//经销商编码

    private String cpCCustomerEname;//经销商名称

    private String cpCShopTitle;//平台店铺标题

    private String shopType;//店铺类型

    private Integer channelType;//渠道类型

    private String sellerNick;//卖家昵称

    private BigDecimal stockScale;//整体库存比例

    private BigDecimal lowStock;//安全库存数

    private Integer isSyncStock;//是否同步库存

    private Boolean isAdd;//是否新增,供俊磊使用

    private List<Long> cpCStoreIdList;//逻辑仓ID集合




    /**
     * 参数校验
     */
    public Boolean check() {
        if (Id == null) {
            return true;
        }
        return false;
    }


}
