package com.jackrain.nea.sg.oms.model.table;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.*;
import com.jackrain.nea.es.annotations.Document;
import com.jackrain.nea.es.annotations.Field;
import com.jackrain.nea.es.constans.FieldType;
import com.jackrain.nea.sys.domain.BaseModel;
import lombok.Data;

import java.math.BigDecimal;

@TableName(value = "sg_b_channel_def")
@Data
@Document(index = "sg_b_channel_def",type = "sg_b_channel_def")
public class SgBChannelDef extends BaseModel {
    @JSONField(name = "ID")
    @TableField(fill = FieldFill.INSERT)
    @TableId(value = "id", type = IdType.INPUT)
    @Field(type = FieldType.Long)
    private Long id;

    @JSONField(name = "CP_C_PLATFORM_ID")
    @Field(type = FieldType.Text)
    private String cpCPlatformId;

    @JSONField(name = "CP_C_CUSTOMER_ID")
    @Field(type = FieldType.Long)
    private Long cpCCustomerId;

    @JSONField(name = "CP_C_CUSTOMER_ECODE")
    @Field(type = FieldType.Keyword)
    private String cpCCustomerEcode;

    @JSONField(name = "CP_C_CUSTOMER_ENAME")
    @Field(type = FieldType.Keyword)
    private String cpCCustomerEname;

    @JSONField(name = "CP_C_SHOP_ID")
    @Field(type = FieldType.Long)
    private Long cpCShopId;

    @JSONField(name = "CP_C_SHOP_TITLE")
    @Field(type = FieldType.Keyword)
    private String cpCShopTitle;

    @JSONField(name = "CP_C_SHOP_TYPE")
    @Field(type = FieldType.Keyword)
    private String cpCShopType;

    @JSONField(name = "CP_C_SHOP_CHANNEL_TYPE")
    @Field(type = FieldType.Integer)
    private Integer cpCShopChannelType;

    @JSONField(name = "CP_C_SHOP_SELLER_NICK")
    @Field(type = FieldType.Keyword)
    private String cpCShopSellerNick;

    @JSONField(name = "STOCK_SCALE")
    @Field(type = FieldType.Double)
    private BigDecimal stockScale;

    @JSONField(name = "QTY_SAFE")
    @Field(type = FieldType.Double)
    private BigDecimal qtySafe;

    @JSONField(name = "IS_SYN")
    @Field(type = FieldType.Integer)
    private Integer isSyn;

    @JSONField(name = "OWNERENAME")
    @Field(type = FieldType.Keyword)
    private String ownerename;

    @JSONField(name = "MODIFIERENAME")
    @Field(type = FieldType.Keyword)
    private String modifierename;

    @JSONField(name = "CP_C_PLATFORM_ECODE")
    @Field(type = FieldType.Keyword)
    private String cpCPlatformEcode;

    @JSONField(name = "CP_C_PLATFORM_ENAME")
    @Field(type = FieldType.Keyword)
    private String cpCPlatformEname;
}