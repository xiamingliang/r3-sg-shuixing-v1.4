package com.jackrain.nea.sg.oms.model.request;

import com.alibaba.fastjson.annotation.JSONField;
import com.jackrain.nea.sg.oms.model.table.SgBChannelManualSyn;
import com.jackrain.nea.sg.oms.model.table.SgBChannelManualSynItem;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Author chenjinjun
 * @Description 手工导入店铺库存
 * @Date  2019-10-21
**/
@Data
public class SgBChannelManualSynRequest implements Serializable {

    @JSONField(name = "SG_B_CHANNEL_MANUAL_SYN")
    private SgBChannelManualSyn sgBChannelManualSyn;

    @JSONField(name = "SG_B_CHANNEL_MANUAL_SYN_ITEM")
    private List<SgBChannelManualSynItem> sgBChannelManualSynItemList;
}
