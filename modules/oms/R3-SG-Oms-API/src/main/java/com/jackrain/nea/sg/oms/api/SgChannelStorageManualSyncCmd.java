package com.jackrain.nea.sg.oms.api;

import com.jackrain.nea.sys.Command;

/**
 * @author zhu lin yu
 * @since 2019-10-16
 * create at : 2019-10-16 14:56
 */
public interface SgChannelStorageManualSyncCmd extends Command {
}
