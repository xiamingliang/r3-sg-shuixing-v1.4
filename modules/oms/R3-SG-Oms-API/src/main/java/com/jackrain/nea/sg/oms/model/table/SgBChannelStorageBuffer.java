package com.jackrain.nea.sg.oms.model.table;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.*;
import com.jackrain.nea.es.annotations.Document;
import com.jackrain.nea.es.annotations.Field;
import com.jackrain.nea.es.constans.FieldType;
import com.jackrain.nea.sys.domain.BaseModel;
import lombok.Data;

@TableName(value = "sg_b_channel_storage_buffer")
@Data
@Document(index = "sg_b_channel_storage_buffer",type = "sg_b_channel_storage_buffer")
public class SgBChannelStorageBuffer extends BaseModel {
    @JSONField(name = "ID")
    @TableField(fill = FieldFill.INSERT)
    @TableId(value = "id", type = IdType.INPUT)
    @Field(type = FieldType.Long)
    private Long id;

    @JSONField(name = "CP_C_SHOP_ID")
    @Field(type = FieldType.Long)
    private Long cpCShopId;

    @JSONField(name = "CP_C_SHOP_TITLE")
    @Field(type = FieldType.Keyword)
    private String cpCShopTitle;

    @JSONField(name = "SKU_ID")
    @Field(type = FieldType.Keyword)
    private String skuId;

    @JSONField(name = "DEAL_STATUS")
    @Field(type = FieldType.Integer)
    private Integer dealStatus;

    @JSONField(name = "SOURCE_CP_C_SHOP_ID")
    @Field(type = FieldType.Long)
    private Long sourceCpCShopId;

    @JSONField(name = "OWNERENAME")
    @Field(type = FieldType.Keyword)
    private String ownerename;

    @JSONField(name = "MODIFIERENAME")
    @Field(type = FieldType.Keyword)
    private String modifierename;

    @JSONField(name = "SOURCE_NO")
    @Field(type = FieldType.Keyword)
    private String sourceNo;

    @JSONField(name = "WARE_TYPE")
    @Field(type = FieldType.Integer)
    private Integer wareType;
}