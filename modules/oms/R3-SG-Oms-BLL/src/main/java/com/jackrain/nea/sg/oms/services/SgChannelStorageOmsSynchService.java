package com.jackrain.nea.sg.oms.services;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.ac.sc.basic.model.request.DistributionQueryRequest;
import com.jackrain.nea.ac.sc.core.api.AcDistributionQueryCmd;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.cpext.model.table.CpShop;
import com.jackrain.nea.data.basic.model.request.ShopQueryRequest;
import com.jackrain.nea.data.basic.services.BasicCpQueryService;
import com.jackrain.nea.es.util.ElasticSearchUtil;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.ip.api.StockSyncCmd;
import com.jackrain.nea.ip.api.others.StandplatSyncStockCmd;
import com.jackrain.nea.ip.model.FullQtyDataDetailIpModel;
import com.jackrain.nea.ip.model.StockSyncModel;
import com.jackrain.nea.ip.model.others.RPCSyncStockModel;
import com.jackrain.nea.ip.model.result.FullQuantityResult;
import com.jackrain.nea.ip.model.result.StockSyncResult;
import com.jackrain.nea.model.util.ModelUtil;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.utils.StorageUtils;
import com.jackrain.nea.sg.oms.common.OmsConstantsIF;
import com.jackrain.nea.sg.oms.common.OmsResultCode;
import com.jackrain.nea.sg.oms.mapper.SgBChannelDefMapper;
import com.jackrain.nea.sg.oms.mapper.SgBChannelStorageMapper;
import com.jackrain.nea.sg.oms.mapper.SgBChannelSynstockQMapper;
import com.jackrain.nea.sg.oms.model.request.SgChannelStorageOmsInitRequest;
import com.jackrain.nea.sg.oms.model.request.SgChannelStorgeOmsCallbackRequest;
import com.jackrain.nea.sg.oms.model.request.SgStorageOmsSynchronousRequest;
import com.jackrain.nea.sg.oms.model.result.SgStorageOmsSynchronousResult;
import com.jackrain.nea.sg.oms.model.table.SgBChannelDef;
import com.jackrain.nea.sg.oms.model.table.SgBChannelStorage;
import com.jackrain.nea.sg.oms.model.table.SgBChannelSynstockQ;
import com.jackrain.nea.sg.oms.model.tableExtend.SgBChannelDefExtend;
import com.jackrain.nea.sg.oms.model.tableExtend.SgBChannelStorageDefExtend;
import com.jackrain.nea.sg.oms.model.tableExtend.SgBChannelSynstockQExtend;
import com.jackrain.nea.sg.out.common.SgOutConstantsIF;
import com.jackrain.nea.st.api.SellOwngoodsQueryCmd;
import com.jackrain.nea.st.model.request.SellOwngoodsQueryRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.dubbo.config.annotation.Reference;
import org.assertj.core.util.Lists;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
@Slf4j
public class SgChannelStorageOmsSynchService {

    @Autowired
    private SgBChannelDefMapper sgBChannelDefMapper;
    @Autowired
    private SgBChannelSynstockQMapper sgBChannelSynstockQMapper;
    @Autowired
    private SgBChannelStorageMapper sgBChannelStorageMapper;

    @Autowired
    private SgChannelStorageOmsInitService sgChannelStorageOmsInitService;

    @Autowired
    private BasicCpQueryService basicCpQueryService;

    @Autowired
    private SyncChannelStorageAllService syncChannelStorageAllService;

    @Reference(protocol = "dubbo", validation = "true", version = "1.0", group = "st")
    private SellOwngoodsQueryCmd sellOwngoodsQueryCmd;

    @Reference(protocol = "dubbo", validation = "true", version = "1.0", group = "ac-sc")
    private AcDistributionQueryCmd acDistributionQueryCmd;

    //数据超过200进行分批处理,分批数量
    private static final double MAXNUM = 200;

    /**
     * ES查询 店铺锁库策略设置 枚举
     */
    //开始时间
    private static final String LOCK_BTIME = "LOCK_BTIME";
    //结束时间
    private static final String LOCK_ETIME = "LOCK_ETIME";
    //店铺锁库策略设置主表
    private static final String ST_C_LOCK_STOCK_STRATEGY = "st_c_lock_stock_strategy";
    //店铺锁库策略设置从表
    private static final String ST_C_LOCK_STOCK_STRATEGY_ITEM = "st_c_lock_stock_strategy_item";

    /**
     * ES 店铺锁库条码特殊设置 枚举
     */
    //渠道条码
    private static final String PT_SKU_ID = "PT_SKU_ID";
    //店铺锁库条码特殊设置主表
    private static final String ST_C_LOCK_SKU_STRATEGY = "st_c_lock_sku_strategy";
    //店铺锁库条码特殊设置从表
    private static final String ST_C_LOCK_SKU_STRATEGY_ITEM = "st_c_lock_sku_strategy_item";

    /**
     * apiType 1 批量 2 单例
     */
    public static final String SINGLE = "2";
    public static final String BATCH = "1";

    /**
     * 库存同步队列表
     */
    private static final Integer INIT = 0;//未同步
    public static final Integer INSYNC = 1;//已同步
    public static final Integer SYNCSUCCESSS = 2;//同步成功
    public static final Integer SYNCFAIL = 3;//同步失败
    public static final Integer NOSYNC = 4;//无需同步

    /**
     * 判断是否需要同步库存
     *
     * @param //渠道id private Long cpCShopId;
     *               //渠道条码id
     *               private String skuId;
     * @return ValueHolderV14
     */

    public int whetherSynchronization(Long cpCShopId, String skuId) {
        if (log.isDebugEnabled()) {
            log.debug("Start SgChannelStorageOmsSynchService.whetherSynchronization[params" + "cpCShopId =" + cpCShopId + "::skuId" + skuId + "]");
        }
        // 根据渠道ID查询【渠道信息表】根据【是否同步库存】字段, 判读值
        SgBChannelDefExtend sgBChannelDefExtend = new SgBChannelDefExtend();
        sgBChannelDefExtend.setCpCShopId(cpCShopId);
        QueryWrapper<SgBChannelDef> queryWrapper = sgBChannelDefExtend.createQueryWrapper();
        SgBChannelDef sgBChannelDef = sgBChannelDefMapper.selectOne(queryWrapper);
        if (sgBChannelDef == null) {
            return OmsConstantsIF.IS_SYNC_FLAG_NO_CHANNEL_INFO;
        } else if (sgBChannelDef.getIsSyn() != null && OmsConstantsIF.YES == sgBChannelDef.getIsSyn()) {
            // 值为‘是‘, 则根据 [渠道ID+当前时间] 查询es【店铺锁库策略设置】, 是否有生效的锁库策略
            return searchES(cpCShopId, skuId);
        } else {
            // 值为‘否’, 则返回结果为否(不同步库存)
            return OmsConstantsIF.IS_SYNC_FLAG_CHANNEL_NOSYNC;
        }
    }

    /**
     * 渠道库存同步
     */
    @Transactional(rollbackFor = Exception.class)
    public ValueHolderV14<SgStorageOmsSynchronousResult> synchronousStock(SgStorageOmsSynchronousRequest request) {
        ValueHolderV14<SgStorageOmsSynchronousResult> valueHolderV14 = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        if (log.isDebugEnabled()) {
            log.debug("Start SgChannelStorageOmsSynchService.synchronousStock[params" + request.toString() + "]");
        }
        //1.增量 2.全量 3.手工 4.全量补偿
        switch (request.getSyncType()) {
            case 1:
                // 增量【渠道库存表】行记录 参数校验
                SgBChannelStorage sgBChannelStorage = request.getSgBChannelStorage();
                if (sgBChannelStorage.getCpCShopId() != null && sgBChannelStorage.getSkuId() != null && sgBChannelStorage.getQty() != null) {
                    return synchronousStock1(request);
                } else {
                    valueHolderV14.setCode(ResultCode.FAIL);
                    valueHolderV14.setMessage("参数校验不通过!");
                    return valueHolderV14;
                }
            case 2:
                // 全量 渠道 ID, 渠道条码ID 参数校验
                if (request.getCpCShopId() != null) {
                    return syncChannelStorageAllService.synchronousStock2(request, false);
                } else {
                    valueHolderV14.setCode(ResultCode.FAIL);
                    valueHolderV14.setMessage("参数校验不通过!");
                    return valueHolderV14;
                }
            case 3:
                // 手工 参数校验
                if (request.getCpCShopId() != null) {
                    return syncChannelStorageAllService.synchronousStock2(request, false);
                } else {
                    valueHolderV14.setCode(ResultCode.FAIL);
                    valueHolderV14.setMessage("参数校验不通过!");
                    return valueHolderV14;
                }
            case 4:
                // 全量补偿
                return compensationAll();
            default:
                valueHolderV14.setCode(ResultCode.FAIL);
                valueHolderV14.setMessage("不存在该同步类型!");
                return valueHolderV14;
        }
    }

    /**
     * 增量同步库存
     *
     * @param request
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public ValueHolderV14 synchronousStock1(SgStorageOmsSynchronousRequest request) {

        String type = null;
        if (log.isDebugEnabled()) {
            log.debug("Start SgChannelStorageOmsSynchService.synchronousStock1[params" + request.toString() + "]");
        }
        ValueHolderV14 holderV = new ValueHolderV14<>(ResultCode.SUCCESS, "增量同步成功");
        SgBChannelStorage sgBChannelStorage = request.getSgBChannelStorage();
        ShopQueryRequest shopQueryRequest = new ShopQueryRequest();
        shopQueryRequest.setShopIds(Lists.newArrayList(sgBChannelStorage.getCpCShopId()));
        try {
            log.info(this.getClass().getName() + " 开始调用店铺查询接口,渠道条码id:{}", sgBChannelStorage.getSkuId());
            HashMap<Long, CpShop> shopInfo = basicCpQueryService.getShopInfo(shopQueryRequest);
            CpShop cpShop = shopInfo.get(sgBChannelStorage.getCpCShopId());
            if (cpShop == null || StringUtils.isEmpty(cpShop.getChannelType())) {
                log.error(this.getClass().getName() + " 未查到渠道类型,渠道条码id:{}", sgBChannelStorage.getSkuId());
                holderV.setCode(ResultCode.FAIL);
                holderV.setMessage("渠道条码id为" + sgBChannelStorage.getSkuId() + "未查到渠道类型");
                return holderV;
            } else {
                if (OmsConstantsIF.SHOP_CHANNEL_TYPE_DIRECTLY.equals(cpShop.getChannelType())) {
                    type = OmsConstantsIF.SHOP_CHANNEL_TYPE_DIRECTLY;
                } else if (OmsConstantsIF.SHOP_CHANNEL_TYPE_DISTRIBUTION.equals(cpShop.getChannelType())) {

                    //调用经销商自有商品策略服务,判断是否自有商品，不是则调用代销分销策略服务
                    type = OmsConstantsIF.SHOP_CHANNEL_TYPE_DISTRIBUTION;
                    SellOwngoodsQueryRequest sellOwngoodsQueryRequest = new SellOwngoodsQueryRequest();
                    sellOwngoodsQueryRequest.setEffectiveDate(new Date());
                    sellOwngoodsQueryRequest.setCpCShopId(sgBChannelStorage.getCpCShopId());
                    sellOwngoodsQueryRequest.setSkuIdList(Lists.newArrayList(sgBChannelStorage.getPsCSkuId()));
                    ValueHolderV14<List<Long>> listValueHolderV14 = sellOwngoodsQueryCmd.selectSkuBySellOwngoods(sellOwngoodsQueryRequest);

                    log.info(this.getClass().getName() + " 调用selectSkuBySellOwngoods方法返回值:{}", JSONObject.toJSONString(listValueHolderV14));
                    if (ResultCode.FAIL == listValueHolderV14.getCode()) {
                        holderV.setCode(ResultCode.FAIL);
                        holderV.setMessage(listValueHolderV14.getMessage());
                        log.error(this.getClass().getName() + " 调用经销商自有商品策略服务失败,条码id:{},失败信息:{}"
                                , sgBChannelStorage.getSkuId(), listValueHolderV14.getMessage());
                        return holderV;
                    } else {
                        List<Long> data = listValueHolderV14.getData();
                        if (CollectionUtils.isEmpty(data)) {

                            //调用代销分销策略服务
                            DistributionQueryRequest distributionQueryRequest = new DistributionQueryRequest();
                            distributionQueryRequest.setEffectiveDate(new Date());
                            distributionQueryRequest.setCpCShopId(sgBChannelStorage.getCpCShopId());
                            distributionQueryRequest.setSkuIdList(Lists.newArrayList(sgBChannelStorage.getPsCSkuId()));
                            ValueHolderV14<List<Long>> holderV14 = acDistributionQueryCmd.selectSkuByDistribution(distributionQueryRequest);

                            log.info(this.getClass().getName() + " 调用selectSkuByDistribution方法返回值:{}", JSONObject.toJSONString(holderV14));
                            if (ResultCode.FAIL == listValueHolderV14.getCode()) {
                                holderV.setCode(ResultCode.FAIL);
                                holderV.setMessage(listValueHolderV14.getMessage());
                                log.error(this.getClass().getName() + " 调用代销分销策略服务失败,条码id:{},失败信息:{}"
                                        , sgBChannelStorage.getSkuId(), listValueHolderV14.getMessage());
                                return holderV;
                            } else {
                                List<Long> result = holderV14.getData();
                                if (CollectionUtils.isEmpty(result)) {
                                    type = OmsConstantsIF.SHOP_CHANNEL_TYPE_NO_DISTRIBUTION_PRO;
                                }
                            }
                        } else {
                            type = OmsConstantsIF.SHOP_CHANNEL_TYPE_OWN_PRO;
                        }
                    }
                } else {
                    log.error(this.getClass().getName() + " 未知的渠道类型,渠道条码id:{}", sgBChannelStorage.getSkuId());
                    holderV.setCode(ResultCode.FAIL);
                    holderV.setMessage("未知的渠道类型,渠道条码id为" + sgBChannelStorage.getSkuId() + ",渠道类型为" + cpShop.getChannelType());
                    return holderV;
                }
            }
        } catch (Exception e) {
            log.error(this.getClass().getName() + " 判断直营分销策略异常,条码id:{}", sgBChannelStorage.getSkuId(), e);
            throw new NDSException(e);
        }

        /**
         * 根据 渠道ID+渠道条码ID 调用判断渠道条码ID是否需要同步服务, 根据返回的结果判断是否需要同步库存
         */
        int isTrue = whetherSynchronization(sgBChannelStorage.getCpCShopId(), sgBChannelStorage.getSkuId());

        /**
         * 需要同步, 则构造数据, 插入到【库存同步队列表】中
         * 调用RPC同步服务(增量)
         */
        SgBChannelSynstockQ sgBChannelSynstockQ = new SgBChannelSynstockQ();
        BeanUtils.copyProperties(request.getSgBChannelStorage(), sgBChannelSynstockQ);
        sgBChannelSynstockQ.setId(ModelUtil.getSequence(SgConstants.SG_B_CHANNEL_SYNSTOCK_Q));
        sgBChannelSynstockQ.setBatchno((System.currentTimeMillis() + "" + (int) (Math.random() * 9000 + 1000)));//批次 :拿当前时间到微秒 然后加4位随机数
        sgBChannelSynstockQ.setQtyStorage(request.getSgBChannelStorage().getQty());//同步库存数
        sgBChannelSynstockQ.setSellerNick(request.getSgBChannelStorage().getCpCShopSellerNick());//卖家昵称
        sgBChannelSynstockQ.setCpCPlatformId(request.getSgBChannelStorage().getCpCPlatformId().longValue());//平台
        sgBChannelSynstockQ.setCreationdate(new Date());
        sgBChannelSynstockQ.setModifieddate(null);

        if (isTrue > 0) {
            if (OmsConstantsIF.SHOP_CHANNEL_TYPE_NO_DISTRIBUTION_PRO.equals(type)) {
                sgBChannelSynstockQ.setSynStatus(SgChannelStorageOmsSynchService.NOSYNC);
                sgBChannelSynstockQ.setErrorMsg("分销店铺的非分销代销商品");
            } else if (OmsConstantsIF.SHOP_CHANNEL_TYPE_OWN_PRO.equals(type)) {
                sgBChannelSynstockQ.setSynStatus(SgChannelStorageOmsSynchService.NOSYNC);
                sgBChannelSynstockQ.setErrorMsg("分销店铺的经销商自有商品");
            } else {
                sgBChannelSynstockQ.setSynStatus(INIT);
            }
        } else {
            sgBChannelSynstockQ.setSynStatus(SgChannelStorageOmsSynchService.NOSYNC);
            if (OmsConstantsIF.IS_SYNC_FLAG_CHANNEL_NOSYNC == isTrue) {
                sgBChannelSynstockQ.setErrorMsg("无需同步,该渠道设置为不同步");
            } else if (OmsConstantsIF.IS_SYNC_FLAG_LOCK_SKU == isTrue) {
                sgBChannelSynstockQ.setErrorMsg("无需同步,请查看锁条码策略");
            } else if (OmsConstantsIF.IS_SYNC_FLAG_LOCK_STORE == isTrue) {
                sgBChannelSynstockQ.setErrorMsg("无需同步,请查看锁库策略");
            } else if (OmsConstantsIF.IS_SYNC_FLAG_NO_CHANNEL_INFO == isTrue) {
                sgBChannelSynstockQ.setErrorMsg("无需同步,无该渠道信息");
            }
        }
        sgBChannelSynstockQ.setSourceNo(request.getSourceNo());
        sgBChannelSynstockQ.setAdOrgId(27L);//默认赋值
        sgBChannelSynstockQ.setAdClientId(37L);//默认赋值
        int i = sgBChannelSynstockQMapper.insert(sgBChannelSynstockQ);
        if (i <= 0) {
            holderV.setCode(ResultCode.FAIL);
            holderV.setMessage("同步任务插入失败!");
            return holderV;
        }

        String cpCPlatformId = sgBChannelStorage.getCpCPlatformId().toString();
        if (INIT.equals(sgBChannelSynstockQ.getSynStatus())) {
            if (SgOutConstantsIF.PLATFORMTYPE_PINDUODUO.equals(cpCPlatformId)
                    || SgOutConstantsIF.PLATFORMTYPE_SUNING.equals(cpCPlatformId)
                    || SgOutConstantsIF.PLATFORMTYPE_WUXIANGYUN.equals(cpCPlatformId)) {
                //调用拼多多苏宁接口
                holderV = stockSyncRPCPddAndSn(Lists.newArrayList(sgBChannelSynstockQ));
            } else if (SgOutConstantsIF.PLATFORMTYPE_TAOBAO.equals(cpCPlatformId)
                    || SgOutConstantsIF.PLATFORMTYPE_JINGDONG.equals(cpCPlatformId)
                    || SgOutConstantsIF.PLATFORMTYPE_TAOBAO_FENXIAO.equals(cpCPlatformId)
                    || SgOutConstantsIF.PLATFORMTYPE_TAOBAO_JINGXIAO.equals(cpCPlatformId)) {
                //调用淘宝天猫接口
                holderV = stockSyncRPC(request, sgBChannelSynstockQ);
            }
        }

        return holderV;
    }

    /**
     * 全量补偿
     */
    public ValueHolderV14<SgStorageOmsSynchronousResult> compensationAll() {
        /**
         * a. 查询【库存同步队列表】所有同步失败的行数据
         * b. 根据 渠道ID+渠道条码ID 查询最新的【渠道库存表】中的行数据
         * c. 根据 渠道ID+渠道条码ID+可售库存 构造数据,
         *     更新【库存同步队列表】, 同步状态改为未同步, 错误信息为 空,可售数量为最新数量, 时间更新为当前时间
         */
        log.info(this.getClass().getName() + " 开始全量补偿！");
        ValueHolderV14<SgStorageOmsSynchronousResult> valueHolderV14 = new ValueHolderV14<>(ResultCode.SUCCESS, "全量补偿成功！");
        // 获取执行失败的记录
        SgBChannelSynstockQExtend sgBChannelSynstockQExtend = new SgBChannelSynstockQExtend();
        sgBChannelSynstockQExtend.setSynStatus(SYNCFAIL);
        QueryWrapper<SgBChannelSynstockQ> synstockQueryWrapper = sgBChannelSynstockQExtend.createQueryWrapper();
        long current = System.currentTimeMillis();//当前时间毫秒数
        long zero = current / (1000 * 3600 * 24) * (1000 * 3600 * 24) - TimeZone.getDefault().getRawOffset();//今天零点零分零秒的毫秒数
        long twelve = zero + 24 * 60 * 60 * 1000 - 1;//今天23点59分59秒的毫秒数
        Date z = new Date(zero);
        Date t = new Date(twelve);
        synstockQueryWrapper.between("creationdate", z, t);
        List<SgBChannelSynstockQ> sgBChannelSynstockQList = sgBChannelSynstockQMapper.selectList(synstockQueryWrapper);
        log.info(this.getClass().getName() + " 全量补偿查询到的失败的数据条数为：" + sgBChannelSynstockQList.size() + "具体明细为" + JSONObject.toJSONString(sgBChannelSynstockQList));

        //查询全部需要的渠道库存
        List<Long> cpCShopIdList = new ArrayList<>();
        List<String> skuIdList = new ArrayList<>();
        for (SgBChannelSynstockQ sgBChannelSynstockQ : sgBChannelSynstockQList) {
            cpCShopIdList.add(sgBChannelSynstockQ.getCpCShopId());
            skuIdList.add(sgBChannelSynstockQ.getSkuId());
        }
        QueryWrapper<SgBChannelStorage> queryWrapper = new QueryWrapper();
        queryWrapper.in("cp_c_shop_id", cpCShopIdList);
        queryWrapper.in("sku_id", skuIdList);
        List<SgBChannelStorage> sgBChannelStorageList = sgBChannelStorageMapper.selectList(queryWrapper);
        log.info(this.getClass().getName() + " 全量补偿查询到的渠道库存条数为：" + sgBChannelStorageList.size() + "具体明细为" + JSONObject.toJSONString(sgBChannelStorageList));
        if (CollectionUtils.isNotEmpty(sgBChannelStorageList)) {
            Map<String, SgBChannelStorage> storageMap = sgBChannelStorageList.stream()
                    .collect(Collectors.toMap(s -> s.getCpCShopId() + "：" + s.getSkuId(), Function.identity()));
            for (SgBChannelSynstockQ sgBChannelSynstockQ : sgBChannelSynstockQList) {
                SgBChannelStorage sgBChannelStorage = storageMap.get(sgBChannelSynstockQ.getCpCShopId() + ":" + sgBChannelSynstockQ.getSkuId());
                if (sgBChannelStorage != null) {
                    sgBChannelSynstockQ.setQtyStorage(sgBChannelStorage.getQty());
                }
                sgBChannelSynstockQ.setSynStatus(INSYNC);
                sgBChannelSynstockQ.setErrorMsg(null);
                sgBChannelSynstockQ.setModifieddate(new Date());
            }
        }

        //平台为拼多多和苏宁
        List<SgBChannelSynstockQ> sgBChannelSynstockQSForPDDandSN = sgBChannelSynstockQList.stream().filter(x -> SgOutConstantsIF.PLATFORMTYPE_PINDUODUO.equals(x.getCpCPlatformId().toString())
                || SgOutConstantsIF.PLATFORMTYPE_SUNING.equals(x.getCpCPlatformId().toString())).collect(Collectors.toList());
        log.info(this.getClass().getName() + " 全量补偿拼多多和苏宁明细:{}", JSONObject.toJSONString(sgBChannelSynstockQSForPDDandSN));

        //平台为淘宝和京东
        List<SgBChannelSynstockQ> sgBChannelSynstockQSForTBandJD = sgBChannelSynstockQList.stream().filter(x -> SgOutConstantsIF.PLATFORMTYPE_TAOBAO.equals(x.getCpCPlatformId().toString())
                || SgOutConstantsIF.PLATFORMTYPE_JINGDONG.equals(x.getCpCPlatformId().toString())).collect(Collectors.toList());
        log.info(this.getClass().getName() + " 全量补偿淘宝和京东明细:{}", JSONObject.toJSONString(sgBChannelSynstockQSForTBandJD));

        //更新状态值
        List<List<SgBChannelSynstockQ>> baseModelPageList = StorageUtils.getBaseModelPageList(sgBChannelSynstockQSForTBandJD, 500);
        int i = 1;
        for (List<SgBChannelSynstockQ> sgBChannelSynstockQS : baseModelPageList) {
            log.info(this.getClass().getName() + " 全量补偿第" + i + "次同步的条数为：" + sgBChannelSynstockQS.size() + "具体明细为" + JSONObject.toJSONString(sgBChannelSynstockQS));
            sgBChannelSynstockQMapper.batchUpdate(sgBChannelSynstockQS);
            syncChannelStorageAllService.stockSyncRPCAll(sgBChannelSynstockQS, true, OmsConstantsIF.AUTO_SNYC);
        }

        if (CollectionUtils.isNotEmpty(sgBChannelSynstockQSForPDDandSN)) {
            log.info(this.getClass().getName() + " 拼多多和苏宁全量补偿明细:{}", JSONObject.toJSONString(sgBChannelSynstockQSForPDDandSN));
            sgBChannelSynstockQMapper.batchUpdate(sgBChannelSynstockQSForPDDandSN);
            stockSyncRPCPddAndSn(sgBChannelSynstockQSForPDDandSN);
        }
        return valueHolderV14;

    }

    /**
     * 增量调用RPC服务
     *
     * @param request
     * @return
     */
    public ValueHolderV14<SgStorageOmsSynchronousResult> stockSyncRPC(SgStorageOmsSynchronousRequest request, SgBChannelSynstockQ sgBChannelSynstockQ) {

        Integer cpCPlatformId = request.getSgBChannelStorage().getCpCPlatformId();
        StockSyncModel stockSyncModel = new StockSyncModel();
        stockSyncModel.setPlatform(cpCPlatformId);//平台类型
        stockSyncModel.setGbCode("1");//国标码，唯品会使用  产品[王波]确认过
        StockSyncModel.StockSyncData stockSyncData = new StockSyncModel.StockSyncData();
        stockSyncData.setId(sgBChannelSynstockQ.getId());
        stockSyncData.setCreationDate(System.currentTimeMillis());//创建时间
        stockSyncData.setPlatform(request.getSgBChannelStorage().getCpCPlatformId());//平台类型
        stockSyncData.setSellerNick(request.getSgBChannelStorage().getCpCShopSellerNick());//卖家昵称
        stockSyncData.setSyncNum(request.getSgBChannelStorage().getQty() == null ? null : request.getSgBChannelStorage().getQty().longValue());//同步库存数
        stockSyncData.setNumIid(request.getSgBChannelStorage().getNumiid() == null ? null : Long.valueOf(request.getSgBChannelStorage().getNumiid()));//平台商品ID
        stockSyncData.setProperties(sgBChannelSynstockQ.getProperties());//属性
        stockSyncData.setOutSkuId(request.getSgBChannelStorage().getSkuId()); //平台SKUID
        stockSyncData.setSku(request.getSgBChannelStorage().getPsCSkuEcode());//sku

        if (SgOutConstantsIF.PLATFORMTYPE_TAOBAO_FENXIAO.equals(cpCPlatformId.toString())
                || SgOutConstantsIF.PLATFORMTYPE_TAOBAO_JINGXIAO.equals(cpCPlatformId.toString())) {
            stockSyncData.setProperties(request.getSkuSpec());
        }
        stockSyncModel.setSyncDataList(Lists.newArrayList(stockSyncData));
        try {
            //调用RPC服务
            StockSyncCmd stockSyncCmd = (StockSyncCmd) ReferenceUtil.refer(
                    ApplicationContextHandle.getApplicationContext(),
                    StockSyncCmd.class.getName(),
                    SgBChannelSynstockQExtend.GROUPNAME, SgBChannelSynstockQExtend.VERSION);
            log.info(">>>>>>========[dsh]增量调用RPC服务,参数为:" + stockSyncModel);
            ValueHolderV14<StockSyncResult> holderV14 = stockSyncCmd.syncStock(stockSyncModel);
            log.info(">>>>>>========[dsh]增量调用RPC服务,返回值为:" + holderV14);

            StockSyncResult stockSyncResult = holderV14.getData();
            List<StockSyncResult.SyncResultData> syncDataList = stockSyncResult.getSyncDataList();
            if (CollectionUtils.isNotEmpty(syncDataList)) {
                StockSyncResult.SyncResultData syncResultData = syncDataList.get(0);
                sgBChannelSynstockQ.setMessageKey(syncResultData.getMsgId());
                sgBChannelSynstockQ.setSynStatus(INSYNC);
                if (OmsResultCode.isSuccess(holderV14)) {
                    sgBChannelSynstockQ.setSynStatus(INSYNC);
                } else if ("false".equals(syncResultData.getIsSend())) {
                    sgBChannelSynstockQ.setSynStatus(SYNCFAIL);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            sgBChannelSynstockQ.setSynStatus(SYNCFAIL);
            return new ValueHolderV14<>(ResultCode.FAIL, "调用增量RPC同步服务失败!");
        } finally {
            sgBChannelSynstockQ.setModifieddate(new Date());
            sgBChannelSynstockQMapper.updateById(sgBChannelSynstockQ);
        }
        return new ValueHolderV14<>(ResultCode.SUCCESS, "增量同步成功!");
    }

    /**
     * 调用拼多多，苏宁,舞象云接口
     */
    public ValueHolderV14 stockSyncRPCPddAndSn(List<SgBChannelSynstockQ> sgBChannelSynstockQList) {

        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + " 开始调用拼多多苏宁同步接口,入参为:{}", JSONObject.toJSONString(sgBChannelSynstockQList));
        }
        ValueHolderV14 holderV14 = new ValueHolderV14(ResultCode.SUCCESS, "增量同步成功");

        List<RPCSyncStockModel> rpcSyncStockModelList = new ArrayList<>();

        for (SgBChannelSynstockQ sgBChannelSynstockQ : sgBChannelSynstockQList) {
            RPCSyncStockModel rpcSyncStockModel = new RPCSyncStockModel();
            //判断是拼多多还是苏宁
            if (SgOutConstantsIF.PLATFORMTYPE_PINDUODUO.equals(sgBChannelSynstockQ.getCpCPlatformId().toString())) {
                //平台skuId
                rpcSyncStockModel.setOutskuId(sgBChannelSynstockQ.getSkuId());
            } else if (SgOutConstantsIF.PLATFORMTYPE_SUNING.equals(sgBChannelSynstockQ.getCpCPlatformId().toString())) {
                //平台skuId
                rpcSyncStockModel.setProductCode(sgBChannelSynstockQ.getSkuId());
            } else if (SgOutConstantsIF.PLATFORMTYPE_WUXIANGYUN.equals(sgBChannelSynstockQ.getCpCPlatformId().toString())) {
                rpcSyncStockModel.setSku(sgBChannelSynstockQ.getSkuId());
            }
            //rpc
            rpcSyncStockModel.setID(sgBChannelSynstockQ.getId());
            //平台id
            rpcSyncStockModel.setPlatform(sgBChannelSynstockQ.getCpCPlatformId().intValue());
            //店铺名称
            rpcSyncStockModel.setSeller_nick(sgBChannelSynstockQ.getSellerNick());
            //商品编号
            rpcSyncStockModel.setNum_iid(sgBChannelSynstockQ.getNumiid());
            //库存
            rpcSyncStockModel.setSyncnum(sgBChannelSynstockQ.getQtyStorage().intValue());
            rpcSyncStockModelList.add(rpcSyncStockModel);
        }

        try {
            //调用RPC服务
            StandplatSyncStockCmd standplatSyncStockCmd = (StandplatSyncStockCmd) ReferenceUtil.refer(
                    ApplicationContextHandle.getApplicationContext(),
                    StandplatSyncStockCmd.class.getName(),
                    SgBChannelSynstockQExtend.GROUPNAME, SgBChannelSynstockQExtend.VERSION);
            log.info(">>>>>>========[dsh]增量拼多多苏宁渠道库存同步RPC服务,参数为:{}", JSONObject.toJSONString(rpcSyncStockModelList));
            ValueHolder valueHolder = standplatSyncStockCmd.syncStock(rpcSyncStockModelList);
            log.info(">>>>>>========[dsh]增量拼多多苏宁渠道库存同步服务,返回值为:{}", holderV14);

            //解析返回结果
            HashMap data = valueHolder.getData();
            int code = (int) valueHolder.get("code");
            if (code == -1) {
                holderV14.setCode(ResultCode.FAIL);
            }

            JSONArray resultList = (JSONArray) data.get("data");
            for (int i = 0; i < resultList.size(); i++) {

                //发送状态
                Boolean sendStatus = ((JSONObject) resultList.get(i)).getBoolean("ISSEND");
                //通过id去匹配返回数据
                Long id = ((JSONObject) resultList.get(i)).getLong("ID");

                for (SgBChannelSynstockQ sgBChannelSynstockQ : sgBChannelSynstockQList) {
                    if (Objects.equals(sgBChannelSynstockQ.getId(), id)) {
                        sgBChannelSynstockQ.setMessageKey(((JSONObject) resultList.get(i)).getString("MSGID"));
                        //如果是发送状态为成功，设置状态为已同步，状态为失败，设置状态为同步失败
                        if (sendStatus) {
                            sgBChannelSynstockQ.setSynStatus(INSYNC);
                        } else {
                            sgBChannelSynstockQ.setSynStatus(SYNCFAIL);
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            sgBChannelSynstockQList.forEach(x -> x.setSynStatus(SYNCFAIL));
            return new ValueHolderV14<>(ResultCode.FAIL, "调用拼多多苏宁渠道库存同步服务失败!");
        } finally {
            sgBChannelSynstockQList.forEach(x -> x.setModifieddate(new Date()));
            sgBChannelSynstockQMapper.batchUpdate(sgBChannelSynstockQList);
        }
        return holderV14;
    }

    /**
     * 回调RPC增量库存同步
     *
     * @param request
     * @return
     */
    public ValueHolderV14<Integer> callbackRPC(SgChannelStorgeOmsCallbackRequest request) {
        //参数校验
        if (request.getId() == null) {
            return new ValueHolderV14<>(ResultCode.FAIL, "ID不能为空!");
        }
        SgBChannelSynstockQExtend sgBChannelSynstockQExtend = new SgBChannelSynstockQExtend();
        sgBChannelSynstockQExtend.setId(request.getId());
        QueryWrapper<SgBChannelSynstockQ> synstockQueryWrapper = sgBChannelSynstockQExtend.createQueryWrapper();
        SgBChannelSynstockQ sgBChannelSynstockQ = sgBChannelSynstockQMapper.selectOne(synstockQueryWrapper);
        sgBChannelSynstockQ.setSynStatus(request.getSynStatus());
        sgBChannelSynstockQ.setErrorMsg(request.getErrorMsg());
        int i = sgBChannelSynstockQMapper.updateById(sgBChannelSynstockQ);
        if (i <= 0) {
            return new ValueHolderV14<>(ResultCode.FAIL, "增量同步库存回调RPC,更新状态失败!");
        }
        return new ValueHolderV14<>(ResultCode.SUCCESS, "增量同步库存回调RPC,更新状态成功!");
    }

    public int searchES(Long cpCShopId, String skuId) {
        /**
         * 根据 [渠道ID+当前时间] 查询es【店铺锁库策略设置】, 是否有生效的锁库策略
         */
        JSONObject whereKeys = new JSONObject();
        JSONObject filterKeys = new JSONObject();
        JSONObject parentWhereKeys = new JSONObject();
        whereKeys.put(SgBChannelStorageDefExtend.CPCSHOPID, cpCShopId);
        parentWhereKeys.put(SgBChannelDefExtend.PT_STATUS, SgBChannelDefExtend.PT_STATUS_CONFIRMED);
        filterKeys.put(LOCK_BTIME, "~" + System.currentTimeMillis());
        filterKeys.put(LOCK_ETIME, System.currentTimeMillis() + "~");
        JSONObject object = ElasticSearchUtil.searchChild(ST_C_LOCK_STOCK_STRATEGY, ST_C_LOCK_STOCK_STRATEGY_ITEM, ST_C_LOCK_STOCK_STRATEGY, whereKeys, filterKeys, null, parentWhereKeys, 1, 0, new String[]{"ID"});
        //date 获取查询数据
        JSONArray jsonArray = new JSONArray();
        if (StringUtils.isNotEmpty(object.getString("data"))) {
            jsonArray = JSONArray.parseArray(object.getString("data"));
        }
        //存在记录, 则返回结果为否(不同步库存)
        if (CollectionUtils.isNotEmpty(jsonArray)) {
            return OmsConstantsIF.IS_SYNC_FLAG_LOCK_STORE;
        }
        /**
         * 不存在记录, 则根据 [渠道ID+渠道条码 ID+当前时间] 再查询 es【店铺锁库条码特殊设置】
         */
        JSONObject whereKeysForSku = new JSONObject();
        JSONObject childWhereKeysForSku = new JSONObject();
        whereKeysForSku.put(SgBChannelStorageDefExtend.CPCSHOPID, cpCShopId);
        whereKeysForSku.put(SgBChannelDefExtend.PT_STATUS, SgBChannelDefExtend.PT_STATUS_CONFIRMED);
        childWhereKeysForSku.put(PT_SKU_ID, skuId);
        JSONObject search = ElasticSearchUtil.search(ST_C_LOCK_SKU_STRATEGY, ST_C_LOCK_SKU_STRATEGY, ST_C_LOCK_SKU_STRATEGY_ITEM, whereKeysForSku, filterKeys, null, childWhereKeysForSku, 1, 0, new String[]{"ID"});
        if (StringUtils.isEmpty(search.getString("data"))) {
            return OmsConstantsIF.IS_SYNC_FLAG_SYNCABLE;
        }
        JSONArray json = JSONArray.parseArray(search.getString("data"));
        //不存在记录, 则返回结果为是(需要同步库存)
        if (CollectionUtils.isEmpty(json)) {
            return OmsConstantsIF.IS_SYNC_FLAG_SYNCABLE;
        }
        //存在记录, 则返回结果为否(不同步库存)
        return OmsConstantsIF.IS_SYNC_FLAG_LOCK_SKU;
    }

    /**
     * 全量同步库存时,修改状态
     */
    public ValueHolderV14 updateSgBChannelSynstockQ(ValueHolderV14<List<FullQuantityResult>> valueHolderV14) {
        log.info("==========>>>>>>>>>>[dsh]全量同步库存调用RPC返回结果::" + valueHolderV14);
        List<FullQuantityResult> data = valueHolderV14.getData();
        for (FullQuantityResult item : data) {
            List<FullQtyDataDetailIpModel> dataDetails = item.getDataDetails();
            for (FullQtyDataDetailIpModel dataDetail : dataDetails) {
                SgBChannelSynstockQ sgBChannelSynstockQ = new SgBChannelSynstockQ();
                sgBChannelSynstockQ.setId(dataDetail.getId());
                sgBChannelSynstockQ.setSynStatus(INSYNC);
                if (OmsResultCode.isSuccess(valueHolderV14)) {
                    sgBChannelSynstockQ.setSynStatus(INSYNC);
                } else if ("false".equals(item.getIsSend())) {
                    sgBChannelSynstockQ.setSynStatus(SYNCFAIL);
                }
                sgBChannelSynstockQ.setModifieddate(new Date());
                sgBChannelSynstockQ.setMessageKey(item.getMsgId());
                sgBChannelSynstockQMapper.updateById(sgBChannelSynstockQ);
            }
        }
        return new ValueHolderV14(ResultCode.SUCCESS, "全量同步库存调用RPC后,修改同步队列表成功!");
    }

    public ValueHolderV14 tbSyncChannelStorageAll() {
        log.info(this.getClass().getName() + " [dsh]淘宝开始全量库存同步！");
        QueryWrapper<SgBChannelDef> queryWrapper = new QueryWrapper<>();
        List<String> cpCPlatformIdList = new ArrayList<>();
        cpCPlatformIdList.add(SgOutConstantsIF.PLATFORMTYPE_TAOBAO);//类型为2为淘宝
        cpCPlatformIdList.add(SgOutConstantsIF.PLATFORMTYPE_TAOBAO_FENXIAO);//类型为3为淘宝分销
        queryWrapper.in("cp_c_platform_id", cpCPlatformIdList);
        queryWrapper.eq("is_syn", 1);//1表示需要同步库存
        List<SgBChannelDef> sgBChannelDefList = sgBChannelDefMapper.selectList(queryWrapper);
        log.info(this.getClass().getName() + " [dsh]淘宝开始全量库存同步,查询到的渠道信息条数为" + sgBChannelDefList.size() + "明细为" + JSONObject.toJSONString(sgBChannelDefList));
        //按店铺类型进行分组，天猫   淘宝（需要休眠 2秒）
        List<SgBChannelDef> tb = new ArrayList<>();
        for (SgBChannelDef sgBChannelDef : sgBChannelDefList) {
            if ("淘宝店".equals(sgBChannelDef.getCpCShopType())) {
                tb.add(sgBChannelDef);
            }
        }
        for (SgBChannelDef sgBChannelDef : tb) {
            Boolean isNeedSleep = true;
            SgStorageOmsSynchronousRequest request = new SgStorageOmsSynchronousRequest();
            request.setCpCShopId(sgBChannelDef.getCpCShopId());
            ValueHolderV14<SgStorageOmsSynchronousResult> holderV14 = syncChannelStorageAllService.synchronousStock2(request, isNeedSleep);
            log.info(this.getClass().getName() + " [dsh]淘宝且平台类型为淘宝店，开始全量库存同步,返回结果为" + JSONObject.toJSONString(holderV14));
        }
        sgBChannelDefList.removeAll(tb);
        for (SgBChannelDef bChannelDef : sgBChannelDefList) {
            SgStorageOmsSynchronousRequest request = new SgStorageOmsSynchronousRequest();
            request.setCpCShopId(bChannelDef.getCpCShopId());
            ValueHolderV14<SgStorageOmsSynchronousResult> holderV14 = syncChannelStorageAllService.synchronousStock2(request, false);
            log.info(this.getClass().getName() + " [dsh]淘宝开始全量库存同步,返回结果为" + JSONObject.toJSONString(holderV14));
        }
        return new ValueHolderV14(ResultCode.SUCCESS, "淘宝全量同步成功！");
    }

    public ValueHolderV14 initChannelStorage(List<String> platformIdList) {
        ValueHolderV14<Integer> holderV14 = new ValueHolderV14<>();
        log.info(this.getClass().getName() + " [dsh]开始初始化渠道库存,平台类型id为{}", JSONObject.toJSONString(platformIdList));

//        queryWrapper.ne(SgBChannelDefExtend.CP_C_PLATFORM_ID, SgBChannelDefExtend.SYSTEM_PLATFORM_ID);
        if (CollectionUtils.isNotEmpty(platformIdList)) {
            QueryWrapper<SgBChannelDef> queryWrapper = new QueryWrapper<>();
            queryWrapper.in(SgBChannelDefExtend.CP_C_PLATFORM_ID, platformIdList);
            List<SgBChannelDef> sgBChannelDefList = sgBChannelDefMapper.selectList(queryWrapper);

            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + " 初始化渠道库存查询渠道信息表结果" + JSONObject.toJSONString(sgBChannelDefList));
            }
            //取出所有渠道ID
            if (CollectionUtils.isNotEmpty(sgBChannelDefList)) {
                List<Long> idList = sgBChannelDefList.stream().map(SgBChannelDef::getCpCShopId).collect(Collectors.toList());
                SgChannelStorageOmsInitRequest request = new SgChannelStorageOmsInitRequest();
                request.setCpCShopIdList(idList);
                holderV14 = sgChannelStorageOmsInitService.initChannelStorage(request);
            } else {
                log.error(this.getClass().getName() + " 没有需要初始化渠道信息");
                holderV14.setMessage("没有需要初始化渠道信息");
                holderV14.setCode(ResultCode.SUCCESS);
            }
            return holderV14;
        }
        holderV14.setMessage("没有需要初始化平台信息");
        holderV14.setCode(ResultCode.FAIL);
        return holderV14;
    }

    public ValueHolderV14 jdSyncChannelStorageAll() {
        log.info(this.getClass().getName() + " [dsh]京东开始全量库存同步！");
        QueryWrapper<SgBChannelDef> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("cp_c_platform_id", "4");//类型为4为京东
        queryWrapper.eq("is_syn", 1);//1表示需要同步库存
        List<SgBChannelDef> sgBChannelDefList = sgBChannelDefMapper.selectList(queryWrapper);
        log.info(this.getClass().getName() + " [dsh]京东开始全量库存同步,查询到的渠道信息条数为" + sgBChannelDefList.size() + "明细为" + JSONObject.toJSONString(sgBChannelDefList));
        for (SgBChannelDef bChannelDef : sgBChannelDefList) {
            SgStorageOmsSynchronousRequest request = new SgStorageOmsSynchronousRequest();
            request.setCpCShopId(bChannelDef.getCpCShopId());
            ValueHolderV14<SgStorageOmsSynchronousResult> holderV14 = syncChannelStorageAllService.synchronousStock2(request, false);
            log.info(this.getClass().getName() + " [dsh]京东开始全量库存同步,同步结果为" + JSONObject.toJSONString(holderV14));
        }
        return new ValueHolderV14(ResultCode.SUCCESS, "京东全量同步成功！");
    }

    public ValueHolderV14 snSyncChannelStorageAll() {
        log.info(this.getClass().getName() + " [dsh]苏宁平台开始全量库存同步！");
        QueryWrapper<SgBChannelDef> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("cp_c_platform_id", SgOutConstantsIF.PLATFORMTYPE_SUNING);
        queryWrapper.eq("is_syn", 1);//1表示需要同步库存
        List<SgBChannelDef> sgBChannelDefList = sgBChannelDefMapper.selectList(queryWrapper);
        log.info(this.getClass().getName() + " [dsh]苏宁平台开始全量库存同步,查询到的渠道信息条数为" + sgBChannelDefList.size() + "明细为" + JSONObject.toJSONString(sgBChannelDefList));
        for (SgBChannelDef bChannelDef : sgBChannelDefList) {
            SgStorageOmsSynchronousRequest request = new SgStorageOmsSynchronousRequest();
            request.setCpCShopId(bChannelDef.getCpCShopId());
            request.setCpCPlatformId(bChannelDef.getCpCPlatformId());
            ValueHolderV14<SgStorageOmsSynchronousResult> holderV14 = syncChannelStorageAllService.synchronousStock2(request, false);
            log.info(this.getClass().getName() + " [dsh]苏宁平台开始全量库存同步,同步结果为" + JSONObject.toJSONString(holderV14));
        }
        return new ValueHolderV14(ResultCode.SUCCESS, "苏宁平台全量同步成功！");
    }

    public ValueHolderV14 pddSyncChannelStorageAll() {
        log.info(this.getClass().getName() + " [dsh]拼多多平台开始全量库存同步！");
        QueryWrapper<SgBChannelDef> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("cp_c_platform_id", SgOutConstantsIF.PLATFORMTYPE_PINDUODUO);//类型为4为京东
        queryWrapper.eq("is_syn", 1);//1表示需要同步库存
        List<SgBChannelDef> sgBChannelDefList = sgBChannelDefMapper.selectList(queryWrapper);
        log.info(this.getClass().getName() + " [dsh]拼多多平台开始全量库存同步,查询到的渠道信息条数为" + sgBChannelDefList.size() + "明细为" + JSONObject.toJSONString(sgBChannelDefList));
        for (SgBChannelDef bChannelDef : sgBChannelDefList) {
            SgStorageOmsSynchronousRequest request = new SgStorageOmsSynchronousRequest();
            request.setCpCShopId(bChannelDef.getCpCShopId());
            request.setCpCPlatformId(bChannelDef.getCpCPlatformId());
            ValueHolderV14<SgStorageOmsSynchronousResult> holderV14 = syncChannelStorageAllService.synchronousStock2(request, false);
            log.info(this.getClass().getName() + " [dsh]拼多多平台开始全量库存同步,同步结果为" + JSONObject.toJSONString(holderV14));
        }
        return new ValueHolderV14(ResultCode.SUCCESS, "拼多多平台全量同步成功！");
    }

    public ValueHolderV14 wxySyncChannelStorageAll() {
        log.info(this.getClass().getName() + " [dsh]舞象云平台开始全量库存同步！");
        QueryWrapper<SgBChannelDef> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("cp_c_platform_id", SgOutConstantsIF.PLATFORMTYPE_WUXIANGYUN);//类型为4为京东
        queryWrapper.eq("is_syn", 1);//1表示需要同步库存
        List<SgBChannelDef> sgBChannelDefList = sgBChannelDefMapper.selectList(queryWrapper);
        log.info(this.getClass().getName() + " [dsh]舞象云平台开始全量库存同步,查询到的渠道信息条数为" + sgBChannelDefList.size() + "明细为" + JSONObject.toJSONString(sgBChannelDefList));
        for (SgBChannelDef bChannelDef : sgBChannelDefList) {
            SgStorageOmsSynchronousRequest request = new SgStorageOmsSynchronousRequest();
            request.setCpCShopId(bChannelDef.getCpCShopId());
            request.setCpCPlatformId(bChannelDef.getCpCPlatformId());
            ValueHolderV14<SgStorageOmsSynchronousResult> holderV14 = syncChannelStorageAllService.synchronousStock2(request, false);
            log.info(this.getClass().getName() + " [dsh]舞象云平台开始全量库存同步,同步结果为" + JSONObject.toJSONString(holderV14));
        }
        return new ValueHolderV14(ResultCode.SUCCESS, "舞象云平台全量同步成功！");
    }

}
