package com.jackrain.nea.sg.oms.services;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.data.basic.model.request.SkuInfoQueryRequest;
import com.jackrain.nea.data.basic.services.BasicPsQueryService;
import com.jackrain.nea.es.util.ElasticSearchUtil;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.ps.api.result.PsCProSkuResult;
import com.jackrain.nea.sg.basic.mapper.SgBGroupStorageMapper;
import com.jackrain.nea.sg.basic.mapper.SgBStorageMapper;
import com.jackrain.nea.sg.basic.model.table.SgBGroupStorage;
import com.jackrain.nea.sg.basic.model.table.SgBStorage;
import com.jackrain.nea.sg.basic.utils.StorageLogUtils;
import com.jackrain.nea.sg.oms.common.OmsConstantsIF;
import com.jackrain.nea.sg.oms.common.OmsResultCode;
import com.jackrain.nea.sg.oms.mapper.SgBChannelDefMapper;
import com.jackrain.nea.sg.oms.mapper.SgBChannelProductMapper;
import com.jackrain.nea.sg.oms.mapper.SgBChannelStorageMapper;
import com.jackrain.nea.sg.oms.mapper.SgBChannelStoreMapper;
import com.jackrain.nea.sg.oms.model.request.SgChannelStorageOmsCalcRequest;
import com.jackrain.nea.sg.oms.model.request.SgChannelStorageOmsChangeRequest;
import com.jackrain.nea.sg.oms.model.result.SgChannelStorageOmsCalcResult;
import com.jackrain.nea.sg.oms.model.result.SgChannelStorageOmsChangeResult;
import com.jackrain.nea.sg.oms.model.table.*;
import com.jackrain.nea.sg.oms.model.tableExtend.*;
import com.jackrain.nea.st.api.StrategyCenterCmd;
import com.jackrain.nea.st.model.result.InventorySkuOwnership;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.dubbo.config.annotation.Reference;
import org.assertj.core.util.Lists;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.util.DateUtils;

import java.math.BigDecimal;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @Description:全渠道库存计算服务
 * @Author: chenb
 * @Date: 2019/4/22 13:15
 */
@Component
@Slf4j
public class SgChannelStorageOmsCalcService {
    @Autowired
    private SgBChannelDefMapper sgBChannelDefMapper;

    @Autowired
    private SgBChannelStoreMapper sgBChannelStoreMapper;

    @Autowired
    private SgBChannelProductMapper sgBChannelProductMapper;

    @Autowired
    private SgBChannelStorageMapper sgBChannelStorageMapper;

    @Autowired
    private SgBStorageMapper sgBStorageMapper;

    @Autowired
    private SgChannelStorageOmsService sgChannelStorageOmsService;

    @Autowired
    private BasicPsQueryService basicPsQueryService;

    @Autowired
    private SgBGroupStorageMapper sgBGroupStorageMapper;

    @Value("${r3.sg.oms.channelStorageCalcBatchNum}")
    private Integer channelStorageCalcBatchNum;
    @Autowired
    private SgChannelStorageOmsBufferService bufferService;

    @Reference(version = "1.0", group = "st")
    private StrategyCenterCmd strategyCenterCmd;

    public ValueHolderV14<Boolean> executeTask() {
        ValueHolderV14 result = new ValueHolderV14();
        /*
            根据配置的查询行数channelStorageCalcBatchNum, 按ID正序查询【渠道库存计算缓存池表】X行处理状态为未处理的数据
         */
        SgBChannelStorageBufferExtend sgBChannelStorageBufferExtend = new SgBChannelStorageBufferExtend();
        sgBChannelStorageBufferExtend.setListSize(channelStorageCalcBatchNum);
        sgBChannelStorageBufferExtend.setDealStatus(SgBChannelStorageBufferExtend.DealStatusEnum.UN_DEAL.getCode());
        log.info(this.getClass().getName() + " 查询渠道库存计算池入参" +
                        "sgBChannelStorageBufferExtend{}",
                JSONObject.toJSONString(sgBChannelStorageBufferExtend));
        ValueHolderV14<List<SgBChannelStorageBuffer>> bufferResult = bufferService.getChannelStorageOmsBuffer(sgBChannelStorageBufferExtend);
        log.info(this.getClass().getName() + " 查询渠道库存计算池结果" +
                        "bufferResult{}",
                bufferResult);
        if (ResultCode.FAIL == bufferResult.getCode()) {
            log.error(this.getClass().getName() + " 获取渠道库存计算池失败bufferResult{}",
                    bufferResult.getMessage());
            result.setData(false);
            result.setMessage(bufferResult.getMessage());
            return result;
        } else if (CollectionUtils.isEmpty(bufferResult.getData())) {
            result.setMessage("无待计算的任务");
            result.setData(true);
            return result;
        }
        List<SgBChannelStorageBuffer> sgBChannelStorageBufferList = bufferResult.getData();
        /*
            批量更新数据行的处理状态为2
         */
        sgBChannelStorageBufferExtend = new SgBChannelStorageBufferExtend();
        BaseExtend baseExtend = new BaseExtend();
        baseExtend.setIdList(sgBChannelStorageBufferList.stream().map(SgBChannelStorageBuffer::getId).collect(Collectors.toList()));
        sgBChannelStorageBufferExtend.setBaseExtend(baseExtend);
        sgBChannelStorageBufferExtend.setUpdateDealStatus(SgBChannelStorageBufferExtend.DealStatusEnum.DEALING.getCode());
        log.info(this.getClass().getName() + " 更新渠道库存计算池状态入参 " +
                        "sgBChannelStorageBufferExtend{}",
                sgBChannelStorageBufferExtend);
        bufferService.updateChannelStorageOmsBufferStatus(sgBChannelStorageBufferExtend);
        /*
            开始循环调用渠道库存计算服务
         */
        SgChannelStorageOmsCalcRequest sgChannelStorageOmsCalcRequest = new SgChannelStorageOmsCalcRequest();
        ValueHolderV14<SgChannelStorageOmsCalcResult> calcResult = new ValueHolderV14<SgChannelStorageOmsCalcResult>();
        Integer successCount = 0;
        List<String> keyLock = Lists.newArrayList();// 再次去除重复记录
        String key = "";
        for (SgBChannelStorageBuffer sgBChannelStorageBuffer : sgBChannelStorageBufferList) {
            try {
                key = sgBChannelStorageBuffer.getCpCShopId() + ":" + sgBChannelStorageBuffer.getSkuId() + ":" + sgBChannelStorageBuffer.getSourceCpCShopId();
                log.info(this.getClass().getName() + " 开始循环调用渠道库存计算服务,key为{},id为{}", key, sgBChannelStorageBuffer.getId());
                BeanUtils.copyProperties(sgBChannelStorageBuffer, sgChannelStorageOmsCalcRequest);
                sgChannelStorageOmsCalcRequest.setMessageKey("[bufferId:" + sgBChannelStorageBuffer.getId() + "]");
                // 初始化部分变量
                baseExtend.setIdList(Lists.newArrayList(sgBChannelStorageBuffer.getId()));
                sgBChannelStorageBufferExtend = new SgBChannelStorageBufferExtend();
                sgBChannelStorageBufferExtend.setUpdateDealStatus(SgBChannelStorageBufferExtend.DealStatusEnum.DEAL_REDEAL.getCode());
                sgBChannelStorageBufferExtend.setBaseExtend(baseExtend);
                if (!keyLock.contains(key)) {
                    keyLock.add(key);
                    log.info(this.getClass().getName() + " 重新计算库存 " +
                                    "sgChannelStorageOmsCalcRequest{},key{},id{}",
                            sgChannelStorageOmsCalcRequest, key, sgBChannelStorageBuffer.getId());
                    calcResult = calcChannelStorageOms(sgChannelStorageOmsCalcRequest);
                    if (ResultCode.SUCCESS == calcResult.getCode()) {
                        // 执行成功
                        sgBChannelStorageBufferExtend.setUpdateDealStatus(SgBChannelStorageBufferExtend.DealStatusEnum.DEAL_SUCCESS.getCode());
                        successCount++;
                    }
                }
                result.setData(true);
                result.setMessage("[" + DateUtils.format(new Date(), "yyyy-MM-dd HH:mm:ss", Locale.CHINA) + "]定时计算全渠道库存计算结果:" +
                        "库存计算缓存池获取记录" + sgBChannelStorageBufferList.size() + "条,执行成功" + successCount + "条");

            } catch (Exception e) {
                e.printStackTrace();
                sgBChannelStorageBufferExtend.setUpdateDealStatus(SgBChannelStorageBufferExtend.DealStatusEnum.DEAL_REDEAL.getCode());
                log.error(this.getClass().getName() + " 重新计算渠道库存异常 " + JSONObject.toJSONString(sgChannelStorageOmsCalcRequest), e);
                String errorMsg = StorageLogUtils.getChannelErrMessage(e, sgChannelStorageOmsCalcRequest.toString());
                result.setMessage(errorMsg);
                result.setData(false);
            } finally {
                // 回写执行结果
                log.info(this.getClass().getName() + " 更新渠道库存计算池入参 sgBChannelStorageBufferExtend{}",
                        sgBChannelStorageBufferExtend);
                try {
                    bufferService.updateChannelStorageOmsBufferStatus(sgBChannelStorageBufferExtend);
                } catch (Exception e) {
                    log.error(this.getClass().getName() + " 更新渠道库存计算池状态失败,id为{}"
                            , sgBChannelStorageBufferExtend.getBaseExtend().getIdList());
                }
            }
        }
        return result;
    }

    /**
     * @param request
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public ValueHolderV14<SgChannelStorageOmsCalcResult> calcChannelStorageOms(SgChannelStorageOmsCalcRequest request) {
        //用来判断是否走指定归属店铺策略
        boolean flag = false;

        if (log.isDebugEnabled()) {
            log.debug("Start SgChannelStorageOmsCalcService.calcChannelStorageOms[params" + request.toString() + "]");
        }
        ValueHolderV14<SgChannelStorageOmsCalcResult> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        if (request.check()) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(request.getMessageKey() + "参数校验不通过");
            return holder;
        }

        /**
         根据渠道ID查询【渠道逻辑仓关系表】,  获取渠道对应的逻辑仓ID list
         */
        SgBChannelStoreExtend sgBChannelStoreExtend = new SgBChannelStoreExtend();
        sgBChannelStoreExtend.setCpCShopId(request.getCpCShopId());
        QueryWrapper<SgBChannelStore> storeQueryWrapper = sgBChannelStoreExtend.createQueryWrapper();
        List<SgBChannelStore> sgBChannelStoreList = sgBChannelStoreMapper.selectList(storeQueryWrapper);
        if (CollectionUtils.isEmpty(sgBChannelStoreList)) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(request.getMessageKey() + "渠道逻辑仓关系信息");
            log.error(this.getClass().getName() + " 渠道逻辑仓关系信息" + JSONObject.toJSONString(request));
            return holder;
        }
        Map<Long, SgBChannelStore> sgBChannelStoreMap = sgBChannelStoreList.stream().collect(Collectors.toMap(SgBChannelStore::getCpCStoreId, Function.identity()));
        List<Long> cpCStoreIdList = sgBChannelStoreList.stream().map(SgBChannelStore::getCpCStoreId).distinct().collect(Collectors.toList());

        /**
         根据渠道ID和渠道条码ID查询【渠道商品表】，获取对应的唯一sku
         */
        SgBChannelProductExtend sgBChannelProductExtend = new SgBChannelProductExtend();
        sgBChannelProductExtend.setCpCShopId(request.getCpCShopId());
        sgBChannelProductExtend.setSkuId(request.getSkuId());
        QueryWrapper<SgBChannelProduct> productQueryWrapper = sgBChannelProductExtend.createQueryWrapper();
        SgBChannelProduct sgBChannelProduct = sgBChannelProductMapper.selectOne(productQueryWrapper);
        if (sgBChannelProduct == null) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(request.getMessageKey() + "无对应渠道商品表");
            log.error(this.getClass().getName() + " 无对应渠道商品表" + JSONObject.toJSONString(request));
            return holder;
        }

        /**
         * 同步库存计算 单sku库存低于设定值时 只在一家店售卖
         */
        //根据SKU条码，查询条码档案，获取SKU条码设置的最低库存数及最低库存归属店铺；
        SkuInfoQueryRequest skuInfoQueryRequest = new SkuInfoQueryRequest();
        skuInfoQueryRequest.setSkuIdList(Lists.newArrayList(sgBChannelProduct.getPsCSkuId()));
        HashMap<Long, PsCProSkuResult> skuInfo;
        try {
            skuInfo = this.basicPsQueryService.getSkuInfo(skuInfoQueryRequest);
        } catch (Exception e) {
            log.error("调用basicPsQueryService.getSkuInfo异常");
            throw new NDSException("调用basicPsQueryService.getSkuInfo异常");
        }

        if (skuInfo == null || skuInfo.get(sgBChannelProduct.getPsCSkuId()) == null) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(request.getMessageKey() + "无该条码信息");
            log.error(this.getClass().getName() + " 条码ID为 " + sgBChannelProduct.getPsCSkuId() + "无查询到的信息" + JSONObject.toJSONString(request));
            return holder;
        }


        BigDecimal channelQty = BigDecimal.ZERO;
        BigDecimal storageQtySum = BigDecimal.ZERO;
        List<SgBStorage> sgBStorageList = new ArrayList<>();

        //调用策略中心服务，查看是否存在【单sku库存低于设定值时 只在一家店售卖】策略，并取得最低库存数和指定的店铺
        PsCProSkuResult psCProSkuResult = skuInfo.get(sgBChannelProduct.getPsCSkuId());
        InventorySkuOwnership inventorySkuOwnership = new InventorySkuOwnership();
        inventorySkuOwnership.setPsCSkuId(sgBChannelProduct.getPsCSkuId());
        inventorySkuOwnership.setPsCBrandId(psCProSkuResult.getPsCBrandId());
        inventorySkuOwnership.setMaterialType(psCProSkuResult.getMaterieltype());

        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + " 开始调用单sku库存低于设定值时只在一家店售卖策略,入参为{}",
                    JSONObject.toJSONString(inventorySkuOwnership));
        }

        ValueHolderV14<InventorySkuOwnership> inventorySkuOwnershipValueHolderV14 = strategyCenterCmd.queryInventoryOwnershipV14(inventorySkuOwnership);

        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + " 开始调用单sku库存低于设定值时只在一家店售卖策略,出参为{}",
                    JSONObject.toJSONString(inventorySkuOwnershipValueHolderV14));
        }

        if (ResultCode.SUCCESS == inventorySkuOwnershipValueHolderV14.getCode()) {

            //根据查询出的逻辑仓ID list 以及对应的sku, 去【逻辑库存表】里查询对应sku的逻辑仓库存
            log.info(this.getClass().getName() + " 开始查询逻辑仓库存表,skuid={}", request.getSkuId());
            sgBStorageList = sgBStorageMapper.selectList(new QueryWrapper<SgBStorage>()
                    .eq("PS_C_SKU_ID", sgBChannelProduct.getPsCSkuId()));

            //计算商品的库存数量【sum(普通商品库存）】
            log.info(this.getClass().getName() + " 开始计算普通商品（有指定归属店铺策略）的渠道库存,skuid={}", request.getSkuId());
            channelQty = sgBStorageList.stream().map(sgBGroupStorage -> {
                if (sgBGroupStorage.getQtyAvailable() != null) {
                    return sgBGroupStorage.getQtyAvailable();
                }
                return BigDecimal.ZERO;
            }).reduce(BigDecimal::add).orElse(BigDecimal.ZERO);

            log.info(this.getClass().getName() + " 计算渠道库存公式" + "sum(普通商品库存)(所有店铺)：" + channelQty);

            //判断系统的库存数是否低于设置的指定库存数
            InventorySkuOwnership resultData = inventorySkuOwnershipValueHolderV14.getData();
            BigDecimal ownershipNum = resultData.getOwnershipNum();
            Long cpCShopId = resultData.getCpCShopId();
            if (channelQty.compareTo(ownershipNum) <= 0) {
                if (!(request.getCpCShopId().equals(cpCShopId))) {
                    log.info(this.getClass().getName() + " 条码{}库存数{}小于或等于设定值{},店铺id:{},指定店铺id{}"
                            , request.getSkuId(), channelQty, ownershipNum, request.getCpCShopId(), cpCShopId);
                    channelQty = BigDecimal.ZERO;
                } else {

                    sgBStorageList = sgBStorageMapper.selectList(new QueryWrapper<SgBStorage>()
                            .eq("PS_C_SKU_ID", sgBChannelProduct.getPsCSkuId())
                            .in("CP_C_STORE_ID", cpCStoreIdList));

                    log.info(this.getClass().getName() + " 开始计算普通商品（有指定归属店铺策略）(且是指定的店铺)的渠道库存,skuid={}", request.getSkuId());
                    channelQty = sgBStorageList.stream().map(sgBGroupStorage -> {
                        if (sgBGroupStorage.getQtyAvailable() != null) {
                            return sgBGroupStorage.getQtyAvailable();
                        }
                        return BigDecimal.ZERO;
                    }).reduce(BigDecimal::add).orElse(BigDecimal.ZERO);

                    log.info(this.getClass().getName() + " 计算渠道库存公式" + "sum(普通商品库存)(指定店铺)：" + channelQty);
                }
            } else {
                flag = true;
            }
        } else {
            flag = true;
        }

        if (flag) {
            /**  根据渠道ID和渠道条码ID先查询 es【渠道商品特殊比例设置】查询是否存在
             存在: 则记录该渠道条码ID的同步比例, 以及安全库存数
             不存在: 则查询 【渠道信息】表, 获取该渠道总的同步比例和安全库存数
             */
            channelQty = BigDecimal.ZERO;
            SgBChannelDef sgBChannelDef = new SgBChannelDef();
            JSONObject whereKeys = new JSONObject();
            JSONObject parentWhereKeys = new JSONObject();
            JSONObject filterKeys = new JSONObject();
            whereKeys.put(SgBChannelDefExtend.CPCSHOPID, request.getCpCShopId());
            whereKeys.put(SgBChannelDefExtend.PT_SKU_ID, request.getSkuId());
            filterKeys.put(SgBChannelDefExtend.BEGIN_TIME, "~" + System.currentTimeMillis());
            filterKeys.put(SgBChannelDefExtend.END_TIME, System.currentTimeMillis() + "~");
            parentWhereKeys.put(SgBChannelDefExtend.PT_STATUS, SgBChannelDefExtend.PT_STATUS_CONFIRMED);
            JSONArray jsonArray = new JSONArray();

            //先查询主表，看这个商品是否有策略
            JSONObject searchfirst = ElasticSearchUtil.search(SgBChannelDefExtend.SG_B_CHANNEL_DEF_ES_INDEX,
                    SgBChannelDefExtend.SG_B_CHANNEL_DEF_ES_INDEX, SgBChannelDefExtend.SG_B_CHANNEL_DEF_ES_TYPE, parentWhereKeys, filterKeys, null, whereKeys, 1, 0,
                    new String[]{"ID"});
            if (null != searchfirst.getString("data")) {
                jsonArray = JSONArray.parseArray(searchfirst.getString("data"));
            }

            if (CollectionUtils.isNotEmpty(jsonArray)) {

                log.info(this.getClass().getName() + " 该条码存在特殊比例策略,skuId:{}", request.getSkuId());

                //再去查子表,查询特殊比例
                Integer id = ((JSONObject) jsonArray.get(0)).getInteger("ID");
                whereKeys.put(SgBChannelDefExtend.ST_C_PRODUCT_STRATEGY_ID, id);
                JSONObject searchSecond = ElasticSearchUtil.searchChild(SgBChannelDefExtend.SG_B_CHANNEL_DEF_ES_INDEX,
                        SgBChannelDefExtend.SG_B_CHANNEL_DEF_ES_TYPE, SgBChannelDefExtend.SG_B_CHANNEL_DEF_ES_INDEX, whereKeys, null, null, parentWhereKeys, 1, 0,
                        new String[]{SgBChannelDefExtend.STOCK_SCALE, SgBChannelDefExtend.LOW_STOCK});
                if (null != searchSecond.getString("data")) {
                    jsonArray = JSONArray.parseArray(searchSecond.getString("data"));
                    if (CollectionUtils.isNotEmpty(jsonArray)) {
                        if (1 != jsonArray.size()) {
                            holder.setCode(ResultCode.FAIL);
                            holder.setMessage(request.getMessageKey() + "店铺锁库策略设置数据异常");
                            log.error(this.getClass().getName() + " 店铺锁库策略设置数据异常");
                            return holder;
                        } else {
                            BigDecimal qtySafe = ((JSONObject) jsonArray.get(0)).getBigDecimal(SgBChannelDefExtend.LOW_STOCK);
                            BigDecimal stockScale = ((JSONObject) jsonArray.get(0)).getBigDecimal(SgBChannelDefExtend.STOCK_SCALE);
                            sgBChannelDef.setQtySafe(qtySafe);
                            sgBChannelDef.setStockScale(stockScale);
                            log.info(this.getClass().getName() + " 该条码存在特殊比例策略,skuId:{},安全数:{},比列:{}", request.getSkuId(), qtySafe, stockScale);
                        }
                    }
                }
            } else {
                SgBChannelStorageDefExtend sgBChannelStorageDefExtend = new SgBChannelStorageDefExtend();
                sgBChannelStorageDefExtend.setCpCShopId(request.getCpCShopId());
                QueryWrapper<SgBChannelDef> defQueryWrapper = sgBChannelStorageDefExtend.createQueryWrapper();
                sgBChannelDef = sgBChannelDefMapper.selectOne(defQueryWrapper);
                if (sgBChannelDef == null) {
                    holder.setCode(ResultCode.FAIL);
                    holder.setMessage(request.getMessageKey() + "无对应渠道信息");
                    log.error(this.getClass().getName() + " 无渠道信息" + JSONObject.toJSONString(request));
                    return holder;
                }
            }

            /**
             * 判断是组合商品还是普通商品，组合商品查询【组合商品库存表】，普通商品查询【逻辑库存表】
             */

            List<SgBGroupStorage> sgBGroupStorageList = new ArrayList<>();
            if (request.getWareType() != null && request.getWareType() == 1) {

                // 根据查询出的逻辑仓ID list 以及对应的sku, 去【组合商品库存表】里查询对应sku的逻辑仓库存
                log.info(this.getClass().getName() + " 开始查询组合商品库存表,skuid={}", request.getSkuId());
                sgBGroupStorageList = sgBGroupStorageMapper.selectList(new QueryWrapper<SgBGroupStorage>()
                        .eq("PS_C_SKU_ID", sgBChannelProduct.getPsCSkuId())
                        .in("CP_C_STORE_ID", cpCStoreIdList));
            } else {

                //根据查询出的逻辑仓ID list 以及对应的sku, 去【逻辑库存表】里查询对应sku的逻辑仓库存
                log.info(this.getClass().getName() + " 开始查询逻辑仓库存表,skuid={}", request.getSkuId());
                sgBStorageList = sgBStorageMapper.selectList(new QueryWrapper<SgBStorage>()
                        .eq("PS_C_SKU_ID", sgBChannelProduct.getPsCSkuId())
                        .in("CP_C_STORE_ID", cpCStoreIdList));
            }

            if (CollectionUtils.isNotEmpty(sgBStorageList)) {

                if (CollectionUtils.isNotEmpty(jsonArray)) {
                    log.info(this.getClass().getName() + " 开始计算特殊商品的渠道库存,skuid={}", request.getSkuId());
                    SgBChannelDef finalSgBChannelDef = sgBChannelDef;
                    storageQtySum = sgBStorageList.stream().map(sgBStorage -> {
                        if (sgBStorage.getQtyAvailable() != null && sgBStorage.getQtyAvailable().compareTo(finalSgBChannelDef.getQtySafe() == null ? BigDecimal.ZERO : finalSgBChannelDef.getQtySafe()) > 0) {
                            // 可用库存有效且大于逻辑仓最低库存数
                            return finalSgBChannelDef.getStockScale().multiply(sgBStorage.getQtyAvailable()).divide(OmsConstantsIF.PERCENT, 4, BigDecimal.ROUND_DOWN);
                        }
                        return BigDecimal.ZERO;
                    }).reduce(BigDecimal::add).orElse(BigDecimal.ZERO);
                    storageQtySum = storageQtySum.subtract(finalSgBChannelDef.getQtySafe() == null ? BigDecimal.ZERO : finalSgBChannelDef.getQtySafe());
                    channelQty = storageQtySum.setScale(0, BigDecimal.ROUND_DOWN);
                    log.info(this.getClass().getName() + " 计算渠道库存公式" + "sum(逻辑仓可用库存*特殊逻辑仓比例) - 特殊安全库存数：" + storageQtySum +
                            "特殊安全库存数" + sgBChannelDef.getQtySafe() + "特殊逻辑仓比例：" + sgBChannelDef.getStockScale());
                } else {
                  /*
                计算渠道库存: 渠道库存= (sum((逻辑仓可用库存-逻辑仓安全库存数)*逻辑仓比例)-渠道安全库存数)*渠道比例
            */
                    log.info(this.getClass().getName() + " 开始计算普通商品的渠道库存,skuid={}", request.getSkuId());
                    storageQtySum = sgBStorageList.stream().map(sgBStorage -> {
                        SgBChannelStore sgBChannelStore = sgBChannelStoreMap.get(sgBStorage.getCpCStoreId());
                        if (sgBStorage.getQtyAvailable() != null && sgBStorage.getQtyAvailable().compareTo(sgBChannelStore.getLowStock() == null ? BigDecimal.ZERO : sgBChannelStore.getLowStock()) > 0) {
                            // 可用库存有效且大于逻辑仓最低库存数
                            return sgBChannelStore.getStockScale().multiply(sgBStorage.getQtyAvailable().subtract(sgBChannelStore.getLowStock() == null ? BigDecimal.ZERO : sgBChannelStore.getLowStock())).divide(OmsConstantsIF.PERCENT, 4, BigDecimal.ROUND_DOWN);
                        }
                        return BigDecimal.ZERO;
                    }).reduce(BigDecimal::add).orElse(BigDecimal.ZERO);
                    log.info(this.getClass().getName() + " 计算渠道库存公式" + "sum((逻辑仓可用库存- 逻辑仓安全库存数)*逻辑仓比例)：" + storageQtySum +
                            "渠道安全库存数" + sgBChannelDef.getQtySafe() + "渠道比例：" + sgBChannelDef.getStockScale());
                    channelQty = sgBChannelDef.getStockScale().multiply(storageQtySum.subtract(sgBChannelDef.getQtySafe())).divide(OmsConstantsIF.PERCENT, 0, BigDecimal.ROUND_DOWN);
                }
            }

          /*
                计算渠道库存: 渠道库存= (sum(组合商品库存))-渠道安全库存数)*渠道比例
            */
            if (CollectionUtils.isNotEmpty(sgBGroupStorageList)) {
                log.info(this.getClass().getName() + " 开始计算组合商品的渠道库存,skuid={}", request.getSkuId());
                storageQtySum = sgBGroupStorageList.stream().map(sgBGroupStorage -> {
                    SgBChannelStore sgBChannelStore = sgBChannelStoreMap.get(sgBGroupStorage.getCpCStoreId());
                    if (sgBGroupStorage.getQtyStorage() != null) {
                        return sgBGroupStorage.getQtyStorage();
                    }
                    return BigDecimal.ZERO;
                }).reduce(BigDecimal::add).orElse(BigDecimal.ZERO);
                log.info(this.getClass().getName() + " 计算渠道库存公式" + "sum(组合商品库存)：" + storageQtySum +
                        "渠道安全库存数" + sgBChannelDef.getQtySafe() + "渠道比例：" + sgBChannelDef.getStockScale());
                channelQty = sgBChannelDef.getStockScale().multiply(storageQtySum.subtract(sgBChannelDef.getQtySafe())).divide(OmsConstantsIF.PERCENT, 0, BigDecimal.ROUND_DOWN);
            }
        }

       /*
            根据渠道 ID和渠道条码 ID, 去【渠道库存表】里查询对应的渠道可售库存, 根据计算的渠道库存和查询返回的渠道可售库存, 对比库存差值
            库存差值为0, 则结束
         */
        SgBChannelStorageExtend sgBChannelStorageStorageExtend = new SgBChannelStorageExtend();
        sgBChannelStorageStorageExtend.setCpCShopId(request.getCpCShopId());
        sgBChannelStorageStorageExtend.setSkuId(request.getSkuId());
        QueryWrapper<SgBChannelStorage> storageQueryWrapper = sgBChannelStorageStorageExtend.createQueryWrapper();
        SgBChannelStorage sgBChannelStorage = sgBChannelStorageMapper.selectOne(storageQueryWrapper);
        holder.setMessage(request.getMessageKey() + " 统计结果逻辑库合计值:" + storageQtySum + ",渠道库存值:" + channelQty + "," +
                "渠道库存表可售库存数:"
                + (sgBChannelStorage == null ? "无记录" : sgBChannelStorage.getQty()));
        log.info(holder.getMessage());
//        if ((sgBChannelStorage == null && channelQty.compareTo(BigDecimal.ZERO) > 0) || (sgBChannelStorage != null && channelQty.compareTo(sgBChannelStorage.getQty()) != 0)) {
        if (sgBChannelStorage != null || channelQty.compareTo(BigDecimal.ZERO) >= 0) {
            /*
                调用【更新/新增渠道库存服务】条件:
                （无渠道库存信息且计算出的渠道库存大于等于0）
                或
                （有渠道库存信息)
             */
            log.info(this.getClass().getName() + " 开始更新/新增渠道库存服务");
            SgChannelStorageOmsChangeRequest sgChannelStorageOmsChangeRequest = new SgChannelStorageOmsChangeRequest();
            sgChannelStorageOmsChangeRequest.setCpCShopId(request.getCpCShopId());
            sgChannelStorageOmsChangeRequest.setSkuId(request.getSkuId());
            sgChannelStorageOmsChangeRequest.setSkuSpec(sgBChannelProduct.getSkuSpec());
            sgChannelStorageOmsChangeRequest.setQtyStorage(sgBChannelStorage == null ? channelQty : channelQty.subtract(sgBChannelStorage.getQty()));
            sgChannelStorageOmsChangeRequest.setSourceCpCShopId(request.getSourceCpCShopId());
            sgChannelStorageOmsChangeRequest.setSourceNo(request.getSourceNo());
            ValueHolderV14<SgChannelStorageOmsChangeResult> changeResult = sgChannelStorageOmsService.changeStorage(sgChannelStorageOmsChangeRequest);
            if (OmsResultCode.isFail(changeResult) && changeResult != null) {
                holder.setMessage(request.getMessageKey() + changeResult.getMessage());
                holder.setCode(changeResult.getCode());
            }
        }
        return holder;
    }

    @Async("myAsync")
    public void initChannelStorageOms(List<SgChannelStorageOmsCalcRequest> requestList) {

        for (SgChannelStorageOmsCalcRequest sgChannelStorageOmsCalcRequest : requestList) {
            try {
                calcChannelStorageOms(sgChannelStorageOmsCalcRequest);
            } catch (Exception e) {
                log.error(this.getClass().getName() + " 初始化库存失败,错误信息:{}", JSONObject.toJSONString(sgChannelStorageOmsCalcRequest), e);
            }
        }
    }
}


