package com.jackrain.nea.sg.oms.services;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.model.util.ModelUtil;
import com.jackrain.nea.psext.api.CskuGroupEcodeQueryCmd;
import com.jackrain.nea.psext.model.table.ExtPsCSku;
import com.jackrain.nea.psext.model.table.PsCSkugroup;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.mapper.SgBStorageMapper;
import com.jackrain.nea.sg.basic.model.table.SgBStorage;
import com.jackrain.nea.sg.oms.mapper.SgBStorageExtMapper;
import com.jackrain.nea.sg.oms.model.result.SgBStorageResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @Author: chenchen
 * @Description:组合商品最低库存服务类
 * @Date: Create in 13:22 2019/4/8
 */

@Slf4j
@Component
public class CombinedCommodityService {

    @Autowired
    private SgBStorageExtMapper sgBStorageExtMapper;

    @Autowired
    private SgBStorageMapper sgBStorageMapper;

    @Reference(group = "ps", version = "1.0")
    CskuGroupEcodeQueryCmd cskuGroupEcodeQueryCmd;

    /**
     * 更新组合商品最低库存
     *
     * @param groupType 1 普通商品 2 福袋商品商品
     * @return
     */
    public ValueHolderV14 updateCombinedCommodity(Integer groupType) {
        ValueHolderV14 holder = new ValueHolderV14();
        Integer pageIndex = 1;
        Integer pageSize = 10;
        ValueHolderV14 result = cskuGroupEcodeQueryCmd.groupEcodeQuery(pageIndex, pageSize, groupType);
        if (result.getCode() == ResultCode.FAIL) {
            return result;
        }
        Integer add = 0;//新增个数
        Integer update = 0;//修改个数

        Date date = new Date();
        //获取组合商品集合
        List<Map> list = (List<Map>) result.getData();
        while (list.size() > 0) {
            //循环遍历组合商品
            for (Map map : list) {
                //虚拟条码
                List<ExtPsCSku> skus = (List<ExtPsCSku>) map.get("sku");
                if (CollectionUtils.isEmpty(skus))
                    continue;
                for (ExtPsCSku sku : skus) {
                    //实际条码
                    List<PsCSkugroup> groups = sku.getPsCSkugroups();
                    List<Long> skuIds = new ArrayList<>();
                    //记录实际条码-组合数量
                    Map<Long, BigDecimal> groupNum = new HashMap<>();
                    groups.forEach(item -> {
                                skuIds.add(item.getPsCSkuId());
                                groupNum.put(item.getPsCSkuId(), item.getNum());
                            }
                    );
                    if (skuIds.isEmpty()) {
                        continue;
                    }

                    //TODO 根据组合商品的逻辑仓列表 分组查询 实际条码可用量
                    //按逻辑仓分组查询 实际条码可用量
                    List<SgBStorageResult> sgBStorageList = sgBStorageExtMapper.selectCombinedCommodityList(skuIds);
                    //TODO 实际条码查不到库存记录  新增为可用为0的库存记录
                    //循环店仓更新实际库存
                    for (SgBStorageResult sb : sgBStorageList) {
                        List<Integer> minList = new ArrayList<>(); //福袋商品计算专用
                        Integer minNum = 0;
                        String[] qtyAvailableList = sb.getQtyAvailableList().trim().split(",");
                        for (int i = 0; i < qtyAvailableList.length; i++) {
                            // 取最低库存为虚拟条码库存
                            Long sku1 = Long.parseLong(qtyAvailableList[i].split(":")[0]);
                            BigDecimal qtyAvailable = new BigDecimal(qtyAvailableList[i].split(":")[1]);
                            //可用数量/组合数量，除不尽取整
                            Integer num = qtyAvailable.divide(groupNum.get(sku1), 0, BigDecimal.ROUND_DOWN).toBigInteger().intValue();
                            minList.add(num);
                        }
                        List<Integer> collect = minList.stream().sorted(Comparator.reverseOrder()).collect(Collectors.toList());
                        minNum = collect.get(collect.size() - 1);
                        //福袋商品
                        if (groupType != null && groupType.equals(2)) {
                            //福袋商品计算最低库存
                            minNum = getLuckyMinStore(collect, sku.getGroupExtractNum());
                        }
                        //根据组合商品Id和店仓id最低库存
                        List<SgBStorage> selectSgBStorageList = sgBStorageMapper.selectList(new QueryWrapper<SgBStorage>().lambda()
                                .eq(SgBStorage::getPsCSkuId, sku.getId())
                                .eq(SgBStorage::getCpCStoreId, sb.getStoreId()));
                        if (selectSgBStorageList.isEmpty()) {//新增
                            insertCombinedCommodity(minNum, date, sb, sku);
                            add++;
                        } else {//修改
                            updateCombinedCommodity(selectSgBStorageList.get(0), minNum, date);
                            update++;
                        }
                    }


                }
            }
            pageIndex++;
            result = cskuGroupEcodeQueryCmd.groupEcodeQuery(pageIndex, pageSize, groupType);
            if (result.getCode() == ResultCode.FAIL) {
                return result;
            }
            list = (List<Map>) result.getData();
        }
        ValueHolderV14<Integer> holder1 = updateDeletedCombinedCommodity(groupType, date);
        if (holder1.getCode() == ResultCode.FAIL) {
            return result;
        }
        holder.setCode(ResultCode.SUCCESS);
        holder.setMessage("新增:" + add + "修改:" + (update + holder1.getData()));
        return holder;
    }

    /**
     * 福袋商品计算最低库存
     *
     * @param minList 实际商品的最低库库存
     * @param num     每组抽取数量
     */
    private Integer getLuckyMinStore(List<Integer> minList, Integer num) {
        int min = 0;
        if (minList.size() >= num) {
            for (int i = 0; i < minList.size(); i = i + num - 1) {
                if (i != 0 && i % num == 0) {
                    min += minList.get(i);
                }
            }
        }
        return min;
    }


    /**
     * 插入组合商品库存
     *
     * @param min  最低库存
     * @param sku  sku实体
     * @param date 日期
     * @param sb   库存按照店仓和可用数量分组实体类
     */
    private void insertCombinedCommodity(Integer min, Date date, SgBStorageResult sb, ExtPsCSku sku) {
        SgBStorage sgBStorage = new SgBStorage();
        sgBStorage.setId(ModelUtil.getSequence(SgConstants.SG_B_STORAGE));
        sgBStorage.setPsCSkuId(sku.getId());
        sgBStorage.setPsCProEname(sku.getPsCProEname());
        sgBStorage.setPsCProEcode(sku.getPsCProEcode());
        sgBStorage.setQtyPreout(BigDecimal.ZERO);//占有数量
        sgBStorage.setQtyPrein(BigDecimal.ZERO);//在途数量
        sgBStorage.setQtyStorage(BigDecimal.valueOf(min));//在库数量
        sgBStorage.setQtyAvailable(BigDecimal.valueOf(min));//可用数量
        sgBStorage.setCpCStoreId(sb.getStoreId());
        sgBStorage.setCpCStoreEname(sb.getStoreName());
        sgBStorage.setCpCStoreEcode(sb.getStoreEcode());
        sgBStorage.setOwnerid(sku.getOwnerid());
        sgBStorage.setOwnerename(sku.getOwnerename());
        sgBStorage.setOwnername(sku.getOwnername());
        sgBStorage.setCreationdate(date);
        sgBStorage.setModifierid(sku.getModifierid());
        sgBStorage.setModifierename(sku.getModifierename());
        sgBStorage.setModifiername(sku.getModifiername());
        sgBStorage.setModifieddate(date);
        sgBStorage.setIsactive("Y");
        sgBStorage.setAdClientId(sku.getAdClientId());
        sgBStorage.setAdOrgId(sku.getAdOrgId());
        sgBStorageMapper.insert(sgBStorage);
    }

    /**
     * 更新组合商品库存
     *
     * @param sgBStorage 组合商品库存实体
     * @param min        最低库存
     * @param date       日期
     */
    private void updateCombinedCommodity(SgBStorage sgBStorage, Integer min, Date date) {
        sgBStorage.setQtyStorage(BigDecimal.valueOf(min));//在库数量
        //可用数量=在库数量-占用数量+在途数量
        sgBStorage.setQtyAvailable(BigDecimal.valueOf(min).subtract(sgBStorage.getQtyPreout()).add(sgBStorage.getQtyPrein()));
        sgBStorage.setModifieddate(date);
        sgBStorageMapper.updateById(sgBStorage);
    }

    /**
     * 更新实际商品已卖完的组合商品的最低库存为0
     *
     * @param groupType 1 普通 2 福袋
     * @param date      日期
     * @return
     */
    private ValueHolderV14 updateDeletedCombinedCommodity(Integer groupType, Date date) {
        ValueHolderV14<Integer> holder = new ValueHolderV14();
        Integer pageIndex = 1;
        Integer pageSize = 10;
        long hour = 24;
        int update = 0;
        ValueHolderV14 result = cskuGroupEcodeQueryCmd.groupVoidEcodeQuery(pageIndex, pageSize, groupType, hour);
        //获取组合商品集合
        List<Map> list = (List<Map>) result.getData();
        while (list.size() > 0) {
            //循环遍历组合商品
            for (Map map : list) {
                //虚拟条码
                List<ExtPsCSku> skus = (List<ExtPsCSku>) map.get("sku");
                if (CollectionUtils.isEmpty(skus))
                    continue;
                //循环遍历虚拟条码
                for (ExtPsCSku sku : skus) {
                    //实际条码
                    List<PsCSkugroup> groups = sku.getPsCSkugroups();
                    List<Long> skuIds = new ArrayList<>();
                    groups.forEach(item -> skuIds.add(item.getPsCSkuId()));
                    if (skuIds.isEmpty()) {
                        continue;
                    }
                    //按逻辑仓分组查询 实际条码可用量
                    List<SgBStorageResult> sgBStorageList = sgBStorageExtMapper.selectCombinedCommodityList(skuIds);

                    //循环店仓更新组合商品库存实际库存
                    for (SgBStorageResult sb : sgBStorageList) {
                        List<SgBStorage> selectSgBStorageList = sgBStorageMapper.selectList(new QueryWrapper<SgBStorage>().lambda()
                                .eq(SgBStorage::getPsCSkuId, sku.getId())
                                .eq(SgBStorage::getCpCStoreId, sb.getStoreId()));
                        if (!selectSgBStorageList.isEmpty()
                                && selectSgBStorageList.get(0).getQtyAvailable().compareTo(BigDecimal.ZERO) > 0) {
                            updateCombinedCommodity(selectSgBStorageList.get(0), 0, date);
                            update++;
                        }
                    }
                }
            }
            pageIndex++;
            result = cskuGroupEcodeQueryCmd.groupVoidEcodeQuery(pageIndex, pageSize, groupType, hour);
            if (result.getCode() == -1) {
                return result;
            }
            list = (List<Map>) result.getData();
        }
        holder.setCode(ResultCode.SUCCESS);
        holder.setData(update);
        return holder;
    }
}
