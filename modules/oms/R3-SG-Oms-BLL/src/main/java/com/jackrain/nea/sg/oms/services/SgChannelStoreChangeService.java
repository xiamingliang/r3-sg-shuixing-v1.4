package com.jackrain.nea.sg.oms.services;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.model.util.ModelUtil;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.common.SystemUserResource;
import com.jackrain.nea.sg.oms.mapper.SgBChannelStoreMapper;
import com.jackrain.nea.sg.oms.model.request.SgChannelStoreChangeRequest;
import com.jackrain.nea.sg.oms.model.table.SgBChannelStore;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 渠道逻辑仓关系表变动
 */
@Component
@Slf4j
public class SgChannelStoreChangeService {
    private static final String CPCSHOPID = "cp_c_shop_id";//平台店铺ID

    private static final String CPCSTOREID = "cp_c_store_id";//逻辑仓ID

    @Autowired
    private SgBChannelStoreMapper sgBChannelStoreMapper;

    public ValueHolderV14 saveSbGChannelStore(SgChannelStoreChangeRequest request) {
        User rootUser = SystemUserResource.getRootUser();
        Map<String, Object> map = new HashMap<>();
        map.put(CPCSHOPID, request.getCpCShopId());
        map.put(CPCSTOREID, request.getCpCStoreId());
        List<SgBChannelStore> sgBChannelStoreList = sgBChannelStoreMapper.selectByMap(map);
        if (CollectionUtils.isNotEmpty(sgBChannelStoreList)) {
            //修改
            SgBChannelStore sgBChannelStore = sgBChannelStoreList.get(0);
            sgBChannelStore.setCpCShopTitle(request.getCpCShopTitle() == null ? sgBChannelStore.getCpCShopTitle() : request.getCpCShopTitle());
            sgBChannelStore.setCpCStoreId(request.getCpCStoreId() == null ? sgBChannelStore.getCpCStoreId() : request.getCpCStoreId());
            sgBChannelStore.setCpCStoreEcode(request.getCpCStoreEcode() == null ? sgBChannelStore.getCpCStoreEcode() : request.getCpCStoreEcode());
            sgBChannelStore.setCpCStoreEname(request.getCpCStoreEname() == null ? sgBChannelStore.getCpCStoreEname() : request.getCpCStoreEname());
            sgBChannelStore.setPriority(request.getPriority() == null ? sgBChannelStore.getPriority() : request.getPriority());
            sgBChannelStore.setStockScale(request.getRate() == null ? sgBChannelStore.getStockScale() : request.getRate());
            sgBChannelStore.setLowStock(request.getLowStock() == null ? sgBChannelStore.getLowStock() : request.getLowStock());
            sgBChannelStore.setIsSend(request.getIsSend() == null ? sgBChannelStore.getIsSend() : request.getIsSend());

            sgBChannelStore.setModifieddate(new Date());
            sgBChannelStore.setModifiername(rootUser.getName());
            sgBChannelStore.setModifierename(rootUser.getEname());
            int i = sgBChannelStoreMapper.updateById(sgBChannelStore);
            if (i < 0) {
                return new ValueHolderV14(ResultCode.FAIL, "修改渠道逻辑仓关系失败!");
            }
        } else {
            //新增
            SgBChannelStore sgBChannelStore = new SgBChannelStore();
            sgBChannelStore.setId(ModelUtil.getSequence(SgConstants.SG_B_CHANNEL_STORE));
            sgBChannelStore.setCpCShopId(request.getCpCShopId());
            sgBChannelStore.setCpCShopTitle(request.getCpCShopTitle());
            sgBChannelStore.setCpCStoreId(request.getCpCStoreId());
            sgBChannelStore.setCpCStoreEcode(request.getCpCStoreEcode());
            sgBChannelStore.setCpCStoreEname(request.getCpCStoreEname());
            sgBChannelStore.setPriority(request.getPriority());
            sgBChannelStore.setStockScale(request.getRate());
            sgBChannelStore.setLowStock(request.getLowStock());
            sgBChannelStore.setIsSend(request.getIsSend());

            sgBChannelStore.setAdClientId(Integer.valueOf(rootUser.getClientId()).longValue());
            sgBChannelStore.setAdOrgId(Integer.valueOf(rootUser.getOrgId()).longValue());
            sgBChannelStore.setOwnerid(rootUser.getId().longValue());
            sgBChannelStore.setModifierid(rootUser.getId().longValue());
            sgBChannelStore.setCreationdate(new Date());
            sgBChannelStore.setOwnername(rootUser.getName());
            sgBChannelStore.setOwnerename(rootUser.getEname());
            int i = sgBChannelStoreMapper.insert(sgBChannelStore);
            if (i < 0) {
                return new ValueHolderV14(ResultCode.FAIL, "新增渠道逻辑仓关系失败!");
            }
        }
        return new ValueHolderV14(ResultCode.SUCCESS, "渠道逻辑仓关系表操作成功!");
    }

    public ValueHolderV14 deleteSbGChannelStore(SgChannelStoreChangeRequest request) {
        Map<String, Object> map = new HashMap<>();
        map.put(CPCSHOPID, request.getCpCShopId());
        map.put(CPCSTOREID, request.getCpCStoreId());
        List<SgBChannelStore> sgBChannelStoreList = sgBChannelStoreMapper.selectByMap(map);
        if (CollectionUtils.isEmpty(sgBChannelStoreList)) {
            return new ValueHolderV14(ResultCode.FAIL, "根据平台店铺ID查询不到渠道逻辑仓关系表");
        }
        SgBChannelStore sgBChannelStore = sgBChannelStoreList.get(0);
        int i = sgBChannelStoreMapper.deleteById(sgBChannelStore);
        if (i < 0) {
            return new ValueHolderV14(ResultCode.FAIL, "删除渠道逻辑仓关系表失败!");
        }
        return new ValueHolderV14(ResultCode.SUCCESS, "删除渠道逻辑仓关系表成功!");

    }

    /**
     * 根据渠道店铺ID 修改渠道逻辑仓标题
     */

    public ValueHolderV14 changeCpCStoreTitleById(SgChannelStoreChangeRequest request) {
        if (request.getCpCShopId() == null) {
            return new ValueHolderV14(ResultCode.FAIL, "渠道店铺ID不能为空！");
        }
        QueryWrapper<SgBChannelStore> queryWrapper = new QueryWrapper();
        queryWrapper.eq("cp_c_shop_id", request.getCpCShopId());
        List<SgBChannelStore> sgBChannelStoreList = sgBChannelStoreMapper.selectList(queryWrapper);
        if (CollectionUtils.isEmpty(sgBChannelStoreList)) {
            return new ValueHolderV14(ResultCode.FAIL, "根据渠道店铺ID，查询不到渠道逻辑仓关系表");
        }
        User rootUser = SystemUserResource.getRootUser();
        for (SgBChannelStore sgBChannelStore : sgBChannelStoreList) {
            sgBChannelStore.setCpCShopTitle(request.getCpCShopTitle());
            sgBChannelStore.setModifieddate(new Date());
            sgBChannelStore.setModifierename(rootUser.getEname());
            sgBChannelStore.setModifiername(rootUser.getName());
            sgBChannelStoreMapper.updateById(sgBChannelStore);
        }
        return new ValueHolderV14(ResultCode.SUCCESS, "操作成功！");

    }
}
