package com.jackrain.nea.sg.oms.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.model.util.ModelUtil;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.oms.common.OmsConstantsIF;
import com.jackrain.nea.sg.oms.common.OmsResultCode;
import com.jackrain.nea.sg.oms.mapper.SgBChannelDefMapper;
import com.jackrain.nea.sg.oms.mapper.SgBChannelProductMapper;
import com.jackrain.nea.sg.oms.mapper.SgBChannelStorageChangeFtpMapper;
import com.jackrain.nea.sg.oms.mapper.SgBChannelStorageMapper;
import com.jackrain.nea.sg.oms.model.request.SgChannelStorageOmsChangeRequest;
import com.jackrain.nea.sg.oms.model.request.SgChannelStorageOmsManualSynchRequest;
import com.jackrain.nea.sg.oms.model.request.SgStorageOmsSynchronousRequest;
import com.jackrain.nea.sg.oms.model.result.SgChannelStorageOmsChangeResult;
import com.jackrain.nea.sg.oms.model.result.SgStorageOmsSynchronousResult;
import com.jackrain.nea.sg.oms.model.table.SgBChannelDef;
import com.jackrain.nea.sg.oms.model.table.SgBChannelProduct;
import com.jackrain.nea.sg.oms.model.table.SgBChannelStorage;
import com.jackrain.nea.sg.oms.model.table.SgBChannelStorageChangeFtp;
import com.jackrain.nea.sg.oms.model.tableExtend.*;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.utility.RedisLocker;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

/**
 * 渠道库存
 */
@Component
@Slf4j
public class SgChannelStorageOmsService {

    @Autowired
    private SgBChannelStorageMapper sgBChannelStorageMapper;

    @Autowired
    private SgBChannelDefMapper sgBChannelDefMapper;

    @Autowired
    private SgBChannelProductMapper sgBChannelProductMapper;

    @Autowired
    private SgBChannelStorageChangeFtpMapper sgBChannelStorageChangeFtpMapper;

    @Autowired
    private SgChannelStorageOmsSynchService sgChannelStorageOmsSynchService;

    @Autowired
    private SyncChannelStorageAllService syncChannelStorageAllService;

    @Autowired
    private SgChannelStorageOmsInitService sgChannelStorageOmsInitService;

    @Value("${r3.sg.oms.manualSyncChannelStorage.maxSyncSize}")
    private Integer sgChannelProductCalcAndSyncNumber;

    /**
     * 更新/新增渠道库存服务
     * a. 根据渠道ID和渠道条码ID查询【渠道库存表】记录行是否存在
     * b. 添加一条渠道库存变动流水
     * c. 更新完成后, 调用增量同步库存服务
     */
    @Transactional(rollbackFor = Exception.class)
    public ValueHolderV14<SgChannelStorageOmsChangeResult> changeStorage(SgChannelStorageOmsChangeRequest request) {
        ValueHolderV14<SgChannelStorageOmsChangeResult> holderV14 = new ValueHolderV14<>(ResultCode.SUCCESS, "处理成功!");
        // 参数校验
        if (request.check()) {
            log.error(this.getClass().getName() + " 更新库存校验参数不通过" + JSONObject.toJSONString(request));
            holderV14.setCode(ResultCode.FAIL);
            holderV14.setMessage("参数校验不通过");
            return holderV14;
        }
        Integer tryCount = 0;
        RedisLocker redisLocker = new RedisLocker(request.getKey());
        while (!redisLocker.lock(OmsConstantsIF.redisLockTime)) {
            // 锁定失败,存在并发渠道id+渠道条码id,延时500处理
            tryCount++;
            try {
                Thread.sleep(500);
            } catch (Exception e) {
            }
            if (tryCount > OmsConstantsIF.maxRedisLockCount) {
                // 达到尝试上限,强制解锁
                tryCount = 0;
                redisLocker.forceUnLock();
            }
        }
        SgBChannelStorage sgBChannelStorage = new SgBChannelStorage();
        try {
            BigDecimal qtyBegin = BigDecimal.ZERO;//期初可售数（流水用）
            /**
             * a. 根据渠道ID和渠道条码ID查询【渠道库存表】记录行是否存在
             * */
            SgBChannelStorageExtend sgBChannelStorageExtend = new SgBChannelStorageExtend();
            sgBChannelStorageExtend.setCpCShopId(request.getCpCShopId()); //渠道id
            sgBChannelStorageExtend.setSkuId(request.getSkuId()); //渠道条码id
            QueryWrapper<SgBChannelStorage> storageWrapper = sgBChannelStorageExtend.createQueryWrapper();
            sgBChannelStorage = sgBChannelStorageMapper.selectOne(storageWrapper);
            if (sgBChannelStorage != null) {
                // 考虑到上述存在强制解锁的逻辑,根据XACT锁更新渠道库存信息
                qtyBegin = updateQtyWithXactLock(request, sgBChannelStorage, 3);
            } else {
                //如果不存在, 则根据渠道ID在【渠道信息表】获取渠道信息, ,
                SgBChannelDefExtend sgBChannelDefExtend = new SgBChannelDefExtend();
                sgBChannelDefExtend.setCpCShopId(request.getCpCShopId());
                QueryWrapper<SgBChannelDef> defWrapper = sgBChannelDefExtend.createQueryWrapper();
                SgBChannelDef sgBChannelDef = sgBChannelDefMapper.selectOne(defWrapper);
                if (sgBChannelDef == null) {
                    holderV14.setCode(ResultCode.FAIL);
                    holderV14.setMessage("查询不到渠道信息");
                    return holderV14;
                }
                //根据渠道ID +渠道条码ID 在【渠道商品表】获取商品信息
                SgBChannelProductExtend sgBChannelProductExtend = new SgBChannelProductExtend();
                sgBChannelProductExtend.setCpCShopId(request.getCpCShopId()); //渠道id
                sgBChannelProductExtend.setSkuId(request.getSkuId()); //渠道条码id
                QueryWrapper<SgBChannelProduct> productWrapper = sgBChannelProductExtend.createQueryWrapper();
                SgBChannelProduct sgBChannelProduct = sgBChannelProductMapper.selectOne(productWrapper);
                if (sgBChannelProduct == null) {
                    holderV14.setCode(ResultCode.FAIL);
                    holderV14.setMessage("查询不到商品信息");
                    return holderV14;
                }
                //渠道可售库存=变化量, 然后聚合插入一条新的【渠道库存表】记录
                sgBChannelStorage = new SgBChannelStorage();
                SgBChannelStorageExtend.complete(sgBChannelStorage, sgBChannelDef, sgBChannelProduct);
                sgBChannelStorage.setId(ModelUtil.getSequence(SgConstants.SG_B_CHANNEL_STORAGE));
                sgBChannelStorage.setQty(request.getQtyStorage());
                sgBChannelStorage.setAdClientId(SgBChannelStorageBufferExtend.DEFAULT_CLIENT_ID);
                sgBChannelStorage.setAdOrgId(SgBChannelStorageBufferExtend.DEFAULT_ORG_ID);
                if (sgBChannelStorage.getQty().compareTo(BigDecimal.ZERO) < 0) {
                    // 变化库库存小于0,则等于0
                    sgBChannelStorage.setQty(BigDecimal.ZERO);
                }
                if (log.isDebugEnabled()) {
                    log.debug(this.getClass().getName() + " 增加渠道库存记录 " + JSONObject.toJSONString(sgBChannelStorage));
                }
                sgBChannelStorageMapper.insert(sgBChannelStorage);
            }
            /**
             * b. 添加一条渠道库存变动流水
             * */
            SgBChannelStorageChangeFtp sgBChannelStorageChangeFtp = new SgBChannelStorageChangeFtp();
            SgBChannelStorageChangeFtpExtend.complete(sgBChannelStorageChangeFtp, sgBChannelStorage);
            sgBChannelStorageChangeFtp.setId(ModelUtil.getSequence(SgConstants.SG_B_CHANNEL_STORAGE_CHANGE_FTP));
            sgBChannelStorageChangeFtp.setQtyBegin(qtyBegin); //期初可售数
            sgBChannelStorageChangeFtp.setQtyChange(sgBChannelStorage.getQty().subtract(qtyBegin)); //变化数量
            sgBChannelStorageChangeFtp.setQtyEnd(sgBChannelStorage.getQty()); //期末可售数
            sgBChannelStorageChangeFtp.setSourceNo(request.getSourceNo());
            sgBChannelStorageChangeFtp.setAdClientId(SgBChannelStorageBufferExtend.DEFAULT_CLIENT_ID);
            sgBChannelStorageChangeFtp.setAdOrgId(SgBChannelStorageBufferExtend.DEFAULT_ORG_ID);
            sgBChannelStorageChangeFtpMapper.insert(sgBChannelStorageChangeFtp);
            holderV14.setMessage("【渠道库存变动流水】：【渠道id:" + request.getCpCShopId() + ",渠道条码id:" + request.getSkuId() + ",期初可售数:" + sgBChannelStorageChangeFtp.getQtyBegin() + ",变化数量:" + sgBChannelStorageChangeFtp.getQtyChange() + ",期末可售数:" + sgBChannelStorageChangeFtp.getQtyEnd() + "】");
        } catch (Exception e) {
            log.error(this.getClass().getName() + " 库存变动异常", e);
            throw new NDSException();
        } finally {
            // 处理完渠道库存变化后就释放锁定
            redisLocker.unLock();
        }
        if (null != request.getSourceCpCShopId() && !request.getSourceCpCShopId().equals(sgBChannelStorage.getCpCShopId())) {
            /**
             * c. 更新完成后, 且渠道ID和来源渠道ID不一致,则调用增量同步库存服务
             * */
            SgStorageOmsSynchronousRequest sgStorageOmsSynchronousRequest = new SgStorageOmsSynchronousRequest();
            sgStorageOmsSynchronousRequest.setSgBChannelStorage(sgBChannelStorage);
            sgStorageOmsSynchronousRequest.setSyncType(SgBChannelSynstockQExtend.SyncTypeEnum.ADD);
            sgStorageOmsSynchronousRequest.setSourceNo(request.getSourceNo());
            sgStorageOmsSynchronousRequest.setSkuSpec(request.getSkuSpec());
            ValueHolderV14<SgStorageOmsSynchronousResult> serviceHolderV14 = sgChannelStorageOmsSynchService.synchronousStock(sgStorageOmsSynchronousRequest);
            if (OmsResultCode.isFail(serviceHolderV14)) {
                holderV14.setCode(serviceHolderV14.getCode());
                holderV14.setMessage(serviceHolderV14.getMessage());
                return holderV14;
            }
        }
        return holderV14;
    }

    /**
     * 根据XACT锁更新渠道库存信息
     *
     * @param request,sgBChannelStorage
     * @param sgBChannelStorage
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public BigDecimal updateQtyWithXactLock(SgChannelStorageOmsChangeRequest request, SgBChannelStorage sgBChannelStorage, int times) {
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + " 更新渠道库存" + JSONObject.toJSONString(request) + JSONObject.toJSONString(sgBChannelStorage));
        }
        BigDecimal qtyBegin = sgBChannelStorage.getQty();
        //如果存在, 则渠道可售库存=当前可售库存+库存变化量, 更新【渠道库存表】行记录
        BigDecimal qty = sgBChannelStorage.getQty() == null ? request.getQtyStorage() : sgBChannelStorage.getQty().add(request.getQtyStorage());
        if (qty.compareTo(BigDecimal.ZERO) < 0) {
            // 变化库库存小于0,则等于0
            qty = BigDecimal.ZERO;
        }
        sgBChannelStorage.setQty(qty);
        int resultCount = sgBChannelStorageMapper.updateQtyWithXactLock(sgBChannelStorage.getId(), qtyBegin, sgBChannelStorage.getQty(), SgConstants.XACT_LOCK_PREFIX_CHANNEL_STORAGE);

        times = times - 1;

        if (resultCount < 0 && times > 0) {
            // 更新失败,重试
            sgBChannelStorage = sgBChannelStorageMapper.selectById(sgBChannelStorage.getId());
            return updateQtyWithXactLock(request, sgBChannelStorage, times);
        }
        return qtyBegin;
    }

    /**
     * 手动同步库存
     *
     * @param request
     * @return
     */
    public ValueHolderV14<Boolean> manualSynchChannelStorage(SgChannelStorageOmsManualSynchRequest request) {
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + " 手动同步渠道库存service入参 " + JSONObject.toJSONString(request));
        }
        ValueHolderV14 holder;
        if (SgChannelStorageOmsManualSynchRequest.QUERY_BY_CONDITION.equals(request.getOperate())) {
            holder = new ValueHolderV14(true, ResultCode.SUCCESS, "");
        } else {
            holder = request.checkManualSynch();
        }
        if (OmsResultCode.isFail(holder)) {
            return holder;
        }
        SgBChannelStorageExtend sgBChannelStorageExtend = new SgBChannelStorageExtend();
        BeanUtils.copyProperties(request, sgBChannelStorageExtend);
        QueryWrapper<SgBChannelStorage> queryWrapper = null;
        try {
            queryWrapper = sgBChannelStorageExtend.createQueryWrapper();
        } catch (Exception e) {
            if (e instanceof NDSException) {
                holder.setMessage(e.getMessage());
                holder.setCode(ResultCode.FAIL);
                holder.setData(false);
                return holder;
            } else {
                log.error(this.getClass().getName() + " 拼接库存查询条件异常", e);
                throw e;
            }
        }
        List<SgBChannelStorage> sgBChannelStorages = sgBChannelStorageMapper.selectList(queryWrapper);
        if (CollectionUtils.isEmpty(sgBChannelStorages)) {
            holder.setData(false);
            holder.setCode(ResultCode.FAIL);
            holder.setMessage("不存在渠道库存");
            log.error(this.getClass().getName() + " 不存在渠道库存 " + JSONObject.toJSONString(request));
            return holder;
        }
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + " 调用同步库存服务" + JSONObject.toJSONString(sgBChannelStorages));
        }
        String batchno = (System.currentTimeMillis() + "" + (int) (Math.random() * 9000 + 1000));
        syncChannelStorageAllService.syncChannelStorage(sgBChannelStorages, false, batchno);
        holder.setMessage("本次同步批次号" + batchno + "，请前往库存同步队列查询同步结果");
        return holder;
    }

    /**
     * 手动计算渠道商品库存并同步
     *
     * @param request 同步的查询条件
     * @return 执行结果
     */
    public ValueHolderV14<Boolean> manualCalcAndSyncChannelProduct(SgChannelStorageOmsManualSynchRequest request) {
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + " 手工计算渠道商品库存并同步service入参 " + JSONObject.toJSONString(request));
        }
        ValueHolderV14<Boolean> holder;
        if (SgChannelStorageOmsManualSynchRequest.QUERY_BY_CONDITION.equals(request.getOperate())) {
            holder = new ValueHolderV14<>(true, ResultCode.SUCCESS, "");
        } else {
            holder = request.checkManualSynch();
        }
        if (OmsResultCode.isFail(holder)) {
            return holder;
        }
        SgBChannelStorageExtend sgBChannelStorageExtend = new SgBChannelStorageExtend();
        BeanUtils.copyProperties(request, sgBChannelStorageExtend);
        QueryWrapper<SgBChannelProduct> queryWrapper;
        try {
            queryWrapper = sgBChannelStorageExtend.createChannelProductQueryWrapper();
        } catch (Exception e) {
            if (e instanceof NDSException) {
                holder.setMessage(e.getMessage());
                holder.setCode(ResultCode.FAIL);
                holder.setData(false);
                return holder;
            } else {
                log.error(this.getClass().getName() + " 拼接库存查询条件异常", e);
                throw e;
            }
        }
        List<SgBChannelProduct> SgBChannelProductList = sgBChannelProductMapper.selectList(queryWrapper);
        if (CollectionUtils.isEmpty(SgBChannelProductList)) {
            holder.setData(false);
            holder.setCode(ResultCode.FAIL);
            holder.setMessage("不存在渠道商品");
            log.error(this.getClass().getName() + " 不存在渠道商品 " + JSONObject.toJSONString(request));
            return holder;
        }
        if (SgBChannelProductList.size() > sgChannelProductCalcAndSyncNumber) {
            holder.setData(false);
            holder.setCode(ResultCode.FAIL);
            holder.setMessage("单次计算不能超过" + sgChannelProductCalcAndSyncNumber + "条");
            return holder;
        }

        holder = sgChannelStorageOmsInitService.calcAndSyncChannelProduct(SgBChannelProductList);
        return holder;
    }
}
