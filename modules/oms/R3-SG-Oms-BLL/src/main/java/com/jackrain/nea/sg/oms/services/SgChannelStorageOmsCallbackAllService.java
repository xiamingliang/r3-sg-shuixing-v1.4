package com.jackrain.nea.sg.oms.services;

import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.oms.mapper.SgBChannelSynstockQMapper;
import com.jackrain.nea.sg.oms.model.request.SgChannelStorgeOmsCallbackAllRequest;
import com.jackrain.nea.sg.oms.model.table.SgBChannelSynstockQ;
import com.jackrain.nea.sg.oms.model.tableExtend.SgBChannelSynstockQExtend;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.common.json.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Date;

@Component
@Slf4j
public class SgChannelStorageOmsCallbackAllService {

    /**
     * 全量同步库存,返回状态
     */
    private static final Integer SUCCESS = 1;
    private static final Integer FAIL = 2;

    /**
     * 全量库存回调RPC
     *
     * @param request
     * @return
     */
    @Autowired
    SgBChannelSynstockQMapper sgBChannelSynstockQMapper;

    public ValueHolderV14<Integer> callbackAllRPC(SgChannelStorgeOmsCallbackAllRequest request) throws SQLException, ParseException {
        log.info("=========>>>>>>>>[dsh]全量同步库存回调服务,返回参数为" + request);
        Long id = request.getId();
        Long shopStock = request.getShopStock();
        Integer status = request.getStatus();
        String msg = request.getMsg();
        Integer isagainstock = request.getIsagainstock();
        Integer platForm = request.getPlatForm();
        //唯品会
        if (platForm != null && (platForm.equals(SgBChannelSynstockQExtend.PlatFormCode.WPHJIT) || platForm.equals(SgBChannelSynstockQExtend.PlatFormCode.WPHOXO))) {
            updateStock(shopStock, status, msg, isagainstock, id);
        } else {
            updateStock(shopStock, status, msg, isagainstock, id);
        }

        return new ValueHolderV14<>(ResultCode.SUCCESS, "全量库存回调成功!");
    }

    public void updateStock(long shopStock, int status, String msg, Integer isagainstock, Long id) throws SQLException, ParseException {

        Date modifiedDate = new Date(System.currentTimeMillis());
        SgBChannelSynstockQ sgBChannelSynstockQ = new SgBChannelSynstockQ();
        sgBChannelSynstockQ.setId(id);
        sgBChannelSynstockQ.setQtyStorage(BigDecimal.valueOf(shopStock));
        if (SUCCESS == status) {
            sgBChannelSynstockQ.setSynStatus(SgChannelStorageOmsSynchService.SYNCSUCCESSS);
            sgBChannelSynstockQ.setIsError(SgBChannelSynstockQExtend.SynStatusEnum.SUCCESS);
        } else {
            sgBChannelSynstockQ.setSynStatus(SgChannelStorageOmsSynchService.SYNCFAIL);
            sgBChannelSynstockQ.setIsError(SgBChannelSynstockQExtend.SynStatusEnum.FAIL);
        }
        sgBChannelSynstockQ.setErrorMsg(msg);
        sgBChannelSynstockQ.setModifieddate(modifiedDate);
        if (FAIL == status) {
            sgBChannelSynstockQ.setQtyStorage(BigDecimal.ZERO);
        }
        int i = sgBChannelSynstockQMapper.updateById(sgBChannelSynstockQ);
        if (i > 0) {
            log.info(this.getClass().getName() + " id:{}更新数据库状态成功,更改的状态为{}"
                    ,id,sgBChannelSynstockQ.getSynStatus());
        }
    }

}