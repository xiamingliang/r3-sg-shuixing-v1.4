package com.jackrain.nea.sg.oms.services;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Lists;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.oms.mapper.SgBChannelProductMapper;
import com.jackrain.nea.sg.oms.model.request.SgChannelStorageOmsCalcRequest;
import com.jackrain.nea.sg.oms.model.request.SgChannelStorageOmsInitRequest;
import com.jackrain.nea.sg.oms.model.table.SgBChannelProduct;
import com.jackrain.nea.sg.oms.model.tableExtend.SgBChannelProductExtend;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @Description:全渠道库存扣减服务
 * @Author: chenb
 * @Date: 2019/4/22 13:15
 */
@Component
@Slf4j
public class SgChannelStorageOmsInitService {

    @Autowired
    private SgBChannelProductMapper sgBChannelProductMapper;

    @Autowired
    private SgChannelStorageOmsCalcService gChannelStorageOmsCalcService;
    @Value("${r3.sg.oms.initChannelStorage.channelProductGroupSize:1000}")
    private Integer channelProductGroupSize;

    /**
     * 渠道ID list , sku list, 渠道条码ID list
     * a. 根据入参查询【渠道商品表】, 查询影响的渠道商品数据
     * b. 根据查询到的行数据的渠道ID+渠道条码ID, 逐行重新计算渠道库存
     *
     * @param request
     * @return
     */
    public ValueHolderV14<Integer> initChannelStorage(SgChannelStorageOmsInitRequest request) {
        try {
            SgBChannelProductExtend sgBChannelProductExtend = new SgBChannelProductExtend();
            sgBChannelProductExtend.setCpCShopIdList(request.getCpCShopIdList());
            sgBChannelProductExtend.setPsCSkuIdList(request.getPsCSkuIdList());
            sgBChannelProductExtend.setSkuIdList(request.getSkuIdList());
            QueryWrapper<SgBChannelProduct> queryWrapper = sgBChannelProductExtend.createQueryWrapper();
            List<SgBChannelProduct> sgBChannelProductListSelect = sgBChannelProductMapper.selectList(queryWrapper);
            List<List<SgBChannelProduct>> sgBChannelProductListGroup = Lists.partition(sgBChannelProductListSelect,
                    channelProductGroupSize);
            log.info(this.getClass().getName() + " 渠道库存初始化商品集合条数为" + sgBChannelProductListSelect.size());
            for (List<SgBChannelProduct> sgBChannelProductList : sgBChannelProductListGroup) {
                List<SgChannelStorageOmsCalcRequest> sgChannelStorageOmsCalcRequestList = sgBChannelProductList.stream().map(sgBChannelProduct -> {
                    SgChannelStorageOmsCalcRequest sgChannelStorageOmsCalcRequest = new SgChannelStorageOmsCalcRequest();
                    sgChannelStorageOmsCalcRequest.setSkuId(sgBChannelProduct.getSkuId());//渠道条码ID
                    sgChannelStorageOmsCalcRequest.setCpCShopId(sgBChannelProduct.getCpCShopId());//渠道ID
                    sgChannelStorageOmsCalcRequest.setWareType(sgBChannelProduct.getWareType());//商品类型
                    return sgChannelStorageOmsCalcRequest;
                }).collect(Collectors.toList());
                gChannelStorageOmsCalcService.initChannelStorageOms(sgChannelStorageOmsCalcRequestList);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(this.getClass().getName() + " 初始化渠道库存异常", e);
            return new ValueHolderV14<>(ResultCode.FAIL, "初始化渠道库存失败！");
        }
        return new ValueHolderV14<>(ResultCode.SUCCESS, "初始化渠道库存成功！");
    }


    public ValueHolderV14<Boolean> calcAndSyncChannelProduct(List<SgBChannelProduct> sgBChannelProductListRequest) {
        try {
            List<List<SgBChannelProduct>> sgBChannelProductListGroup = Lists.partition(sgBChannelProductListRequest,
                    channelProductGroupSize);
            log.info(this.getClass().getName() + " 渠道商品计算并同步的集合条数为" + sgBChannelProductListRequest.size());
            for (List<SgBChannelProduct> sgBChannelProductList : sgBChannelProductListGroup) {
                List<SgChannelStorageOmsCalcRequest> sgChannelStorageOmsCalcRequestList = sgBChannelProductList.stream().map(sgBChannelProduct -> {
                    SgChannelStorageOmsCalcRequest sgChannelStorageOmsCalcRequest = new SgChannelStorageOmsCalcRequest();
                    sgChannelStorageOmsCalcRequest.setSkuId(sgBChannelProduct.getSkuId());//渠道条码ID
                    sgChannelStorageOmsCalcRequest.setSourceCpCShopId(-1L);//来源渠道id
                    sgChannelStorageOmsCalcRequest.setCpCShopId(sgBChannelProduct.getCpCShopId());//渠道ID
                    sgChannelStorageOmsCalcRequest.setWareType(sgBChannelProduct.getWareType());//商品类型
                    return sgChannelStorageOmsCalcRequest;
                }).collect(Collectors.toList());
                gChannelStorageOmsCalcService.initChannelStorageOms(sgChannelStorageOmsCalcRequestList);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(this.getClass().getName() + " 计算并同步渠道商品异常", e);
            return new ValueHolderV14<>(false, ResultCode.FAIL, "计算并同步渠道商品失败！");
        }
        return new ValueHolderV14<>(true, ResultCode.SUCCESS, "计算并同步渠道商品成功！");
    }
}


