package com.jackrain.nea.sg.oms.mapper;

import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.oms.model.table.SgBChannelDef;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SgBChannelDefMapper extends ExtentionMapper<SgBChannelDef> {
}