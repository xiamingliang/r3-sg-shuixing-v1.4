package com.jackrain.nea.sg.oms.mapper;

import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.oms.model.table.SgBChannelProduct;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author zhu lin yu
 * @since 2019/5/14
 * create at : 2019/5/14 10:48
 */
@Mapper
public interface SgChannelProductMapper extends ExtentionMapper<SgBChannelProduct> {
}
