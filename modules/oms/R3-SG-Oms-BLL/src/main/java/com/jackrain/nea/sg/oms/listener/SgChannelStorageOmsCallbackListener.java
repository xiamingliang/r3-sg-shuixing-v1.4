package com.jackrain.nea.sg.oms.listener;

import com.aliyun.openservices.ons.api.Action;
import com.aliyun.openservices.ons.api.ConsumeContext;
import com.aliyun.openservices.ons.api.Message;
import com.aliyun.openservices.ons.api.MessageListener;
import com.jackrain.nea.sg.oms.consumer.SgChannelStorageOmsCallbackConsumer;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author zhu lin yu
 * @since 2019/7/25
 * create at : 2019/7/25 19:06
 */
@Slf4j
@Component
public class SgChannelStorageOmsCallbackListener implements MessageListener {

    @Value("${r3.mq.consumer.wms.listener.expr3}")
    String tag;

    @Autowired
    private SgChannelStorageOmsCallbackConsumer sgChannelStorageOmsCallbackConsumer;


    @Override
    public Action consume(Message message, ConsumeContext consumeContext) {

        String messageTag = message.getTag();
        //通用平台渠道库存同步mq消费tag
        if (StringUtils.isNotEmpty(tag) && tag.equals(messageTag)) {
            return sgChannelStorageOmsCallbackConsumer.consume(message);
        }
        return Action.ReconsumeLater;
    }
}
