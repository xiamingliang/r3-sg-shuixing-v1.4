package com.jackrain.nea.sg.oms.mapper;

import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.oms.model.table.SgBChannelManualSyn;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SgBChannelManualSynMapper extends ExtentionMapper<SgBChannelManualSyn> {
}