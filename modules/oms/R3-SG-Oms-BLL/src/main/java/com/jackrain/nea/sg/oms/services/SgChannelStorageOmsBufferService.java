package com.jackrain.nea.sg.oms.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.model.util.ModelUtil;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.utils.StorageUtils;
import com.jackrain.nea.sg.oms.common.OmsResultCode;
import com.jackrain.nea.sg.oms.mapper.SgBChannelProductMapper;
import com.jackrain.nea.sg.oms.mapper.SgBChannelStorageBufferMapper;
import com.jackrain.nea.sg.oms.mapper.SgBChannelStoreMapper;
import com.jackrain.nea.sg.oms.model.request.SgChannelStorageOmsBufferRequest;
import com.jackrain.nea.sg.oms.model.request.SgChannelStorageOmsReduceItemRequest;
import com.jackrain.nea.sg.oms.model.request.SgChannelStorageOmsReduceRequest;
import com.jackrain.nea.sg.oms.model.result.SgChannelStorageOmsReduceResult;
import com.jackrain.nea.sg.oms.model.table.SgBChannelProduct;
import com.jackrain.nea.sg.oms.model.table.SgBChannelStorageBuffer;
import com.jackrain.nea.sg.oms.model.table.SgBChannelStore;
import com.jackrain.nea.sg.oms.model.tableExtend.SgBChannelProductExtend;
import com.jackrain.nea.sg.oms.model.tableExtend.SgBChannelStorageBufferExtend;
import com.jackrain.nea.sg.oms.model.tableExtend.SgBChannelStoreExtend;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.assertj.core.util.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @Description:全渠道库存计算缓存池
 * @Author: chenss
 * @Date: 2019/4/23 13:15
 */
@Component
@Slf4j
public class SgChannelStorageOmsBufferService {

    @Autowired
    private SgBChannelStorageBufferMapper sgBChannelStorageBufferMapper;
    @Autowired
    private SgChannelStorageOmsReduceService sgChannelStorageOmsReduceService;
    @Autowired
    private SgBChannelStoreMapper sgBChannelStoreMapper;
    @Autowired
    private SgBChannelProductMapper sgBChannelProductMapper;

    /**
     * 增加库存计算缓冲数据
     *
     * @param sgBChannelStorageBufferRequestList
     * @return
     */
    public ValueHolderV14<Integer> createChannelStorageOmsBuffer(List<SgChannelStorageOmsBufferRequest> sgBChannelStorageBufferRequestList, String messageTag) {
        if (log.isDebugEnabled()) {
            log.debug("Start SgChannelStorageOmsBufferService.countChannelStorageOmsBuffer[params" + (null == sgBChannelStorageBufferRequestList ? "null" : sgBChannelStorageBufferRequestList.toString()) + "]");
        }
        ValueHolderV14 holder = new ValueHolderV14(ResultCode.SUCCESS, "");
        //校验入参是否合法
        for (SgChannelStorageOmsBufferRequest sgChannelStorageOmsBufferRequest : sgBChannelStorageBufferRequestList) {
            holder = sgChannelStorageOmsBufferRequest.checkCreate();
            if (OmsResultCode.isFail(holder)) {
                log.error("SgChannelStorageOmsBufferService createChannelStorageOmsBuffer message{}",
                        holder.getMessage());
                holder.setCode(OmsResultCode.SUCCESS);
                return holder;
            }
        }
        List<SgBChannelStorageBuffer> sgBChannelStorageBufferList = Lists.newArrayList();
        //筛选库存变化为正数和负数的数据
        List<SgChannelStorageOmsBufferRequest> sgCSOBufferRequestMoreList =
                sgBChannelStorageBufferRequestList.stream().filter(sgBChannelStorageBufferRequest -> BigDecimal.ZERO.compareTo(sgBChannelStorageBufferRequest.getChangeNum()) < 0).collect(Collectors.toList());
        List<SgChannelStorageOmsBufferRequest> sgCSOBufferRequestLessList =
                sgBChannelStorageBufferRequestList.stream().filter(sgBChannelStorageBufferRequest -> BigDecimal.ZERO.compareTo(sgBChannelStorageBufferRequest.getChangeNum()) > 0).collect(Collectors.toList());
        //处理库存变化量为负数
        if (CollectionUtils.isNotEmpty(sgCSOBufferRequestLessList)) {
            ValueHolderV14<List<SgBChannelStorageBuffer>> holderLessResult =
                    handleBufferRequestLessList(sgCSOBufferRequestLessList, messageTag);
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + " 处理库存变化量为负数结果" + JSONObject.toJSONString(holderLessResult));
            }
            if (OmsResultCode.isFail(holderLessResult)) {
                log.error(this.getClass().getName() + "处理库存变量为负数失败" + JSONObject.toJSONString(holderLessResult));
                holder.setMessage(holderLessResult.getMessage());
                holder.setCode(OmsResultCode.SUCCESS);
                return holder;
            }
            sgBChannelStorageBufferList.addAll(holderLessResult.getData());
        }
        //处理库存变化量为正数
        if (CollectionUtils.isNotEmpty(sgCSOBufferRequestMoreList)) {
            ValueHolderV14<List<SgBChannelStorageBuffer>> holderMoreResult =
                    handleBufferRequestMoreList(sgCSOBufferRequestMoreList, messageTag);
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + " 处理库存变化量为正数结果" + JSONObject.toJSONString(holderMoreResult));
            }
            if (OmsResultCode.isFail(holderMoreResult)) {
                holder.setMessage(holderMoreResult.getMessage());
                log.error(this.getClass().getName() + "处理库存变量为负数失败" + JSONObject.toJSONString(holderMoreResult));
                holder.setCode(OmsResultCode.SUCCESS);
                return holder;
            }
            sgBChannelStorageBufferList.addAll(holderMoreResult.getData());
        }

        return addSgBChannelStorageBuffer(sgBChannelStorageBufferList, sgBChannelStorageBufferRequestList);


    }

    /**
     * 插入渠道计算缓存池
     *
     * @param sgBChannelStorageBufferList
     */
    public ValueHolderV14 addSgBChannelStorageBuffer(List<SgBChannelStorageBuffer> sgBChannelStorageBufferList, List<SgChannelStorageOmsBufferRequest> sgBChannelStorageBufferRequestList) {
        ValueHolderV14 holder = new ValueHolderV14(ResultCode.SUCCESS, "");
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + " 新增库存变化计算池设置id,cpcshopId" + JSONObject.toJSONString(sgBChannelStorageBufferList));
        }

        List<SgBChannelStorageBuffer> insertList = new ArrayList<>();

        for (SgBChannelStorageBuffer sgBChannelStorageBuffer : sgBChannelStorageBufferList) {

            //来源渠道ID如果不传则赋值为-1
            if (CollectionUtils.isEmpty(sgBChannelStorageBufferRequestList)) {
                sgBChannelStorageBuffer.setSourceCpCShopId(-1L);
            } else {
                sgBChannelStorageBuffer.setSourceCpCShopId(null ==
                        sgBChannelStorageBufferRequestList.get(0).getSourceCpCShopId() ? -1L : sgBChannelStorageBufferRequestList.get(0).getSourceCpCShopId());
            }

            int result = sgBChannelStorageBufferMapper.selectCount(
                    new QueryWrapper<SgBChannelStorageBuffer>().lambda()
                            .eq(SgBChannelStorageBuffer::getSkuId, sgBChannelStorageBuffer.getSkuId())
                            .eq(SgBChannelStorageBuffer::getCpCShopId, sgBChannelStorageBuffer.getCpCShopId())
                            .eq(SgBChannelStorageBuffer::getSourceCpCShopId, sgBChannelStorageBuffer.getSourceCpCShopId())
                            .eq(SgBChannelStorageBuffer::getDealStatus, 1L));

            if (result < 1) {

                sgBChannelStorageBuffer.setId(ModelUtil.getSequence(SgConstants.SG_B_CHANNEL_STORAGE_BUFFER));
                sgBChannelStorageBuffer.setCreationdate(new Date());

                if (CollectionUtils.isEmpty(sgBChannelStorageBufferRequestList)) {
                    sgBChannelStorageBuffer.setSourceNo("渠道商品添加或更改" + UUID.randomUUID().toString().replaceAll("-", "").substring(0,6));
                } else {
                    // 来源单据号
                    sgBChannelStorageBuffer.setSourceNo(null == sgBChannelStorageBufferRequestList.get(0).getSourceNo() ?
                            "" : sgBChannelStorageBufferRequestList.get(0).getSourceNo());
                }
                sgBChannelStorageBuffer.setAdClientId(SgBChannelStorageBufferExtend.DEFAULT_CLIENT_ID);
                sgBChannelStorageBuffer.setAdOrgId(SgBChannelStorageBufferExtend.DEFAULT_ORG_ID);

                insertList.add(sgBChannelStorageBuffer);
            }
        }

        if (CollectionUtils.isNotEmpty(insertList)) {

            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + " 新增库存计算池" + JSONObject.toJSONString(insertList));
            }

            List<List<SgBChannelStorageBuffer>> insertPageList =
                    StorageUtils.getBaseModelPageList(insertList, SgConstants.SG_COMMON_INSERT_PAGE_SIZE);

            int insert = 0;
            //分页批量更新
            for (List<SgBChannelStorageBuffer> pageList : insertPageList) {
                int resultNum = sgBChannelStorageBufferMapper.batchInsert(pageList);
                insert = resultNum + insert;
            }

            holder.setData(insert);
            holder.setCode(OmsResultCode.SUCCESS);
            holder.setMessage(insert > 0 ? "插入渠道库存计算池成功" : "渠道库存计算池已存在");

        } else {
            holder.setData(0);
            holder.setCode(OmsResultCode.SUCCESS);
        }
        return holder;
    }

    /**
     * 处理库存变化为负数的逻辑
     *
     * @param sgCSOBufferRequestLessList
     * @return
     */
    private ValueHolderV14<List<SgBChannelStorageBuffer>> handleBufferRequestLessList(List<SgChannelStorageOmsBufferRequest> sgCSOBufferRequestLessList, String messageTag) {
        ValueHolderV14<List<SgBChannelStorageBuffer>> holderResult = new ValueHolderV14<>(OmsResultCode.SUCCESS, "");
        //直接扣减渠道库存
        SgChannelStorageOmsReduceRequest sgChannelStorageOmsReduceRequest = new SgChannelStorageOmsReduceRequest();
        //返回list SgBChannelStorageOmsBuffer
        List<SgChannelStorageOmsReduceItemRequest> sgCSORItemList =
                sgCSOBufferRequestLessList.stream().map(sg -> {
                    SgChannelStorageOmsReduceItemRequest sgChannelStorageOmsReduceItemRequest =
                            new SgChannelStorageOmsReduceItemRequest();
                    sgChannelStorageOmsReduceItemRequest.setCpCStoreId(sg.getCpCStoreId());
                    sgChannelStorageOmsReduceItemRequest.setPsCSkuId(sg.getPsCSkuId());
                    sgChannelStorageOmsReduceItemRequest.setQtyStorage(sg.getChangeNum());
                    sgChannelStorageOmsReduceItemRequest.setSourceCpCShopId(sg.getSourceCpCShopId());
                    sgChannelStorageOmsReduceItemRequest.setSourceNo(sg.getSourceNo());
                    return sgChannelStorageOmsReduceItemRequest;
                }).collect(Collectors.toList());
        sgChannelStorageOmsReduceRequest.setItemList(sgCSORItemList);
        log.debug("SgChannelStorageOmsBufferService handleBufferRequestLessList sgChannelStorageOmsReduceRequest{}",
                sgChannelStorageOmsReduceRequest);
        ValueHolderV14<SgChannelStorageOmsReduceResult> holderSCSOR =
                sgChannelStorageOmsReduceService.reductChannelStorageOms(sgChannelStorageOmsReduceRequest, messageTag);
        if (OmsResultCode.isFail(holderSCSOR)) {
            holderResult.setCode(OmsResultCode.FAIL);
            holderResult.setMessage(holderSCSOR.getMessage());
        } else {
            holderResult.setData(holderSCSOR.getData().getSgBChannelStorageBufferList());
        }
        return holderResult;
    }

    /**
     * 处理库存变化为正数的逻辑
     *
     * @param sgCSOBufferRequestMoreList
     * @return
     */
    private ValueHolderV14<List<SgBChannelStorageBuffer>> handleBufferRequestMoreList(List<SgChannelStorageOmsBufferRequest> sgCSOBufferRequestMoreList, String messageTag) {
        ValueHolderV14<List<SgBChannelStorageBuffer>> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        //库存变化量为正数
        List<SgBChannelProduct> sgBChannelProductList = Lists.newArrayList();
                 /*
            根据 逻辑仓ID+"供货比例大于0" 作为查询条件, 查询 【渠道逻辑仓关系表】, 根据返回的数据 聚合渠道ID list
            条件改为筛选逻辑仓IDList + 供货比例大于0
        */
        List<Long> cpCStoreIdList =
                sgCSOBufferRequestMoreList.stream().map(SgChannelStorageOmsBufferRequest::getCpCStoreId).distinct().collect(Collectors.toList());
        SgBChannelStoreExtend sgBChannelStoreExtend = new SgBChannelStoreExtend();
        sgBChannelStoreExtend.setCpCStoreIdList(cpCStoreIdList);
        QueryWrapper<SgBChannelStore> storeQueryWrapper = sgBChannelStoreExtend.createQueryWrapper();
        storeQueryWrapper.gt(SgBChannelStoreExtend.STOCKSCALE, 0);
        log.debug("SgChannelStorageOmsBufferService handleBufferRequestMoreList sgCSOBufferRequestMoreList{}",
                sgBChannelProductList);
        List<SgBChannelStore> sgBChannelStoreList = sgBChannelStoreMapper.selectList(storeQueryWrapper);
        if (CollectionUtils.isEmpty(sgBChannelStoreList)) {
            holder.setMessage("无渠道信息");
            holder.setCode(OmsResultCode.FAIL);
            return holder;
        }
        List<Long> cpCShopIdList = sgBChannelStoreList.stream().map(SgBChannelStore::getCpCShopId).distinct().collect(Collectors.toList());
        /*
            根据渠道ID list和sku查询【渠道商品表】, 返回对应的渠道ID+渠道条码ID list，此集合和来源平台渠道ID构造数据(默认处理状态为1),
            条件改为 渠道ID list 和skulist 查询所有基础数据
         */
        List<Long> psCSkuIdList =
                sgCSOBufferRequestMoreList.stream().map(SgChannelStorageOmsBufferRequest::getPsCSkuId).collect(Collectors.toList());
        SgBChannelProductExtend sgBChannelProductExtend = new SgBChannelProductExtend();
        sgBChannelProductExtend.setCpCShopIdList(cpCShopIdList);
        sgBChannelProductExtend.setPsCSkuIdList(psCSkuIdList);
        QueryWrapper<SgBChannelProduct> productQueryWrapper = sgBChannelProductExtend.createQueryWrapper();
        List<SgBChannelProduct> sgBChannelProductListSelect =
                sgBChannelProductMapper.selectList(productQueryWrapper);
        if (CollectionUtils.isEmpty(sgBChannelProductListSelect)) {
            holder.setMessage("无渠道商品信息");
            holder.setCode(OmsResultCode.FAIL);
            return holder;
        }
        Map<Long, List<SgBChannelStore>> storeMap = sgBChannelStoreList.stream().collect(Collectors.groupingBy(SgBChannelStore::getCpCStoreId));
        for (SgChannelStorageOmsBufferRequest sgBSBRequest : sgCSOBufferRequestMoreList) {
            List<SgBChannelStore> sgBChannelStores = storeMap.get(sgBSBRequest.getCpCStoreId());
            if (CollectionUtils.isEmpty(sgBChannelStores)) {
                holder.setMessage("无渠道信息");
                holder.setCode(ResultCode.FAIL);
                return holder;
            }
            List<Long> cpsShopIdSelectList =
                    sgBChannelStores.stream().map(SgBChannelStore::getCpCShopId).collect(Collectors.toList());
            List<SgBChannelProduct> sgBChannelProductListFilter =
                    sgBChannelProductListSelect.stream().filter(sgbcProduct -> sgbcProduct.getPsCSkuId().equals(sgBSBRequest.getPsCSkuId())).filter(sgbcProduct -> cpsShopIdSelectList.contains(sgbcProduct.getCpCShopId())).collect(Collectors.toList());
            sgBChannelProductList.addAll(sgBChannelProductListFilter);
        }
        List<SgBChannelStorageBuffer> sgBChannelStorageBufferList = SgBChannelStorageBufferExtend.parseChannelProductToCSBuffer(sgBChannelProductList, messageTag);
        holder.setData(sgBChannelStorageBufferList);
        return holder;
    }

    /**
     * 获取库存计算缓存池数据计数
     *
     * @param sgBChannelStorageBufferExtend
     * @return
     */
    public ValueHolderV14<Integer> countChannelStorageOmsBuffer(SgBChannelStorageBufferExtend sgBChannelStorageBufferExtend) {
        if (log.isDebugEnabled()) {
            log.debug("Start SgChannelStorageOmsBufferService.countChannelStorageOmsBuffer[params" + sgBChannelStorageBufferExtend.toString() + "]");
        }
        ValueHolderV14<Integer> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        QueryWrapper<SgBChannelStorageBuffer> queryWrapper = sgBChannelStorageBufferExtend.createQueryWrapper();
        holder.setData(sgBChannelStorageBufferMapper.selectCount(queryWrapper));
        return holder;
    }

    /**
     * 按照ID顺序获取库存计算缓存池数据
     *
     * @param sgBChannelStorageBufferExtend
     * @return
     */
    public ValueHolderV14<List<SgBChannelStorageBuffer>> getChannelStorageOmsBuffer(SgBChannelStorageBufferExtend sgBChannelStorageBufferExtend) {
        if (log.isDebugEnabled()) {
            log.debug("Start SgChannelStorageOmsBufferService.getChannelStorageOmsBuffer " +
                    "sgBChannelStorageBufferExtend{}", sgBChannelStorageBufferExtend);
        }
        ValueHolderV14<List<SgBChannelStorageBuffer>> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        holder.setData(Lists.newArrayList());
        QueryWrapper<SgBChannelStorageBuffer> queryWrapper = sgBChannelStorageBufferExtend.createQueryWrapper();
        if (queryWrapper.getExpression() == null || CollectionUtils.isEmpty(queryWrapper.getExpression().getNormal())) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage("不支持全表查询");
            return holder;
        }
        queryWrapper.orderByAsc(SgBChannelStorageBufferExtend.ID);
        if (sgBChannelStorageBufferExtend.getListSize() != null) {
            queryWrapper.last("limit " + sgBChannelStorageBufferExtend.getListSize());
        }
        List<SgBChannelStorageBuffer> sgBChannelStorageBufferList =
                sgBChannelStorageBufferMapper.selectList(queryWrapper);
        holder.setData(sgBChannelStorageBufferList);
        return holder;
    }

    /**
     * 更新库存计算缓存池数据状态
     *
     * @param sgBChannelStorageBufferExtend
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public ValueHolderV14<Integer> updateChannelStorageOmsBufferStatus(SgBChannelStorageBufferExtend sgBChannelStorageBufferExtend) {
        if (log.isDebugEnabled()) {
            log.debug("Start SgChannelStorageOmsBufferService.updateChannelStorageOmsBufferStatus[params" + sgBChannelStorageBufferExtend.toString() + "]");
        }
        ValueHolderV14<Integer> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        QueryWrapper<SgBChannelStorageBuffer> queryWrapper = sgBChannelStorageBufferExtend.createQueryWrapper();
        if (queryWrapper.getExpression() == null || CollectionUtils.isEmpty(queryWrapper.getExpression().getNormal())) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage("不支持全表更新");
            return holder;
        } else if (sgBChannelStorageBufferExtend.getUpdateDealStatus() == null) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage("请传入要更新的状态");
            return holder;
        }
        SgBChannelStorageBuffer sgBChannelStorageBuffer = new SgBChannelStorageBuffer();
        sgBChannelStorageBuffer.setDealStatus(sgBChannelStorageBufferExtend.getUpdateDealStatus());
        log.info(this.getClass().getName() + " 开始跟新渠道库存计算缓冲池状态，id为{}，状态为{}"
                , sgBChannelStorageBufferExtend.getBaseExtend().getIdList(), sgBChannelStorageBufferExtend.getUpdateDealStatus());
        holder.setData(sgBChannelStorageBufferMapper.update(sgBChannelStorageBuffer, queryWrapper));
        return holder;
    }
}


