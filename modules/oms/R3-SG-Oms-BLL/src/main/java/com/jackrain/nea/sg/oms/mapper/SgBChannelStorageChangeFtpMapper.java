package com.jackrain.nea.sg.oms.mapper;

import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.oms.model.table.SgBChannelStorageChangeFtp;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SgBChannelStorageChangeFtpMapper extends ExtentionMapper<SgBChannelStorageChangeFtp> {
}