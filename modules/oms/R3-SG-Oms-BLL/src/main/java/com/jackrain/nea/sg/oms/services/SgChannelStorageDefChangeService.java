package com.jackrain.nea.sg.oms.services;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.model.util.ModelUtil;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.common.SystemUserResource;
import com.jackrain.nea.sg.oms.mapper.SgBChannelDefMapper;
import com.jackrain.nea.sg.oms.mapper.SgBChannelStoreMapper;
import com.jackrain.nea.sg.oms.model.request.SgChannelStorageDefChangeRequest;
import com.jackrain.nea.sg.oms.model.table.SgBChannelDef;
import com.jackrain.nea.sg.oms.model.tableExtend.SgBChannelDefExtend;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;



/**
 * @author 杜胜辉
 * 新增或更新渠道信息表服务同时，新增、修改逻辑仓关系表
 */
@Component
@Slf4j
public class SgChannelStorageDefChangeService {

    @Autowired
    SgBChannelDefMapper sgBChannelDefMapper;
    @Autowired
    SgBChannelStoreMapper sgBChannelStoreMapper;

    public ValueHolderV14 changeSbGChannelDef(SgChannelStorageDefChangeRequest request) {
        log.info(this.getClass().getName()+" 新增渠道信息dsh,供黄超使用，入参为："+request);
        SgBChannelDefExtend sgBChannelDefExtend = new SgBChannelDefExtend();
        sgBChannelDefExtend.setCpCShopId(request.getId());
        QueryWrapper<SgBChannelDef> queryWrapper = sgBChannelDefExtend.createQueryWrapper();
        List<SgBChannelDef> sgBChannelDefList = sgBChannelDefMapper.selectList(queryWrapper);
        if (sgBChannelDefList.size() > 1) {
            return new ValueHolderV14(ResultCode.FAIL, "根据平台店铺ID: " + sgBChannelDefList.get(0).getCpCShopId() + ", 查询到多条渠道信息数据, 请维护!");
        }
        //根据店铺ID查询渠道信息表,无则返回错误信息,有责修改
        if (CollectionUtils.isEmpty(sgBChannelDefList) && !request.getIsAdd()) {
            return new ValueHolderV14(ResultCode.FAIL, "根据渠道ID查询不到渠道信息!");
        } else {
            SgBChannelDef sgBChannelDef = sgBChannelDefList.get(0);
            sgBChannelDef.setQtySafe(request.getLowStock() == null ? sgBChannelDef.getQtySafe() : request.getLowStock());//安全库存数
            sgBChannelDef.setStockScale(request.getStockScale() == null ? sgBChannelDef.getStockScale() : request.getStockScale());//库存比例
            sgBChannelDef.setCpCPlatformId(request.getPlatForm() == null ? sgBChannelDef.getCpCPlatformId() : request.getPlatForm()+"");//平台类型
            sgBChannelDef.setCpCCustomerId(request.getCpCCustomerId() == null ? sgBChannelDef.getCpCCustomerId() : request.getCpCCustomerId());//经销商ID
            sgBChannelDef.setCpCCustomerEcode(request.getCpCCustomerEcode() == null ? sgBChannelDef.getCpCCustomerEcode() : request.getCpCCustomerEcode());//经销商编码
            sgBChannelDef.setCpCCustomerEname(request.getCpCCustomerEname() == null ? sgBChannelDef.getCpCCustomerEname() : request.getCpCCustomerEname());//经销商名称
            //sgBChannelDef.setCpCShopId(request.getId());//平台店铺ID
            sgBChannelDef.setCpCShopType(request.getShopType() == null ? sgBChannelDef.getCpCShopType() : request.getShopType());//店铺类型
            sgBChannelDef.setCpCShopSellerNick(request.getSellerNick() == null ? sgBChannelDef.getCpCShopSellerNick() : request.getSellerNick());//卖家昵称
            sgBChannelDef.setIsSyn(request.getIsSyncStock() == null ? sgBChannelDef.getIsSyn() : request.getIsSyncStock());//是否同步
            sgBChannelDef.setModifieddate(new Date());//修改时间
            sgBChannelDef.setCpCShopTitle(request.getCpCShopTitle() == null ? sgBChannelDef.getCpCShopTitle() : request.getCpCShopTitle());
            sgBChannelDef.setCpCShopChannelType(request.getChannelType() == null ? sgBChannelDef.getCpCShopChannelType() : request.getChannelType());
            int i = sgBChannelDefMapper.updateById(sgBChannelDef);
            if (i < 0) {
                return new ValueHolderV14(ResultCode.FAIL, "修改渠道信息表数据失败!");
            }
            return new ValueHolderV14(ResultCode.SUCCESS, "修改渠道信息表数据成功!");
        }
    }

    public ValueHolderV14 addSbGChannelDef(SgChannelStorageDefChangeRequest request) {
        log.info(this.getClass().getName()+" 新增渠道信息dsh,供俊磊使用入参为："+request);

        SgBChannelDefExtend sgBChannelDefExtend = new SgBChannelDefExtend();
        sgBChannelDefExtend.setCpCShopId(request.getId());
        QueryWrapper<SgBChannelDef> queryWrapper = sgBChannelDefExtend.createQueryWrapper();
        List<SgBChannelDef> sgBChannelDefList = sgBChannelDefMapper.selectList(queryWrapper);
        if (sgBChannelDefList.size() > 1) {
            return new ValueHolderV14(ResultCode.FAIL, "根据平台店铺ID: " + sgBChannelDefList.get(0).getCpCShopId() + ", 查询到多条渠道信息数据, 请维护!");
        }
        User rootUser = SystemUserResource.getRootUser();
        //根据店铺ID查询渠道信息表,无则返回错误信息,有责修改
        if (CollectionUtils.isEmpty(sgBChannelDefList)) {
            SgBChannelDef newSgBChannelDef = new SgBChannelDef();
            newSgBChannelDef.setId(ModelUtil.getSequence(SgConstants.SG_B_CHANNEL_DEF));
            newSgBChannelDef.setQtySafe(request.getLowStock());//安全库存数
            newSgBChannelDef.setStockScale(request.getStockScale());//库存比例
            newSgBChannelDef.setCpCPlatformId(request.getPlatForm()+"");//平台类型
            newSgBChannelDef.setCpCCustomerId(request.getCpCCustomerId());//经销商ID
            newSgBChannelDef.setCpCCustomerEcode(request.getCpCCustomerEcode());//经销商编码
            newSgBChannelDef.setCpCCustomerEname(request.getCpCCustomerEname());//经销商名称
            newSgBChannelDef.setCpCShopId(request.getId());//平台店铺ID
            newSgBChannelDef.setCpCShopType(request.getShopType());//店铺类型
            newSgBChannelDef.setCpCShopSellerNick(request.getSellerNick());//卖家昵称
            newSgBChannelDef.setIsSyn(request.getIsSyncStock());//是否同步
            newSgBChannelDef.setCreationdate(new Date());//创建时间
            newSgBChannelDef.setOwnerename(rootUser.getEname());
            newSgBChannelDef.setOwnerid(rootUser.getId().longValue());
            newSgBChannelDef.setCpCShopTitle(request.getCpCShopTitle());
            newSgBChannelDef.setCpCShopChannelType(request.getChannelType());
            newSgBChannelDef.setAdOrgId(27L);//默认赋值
            newSgBChannelDef.setAdClientId(37L);//默认赋值

            newSgBChannelDef.setAdClientId(Integer.valueOf(rootUser.getClientId()).longValue());
            newSgBChannelDef.setAdOrgId(Integer.valueOf(rootUser.getOrgId()).longValue());
            newSgBChannelDef.setOwnerid(rootUser.getId().longValue());
            newSgBChannelDef.setModifierid(rootUser.getId().longValue());
            newSgBChannelDef.setCreationdate(new Date());
            newSgBChannelDef.setOwnername(rootUser.getName());
            newSgBChannelDef.setOwnerename(rootUser.getEname());
            int i = sgBChannelDefMapper.insert(newSgBChannelDef);
            if (i > 0) {
                return new ValueHolderV14(ResultCode.SUCCESS, "新增渠道信息成功!");
            }
        } else {
            SgBChannelDef sgBChannelDef = sgBChannelDefList.get(0);
            sgBChannelDef.setQtySafe(request.getLowStock() == null ? sgBChannelDef.getQtySafe() : request.getLowStock());//安全库存数
            sgBChannelDef.setStockScale(request.getStockScale() == null ? sgBChannelDef.getStockScale() : request.getStockScale());//库存比例
            sgBChannelDef.setCpCPlatformId(request.getPlatForm() == null ? sgBChannelDef.getCpCPlatformId() : request.getPlatForm()+"");//平台类型
            sgBChannelDef.setCpCCustomerId(request.getCpCCustomerId() == null ? sgBChannelDef.getCpCCustomerId() : request.getCpCCustomerId());//经销商ID
            sgBChannelDef.setCpCCustomerEcode(request.getCpCCustomerEcode() == null ? sgBChannelDef.getCpCCustomerEcode() : request.getCpCCustomerEcode());//经销商编码
            sgBChannelDef.setCpCCustomerEname(request.getCpCCustomerEname() == null ? sgBChannelDef.getCpCCustomerEname() : request.getCpCCustomerEname());//经销商名称
            sgBChannelDef.setCpCShopType(request.getShopType() == null ? sgBChannelDef.getCpCShopType() : request.getShopType());//店铺类型
            sgBChannelDef.setCpCShopSellerNick(request.getSellerNick() == null ? sgBChannelDef.getCpCShopSellerNick() : request.getSellerNick());//卖家昵称
            sgBChannelDef.setIsSyn(request.getIsSyncStock() == null ? sgBChannelDef.getIsSyn() : request.getIsSyncStock());//是否同步
            sgBChannelDef.setModifieddate(new Date());//修改时间
            sgBChannelDef.setCpCShopTitle(request.getCpCShopTitle() == null ? sgBChannelDef.getCpCShopTitle() : request.getCpCShopTitle());
            sgBChannelDef.setCpCShopChannelType(request.getChannelType() == null ? sgBChannelDef.getCpCShopChannelType() : request.getChannelType());

            sgBChannelDef.setModifieddate(new Date());
            sgBChannelDef.setModifiername(rootUser.getName());
            sgBChannelDef.setModifierename(rootUser.getEname());
            int i = sgBChannelDefMapper.updateById(sgBChannelDef);
            if (i < 0) {
                return new ValueHolderV14(ResultCode.FAIL, "修改渠道信息表数据失败!");
            }
            return new ValueHolderV14(ResultCode.SUCCESS, "修改渠道信息表数据成功!");
        }
        return new ValueHolderV14(ResultCode.SUCCESS, "操作成功!");
    }


}
