package com.jackrain.nea.sg.oms.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * @author caopengflying
 * @time 2019/6/16 11:37
 */
@Configuration
@EnableAsync
public class InitChannelStorageExecutorConfig {
    /** Set the ThreadPoolExecutor's core pool size. */
    @Value("${r3.sg.oms.initChannelStorage.corePoolSize:10}")
    private Integer corePoolSize;
    /** Set the ThreadPoolExecutor's maximum pool size. */
    @Value("${r3.sg.oms.initChannelStorage.maxPoolSize:20}")
    private Integer maxPoolSize;
    /** Set the capacity for the ThreadPoolExecutor's BlockingQueue. */
    @Value("${r3.sg.oms.initChannelStorage.queueCapacity:10}")
    private Integer queueCapacity;


    /** Set the ThreadPoolExecutor's core pool size. */
    @Value("${r3.sg.update_sg_bill_es_data.corePoolSize:100}")
    private Integer esCorePoolSize;
    /** Set the ThreadPoolExecutor's maximum pool size. */
    @Value("${r3.sg.update_sg_bill_es_data.maxPoolSize:200}")
    private Integer esMaxPoolSize;
    /** Set the capacity for the ThreadPoolExecutor's BlockingQueue. */
    @Value("${r3.sg.update_sg_bill_es_data.queueCapacity:10}")
    private Integer esQueueCapacity;

    @Bean
    public Executor myAsync() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(corePoolSize);
        executor.setMaxPoolSize(maxPoolSize);
        executor.setQueueCapacity(queueCapacity);
        executor.setThreadNamePrefix("UpdateSgBillEsData-");
        // rejection-policy：当pool已经达到max size的时候，如何处理新任务
        // CALLER_RUNS：不在新线程中执行任务，而是有调用者所在的线程来执行
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        executor.initialize();
        return executor;
    }

    @Bean
    public Executor esAsync() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(esCorePoolSize);
        executor.setMaxPoolSize(esMaxPoolSize);
        executor.setQueueCapacity(esQueueCapacity);
        executor.setThreadNamePrefix("UpdateSgBillESData-");
        // rejection-policy：当pool已经达到max size的时候，如何处理新任务
        // CALLER_RUNS：不在新线程中执行任务，而是有调用者所在的线程来执行
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        executor.initialize();
        return executor;
    }
}
