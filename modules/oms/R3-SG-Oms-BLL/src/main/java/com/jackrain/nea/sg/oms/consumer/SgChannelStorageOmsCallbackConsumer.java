package com.jackrain.nea.sg.oms.consumer;

import com.alibaba.fastjson.JSON;
import com.aliyun.openservices.ons.api.Action;
import com.aliyun.openservices.ons.api.Message;
import com.aliyun.openservices.ons.api.impl.util.MsgConvertUtil;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.oms.common.OmsResultCode;
import com.jackrain.nea.sg.oms.model.request.StandplatRequest;
import com.jackrain.nea.sg.oms.services.SgChannelStorageOmsCallbackService;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author zhu lin yu
 * @since 2019/7/25
 * create at : 2019/7/25 19:15
 */
@Slf4j
@Component
public class SgChannelStorageOmsCallbackConsumer {

    @Autowired
    private SgChannelStorageOmsCallbackService sgChannelStorageOmsCallbackService;

    public Action consume(Message message) {
        String messageTopic = message.getTopic();
        String messageKey = message.getKey();
        String messageTag = message.getTag();
        try {
            String messageBody = MsgConvertUtil.objectDeserialize(message.getBody()).toString();
            log.info(this.getClass().getName() + " 通用平台渠道库存同步回调服务接收MQ消息 messageBody{}," +
                            "messageTopic{}," +
                            "messageKey{}," +
                            "messageTag{}",
                    new Object[]{messageBody, messageTopic, messageKey, messageTag});
            Action var19 = Action.CommitMessage;

            List<StandplatRequest> standplatRequestList;
            try {
                standplatRequestList = JSON.parseArray(messageBody, StandplatRequest.class);
            } catch (Exception e) {
                e.printStackTrace();
                log.error(this.getClass().getName() + " 解析MQ消息失败" +
                        "ResultMessage={};" +
                        "Topic={};" +
                        "Body={};" +
                        "Key={};Tag={}", new Object[]{e, messageTopic, messageKey, messageBody, messageTag});
                return var19;
            }

            ValueHolderV14<Integer> valueHolderV14 = sgChannelStorageOmsCallbackService.callbackRPCForPDDandSN(standplatRequestList);
            //如果处理失败则MQ重新消费
            if (OmsResultCode.isFail(valueHolderV14)) {
                log.error(this.getClass().getName() + " 通用平台渠道库存回传信息处理失败" +
                        "ResultMessage={};" +
                        "Topic={};" +
                        "Body={};" +
                        "Key={};Tag={}", new Object[]{valueHolderV14.getMessage(), messageTopic, messageKey,
                        messageBody, messageTag});
                var19 = Action.ReconsumeLater;
            }
            return var19;
        } catch (Exception var16) {
            var16.printStackTrace();
            log.error(this.getClass().getName() + " 通用平台渠道库存回传信息处理失败", var16);
            throw new NDSException(var16);
        }
    }
}
