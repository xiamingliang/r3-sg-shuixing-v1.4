package com.jackrain.nea.sg.oms.mapper;

import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.oms.model.table.SgBChannelStorageBuffer;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface SgBChannelStorageBufferMapper extends ExtentionMapper<SgBChannelStorageBuffer> {
    @Insert("<script> " +
            "insert into sg_b_channel_storage_buffer(id,cp_c_shop_id,cp_c_shop_title,sku_id,deal_status," +
            "creationdate, ad_org_id,ad_client_id, source_no,ware_type,source_cp_c_shop_id)"+
            "select id,cpCShopId,cpCShopTitle,skuId,dealStatus,creationdate,adOrgId,adClientId,sourceNo,wareType," +
            "sourceCpCShopId from ("+
            "<foreach item='item' index='index' collection='sgBChannelStorageBufferList' separator='union all' > " +
            "( select ${item.id} AS id," +
            "${item.cpCShopId} AS cpCShopId, " +
            "'${item.cpCShopTitle}' AS cpCShopTitle, " +
            "'${item.skuId}' AS skuId, " +
            "${item.dealStatus} AS dealStatus, " +
            "'${item.creationdate}'::TIMESTAMP AS creationdate, "+
            "${item.adOrgId} AS adOrgId, "+
            "${item.adClientId} AS adClientId, "+
            "'${item.sourceNo}' AS sourceNo, "+
            " ${item.wareType} AS wareType, "+
            "${item.sourceCpCShopId} AS sourceCpCShopId FROM sg_b_channel_product "+
            "limit 1)"+
            "</foreach>"+
            ") a WHERE NOT EXISTS (SELECT * FROM sg_b_channel_storage_buffer sg WHERE sg.CP_C_SHOP_ID = a.cpCShopId " +
            "AND sg.SKU_ID = a.skuId AND sg.SOURCE_CP_C_SHOP_ID = a.sourceCpCShopId AND sg.DEAL_STATUS = 1)"+
            " </script> ")
    Integer insertSgBChannelStorageBufferList(@Param("sgBChannelStorageBufferList") List<SgBChannelStorageBuffer> sgBChannelStorageBufferList);

}