package com.jackrain.nea.sg.oms.services;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.oms.common.OmsResultCode;
import com.jackrain.nea.sg.oms.mapper.SgBChannelProductMapper;
import com.jackrain.nea.sg.oms.mapper.SgBChannelStoreMapper;
import com.jackrain.nea.sg.oms.model.request.SgChannelStorageOmsChangeRequest;
import com.jackrain.nea.sg.oms.model.request.SgChannelStorageOmsReduceItemRequest;
import com.jackrain.nea.sg.oms.model.request.SgChannelStorageOmsReduceRequest;
import com.jackrain.nea.sg.oms.model.result.SgChannelStorageOmsChangeResult;
import com.jackrain.nea.sg.oms.model.result.SgChannelStorageOmsReduceResult;
import com.jackrain.nea.sg.oms.model.table.SgBChannelProduct;
import com.jackrain.nea.sg.oms.model.table.SgBChannelStorageBuffer;
import com.jackrain.nea.sg.oms.model.table.SgBChannelStore;
import com.jackrain.nea.sg.oms.model.tableExtend.SgBChannelProductExtend;
import com.jackrain.nea.sg.oms.model.tableExtend.SgBChannelStorageBufferExtend;
import com.jackrain.nea.sg.oms.model.tableExtend.SgBChannelStoreExtend;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.assertj.core.util.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @Description:全渠道库存扣减服务
 * @Author: chenb
 * @Date: 2019/4/22 13:15
 */
@Component
@Slf4j
public class SgChannelStorageOmsReduceService {

    @Autowired
    private SgBChannelStoreMapper sgBChannelStoreMapper;

    @Autowired
    private SgBChannelProductMapper sgBChannelProductMapper;

    @Autowired
    private SgChannelStorageOmsService sgChannelStorageOmsService;

    /**
     * @param request
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public ValueHolderV14<SgChannelStorageOmsReduceResult> reductChannelStorageOms(SgChannelStorageOmsReduceRequest request,String messageTag) {
        ValueHolderV14<SgChannelStorageOmsReduceResult> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        SgChannelStorageOmsReduceResult sgChannelStorageOmsReduceResult = new SgChannelStorageOmsReduceResult();
        if (CollectionUtils.isEmpty(request.getItemList())) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage("请传入渠道库存扣减记录");
            return holder;
        }
        // 过滤行记录无效的数据
        List<SgChannelStorageOmsReduceItemRequest> itemList = request.getItemList();
        itemList = itemList.stream().filter(x -> x.getQtyStorage() != null && x.getQtyStorage().compareTo(BigDecimal.ZERO) != 0
                && x.getCpCStoreId() != null && x.getPsCSkuId() != null).collect(Collectors.toList());
        if (CollectionUtils.isEmpty(itemList)) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage("无有效的渠道库存扣减记录");
            return holder;
        }
        List<Long> psCSkuIdList = itemList.stream().map(SgChannelStorageOmsReduceItemRequest::getPsCSkuId).distinct().collect(Collectors.toList());
        List<Long> cpCStoreIdList = itemList.stream().map(SgChannelStorageOmsReduceItemRequest::getCpCStoreId).distinct().collect(Collectors.toList());
        /*
            根据 逻辑仓ID List+"供货比例大于0" 作为查询条件, 查询 【渠道逻辑仓关系表】, 根据返回的数据 聚合渠道ID list
        */
        SgBChannelStoreExtend sgBChannelStoreExtend = new SgBChannelStoreExtend();
        sgBChannelStoreExtend.setCpCStoreIdList(cpCStoreIdList);
        QueryWrapper<SgBChannelStore> storeQueryWrapper = sgBChannelStoreExtend.createQueryWrapper();
        storeQueryWrapper.gt(SgBChannelStoreExtend.STOCKSCALE, 0);
        List<SgBChannelStore> sgBChannelStoreList = sgBChannelStoreMapper.selectList(storeQueryWrapper);
        if (CollectionUtils.isEmpty(sgBChannelStoreList)) {
            log.error(this.getClass().getName() + " 逻辑仓无对应渠道 " +
                            "cpCStoreIdList{}",
                    cpCStoreIdList);
            holder.setCode(ResultCode.FAIL);
            holder.setMessage("逻辑仓无对应渠道");
            return holder;
        }
        /*
            获取渠道商品集合
         */
        List<Long> cpCShopIdList = sgBChannelStoreList.stream().map(SgBChannelStore::getCpCShopId).distinct().collect(Collectors.toList());
        SgBChannelProductExtend sgBChannelProductExtend = new SgBChannelProductExtend();
        sgBChannelProductExtend.setCpCShopIdList(cpCShopIdList);
        sgBChannelProductExtend.setPsCSkuIdList(psCSkuIdList);
        QueryWrapper<SgBChannelProduct> productQueryWrapper = sgBChannelProductExtend.createQueryWrapper();
        List<SgBChannelProduct> sgBChannelProductList = sgBChannelProductMapper.selectList(productQueryWrapper);
        if (CollectionUtils.isEmpty(sgBChannelProductList)) {
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + " 无渠道商品信息 ");
            }
            holder.setCode(ResultCode.FAIL);
            holder.setMessage("无渠道商品信息");
            return holder;
        }
        Map<Long, List<SgBChannelStore>> storeMap = sgBChannelStoreList.stream().collect(Collectors.groupingBy(SgBChannelStore::getCpCStoreId));
        List<Long> reduceProductIdList = Lists.newArrayList();// 有影响的渠道商品ID
        List<SgChannelStorageOmsReduceItemRequest> successList = Lists.newArrayList();
        for (SgChannelStorageOmsReduceItemRequest item : itemList) {
            /*
            根据渠道ID list和sku查询【渠道商品表】, 返回对应的渠道ID+渠道条码ID list，此集合和来源平台渠道ID构造数据(默认处理状态为1),
            */
            if (!storeMap.containsKey(item.getCpCStoreId())) {
                log.error(this.getClass().getName() + " SgChannelStorageOmsReduceService reductChannelStorageOms 无对应渠道",
                        new Object[]{item.getCpCStoreId()});
                continue;// 该明细记录无对应渠道
            }
            List<SgBChannelStore> singleSgBChannelStoreList = storeMap.get(item.getCpCStoreId());
            request.setSingleCpCShopIdList(singleSgBChannelStoreList.stream().map(SgBChannelStore::getCpCShopId).distinct().collect(Collectors.toList()));
            List<SgBChannelProduct> singleSgBChannelProductList = sgBChannelProductList.stream().filter(x -> request.getSingleCpCShopIdList().contains(x.getCpCShopId()) && item.getPsCSkuId().equals(x.getPsCSkuId())).collect(Collectors.toList());
            if (CollectionUtils.isEmpty(singleSgBChannelProductList)) {
                if (log.isDebugEnabled()) {
                    log.debug(this.getClass().getName() + " 无对应渠道商品信息 ");
                }
                continue;// 该明细记录无渠道商品信息
            }
            reduceProductIdList.addAll(singleSgBChannelProductList.stream().map(SgBChannelProduct::getId).collect(Collectors.toList()));
            for (SgBChannelProduct sgBChannelProduct : singleSgBChannelProductList) {
                // 执行具体的扣减方法
                try {
                    SgChannelStorageOmsChangeRequest sgChannelStorageOmsChangeRequest = new SgChannelStorageOmsChangeRequest();
                    sgChannelStorageOmsChangeRequest.setCpCShopId(sgBChannelProduct.getCpCShopId());
                    sgChannelStorageOmsChangeRequest.setSkuSpec(sgBChannelProduct.getSkuSpec());
                    sgChannelStorageOmsChangeRequest.setSkuId(sgBChannelProduct.getSkuId());
                    sgChannelStorageOmsChangeRequest.setQtyStorage(item.getQtyStorage());
                    sgChannelStorageOmsChangeRequest.setSourceCpCShopId(item.getSourceCpCShopId());
                    sgChannelStorageOmsChangeRequest.setSourceNo(item.getSourceNo());
                    ValueHolderV14<SgChannelStorageOmsChangeResult> changeResult = sgChannelStorageOmsService.changeStorage(sgChannelStorageOmsChangeRequest);
                    if (OmsResultCode.isSuccess(changeResult)) {
                        successList.add(item);
                    }
                } catch (Exception e) {
                    log.error(this.getClass().getName() + "扣减库存失败exception{}", e);
                    throw new NDSException(e);
                }
            }
        }
        // 拼接返回结果
        if (CollectionUtils.isNotEmpty(reduceProductIdList)) {
            // 有影响的渠道商品行记录
            sgBChannelProductList = sgBChannelProductList.stream().filter(x -> reduceProductIdList.contains(x.getId())).collect(Collectors.toList());
            List<SgBChannelStorageBuffer> sgBChannelStorageBufferList = SgBChannelStorageBufferExtend.parseChannelProductToCSBuffer(sgBChannelProductList,messageTag);
            sgChannelStorageOmsReduceResult.setSgBChannelStorageBufferList(sgBChannelStorageBufferList);
        }
        sgChannelStorageOmsReduceResult.setSuccessList(successList);
        holder.setData(sgChannelStorageOmsReduceResult);
        return holder;
    }
}


