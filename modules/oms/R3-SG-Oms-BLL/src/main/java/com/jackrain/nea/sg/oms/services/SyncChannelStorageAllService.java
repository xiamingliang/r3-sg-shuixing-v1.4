package com.jackrain.nea.sg.oms.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.ac.sc.basic.model.request.DistributionQueryRequest;
import com.jackrain.nea.ac.sc.core.api.AcDistributionQueryCmd;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.cpext.model.table.CpShop;
import com.jackrain.nea.data.basic.model.request.ShopQueryRequest;
import com.jackrain.nea.data.basic.services.BasicCpQueryService;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.ip.api.FullQuantityCmd;
import com.jackrain.nea.ip.model.FullQtyDataDetailIpModel;
import com.jackrain.nea.ip.model.FullQuantityIpModel;
import com.jackrain.nea.ip.model.result.FullQuantityResult;
import com.jackrain.nea.model.util.AdParamUtil;
import com.jackrain.nea.model.util.ModelUtil;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.utils.StorageUtils;
import com.jackrain.nea.sg.oms.common.OmsConstantsIF;
import com.jackrain.nea.sg.oms.common.OmsResultCode;
import com.jackrain.nea.sg.oms.mapper.SgBChannelProductMapper;
import com.jackrain.nea.sg.oms.mapper.SgBChannelStorageMapper;
import com.jackrain.nea.sg.oms.mapper.SgBChannelSynstockQMapper;
import com.jackrain.nea.sg.oms.model.request.SgStorageOmsSynchronousRequest;
import com.jackrain.nea.sg.oms.model.result.SgStorageOmsSynchronousResult;
import com.jackrain.nea.sg.oms.model.table.SgBChannelProduct;
import com.jackrain.nea.sg.oms.model.table.SgBChannelStorage;
import com.jackrain.nea.sg.oms.model.table.SgBChannelSynstockQ;
import com.jackrain.nea.sg.oms.model.tableExtend.SgBChannelProductExtend;
import com.jackrain.nea.sg.oms.model.tableExtend.SgBChannelStorageExtend;
import com.jackrain.nea.sg.oms.model.tableExtend.SgBChannelSynstockQExtend;
import com.jackrain.nea.sg.out.common.SgOutConstantsIF;
import com.jackrain.nea.st.api.SellOwngoodsQueryCmd;
import com.jackrain.nea.st.model.request.SellOwngoodsQueryRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.dubbo.config.annotation.Reference;
import org.assertj.core.util.Lists;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

@Component
@Slf4j
public class SyncChannelStorageAllService {

    @Autowired
    private SgBChannelStorageMapper sgBChannelStorageMapper;
    @Autowired
    private SgChannelStorageOmsSynchService sgChannelStorageOmsSynchService;
    @Autowired
    private SgBChannelSynstockQMapper sgBChannelSynstockQMapper;

    @Autowired
    private SgBChannelProductMapper sgBChannelProductMapper;

    @Autowired
    private BasicCpQueryService basicCpQueryService;

    @Reference(protocol = "dubbo", validation = "true", version = "1.0", group = "st")
    private SellOwngoodsQueryCmd sellOwngoodsQueryCmd;

    @Reference(protocol = "dubbo", validation = "true", version = "1.0", group = "ac-sc")
    private AcDistributionQueryCmd acDistributionQueryCmd;

    /**
     * 全量/手工同步
     *
     * @param request
     * @return
     */
    @Async
    public ValueHolderV14<SgStorageOmsSynchronousResult> synchronousStock2(SgStorageOmsSynchronousRequest request, Boolean isNeedSleep) {
        /**
         * a. 按渠道ID查询【渠道库存表】, 获取所有需要同步的【渠道库存表】行数据
         * b. 构造数据, 批量调用【增量同步库存】
         */
        if (log.isDebugEnabled()) {
            log.debug("Start SgChannelStorageOmsSynchService.synchronousStock2[params" + request.toString() + "]");
        }
        SgBChannelStorageExtend sgBChannelStorageExtend = new SgBChannelStorageExtend();
        sgBChannelStorageExtend.setCpCShopId(request.getCpCShopId());
        QueryWrapper<SgBChannelStorage> queryWrapper = sgBChannelStorageExtend.createQueryWrapper();
        List<SgBChannelStorage> sgBChannelStorageList = sgBChannelStorageMapper.selectList(queryWrapper);
        if (CollectionUtils.isEmpty(sgBChannelStorageList)) {
            return new ValueHolderV14<>(ResultCode.FAIL, "根据渠道ID[" + request.getCpCShopId() + "]+查询不到需要同步的数据!");
        }
        String batchno = (System.currentTimeMillis() + "" + (int) (Math.random() * 9000 + 1000));
        return syncChannelStorage(sgBChannelStorageList, isNeedSleep, batchno);
    }

    @Async
    public ValueHolderV14 syncChannelStorage(List<SgBChannelStorage> sgBChannelStorageList, Boolean isNeedSleep,
                                             String batchno) {
        // 过滤无需同步的渠道库存表
        ValueHolderV14 holderV = new ValueHolderV14<>(ResultCode.SUCCESS, "增量同步成功");
        List<SgBChannelSynstockQ> syncList = new ArrayList<>();
        SgStorageOmsSynchronousRequest synchronousRequest = new SgStorageOmsSynchronousRequest();
        SgBChannelSynstockQ sgBChannelSynstockQ = new SgBChannelSynstockQ();

        if (CollectionUtils.isEmpty(sgBChannelStorageList)) {
            holderV.setCode(ResultCode.FAIL);
            holderV.setMessage("syncChannelStorage服务入参不能为空");
            return holderV;
        }

        List<Long> skuIdList = sgBChannelStorageList.stream()
                .filter(x -> x.getPsCSkuId() != null && x.getCpCShopId() != null)
                .map(SgBChannelStorage::getPsCSkuId)
                .collect(toList());

        Long cpCShopId = sgBChannelStorageList.get(0).getCpCShopId();
        ShopQueryRequest shopQueryRequest = new ShopQueryRequest();
        shopQueryRequest.setShopIds(Lists.newArrayList(cpCShopId));

        //分类各种商品
        List<Long> directlyProSkuIdList = new ArrayList<>();
        List<Long> ownProSkuIdList = new ArrayList<>();
        List<Long> noOwnProSkuIdList = new ArrayList<>();
        List<Long> noDistributionProSkuIdList = new ArrayList<>();
        List<Long> distributionProSkuIdList = new ArrayList<>();

        try {

            //调用查询店铺信息接口
            log.info(this.getClass().getName() + " 开始调用店铺查询接口,店铺id:{}", cpCShopId);
            HashMap<Long, CpShop> shopInfo = basicCpQueryService.getShopInfo(shopQueryRequest);
            CpShop cpShop = shopInfo.get(cpCShopId);

            //如果没查到信息,直接返回
            if (cpShop == null || StringUtils.isEmpty(cpShop.getChannelType())) {
                log.error(this.getClass().getName() + " 未查到渠道类型,店铺id:{}", cpCShopId);
                holderV.setCode(ResultCode.FAIL);
                holderV.setMessage("店铺id为" + cpCShopId + "未查到渠道类型");
                return holderV;
            } else {
                if (OmsConstantsIF.SHOP_CHANNEL_TYPE_DIRECTLY.equals(cpShop.getChannelType())) {
                    directlyProSkuIdList.addAll(skuIdList);
                } else if (OmsConstantsIF.SHOP_CHANNEL_TYPE_DISTRIBUTION.equals(cpShop.getChannelType())) {

                    //调用经销商自有商品策略服务,判断是否自有商品，不是则调用代销分销策略服务
                    SellOwngoodsQueryRequest sellOwngoodsQueryRequest = new SellOwngoodsQueryRequest();
                    sellOwngoodsQueryRequest.setEffectiveDate(new Date());
                    sellOwngoodsQueryRequest.setCpCShopId(cpCShopId);

                    ValueHolderV14<List<Long>> listValueHolderV14 =
                            sellOwngoodsQueryCmd.selectSkuBySellOwngoods(sellOwngoodsQueryRequest);

                    if (log.isDebugEnabled()) {
                        log.debug(this.getClass().getName() + " 调用selectSkuBySellOwngoods方法返回值:{}"
                                , JSONObject.toJSONString(listValueHolderV14));
                    }

                    if (ResultCode.FAIL == listValueHolderV14.getCode()) {
                        holderV.setCode(ResultCode.FAIL);
                        holderV.setMessage(listValueHolderV14.getMessage());
                        log.error(this.getClass().getName() + " 调用经销商自有商品策略服务失败,入参:{},失败信息:{}"
                                , JSONObject.toJSONString(sellOwngoodsQueryRequest), listValueHolderV14.getMessage());
                        return holderV;
                    } else {
                        List<Long> data = listValueHolderV14.getData();
                        if (CollectionUtils.isNotEmpty(data)) {
                            ownProSkuIdList = skuIdList.stream().filter(data::contains).collect(toList());
                            noOwnProSkuIdList = skuIdList.stream().filter(x -> !data.contains(x)).collect(toList());
                        } else {
                            log.info(this.getClass().getName() + " shopId为:{}的店铺无经销商自由商品", cpCShopId);
                            noOwnProSkuIdList.addAll(skuIdList);
                        }

                        //调用代销分销策略服务
                        DistributionQueryRequest distributionQueryRequest = new DistributionQueryRequest();
                        distributionQueryRequest.setEffectiveDate(new Date());
                        distributionQueryRequest.setCpCShopId(cpCShopId);
                        ValueHolderV14<List<Long>> holderV14 = acDistributionQueryCmd.selectSkuByDistribution(distributionQueryRequest);

                        if (log.isDebugEnabled()) {
                            log.info(this.getClass().getName() + " 调用selectSkuByDistribution方法返回值:{}"
                                    , JSONObject.toJSONString(holderV14));
                        }

                        if (ResultCode.FAIL == holderV14.getCode()) {
                            holderV.setCode(ResultCode.FAIL);
                            holderV.setMessage(holderV14.getMessage());
                            log.error(this.getClass().getName() + " 调用代销分销策略服务失败,入参:{},失败信息:{}"
                                    , JSONObject.toJSONString(distributionQueryRequest), holderV14.getMessage());
                            return holderV;
                        } else {
                            List<Long> result = holderV14.getData();
                            if (CollectionUtils.isEmpty(result)) {
                                log.info(this.getClass().getName() + " shopId为:{}的店铺无分销代销商品", cpCShopId);
                                noDistributionProSkuIdList.addAll(noOwnProSkuIdList);
                            } else {
                                distributionProSkuIdList = noOwnProSkuIdList.stream()
                                        .filter(result::contains).collect(Collectors.toList());
                                noDistributionProSkuIdList = noOwnProSkuIdList.stream()
                                        .filter(x -> !result.contains(x)).collect(Collectors.toList());
                            }
                        }
                    }
                } else {
                    log.error(this.getClass().getName() + " 未知的渠道类型,店铺id:{}", cpCShopId);
                }
            }
        } catch (Exception e) {
            log.error(this.getClass().getName() + " 判断直营分销策略异常", e);
            throw new NDSException(e);
        }

        //设置批次号  拿当前时间到微秒 然后加4位随机数
        for (SgBChannelStorage sgBChannelStorage : sgBChannelStorageList) {
            Long skuId = sgBChannelStorage.getPsCSkuId();
            synchronousRequest.setCpCShopId(sgBChannelStorage.getCpCShopId());
            synchronousRequest.setSkuId(sgBChannelStorage.getSkuId());
            int isSyncFlag = sgChannelStorageOmsSynchService.whetherSynchronization(sgBChannelStorage.getCpCShopId(), sgBChannelStorage.getSkuId());

            sgBChannelSynstockQ = new SgBChannelSynstockQ();
            BeanUtils.copyProperties(sgBChannelStorage, sgBChannelSynstockQ);
            //设置主键
            sgBChannelSynstockQ.setId(ModelUtil.getSequence(SgConstants.SG_B_CHANNEL_SYNSTOCK_Q));
            //设置批次号  拿当前时间到微秒 然后加4位随机数
            sgBChannelSynstockQ.setBatchno(batchno);
            sgBChannelSynstockQ.setCpCPlatformId(sgBChannelStorage.getCpCPlatformId() == null ? null : sgBChannelStorage.getCpCPlatformId().longValue());//平台
            sgBChannelSynstockQ.setQtyStorage(sgBChannelStorage.getQty());//同步库存数
            sgBChannelSynstockQ.setSellerNick(sgBChannelStorage.getCpCShopSellerNick());//卖家昵称
            sgBChannelSynstockQ.setCreationdate(new Date());
            sgBChannelSynstockQ.setModifieddate(null);
            if (isSyncFlag > 0) {
                if (directlyProSkuIdList.contains(skuId) || distributionProSkuIdList.contains(skuId)) {
                    sgBChannelSynstockQ.setSynStatus(SgChannelStorageOmsSynchService.INSYNC);
                } else if (ownProSkuIdList.contains(skuId)) {
                    sgBChannelSynstockQ.setSynStatus(SgChannelStorageOmsSynchService.NOSYNC);
                    sgBChannelSynstockQ.setErrorMsg("分销店铺的经销商自有商品");
                } else if (noDistributionProSkuIdList.contains(skuId)) {
                    sgBChannelSynstockQ.setSynStatus(SgChannelStorageOmsSynchService.NOSYNC);
                    sgBChannelSynstockQ.setErrorMsg("分销店铺的非分销代销商品");
                }
            } else {
                sgBChannelSynstockQ.setSynStatus(SgChannelStorageOmsSynchService.NOSYNC);
                if (OmsConstantsIF.IS_SYNC_FLAG_CHANNEL_NOSYNC == isSyncFlag) {
                    sgBChannelSynstockQ.setErrorMsg("无需同步,该渠道设置为不同步");
                } else if (OmsConstantsIF.IS_SYNC_FLAG_LOCK_SKU == isSyncFlag) {
                    sgBChannelSynstockQ.setErrorMsg("无需同步,请查看锁条码策略");
                } else if (OmsConstantsIF.IS_SYNC_FLAG_LOCK_STORE == isSyncFlag) {
                    sgBChannelSynstockQ.setErrorMsg("无需同步,请查看锁库策略");
                } else if (OmsConstantsIF.IS_SYNC_FLAG_NO_CHANNEL_INFO == isSyncFlag) {
                    sgBChannelSynstockQ.setErrorMsg("无需同步,无该渠道信息");
                }
            }
            sgBChannelSynstockQ.setAdOrgId(27L);//默认赋值
            sgBChannelSynstockQ.setAdClientId(37L);//默认赋值
            syncList.add(sgBChannelSynstockQ);
            log.info(this.getClass().getName() + " 开始插入同步队列表,批次号为{}", batchno);

        }
        // 还存在需要同步的渠道库存数据,继续后续逻辑
        if (CollectionUtils.isEmpty(syncList)) {
            log.info(this.getClass().getName() + " 全量库存同步时，查询ES后无需要同步的数据!");
            return new ValueHolderV14<>(ResultCode.FAIL, "全量库存同步时，查询ES后无需要同步的数据!");
        }
        List<List<SgBChannelSynstockQ>> baseModelPageList = StorageUtils.getBaseModelPageList(syncList, 200);
        for (List<SgBChannelSynstockQ> sgBChannelSynstockQList : baseModelPageList) {
            int i = sgBChannelSynstockQMapper.batchInsert(sgBChannelSynstockQList);
            if (i <= 0) {
                return new ValueHolderV14<>(ResultCode.FAIL, "全量/手工同步库存失败!");
            }
        }


        /**
         * 构造数据, 调用RPC同步服务(全量)
         */
        //平台为拼多多和苏宁或舞象云
        List<SgBChannelSynstockQ> sgBChannelSynstockQSForPDDandSN = syncList.stream().filter(x -> (SgOutConstantsIF.PLATFORMTYPE_PINDUODUO.equals(x.getCpCPlatformId().toString())
                || SgOutConstantsIF.PLATFORMTYPE_SUNING.equals(x.getCpCPlatformId().toString())
                || SgOutConstantsIF.PLATFORMTYPE_WUXIANGYUN.equals(x.getCpCPlatformId().toString()))
                && x.getSynStatus().equals(SgChannelStorageOmsSynchService.INSYNC)).collect(Collectors.toList());

        //平台为淘宝和京东
        List<SgBChannelSynstockQ> sgBChannelSynstockQSForTBandJD = syncList.stream().filter(
                x -> (SgOutConstantsIF.PLATFORMTYPE_TAOBAO.equals(x.getCpCPlatformId().toString())
                        || SgOutConstantsIF.PLATFORMTYPE_JINGDONG.equals(x.getCpCPlatformId().toString())
                        || SgOutConstantsIF.PLATFORMTYPE_TAOBAO_JINGXIAO.equals(x.getCpCPlatformId().toString())
                        || SgOutConstantsIF.PLATFORMTYPE_TAOBAO_FENXIAO.equals(x.getCpCPlatformId().toString()))
                        && Objects.equals(x.getSynStatus(), SgChannelStorageOmsSynchService.INSYNC)).collect(Collectors.toList());

        ValueHolderV14 valueHolderV14 = new ValueHolderV14();
        if (CollectionUtils.isNotEmpty(sgBChannelSynstockQSForPDDandSN)) {
            valueHolderV14 = sgChannelStorageOmsSynchService.stockSyncRPCPddAndSn(sgBChannelSynstockQSForPDDandSN);
        }

        if (OmsResultCode.isFail(valueHolderV14)) {
            return new ValueHolderV14<>(ResultCode.FAIL, "全量调用拼多多苏宁PRC同步库存服务失败!");
        }

        if (CollectionUtils.isNotEmpty(sgBChannelSynstockQSForTBandJD)) {
            valueHolderV14 = stockSyncRPCAll(sgBChannelSynstockQSForTBandJD, isNeedSleep, OmsConstantsIF.AUTO_SNYC);
        }

        if (OmsResultCode.isFail(valueHolderV14)) {
            return new ValueHolderV14<>(ResultCode.FAIL, "全量调用淘宝天猫PRC同步库存服务失败!");
        }
        return new ValueHolderV14<>(ResultCode.SUCCESS, "全量/手工同步成功！");
    }

    /**
     * 全量/手工调用RPC服务
     *
     * @return
     */
    public ValueHolderV14<Integer> stockSyncRPCAll(List<SgBChannelSynstockQ> syncList, Boolean isNeedSleep, String syncFlag) {
        if (log.isDebugEnabled()) {
            log.debug("Start SgChannelStorageOmsSynchService.synchronousStock2[params" + syncList.toString() + "]");
        }
        //获取配置参数
        String syncChannelStorageMaxNum = AdParamUtil.getParam("syncChannelStorageMaxNum");
        int syncStorageMaxNum = StringUtils.isEmpty(syncChannelStorageMaxNum) ? 200 : Integer.valueOf(syncChannelStorageMaxNum);
        String syncChannelStorageAllSleepTime = AdParamUtil.getParam("syncChannelStorageAllSleepTime");
        long syncStorageAllSleepTime = StringUtils.isEmpty(syncChannelStorageAllSleepTime) ? 5000L : Long.valueOf(syncChannelStorageAllSleepTime);

        /**
         * 获取RPC服务  全量调用RPC服务
         */
        FullQuantityCmd fullQuantityCmd = (FullQuantityCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(),
                FullQuantityCmd.class.getName(),
                SgBChannelSynstockQExtend.GROUPNAME, SgBChannelSynstockQExtend.VERSION);
        FullQuantityIpModel fullQuantityIpModel = new FullQuantityIpModel();
        List<FullQuantityIpModel.FullQtysynModel> fullQtysynModelList = new ArrayList<>();

        for (SgBChannelSynstockQ channelSynstockQ : syncList) {
            FullQtyDataDetailIpModel fullQtyDataDetailIpModel = new FullQtyDataDetailIpModel();//明细
            FullQuantityIpModel.FullQtysynModel fullQtysynModel = new FullQuantityIpModel.FullQtysynModel();
            fullQtysynModel.setSendTime(System.currentTimeMillis());
            fullQtysynModel.setPlatform(channelSynstockQ.getCpCPlatformId());//平台
            fullQtysynModel.setSellerNick(channelSynstockQ.getSellerNick());//卖家昵称
            fullQtysynModel.setBatchNo(channelSynstockQ.getBatchno());
            fullQtysynModel.setNumIID(channelSynstockQ.getNumiid());
            fullQtysynModel.setDataSourceType(syncFlag);

            //apiType  单例还是批量 1：批量，2：单条  与永晨商定当skuid为空时即为单条,或者为批量
            if (StringUtils.isEmpty(channelSynstockQ.getSkuId())) {
                fullQtysynModel.setApiType(SgChannelStorageOmsSynchService.SINGLE);
            } else {
                fullQtysynModel.setApiType(SgChannelStorageOmsSynchService.BATCH);
            }
            fullQtyDataDetailIpModel.setId(channelSynstockQ.getId());
            fullQtyDataDetailIpModel.setBarCode(channelSynstockQ.getPsCSkuEcode());//商品条码
            fullQtyDataDetailIpModel.setExtSkuID(channelSynstockQ.getSkuId());

            fullQtyDataDetailIpModel.setQuantity(channelSynstockQ.getQtyStorage() == null ? null : channelSynstockQ.getQtyStorage().intValue());//库存数量
            fullQtyDataDetailIpModel.setShopStock(channelSynstockQ.getQtyStorage() == null ? null : channelSynstockQ.getQtyStorage().longValue());//库存数量
            if (SgOutConstantsIF.PLATFORMTYPE_TAOBAO_FENXIAO.equals(channelSynstockQ.getCpCPlatformId().toString())
                    || SgOutConstantsIF.PLATFORMTYPE_TAOBAO_JINGXIAO.equals(channelSynstockQ.getCpCPlatformId().toString())) {
                //如果是分销平台，则查询渠道商品表中的skuSpec
                SgBChannelProductExtend sgBChannelProductExtend = new SgBChannelProductExtend();
                sgBChannelProductExtend.setCpCShopId(channelSynstockQ.getCpCShopId());
                sgBChannelProductExtend.setSkuId(channelSynstockQ.getSkuId());
                QueryWrapper<SgBChannelProduct> productQueryWrapper = sgBChannelProductExtend.createQueryWrapper();
                SgBChannelProduct sgBChannelProduct = sgBChannelProductMapper.selectOne(productQueryWrapper);

                fullQtyDataDetailIpModel.setProperties(sgBChannelProduct.getSkuSpec());
            }
            if (StringUtils.isNotEmpty(channelSynstockQ.getProperties())) {
                String[] properties = channelSynstockQ.getProperties().split("\\|");//属性
                if (properties.length == 3) {
                    fullQtyDataDetailIpModel.setCooperationNo(Integer.valueOf(properties[1]));//常态合作编号 徐永晨定
                    fullQtyDataDetailIpModel.setWareHouse(properties[2]);
                    fullQtyDataDetailIpModel.setBarCode(properties[0]);
                }
            }
            fullQtysynModel.setDataDetails(Lists.newArrayList(fullQtyDataDetailIpModel));
            fullQtysynModelList.add(fullQtysynModel);
        }
        //按照numID与platform进行分组
        Map<String, List<FullQuantityIpModel.FullQtysynModel>> listMap = fullQtysynModelList.stream().collect(Collectors.groupingBy(d -> {
            return d.getNumIID() + ":" + d.getPlatform();
        }));
        for (String s : listMap.keySet()) {
            List<FullQuantityIpModel.FullQtysynModel> fullQtysynModels = listMap.get(s);
            //分组数不要超过200,否则进行拆分
            int i = 1;
            List<List<FullQuantityIpModel.FullQtysynModel>> baseModelPageList = getBaseModelPageList(fullQtysynModels, syncStorageMaxNum);
            for (List<FullQuantityIpModel.FullQtysynModel> fullQtysynModelLimit : baseModelPageList) {
                fullQuantityIpModel.setFullQtysynModels(fullQtysynModelLimit);
                log.info(">>>>>>========[dsh]全量调用RPC服务,分批次:" + i + ",参数为:" + fullQtysynModelLimit);
                fullQuantityIpModel.setFullQtysynModels(fullQtysynModelLimit);
                ValueHolderV14<List<FullQuantityResult>> valueHolder = null;
                try {
                    valueHolder = fullQuantityCmd.fullQuantity(fullQuantityIpModel);
                    List<FullQuantityResult> data = valueHolder.getData();
                    log.info(this.getClass().getName() + " 调用全量同步RPC服务返回参数为：" + JSONObject.toJSONString(data));
                    if (isNeedSleep) {
                        Thread.sleep(syncStorageAllSleepTime);
                    }
                } catch (Exception e) {
                    log.info(this.getClass().getName() + " 全量调用RPC服务失败,分批次: " + i + "错误信息为：" + e);
                }
                log.info(">>>>>>========[dsh]全量调用RPC服务,分批次:" + i + ",返回值为:" + valueHolder);
                i++;
            }
        }

        return new ValueHolderV14<>(ResultCode.SUCCESS, "全量调用PRC同步库存服务成功!");
    }

    public List<List<FullQuantityIpModel.FullQtysynModel>> getBaseModelPageList
            (List<FullQuantityIpModel.FullQtysynModel> sourceList, int pageSize) {

        List<List<FullQuantityIpModel.FullQtysynModel>> resultList = new ArrayList<>();

        if (CollectionUtils.isEmpty(sourceList)) {
            return resultList;
        }

        int listSize = sourceList.size();
        int page = listSize / pageSize;

        if (listSize % pageSize != 0) {
            page++;
        }

        for (int i = 0; i < page; i++) {

            List<FullQuantityIpModel.FullQtysynModel> pageList = sourceList.subList(i * pageSize,
                    (((i + 1) * pageSize > listSize ? listSize : pageSize * (i + 1))));
            resultList.add(pageList);
        }

        return resultList;
    }
}
