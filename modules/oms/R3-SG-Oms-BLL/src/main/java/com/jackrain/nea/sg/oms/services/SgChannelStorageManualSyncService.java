package com.jackrain.nea.sg.oms.services;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.google.common.collect.Lists;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.basic.utils.R3ParamUtil;
import com.jackrain.nea.sg.oms.common.OmsConstantsIF;
import com.jackrain.nea.sg.oms.mapper.SgBChannelDefMapper;
import com.jackrain.nea.sg.oms.mapper.SgBChannelManualSynItemMapper;
import com.jackrain.nea.sg.oms.mapper.SgBChannelManualSynMapper;
import com.jackrain.nea.sg.oms.model.table.SgBChannelDef;
import com.jackrain.nea.sg.oms.model.table.SgBChannelManualSyn;
import com.jackrain.nea.sg.oms.model.table.SgBChannelManualSynItem;
import com.jackrain.nea.sg.oms.model.table.SgBChannelSynstockQ;
import com.jackrain.nea.sg.out.common.SgOutConstantsIF;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

/**
 * @author zhu lin yu
 * @since 2019-10-16
 * create at : 2019-10-16 15:00
 */
@Component
@Slf4j
public class SgChannelStorageManualSyncService {

    @Autowired
    private SgBChannelManualSynMapper sgBChannelManualSynMapper;

    @Autowired
    private SgBChannelManualSynItemMapper sgBChannelManualSynItemMapper;

    @Autowired
    private SgBChannelDefMapper sgBChannelDefMapper;

    @Autowired
    private SyncChannelStorageAllService syncChannelStorageAllService;

    @Value("${r3.sg.oms.initChannelStorage.maxPoolSize:20}")
    private Integer maxPoolSize;


    /**
     * 渠道库存(手工导入)同步
     *
     * @param ids  渠道库存手工导入的主表id集合
     * @param user 操作用户
     * @return 处理结果
     */
    public ValueHolder manualSync(JSONArray ids, User user) {
        long startTime = System.currentTimeMillis();

        if (log.isDebugEnabled()) {
            log.debug("Start SgChannelStorageManualSyncService.manualSync.ReceiveParams:,param:{}", JSONObject.toJSONString(ids));
        }

        ValueHolder valueHolder = new ValueHolder();
        if (CollectionUtils.isEmpty(ids)) {
            throw new NDSException("请求参数不能为空");
        }
        Locale locale = user.getLocale();
        JSONArray errData = new JSONArray();
        int totalFail = 0;
        int total = ids.size();

        //循环处理，按店铺循环处理
        for (Object id : ids) {
            try {
                dataEncapsulationAndSync(id, ids, user);
            } catch (Exception e) {
                JSONObject errInfo = new JSONObject();
                errInfo.put("message", e.getMessage());
                errInfo.put("objid", id);
                errData.add(errInfo);
                totalFail++;
            }
        }

        if (log.isDebugEnabled()) {
            log.debug("Finish SgChannelStorageManualSyncService.manualSync.Spend Time:{}", System.currentTimeMillis() - startTime);
        }

        if (totalFail > 0) {
            valueHolder.put("code", ResultCode.FAIL);
            valueHolder.put("message", Resources.getMessage("同步成功记录数 :" + (total - totalFail) + "条,同步失败记录数 :" + totalFail + "条", locale));
            valueHolder.put("data", errData);
            return valueHolder;
        }

        return R3ParamUtil.convertV14(new ValueHolderV14(ResultCode.SUCCESS, "正在全量同步中!具体同步信息请查看同步状态!"));
    }


    /**
     * 数据封装，封装成同步库存方法入参,多线程调用库存同步方法
     *
     * @param id 店铺id
     */
    private void dataEncapsulationAndSync(Object id, JSONArray ids, User user) {

        boolean isNeedSleep = false;

        long objId = Long.parseLong(id.toString());
        //查询主表信息
        SgBChannelManualSyn sgBChannelManualSyn = this.sgBChannelManualSynMapper
                .selectOne(new QueryWrapper<SgBChannelManualSyn>()
                        .lambda().eq(SgBChannelManualSyn::getId, objId));
        AssertUtils.notNull(sgBChannelManualSyn, "未查到主表信息!");

        //通过店铺id查询平台类型(现在支持淘宝和京东)
        Long cpCShopId = sgBChannelManualSyn.getCpCShopId();
        SgBChannelDef sgBChannelDef = this.sgBChannelDefMapper
                .selectOne(new QueryWrapper<SgBChannelDef>()
                        .lambda().eq(SgBChannelDef::getCpCShopId, cpCShopId));
        String cpCPlatformId = sgBChannelDef.getCpCPlatformId();
        AssertUtils.isTrue(SgOutConstantsIF.PLATFORMTYPE_TAOBAO.equals(cpCPlatformId)
                || SgOutConstantsIF.PLATFORMTYPE_JINGDONG.equals(cpCPlatformId), "不支持的渠道类型!");

        if ("淘宝店".equals(sgBChannelDef.getCpCShopType())) {
            isNeedSleep = true;
        }

        //查出该店铺下所有要同步的库存数据
        List<SgBChannelManualSynItem> sgBChannelManualSynItemList = this.sgBChannelManualSynItemMapper
                .selectList(new QueryWrapper<SgBChannelManualSynItem>()
                        .lambda().eq(SgBChannelManualSynItem::getSgBChannelManualSynId, objId));
        AssertUtils.isTrue(CollectionUtils.isNotEmpty(sgBChannelManualSynItemList), "未查到明细表信息!");


        try {

            //数据切分，按照每个店铺分到的线程数，算出切分数量
            int size;
            if (sgBChannelManualSynItemList.size() > maxPoolSize) {
                size = sgBChannelManualSynItemList.size() / (maxPoolSize / ids.size());
            } else {
                size = sgBChannelManualSynItemList.size();
            }

            //按size把数据分批次
            List<List<SgBChannelManualSynItem>> partition = Lists.partition(sgBChannelManualSynItemList, size);
            for (List<SgBChannelManualSynItem> sgBChannelSynStockQPartition : partition) {
                int i = 1;
                log.info(this.getClass().getName() + " 开始第{}批全量手工导入库存同步,共{}批,店铺id为{}", i, partition.size(), objId);

                try {

                    //多线程调用库存同步服务
                    ValueHolderV14 valueHolderV14 = ManualSyncChannelStorageAll(sgBChannelSynStockQPartition, sgBChannelDef, isNeedSleep, user, cpCPlatformId);
                    if (ResultCode.FAIL == valueHolderV14.getCode()) {
                        log.error(this.getClass().getName() + " 第{}批全量手工导入库存同步失败,共{}批,店铺id为{}",
                                i, partition.size(), objId, valueHolderV14.getMessage());
                    }

                    log.info(this.getClass().getName() + " 结束第{}批全量手工导入库存同步,共{}批,店铺id为{},返回结果为{}",
                            i, partition.size(), objId, JSONObject.toJSONString(valueHolderV14));

                } catch (Exception e) {
                    log.error(this.getClass().getName() + " 第{}批全量手工导入库存同步失败,共{}批,店铺id为{}",
                            i, partition.size(), objId, e);
                }
                i++;
            }
        } catch (Exception e) {
            log.error(this.getClass().getName() + " 全量手工导入库存同步失败,店铺id为{}", objId, e);
        }
    }


    @Async("myAsync")
    public ValueHolderV14 ManualSyncChannelStorageAll(List<SgBChannelManualSynItem> sgBChannelManualSynItemList, SgBChannelDef sgBChannelDef, Boolean isNeedSleep, User user, String cpCPlatformId) {

        String batchNo = (System.currentTimeMillis() + "" + (int) (Math.random() * 9000 + 1000));

        //跟新状态为已同步
        List<Long> idList = sgBChannelManualSynItemList.stream().map(SgBChannelManualSynItem::getId).collect(Collectors.toList());
        SgBChannelManualSynItem sgBChannelManualSynItemUpdate = new SgBChannelManualSynItem();
        sgBChannelManualSynItemUpdate.setSynStatus(SgChannelStorageOmsSynchService.INSYNC);
        sgBChannelManualSynItemUpdate.setModifierid(Long.valueOf(user.getId()));
        sgBChannelManualSynItemUpdate.setModifiername(user.getName());
        sgBChannelManualSynItemUpdate.setModifieddate(new Date());
        sgBChannelManualSynItemMapper
                .update(sgBChannelManualSynItemUpdate, new UpdateWrapper<SgBChannelManualSynItem>()
                        .lambda().in(SgBChannelManualSynItem::getId, idList));

        //封装成同步库存方法(云枢纽)的入参
        List<SgBChannelSynstockQ> sgBChannelSynstockQList = new ArrayList<>();

        for (SgBChannelManualSynItem sgBChannelManualSynItem : sgBChannelManualSynItemList) {
            SgBChannelSynstockQ sgBChannelSynstockQ = new SgBChannelSynstockQ();

            BeanUtils.copyProperties(sgBChannelManualSynItem, sgBChannelSynstockQ);
            sgBChannelSynstockQ.setCpCPlatformId(Long.parseLong(cpCPlatformId));
            sgBChannelSynstockQ.setBatchno(batchNo);
            sgBChannelSynstockQ.setSellerNick(sgBChannelDef.getCpCShopSellerNick());
            sgBChannelSynstockQList.add(sgBChannelSynstockQ);
        }

        return this.syncChannelStorageAllService.stockSyncRPCAll(sgBChannelSynstockQList, isNeedSleep, OmsConstantsIF.MANUAL_SNYC);
    }
}
