package com.jackrain.nea.sg.oms.mapper;

import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.oms.model.table.SgBChannelStorage;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import java.math.BigDecimal;

@Mapper
public interface SgBChannelStorageMapper extends ExtentionMapper<SgBChannelStorage> {

    /**
     * 根据XACT锁更新渠道库存信息
     *
     * @param id 库存查询表ID
     */
    @Update(" update sg_b_channel_storage " +
            " set qty = #{qty},modifieddate=now() " +
            " where id = #{id} and qty = #{sourceQty} and pg_try_advisory_xact_lock(#{prefix}+id) ")
    Integer updateQtyWithXactLock(@Param("id") Long id, @Param("sourceQty") BigDecimal sourceQty,
                                  @Param("qty") BigDecimal qty, @Param("prefix") Long prefix);
}