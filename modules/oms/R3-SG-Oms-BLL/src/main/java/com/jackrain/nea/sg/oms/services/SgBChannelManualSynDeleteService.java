package com.jackrain.nea.sg.oms.services;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.Feature;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.oms.mapper.SgBChannelManualSynItemMapper;
import com.jackrain.nea.sg.oms.mapper.SgBChannelManualSynMapper;
import com.jackrain.nea.sg.oms.model.table.SgBChannelManualSynItem;
import com.jackrain.nea.st.utils.ValueHolderUtils;
import com.jackrain.nea.sys.CommandAdapter;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.query.DefaultWebEvent;
import com.jackrain.nea.web.query.QuerySession;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Author chenjinjun
 * @Description 手工导入店铺库存删除接口
 * @Date 2019-10-21
 **/
@Slf4j
@Component
public class SgBChannelManualSynDeleteService extends CommandAdapter {
    @Autowired
    private SgBChannelManualSynMapper sgBChannelManualSynMapper;
    @Autowired
    private SgBChannelManualSynItemMapper sgBChannelManualSynItemMapper;

    @Override
    public ValueHolder execute(QuerySession session) throws NDSException {
        DefaultWebEvent event = session.getEvent();
        JSONObject param = JSON.parseObject(JSON.toJSONStringWithDateFormat(
                event.getParameterValue("param"), "yyyy-MM-dd HH:mm:ss"), Feature.OrderedField);
        Boolean isDel = param.getBoolean("isdelmtable");
        Long objid = param.getLong("objid");

        JSONObject tabitem = param.getJSONObject("tabitem");
        JSONArray errorArray = new JSONArray();
        try {
            //判断是删除主表还是明细表单独删除
            if (!isDel) {
                //单独删除明细
                JSONArray itemArray = tabitem.getJSONArray("SG_B_CHANNEL_MANUAL_SYN_ITEM");
                if (CollectionUtils.isNotEmpty(itemArray)) {
                    SgBChannelManualSynDeleteService service = ApplicationContextHandle.getBean(SgBChannelManualSynDeleteService.class);
                    service.delItem(itemArray);
                }
            } else {
                //删除主表
                SgBChannelManualSynDeleteService service = ApplicationContextHandle.getBean(SgBChannelManualSynDeleteService.class);
                service.delMain(objid, errorArray);
            }
        } catch (Exception e) {
            return ValueHolderUtils.getFailValueHolder(e.getMessage());
        }
        return getExcuteValueHolder(errorArray);
    }

    /**
     * @return void
     * @Author chenjinjun
     * @Description 删除主表
     * @Date 2019-10-25
     * @Param [id, errorArray]
     **/
    @Transactional(rollbackFor = Exception.class)
    public void delMain(Long id, JSONArray errorArray) throws Exception {
        //数据库删除数据
        int deleteCount = sgBChannelManualSynMapper.deleteById(id);
        if (deleteCount == 0) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("id", id);
            jsonObject.put("message", "主表删除失败！");
            errorArray.add(jsonObject);
            throw new Exception("主表删除失败");
        }
        int itemCount = sgBChannelManualSynItemMapper.delete(new QueryWrapper<SgBChannelManualSynItem>()
                .lambda()
                .eq(SgBChannelManualSynItem::getSgBChannelManualSynId, id));
    }

    /**
     * @return void
     * @Author chenjinjun
     * @Description 删除明细
     * @Date 2019-10-21
     * @Param [itemArray, errorArray]
     **/
    @Transactional(rollbackFor = Exception.class)
    public void delItem(JSONArray itemArray) throws Exception {
        List<Long> ids = JSONArray.parseArray(JSON.toJSONString(itemArray), Long.class);
        sgBChannelManualSynItemMapper.deleteBatchIds(ids);
    }

    /**
     * @return com.jackrain.nea.util.ValueHolder
     * @Author chenjinjun
     * @Description
     * @Date 2019-10-21
     * @Param [errorArray]
     **/
    public static ValueHolder getExcuteValueHolder(JSONArray errorArray) {
        ValueHolder valueHolder = new ValueHolder();
        if (errorArray.size() <= 0) {
            valueHolder.put("code", ResultCode.SUCCESS);
            valueHolder.put("message", "删除成功！");
        } else {
            valueHolder.put("code", ResultCode.FAIL);
            valueHolder.put("message", "执行失败记录数：" + errorArray.size());
        }
        valueHolder.put("data", errorArray);
        return valueHolder;
    }
}
