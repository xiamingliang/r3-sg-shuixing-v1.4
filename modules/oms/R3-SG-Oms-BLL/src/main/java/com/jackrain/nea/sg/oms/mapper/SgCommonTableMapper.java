package com.jackrain.nea.sg.oms.mapper;

import com.alibaba.fastjson.JSONObject;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.jdbc.SQL;

import java.util.HashMap;
import java.util.List;

import static org.apache.ibatis.mapping.StatementType.STATEMENT;

@Mapper
public interface SgCommonTableMapper {

    /**
     * 通用查询
     *
     * @param jsonObject 查询参数
     */
    @SelectProvider(type = CommonTableSql.class, method = "lisyByCondition")
    @Options(statementType = STATEMENT)
    List<HashMap<String, Object>> listByCondition(JSONObject jsonObject);

    class CommonTableSql {

        public String excuteSql(String sqlString) {
            return sqlString;
        }

        public String lisyByCondition(JSONObject jsonObject) {
            return new SQL() {
                {
                    SELECT(jsonObject.getString("fields"));
                    FROM(jsonObject.getString("table"));
                    WHERE(jsonObject.getString("conditions"));
                }
            }.toString();
        }
    }
}
