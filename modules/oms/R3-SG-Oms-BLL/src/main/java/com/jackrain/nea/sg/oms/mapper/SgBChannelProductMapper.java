package com.jackrain.nea.sg.oms.mapper;

import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.oms.model.table.SgBChannelProduct;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SgBChannelProductMapper extends ExtentionMapper<SgBChannelProduct> {
}