package com.jackrain.nea.sg.oms.mapper;

import com.jackrain.nea.sg.oms.model.result.SgBStorageResult;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @Author: chenchen
 * @Description:
 * @Date: Create in 15:55 2019/4/8
 */
public interface SgBStorageExtMapper {

    /**
     * 组合商品和福袋商品的对应的实际商品安装店仓分组查询
     *
     * @param skuIds
     * @return
     */
    @Select("<script> " +
            "select  cp_c_store_id as storeId,cp_c_store_ename as storeName,cp_c_store_ecode  as storeEcode," +
            " array_to_string(array_agg(ps_c_sku_id ||':' ||qty_available),',') as qtyAvailableList" +
            " from sg_b_storage   where ps_c_sku_id  in" +
            " <foreach item='item' index='index' collection='skuIds' open='(' separator=',' close=')'> #{item} </foreach> " +
            " and qty_available>0 GROUP BY  cp_c_store_id,cp_c_store_ename,cp_c_store_ecode" +
            " </script> ")
    List<SgBStorageResult> selectCombinedCommodityList(@Param("skuIds") List<Long> skuIds);


}
