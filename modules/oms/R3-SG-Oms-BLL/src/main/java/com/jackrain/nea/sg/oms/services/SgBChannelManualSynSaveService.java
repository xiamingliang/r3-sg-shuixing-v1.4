package com.jackrain.nea.sg.oms.services;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.Feature;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.cpext.utils.ValueHolderUtils;
import com.jackrain.nea.data.basic.model.request.SkuInfoQueryRequest;
import com.jackrain.nea.data.basic.services.BasicPsQueryService;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.model.util.ModelUtil;
import com.jackrain.nea.ps.api.result.PsCProSkuResult;
import com.jackrain.nea.psext.api.SkuLikeQueryCmd;
import com.jackrain.nea.psext.api.utils.JsonUtils;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.oms.mapper.SgBChannelManualSynItemMapper;
import com.jackrain.nea.sg.oms.mapper.SgBChannelManualSynMapper;
import com.jackrain.nea.sg.oms.model.request.SgBChannelManualSynRequest;
import com.jackrain.nea.sg.oms.model.table.SgBChannelManualSyn;
import com.jackrain.nea.sg.oms.model.table.SgBChannelManualSynItem;
import com.jackrain.nea.sys.CommandAdapter;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.query.DefaultWebEvent;
import com.jackrain.nea.web.query.QuerySession;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * @Author chenjinjun
 * @Description 手工导入店铺库存新增修改实现逻辑
 * @Date 2019-10-21
 **/
@Slf4j
@Component
public class SgBChannelManualSynSaveService extends CommandAdapter {

    @Autowired
    private SgBChannelManualSynMapper sgBChannelManualSynMapper;
    @Autowired
    private SgBChannelManualSynItemMapper sgBChannelManualSynItemMapper;
    @Reference(version = "1.0", group = "ps-ext")
    private SkuLikeQueryCmd skuLikeQueryCmd;

    @Override
    public ValueHolder execute(QuerySession session) throws NDSException {
        DefaultWebEvent event = session.getEvent();
        JSONObject param = JSON.parseObject(JSON.toJSONStringWithDateFormat(event.getParameterValue("param"),
                "yyyy-MM-dd HH:mm:ss", SerializerFeature.WriteMapNullValue), Feature.OrderedField);
        Long objid = param.getLong("objid");
        JSONObject fixColumn = param.getJSONObject("fixcolumn");

        SgBChannelManualSynRequest sgBChannelManualSynRequest = JsonUtils.jsonParseClass(fixColumn, SgBChannelManualSynRequest.class);
        if (null == sgBChannelManualSynRequest) {
            return ValueHolderUtils.getFailValueHolder("请求数据为空！");
        }
        try {
            if (fixColumn != null) {
                if (objid != null && objid > 0) {
                    SgBChannelManualSynSaveService service = ApplicationContextHandle.getBean(SgBChannelManualSynSaveService.class);
                    return service.updateChannelManual(session.getUser(), objid, sgBChannelManualSynRequest);
                } else {
                    SgBChannelManualSynSaveService service = ApplicationContextHandle.getBean(SgBChannelManualSynSaveService.class);
                    return service.addChannelManual(session.getUser(), sgBChannelManualSynRequest);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(this.getClass().getName() + "手工导入店铺库存保存服务异常：" + e.getMessage());
            return ValueHolderUtils.getFailValueHolder(e.getMessage());
        }
        throw new NDSException("当前记录已不存在！");
    }

    /**
     * @return com.jackrain.nea.util.ValueHolder
     * @Author chenjinjun
     * @Description 修改手工导入店铺库存
     * @Date 2019-10-21
     * @Param [user, id, sgBChannelManualSynRequest]
     **/
    @Transactional(rollbackFor = Exception.class)
    public ValueHolder updateChannelManual(User user, Long id, SgBChannelManualSynRequest sgBChannelManualSynRequest) throws Exception {
        SgBChannelManualSyn sgBChannelManualSyn = sgBChannelManualSynRequest.getSgBChannelManualSyn();
        //更新主表
        if (null != sgBChannelManualSyn) {
            sgBChannelManualSyn.setId(id);
            sgBChannelManualSyn.setModifierid(Long.valueOf(user.getId()));
            sgBChannelManualSyn.setModifierename(user.getEname());
            sgBChannelManualSyn.setModifiername(user.getName());
            sgBChannelManualSyn.setModifieddate(new Date());
            int num = sgBChannelManualSynMapper.updateById(sgBChannelManualSyn);
            if (num <= 0) {
                throw new Exception("主表更新失败！");
            }
        }
        //子表只有新增
        List<SgBChannelManualSynItem> itemList = sgBChannelManualSynRequest.getSgBChannelManualSynItemList();
        StringBuffer sb = new StringBuffer();
        StringBuffer repeatSb = new StringBuffer();
        if (CollectionUtils.isNotEmpty(itemList)) {
            List<SgBChannelManualSynItem> insertList = new ArrayList<>();
            for (SgBChannelManualSynItem sgBChannelManualSynItem : itemList) {
                //验证商品编码
                boolean isExist = checkSkuCode(sgBChannelManualSynItem.getPsCSkuEcode());
                if (!isExist) {
                    if (StringUtils.isBlank(sb)) {
                        sb.append(sgBChannelManualSynItem.getPsCSkuEcode());
                    } else {
                        sb.append(",").append(sgBChannelManualSynItem.getPsCSkuEcode());
                    }
                    continue;
                }
                //验证是否重复
                boolean flag = checkIsExist(sgBChannelManualSynItem, id);
                if (flag) {
                    if (StringUtils.isBlank(repeatSb)) {
                        repeatSb.append(sgBChannelManualSynItem.getPsCSkuEcode());
                    } else {
                        repeatSb.append(",").append(sgBChannelManualSynItem.getPsCSkuEcode());
                    }
                    continue;
                }
                sgBChannelManualSynItem.setId(ModelUtil.getSequence(SgConstants.SG_B_CHANNEL_MANUAL_SYN_ITEM));
                //主表ID
                sgBChannelManualSynItem.setSgBChannelManualSynId(id);
                sgBChannelManualSynItem.setOwnerename(user.getEname());
                sgBChannelManualSynItem.setOwnerid(Long.valueOf(user.getId()));
                sgBChannelManualSynItem.setOwnername(user.getName());
                sgBChannelManualSynItem.setCreationdate(new Date());
                insertList.add(sgBChannelManualSynItem);
            }
            //批量新增
            if (CollectionUtils.isNotEmpty(insertList)) {
                int insertNum = sgBChannelManualSynItemMapper.batchInsert(insertList);
                if (insertNum <= 0) {
                    throw new Exception("子表更新失败！");
                }
            }
        }
        //商品条码不存在时提示部分新增成功，部分不存在则提示
        if (StringUtils.isBlank(sb) && StringUtils.isBlank(repeatSb)) {
            return ValueHolderUtils.getSuccessValueHolder(id, SgConstants.SG_B_CHANNEL_MANUAL_SYN);
        } else {
            if (StringUtils.isNotBlank(sb) && StringUtils.isNotBlank(repeatSb)) {
                return ValueHolderUtils.getFailValueHolder(sb.toString() + "商品条码不存在，"
                        + repeatSb.toString() + "商品条码重复！");
            } else if (StringUtils.isBlank(sb) && StringUtils.isNotBlank(repeatSb)) {
                return ValueHolderUtils.getFailValueHolder(repeatSb.toString() + "商品条码重复！");
            } else if (StringUtils.isNotBlank(sb) && StringUtils.isBlank(repeatSb)) {
                return ValueHolderUtils.getFailValueHolder(sb.toString() + "商品条码不存在！");
            } else {
                return ValueHolderUtils.getSuccessValueHolder(id
                        , SgConstants.SG_B_CHANNEL_MANUAL_SYN);
            }
        }
    }

    /**
     * @return com.jackrain.nea.util.ValueHolder
     * @Author chenjinjun
     * @Description 新增手工导入店铺库存
     * @Date 2019-10-21
     * @Param [user, sgBChannelManualSynRequest]
     **/
    @Transactional(rollbackFor = Exception.class)
    public ValueHolder addChannelManual(User user, SgBChannelManualSynRequest sgBChannelManualSynRequest) throws Exception {
        SgBChannelManualSyn sgBChannelManualSyn = sgBChannelManualSynRequest.getSgBChannelManualSyn();
        if (null == sgBChannelManualSyn) {
            return ValueHolderUtils.getFailValueHolder("主表信息为空！");
        }
        //主表新增
        sgBChannelManualSyn.setId(ModelUtil.getSequence(SgConstants.SG_B_CHANNEL_MANUAL_SYN));
        sgBChannelManualSyn.setOwnerid(Long.valueOf(user.getId()));
        sgBChannelManualSyn.setOwnername(user.getName());
        sgBChannelManualSyn.setOwnerename(user.getEname());
        sgBChannelManualSyn.setCreationdate(new Date());

        int num = sgBChannelManualSynMapper.insert(sgBChannelManualSyn);
        StringBuffer sb = new StringBuffer();
        if (num > 0) {
            //子表新增
            List<SgBChannelManualSynItem> itemList = sgBChannelManualSynRequest.getSgBChannelManualSynItemList();
            if (CollectionUtils.isEmpty(itemList)) {
                throw new Exception("子表信息为空！");
            }
            List<SgBChannelManualSynItem> insertItemList = new ArrayList<>();
            for (SgBChannelManualSynItem sgBChannelManualSynItem : itemList) {
                //验证商品条码是否存在
                boolean isExist = checkSkuCode(sgBChannelManualSynItem.getPsCSkuEcode());
                if (!isExist) {
                    if (StringUtils.isBlank(sb)) {
                        sb.append(sgBChannelManualSynItem.getPsCSkuEcode());
                    } else {
                        sb.append(",").append(sgBChannelManualSynItem.getPsCSkuEcode());
                    }
                    continue;
                }
                SgBChannelManualSynItem insertItemBean = new SgBChannelManualSynItem();
                insertItemBean = sgBChannelManualSynItem;
                insertItemBean.setId(ModelUtil.getSequence(SgConstants.SG_B_CHANNEL_MANUAL_SYN_ITEM));
                insertItemBean.setOwnerid(Long.valueOf(user.getId()));
                insertItemBean.setOwnerename(user.getEname());
                insertItemBean.setOwnername(user.getName());
                insertItemBean.setCreationdate(new Date());
                insertItemBean.setSgBChannelManualSynId(sgBChannelManualSyn.getId());
                insertItemList.add(insertItemBean);
            }
            if (CollectionUtils.isEmpty(insertItemList)) {
                return ValueHolderUtils.getFailValueHolder(sb.toString() + "商品条码不存在！");
            }
            int itemNum = sgBChannelManualSynItemMapper.batchInsert(insertItemList);
            if (itemNum <= 0) {
                return ValueHolderUtils.getFailValueHolder("子表新增失败");
            }
        }
        //商品条码不存在时提示部分新增成功，部分不存在则提示
        if (StringUtils.isBlank(sb)) {
            return ValueHolderUtils.getSuccessValueHolder(sgBChannelManualSyn.getId()
                    , SgConstants.SG_B_CHANNEL_MANUAL_SYN);
        } else {
            return ValueHolderUtils.getFailValueHolder(sb.toString() + "商品条码不存在，部分新增成功！");
        }
    }

    /**
     * @return boolean
     * @Author chenjinjun
     * @Description 商品条码验证存在性
     * @Date 2019-10-21
     * @Param [ecode]
     **/
    private boolean checkSkuCode(String ecode) {
        List<String> ecodeList = new ArrayList<>();
        ecodeList.add(ecode);
        SkuInfoQueryRequest request = new SkuInfoQueryRequest();
        request.setSkuEcodeList(ecodeList);
        HashMap<String, PsCProSkuResult> resultHashMap = new HashMap<>();
        BasicPsQueryService psQueryService = ApplicationContextHandle.getBean(BasicPsQueryService.class);
        try {
            resultHashMap = psQueryService.getSkuInfoByEcode(request);
        } catch (Exception e) {
            e.printStackTrace();
            log.error(this.getClass().getName() + " 手工导入店铺库存验证商品条码调用接口异常！");
            return false;
        }
        if (resultHashMap.isEmpty()) {
            return false;
        } else {
            PsCProSkuResult result = resultHashMap.get(ecode);
            if (null == result) {
                return false;
            } else {
                return true;
            }
        }
    }

    /**
     * @return boolean
     * @Author chenjinjun
     * @Description 验证是否重复
     * @Date 2019-11-6
     * @Param [sgBChannelManualSynItem]
     **/
    private boolean checkIsExist(SgBChannelManualSynItem sgBChannelManualSynItem, Long id) {
        Wrapper<SgBChannelManualSynItem> wrapper = new QueryWrapper<>();

        ((QueryWrapper<SgBChannelManualSynItem>) wrapper).lambda()
                .eq(SgBChannelManualSynItem::getPsCSkuEcode, sgBChannelManualSynItem.getPsCSkuEcode())
                .eq(SgBChannelManualSynItem::getNumiid, sgBChannelManualSynItem.getNumiid())
                .eq(SgBChannelManualSynItem::getSkuId, sgBChannelManualSynItem.getSkuId())
                .eq(SgBChannelManualSynItem::getSgBChannelManualSynId, id);
        Integer count = sgBChannelManualSynItemMapper.selectCount(wrapper);
        if (count > 0) {
            return true;
        } else {
            return false;
        }
    }
}
