package com.jackrain.nea.sg.oms.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.common.R3ParamConstants;
import com.jackrain.nea.sg.oms.mapper.SgBChannelProductMapper;
import com.jackrain.nea.sg.oms.mapper.SgCommonTableMapper;
import com.jackrain.nea.sg.oms.model.request.SgChannelProductQueryRequest;
import com.jackrain.nea.sg.oms.model.table.SgBChannelProduct;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author zhu lin yu
 * @since 2019/7/16
 * create at : 2019/7/16 19:59
 */
@Slf4j
@Component
public class SgChannelProductQueryService {

    @Autowired
    private SgBChannelProductMapper sgBChannelProductMapper;

    public ValueHolderV14<List<SgBChannelProduct>> queryChannelProduct(SgChannelProductQueryRequest sgChannelProductQueryRequest) {
        ValueHolderV14<List<SgBChannelProduct>> holder;

        //入参日志记录
        if (log.isDebugEnabled()) {
            log.debug("Start SgChannelProductSaveService.saveChannelProduct. ReceiveParams:SgChannelProductSaveRequest="
                    + JSONObject.toJSONString(sgChannelProductQueryRequest) + ";");
        }

        //检查入参
        holder = checkServiceParam(sgChannelProductQueryRequest);
        if (ResultCode.FAIL == holder.getCode()) {
            return holder;
        }

        List<Long> sgBChannelProductIdList = sgChannelProductQueryRequest.getSgBChannelProductIdList();
        List<Long> psCSkuIdList = sgChannelProductQueryRequest.getPsCSkuIdList();
        List<SgBChannelProduct> sgBChannelProducts = new ArrayList<>();
        try {
            if (CollectionUtils.isNotEmpty(sgBChannelProductIdList)) {
                sgBChannelProducts = this.sgBChannelProductMapper.selectBatchIds(sgBChannelProductIdList);
            } else if (CollectionUtils.isNotEmpty(psCSkuIdList)) {
                sgBChannelProducts = this.sgBChannelProductMapper.selectList(new QueryWrapper<SgBChannelProduct>().lambda()
                        .in(SgBChannelProduct::getPsCSkuId, psCSkuIdList));
            }
        } catch (Exception e) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage("数据库查询异常" + e.getMessage());
            log.error("数据库查询异常" + e.getMessage());
        }
        holder.setMessage("查询渠道商品成功");
        holder.setData(sgBChannelProducts);
        return holder;
    }

    private ValueHolderV14<List<SgBChannelProduct>> checkServiceParam(SgChannelProductQueryRequest sgChannelProductQueryRequest) {
        ValueHolderV14<List<SgBChannelProduct>> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");

        if (sgChannelProductQueryRequest == null) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage("请求体不能为空!");
        }

        if (sgChannelProductQueryRequest != null && (CollectionUtils.isEmpty(sgChannelProductQueryRequest.getSgBChannelProductIdList())
                && CollectionUtils.isEmpty(sgChannelProductQueryRequest.getPsCSkuIdList()))) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage("查询的id集合不能为空!");
        }

        if (sgChannelProductQueryRequest != null && (CollectionUtils.isNotEmpty(sgChannelProductQueryRequest.getSgBChannelProductIdList())
                && CollectionUtils.isNotEmpty(sgChannelProductQueryRequest.getPsCSkuIdList()))) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage("暂不支持id、条码id同时查询渠道商品信息!");
        }
        return holder;
    }

    /**
     * 渠道商品页面配置查询
     * @param map
     * @return
     */
    public List<HashMap<String,Object>> queryChannelProductByPageLayout(HashMap map) {
        SgCommonTableMapper sgCommonTableMapper=ApplicationContextHandle.getBean(SgCommonTableMapper.class);
        JSONObject jsonObject= (JSONObject) map.get(R3ParamConstants.DATA);
        return sgCommonTableMapper.listByCondition(jsonObject);
    }
}
