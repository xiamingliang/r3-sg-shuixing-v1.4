package com.jackrain.nea.sg.oms.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Lists;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.model.util.ModelUtil;
import com.jackrain.nea.ps.api.SkuListByProIdCmd;
import com.jackrain.nea.ps.api.request.SkuListCmdRequest;
import com.jackrain.nea.ps.api.table.PsCSku;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.model.result.SgR3BaseResult;
import com.jackrain.nea.sg.basic.utils.StorageUtils;
import com.jackrain.nea.sg.oms.common.OmsConstantsIF;
import com.jackrain.nea.sg.oms.mapper.SgBChannelDefMapper;
import com.jackrain.nea.sg.oms.mapper.SgChannelProductMapper;
import com.jackrain.nea.sg.oms.model.request.SgChannelProductBoundRequest;
import com.jackrain.nea.sg.oms.model.table.SgBChannelDef;
import com.jackrain.nea.sg.oms.model.table.SgBChannelProduct;
import com.jackrain.nea.sg.oms.model.table.SgBChannelStorageBuffer;
import com.jackrain.nea.sg.oms.model.tableExtend.SgBChannelStorageBufferExtend;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author zhu lin yu
 * @since 2019-11-08
 * create at : 2019-11-08 11:09
 */
@Slf4j
@Component
public class SgChannelProductBoundService {

    @Autowired
    private SgChannelStorageOmsBufferService sgChannelStorageOmsBufferService;

    public ValueHolderV14<SgR3BaseResult> channelProductBound(SgChannelProductBoundRequest request) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgChannelProductBoundService.channelProductBound. ReceiveParams:params:{};" + JSONObject.toJSONString(request));
        }

        ValueHolderV14<SgR3BaseResult> valueHolderV14;

        //参数校验
        valueHolderV14 = checkParam(request);
        if (ResultCode.FAIL == valueHolderV14.getCode()) {
            return valueHolderV14;
        }

        //查询绑定店铺信息
        Long shopId = request.getShopId();
        List<Long> proIdList = request.getProIdList();
        User loginUser = request.getLoginUser();
        SgBChannelDefMapper defMapper = ApplicationContextHandle.getBean(SgBChannelDefMapper.class);
        SgBChannelDef sgBChannelDef = defMapper.selectOne(new QueryWrapper<SgBChannelDef>().lambda().eq(SgBChannelDef::getCpCShopId, shopId));

        if (sgBChannelDef == null) {
            valueHolderV14.setCode(ResultCode.FAIL);
            valueHolderV14.setMessage("渠道信息表未查到该渠道信息");
            return valueHolderV14;
        }

        int totalNum = proIdList.size();
        int errorNum = 0;

        //批量查询商品条码
        SgChannelProductMapper sgChannelProductMapper = ApplicationContextHandle.getBean(SgChannelProductMapper.class);
        List<List<Long>> partition = Lists.partition(proIdList, SgConstants.SG_COMMON_INSERT_PAGE_SIZE);
        SkuListByProIdCmd skuListByProIdCmd = (SkuListByProIdCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(),
                SkuListByProIdCmd.class.getName(),
                "ps", "1.0");

        for (List<Long> proIds : partition) {
            try {
                SkuListCmdRequest skuListCmdRequest = new SkuListCmdRequest();
                skuListCmdRequest.setProIdList(proIds);
                ValueHolderV14<List<PsCSku>> listValueHolderV14 = skuListByProIdCmd.querySkuListByProInfoList(skuListCmdRequest);

                if (ResultCode.SUCCESS == listValueHolderV14.getCode()) {
                    List<PsCSku> psCSkuList = listValueHolderV14.getData();
                    if (CollectionUtils.isNotEmpty(psCSkuList)) {
                        List<List<PsCSku>> skuPartition = Lists.partition(psCSkuList, SgConstants.SG_COMMON_INSERT_PAGE_SIZE);
                        for (List<PsCSku> psCSkus : skuPartition) {
                            SgChannelProductBoundService service = ApplicationContextHandle.getBean(SgChannelProductBoundService.class);
                            service.insertChannelProducts(psCSkus, sgBChannelDef, shopId, sgChannelProductMapper, loginUser);
                        }
                    }
                }
            } catch (Exception e) {
                log.error("绑定渠道商品失败,失败商品id为{}", proIds, e);
                errorNum++;
            }

        }

        //封装返回结果
        if (errorNum > 0) {
            valueHolderV14.setCode(ResultCode.FAIL);
            valueHolderV14.setMessage("共绑定" + totalNum + "款商品，失败" + errorNum + "款");
        } else {
            valueHolderV14.setCode(ResultCode.SUCCESS);
            valueHolderV14.setMessage("绑定渠道商品成功");
        }

        if (log.isDebugEnabled()) {
            log.debug("Finish SgChannelProductBoundService.channelProductBound. ReturnParams:params:{};" + JSONObject.toJSONString(valueHolderV14));
        }
        return valueHolderV14;
    }


    /**
     * 新增渠道商品并新增渠道计算缓存池
     *
     * @param psCSkus                条码集合
     * @param sgBChannelDef          渠道信息
     * @param shopId                 店铺id
     * @param sgChannelProductMapper 渠道商品表mapper
     */
    @Transactional(rollbackFor = Exception.class)
    public void insertChannelProducts(List<PsCSku> psCSkus, SgBChannelDef sgBChannelDef, Long shopId, SgChannelProductMapper sgChannelProductMapper, User loginUser) {

        //根据shopId和skuId来判断该条码是否已经绑定，如果已经绑定，则不用绑定该条码
        List<String> psCskuIdList = psCSkus.stream().map(PsCSku::getId).map(Object::toString).collect(Collectors.toList());

        List<SgBChannelProduct> sgBChannelProducts = sgChannelProductMapper.selectList(new QueryWrapper<SgBChannelProduct>().lambda()
                .eq(SgBChannelProduct::getCpCShopId, shopId).in(SgBChannelProduct::getSkuId, psCskuIdList));

        if (CollectionUtils.isNotEmpty(sgBChannelProducts)) {
            List<String> skuIdList = sgBChannelProducts.stream().map(SgBChannelProduct::getSkuId).collect(Collectors.toList());
            psCSkus = psCSkus.stream().filter(psCSku -> !(skuIdList.contains(psCSku.getId().toString()))).collect(Collectors.toList());
        }

        if (CollectionUtils.isNotEmpty(psCSkus)) {
            List<SgBChannelProduct> sgBChannelProductList = new ArrayList<>();
            List<SgBChannelStorageBuffer> sgBChannelStorageBufferList = new ArrayList<>();
            //封装渠道商品表新增入参
            for (PsCSku psCSku : psCSkus) {
                SgBChannelProduct sgBChannelProduct = new SgBChannelProduct();
                Long id = ModelUtil.getSequence(SgConstants.SG_B_CHANNEL_PRODUCT);
                BeanUtils.copyProperties(psCSku, sgBChannelProduct);
                sgBChannelProduct.setId(id);
                sgBChannelProduct.setPsCSpec1Id(psCSku.getPsCSpec1objId());
                sgBChannelProduct.setPsCSpec2Id(psCSku.getPsCSpec2objId());
                sgBChannelProduct.setCpCPlatformId(Integer.parseInt(sgBChannelDef.getCpCPlatformId()));
                sgBChannelProduct.setCpCPlatformEcode(sgBChannelDef.getCpCPlatformEcode());
                sgBChannelProduct.setCpCPlatformEname(sgBChannelDef.getCpCPlatformEname());
                sgBChannelProduct.setCpCShopId(shopId);
                sgBChannelProduct.setCpCShopTitle(sgBChannelDef.getCpCShopTitle());
                sgBChannelProduct.setSkuId(psCSku.getEcode());
                sgBChannelProduct.setPsCSkuId(psCSku.getId());
                sgBChannelProduct.setPsCSkuEcode(psCSku.getEcode());
                StorageUtils.setBModelDefalutData(sgBChannelProduct, loginUser);
                sgBChannelProductList.add(sgBChannelProduct);

                //封装渠道计算缓存池数据
                SgBChannelStorageBuffer sgBChannelStorageBuffer = new SgBChannelStorageBuffer();
                StorageUtils.setBModelDefalutData(sgBChannelStorageBuffer, loginUser);
                sgBChannelStorageBuffer.setWareType(0);
                sgBChannelStorageBuffer.setCpCShopId(shopId);
                sgBChannelStorageBuffer.setCpCShopTitle(sgBChannelDef.getCpCShopTitle());
                sgBChannelStorageBuffer.setSkuId(psCSku.getId().toString());
                sgBChannelStorageBuffer.setDealStatus(SgBChannelStorageBufferExtend.DealStatusEnum.UN_DEAL.getCode());
                sgBChannelStorageBufferList.add(sgBChannelStorageBuffer);
            }

            if (CollectionUtils.isNotEmpty(sgBChannelProductList)) {
                sgChannelProductMapper.batchInsert(sgBChannelProductList);
            }

            //调用渠道库存计算缓存池服务
            if (CollectionUtils.isNotEmpty(sgBChannelStorageBufferList)) {
                try {
                    log.info(this.getClass().getName() + " 开始调用新增渠道库存计算缓存池方法,明细:{}", JSONObject.toJSONString(sgBChannelStorageBufferList));
                    ValueHolderV14 holderV14 = sgChannelStorageOmsBufferService.addSgBChannelStorageBuffer(sgBChannelStorageBufferList, null);
                    if (0 == (int) holderV14.getData()) {
                        log.error(this.getClass().getName() + " 调用新增渠道库存计算缓存池出错,明细为 {}", JSONObject.toJSONString(sgBChannelStorageBufferList));
                    }
                } catch (Exception e) {
                    log.error(this.getClass().getName() + " 新增渠道库存计算缓存池异常,明细为 {}", JSONObject.toJSONString(sgBChannelStorageBufferList), e);
                    throw new NDSException("新增渠道库存计算缓存池异常");
                }
            }
        }

    }

    private ValueHolderV14<SgR3BaseResult> checkParam(SgChannelProductBoundRequest request) {
        ValueHolderV14<SgR3BaseResult> valueHolderV14 = new ValueHolderV14<>(ResultCode.FAIL, "");

        if (request == null) {
            valueHolderV14.setMessage("request is null");
            return valueHolderV14;

        } else if (request.getShopId() == null) {
            valueHolderV14.setMessage("请绑定渠道");
            return valueHolderV14;

        } else if (CollectionUtils.isEmpty(request.getProIdList())) {
            valueHolderV14.setMessage("请绑定商品");
            return valueHolderV14;

        } else if (request.getLoginUser() == null) {
            valueHolderV14.setMessage("用户不能为空");
            return valueHolderV14;
        }

        valueHolderV14.setCode(ResultCode.SUCCESS);
        return valueHolderV14;
    }
}
