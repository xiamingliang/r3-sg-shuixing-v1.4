package com.jackrain.nea.sg.oms.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.oms.mapper.SgBChannelSynstockQMapper;
import com.jackrain.nea.sg.oms.model.request.SgChannelStorgeOmsCallbackRequest;
import com.jackrain.nea.sg.oms.model.request.StandplatRequest;
import com.jackrain.nea.sg.oms.model.table.SgBChannelSynstockQ;
import com.jackrain.nea.sg.oms.model.tableExtend.SgBChannelSynstockQExtend;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


@Component
@Slf4j
public class SgChannelStorageOmsCallbackService {
    /**
     * 判断是否需要同步库存
     *
     * @param request
     * @return ValueHolderV14
     */

    @Autowired
    private SgBChannelSynstockQMapper sgBChannelSynstockQMapper;

    /**
     * 回调RPC增量库存同步
     *
     * @param request
     * @return
     */
    public ValueHolderV14<Integer> callbackRPC(SgChannelStorgeOmsCallbackRequest request) {
        log.info("=========>>>>>>>>[dsh]增量同步库存回调服务,返回参数为" + request);
        //参数校验
        if (request.getId() == null) {
            return new ValueHolderV14<>(ResultCode.FAIL, "ID不能为空!");
        }
        SgBChannelSynstockQExtend sgBChannelSynstockQExtend = new SgBChannelSynstockQExtend();
        sgBChannelSynstockQExtend.setId(request.getId());
        QueryWrapper<SgBChannelSynstockQ> synstockQueryWrapper = sgBChannelSynstockQExtend.createQueryWrapper();
        SgBChannelSynstockQ sgBChannelSynstockQ = sgBChannelSynstockQMapper.selectOne(synstockQueryWrapper);
        if (sgBChannelSynstockQ == null) {
            return new ValueHolderV14<>(ResultCode.FAIL, "数据发生变化,请重试!");
        }
        if (SgBChannelSynstockQExtend.SynStatusEnum.SUCCESS.equals(request.getSynStatus())) {
            sgBChannelSynstockQ.setSynStatus(SgChannelStorageOmsSynchService.SYNCSUCCESSS);
        } else {
            sgBChannelSynstockQ.setSynStatus(SgChannelStorageOmsSynchService.SYNCFAIL);
        }
        sgBChannelSynstockQ.setIsError(request.getSynStatus());
        sgBChannelSynstockQ.setErrorMsg(request.getErrorMsg());
        int i = sgBChannelSynstockQMapper.updateById(sgBChannelSynstockQ);
        if (i <= 0) {
            return new ValueHolderV14<>(ResultCode.FAIL, "增量同步库存回调RPC,更新状态失败!");
        }
        return new ValueHolderV14<>(ResultCode.SUCCESS, "增量同步库存回调RPC,更新状态成功!");
    }

    /**
     * 通用平台渠道库存回传信息处理
     */
    public ValueHolderV14<Integer> callbackRPCForPDDandSN(List<StandplatRequest> standplatRequestList) {
        log.info(this.getClass().getName() + " 开始处理通用平台渠道库存回传信息,{}", JSONObject.toJSONString(standplatRequestList));
        //参数校验
        if (CollectionUtils.isEmpty(standplatRequestList)) {
            return new ValueHolderV14<>(ResultCode.FAIL, "处理数据为空");
        }

        List<SgBChannelSynstockQ> sgBChannelSynstockQSList = new ArrayList<>();

        //跟新状态
        standplatRequestList.forEach(x -> {
            SgBChannelSynstockQ sgBChannelSynstockQ;
            if (x.isSuccess()) {
                sgBChannelSynstockQ = new SgBChannelSynstockQ();
                sgBChannelSynstockQ.setId(x.getID());
                sgBChannelSynstockQ.setSynStatus(SgChannelStorageOmsSynchService.SYNCSUCCESSS);
                sgBChannelSynstockQ.setErrorMsg(x.getMessage());
            } else {
                sgBChannelSynstockQ = new SgBChannelSynstockQ();
                sgBChannelSynstockQ.setId(x.getID());
                sgBChannelSynstockQ.setSynStatus(SgChannelStorageOmsSynchService.SYNCFAIL);
                sgBChannelSynstockQ.setErrorMsg(x.getMessage());
            }
            int i = sgBChannelSynstockQMapper.updateById(sgBChannelSynstockQ);
            if (i <= 0) {
                log.error(this.getClass().getName() + " 通用平台渠道库存回传,更新状态失败,错误信息为{}", JSONObject.toJSONString(sgBChannelSynstockQ));
            }
        });
        return new ValueHolderV14<>(ResultCode.SUCCESS, "通用平台渠道库存回传,更新状态成功!");
    }


}
