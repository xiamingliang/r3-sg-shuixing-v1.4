package com.jackrain.nea.sg.oms.services;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.oms.mapper.SgBChannelManualSynItemMapper;
import com.jackrain.nea.sg.oms.model.request.SgChannelStorgeOmsCallbackAllRequest;
import com.jackrain.nea.sg.oms.model.table.SgBChannelManualSynItem;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @author zhu lin yu
 * @since 2019-10-18
 * create at : 2019-10-18 11:02
 */

@Component
@Slf4j
public class SgChannelStorageManualSyncCallBackService {

    /**
     * 全量同步库存,返回状态
     */
    private static final Integer SUCCESS = 1;
    private static final Integer FAIL = 2;

    @Autowired
    private SgBChannelManualSynItemMapper sgBChannelManualSynItemMapper;

    public ValueHolderV14<Integer> callbackAllRPC(SgChannelStorgeOmsCallbackAllRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("Start SgChannelStorageManualSyncCallBackService.callbackAllRPC.ReceiveParams:,param:{}", JSONObject.toJSONString(request));

        }

        Long id = request.getId();
        Integer status = request.getStatus();
        String msg = request.getMsg();

        updateStock(status, msg, id);
        return new ValueHolderV14<>(ResultCode.SUCCESS, "全量库存回调成功!");
    }

    private void updateStock(Integer status, String msg, Long id) {

        Date modifiedDate = new Date(System.currentTimeMillis());
        SgBChannelManualSynItem sgBChannelManualSynItem = new SgBChannelManualSynItem();
        sgBChannelManualSynItem.setId(id);

        if (SUCCESS.equals(status)) {
            sgBChannelManualSynItem.setSynStatus(SgChannelStorageOmsSynchService.SYNCSUCCESSS);
            sgBChannelManualSynItem.setErrorMsg("");
        } else {
            sgBChannelManualSynItem.setSynStatus(SgChannelStorageOmsSynchService.SYNCFAIL);
            sgBChannelManualSynItem.setErrorMsg(msg);
        }

        sgBChannelManualSynItem.setModifieddate(modifiedDate);

        int i = sgBChannelManualSynItemMapper.updateById(sgBChannelManualSynItem);

        if (i > 0) {
            log.info(this.getClass().getName() + " id:{}更新数据库状态成功,更改的状态为{}"
                    , id, sgBChannelManualSynItem.getSynStatus());
        }
    }
}
