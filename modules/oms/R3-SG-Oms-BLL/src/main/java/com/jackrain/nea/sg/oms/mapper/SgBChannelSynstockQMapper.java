package com.jackrain.nea.sg.oms.mapper;

import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.oms.model.table.SgBChannelSynstockQ;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface SgBChannelSynstockQMapper extends ExtentionMapper<SgBChannelSynstockQ> {

    @Update(
            "<script>" +
                    "update sg_b_channel_synstock_q\n" +
                    "        <trim prefix=\"set\" suffixOverrides=\",\">\n" +
                    "            <trim prefix=\"syn_status = case\" suffix=\"end,\">\n" +
                    "                <foreach collection=\"list\" item=\"item\" index=\"index\">\n" +
                    "                    <if test=\"item.synStatus != null\">\n" +
                    "                        when id = #{item.id} then  #{item.synStatus}\n" +
                    "                    </if>\n" +
                    "\n" +
                    "                </foreach>\n" +
                    "            </trim>\n" +
                    "            <trim prefix=\"error_msg = case\" suffix=\"end,\">\n" +
                    "                <foreach collection=\"list\" item=\"item\" index=\"index\">\n" +
                    "                    <if test=\"item.errorMsg != null\">\n" +
                    "                        when true then  #{item.errorMsg}\n" +
                    "                    </if>\n" +
                    "\n" +
                    "                </foreach>\n" +
                    "            </trim>\n" +
                    "            <trim prefix=\"modifieddate = case\" suffix=\"end,\">\n" +
                    "                <foreach collection=\"list\" item=\"item\" index=\"index\">\n" +
                    "                    <if test=\"item.modifieddate != null\">\n" +
                    "                        when true then  now() \n" +
                    "                    </if>\n" +
                    "                </foreach>\n" +
                    "            </trim>\n" +
                    "        </trim>\n" +
                    "        where id in\n" +
                    "        <foreach item=\"item\" collection=\"list\" open=\"(\" separator=\",\" close=\")\">\n" +
                    "            #{item.id}\n" +
                    "        </foreach>  " +
                    "</script>")
    int batchUpdate(@Param("list") List<SgBChannelSynstockQ> list);
}