package com.jackrain.nea.sg.oms.logic;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.config.SgStorageControlConfig;
import com.jackrain.nea.sg.basic.logic.SgRedisStorageLogic;
import com.jackrain.nea.sg.basic.mapper.SgBStorageMapper;
import com.jackrain.nea.sg.basic.model.request.SgRedisStorageQueryModel;
import com.jackrain.nea.sg.basic.model.request.SgStoreWithPrioritySearchItemRequest;
import com.jackrain.nea.sg.basic.model.request.StStockPriorityRequest;
import com.jackrain.nea.sg.basic.model.result.SgRedisStorageQueryResult;
import com.jackrain.nea.sg.basic.model.result.SgStoreWithPrioritySearchItemResult;
import com.jackrain.nea.sg.basic.model.result.SgStoreWithPrioritySearchResult;
import com.jackrain.nea.sg.basic.model.table.SgBStorage;
import com.jackrain.nea.sg.send.mapper.SgBSendItemMapper;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.*;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/4/23 15:22
 */
@Component
@Slf4j
public class SgStorageOmsQueryLogic {

    @Autowired
    private SgBSendItemMapper sgBSendItemMapper;

    @Autowired
    private SgBStorageMapper sgBStorageMapper;

    @Autowired
    private SgRedisStorageLogic sgRedisStorageLogic;

    @Autowired
    private SgStorageControlConfig sgStorageControlConfig;

    /**
     * 根据单据原店仓占用信息计算库存释放计划
     *
     * @param itemRequest
     * @param billNo
     * @param loginUser
     * @return
     */
    public ValueHolderV14<SgStoreWithPrioritySearchResult> getSgStoreReleasePlan(
            SgStoreWithPrioritySearchItemRequest itemRequest,
            String billNo,
            User loginUser) {

        ValueHolderV14<SgStoreWithPrioritySearchResult> holder = new ValueHolderV14<>();
        SgStoreWithPrioritySearchResult result = new SgStoreWithPrioritySearchResult();
        List<SgStoreWithPrioritySearchItemResult> itemResultList = new ArrayList<>();
        List<StStockPriorityRequest> priorityList = itemRequest.getPriorityList();
        HashMap<String, SgRedisStorageQueryResult> redisQueryResult = null;
        List<SgRedisStorageQueryModel> sgRedisStorageQueryList = null;
        String redisKey;

        HashMap<Long, Map> occupyedStoreMap;

        if (CollectionUtils.isEmpty(priorityList)) {
            priorityList = new ArrayList<>();
        }

        //获取指定商品条码的店仓占用信息
        List<Map<String, Object>> OmsSelectList = sgBSendItemMapper.selectStoreInfoBySku(
                billNo, itemRequest.getSourceItemId(), itemRequest.getPsCSkuId());

        if (log.isDebugEnabled()) {
            log.debug("SgStorageOmsQueryLogic.getSgStoreReleasePlan. ReturnResult:OmsSelectList:{};"
                    , JSONObject.toJSONString(OmsSelectList));
        }

        if (!CollectionUtils.isEmpty(OmsSelectList)) {

            occupyedStoreMap = new HashMap<>();
            int priorityLevel = 1;

            for (Map omsSelectItem : OmsSelectList) {

                occupyedStoreMap.put((Long) omsSelectItem.get("cp_c_store_id"),
                        omsSelectItem);

                //逻辑仓优先级未设定的情况下，根据原占用的逻辑仓（修改日降序）进行释放
                if (CollectionUtils.isEmpty(priorityList)) {
                    StStockPriorityRequest priority = new StStockPriorityRequest();
                    priority.setSupplyStoreId((Long) omsSelectItem.get("cp_c_store_id"));
                    priority.setPriority(priorityLevel);
                    priorityList.add(priority);
                    priorityLevel++;
                }
            }

        } else {
            log.debug("SgStorageQueryLogic.searchSgStoreForRelease 单据原店仓占用信息获取失败！来源单据编号:{}，来源单据明细编号:{}",
                    billNo, itemRequest.getSourceItemId());
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("单据原店仓占用信息获取失败！",
                    loginUser.getLocale(), billNo, itemRequest.getSourceItemId()));
            return holder;
        }

        //根据虚拟仓优先级列表降序排序
        Collections.reverse(priorityList);

        //剩余未占用数量
        BigDecimal remainPinoutQty = itemRequest.getQtyChange();

        boolean isRedisFunction = sgStorageControlConfig.isRedisFunction();

        if (isRedisFunction) {

            sgRedisStorageQueryList = new ArrayList<>();

            for (StStockPriorityRequest stockPriority : priorityList) {

                SgRedisStorageQueryModel queryModel = new SgRedisStorageQueryModel();
                queryModel.setCpCStoreId(stockPriority.getSupplyStoreId());
                queryModel.setPsCSkuId(itemRequest.getPsCSkuId());

                sgRedisStorageQueryList.add(queryModel);

            }

            redisQueryResult = sgRedisStorageLogic.queryStorage(sgRedisStorageQueryList, loginUser).getData();

        }

        //遍历逻辑仓优先级
        for (StStockPriorityRequest stockPriority : priorityList) {

            if (remainPinoutQty.compareTo(BigDecimal.ZERO) < 0 &&
                    occupyedStoreMap.containsKey(stockPriority.getSupplyStoreId())) {

                SgBStorage updateRecord;
                redisKey = stockPriority.getSupplyStoreId() + ":" + itemRequest.getPsCSkuId();

                if (redisQueryResult != null &&
                        redisQueryResult.containsKey(redisKey)) {

                    updateRecord = new SgBStorage();
                    BeanUtils.copyProperties(redisQueryResult.get(redisKey), updateRecord);

                } else {

                    updateRecord = sgBStorageMapper.selectOne(
                            new QueryWrapper<SgBStorage>().lambda()
                                    .eq(SgBStorage::getCpCStoreId, stockPriority.getSupplyStoreId())
                                    .eq(SgBStorage::getPsCSkuId, itemRequest.getPsCSkuId()));
                }

                if (log.isDebugEnabled()) {
                    log.debug("SgStorageOmsQueryLogic.getSgStoreReleasePlan. step 1 ReturnResult:updateRecord:{};"
                            , JSONObject.toJSONString(updateRecord));
                }

                if (updateRecord != null && updateRecord.getQtyAvailable() != null) {

                    SgStoreWithPrioritySearchItemResult itemResult = new SgStoreWithPrioritySearchItemResult();
                    itemResult.setCpCStoreId(stockPriority.getSupplyStoreId());
                    itemResult.setCpCStoreEcode(stockPriority.getSupplyStoreEcode());
                    itemResult.setCpCStoreEname(stockPriority.getSupplyStoreEname());
                    itemResult.setPsCSkuId(itemRequest.getPsCSkuId());
                    itemResult.setSourceItemId(itemRequest.getSourceItemId());

                    //判断当前店仓的可用库存是否充足
                    if (updateRecord.getQtyAvailable().compareTo(remainPinoutQty) >= 0) {
                        //可用充足的情况下，使用单据全量数据进行占用
                        itemResult.setQtyPreout(remainPinoutQty);
                        remainPinoutQty = remainPinoutQty.subtract(remainPinoutQty);
                    } else {
                        //可用不足的情况下，使用库存可用量进行占用
                        itemResult.setQtyPreout(updateRecord.getQtyAvailable());
                        remainPinoutQty = remainPinoutQty.subtract(updateRecord.getQtyAvailable());
                    }

                    itemResultList.add(itemResult);
                    //排除已释放过的店仓
                    occupyedStoreMap.remove(stockPriority.getSupplyStoreId());

                    if (remainPinoutQty.compareTo(BigDecimal.ZERO) <= 0) {
                        break;
                    }

                }
            }

        }

        //剩余未释放数量<0的情况下，根据原店仓释放信息进行释放
        if (remainPinoutQty.compareTo(BigDecimal.ZERO) < 0) {

            if (isRedisFunction) {

                sgRedisStorageQueryList = new ArrayList<>();

                for (Long storeId : occupyedStoreMap.keySet()) {

                    SgRedisStorageQueryModel queryModel = new SgRedisStorageQueryModel();
                    queryModel.setCpCStoreId(storeId);
                    queryModel.setPsCSkuId(itemRequest.getPsCSkuId());

                    sgRedisStorageQueryList.add(queryModel);

                }

                redisQueryResult = sgRedisStorageLogic.queryStorage(sgRedisStorageQueryList, loginUser).getData();

            }

            for (Long storeId : occupyedStoreMap.keySet()) {

                SgBStorage updateRecord;
                redisKey = storeId + ":" + itemRequest.getPsCSkuId();

                if (redisQueryResult != null &&
                        redisQueryResult.containsKey(redisKey)) {

                    updateRecord = new SgBStorage();
                    BeanUtils.copyProperties(redisQueryResult.get(redisKey), updateRecord);

                } else {

                    updateRecord = sgBStorageMapper.selectOne(
                            new QueryWrapper<SgBStorage>().lambda()
                                    .eq(SgBStorage::getCpCStoreId, storeId)
                                    .eq(SgBStorage::getPsCSkuId, itemRequest.getPsCSkuId()));

                }

                if (log.isDebugEnabled()) {
                    log.debug("SgStorageOmsQueryLogic.getSgStoreReleasePlan. step 2 ReturnResult:updateRecord:{};"
                            , JSONObject.toJSONString(updateRecord));
                }

                if (updateRecord != null && updateRecord.getQtyAvailable() != null) {

                    SgStoreWithPrioritySearchItemResult itemResult = new SgStoreWithPrioritySearchItemResult();
                    itemResult.setCpCStoreId(storeId);
                    itemResult.setCpCStoreEcode((String) occupyedStoreMap.get(storeId).get("cp_c_store_ecode"));
                    itemResult.setCpCStoreEname((String) occupyedStoreMap.get(storeId).get("cp_c_store_ename"));
                    itemResult.setPsCSkuId(itemRequest.getPsCSkuId());
                    itemResult.setSourceItemId(itemRequest.getSourceItemId());

                    //判断当前店仓的可用库存是否充足
                    if (updateRecord.getQtyAvailable().compareTo(remainPinoutQty) >= 0) {
                        //可用充足的情况下，使用单据全量数据进行释放
                        itemResult.setQtyPreout(remainPinoutQty);
                        remainPinoutQty = remainPinoutQty.subtract(remainPinoutQty);
                    } else {
                        //可用不足的情况下，使用库存可用量进行释放
                        itemResult.setQtyPreout(updateRecord.getQtyAvailable());
                        remainPinoutQty = remainPinoutQty.subtract(updateRecord.getQtyAvailable());
                    }

                    itemResultList.add(itemResult);

                    if (remainPinoutQty.compareTo(BigDecimal.ZERO) <= 0) {
                        break;
                    }

                }
            }
        }

        if (remainPinoutQty.compareTo(BigDecimal.ZERO) < 0) {

            log.debug("SgStorageQueryLogic.searchSgStoreForRelease根据单据原店仓占用信息计算库存释放计划失败！" +
                            "来源单据编号:{}，来源单据明细编号:{}",
                    billNo, itemRequest.getSourceItemId());
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("根据单据原店仓占用信息计算库存释放计划失败！",
                    loginUser.getLocale(), billNo, itemRequest.getSourceItemId()));

            return holder;
        }

        result.setItemResultList(itemResultList);
        holder.setData(result);

        if (log.isDebugEnabled()) {
            log.debug("Finish SgStorageOmsQueryLogic.getSgStoreReleasePlan. ReturnResult:result:{} remainPinoutQty:{};"
                    , JSONObject.toJSONString(result),remainPinoutQty);
        }

        holder.setCode(ResultCode.SUCCESS);
        holder.setMessage(Resources.getMessage("根据单据原店仓占用信息计算库存释放计划成功！", loginUser.getLocale()));

        return holder;
    }

}
