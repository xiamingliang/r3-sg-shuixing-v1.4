package com.jackrain.nea.sg.oms.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.data.basic.model.request.SkuInfoQueryRequest;
import com.jackrain.nea.data.basic.services.BasicPsQueryService;
import com.jackrain.nea.model.util.ModelUtil;
import com.jackrain.nea.ps.api.result.PsCProSkuResult;
import com.jackrain.nea.sg.basic.common.SystemUserResource;
import com.jackrain.nea.sg.oms.mapper.SgBChannelStorageMapper;
import com.jackrain.nea.sg.oms.mapper.SgChannelProductMapper;
import com.jackrain.nea.sg.oms.model.request.SgChannelProductSaveRequest;
import com.jackrain.nea.sg.oms.model.table.SgBChannelProduct;
import com.jackrain.nea.sg.oms.model.table.SgBChannelStorage;
import com.jackrain.nea.sg.oms.model.table.SgBChannelStorageBuffer;
import com.jackrain.nea.sg.oms.model.tableExtend.SgBChannelStorageBufferExtend;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.*;

/**
 * @author zhu lin yu
 * @since 2019/5/14
 * create at : 2019/5/14 10:26
 */
@Component
@Slf4j
public class SgChannelProductSaveService {

    @Autowired
    private SgChannelProductMapper sgChannelProductMapper;

    @Autowired
    private BasicPsQueryService basicPsQueryService;

    @Autowired
    SgChannelStorageOmsBufferService sgChannelStorageOmsBufferService;

    @Autowired
    SgBChannelStorageMapper sgBChannelStorageMapper;

    /**
     * 保存或新增渠道商品
     *
     * @param sgChannelProductSaveRequest 淘宝云枢纽商品信息集合
     * @return 执行结果
     */
    public ValueHolderV14 saveChannelProduct(SgChannelProductSaveRequest sgChannelProductSaveRequest) {
        ValueHolderV14 holder;
        long startTime = System.currentTimeMillis();

        //入参日志记录
        if (log.isDebugEnabled()) {
            log.debug("Start SgChannelProductSaveService.saveChannelProduct. ReceiveParams:SgChannelProductSaveRequest="
                    + JSONObject.toJSONString(sgChannelProductSaveRequest) + ";");
        }

        //检查入参
        holder = checkServiceParam(sgChannelProductSaveRequest);
        if (ResultCode.FAIL == holder.getCode()) {
            return holder;
        }

        //获取渠道信息集合
        List<SgBChannelProduct> sgBChannelProductList = sgChannelProductSaveRequest.getSgBChannelProductList();

        //定义总条数和错误条数
        int totalNum = sgBChannelProductList.size();
        int errorNum = 0;

        //查询所有渠道商品的条码信息，商品信息，颜色尺寸信息
        SkuInfoQueryRequest skuInfoQueryRequest = new SkuInfoQueryRequest();
        List<String> ecodeList = new ArrayList<>();
        sgBChannelProductList.forEach(preSgBChannelProduct -> {
            if (preSgBChannelProduct.getNumiid() != null && preSgBChannelProduct.getSkuId() != null) {
                //获取商品条码编码
                String psCSkuEcode = preSgBChannelProduct.getPsCSkuEcode();
                ecodeList.add(psCSkuEcode);
            } else {
                log.error(this.getClass().getName() + " skuId:{},numId:{},数据不完整",
                        preSgBChannelProduct.getSkuId(), preSgBChannelProduct.getNumiid());
            }
        });
        Map<String, PsCProSkuResult> psCProSkuResultMap = new HashMap<>();
        skuInfoQueryRequest.setSkuEcodeList(ecodeList);
        try {
            psCProSkuResultMap = basicPsQueryService.getSkuInfoByEcode(skuInfoQueryRequest);
        } catch (Exception e) {
            log.error("调用basicPsQueryService服务失败：{}  ", e.getMessage());
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(e.getMessage());
            return holder;
        }

        //对于新增的商品和更改掉psCEcode的商品，需要插入渠道计算缓存池
        List<SgBChannelStorageBuffer> sgBChannelStorageBufferList = new ArrayList<>();
        List<SgBChannelProduct> batchInsertSgBChannelProduct = new ArrayList<>();

        //判断入参数据是新增还是修改，并执行
        for (SgBChannelProduct preSgBChannelProduct : sgBChannelProductList) {
            try {
                //numiid和ps_c_sku_ecode必须不为空
                if ((preSgBChannelProduct.getNumiid() == null && preSgBChannelProduct.getSkuId() == null) || preSgBChannelProduct.getPsCSkuEcode() == null) {
                    errorNum++;
                } else {
                    //构建查询条件
                    QueryWrapper<SgBChannelProduct> queryWrapper = new QueryWrapper<>();
                    if (preSgBChannelProduct.getSkuId() != null) {
                        queryWrapper.eq("sku_id", preSgBChannelProduct.getSkuId());
                    }
                    queryWrapper.eq("numiid", preSgBChannelProduct.getNumiid());
                    SgBChannelProduct sgBChannelProduct = this.sgChannelProductMapper.selectOne(queryWrapper);

                    String psCSkuEcode = preSgBChannelProduct.getPsCSkuEcode();
                    PsCProSkuResult psCProSkuResult = psCProSkuResultMap.get(psCSkuEcode.toUpperCase());

                    //判断该渠道商品的条码是否查询到条码、商品、颜色尺寸信息
                    if (psCProSkuResult == null) {
                        errorNum++;
                        log.error("条码编码为" + psCSkuEcode + "未查到条码、商品、颜色尺寸信息");
                        continue;
                    }

                    //为0新增，不为0更新
                    if (sgBChannelProduct == null) {
                        //封装条码，商品，颜色尺寸信息,封装渠道库存计算缓存池入参
                        dataEncapsulation(preSgBChannelProduct, psCProSkuResult, batchInsertSgBChannelProduct, sgBChannelStorageBufferList);
                    } else {
                        preSgBChannelProduct.setModifieddate(new Timestamp(System.currentTimeMillis()));

                        //如果更新了商品的psCEcode,也需要添加到渠道库存计算缓存池
                        if (!(Objects.equals(preSgBChannelProduct.getPsCSkuEcode(), sgBChannelProduct.getPsCSkuEcode()))){
                            SgBChannelStorageBuffer sgBChannelStorageBuffer = new SgBChannelStorageBuffer();
                            sgBChannelStorageBuffer.setWareType("Y".equalsIgnoreCase(psCProSkuResult.getIsGroup()) ? 1 : 0);
                            sgBChannelStorageBuffer.setCpCShopId(preSgBChannelProduct.getCpCShopId());
                            sgBChannelStorageBuffer.setCpCShopTitle(preSgBChannelProduct.getCpCShopTitle());
                            if (preSgBChannelProduct.getSkuId() != null) {
                                sgBChannelStorageBuffer.setSkuId(preSgBChannelProduct.getSkuId());
                            }
                            sgBChannelStorageBuffer.setDealStatus(SgBChannelStorageBufferExtend.DealStatusEnum.UN_DEAL.getCode());
                            sgBChannelStorageBufferList.add(sgBChannelStorageBuffer);

                            //如果更新了商品的psCEcode,去条码表找到对应的条码信息
                            preSgBChannelProduct.setPsCProId(psCProSkuResult.getPsCProId());
                            preSgBChannelProduct.setPsCProEcode(psCProSkuResult.getPsCProEcode());
                            preSgBChannelProduct.setPsCProEname(psCProSkuResult.getPsCProEname());
                            preSgBChannelProduct.setPsCSkuId(psCProSkuResult.getId());
                            preSgBChannelProduct.setPsCSpec1Id(psCProSkuResult.getPsCSpec1objId());
                            preSgBChannelProduct.setPsCSpec1Ecode(psCProSkuResult.getClrsEcode());
                            preSgBChannelProduct.setPsCSpec1Ename(psCProSkuResult.getClrsEname());
                            preSgBChannelProduct.setPsCSpec2Id(psCProSkuResult.getPsCSpec2objId());
                            preSgBChannelProduct.setPsCSpec2Ecode(psCProSkuResult.getSizesEcode());
                            preSgBChannelProduct.setPsCSpec2Ename(psCProSkuResult.getSizesEname());
                            preSgBChannelProduct.setGbcode(psCProSkuResult.getGbcode());
                        }
                        int update = this.sgChannelProductMapper.update(preSgBChannelProduct, queryWrapper);
                        if (update < 0) {
                            log.error(this.getClass().getName() + " 更新渠道商品出错,明细为 {}", preSgBChannelProduct);
                        } else {
                            //在psCEcode变化时，删除渠道库存的记录

                            if (!(Objects.equals(preSgBChannelProduct.getPsCSkuEcode(), sgBChannelProduct.getPsCSkuEcode()))) {
                                log.info(this.getClass().getName() + " 开始删除渠道库存记录,渠道id:{},skuId:{}"
                                        , preSgBChannelProduct.getCpCShopId(), preSgBChannelProduct.getSkuId());

                                UpdateWrapper<SgBChannelStorage> updateWrapper = new UpdateWrapper<>();
                                updateWrapper.eq("numiid", preSgBChannelProduct.getNumiid());
                                updateWrapper.eq("cp_c_shop_id", preSgBChannelProduct.getCpCShopId());
                                if (preSgBChannelProduct.getSkuId() != null) {
                                    updateWrapper.eq("sku_id", preSgBChannelProduct.getSkuId());
                                }
                                int delete = sgBChannelStorageMapper.delete(updateWrapper);
                                if (delete < 0) {
                                    log.error(this.getClass().getName() + " 删除渠道库存出错,明细为 {}", preSgBChannelProduct);
                                }
                            }
                        }
                    }
                }
            } catch (Exception e) {
                log.error("错误信息:{} ", e.getMessage());
                holder.setCode(ResultCode.FAIL);
                holder.setMessage(e.getMessage());
            }
        }


        if (CollectionUtils.isNotEmpty(batchInsertSgBChannelProduct)) {
            int i = this.sgChannelProductMapper.batchInsert(batchInsertSgBChannelProduct);
            if (i < 0) {
                log.error(this.getClass().getName() + " 新增渠道商品出错,明细为 {}", JSONObject.toJSONString(batchInsertSgBChannelProduct));
            }
        }

        //调用渠道库存计算缓存池服务
        if (CollectionUtils.isNotEmpty(sgBChannelStorageBufferList)){
            try {
                log.info(this.getClass().getName() + " 开始调用新增渠道库存计算缓存池方法,明细:{}", JSONObject.toJSONString(sgBChannelStorageBufferList));
                ValueHolderV14 holderV14 = sgChannelStorageOmsBufferService.addSgBChannelStorageBuffer(sgBChannelStorageBufferList, null);
                if (0 == (int) holderV14.getData()) {
                    log.error(this.getClass().getName() + " 调用新增渠道库存计算缓存池出错,明细为 {}", JSONObject.toJSONString(sgBChannelStorageBufferList));
                }
            } catch (Exception e) {
                log.error(this.getClass().getName() + " 新增渠道库存计算缓存池异常,明细为 {}", JSONObject.toJSONString(sgBChannelStorageBufferList), e);
            }
        }


        //封装返回结果
        if (errorNum > 0) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage("共" + totalNum + "条数据，失败" + errorNum + "条");
        } else {
            holder.setCode(ResultCode.SUCCESS);
            holder.setMessage("调用成功");
        }

        if (log.isDebugEnabled()) {
            log.debug("Finish SgChannelProductSaveService.saveChannelProduct. ReturnResult:holder:{} spend time:{}ms;"
                    , JSONObject.toJSONString(holder), System.currentTimeMillis() - startTime);
        }
        return holder;
    }

    /**
     * 根据淘宝商品编码查询条码信息，商品信息，颜色尺寸信息，并新增
     *
     * @param preSgBChannelProduct 淘宝云枢纽商品信息
     */
    private void dataEncapsulation(SgBChannelProduct preSgBChannelProduct, PsCProSkuResult psCProSkuResult
            , List<SgBChannelProduct> batchInsertSgBChannelProduct, List<SgBChannelStorageBuffer> sgBChannelStorageBufferList) {
        User rootUser = SystemUserResource.getRootUser();
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        SgBChannelStorageBuffer sgBChannelStorageBuffer = new SgBChannelStorageBuffer();

        Long id = ModelUtil.getSequence("sg_b_channel_product");
        preSgBChannelProduct.setId(id);
        preSgBChannelProduct.setPsCSkuId(psCProSkuResult.getId());
        preSgBChannelProduct.setPsCProId(psCProSkuResult.getPsCProId());
        preSgBChannelProduct.setPsCProEcode(psCProSkuResult.getPsCProEcode());
        preSgBChannelProduct.setPsCProEname(psCProSkuResult.getPsCProEname());
        preSgBChannelProduct.setGbcode(psCProSkuResult.getGbcode());
        preSgBChannelProduct.setPsCSpec1Id(psCProSkuResult.getPsCSpec1objId());
        preSgBChannelProduct.setPsCSpec1Ecode(psCProSkuResult.getClrsEcode());
        preSgBChannelProduct.setPsCSpec1Ename(psCProSkuResult.getClrsEname());
        preSgBChannelProduct.setPsCSpec2Id(psCProSkuResult.getPsCSpec2objId());
        preSgBChannelProduct.setPsCSpec2Ecode(psCProSkuResult.getSizesEcode());
        preSgBChannelProduct.setPsCSpec2Ename(psCProSkuResult.getSizesEname());

        preSgBChannelProduct.setAdClientId(Integer.valueOf(rootUser.getClientId()).longValue());
        preSgBChannelProduct.setAdOrgId(Integer.valueOf(rootUser.getOrgId()).longValue());
        preSgBChannelProduct.setOwnerid(rootUser.getId().longValue());
        preSgBChannelProduct.setModifierid(rootUser.getId().longValue());
        preSgBChannelProduct.setCreationdate(timestamp);
        preSgBChannelProduct.setModifieddate(timestamp);
        preSgBChannelProduct.setModifiername(rootUser.getName());
        preSgBChannelProduct.setModifierename(rootUser.getEname());
        preSgBChannelProduct.setOwnername(rootUser.getName());
        preSgBChannelProduct.setOwnerename(rootUser.getEname());
        preSgBChannelProduct.setOwnerename(rootUser.getEname());
        preSgBChannelProduct.setWareType("Y".equalsIgnoreCase(psCProSkuResult.getIsGroup()) ? 1 : 0);
        batchInsertSgBChannelProduct.add(preSgBChannelProduct);

        //封装渠道计算缓存池数据
        sgBChannelStorageBuffer.setWareType("Y".equalsIgnoreCase(psCProSkuResult.getIsGroup()) ? 1 : 0);
        sgBChannelStorageBuffer.setCpCShopId(preSgBChannelProduct.getCpCShopId());
        sgBChannelStorageBuffer.setCpCShopTitle(preSgBChannelProduct.getCpCShopTitle());
        sgBChannelStorageBuffer.setSkuId(preSgBChannelProduct.getSkuId());
        sgBChannelStorageBuffer.setDealStatus(SgBChannelStorageBufferExtend.DealStatusEnum.UN_DEAL.getCode());
        sgBChannelStorageBufferList.add(sgBChannelStorageBuffer);
    }


    /**
     * 入参校验
     *
     * @param request 淘宝云枢纽商品信息集合
     * @return 执行结果
     */
    private ValueHolderV14 checkServiceParam(SgChannelProductSaveRequest request) {
        ValueHolderV14 holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");

        if (request == null) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage("请求体不能为空!");
        }

        if (request != null && CollectionUtils.isEmpty(request.getSgBChannelProductList())) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage("渠道商品信息不能为空!");
        }
        return holder;
    }
}
