package com.jackrain.nea.sg.oms.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Lists;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.oms.mapper.SgBChannelStorageMapper;
import com.jackrain.nea.sg.oms.model.request.SgChannelStorageQueryRequest;
import com.jackrain.nea.sg.oms.model.table.SgBChannelStorage;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zhu lin yu
 * @since 2019-11-11
 * create at : 2019-11-11 20:52
 */
@Slf4j
@Component
public class SgChannelStorageQueryCmdService {

    public ValueHolderV14<List<SgBChannelStorage>> queryChannelStorage(List<SgChannelStorageQueryRequest> requestList) {
        if (log.isDebugEnabled()) {
            log.debug("Start SgChannelStorageQueryCmdService.queryChannelStorage. ReceiveParams:params:{};" + JSONObject.toJSONString(requestList));
        }

        ValueHolderV14<List<SgBChannelStorage>> valueHolderV14;

        //参数校验
        valueHolderV14 = checkParam(requestList);
        if (ResultCode.FAIL == valueHolderV14.getCode()) {
            return valueHolderV14;
        }

        SgBChannelStorageMapper mapper = ApplicationContextHandle.getBean(SgBChannelStorageMapper.class);
        List<SgBChannelStorage> resultList = new ArrayList<>();
        for (SgChannelStorageQueryRequest request : requestList) {
            Long shopId = request.getShopId();
            List<String> skuEcodeList = request.getSkuEcodeList();

            if (shopId == null) {
                valueHolderV14.setCode(ResultCode.FAIL);
                valueHolderV14.setMessage("渠道id不能为空");
                return valueHolderV14;
            } else if (CollectionUtils.isEmpty(skuEcodeList)) {
                valueHolderV14.setCode(ResultCode.FAIL);
                valueHolderV14.setMessage("查询的条码不能为空");
                return valueHolderV14;
            }

            List<List<String>> partition = Lists.partition(skuEcodeList, SgConstants.SG_NORMAL_MAX_QUERY_PAGE_SIZE);

            for (List<String> ecodeList : partition) {
                List<SgBChannelStorage> sgBChannelStorageList = mapper.selectList(new QueryWrapper<SgBChannelStorage>().lambda()
                        .eq(SgBChannelStorage::getCpCShopId, shopId).in(SgBChannelStorage::getSkuId, ecodeList));

                if (CollectionUtils.isNotEmpty(sgBChannelStorageList)) {
                    resultList.addAll(sgBChannelStorageList);
                }
            }

        }

        valueHolderV14.setCode(ResultCode.SUCCESS);
        valueHolderV14.setMessage("查询成功");
        valueHolderV14.setData(resultList);

        if (log.isDebugEnabled()) {
            log.debug("Finish SgChannelStorageQueryCmdService.queryChannelStorage. ReturnParams:params:{};" + JSONObject.toJSONString(valueHolderV14));
        }
        return valueHolderV14;
    }

    private ValueHolderV14<List<SgBChannelStorage>> checkParam(List<SgChannelStorageQueryRequest> requestList) {
        ValueHolderV14<List<SgBChannelStorage>> valueHolderV14 = new ValueHolderV14<>(ResultCode.FAIL, "");

        if (CollectionUtils.isEmpty(requestList)) {
            valueHolderV14.setMessage("request is null");
            return valueHolderV14;
        }

        valueHolderV14.setCode(ResultCode.SUCCESS);
        return valueHolderV14;
    }
}
