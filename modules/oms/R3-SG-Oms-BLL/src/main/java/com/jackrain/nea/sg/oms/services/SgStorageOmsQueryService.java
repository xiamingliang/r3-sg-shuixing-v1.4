package com.jackrain.nea.sg.oms.services;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.logic.SgStorageQtyCalculateLogic;
import com.jackrain.nea.sg.basic.model.SgStorageUpdateCommonModel;
import com.jackrain.nea.sg.basic.model.request.SgStorageUpdateBillRequest;
import com.jackrain.nea.sg.basic.model.request.SgStoreWithPrioritySearchItemRequest;
import com.jackrain.nea.sg.basic.model.result.SgStoreWithPrioritySearchItemResult;
import com.jackrain.nea.sg.basic.model.result.SgStoreWithPrioritySearchResult;
import com.jackrain.nea.sg.basic.services.SgStorageSynchService;
import com.jackrain.nea.sg.oms.logic.SgStorageOmsQueryLogic;
import com.jackrain.nea.sg.oms.model.request.SgStoreWithPrioritySearchRequest;
import com.jackrain.nea.sg.send.mapper.SgBSendItemMapper;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.*;

/**
 * @Description:
 * @Author: chenb
 * @Date: 2019/4/23 12:45
 */
@Component
@Slf4j
public class SgStorageOmsQueryService {

    @Autowired
    private SgBSendItemMapper sgBSendItemMapper;

    @Autowired
    private SgStorageOmsQueryLogic sgStorageOmsQueryLogic;

    @Autowired
    private SgStorageQtyCalculateLogic sgStorageQtyCalculateLogic;

    @Autowired
    private SgStorageSynchService sgStorageSynchService;

    public ValueHolderV14<SgStoreWithPrioritySearchResult> searchSgStoreWithPriority(SgStoreWithPrioritySearchRequest request) {

        if (log.isDebugEnabled()) {
            log.debug("Start SgStorageOmsQueryService.searchSgStoreWithPriority. ReceiveParams:request="
                    + JSONObject.toJSONString(request) + ";");
        }

        ValueHolderV14<SgStoreWithPrioritySearchResult> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");
        ValueHolderV14<SgStoreWithPrioritySearchResult> itemResult = null;
        List<SgStoreWithPrioritySearchItemResult> itemResultList = new ArrayList<>();
        List<SgStoreWithPrioritySearchItemRequest> itemListForCalc = new ArrayList<>();
        SgStoreWithPrioritySearchResult searchResult = new SgStoreWithPrioritySearchResult();
        List<SgStoreWithPrioritySearchItemResult> outStockItemList = new ArrayList<>();

        List<SgStoreWithPrioritySearchItemRequest> itemList = request.getItemList();
        User loginUser = request.getLoginUser();
        String billNo = request.getBillNo();

        //是否是新增逻辑发货单，用于预判，简化逻辑发货单判断逻辑
        boolean isAddBillFlg = false;

        holder = checkServiceParam(request, loginUser);
        if (ResultCode.FAIL == holder.getCode()) {
            return holder;
        }

        //获取单据历史占用信息
        HashMap<String, BigDecimal> sumQtyGSkuResult;

        List<Map<String, Object>> sumQtyGSkuList = sgBSendItemMapper.selectSumQtyGroupBySku(billNo);

        if (log.isDebugEnabled()) {
            log.debug("SgStorageOmsQueryService.searchSgStoreWithPriority. selectSumQtyGroupBySku:sumQtyGSkuList="
                    + JSONObject.toJSONString(sumQtyGSkuList) + ";");
        }

        if (!CollectionUtils.isEmpty(sumQtyGSkuList)) {

            sumQtyGSkuResult = new HashMap();

            for (Map omsSelectItem : sumQtyGSkuList) {

                sumQtyGSkuResult.put(omsSelectItem.get("ps_c_sku_id") + "_" +
                                omsSelectItem.get("source_bill_item_id"),
                        (BigDecimal) omsSelectItem.get("sum_qty_preout"));
            }

            String itemKey;

            /** 全渠道单据库存变动更新 **/

            //遍历当前订单中的明细信息
            for (SgStoreWithPrioritySearchItemRequest itemInfo : itemList) {

                itemKey = itemInfo.getPsCSkuId() + "_" + itemInfo.getSourceItemId();

                //从修改前的订单占用信息中获取当前SKU的合计占用数量
                if (sumQtyGSkuResult.containsKey(itemKey)) {

                    //当修改前的订单占用合计数量与当前订单的占用数量不一致的情况下，添加到待更新明细列表中
                    int diff = itemInfo.getQtyChange().compareTo(sumQtyGSkuResult.get(itemKey));

                    if (diff != 0) {

                        if (diff > 0) {
                            itemInfo.setOperateType(SgConstantsIF.OPERATE_TYPE_OCCUPY);
                        } else if (diff < 0) {
                            itemInfo.setOperateType(SgConstantsIF.OPERATE_TYPE_RELEASE);
                        }

                        itemInfo.setQtyChange(itemInfo.getQtyChange().subtract(sumQtyGSkuResult.get(itemKey)));
                        itemListForCalc.add(itemInfo);
                    }

                    //删除已匹配的SKU
                    sumQtyGSkuResult.remove(itemKey);

                } else {

                    //修改前的订单占用信息未匹配到该SKU的情况下，直接添加到待更新明细列表中
                    itemListForCalc.add(itemInfo);
                }

            }

            //修改前的订单占用信息有残余SKU（修改前明细多）
            if (SgConstantsIF.ITEM_UPDATE_TYPE_ALL.equals(request.getItemUpdateType()) &&
                    !sumQtyGSkuResult.isEmpty()) {

                String[] itemKeyList;

                //遍历当前订单中的明细信息,直接添加到待更新明细列表中
                for (String key : sumQtyGSkuResult.keySet()) {

                    itemKeyList = key.split("_");

                    //针对原流水中非零占用进行释放操作
                    if (sumQtyGSkuResult.get(key).compareTo(BigDecimal.ZERO) != 0 &&
                            !StringUtils.isEmpty(itemKeyList[0]) && !StringUtils.isEmpty(itemKeyList[1])) {
                        SgStoreWithPrioritySearchItemRequest itemRequest = new SgStoreWithPrioritySearchItemRequest();
                        itemRequest.setPsCSkuId(Long.valueOf(itemKeyList[0]));
                        itemRequest.setSourceItemId(Long.valueOf(itemKeyList[1]));
                        //直接取反
                        itemRequest.setQtyChange(sumQtyGSkuResult.get(key).negate());
                        itemRequest.setOperateType(SgConstantsIF.OPERATE_TYPE_RELEASE);
                        itemListForCalc.add(itemRequest);
                    }
                }
            }

        } else {

            /** 全渠道单据库存变动新增 **/
            isAddBillFlg = true;

            //直接添加到待更新明细列表中
            itemListForCalc.addAll(itemList);
        }

        for (SgStoreWithPrioritySearchItemRequest itemInfo : itemListForCalc) {

            //明细虚拟仓优先级列表为空时，获取主虚拟仓优先级列表
            if (CollectionUtils.isEmpty(itemInfo.getPriorityList())) {
                itemInfo.setPriorityList(request.getPriorityList());
            }

            if (SgConstantsIF.OPERATE_TYPE_OCCUPY == itemInfo.getOperateType()) {
                itemResult = sgStorageQtyCalculateLogic.getSgStoreOccupyPlan(itemInfo,
                        loginUser);
            } else {
                itemResult = sgStorageOmsQueryLogic.getSgStoreReleasePlan(itemInfo,
                        billNo,
                        loginUser);
            }

            if (itemResult.getData() != null) {

                if (SgConstantsIF.PREOUT_RESULT_SUCCESS != itemResult.getData().getPreoutUpdateResult()) {

                    if (!CollectionUtils.isEmpty(itemResult.getData().getOutStockItemList())) {
                        outStockItemList.addAll(itemResult.getData().getOutStockItemList());

                        //发生缺货的情况下，为了避免平台库存错误，添加强制同步渠道库存逻辑
                        List<SgStorageUpdateCommonModel> channelMqItemList = new ArrayList<>();
                        for (SgStoreWithPrioritySearchItemResult outItemResult : itemResult.getData().getOutStockItemList()) {

                            SgStorageUpdateCommonModel channelMqItem = new SgStorageUpdateCommonModel();
                            channelMqItem.setCpCStoreId(outItemResult.getCpCStoreId());
                            channelMqItem.setPsCSkuId(outItemResult.getPsCSkuId());
                            channelMqItem.setQtyPreoutChange(outItemResult.getQtyPreout());
                            channelMqItem.setBillNo(request.getBillNo());
                            channelMqItem.setChangeDate(new Date());
                            channelMqItemList.add(channelMqItem);
                        }

                        SgStorageUpdateBillRequest billRequest = new SgStorageUpdateBillRequest();

                        sgStorageSynchService.synchOthersStorage(channelMqItemList, loginUser);

                    }

                    searchResult.setPreoutUpdateResult(itemResult.getData().getPreoutUpdateResult());
                }

                itemResultList.addAll(itemResult.getData().getItemResultList());
            }

        }

        searchResult.setOutStockItemList(outStockItemList);
        searchResult.setItemResultList(itemResultList);
        searchResult.setAddBillFlg(isAddBillFlg);
        holder.setData(searchResult);

        return holder;

    }

    /**
     * @param request
     * @param loginUser
     * @return
     */
    private ValueHolderV14<SgStoreWithPrioritySearchResult> checkServiceParam(SgStoreWithPrioritySearchRequest request,
                                                                              User loginUser) {

        ValueHolderV14<SgStoreWithPrioritySearchResult> holder = new ValueHolderV14<>(ResultCode.SUCCESS, "");

        if (request == null) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("请求体为空！", loginUser.getLocale()));
            return holder;
        }

        if (request.getLoginUser() == null) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("操作用户信息不能为空！", loginUser.getLocale())
                    .concat(" " + holder.getMessage()));
        }

        if (CollectionUtils.isEmpty(request.getItemList())) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("供货仓信息不能为空！", loginUser.getLocale())
                    .concat(" " + holder.getMessage()));
        }

        if (CollectionUtils.isEmpty(request.getPriorityList())) {
            holder.setCode(ResultCode.FAIL);
            holder.setMessage(Resources.getMessage("供货仓优先级列表不能为空！", loginUser.getLocale())
                    .concat(" " + holder.getMessage()));
        }

        for (SgStoreWithPrioritySearchItemRequest itemRequest : request.getItemList()) {

            if (itemRequest.getPsCSkuId() == null) {
                holder.setCode(ResultCode.FAIL);
                holder.setMessage(Resources.getMessage("条码信息不能为空！", loginUser.getLocale())
                        .concat(" " + holder.getMessage()));
            }

            if (itemRequest.getQtyChange() == null) {
                holder.setCode(ResultCode.FAIL);
                holder.setMessage(Resources.getMessage("条码数量不能为空！", loginUser.getLocale())
                        .concat(" " + holder.getMessage()));
            }

            if (itemRequest.getSourceItemId() == null) {
                holder.setCode(ResultCode.FAIL);
                holder.setMessage(Resources.getMessage("来源单据明细ID不能为空！", loginUser.getLocale())
                        .concat(" " + holder.getMessage()));
            }

        }

        return holder;

    }
}
