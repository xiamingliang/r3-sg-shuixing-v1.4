package com.jackrain.nea.sg.send.services;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.send.api.SgSendCleanSaveCmd;
import com.jackrain.nea.sg.send.model.request.SgSendBillCleanRequest;
import com.jackrain.nea.sg.send.model.request.SgSendCleanSaveRequest;
import com.jackrain.nea.sg.send.model.request.SgSendSaveWithPriorityRequest;
import com.jackrain.nea.sg.send.model.result.SgSendBillCleanResult;
import com.jackrain.nea.sg.send.model.result.SgSendSaveWithPriorityResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author 舒威
 * @since 2019/4/26
 * create at : 2019/4/26 13:26
 */
@Slf4j
@Component
@Transactional(rollbackFor = Exception.class)
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgSendCleanSaveCmdImpl implements SgSendCleanSaveCmd {

    @Override
    public ValueHolderV14<SgSendBillCleanResult> cleanSgBSend(SgSendBillCleanRequest request) {
        SgSendCleanService cleanService = ApplicationContextHandle.getBean(SgSendCleanService.class);
        return cleanService.cleanSgBSend(request);
    }

    @Override
    public ValueHolderV14<SgSendSaveWithPriorityResult> cleanSaveSgBSend(SgSendCleanSaveRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("来源单据id[" + request.getSendBillCleanRequest().getSourceBillId()
                    + "],清空逻辑发货单后重新寻源占单入参:" + JSONObject.toJSONString(request) + ";");
        }

        SgSendCleanService cleanService = ApplicationContextHandle.getBean(SgSendCleanService.class);
        SgSendSaveWithPriorityService logicSearchService = ApplicationContextHandle.getBean(SgSendSaveWithPriorityService.class);
        ValueHolderV14<SgSendSaveWithPriorityResult> v14 = new ValueHolderV14<>();
        //清空逻辑发货单
        ValueHolderV14<SgSendBillCleanResult> cleanResult = cleanService.cleanSgBSend(request.getSendBillCleanRequest());
        if (cleanResult.isOK()) {
            //寻源逻辑仓并新增更新逻辑发货单
            SgSendSaveWithPriorityRequest logicSearchRequest = request.getLogicSearchRequest();
            logicSearchRequest.setIsSpecChange(true);
            request.setLogicSearchRequest(logicSearchRequest);
            ValueHolderV14<SgSendSaveWithPriorityResult> logicSearchResult = logicSearchService.searchLogic(request.getLogicSearchRequest());
            SgSendSaveWithPriorityResult resultData = logicSearchResult.getData();
            if (logicSearchResult.isOK() && resultData.getPreoutResult() != SgConstantsIF.PREOUT_RESULT_OUT_STOCK) {
                v14.setCode(logicSearchResult.getCode());
                v14.setMessage(logicSearchResult.getMessage());
                v14.setData(resultData);
            } else {
                log.error("寻源逻辑仓占单服务出参:"+logicSearchResult.toJSONObject().toJSONString());
                AssertUtils.logAndThrow("寻源占单失败!");
            }
        } else {
            v14.setCode(ResultCode.FAIL);
            v14.setMessage(cleanResult.getMessage());
        }

        if (log.isDebugEnabled()) {
            log.debug("来源单据id[" + request.getSendBillCleanRequest().getSourceBillId()
                    + "],清空逻辑发货单后重新寻源占单出参:" + v14.toJSONObject().toJSONString() + ";");
        }
        return v14;
    }
}
