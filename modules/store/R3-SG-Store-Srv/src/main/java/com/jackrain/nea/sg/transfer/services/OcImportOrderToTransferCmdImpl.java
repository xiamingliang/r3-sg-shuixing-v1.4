package com.jackrain.nea.sg.transfer.services;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.basic.config.SgStorageBoxConfig;
import com.jackrain.nea.sg.transfer.api.OcImportOrderToTransferCmd;
import com.jackrain.nea.sys.CommandAdapter;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.query.QuerySession;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author leexh
 * @since 2019/7/2 15:20
 * desc: 导入采购订单未完成量服务
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class OcImportOrderToTransferCmdImpl extends CommandAdapter implements OcImportOrderToTransferCmd {

    @Autowired
    private SgStorageBoxConfig storageBoxConfig;

    @Override
    public ValueHolder execute(QuerySession session) throws NDSException {
        SgImportOrderToTransferServices services = ApplicationContextHandle.getBean(SgImportOrderToTransferServices.class);

        /**
         *  cs 添加 导入订单未完成量 箱功能 相关逻辑
         * @data 2019.10.22 21:25
         * 开启 倒入订单未完成量 箱功能
         */
        if (storageBoxConfig.getBoxEnable()) {
            return services.importOrderToTransferInTeus(session);
        }

        return services.importOrderToTransfer(session);
    }
}
