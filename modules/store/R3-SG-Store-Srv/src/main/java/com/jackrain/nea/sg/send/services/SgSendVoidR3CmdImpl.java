package com.jackrain.nea.sg.send.services;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.basic.model.result.SgR3BaseResult;
import com.jackrain.nea.sg.send.api.SgSendVoidR3Cmd;
import com.jackrain.nea.sg.send.model.result.SgSendBillVoidResult;
import com.jackrain.nea.sys.CommandAdapter;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.query.QuerySession;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

/**
 * @author 舒威
 * @since 2019/4/24
 * create at : 2019/4/24 16:21
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgSendVoidR3CmdImpl extends CommandAdapter implements SgSendVoidR3Cmd {

    @Override
    public ValueHolder execute(QuerySession session) throws NDSException {
//        SgSendBillVoidRequest request = R3ParamUtil.parseSaveObject(session, SgSendBillVoidRequest.class);
//        request.setLoginUser(session.getUser());
        SgSendCmdImpl sgSendCmd = ApplicationContextHandle.getBean(SgSendCmdImpl.class);
        ValueHolderV14<SgSendBillVoidResult> v14Result = sgSendCmd.voidSgBSend(null);
        return v14Result.isOK() ?
                v14Result.getData().isOk(SgR3BaseResult.VOID, "作废成功", null, null)
                : v14Result.getData().isFail(SgR3BaseResult.VOID, v14Result.getMessage(), null);
    }
}
