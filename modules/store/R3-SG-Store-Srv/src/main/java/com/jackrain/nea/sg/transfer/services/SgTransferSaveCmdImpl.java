package com.jackrain.nea.sg.transfer.services;

import com.jackrain.nea.sg.basic.model.result.SgR3BaseResult;
import com.jackrain.nea.sg.transfer.api.SgTransferSaveCmd;
import com.jackrain.nea.sg.transfer.model.request.SgTransferBillSaveRequest;
import com.jackrain.nea.sg.transfer.model.request.SgTransferUpdateSapStatusRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author csy
 * Date: 2019/5/5
 * Description:
 */
@Slf4j
@Component
@Transactional(rollbackFor = Exception.class)
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgTransferSaveCmdImpl implements SgTransferSaveCmd {

    @Autowired
    SgTransferSaveService sgTransferSaveService;

    @Override
    public ValueHolderV14<SgR3BaseResult> save(SgTransferBillSaveRequest request) {
        SgTransferSaveService service = ApplicationContextHandle.getBean(SgTransferSaveService.class);
        return service.save(request, true);
    }

    @Override
    public ValueHolderV14 updateSapStatus(SgTransferUpdateSapStatusRequest request) {
        return sgTransferSaveService.updateSapStatus(request);
    }
}
