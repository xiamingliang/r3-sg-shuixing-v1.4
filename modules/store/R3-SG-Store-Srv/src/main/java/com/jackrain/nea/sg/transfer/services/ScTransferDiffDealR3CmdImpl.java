package com.jackrain.nea.sg.transfer.services;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.Feature;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.basic.common.R3ParamConstants;
import com.jackrain.nea.sg.basic.model.request.SgR3BaseRequest;
import com.jackrain.nea.sg.basic.model.result.SgR3BaseResult;
import com.jackrain.nea.sg.basic.utils.R3ParamUtil;
import com.jackrain.nea.sg.transfer.api.ScTransferDiffDealR3Cmd;
import com.jackrain.nea.sys.CommandAdapter;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.utility.ExceptionUtil;
import com.jackrain.nea.web.query.DefaultWebEvent;
import com.jackrain.nea.web.query.QuerySession;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

/**
 * @author leexh
 * @since 2019/7/22 14:28
 * desc:
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class ScTransferDiffDealR3CmdImpl extends CommandAdapter implements ScTransferDiffDealR3Cmd {
    @Override
    public ValueHolder execute(QuerySession session) throws NDSException {
        DefaultWebEvent event = session.getEvent();
        JSONObject param = JSON.parseObject(JSON.toJSONStringWithDateFormat(event.getParameterValue("param"),
                "yyyy-MM-dd HH:mm:ss", SerializerFeature.WriteMapNullValue), Feature.OrderedField);
        log.debug(this.getClass().getName() + ",execute,差异处理服务入参:" + param);

        SgR3BaseRequest request = new SgR3BaseRequest();
        request.setObjId(param.getLong(R3ParamConstants.OBJID));
        request.setLoginUser(session.getUser());
        ScTransferDiffService service = ApplicationContextHandle.getBean(ScTransferDiffService.class);
        ValueHolderV14<SgR3BaseResult> v14 = new ValueHolderV14<>();
        try {
            v14 = service.dealTransferDiff(request);
        } catch (Exception e) {
            String errMsg = e.getMessage();
            if (StringUtils.isNotEmpty(errMsg)) {
                errMsg = ExceptionUtil.getMessage(e);
                v14.setCode(ResultCode.FAIL);
                v14.setMessage(errMsg);
                return R3ParamUtil.convertV14(v14);
            }
        }
        return R3ParamUtil.convertV14(v14);
    }
}
