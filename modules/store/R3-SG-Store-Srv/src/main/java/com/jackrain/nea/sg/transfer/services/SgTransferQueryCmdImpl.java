package com.jackrain.nea.sg.transfer.services;

import com.github.pagehelper.PageInfo;
import com.jackrain.nea.sg.transfer.api.SgTransferQueryCmd;
import com.jackrain.nea.sg.transfer.model.request.SgTransferCommonQueryRequest;
import com.jackrain.nea.sg.transfer.model.request.SgTransferForPdaRequest;
import com.jackrain.nea.sg.transfer.model.request.SgTransferQueryByStatusRequest;
import com.jackrain.nea.sg.transfer.model.result.ScTransferImpItemAndTeusItemResult;
import com.jackrain.nea.sg.transfer.model.table.ScBTransfer;
import com.jackrain.nea.sg.transfer.model.table.ScBTransferImpItem;
import com.jackrain.nea.sg.transfer.model.table.ScBTransferItem;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author csy
 * Date: 2019/5/15
 * Description: 调拨单 查询服务
 */

@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgTransferQueryCmdImpl implements SgTransferQueryCmd {

    @Autowired
    private SgTransferQueryService sgTransferQueryService;

    @Override
    public ValueHolderV14<PageInfo<ScBTransfer>> queryTransferList(SgTransferQueryByStatusRequest request) {
        SgTransferQueryService service = ApplicationContextHandle.getBean(SgTransferQueryService.class);
        return service.queryTransferList(request);
    }

    @Override
    public ValueHolderV14<List<ScBTransferItem>> queryItemsById(Long objId) {
        SgTransferQueryService service = ApplicationContextHandle.getBean(SgTransferQueryService.class);
        return service.queryItemsById(objId);
    }

    @Override
    public ValueHolderV14<List<ScBTransfer>> queryTransferByCommon(SgTransferCommonQueryRequest request) {
        SgTransferQueryService service = ApplicationContextHandle.getBean(SgTransferQueryService.class);
        return service.queryTransfer(request);
    }

    @Override
    public ValueHolderV14<PageInfo<ScBTransfer>> queryTransferForPda(SgTransferForPdaRequest request) {
        SgTransferQueryService service = ApplicationContextHandle.getBean(SgTransferQueryService.class);
        return service.queryTransferForPda(request);
    }

    @Override
    public ValueHolderV14<PageInfo<ScTransferImpItemAndTeusItemResult>> queryTransferImpItemForPda(
            SgTransferForPdaRequest request) {
        SgTransferQueryService service = ApplicationContextHandle.getBean(SgTransferQueryService.class);
        return service.queryTransferImpItemForPda(request);
    }

    @Override
    public List<ScBTransfer> queryScBTransferByIds(List<Long> objIds) {
        SgTransferQueryService service = ApplicationContextHandle.getBean(SgTransferQueryService.class);
        return service.queryScBTransferByIds(objIds);
    }

    @Override
    public List<ScBTransfer> queryScBTransferByStatus(List<Integer> statusList, String applyBillNo) {
        SgTransferQueryService service = ApplicationContextHandle.getBean(SgTransferQueryService.class);
        return service.queryScBTransferByStatus(statusList,applyBillNo);
    }

    @Override
    public List<ScBTransferItem> queryItems(Long objId) {
        return sgTransferQueryService.queryItems(objId);
    }

    @Override
    public List<ScBTransferImpItem> queryImpItems(Long objId) {
        return sgTransferQueryService.queryImpItems(objId);
    }

    @Override
    public ValueHolderV14<ScBTransfer> queryScBTransferByStockBillNo(String stockBillNo) {
        return sgTransferQueryService.queryScBTransferByStockBillNo(stockBillNo);
    }
}
