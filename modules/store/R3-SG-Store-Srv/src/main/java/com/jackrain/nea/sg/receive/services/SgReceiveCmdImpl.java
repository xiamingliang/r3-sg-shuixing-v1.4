package com.jackrain.nea.sg.receive.services;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.receive.api.SgReceiveCmd;
import com.jackrain.nea.sg.receive.model.request.SgReceiveBillCleanRequest;
import com.jackrain.nea.sg.receive.model.request.SgReceiveBillSaveRequest;
import com.jackrain.nea.sg.receive.model.request.SgReceiveBillSubmitRequest;
import com.jackrain.nea.sg.receive.model.request.SgReceiveBillVoidRequest;
import com.jackrain.nea.sg.receive.model.result.*;
import com.jackrain.nea.sg.send.model.request.SgStoreBillBase;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author: 舒威
 * @since: 2019/4/28
 * create at : 2019/4/28 22:10
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgReceiveCmdImpl implements SgReceiveCmd {

    @Autowired
    private SgReceiveSaveService saveService;

    @Autowired
    private SgReceiveVoidService voidService;

    @Autowired
    private SgReceiveSubmitService submitService;

    @Autowired
    private SgReceiveCleanService cleanService;

    @Autowired
    private SgReceiveQueryService receiveQueryService;

    @Override
    public ValueHolderV14<SgReceiveBillQueryResult> querySgBReceive(List<SgStoreBillBase> searchs) {
        return receiveQueryService.querySgBReceive(searchs);
    }

    @Override
    public ValueHolderV14<SgReceiveBillSaveResult> saveSgBReceive(SgReceiveBillSaveRequest request) {
        return saveService.saveSgBReceive(request);
    }

    @Override
    public ValueHolderV14<SgReceiveBillVoidResult> voidSgBReceive(SgReceiveBillVoidRequest request) {
        return voidService.voidSgBReceive(request);
    }

    @Override
    public ValueHolderV14<SgReceiveBillSubmitResult> submitSgBReceive(SgReceiveBillSubmitRequest request) {
        return submitService.submitSgBReceive(request);
    }

    @Override
    public ValueHolderV14<SgReceiveBillCleanResult> cleanSgBReceive(SgReceiveBillCleanRequest request) throws NDSException {
        return cleanService.cleanSgBReceive(request);
    }
}
