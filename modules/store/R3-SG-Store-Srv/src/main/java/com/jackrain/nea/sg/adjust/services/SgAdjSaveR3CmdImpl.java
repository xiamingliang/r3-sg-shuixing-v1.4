package com.jackrain.nea.sg.adjust.services;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.adjust.api.SgAdjSaveR3Cmd;
import com.jackrain.nea.sg.adjust.model.request.SgAdjustBillSaveRequest;
import com.jackrain.nea.sg.basic.utils.R3ParamUtil;
import com.jackrain.nea.sys.CommandAdapter;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.query.QuerySession;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;


/**
 * @author csy
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgAdjSaveR3CmdImpl extends CommandAdapter implements SgAdjSaveR3Cmd {

    @Override
    public ValueHolder execute(QuerySession session) throws NDSException {
        SgAdjSaveService service = ApplicationContextHandle.getBean(SgAdjSaveService.class);
        return service.saveSgBAdjustR3(R3ParamUtil.parseSaveObject(session, SgAdjustBillSaveRequest.class));
    }
}
