package com.jackrain.nea.sg.receive.services;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.receive.api.SgReceiveCancelCmd;
import com.jackrain.nea.sg.receive.model.request.SgReceiveBillSaveRequest;
import com.jackrain.nea.sg.receive.model.result.SgReceiveBillSaveResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author 舒威
 * @since 2019/6/10
 * create at : 2019/6/10 14:50
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgReceiveCancelCmdImpl implements SgReceiveCancelCmd {

    @Autowired
    private SgReceiveSaveCancelService saveCancelService;

    @Override
    public ValueHolderV14<SgReceiveBillSaveResult> cancelSaveSgBReceive(SgReceiveBillSaveRequest request) throws NDSException {
        return saveCancelService.cancelSaveSgBReceive(request);
    }
}
