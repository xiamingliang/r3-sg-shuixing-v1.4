package com.jackrain.nea.sg.transfer.services;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.basic.common.R3ParamConstants;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.basic.utils.R3ParamUtil;
import com.jackrain.nea.sg.transfer.api.SgTransferCancelAuditR3Cmd;
import com.jackrain.nea.sys.CommandAdapter;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.query.QuerySession;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Locale;

/**
 * @author csy
 * Date: 2019/5/10
 * Description:
 */
@Slf4j
@Component
@Transactional(rollbackFor = Exception.class)
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgTransferCancelAuditR3CmdImpl extends CommandAdapter implements SgTransferCancelAuditR3Cmd {


    @Override
    public ValueHolder execute(QuerySession session) throws NDSException {
        SgTransferCancelAuditService service = ApplicationContextHandle.getBean(SgTransferCancelAuditService.class);
        Locale locale = session.getLocale();
        JSONObject param = R3ParamUtil.getParamJo(session);
        User user = session.getUser();
        AssertUtils.notNull(user, "参数不存在-user", locale);
        AssertUtils.notNull(param, "参数不存在-param", locale);
        if (log.isDebugEnabled()) {
            log.debug("Start SgTransferAuditService.audit. ReceiveParams:param="
                    + JSONObject.toJSONString(param) + ";");
        }
        //页面批量取消审核
        JSONArray ids = param.getJSONArray("ids");
        if (CollectionUtils.isNotEmpty(ids)) {
            ValueHolder batchResult = new ValueHolder();
            int total = ids.size();
            int totalFail = 0;
            JSONArray errData = new JSONArray();
            for (int i = 0; i < ids.size(); i++) {
                Long id = ids.getLongValue(i);
                try {
                    ValueHolder result = service.cancelAuditR3(id, user);
                    if ((Integer) result.getData().get(R3ParamConstants.CODE) == -1) {
                        JSONObject errInfo = new JSONObject();
                        errInfo.put(R3ParamConstants.MESSAGE, result.getData().get(R3ParamConstants.MESSAGE));
                        errInfo.put(R3ParamConstants.CODE, ResultCode.FAIL);
                        errInfo.put(R3ParamConstants.OBJID, id);
                        errData.add(errInfo);
                        totalFail++;
                    }
                } catch (Exception e) {
                    JSONObject errInfo = new JSONObject();
                    errInfo.put(R3ParamConstants.MESSAGE, "取消审核失败:" + e);
                    errInfo.put(R3ParamConstants.CODE, ResultCode.FAIL);
                    errInfo.put(R3ParamConstants.OBJID, id);
                    errData.add(errInfo);
                    totalFail++;
                    log.error("调拨单取消审核失败", e);
                }
            }
            if (totalFail > 0) {
                batchResult.put(R3ParamConstants.CODE, ResultCode.FAIL);
                batchResult.put(R3ParamConstants.MESSAGE, Resources.getMessage("取消审核成功记录数 :" + (total - totalFail) + "条,取消审核失败记录数 :" + totalFail + "条", locale));
                batchResult.put(R3ParamConstants.DATA, errData);
            } else {
                batchResult.put(R3ParamConstants.CODE, ResultCode.SUCCESS);
                batchResult.put(R3ParamConstants.MESSAGE, Resources.getMessage("取消审核成功记录数 :" + total + "条", locale));
            }
            return batchResult;
        } else {
            Long objId = param.getLong(R3ParamConstants.OBJID);
            AssertUtils.notNull(objId, "参数不存在-objId", locale);
            return service.cancelAuditR3(objId, user);
        }
    }
}
