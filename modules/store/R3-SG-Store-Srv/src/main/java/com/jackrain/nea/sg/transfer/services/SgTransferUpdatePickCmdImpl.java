package com.jackrain.nea.sg.transfer.services;

import com.jackrain.nea.sg.transfer.api.SgTransferUpdatePickCmd;
import com.jackrain.nea.sg.transfer.model.request.OcTransferUpdatePickRequest;
import com.jackrain.nea.sg.transfer.model.result.OcTransferUpdatePickResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author leexh
 * @since 2019/11/29 14:10
 * desc:
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgTransferUpdatePickCmdImpl implements SgTransferUpdatePickCmd {

    @Override
    public ValueHolderV14<List<OcTransferUpdatePickResult>> batchUpdateTransferPickStatus(OcTransferUpdatePickRequest request) {
        SgTransferSaveService service = ApplicationContextHandle.getBean(SgTransferSaveService.class);
        return service.batchUpdateTransferPickStatus(request);
    }
}
