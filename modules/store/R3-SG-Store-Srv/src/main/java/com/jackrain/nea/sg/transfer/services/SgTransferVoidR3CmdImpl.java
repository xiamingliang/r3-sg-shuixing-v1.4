package com.jackrain.nea.sg.transfer.services;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.basic.common.R3ParamConstants;
import com.jackrain.nea.sg.basic.utils.R3ParamUtil;
import com.jackrain.nea.sg.transfer.api.SgTransferVoidR3Cmd;
import com.jackrain.nea.sys.CommandAdapter;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.utils.AssertUtils;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.query.QuerySession;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Locale;

/**
 * @author csy
 * Date: 2019/5/10
 * Description:
 */
@Slf4j
@Component
@Transactional(rollbackFor = Exception.class)
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgTransferVoidR3CmdImpl extends CommandAdapter implements SgTransferVoidR3Cmd {

    @Autowired
    private SgTransferVoidService sgTransferVoidService;

    @Override
    public ValueHolder execute(QuerySession session) throws NDSException {
        Locale locale = session.getLocale();
        SgTransferVoidService service = ApplicationContextHandle.getBean(SgTransferVoidService.class);
        JSONObject param = R3ParamUtil.getParamJo(session);
        User user = session.getUser();
        AssertUtils.notNull(user, "参数不存在-user", locale);
        AssertUtils.notNull(param, "参数不存在-param", locale);
        if (log.isDebugEnabled()) {
            log.debug("Start SgTransferAuditService.audit. ReceiveParams:param="
                    + JSONObject.toJSONString(param) + ";");
        }
        Long id = param.getLong(R3ParamConstants.OBJID);
        AssertUtils.notNull(id, "参数为空-objId", locale);
        return R3ParamUtil.convertV14(service.cancelTransfer(id, user));
    }

    @Override
    public ValueHolderV14 cancelTransfer(Long id, User user) {
        return sgTransferVoidService.cancelTransfer(id, user);
    }
}
