package com.jackrain.nea.sg.transfer.services;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.Feature;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.basic.common.R3ParamConstants;
import com.jackrain.nea.sg.basic.model.result.SgR3BaseResult;
import com.jackrain.nea.sg.basic.utils.R3ParamUtil;
import com.jackrain.nea.sg.transfer.api.ScTransferDiffSaveR3Cmd;
import com.jackrain.nea.sg.transfer.model.request.ScBTransferDiffBillRequest;
import com.jackrain.nea.sys.CommandAdapter;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.query.DefaultWebEvent;
import com.jackrain.nea.web.query.QuerySession;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

/**
 * @author leexh
 * @since 2019/7/19 20:26
 * desc:
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class ScTransferDiffSaveR3CmdImpl extends CommandAdapter implements ScTransferDiffSaveR3Cmd {
    @Override
    public ValueHolder execute(QuerySession session) throws NDSException {

        ValueHolder vh = new ValueHolder();
        vh.put("code", ResultCode.SUCCESS);
        vh.put("message", "保存成功！");

        DefaultWebEvent event = session.getEvent();
        JSONObject param = JSON.parseObject(JSON.toJSONStringWithDateFormat(event.getParameterValue("param"),
                "yyyy-MM-dd HH:mm:ss", SerializerFeature.WriteMapNullValue), Feature.OrderedField);
        log.debug(this.getClass().getName() + ",execute,调拨差异处理保存服务入参:" + param);

        ScBTransferDiffBillRequest request = R3ParamUtil.parseSaveObject(session, ScBTransferDiffBillRequest.class);
        ScTransferDiffService service = ApplicationContextHandle.getBean(ScTransferDiffService.class);
        ValueHolderV14<SgR3BaseResult> v14 = service.saveTransferDiff(request);
        if (v14 != null) {
            vh.put(R3ParamConstants.CODE, v14.getCode());
            vh.put(R3ParamConstants.MESSAGE, v14.getMessage());
            if (v14.getData() != null) {
                vh.put(R3ParamConstants.DATA, v14.getData().getDataJo());
            }
        }

        return vh;
    }
}
