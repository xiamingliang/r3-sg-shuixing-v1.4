package com.jackrain.nea.sg.transfer.services;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.basic.utils.R3ParamUtil;
import com.jackrain.nea.sg.transfer.api.SgTransferSaveR3Cmd;
import com.jackrain.nea.sg.transfer.model.request.SgTransferBillSaveRequest;
import com.jackrain.nea.sys.CommandAdapter;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.query.QuerySession;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author csy
 * Date: 2019/5/5
 * Description:
 */

@Slf4j
@Component
@Transactional(rollbackFor = Exception.class)
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgTransferSaveR3CmdImpl extends CommandAdapter implements SgTransferSaveR3Cmd {

    @Override
    public ValueHolder execute(QuerySession session) throws NDSException {
        SgTransferSaveService service = ApplicationContextHandle.getBean(SgTransferSaveService.class);
        return service.save(R3ParamUtil.parseSaveObject(session, SgTransferBillSaveRequest.class));
    }
}
