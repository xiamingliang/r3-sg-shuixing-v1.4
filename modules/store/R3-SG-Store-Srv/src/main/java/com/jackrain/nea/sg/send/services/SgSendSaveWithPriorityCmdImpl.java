package com.jackrain.nea.sg.send.services;

import com.jackrain.nea.sg.send.api.SgSendSaveWithPriorityCmd;
import com.jackrain.nea.sg.send.model.request.SgSendSaveWithPriorityRequest;
import com.jackrain.nea.sg.send.model.result.SgSendSaveWithPriorityResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author 舒威
 * @since 2019/4/25
 * create at : 2019/4/25 13:36
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgSendSaveWithPriorityCmdImpl implements SgSendSaveWithPriorityCmd {

    @Autowired
    private SgSendSaveWithPriorityService logicSearchService;

    @Override
    public ValueHolderV14<SgSendSaveWithPriorityResult> saveSendWithPriority(SgSendSaveWithPriorityRequest request) {
        return logicSearchService.searchLogic(request);
    }
}
