package com.jackrain.nea.sg.receive.services;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.basic.model.result.SgR3BaseResult;
import com.jackrain.nea.sg.basic.utils.R3ParamUtil;
import com.jackrain.nea.sg.receive.api.SgReceiveVoidR3Cmd;
import com.jackrain.nea.sg.receive.model.request.SgReceiveBillVoidRequest;
import com.jackrain.nea.sg.receive.model.result.SgReceiveBillVoidResult;
import com.jackrain.nea.sys.CommandAdapter;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.query.QuerySession;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

/**
 * @author: 舒威
 * @since: 2019/4/28
 * create at : 2019/4/28 23:12
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgReceiveVoidR3CmdImpl extends CommandAdapter implements SgReceiveVoidR3Cmd {

    @Override
    public ValueHolder execute(QuerySession session) throws NDSException {
        SgReceiveBillVoidRequest request = R3ParamUtil.parseSaveObject(session, SgReceiveBillVoidRequest.class);
        request.setLoginUser(session.getUser());
        SgReceiveCmdImpl receiveCmd = ApplicationContextHandle.getBean(SgReceiveCmdImpl.class);
        ValueHolderV14<SgReceiveBillVoidResult> v14Result = receiveCmd.voidSgBReceive(request);
        return v14Result.isOK() ?
                v14Result.getData().isOk(SgR3BaseResult.VOID, "作废成功", null, null)
                : v14Result.getData().isFail(SgR3BaseResult.VOID, v14Result.getMessage(), null);
    }
}
