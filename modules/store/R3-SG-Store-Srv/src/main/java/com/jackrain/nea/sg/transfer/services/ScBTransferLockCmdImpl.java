package com.jackrain.nea.sg.transfer.services;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.transfer.api.ScBTransferLockCmd;
import com.jackrain.nea.sys.CommandAdapter;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.query.QuerySession;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author: 汪聿森
 * @Date: Created in 2019-08-23 17:10
 * @Description : 锁单
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class ScBTransferLockCmdImpl extends CommandAdapter implements ScBTransferLockCmd {
    @Autowired
    private ScBTransferLockService scBTransferLockService;

    @Override
    public ValueHolder execute(QuerySession session) throws NDSException {
        return scBTransferLockService.execute(session);
    }
}
