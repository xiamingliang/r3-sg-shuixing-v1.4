package com.jackrain.nea.sg.send.services;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.send.api.SgSendCancelCmd;
import com.jackrain.nea.sg.send.model.request.SgSendBillSaveRequest;
import com.jackrain.nea.sg.send.model.result.SgSendBillSaveResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author 舒威
 * @since 2019/6/10
 * create at : 2019/6/10 20:40
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgSendCancelCmdImpl implements SgSendCancelCmd {

    @Autowired
    private SgSendSaveCancelService cancelService;

    @Override
    public ValueHolderV14<SgSendBillSaveResult> cancelSaveSgBSend(SgSendBillSaveRequest request) throws NDSException {
        return cancelService.cancelSaveSgBSend(request);
    }
}
