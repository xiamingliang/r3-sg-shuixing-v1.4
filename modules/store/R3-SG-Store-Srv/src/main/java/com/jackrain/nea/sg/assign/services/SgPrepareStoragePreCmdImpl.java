package com.jackrain.nea.sg.assign.services;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.assign.api.SgPrepareStoragePreCmd;
import com.jackrain.nea.sg.assign.model.request.SgPrepareStoragePreRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.utility.ExceptionUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

/**
 * @author :csy
 * @version :1.0
 * description ：
 * @date :2019/11/12 16:46
 */

@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgPrepareStoragePreCmdImpl implements SgPrepareStoragePreCmd {

    @Override
    public ValueHolderV14 prepareStorage(SgPrepareStoragePreRequest request) {
        SgPrepareStoragePreService service = ApplicationContextHandle.getBean(SgPrepareStoragePreService.class);
        try {
            return service.prepareStorage(request);
        } catch (Exception e) {
            log.error(String.format("配货单 配货失败 request : %s", JSONObject.toJSONString(request)), e);
            String errMsg = e.getMessage();
            if (StringUtils.isEmpty(errMsg)) {
                errMsg = ExceptionUtil.getMessage(e);
            }
            return new ValueHolderV14(ResultCode.FAIL, errMsg);
        }
    }
}
