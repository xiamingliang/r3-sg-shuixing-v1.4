package com.jackrain.nea.sg.adjust.services;

import com.jackrain.nea.sg.adjust.api.SgAdjSaveAndAuditCmd;
import com.jackrain.nea.sg.adjust.model.request.SgAdjustBillSaveRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;


/**
 * @author csy
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgAdjSaveAndAuditCmdImpl implements SgAdjSaveAndAuditCmd {

    @Override
    public ValueHolderV14 saveAndAuditAdj(SgAdjustBillSaveRequest request) {
        SgAdjSaveAndAuditService service = ApplicationContextHandle.getBean(SgAdjSaveAndAuditService.class);
        return service.saveAndAudit(request);
    }
}
