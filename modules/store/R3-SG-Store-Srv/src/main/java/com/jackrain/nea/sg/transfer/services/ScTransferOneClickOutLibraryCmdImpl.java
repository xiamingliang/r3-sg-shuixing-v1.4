package com.jackrain.nea.sg.transfer.services;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.transfer.api.ScTransferOneClickOutLibraryCmd;
import com.jackrain.nea.sys.CommandAdapter;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.query.QuerySession;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author: 汪聿森
 * @Date: Created in 2019-09-12 14:04
 * @Description : 调拨单一键出库
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class ScTransferOneClickOutLibraryCmdImpl extends CommandAdapter implements ScTransferOneClickOutLibraryCmd {
    @Autowired
    private ScTransferOneClickOutLibraryService scTransferOneClickOutLibraryService;

    @Override
    public ValueHolder execute(QuerySession session) throws NDSException {
        return scTransferOneClickOutLibraryService.transferOneClickOutLibrary(session);
    }
}
