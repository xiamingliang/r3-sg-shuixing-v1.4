package com.jackrain.nea.sg.transfer.services;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.transfer.api.ScTransferQueryMatrixCmd;
import com.jackrain.nea.sys.CommandAdapter;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.query.QuerySession;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;


@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class ScTransferQueryMatrixCmdImpl extends CommandAdapter implements ScTransferQueryMatrixCmd {


    @Override
    public ValueHolder execute(QuerySession session) throws NDSException {
        ScTransferMatrixServices services = ApplicationContextHandle.getBean(ScTransferMatrixServices.class);
        return services.queryMatrix(session);
    }
}
