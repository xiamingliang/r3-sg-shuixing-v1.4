package com.jackrain.nea.sg.transfer.services;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.transfer.api.SgTransferAuditCmd;
import com.jackrain.nea.sg.transfer.model.request.SgTransferBillAuditRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author csy
 * Date: 2019/5/5
 * Description:
 */
@Slf4j
@Component
@Transactional(rollbackFor = Exception.class)
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgTransferAuditCmdImpl implements SgTransferAuditCmd {


    @Override
    public ValueHolder audit(Long objId, User user) throws NDSException {
        SgTransferAuditService service = ApplicationContextHandle.getBean(SgTransferAuditService.class);
        return service.audit(objId, user);
    }

    @Override
    public ValueHolderV14 audit(SgTransferBillAuditRequest request) {
        SgTransferAuditService service = ApplicationContextHandle.getBean(SgTransferAuditService.class);
        return service.audit(request);
    }
}
