package com.jackrain.nea.sg.transfer.services;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.transfer.api.SgTransferAutoOutCmd;
import com.jackrain.nea.sg.transfer.model.request.SgTransferBillSaveRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

/**
 * @author csy
 * Date: 2019/5/20
 * Description:
 */

@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgTransferAutoOutCmdImpl implements SgTransferAutoOutCmd {


    @Override
    public ValueHolderV14 autoOut(SgTransferBillSaveRequest request) throws NDSException {
        SgTransferAutoOutService service = ApplicationContextHandle.getBean(SgTransferAutoOutService.class);
        return service.autoOut(request);
    }
}
