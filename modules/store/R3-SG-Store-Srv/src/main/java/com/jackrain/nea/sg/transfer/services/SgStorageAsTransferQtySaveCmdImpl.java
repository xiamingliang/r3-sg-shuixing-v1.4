package com.jackrain.nea.sg.transfer.services;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.transfer.api.SgStorageAsTransferQtySaveCmd;
import com.jackrain.nea.sys.CommandAdapter;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.query.QuerySession;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Component
@Transactional(rollbackFor = Exception.class)
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgStorageAsTransferQtySaveCmdImpl extends CommandAdapter implements SgStorageAsTransferQtySaveCmd {

    @Autowired
    private SgStorageAsTransferQtySaveService sgStorageAsTransferQtySaveService;

    @Override
    public ValueHolder execute(QuerySession session) throws NDSException {
        return sgStorageAsTransferQtySaveService.changTransferQty(session);
    }
}
