package com.jackrain.nea.sg.transfer.services;

import com.jackrain.nea.sg.transfer.api.SgRequisitionReturnOfGoodsReceiptForReceiptCmd;
import com.jackrain.nea.sg.transfer.model.request.SgRequisitionRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * @author ZhangQK
 * 销退收货签收--调拨单
 * @date 2020/4/8 18:30
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgRequisitionReturnOfGoodsReceiptForReceiptCmdImpl implements SgRequisitionReturnOfGoodsReceiptForReceiptCmd {
    @Autowired
    private SgRequisitionReturnOfGoodsReceiptForReceiptCmdImplService scSaleReceivingSignService;
    @Override
    public ValueHolderV14<Map<String,Integer>>  sgSaleReceiving(List<SgRequisitionRequest> ocSaleReundQueryRequestList){
        return scSaleReceivingSignService.sgSaleReceiving(ocSaleReundQueryRequestList);
    }
}
