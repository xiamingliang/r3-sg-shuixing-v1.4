package com.jackrain.nea.sg.transfer.services;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.transfer.api.SgTransferSaveAndAuditCmd;
import com.jackrain.nea.sg.transfer.model.request.SgTransferBillSaveRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author csy
 * Date: 2019/5/7
 * Description:
 */
@Slf4j
@Component
@Transactional(rollbackFor = Exception.class)
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgTransferSaveAndAuditCmdImpl implements SgTransferSaveAndAuditCmd {

    @Override
    public ValueHolderV14 saveAndAudit(SgTransferBillSaveRequest request) throws NDSException {
        SgTransferSaveAndAuditService service = ApplicationContextHandle.getBean(SgTransferSaveAndAuditService.class);
        return service.saveAndAudit(request);
    }

    @Override
    public ValueHolderV14 saveAndAuditBatch(List<SgTransferBillSaveRequest> requests) throws NDSException {
        SgTransferSaveAndAuditService service = ApplicationContextHandle.getBean(SgTransferSaveAndAuditService.class);
        return service.saveAndAuditBatch(requests);
    }
}
