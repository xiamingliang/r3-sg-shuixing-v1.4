package com.jackrain.nea.sg.transfer.services;

import com.jackrain.nea.sg.transfer.api.SgTransferDiffQueryCmd;
import com.jackrain.nea.sg.transfer.model.request.SgTransferDiffQueryRequest;
import com.jackrain.nea.sg.transfer.model.table.ScBTransferDiff;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author leexh
 * @since 2019/7/29 17:33
 * desc: 调拨差异单查询服务
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgTransferDiffQueryCmdImpl implements SgTransferDiffQueryCmd {
    @Override
    public ValueHolderV14<List<ScBTransferDiff>> queryTransferDiff(SgTransferDiffQueryRequest request) {
        ScTransferDiffQueryService service = ApplicationContextHandle.getBean(ScTransferDiffQueryService.class);
        return service.queryTransferDiff(request);
    }
}
