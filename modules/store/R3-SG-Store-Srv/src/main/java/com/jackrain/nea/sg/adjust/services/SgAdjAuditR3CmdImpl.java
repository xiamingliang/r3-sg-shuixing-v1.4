package com.jackrain.nea.sg.adjust.services;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.adjust.api.SgAdjAuditR3Cmd;
import com.jackrain.nea.sg.basic.common.R3ParamConstants;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.basic.utils.R3ParamUtil;
import com.jackrain.nea.sys.CommandAdapter;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.query.QuerySession;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

import java.util.Locale;

/**
 * @author csy
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgAdjAuditR3CmdImpl extends CommandAdapter implements SgAdjAuditR3Cmd {

    @Override
    public ValueHolder execute(QuerySession session) throws NDSException {
        SgAdjAuditService service = ApplicationContextHandle.getBean(SgAdjAuditService.class);
        Locale locale = session.getLocale();
        JSONObject param = R3ParamUtil.getParamJo(session);
        User user = session.getUser();
        AssertUtils.notNull(user, "参数不存在-user", locale);
        AssertUtils.notNull(param, "参数不存在-param", locale);
        if (log.isDebugEnabled()) {
            log.debug("Start SgAdjAuditService.audit. ReceiveParams:param="
                    + JSONObject.toJSONString(param) + ";");
        }
        //页面批量审核
        JSONArray ids = param.getJSONArray("ids");
        if (CollectionUtils.isNotEmpty(ids)) {
            ValueHolder batchResult = new ValueHolder();
            int total = ids.size();
            int totalFail = 0;
            JSONArray errData = new JSONArray();
            for (int i = 0; i < ids.size(); i++) {
                Long id = ids.getLongValue(i);
                try {
                    ValueHolder result = service.audit(id, user);
                    if ((Integer) result.getData().get(R3ParamConstants.CODE) == -1) {
                        JSONObject errInfo = new JSONObject();
                        errInfo.put(R3ParamConstants.MESSAGE, result.getData().get(R3ParamConstants.MESSAGE));
                        errInfo.put(R3ParamConstants.CODE, ResultCode.FAIL);
                        errInfo.put(R3ParamConstants.OBJID, id);
                        errData.add(errInfo);
                        totalFail++;
                    }
                } catch (Exception e) {
                    JSONObject errInfo = new JSONObject();
                    errInfo.put(R3ParamConstants.MESSAGE, "审核失败:" + e);
                    errInfo.put(R3ParamConstants.CODE, ResultCode.FAIL);
                    errInfo.put(R3ParamConstants.OBJID, id);
                    errData.add(errInfo);
                    totalFail++;
                    log.error("逻辑库存调整单审核失败", e);
                }
            }
            if (totalFail > 0) {
                batchResult.put(R3ParamConstants.CODE, ResultCode.FAIL);
                batchResult.put(R3ParamConstants.MESSAGE, Resources.getMessage("审核成功记录数 :" + (total - totalFail) + "条,审核失败记录数 :" + totalFail + "条", locale));
                batchResult.put(R3ParamConstants.DATA, errData);
            } else {
                batchResult.put(R3ParamConstants.CODE, ResultCode.SUCCESS);
                batchResult.put(R3ParamConstants.MESSAGE, Resources.getMessage("审核成功记录数 :" + total + "条", locale));
            }
            return batchResult;
        } else {
            Long objId = param.getLong(R3ParamConstants.OBJID);
            AssertUtils.notNull(objId, "参数不存在-objId", locale);
            return service.audit(objId, user);
        }
    }
}
