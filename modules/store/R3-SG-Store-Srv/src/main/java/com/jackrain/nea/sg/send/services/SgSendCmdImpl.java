package com.jackrain.nea.sg.send.services;

import com.jackrain.nea.sg.send.api.SgSendCmd;
import com.jackrain.nea.sg.send.model.request.*;
import com.jackrain.nea.sg.send.model.result.*;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author 舒威
 * @since 2019/4/19
 * create at : 2019/4/19 14:27
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgSendCmdImpl implements SgSendCmd {

    @Override
    public ValueHolderV14<SgSendBillQueryResult> querySgBSend(List<SgStoreBillBase> searchs) {
        SgSendQueryService queryService = ApplicationContextHandle.getBean(SgSendQueryService.class);
        return queryService.querySgBSend(searchs);
    }

    @Override
    public ValueHolderV14<SgSendActualLackQueryResult> querySgBSend(SgSendBillQueryRequest searchs) {
        SgSendActualLackQueryService queryService = ApplicationContextHandle.getBean(SgSendActualLackQueryService.class);
        return queryService.querySgBSend(searchs);
    }

    @Override
    public ValueHolderV14<SgSendBillSaveResult> saveSgBSend(SgSendBillSaveRequest request) {
        SgSendSaveService saveService = ApplicationContextHandle.getBean(SgSendSaveService.class);
        return saveService.saveSgBSend(request);
    }

    @Override
    public ValueHolderV14<SgSendBillVoidResult> voidSgBSend(SgSendBillVoidRequest request) {
        SgSendVoidService voidService = ApplicationContextHandle.getBean(SgSendVoidService.class);
        return voidService.voidSgBSend(request);
    }

    @Override
    public ValueHolderV14<SgSendBillSubmitResult> submitSgBSend(SgSendBillSubmitRequest request) {
        SgSendSubmitService submitService = ApplicationContextHandle.getBean(SgSendSubmitService.class);
        return submitService.submitSgBSend(request);
    }
}
