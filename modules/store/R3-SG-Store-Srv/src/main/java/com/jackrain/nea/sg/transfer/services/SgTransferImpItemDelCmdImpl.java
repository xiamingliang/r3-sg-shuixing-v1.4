package com.jackrain.nea.sg.transfer.services;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.basic.model.result.SgR3BaseResult;
import com.jackrain.nea.sg.transfer.api.SgTransferImpItemDelCmd;
import com.jackrain.nea.sg.transfer.model.request.ScTransferItemDelRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author leexh
 * @since 2019/10/21 18:27
 * desc:
 */
@Slf4j
@Component
@Transactional(rollbackFor = Exception.class)
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgTransferImpItemDelCmdImpl implements SgTransferImpItemDelCmd {
    @Override
    public ValueHolderV14<SgR3BaseResult> delTransferItem(ScTransferItemDelRequest request) throws NDSException {
        SgTransferImpDelService delService = ApplicationContextHandle.getBean(SgTransferImpDelService.class);
        return delService.delTransferImpItem(request);
    }
}
