package com.jackrain.nea.sg.adjust.services;

import com.jackrain.nea.sg.adjust.api.SgAdjSaveCmd;
import com.jackrain.nea.sg.adjust.model.request.SgAdjustBillSaveRequest;
import com.jackrain.nea.sg.basic.model.result.SgR3BaseResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;


/**
 * @author csy
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgAdjSaveCmdImpl implements SgAdjSaveCmd {

    @Override
    public ValueHolderV14<SgR3BaseResult> saveAdj(SgAdjustBillSaveRequest request) {
        SgAdjSaveService service = ApplicationContextHandle.getBean(SgAdjSaveService.class);
        return service.saveSgBAdjust(request, false);
    }

}
