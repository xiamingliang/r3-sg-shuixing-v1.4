package com.jackrain.nea.sg.transfer.services;

import com.jackrain.nea.sg.transfer.api.SgTransferInResultCmd;
import com.jackrain.nea.sg.transfer.model.request.SgTransferInWBRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;


/**
 * @author leexh
 * @since 2020/4/20 18:58
 * desc: 退货调拨入库（erp使用）
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgTransferInResultCmdImpl implements SgTransferInResultCmd {

    @Override
    public ValueHolderV14 writeBackInResultForErp(SgTransferInWBRequest request) {
        SgTransferInResultService service = ApplicationContextHandle.getBean(SgTransferInResultService.class);
        return service.writeBackInResultForErp(request);
    }
}
