package com.jackrain.nea.sg.send.services;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.send.api.SgSendVoidSaveCmd;
import com.jackrain.nea.sg.send.model.request.SgSendBillVoidRequest;
import com.jackrain.nea.sg.send.model.request.SgSendVoidSaveRequest;
import com.jackrain.nea.sg.send.model.result.SgSendBatchVoidResult;
import com.jackrain.nea.sg.send.model.result.SgSendSaveWithPriorityResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author 舒威
 * @since 2019/6/27
 * create at : 2019/6/27 15:18
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgSendVoidSaveCmdImpl implements SgSendVoidSaveCmd {

    @Override
    public ValueHolderV14<List<SgSendSaveWithPriorityResult>> voidSaveSgBSend(SgSendVoidSaveRequest request) throws NDSException {
        SgSendVoidSaveService voidSaveService = ApplicationContextHandle.getBean(SgSendVoidSaveService.class);
        return voidSaveService.voidSaveSgBSend(request);
    }

    @Override
    public ValueHolderV14<SgSendBatchVoidResult> batchVoidSgBSend(List<SgSendBillVoidRequest> requests, User user) throws NDSException {
        SgSendVoidSaveService voidSaveService = ApplicationContextHandle.getBean(SgSendVoidSaveService.class);
        return voidSaveService.batchVoidSgBSend(requests,user);
    }
}
