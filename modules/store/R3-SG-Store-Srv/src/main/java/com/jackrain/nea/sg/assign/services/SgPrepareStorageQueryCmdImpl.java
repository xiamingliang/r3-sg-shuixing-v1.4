package com.jackrain.nea.sg.assign.services;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.assign.api.SgPrepareStorageQueryCmd;
import com.jackrain.nea.sys.CommandAdapter;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.query.QuerySession;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

/**
 * @author :csy
 * @version :1.0
 * description ：
 * @date :2019/11/7 13:20
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgPrepareStorageQueryCmdImpl extends CommandAdapter implements SgPrepareStorageQueryCmd {

    @Override
    public ValueHolder execute(QuerySession session) throws NDSException {
        SgPrepareStorageQueryService service = ApplicationContextHandle.getBean(SgPrepareStorageQueryService.class);
        return service.execute(session);
    }
}
