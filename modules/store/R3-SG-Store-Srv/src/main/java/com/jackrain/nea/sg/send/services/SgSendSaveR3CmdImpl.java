package com.jackrain.nea.sg.send.services;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.model.result.SgR3BaseResult;
import com.jackrain.nea.sg.basic.utils.R3ParamUtil;
import com.jackrain.nea.sg.send.api.SgSendSaveR3Cmd;
import com.jackrain.nea.sg.send.model.request.SgSendBillSaveRequest;
import com.jackrain.nea.sg.send.model.result.SgSendBillSaveResult;
import com.jackrain.nea.sys.CommandAdapter;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.query.QuerySession;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;


/**
 * @author 舒威
 * @since 2019/4/22
 * create at : 2019/4/22 13:31
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgSendSaveR3CmdImpl extends CommandAdapter implements SgSendSaveR3Cmd {

    @Override
    public ValueHolder execute(QuerySession session) throws NDSException {
        SgSendBillSaveRequest request = R3ParamUtil.parseSaveObject(session, SgSendBillSaveRequest.class);
        request.setPreoutWarningType(SgConstantsIF.PREOUT_RESULT_OUT_STOCK);
        request.setLoginUser(session.getUser());
        request = R3ParamUtil.parseSaveObject(session, SgSendBillSaveRequest.class);
        request.setPreoutWarningType(SgConstantsIF.PREOUT_RESULT_OUT_STOCK);

        SgSendCmdImpl sgSendCmd = ApplicationContextHandle.getBean(SgSendCmdImpl.class);
        ValueHolderV14<SgSendBillSaveResult> v14Result = sgSendCmd.saveSgBSend(request);
        if (v14Result.isOK()) {
            return v14Result.getData().isOk(SgR3BaseResult.SAVE, "保存成功",
                    null, v14Result.getData().getDataJo());
        } else {
            return v14Result.getData().isFail(SgR3BaseResult.SAVE, v14Result.getMessage(), null);
        }
    }
}
