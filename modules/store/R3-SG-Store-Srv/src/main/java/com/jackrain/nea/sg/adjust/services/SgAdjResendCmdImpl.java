package com.jackrain.nea.sg.adjust.services;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.sg.adjust.api.SgAdjResendCmd;
import com.jackrain.nea.sg.task.SgAdjustSendLogService;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author: 舒威
 * @since: 2019/8/20
 * create at : 2019/8/20 9:56
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgAdjResendCmdImpl implements SgAdjResendCmd{

    @Autowired
    private SgAdjustSendLogService adjustSendLogService;

    @Override
    public ValueHolderV14 resendAdjLog(JSONObject param) {
        return adjustSendLogService.sendLogForAdjust(param);
    }
}
