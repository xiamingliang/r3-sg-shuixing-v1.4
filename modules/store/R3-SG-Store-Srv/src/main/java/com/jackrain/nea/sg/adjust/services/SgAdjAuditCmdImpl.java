package com.jackrain.nea.sg.adjust.services;

import com.jackrain.nea.sg.adjust.api.SgAdjAuditCmd;
import com.jackrain.nea.sg.adjust.model.request.SgAdjustBillAuditRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;


/**
 * @author csy
 */
@Slf4j
@Component
@Service(protocol = "dubbo", validation = "true", version = "1.0", group = "sg")
public class SgAdjAuditCmdImpl implements SgAdjAuditCmd {

    @Override
    public ValueHolderV14 audit(SgAdjustBillAuditRequest request) {
        SgAdjAuditService service = ApplicationContextHandle.getBean(SgAdjAuditService.class);
        return service.audit(request);
    }
}
