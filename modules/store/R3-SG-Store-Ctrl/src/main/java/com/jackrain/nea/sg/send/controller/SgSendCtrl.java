package com.jackrain.nea.sg.send.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.send.api.SgSendCleanSaveCmd;
import com.jackrain.nea.sg.send.api.SgSendCmd;
import com.jackrain.nea.sg.send.api.SgSendSaveWithPriorityCmd;
import com.jackrain.nea.sg.send.api.SgSendVoidSaveCmd;
import com.jackrain.nea.sg.send.model.request.*;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.face.impl.UserImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author 舒威
 * @since 2019/4/22
 * create at : 2019/4/22 14:12
 */
@RestController
@Slf4j
@Api(value = "SgSendCtrl", description = "逻辑发货单接口")
public class SgSendCtrl {

    @ApiOperation(value = "新增/更新")
    @PostMapping(path = "/sg/send/save")
    public JSONObject saveSend(HttpServletRequest request, @RequestParam(value = "param", required = true) String param) {
        //TODO 測試用ctr
        if (log.isDebugEnabled()) {
            log.debug("Strat SgSendCtrl.saveSend. ReceiveParams=" + param + ";");
        }
        User user = getRootUser();
        ValueHolderV14 result;
        try {
            SgSendCmd sendCmd = (SgSendCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                    SgSendCmd.class.getName(), "sg", "1.0");
            SgSendBillSaveRequest model = JSONObject.parseObject(param, SgSendBillSaveRequest.class);
            model.setLoginUser(user);
            result = sendCmd.saveSgBSend(model);
        } catch (Exception e) {
            throw new NDSException(Resources.getMessage(e.getMessage(), user.getLocale()));
        }
        if (log.isDebugEnabled()) {
            log.debug("Finish SgSendCtrl.saveSend. ReturnResult=" + result);
        }
        return result.toJSONObject();
    }

    @ApiOperation(value = "逻辑发货单出库")
    @PostMapping(path = "/sg/send/submit")
    public JSONObject submitSend(HttpServletRequest request, @RequestParam(value = "param", required = true) String param) {
        //TODO 測試用ctr
        if (log.isDebugEnabled()) {
            log.debug("Strat SgSendCtrl.submitSend. ReceiveParams=" + param + ";");
        }
        User user = getRootUser();
        ValueHolderV14 result;
        try {
            SgSendCmd sendCmd = (SgSendCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                    SgSendCmd.class.getName(), "sg", "1.0");
            SgSendBillSubmitRequest model = JSONObject.parseObject(param, SgSendBillSubmitRequest.class);
            model.setLoginUser(user);
            result = sendCmd.submitSgBSend(model);
        } catch (Exception e) {
            throw new NDSException(Resources.getMessage(e.getMessage(), user.getLocale()));
        }
        if (log.isDebugEnabled()) {
            log.debug("Finish SgSendCtrl.submitSend. ReturnResult=" + result);
        }
        return result.toJSONObject();
    }

    @ApiOperation(value = "取消逻辑发货单")
    @PostMapping(path = "/sg/send/void")
    public JSONObject voidSend(HttpServletRequest request, @RequestParam(value = "param", required = true) String param) {
        //TODO 測試用ctr
        if (log.isDebugEnabled()) {
            log.debug("Strat SgSendCtrl.voidSend. ReceiveParams=" + param + ";");
        }
        User user = getRootUser();
        ValueHolderV14 result;
        try {
            SgSendCmd sendCmd = (SgSendCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                    SgSendCmd.class.getName(), "sg", "1.0");
            SgSendBillVoidRequest model = JSONObject.parseObject(param, SgSendBillVoidRequest.class);
            model.setLoginUser(user);
            result = sendCmd.voidSgBSend(model);
        } catch (Exception e) {
            throw new NDSException(Resources.getMessage(e.getMessage(), user.getLocale()));
        }
        if (log.isDebugEnabled()) {
            log.debug("Finish SgSendCtrl.voidSend. ReturnResult=" + result);
        }
        return result.toJSONObject();
    }

    @ApiOperation(value = "寻源逻辑仓")
    @PostMapping(path = "/sg/logic/source")
    public JSONObject sourceLogic(HttpServletRequest request, @RequestParam(value = "param", required = true) String param
            , @RequestParam(value = "source_bill_type", required = false) Integer source_bill_type) {
        //TODO 測試用ctr
        if (log.isDebugEnabled()) {
            log.debug("Strat SgSendCtrl.sourceLogic. ReceiveParams=" + param + ";");
        }
        User user = getRootUser();
        ValueHolderV14 result;
        try {
            SgSendSaveWithPriorityCmd saveWithPriorityCmd = (SgSendSaveWithPriorityCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                    SgSendSaveWithPriorityCmd.class.getName(), "sg", "1.0");
            SgSendSaveWithPriorityRequest model = JSONObject.parseObject(param, SgSendSaveWithPriorityRequest.class);
            if (source_bill_type != null) {
                model.getSgSend().setSourceBillType(source_bill_type);
                String sourceBillNo = model.getSgSend().getSourceBillNo();
                Long sourceBillId = model.getSgSend().getSourceBillId();
                model.getSgSend().setSourceBillNo(sourceBillNo + "--" + source_bill_type);
                model.getSgSend().setSourceBillId(sourceBillId + source_bill_type);
            }
            model.setLoginUser(user);
            result = saveWithPriorityCmd.saveSendWithPriority(model);
        } catch (Exception e) {
            throw new NDSException(Resources.getMessage(e.getMessage(), user.getLocale()));
        }
        if (log.isDebugEnabled()) {
            log.debug("Finish SgSendCtrl.sourceLogic. ReturnResult=" + result);
        }
        return result.toJSONObject();
    }

    @ApiOperation(value = "清空逻辑发货单")
    @PostMapping(path = "/sg/send/clean")
    public JSONObject clean(HttpServletRequest request, @RequestParam(value = "param", required = true) String param) {
        //TODO 測試用ctr
        if (log.isDebugEnabled()) {
            log.debug("Strat SgSendCtrl.clean. ReceiveParams=" + param + ";");
        }
        User user = getRootUser();
        ValueHolderV14 result;
        try {
            SgSendCleanSaveCmd cleanSaveCmd = (SgSendCleanSaveCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                    SgSendCleanSaveCmd.class.getName(), "sg", "1.0");
            SgSendBillCleanRequest model = JSONObject.parseObject(param, SgSendBillCleanRequest.class);
            model.setLoginUser(user);
            result = cleanSaveCmd.cleanSgBSend(model);
        } catch (Exception e) {
            throw new NDSException(Resources.getMessage(e.getMessage(), user.getLocale()));
        }
        if (log.isDebugEnabled()) {
            log.debug("Finish SgSendCtrl.clean. ReturnResult=" + result);
        }
        return result.toJSONObject();
    }

    @ApiOperation(value = "清空逻辑发货单后重新寻源占单")
    @PostMapping(path = "/sg/send/cleansave")
    public JSONObject cleanSave(HttpServletRequest request, @RequestParam(value = "param", required = true) String param) {
        //TODO 測試用ctr
        if (log.isDebugEnabled()) {
            log.debug("Strat SgSendCtrl.cleanSave. ReceiveParams=" + param + ";");
        }
        User user = getRootUser();
        ValueHolderV14 result;
        try {
            SgSendCleanSaveCmd cleanSaveCmd = (SgSendCleanSaveCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                    SgSendCleanSaveCmd.class.getName(), "sg", "1.0");
            SgSendCleanSaveRequest model = JSONObject.parseObject(param, SgSendCleanSaveRequest.class);
            SgSendBillCleanRequest sendBillCleanRequest = model.getSendBillCleanRequest();
            sendBillCleanRequest.setLoginUser(user);
            model.setSendBillCleanRequest(sendBillCleanRequest);
            SgSendSaveWithPriorityRequest logicSearchRequest = model.getLogicSearchRequest();
            logicSearchRequest.setLoginUser(user);
            model.setLogicSearchRequest(logicSearchRequest);
            result = cleanSaveCmd.cleanSaveSgBSend(model);
        } catch (Exception e) {
            throw new NDSException(Resources.getMessage(e.getMessage(), user.getLocale()));
        }
        if (log.isDebugEnabled()) {
            log.debug("Finish SgSendCtrl.cleanSave. ReturnResult=" + result);
        }
        return result.toJSONObject();
    }

    @ApiOperation(value = "批量查询逻辑发货单")
    @PostMapping(path = "/sg/send/batchquery")
    public String batchQuery(HttpServletRequest request, @RequestParam(value = "param", required = true) String param) {
        //TODO 測試用ctr
        if (log.isDebugEnabled()) {
            log.debug("Strat SgSendCtrl.batchQuery. ReceiveParams=" + param + ";");
        }
        User user = getRootUser();
        ValueHolderV14 result;
        try {
            SgSendCmd sendCmd = (SgSendCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                    SgSendCmd.class.getName(), "sg", "1.0");
            List<SgStoreBillBase> list = JSONArray.parseArray(param, SgStoreBillBase.class);
            result = sendCmd.querySgBSend(list);
        } catch (Exception e) {
            throw new NDSException(Resources.getMessage(e.getMessage(), user.getLocale()));
        }
        if (log.isDebugEnabled()) {
            log.debug("Finish SgSendCtrl.batchQuery. ReturnResult=" + result);
        }
        return result.toString();
    }

    @ApiOperation(value = "查询逻辑发货单-实缺用")
    @PostMapping(path = "/sg/send/querySend")
    public JSONObject querySend(@RequestBody SgSendBillQueryRequest request) {
        //TODO 測試用ctr
        if (log.isDebugEnabled()) {
            log.debug("Strat SgSendCtrl.querySend. ReceiveParams=" + request + ";");
        }
        User user = getRootUser();
        ValueHolderV14 result;
        try {
            SgSendCmd sendCmd = (SgSendCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                    SgSendCmd.class.getName(), "sg", "1.0");
            result = sendCmd.querySgBSend(request);
        } catch (Exception e) {
            throw new NDSException(Resources.getMessage(e.getMessage(), user.getLocale()));
        }
        if (log.isDebugEnabled()) {
            log.debug("Finish SgSendCtrl.querySend. ReturnResult=" + result);
        }
        return result.toJSONObject();
    }

    @ApiOperation(value = "批量作废(取消)逻辑发货单并重新占单入参")
    @PostMapping(path = "/sg/send/batchvoidsave")
    public JSONObject batchVoidSave(HttpServletRequest request, @RequestParam(value = "param", required = true) String param) {
        //TODO 測試用ctr
        if (log.isDebugEnabled()) {
            log.debug("Strat SgSendCtrl.batchQuery. ReceiveParams=" + param + ";");
        }
        User user = getRootUser();
        ValueHolderV14 result;
        try {
            SgSendVoidSaveCmd voidSaveCmd = (SgSendVoidSaveCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                    SgSendVoidSaveCmd.class.getName(), "sg", "1.0");
            SgSendVoidSaveRequest model = JSONObject.parseObject(param, SgSendVoidSaveRequest.class);
            model.setLoginUser(user);
            result = voidSaveCmd.voidSaveSgBSend(model);
        } catch (Exception e) {
            throw new NDSException(Resources.getMessage(e.getMessage(), user.getLocale()));
        }
        if (log.isDebugEnabled()) {
            log.debug("Finish SgSendCtrl.batchQuery. ReturnResult=" + result);
        }
        return result.toJSONObject();
    }


    private User getRootUser() {
        UserImpl user = new UserImpl();
        user.setId(893);
        user.setName("Fire Blast");
        user.setEname("Pokemon-mapper");
        user.setActive(true);
        user.setClientId(37);
        user.setOrgId(27);
        user.setIsAdmin(2);
        user.setIsDev(2);
        return user;
    }
}
