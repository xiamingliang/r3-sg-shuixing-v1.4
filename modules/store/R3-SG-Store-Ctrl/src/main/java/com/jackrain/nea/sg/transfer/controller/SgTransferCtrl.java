package com.jackrain.nea.sg.transfer.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.transfer.api.*;
import com.jackrain.nea.sg.transfer.model.request.SgTransferBillSaveRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.face.impl.UserImpl;
import com.jackrain.nea.web.query.DefaultWebEvent;
import com.jackrain.nea.web.query.QuerySession;
import com.jackrain.nea.web.query.QuerySessionImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.jackrain.nea.oc.basic.api.TableServiceCmd;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

/**
 * @author csy
 */
@RestController
@Slf4j
@Api(value = "SgTransferCtrl", description = "逻拨单接口")
public class SgTransferCtrl {


    @ApiOperation(value = "调拨单新增")
    @PostMapping(path = "/sg/store/transfer/save")
    public JSONObject save(@RequestBody SgTransferBillSaveRequest saveRequest) {
        User user = getRootUser();
        saveRequest.setLoginUser(user);
        SgTransferSaveCmd saveCmd = (SgTransferSaveCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                SgTransferSaveCmd.class.getName(), "sg", "1.0");
        ValueHolderV14 result;
        try {
            if (log.isDebugEnabled()) {
                log.debug("开始调拨单保存!");
            }
            result = saveCmd.save(saveRequest);
        } catch (Exception e) {
            result = new ValueHolderV14();
            result.setCode(ResultCode.FAIL);
            result.setMessage(e.getMessage());
        }
        return result.toJSONObject();
    }


    @ApiOperation(value = "调拨单审核")
    @PostMapping(path = "/sg/store/transfer/audit")
    public JSONObject audit(Long objid) {
        User user = getRootUser();
        SgTransferAuditR3Cmd auditR3Cmd = (SgTransferAuditR3Cmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                SgTransferAuditR3Cmd.class.getName(), "sg", "1.0");
        ValueHolder result = new ValueHolder();
        try {
            QuerySession session = new QuerySessionImpl(getRootUser());
            DefaultWebEvent event = new DefaultWebEvent("audit", new HashMap(16));
            JSONObject param = new JSONObject();
            param.put("objid", objid);
            event.put("param", param);
            session.setEvent(event);
            result = auditR3Cmd.execute(session);
        } catch (Exception e) {
            e.printStackTrace();
            result.put("code", -1);
            result.put("message", e.getMessage());
        }
        return result.toJSONObject();
    }

    @ApiOperation(value = "调拨单自动出库")
    @PostMapping(path = "/sg/store/transfer/autoOut")
    public JSONObject autoOut(@RequestBody SgTransferBillSaveRequest saveRequest) {
        User user = getRootUser();
        saveRequest.setLoginUser(user);
        SgTransferAutoOutCmd saveCmd = (SgTransferAutoOutCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                SgTransferAutoOutCmd.class.getName(), "sg", "1.0");
        ValueHolderV14 result;
        try {
            if (log.isDebugEnabled()) {
                log.debug("开始调拨单自动出入库！");
            }
            result = saveCmd.autoOut(saveRequest);
        } catch (Exception e) {
            result = new ValueHolderV14();
            result.setCode(ResultCode.FAIL);
            result.setMessage(e.getMessage());
        }
        return result.toJSONObject();
    }


    @ApiOperation(value = "调拨单新增并审核")
    @PostMapping(path = "/sg/store/transfer/autoSaveSubmit")
    public JSONObject autoSaveSubmit(@RequestBody SgTransferBillSaveRequest saveRequest) {
        User user = getRootUser();
        saveRequest.setLoginUser(user);
        SgTransferSaveAndAuditCmd saveAndAuditCmd = (SgTransferSaveAndAuditCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                SgTransferSaveAndAuditCmd.class.getName(), "sg", "1.0");
        ValueHolderV14 result;
        try {
            if (log.isDebugEnabled()) {
                log.debug("开始调拨单自动新增并审核！");
            }
            result = saveAndAuditCmd.saveAndAudit(saveRequest);
        } catch (Exception e) {
            result = new ValueHolderV14();
            result.setCode(ResultCode.FAIL);
            result.setMessage(e.getMessage());
        }
        return result.toJSONObject();
    }


    @ApiOperation(value = "调拨单审核")
    @PostMapping(path = "/sg/store/transfer/diffDeal")
    public JSONObject diffDeal(Long objid) {
        User user = getRootUser();
        ScTransferDiffDealR3Cmd auditR3Cmd = (ScTransferDiffDealR3Cmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                ScTransferDiffDealR3Cmd.class.getName(), "sg", "1.0");
        ValueHolder result = new ValueHolder();
        try {
            QuerySession session = new QuerySessionImpl(getRootUser());
            DefaultWebEvent event = new DefaultWebEvent("deal", new HashMap(16));
            JSONObject param = new JSONObject();
            param.put("objid", objid);
            event.put("param", param);
            session.setEvent(event);
            result = auditR3Cmd.execute(session);
        } catch (Exception e) {
            e.printStackTrace();
            result.put("code", -1);
            result.put("message", e.getMessage());
        }
        return result.toJSONObject();
    }


    @ApiOperation(value = "调拨单差异处理")
    @PostMapping(path = "/sg/store/transfer/dealDiff")
    public JSONObject dealDiff(Long id) {
        User user = getRootUser();
        QuerySession session = new QuerySessionImpl(user);
        DefaultWebEvent event = new DefaultWebEvent("deal", new HashMap(16));
        JSONObject param = new JSONObject();
        param.put("objid", id);
        event.put("param", param);
        session.setEvent(event);
        ScTransferDiffDealR3Cmd saveAndAuditCmd = (ScTransferDiffDealR3Cmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                ScTransferDiffDealR3Cmd.class.getName(), "sg", "1.0");
        ValueHolder result;
        try {
            if (log.isDebugEnabled()) {
                log.debug("开始调拨单差异处理！");
            }
            result = saveAndAuditCmd.execute(session);
        } catch (Exception e) {
            result = new ValueHolder();
            result.put("code", ResultCode.FAIL);
            result.put("message", e.getMessage());
        }
        return result.toJSONObject();
    }

    /**
     * 测试用户
     */
    private User getRootUser() {
        UserImpl user = new UserImpl();
        user.setId(893);
        user.setName("root");
        user.setEname("root");
        user.setActive(true);
        user.setClientId(37);
        user.setOrgId(27);
        user.setIsAdmin(2);
        user.setIsDev(2);
        return user;
    }



    @ApiOperation(value = "保存")
    @PostMapping(path = "/sg/store/scrm/save")
    public JSONObject save(HttpServletRequest request, @RequestBody String param) {
        //TODO 測試用ctr
        if (log.isDebugEnabled()) {
            log.debug("Strat TableServiceTestCtrl.save. ReceiveParams=" + param + ";");
        }
        ValueHolder result;
        User user = getRootUser();
        QuerySessionImpl querySession = new QuerySessionImpl(user);
        JSONObject paramJo = JSON.parseObject(param);
        //默认 走save操作
        DefaultWebEvent event = new DefaultWebEvent(paramJo.getString("eventName") == null ? "dosave" : paramJo.getString("eventName"), request, false);
        event.put("param", paramJo);
        querySession.setEvent(event);
        Object o = ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                TableServiceCmd.class.getName(), "sg", "7");
        try {
            if (o == null) {
                result = new ValueHolder();
                result.put("code", -1);
                result.put("message", "未获取到对应的bean");
            } else {
                result = ((TableServiceCmd) o).execute(querySession);
            }
        } catch (Exception e) {
            result = new ValueHolder();
            result.put("code", -1);
            result.put("message", "error--" + e);
            e.printStackTrace();
        }
        if (log.isDebugEnabled()) {
            log.debug("Finish TableServiceTestCtrl.save. ReturnResult=" + result);
        }
        return result.toJSONObject();
    }


}
