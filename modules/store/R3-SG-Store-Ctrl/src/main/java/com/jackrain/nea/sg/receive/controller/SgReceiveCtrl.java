package com.jackrain.nea.sg.receive.controller;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.receive.api.SgReceiveCmd;
import com.jackrain.nea.sg.receive.model.request.SgReceiveBillCleanRequest;
import com.jackrain.nea.sg.receive.model.request.SgReceiveBillSaveRequest;
import com.jackrain.nea.sg.receive.model.request.SgReceiveBillSubmitRequest;
import com.jackrain.nea.sg.receive.model.request.SgReceiveBillVoidRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.face.impl.UserImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @author 舒威
 * @since 2019/4/29
 * create at : 2019/4/29 14:28
 */
@RestController
@Slf4j
@Api(value = "SgReceiveCtrl", description = "逻辑收货单接口")
public class SgReceiveCtrl {

    @ApiOperation(value = "新增/更新")
    @PostMapping(path = "/sg/receive/save")
    public JSONObject saveReceive(@RequestBody SgReceiveBillSaveRequest sgReceiveBillSaveRequest) {
        //TODO 測試用ctr
        if (log.isDebugEnabled()) {
            log.debug("Strat SgReceiveCtrl.saveReceive. ReceiveParams=" + sgReceiveBillSaveRequest + ";");
        }
        User user = getRootUser();
        ValueHolderV14 result;
        try {
            SgReceiveCmd receiveCmd = (SgReceiveCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                    SgReceiveCmd.class.getName(), "sg", "1.0");
            sgReceiveBillSaveRequest.setLoginUser(user);
            result = receiveCmd.saveSgBReceive(sgReceiveBillSaveRequest);
        } catch (Exception e) {
            throw new NDSException(Resources.getMessage(e.getMessage(), user.getLocale()));
        }
        if (log.isDebugEnabled()) {
            log.debug("Finish SgReceiveCtrl.saveReceive. ReturnResult=" + result);
        }
        return result.toJSONObject();
    }

    @ApiOperation(value = "逻辑收货单收货")
    @PostMapping(path = "/sg/receive/submit")
    public JSONObject submitReceive(HttpServletRequest request, @RequestParam(value = "param", required = true) String param) {
        //TODO 測試用ctr
        if (log.isDebugEnabled()) {
            log.debug("Strat SgReceiveCtrl.submitReceive. ReceiveParams=" + param + ";");
        }
        User user = getRootUser();
        ValueHolderV14 result;
        try {
            SgReceiveCmd receiveCmd = (SgReceiveCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                    SgReceiveCmd.class.getName(), "sg", "1.0");
            SgReceiveBillSubmitRequest model = JSONObject.parseObject(param, SgReceiveBillSubmitRequest.class);
            model.setLoginUser(user);
            result = receiveCmd.submitSgBReceive(model);
        } catch (Exception e) {
            throw new NDSException(Resources.getMessage(e.getMessage(), user.getLocale()));
        }
        if (log.isDebugEnabled()) {
            log.debug("Finish SgReceiveCtrl.submitReceive. ReturnResult=" + result);
        }
        return result.toJSONObject();
    }

    @ApiOperation(value = "逻辑收货单作废")
    @PostMapping(path = "/sg/receive/void")
    public JSONObject voidReceive(HttpServletRequest request, @RequestParam(value = "param", required = true) String param) {
        //TODO 測試用ctr
        if (log.isDebugEnabled()) {
            log.debug("Strat SgReceiveCtrl.voidReceive. ReceiveParams=" + param + ";");
        }
        User user = getRootUser();
        ValueHolderV14 result;
        try {
            SgReceiveCmd receiveCmd = (SgReceiveCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                    SgReceiveCmd.class.getName(), "sg", "1.0");
            SgReceiveBillVoidRequest model = JSONObject.parseObject(param, SgReceiveBillVoidRequest.class);
            model.setLoginUser(user);
            result = receiveCmd.voidSgBReceive(model);

        } catch (Exception e) {
            throw new NDSException(Resources.getMessage(e.getMessage(), user.getLocale()));
        }
        if (log.isDebugEnabled()) {
            log.debug("Finish SgReceiveCtrl.voidReceive. ReturnResult=" + result);
        }
        return result.toJSONObject();
    }

    @ApiOperation(value = "逻辑收货单清空")
    @PostMapping(path = "/sg/receive/clean")
    public JSONObject cleanReceive(HttpServletRequest request, @RequestParam(value = "param", required = true) String param) {
        //TODO 測試用ctr
        if (log.isDebugEnabled()) {
            log.debug("Strat SgReceiveCtrl.cleanReceive. ReceiveParams=" + param + ";");
        }
        User user = getRootUser();
        ValueHolderV14 result;
        try {
            SgReceiveCmd receiveCmd = (SgReceiveCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                    SgReceiveCmd.class.getName(), "sg", "1.0");
            SgReceiveBillCleanRequest model = JSONObject.parseObject(param, SgReceiveBillCleanRequest.class);
            model.setLoginUser(user);
            result = receiveCmd.cleanSgBReceive(model);
        } catch (Exception e) {
            throw new NDSException(Resources.getMessage(e.getMessage(), user.getLocale()));
        }
        if (log.isDebugEnabled()) {
            log.debug("Finish SgReceiveCtrl.sourceLogic. ReturnResult=" + result);
        }
        return result.toJSONObject();
    }


    private User getRootUser() {
        UserImpl user = new UserImpl();
        user.setId(893);
        user.setName("Fire Blast");
        user.setEname("Pokemon-mapper");
        user.setActive(true);
        user.setClientId(37);
        user.setOrgId(27);
        user.setIsAdmin(2);
        user.setIsDev(2);
        return user;
    }
}
