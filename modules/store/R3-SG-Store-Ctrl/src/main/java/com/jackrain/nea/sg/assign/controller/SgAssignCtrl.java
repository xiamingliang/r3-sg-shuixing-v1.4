package com.jackrain.nea.sg.assign.controller;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.assign.api.SgPrepareStoragePreCmd;
import com.jackrain.nea.sg.assign.api.SgPrepareStorageQueryCmd;
import com.jackrain.nea.sg.assign.model.request.SgPrepareStoragePreRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.face.impl.UserImpl;
import com.jackrain.nea.web.query.DefaultWebEvent;
import com.jackrain.nea.web.query.QuerySession;
import com.jackrain.nea.web.query.QuerySessionImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;

/**
 * @author : 孙俊磊
 * @since : 2019-10-10
 * create at : 2019-10-10 2:44 PM
 */
@RestController
@Slf4j
@Api(value = "SgAdjustCtrl", description = "逻辑调整单接口")
public class SgAssignCtrl {

    @ApiOperation(value = "铺货单 商品编码查询")
    @PostMapping(path = "/p/cs/sg/qbd/v1/queryTeus")
    public JSONObject query(HttpServletRequest request, @RequestBody String param) {
        //todo 待矩阵完成后调整
        User user = getRootUser();
//        User loginUser = (UserImpl) request.getSession().getAttribute("user");
//        ValueHolderV14 holderV14 = queryCmd.query(param, user);
//        return holderV14.toJSONObject();
        return null;
    }


    @ApiOperation(value = "配货单 页面查询")
    @PostMapping(path = "/p/cs/sg/qbd/v1/prepareStorageQuery")
    public JSONObject sgPrepareStorage(HttpServletRequest request, String param) {
        User user = getRootUser();
        QuerySession session = new QuerySessionImpl(user);
        SgPrepareStorageQueryCmd cmd = (SgPrepareStorageQueryCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(), SgPrepareStorageQueryCmd.class.getName()
                , "sg", "1.0");
        DefaultWebEvent event = new DefaultWebEvent("query", new HashMap(16));
        event.put("param", JSONObject.parseObject(param));
        session.setEvent(event);
        try {
            return cmd.execute(session).toJSONObject();
        } catch (Exception e) {
            return new ValueHolderV14<>(ResultCode.FAIL, getErrMsg(e)).toJSONObject();
        }
    }

    @ApiOperation(value = "配货单.保存")
    @PostMapping(path = "/p/cs/sg/qbd/v1/prepareStorageSave")
    public JSONObject savePs(@RequestBody SgPrepareStoragePreRequest request) {
        User user = getRootUser();
        request.setUser(user);
        SgPrepareStoragePreCmd cmd = (SgPrepareStoragePreCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(), SgPrepareStoragePreCmd.class.getName()
                , "sg", "1.0");
        try {
            return cmd.prepareStorage(request).toJSONObject();
        } catch (Exception e) {
            return new ValueHolderV14<>(ResultCode.FAIL, getErrMsg(e)).toJSONObject();
        }
    }


    private String getErrMsg(Exception e) {
        String msg = e.getMessage();
        if (StringUtils.isEmpty(msg)) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            msg = sw.toString().replace("\n", "<br/>").replace("\tat", "");
        }
        return msg;
    }

    /**
     * 获取 测试用户
     */
    private User getRootUser() {
        UserImpl user = new UserImpl();
        user.setId(893);
        user.setName("SHU WEI IS BONE OF HIS SWORD!");
        user.setEname("Pokemon-mapper");
        user.setActive(true);
        user.setClientId(37);
        user.setOrgId(27);
        user.setIsAdmin(2);
        user.setIsDev(2);
        return user;
    }
}
