package com.jackrain.nea.sg.adjust.controller;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.adjust.api.SgAdjAuditCmd;
import com.jackrain.nea.sg.adjust.api.SgAdjResendCmd;
import com.jackrain.nea.sg.adjust.api.SgAdjSaveAndAuditCmd;
import com.jackrain.nea.sg.adjust.api.SgAdjSaveCmd;
import com.jackrain.nea.sg.adjust.model.request.SgAdjustBillAuditRequest;
import com.jackrain.nea.sg.adjust.model.request.SgAdjustBillSaveRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.face.impl.UserImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @author csy
 */
@RestController
@Slf4j
@Api(value = "SgAdjustCtrl", description = "逻辑调整单接口")
public class SgAdjustCtrl {


    @ApiOperation(value = "逻辑调整单 新增/更新")
    @PostMapping(path = "/sg/store/adjust/save")
    public JSONObject save(@RequestBody SgAdjustBillSaveRequest saveRequest) {
        User user = getRootUser();
        saveRequest.setLoginUser(user);
        SgAdjSaveCmd saveCmd = (SgAdjSaveCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                SgAdjSaveCmd.class.getName(), "sg", "1.0");
        ValueHolderV14 result;
        try {
            result = saveCmd.saveAdj(saveRequest);
        } catch (Exception e) {
            result = new ValueHolderV14();
            result.setCode(ResultCode.FAIL);
            result.setMessage(e.getMessage());
        }
        return result.toJSONObject();
    }


    @ApiOperation(value = "逻辑调整单 审核")
    @PostMapping(path = "/sg/store/adjust/audit")
    public JSONObject audit(@RequestBody SgAdjustBillAuditRequest auditRequest) {
        User user = getRootUser();
        auditRequest.setUser(user);
        SgAdjAuditCmd auditCmd = (SgAdjAuditCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                SgAdjAuditCmd.class.getName(), "sg", "1.0");
        ValueHolderV14 result;
        try {
            result = auditCmd.audit(auditRequest);
        } catch (Exception e) {
            result = new ValueHolderV14();
            result.setCode(ResultCode.FAIL);
            result.setMessage(e.getMessage());
        }
        return result.toJSONObject();
    }

    @ApiOperation(value = "逻辑调整单 保存并审核")
    @PostMapping(path = "/sg/store/adjust/saveAndAudit")
    public JSONObject saveAndAudit(@RequestBody SgAdjustBillSaveRequest saveRequest) {
        User user = getRootUser();
        saveRequest.setLoginUser(user);
        SgAdjSaveAndAuditCmd auditCmd = (SgAdjSaveAndAuditCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                SgAdjSaveAndAuditCmd.class.getName(), "sg", "1.0");
        ValueHolderV14 result;
        try {
            result = auditCmd.saveAndAuditAdj(saveRequest);
        } catch (Exception e) {
            result = new ValueHolderV14();
            result.setCode(ResultCode.FAIL);
            result.setMessage(e.getMessage());
        }
        return result.toJSONObject();
    }


    @ApiOperation(value = "调整单补发生成结算日志task")
    @PostMapping(path = "/sg/store/adjust/resend")
    public JSONObject resend(HttpServletRequest request, @RequestParam(value = "param", required = true) String param) {
        SgAdjResendCmd resendCmd = (SgAdjResendCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                SgAdjResendCmd.class.getName(), "sg", "1.0");
        ValueHolderV14 result;
        try {
            result = resendCmd.resendAdjLog(JSONObject.parseObject(param));
        } catch (Exception e) {
            result = new ValueHolderV14();
            result.setCode(ResultCode.FAIL);
            result.setMessage(e.getMessage());
        }
        return result.toJSONObject();
    }

    /**
     * 获取 测试用户
     */
    private User getRootUser() {
        UserImpl user = new UserImpl();
        user.setId(893);
        user.setName("Fire BALL");
        user.setEname("Pokemon-mapper");
        user.setActive(true);
        user.setClientId(37);
        user.setOrgId(27);
        user.setIsAdmin(2);
        user.setIsDev(2);
        return user;
    }
}
