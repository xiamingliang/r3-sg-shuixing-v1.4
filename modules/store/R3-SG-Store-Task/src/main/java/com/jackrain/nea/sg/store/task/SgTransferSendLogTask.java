package com.jackrain.nea.sg.store.task;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.task.SgTransferSendLogService;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.task.IR3Task;
import com.jackrain.nea.task.RunTaskResult;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author leexh
 * @since 2019/8/12 13:14
 * desc: 调拨单生成结算日志，定时任务
 */
@Slf4j
@Component
public class SgTransferSendLogTask implements IR3Task {

    @Override
    public RunTaskResult execute(JSONObject params) {
        if (log.isDebugEnabled()) {
            log.info(this.getClass().getName() + ",SgTransferSendLogTask,params" + JSONObject.toJSONString(params));
        }

        boolean success = false;
        String message = "执行失败！";
        SgTransferSendLogService logService = ApplicationContextHandle.getBean(SgTransferSendLogService.class);
        ValueHolderV14 v14 = logService.sendLogForTransfer(params);
        if (v14 != null) {
            message = v14.getMessage();
            if (v14.getCode() == ResultCode.SUCCESS) {
                success = true;
            }
        }
        RunTaskResult runTaskResult = new RunTaskResult();
        runTaskResult.setSuccess(success);
        runTaskResult.setMessage(message);
        return runTaskResult;
    }
}
