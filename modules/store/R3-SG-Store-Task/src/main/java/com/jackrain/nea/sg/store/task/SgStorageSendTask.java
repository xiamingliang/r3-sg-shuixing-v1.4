package com.jackrain.nea.sg.store.task;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.sg.task.SgStorageSendTaskService;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.task.IR3Task;
import com.jackrain.nea.task.RunTaskResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author 舒威
 * @since 2019/7/16
 * create at : 2019/7/16 9:32
 */
@Slf4j
@Component
public class SgStorageSendTask implements IR3Task {

    @Autowired
    private SgStorageSendTaskService sendTaskService;

    @Override
    public RunTaskResult execute(JSONObject params) {
        log.info("Start SgStorageSendTask.execute. ReceiveParams:params:{};",
                JSONObject.toJSONString(params));

        long start = System.currentTimeMillis();
        RunTaskResult runTaskResult = new RunTaskResult();
        runTaskResult.setSuccess(Boolean.TRUE);
        ValueHolderV14 v14 = sendTaskService.sendTaskService(params);
        if (!v14.isOK()) {
            runTaskResult.setSuccess(Boolean.FALSE);
            runTaskResult.setMessage(v14.getMessage());
        }
        log.info("Finish SgStorageSendTask.execute. Return:results:{},spend time:{};",
                JSONObject.toJSONString(runTaskResult), (System.currentTimeMillis() - start));
        return runTaskResult;
    }
}
