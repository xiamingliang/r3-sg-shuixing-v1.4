package com.jackrain.nea.sg.store.task;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.sg.task.SgStoragePushESTaskService;
import com.jackrain.nea.task.IR3Task;
import com.jackrain.nea.task.RunTaskResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author 舒威
 * @since 2019/7/16
 * create at : 2019/7/16 9:32
 */
@Slf4j
@Component
public class SgStoragePushESTask implements IR3Task {

    @Autowired
    private SgStoragePushESTaskService pushESTaskService;

    @Override
    public RunTaskResult execute(JSONObject params) {
        log.info("Start SgStoragePushESTask.execute. ReceiveParams:params:{};",
                JSONObject.toJSONString(params));

        RunTaskResult runTaskResult = new RunTaskResult();
        runTaskResult.setSuccess(Boolean.TRUE);
        pushESTaskService.pushStoreES(params);
        return runTaskResult;
    }
}
