package com.jackrain.nea.sg.store.task;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.sg.task.SgAdjustSendLogService;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.task.IR3Task;
import com.jackrain.nea.task.RunTaskResult;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author leexh
 * @since 2019/8/14 16:14
 * desc: 调整单生成结算日志，实时任务
 */
@Slf4j
@Component
public class SgAdjustSendLogTask implements IR3Task {

    @Override
    public RunTaskResult execute(JSONObject params) {
        if (log.isDebugEnabled()) {
            log.info(this.getClass().getName() + ",SgAdjustSendLogTask,params" + JSONObject.toJSONString(params));
        }

        RunTaskResult runTaskResult = new RunTaskResult();
        runTaskResult.setSuccess(Boolean.TRUE);
        runTaskResult.setMessage("success");
        SgAdjustSendLogService logService = ApplicationContextHandle.getBean(SgAdjustSendLogService.class);
        ValueHolderV14 v14 = logService.sendLogForAdjust(params);
        if (!v14.isOK()) {
            runTaskResult.setSuccess(Boolean.FALSE);
            runTaskResult.setMessage(v14.getMessage());
        }
        return runTaskResult;
    }
}
