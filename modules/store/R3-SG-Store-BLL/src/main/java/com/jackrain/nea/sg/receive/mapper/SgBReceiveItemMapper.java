package com.jackrain.nea.sg.receive.mapper;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.receive.model.table.SgBReceiveItem;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface SgBReceiveItemMapper extends ExtentionMapper<SgBReceiveItem> {

    /**
     * 获取逻辑收货单占用合计数量
     *
     * @param id 逻辑收货单id
     */
    @Select("select SUM(qty_prein) as qty_prein,SUM(qty_receive) as qty_receive from sg_b_receive_item where sg_b_receive_id = #{id}")
    JSONObject selectTotalPreinByReceiveId(@Param("id") Long id);

    /**
     * 更新逻辑收货单明细的在途数量为0
     *
     * @param id 逻辑收货单id
     */
    @Update("UPDATE sg_b_receive_item SET qty_prein=0 WHERE sg_b_receive_id=#{id}")
    int updatePreinByReceiveId(@Param("id") Long id);

    /**
     * 分页查询逻辑收货单
     */
    @Select("SELECT * FROM " + SgConstants.SG_B_RECEIVE_ITEM + " ORDER BY ID LIMIT #{page} OFFSET #{offset}")
    List<SgBReceiveItem> selectListByPage(@Param("page") int page, @Param("offset") int offset);
}