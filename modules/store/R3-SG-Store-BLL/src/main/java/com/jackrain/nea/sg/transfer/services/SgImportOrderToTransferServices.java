package com.jackrain.nea.sg.transfer.services;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.Feature;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.core.db.Tools;
import com.jackrain.nea.oc.sale.api.OcSendOutQueryCmd;
import com.jackrain.nea.oc.sale.model.request.OcSendOutBillQueryRequest;
import com.jackrain.nea.oc.sale.model.result.OcSendOutResult;
import com.jackrain.nea.oc.sale.model.table.OcBSendOut;
import com.jackrain.nea.oc.sale.model.table.OcBSendOutImpItem;
import com.jackrain.nea.oc.sale.model.table.OcBSendOutItem;
import com.jackrain.nea.redis.config.CusRedisTemplate;
import com.jackrain.nea.redis.util.RedisOpsUtil;
import com.jackrain.nea.sg.basic.api.SgStorageQueryCmd;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.model.request.SgStorageQueryRequest;
import com.jackrain.nea.sg.basic.model.table.SgBStorage;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.send.utils.SgStoreESUtils;
import com.jackrain.nea.sg.transfer.common.ScTransferConstants;
import com.jackrain.nea.sg.transfer.common.SgTransferConstantsIF;
import com.jackrain.nea.sg.transfer.enums.SgTransferBillStatusEnum;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferImpItemMapper;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferItemMapper;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferMapper;
import com.jackrain.nea.sg.transfer.model.table.ScBTransfer;
import com.jackrain.nea.sg.transfer.model.table.ScBTransferImpItem;
import com.jackrain.nea.sg.transfer.model.table.ScBTransferItem;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.utils.AssertUtils;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.query.DefaultWebEvent;
import com.jackrain.nea.web.query.QuerySession;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @author leexh
 * @since 2019/7/2 16:52
 * desc: 导入发货订单未完成量
 */
@Slf4j
@Component
public class SgImportOrderToTransferServices {

    @Autowired
    private SgStoreESUtils storeESUtils;

    @Transactional(rollbackFor = Exception.class)
    public ValueHolder importOrderToTransfer(QuerySession session) {
        ValueHolder valueHolder = new ValueHolder();

        DefaultWebEvent event = session.getEvent();
        JSONObject param = JSON.parseObject(JSON.toJSONStringWithDateFormat(event.getParameterValue("param"),
                "yyyy-MM-dd HH:mm:ss", SerializerFeature.WriteMapNullValue), Feature.OrderedField);
        if (log.isDebugEnabled()) {
            log.debug("--------------------------> 进入SgImportOrderToTransferServices.importOrderToTransfer方法 <--------------------------");
            log.debug("入参：" + JSONObject.toJSONString(param));
        }
        User user = session.getUser();
        Locale locale = user.getLocale();

        AssertUtils.notNull(param, "参数不能为空！", locale);
        Long objId = param.getLong("objid");
        AssertUtils.notNull(objId, "请选择要要操作的数据！", locale);

        ScBTransferMapper transferMapper = ApplicationContextHandle.getBean(ScBTransferMapper.class);
        ScBTransfer scBTransfer = transferMapper.selectById(objId);
        AssertUtils.notNull(scBTransfer, "当前记录已不存在！", locale);
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ".debug, scBTransfer:" + JSONObject.toJSONString(scBTransfer));
        }

        CusRedisTemplate<String, String> redisTemplate = RedisOpsUtil.getStrRedisTemplate();
        String lockKsy = ScTransferConstants.TRANSFER_TYPE + ":" + scBTransfer.getBillNo();
        if (redisTemplate.opsForValue().get(lockKsy) == null) {
            redisTemplate.opsForValue().set(lockKsy, "OK", 30, TimeUnit.MINUTES);
        } else {
            String msg = "当前调拨单正在被操作，导入发货订单未完成量失败！请稍后重试...";
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + "，debug," + msg);
            }
            AssertUtils.logAndThrow(msg, user.getLocale());
        }

        try {
            Integer status = scBTransfer.getStatus();
            if (status == SgTransferBillStatusEnum.AUDITED_NOT_OUT.getVal() ||
                    status == SgTransferBillStatusEnum.AUDITED_PART_OUT_NOT_IN.getVal() ||
                    status == SgTransferBillStatusEnum.AUDITED_PART_OUT_PART_IN.getVal() ||
                    status == SgTransferBillStatusEnum.AUDITED_ALL_OUT_NOT_IN.getVal() ||
                    status == SgTransferBillStatusEnum.AUDITED_ALL_OUT_PART_IN.getVal() ||
                    status == SgTransferBillStatusEnum.AUDITED_ALL_OUT_ALL_IN.getVal()) {
                AssertUtils.logAndThrow("当前记录已审核，不允许导入！", locale);
            }

            if (status == SgTransferBillStatusEnum.VOIDED.getVal()) {
                AssertUtils.logAndThrow("当前记录已作废，不允许导入！", locale);
            }

            Integer transferType = scBTransfer.getTransferType();
            if (transferType != null && transferType != SgTransferConstantsIF.TRANSFER_TYPE_ORDER) {
                AssertUtils.logAndThrow("当前记录不是订单调拨，不允许导入！", locale);
            }
            Integer isLocked = scBTransfer.getIsLocked();
            if (isLocked != null && isLocked == ScTransferConstants.IS_LOCKED) {
                AssertUtils.logAndThrow("当前记录已经锁单，不允许导入！", locale);
            }
            Long ocBSendOutId = scBTransfer.getOcBSendOutId();
            AssertUtils.notNull(ocBSendOutId, "调拨单所关联的发货订单ID为空！", locale);

            // rpc调用发货订单查询接口
            OcSendOutQueryCmd sendOutQueryCmd = (OcSendOutQueryCmd) ReferenceUtil.refer(
                    ApplicationContextHandle.getApplicationContext(),
                    OcSendOutQueryCmd.class.getName(),
                    "oc", "1.0");

            List<OcSendOutBillQueryRequest> requests = new ArrayList<>();
            OcSendOutBillQueryRequest request = new OcSendOutBillQueryRequest();
            request.setId(ocBSendOutId);
            requests.add(request);
            ValueHolderV14<List<OcSendOutResult>> v14 = sendOutQueryCmd.querySendOut(requests);
            List<OcSendOutResult> data = v14.getData();
            if (CollectionUtils.isEmpty(data)) {
                AssertUtils.logAndThrow("调拨单所关联的发货订单不存在！", locale);
            }

            OcBSendOut sendOut = data.get(0).getSendOut();
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + ".debug, 发货订单对象:" + JSONObject.toJSONString(sendOut));
            }

            Integer sendOutStatus = sendOut.getStatus();
            if (sendOutStatus != null && sendOutStatus == ScTransferConstants.SEND_OUT_STATUS_WAIT) {
                AssertUtils.logAndThrow("发货订单未审核，不允许导入！", locale);
            }

            if (sendOutStatus != null && sendOutStatus == ScTransferConstants.SEND_OUT_STATUS_CASE) {
                AssertUtils.logAndThrow("发货订单已结案，不允许导入！", locale);
            }

            // （物理）删除当前调拨单的调拨单明细
            ScBTransferItemMapper itemMapper = ApplicationContextHandle.getBean(ScBTransferItemMapper.class);
            // 保存删除调拨单明细的id
            List<Long> itemLongList = itemMapper.selectList(new QueryWrapper<ScBTransferItem>().lambda()
                    .eq(ScBTransferItem::getScBTransferId, objId)).stream().map(s -> s.getId()).collect(Collectors.toList());
            JSONArray itemArray = new JSONArray();
            for (Long itemid : itemLongList) {
                itemArray.add(itemid);
            }
            itemMapper.delete(new QueryWrapper<ScBTransferItem>().lambda()
                    .eq(ScBTransferItem::getScBTransferId, objId));

            // 所关联【发货订单】的订单明细中剩余量>0的数据插入本单明细
            List<OcBSendOutItem> items = data.get(0).getItems();
            insertTransferItem(user, objId, scBTransfer.getCpCOrigId(), items, itemMapper);

            // 更新主表统计字段
            updateTransfer(user, objId, transferMapper, itemMapper);

            //推送ES 先删除旧的明细
            storeESUtils.pushESByTransfer(objId, true, true, itemArray, transferMapper, itemMapper);
        } catch (Exception e) {
            log.error(this.getClass().getName() + "导入发货订单未完成量失败异常" + e);
            AssertUtils.logAndThrowExtra("导入发货订单未完成量失败异常", e);
        } finally {
            try {
                redisTemplate.delete(lockKsy);
            } catch (Exception e) {
                log.error(this.getClass().getName() + "redis锁释放失败，请联系管理员！lockKsy：" + lockKsy);
                AssertUtils.logAndThrow("redis锁释放失败，请联系管理员！");
            }
        }

        valueHolder.put("code", ResultCode.SUCCESS);
        valueHolder.put("message", "导入成功！");
        return valueHolder;
    }

    /**
     * 更新主表统计字段
     *
     * @param user           用户信息
     * @param transferId     调拨单ID
     * @param transferMapper mapper
     * @param itemMapper     mapper
     */
    private void updateTransfer(User user, Long transferId, ScBTransferMapper transferMapper, ScBTransferItemMapper itemMapper) {
        ScBTransferItem transferItem = itemMapper.selectSumQtyAndAmtByTransferId(transferId);
        if (transferItem != null) {
            ScBTransfer scBTransfer = transferMapper.selectById(transferId);
            AssertUtils.notNull(scBTransfer, "调拨单不存在！调拨单ID=" + transferId, user.getLocale());

            ScBTransfer updateTransfer = new ScBTransfer();
            updateTransfer.setId(scBTransfer.getId());
            updateTransfer.setTotQty(transferItem.getQty());
            updateTransfer.setTotQtyIn(transferItem.getQtyIn());
            updateTransfer.setTotAmtList(transferItem.getAmtList());
            updateTransfer.setTotAmtInList(transferItem.getAmtInList());
            updateTransfer.setModifierid(user.getId().longValue());
            updateTransfer.setModifiername(user.getName());
            updateTransfer.setModifierename(user.getEname());
            updateTransfer.setModifieddate(new Timestamp(System.currentTimeMillis()));
            transferMapper.updateById(updateTransfer);
        }
    }

    /**
     * 发后订单明细复制到调拨单明细
     *
     * @param user            用户信息
     * @param transferId      调拨单ID
     * @param ocBSendOutItems 发货订单明细
     * @param itemMapper      mapper对象
     */
    private void insertTransferItem(User user, Long transferId, Long storeId, List<OcBSendOutItem> ocBSendOutItems,
                                    ScBTransferItemMapper itemMapper) {

        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        long userId = user.getId().longValue();
        String name = user.getName();
        String eName = user.getEname();

        List<OcBSendOutItem> sendOutList = new ArrayList<>();
        if (!CollectionUtils.isEmpty(ocBSendOutItems)) {
            ocBSendOutItems.forEach(sendOutItem -> {
                BigDecimal qtyRem = sendOutItem.getQtyRem();
                if (qtyRem != null && qtyRem.compareTo(BigDecimal.ZERO) > 0) {
                    sendOutList.add(sendOutItem);
                }
            });
        }

        if (CollectionUtils.isEmpty(sendOutList)) {
            AssertUtils.logAndThrow("所关联的发货订单明细中【剩余数量】>0的明细不存在！", user.getLocale());
        }

        // list转map
        HashMap<String, SgBStorage> skuMap = new HashMap<>();
        List<SgBStorage> storageList = queryPhysicalStorage(user, storeId, ocBSendOutItems);
        if (!CollectionUtils.isEmpty(storageList)) {
            for (SgBStorage storage : storageList) {
                skuMap.put(storage.getPsCSkuEcode(), storage);
            }
        }

        ocBSendOutItems.forEach(sendOutItem -> {
            ScBTransferItem transferItem = new ScBTransferItem();
            BeanUtils.copyProperties(sendOutItem, transferItem);

            // 获取明细ID
            transferItem.setId(Tools.getSequence(ScTransferConstants.TRANSFER_ITEM_TYPE.toUpperCase()));
            transferItem.setScBTransferId(transferId);
            transferItem.setQty(BigDecimal.ZERO);
            // 【调拨数量】：如果当前SKU在发货店仓库存查询中的可用库存 >= 发货订单中的剩余量,
            //  取值发货订单中的剩余量，否则，取值库存查询中的可用库存
            if (!CollectionUtils.isEmpty(skuMap)) {
                BigDecimal qtyRem = sendOutItem.getQtyRem();
                SgBStorage storage = skuMap.get(sendOutItem.getPsCSkuEcode());
                if (storage != null && storage.getQtyAvailable() != null) {
                    if (storage.getQtyAvailable().compareTo(qtyRem) >= 0) {
                        transferItem.setQty(qtyRem);
                    } else {
                        transferItem.setQty(storage.getQtyAvailable());
                    }
                }
            }

            // 【出库数量】、【入库数量】：0,
            // 【差异数量】=【出库数量】-【入库数量】：0
            transferItem.setQtyOut(BigDecimal.ZERO);
            transferItem.setQtyIn(BigDecimal.ZERO);
            transferItem.setQtyDiff(BigDecimal.ZERO);
            transferItem.setAmtOutList(BigDecimal.ZERO);
            transferItem.setAmtInList(BigDecimal.ZERO);

            // 创建人、修改人
            transferItem.setOwnerid(userId);
            transferItem.setOwnername(name);
            transferItem.setOwnerename(eName);
            transferItem.setCreationdate(timestamp);
            transferItem.setModifierid(userId);
            transferItem.setModifiername(name);
            transferItem.setModifierename(eName);
            transferItem.setModifieddate(timestamp);
            itemMapper.insert(transferItem);
        });
    }

    /**
     * 批量查询【库存查询】sku数量信息
     *
     * @param loginUser       用户信息
     * @param storeId         店仓ID
     * @param ocBSendOutItems 发货订单明细
     * @return 【库存查询结果】
     */
    private List<SgBStorage> queryPhysicalStorage(User loginUser, Long storeId, List<OcBSendOutItem> ocBSendOutItems) {
        SgStorageQueryCmd storageQueryCmd = (SgStorageQueryCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(), SgStorageQueryCmd.class.getName(),
                SgConstantsIF.GROUP, SgConstantsIF.VERSION);

        List<SgBStorage> storageList = new ArrayList<>();
        List<String> skuList = ocBSendOutItems.stream().map(OcBSendOutItem::getPsCSkuEcode).collect(Collectors.toList());

        List<Long> storeIds = new ArrayList<>();
        storeIds.add(storeId);

        SgStorageQueryRequest request = new SgStorageQueryRequest();
        request.setStoreIds(storeIds);

        int itemNum = skuList.size();
        int pageSize = SgConstants.SG_COMMON_INSERT_PAGE_SIZE;
        int page = itemNum / pageSize;
        if (itemNum % pageSize != 0) page++;
        for (int i = 0; i < page; i++) {
            int startIndex = i * pageSize;
            int endIndex = (i + 1) * pageSize;
            List<String> skuCodes = skuList.subList(startIndex, endIndex < itemNum ? endIndex : itemNum);
            request.setSkuEcodes(skuCodes);
            ValueHolderV14<List<SgBStorage>> v14 = storageQueryCmd.queryStorage(request, loginUser);
            AssertUtils.notNull(v14, "库存查询未知错误！", loginUser.getLocale());
            if (v14.getCode() == ResultCode.FAIL) {
                AssertUtils.logAndThrow("库存查询异常：" + v14.getMessage(), loginUser.getLocale());
            }
            storageList.addAll(v14.getData());
        }
        return storageList;
    }

    /**
     * 导入订单 未完成量 箱操作 相关 ， 箱操作导入 即导入 录入明细
     *
     * @param session
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public ValueHolder importOrderToTransferInTeus(QuerySession session) {
        ValueHolder valueHolder = new ValueHolder();

        DefaultWebEvent event = session.getEvent();
        JSONObject param = JSON.parseObject(JSON.toJSONStringWithDateFormat(event.getParameterValue("param"),
                "yyyy-MM-dd HH:mm:ss", SerializerFeature.WriteMapNullValue), Feature.OrderedField);
        if (log.isDebugEnabled()) {
            log.debug("--------------------------> 进入SgImportOrderToTransferServices.importOrderToTransfer方法 <--------------------------");
            log.debug("入参：" + JSONObject.toJSONString(param));
        }
        User user = session.getUser();
        Locale locale = user.getLocale();

        AssertUtils.notNull(param, "参数不能为空！", locale);
        Long objId = param.getLong("objid");
        AssertUtils.notNull(objId, "请选择要要操作的数据！", locale);

        ScBTransferMapper transferMapper = ApplicationContextHandle.getBean(ScBTransferMapper.class);
        ScBTransfer scBTransfer = transferMapper.selectById(objId);
        AssertUtils.notNull(scBTransfer, "当前记录已不存在！", locale);
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ".debug, scBTransfer:" + JSONObject.toJSONString(scBTransfer));
        }

        //  箱操作， 以单据 ID 为 redis 锁
        CusRedisTemplate<String, String> redisTemplate = RedisOpsUtil.getStrRedisTemplate();
        String lockKsy = ScTransferConstants.TRANSFER_TYPE + ":" + scBTransfer.getId();
        if (redisTemplate.opsForValue().get(lockKsy) == null) {
            redisTemplate.opsForValue().set(lockKsy, "OK", 30, TimeUnit.MINUTES);
        } else {
            String msg = "当前调拨单正在被操作，导入发货订单未完成量失败！请稍后重试...";
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + "，debug," + msg);
            }
            AssertUtils.logAndThrow(msg, user.getLocale());
        }

        try {
            Integer status = scBTransfer.getStatus();
            if (status == SgTransferBillStatusEnum.AUDITED_NOT_OUT.getVal() ||
                    status == SgTransferBillStatusEnum.AUDITED_PART_OUT_NOT_IN.getVal() ||
                    status == SgTransferBillStatusEnum.AUDITED_PART_OUT_PART_IN.getVal() ||
                    status == SgTransferBillStatusEnum.AUDITED_ALL_OUT_NOT_IN.getVal() ||
                    status == SgTransferBillStatusEnum.AUDITED_ALL_OUT_PART_IN.getVal() ||
                    status == SgTransferBillStatusEnum.AUDITED_ALL_OUT_ALL_IN.getVal()) {
                AssertUtils.logAndThrow("当前记录已审核，不允许导入！", locale);
            }

            if (status == SgTransferBillStatusEnum.VOIDED.getVal()) {
                AssertUtils.logAndThrow("当前记录已作废，不允许导入！", locale);
            }

            Integer transferType = scBTransfer.getTransferType();
            if (transferType != null && transferType != SgTransferConstantsIF.TRANSFER_TYPE_ORDER) {
                AssertUtils.logAndThrow("当前记录不是订单调拨，不允许导入！", locale);
            }
            Integer isLocked = scBTransfer.getIsLocked();
            if (isLocked != null && isLocked == ScTransferConstants.IS_LOCKED) {
                AssertUtils.logAndThrow("当前记录已经锁单，不允许导入！", locale);
            }
            Long ocBSendOutId = scBTransfer.getOcBSendOutId();
            AssertUtils.notNull(ocBSendOutId, "调拨单所关联的发货订单ID为空！", locale);

            // rpc调用发货订单查询接口
            OcSendOutQueryCmd sendOutQueryCmd = (OcSendOutQueryCmd) ReferenceUtil.refer(
                    ApplicationContextHandle.getApplicationContext(),
                    OcSendOutQueryCmd.class.getName(),
                    "oc", "1.0");

            List<OcSendOutBillQueryRequest> requests = new ArrayList<>();
            OcSendOutBillQueryRequest request = new OcSendOutBillQueryRequest();
            request.setId(ocBSendOutId);
            requests.add(request);
            ValueHolderV14<List<OcSendOutResult>> v14 = sendOutQueryCmd.querySendOut(requests);
            List<OcSendOutResult> data = v14.getData();
            if (CollectionUtils.isEmpty(data)) {
                AssertUtils.logAndThrow("调拨单所关联的发货订单不存在！", locale);
            }

            OcBSendOut sendOut = data.get(0).getSendOut();
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + ".debug, 发货订单对象:" + JSONObject.toJSONString(sendOut));
            }

            /**
             * cs 新增 箱操作 逻辑
             */
            String psCTeusType = sendOut.getPsCTeusType();
            if (!ScTransferConstants.TEUS_TYPE_SM.equals(psCTeusType)) {
                AssertUtils.logAndThrow("当前发货订单不是散码订单，不允许导入！", locale);
            }

            Integer sendOutStatus = sendOut.getStatus();
            if (sendOutStatus != null && sendOutStatus == ScTransferConstants.SEND_OUT_STATUS_WAIT) {
                AssertUtils.logAndThrow("发货订单未审核，不允许导入！", locale);
            }

            if (sendOutStatus != null && sendOutStatus == ScTransferConstants.SEND_OUT_STATUS_CASE) {
                AssertUtils.logAndThrow("发货订单已结案，不允许导入！", locale);
            }

            // （物理）删除当前调拨单的调拨单明细 todo 操作 录入明细
            ScBTransferImpItemMapper impItemMapper = ApplicationContextHandle.getBean(ScBTransferImpItemMapper.class);
            // 保存删除调拨单明细的id
            List<Long> itemLongList = impItemMapper.selectList(new QueryWrapper<ScBTransferImpItem>().lambda()
                    .eq(ScBTransferImpItem::getScBTransferId, objId)).stream().map(s -> s.getId()).collect(Collectors.toList());
            JSONArray itemArray = new JSONArray();
            for (Long itemid : itemLongList) {
                itemArray.add(itemid);
            }
            impItemMapper.delete(new QueryWrapper<ScBTransferImpItem>().lambda()
                    .eq(ScBTransferImpItem::getScBTransferId, objId));

            // 所关联【发货订单】的订单录入明细中剩余量>0的数据插入本单录入明细
            List<OcBSendOutImpItem> items = data.get(0).getImpItems();
            insertTransferImpItem(user, objId, scBTransfer.getCpCOrigId(), items, impItemMapper);

//            // 更新主表统计字段
//            updateTransferInTeus(user, objId, transferMapper, impItemMapper);

//            //推送ES 先删除旧的明细
//            String index = ScTransferConstants.TRANSFER_INDEX;
//            String type = ScTransferConstants.TRANSFER_TYPE;
//            String child_type = ScTransferConstants.TRANSFER_ITEM_TYPE;
//            StorageESUtils.pushESBModelByUpdate(null, null, objId, itemArray, index, type, child_type, null, null, null, true);
//
//            //推送ES
//            ScBTransfer transfer = transferMapper.selectById(objId);
//            List<ScBTransferItem> scBTransferItemList = itemMapper.selectList(new QueryWrapper<ScBTransferItem>()
//                    .eq(ScTransferConstants.TRANSFER_PARENT_FIELD, objId));
//            String parentKey = ScTransferConstants.TRANSFER_PARENT_FIELD;
//            StorageESUtils.pushESBModelByUpdate(transfer, scBTransferItemList, transfer.getId(), null, index, type, child_type, parentKey, ScBTransfer.class, ScBTransferItem.class, false);

        } catch (Exception e) {
            log.error(this.getClass().getName() + "导入发货订单未完成量失败异常" + e);
            AssertUtils.logAndThrowExtra("导入发货订单未完成量失败异常", e);
        } finally {
            try {
                redisTemplate.delete(lockKsy);
            } catch (Exception e) {
                log.error(this.getClass().getName() + "redis锁释放失败，请联系管理员！lockKsy：" + lockKsy);
                AssertUtils.logAndThrow("redis锁释放失败，请联系管理员！");
            }
        }

        valueHolder.put("code", ResultCode.SUCCESS);
        valueHolder.put("message", "导入成功！");
        return valueHolder;
    }

    /**
     * 发货订单 录入明细 复制到 调拨单录入明细
     *
     * @param user               用户信息
     * @param transferId         调拨单ID
     * @param ocBSendOutImpItems 发货订单明细
     * @param impItemMapper      mapper对象
     */
    private void insertTransferImpItem(User user, Long transferId, Long storeId, List<OcBSendOutImpItem> ocBSendOutImpItems,
                                       ScBTransferImpItemMapper impItemMapper) {

        List<OcBSendOutImpItem> sendOutList = new ArrayList<>();
        if (!CollectionUtils.isEmpty(ocBSendOutImpItems)) {
            ocBSendOutImpItems.forEach(sendOutImpItem -> {
                BigDecimal qtyRem = sendOutImpItem.getQtyRem();
                if (qtyRem != null && qtyRem.compareTo(BigDecimal.ZERO) > 0) {
                    sendOutList.add(sendOutImpItem);
                }
            });
        }

        if (CollectionUtils.isEmpty(sendOutList)) {
            AssertUtils.logAndThrow("所关联的发货订单明细中【剩余数量】>0的明细不存在！", user.getLocale());
        }

        // list转map
        HashMap<String, SgBStorage> skuMap = new HashMap<>();
        List<SgBStorage> storageList = queryPhysicalStorageInTeus(user, storeId, ocBSendOutImpItems);
        if (!CollectionUtils.isEmpty(storageList)) {
            for (SgBStorage storage : storageList) {
                skuMap.put(storage.getPsCSkuEcode(), storage);
            }
        }

        //  排除 <= 0 的数据 ，循环
        sendOutList.forEach(sendOutImpItem -> {
            ScBTransferImpItem transferImpItem = new ScBTransferImpItem();
            BeanUtils.copyProperties(sendOutImpItem, transferImpItem);

            // 获取明细ID
            transferImpItem.setId(Tools.getSequence(SgConstants.SC_B_TRANSFER_IMP_ITEM.toUpperCase()));
            transferImpItem.setScBTransferId(transferId);
            transferImpItem.setQty(BigDecimal.ZERO);
            // 【调拨数量】：如果当前SKU在发货店仓库存查询中的可用库存 >= 发货订单中的剩余量,
            //  取值发货订单中的剩余量，否则，取值库存查询中的可用库存
            if (!CollectionUtils.isEmpty(skuMap)) {
                BigDecimal qtyRem = sendOutImpItem.getQtyRem();
                SgBStorage storage = skuMap.get(sendOutImpItem.getPsCSkuEcode());
                if (storage != null && storage.getQtyAvailable() != null) {
                    if (storage.getQtyAvailable().compareTo(qtyRem) >= 0) {
                        transferImpItem.setQty(qtyRem);
                    } else {
                        transferImpItem.setQty(storage.getQtyAvailable());
                    }
                }
            }

            // 【出库数量】、【入库数量】：0,
            // 【差异数量】=【出库数量】-【入库数量】：0
            transferImpItem.setQtyOut(BigDecimal.ZERO);
            transferImpItem.setQtyIn(BigDecimal.ZERO);

            // 创建人、修改人
            StorageESUtils.setBModelDefalutData(transferImpItem, user);
            impItemMapper.insert(transferImpItem);
        });
    }


    /**
     * 批量查询【库存查询】sku数量信息
     *
     * @param loginUser          用户信息
     * @param storeId            店仓ID
     * @param ocBSendOutImpItems 发货订单明细
     * @return 【库存查询结果】
     */
    private List<SgBStorage> queryPhysicalStorageInTeus(User loginUser, Long storeId, List<OcBSendOutImpItem> ocBSendOutImpItems) {
        SgStorageQueryCmd storageQueryCmd = (SgStorageQueryCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(), SgStorageQueryCmd.class.getName(),
                SgConstantsIF.GROUP, SgConstantsIF.VERSION);

        List<SgBStorage> storageList = new ArrayList<>();
        // 用 商品 ECODE  不用 条码 （可能为空）
        List<String> skuList = ocBSendOutImpItems.stream().map(OcBSendOutImpItem::getPsCProEcode).collect(Collectors.toList());

        List<Long> storeIds = new ArrayList<>();
        storeIds.add(storeId);

        SgStorageQueryRequest request = new SgStorageQueryRequest();
        request.setStoreIds(storeIds);

        int itemNum = skuList.size();
        int pageSize = SgConstants.SG_COMMON_INSERT_PAGE_SIZE;
        int page = itemNum / pageSize;
        if (itemNum % pageSize != 0) page++;
        for (int i = 0; i < page; i++) {
            int startIndex = i * pageSize;
            int endIndex = (i + 1) * pageSize;
            List<String> skuCodes = skuList.subList(startIndex, endIndex < itemNum ? endIndex : itemNum);
            request.setSkuEcodes(skuCodes);
            ValueHolderV14<List<SgBStorage>> v14 = storageQueryCmd.queryStorage(request, loginUser);
            AssertUtils.notNull(v14, "库存查询未知错误！", loginUser.getLocale());
            if (v14.getCode() == ResultCode.FAIL) {
                AssertUtils.logAndThrow("库存查询异常：" + v14.getMessage(), loginUser.getLocale());
            }
            storageList.addAll(v14.getData());
        }
        return storageList;
    }


}
