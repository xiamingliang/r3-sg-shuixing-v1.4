package com.jackrain.nea.sg.transfer.utils;

import com.jackrain.nea.config.Resources;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.transfer.enums.SgTransferBillStatusEnum;
import com.jackrain.nea.utils.AssertUtils;

import java.util.Locale;

/**
 * @author csy
 * Date: 2019/5/8
 * Description: 获取回写后的 单据状态
 */
public class SgTransferBillStatusUtil {


    /**
     * 根据出库结果单 的 单据状态
     *
     * @param isTotalEq     总数是否相等
     * @param preBillStatus 之前的单据状态
     * @param isLast        是否是最后
     */
    public static int getOutStatusIntVal(boolean isTotalEq, SgTransferBillStatusEnum preBillStatus, boolean isLast, Locale locale) {
        SgTransferBillStatusEnum statusEnum = getOutBillStatus(isTotalEq, preBillStatus, isLast);
        AssertUtils.notNull(statusEnum, "出库单据状态有误!", locale);
        return statusEnum.getVal();
    }

    /**
     * 获取入库结果单 的 单据状态
     *
     * @param isTotalEq     针对入库单 表示 是否可以跟改为最终状态
     * @param preBillStatus 之前的单据状态
     * @param locale        国际化
     * @return 入库单的 单据状态
     */
    public static int getInBillStatusIntVal(boolean isTotalEq, SgTransferBillStatusEnum preBillStatus, Locale locale) {
        SgTransferBillStatusEnum statusEnum = getInStatus(preBillStatus, isTotalEq);
        if (statusEnum == null) {
            throw new NDSException(Resources.getMessage("入库单据状态有误", locale));
        }
        return statusEnum.getVal();

    }


    /**
     * 根据出入库 数量判断现在的单据状态
     *
     * @param isTotalEq     总数是否相等
     * @param preBillStatus 之前的单据状态
     * @param isLast        是否是最后
     */
    private static SgTransferBillStatusEnum getOutBillStatus(boolean isTotalEq
            , SgTransferBillStatusEnum preBillStatus, boolean isLast) {
        if (isTotalEq || isLast) {
            return getOutFinalStatus(preBillStatus);
        } else {
            return getOutNextStatus(preBillStatus);
        }
    }


    /**
     * 获取下一阶段的出库单据状态
     *
     * @param preBillStatus 前次的 单据编号
     */
    private static SgTransferBillStatusEnum getOutNextStatus(SgTransferBillStatusEnum preBillStatus) {
        switch (preBillStatus) {
            case UN_AUDITED:
            case VOIDED:
            case AUDITED_ALL_OUT_ALL_IN:
                return null;
            case AUDITED_NOT_OUT:
                return SgTransferBillStatusEnum.AUDITED_PART_OUT_NOT_IN;
            case AUDITED_ALL_OUT_NOT_IN:
                return SgTransferBillStatusEnum.AUDITED_NOT_OUT;

            default:
                return preBillStatus;
        }
    }


    /**
     * 获取最终阶段的出库单据状态
     *
     * @param preBillStatus 前次的 单据编号
     */
    private static SgTransferBillStatusEnum getOutFinalStatus(SgTransferBillStatusEnum preBillStatus) {
        switch (preBillStatus) {
            case UN_AUDITED:
            case VOIDED:
            case AUDITED_ALL_OUT_ALL_IN:
                return null;
            case AUDITED_NOT_OUT:
                return SgTransferBillStatusEnum.AUDITED_ALL_OUT_NOT_IN;
            case AUDITED_PART_OUT_PART_IN:
                return SgTransferBillStatusEnum.AUDITED_ALL_OUT_PART_IN;
            case AUDITED_PART_OUT_NOT_IN:
                return SgTransferBillStatusEnum.AUDITED_ALL_OUT_NOT_IN;
            default:
                return preBillStatus;
        }
    }


    /**
     * 获取入库结果状态
     *
     * @param preBillStatus 当前单据的 单据状态
     * @param isTotal       针对入库单 表示 是否可以跟改为最终状态
     * @return 获取下一阶段的 单据状态
     */
    private static SgTransferBillStatusEnum getInStatus(SgTransferBillStatusEnum preBillStatus, boolean isTotal) {
        switch (preBillStatus) {
            case AUDITED_PART_OUT_NOT_IN:
            case AUDITED_PART_OUT_PART_IN:
                return SgTransferBillStatusEnum.AUDITED_PART_OUT_PART_IN;
            case AUDITED_ALL_OUT_NOT_IN:
            case AUDITED_ALL_OUT_PART_IN:
                if (isTotal) {
                    return SgTransferBillStatusEnum.AUDITED_ALL_OUT_ALL_IN;
                } else {
                    return SgTransferBillStatusEnum.AUDITED_ALL_OUT_PART_IN;
                }
            case AUDITED_ALL_OUT_ALL_IN:
            case AUDITED_NOT_OUT:
            case VOIDED:
            case UN_AUDITED:
                return null;
            default:
                return null;
        }
    }
}
