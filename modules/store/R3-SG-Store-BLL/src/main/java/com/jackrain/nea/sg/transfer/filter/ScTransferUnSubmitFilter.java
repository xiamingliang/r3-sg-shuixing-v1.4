package com.jackrain.nea.sg.transfer.filter;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.es.util.ElasticSearchUtil;
import com.jackrain.nea.sg.transfer.common.ScTransferConstants;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferItemMapper;
import com.jackrain.nea.sg.transfer.model.table.ScBTransferItem;
import com.jackrain.nea.sg.transfer.services.ScTransferStorageService;
import com.jackrain.nea.tableService.MainTableRecord;
import com.jackrain.nea.tableService.TableServiceContext;
import com.jackrain.nea.tableService.filter.BaseFilter;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.utils.AssertUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Locale;

/**
 * @author csy
 * 调拨单 取消提交filter
 */
@Component
@Slf4j
@Deprecated
public class ScTransferUnSubmitFilter extends BaseFilter {


    @Override
    public void before(TableServiceContext context, MainTableRecord row) {

        Locale locale = context.getLocale();
        JSONObject origJo = row.getMainData().getOrignalData();
        AssertUtils.notNull(origJo, "当前记录已不存在！", locale);
        Integer pickStatus = origJo.getInteger("PICK_STATUS");
        Integer outStatus = origJo.getInteger("OUT_STATUS");
        AssertUtils.notNull(pickStatus, "数据错误拣货状态不存在!", locale);
        AssertUtils.notNull(outStatus, "数据错误状态不存在!", locale);

        AssertUtils.cannot(pickStatus == 2, "当前记录拣货中，不允许取消提交，请联系仓库人员将出库单取消拣货！", locale);
        AssertUtils.cannot(outStatus == 2, "当前记录已出库，不允许取消提交！", locale);
    }

    @Override
    public void after(TableServiceContext context, MainTableRecord row) {
        Locale locale = context.getLocale();


        JSONObject whereKeys = new JSONObject();
        whereKeys.put("BILL_NO", row.getOrignalData().getString("BILL_NO"));
        String[] queryFDs = new String[]{"ID"};
        JSONObject esResult = ElasticSearchUtil.search(ScTransferConstants.OUT_INDEX, ScTransferConstants.OUT_TYPE, whereKeys, null, null, 1, 0, queryFDs);
        log.debug("es query result" + esResult.toJSONString());
        Long outId = null;

        if (esResult.getJSONArray("data") != null && esResult.getJSONArray("data").size() > 0) {
            outId = esResult.getJSONArray("data").getJSONObject(0).getLong("ID");
        } else if (esResult.getIntValue("code") == ResultCode.FAIL) {
            AssertUtils.logAndThrow("es search fail:" + esResult.getString("message"), context.getLocale());
        }

//        if (outId != null) {
//            ScOutDeleteCmd outDeleteCmd = (ScOutDeleteCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
//                    ScOutDeleteCmd.class.getName(), "sc", "1.0");
//            ScOutDeleteCmdRequest request = new ScOutDeleteCmdRequest();
//            request.setId(outId);
//            request.setLoginUser(context.getUser());
//            ValueHolderV14 holder = outDeleteCmd.delRow(request);
//            AssertUtils.notNull(holder, "删除出库单 返回为空", locale);
//            AssertUtils.cannot(holder.getCode() == ResultCode.FAIL, "删除出库单失败：" + holder.getMessage(), locale);
//        }


        //获取 明细
        ScBTransferItemMapper itemMapper = ApplicationContextHandle.getBean(ScBTransferItemMapper.class);
        QueryWrapper<ScBTransferItem> itemQuery = new QueryWrapper<>();
        itemQuery.eq("SC_B_TRANSFER_ID", row.getId());
        List<ScBTransferItem> items = itemMapper.selectList(itemQuery);

        //调用库存
        ScTransferStorageService storageService = ApplicationContextHandle.getBean(ScTransferStorageService.class);
        storageService.transferUnSubmitStorageChange(items, row, context);
    }


}
