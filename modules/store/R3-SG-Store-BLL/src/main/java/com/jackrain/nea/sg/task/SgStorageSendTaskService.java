package com.jackrain.nea.sg.task;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.send.api.SgSendCmd;
import com.jackrain.nea.sg.send.common.SgSendConstants;
import com.jackrain.nea.sg.send.model.request.SgSendBillVoidRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;


/**
 * @author 舒威
 * @since 2019/10/9
 * create at : 2019/10/9 18:45
 */
@Slf4j
@Component
public class SgStorageSendTaskService {

    /**
     * 逻辑单相关服务
     */
    public ValueHolderV14 sendTaskService(JSONObject params) {
        ValueHolderV14 result = new ValueHolderV14(ResultCode.SUCCESS, "success");
        if (params != null && StringUtils.isNotEmpty(params.getString("action"))) {
            String action = params.getString("action");
            String request = params.getString("request");
            switch (action) {
                case SgSendConstants.ACTION_VOID:
                    voidSend(request);
                    break;
                default:
                    log.debug(action + "暂未开放相应服务!");
            }
        } else {
            result.setCode(ResultCode.FAIL);
            result.setMessage("参数不能为空!");
        }
        return result;
    }

    public void voidSend(String request) {
        SgSendCmd sendCmd = (SgSendCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                SgSendCmd.class.getName(), SgConstantsIF.GROUP, SgConstantsIF.VERSION);
        SgSendBillVoidRequest model = JSONObject.parseObject(request, SgSendBillVoidRequest.class);
        sendCmd.voidSgBSend(model);
    }

}
