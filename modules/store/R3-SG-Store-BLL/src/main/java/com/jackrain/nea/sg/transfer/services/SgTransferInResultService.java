package com.jackrain.nea.sg.transfer.services;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Lists;
import com.jackrain.nea.ac.sc.basic.model.enums.SourceBillType;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.core.db.Tools;
import com.jackrain.nea.cpext.model.table.CpCustomer;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.model.util.ModelUtil;
import com.jackrain.nea.oc.sale.common.OcSaleConstantsIF;
import com.jackrain.nea.oc.sale.model.request.OcSaleBillSaveRequest;
import com.jackrain.nea.oc.sale.model.request.OcSaleImpItemSaveRequest;
import com.jackrain.nea.oc.sale.model.request.OcSaleSaveRequest;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.config.SgStorageBoxConfig;
import com.jackrain.nea.sg.basic.model.result.SgR3BaseResult;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.basic.utils.StorageUtils;
import com.jackrain.nea.sg.in.api.SgPhyInResultSelectCmd;
import com.jackrain.nea.sg.in.model.request.*;
import com.jackrain.nea.sg.transfer.common.ScTransferConstants;
import com.jackrain.nea.sg.transfer.common.SgTransferConstantsIF;
import com.jackrain.nea.sg.transfer.enums.SgTransferBillStatusEnum;
import com.jackrain.nea.sg.transfer.erp.SgTransferInSynErp;
import com.jackrain.nea.sg.transfer.mapper.*;
import com.jackrain.nea.sg.transfer.model.request.*;
import com.jackrain.nea.sg.transfer.model.table.*;
import com.jackrain.nea.sg.transfer.utils.SgTransferBillGeneraUtil;
import com.jackrain.nea.sg.transfer.utils.SgTransferBillStatusUtil;
import com.jackrain.nea.sg.transfer.utils.SgTransferBillUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.utils.AssertUtils;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author csy
 * Date: 2019/5/9
 * Description: 调拨单-入库单回写
 */
@Slf4j
@Component
public class SgTransferInResultService {

    @Autowired
    private ScBTransferBoxFunctionService boxFunctionService;

    @Autowired
    private SgStorageBoxConfig boxConfig;

    @Autowired
    private SgTransferBillUtils billUtils;


    public ValueHolderV14 writeBackInResult(SgInResultMQRequest requestMQ) {
        if (log.isDebugEnabled()) {
            log.debug("入库单参数：" + requestMQ);
        }
        User loginUser = requestMQ.getLoginUser();
        AssertUtils.notNull(loginUser, "用户信息不能为空！");
        Locale locale = loginUser.getLocale();

        Long inResultId = requestMQ.getId();
        AssertUtils.notNull(inResultId, "入库结果单ID不能为空！", locale);
        //mq入参中是否一键入库标志
        Boolean isOneClickLibrary = requestMQ.getIsOneClickLibrary();
        if (isOneClickLibrary == null) {
            isOneClickLibrary = false;
        }
        List<Long> ids = new ArrayList<>();
        ids.add(inResultId);
        SgPhyInResultSelectCmd inResultSelectCmd = (SgPhyInResultSelectCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(),
                SgPhyInResultSelectCmd.class.getName(),
                SgConstantsIF.GROUP, SgConstantsIF.VERSION);
        ValueHolderV14<List<SgPhyInResultBillSaveRequest>> holderV14 = inResultSelectCmd.querySgPhyInResultMQBody(ids, loginUser);
        log.debug(this.getClass().getName() + ",SgTransferInResultService:" + JSON.toJSONString(holderV14));
        if (holderV14.getCode() == ResultCode.FAIL) {
            AssertUtils.logAndThrow("入库结果单查询失败！");
        }

        List<SgPhyInResultBillSaveRequest> data = holderV14.getData();
        if (CollectionUtils.isEmpty(data)) {
            AssertUtils.logAndThrow("入库结果单查询为空！");
        }
        SgTransferInResultService service = ApplicationContextHandle.getBean(SgTransferInResultService.class);
        return service.writeBackInResult(msgBodyToRequestModel(data.get(0)), isOneClickLibrary, false);
    }

    /**
     * 回写出结果
     */
    @Transactional(rollbackFor = Exception.class)
    public ValueHolderV14 writeBackInResult(SgTransferInWBRequest request, Boolean isOneClickLibrary, Boolean isErp) {
        if (log.isDebugEnabled()) {
            log.debug("start SgTransferInResultService.writeBackInResult:" + JSON.toJSONString(request));
        }
        AssertUtils.notNull(request, "请求参数为空!");
        User user = request.getLoginUser();
        AssertUtils.notNull(user, "user is null,login first!");
        Locale locale = user.getLocale();
        List<SgTransferInWBItemRequest> requestItems = request.getItems();
        Long objId = request.getObjId();
        Long inBillId = request.getInId();
        String inBillNo = request.getInBillNo();
        AssertUtils.notNull(objId, "参数为空-objId", locale);
//        AssertUtils.notNull(inBillId, "参数为空-入库结果单id", locale);
//        AssertUtils.cannot(StringUtils.isEmpty(inBillNo), "参数为空-入库结果单单据编号", locale);
        AssertUtils.notNull(CollectionUtils.isEmpty(requestItems), "明细不能为空!", locale);


        if (inBillId != null && StringUtils.isNotEmpty(inBillNo)) {
            // 新增入库结果单明细
            ScBTransferInItemMapper inItemMapper = ApplicationContextHandle.getBean(ScBTransferInItemMapper.class);
            int total = inItemMapper.selectCount(new QueryWrapper<ScBTransferInItem>().lambda()
                    .eq(ScBTransferInItem::getIsactive, SgConstants.IS_ACTIVE_Y)
                    .eq(ScBTransferInItem::getSgBPhyInResultBillNo, inBillNo)
                    .eq(ScBTransferInItem::getSgBPhyInResultId, inBillId));
            AssertUtils.cannot(total > 0, "已经消费过的入库结果单", locale);
            ScBTransferInItem inItem = new ScBTransferInItem();
            inItem.setId(ModelUtil.getSequence("SC_B_TRANSFER_IN_ITEM"));
            StorageUtils.setBModelDefalutData(inItem, user);
            inItem.setModifierename(user.getEname());
            inItem.setSgBPhyInResultBillNo(inBillNo);
            inItem.setSgBPhyInResultId(inBillId);
            inItem.setScBTransferId(objId);
            inItemMapper.insert(inItem);
        }

        ScBTransferMapper mapper = ApplicationContextHandle.getBean(ScBTransferMapper.class);
        ScBTransfer transfer = mapper.selectById(objId);
        statusCheck(transfer, locale);
        ScBTransferItemMapper itemMapper = ApplicationContextHandle.getBean(ScBTransferItemMapper.class);
        List<ScBTransferItem> transferItems = new ArrayList<>();

        // 2020-04-07添加逻辑：串码入库
        List<SgTransferInWBItemRequest> codeStringItem = Lists.newArrayList();
        for (SgTransferInWBItemRequest requestItem : requestItems) {
            itemParamCheck(requestItem, locale);
            ScBTransferItem origItem = itemMapper.selectOne(new QueryWrapper<ScBTransferItem>().lambda()
                    .eq(ScBTransferItem::getPsCSkuEcode, requestItem.getPsCSkuEcode())
                    .eq(ScBTransferItem::getScBTransferId, objId));

            // 2020-04-07新增逻辑：调拨单串码入库
            if (origItem == null) {
                codeStringItem.add(requestItem);
                continue;
            }

            ScBTransferItem updateItem = new ScBTransferItem();
            StorageESUtils.setBModelDefalutDataByUpdate(updateItem, user);
            updateItem.setQtyIn(requestItem.getQtyIn().add(origItem.getQtyIn()));
            updateItem.setQtyDiff(origItem.getQtyOut().subtract(updateItem.getQtyIn()));
            BigDecimal priceList = origItem.getPriceList();
            if (priceList == null) {
                priceList = BigDecimal.ZERO;
            }
            updateItem.setId(origItem.getId());
            updateItem.setAmtInList(updateItem.getQtyIn().multiply(priceList));

            // 入库改调拨价格
            BigDecimal transferPrice = requestItem.getTransferPrice();
            if (transferPrice != null) {
                updateItem.setPriceOrder(transferPrice);
                updateItem.setAmtOrder(origItem.getQty().multiply(transferPrice));
            }
            itemMapper.updateById(updateItem);

            origItem.setQtyIn(requestItem.getQtyIn());
            transferItems.add(origItem);
        }

        // 箱功能开启时，更新录入明细出库数量
        if (boxConfig.getBoxEnable()) {
            boxFunctionService.updateTransferImpForOut(user, objId, request.getImpItemRequests(),
                    ScTransferConstants.ACTION_IN);
        }

        // 2020-04-07新增逻辑：调拨单串码入库新增
        if (CollectionUtils.isNotEmpty(codeStringItem)) {

            // 获取系统参数是否串码入库，如果否则报错
            AssertUtils.isTrue(SgTransferBillUtils.getCodeStringSystemParam(), "不允许串码入库，请检查系统参数！", locale);
            billUtils.codeStringItemInsert(user, objId, codeStringItem);
        }

        SgTransferBillStatusEnum preStatus = SgTransferBillStatusEnum.getTransferBillStatus(transfer.getStatus());
        if (preStatus == null) {
            throw new NDSException(Resources.getMessage("当前单据状态不存在!", locale));
        }
        //单据状态 是否可以进行到最后一步
        HashMap<String, BigDecimal> qtySumMap = itemMapper.selectsAllQtySum(objId);
        BigDecimal totQtyOut = qtySumMap.get("qty_out");
        BigDecimal totQtyIn = qtySumMap.get("qty_in");
        BigDecimal amtInList = qtySumMap.get("amt_in_list");
        Boolean isTotalEq = false;
        if (preStatus.equals(SgTransferBillStatusEnum.AUDITED_ALL_OUT_NOT_IN) || preStatus.equals(SgTransferBillStatusEnum.AUDITED_ALL_OUT_PART_IN)) {

            // 2020-04-22添加逻辑：erp过来的单据=入库完成
            if (!isErp) {
                //是否总数相等 【出库数量】=【入库数量】
                isTotalEq = totQtyOut.equals(totQtyIn);
                if (!isTotalEq) {
                    SgTransferBillGeneraUtil generaUtil = ApplicationContextHandle.getBean(SgTransferBillGeneraUtil.class);
                    isTotalEq = generaUtil.compareInResult(user, transfer);
                }
            } else {
                isTotalEq = true;
            }
        }


        ScBTransfer updateTransfer = new ScBTransfer();
        ScBTransfer scBTransfer = new ScBTransfer();
        ScBTransfer scBTransferNew = new ScBTransfer();
        try {
            updateTransfer.setId(transfer.getId());
            updateTransfer = StorageESUtils.setBModelDefalutDataByUpdate(updateTransfer, user);
            updateTransfer.setModifierename(user.getEname());
            updateTransfer.setTotQtyIn(totQtyIn);
            updateTransfer.setTotQtyDiff(totQtyOut.subtract(totQtyIn));
            updateTransfer.setTotAmtInList(amtInList);
            int statusIntVal = SgTransferBillStatusUtil.getInBillStatusIntVal(isTotalEq, preStatus, locale);
            updateTransfer.setStatus(statusIntVal);

            // 更新入库拣货状态=拣货完成
            if (statusIntVal == SgTransferBillStatusEnum.AUDITED_ALL_OUT_ALL_IN.getVal()) {
                updateTransfer.setPickStatus(ScTransferConstants.PICK_STATUS_PICKED);
            }
            updateTransfer.setBillNo(transfer.getBillNo());
            updateTransfer.setRequestBillNo(transfer.getRequestBillNo());
            // 根据【单据状态】，进行赋值
            invokeRequireOrder(updateTransfer, statusIntVal, user);

            updateTransfer.setInDate(new Date());
            updateTransfer.setInTime(request.getInTime());
            updateTransfer.setInerId(user.getId().longValue());
            updateTransfer.setInerEname(user.getEname());
            updateTransfer.setInerName(user.getName());

            mapper.updateById(updateTransfer);

            // 新增残次品明细
            insertTransferDefectItem(request.getDefectItems());
            scBTransfer = mapper.selectById(objId);
            List<ScBTransferItem> transferItemList = itemMapper.selectList(new QueryWrapper<ScBTransferItem>().lambda()
                    .eq(ScBTransferItem::getScBTransferId, objId));

            // 本单的【单据状态】=出库完成入库完成，并且本单单据类型不是差异调拨不是发货方调整，则生成调拨差异单
            if (scBTransfer.getStatus() == SgTransferBillStatusEnum.AUDITED_ALL_OUT_ALL_IN.getVal()) {
                Integer transferType = scBTransfer.getTransferType();
                String handleWay = scBTransfer.getHandleWay();
                if (!(transferType == SgTransferConstantsIF.TRANSFER_TYPE_DIFF && StringUtils.isNotEmpty(handleWay) &&
                        StringUtils.equals(handleWay, SgTransferConstantsIF.HANDLE_WAY_FHTZ))) {
                    insertTransferDiff(user, scBTransfer, transferItemList);
                }
            }

            //推送ES
            String index = ScTransferConstants.TRANSFER_INDEX;
            String parentKey = ScTransferConstants.TRANSFER_PARENT_FIELD;

            // 箱功能开启时，推送录入明细到es
            if (boxConfig.getBoxEnable()) {
                ScBTransferImpItemMapper impMapper = ApplicationContextHandle.getBean(ScBTransferImpItemMapper.class);
                List<ScBTransferImpItem> impItems = impMapper.selectList(
                        new QueryWrapper<ScBTransferImpItem>().lambda()
                                .eq(ScBTransferImpItem::getScBTransferId, objId));
                String type = SgConstants.SC_B_TRANSFER_IMP_ITEM;
                StorageESUtils.pushESBModelByUpdate(scBTransfer, impItems, scBTransfer.getId(), null,
                        index, index, type, parentKey, ScBTransfer.class, ScBTransferImpItem.class, false);
            } else {
                String type = ScTransferConstants.TRANSFER_TYPE;
                StorageESUtils.pushESBModelByUpdate(scBTransfer, transferItemList, scBTransfer.getId(), null, index,
                        index, type, parentKey, ScBTransfer.class, ScBTransferItem.class, false);
            }

            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + ",调拨入库服务调拨性质:" + transfer.getSgBTransferPropEname());
            }

            // 2020-03-31添加逻辑：调拨出库单出库新增逻辑
            List<ScBTransferItem> itemList = transferItemList.stream().filter(item ->
                    item.getQtyIn().compareTo(BigDecimal.ZERO) > 0).collect(Collectors.toList());
            if (SgTransferConstantsIF.TRANSFER_PROP_TJ_STR.equals(transfer.getSgBTransferPropEname())) {
                if (CollectionUtils.isNotEmpty(itemList)) {
                    // 1.生成多角单据（销售单）
                    createSaleForTransferIn(user, transfer, itemList);
                    // 2.通知erp
                    SgTransferInSynErp synErp = ApplicationContextHandle.getBean(SgTransferInSynErp.class);
                    synErp.synErpTransferIn(user.getLocale(), scBTransfer, itemList);
                }
            } else if (SgTransferConstantsIF.TRANSFER_PROP_TH_STR.equals(transfer.getSgBTransferPropEname())) {

                // 调拨性质=退货调拨：回写退货申请单入库数量和单据状态
                billUtils.updateRefundApply(user, scBTransfer, itemList, ScTransferConstants.ACTION_IN);
            }

            // 2019-07-18添加逻辑：生成结算日志
            if (statusIntVal == SgTransferBillStatusEnum.AUDITED_ALL_OUT_ALL_IN.getVal()) {
                List<ScBTransferItem> itemsLog = itemMapper.selectList(new QueryWrapper<ScBTransferItem>().lambda()
                        .eq(ScBTransferItem::getScBTransferId, objId));
                SgTransferBillGeneraUtil generaUtil = ApplicationContextHandle.getBean(SgTransferBillGeneraUtil.class);
                generaUtil.createSettleLogForTransfer(user, scBTransfer, itemsLog, SourceBillType.TRANSFER_IN, false, false);
            }

            //如果一键入库标志为true，那么更新采购单一键入库为入库完成
            if (isOneClickLibrary) {
                scBTransferNew.setId(scBTransfer.getId());
                scBTransferNew.setReserveBigint02(ScTransferConstants.TYPE_LIBRARY_STATUS_SUCCESS_INSTRAGE);
                if (mapper.updateById(scBTransferNew) > 0) {
                    if (log.isDebugEnabled()) {
                        log.debug("一键入库入库完成");
                    }
                }
            }
        } catch (Exception e) {
            if (isOneClickLibrary) {
                scBTransferNew.setId(scBTransfer.getId());
                scBTransferNew.setReserveBigint02(ScTransferConstants.TYPE_LIBRARY_STATUS_FAIL_INSTRAGE);
                mapper.updateById(scBTransferNew);
            }
            e.printStackTrace();
        }
        return new ValueHolderV14(ResultCode.SUCCESS, SgConstants.MESSAGE_STATUS_SUCCESS);
    }


    /**
     * 单据状态校验
     *
     * @param transfer 调拨单信息
     */

    private void statusCheck(ScBTransfer transfer, Locale locale) {
/* 	判断传入的【调拨单ID】对应的调拨单的【单据状态】=未审核，则结束服务；
 	判断传入的【调拨单ID】对应的调拨单的【单据状态】=已作废，则结束服务；
 	判断传入的【调拨单ID】对应的调拨单的【单据状态】=出库完成入库完成，则结束服务；
 	判断传入的【最后一次入库】=Y、且传入的【调拨单ID】对应的调拨单的【单据状态】=部分出库未入库，则结束服务；（PS. 最后一次入库=Y、但并不是出库完成，不OK）
 	判断传入的【最后一次入库】=Y、且传入的【调拨单ID】对应的调拨单的【单据状态】=部分出库部分入库，则结束服务；（PS. 最后一次入库=Y、但并不是出库完成，不OK）* */
        AssertUtils.notNull(transfer, "当前单据已不存在!", locale);
        Integer billStatus = transfer.getStatus();
        AssertUtils.notNull(billStatus, "数据异常-STATUS不存在!", locale);
        AssertUtils.cannot(billStatus == SgTransferBillStatusEnum.VOIDED.getVal(), "当前单据已作废!", locale);
        AssertUtils.cannot(billStatus == SgTransferBillStatusEnum.UN_AUDITED.getVal(), "当前单据未审核!", locale);
    }


    /**
     * 明细校验
     */
    private void itemParamCheck(SgTransferInWBItemRequest itemRequest, Locale locale) {
        AssertUtils.cannot(StringUtils.isEmpty(itemRequest.getPsCSkuEcode()), "明细参数存在空字段!", locale);
        AssertUtils.notNull(itemRequest.getQtyIn(), "入库数量不能为空!", locale);
    }

    /**
     * 新增残次品明细
     *
     * @param defectItemRequests 入参
     */
    public void insertTransferDefectItem(List<ScBTransferDefectItemRequest> defectItemRequests) {

        if (!CollectionUtils.isEmpty(defectItemRequests)) {
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + ",insertTransferDefectItem,残次品明细数量:" +
                        defectItemRequests.size() + ",残次品明细:" + JSONObject.toJSONString(defectItemRequests));
            }

            List<ScBTransferDefectItem> defectItemList = new ArrayList<>();
            defectItemRequests.forEach(defectItemRequest -> {
                ScBTransferDefectItem defectItem = new ScBTransferDefectItem();
                BeanUtils.copyProperties(defectItemRequest, defectItem);
                Long defectId = Tools.getSequence(SgConstants.SC_B_TRANSFER_DEFECT_ITEM);
                defectItem.setId(defectId);
                defectItemList.add(defectItem);
            });

            ScBTransferDefectItemMapper defectItemMapper = ApplicationContextHandle
                    .getBean(ScBTransferDefectItemMapper.class);
            List<List<ScBTransferDefectItem>> baseModelPageList = StorageUtils.getBaseModelPageList(
                    defectItemList, SgConstants.SG_COMMON_INSERT_PAGE_SIZE);

            int result = 0;
            for (List<ScBTransferDefectItem> pageList : baseModelPageList) {

                if (CollectionUtils.isEmpty(pageList)) {
                    continue;
                }
                int count = defectItemMapper.batchInsert(pageList);
                if (count != pageList.size()) {
                    log.error(this.getClass().getName() + ",调拨单残次品批量新增失败！");
                }
                result += count;
            }
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + ",调拨单残次品批量新增条数:" + result);
            }
        }
    }

    /**
     * 生成调拨差异单
     *
     * @param user     用户信息
     * @param transfer 调拨单信息
     * @param itemList 明细
     */
    public void insertTransferDiff(User user, ScBTransfer transfer, List<ScBTransferItem> itemList) {

        List<ScBTransferItem> addDiffItems = new ArrayList<>();
        if (!CollectionUtils.isEmpty(itemList)) {
            itemList.forEach(item -> {
                BigDecimal qtyIn = item.getQtyIn();
                BigDecimal qtyOut = item.getQtyOut();
                if (qtyIn != null && qtyOut != null && qtyIn.compareTo(qtyOut) != 0) {
                    addDiffItems.add(item);
                }
            });
        }

        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ",insertTransferDiff,存在差异的调拨明细数量:" + addDiffItems.size());
        }
        if (addDiffItems.size() > 0) {
            ScBTransferDiffBillRequest request = new ScBTransferDiffBillRequest();
            // 主表信息
            ScBTransferDiffRequest diffRequest = new ScBTransferDiffRequest();
            BeanUtils.copyProperties(transfer, diffRequest);
            // 单据编号、单据日期
            diffRequest.setBillDate(new Date());
            // 发货店仓
            diffRequest.setCpCStoreIdSend(transfer.getCpCOrigId());
            diffRequest.setCpCStoreEcodeSend(transfer.getCpCOrigEcode());
            diffRequest.setCpCStoreEnameSend(transfer.getCpCOrigEname());

            // 收货店仓
            diffRequest.setCpCStoreId(transfer.getCpCDestId());
            diffRequest.setCpCStoreEcode(transfer.getCpCDestEcode());
            diffRequest.setCpCStoreEname(transfer.getCpCDestEname());
            // 出入库日期
            diffRequest.setOutTime(transfer.getOutDate());
            diffRequest.setInTime(transfer.getInDate());
            // 单据状态、备注、批量处理默认否
            diffRequest.setBillStatus(SgTransferConstantsIF.BILL_STATUS_WAIT);
            diffRequest.setIsBatch(SgTransferConstantsIF.IS_BATCH_N);
            // 总差异数量
            diffRequest.setTotQty(transfer.getTotQtyDiff());
            request.setTransferDiffRequest(diffRequest);

            // 明细信息
            List<ScBTransferDiffItemRequest> itemRequests = new ArrayList<>();
            addDiffItems.forEach(item -> {
                ScBTransferDiffItemRequest itemRequest = new ScBTransferDiffItemRequest();
                BeanUtils.copyProperties(item, itemRequest);
                itemRequest.setHandleStatus(SgTransferConstantsIF.HANDLE_STATUS_N);
                //生成调拨差异单时，处理方式默认为【发货方调整】
                itemRequest.setHandleWay(SgTransferConstantsIF.HANDLE_WAY_FHTZ);
                itemRequests.add(itemRequest);
            });
            request.setTransferDiffItemRequests(itemRequests);
            request.setObjId(-1L);
            request.setLoginUser(user);

            ScTransferDiffService diffService = ApplicationContextHandle.getBean(ScTransferDiffService.class);
            ValueHolderV14<SgR3BaseResult> v14 = diffService.saveTransferDiff(request);
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + ",insertTransferDiff,v14:" + JSON.toJSONString(v14));
            }
            if (v14.getCode() == ResultCode.FAIL) {
                AssertUtils.logAndThrow("生成调拨差异单异常:" + v14.getMessage(), user.getLocale());
            }
        }
    }

    private SgTransferInWBRequest msgBodyToRequestModel(SgPhyInResultBillSaveRequest sendMsgRequest) {
        SgTransferInWBRequest wbRequest = new SgTransferInWBRequest();
        SgBPhyInResultSaveRequest inResultSaveRequest = sendMsgRequest.getSgBPhyInResultSaveRequest();
        wbRequest.setBillNo(inResultSaveRequest.getSourceBillNo());
        wbRequest.setObjId(inResultSaveRequest.getSourceBillId());
        wbRequest.setInId(inResultSaveRequest.getId());
        wbRequest.setInBillNo(inResultSaveRequest.getBillNo());
        wbRequest.setInBillType(inResultSaveRequest.getSourceBillType().longValue());
        wbRequest.setIsLastIn(inResultSaveRequest.getIsLast());
        wbRequest.setInTime(inResultSaveRequest.getInTime());
        List<SgTransferInWBItemRequest> inItemRequest = new ArrayList<>();

        List<SgBPhyInResultItemSaveRequest> inResultItemSaveRequests = sendMsgRequest.getItemList();
        for (SgBPhyInResultItemSaveRequest inResultItemSaveRequest : inResultItemSaveRequests) {
            SgTransferInWBItemRequest itemRequest = new SgTransferInWBItemRequest();
            BeanUtils.copyProperties(inResultItemSaveRequest, itemRequest);
            itemRequest.setQtyIn(inResultItemSaveRequest.getQtyIn());
            inItemRequest.add(itemRequest);
        }

        // 箱功能开启时，需要回写录入明细
        if (boxConfig.getBoxEnable()) {
            if (CollectionUtils.isNotEmpty(sendMsgRequest.getImpItemList())) {
                List<SgTransferImpItemRequest> impItemRequests = Lists.newArrayList();
                for (SgPhyInResultImpItemSaveRequest resultImpItem : sendMsgRequest.getImpItemList()) {
                    SgTransferImpItemRequest impItemRequest = new SgTransferImpItemRequest();
                    BeanUtils.copyProperties(resultImpItem, impItemRequest);
                    impItemRequests.add(impItemRequest);
                }
                wbRequest.setImpItemRequests(impItemRequests);
            }
        }

        long userId = sendMsgRequest.getLoginUser().getId().longValue();
        String eName = sendMsgRequest.getLoginUser().getEname();
        String name = sendMsgRequest.getLoginUser().getName();
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        Long sourceBillId = inResultSaveRequest.getSourceBillId();
        // 残次品明细
        List<SgBPhyInResultDefectItemSaveRequest> defectItemList = sendMsgRequest.getDefectItemList();
        if (!org.springframework.util.CollectionUtils.isEmpty(defectItemList)) {
            List<ScBTransferDefectItemRequest> defectItemRequests = new ArrayList<>();
            defectItemList.forEach(defectItemSaveRequest -> {
                ScBTransferDefectItemRequest defectItemRequest = new ScBTransferDefectItemRequest();
                BeanUtils.copyProperties(defectItemSaveRequest, defectItemRequest);
                defectItemRequest.setScBTransferId(sourceBillId);
                defectItemRequest.setSgBPhyInResultId(defectItemSaveRequest.getSgBPhyInResultId());
                defectItemRequest.setSgBPhyInResultBillNo(defectItemSaveRequest.getSgBPhyInResultBillNo());

                defectItemRequest.setOwnerid(userId);
                defectItemRequest.setOwnerename(eName);
                defectItemRequest.setOwnername(name);
                defectItemRequest.setCreationdate(timestamp);
                defectItemRequest.setModifierid(userId);
                defectItemRequest.setModifierename(eName);
                defectItemRequest.setModifiername(name);
                defectItemRequest.setModifieddate(timestamp);
                defectItemRequests.add(defectItemRequest);
            });
            wbRequest.setDefectItems(defectItemRequests);
        }
        wbRequest.setItems(inItemRequest);
        wbRequest.setLoginUser(sendMsgRequest.getLoginUser());
        return wbRequest;
    }


    /**
     * 根据单据状态判断，同步要货申请单
     *
     * @param updateTransfer
     * @param statusIntVal
     * @param user
     */
    public void invokeRequireOrder(ScBTransfer updateTransfer, int statusIntVal, User user) {

        if (SgTransferBillStatusEnum.AUDITED_ALL_OUT_ALL_IN.getVal() == statusIntVal) {
            // 【入库拣货状态】=拣货完成，
            updateTransfer.setPickStatus(ScTransferConstants.PICK_STATUS_PICKED);
            String requestBillNo = updateTransfer.getRequestBillNo();
            if (!StringUtils.isEmpty(requestBillNo)) {
                //2020.04.10注释该逻辑   暂不回写要货申请
//                OcRequireOrderSearchCmd updateCmd = (OcRequireOrderSearchCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
//                        OcRequireOrderSearchCmd.class.getName(), "oc", "1.0");
//                OcRequireOrderSearchRequest requireOrderSearchRequest = new OcRequireOrderSearchRequest();
//                requireOrderSearchRequest.setBillNo(requestBillNo);
//                requireOrderSearchRequest.setUser(user);
//                requireOrderSearchRequest.setStatus(RequireOrderBillStatuSaveMainTableEnum.CONDITION4.getIndex());
//                updateCmd.execute(requireOrderSearchRequest);
            }
        }
        if (SgTransferBillStatusEnum.AUDITED_ALL_OUT_PART_IN.getVal() == statusIntVal) {
            // 【入库拣货状态】=拣货中，
            updateTransfer.setPickStatus(ScTransferConstants.PICK_STATUS_PICKING);
            String requestBillNo = updateTransfer.getRequestBillNo();
            if (!StringUtils.isEmpty(requestBillNo)) {
                //2020.04.10注释该逻辑   暂不回写要货申请
//                OcRequireOrderSearchCmd updateCmd = (OcRequireOrderSearchCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
//                        OcRequireOrderSearchCmd.class.getName(), "oc", "1.0");
//                OcRequireOrderSearchRequest requireOrderSearchRequest = new OcRequireOrderSearchRequest();
//                requireOrderSearchRequest.setBillNo(requestBillNo);
//                requireOrderSearchRequest.setUser(user);
//                requireOrderSearchRequest.setStatus(RequireOrderBillStatuSaveMainTableEnum.CONDITION3.getIndex());
//                updateCmd.execute(requireOrderSearchRequest);
            }
        }
    }

    enum RequireOrderBillStatuSaveMainTableEnum {

        CONDITION1(2, "单据状态已审核未出库"),
        CONDITION2(3, "单据状态已作废"),
        CONDITION3(4, "出库完成部分入库"),
        CONDITION4(5, "出库完成入库完成"),
        CONDITION5(6, "单据状态已作废");

        private String name;
        private int index;

        private RequireOrderBillStatuSaveMainTableEnum(int index, String name) {
            this.name = name;
            this.index = index;
        }

        public static String getName(int index) {
            for (RequireOrderBillStatuSaveMainTableEnum c : RequireOrderBillStatuSaveMainTableEnum.values()) {

                if (c.getIndex() == index) {
                    return c.name;
                }
            }
            return null;
        }

        public int getIndex() {
            return index;
        }
    }


    /**
     * 调拨出库单-出库服务增加逻辑
     *
     * @param user     用户信息
     * @param transfer 调拨单
     * @param itemList 调拨单明细
     */
    private boolean createSaleForTransferIn(User user, ScBTransfer transfer, List<ScBTransferItem> itemList) {

        try {

            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + ",createSaleForTransferIn:" + JSON.toJSONString(transfer));
            }
            // 检查经营类型和对应ERP账套、门店经销商品牌，返回中转仓
            Locale locale = user.getLocale();
            CpCustomer customer = billUtils.checkStoreAndCustomer(locale, transfer.getCpCOrigId(), transfer.getCpCDestId(),
                    ScTransferConstants.ACTION_IN);

            ValueHolderV14 holderV14 = new ValueHolderV14(ResultCode.SUCCESS, "success");
            if (customer != null) {

                // 生成一张出库完成入库完成的销售单（不影响库存）
                OcSaleBillSaveRequest request = new OcSaleBillSaveRequest();
                request.setObjId(-1L);
                request.setLoginUser(user);
                // 主表信息
                OcSaleSaveRequest saleRequest = new OcSaleSaveRequest();
                // 收发货店仓
                saleRequest.setCpCOrigId(customer.getCpCStoreId());
                saleRequest.setCpCOrigEcode(customer.getCpCStoreEcode());
                saleRequest.setCpCOrigEname(customer.getCpCStoreEname());
                saleRequest.setCpCDestId(transfer.getCpCDestId());
                saleRequest.setCpCDestEcode(transfer.getCpCDestEcode());
                saleRequest.setCpCDestEname(transfer.getCpCDestEname());

                // 单据日期、出入库日期、销售类型
                Date sysDate = new Date();
                saleRequest.setBillDate(sysDate);
                saleRequest.setOutDate(sysDate);
                saleRequest.setInDate(sysDate);
                saleRequest.setSaleType(OcSaleConstantsIF.SALE_TYPE_NOR);

                // 出否影响库存（否），是否自动出入库（是）
                saleRequest.setIsOutReduce(ScTransferConstants.IS_AUTO_N);
                saleRequest.setIsInIncrease(ScTransferConstants.IS_AUTO_N);
                saleRequest.setIsAutoOut(ScTransferConstants.IS_AUTO_Y);
                saleRequest.setIsAutoIn(ScTransferConstants.IS_AUTO_Y);
                // 备注
                saleRequest.setRemark("由调拨单入库[" + transfer.getBillNo() + "]多角产生");
                request.setSaleSaveRequest(saleRequest);

                // 明细信息
                List<OcSaleImpItemSaveRequest> itemListRequest = Lists.newArrayList();
                itemList.forEach(item -> {
                    OcSaleImpItemSaveRequest itemImp = new OcSaleImpItemSaveRequest();
                    BeanUtils.copyProperties(item, itemImp);
                    itemImp.setId(-1L);
                    itemListRequest.add(itemImp);
                });
                request.setImpItemSaveRequests(itemListRequest);

                if (log.isDebugEnabled()) {
                    log.debug(this.getClass().getName() + ",调拨出库单入库生成销售单入参:" + JSON.toJSONString(request));
                }
                SgTransferRpcService rpcService = ApplicationContextHandle.getBean(SgTransferRpcService.class);
                holderV14 = rpcService.saleSaveAndAudit(request);
                if (log.isDebugEnabled()) {
                    log.debug(this.getClass().getName() + ",调拨出库单入库生成销售单出参:" + JSON.toJSONString(holderV14));
                }
            }
            return holderV14.isOK();

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error(this.getClass().getName() + ",调拨出库单入库失败:" + StorageUtils.getExceptionMsg(e));
            }
            return false;
        }
    }


    /**
     * 退货调拨入库erp调中台
     *
     * @param request 入参
     * @return v14
     */
    public ValueHolderV14 writeBackInResultForErp(SgTransferInWBRequest request) {

        User loginUser = request.getLoginUser();
        String billNo = request.getBillNo();
        AssertUtils.cannot(StringUtils.isEmpty(billNo), "调拨单单号为空！", loginUser.getLocale());

        SgTransferQueryService queryService = ApplicationContextHandle.getBean(SgTransferQueryService.class);
        ScBTransfer transfer = queryService.queryTransferByBillNoAndStatus(billNo, SgTransferBillStatusEnum.AUDITED_ALL_OUT_NOT_IN.getVal());
        AssertUtils.notNull(transfer, "不存在出库完成未入库的调拨单" + billNo, loginUser.getLocale());
        request.setObjId(transfer.getId());

        ValueHolderV14 holderV14 = new ValueHolderV14(ResultCode.FAIL, "fail");
        try {
            holderV14 = this.writeBackInResult(request, false, true);
        } catch (Exception e) {
            holderV14.setMessage(e.getMessage());
        }
        return holderV14;
    }
}
