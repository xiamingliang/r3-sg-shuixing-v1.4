package com.jackrain.nea.sg.receive.common;

/**
 * @author 舒威
 * @since 2019/4/19
 * create at : 2019/4/19 16:35
 */
public interface SgReceiveConstants {

    /**
     * 主键
     */
    String ID = "id";

    /**
     * 条码id
     */
    String PS_C_SKU_ID = "ps_c_sku_id";

    /**
     * 逻辑仓id
     */
    String CP_C_STORE_ID = "cp_c_store_id";

    /**
     * 是否启用
     */
    String IS_ACTIVE = "isactive";

    /**
     * 序号生成器-逻辑收货单
     */
    String SEQ_RECEIVE = "SQE_SG_B_RECEIVE";

    /**
     * 表名
     */
    String BILL_RECEIVE = "sg_b_receive";
    String BILL_RECEIVE_ITEM = "sg_b_receive_item";
    String BILL_RECEIVE_ID = "sg_b_receive_id";

    /**
     * 来源单据信息
     */
    String SOURCE_BILL_ID = "source_bill_id";
    String SOURCE_BILL_NO = "source_bill_no";
    String SOURCE_BILL_TYPE = "source_bill_type";
    String SOURCE_BILL_ITEM_ID = "source_bill_item_id";

}
