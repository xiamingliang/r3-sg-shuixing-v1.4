package com.jackrain.nea.sg.adjust.services;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.adjust.model.request.SgAdjustBillSaveRequest;
import com.jackrain.nea.sg.basic.common.R3ParamConstants;
import com.jackrain.nea.sg.basic.model.result.SgR3BaseResult;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Locale;


/**
 * @author csy
 */

@Slf4j
@Component
public class SgAdjSaveAndAuditService {

    @Transactional(rollbackFor = Exception.class)
    public ValueHolderV14 saveAndAudit(SgAdjustBillSaveRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("Start SgAdjSaveAndAuditService.saveAndAudit. ReceiveParams:request:{};", JSONObject.toJSONString(request));
        }

        if (request == null || request.getLoginUser() == null) {
            throw new NDSException(Resources.getMessage("user is null,login first!"));
        }

        Long startTime = System.currentTimeMillis();
        User user = request.getLoginUser();
        Locale locale = user.getLocale();
        SgAdjSaveService saveService = ApplicationContextHandle.getBean(SgAdjSaveService.class);
        ValueHolderV14<SgR3BaseResult> saveResult = saveService.saveSgBAdjust(request, false);
        AssertUtils.notNull(saveResult, "调整单保存返回结果为空!", locale);
        AssertUtils.cannot(saveResult.getCode() == ResultCode.FAIL, "调整单新增失败-" + saveResult.getMessage(), locale);
        AssertUtils.cannot(saveResult.getData() == null ||
                        saveResult.getData().getDataJo() == null || saveResult.getData().getDataJo().getLong(R3ParamConstants.OBJID) == null
                , "调整单返回结果有误-无objId返回", locale);
        Long objId = saveResult.getData().getDataJo().getLong(R3ParamConstants.OBJID);
        AssertUtils.cannot(objId < 0, "调整单返回结果有误-objId:" + objId, locale);
        SgAdjAuditService adjAuditService = ApplicationContextHandle.getBean(SgAdjAuditService.class);
        ValueHolder auditResult = adjAuditService.audit(objId, user);
        AssertUtils.notNull(auditResult, "调整单审核返回结果为空!", locale);
        AssertUtils.cannot((Integer) auditResult.get("code") == ResultCode.FAIL, "调整单审核失败-" + saveResult.getMessage(), locale);
        
        if (log.isDebugEnabled()) {
            log.debug("Finish SgAdjSaveAndAuditService.saveAndAudit. spend time:{};", System.currentTimeMillis() - startTime);
        }
        return new ValueHolderV14(ResultCode.SUCCESS, "逻辑调整单新增审核成功");
    }
}
