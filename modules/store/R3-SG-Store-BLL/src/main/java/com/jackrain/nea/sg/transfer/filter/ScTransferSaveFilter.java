package com.jackrain.nea.sg.transfer.filter;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.cpext.api.CpLogisticsSelectServiceCmd;
import com.jackrain.nea.cpext.model.table.CpLogistics;
import com.jackrain.nea.model.util.AdParamUtil;
import com.jackrain.nea.redis.config.CusRedisTemplate;
import com.jackrain.nea.redis.util.RedisOpsUtil;
import com.jackrain.nea.sg.basic.common.R3ParamConstants;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.config.SgStorageBoxConfig;
import com.jackrain.nea.sg.basic.mapper.CpCPhyWarehouseMapper;
import com.jackrain.nea.sg.basic.model.table.CpCPhyWarehouse;
import com.jackrain.nea.sg.transfer.common.ScTransferConstants;
import com.jackrain.nea.sg.transfer.common.SgTransferConstantsIF;
import com.jackrain.nea.sg.transfer.services.ScBTransferBoxFunctionService;
import com.jackrain.nea.tableService.*;
import com.jackrain.nea.tableService.constants.Constants;
import com.jackrain.nea.tableService.filter.BaseFilter;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.utils.AssertUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * @author csy
 * 调拨单 明细数量默认给0
 */
@Slf4j
@Component
public class ScTransferSaveFilter extends BaseFilter {

    @Autowired
    private SgStorageBoxConfig boxConfig;

    /**
     * 上锁
     *
     * @param param 参数
     */
    @Override
    public void firstAct(JSONObject param) {
        Long objId = param.getLong(R3ParamConstants.OBJID);
        if (objId > 0) {
            CusRedisTemplate<String, String> redisTemplate = RedisOpsUtil.getStrRedisTemplate();
            String lockKsy = SgConstants.SG_B_TRANSFER + ":" + objId;
            Boolean blnCanInit = redisTemplate.opsForValue().setIfAbsent(lockKsy, "OK");
            if (blnCanInit != null && blnCanInit) {
                redisTemplate.expire(lockKsy, 60, TimeUnit.SECONDS);
            } else {
                AssertUtils.logAndThrow("当前单据操作中，请稍后重试...");
            }
        }
    }

    /**
     * 解锁
     *
     * @param param 参数
     */
    @Override
    public void finalAct(JSONObject param) {
        Long objId = param.getLong(R3ParamConstants.OBJID);
        if (objId > 0) {
            CusRedisTemplate<String, String> redisTemplate = RedisOpsUtil.getStrRedisTemplate();
            String lockKsy = SgConstants.SG_B_TRANSFER + ":" + objId;
            if (redisTemplate.opsForValue().get(lockKsy) != null) {
                redisTemplate.delete(lockKsy);
            }
        }
    }

    @Override
    public void before(TableServiceContext context, MainTableRecord row) {
        if (row.getAction().equals(DbRowAction.INSERT)) {
            //主表状态 初始化
            JSONObject commitData = row.getCommitData();
            if (StringUtils.isEmpty(commitData.getString("IS_OUT_REDUCE"))) {
                commitData.put("IS_OUT_REDUCE", SgConstants.IS_ACTIVE_Y);
            }
            if (StringUtils.isEmpty(commitData.getString("IS_IN_INCREASE"))) {
                commitData.put("IS_IN_INCREASE", SgConstants.IS_ACTIVE_Y);
            }
            commitData.put(Constants.STATUS, Constants.STATUS_UNFINISHED);
            commitData.put(ScTransferConstants.PICK_STATUS, ScTransferConstants.PICK_STATUS_UNPICKED);
            commitData.put(ScTransferConstants.IN_STATUS, Constants.STATUS_UNFINISHED);
            commitData.put(ScTransferConstants.OUT_STATUS, Constants.STATUS_UNFINISHED);
            if (commitData.getDate("BILL_DATE") == null) {
                commitData.put("BILL_DATE", new Date());
            }

            // 调拨出库单逻辑：调拨性质=同级调拨；发货类型=自提
            String tableName = row.getTable().getName();
            if (ScTransferConstants.SC_B_TRANSFER_OUT.equals(tableName)) {
                commitData.put("SEND_TYPE", SgTransferConstantsIF.SEND_TYPE_ZT);
                commitData.put("SG_B_TRANSFER_PROP_ID", SgTransferConstantsIF.TRANSFER_PROP_TJ);
                commitData.put("SG_B_TRANSFER_PROP_ENAME", SgTransferConstantsIF.TRANSFER_PROP_TJ_STR);
            } else if (ScTransferConstants.SC_B_TRANSFER_OUT_DIRECTLY.equals(tableName)) {

                // 2020-05-13添加逻辑：调拨性质=直接调拨时，1.自动出入库=是；2.发货类型=自提；3.调拨类型=正常调拨
                commitData.put("SEND_TYPE", SgTransferConstantsIF.SEND_TYPE_ZT);
                commitData.put("SG_B_TRANSFER_PROP_ID", SgTransferConstantsIF.TRANSFER_PROP_ZJ);
                commitData.put("SG_B_TRANSFER_PROP_ENAME", SgTransferConstantsIF.TRANSFER_PROP_ZJ_STR);
                commitData.put("IS_AUTO_OUT", ScTransferConstants.IS_AUTO_Y);
                commitData.put("IS_AUTO_IN", ScTransferConstants.IS_AUTO_Y);
                commitData.put("TRANSFER_TYPE", SgTransferConstantsIF.TRANSFER_TYPE_NORMAL);
            }

            // 水星新增字段默认值
            suppDefaultQty(commitData);

            // 收货人相关信息
            fillWareHouseInfo(context, row);
        }
        //物流信息补充
        fillCpLogisticsInfo(context, row);

        // 箱功能开启时，操作录入明细
        if (boxConfig.getBoxEnable()) {

            ScBTransferBoxFunctionService bean = ApplicationContextHandle.getBean(ScBTransferBoxFunctionService.class);
            bean.saveTransferImpItem(row);

        } else {
            //明细 状态是save
            for (SubTableRecord subTableRecord : row.getSubTables().values()) {
                for (RowRecord rowRecord : subTableRecord.getRows()) {
                    JSONObject commitData = rowRecord.getCommitData();
                    if (commitData.getBigDecimal("QTY") == null) {
                        rowRecord.getCommitData().put("QTY", BigDecimal.ONE);
                    }
                    rowRecord.getCommitData().put("QTY_OUT", BigDecimal.ZERO);
                    rowRecord.getCommitData().put("QTY_IN", BigDecimal.ZERO);
                    rowRecord.getCommitData().put("QTY_DIFF", BigDecimal.ZERO);
                    if (rowRecord.getCommitData().containsKey("ID") && rowRecord.getCommitData().getLongValue("ID") == -1) {
                        rowRecord.getCommitData().put("ISACTIVE", SgConstants.IS_ACTIVE_Y);
                        rowRecord.getCommitData().put("CREATIONDATE", new Date());
                    }
                }
            }
        }
    }


    /**
     * 初始收货人相关信息
     */
    private void fillWareHouseInfo(TableServiceContext context, MainTableRecord row) {
        Locale locale = context.getLocale();
        JSONObject commitData = row.getMainData().getCommitData();
        log.debug("fillWareHouseInfo,commitData:" + commitData);
        String sendType = commitData.getString("SEND_TYPE");
        Long destStoreId = commitData.getLong("CP_C_DEST_ID");

        AssertUtils.notNull(destStoreId, "收货店仓不能为空!", locale);
        CpCPhyWarehouseMapper warehouseMapper = ApplicationContextHandle.getBean(CpCPhyWarehouseMapper.class);
        CpCPhyWarehouse warehouse = warehouseMapper.selectByStoreId(destStoreId);
        AssertUtils.notNull(warehouse, "收货店仓实体仓不存在!", locale);
        
        //水星-zhanghang-2019-12-13-start
        Long oriStoreId = commitData.getLong("CP_C_ORIG_ID");
        CpCPhyWarehouse origWarehouse = warehouseMapper.selectByStoreId(oriStoreId);
        AssertUtils.notNull(origWarehouse, "发货店仓实体仓不存在!", locale);
        //水星-zhanghang-2019-12-13-end
        if (StringUtils.isNotEmpty(sendType) && SgTransferConstantsIF.SEND_TYPE_ZT.equalsIgnoreCase(sendType)) {


            if (!commitData.containsKey("RECEIVER_PROVINCE_ID")) {
                commitData.put("RECEIVER_PROVINCE_ID", warehouse.getSellerProvinceId());
            }
            if (!commitData.containsKey("RECEIVER_CITY_ID")) {
                commitData.put("RECEIVER_CITY_ID", warehouse.getSellerCityId());
            }
            if (!commitData.containsKey("RECEIVER_DISTRICT_ID")) {
                commitData.put("RECEIVER_DISTRICT_ID", warehouse.getSellerAreaId());
            }
            if (!commitData.containsKey("RECEIVER_ADDRESS")) {
                commitData.put("RECEIVER_ADDRESS", warehouse.getSendAddress());
            }
            if (!commitData.containsKey("RECEIVER_NAME")) {
                commitData.put("RECEIVER_NAME", warehouse.getContactName());
            }
            if (!commitData.containsKey("RECEIVER_MOBILE")) {
                commitData.put("RECEIVER_MOBILE", warehouse.getMobilephoneNum());
            }
            if (!commitData.containsKey("RECEIVER_PHONE")) {
                commitData.put("RECEIVER_PHONE", warehouse.getPhoneNum());
            }
            if (!commitData.containsKey("RECEIVER_ZIP")) {
                commitData.put("RECEIVER_ZIP", warehouse.getSellerZip());
            }
        }
    }

    /**
     * 初始物流公司信息
     */
    private void fillCpLogisticsInfo(TableServiceContext context, MainTableRecord row) {
        JSONObject commitData = row.getMainData().getCommitData();
        Long logisticsId = commitData.getLong("CP_C_LOGISTICS_ID");
        String sendType = commitData.getString("SEND_TYPE");

        // 只修改物流公司的时候，发货类型不传 取调拨单中发货类型
        if (StringUtils.isEmpty(sendType)) {
            sendType = row.getMainData().getOrignalData().getString("SEND_TYPE");
        }
        CpLogisticsSelectServiceCmd logisticsSelectServiceCmd = (CpLogisticsSelectServiceCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(),
                CpLogisticsSelectServiceCmd.class.getName(),
                "cp-ext", "1.0");

        if (StringUtils.isNotEmpty(sendType) && SgTransferConstantsIF.SEND_TYPE_ZT.equalsIgnoreCase(sendType)) {
            String logisticsIdCode = AdParamUtil.getParam(ScTransferConstants.SYSTEM_TOB);
            AssertUtils.cannot(StringUtils.isEmpty(logisticsIdCode), "默认物流公司系统参数读取失败！", context.getLocale());
            CpLogistics cpLogistics = logisticsSelectServiceCmd.queryCpLogisticByEcode(logisticsIdCode);
            AssertUtils.notNull(cpLogistics, String.format("系统中不存在默认物流公司：%s", logisticsIdCode), context.getLocale());
            commitData.put("CP_C_LOGISTICS_ID", cpLogistics.getId());
            commitData.put("CP_C_LOGISTICS_ENAME", cpLogistics.getEname());
            commitData.put("CP_C_LOGISTICS_ECODE", cpLogistics.getEcode());
        } else {
            if (logisticsId != null && logisticsId > 0) {
                String cpCLogisticsECode = commitData.getString("CP_C_LOGISTICS_ECODE");
                if (StringUtils.isEmpty(cpCLogisticsECode)) {
                    CpLogistics cpLogistics = logisticsSelectServiceCmd.checkCpLogistic(logisticsId);
                    AssertUtils.notNull(cpLogistics, "物流公司不存在!", context.getLocale());
                    commitData.put("CP_C_LOGISTICS_ECODE", cpLogistics.getEcode());
                    commitData.put("CP_C_LOGISTICS_ENAME", cpLogistics.getEname());
                }
            }
        }
    }

    /**
     * 水星新增字段默认值
     *
     * @param commitData 数据
     */
    private void suppDefaultQty(JSONObject commitData) {

        // 水星新增字段默认值

        // 箱装数量
        if (commitData.getBigDecimal("QTY_TEUS_LOAD") == null) {
            commitData.put("QTY_TEUS_LOAD", BigDecimal.ZERO);
        }
        // 标准箱数量
        if (commitData.getBigDecimal("QTY_TEUS_STANDARD") == null) {
            commitData.put("QTY_TEUS_STANDARD", BigDecimal.ZERO);
        }
        // 包装数量
        if (commitData.getBigDecimal("QTY_PACK") == null) {
            commitData.put("QTY_PACK", BigDecimal.ZERO);
        }
        // 加大包数
        if (commitData.getBigDecimal("QTY_ENLARGE") == null) {
            commitData.put("QTY_ENLARGE", BigDecimal.ZERO);
        }
        // 附件数量
        if (commitData.getBigDecimal("QTY_ANNEX") == null) {
            commitData.put("QTY_ANNEX", BigDecimal.ZERO);
        }
        // 中包数量
        if (commitData.getBigDecimal("QTY_MEDIUM") == null) {
            commitData.put("QTY_MEDIUM", BigDecimal.ZERO);
        }
        // 床垫箱数量
        if (commitData.getBigDecimal("QTY_TEUS_MATTRESS") == null) {
            commitData.put("QTY_TEUS_MATTRESS", BigDecimal.ZERO);
        }
        // 床垫加大箱数量
        if (commitData.getBigDecimal("QTY_TEUS_MATTRESS_ENLARGE") == null) {
            commitData.put("QTY_TEUS_MATTRESS_ENLARGE", BigDecimal.ZERO);
        }
    }

}
