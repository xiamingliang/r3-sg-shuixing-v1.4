package com.jackrain.nea.sg.send.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.google.common.collect.Maps;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.model.result.SgStorageUpdateResult;
import com.jackrain.nea.sg.basic.model.table.CpCStore;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.send.mapper.SgBSendItemMapper;
import com.jackrain.nea.sg.send.mapper.SgBSendMapper;
import com.jackrain.nea.sg.send.model.request.SgSendBillSaveRequest;
import com.jackrain.nea.sg.send.model.result.SgSendBillSaveResult;
import com.jackrain.nea.sg.send.model.table.SgBSend;
import com.jackrain.nea.sg.send.model.table.SgBSendItem;
import com.jackrain.nea.sg.send.utils.SgStoreUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

/**
 * @author 舒威
 * @since 2019/6/10
 * create at : 2019/6/10 20:39
 */
@Slf4j
@Component
public class SgSendSaveCancelService {

    @Autowired
    private SgBSendMapper sendMapper;

    @Autowired
    private SgBSendItemMapper sendItemMapper;

    @Autowired
    private SgSendStorageService storageService;

    @Autowired
    private SgSendSaveService sendSaveService;

    @Transactional(rollbackFor = Exception.class)
    public ValueHolderV14<SgSendBillSaveResult> cancelSaveSgBSend(SgSendBillSaveRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("来源单据id[" + request.getSgSend().getSourceBillId()
                    + "],逻辑发货单保存补偿服务入参:" + JSONObject.toJSONString(request) + ";");
        }

        sendSaveService.checkParams(request);
        ValueHolderV14<SgSendBillSaveResult> v14 = new ValueHolderV14();
        v14.setCode(ResultCode.SUCCESS);
        v14.setMessage("success");

        try {
            SgBSend send = sendMapper.selectOne(new QueryWrapper<SgBSend>().lambda()
                    .eq(SgBSend::getSourceBillId, request.getSgSend().getSourceBillId())
                    .eq(SgBSend::getSourceBillType, request.getSgSend().getSourceBillType())
                    .eq(SgBSend::getIsactive, SgConstants.IS_ACTIVE_Y));
            if (send == null) {
                v14.setCode(ResultCode.FAIL);
                v14.setMessage("当前记录已不存在!");
                return v14;
            }

            sendItemMapper.delete(new UpdateWrapper<SgBSendItem>().lambda().eq(SgBSendItem::getSgBSendId, send.getId()));
            sendMapper.delete(new UpdateWrapper<SgBSend>().lambda().eq(SgBSend::getId, send.getId()));

            request.setIsCancel(true);
            request.setUpdateMethod(SgConstantsIF.ITEM_UPDATE_TYPE_INC);
            Map<Long, CpCStore> negativeStock = SgStoreUtils.getNegativeStock(request, null, null, null);
            ValueHolderV14<SgStorageUpdateResult> storageResult = storageService.updateSendStoragePreout(send, null, request,
                    null, null, request.getLoginUser(), false, negativeStock, false, false,
                    null, null, null, null, null, Maps.newHashMap());
            if (storageResult.isOK()) {
                //分销单据 -> check同步库存占用结果
                SgStoreUtils.isSuccess("逻辑发货单保存补偿失败!", storageResult);
                SgSendBillSaveResult data = new SgSendBillSaveResult();
                data.setPreoutUpdateResult(storageResult.getData().getPreoutUpdateResult());
                v14.setData(data);
            } else {
                v14.setCode(ResultCode.FAIL);
                v14.setMessage(storageResult.getMessage());
            }
        } catch (Exception e) {
            e.printStackTrace();
            AssertUtils.logAndThrowExtra("逻辑发货单保存补偿服务异常!", e, request.getLoginUser().getLocale());
        }
        return v14;
    }
}
