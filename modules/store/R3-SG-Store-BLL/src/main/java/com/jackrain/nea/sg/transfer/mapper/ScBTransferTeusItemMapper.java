package com.jackrain.nea.sg.transfer.mapper;

import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.transfer.model.table.ScBTransferTeusItem;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ScBTransferTeusItemMapper extends ExtentionMapper<ScBTransferTeusItem> {

}