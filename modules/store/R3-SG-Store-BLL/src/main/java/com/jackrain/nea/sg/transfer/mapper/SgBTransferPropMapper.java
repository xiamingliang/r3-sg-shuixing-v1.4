package com.jackrain.nea.sg.transfer.mapper;

import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.transfer.model.table.ScBTransfer;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface SgBTransferPropMapper extends ExtentionMapper<ScBTransfer> {

    @Select("SELECT COUNT(*) FROM SG_B_TRANSFER_PROP WHERE ENAME = #{ename}")
    int selectTransferPropByEname(@Param("ename")String ename);
}