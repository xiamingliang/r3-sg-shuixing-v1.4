package com.jackrain.nea.sg.transfer.services;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.sg.basic.common.R3ParamConstants;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.model.result.SgR3BaseResult;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.transfer.model.request.ScTransferItemDelRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.tableService.service.TableServiceCmdService;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.query.DefaultWebEvent;
import com.jackrain.nea.web.query.QuerySession;
import com.jackrain.nea.web.query.QuerySessionImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;

/**
 * @author leexh
 * @since 2019/10/21 16:53
 * desc:
 */
@Slf4j
@Component
public class SgTransferImpDelService {

    /**
     * 删除
     *
     * @param request 入参
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public ValueHolderV14<SgR3BaseResult> delTransferImpItem(ScTransferItemDelRequest request) {
        AssertUtils.notNull(request, "请求参数不能为空!");
        User user = request.getLoginUser();
        AssertUtils.notNull(user, "用户未登录");

        ValueHolderV14<SgR3BaseResult> holderV14 = new ValueHolderV14<>();
        TableServiceCmdService service = ApplicationContextHandle.getBean(TableServiceCmdService.class);
        QuerySession session = new QuerySessionImpl(user);
        DefaultWebEvent event = new DefaultWebEvent("dodelete", new HashMap(16));

        JSONObject param = new JSONObject();
        Long objId = request.getObjId();
        AssertUtils.notNull(objId, "objId不能为空！", user.getLocale());
        List<Long> itemIds = request.getItemIds();
        if (CollectionUtils.isEmpty(itemIds)) {
            AssertUtils.logAndThrow("明细id不能为空！", user.getLocale());
        }


        JSONObject impItem = new JSONObject();
        impItem.put(SgConstants.SC_B_TRANSFER_IMP_ITEM.toUpperCase(), itemIds);

        param.put(R3ParamConstants.TABITEM, impItem);
        param.put(R3ParamConstants.OBJID, objId);
        param.put(R3ParamConstants.TABLE, SgConstants.SG_B_TRANSFER.toUpperCase());
        event.put(R3ParamConstants.PARAM, param);
        session.setEvent(event);
        ValueHolder holder = service.execute(session);

        AssertUtils.notNull(holder, "调拨单录入明细删除返回为空!", user.getLocale());
        AssertUtils.cannot(holder.get("code") == null || (Integer) holder.get("code") == -1,
                "调拨单录入明细删除失败:" + holder.get("message"), user.getLocale());
        holderV14.setCode((Integer) holder.get(R3ParamConstants.CODE));
        holderV14.setMessage((String) holder.get(R3ParamConstants.MESSAGE));
        return holderV14;
    }
}
