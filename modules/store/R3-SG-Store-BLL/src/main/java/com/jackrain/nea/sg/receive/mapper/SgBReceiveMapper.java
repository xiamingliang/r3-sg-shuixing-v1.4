package com.jackrain.nea.sg.receive.mapper;

import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.receive.model.table.SgBReceive;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface SgBReceiveMapper extends ExtentionMapper<SgBReceive> {

    /**
     * 分页查询逻辑收货单
     */
    @Select("SELECT * FROM " + SgConstants.SG_B_RECEIVE + " ORDER BY ID LIMIT #{page} OFFSET #{offset}")
    List<SgBReceive> selectListByPage(@Param("page") int page, @Param("offset") int offset);
}