package com.jackrain.nea.sg.assign.validate;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.sg.assign.common.SgAssignConstantsIF;
import com.jackrain.nea.tableService.MainTableRecord;
import com.jackrain.nea.tableService.TableServiceContext;
import com.jackrain.nea.tableService.validate.BaseValidator;
import com.jackrain.nea.utils.AssertUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author leexh
 * @since 2019/12/3 10:49
 * desc:
 */
@Slf4j
@Component
public class SgAssignVoidValidate  extends BaseValidator {

    @Override
    public void validate(TableServiceContext context, MainTableRecord mainTableRecord) {

        JSONObject origData = mainTableRecord.getOrignalData();
        Integer status = origData.getInteger("STATUS");
        AssertUtils.cannot(status == SgAssignConstantsIF.ASSIGN_STATUS_AUDIT, "当前记录已审核，不允许作废！", context.getLocale());
        AssertUtils.cannot(status == SgAssignConstantsIF.ASSIGN_STATUS_VOID, "当前记录已作废，不允许重复作废！", context.getLocale());
    }
}
