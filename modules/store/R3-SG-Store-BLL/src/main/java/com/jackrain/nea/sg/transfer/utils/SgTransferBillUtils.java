package com.jackrain.nea.sg.transfer.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.google.common.collect.Lists;
import com.jackrain.nea.cpext.model.table.CpCStore;
import com.jackrain.nea.cpext.model.table.CpCustomer;
import com.jackrain.nea.data.basic.model.request.CpcStoreWrapper;
import com.jackrain.nea.data.basic.model.request.SkuInfoQueryRequest;
import com.jackrain.nea.data.basic.services.BasicCpQueryService;
import com.jackrain.nea.data.basic.services.BasicPsQueryService;
import com.jackrain.nea.model.util.AdParamUtil;
import com.jackrain.nea.model.util.ModelUtil;
import com.jackrain.nea.oc.sx.model.request.OcRefundApplyOrderRequest;
import com.jackrain.nea.oc.sx.model.request.table.OcBRefundApply;
import com.jackrain.nea.oc.sx.model.request.table.OcBRefundApplyItem;
import com.jackrain.nea.ps.api.result.PsCProSkuResult;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.basic.utils.StorageUtils;
import com.jackrain.nea.sg.transfer.common.ScTransferConstants;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferImpItemMapper;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferItemMapper;
import com.jackrain.nea.sg.transfer.model.request.SgTransferInWBItemRequest;
import com.jackrain.nea.sg.transfer.model.table.ScBTransfer;
import com.jackrain.nea.sg.transfer.model.table.ScBTransferImpItem;
import com.jackrain.nea.sg.transfer.model.table.ScBTransferItem;
import com.jackrain.nea.sg.transfer.services.SgTransferRpcService;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.utils.AssertUtils;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author leexh
 * @since 2020/3/31 11:20
 * desc: 调拨单工具类
 */
@Slf4j
@Component
public class SgTransferBillUtils {

    @Autowired
    private SgTransferRpcService rpcService;

    /**
     * 检查经营类型和对应ERP账套
     *
     * @param locale 用户信息
     * @param origId 发货店仓
     * @param destId 收货店仓
     * @param action 出/入库
     * @return 门店
     */
    private CpCStore checkStore(Locale locale, Long origId, Long destId, String action) {

        // 根据收发货店仓查询【经营类型】和【对应ERP账套】
        AssertUtils.cannot(origId == null || destId == null, "收发货店仓为空,请检查！", locale);

        BasicCpQueryService psQueryService = ApplicationContextHandle.getBean(BasicCpQueryService.class);
        CpcStoreWrapper origStore = psQueryService.findCpcStore(origId);
        CpcStoreWrapper destStore = psQueryService.findCpcStore(destId);
        if (origStore == null || destStore == null) {
            AssertUtils.logAndThrow("收发货店仓查询为空,请检查！", locale);
        }

        // 获取收发货店仓的【经营类型】、【对应ERP账套】
        String origManageTypeCode = origStore.getManageTypeCode();
        String destManageTypeCode = destStore.getManageTypeCode();

        if (StringUtils.isEmpty(origManageTypeCode) || StringUtils.isEmpty(destManageTypeCode)) {
            AssertUtils.logAndThrow("收发货店仓经营类型查询为空,请检查！", locale);
        }

        AssertUtils.cannot(!(ScTransferConstants.MANAGE_TYPE_CODE_DIRECTLY.equals(origManageTypeCode) && StringUtils.equals(origManageTypeCode, destManageTypeCode)),
                "收发货店仓经营类型不是集团直营，请检查！" + origManageTypeCode + "," + destManageTypeCode, locale);

        CpCStore storeOut = origStore.getCpCStore();
        CpCStore storeIn = destStore.getCpCStore();
        String origErpAccount = storeOut.getErpAccount();
        String destErpAccount = storeIn.getErpAccount();

        if (StringUtils.isEmpty(origErpAccount) || StringUtils.isEmpty(destErpAccount)) {
            AssertUtils.logAndThrow("收发货店仓对应ERP账套查询为空,请检查！", locale);
        }

        // 对应ERP账套相同时，不生成多角单据
//        AssertUtils.cannot(StringUtils.equals(origErpAccount, destErpAccount), "收发货店仓对应ERP账套相同，请检查！", locale);
        if (!StringUtils.equals(origErpAccount, destErpAccount)) {
            // 出库还是入库
            return ScTransferConstants.ACTION_OUT.equals(action) ? storeOut : storeIn;
        } else {
            return null;
        }

    }

    /**
     * 店仓在【店仓档案】上的“品牌”与经销商的上级经销商在【经销商档案】上的“品牌”进行匹配
     *
     * @param locale 用户信息
     * @param origId 发货店仓
     * @param destId 收货店仓
     * @param action 出/入库
     * @return 中转仓
     */
    public CpCustomer checkStoreAndCustomer(Locale locale, Long origId, Long destId, String action) {

        // 检查经营类型和对应ERP账套，返回门店信息
        CpCStore store = checkStore(locale, origId, destId, action);

        CpCustomer customerUpInfo = null;
        if (store != null) {
            // 获取门店品牌和经销商上级经销商的品牌
            String storeBrandIds = store.getPsCBrandIds();
            Long customerId = store.getCpCCustomerId();
            if (StringUtils.isEmpty(storeBrandIds) || customerId == null) {
                AssertUtils.logAndThrow(getDesc(action) + "店仓品牌/经销商为空，请检查！" + storeBrandIds + "===" + customerId, locale);
            }

            // 查询上级经销商/中转仓
            List<CpCustomer> customerList = rpcService.queryCustomerUpInfoById(customerId);
            if (CollectionUtils.isEmpty(customerList)) {
                AssertUtils.logAndThrow(getDesc(action) + "上级经销商查询为空，请检查！", locale);
            }

            // 判断发货店仓在【店仓档案】上的“品牌”与发货经销商的上级经销商在【经销商档案】上的“品牌”进行匹配
            StringBuilder cBrandIds = new StringBuilder();
            for (CpCustomer customerUp : customerList) {
                String customerBrandIds = customerUp.getPsCBrandIds();
                if (StringUtils.isNotEmpty(customerBrandIds) && customerBrandIds.contains(storeBrandIds)) {
                    customerUpInfo = customerUp;
                    break;
                } else {
                    cBrandIds.append(customerUp.getId() + ":" + customerBrandIds);
                }
            }
            if (customerUpInfo == null) {
                AssertUtils.logAndThrow(getDesc(action) + "店仓品牌和经销商品牌匹配失败！" +
                        cBrandIds + "<---->" + storeBrandIds, locale);

            }
            AssertUtils.notNull(customerUpInfo.getCpCStoreId(), "经销商上的中转仓为空，请检查！", locale);

        }
        return customerUpInfo;
    }


    private String getDesc(String action) {
        return ScTransferConstants.ACTION_OUT.equals(action) ? "发货" : "收货";
    }

    private String getDescOurOrIn(String action) {
        return ScTransferConstants.ACTION_OUT.equals(action) ? "出库" : "入库";
    }

    /**
     * 串码入库新增
     *
     * @param user     用户信息
     * @param requests 明细
     */
    @Transactional(rollbackFor = Exception.class)
    public void codeStringItemInsert(User user, Long objId, List<SgTransferInWBItemRequest> requests) {

        if (CollectionUtils.isNotEmpty(requests)) {

            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + ",串码入库新增入参:" + JSON.toJSONString(requests));
            }

            Map<String, SgTransferInWBItemRequest> skuMap = requests.stream().filter(request -> StringUtils.isNotEmpty(request.getPsCSkuEcode()))
                    .collect(Collectors.toMap(SgTransferInWBItemRequest::getPsCSkuEcode, Function.identity()));

            if (MapUtils.isNotEmpty(skuMap)) {

                // 录入明细、条码明细
                List<ScBTransferImpItem> impItemList = Lists.newArrayList();
                List<ScBTransferItem> itemList = Lists.newArrayList();

                List<String> skuCodes = Lists.newArrayList(skuMap.keySet());
                HashMap<String, PsCProSkuResult> skuProMap = getSkuProInfoBySkuCodes(skuCodes);
                if (MapUtils.isNotEmpty(skuProMap)) {

                    JSONArray errSku = new JSONArray();
                    for (Map.Entry<String, SgTransferInWBItemRequest> entry : skuMap.entrySet()) {

                        String skuCode = entry.getKey();
                        SgTransferInWBItemRequest itemRequest = entry.getValue();
                        if (itemRequest.getQtyIn() == null) {
                            errSku.add("串码入库入库数量为空：" + skuCode);
                            continue;
                        }

                        PsCProSkuResult skuResult = skuProMap.get(skuCode);
                        if (skuResult == null) {
                            errSku.add("串码入库商品查询为空：" + skuCode);
                            continue;
                        }

                        // 获取录入明细对象
                        ScBTransferImpItem transferImpItem = this.getTransferImpItem(user, objId, itemRequest, skuResult);
                        impItemList.add(transferImpItem);

                        // 获取条码明细对象
                        itemList.add(this.getTransferItem(transferImpItem));
                    }

                    // 批量新增录入明细和条码明细
                    this.batchInsertTransferItem(impItemList, itemList);

                    // erp串码入库，中台查询不到的sku收集打印
                    if (errSku.size() > 0) {
                        log.debug(this.getClass().getName() + ",调拨入库回写串码新增商品查询不存在的商品:" + JSON.toJSONString(errSku));
                    }

                } else {
                    if (log.isDebugEnabled()) {
                        log.debug(this.getClass().getName() + ",调拨入库回写串码新增商品查询为空:" + JSON.toJSONString(skuCodes));
                    }
                }
            }
        }
    }

    /**
     * 批量新增录入明细和条码明细
     *
     * @param impItemList 录入明细
     * @param itemList    条码明细
     */
    @Transactional(rollbackFor = Exception.class)
    public void batchInsertTransferItem(List<ScBTransferImpItem> impItemList, List<ScBTransferItem> itemList) {

        if (CollectionUtils.isNotEmpty(impItemList) && CollectionUtils.isNotEmpty(itemList)) {

            // 批量新增录入明细
            ScBTransferImpItemMapper impItemMapper = ApplicationContextHandle.getBean(ScBTransferImpItemMapper.class);
            int impItemCount = impItemMapper.batchInsert(impItemList);
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + ",调拨单串码入库录入明细新增结果:" + impItemCount);
            }

            // 批量新增条码明细
            ScBTransferItemMapper itemMapper = ApplicationContextHandle.getBean(ScBTransferItemMapper.class);
            int itemCount = itemMapper.batchInsert(itemList);
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + ",调拨单串码入库条码明细新增结果:" + itemCount);
            }
        }
    }


    /**
     * 组装录入明细对象
     *
     * @param user        用户信息
     * @param objId       调拨单id
     * @param itemRequest 入参
     * @param skuResult   商品信息
     * @return 录入明细对象
     */
    private ScBTransferImpItem getTransferImpItem(User user, Long objId, SgTransferInWBItemRequest itemRequest, PsCProSkuResult skuResult) {
        ScBTransferImpItem impItem = new ScBTransferImpItem();
        // 主键、外键
        impItem.setId(ModelUtil.getSequence(SgConstants.SC_B_TRANSFER_IMP_ITEM));
        impItem.setScBTransferId(objId);

        // 创建人、修改人
        StorageESUtils.setBModelDefalutData(impItem, user);
        impItem.setOwnerename(user.getEname());
        impItem.setModifierename(user.getEname());

        // 商品、条码信息
        impItem.setPsCProId(skuResult.getPsCProId());
        impItem.setPsCProEcode(skuResult.getPsCProEcode());
        impItem.setPsCProEname(skuResult.getPsCProEname());
        impItem.setPsCSkuId(skuResult.getId());
        impItem.setPsCSkuEcode(skuResult.getSkuEcode());

        // 颜色、尺寸、吊牌价、调拨价
        impItem.setPsCSpec1Id(skuResult.getPsCSpec1objId());
        impItem.setPsCSpec1Ecode(skuResult.getClrsEcode());
        impItem.setPsCSpec1Ename(skuResult.getClrsEname());
        impItem.setPsCSpec2Id(skuResult.getPsCSpec2objId());
        impItem.setPsCSpec2Ecode(skuResult.getSizesEcode());
        impItem.setPsCSpec2Ename(skuResult.getSizesEname());
        impItem.setPriceList(skuResult.getPricelist());
        impItem.setPriceOrder(itemRequest.getTransferPrice());

        // 基本单位
        impItem.setUnitId(skuResult.getBasicunit());
        impItem.setUnitEcode(skuResult.getBasicunitEcode());
        impItem.setUnitEname(skuResult.getBasicunitEname());

        // 调拨数量、出库数量、入库数量、差异数量（产品说调拨数量给0）
        BigDecimal zero = BigDecimal.ZERO;
        impItem.setQty(zero);
        impItem.setQtyOut(zero);
        impItem.setQtyIn(itemRequest.getQtyIn());
        impItem.setQtyDiff(itemRequest.getQtyIn().negate());

        return impItem;
    }

    /**
     * 组装条码明细对象
     *
     * @param impItem 录入明细
     * @return 条码明细对象
     */
    private ScBTransferItem getTransferItem(ScBTransferImpItem impItem) {

        ScBTransferItem item = new ScBTransferItem();
        BeanUtils.copyProperties(impItem, item);
        item.setId(ModelUtil.getSequence(SgConstants.SG_B_TRANSFER_ITEM));

        // 颜色、尺寸信息
        item.setPsCClrId(impItem.getPsCSpec1Id());
        item.setPsCClrEcode(impItem.getPsCSpec1Ecode());
        item.setPsCClrEname(impItem.getPsCSpec1Ename());
        item.setPsCSizeId(impItem.getPsCSpec2Id());
        item.setPsCSizeEcode(impItem.getPsCSpec2Ecode());
        item.setPsCSizeEname(impItem.getPsCSpec2Ename());

        // 吊牌金额、调拨金额、出库金额、入库金额
        BigDecimal zero = BigDecimal.ZERO;
        item.setAmtList(zero);
        item.setAmtOrder(zero);
        item.setAmtOutList(zero);
        item.setAmtInList(impItem.getPriceList().multiply(impItem.getQtyIn()));

        return item;
    }


    /**
     * 查询商品信息
     *
     * @param skuCodes 条码编码
     * @return sku信息
     */
    private HashMap<String, PsCProSkuResult> getSkuProInfoBySkuCodes(List<String> skuCodes) {

        HashMap<String, PsCProSkuResult> result = null;
        try {
            SkuInfoQueryRequest skuRequest = new SkuInfoQueryRequest();
            skuRequest.setSkuEcodeList(skuCodes);
            BasicPsQueryService psQueryService = ApplicationContextHandle.getBean(BasicPsQueryService.class);
            result = psQueryService.getSkuInfoByEcode(skuRequest);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error(this.getClass().getName() + ",串码入库商品查询异常:" + StorageUtils.getExceptionMsg(e));
            }
        }
        return result;
    }

    /**
     * 系统参数是否串码入库
     *
     * @return boolean
     */
    public static boolean getCodeStringSystemParam() {
        return Boolean.valueOf(AdParamUtil.getParam("r3.cm.enable"));
    }

    /**
     * 更新退货申请单出/入库数量和状态
     *
     * @param transfer 调拨单
     * @param itemList 条码明细
     */
    public void updateRefundApply(User user, ScBTransfer transfer, List<ScBTransferItem> itemList, String action) {
        try {

            if (CollectionUtils.isNotEmpty(itemList)) {
                OcRefundApplyOrderRequest request = new OcRefundApplyOrderRequest();
                request.setUser(user);

                // 主表单据编号和单据状态（和调拨单状态保持一致）
                OcBRefundApply refundApply = new OcBRefundApply();
                refundApply.setBillNo(transfer.getApplyBillNo());
                refundApply.setStatus(transfer.getStatus());
                request.setOcBRefundApply(refundApply);
                refundApply.setOutTime(transfer.getOutTime());

                // 条码明细数据
                List<OcBRefundApplyItem> applyItemList = Lists.newArrayList();
                itemList.forEach(item -> {
                    OcBRefundApplyItem applyItem = new OcBRefundApplyItem();
                    BeanUtils.copyProperties(item, applyItem);

                    // 出库时，入库数量置空，相反之
                    BigDecimal priceOrder = item.getPriceOrder() != null ? item.getPriceOrder() : BigDecimal.ZERO;
                    if (ScTransferConstants.ACTION_OUT.equals(action)) {
                        applyItem.setAmtOut(item.getQtyOut().multiply(priceOrder));
                        applyItem.setQtyIn(null);
                    } else if (ScTransferConstants.ACTION_IN.equals(action)) {
                        applyItem.setAmtIn(item.getQtyIn().multiply(priceOrder));
                        applyItem.setQtyOut(null);
                    }
                    applyItem.setPriceRefund(priceOrder);

                    applyItemList.add(applyItem);
                });
                request.setOcBRefundApplyItemList(applyItemList);

                if (log.isDebugEnabled()) {
                    log.debug(this.getClass().getName() + ",调拨" + this.getDescOurOrIn(action) + ",回写退货申请单入参:"
                            + JSON.toJSONString(request));
                }

                ValueHolderV14<OcRefundApplyOrderRequest> holderV14;
                if (ScTransferConstants.ACTION_OUT.equals(action)) {
                    holderV14 = rpcService.updateOutRefundApply(request);
                } else {
                    holderV14 = rpcService.updateInRefundApply(request);
                }
                if (log.isDebugEnabled()) {
                    log.debug(this.getClass().getName() + ",调拨" + this.getDescOurOrIn(action) + ",回写退货申请单出参:"
                            + JSON.toJSONString(holderV14));
                }
            }
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error(this.getClass().getName() + ",调拨" + this.getDescOurOrIn(action) + ",回写退货申请单异常:"
                        + StorageUtils.getExceptionMsg(e));
            }
        }
    }

}
