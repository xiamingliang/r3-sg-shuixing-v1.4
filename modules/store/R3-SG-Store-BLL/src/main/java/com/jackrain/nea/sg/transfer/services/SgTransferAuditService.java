package com.jackrain.nea.sg.transfer.services;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.ac.sc.basic.model.enums.FullLinkQueryEnum;
import com.jackrain.nea.ac.sc.basic.model.enums.SourceBillType;
import com.jackrain.nea.ac.sc.basic.model.request.AcFullLinkQueryRequest;
import com.jackrain.nea.ac.sc.basic.model.result.AcFullLinkQueryResult;
import com.jackrain.nea.ac.sc.core.api.AcFullLinkQueryCmd;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.cp.result.CpCStore;
import com.jackrain.nea.es.util.ElasticSearchUtil;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.model.util.AdParamUtil;
import com.jackrain.nea.oc.basic.cache.CommonCacheValUtils;
import com.jackrain.nea.redis.config.CusRedisTemplate;
import com.jackrain.nea.sg.basic.api.SgStorageQueryCmd;
import com.jackrain.nea.sg.basic.common.R3ParamConstants;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.config.SgStorageBoxConfig;
import com.jackrain.nea.sg.basic.mapper.CpCPhyWarehouseMapper;
import com.jackrain.nea.sg.basic.model.request.SgStorageQueryRequest;
import com.jackrain.nea.sg.basic.model.result.SgR3BaseResult;
import com.jackrain.nea.sg.basic.model.table.CpCPhyWarehouse;
import com.jackrain.nea.sg.basic.model.table.SgBStorageInclPhy;
import com.jackrain.nea.sg.basic.utils.R3ParamUtil;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.out.common.SgOutConstantsIF;
import com.jackrain.nea.sg.send.utils.SgStoreUtils;
import com.jackrain.nea.sg.transfer.common.ScTransferConstants;
import com.jackrain.nea.sg.transfer.common.SgTransferConstantsIF;
import com.jackrain.nea.sg.transfer.enums.SgTransferBillStatusEnum;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferImpItemMapper;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferItemMapper;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferMapper;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferTeusItemMapper;
import com.jackrain.nea.sg.transfer.model.request.SgTransferBillAuditRequest;
import com.jackrain.nea.sg.transfer.model.table.ScBTransfer;
import com.jackrain.nea.sg.transfer.model.table.ScBTransferImpItem;
import com.jackrain.nea.sg.transfer.model.table.ScBTransferItem;
import com.jackrain.nea.sg.transfer.model.table.ScBTransferTeusItem;
import com.jackrain.nea.sg.transfer.utils.SgTransferBillGeneraUtil;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.utils.AssertUtils;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @author csy
 * Date: 2019/5/5
 * Description: 调拨单-审核
 */
@Slf4j
@Component
public class SgTransferAuditService {


    @Autowired
    private SgStorageBoxConfig storageBoxConfig;

    /**
     * 接口入参
     */
    @Transactional(rollbackFor = Exception.class)
    @Deprecated
    public ValueHolderV14 audit(SgTransferBillAuditRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("Start SgTransferAuditService.audit. ReceiveParams:SgTransferBillAuditRequest="
                    + JSONObject.toJSONString(request) + ";");
        }
        AssertUtils.notNull(request, "参数为空-request");
        User user = request.getUser();
        Long sourceId = request.getSourceBillId();
        Integer sourceBillType = request.getSourceBillType();
        AssertUtils.notNull(user, "参数为空-user");
        AssertUtils.notNull(sourceId, "参数为空-sourceId");
        AssertUtils.notNull(sourceBillType, "参数为空-sourceBillType");
        return auditTransfer(sourceId, user, false);
    }

    /**
     * 页面审核入口
     *
     * @param objId 调拨单ID
     * @param user  用户信息
     * @return v14
     */
    @Transactional(rollbackFor = Exception.class)
    public ValueHolder auditR3(Long objId, User user) {
        if (log.isDebugEnabled()) {
            log.debug("Start auditR3,objId=" + objId + ";");
        }
        return R3ParamUtil.convertV14(auditTransfer(objId, user, false));
    }

    @Transactional(rollbackFor = Exception.class)
    public ValueHolder audit(Long objId, User user) {
        if (log.isDebugEnabled()) {
            log.debug("Start SgAdjAuditService.audit. ReceiveParams:objId="
                    + objId + ";");
        }
        return R3ParamUtil.convertV14(auditTransfer(objId, user, true));
    }

    /**
     * 审核
     *
     * @param user  用户
     * @param objId id
     */
    private ValueHolderV14 auditTransfer(Long objId, User user, boolean isSameTrans) {
        log.debug("审核返回 auditTransfer 1111===》");

        SgTransferBillGeneraUtil generaUtil = ApplicationContextHandle.getBean(SgTransferBillGeneraUtil.class);
        //===单据状态校验===
        Locale locale = user.getLocale();
        CusRedisTemplate<String, String> redisTemplate = SgStoreUtils.initRedisTemplate();
        String lockKsy = ScTransferConstants.TRANSFER_INDEX + ":" + objId;
        ValueHolderV14 holderV14 = new ValueHolderV14(ResultCode.SUCCESS, Resources.getMessage("审核成功!", locale));
        if (redisTemplate.opsForValue().get(lockKsy) != null) {
            throw new NDSException(Resources.getMessage("当前记录正在被操作,请稍后重试!", user.getLocale()));
        } else {
            log.debug("审核返回 auditTransfer 222===》");
            ScBTransferMapper mapper = ApplicationContextHandle.getBean(ScBTransferMapper.class);
            ScBTransferItemMapper itemMapper = ApplicationContextHandle.getBean(ScBTransferItemMapper.class);
            ScBTransferImpItemMapper impItemMapper = ApplicationContextHandle.getBean(ScBTransferImpItemMapper.class);
            try {
                redisTemplate.opsForValue().set(lockKsy, "OK", 30, TimeUnit.MINUTES);
                ScBTransfer transfer = mapper.selectById(objId);
                statusCheck(transfer, locale);
                // 2019-08-05添加：获取全链路信息
//                transfer.setFullLink(queryAcFullLinkForTransfer(transfer.getCpCOrigEcode(), transfer.getCpCDestEcode(),
//                        transfer.getBillNo()));

                Integer transferType = transfer.getTransferType();
                AssertUtils.notNull(transferType, "数据异常-单据类型不存在", locale);
                Long origStoreId = transfer.getCpCOrigId();
                Long destStoreId = transfer.getCpCDestId();
                AssertUtils.notNull(origStoreId, "数据异常-发货店仓id不存在", locale);
                AssertUtils.notNull(destStoreId, "数据异常-收货店仓id不存在", locale);
                CpCPhyWarehouse origWarehouse = getWareHouseInfo(origStoreId);
                CpCPhyWarehouse destWarehouse = getWareHouseInfo(destStoreId);
                AssertUtils.notNull(origWarehouse, "数据异常-发货实体仓不存在", locale);
                AssertUtils.notNull(destWarehouse, "数据异常-收货实体仓不存在", locale);

                /**
                 * cs 开启箱功能
                 *  2019.10.23 10:43
                 */
                // 录入明细和箱内明细只做一次查询
                List<ScBTransferImpItem> impItems = null;
                List<ScBTransferTeusItem> boxItems = null;
                JSONArray impDelIds = null;

                if (storageBoxConfig.getBoxEnable()) {
                    ScBTransferBoxFunctionService boxFunctionService = ApplicationContextHandle.getBean(ScBTransferBoxFunctionService.class);
                    // 本调拨单关联 条码明细 为空，删除录入明细中  数量 =0 的记录
                    impDelIds = boxFunctionService.deleteImpItems(transfer);
                    int impItemCount = impItemMapper.selectCount(new QueryWrapper<ScBTransferImpItem>().lambda()
                            .eq(ScBTransferImpItem::getScBTransferId, transfer.getId()));

                    AssertUtils.cannot(impItemCount <= 0, "录入明细为空", locale);

                    // 如果本单为订单调拨，则检查发货订单剩余量
//                    if (SgTransferConstantsIF.TRANSFER_TYPE_ORDER == transfer.getTransferType()) {
//                        AssertUtils.notNull(transfer.getOcBSendOutId(), "订单类型调拨单，发货订单不能为空！", locale);
//                        boxFunctionService.checkTransferQty(locale, objId, transfer.getOcBSendOutId(), impItemMapper);
//                    }


                    // 开启箱功能 ，将 录入明细 写入 条码明细 ，箱内明细
                    boxFunctionService.insertItemFromImp(transfer, user);

                    // 箱功能开启 查询录入明细
                    impItems = impItemMapper.selectList(
                            new QueryWrapper<ScBTransferImpItem>().lambda().
                                    eq(ScBTransferImpItem::getScBTransferId, objId));

                    // 箱功能开启 查询录入明细
                    ScBTransferTeusItemMapper boxMapper = ApplicationContextHandle.getBean(ScBTransferTeusItemMapper.class);
                    boxItems = boxMapper.selectList(
                            new QueryWrapper<ScBTransferTeusItem>().lambda().
                                    eq(ScBTransferTeusItem::getScBTransferId, objId));
                }

                log.debug("审核返回 auditTransfer 3333===》" + holderV14.toJSONObject());
                // 当【条码】数量为0时，自动删除该条码 保存删除调拨单明细的id
                List<Long> itemLongList = itemMapper.selectList(new QueryWrapper<ScBTransferItem>().lambda()
                        .eq(ScBTransferItem::getScBTransferId, transfer.getId())
                        .eq(ScBTransferItem::getQty, BigDecimal.ZERO))
                        .stream().map(s -> s.getId()).collect(Collectors.toList());

                JSONArray itemArray = new JSONArray();
                for (Long itemid : itemLongList) {
                    itemArray.add(itemid);
                }
                //e)	当【条码】数量为0时，自动删除该条码 ；
                itemMapper.delete(new QueryWrapper<ScBTransferItem>().lambda()
                        .eq(ScBTransferItem::getScBTransferId, transfer.getId())
                        .eq(ScBTransferItem::getQty, BigDecimal.ZERO));

                List<ScBTransferItem> itemList = itemMapper.selectList(new QueryWrapper<ScBTransferItem>().lambda()
                        .eq(ScBTransferItem::getScBTransferId, transfer.getId()));
                AssertUtils.cannot(CollectionUtils.isEmpty(itemList), "明细为空", locale);
                Date currentDate = new Date();
                // 实缺调拨、差异调拨不做控制
                if (SgTransferConstantsIF.TRANSFER_TYPE_DEFICIENCY != transferType && transferType != SgTransferConstantsIF.TRANSFER_TYPE_DIFF) {
                    //负库存控制
                    storageCheck(user, transfer, itemList);
                }
                if (origWarehouse.getId().equals(destWarehouse.getId())) {

                    createBillWithOutPhy(user, transfer, itemList, generaUtil, objId, currentDate, mapper, itemMapper,
                            impItems, boxItems);
                    // 2019-07-29修改：判断调拨类型根据订单编号，原因：订单类型的调拨单生成的调拨差异单，差异处理后存在冲突
//                    if (transfer.getOcBSendOutId() != null) {
//                        generaUtil.createOcSend(user, transfer, itemList, null, SgTransferBillStatusEnum.AUDITED_ALL_OUT_ALL_IN.getVal());
//                    }

                    // 同一实体仓自动出入库，不生成出入库单，生成结算日志
                    ScBTransfer transferLog = mapper.selectById(transfer.getId());
                    List<ScBTransferItem> transferItems = itemMapper.selectList(new QueryWrapper<ScBTransferItem>().lambda()
                            .eq(ScBTransferItem::getScBTransferId, transfer.getId()));
                    generaUtil.createSettleLogForTransfer(user, transferLog, transferItems, SourceBillType.TRANSFER_OUT, true, false);
                    generaUtil.createSettleLogForTransfer(user, transferLog, transferItems, SourceBillType.TRANSFER_IN, true, false);
                } else {
                    createBillWithPhy(user, transfer, itemList, generaUtil, objId, currentDate, holderV14, mapper,
                            origWarehouse, destWarehouse, locale, impItems, boxItems);

                    // 发货订单 id 不为空
//                    if (transfer.getOcBSendOutId() != null) {
//                        generaUtil.createOcSend(user, transfer, itemList, null, SgTransferBillStatusEnum.AUDITED_NOT_OUT.getVal());
//                    }
                }

                String index = ScTransferConstants.TRANSFER_INDEX;
                ScBTransfer scBTransfer = mapper.selectById(objId);
                String parentKey = ScTransferConstants.TRANSFER_PARENT_FIELD;
                // 箱功能开启时，操作录入明细
                if (storageBoxConfig.getBoxEnable()) {
                    //推送ES 先删除 【条码】数量为0 的录入明细
                    String type = SgConstants.SC_B_TRANSFER_IMP_ITEM;
                    StorageESUtils.pushESBModelByUpdate(null, null, objId, impDelIds, index,
                            index, type, null, null, null, true);

                    List<ScBTransferImpItem> impItemList = impItemMapper.selectList(
                            new QueryWrapper<ScBTransferImpItem>().lambda()
                                    .eq(ScBTransferImpItem::getScBTransferId, objId));
                    StorageESUtils.pushESBModelByUpdate(scBTransfer, impItemList, scBTransfer.getId(), null,
                            index, index, type, parentKey, ScBTransfer.class, ScBTransferImpItem.class, false);

                } else {
                    //推送ES 先删除 【条码】数量为0 的明细
                    String type = ScTransferConstants.TRANSFER_ITEM_TYPE;
                    StorageESUtils.pushESBModelByUpdate(null, null, objId, itemArray, index,
                            index, type, null, null, null, true);
                    //推送ES
                    List<ScBTransferItem> scBTransferItemList = itemMapper.selectList(
                            new QueryWrapper<ScBTransferItem>().lambda()
                                    .eq(ScBTransferItem::getScBTransferId, objId));
                    StorageESUtils.pushESBModelByUpdate(scBTransfer, scBTransferItemList, scBTransfer.getId(), null,
                            index, index, type, parentKey, ScBTransfer.class, ScBTransferItem.class, false);
                }

                //todo:如果本单【调拨类型】为订单调拨，调用发货订单的【更新已配已发量】服务（注：如果调用服务返回失败，记录系统日志）  暂无服务!  考虑补偿!

            } catch (Exception e) {
                log.error(this.getClass().getName() + ".auditTransfer.error:", e);
                // 当新增和审核在同一事务时，审核失败事务回滚手动删除es数据
                if (isSameTrans) delTransferEs(mapper, itemMapper, objId);
                AssertUtils.logAndThrow(e.getMessage(), locale);
            } finally {
                redisTemplate.delete(lockKsy);
            }

            if (holderV14.getData() == null) {
                //   objId 必返回
                JSONObject data = new JSONObject();
                data.put(R3ParamConstants.OBJID, objId);
                holderV14.setData(data);

            }
            return holderV14;
        }
    }

    /**
     * 新增和审核同一事务时，审核失败手动删除es
     *
     * @param mapper     主表mapper
     * @param itemMapper 明细mapper
     * @param objId      调拨单id
     */
    private void delTransferEs(ScBTransferMapper mapper, ScBTransferItemMapper itemMapper, Long objId) {
        try {
            Integer count = mapper.selectCount(new QueryWrapper<ScBTransfer>().lambda().eq(ScBTransfer::getId, objId));
            if (count > 0) {
                ElasticSearchUtil.delDocument(ScTransferConstants.TRANSFER_INDEX, ScTransferConstants.TRANSFER_TYPE, objId);
                List<ScBTransferItem> transferItems = itemMapper.selectList(
                        new QueryWrapper<ScBTransferItem>().lambda().eq(ScBTransferItem::getScBTransferId, objId));
                if (!CollectionUtils.isEmpty(transferItems)) {
                    for (ScBTransferItem transferItem : transferItems) {
                        ElasticSearchUtil.delDocument(ScTransferConstants.TRANSFER_INDEX,
                                ScTransferConstants.TRANSFER_ITEM_TYPE, transferItem.getId(), objId);
                    }
                }
            }
        } catch (Exception e) {
            log.error(this.getClass().getName() + ",delTransferEs,error:" + e.getMessage());
        }
    }

    private void createBillWithOutPhy(User user, ScBTransfer transfer, List<ScBTransferItem> itemList,
                                      SgTransferBillGeneraUtil generaUtil, Long objId, Date currentDate,
                                      ScBTransferMapper mapper, ScBTransferItemMapper itemMapper,
                                      List<ScBTransferImpItem> impItems, List<ScBTransferTeusItem> boxItems) {

        if (SgConstants.IS_ACTIVE_Y.equals(transfer.getIsOutReduce()) && SgConstants.IS_ACTIVE_Y.equals(transfer.getIsInIncrease())) {
            //另一条链路 只调逻辑仓库存
            // 新增逻辑发货单
            generaUtil.createSendBill(user, transfer, itemList, impItems, boxItems);
            // 新增逻辑收货单
            generaUtil.createReceiveBill(user, transfer, itemList, false, impItems, boxItems, null);
            // 逻辑发货单 发货
            generaUtil.sendOut(transfer, itemList, user, impItems, boxItems);
            // 逻辑收货单 收货
            generaUtil.receiveIn(transfer, itemList, user, impItems, boxItems);
        }

        // 更新主表信息
        itemMapper.updateQtyOutInByTransferId(objId, user, currentDate);
        ScBTransfer updateTransfer = new ScBTransfer();
        updateTransfer.setId(objId);
        StorageESUtils.setBModelDefalutDataByUpdate(updateTransfer, user);
        updateTransfer.setModifierename(user.getEname());
        updateTransfer.setStatus(SgTransferBillStatusEnum.AUDITED_ALL_OUT_ALL_IN.getVal());
        updateTransfer.setStatusId(user.getId().longValue());
        updateTransfer.setStatusEname(user.getEname());
        updateTransfer.setStatusName(user.getName());
        updateTransfer.setStatusTime(currentDate);
        updateTransfer.setOuterName(user.getName());
        updateTransfer.setOuterEname(user.getEname());
        updateTransfer.setOuterId(user.getId().longValue());
        updateTransfer.setOutTime(currentDate);
        updateTransfer.setOutDate(currentDate);
        updateTransfer.setInDate(currentDate);
        updateTransfer.setInTime(currentDate);
        updateTransfer.setInerId(user.getId().longValue());
        updateTransfer.setInerEname(user.getEname());
        updateTransfer.setInerName(user.getName());
        updateTransfer.setTotQtyIn(transfer.getTotQty());
        updateTransfer.setTotQtyOut(transfer.getTotQty());
        updateTransfer.setTotAmtInList(transfer.getTotAmtList());
        updateTransfer.setTotAmtOutList(transfer.getTotAmtList());
        updateTransfer.setPickTime(currentDate);
        updateTransfer.setPickerId(user.getId().longValue());
        updateTransfer.setPickerEname(user.getEname());
        updateTransfer.setPickerName(user.getName());
        updateTransfer.setPickStatus(ScTransferConstants.PICK_STATUS_PICKED);
        // 2019-08-05添加：更新全链路字段
        updateTransfer.setFullLink(transfer.getFullLink());
        mapper.updateById(updateTransfer);
    }


    /**
     * @param user          用户
     * @param transfer      调拨单
     * @param itemList      明细
     * @param generaUtil    util
     * @param objId         主表id
     * @param currentDate   当前日期
     * @param holderV14     holder
     * @param mapper        mapper
     * @param origWarehouse 发货店仓
     * @param destWarehouse 收货店仓
     * @param locale        locale
     */
    private void createBillWithPhy(User user, ScBTransfer transfer, List<ScBTransferItem> itemList,
                                   SgTransferBillGeneraUtil generaUtil, Long objId, Date currentDate,
                                   ValueHolderV14 holderV14, ScBTransferMapper mapper, CpCPhyWarehouse origWarehouse,
                                   CpCPhyWarehouse destWarehouse, Locale locale, List<ScBTransferImpItem> impItems,
                                   List<ScBTransferTeusItem> boxItems) {

        //===跟新单据审核状态 审核人等相关信息===
        ScBTransfer updateTransfer = new ScBTransfer();
        updateTransfer.setId(objId);
        StorageESUtils.setBModelDefalutDataByUpdate(updateTransfer, user);
        updateTransfer.setModifierename(user.getEname());
        updateTransfer.setStatusId(user.getId().longValue());
        updateTransfer.setStatusEname(user.getEname());
        updateTransfer.setStatusName(user.getName());
        updateTransfer.setStatusTime(currentDate);
        // 2019-08-05添加：更新全链路字段
        updateTransfer.setFullLink(transfer.getFullLink());

        // 1.本单的【是否锁单】=否且【是否出库减库存】=是时,调用【新增或更新逻辑发货单】
        Integer isLocked = transfer.getIsLocked();
        String isOutReduce = transfer.getIsOutReduce();
        String isInIncrease = transfer.getIsInIncrease();
        if (ScTransferConstants.IS_UNLOCKED == isLocked && ScTransferConstants.IS_AUTO_Y.equals(isOutReduce)) {
            generaUtil.createSendBill(user, transfer, itemList, impItems, boxItems);
        }

        // 2.系统参数【生成逻辑收货单节点】=审核且【是否入库加库存】=是时，调用【新增或更新逻辑收货单】服务
        String checkPoint = AdParamUtil.getParam(ScTransferConstants.SYSTEM_RECEIVE);
        AssertUtils.cannot(StringUtils.isEmpty(checkPoint), "系统参数 收货生成节点不存在!", locale);
        if (ScTransferConstants.ACTION_SUBMIT.equals(checkPoint) && ScTransferConstants.IS_AUTO_Y.equals(isInIncrease)) {
            //是否入库加库存  新增逻辑收货单
            generaUtil.createReceiveBill(user, transfer, itemList, false, impItems, boxItems, null);
        }

        // 3.【是否出库减库存】=是时
        JSONObject data = new JSONObject();
        if (ScTransferConstants.IS_AUTO_Y.equals(isOutReduce)) {

            // 调用【新增出库通知单】的服务
            ValueHolderV14<SgR3BaseResult> outResult = generaUtil.createOutNotice(
                    user, transfer, itemList, origWarehouse, destWarehouse, impItems, boxItems);
            long outNoticesId = outResult.getData().getDataJo().getLongValue(R3ParamConstants.OBJID);
            String outNoticesBillNo = outResult.getData().getDataJo().getString("bill_no");
            HashMap outSkuMap = (HashMap) outResult.getData().getDataJo().get(SgOutConstantsIF.RETURN_OUT_NOTICES_ITEM_KEY);

            // 2019-07-18添加逻辑：如果本单的【自动出库】=是，则调用【新增并审核出库结果单】
            String isAutoOut = transfer.getIsAutoOut();
            if (StringUtils.equals(isAutoOut, ScTransferConstants.IS_AUTO_Y)) {
                generaUtil.createOutResult(user, transfer, itemList, outNoticesId, outNoticesBillNo, outSkuMap,
                        impItems, boxItems);
            }

            data.put("outId", outNoticesId);
            data.put("outBillNo", outNoticesBillNo);
            data.put(SgOutConstantsIF.RETURN_OUT_NOTICES_ITEM_KEY, outSkuMap);
            holderV14.setData(data);
        }

        data.put(R3ParamConstants.OBJID, objId);
        log.debug("createBillWithPhy===>holderV14" + holderV14.toJSONObject());
        holderV14.setData(data);

        updateTransfer.setStatus(SgTransferBillStatusEnum.AUDITED_NOT_OUT.getVal());
        mapper.updateById(updateTransfer);
    }

    /**
     * 单据状态校验
     */
    private void statusCheck(ScBTransfer transfer, Locale locale) {
        AssertUtils.notNull(transfer, "当前单据已不存在!", locale);
        Integer status = transfer.getStatus();
        AssertUtils.notNull(status, "当数据错误-status不存在!", locale);
        AssertUtils.cannot(status == SgTransferBillStatusEnum.VOIDED.getVal(), "当前记录已作废，不允许审核！", locale);
        AssertUtils.cannot(status == SgTransferBillStatusEnum.AUDITED_NOT_OUT.getVal(), "当前记录已审核，不允许重复审核！", locale);
        AssertUtils.cannot((status > SgTransferBillStatusEnum.AUDITED_NOT_OUT.getVal() &&
                status < SgTransferBillStatusEnum.VOIDED.getVal()), "当前记录已有出/入库记录，不允许审核！", locale);
        AssertUtils.cannot(StringUtils.isEmpty(transfer.getReceiverAddress()), "收货人详细地址为空!", locale);
        AssertUtils.notNull(transfer.getReceiverProvinceId(), "收货人 省信息为空!", locale);
        AssertUtils.notNull(transfer.getReceiverCityId(), "收货人 市信息为空!", locale);
        AssertUtils.notNull(transfer.getReceiverDistrictId(), "收货人 区信息为空!", locale);
        AssertUtils.notNull(transfer.getReceiverMobile(), "收货人 手机号为空!", locale);
        AssertUtils.notNull(transfer.getReceiverName(), "收货人 名称为空!", locale);
        AssertUtils.notNull(transfer.getCpCLogisticsId(), "物流公司不能为空！", locale);

        // 发货店仓对应的实体仓档案中【WMS管控仓】为是、且本单的【自动出库】为是，则提示：“发货店仓为WMS管控仓，不允许自动出库！”
        CpCPhyWarehouseMapper warehouseMapper = ApplicationContextHandle.getBean(CpCPhyWarehouseMapper.class);
        String isAutoOut = transfer.getIsAutoOut();
        if (StringUtils.equals(isAutoOut, ScTransferConstants.IS_AUTO_Y)) {
            CpCPhyWarehouse origWarehouse = warehouseMapper.selectByStoreId(transfer.getCpCOrigId());
            AssertUtils.notNull(origWarehouse, "发货店仓对应的实体仓不存在", locale);
            Integer wmsControl = origWarehouse.getWmsControlWarehouse();
            if (wmsControl != null && wmsControl == ScTransferConstants.IS_PASS_WMS_Y) {
                AssertUtils.logAndThrow("发货店仓为WMS管控仓，不允许自动出库！", locale);
            }
        }


        // 收货店仓对应的实体仓档案中【WMS管控仓】为是、且本单的【自动入库】为是，则提示：“收货店仓为WMS管控仓，不允许自动入库！”
        String isAutoIn = transfer.getIsAutoIn();
        if (StringUtils.equals(isAutoIn, ScTransferConstants.IS_AUTO_Y)) {
            String reserveVarchar01 = transfer.getReserveVarchar01();
            if (reserveVarchar01 == null || !(reserveVarchar01.equals(SgConstants.IS_ACTIVE_Y))) {
                CpCPhyWarehouse origWarehouse = warehouseMapper.selectByStoreId(transfer.getCpCDestId());
                AssertUtils.notNull(origWarehouse, "收货店仓对应的实体仓不存在", locale);
                Integer wmsControl = origWarehouse.getWmsControlWarehouse();
                if (wmsControl != null && wmsControl == ScTransferConstants.IS_PASS_WMS_Y) {
                    AssertUtils.logAndThrow("收货店仓为WMS管控仓，不允许自动入库！", locale);
                }
            }
        }
    }


    /**
     * 获取实体仓信息
     * <p>
     * 单条
     *
     * @param storeId 店仓id
     */
    private CpCPhyWarehouse getWareHouseInfo(Long storeId) {
        CpCPhyWarehouseMapper warehouseMapper = ApplicationContextHandle.getBean(CpCPhyWarehouseMapper.class);
        return warehouseMapper.selectByStoreId(storeId);
    }


    /**
     * 负库存check why?
     *
     * @param user     用户
     * @param transfer 调拨单
     * @param items    调拨单明细
     */
    private void storageCheck(User user, ScBTransfer transfer, List<ScBTransferItem> items) {
        Locale locale = user.getLocale();
        CommonCacheValUtils cacheValUtils = ApplicationContextHandle.getBean(CommonCacheValUtils.class);
        CpCStore storeInfo = cacheValUtils.getStoreInfo(transfer.getCpCOrigId());
        AssertUtils.notNull(storeInfo, "【发货店仓】对应【逻辑仓档案】不存在", locale);
        AssertUtils.notNull(storeInfo.getIsnegative(), "【发货店仓】的负库存控制必须设置", locale);
        if (storeInfo.getIsnegative() == 1) {
            SgStorageQueryCmd storageQueryCmd = (SgStorageQueryCmd) ReferenceUtil.refer(
                    ApplicationContextHandle.getApplicationContext(),
                    SgStorageQueryCmd.class.getName(),
                    SgConstantsIF.GROUP, SgConstantsIF.VERSION);
            SgStorageQueryRequest storageQueryRequest = new SgStorageQueryRequest();
            // 发货店仓ID
            List<Long> storeIds = new ArrayList<>();
            storeIds.add(transfer.getCpCOrigId());
            storageQueryRequest.setStoreIds(storeIds);

            // 修改
            List<SgBStorageInclPhy> storageList = new ArrayList<>();
            List<String> skuList = items.stream().map(ScBTransferItem::getPsCSkuEcode).collect(Collectors.toList());

            int itemNum = skuList.size();
            int pageSize = SgConstants.SG_COMMON_INSERT_PAGE_SIZE;
            int page = itemNum / pageSize;
            if (itemNum % pageSize != 0) page++;
            for (int i = 0; i < page; i++) {
                int startIndex = i * pageSize;
                int endIndex = (i + 1) * pageSize;
                List<String> skuCodes = skuList.subList(startIndex, endIndex < itemNum ? endIndex : itemNum);

                storageQueryRequest.setSkuEcodes(skuCodes);
                ValueHolderV14<List<SgBStorageInclPhy>> storageInclPhyV14 = storageQueryCmd.queryStorageInclPhy(storageQueryRequest, user);
                if (log.isDebugEnabled()) {
                    log.debug(this.getClass().getName() + ",storageCheck,第" + i + "逻辑仓库存查询结果:" + JSON.toJSONString(storageInclPhyV14));
                }
                if (storageInclPhyV14.getCode() == ResultCode.FAIL) {
                    AssertUtils.logAndThrow("storageCheck,第" + i + "次逻辑仓库存查询异常:" + storageInclPhyV14.getMessage(), user.getLocale());
                }
                storageList.addAll(storageInclPhyV14.getData());
            }

            JSONArray errorSkuQty = new JSONArray();
            HashMap<String, BigDecimal> qtyMap = getQtyAvailable(storageList);
            items.forEach(saleItem -> {
                String skuCode = saleItem.getPsCSkuEcode();
                BigDecimal qtyAvailable = qtyMap.get(skuCode);
                if (qtyAvailable == null) qtyAvailable = BigDecimal.ZERO;
                BigDecimal saleQty = saleItem.getQty();
                if (saleQty.compareTo(qtyAvailable) > 0) {
                    errorSkuQty.add("条码:" + skuCode + ";调拨数量:" + saleQty + ";可用库存:" + qtyAvailable);
                }
            });


            if (errorSkuQty.size() > 0) {
                AssertUtils.logAndThrow("库存不足:" + transfer.getBillNo() + "明细调拨数量大于可用库存:" + JSONObject.toJSONString(errorSkuQty));
            }
        }
    }

    /**
     * 全链路查询
     *
     * @param origStore 发货店仓
     * @param destStore 收货店仓
     * @return 全链路
     */
    private String queryAcFullLinkForTransfer(String origStore, String destStore, String billNo) {
        AcFullLinkQueryRequest request = new AcFullLinkQueryRequest();
        request.setQueryEnum(FullLinkQueryEnum.queryType4);
        request.setStartECode(origStore);
        request.setEndECode(destStore);

        AcFullLinkQueryCmd linkQueryCmd = (AcFullLinkQueryCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(),
                AcFullLinkQueryCmd.class.getName(),
                "ac-sc", "1.0");
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ",queryAcFullLinkForTransfer,billNo:" + billNo +
                    ",request:" + JSON.toJSONString(request));
        }
        ValueHolderV14<AcFullLinkQueryResult> v14 = linkQueryCmd.queryFullLink(request);
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ",queryAcFullLinkForTransfer,v14:" + JSON.toJSONString(v14));
        }

        if (v14.getCode() != ResultCode.SUCCESS) {
            AssertUtils.logAndThrow("全链路查询失败！失败原因：" + v14.getMessage());
        }

        AcFullLinkQueryResult data = v14.getData();
        if (data == null || data.getBillLinkStr() == null) {
            AssertUtils.logAndThrow("全链路查询为空！");
        }
        return data.getBillLinkStr();
    }

    /**
     * list 转 map
     *
     * @param storageList 库存lisy
     * @return map
     */
    private HashMap<String, BigDecimal> getQtyAvailable(List<SgBStorageInclPhy> storageList) {
        HashMap<String, BigDecimal> hashMap = new HashMap<>(16);
        storageList.forEach(storage ->
                hashMap.put(storage.getPsCSkuEcode(), storage.getQtyAvailable())
        );
        return hashMap;
    }
}