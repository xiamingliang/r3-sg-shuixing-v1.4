package com.jackrain.nea.sg.transfer.services;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.cp.result.CpCStore;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.oc.basic.cache.CommonCacheValUtils;
import com.jackrain.nea.sg.basic.api.SgStorageQueryCmd;
import com.jackrain.nea.sg.basic.common.R3ParamConstants;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.model.request.SgStorageQueryRequest;
import com.jackrain.nea.sg.basic.model.table.SgBStorageInclPhy;
import com.jackrain.nea.sg.basic.utils.R3ParamUtil;
import com.jackrain.nea.sg.transfer.api.SgTransferAuditR3Cmd;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferItemMapper;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferMapper;
import com.jackrain.nea.sg.transfer.model.table.ScBTransfer;
import com.jackrain.nea.sg.transfer.model.table.ScBTransferItem;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.utils.AssertUtils;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.query.QuerySession;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

/**
 * @author zhu lin yu
 * @since 2019/10/10
 * create at : 2019/10/10 19:04
 */
@Slf4j
@Component
public class SgStorageAsTransferQtySaveService {
    @Autowired
    private ScBTransferMapper scBTransferMapper;

    @Autowired
    private ScBTransferItemMapper scBTransferItemMapper;

    @Autowired
    private CommonCacheValUtils commonCacheValUtils;

    /**
     * 把条码可用库存赋值给调拨数量，并掉用审核接口
     */
    public ValueHolder changTransferQty(QuerySession session) {
        SgStorageAsTransferQtySaveService sgStorageAsTransferQtySaveService = ApplicationContextHandle.getBean(SgStorageAsTransferQtySaveService.class);

        Locale locale = session.getLocale();
        User user = session.getUser();
        JSONObject param = R3ParamUtil.getParamJo(session);
        JSONArray ids = param.getJSONArray("ids");
        StringBuilder exceptionMessage = new StringBuilder();
        int errorNumber = 0;

        if (CollectionUtils.isNotEmpty(ids)) {
            log.info(this.getClass().getName() + " 调拨单id为:{}", JSONObject.toJSONString(ids));
            for (int i = 0; i < ids.size(); i++) {
                Long id = ids.getLongValue(i);
                try {
                    sgStorageAsTransferQtySaveService.storageCheck(id, user);
                } catch (Exception e) {
                    exceptionMessage.append("[ ").append(e.getMessage()).append(" ]");
                    errorNumber++;
                    log.error(this.getClass().getName() + " 调拨单调拨数量设置为可用库存服务失败", e);
                }
            }
        } else {
            Long objId = param.getLong(R3ParamConstants.OBJID);
            AssertUtils.notNull(objId, "参数不存在-objId", locale);
            sgStorageAsTransferQtySaveService.storageCheck(objId, user);
        }

        if (errorNumber > 0) {
            throw new NDSException(exceptionMessage.toString());
        }

        //掉用调拨审核服务
        SgTransferAuditR3Cmd sgTransferAuditR3Cmd = (SgTransferAuditR3Cmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(),
                SgTransferAuditR3Cmd.class.getName(),
                SgConstantsIF.GROUP, SgConstantsIF.VERSION);

        return sgTransferAuditR3Cmd.execute(session);
    }

    @Transactional(rollbackFor = Exception.class)
    public void storageCheck(Long objId, User user) {
        Locale locale = user.getLocale();
        log.info(this.getClass().getName() + " 调拨单id为:{}", JSONObject.toJSONString(objId));

        ScBTransfer transfer = scBTransferMapper.selectById(objId);
        CpCStore storeInfo = commonCacheValUtils.getStoreInfo(transfer.getCpCOrigId());
        AssertUtils.notNull(storeInfo, "单据编号为:" + transfer.getBillNo() + "的【发货店仓】对应【逻辑仓档案】不存在", locale);

        SgStorageQueryCmd storageQueryCmd = (SgStorageQueryCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(),
                SgStorageQueryCmd.class.getName(),
                SgConstantsIF.GROUP, SgConstantsIF.VERSION);
        SgStorageQueryRequest storageQueryRequest = new SgStorageQueryRequest();

        // 发货店仓ID
        List<Long> storeIds = new ArrayList<>();
        storeIds.add(transfer.getCpCOrigId());
        storageQueryRequest.setStoreIds(storeIds);

        List<ScBTransferItem> itemList = scBTransferItemMapper.selectList(new QueryWrapper<ScBTransferItem>().lambda()
                .eq(ScBTransferItem::getScBTransferId, transfer.getId()));

        // 修改
        List<SgBStorageInclPhy> storageList = new ArrayList<>();
        List<String> skuList = itemList.stream().map(ScBTransferItem::getPsCSkuEcode).collect(Collectors.toList());

        int itemNum = skuList.size();
        int pageSize = SgConstants.SG_COMMON_INSERT_PAGE_SIZE;
        int page = itemNum / pageSize;
        if (itemNum % pageSize != 0) page++;
        for (int i = 0; i < page; i++) {
            int startIndex = i * pageSize;
            int endIndex = (i + 1) * pageSize;
            List<String> skuCodes = skuList.subList(startIndex, endIndex < itemNum ? endIndex : itemNum);

            storageQueryRequest.setSkuEcodes(skuCodes);
            ValueHolderV14<List<SgBStorageInclPhy>> storageInclPhyV14 = storageQueryCmd.queryStorageInclPhy(storageQueryRequest, user);
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + ",storageCheck,第" + i + "逻辑仓库存查询结果:" + JSON.toJSONString(storageInclPhyV14));
            }
            if (storageInclPhyV14.getCode() == ResultCode.FAIL) {
                AssertUtils.logAndThrow("storageCheck,第" + i + "次逻辑仓库存查询异常:" + storageInclPhyV14.getMessage(), user.getLocale());
            }
            storageList.addAll(storageInclPhyV14.getData());
        }


        HashMap<String, BigDecimal> qtyMap = this.getQtyAvailable(storageList);

        //判断调拨单的所有明细的可用库存是否为0，如果都为0则不让生成调拨单
        int flag = 0;
        for (ScBTransferItem scBTransferItem : itemList) {
            String skuCode = scBTransferItem.getPsCSkuEcode();
            BigDecimal qtyAvailable = qtyMap.get(skuCode);
            if (qtyAvailable == null) {
                qtyAvailable = BigDecimal.ZERO;
            }
            if (qtyAvailable.compareTo(BigDecimal.ZERO) <= 0) {
                flag++;
            }
        }
        if (itemList.size() == flag) {
            throw new NDSException(" 单据编号为:" + transfer.getBillNo() + "的明细所有可用库存都为0，不能进行审核");
        }

        BigDecimal totalNum = BigDecimal.ZERO;
        BigDecimal totAmtList = BigDecimal.ZERO;
        //可用库存数量赋值给调拨数量
        for (ScBTransferItem saleItem : itemList) {
            String skuCode = saleItem.getPsCSkuEcode();
            BigDecimal qtyAvailable = qtyMap.get(skuCode);
            if (qtyAvailable == null) qtyAvailable = BigDecimal.ZERO;
            BigDecimal saleQty = saleItem.getQty();
            BigDecimal amtList = BigDecimal.ZERO;

            //如果库存为负，则赋值为0
            if (saleQty.compareTo(qtyAvailable) > 0) {
                if (qtyAvailable.compareTo(BigDecimal.ZERO) < 0) {
                    qtyAvailable = BigDecimal.ZERO;
                }

                totalNum = totalNum.add(qtyAvailable);
                amtList = qtyAvailable.multiply(saleItem.getPriceList());
                totAmtList = totAmtList.add(amtList);
                ScBTransferItem scBTransferItem = new ScBTransferItem();
                scBTransferItem.setQty(qtyAvailable);
                scBTransferItem.setAmtList(amtList);
                scBTransferItemMapper.update(scBTransferItem, new UpdateWrapper<ScBTransferItem>()
                        .lambda().eq(ScBTransferItem::getId, saleItem.getId()));
            } else {
                totalNum = totalNum.add(saleQty);
                totAmtList = totAmtList.add(saleQty.multiply(saleItem.getPriceList()));
            }
        }

        //更改主表的调拨总数字段
        ScBTransfer scBTransfer = new ScBTransfer();
        scBTransfer.setTotQty(totalNum);
        scBTransfer.setTotAmtList(totAmtList);
        scBTransferMapper.update(scBTransfer, new UpdateWrapper<ScBTransfer>()
                .lambda().eq(ScBTransfer::getId, objId));
    }

    private HashMap<String, BigDecimal> getQtyAvailable(List<SgBStorageInclPhy> storageList) {
        HashMap<String, BigDecimal> hashMap = new HashMap<>(16);
        storageList.forEach(storage ->
                hashMap.put(storage.getPsCSkuEcode(), storage.getQtyAvailable())
        );
        return hashMap;
    }
}
