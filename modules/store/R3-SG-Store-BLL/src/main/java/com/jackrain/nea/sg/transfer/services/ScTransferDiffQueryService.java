package com.jackrain.nea.sg.transfer.services;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferDiffMapper;
import com.jackrain.nea.sg.transfer.model.request.SgTransferDiffQueryRequest;
import com.jackrain.nea.sg.transfer.model.table.ScBTransferDiff;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author leexh
 * @since 2019/7/29 17:28
 * desc: 调拨差异单处理服务
 */
@Slf4j
@Component
public class ScTransferDiffQueryService {

    public ValueHolderV14<List<ScBTransferDiff>> queryTransferDiff(SgTransferDiffQueryRequest request){

        User user = request.getUser();
        AssertUtils.notNull(request, "参数不能为空！");
        AssertUtils.notNull(user, "用户信息不能为空！");

        Long sendOutId = request.getOcBSendOutId();
        AssertUtils.notNull(sendOutId, "订单ID不能为空！", user.getLocale());

        ScBTransferDiffMapper diffMapper = ApplicationContextHandle.getBean(ScBTransferDiffMapper.class);
        List<ScBTransferDiff> transferDiffs = diffMapper.selectList(
                new QueryWrapper<ScBTransferDiff>().lambda()
                        .eq(ScBTransferDiff::getOcBSendOutId, sendOutId));

        ValueHolderV14<List<ScBTransferDiff>> v14 = new ValueHolderV14<>();
        v14.setData(transferDiffs);
        v14.setCode(ResultCode.SUCCESS);
        v14.setMessage("查询成功！");
        return v14;
    }
}
