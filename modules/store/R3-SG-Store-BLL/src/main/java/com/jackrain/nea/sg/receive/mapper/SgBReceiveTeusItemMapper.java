package com.jackrain.nea.sg.receive.mapper;

import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.receive.model.table.SgBReceiveTeusItem;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SgBReceiveTeusItemMapper extends ExtentionMapper<SgBReceiveTeusItem> {
}