package com.jackrain.nea.sg.transfer.filter;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.oc.sale.api.OcSendOutQueryCmd;
import com.jackrain.nea.oc.sale.model.request.OcSendOutBillQueryRequest;
import com.jackrain.nea.oc.sale.model.result.OcSendOutResult;
import com.jackrain.nea.oc.sale.model.table.OcBSendOut;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.transfer.common.ScTransferConstants;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.tableService.DbRowAction;
import com.jackrain.nea.tableService.MainTableRecord;
import com.jackrain.nea.tableService.TableServiceContext;
import com.jackrain.nea.tableService.filter.BaseFilter;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * @author csy
 * Date: 2019/6/27
 * Description: 发货订单 保存filter
 */
@Component
@Slf4j
public class ScTransferOrderSaveFilter extends BaseFilter {


    @Override
    public void before(TableServiceContext context, MainTableRecord row) {
        JSONObject commitData = row.getMainData().getCommitData();
        JSONObject origData = row.getMainData().getOrignalData();
        Integer transferType = commitData.getInteger("TRANSFER_TYPE");
        if (transferType != null && transferType == ScTransferConstants.TRANSFER_TYPE_ORDER) {
            orderCheck(context, row);
        } else if (row.getAction().equals(DbRowAction.UPDATE)) {
            //原单据类型==order commitData中有店仓修改
            boolean isCheck = origData.getLongValue("TRANSFER_TYPE") == ScTransferConstants.TRANSFER_TYPE_ORDER &&
                    (commitData.getLong("CP_C_ORIG_ID") != null || commitData.getLong("CP_C_DEST_ID") != null);
            if (isCheck) {
                orderCheck(context, row);
            }
        }
    }


    private void orderCheck(TableServiceContext context, MainTableRecord row) {
        Locale locale = context.getLocale();
        JSONObject newData = row.getMainData().getNewData();
        Long orderId = newData.getLong("OC_B_SEND_OUT_ID");
        AssertUtils.notNull(orderId, "发货订单入参为空！", locale);
        OcSendOutQueryCmd queryCmd = (OcSendOutQueryCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(), OcSendOutQueryCmd.class.getName(), "oc", "1.0");
        List<OcSendOutBillQueryRequest> requestList = new ArrayList<>();
        OcSendOutBillQueryRequest request = new OcSendOutBillQueryRequest();
        request.setId(newData.getLong("OC_B_SEND_OUT_ID"));
        requestList.add(request);
        ValueHolderV14<List<OcSendOutResult>> holderV14 = queryCmd.querySendOut(requestList);
        AssertUtils.cannot(holderV14.getCode() == ResultCode.FAIL, "发货订单查询失败!:" + holderV14.getMessage(), locale);
        AssertUtils.cannot(CollectionUtils.isEmpty(holderV14.getData()), "发货订单不存在", locale);
        OcBSendOut ocBSendOut = holderV14.getData().get(0).getSendOut();
        if (row.getAction().equals(DbRowAction.INSERT)) {
            JSONObject commitData = row.getCommitData();
            Long origId = commitData.getLong("CP_C_ORIG_ID");
            Long destId = commitData.getLong("CP_C_DEST_ID");
            if (origId == null && destId == null) {
                commitData.put("CP_C_ORIG_ID", ocBSendOut.getCpCOrigId());
                commitData.put("CP_C_ORIG_ECODE", ocBSendOut.getCpCOrigEcode());
                commitData.put("CP_C_ORIG_ENAME", ocBSendOut.getCpCOrigEname());
                commitData.put("CP_C_DEST_ID", ocBSendOut.getCpCDestId());
                commitData.put("CP_C_DEST_ECODE", ocBSendOut.getCpCDestEcode());
                commitData.put("CP_C_DEST_ENAME", ocBSendOut.getCpCDestEname());
            } else if (origId == null || destId == null) {
                AssertUtils.logAndThrow("请输入发货店仓或者收货店仓", locale);
            }
        }


    }
}
