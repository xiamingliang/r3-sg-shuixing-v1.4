package com.jackrain.nea.sg.adjust.mapper;

import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.adjust.model.table.SgBAdjustImpItem;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SgBAdjustImpItemMapper extends ExtentionMapper<SgBAdjustImpItem> {
}