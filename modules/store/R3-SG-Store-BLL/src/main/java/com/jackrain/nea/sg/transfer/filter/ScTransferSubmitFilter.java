package com.jackrain.nea.sg.transfer.filter;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.oc.basic.api.TableServiceCmd;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferItemMapper;
import com.jackrain.nea.sg.transfer.model.table.ScBTransferItem;
import com.jackrain.nea.sg.transfer.services.ScTransferStorageService;
import com.jackrain.nea.tableService.MainTableRecord;
import com.jackrain.nea.tableService.TableServiceContext;
import com.jackrain.nea.tableService.constants.Constants;
import com.jackrain.nea.tableService.filter.BaseFilter;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.utils.AssertUtils;
import com.jackrain.nea.web.query.DefaultWebEvent;
import com.jackrain.nea.web.query.QuerySession;
import com.jackrain.nea.web.query.QuerySessionImpl;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * @author csy
 * 调拨单 提交filter
 */
@Component
@Deprecated
public class ScTransferSubmitFilter extends BaseFilter {


    @Override
    public void before(TableServiceContext context, MainTableRecord row) {
        //前置条件check
           /*
a)	已提交，则提示：“当前记录已提交，不允许重复提交！”
b)	已作废，则提示：“当前记录已作废，不允许提交！”
c)	记录不存在，则提示：“当前记录已不存在！”确认返回列表界面
d)	明细为空，则提示：“当前记录无明细，不允许提交！”
e)	当【条码】数量为0时，自动删除该条码 ；
        * */
        ScBTransferItemMapper itemMapper = ApplicationContextHandle.getBean(ScBTransferItemMapper.class);
        QueryWrapper<ScBTransferItem> query = new QueryWrapper<>();
        query.eq("SC_B_TRANSFER_ID", row.getId()).eq("QTY", BigDecimal.ZERO);
        itemMapper.delete(query);
        QueryWrapper<ScBTransferItem> queryCount = new QueryWrapper<>();
        queryCount.eq("SC_B_TRANSFER_ID", row.getId()).isNotNull("QTY").ne("QTY", BigDecimal.ZERO);
        int count = itemMapper.selectCount(queryCount);
        AssertUtils.cannot(count == 0, "当前记录无明细，不允许提交！", context.getLocale());
    }

    @Override
    public void after(TableServiceContext context, MainTableRecord row) {


        //获取 明细
        ScBTransferItemMapper itemMapper = ApplicationContextHandle.getBean(ScBTransferItemMapper.class);
        QueryWrapper<ScBTransferItem> itemQuery = new QueryWrapper<>();
        itemQuery.eq("SC_B_TRANSFER_ID", row.getId());
        List<ScBTransferItem> items = itemMapper.selectList(itemQuery);

        //调用出库单 保存
        TableServiceCmd tableServiceCmd = (TableServiceCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                TableServiceCmd.class.getName(), "sc", "5");
        QuerySession session = new QuerySessionImpl(context.getUser());
        DefaultWebEvent event = new DefaultWebEvent("dosave", new HashMap());
        event.put("param", createParam(row, items));
        session.setEvent(event);
        ValueHolder outSaveResult = tableServiceCmd.execute(session);
        if (outSaveResult == null || outSaveResult.toJSONObject() == null) {
            AssertUtils.logAndThrow("出库单保存失败 出库单无返回!");
        } else if (outSaveResult.toJSONObject().getIntValue("code") == -1) {
            AssertUtils.logAndThrow("出库单保存失败" + outSaveResult.toJSONObject().getString("message"), context.getLocale());
        }
        Long outId = outSaveResult.toJSONObject().getJSONObject("data").getLong("objid");
        AssertUtils.notNull(outId, "出库单 保存返回id为空!", context.getLocale());
        JSONObject extraData = new JSONObject();
        extraData.put("outId", outId);
        context.setExtraResultData(extraData);

        //调用库存
        ScTransferStorageService storageService = ApplicationContextHandle.getBean(ScTransferStorageService.class);
        storageService.transferSubmitStorageChange(items, row, context);
    }


    /**
     * 构造入库单数据
     *
     * @param row 一条主表记录
     */
    private JSONObject createParam(MainTableRecord row, List<ScBTransferItem> transferItems) {
        JSONObject fixC = new JSONObject();
        JSONObject mainJo = createOutMainJo(row.getMainData().getOrignalData());
        List<JSONObject> items = createOutItems(mainJo.getIntValue("OUT_STATUS"), transferItems);
        fixC.put("SC_B_OUT", mainJo);
        fixC.put("SC_B_OUT_ITEM", items);
        return createSaveParam(fixC);
    }

    /**
     * 构造入库单明细数据
     *
     * @param items    明细信息
     * @param ouStatus 出库状态
     */
    private List<JSONObject> createOutItems(int ouStatus, List<ScBTransferItem> items) {
        List<JSONObject> outItems = new ArrayList<>();
        boolean isOut = ouStatus == 2;
        for (ScBTransferItem item : items) {
            JSONObject jo = createOutItemJo(item, isOut);
            outItems.add(jo);
        }
        return outItems;
    }


    /**
     * 构造出库单单条明细数据
     *
     * @param transferItem 一条挑拨单明细
     * @param isOut        是否已出库
     *                     <p>
     *                     出库单明细表
     *                     条码、商品编码、商品名称、颜色、尺寸、吊牌价、吊牌金额：取值调拨单明细对应字段
     *                     调拨数量：调拨单明细中的调拨数量
     *                     出库数量：调拨单实际出库的数量，未出库则为0
     *                     差异数量：调拨单明细中的调拨数量-出库数量
     *                     成交价、成交金额：取0
     */

    private JSONObject createOutItemJo(ScBTransferItem transferItem, boolean isOut) {
        JSONObject item = new JSONObject();
        //条码
        item.put("PS_C_SKU_ID", transferItem.getPsCSkuId());
        item.put("PS_C_SKU_ECODE", transferItem.getPsCSkuEcode());
        //款号
        item.put("PS_C_PRO_ID", transferItem.getPsCProId());
        item.put("PS_C_PRO_ECODE", transferItem.getPsCProEcode());
        item.put("PS_C_PRO_ENAME", transferItem.getPsCProEname());
        //颜色
        item.put("PS_C_CLR_ID", transferItem.getPsCClrId());
        item.put("PS_C_CLR_ECODE", transferItem.getPsCClrEcode());
        item.put("PS_C_CLR_ENAME", transferItem.getPsCClrEname());
        //尺寸
        item.put("PS_C_SIZE_ID", transferItem.getPsCSizeId());
        item.put("PS_C_SIZE_ECODE", transferItem.getPsCSizeEcode());
        item.put("PS_C_SIZE_ENAME", transferItem.getPsCSizeEname());
        //标准价及金额
        item.put("PRICE_LIST", transferItem.getPriceList());
        item.put("AMT_LIST", transferItem.getAmtList());

        BigDecimal outNumber = transferItem.getQtyOut();
        if (!isOut) {
            outNumber = BigDecimal.ZERO;
        }
        item.put("QTY_BILL", transferItem.getQty());
        item.put("QTY", outNumber);
        item.put("QTY_DIFF", transferItem.getQty().subtract(outNumber));
        //成交价及金额
        item.put("PRICE_ACTUAL", BigDecimal.ZERO);
        item.put("AMT_OUT_ACTUAL", BigDecimal.ZERO);

        return item;
    }


    /**
     * 构造出库单主表记录
     *
     * @param jo 一条主表记录
     *           <p>
     *           出库单主表：
     *           单据类型：调拨出库
     *           单据编号、单据日期、发货店仓、发货经销商、收货店仓、收货经销商：取值调拨单对应字段
     *           收货方：取本单收货店仓的店仓名称
     *           出库状态：未提交
     *           拣货状态：未拣货
     *           总数量、总吊牌金额：取调拨单的总调拨数量、总吊牌金额
     */
    private JSONObject createOutMainJo(JSONObject jo) {
        JSONObject mainJo = new JSONObject();
        mainJo.put("BILL_TYPE", "3");
        mainJo.put("BILL_NO", jo.get("BILL_NO"));
        mainJo.put("BILL_DATE", new Date());
        mainJo.put("CP_C_ORIG_ID", jo.get("CP_C_ORIG_ID"));
        mainJo.put("CP_C_ORIG_ECODE", jo.get("CP_C_ORIG_ECODE"));
        mainJo.put("CP_C_ORIG_ENAME", jo.get("CP_C_ORIG_ENAME"));
        mainJo.put("CP_C_DEST_ID", jo.get("CP_C_DEST_ID"));
        mainJo.put("CP_C_DEST_ECODE", jo.get("CP_C_DEST_ECODE"));
        mainJo.put("CP_C_DEST_ENAME", jo.get("CP_C_DEST_ENAME"));
        mainJo.put("CP_C_CUSTOMERUP_ID", jo.get("CP_C_CUSTOMER_ID"));
        mainJo.put("CP_C_CUSTOMERUP_ECODE", jo.get("CP_C_CUSTOMER_ECODE"));
        mainJo.put("CP_C_CUSTOMERUP_ENAME", jo.get("CP_C_CUSTOMER_ENAME"));
        mainJo.put("CP_C_CUSTOMER_ID", jo.get("CP_C_CUSTOMER_ID"));
        mainJo.put("CP_C_CUSTOMER_ECODE", jo.get("CP_C_CUSTOMER_ECODE"));
        mainJo.put("CP_C_CUSTOMER_ENAME", jo.get("CP_C_CUSTOMER_ENAME"));
        mainJo.put("DESTNAME", jo.get("CP_C_DEST_ENAME"));
        mainJo.put("STATUS", Constants.STATUS_UNFINISHED);
        mainJo.put("PICK_STATUS", Constants.STATUS_UNFINISHED);
        mainJo.put("TOT_QTY", jo.getBigDecimal("TOT_QTY"));
        mainJo.put("TOT_AMT_LIST", jo.getBigDecimal("TOT_AMT_LIST"));
        return mainJo;
    }


    /**
     * 生成标准传参
     */
    private JSONObject createSaveParam(JSONObject fixC) {
        JSONObject param = new JSONObject();
        param.put("objid", -1);
        param.put("fixcolumn", fixC);
        param.put("table", "SC_B_OUT");
        return param;
    }
}
