package com.jackrain.nea.sg.receive.services;

import com.google.common.collect.Lists;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.sg.in.api.SgPhyInNoticesSelectBySourceCmd;
import com.jackrain.nea.sg.in.model.request.SgPhyInNoticesSelectBySourceRequest;
import com.jackrain.nea.sg.in.model.result.SgBPhyInNoticesResult;
import com.jackrain.nea.sg.out.api.SgPhyOutQueryCmd;
import com.jackrain.nea.sg.out.model.request.SgPhyOutBillBaseRequest;
import com.jackrain.nea.sg.out.model.request.SgPhyOutBillQueryRequest;
import com.jackrain.nea.sg.out.model.result.SgOutQueryResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author 舒威
 * @since 2019/7/2
 * create at : 2019/7/2 16:03
 */
@Slf4j
@Component
public class SgReceiveRPCService {

    public ValueHolderV14<List<SgOutQueryResult>> queryPhyOutNotices(Long sourceBillId, Integer sourceBillType) {
        SgPhyOutQueryCmd phyOutQueryCmd = (SgPhyOutQueryCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                SgPhyOutQueryCmd.class.getName(), "sg", "1.0");
        SgPhyOutBillQueryRequest request=new SgPhyOutBillQueryRequest();
        List<SgPhyOutBillBaseRequest> baseRequests= Lists.newArrayList();
        SgPhyOutBillBaseRequest baseRequest=new SgPhyOutBillBaseRequest();
        baseRequest.setSourceBillId(sourceBillId);
        baseRequest.setSourceBillType(sourceBillType);
        baseRequests.add(baseRequest);
        request.setBaseRequests(baseRequests);
        return phyOutQueryCmd.queryOutBySource(request);
    }

    public ValueHolderV14<List<SgBPhyInNoticesResult>> queryPhyInNotices(Long sourceBillId, Integer sourceBillType, User user){
        SgPhyInNoticesSelectBySourceCmd phyInQueryCmd = (SgPhyInNoticesSelectBySourceCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                SgPhyInNoticesSelectBySourceCmd.class.getName(), "sg", "1.0");
        SgPhyInNoticesSelectBySourceRequest request=new SgPhyInNoticesSelectBySourceRequest();
        request.setSourceBillId(sourceBillId);
        request.setSourceBillType(sourceBillType);
        request.setLoginUser(user);
        return phyInQueryCmd.selectBySource(request);
    }
}
