package com.jackrain.nea.sg.transfer.services;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.Feature;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferItemMapper;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.query.DefaultWebEvent;
import com.jackrain.nea.web.query.QuerySession;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

@Slf4j
@Component
public class ScTransferMatrixServices {


    ValueHolder queryMatrix(QuerySession session) {
        ValueHolder vh = new ValueHolder();
        DefaultWebEvent event = session.getEvent();
        JSONObject searchdata = JSON.parseObject(JSON.toJSONStringWithDateFormat(event.getParameterValue("PARAM"), "yyyy-MM-dd HH:mm:ss", SerializerFeature.WriteMapNullValue), Feature.OrderedField);
        JSONObject fixedcolumns = searchdata.getJSONObject("fixedcolumns");
        if (fixedcolumns == null) {
            throw new NDSException("查询时需传入对应数据！");
        }

        Long saleId = fixedcolumns.getLong("SC_B_TRANSFER_ID");

        String strStartIndex = searchdata.getString("startindex");
        String strPageSize = searchdata.getString("range");

        if (strStartIndex == null) {
            strStartIndex = "0";
        }
        if (strPageSize == null) {
            strPageSize = "10";
        }

        ScBTransferItemMapper itemMapper = ApplicationContextHandle.getBean(ScBTransferItemMapper.class);

        JSONArray row = new JSONArray();
        List<HashMap> result = itemMapper.selectByCondition(saleId);

        BigDecimal startIndex = new BigDecimal(strStartIndex);
        BigDecimal pageSize = new BigDecimal(strPageSize);
        int start = startIndex.intValue();
        int end = startIndex.add(pageSize).intValue();
        int count = start;

        Integer totalRowCount = result.size();

        for (int i = startIndex.intValue(); i < result.size(); i++) {
            if (count >= start && count < end) {
                StringBuffer strJson = new StringBuffer();
                strJson.append("{");

                result.get(i).forEach((k, v) -> {
                    String key = k.toString();
                    key = key.toUpperCase();
                    if ("QTY".equals(key)) {
                        BigDecimal value = new BigDecimal(v.toString());
                        strJson.append(key).append(":{val:'").append(value.setScale(0, BigDecimal.ROUND_HALF_UP).toString()).append("'},");
                    } else if ("AMT_LIST".equals(key)) {
                        BigDecimal value = new BigDecimal(v.toString());
                        strJson.append(key).append(":{val:'").append(value.setScale(2, BigDecimal.ROUND_HALF_UP).toString()).append("'},");
                    } else if ("PRICE_LIST".equals(key)) {
                        BigDecimal value = new BigDecimal(v.toString());
                        strJson.append(key).append(":{val:'").append(value.setScale(2, BigDecimal.ROUND_HALF_UP).toString()).append("'},");
                    } else {
                        strJson.append(key).append(":{val:'").append(v).append("'},");
                    }
                });

                strJson.append("CONTROL:{val:").append("'查看日志'").append("}");
                strJson.append("}");
                row.add(JSON.parseObject(strJson.toString()));
            } else {
                break;
            }
            count++;
        }

        JSONObject data = new JSONObject();
        data.put("start", strStartIndex);
        data.put("totalRowCount", totalRowCount);
        data.put("rowCount", pageSize);
        data.put("row", row);

        vh.put("code", 0);
        vh.put("data", data);
        vh.put("message", "成功");
        return vh;


    }
}
