package com.jackrain.nea.sg.transfer.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.jackrain.nea.ps.api.result.PsCProResult;
import com.jackrain.nea.sg.transfer.model.table.ScBTransferItem;
import lombok.Data;

import java.io.Serializable;

/**
 * @author leexh
 * @since 2019/8/1 11:50
 * desc:
 */
@Data
public class AcTransferItemRequest extends ScBTransferItem implements Serializable {

    /**
     * 商品信息
     */
    @JSONField(name = "PRO_LABEL_JSON")
    private PsCProResult proLabelJson;

}
