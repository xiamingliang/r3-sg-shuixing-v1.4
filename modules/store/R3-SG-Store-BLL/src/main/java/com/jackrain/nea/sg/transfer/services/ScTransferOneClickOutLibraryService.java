package com.jackrain.nea.sg.transfer.services;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.Feature;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.cpext.api.CpcPhyWareHouseQueryCmd;
import com.jackrain.nea.cpext.api.QueryStoreListCmd;
import com.jackrain.nea.cpext.utils.ValueHolderUtils;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.mapper.CpCPhyWarehouseMapper;
import com.jackrain.nea.sg.basic.model.table.CpCPhyWarehouse;
import com.jackrain.nea.sg.out.api.SgPhyOneClickOutLibraryCmd;
import com.jackrain.nea.sg.out.model.request.SgPhyOneClickOutLibraryRequest;
import com.jackrain.nea.sg.transfer.common.ScTransferConstants;
import com.jackrain.nea.sg.transfer.enums.SgTransferBillStatusEnum;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferMapper;
import com.jackrain.nea.sg.transfer.model.table.ScBTransfer;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.query.DefaultWebEvent;
import com.jackrain.nea.web.query.QuerySession;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author: 汪聿森
 * @Date: Created in 2019-09-12 14:10
 * @Description : 调拨单一键出库
 */
@Slf4j
@Component
public class ScTransferOneClickOutLibraryService {

    @Autowired
    private ScBTransferMapper scBTransferMapper;
    @Reference(group = "cp-ext", version = "1.0")
    private QueryStoreListCmd queryStoreListCmd;
    @Reference(group = "cp-ext", version = "1.0")
    private CpcPhyWareHouseQueryCmd cpcPhyWareHouseQueryCmd;
    @Reference(group = "sg", version = "1.0")
    private SgPhyOneClickOutLibraryCmd sgPhyOneClickOutLibraryCmd;

    @Transactional(rollbackFor = Exception.class)
    public ValueHolder transferOneClickOutLibrary(QuerySession session) {
        ValueHolder vh = new ValueHolder();
        DefaultWebEvent event = session.getEvent();
        JSONObject param = JSON.parseObject(JSON.toJSONStringWithDateFormat(event.getParameterValue("param"),
                "yyyy-MM-dd HH:mm:ss", SerializerFeature.WriteMapNullValue), Feature.OrderedField);
        if (log.isDebugEnabled()) {
            log.info("ScTransferOneClickOutLibraryService 参数:" + JSONObject.toJSONString(param));
        }
        if (param == null || param.isEmpty()) {
            return ValueHolderUtils.fail(Resources.getMessage("参数不能为空！", session.getUser().getLocale()));
        }
        // 动作定义列表页面传参为数组，单对象为单个单据
        Boolean isObj = true;
        JSONArray ids = new JSONArray();
        if (param.containsKey("ids")) {
            ids = param.getJSONArray("ids");
            isObj = false;
        } else {
            ids.add(param.getLong("objid"));
        }
        if (CollectionUtils.isEmpty(ids)) {
            return ValueHolderUtils.fail(Resources.getMessage("请选择要一键出库的数据！", session.getUser().getLocale()));
        }
        JSONArray errorArr = new JSONArray();
        Integer accSuccess = 0, accFailed = 0;
        List listData = new ArrayList();
        for (int i = 0; i < ids.size(); i++) {
            Long objId = ids.getLong(i);
            ScBTransfer scBTransfer = scBTransferMapper.selectById(objId);
            if (scBTransfer == null) {
                accFailed = errorRecord(objId, "当前记录已不存在!", errorArr, accFailed);
                continue;
            }
            //	如果本单的【单据状态】<>已审核未出库，则不允许一键出库
            Integer status = scBTransfer.getStatus();
            if (status != null && status != SgTransferBillStatusEnum.AUDITED_NOT_OUT.getVal()) {
                accFailed = errorRecord(objId, "当前单据不是已审核未出库状态，不允许一键出库", errorArr, accFailed);
                continue;
            }
            //	如果本单的【拣货状态】<> 未拣货，则不允许一键出库
            Integer pickStatus = scBTransfer.getPickStatus();
            if (pickStatus != null && pickStatus != ScTransferConstants.PICK_STATUS_UNPICKED) {
                accFailed = errorRecord(objId, "当前单据拣货状态不是未拣货，不允许一键出库", errorArr, accFailed);
                continue;
            }
            //一键出库状态
            Long oneClickOutLibraryStatus = scBTransfer.getReserveBigint01();
            if (oneClickOutLibraryStatus != null && (oneClickOutLibraryStatus.equals(ScTransferConstants.TYPE_LIBRARY_STATUS_OUTSTRAGEING) || oneClickOutLibraryStatus.equals(ScTransferConstants.TYPE_LIBRARY_STATUS_SUCCESS_OUTSTRAGE))) {
                accFailed = errorRecord(objId, "当前单据一键出库状态为出库中或者出库完成，不允许一键出库", errorArr, accFailed);
                continue;
            }
            //	如果本单的发货店仓为WMS管控仓，则不允许一键出库
            try {
                Long cpCStoreId = scBTransfer.getCpCOrigId();

                CpCPhyWarehouseMapper warehouseMapper = ApplicationContextHandle.getBean(CpCPhyWarehouseMapper.class);
                CpCPhyWarehouse cpCPhyWarehouse = warehouseMapper.selectByStoreId(cpCStoreId);
                if(cpCPhyWarehouse==null){
                    accFailed = errorRecord(objId, "根据逻辑仓ID:"+cpCStoreId+"查询实体仓信息为空", errorArr, accFailed);
                    continue;
                }
                Integer iswms = cpCPhyWarehouse.getWmsControlWarehouse();
                if (iswms != null && ScTransferConstants.IS_PASS_WMS_Y == iswms) {
                    accFailed = errorRecord(objId, "当前单据发货店仓为WMS管控，不允许一键出库", errorArr, accFailed);
                    continue;
                }
            } catch (Exception e) {
                log.error(this.getClass().getName() + "根据发货店仓ID查询店仓信息异常：" + e);
                accFailed = errorRecord(objId, "根据发货店仓ID查询店仓信息异常：" + e, errorArr, accFailed);
            }
            //	更新【一键出库状态】=出库中
            ScBTransfer scBTransferNew = new ScBTransfer();
            scBTransferNew.setId(objId);
            scBTransferNew.setReserveBigint01(ScTransferConstants.TYPE_LIBRARY_STATUS_OUTSTRAGEING);
            if (scBTransferMapper.updateById(scBTransferNew) > 0) {
                // 统计成功条数
                accSuccess++;
                listData.add(scBTransferNew);
                //异步任务调用库存提供的新增入库结果单并审核接口
                toSgInterface(scBTransfer, session.getUser(), scBTransferNew);
            } else {
                accFailed = errorRecord(objId, "一键出库失败", errorArr, accFailed);
                continue;
            }
        }
        String message = "一键出库成功记录数：" + accSuccess;
        if (!CollectionUtils.isEmpty(errorArr)) {
            if (isObj) {
                vh = ValueHolderUtils.fail(errorArr.getJSONObject(0).getString("message"));
            } else {
                vh = ValueHolderUtils.fail(message + "，一键出库失败记录数：" + accFailed, errorArr);
            }
        } else {
            if (isObj) {
                vh = ValueHolderUtils.success("一键出库中...");
            } else {
                vh = ValueHolderUtils.success(message);
            }
        }
        return vh;
    }

    @Async
    public void toSgInterface(ScBTransfer scBTransfer, User user, ScBTransfer scBTransferNew) {
        SgPhyOneClickOutLibraryRequest sgPhyOneClickOutLibraryRequest = new SgPhyOneClickOutLibraryRequest();
        Long objId = scBTransfer.getId();
        String lockKsy = ScTransferConstants.TRANSFER_TYPE + ":" + objId;
        sgPhyOneClickOutLibraryRequest.setSourceBillId(scBTransfer.getId());
        sgPhyOneClickOutLibraryRequest.setSourceBillType(SgConstantsIF.BILL_TYPE_TRANSFER);
        sgPhyOneClickOutLibraryRequest.setLockKey(lockKsy);
        sgPhyOneClickOutLibraryRequest.setLoginUser(user);
        try {
            if (log.isDebugEnabled()) {
                log.debug("调用一键出库入参：" + sgPhyOneClickOutLibraryRequest);
            }
            ValueHolderV14 valueHolderV14 = sgPhyOneClickOutLibraryCmd.oneClickOutLibrary(sgPhyOneClickOutLibraryRequest);
            if (log.isDebugEnabled()) {
                log.debug("调用一键出库结果：" + valueHolderV14);
            }
            if (valueHolderV14.getCode() == -1) {
                scBTransferNew.setId(scBTransfer.getId());
                scBTransferNew.setReserveBigint01(ScTransferConstants.TYPE_LIBRARY_STATUS_FAIL_OUTSTRAGE);
                if (scBTransferMapper.updateById(scBTransferNew) > 0) {
                    if (log.isDebugEnabled()) {
                        log.debug("调用一键出库服务更新一键出库状态为出库失败");
                    }
                }
            }
        } catch (Exception e) {
            scBTransferNew.setId(scBTransfer.getId());
            scBTransferNew.setReserveBigint01(ScTransferConstants.TYPE_LIBRARY_STATUS_FAIL_OUTSTRAGE);
            scBTransferMapper.updateById(scBTransferNew);
            if (log.isDebugEnabled()) {
                log.debug("调用一键出库服务更新异常：" + e.getMessage());
            }
        }
    }

    /**
     * 错误数据收集
     *
     * @param objid
     * @param message
     * @param errorArr
     */
    public Integer errorRecord(Long objid, String message, JSONArray errorArr, Integer accFailed) {
        JSONObject errorDate = new JSONObject();
        errorDate.put("code", ResultCode.FAIL);
        errorDate.put("objid", objid);
        errorDate.put("message", message);
        errorArr.add(errorDate);
        return ++accFailed;
    }
}
