package com.jackrain.nea.sg.receive.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Lists;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.redis.config.CusRedisTemplate;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.model.result.SgStorageUpdateResult;
import com.jackrain.nea.sg.basic.model.table.CpCStore;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.receive.common.SgReceiveConstantsIF;
import com.jackrain.nea.sg.receive.mapper.SgBReceiveItemMapper;
import com.jackrain.nea.sg.receive.mapper.SgBReceiveMapper;
import com.jackrain.nea.sg.receive.model.request.SgReceiveBillVoidRequest;
import com.jackrain.nea.sg.receive.model.result.SgReceiveBillVoidResult;
import com.jackrain.nea.sg.receive.model.table.SgBReceive;
import com.jackrain.nea.sg.receive.model.table.SgBReceiveItem;
import com.jackrain.nea.sg.send.utils.SgStoreESUtils;
import com.jackrain.nea.sg.send.utils.SgStoreUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @author 舒威
 * @since 2019/4/28
 * create at :2019/4/28 22:58
 */
@Slf4j
@Component
public class SgReceiveVoidService {

    @Autowired
    private SgBReceiveMapper receiveMapper;

    @Autowired
    private SgBReceiveItemMapper receiveItemMapper;

    @Autowired
    private SgReceiveStorageService storageService;

    @Autowired
    private SgStoreESUtils storeESUtils;

    @Transactional(rollbackFor = Exception.class)
    public ValueHolderV14<SgReceiveBillVoidResult> voidSgBReceive(SgReceiveBillVoidRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("来源单据id[" + request.getSourceBillId()
                    + "],逻辑收货单取消入参:" + JSONObject.toJSONString(request) + ";");
        }

        ValueHolderV14<SgReceiveBillVoidResult> v14 = new ValueHolderV14<>();
        SgStoreUtils.checkR3BModelDefalut(request);
        SgStoreUtils.checkBModelDefalut(request, false);
        Long id = request.getObjId();
        User user = request.getLoginUser();
        Locale locale = user.getLocale();

        Long sourceBillId = request.getSourceBillId();
        Integer sourceBillType = request.getSourceBillType();

        CusRedisTemplate<String, String> redisTemplate = SgStoreUtils.initRedisTemplate();
        String lockKsy = SgConstants.SG_B_RECEIVE + ":" + sourceBillId + ":" + sourceBillType;

        if (redisTemplate.opsForValue().get(lockKsy) == null) {
            try {
                redisTemplate.opsForValue().set(lockKsy, "OK", 30, TimeUnit.MINUTES);
                SgBReceive receive = new SgBReceive();
                List<SgBReceiveItem> orgSgBReceiveItems = Lists.newArrayList();
                List<SgBReceiveItem> receiveItems = Lists.newArrayList();
                try {
                    if (null != id) {
                        receive = receiveMapper.selectOne(new QueryWrapper<SgBReceive>().lambda()
                                .eq(SgBReceive::getId, id));
                    } else {
                        List<SgBReceive> bReceives = receiveMapper.selectList(new QueryWrapper<SgBReceive>().lambda()
                                .eq(SgBReceive::getSourceBillId, sourceBillId)
                                .eq(SgBReceive::getSourceBillType, sourceBillType)
                                .eq(SgBReceive::getIsactive, SgConstants.IS_ACTIVE_Y));
                        if (CollectionUtils.isEmpty(bReceives)) {
                            v14.setCode(request.getIsExist() ? ResultCode.SUCCESS : ResultCode.FAIL);
                            v14.setMessage("不存在来源单据id" + sourceBillId + ",来源单据类型" + sourceBillType + "的逻辑收货单!");
                            return v14;
                        } else if (bReceives.size() > 1) {
                            v14.setCode(request.getIsExist() ? ResultCode.SUCCESS : ResultCode.FAIL);
                            v14.setMessage("存在多条来源单据id" + sourceBillId + ",来源单据类型" + sourceBillType + "的逻辑收货单!");
                            return v14;
                        } else {
                            receive = bReceives.get(0);
                        }
                    }
                    if (receive == null) {
                        v14.setCode(request.getIsExist() ? ResultCode.SUCCESS : ResultCode.FAIL);
                        v14.setMessage("当前记录已不存在！");
                        return v14;
                    } else {
                        if (SgConstants.IS_ACTIVE_N.equalsIgnoreCase(receive.getIsactive())) {
                            v14.setCode(ResultCode.FAIL);
                            v14.setMessage("当前记录已作废,不允许重复作废！");
                            return v14;
                        }
                    }

                    id = receive.getId();

                    //更新逻辑发货单表
                    receive.setTotQtyPrein(BigDecimal.ZERO);
                    receive.setIsactive(SgConstants.IS_ACTIVE_N);
                    receive.setBillStatus(SgReceiveConstantsIF.BILL_RECEIVE_STATUS_CANCELED);
                    StorageESUtils.setBModelDefalutDataByUpdate(receive, user);
                    receive.setModifierename(user.getEname());
                    receiveMapper.updateById(receive);

                    orgSgBReceiveItems = receiveItemMapper.selectList(new QueryWrapper<SgBReceiveItem>().lambda()
                            .eq(SgBReceiveItem::getSgBReceiveId, id));

                    for (SgBReceiveItem orgSgBReceiveItem : orgSgBReceiveItems) {
                        SgBReceiveItem clone = (SgBReceiveItem) orgSgBReceiveItem.clone();
                        receiveItems.add(clone);
                    }

                    orgSgBReceiveItems.forEach(orgSgBReceiveItem -> {
                        orgSgBReceiveItem.setQtyPrein(BigDecimal.ZERO);
                        StorageESUtils.setBModelDefalutDataByUpdate(orgSgBReceiveItem, user);
                        orgSgBReceiveItem.setModifierename(user.getEname());
                        receiveItemMapper.updateById(orgSgBReceiveItem);
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                    AssertUtils.logAndThrowExtra("取消逻辑收货单失败", e, locale);
                }

                Map<Long, CpCStore> negativeStock = SgStoreUtils.getNegativeStock(null, null, receiveItems, null);
                ValueHolderV14<SgStorageUpdateResult> callAPIResult = storageService.updateReceiveStoragePrein(receive, receiveItems, null, null, "void",
                        request.getLoginUser(), false, negativeStock, false, null, null, null, null, null);
                if (callAPIResult.isOK()) {
                    //check同步库存占用结果
                    SgStoreUtils.isSuccess("取消逻辑收货单失败!", callAPIResult);
                    v14.setCode(ResultCode.SUCCESS);
                    v14.setMessage("取消逻辑收货单成功");
                } else {
                    AssertUtils.logAndThrow("取消逻辑收货单失败!" + callAPIResult.getMessage(), locale);
                }

                //推送ES
                storeESUtils.pushESByReceive(id, true, false, null, receiveMapper, receiveItemMapper);
            } catch (Exception e) {
                e.printStackTrace();
                AssertUtils.logAndThrowExtra(e, locale);
            } finally {
                try {
                    redisTemplate.delete(lockKsy);
                } catch (Exception e) {
                    e.printStackTrace();
                    log.error(this.getClass().getName() + lockKsy + "释放锁失败，请联系管理员！");
                    AssertUtils.logAndThrow("释放锁失败，请联系管理员！");
                }
            }
        } else {
            v14.setCode(ResultCode.FAIL);
            v14.setMessage("取消逻辑收货单，请稍后重试！");
        }

        if (log.isDebugEnabled()) {
            log.debug("来源单据id[" + request.getSourceBillId()
                    + "],逻辑收货单取消出参:" + v14.toJSONObject().toJSONString() + ";");
        }
        return v14;
    }
}
