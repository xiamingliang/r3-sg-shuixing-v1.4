package com.jackrain.nea.sg.transfer.mapper;

import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.transfer.model.table.ScBTransferItem;
import com.jackrain.nea.web.face.User;
import org.apache.ibatis.annotations.*;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Mapper
public interface ScBTransferItemMapper extends ExtentionMapper<ScBTransferItem> {

    /**
     * 矩阵明细查询
     *
     * @param id 主表id
     * @return 矩阵明细
     */
    @Select("SELECT   MAX (PS_C_PRO_ID) AS PS_C_PRO_ID, " +
            "   MAX (PS_C_PRO_ECODE) AS PS_C_PRO_ECODE, " +
            "   MAX (PS_C_PRO_ENAME) AS PS_C_PRO_ENAME, " +
            "   AVG (PRICE_LIST) AS PRICE_LIST, " +
            "   COALESCE (SUM(QTY), 0) AS QTY, " +
            "   COALESCE (SUM(amt_list), 0) AS AMT_LIST" +
            " FROM sc_b_transfer_item WHERE sc_b_transfer_id = #{id} GROUP BY ps_c_pro_ecode")
    List<HashMap> selectByCondition(@Param("id") Long id);


    /**
     * 调拨单 调拨总数查询
     *
     * @param id 主表id
     * @return sum
     */
    @Select("select sum(qty) as qty from sc_b_transfer_item WHERE  sc_b_transfer_id = #{id}  ")
    BigDecimal selectQtySum(@Param("id") Long id);


    /**
     * 调拨单 出库总数查询
     *
     * @param id 主表id
     * @return sum
     */
    @Select("select sum(qty_out) as qty_out from sc_b_transfer_item WHERE  sc_b_transfer_id = #{id}  ")
    BigDecimal selectQtyOutSum(@Param("id") Long id);


    /**
     * 查询 调拨数量 出库数量 入库数量的 汇总
     *
     * @param id 主表id
     * @return 调拨数量 出库数量 入库数量的 汇总
     */
    @Select("SELECT\n" +
            "\tSUM (qty) AS qty,\n" +
            "\tSUM (qty_out) AS qty_out,\n" +
            "\tSUM (qty_in) AS qty_in,\n" +
            "\tSUM (qty_diff) AS qty_diff,\n" +
            "\tSUM (amt_list) AS amt_list,\n" +
            "\tSUM (amt_out_list) AS amt_out_list,\n" +
            "\tSUM (amt_in_list) AS amt_in_list\n" +
            "FROM\n" +
            "\tsc_b_transfer_item\n" +
            "WHERE\n" +
            "\tsc_b_transfer_id = #{id}")
    HashMap<String, BigDecimal> selectsAllQtySum(@Param("id") Long id);


    @Update("UPDATE sc_b_transfer_item " +
            "SET qty_diff = 0, " +
            " qty_in = qty, " +
            " qty_out = qty, " +
            " modifieddate = #{date}, " +
            " modifierename = #{user.ename}, " +
            " modifierid = #{user.id}, " +
            " modifiername =  #{user.name}, " +
            " amt_in_list = amt_list, " +
            " amt_out_list = amt_list " +
            "WHERE " +
            " sc_b_transfer_id =  #{id}")
    int updateQtyOutInByTransferId(@Param("id") Long id, @Param("user") User user, @Param("date") Date date);

    /**
     * 合计统计字段
     *
     * @param transferId 调拨单ID
     * @return
     */
    @Select("SELECT\n" +
            "\tSUM(qty) AS qty,\n" +
            "\tSUM(qty_out) AS qty_out,\n" +
            "\tSUM(qty_in) AS qty_in,\n" +
            "\tSUM(qty_diff) AS qty_diff,\n" +
            "\tSUM(amt_list) AS amt_list,\n" +
            "\tSUM(amt_out_list) AS amt_out_list,\n" +
            "\tSUM(amt_in_list) AS amt_in_list\n" +
            "FROM\n" +
            "\tsc_b_transfer_item\n" +
            "WHERE\n" +
            "\tsc_b_transfer_id = #{transferId}")
    ScBTransferItem selectSumQtyAndAmtByTransferId(@Param("transferId") Long transferId);

    /**
     * 分页查询逻辑发货单
     */
    @Select("SELECT * FROM " + SgConstants.SG_B_TRANSFER_ITEM + " ORDER BY ID LIMIT #{page} OFFSET #{offset}")
    List<ScBTransferItem> selectListByPage(@Param("page") int page, @Param("offset") int offset);


    /**
     * 分页查询调拨单明细
     */
    @Select("SELECT * FROM sc_b_transfer_item WHERE sc_b_transfer_id = #{objId} ORDER BY ID LIMIT #{page} OFFSET #{offset}")
    List<ScBTransferItem> selectItemListByPage(@Param("objId") Long objId, @Param("page") int page, @Param("offset") int offset);

    /**
     * 根据 外键 删除 调拨单 条码明细
     * @param id
     * @return
     */
    @Delete(" delete from sc_b_transfer_item where sc_b_transfer_id = #{id}")
    int deleteItemListByFerId(@Param("id") Long id);

}