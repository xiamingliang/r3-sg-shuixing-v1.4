package com.jackrain.nea.sg.transfer.common;

/**
 * @author csy
 */
public interface ScTransferConstants {

    String TRANSFER_INDEX = "sc_b_transfer";

    String TRANSFER_TYPE = "sc_b_transfer";

    String TRANSFER_ITEM_TYPE = "sc_b_transfer_item";

    String OUT_INDEX = "sc_b_out";

    String OUT_TYPE = "sc_b_out";

    String OUT_ITEM_TYPE = "sc_b_out_item";


    String PICK_STATUS = "PICK_STATUS";

    String OUT_STATUS = "OUT_STATUS";

    String IN_STATUS = "IN_STATUS";

    /**
     * 调拨出库单-双方确认（调拨单视图）
     */
    String SC_B_TRANSFER_OUT = "SC_B_TRANSFER_OUT";

    /**
     * 调拨出库单-直接调拨（调拨单视图）
     */
    String SC_B_TRANSFER_OUT_DIRECTLY = "SC_B_TRANSFER_OUT_DIRECTLY";


    /**
     * 未拣货
     */
    int PICK_STATUS_UNPICKED = 1;

    /**
     * 拣货中
     */
    int PICK_STATUS_PICKING = 2;

    /**
     * 已拣货
     */
    int PICK_STATUS_PICKED = 3;

    /**
     * es父ID的key
     */
    String TRANSFER_PARENT_FIELD = "SC_B_TRANSFER_ID";


    String TRANSFER_AUDIT_CHECK_POINT = "";


    int TRANSFER_TYPE_NORMAL = 1;

    int TRANSFER_TYPE_ORDER = 2;


    /**
     * 系统参数
     * 生成逻辑收货单节点
     */
    String SYSTEM_RECEIVE = "sg_b_receive.addtime";
    String SYSTEM_TOB = "toB.defaultLogistics";
    //是否自动生成入库拣货单
    String SYSTEM_AUTO_PICKNO = "sg.pickorder.enable";


    /**
     * 生成逻辑收货单节点 动作
     */
    String ACTION_SUBMIT = "SUBMIT";
    String ACTION_OUT = "OUT";
    String ACTION_IN = "IN";

    /**
     * 发货订单-单据状态
     */
    int SEND_OUT_STATUS_WAIT = 1;   // 未审核
    int SEND_OUT_STATUS_AUDIT = 2;  // 已审核
    int SEND_OUT_STATUS_CASE = 3;   // 已结案
    int SEND_OUT_STATUS_VOID = 4;   // 已作废

    /**
     * 是否自动出入库
     * 0=否
     * 1=是
     */
    String IS_AUTO_N = "N";
    String IS_AUTO_Y = "Y";

    /**
     * mq发送结果
     * 1=待发
     * 2=发送成功
     * 3=发送失败
     */
    int SEND_MQ_SUCCESS = 2;
    int SEND_MQ_FAIL = 3;

    /**
     * 是否传wms
     * 0=否
     * 1=是
     */
    int IS_PASS_WMS_Y = 1;

    /**
     * 调拨差异关联字段
     */
    String SC_B_TRANSFER_DIFF_ID = "SC_B_TRANSFER_DIFF_ID";

    String PRO_LABEL_JSON = "pro_label_json";

    /**
     * 锁单状态
     * 0=未锁单
     * 1=锁单
     */
    int IS_UNLOCKED = 0;
    int IS_LOCKED = 1;

    //一键出库状态
    Long TYPE_LIBRARY_STATUS_WAIT_OUTSTRAGE = 0L; //待出库
    Long TYPE_LIBRARY_STATUS_OUTSTRAGEING = 1L; //出库中
    Long TYPE_LIBRARY_STATUS_SUCCESS_OUTSTRAGE = 2L; //出库成功
    Long TYPE_LIBRARY_STATUS_FAIL_OUTSTRAGE = 3L; //出库失败

    //一键入库状态
    Long TYPE_LIBRARY_STATUS_WAIT_INSTRAGE = 0L; //待入库
    Long TYPE_LIBRARY_STATUS_INSTRAGEING = 1L; //入库中
    Long TYPE_LIBRARY_STATUS_SUCCESS_INSTRAGE = 2L; //入库成功
    Long TYPE_LIBRARY_STATUS_FAIL_INSTRAGE = 3L; //入库失败

    // 批量插入 条码明细 表的 最大数量
    Long INSERT_ITEMS_NUM = 200L;

    // 批量插入 箱内条码明细 表的 最大数量
    Long INSERT_TEUS_NUM = 200L;

    /**
     * 是否配码  IsMatchsize
     */
    int IS_UNMATCHSIZE = 0;
    int IS_MATCHSIZE = 1;

    // 箱类型 是否散码
    String TEUS_TYPE_SM = "1";//散码
    String TEUS_TYPE_AX = "2";//按箱
    String TEUS_TYPE_HZ = "3";//混装

    /**
     * 经营类型：集团直营
     */
    String MANAGE_TYPE_CODE_DIRECTLY = "01";
}
