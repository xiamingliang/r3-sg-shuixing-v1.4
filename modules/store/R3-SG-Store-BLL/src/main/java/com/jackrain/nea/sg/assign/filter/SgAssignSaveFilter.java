package com.jackrain.nea.sg.assign.filter;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.cpext.model.request.CpStoreCustomerRequest;
import com.jackrain.nea.cpext.model.table.CpCStore;
import com.jackrain.nea.cpext.model.table.CpCustomer;
import com.jackrain.nea.oc.basic.cache.CommonCacheValUtils;
import com.jackrain.nea.sg.assign.common.SgAssignConstantsIF;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.tableService.DbRowAction;
import com.jackrain.nea.tableService.MainTableRecord;
import com.jackrain.nea.tableService.SubTableRecord;
import com.jackrain.nea.tableService.TableServiceContext;
import com.jackrain.nea.tableService.filter.BaseFilter;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Locale;

/**
 * @author : 孙俊磊
 * @since : 2019-10-11
 * create at : 2019-10-11 10:43 AM
 * 保存铺货单
 */
@Slf4j
@Component
public class SgAssignSaveFilter extends BaseFilter {
    @Override
    public void before(TableServiceContext context, MainTableRecord row) {

        Locale locale = context.getLocale();
        JSONObject commitData = row.getCommitData();
        if (row.getAction().equals(DbRowAction.INSERT)) {

            // 新增时补充信息
            commitData.put("STATUS", SgAssignConstantsIF.ASSIGN_STATUS_WAIT);
            if (commitData.getDate("BILL_DATE") == null) {
                commitData.put("BILL_DATE", new Date());
            }

            // 补充门店经销商信息
            Long origId = commitData.getLong("CP_C_ORIG_ID");
            AssertUtils.notNull(origId, "发货店仓不能为空！", locale);
            checkCustomerEqual(origId, commitData, false, locale);

        } else if (row.getAction().equals(DbRowAction.UPDATE)) {

            Long origStoreId = commitData.getLong("CP_C_ORIG_ID");
            if (origStoreId != null) {
                checkCustomerEqual(origStoreId, commitData, false, locale);
            }
        }
        //计算折扣和补充收货店仓code,name
        setImpItemData(row, context.getLocale());
    }

    private void setImpItemData(MainTableRecord row, Locale locale) {

        // 补充明细订单价和折扣 默认为0
        if (MapUtils.isNotEmpty(row.getSubTables())) {
            SubTableRecord subTableRecord = row.getSubTables().get(SgConstants.SG_B_ASSIGN_IMP_ITEM.toUpperCase());
            if (subTableRecord != null && CollectionUtils.isNotEmpty(subTableRecord.getRows())) {
                subTableRecord.getRows().forEach(item -> {
                    JSONObject commitData = item.getCommitData();

                    // 吊牌价、数量
                    BigDecimal priceList = item.getNewData().getBigDecimal("PRICE_LIST");
                    BigDecimal qty = commitData.getBigDecimal("QTY");
                    if (DbRowAction.INSERT.equals(item.getAction())) {

                        // 此处暂时单个查询，后期需优化改批量，缺少批量查询店仓和经销商接口
                        Long destId = commitData.getLong("CP_C_DEST_ID");
                        AssertUtils.notNull(destId, "收货店仓不能为空！", locale);
                        checkCustomerEqual(destId, commitData, true, locale);

                        // todo 订单价取值逻辑待定，暂时默认给0
                        if (commitData.getBigDecimal("PRICE_ORDER") == null) {
                            commitData.put("PRICE_ORDER", BigDecimal.ZERO);
                        }
                    }

                    // 吊牌金额、订单金额
                    if (qty != null) {
                        commitData.put("AMT_LIST", qty.multiply(priceList));
                        commitData.put("AMT_ORDER", commitData.getBigDecimal("PRICE_ORDER") != null ?
                                qty.multiply(commitData.getBigDecimal("PRICE_ORDER")) :
                                qty.multiply(item.getOrignalData().getBigDecimal("PRICE_ORDER")));
                    }


                    // 计算折扣
                    BigDecimal priceOrder = commitData.getBigDecimal("PRICE_ORDER");
                    if (priceOrder != null) {
                        if (priceList != null && priceList.compareTo(BigDecimal.ZERO) != 0) {
                            commitData.put("DISCOUNT", priceOrder.divide(priceList, 2, BigDecimal.ROUND_HALF_UP));
                        } else {
                            commitData.put("DISCOUNT", BigDecimal.ZERO);
                        }
                    }
                });
            }
        }
    }

    /**
     * 补充门店和经销商信息
     *
     * @param storeId    店仓
     * @param commitData 入参
     * @param locale     国际化
     */
    private void checkCustomerEqual(Long storeId, JSONObject commitData, boolean isItem, Locale locale) {
        CommonCacheValUtils commonUtils = ApplicationContextHandle.getBean(CommonCacheValUtils.class);
        CpStoreCustomerRequest request = commonUtils.getStoreAndCustomer(storeId);

        AssertUtils.cannot((request == null || request.getCpCStore() == null), "店仓不存在!", locale);
        AssertUtils.cannot((request == null || request.getCpCustomer() == null), "经销商不存在!", locale);

        CpCStore store = request.getCpCStore();
        CpCustomer customer = request.getCpCustomer();


        if (!isItem) {
            // 补充主表发货店仓、发货经销商
            commitData.put("CP_C_ORIG_ECODE", store.getEcode());
            commitData.put("CP_C_ORIG_ENAME", store.getEname());

            commitData.put("CP_C_CUSTOMERUP_ID", customer.getId());
            commitData.put("CP_C_CUSTOMERUP_ECODE", customer.getEcode());
            commitData.put("CP_C_CUSTOMERUP_ENAME", customer.getEname());
        } else {

            // 1.若发货店仓与收货店仓相同，则提示：“收货店仓与发货店仓不允许相同”
//            AssertUtils.cannot(origStoreId.equals(storeId), "收货店仓与发货店仓不允许相同", locale);

            // 补充明细收货店仓和经销商时，判断是否合法（如果发货店仓所属经销商不等于收货店仓的所属经销商，且不等于收货店仓所属上级经销商，
            //             则提示：“发货经销商需要等于收货经销商或者等于收货经销商上级经销商”）
//            AssertUtils.notNull(origCustomerUpId, "主表发货经销商为空！", locale);
//            AssertUtils.cannot(!origCustomerUpId.equals(customer.getId()) && !origCustomerUpId.toString()
//                            .equals(customer.getCpCCustomerupId()),
//                    "发货经销商需要等于收货经销商或者等于收货经销商的上级经销商", locale);

            // 补充明细收货店仓、收货经销商
            commitData.put("CP_C_DEST_ENAME", store.getEname());
            commitData.put("CP_C_DEST_ECODE", store.getEcode());

            commitData.put("CP_C_CUSTOMERDEST_ID", customer.getId());
            commitData.put("CP_C_CUSTOMERDEST_ECODE", customer.getEcode());
            commitData.put("CP_C_CUSTOMERDEST_ENAME", customer.getEname());

            commitData.put("CP_C_CUSTOMERDESTUP_ID", StringUtils.isNotEmpty(customer.getCpCCustomerupId()) ?
                    Long.valueOf(customer.getCpCCustomerupId()) : null);
            commitData.put("CP_C_CUSTOMERDESTUP_ECODE", customer.getCpCCustomerupEcode());
            commitData.put("CP_C_CUSTOMERDESTUP_ENAME", customer.getCpCCustomerupEname());
        }
    }
}
