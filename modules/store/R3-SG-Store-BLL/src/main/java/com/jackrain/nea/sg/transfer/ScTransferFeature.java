package com.jackrain.nea.sg.transfer;

import com.jackrain.nea.oc.basic.filter.BasicInfoSuppByRedisFilter;
import com.jackrain.nea.sg.transfer.filter.ScTransferEsPushFilter;
import com.jackrain.nea.sg.transfer.filter.ScTransferOrderSaveFilter;
import com.jackrain.nea.sg.transfer.filter.ScTransferSaveFilter;
import com.jackrain.nea.sg.transfer.filter.ScTransferSaveRegionFilter;
import com.jackrain.nea.sg.transfer.validate.ScCustomerEqualValidate;
import com.jackrain.nea.sg.transfer.validate.ScTransferPropValidate;
import com.jackrain.nea.sg.transfer.validate.ScTransferSaveValidate;
import com.jackrain.nea.tableService.Feature;
import com.jackrain.nea.tableService.constants.Constants;
import com.jackrain.nea.tableService.feature.FeatureAnnotation;
import com.jackrain.nea.util.ApplicationContextHandle;

/**
 * @author csy
 */
@FeatureAnnotation(value = "ScTransferFeature", description = "调拨单feature")
public class ScTransferFeature extends Feature {

    @Override
    protected void initialization() {
        //调拨单-保存check
        ScTransferSaveValidate scTransferSaveValidate = ApplicationContextHandle.getBean(ScTransferSaveValidate.class);
        addValidator(scTransferSaveValidate, (tableName, actionName) ->
                (Constants.ACTION_SAVE.equals(actionName))
                        && ("SC_B_TRANSFER".equalsIgnoreCase(tableName)
                        || "SC_B_TRANSFER_OUT".equalsIgnoreCase(tableName)
                        || "SC_B_TRANSFER_OUT_DIRECTLY".equalsIgnoreCase(tableName)));

        // 基本信息补充 oc
        BasicInfoSuppByRedisFilter suppByRedisFilter = ApplicationContextHandle.getBean(BasicInfoSuppByRedisFilter.class);
        addFilter(suppByRedisFilter, (tableName, actionName) ->
                (Constants.ACTION_ADD.equals(actionName) || Constants.ACTION_SAVE.equals(actionName))
                        && ("SC_B_TRANSFER".equalsIgnoreCase(tableName)
                        || "SC_B_TRANSFER_OUT".equalsIgnoreCase(tableName)
                        || "SC_B_TRANSFER_OUT_DIRECTLY".equalsIgnoreCase(tableName)));


        //调拨单-订单 内容
        ScTransferOrderSaveFilter scTransferOrderSaveFilter = ApplicationContextHandle.getBean(ScTransferOrderSaveFilter.class);
        addFilter(scTransferOrderSaveFilter, (tableName, actionName) ->
                (Constants.ACTION_ADD.equals(actionName) || Constants.ACTION_SAVE.equals(actionName))
                        && ("SC_B_TRANSFER".equalsIgnoreCase(tableName)
                        || "SC_B_TRANSFER_OUT".equalsIgnoreCase(tableName)
                        || "SC_B_TRANSFER_OUT_DIRECTLY".equalsIgnoreCase(tableName)));


        //调拨单 初始化赋值
        ScTransferSaveFilter scTransferSaveFilter = ApplicationContextHandle.getBean(ScTransferSaveFilter.class);
        addFilter(scTransferSaveFilter, (tableName, actionName) ->
                (Constants.ACTION_ADD.equals(actionName) || Constants.ACTION_SAVE.equals(actionName))
                        && ("SC_B_TRANSFER".equalsIgnoreCase(tableName)
                        || "SC_B_TRANSFER_OUT".equalsIgnoreCase(tableName)
                        || "SC_B_TRANSFER_OUT_DIRECTLY".equalsIgnoreCase(tableName)));

        //调拨单 初始化赋值 省市区
        ScTransferSaveRegionFilter scTransferSaveRegionFilter = ApplicationContextHandle.getBean(ScTransferSaveRegionFilter.class);
        addFilter(scTransferSaveRegionFilter, (tableName, actionName) ->
                (Constants.ACTION_ADD.equals(actionName) || Constants.ACTION_SAVE.equals(actionName))
                        && ("SC_B_TRANSFER".equalsIgnoreCase(tableName)
                        || "SC_B_TRANSFER_OUT".equalsIgnoreCase(tableName)
                        || "SC_B_TRANSFER_OUT_DIRECTLY".equalsIgnoreCase(tableName)));


        //调拨单 上级经销商相同check
        ScCustomerEqualValidate scCustomerEqualValidate = ApplicationContextHandle.getBean(ScCustomerEqualValidate.class);
        addValidator(scCustomerEqualValidate, (tableName, actionName) ->
                (Constants.ACTION_ADD.equals(actionName) || Constants.ACTION_SAVE.equals(actionName))
                        && ("SC_B_TRANSFER".equalsIgnoreCase(tableName)
                        || "SC_B_TRANSFER_OUT".equalsIgnoreCase(tableName)
                        || "SC_B_TRANSFER_OUT_DIRECTLY".equalsIgnoreCase(tableName)));

        //调拨性质 check
        ScTransferPropValidate scTransferPropValidate = ApplicationContextHandle.getBean(ScTransferPropValidate.class);
        addValidator(scTransferPropValidate, (tableName, actionName) ->
                (Constants.ACTION_ADD.equals(actionName) || Constants.ACTION_SAVE.equals(actionName)
                        || Constants.ACTION_DELETE.equals(actionName))
                        && "SG_B_TRANSFER_PROP".equalsIgnoreCase(tableName));

        //调拨单 保存后推送ES   filter
        ScTransferEsPushFilter scTransferEsPushFilter = ApplicationContextHandle.getBean(ScTransferEsPushFilter.class);
        addFilter(scTransferEsPushFilter, (tableName, actionName) ->
                (Constants.ACTION_ADD.equals(actionName) || Constants.ACTION_SAVE.equals(actionName))
                        && ("SC_B_TRANSFER".equalsIgnoreCase(tableName)
                        || "SC_B_TRANSFER_OUT".equalsIgnoreCase(tableName)
                        || "SC_B_TRANSFER_OUT_DIRECTLY".equalsIgnoreCase(tableName)));


    }
}
