package com.jackrain.nea.sg.send.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Maps;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.send.mapper.SgBSendItemMapper;
import com.jackrain.nea.sg.send.mapper.SgBSendMapper;
import com.jackrain.nea.sg.send.model.request.SgSendBillQueryRequest;
import com.jackrain.nea.sg.send.model.result.SgSendActualLackQueryResult;
import com.jackrain.nea.sg.send.model.table.SgBSend;
import com.jackrain.nea.sg.send.model.table.SgBSendItem;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author 舒威
 * @since 2019/7/18
 * create at : 2019/7/18 16:13
 */
@Slf4j
@Component
public class SgSendActualLackQueryService {

    @Autowired
    private SgBSendMapper sendMapper;

    @Autowired
    private SgBSendItemMapper sendItemMapper;

    public ValueHolderV14<SgSendActualLackQueryResult> querySgBSend(SgSendBillQueryRequest searchs) {
        if (log.isDebugEnabled()) {
            log.debug("查询逻辑发货单(实缺)服务入参:" + JSONObject.toJSONString(searchs) + ";");
        }


        ValueHolderV14<SgSendActualLackQueryResult> v14 = new ValueHolderV14<>();
        HashMap<SgBSend, HashMap<String, SgBSendItem>> results = Maps.newHashMap();

        List<Integer> billStatus = searchs.getBillStatus();
        List<Integer> sourceBillTypes = searchs.getSourceBillTypes();
        List<Long> storeIds = searchs.getStoreIds();
        List<Long> skuIds = searchs.getSkuIds();

        if (CollectionUtils.isEmpty(billStatus) ||
                CollectionUtils.isEmpty(sourceBillTypes) ||
                CollectionUtils.isEmpty(storeIds) ||
                CollectionUtils.isEmpty(skuIds)) {
            AssertUtils.logAndThrow("请确定查找范围！");
        }

        String status = StringUtils.join(billStatus, ",");
        String sourceBillType =  StringUtils.join(sourceBillTypes, ",");
        String stores = StringUtils.join(storeIds, ",");
        String skus = StringUtils.join(skuIds, ",") ;

        List<SgBSend> sends = sendMapper.selectSendList(status, sourceBillType, stores, skus);
        if (CollectionUtils.isNotEmpty(sends)) {
            Map<Long, SgBSend> sendMap = sends.stream().collect(Collectors.toMap(SgBSend::getId, send -> send));
            List<Long> ids = sends.stream().map(SgBSend::getId).collect(Collectors.toList());
            List<SgBSendItem> sendItems = sendItemMapper.selectList(new QueryWrapper<SgBSendItem>().lambda().in(SgBSendItem::getSgBSendId, ids));
            if (CollectionUtils.isNotEmpty(sendItems)) {
                HashMap<Long, HashMap<String, SgBSendItem>> sendItemMap = Maps.newHashMap();
                for (SgBSendItem sendItem : sendItems) {
                    Long id = sendItem.getSgBSendId();
                    Long storeId = sendItem.getCpCStoreId();
                    Long psCSkuId = sendItem.getPsCSkuId();
                    String key = storeId + "," + psCSkuId;
                    if (sendItemMap.containsKey(id)) {
                        HashMap<String, SgBSendItem> map = sendItemMap.get(id);
                        map.put(key, sendItem);
                    } else {
                        HashMap<String, SgBSendItem> map = Maps.newHashMap();
                        map.put(key, sendItem);
                        sendItemMap.put(id, map);
                    }
                }
                sendItemMap.forEach((k, v) -> {
                    results.put(sendMap.get(k), v);
                });
            }
        }

        v14.setCode(ResultCode.SUCCESS);
        v14.setMessage("success");
        SgSendActualLackQueryResult data = new SgSendActualLackQueryResult();
        data.setResults(results);
        v14.setData(data);
        return v14;
    }
}
