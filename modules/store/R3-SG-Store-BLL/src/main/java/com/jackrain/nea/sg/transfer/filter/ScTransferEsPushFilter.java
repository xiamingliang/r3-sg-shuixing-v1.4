package com.jackrain.nea.sg.transfer.filter;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.es.util.ElasticSearchUtil;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.config.SgStorageBoxConfig;
import com.jackrain.nea.sg.transfer.common.ScTransferConstants;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferImpItemMapper;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferItemMapper;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferMapper;
import com.jackrain.nea.sg.transfer.model.table.ScBTransfer;
import com.jackrain.nea.sg.transfer.model.table.ScBTransferImpItem;
import com.jackrain.nea.sg.transfer.model.table.ScBTransferItem;
import com.jackrain.nea.tableService.DbRowAction;
import com.jackrain.nea.tableService.MainTableRecord;
import com.jackrain.nea.tableService.RowRecord;
import com.jackrain.nea.tableService.TableServiceContext;
import com.jackrain.nea.tableService.filter.BaseFilter;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Collection;
import java.util.List;

@Component
@Slf4j
public class ScTransferEsPushFilter extends BaseFilter {

    @Autowired
    private SgStorageBoxConfig boxConfig;

    @Override
    public void after(TableServiceContext context, MainTableRecord row) {

        try {
            String index = ScTransferConstants.TRANSFER_INDEX;
            String type = ScTransferConstants.TRANSFER_ITEM_TYPE;

            // 箱功能开启时，es操作录入明细表
            if (boxConfig.getBoxEnable()) {
                type = SgConstants.SC_B_TRANSFER_IMP_ITEM;
            }

            String itemTable = type.toUpperCase();

            Long objId = row.getId();

            ScBTransferMapper mapper = ApplicationContextHandle.getBean(ScBTransferMapper.class);
            ScBTransferItemMapper itemMapper = ApplicationContextHandle.getBean(ScBTransferItemMapper.class);

            ScBTransfer bTransfer = mapper.selectById(objId);

            if (DbRowAction.DELETE == row.getAction()) {
                JSONArray ids = new JSONArray();
                if (row.getSubTables() != null && row.getSubTables().size() > 0
                        && row.getSubTables().get(itemTable) != null && org.apache.commons.collections.CollectionUtils.isNotEmpty(row.getSubTables().get(itemTable).getRows())) {
                    Collection<RowRecord> rows = row.getSubTables().get(itemTable).getRows();
                    rows.forEach(transferItem ->
                            ids.add(transferItem.getId()));
                }
                deleteById(index, type, objId, ids);
                ElasticSearchUtil.indexDocument(index, index, bTransfer, objId);
            } else {
                // 判断索引是否存在
                if (!ElasticSearchUtil.indexExists(index)) {
                    // 箱功能开启时，es操作录入明细表
                    if (boxConfig.getBoxEnable()) {
                        ElasticSearchUtil.indexCreate(ScBTransferImpItem.class, ScBTransfer.class);
                    } else {
                        ElasticSearchUtil.indexCreate(ScBTransferItem.class, ScBTransfer.class);
                    }
                }

                // 箱功能开启时，es操作录入明细表
                if (boxConfig.getBoxEnable()) {
                    ScBTransferImpItemMapper impMapper = ApplicationContextHandle.getBean(ScBTransferImpItemMapper.class);
                    List<ScBTransferImpItem> impItems = impMapper.selectList(
                            new QueryWrapper<ScBTransferImpItem>().lambda()
                                    .eq(ScBTransferImpItem::getScBTransferId, objId));
                    pushById(index, type, bTransfer, impItems);
                } else {

                    List<ScBTransferItem> items = itemMapper.selectList(
                            new QueryWrapper<ScBTransferItem>().lambda().eq(ScBTransferItem::getScBTransferId, objId));
                    pushById(index, type, bTransfer, items);
                }
            }
        } catch (Exception e) {
            log.error("调拨单 es推送失败", e);
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            String message = sw.toString().replace("\n", "<br/>").replace("\tat", "");
            throw new NDSException(this.getClass().getName() + ".error" + message);
        }
    }

    /**
     * 销售单es推送
     *
     * @param transfer 主表记录
     * @param list     子表记录
     */
    private void pushById(String index, String type, ScBTransfer transfer, List list) throws IOException {

        // 主表推送es
        if (transfer != null) {
            ElasticSearchUtil.indexDocument(index, index, transfer, transfer.getId());
        }
        // 明细推送es
        if (!CollectionUtils.isEmpty(list)) {
            // 批量
            ElasticSearchUtil.indexDocuments(index, type, list, ScTransferConstants.TRANSFER_PARENT_FIELD);
        }
    }

    /**
     * 销售单明细es删除
     *
     * @param objId 父ID
     * @param ids   子ID
     */
    private void deleteById(String index, String type, Long objId, JSONArray ids) throws IOException {
        if (!CollectionUtils.isEmpty(ids)) {
            for (int i = 0; i < ids.size(); i++) {
                String id = ids.getString(i);
                ElasticSearchUtil.delDocument(index, type, id, objId);
            }
        }
    }
}
