package com.jackrain.nea.sg.transfer.validate;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.cpext.model.request.CpStoreCustomerRequest;
import com.jackrain.nea.cpext.model.table.CpCStore;
import com.jackrain.nea.cpext.model.table.CpCustomer;
import com.jackrain.nea.oc.basic.cache.CommonCacheValUtils;
import com.jackrain.nea.oc.sale.api.OcSendOutQueryCmd;
import com.jackrain.nea.oc.sale.model.request.OcSendOutBillQueryRequest;
import com.jackrain.nea.oc.sale.model.result.OcSendOutResult;
import com.jackrain.nea.oc.sale.model.table.OcBSendOut;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.transfer.common.ScTransferConstants;
import com.jackrain.nea.sg.transfer.common.SgTransferConstantsIF;
import com.jackrain.nea.sg.transfer.services.SgTransferRpcService;
import com.jackrain.nea.sx.model.table.ClCClientOrgRelation;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.tableService.DbRowAction;
import com.jackrain.nea.tableService.MainTableRecord;
import com.jackrain.nea.tableService.TableServiceContext;
import com.jackrain.nea.tableService.validate.BaseValidator;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.utils.AssertUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * @author csy
 * 校验 单据收货店仓与发货店仓-店仓经销商相同
 */
@Slf4j
@Component
public class ScCustomerEqualValidate extends BaseValidator {

    @Autowired
    private SgTransferRpcService rpcService;

    @Override
    public void validate(TableServiceContext context, MainTableRecord currentRow) {
        Locale locale = context.getLocale();
        JSONObject commitData = currentRow.getCommitData();
        // 差异类型的调拨单不允许新增保存
        checkTransferType(commitData, locale);
        //插入时判断
        if (currentRow.getAction().equals(DbRowAction.INSERT)) {
            Long origStoreId = commitData.getLong("CP_C_ORIG_ID");
            Long destStoreId = commitData.getLong("CP_C_DEST_ID");
            Integer transferType = commitData.getInteger("TRANSFER_TYPE");
            Long transferPropId = commitData.getLong("SG_B_TRANSFER_PROP_ID");

            // 调拨单出库单时传调拨类型，默认给正常调拨
            if (transferType == null) {
                transferType = SgTransferConstantsIF.TRANSFER_TYPE_NORMAL;
                commitData.put("TRANSFER_TYPE", transferType);
            }

            if (transferType == SgTransferConstantsIF.TRANSFER_TYPE_ORDER) {
                Long sendId = commitData.getLong("OC_B_SEND_OUT_ID");
                AssertUtils.notNull(sendId, "请输入发货订单！", locale);
                OcBSendOut sendOut = querySendOut(sendId, locale);
                checkOrderCus(origStoreId, destStoreId, sendOut.getCpCOrigId(), sendOut.getCpCDestId(), locale, currentRow);
                origStoreId = sendOut.getCpCOrigId();
            } else {
                checkNormalCus(origStoreId, destStoreId, locale, currentRow);
            }
//            checkTransProp(origStoreId, transferPropId, locale);
        } else if (currentRow.getAction().equals(DbRowAction.UPDATE)) {
            if (commitData.containsKey("CP_C_ORIG_ID") && commitData.getLong("CP_C_ORIG_ID") == null) {
                AssertUtils.logAndThrow("发货店仓不能为空！", context.getLocale());
            }
            if (commitData.containsKey("CP_C_DEST_ID") && commitData.getLong("CP_C_DEST_ID") == null) {
                AssertUtils.logAndThrow("收货店仓不能为空！", context.getLocale());
            }

            JSONObject newData = currentRow.getMainData().getNewData();
            log.debug("step1->newData:" + newData.toJSONString());
            log.debug("step1->commitData:" + commitData.toJSONString());
            Long commitOrig = commitData.getLong("CP_C_ORIG_ID");
            Long commitDest = commitData.getLong("CP_C_DEST_ID");
            Integer commitType = commitData.getInteger("TRANSFER_TYPE");
            if (commitType == null) {
                commitType = currentRow.getOrignalData().getInteger("TRANSFER_TYPE");
            }

            boolean isCheckNor =
                    (newData.getInteger("TRANSFER_TYPE") == ScTransferConstants.TRANSFER_TYPE_NORMAL)
                            && (commitOrig != null || commitDest != null);

            if (commitType == ScTransferConstants.TRANSFER_TYPE_ORDER) {
                Long sendId = commitData.getLong("OC_B_SEND_OUT_ID");
                // 2019-07-13添加逻辑：
                if (sendId == null) {
                    sendId = currentRow.getOrignalData().getLong("OC_B_SEND_OUT_ID");
                }
                AssertUtils.notNull(sendId, "请输入发货订单！", locale);
                OcBSendOut sendOut = querySendOut(sendId, locale);
                checkOrderCus(commitOrig, commitDest, sendOut.getCpCOrigId(), sendOut.getCpCDestId(), locale, currentRow);
            } else if (isCheckNor) {
                checkNormalCus(newData.getLong("CP_C_ORIG_ID"), newData.getLong("CP_C_DEST_ID"), locale, currentRow);
            }

            Long clientId = commitData.getLong("CP_C_CLIENT_ID");
            String clientCode = commitData.getString("CP_C_CLIENT_ECODE");
            if (clientId != null && StringUtils.isEmpty(clientCode)) {
                ClCClientOrgRelation clientInfo = rpcService.queryClientOrgRelationById(clientId);
                AssertUtils.notNull(clientInfo, "送货客户不存在，请检查！", locale);

                // 补充送货客户信息
                commitData.put("CP_C_CLIENT_ID", clientInfo.getCpCClientId());
                commitData.put("CP_C_CLIENT_ECODE", clientInfo.getCpCClientEcode());
                commitData.put("CP_C_CLIENT_ENAME", clientInfo.getCpCClientEname());
            }

            //调拨性质或者逻辑仓发生了变化
//            log.debug("step2->newData:" + newData.toJSONString());
//            log.debug("step2->commitData:" + commitData.toJSONString());
//            Long transferPropId = newData.getLong("SG_B_TRANSFER_PROP_ID");
//            Long origStoreId = newData.getLong("CP_C_ORIG_ID");
//            if (transferPropId != null || commitOrig != null) {
//                checkTransProp(origStoreId, transferPropId, locale);
//            }
        }
    }


    /**
     * check调拨性质
     *
     * @param origStoreId    发货仓id
     * @param transferPropId 调拨性质id
     * @param locale         local
     */
    private void checkTransProp(Long origStoreId, Long transferPropId, Locale locale) {
        log.debug("发货店仓" + origStoreId + ",调拨性质" + transferPropId);
        CommonCacheValUtils cacheValUtils = ApplicationContextHandle.getBean(CommonCacheValUtils.class);
        CpStoreCustomerRequest origInfo = cacheValUtils.getStoreAndCustomer(origStoreId);
        if (origInfo == null || origInfo.getCpCStore() == null) {
            AssertUtils.logAndThrow("发货店仓不存在！", locale);
        }
        String storetype = origInfo.getCpCStore().getStoretype();
        if (StringUtils.isEmpty(storetype)) {
            AssertUtils.logAndThrow("发货店仓 店仓类型为空", locale);
        }
        if (SgConstantsIF.STORE_TYPE_CC.equals(storetype) && (transferPropId == null || !transferPropId.equals(SgTransferConstantsIF.TRANSFER_PROP_CP))) {
            AssertUtils.logAndThrow("次品仓调拨，调拨性质需设置为次品调拨", locale);
        }
        if (SgConstantsIF.STORE_TYPE_ZP.equals(storetype) && transferPropId != null && transferPropId.equals(SgTransferConstantsIF.TRANSFER_PROP_CP)) {
            AssertUtils.logAndThrow("当前发货店仓为正品仓，不予许设置调拨性质为次品调拨", locale);
        }
    }

    /**
     * check正常调拨单
     *
     * @param origStoreId 发货仓id
     * @param desStoreId  收货仓id
     * @param locale      local
     * @param currentRow  row
     */
    private void checkNormalCus(Long origStoreId, Long desStoreId, Locale locale, MainTableRecord currentRow) {
        AssertUtils.notNull(origStoreId, "发货店仓不能为空!", locale);
        AssertUtils.notNull(desStoreId, "收货店仓不能为空!", locale);
        if (currentRow.getAction().equals(DbRowAction.UPDATE)) {
            currentRow.getCommitData().put("CP_C_ORIG_ID", origStoreId);
            currentRow.getCommitData().put("CP_C_DEST_ID", desStoreId);
        }
        checkCustomer(origStoreId, desStoreId, null, null, locale, currentRow, false);
    }


    /**
     * check订单类型调拨单
     *
     * @param origStoreId 发货仓id
     * @param desStoreId  收货仓id
     * @param locale      local
     * @param currentRow  row
     * @param orderDestId 发货订单发货仓id
     * @param orderOrigId 发货订单收货仓id
     */
    private void checkOrderCus(Long origStoreId, Long desStoreId, Long orderOrigId, Long orderDestId,
                               Locale locale, MainTableRecord currentRow) {
        AssertUtils.notNull(orderOrigId, "订单发货店仓不能为空!", locale);
        AssertUtils.notNull(orderDestId, "订单收货店仓不能为空!", locale);
        // 2019-07-25添加逻辑：保存时收发货店仓为空时取发货订单的收发货店仓
        if (origStoreId == null) {
            if (currentRow.getAction().equals(DbRowAction.INSERT)) {
                currentRow.getCommitData().put("CP_C_ORIG_ID", orderOrigId);
                origStoreId = orderOrigId;
            } else if (currentRow.getAction().equals(DbRowAction.UPDATE)) {
                Long origId = currentRow.getOrignalData().getLong("CP_C_ORIG_ID");
                currentRow.getCommitData().put("CP_C_ORIG_ID", origId);
                origStoreId = origId;
            }
        }

        if (desStoreId == null) {
            if (currentRow.getAction().equals(DbRowAction.INSERT)) {
                currentRow.getCommitData().put("CP_C_DEST_ID", orderDestId);
                desStoreId = orderDestId;
            } else if (currentRow.getAction().equals(DbRowAction.UPDATE)) {
                Long destId = currentRow.getOrignalData().getLong("CP_C_DEST_ID");
                currentRow.getCommitData().put("CP_C_DEST_ID", destId);
                desStoreId = destId;
            }
        }

        AssertUtils.notNull(origStoreId, "发货店仓不能为空!", locale);
        AssertUtils.notNull(desStoreId, "收货店仓不能为空!", locale);
        checkCustomer(origStoreId, desStoreId, orderOrigId, orderDestId, locale, currentRow, true);
    }


    private void checkCustomer(Long origStoreId, Long desStoreId, Long orderOrigId, Long orderDestId,
                               Locale locale, MainTableRecord currentRow, boolean isOrder) {
        AssertUtils.cannot(origStoreId.equals(desStoreId), "收货店仓与发货店仓不能为同一店仓！", locale);
        CommonCacheValUtils cacheValUtils = ApplicationContextHandle.getBean(CommonCacheValUtils.class);
        CpStoreCustomerRequest origInfo = cacheValUtils.getStoreAndCustomer(origStoreId);
        CpStoreCustomerRequest destInfo = cacheValUtils.getStoreAndCustomer(desStoreId);
        if (origInfo == null || origInfo.getCpCStore() == null) {
            AssertUtils.logAndThrow("发货店仓不存在!", locale);
        }
        if (destInfo == null || destInfo.getCpCStore() == null) {
            AssertUtils.logAndThrow("收货店仓不存在!", locale);
        }
        CpCStore origStore = origInfo.getCpCStore();
        CpCStore destStore = destInfo.getCpCStore();
        if (origInfo.getCpCustomer() == null || origStore.getCpCCustomerId() == null) {
            AssertUtils.logAndThrow("发货店仓经销商不存在", locale);
        }
        if (destInfo.getCpCustomer() == null || destStore.getCpCCustomerId() == null) {
            AssertUtils.logAndThrow("收货店仓经销商不存在!", locale);
        }

        // 2020-04-14注释逻辑
//        AssertUtils.isTrue(origStore.getCpCCustomerId().equals(destStore.getCpCCustomerId()), "收货店仓与发货店仓必须为同一经销商！", locale);

        if (isOrder) {
            if (!origStoreId.equals(orderOrigId)) {
                CpStoreCustomerRequest orderOrigInfo = cacheValUtils.getStoreAndCustomer(orderOrigId);
                if (orderOrigInfo == null || orderOrigInfo.getCpCStore() == null) {
                    AssertUtils.logAndThrow("发货订单发货店仓不存在!", locale);
                }
                CpCStore orderOrigStore = orderOrigInfo.getCpCStore();
                AssertUtils.isTrue(origStore.getCpCCustomerId().equals(orderOrigStore.getCpCCustomerId()), "发货店仓所属经销商与发货订单发货店仓所属经销商不相同，不允许保存！", locale);
            }

            if (!origStoreId.equals(orderDestId)) {
                CpStoreCustomerRequest orderDestInfo = cacheValUtils.getStoreAndCustomer(orderDestId);
                if (orderDestInfo == null || orderDestInfo.getCpCStore() == null) {
                    AssertUtils.logAndThrow("收货订单收货店仓不存在!", locale);
                }
                CpCStore orderDestStore = orderDestInfo.getCpCStore();
                AssertUtils.isTrue(origStore.getCpCCustomerId().equals(orderDestStore.getCpCCustomerId()), "收货店仓所属经销商与发货订单收货店仓所属经销商不相同，不允许保存！", locale);
            }
        }
        log.debug("当前的发货店仓信息===》" + JSONObject.toJSONString(origInfo));
        //填充冗余信息
        CpCustomer customer = origInfo.getCpCustomer();
        currentRow.getCommitData().put("CP_C_CUSTOMERUP_ID", customer.getId());
        currentRow.getCommitData().put("CP_C_CUSTOMERUP_ECODE", customer.getEcode());
        currentRow.getCommitData().put("CP_C_CUSTOMERUP_ENAME", customer.getEname());
        log.debug("当前的收货店仓信息===》" + JSONObject.toJSONString(destInfo));
        // 收货经销商
        CpCustomer customerDest = destInfo.getCpCustomer();
        currentRow.getCommitData().put("CP_C_CUSTOMER_ID", customerDest.getId());
        currentRow.getCommitData().put("CP_C_CUSTOMER_ECODE", customerDest.getEcode());
        currentRow.getCommitData().put("CP_C_CUSTOMER_ENAME", customerDest.getEname());

        currentRow.getCommitData().put("CP_C_ORIG_ENAME", origStore.getEname());
        currentRow.getCommitData().put("CP_C_ORIG_ECODE", origStore.getEcode());
        currentRow.getCommitData().put("CP_C_DEST_ENAME", destStore.getEname());
        currentRow.getCommitData().put("CP_C_DEST_ECODE", destStore.getEcode());

        // 补充：发货店仓所属法人、收货店仓所属法人
        currentRow.getCommitData().put("ORIG_BELONGS_LEGAL", origStore.getErpAccount());
        currentRow.getCommitData().put("DEST_BELONGS_LEGAL", destStore.getErpAccount());

        //水星-zhanghang-2019-12-13-start
        if (null != destStore.getIsChangeStorage() && destStore.getIsChangeStorage() != 1) {
            currentRow.getCommitData().put("IS_IN_INCREASE", "N");
        }
        if (null != origStore.getIsChangeStorage() && origStore.getIsChangeStorage() != 1) {
            currentRow.getCommitData().put("IS_OUT_REDUCE", "N");
        }

        // 送货客户信息补充
        suppClientInfo(locale, currentRow.getAction(), destInfo.getCpCStore(), currentRow.getCommitData());

        // 新增时获取发货店仓的在中“自动出库和
        if (currentRow.getAction().equals(DbRowAction.INSERT)) {
            JSONObject commitData = currentRow.getCommitData();
            String isAutoOut = commitData.getString("IS_AUTO_OUT");
            if (StringUtils.isEmpty(isAutoOut)) {
                commitData.put("IS_AUTO_OUT", origStore.getIsAutoOut());
            }

            // 新增时获取收货店仓的在中“自动入库
            String isAutoIn = commitData.getString("IS_AUTO_IN");
            if (StringUtils.isEmpty(isAutoIn)) {
                commitData.put("IS_AUTO_IN", destStore.getIsAutoIn());
            }
        }
    }

    /**
     * 获取发货订单主表信息
     */
    private OcBSendOut querySendOut(Long sendId, Locale locale) {
        OcSendOutQueryCmd queryCmd = (OcSendOutQueryCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(), OcSendOutQueryCmd.class.getName(), "oc", "1.0");
        List<OcSendOutBillQueryRequest> requestList = new ArrayList<>();
        OcSendOutBillQueryRequest request = new OcSendOutBillQueryRequest();
        request.setId(sendId);
        requestList.add(request);
        ValueHolderV14<List<OcSendOutResult>> holderV14 = queryCmd.querySendOut(requestList);
        AssertUtils.cannot(holderV14.getCode() == ResultCode.FAIL, "发货订单查询失败!:" + holderV14.getMessage(), locale);
        AssertUtils.cannot(CollectionUtils.isEmpty(holderV14.getData()), "发货订单不存在", locale);
        return holderV14.getData().get(0).getSendOut();
    }

    /**
     * 新增时判断调拨类型
     *
     * @param commitData 入参
     * @param locale     local
     */
    private void checkTransferType(JSONObject commitData, Locale locale) {
        if (!commitData.containsKey("RESERVE_VARCHAR01") ||
                StringUtils.isEmpty(commitData.getString("RESERVE_VARCHAR01"))) {
            int transferType = commitData.getIntValue("TRANSFER_TYPE");
            if (transferType == SgTransferConstantsIF.TRANSFER_TYPE_DIFF) {
                AssertUtils.logAndThrow("差异调拨类型的调拨单不允许保存！", locale);
            }
        }
    }


    /**
     * 获取送货信息
     *
     * @param storeInfo  收货店仓编码
     * @param commitData 要提交的数据
     */
    private void suppClientInfo(Locale locale, DbRowAction action, CpCStore storeInfo, JSONObject commitData) {

        if (DbRowAction.INSERT.equals(action)) {
            String storeNature = storeInfo.getStorenature();
            AssertUtils.cannot(StringUtils.isEmpty(storeNature), "收货店仓类型为空，请检查基础数据！", locale);

            String code = "STO".equalsIgnoreCase(storeNature) ? storeInfo.getEcode() : storeInfo.getCpCCustomerEcode();
            AssertUtils.cannot(StringUtils.isEmpty(code), "店仓编号/经销商编码为空，请检查！", locale);

            List<ClCClientOrgRelation> clientOrgRelations = rpcService.queryOrgRelationsByStore(code);

            // 2020-04-13修改逻辑：送货客户为空时不报错
//            AssertUtils.cannot(CollectionUtils.isEmpty(clientOrgRelations), "送货客户查询为空！" + code, locale);

            if(CollectionUtils.isNotEmpty(clientOrgRelations)){
                // 补充送货客户信息
                ClCClientOrgRelation clientInfo = clientOrgRelations.get(0);
                commitData.put("CP_C_CLIENT_ID", clientInfo.getCpCClientId());
                commitData.put("CP_C_CLIENT_ECODE", clientInfo.getCpCClientEcode());
                commitData.put("CP_C_CLIENT_ENAME", clientInfo.getCpCClientEname());
            }
        }
    }

}