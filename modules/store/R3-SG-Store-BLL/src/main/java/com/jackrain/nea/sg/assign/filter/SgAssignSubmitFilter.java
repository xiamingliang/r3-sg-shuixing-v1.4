package com.jackrain.nea.sg.assign.filter;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.oc.sale.api.OcSaleSaveAndAuditCmd;
import com.jackrain.nea.oc.sale.common.OcSaleConstantsIF;
import com.jackrain.nea.oc.sale.model.request.OcSaleBillSaveRequest;
import com.jackrain.nea.oc.sale.model.request.OcSaleImpItemSaveRequest;
import com.jackrain.nea.oc.sale.model.request.OcSaleSaveRequest;
import com.jackrain.nea.sg.assign.mapper.SgBAssignImpItemMapper;
import com.jackrain.nea.sg.assign.mapper.SgBAssignMapper;
import com.jackrain.nea.sg.assign.model.table.SgBAssign;
import com.jackrain.nea.sg.assign.model.table.SgBAssignImpItem;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.transfer.common.ScTransferConstants;
import com.jackrain.nea.sg.transfer.common.SgTransferConstantsIF;
import com.jackrain.nea.sg.transfer.model.request.SgTransferBillSaveRequest;
import com.jackrain.nea.sg.transfer.model.request.SgTransferImpItemSaveRequest;
import com.jackrain.nea.sg.transfer.model.request.SgTransferSaveRequest;
import com.jackrain.nea.sg.transfer.services.SgTransferSaveAndAuditService;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.tableService.MainTableRecord;
import com.jackrain.nea.tableService.TableServiceContext;
import com.jackrain.nea.tableService.filter.BaseFilter;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.utility.ExceptionUtil;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.assertj.core.util.Lists;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

/**
 * @author : 孙俊磊
 * @since : 2019-10-11
 * create at : 2019-10-11 11:44 AM
 * 铺货单审核
 */
@Slf4j
@Component
public class SgAssignSubmitFilter extends BaseFilter {

    @Override
    public void after(TableServiceContext context, MainTableRecord row) {

        Long objId = row.getId();
        // 根据主表【发货店仓】和子表【收货店仓】唯一条件进行拆分生成发货店仓到收货店仓的已审核未出库的调拨单或销售单

        SgBAssignMapper assignMapper = ApplicationContextHandle.getBean(SgBAssignMapper.class);
        SgBAssign assign = assignMapper.selectById(objId);
        AssertUtils.notNull(assign, "当前记录已不存在！", context.getLocale());

        // 查询明细
        List<SgBAssignImpItem> saleImpItem = null;
        List<SgBAssignImpItem> transferImpItem = null;
        try {

            SgBAssignImpItemMapper itemMapper = ApplicationContextHandle.getBean(SgBAssignImpItemMapper.class);
            List<SgBAssignImpItem> impItems = itemMapper.selectList(new QueryWrapper<SgBAssignImpItem>().lambda()
                    .eq(SgBAssignImpItem::getSgBAssignId, objId));
            if (CollectionUtils.isNotEmpty(impItems)) {
                saleImpItem = Lists.newArrayList();
                transferImpItem = Lists.newArrayList();

                // 获取主表发货店仓
                Long origCustomerId = assign.getCpCCustomerupId();
                for (SgBAssignImpItem impItem : impItems) {

                    // 明细收货经销商、收货经销商的上级经销商
                    Long customerDestId = impItem.getCpCCustomerdestId();
                    Long customerDestUpId = impItem.getCpCCustomerdestupId();
                    if (origCustomerId.equals(customerDestId)) {
                        // 同级经销商生成调拨单
                        transferImpItem.add(impItem);
                    } else if (origCustomerId.equals(customerDestUpId)) {
                        // 上下级经销商生成销售单
                        saleImpItem.add(impItem);
                    }
                }
            }

            // 生成销售
            if (CollectionUtils.isNotEmpty(saleImpItem)) {
                createSale(context.getUser(), assign, saleImpItem);
            }

            // 生成调拨单
            if (CollectionUtils.isNotEmpty(transferImpItem)) {
                createTransfer(context.getUser(), assign, transferImpItem);
            }
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error(this.getClass().getName() + ",铺货单审核异常:" + ExceptionUtil.getMessage(e), e);
            }
            AssertUtils.logAndThrowExtra("铺货单审核异常:", e);
        }
    }

    /**
     * 生成已审核未出库的销售单
     *
     * @param user     用户信息
     * @param assign   铺货单
     * @param impItems 明细
     */
    private void createSale(User user, SgBAssign assign, List<SgBAssignImpItem> impItems) {

        OcSaleBillSaveRequest request = new OcSaleBillSaveRequest();
        request.setObjId(-1L);
        request.setLoginUser(user);

        // 销售单主表信息封装
        OcSaleSaveRequest saleSaveRequest = new OcSaleSaveRequest();
        BeanUtils.copyProperties(assign, saleSaveRequest);
        saleSaveRequest.setId(-1L);
        saleSaveRequest.setBillNo(null);

        // 收货店仓、收货经销商
        SgBAssignImpItem impItem = impItems.get(0);
        saleSaveRequest.setCpCDestId(impItem.getCpCDestId());
        saleSaveRequest.setCpCDestEcode(impItem.getCpCDestEcode());
        saleSaveRequest.setCpCDestEname(impItem.getCpCDestEname());
        saleSaveRequest.setCpCCustomerId(impItem.getCpCCustomerdestId());
        saleSaveRequest.setCpCCustomerEcode(impItem.getCpCCustomerdestEcode());
        saleSaveRequest.setCpCCustomerEname(impItem.getCpCCustomerdestEname());

        // 自动出入库
        saleSaveRequest.setIsAutoOut(SgConstants.IS_ACTIVE_N);
        saleSaveRequest.setIsAutoIn(SgConstants.IS_ACTIVE_N);

        // 单据日期、销售类型、备注
        saleSaveRequest.setBillDate(new Date());
        saleSaveRequest.setSaleType(OcSaleConstantsIF.SALE_TYPE_NOR);
        saleSaveRequest.setRemark("由铺货单[" + assign.getBillNo() + "]审核生成！");
        request.setSaleSaveRequest(saleSaveRequest);

        // 销售单明细信息封装
        List<OcSaleImpItemSaveRequest> impItemSaveRequests = Lists.newArrayList();
        for (SgBAssignImpItem item : impItems) {
            OcSaleImpItemSaveRequest impItemSaveRequest = new OcSaleImpItemSaveRequest();
            BeanUtils.copyProperties(item, impItemSaveRequest);
            impItemSaveRequest.setId(-1L);
            impItemSaveRequests.add(impItemSaveRequest);
        }
        request.setImpItemSaveRequests(impItemSaveRequests);

        List<OcSaleBillSaveRequest> requests = Lists.newArrayList();
        requests.add(request);
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ",铺货单生成销售单入参:" + JSON.toJSONString(requests));
        }

        OcSaleSaveAndAuditCmd saveAndAuditBatchCmd = (OcSaleSaveAndAuditCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(),
                OcSaleSaveAndAuditCmd.class.getName(),
                "oc", "1.0");
        ValueHolderV14 holderV14 = saveAndAuditBatchCmd.batchSaveAndAuditSale(requests);
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ",铺货单生成销售单出参:" + JSON.toJSONString(holderV14));
        }
        if (holderV14.getCode() == ResultCode.FAIL) {
            AssertUtils.logAndThrow("铺户单生成销售单失败！" + holderV14.getMessage(), user.getLocale());
        }
    }

    /**
     * 生成已审核未出库的调拨单
     *
     * @param user     用户信息
     * @param assign   铺货单
     * @param impItems 明细
     */
    private void createTransfer(User user, SgBAssign assign, List<SgBAssignImpItem> impItems) {

        SgTransferBillSaveRequest request = new SgTransferBillSaveRequest();
        request.setObjId(-1L);
        request.setLoginUser(user);

        // 调拨单主表信息封装
        SgTransferSaveRequest transfer = new SgTransferSaveRequest();
        BeanUtils.copyProperties(assign, transfer);
        // 单据id、单据编号
        transfer.setId(-1L);
        transfer.setBillNo(null);

        // 收货店仓
        SgBAssignImpItem assignImpItem = impItems.get(0);
        transfer.setCpCDestId(assignImpItem.getCpCDestId());
        transfer.setCpCDestEcode(assignImpItem.getCpCDestEcode());
        transfer.setCpCDestEname(assignImpItem.getCpCDestEname());

        // 调拨类型、发货类型、单据日期
        transfer.setTransferType(SgTransferConstantsIF.TRANSFER_TYPE_NORMAL);
        transfer.setSendType(SgTransferConstantsIF.SEND_TYPE_ZT);
        transfer.setBillDate(new Date());

        // 自动出入库、备注
        transfer.setIsAutoOut(ScTransferConstants.IS_AUTO_N);
        transfer.setIsAutoIn(ScTransferConstants.IS_AUTO_N);
        transfer.setRemark("由铺货单[" + assign.getBillNo() + "]审核生成！");
        request.setTransfer(transfer);

        List<SgTransferImpItemSaveRequest> impItemSaveRequests = Lists.newArrayList();
        // 调拨明细信息封装
        for (SgBAssignImpItem impItem : impItems) {
            SgTransferImpItemSaveRequest itemSaveRequest = new SgTransferImpItemSaveRequest();
            BeanUtils.copyProperties(impItem, itemSaveRequest);
            itemSaveRequest.setId(-1L);
            impItemSaveRequests.add(itemSaveRequest);
        }
        request.setImpItems(impItemSaveRequests);
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ",铺货单生成调拨单入参:" + JSON.toJSONString(request));
        }

        SgTransferSaveAndAuditService service = ApplicationContextHandle.getBean(SgTransferSaveAndAuditService.class);
        ValueHolderV14 holderV14 = service.saveAndAudit(request);
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ",铺货单生成调拨单出参:" + JSON.toJSONString(holderV14));
        }
        if (holderV14.getCode() == ResultCode.FAIL) {
            AssertUtils.logAndThrow("铺户单生成调拨单失败！" + holderV14.getMessage(), user.getLocale());
        }
    }

}
