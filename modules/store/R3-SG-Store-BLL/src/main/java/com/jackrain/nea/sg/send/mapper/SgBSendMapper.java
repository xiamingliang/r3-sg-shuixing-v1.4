package com.jackrain.nea.sg.send.mapper;

import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.send.model.table.SgBSend;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface SgBSendMapper extends ExtentionMapper<SgBSend> {

    /**
     * 分页查询逻辑发货单
     */
    @Select("SELECT * FROM " + SgConstants.SG_B_SEND + " ORDER BY ID LIMIT #{page} OFFSET #{offset}")
    List<SgBSend> selectListByPage(@Param("page") int page, @Param("offset") int offset);

    @Select("SELECT\n" +
            "\tA.*\n" +
            "FROM\n" +
            "\t" + SgConstants.SG_B_SEND + " A\n" +
            "WHERE\n" +
            "\tEXISTS (\n" +
            "\t\tSELECT\n" +
            "\t\t\tID\n" +
            "\t\tFROM\n" +
            "\t\t\t" + SgConstants.SG_B_SEND_ITEM + "\n" +
            "\t\tWHERE\n" +
            "\t\t\tsg_b_send_id = A . ID\n" +
            "\t\tAND A .bill_status IN (${status})\n" +
            "\t\tAND A .source_bill_type IN (${types})\n" +
            "\t\tAND qty_preout <> 0\n" +
            "\t\tAND qty_send = 0\n" +
            "\t\tAND cp_c_store_id IN (${storeIds})\n" +
            "\t\tAND ps_c_sku_id IN (${skuIds})\n" +
            "\t\tAND A .isactive = '" + SgConstants.IS_ACTIVE_Y + "'\n" +
            "\t)")
    List<SgBSend> selectSendList(@Param("status") String status, @Param("types") String types, @Param("storeIds") String storeIds, @Param("skuIds") String skuIds);
}