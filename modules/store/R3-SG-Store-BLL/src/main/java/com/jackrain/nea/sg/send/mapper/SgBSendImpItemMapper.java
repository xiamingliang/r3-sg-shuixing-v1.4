package com.jackrain.nea.sg.send.mapper;

import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.send.model.table.SgBSendImpItem;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SgBSendImpItemMapper extends ExtentionMapper<SgBSendImpItem> {
}