package com.jackrain.nea.sg.send.services;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.send.model.request.SgSendBillVoidRequest;
import com.jackrain.nea.sg.send.model.request.SgSendSaveRequest;
import com.jackrain.nea.sg.send.model.request.SgSendSaveWithPriorityRequest;
import com.jackrain.nea.sg.send.model.request.SgSendVoidSaveRequest;
import com.jackrain.nea.sg.send.model.result.SgSendBatchVoidResult;
import com.jackrain.nea.sg.send.model.result.SgSendBillVoidResult;
import com.jackrain.nea.sg.send.model.result.SgSendSaveWithPriorityResult;
import com.jackrain.nea.sg.send.model.result.SgSendVoidResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Locale;

/**
 * @author 舒威
 * @since 2019/6/27
 * create at : 2019/6/27 15:21
 */
@Slf4j
@Component
public class SgSendVoidSaveService {

    @Autowired
    private SgSendVoidService voidService;

    @Autowired
    private SgSendSaveWithPriorityService withPriorityService;

    @Transactional(rollbackFor = Exception.class)
    public ValueHolderV14<List<SgSendSaveWithPriorityResult>> voidSaveSgBSend(SgSendVoidSaveRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("批量作废取消逻辑发货单并重新占单入参:" + JSONObject.toJSONString(request) + ";");
        }

        User user = request.getLoginUser();
        AssertUtils.notNull(user, "用户未登录！");
        Locale locale = user.getLocale();

        ValueHolderV14<List<SgSendSaveWithPriorityResult>> v14 = new ValueHolderV14<>();
        v14.setCode(ResultCode.SUCCESS);
        v14.setMessage("success");

        List<SgSendBillVoidRequest> voidRequests = request.getVoidRequests();
        for (SgSendBillVoidRequest voidRequest : voidRequests) {
            voidRequest.setLoginUser(user);
            ValueHolderV14<SgSendBillVoidResult> voidResult = voidService.voidSgBSend(voidRequest);
            if (!voidResult.isOK()) {
                AssertUtils.logAndThrow("作废逻辑发货单失败！" + voidResult.getMessage(), locale);
            }
        }

        //只需要批量作废直接返回
        if (request.getIsBatchVoid()) {
            return v14;
        }

        List<SgSendSaveWithPriorityResult> withPriorityResults = Lists.newArrayList();
        List<SgSendSaveWithPriorityRequest> withPriorityRequests = request.getSaveWithPriorityRequests();
        for (SgSendSaveWithPriorityRequest withPriorityRequest : withPriorityRequests) {
            withPriorityRequest.setLoginUser(user);
            ValueHolderV14<SgSendSaveWithPriorityResult> withPriorityResult = withPriorityService.searchLogicWithTrans(withPriorityRequest);
            if (withPriorityResult.isOK()) {
                SgSendSaveRequest send = withPriorityRequest.getSgSend();
                SgSendSaveWithPriorityResult withPriorityResultData = withPriorityResult.getData();
                withPriorityResultData.setSourceBillId(send.getSourceBillId());
                withPriorityResultData.setSourceBillNo(send.getSourceBillNo());
                withPriorityResultData.setSourceBillType(send.getSourceBillType());
                withPriorityResults.add(withPriorityResultData);
            } else {
                AssertUtils.logAndThrow("重新占单失败！" + withPriorityResult.getMessage());
            }
        }

        v14.setData(withPriorityResults);

        if (log.isDebugEnabled()) {
            log.debug("批量作废取消逻辑发货单并重新占单出参" + v14.toJSONObject().toJSONString() + ";");
        }
        return v14;
    }


    public ValueHolderV14<SgSendBatchVoidResult> batchVoidSgBSend(List<SgSendBillVoidRequest> requests, User user) {
        AssertUtils.isTrue(CollectionUtils.isNotEmpty(requests), "逻辑单作废集合为空!");
        if (log.isDebugEnabled()) {
            log.debug("实缺批量作废取消逻辑发货单入参:" + JSONObject.toJSONString(requests) + ";");
        }

        ValueHolderV14<SgSendBatchVoidResult> vh = new ValueHolderV14<>(ResultCode.SUCCESS, "success");
        SgSendBatchVoidResult data = new SgSendBatchVoidResult();
        List<SgSendVoidResult> voidResults = Lists.newArrayList();

        for (SgSendBillVoidRequest voidRequest : requests) {
            SgSendVoidResult result = new SgSendVoidResult();
            voidRequest.setLoginUser(user);
            Long sourceBillId = voidRequest.getSourceBillId();
            Integer sourceBillType = voidRequest.getSourceBillType();
            String message = "";
            try {
                ValueHolderV14<SgSendBillVoidResult> voidResult = voidService.voidSgBSend(voidRequest);
                if (log.isDebugEnabled()) {
                    log.debug("来源单据id[" + sourceBillId + "],批量作废取消逻辑发货单并重新占单入参:" + JSONObject.toJSONString(requests) + ";");
                }
                if (voidResult.isOK()) {
                    message = voidResult.getMessage();
                    result.setCode(ResultCode.SUCCESS);
                } else {
                    AssertUtils.logAndThrow(voidResult.getMessage());
                }
            } catch (Exception e) {
                e.printStackTrace();
                result.setCode(ResultCode.FAIL);
                message = AssertUtils.getMessageExtra("作废逻辑发货单失败！", e);
            }
            result.setSourceBillId(sourceBillId);
            result.setSourceBillType(sourceBillType);
            result.setMessage(message);
            voidResults.add(result);
        }

        data.setVoidResults(voidResults);
        vh.setData(data);
        if (log.isDebugEnabled()) {
            log.debug("实缺批量作废取消逻辑发货单出参:" + JSONObject.toJSONString(vh) + ";");
        }
        return vh;
    }
}
