package com.jackrain.nea.sg.transfer.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.common.R3ParamConstants;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.config.SgStorageBoxConfig;
import com.jackrain.nea.sg.basic.mapper.CpCPhyWarehouseMapper;
import com.jackrain.nea.sg.basic.model.table.CpCPhyWarehouse;
import com.jackrain.nea.sg.out.common.SgOutConstantsIF;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferImpItemMapper;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferItemMapper;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferMapper;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferTeusItemMapper;
import com.jackrain.nea.sg.transfer.model.request.SgTransferBillSaveRequest;
import com.jackrain.nea.sg.transfer.model.table.ScBTransfer;
import com.jackrain.nea.sg.transfer.model.table.ScBTransferImpItem;
import com.jackrain.nea.sg.transfer.model.table.ScBTransferItem;
import com.jackrain.nea.sg.transfer.model.table.ScBTransferTeusItem;
import com.jackrain.nea.sg.transfer.utils.SgTransferBillGeneraUtil;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.utils.AssertUtils;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * @author csy
 * Date: 2019/5/20
 * Description: 调拨单自动出库
 */

@Slf4j
@Component
public class SgTransferAutoOutService {

    @Autowired
    private SgStorageBoxConfig boxConfig;

    /**
     * 自动出入库
     */
    @Transactional(rollbackFor = Exception.class)
    public ValueHolderV14 autoOut(SgTransferBillSaveRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("Start SgTransferAutoOutService.autoOut. ReceiveParams:SgTransferBillSaveRequest="
                    + JSONObject.toJSONString(request) + ";");
        }
        User user = request.getLoginUser();
        Locale locale = user.getLocale();

        Long origStoreId = request.getTransfer().getCpCOrigId();
        Long destStoreId = request.getTransfer().getCpCDestId();
        CpCPhyWarehouseMapper warehouseMapper = ApplicationContextHandle.getBean(CpCPhyWarehouseMapper.class);
        AssertUtils.notNull(origStoreId, "发货店仓不存在!", locale);
        AssertUtils.notNull(destStoreId, "收货店仓不存在!", locale);
        CpCPhyWarehouse origWarehouse = warehouseMapper.selectByStoreId(origStoreId);
        CpCPhyWarehouse destWarehouse = warehouseMapper.selectByStoreId(destStoreId);
        AssertUtils.notNull(origWarehouse, "发货店实体仓不存在!", locale);
        AssertUtils.notNull(destWarehouse, "收货店实体仓不存在!", locale);
        AssertUtils.cannot(origWarehouse.getId().equals(destWarehouse.getId()), "收发店仓对应同一实体仓，不允许自动出库", locale);
        SgTransferSaveAndAuditService service = ApplicationContextHandle.getBean(SgTransferSaveAndAuditService.class);
        ValueHolderV14<JSONObject> auditResult = service.saveAndAudit(request);
        if (auditResult.getCode() == ResultCode.FAIL) {
            AssertUtils.logAndThrow("新增并审核调拨单失败!:" + auditResult.getMessage(), locale);
        }
        JSONObject data = auditResult.getData();
        AssertUtils.notNull(data, "调拨单审核返回结果为空!", locale);
        String outBillNo = data.getString("outBillNo");
        Long outId = data.getLong("outId");
        Long objId = data.getLong(R3ParamConstants.OBJID);
        HashMap outSkuMap = (HashMap) data.get(SgOutConstantsIF.RETURN_OUT_NOTICES_ITEM_KEY);
        AssertUtils.notNull(outBillNo, "出库通知单新增返回单据编号为空!", locale);
        AssertUtils.notNull(outId, "出库通知单新增返回id为空!", locale);
        AssertUtils.notNull(objId, "调拨单新增返回id为空", locale);
        ScBTransferMapper mapper = ApplicationContextHandle.getBean(ScBTransferMapper.class);
        ScBTransferItemMapper itemMapper = ApplicationContextHandle.getBean(ScBTransferItemMapper.class);
        ScBTransfer transfer = mapper.selectById(objId);
        List<ScBTransferItem> items = itemMapper.selectList(new QueryWrapper<ScBTransferItem>().lambda()
                .eq(ScBTransferItem::getScBTransferId, objId));

        // 录入明细和箱内明细只做一次查询
        List<ScBTransferImpItem> impItems = null;
        List<ScBTransferTeusItem> boxItems = null;
        if(boxConfig.getBoxEnable()){
            // 箱功能开启 查询录入明细
            ScBTransferImpItemMapper impItemMapper = ApplicationContextHandle.getBean(ScBTransferImpItemMapper.class);
            impItems = impItemMapper.selectList(
                    new QueryWrapper<ScBTransferImpItem>().lambda().
                            eq(ScBTransferImpItem::getScBTransferId, objId));

            // 箱功能开启 查询录入明细
            ScBTransferTeusItemMapper boxItemMapper = ApplicationContextHandle.getBean(ScBTransferTeusItemMapper.class);
            boxItems = boxItemMapper.selectList(
                    new QueryWrapper<ScBTransferTeusItem>().lambda().
                            eq(ScBTransferTeusItem::getScBTransferId, objId));
        }

        SgTransferBillGeneraUtil generaUtil = ApplicationContextHandle.getBean(SgTransferBillGeneraUtil.class);
        generaUtil.createOutResult(user, transfer, items, outId, outBillNo, outSkuMap, impItems, boxItems);
        return new ValueHolderV14(ResultCode.SUCCESS, SgConstants.MESSAGE_STATUS_SUCCESS);
    }
}
