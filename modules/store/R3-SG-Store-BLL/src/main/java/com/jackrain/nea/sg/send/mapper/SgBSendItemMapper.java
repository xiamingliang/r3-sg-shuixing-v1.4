package com.jackrain.nea.sg.send.mapper;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.send.model.table.SgBSendItem;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@Repository
@Mapper
public interface SgBSendItemMapper extends ExtentionMapper<SgBSendItem> {

    /**
     * 获取逻辑发货单待发货合计数量
     *
     * @param billNo 单据编号
     */
    @Select("select sg_b_send_item.ps_c_sku_id, sg_b_send_item.source_bill_item_id, " +
            "sum(sg_b_send_item.qty_preout) as sum_qty_preout " +
            "from sg_b_send inner join sg_b_send_item on sg_b_send.id = sg_b_send_item.sg_b_send_id " +
            "where sg_b_send.source_bill_no = #{billNo} and sg_b_send.isactive = 'Y' " +
            "and sg_b_send_item.isactive = 'Y' " +
            "group by ps_c_sku_id,source_bill_item_id")
    List<Map<String, Object>> selectSumQtyGroupBySku(@Param("billNo") String billNo);

    /**
     * 获取逻辑发货单待发货合计数量
     *
     * @param billNo 单据编号
     */
    @Select("select sg_b_send_item.cp_c_store_id, sg_b_send_item.cp_c_store_ecode, sg_b_send_item.cp_c_store_ename " +
            "from sg_b_send inner join sg_b_send_item on sg_b_send.id = sg_b_send_item.sg_b_send_id " +
            "where sg_b_send.source_bill_no = #{billNo} " +
            "and sg_b_send_item.ps_c_sku_id = #{psCSkuId} " +
            "and sg_b_send_item.source_bill_item_id = #{sourceItemId} " +
            "and sg_b_send.isactive = 'Y' order by sg_b_send_item.modifieddate desc")
    List<Map<String, Object>> selectStoreInfoBySku(@Param("billNo") String billNo,
                                                   @Param("sourceItemId") Long sourceItemId,
                                                   @Param("psCSkuId") Long psCSkuId);

    /**
     * 获取逻辑发货单占用合计数量
     *
     * @param sendId 逻辑发货单id
     */
    @Select("select SUM(qty_preout) as qty_preout,SUM(qty_send) as qty_send from sg_b_send_item where sg_b_send_id = #{sendId}")
    JSONObject selectTotalPreoutBySendId(@Param("sendId") Long sendId);

    /**
     * 更新逻辑发货单明细的在途数量为0
     *
     * @param id 逻辑收货单id
     */
    @Update("UPDATE sg_b_send_item SET qty_preout=0 WHERE sg_b_send_id=#{id}")
    int updatePreoutBySendId(@Param("id") Long id);

    /**
     * 分页查询逻辑发货单
     */
    @Select("SELECT * FROM " + SgConstants.SG_B_SEND_ITEM + " ORDER BY ID LIMIT #{page} OFFSET #{offset}")
    List<SgBSendItem> selectListByPage(@Param("page") int page, @Param("offset") int offset);

    @Update("UPDATE sg_b_send_item SET qty_preout=qty_preout-${cnt} WHERE id=#{id}")
    int updateByOutStock(@Param("id") Long id, @Param("cnt") BigDecimal cnt);
}