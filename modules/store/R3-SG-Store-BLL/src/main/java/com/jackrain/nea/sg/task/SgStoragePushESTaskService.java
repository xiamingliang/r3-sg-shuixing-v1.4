package com.jackrain.nea.sg.task;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.es.util.ElasticSearchUtil;
import com.jackrain.nea.sg.adjust.common.SgAdjustConstants;
import com.jackrain.nea.sg.adjust.mapper.SgBAdjustItemMapper;
import com.jackrain.nea.sg.adjust.mapper.SgBAdjustMapper;
import com.jackrain.nea.sg.adjust.model.table.SgBAdjust;
import com.jackrain.nea.sg.adjust.model.table.SgBAdjustItem;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.receive.common.SgReceiveConstants;
import com.jackrain.nea.sg.receive.mapper.SgBReceiveItemMapper;
import com.jackrain.nea.sg.receive.mapper.SgBReceiveMapper;
import com.jackrain.nea.sg.receive.model.table.SgBReceive;
import com.jackrain.nea.sg.receive.model.table.SgBReceiveItem;
import com.jackrain.nea.sg.send.common.SgSendConstants;
import com.jackrain.nea.sg.send.mapper.SgBSendItemMapper;
import com.jackrain.nea.sg.send.mapper.SgBSendMapper;
import com.jackrain.nea.sg.send.model.table.SgBSend;
import com.jackrain.nea.sg.send.model.table.SgBSendItem;
import com.jackrain.nea.sg.transfer.common.ScTransferConstants;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferItemMapper;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferMapper;
import com.jackrain.nea.sg.transfer.model.table.ScBTransfer;
import com.jackrain.nea.sg.transfer.model.table.ScBTransferItem;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

/**
 * @author 舒威
 * @since 2019/7/16
 * create at : 2019/7/16 9:41
 */
@Slf4j
@Component
public class SgStoragePushESTaskService {

    @Value("${sg.bill.update_es_data_max_threads}")
    private Integer threadsNum;

    /**
     * 刷新逻辑仓相关业务es
     */
    public ValueHolderV14 pushStoreES(JSONObject params) {
        if (log.isDebugEnabled()) {
            log.debug("Start Batch Push ES Store. ReceiveParams:params:{};", JSONObject.toJSONString(params));
        }
        ValueHolderV14 result = new ValueHolderV14(ResultCode.SUCCESS, "message");
        long startTime = System.currentTimeMillis();
        if (params != null && StringUtils.isNotEmpty(params.getString("table"))) {
            String table = params.getString("table");
            Boolean isDelete = Optional.ofNullable(params.getBoolean("isDelete")).orElse(false);
            log.debug("isDelete:" + isDelete);
            switch (table) {
                case SgConstants.SG_B_SEND:
                    pushESSend(isDelete);
                    break;
                case SgConstants.SG_B_RECEIVE:
                    pushESReceive(isDelete);
                    break;
                case SgConstants.SG_B_ADJUST:
                    pushESAdjust(isDelete);
                    break;
                case SgConstants.SG_B_TRANSFER:
                    pushESTransfer(isDelete);
                    break;
                default:
                    log.debug(table + "暂不支持推送es!");
            }
        } else {
            pushESSend(false);
            pushESReceive(false);
            pushESAdjust(false);
            pushESTransfer(false);
        }

        if (log.isDebugEnabled()) {
            log.debug("Finish  Batch Push ES Store: spend time:{}ms;", System.currentTimeMillis() - startTime);
        }

        return result;
    }


    /**
     * 刷新调拨单es
     * @param isDelete
     */
    private void pushESTransfer(Boolean isDelete) {
        if (log.isDebugEnabled()) {
            log.debug("Start Batch Push ES Transfer");
        }

        long startTime = System.currentTimeMillis();
        ScBTransferMapper transferMapper = ApplicationContextHandle.getBean(ScBTransferMapper.class);
        ScBTransferItemMapper transferItemMapper = ApplicationContextHandle.getBean(ScBTransferItemMapper.class);
        String index = SgConstants.SG_B_TRANSFER;
        String childType = SgConstants.SG_B_TRANSFER_ITEM;

        if (isDelete) {
            try {
                ElasticSearchUtil.indexDelete(index);
            } catch (Exception e) {
                e.printStackTrace();
                log.error(index + "index删除失败!");
            }
        }

        int pageSize = threadsNum * 500;
        //获取主表分页数
        int cnt = transferMapper.selectCount(null);
        int page = cnt / pageSize;
        if (cnt % pageSize != 0) {
            page++;
        }

        for (int i = 0; i < page; i++) {
            //批量推送主表
            List<ScBTransfer> adjusts = transferMapper.selectListByPage(pageSize, i * pageSize);
            StorageESUtils.batchPushES(adjusts, threadsNum, index,
                    null, null, ScBTransfer.class, ScBTransferItem.class);
        }

        //获取明细分页数
        int itemCnt = transferItemMapper.selectCount(null);
        int itemPage = itemCnt / pageSize;
        if (itemCnt % pageSize != 0) {
            itemPage++;
        }

        for (int i = 0; i < itemPage; i++) {
            //批量推送明细表
            List<ScBTransferItem> senditems = transferItemMapper.selectListByPage(pageSize, i * pageSize);
            StorageESUtils.batchPushES(senditems, threadsNum, index, childType,
                    ScTransferConstants.TRANSFER_PARENT_FIELD.toUpperCase(), ScBTransfer.class, ScBTransferItem.class);
        }

        if (log.isDebugEnabled()) {
            log.debug("Finish  Batch Push ES Transfer: spend time:{}ms;", System.currentTimeMillis() - startTime);
        }
    }

    /**
     * 刷新逻辑调整单es
     * @param isDelete
     */
    private void pushESAdjust(Boolean isDelete) {
        if (log.isDebugEnabled()) {
            log.debug("Start Batch Push ES Adjust");
        }

        long startTime = System.currentTimeMillis();
        SgBAdjustMapper adjustMapper = ApplicationContextHandle.getBean(SgBAdjustMapper.class);
        SgBAdjustItemMapper adjustItemMapper = ApplicationContextHandle.getBean(SgBAdjustItemMapper.class);
        String index = SgConstants.SG_B_ADJUST;
        String childType = SgConstants.SG_B_ADJUST_ITEM;

        if (isDelete) {
            try {
                ElasticSearchUtil.indexDelete(index);
            } catch (Exception e) {
                e.printStackTrace();
                log.error(index + "index删除失败!");
            }
        }

        int pageSize = threadsNum * 500;
        //获取主表分页数
        int cnt = adjustMapper.selectCount(null);
        int page = cnt / pageSize;
        if (cnt % pageSize != 0) {
            page++;
        }

        for (int i = 0; i < page; i++) {
            //批量推送主表
            List<SgBAdjust> adjusts = adjustMapper.selectListByPage(pageSize, i * pageSize);
            StorageESUtils.batchPushES(adjusts, threadsNum, index,
                    null, null, SgBAdjust.class, SgBAdjustItem.class);
        }

        //获取明细分页数
        int itemCnt = adjustItemMapper.selectCount(null);
        int itemPage = itemCnt / pageSize;
        if (itemCnt % pageSize != 0) {
            itemPage++;
        }

        for (int i = 0; i < itemPage; i++) {
            //批量推送明细表
            List<SgBAdjustItem> senditems = adjustItemMapper.selectListByPage(pageSize, i * pageSize);
            StorageESUtils.batchPushES(senditems, threadsNum, index, childType,
                    SgAdjustConstants.SG_B_ADJUST_ID.toUpperCase(), SgBAdjust.class, SgBAdjustItem.class);
        }

        if (log.isDebugEnabled()) {
            log.debug("Finish  Batch Push ES Adjust: spend time:{}ms;", System.currentTimeMillis() - startTime);
        }
    }

    /**
     * 刷新逻辑收货单es
     * @param isDelete
     */
    private void pushESReceive(Boolean isDelete) {
        if (log.isDebugEnabled()) {
            log.debug("Start Batch Push ES Receive");
        }

        long startTime = System.currentTimeMillis();
        SgBReceiveMapper receiveMapper = ApplicationContextHandle.getBean(SgBReceiveMapper.class);
        SgBReceiveItemMapper receiveItemMapper = ApplicationContextHandle.getBean(SgBReceiveItemMapper.class);
        String index = SgConstants.SG_B_RECEIVE;
        String childType = SgConstants.SG_B_RECEIVE_ITEM;

        if (isDelete) {
            try {
                ElasticSearchUtil.indexDelete(index);
            } catch (Exception e) {
                e.printStackTrace();
                log.error(index + "index删除失败!");
            }
        }

        int pageSize = threadsNum * 500;
        //获取主表分页数
        int cnt = receiveMapper.selectCount(null);
        int page = cnt / pageSize;
        if (cnt % pageSize != 0) {
            page++;
        }

        for (int i = 0; i < page; i++) {
            //批量推送主表
            List<SgBReceive> receives = receiveMapper.selectListByPage(pageSize, i * pageSize);
            StorageESUtils.batchPushES(receives, threadsNum, index,
                    null, null, SgBReceive.class, SgBReceiveItem.class);
        }

        //获取明细分页数
        int itemCnt = receiveItemMapper.selectCount(null);
        int itemPage = itemCnt / pageSize;
        if (itemCnt % pageSize != 0) {
            itemPage++;
        }

        for (int i = 0; i < itemPage; i++) {
            //批量推送明细表
            List<SgBReceiveItem> receiveitems = receiveItemMapper.selectListByPage(pageSize, i * pageSize);
            StorageESUtils.batchPushES(receiveitems, threadsNum, index, childType,
                    SgReceiveConstants.BILL_RECEIVE_ID.toUpperCase(), SgBReceive.class, SgBReceiveItem.class);
        }

        if (log.isDebugEnabled()) {
            log.debug("Finish  Batch Push ES Receive: spend time:{}ms;", System.currentTimeMillis() - startTime);
        }
    }

    /**
     * 刷新逻辑发货单es
     * @param isDelete
     */
    private void pushESSend(Boolean isDelete) {
        if (log.isDebugEnabled()) {
            log.debug("Start Batch Push ES Send");
        }

        long startTime = System.currentTimeMillis();
        SgBSendMapper sendMapper = ApplicationContextHandle.getBean(SgBSendMapper.class);
        SgBSendItemMapper sendItemMapper = ApplicationContextHandle.getBean(SgBSendItemMapper.class);
        String index = SgConstants.SG_B_SEND;
        String childType = SgConstants.SG_B_SEND_ITEM;

        if (isDelete) {
            try {
                ElasticSearchUtil.indexDelete(index);
            } catch (Exception e) {
                e.printStackTrace();
                log.error(index + "index删除失败!");
            }
        }

        int pageSize = threadsNum * 500;
        //获取主表分页数
        int cnt = sendMapper.selectCount(null);
        int page = cnt / pageSize;
        if (cnt % pageSize != 0) {
            page++;
        }

        for (int i = 0; i < page; i++) {
            //批量推送主表
            List<SgBSend> sends = sendMapper.selectListByPage(pageSize, i * pageSize);
            StorageESUtils.batchPushES(sends, threadsNum, index,
                    null, null, SgBSend.class, SgBSendItem.class);
        }

        //获取明细分页数
        int itemCnt = sendItemMapper.selectCount(null);
        int itemPage = itemCnt / pageSize;
        if (itemCnt % pageSize != 0) {
            itemPage++;
        }

        for (int i = 0; i < itemPage; i++) {
            //批量推送明细表
            List<SgBSendItem> senditems = sendItemMapper.selectListByPage(pageSize, i * pageSize);
            StorageESUtils.batchPushES(senditems, threadsNum, index, childType,
                    SgSendConstants.BILL_SEND_ID.toUpperCase(), SgBSend.class, SgBSendItem.class);
        }

        if (log.isDebugEnabled()) {
            log.debug("Finish  Batch Push ES Send: spend time:{}ms;", System.currentTimeMillis() - startTime);
        }
    }

}
