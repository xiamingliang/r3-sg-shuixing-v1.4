package com.jackrain.nea.sg.assign.filter;

import com.jackrain.nea.sg.assign.common.SgAssignConstantsIF;
import com.jackrain.nea.tableService.MainTableRecord;
import com.jackrain.nea.tableService.TableServiceContext;
import com.jackrain.nea.tableService.filter.BaseFilter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author leexh
 * @since 2019/12/3 10:40
 * desc:
 */
@Slf4j
@Component
public class SgAssignVoidFilter extends BaseFilter {

    @Override
    public void before(TableServiceContext context, MainTableRecord row) {

        row.getCommitData().put("STATUS", SgAssignConstantsIF.ASSIGN_STATUS_VOID);

    }
}
