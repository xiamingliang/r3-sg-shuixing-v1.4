package com.jackrain.nea.sg.transfer.services;

import com.alibaba.fastjson.JSON;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.common.R3ParamConstants;
import com.jackrain.nea.sg.basic.model.result.SgR3BaseResult;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.transfer.model.request.SgTransferBillSaveRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Locale;

/**
 * @author csy
 * Date: 2019/5/7
 * Description: 新增并审核调拨单
 */
@Slf4j
@Component
public class SgTransferSaveAndAuditService {

    @Transactional(rollbackFor = Exception.class)
    public ValueHolderV14 saveAndAudit(SgTransferBillSaveRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("start SgTransferSaveAndAuditService.saveAndAudit" +
                    " SgTransferBillSaveRequest:" + JSON.toJSONString(request));
        }
        SgTransferSaveService saveService = ApplicationContextHandle.getBean(SgTransferSaveService.class);
        ValueHolderV14<SgR3BaseResult> holderV14 = saveService.save(request, true);
        if (holderV14.getCode() == ResultCode.FAIL) {
            return holderV14;
        }
        User user = request.getLoginUser();
        Locale locale = user.getLocale();
        Long objId = holderV14.getData().getDataJo().getLong(R3ParamConstants.OBJID);
        AssertUtils.notNull(objId, "新增没有返回objId", locale);
        SgTransferAuditService auditService = ApplicationContextHandle.getBean(SgTransferAuditService.class);
        ValueHolder auditResult = auditService.audit(objId, user);
        log.debug("新增并审核返回结果===》"+auditResult.toJSONObject());
        if (auditResult.get(R3ParamConstants.CODE) != null && (Integer) auditResult.get(R3ParamConstants.CODE) == ResultCode.FAIL) {
            AssertUtils.logAndThrow("审核调拨单失败-" + auditResult.get(R3ParamConstants.MESSAGE), locale);
        }
        ValueHolderV14 result = new ValueHolderV14();
        result.setMessage(Resources.getMessage("新增并审核调拨单成功!", locale));
        result.setCode(ResultCode.SUCCESS);
        result.setData(auditResult.get("data"));
        return result;
    }

    @Transactional(rollbackFor = Exception.class)
    public ValueHolderV14 saveAndAuditBatch(List<SgTransferBillSaveRequest> requests) {

        ValueHolderV14 result = new ValueHolderV14(ResultCode.FAIL, "新增并审核失败！");
        if (CollectionUtils.isEmpty(requests)) {
            result.setMessage("参数不能为空！");
        }

        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ",saveAndAuditBatch,入参:" + JSON.toJSONString(requests));
        }

        Locale locale = requests.get(0).getLoginUser().getLocale();
        SgTransferSaveAndAuditService bean = ApplicationContextHandle.getBean(SgTransferSaveAndAuditService.class);
        for (SgTransferBillSaveRequest request : requests) {
            ValueHolderV14 v14 = bean.saveAndAudit(request);
            if (v14.getCode() == ResultCode.FAIL) {
                return v14;
            }
        }
        result.setCode(ResultCode.SUCCESS);
        result.setMessage(Resources.getMessage("新增并审核调拨单成功！", locale));
        return result;
    }
}
