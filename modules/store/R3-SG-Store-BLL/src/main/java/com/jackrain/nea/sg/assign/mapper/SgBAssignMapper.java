package com.jackrain.nea.sg.assign.mapper;

import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.assign.model.table.SgBAssign;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SgBAssignMapper extends ExtentionMapper<SgBAssign> {
}