package com.jackrain.nea.sg.send.utils;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.sg.adjust.common.SgAdjustConstants;
import com.jackrain.nea.sg.adjust.mapper.SgBAdjustItemMapper;
import com.jackrain.nea.sg.adjust.mapper.SgBAdjustMapper;
import com.jackrain.nea.sg.adjust.model.table.SgBAdjust;
import com.jackrain.nea.sg.adjust.model.table.SgBAdjustItem;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.config.SgStorageESConfig;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.receive.common.SgReceiveConstants;
import com.jackrain.nea.sg.receive.mapper.SgBReceiveItemMapper;
import com.jackrain.nea.sg.receive.mapper.SgBReceiveMapper;
import com.jackrain.nea.sg.receive.model.table.SgBReceive;
import com.jackrain.nea.sg.receive.model.table.SgBReceiveItem;
import com.jackrain.nea.sg.send.common.SgSendConstants;
import com.jackrain.nea.sg.send.mapper.SgBSendItemMapper;
import com.jackrain.nea.sg.send.mapper.SgBSendMapper;
import com.jackrain.nea.sg.send.model.table.SgBSend;
import com.jackrain.nea.sg.send.model.table.SgBSendItem;
import com.jackrain.nea.sg.transfer.common.ScTransferConstants;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferDiffItemMapper;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferDiffMapper;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferItemMapper;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferMapper;
import com.jackrain.nea.sg.transfer.model.table.ScBTransfer;
import com.jackrain.nea.sg.transfer.model.table.ScBTransferDiff;
import com.jackrain.nea.sg.transfer.model.table.ScBTransferDiffItem;
import com.jackrain.nea.sg.transfer.model.table.ScBTransferItem;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author: 舒威
 * @since: 2019/10/25
 * create at : 2019/10/25 20:01
 */
@Slf4j
@Component
public class SgStoreESUtils {

    @Autowired
    private SgStorageESConfig esConfig;

    public void pushESBySend(Long id, Boolean isNeedItems, Boolean isDeleteItems, JSONArray deleteItemIds, SgBSendMapper sendMapper, SgBSendItemMapper sendItemMapper) {
        if (esConfig.isOpenES()) {
            try {
                log.debug("逻辑发货单" + id + "开始推送ES!");
                SgBSend send = sendMapper.selectOne(new QueryWrapper<SgBSend>().lambda().eq(SgBSend::getId, id));
                List<SgBSendItem> bSendItems = isNeedItems ? sendItemMapper.selectList(new QueryWrapper<SgBSendItem>().lambda()
                        .eq(SgBSendItem::getSgBSendId, id)) : null;
                String index = SgSendConstants.BILL_SEND;
                String type = SgSendConstants.BILL_SEND_ITEM;
                String parentKey = SgSendConstants.BILL_SEND_ID;
                StorageESUtils.pushESBModelByUpdate(send, bSendItems, id, deleteItemIds, index, index, type, parentKey, SgBSend.class, SgBSendItem.class, isDeleteItems);
            } catch (Exception e) {
                e.printStackTrace();
                String messageExtra = AssertUtils.getMessageExtra("逻辑发货单推送ES异常!", e);
                log.error(messageExtra);
            }
        }
    }

    public void pushESByReceive(Long id, Boolean isNeedItems, Boolean isDeleteItems, JSONArray deleteItemIds, SgBReceiveMapper receiveMapper, SgBReceiveItemMapper receiveItemMapper) {
        if (esConfig.isOpenES()) {
            try {
                log.debug("逻辑收货单" + id + "开始推送ES!");
                SgBReceive receive = receiveMapper.selectOne(new QueryWrapper<SgBReceive>().lambda().eq(SgBReceive::getId, id));
                List<SgBReceiveItem> bReceiveItems = isNeedItems ? receiveItemMapper.selectList(new QueryWrapper<SgBReceiveItem>().lambda()
                        .eq(SgBReceiveItem::getSgBReceiveId, id)) : null;
                String index = SgReceiveConstants.BILL_RECEIVE;
                String type = SgReceiveConstants.BILL_RECEIVE_ITEM;
                String parentKey = SgReceiveConstants.BILL_RECEIVE_ID;
                StorageESUtils.pushESBModelByUpdate(receive, bReceiveItems, id, deleteItemIds, index, index, type, parentKey, SgBReceive.class, SgBReceiveItem.class, isDeleteItems);
            } catch (Exception e) {
                e.printStackTrace();
                String messageExtra = AssertUtils.getMessageExtra("逻辑收货单推送ES异常!", e);
                log.error(messageExtra);
            }
        }
    }

    public void pushESByAdjust(Long id, Boolean isNeedItems, Boolean isDeleteItems, JSONArray deleteItemIds, SgBAdjustMapper adjustMapper, SgBAdjustItemMapper adjustItemMapper) {
        if (esConfig.isOpenES()) {
            try {
                log.debug("逻辑调整单" + id + "开始推送ES!");
                SgBAdjust adjust = adjustMapper.selectOne(new QueryWrapper<SgBAdjust>().lambda().eq(SgBAdjust::getId, id));
                List<SgBAdjustItem> adjustItems = isNeedItems ? adjustItemMapper.selectList(new QueryWrapper<SgBAdjustItem>().lambda()
                        .eq(SgBAdjustItem::getSgBAdjustId, id)) : null;
                String index = SgConstants.SG_B_ADJUST;
                String type = SgConstants.SG_B_ADJUST_ITEM;
                String parentKey = SgAdjustConstants.SG_B_ADJUST_ID;
                StorageESUtils.pushESBModelByUpdate(adjust, adjustItems, id, deleteItemIds, index, index, type, parentKey, SgBAdjust.class, SgBAdjustItem.class, isDeleteItems);
            } catch (Exception e) {
                e.printStackTrace();
                String messageExtra = AssertUtils.getMessageExtra("逻辑调整单推送ES异常!", e);
                log.error(messageExtra);
            }
        }
    }

    public void pushESByTransfer(Long id, Boolean isNeedItems, Boolean isDeleteItems, JSONArray deleteItemIds, ScBTransferMapper transferMapper, ScBTransferItemMapper transferItemMapper) {
        if (esConfig.isOpenES()) {
            try {
                log.debug("调拨单" + id + "开始推送ES!");
                ScBTransfer transfer = transferMapper.selectOne(new QueryWrapper<ScBTransfer>().lambda().eq(ScBTransfer::getId, id));
                List<ScBTransferItem> transferItems = isNeedItems ? transferItemMapper.selectList(new QueryWrapper<ScBTransferItem>().lambda()
                        .eq(ScBTransferItem::getScBTransferId, id)) : null;
                String index = ScTransferConstants.TRANSFER_INDEX;
                String type = ScTransferConstants.TRANSFER_ITEM_TYPE;
                String parentKey = ScTransferConstants.TRANSFER_PARENT_FIELD;
                StorageESUtils.pushESBModelByUpdate(transfer, transferItems, id, deleteItemIds, index, index, type, parentKey, ScBTransfer.class, ScBTransferItem.class, isDeleteItems);
            } catch (Exception e) {
                e.printStackTrace();
                String messageExtra = AssertUtils.getMessageExtra("调拨单推送ES异常!", e);
                log.error(messageExtra);
            }
        }
    }

    public void pushESByTransferDiff(Long id, Boolean isNeedItems, Boolean isDeleteItems, JSONArray deleteItemIds, ScBTransferDiffMapper transferMapper, ScBTransferDiffItemMapper transferItemMapper) {
        if (esConfig.isOpenES()) {
            try {
                log.debug("调拨差异单" + id + "开始推送ES!");
                ScBTransferDiff transferDiff = transferMapper.selectOne(new QueryWrapper<ScBTransferDiff>().lambda().eq(ScBTransferDiff::getId, id));
                List<ScBTransferDiffItem> transferDiffItems = isNeedItems ? transferItemMapper.selectList(new QueryWrapper<ScBTransferDiffItem>().lambda()
                        .eq(ScBTransferDiffItem::getScBTransferDiffId, id)) : null;
                String index = SgConstants.SC_B_TRANSFER_DIFF;
                String type = SgConstants.SC_B_TRANSFER_DIFF_ITEM;
                String parentKey = ScTransferConstants.SC_B_TRANSFER_DIFF_ID;
                StorageESUtils.pushESBModelByUpdate(transferDiff, transferDiffItems, id, deleteItemIds, index, index, type, parentKey, ScBTransferDiff.class, ScBTransferDiffItem.class, isDeleteItems);
            } catch (Exception e) {
                e.printStackTrace();
                String messageExtra = AssertUtils.getMessageExtra("调拨差异单推送ES异常!", e);
                log.error(messageExtra);
            }
        }
    }
}
