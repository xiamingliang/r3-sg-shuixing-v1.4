package com.jackrain.nea.sg.transfer.mapper;

import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.transfer.model.table.ScBTransferImpItem;
import com.jackrain.nea.web.face.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.Date;
import java.util.List;

@Mapper
public interface ScBTransferImpItemMapper extends ExtentionMapper<ScBTransferImpItem> {

    /**
     * 查询 合并 同一调拨单下 的 箱相关的 录入明细 ，并统计 qty
     *
     * @param id
     * @return
     */
    @Select("SELECT\n" +
            "\tps_c_pro_id,\n" +
            "\tps_c_matchsize_id,\n" +
            "\tps_c_spec1_id,\n" +
            "\tps_c_pro_ecode,\n" +
            "\tps_c_matchsize_ecode,\n" +
            "\tps_c_spec1_ecode,\n" +
            "\tSUM (qty) AS qty\n" +
            "FROM\n" +
            "\tsc_b_transfer_imp_item\n" +
            "WHERE\n" +
            "\tsc_b_transfer_id = #{id}\n" +
            "AND is_teus = 1\n" +
            "GROUP BY\n" +
            "\tps_c_pro_id,\n" +
            "\tps_c_matchsize_id,\n" +
            "\tps_c_spec1_id,\n" +
            "\tps_c_pro_ecode,\n" +
            "\tps_c_matchsize_ecode,\n" +
            "\tps_c_spec1_ecode")
    List<ScBTransferImpItem> selectImpIsBoxList(@Param("id") Long id);
    //水星-zhanghang-2019-12-13-start
    /**
     * 根据 调拨单id，批量更改 调拨数量为 0
     * @param id
     * @return
     */
    @Update(" UPDATE sc_b_transfer_item " +
            "set qty = 0 , " +
            " modifieddate = #{date}, " +
            " modifierename = #{user.ename}, " +
            " modifierid = #{user.id}, " +
            " modifiername =  #{user.name}" +
            "  where sc_b_transfer_id =  #{id}")
    int updateQtyByTranId(@Param("id") Long id, @Param("user") User user, @Param("date") Date date);

    //水星-zhanghang-2019-12-13-end

}