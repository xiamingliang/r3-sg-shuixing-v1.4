package com.jackrain.nea.sg.transfer.services;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.utils.StorageUtils;
import com.jackrain.nea.sg.transfer.common.SgTransferConstantsIF;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferImpItemMapper;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferItemMapper;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferMapper;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferTeusItemMapper;
import com.jackrain.nea.sg.transfer.model.request.SgTransferCommonQueryRequest;
import com.jackrain.nea.sg.transfer.model.request.SgTransferForPdaRequest;
import com.jackrain.nea.sg.transfer.model.request.SgTransferQueryByStatusRequest;
import com.jackrain.nea.sg.transfer.model.result.ScTransferImpItemAndTeusItemResult;
import com.jackrain.nea.sg.transfer.model.table.ScBTransfer;
import com.jackrain.nea.sg.transfer.model.table.ScBTransferImpItem;
import com.jackrain.nea.sg.transfer.model.table.ScBTransferItem;
import com.jackrain.nea.sg.transfer.model.table.ScBTransferTeusItem;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.utils.AssertUtils;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.utils.ObjectToUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author csy
 * Date: 2019/5/15
 * Description:调拨单-查询
 */


@Slf4j
@Component
public class SgTransferQueryService {

    @Autowired
    private ScBTransferItemMapper scBTransferItemMapper;

    @Autowired
    private ScBTransferImpItemMapper scBTransferImpItemMapper;

    /**
     * 根据单据状态查询 调拨单列表（分页）
     *
     * @param request 分页入参
     */
    ValueHolderV14<PageInfo<ScBTransfer>> queryTransferList(SgTransferQueryByStatusRequest request) {
        ScBTransferMapper mapper = ApplicationContextHandle.getBean(ScBTransferMapper.class);
        AssertUtils.notNull(request, "请求参数为空");
        PageHelper.startPage(request.getPageNum(), request.getPageSize());
        List<ScBTransfer> resultList = mapper.selectList(new QueryWrapper<ScBTransfer>().lambda().
                eq(ScBTransfer::getStatus, request.getBillStatus()));
        PageInfo<ScBTransfer> resultPage = new PageInfo<>(resultList);
        ValueHolderV14<PageInfo<ScBTransfer>> holderV14 = new ValueHolderV14<>();
        holderV14.setCode(ResultCode.SUCCESS);
        holderV14.setData(resultPage);
        return holderV14;
    }


    /**
     * 根据主表查询调拨单明细
     *
     * @param objId 主表id
     */
    ValueHolderV14<List<ScBTransferItem>> queryItemsById(Long objId) {
        ScBTransferItemMapper itemMapper = ApplicationContextHandle.getBean(ScBTransferItemMapper.class);
        List<ScBTransferItem> items = itemMapper.selectList(new QueryWrapper<ScBTransferItem>().lambda().
                eq(ScBTransferItem::getScBTransferId, objId));
        ValueHolderV14<List<ScBTransferItem>> holderV14 = new ValueHolderV14<>();
        holderV14.setCode(ResultCode.SUCCESS);
        holderV14.setData(items);
        return holderV14;
    }


    /**
     * 根据单据状态查询 调拨单列表（分页）
     *
     * @param request 分页入参
     */
    ValueHolderV14<List<ScBTransfer>> queryTransfer(SgTransferCommonQueryRequest request) {
        ScBTransferMapper mapper = ApplicationContextHandle.getBean(ScBTransferMapper.class);
        AssertUtils.notNull(request, "请求参数为空");
        List<ScBTransfer> list = new ArrayList<>();
        List<Long> ids = request.getIds();
        if (CollectionUtils.isNotEmpty(ids)) {
            List<List<Long>> pageList = StorageUtils.getPageList(ids, 200);
            for (List<Long> page : pageList) {
                list.addAll(mapper.selectList(new QueryWrapper<ScBTransfer>().lambda()
                        .eq(ScBTransfer::getIsactive, SgConstants.IS_ACTIVE_Y)
                        .in(ScBTransfer::getId, page).orderByDesc(ScBTransfer::getModifieddate)));
            }
        } else {
            boolean isAllNull = CollectionUtils.isEmpty(request.getBillNos()) && request.getOcBSendOutId() == null && request.getOcBSendOutNo() == null && request.getTransferType() == null;
            AssertUtils.cannot(isAllNull, "请求参数均为空!");
            list = mapper.selectList(new QueryWrapper<ScBTransfer>().lambda()
                    .in(CollectionUtils.isNotEmpty(request.getBillNos()), ScBTransfer::getBillNo, request.getBillNos())
                    .eq(request.getOcBSendOutId() != null, ScBTransfer::getOcBSendOutId, request.getOcBSendOutId())
                    .eq(StringUtils.isNotEmpty(request.getOcBSendOutNo()), ScBTransfer::getOcBSendOutNo, request.getOcBSendOutNo())
                    .eq(request.getTransferType() != null, ScBTransfer::getTransferType, request.getTransferType())
                    .eq(ScBTransfer::getIsactive, SgConstants.IS_ACTIVE_Y).orderByDesc(ScBTransfer::getModifieddate));
        }
        ValueHolderV14<List<ScBTransfer>> holderV14 = new ValueHolderV14<>();
        holderV14.setCode(ResultCode.SUCCESS);
        holderV14.setData(list);
        return holderV14;
    }

    /**
     * 根据单据编号查询调拨单
     *
     * @param objId 调拨单ID
     * @return 出参
     */
    public ScBTransfer queryTransferByBillNo(Long objId) {

        if (objId == null) {
            AssertUtils.logAndThrow("调拨单ID不能为空！");
        }

        ScBTransferMapper transferMapper = ApplicationContextHandle.getBean(ScBTransferMapper.class);
        ScBTransfer transfer = transferMapper.selectOne(
                new QueryWrapper<ScBTransfer>().lambda()
                        .eq(ScBTransfer::getId, objId)
                        .eq(ScBTransfer::getIsactive, SgConstants.IS_ACTIVE_Y));
        return transfer;
    }

    /**
     * 查询调拨单信息， 分页(pda用)
     *
     * @param request 入参
     * @return 出参
     */
    public ValueHolderV14<PageInfo<ScBTransfer>> queryTransferForPda(SgTransferForPdaRequest request) {

        AssertUtils.notNull(request, "请求参数不能为空！");
        User user = request.getUser();
        AssertUtils.notNull(user, "用户信息不能为空！");
        Long storeId = request.getStoreId();
        AssertUtils.notNull(storeId, "店仓不能为空！", user.getLocale());

        ScBTransferMapper transferMapper = ApplicationContextHandle.getBean(ScBTransferMapper.class);
        PageHelper.startPage(ObjectToUtil.getInt(request.getCurrentPage(), 1),
                ObjectToUtil.getInt(request.getPageSize(), 20));
        List<ScBTransfer> transferList = transferMapper.selectList(new QueryWrapper<ScBTransfer>().lambda()
                .eq(ScBTransfer::getCpCOrigId, storeId)
                .eq(request.getId() != null, ScBTransfer::getId, request.getId())
                .like(request.getBillNo() != null, ScBTransfer::getBillNo, request.getBillNo())
                .eq(request.getTransferType() != null, ScBTransfer::getTransferType, request.getTransferType())
                .ge(request.getStartTime() != null, ScBTransfer::getModifieddate, request.getStartTime())
                .le(request.getEndTime() != null, ScBTransfer::getModifieddate, request.getEndTime())
                .orderByDesc(ScBTransfer::getModifieddate));

        PageInfo<ScBTransfer> resultPage = new PageInfo<>(transferList);
        ValueHolderV14<PageInfo<ScBTransfer>> v14 = new ValueHolderV14<>();
        v14.setCode(ResultCode.SUCCESS);
        v14.setData(resultPage);
        return v14;
    }


    /**
     * 根据调拨单id查询调拨单录入明细信息，分页(pda用)
     *
     * @param request 入参
     * @return 出参
     */
    public ValueHolderV14<PageInfo<ScTransferImpItemAndTeusItemResult>> queryTransferImpItemForPda(SgTransferForPdaRequest request) {

        AssertUtils.notNull(request, "请求参数不能为空！");
        User user = request.getUser();
        AssertUtils.notNull(user, "用户信息不能为空！");
        Long transferId = request.getId();
        AssertUtils.notNull(transferId, "调拨单ID不能为空！", user.getLocale());

        // 分页查询查询录入明细
        ScBTransferImpItemMapper impItemMapper = ApplicationContextHandle.getBean(ScBTransferImpItemMapper.class);
        PageHelper.startPage(ObjectToUtil.getInt(request.getCurrentPage(), 1),
                ObjectToUtil.getInt(request.getPageSize(), 20));
        List<ScBTransferImpItem> impItemList = impItemMapper.selectList(new QueryWrapper<ScBTransferImpItem>().lambda()
                .eq(ScBTransferImpItem::getScBTransferId, transferId));

        List<ScTransferImpItemAndTeusItemResult> impItemAndTeusItemResults = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(impItemList)) {
            ScBTransferTeusItemMapper teusItemMapper = ApplicationContextHandle.getBean(ScBTransferTeusItemMapper.class);
            for (ScBTransferImpItem impItem : impItemList) {
                ScTransferImpItemAndTeusItemResult impItemAndTeusItemResult = new ScTransferImpItemAndTeusItemResult();
                BeanUtils.copyProperties(impItem, impItemAndTeusItemResult);

                // 查询录入明细中“是否箱”=是的箱明细
                Integer isTeus = impItem.getIsTeus();
                if (isTeus != null && isTeus == SgTransferConstantsIF.IS_TEUS_Y) {
                    List<ScBTransferTeusItem> transferTeusItems = teusItemMapper.selectList(
                            new QueryWrapper<ScBTransferTeusItem>().lambda()
                                    .eq(ScBTransferTeusItem::getScBTransferId, impItem.getScBTransferId())
                                    .eq(ScBTransferTeusItem::getPsCTeusId, impItem.getPsCTeusId()));
                    impItemAndTeusItemResult.setTeusItems(transferTeusItems);
                }
                impItemAndTeusItemResults.add(impItemAndTeusItemResult);
            }
        }

        PageInfo<ScTransferImpItemAndTeusItemResult> resultPage = new PageInfo<>(impItemAndTeusItemResults);
        ValueHolderV14<PageInfo<ScTransferImpItemAndTeusItemResult>> v14 = new ValueHolderV14<>();
        v14.setCode(ResultCode.SUCCESS);
        v14.setData(resultPage);
        return v14;
    }


    /**
     * add chenxinxing  2020.01.04
     * 根据主表ids查询调拨单
     *
     * @param objIds 主表ids
     */
    List<ScBTransfer> queryScBTransferByIds(List<Long> objIds) {
        ScBTransferMapper mapper = ApplicationContextHandle.getBean(ScBTransferMapper.class);
        List<ScBTransfer> scBTransfers = mapper.selectList(new QueryWrapper<ScBTransfer>().lambda().
                in(ScBTransfer::getId, objIds).eq(ScBTransfer::getIsactive, SgConstants.IS_ACTIVE_Y));
        return scBTransfers;
    }

    /**
     * 根据退货申请单号查询指定状态的调拨单
     *
     * @param statusList  状态List
     * @param applyBillNo 退货申请单号
     * @return
     */
    List<ScBTransfer> queryScBTransferByStatus(List<Integer> statusList, String applyBillNo) {
        ScBTransferMapper mapper = ApplicationContextHandle.getBean(ScBTransferMapper.class);
        return mapper.selectList(new QueryWrapper<ScBTransfer>().lambda()
                .eq(ScBTransfer::getApplyBillNo, applyBillNo)
                .in(ScBTransfer::getStatus, statusList)
        );


    }

    /**
     * 根据主表查询调拨单明细
     *
     * @param objId 主表id
     * @return 明细信息
     */
    List<ScBTransferItem> queryItems(Long objId) {
        return scBTransferItemMapper.selectList(new QueryWrapper<ScBTransferItem>().lambda().eq(ScBTransferItem::getScBTransferId, objId));
    }

    /**
     * 根据主表查询调拨单明细
     *
     * @param objId 主表id
     * @return 明细信息
     */
    List<ScBTransferImpItem> queryImpItems(Long objId) {
        return scBTransferImpItemMapper.selectList(new QueryWrapper<ScBTransferImpItem>().lambda().eq(ScBTransferImpItem::getScBTransferId, objId));
    }

    public ValueHolderV14<ScBTransfer> queryScBTransferByStockBillNo(String stockBillNo) {
        ScBTransferMapper transferMapper = ApplicationContextHandle.getBean(ScBTransferMapper.class);
        ScBTransfer scBTransfer = transferMapper.selectOne(new QueryWrapper<ScBTransfer>().lambda().eq(ScBTransfer::getStockBillNo, stockBillNo).eq(ScBTransfer::getIsactive, SgConstants.IS_ACTIVE_Y));
        ValueHolderV14<ScBTransfer> v14 = new ValueHolderV14<>();
        v14.setCode(ResultCode.SUCCESS);
        v14.setData(scBTransfer);
        return v14;
    }

    /**
     * 根据调拨单单据编号和状态查询调拨单
     *
     * @param billNo 调拨单单号
     * @param status 调拨单状态
     * @return 调拨单对象
     */
    public ScBTransfer queryTransferByBillNoAndStatus(String billNo, int status) {

        ScBTransferMapper transferMapper = ApplicationContextHandle.getBean(ScBTransferMapper.class);
        return transferMapper.selectOne(new QueryWrapper<ScBTransfer>().lambda()
                .eq(StringUtils.isNotEmpty(billNo), ScBTransfer::getBillNo, billNo)
                .eq(ScBTransfer::getStatus, status));
    }
}
