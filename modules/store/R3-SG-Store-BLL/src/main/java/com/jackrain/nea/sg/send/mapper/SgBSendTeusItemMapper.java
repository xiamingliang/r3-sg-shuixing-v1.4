package com.jackrain.nea.sg.send.mapper;

import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.send.model.table.SgBSendTeusItem;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SgBSendTeusItemMapper extends ExtentionMapper<SgBSendTeusItem> {
}