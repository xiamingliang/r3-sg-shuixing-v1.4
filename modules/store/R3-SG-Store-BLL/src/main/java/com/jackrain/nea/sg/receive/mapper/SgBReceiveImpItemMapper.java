package com.jackrain.nea.sg.receive.mapper;

import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.receive.model.table.SgBReceiveImpItem;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SgBReceiveImpItemMapper extends ExtentionMapper<SgBReceiveImpItem> {
}