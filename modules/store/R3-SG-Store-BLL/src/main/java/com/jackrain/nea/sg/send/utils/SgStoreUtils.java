package com.jackrain.nea.sg.send.utils;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Maps;
import com.jackrain.nea.data.basic.model.request.GenerateSerialNumberRequest;
import com.jackrain.nea.data.basic.model.request.SkuInfoQueryRequest;
import com.jackrain.nea.data.basic.services.BasicAdQueryService;
import com.jackrain.nea.data.basic.services.BasicPsQueryService;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.ext.sequence.util.SequenceGenUtil;
import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.ps.api.result.PsCProSkuResult;
import com.jackrain.nea.psext.model.table.PsCTeus;
import com.jackrain.nea.redis.config.CusRedisTemplate;
import com.jackrain.nea.redis.util.RedisOpsUtil;
import com.jackrain.nea.sg.adjust.model.request.SgAdjustBillSaveRequest;
import com.jackrain.nea.sg.adjust.model.request.SgAdjustImpItemSaveRequest;
import com.jackrain.nea.sg.adjust.model.request.SgAdjustItemSaveRequest;
import com.jackrain.nea.sg.adjust.model.request.SgAdjustTeusItemSaveRequest;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.mapper.CpStoreMapper;
import com.jackrain.nea.sg.basic.model.request.SgR3BaseRequest;
import com.jackrain.nea.sg.basic.model.result.SgStorageUpdateResult;
import com.jackrain.nea.sg.basic.model.table.CpCStore;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.phyadjust.model.request.SgPhyAdjustImpItemRequest;
import com.jackrain.nea.sg.receive.model.request.SgReceiveBillSaveRequest;
import com.jackrain.nea.sg.receive.model.request.SgReceiveItemSaveRequest;
import com.jackrain.nea.sg.receive.model.table.SgBReceiveItem;
import com.jackrain.nea.sg.send.common.SgSendConstantsIF;
import com.jackrain.nea.sg.send.common.SgStoreCommonResult;
import com.jackrain.nea.sg.send.model.request.SgSendBillSaveRequest;
import com.jackrain.nea.sg.send.model.request.SgSendItemSaveRequest;
import com.jackrain.nea.sg.send.model.request.SgStoreBillBase;
import com.jackrain.nea.sg.send.model.request.SgStoreBillItemBase;
import com.jackrain.nea.sg.send.model.table.SgBSendItem;
import com.jackrain.nea.sys.domain.BaseModelES;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author 舒威
 * @since 2019/4/29
 * create at : 2019/4/29 20:24
 */
@Slf4j
public class SgStoreUtils {

    public static <T extends SgR3BaseRequest> void checkR3BModelDefalut(T sourceModel) {
        User loginUser = sourceModel.getLoginUser();
        AssertUtils.notNull(loginUser, "用户未登录！");
    }

    public static <T extends SgStoreBillBase> void checkBModelDefalut(T sourceModel, boolean b) {
        AssertUtils.notNull(sourceModel.getSourceBillId(), "来源单据ID不能为空！");
        AssertUtils.notNull(sourceModel.getSourceBillType(), "单据类型不能为空！");
        if (b) {
            AssertUtils.notNull(sourceModel.getSourceBillNo(), "来源单据编号为空！");
        }
    }

    public static <T extends SgStoreBillItemBase> void checkBModelDefalut(List<T> sourceModels) {
        AssertUtils.notEmpty(sourceModels, "子表信息不能为空！");
        for (T sourceModel : sourceModels) {
            String psCSkuEcode = sourceModel.getPsCSkuEcode();
            AssertUtils.notNull(psCSkuEcode, "条码编码不能为空！");
            AssertUtils.notNull(sourceModel.getSourceBillItemId(), "[" + psCSkuEcode + "]来源单据ID不能为空！");
            AssertUtils.notNull(sourceModel.getCpCStoreId(), "[" + psCSkuEcode + "]逻辑仓ID不能为空！");
            AssertUtils.notNull(sourceModel.getCpCStoreEcode(), "[" + psCSkuEcode + "]逻辑仓编码不能为空！");
            AssertUtils.notNull(sourceModel.getCpCStoreEname(), "[" + psCSkuEcode + "]逻辑仓名称不能为空！");
            AssertUtils.notNull(sourceModel.getPsCSkuId(), "[" + psCSkuEcode + "]条码ID不能为空！");
            AssertUtils.notNull(sourceModel.getPsCProId(), "[" + psCSkuEcode + "]商品ID不能为空！");
            AssertUtils.notNull(sourceModel.getPsCProEcode(), "[" + psCSkuEcode + "]商品编码不能为空！");
            AssertUtils.notNull(sourceModel.getPsCProEname(), "[" + psCSkuEcode + "]商品名称不能为空！");
            AssertUtils.notNull(sourceModel.getPsCSpec1Id(), "[" + psCSkuEcode + "]规格1ID不能为空！");
            AssertUtils.notNull(sourceModel.getPsCSpec1Ecode(), "[" + psCSkuEcode + "]规格1编码不能为空！");
            AssertUtils.notNull(sourceModel.getPsCSpec1Ename(), "[" + psCSkuEcode + "]规格1名称不能为空！");
            AssertUtils.notNull(sourceModel.getPsCSpec2Id(), "[" + psCSkuEcode + "]规格2ID不能为空！");
            AssertUtils.notNull(sourceModel.getPsCSpec2Ecode(), "[" + psCSkuEcode + "]规格2编码不能为空！");
            AssertUtils.notNull(sourceModel.getPsCSpec2Ename(), "[" + psCSkuEcode + "]规格2名称不能为空！");
        }
    }


    /**
     * 逻辑发货单-单据类型 转 库存更新-单据类型
     *
     * @param type 逻辑发货单-单据类型
     * @return 库存更新-单据类型
     */
    public static int sendBillTypeConver(int type) {
        switch (type) {
            case SgConstantsIF.BILL_TYPE_RETAIL:
                return SgConstantsIF.BILL_TYPE_RETAIL;
            case SgConstantsIF.BILL_TYPE_SALE:
                return SgConstantsIF.BILL_TYPE_SALE;
            case SgConstantsIF.BILL_TYPE_SALE_REF:
                return SgConstantsIF.BILL_TYPE_SALE_REF;
            case SgConstantsIF.BILL_TYPE_PUR_REF:
                return SgConstantsIF.BILL_TYPE_PUR_REF;
            case SgConstantsIF.BILL_TYPE_TRANSFER:
                return SgConstantsIF.BILL_TYPE_TRANSFER;
            case SgConstantsIF.BILL_TYPE_VIPSHOP:
                return SgConstantsIF.BILL_TYPE_VIPSHOP;
            default:
                AssertUtils.logAndThrow("单据类型转换异常！");
                return 0;
        }
    }

    /**
     * 逻辑收货单-单据类型 转 库存更新-单据类型
     *
     * @param type 逻辑收货单-单据类型
     * @return 库存更新-单据类型
     */
    public static int receiveBillTypeConver(int type) {
        switch (type) {
            case SgConstantsIF.BILL_TYPE_RETAIL_REF:
                return SgConstantsIF.BILL_TYPE_RETAIL_REF;
            case SgConstantsIF.BILL_TYPE_SALE:
                return SgConstantsIF.BILL_TYPE_SALE;
            case SgConstantsIF.BILL_TYPE_SALE_REF:
                return SgConstantsIF.BILL_TYPE_SALE_REF;
            case SgConstantsIF.BILL_TYPE_PUR:
                return SgConstantsIF.BILL_TYPE_PUR;
            case SgConstantsIF.BILL_TYPE_TRANSFER:
                return SgConstantsIF.BILL_TYPE_TRANSFER;
            case SgConstantsIF.BILL_TYPE_VIPSHOP:
                return SgConstantsIF.BILL_TYPE_VIPSHOP;
            default:
                AssertUtils.logAndThrow("单据类型转换异常！");
                return 0;
        }
    }

    /**
     * 获取单据编号
     *
     * @param seq         序号生成器
     * @param table       业务单据表名
     * @param sourceModel
     * @param locale
     * @return 单据编号
     */
    public static <T extends BaseModelES> String getBillNo(String seq, String table, T sourceModel, Locale locale) {
        // 获取单据编号
        JSONObject obj = new JSONObject();
        obj.put(table, sourceModel);
        return SequenceGenUtil.generateSquence(seq, obj, locale, false);
    }

    public static <T extends BaseModelES> String getBillNoByRedis(String seq, String table, T sourceModel, User user) {
        BasicAdQueryService adQueryService = ApplicationContextHandle.getBean(BasicAdQueryService.class);
        GenerateSerialNumberRequest adRequest = new GenerateSerialNumberRequest();
        adRequest.setSequenceName(seq);
        JSONObject obj = new JSONObject();
        obj.put(table.toUpperCase(), sourceModel);
        adRequest.setFixColumn(obj);
        adRequest.setTest(false);
        List<String> billNos = adQueryService.generateSerialNumber(adRequest, user);
        if (CollectionUtils.isNotEmpty(billNos)) {
            return billNos.get(0);
        } else {
            throw new NDSException("获取单据编号失败!");
        }
    }

    /**
     * 获取逻辑仓库存控制
     *
     * @param sendRequest    逻辑发货单request
     * @param receiveRequest 逻辑收货单request
     * @param receiveItems   逻辑收货单明细
     * @param sendItems      逻辑发货单明细
     * @return map
     */
    public static Map<Long, CpCStore> getNegativeStock(SgSendBillSaveRequest sendRequest,
                                                       SgReceiveBillSaveRequest receiveRequest,
                                                       List<SgBReceiveItem> receiveItems,
                                                       List<SgBSendItem> sendItems) {
        List<Long> stores = null;
        if (receiveRequest != null) {
            stores = receiveRequest.getItemList().stream()
                    .map(SgReceiveItemSaveRequest::getCpCStoreId).distinct().collect(Collectors.toList());
        } else if (sendRequest != null) {
            stores = sendRequest.getItemList().stream()
                    .map(SgSendItemSaveRequest::getCpCStoreId).distinct().collect(Collectors.toList());
        } else if (CollectionUtils.isNotEmpty(receiveItems)) {
            stores = receiveItems.stream()
                    .map(SgBReceiveItem::getCpCStoreId).distinct().collect(Collectors.toList());
        } else if (CollectionUtils.isNotEmpty(sendItems)) {
            stores = sendItems.stream()
                    .map(SgBSendItem::getCpCStoreId).distinct().collect(Collectors.toList());
        }
        if (CollectionUtils.isNotEmpty(stores)) {
            return getNegativeStock(stores);
        }
        return null;
    }


    /**
     * 获取逻辑仓库存控制
     *
     * @param stores 逻辑仓ids
     * @return map
     */
    public static Map<Long, CpCStore> getNegativeStock(List<Long> stores) {
        CpStoreMapper storeMapper = ApplicationContextHandle.getBean(CpStoreMapper.class);
        List<CpCStore> cpCStores = storeMapper.selectList(new QueryWrapper<CpCStore>()
                .lambda().select(CpCStore::getId, CpCStore::getIsnegative).in(CpCStore::getId, stores));
        return cpCStores.stream().collect(Collectors.toMap(e -> e.getId(), e -> e));
    }


    /**
     * check同步库存占用结果
     *
     * @param err 自定义错误信息
     * @param v14 同步库存结果
     */
    public static void isSuccess(String err, ValueHolderV14<SgStorageUpdateResult> v14) {
        SgStorageUpdateResult result = v14.getData();
        boolean flag = result.getPreoutUpdateResult() == SgConstantsIF.PREOUT_RESULT_ERROR;
      /*  List<SgStorageOutStockResult> outStockItemList = result.getOutStockItemList();
        if (CollectionUtils.isNotEmpty(outStockItemList)) {
            String errMsg = err;
            for (SgStorageOutStockResult outStockResult : outStockItemList) {
                Long billId = outStockResult.getBillId();
                Long sourceBillItemId = outStockResult.getBillItemId();
                Long psCSkuId = outStockResult.getPsCSkuId();
                Long storeId = outStockResult.getCpCStoreId();
                errMsg += ",来源单据id" + billId + ",来源单据明细" + sourceBillItemId + ",逻辑仓" + storeId + ",条码" + psCSkuId + "占单失败!";
            }
            AssertUtils.logAndThrow(errMsg);
        }*/
        if (flag) {
            AssertUtils.logAndThrow(err + "同步库存占用失败:" + v14.getMessage());
        }
    }

    /**
     * 初始化RedisTemplate
     */
    public static CusRedisTemplate<String, String> initRedisTemplate() {
        return RedisOpsUtil.getObjRedisTemplate();
    }

    /**
     * 逻辑发货单-录入/箱内明细 转换 条码明细
     * 注意：各业务单据间出现交互时,上下游传递参数时会传条码明细、录入明细、箱内明细
     * 同时上下游有约定：request大小>=4MB，request整个对象存入redis(方便补偿任务直接转换)，原来的request对象会传入redisKey
     * request里 除了录入明细、箱内明细不传外，其他参数按原逻辑传入不变(同时需要将redisKey传过来)
     * redisKey 定义原则：中心:表名:业务节点:上游单据id  比如 采购退审核生成逻辑发货单redisKey: oc:oc_b_refund_purchase:13:1998
     */
    public static void impRequestConvertMethod(SgSendBillSaveRequest request) {
        if (CollectionUtils.isEmpty(request.getImpItemList()) && StringUtils.isEmpty(request.getRedisKey())) {
            AssertUtils.logAndThrow("录入明细不能为空!");
        } else if (CollectionUtils.isEmpty(request.getImpItemList()) && StringUtils.isNotEmpty(request.getRedisKey())) {
            //从redis中取出大数据量参数,并放入request
            log.debug("redisKey:" + request.getRedisKey());
            CusRedisTemplate<String, String> redisTemplate = RedisOpsUtil.getStrRedisTemplate();
            String redisKey = redisTemplate.opsForValue().get(request.getRedisKey());
            SgSendBillSaveRequest redisRequest = JSONObject.parseObject(redisKey, SgSendBillSaveRequest.class);
            if (redisRequest == null || CollectionUtils.isEmpty(redisRequest.getImpItemList())) {
                AssertUtils.logAndThrow("[" + redisKey + "]录入明细不能为空!");
            } else {
                request.setItemList(redisRequest.getItemList());
                request.setImpItemList(redisRequest.getImpItemList());
                request.setTeusItemList(redisRequest.getTeusItemList());
            }
        }
    }


    public static <T extends ExtentionMapper> void batchInsertTeus(List models, String errMsg, Integer pageSize, T mapper) {
        List<List> baseModelPageList = StorageESUtils.getBaseModelPageList(models, pageSize);
        for (List model : baseModelPageList) {
            int i = mapper.batchInsert(model);
            if (i != model.size()) {
                AssertUtils.logAndThrow(errMsg + "批量新增失败!");
            }
        }
    }

    /**
     * 逻辑收货单-录入明细转换成条码明细/箱内明细
     */
    public static void impSgReceiveRequestConvertMethod(SgReceiveBillSaveRequest request) {
        if (CollectionUtils.isEmpty(request.getImpItemList()) && StringUtils.isEmpty(request.getRedisKey())) {
            AssertUtils.logAndThrow("录入明细不能为空!");
        } else if (CollectionUtils.isEmpty(request.getImpItemList()) && StringUtils.isNotEmpty(request.getRedisKey())) {
            //从redis中取出大数据量参数,并放入request
            log.debug("redisKey:" + request.getRedisKey());
            CusRedisTemplate<String, String> redisTemplate = RedisOpsUtil.getStrRedisTemplate();
            String redisKey = redisTemplate.opsForValue().get(request.getRedisKey());
            SgReceiveBillSaveRequest redisRequest = JSONObject.parseObject(redisKey, SgReceiveBillSaveRequest.class);
            if (redisRequest == null || CollectionUtils.isEmpty(redisRequest.getImpItemList())) {
                AssertUtils.logAndThrow("[" + redisKey + "]录入明细不能为空!");
            } else {
                request.setItemList(redisRequest.getItemList());
                request.setImpItemList(redisRequest.getImpItemList());
                request.setTeusItemList(redisRequest.getTeusItemList());
            }
        }
    }

    /**
     * 逻辑调整单-录入明细转换成条码明细/箱内明细
     */
    public static void impSgAdjustRequestConvertMethod(SgAdjustBillSaveRequest request) {

        if (CollectionUtils.isEmpty(request.getImpItems()) && StringUtils.isEmpty(request.getRedisKey())) {
            AssertUtils.logAndThrow("录入明细不能为空!");
        } else if (CollectionUtils.isEmpty(request.getImpItems()) && StringUtils.isNotEmpty(request.getRedisKey())) {
            //从redis中取出大数据量参数,并放入request
            log.debug("redisKey:" + request.getRedisKey());
            CusRedisTemplate<String, String> redisTemplate = RedisOpsUtil.getStrRedisTemplate();
            String redisKey = redisTemplate.opsForValue().get(request.getRedisKey());
            SgAdjustBillSaveRequest redisRequest = JSONObject.parseObject(redisKey, SgAdjustBillSaveRequest.class);
            if (redisRequest == null || CollectionUtils.isEmpty(redisRequest.getImpItems())) {
                AssertUtils.logAndThrow("[" + redisKey + "]录入明细不能为空!");
            } else {
                request.setImpItems(redisRequest.getImpItems());
                request.setTeusItems(redisRequest.getTeusItems());
            }
        }

        HashMap<Long, SgAdjustItemSaveRequest> itemsMap = Maps.newHashMap();

        //录入明细转条码明细
        for (SgAdjustImpItemSaveRequest impitem : request.getImpItems()) {
            if (impitem.getIsTeus().equals(SgSendConstantsIF.IS_TEUS_Y)) {
                if (impitem.getIsTeus() == null || StringUtils.isEmpty(impitem.getPsCTeusEcode())) {
                    AssertUtils.logAndThrow("录入明细中商品编码[" + impitem.getPsCProEcode() + "],箱号为空!");
                }
            } else {
                SgAdjustItemSaveRequest itemSaveRequest = new SgAdjustItemSaveRequest();
                BeanUtils.copyProperties(impitem, itemSaveRequest);
                itemsMap.put(itemSaveRequest.getPsCSkuId(), itemSaveRequest);
            }
        }

        //箱内明细转条码明细    注意箱内明细和条码明细合并
        if (CollectionUtils.isNotEmpty(request.getTeusItems())) {
            for (SgAdjustTeusItemSaveRequest teusItemSaveRequest : request.getTeusItems()) {
                SgAdjustItemSaveRequest itemSaveRequest = new SgAdjustItemSaveRequest();
                BeanCopyUtils.copyProperties(teusItemSaveRequest, itemSaveRequest);
                //箱内明细转条码明细后 占用数量默认就是数量
                if (itemsMap.containsKey(itemSaveRequest.getPsCSkuId())) {
                    SgAdjustItemSaveRequest orgItemRequest = itemsMap.get(itemSaveRequest.getPsCSkuId());
                    BigDecimal qty = Optional.ofNullable(itemSaveRequest.getQty()).orElse(BigDecimal.ZERO);
                    orgItemRequest.setQty(orgItemRequest.getQty().add(qty));
                } else {
                    itemsMap.put(itemSaveRequest.getPsCSkuId(), itemSaveRequest);
                }
            }
            BeanCopyUtils.clear();
        }
        request.setItems(new ArrayList<>(itemsMap.values()));
    }

    /**
     * 箱库存补充信息//补充条码明细和箱明细
     *
     * @param sgPhyAdjustImpItemSaveRequest 库存调整单录入明细
     * @param locale                        本地化
     * @return
     */
    public static SgStoreCommonResult queryPsCTeusAndPsCProSkuResult(List<SgPhyAdjustImpItemRequest> sgPhyAdjustImpItemSaveRequest, Locale locale) {

        SgStoreCommonResult sgStoreCommonResult = new SgStoreCommonResult();
        BasicPsQueryService psQueryService = ApplicationContextHandle.getBean(BasicPsQueryService.class);
        SkuInfoQueryRequest request = new SkuInfoQueryRequest();
        List<String> skuECodes = new ArrayList<>();
        List<String> boxCodeList = new ArrayList<>();
        //暂存库存调整单录入明细
        if (CollectionUtils.isNotEmpty(sgPhyAdjustImpItemSaveRequest)) {
            sgPhyAdjustImpItemSaveRequest.forEach(sgPhyAdjustImpItemRequest -> {
                //补全库存调整单录入明细数据
                Long rowId = sgPhyAdjustImpItemRequest.getId();
                // 获取条码、箱编码
                String skuECode = sgPhyAdjustImpItemRequest.getPsCSkuEcode();
                String boxCode = sgPhyAdjustImpItemRequest.getPsCTeusEcode();
                if (rowId == -1 && StringUtils.isNotEmpty(skuECode)) {
                    skuECodes.add(skuECode);
                } else if (rowId == -1 && StringUtils.isNotEmpty(boxCode)) {
                    boxCodeList.add(boxCode);
                }
            });
        }
        HashMap<String, PsCTeus> boxHashMap = new HashMap<>();
        HashMap<String, PsCProSkuResult> queryResult = new HashMap<>();
        try {
            if (org.apache.dubbo.common.utils.CollectionUtils.isNotEmpty(skuECodes) || org.apache.dubbo.common.utils.CollectionUtils.isNotEmpty(boxCodeList)) {
                if (skuECodes.size() > 0) {
                    request.setSkuEcodeList(skuECodes);
                    queryResult = psQueryService.getSkuInfoByEcode(request);
                } else if (boxCodeList.size() > 0) {
                    boxHashMap = psQueryService.queryTeusInfoByEcode(boxCodeList);
                }
                sgStoreCommonResult.setBoxHashMap(boxHashMap);
                sgStoreCommonResult.setQueryResult(queryResult);
            }
        } catch (Exception e) {
            com.jackrain.nea.utils.AssertUtils.logAndThrow("商品信息获取失败！" + e.getMessage(), locale);
        }
        return sgStoreCommonResult;
    }
}
