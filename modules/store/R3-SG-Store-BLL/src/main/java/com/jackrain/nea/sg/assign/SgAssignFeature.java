package com.jackrain.nea.sg.assign;

import com.jackrain.nea.oc.basic.filter.SkuInfoByRedisForBoxFilter;
import com.jackrain.nea.sg.assign.filter.OcAssignRedisLockFilter;
import com.jackrain.nea.sg.assign.filter.SgAssignSaveFilter;
import com.jackrain.nea.sg.assign.filter.SgAssignSubmitFilter;
import com.jackrain.nea.sg.assign.filter.SgAssignVoidFilter;
import com.jackrain.nea.sg.assign.validate.SgAssignSubmitValidate;
import com.jackrain.nea.sg.assign.validate.SgAssignVoidValidate;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.tableService.Feature;
import com.jackrain.nea.tableService.constants.Constants;
import com.jackrain.nea.tableService.feature.FeatureAnnotation;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author : 孙俊磊
 * @since : 2019-10-11
 * create at : 2019-10-11 3:29 PM
 */
@FeatureAnnotation(value = "SgAssignFeature", description = "铺货单Feature", isSystem = true)
public class SgAssignFeature extends Feature {

    /**
     * 校验器
     */
    @Autowired
    private SgAssignSubmitValidate assignSubmitValidate;
    @Autowired
    private SgAssignVoidValidate assignVoidValidate;

    /**
     * 逻辑Filter
     */
    @Autowired
    private OcAssignRedisLockFilter redisLockFilter;

    @Autowired
    private SkuInfoByRedisForBoxFilter boxFilter;

    @Autowired
    private SgAssignSaveFilter assignSaveFilter;

    @Autowired
    private SgAssignSubmitFilter assignSubmitFilter;

    @Autowired
    private SgAssignVoidFilter assignVoidFilter;

    @Override
    protected void initialization() {
        addValidator(assignSubmitValidate, (tableName, actionName) -> tableName.equalsIgnoreCase(SgConstants.SG_B_ASSIGN) &&
                actionName.equals(Constants.ACTION_SUBMIT));
        addValidator(assignVoidValidate, (tableName, actionName) -> tableName.equalsIgnoreCase(SgConstants.SG_B_ASSIGN) &&
                actionName.equals(Constants.ACTION_VOID));

        addFilter(redisLockFilter, (tableName, actionName)
                -> tableName.equalsIgnoreCase(SgConstants.SG_B_ASSIGN));

        // 补充基本信息（箱信息或者条码信息）
        addFilter(boxFilter, (tableName, actionName) -> tableName.equalsIgnoreCase(SgConstants.SG_B_ASSIGN) &&
                (actionName.equals(Constants.ACTION_SAVE) || actionName.equals(Constants.ACTION_ADD)));

        addFilter(assignSaveFilter, (tableName, actionName) -> tableName.equalsIgnoreCase(SgConstants.SG_B_ASSIGN) &&
                (actionName.equals(Constants.ACTION_SAVE) || actionName.equals(Constants.ACTION_ADD)));

        addFilter(assignSubmitFilter, (tableName, actionName) -> tableName.equalsIgnoreCase(SgConstants.SG_B_ASSIGN) &&
                actionName.equals(Constants.ACTION_SUBMIT));

        addFilter(assignVoidFilter, (tableName, actionName) -> tableName.equalsIgnoreCase(SgConstants.SG_B_ASSIGN) &&
                actionName.equals(Constants.ACTION_VOID));
    }
}
