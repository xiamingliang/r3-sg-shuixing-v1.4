package com.jackrain.nea.sg.receive.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.model.result.SgStorageUpdateResult;
import com.jackrain.nea.sg.basic.model.table.CpCStore;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.receive.mapper.SgBReceiveItemMapper;
import com.jackrain.nea.sg.receive.mapper.SgBReceiveMapper;
import com.jackrain.nea.sg.receive.model.request.SgReceiveBillSaveRequest;
import com.jackrain.nea.sg.receive.model.result.SgReceiveBillSaveResult;
import com.jackrain.nea.sg.receive.model.table.SgBReceive;
import com.jackrain.nea.sg.receive.model.table.SgBReceiveItem;
import com.jackrain.nea.sg.send.utils.SgStoreUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

/**
 * @author 舒威
 * @since 2019/6/10
 * create at : 2019/6/10 14:51
 */
@Slf4j
@Component
public class SgReceiveSaveCancelService {

    @Autowired
    private SgBReceiveMapper receiveMapper;

    @Autowired
    private SgBReceiveItemMapper receiveItemMapper;

    @Autowired
    private SgReceiveStorageService storageService;

    @Autowired
    private SgReceiveSaveService saveService;


    @Transactional(rollbackFor = Exception.class)
    public ValueHolderV14<SgReceiveBillSaveResult> cancelSaveSgBReceive(SgReceiveBillSaveRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("来源单据id[" + request.getSgReceive().getSourceBillId()
                    + "],逻辑收货单保存补偿服务入参:" + JSONObject.toJSONString(request) + ";");
        }

        saveService.checkParams(request);
        ValueHolderV14<SgReceiveBillSaveResult> v14 = new ValueHolderV14();
        v14.setCode(ResultCode.SUCCESS);
        v14.setMessage("success");

        try {
            SgBReceive receive = receiveMapper.selectOne(new QueryWrapper<SgBReceive>().lambda()
                    .eq(SgBReceive::getSourceBillId, request.getSgReceive().getSourceBillId())
                    .eq(SgBReceive::getSourceBillType, request.getSgReceive().getSourceBillType())
                    .eq(SgBReceive::getIsactive, SgConstants.IS_ACTIVE_Y));
            if (receive == null) {
                v14.setCode(ResultCode.FAIL);
                v14.setMessage("当前记录已不存在!");
                return v14;
            }

            receiveItemMapper.delete(new UpdateWrapper<SgBReceiveItem>().lambda().eq(SgBReceiveItem::getSgBReceiveId, receive.getId()));
            receiveMapper.delete(new UpdateWrapper<SgBReceive>().lambda().eq(SgBReceive::getId, receive.getId()));

            request.setIsCancel(true);
            request.setUpdateMethod(SgConstantsIF.ITEM_UPDATE_TYPE_INC);
            Map<Long, CpCStore> negativeStock = SgStoreUtils.getNegativeStock(null, request, null, null);
            ValueHolderV14<SgStorageUpdateResult> storageResult = storageService.updateReceiveStoragePrein(receive, null, request,
                    null, null, request.getLoginUser(), false, negativeStock, false, null, null, null, null, null);
            if (storageResult.isOK()) {
                //分销单据 -> check同步库存占用结果
                SgStoreUtils.isSuccess("逻辑收货单保存补偿失败!", storageResult);
                SgReceiveBillSaveResult data = new SgReceiveBillSaveResult();
                data.setPreoutUpdateResult(storageResult.getData().getPreoutUpdateResult());
                v14.setData(data);
            } else {
                v14.setCode(ResultCode.FAIL);
                v14.setMessage(storageResult.getMessage());
            }
        } catch (Exception e) {
            e.printStackTrace();
            AssertUtils.logAndThrowExtra("逻辑收货单保存补偿服务异常!", e, request.getLoginUser().getLocale());
        }
        return v14;
    }
}
