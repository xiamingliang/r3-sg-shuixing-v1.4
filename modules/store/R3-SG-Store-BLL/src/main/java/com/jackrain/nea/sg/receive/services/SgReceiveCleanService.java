package com.jackrain.nea.sg.receive.services;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.redis.config.CusRedisTemplate;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.config.SgStorageBoxConfig;
import com.jackrain.nea.sg.basic.model.result.SgStorageUpdateResult;
import com.jackrain.nea.sg.basic.model.table.CpCStore;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.receive.common.SgReceiveConstantsIF;
import com.jackrain.nea.sg.receive.mapper.SgBReceiveImpItemMapper;
import com.jackrain.nea.sg.receive.mapper.SgBReceiveItemMapper;
import com.jackrain.nea.sg.receive.mapper.SgBReceiveMapper;
import com.jackrain.nea.sg.receive.mapper.SgBReceiveTeusItemMapper;
import com.jackrain.nea.sg.receive.model.request.SgReceiveBillCleanRequest;
import com.jackrain.nea.sg.receive.model.result.SgReceiveBillCleanResult;
import com.jackrain.nea.sg.receive.model.table.SgBReceive;
import com.jackrain.nea.sg.receive.model.table.SgBReceiveImpItem;
import com.jackrain.nea.sg.receive.model.table.SgBReceiveItem;
import com.jackrain.nea.sg.receive.model.table.SgBReceiveTeusItem;
import com.jackrain.nea.sg.send.utils.SgStoreESUtils;
import com.jackrain.nea.sg.send.utils.SgStoreUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @author 舒威
 * @since 2019/4/24
 * create at : 2019/5/27 16:10
 */
@Slf4j
@Component
public class SgReceiveCleanService {

    @Autowired
    private SgReceiveStorageService storageService;

    @Autowired
    private SgStorageBoxConfig storageBoxConfig;

    @Autowired
    private SgStoreESUtils storeESUtils;

    @Transactional(rollbackFor = Exception.class)
    public ValueHolderV14<SgReceiveBillCleanResult> cleanSgBReceive(SgReceiveBillCleanRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("来源单据id[" + request.getSourceBillId()
                    + "],逻辑收货单清空服务入参" + JSONObject.toJSONString(request) + ";");
        }

        ValueHolderV14<SgReceiveBillCleanResult> v14 = new ValueHolderV14<>();
        SgStoreUtils.checkBModelDefalut(request, false);
        User user = request.getLoginUser();
        Locale locale = user.getLocale();

        Long sourceBillId = request.getSourceBillId();
        Integer sourceBillType = request.getSourceBillType();

        SgBReceiveMapper receiveMapper = ApplicationContextHandle.getBean(SgBReceiveMapper.class);
        SgBReceiveItemMapper receiveItemMapper = ApplicationContextHandle.getBean(SgBReceiveItemMapper.class);
        SgBReceiveImpItemMapper impItemMapper = ApplicationContextHandle.getBean(SgBReceiveImpItemMapper.class);
        SgBReceiveTeusItemMapper teusItemMapper = ApplicationContextHandle.getBean(SgBReceiveTeusItemMapper.class);


        CusRedisTemplate<String, String> redisTemplate = SgStoreUtils.initRedisTemplate();
        String lockKsy = SgConstants.SG_B_RECEIVE + ":" + sourceBillId + ":" + sourceBillType;

        if (redisTemplate.opsForValue().get(lockKsy) == null) {
            try {
                redisTemplate.opsForValue().set(lockKsy, "OK", 30, TimeUnit.MINUTES);
                SgBReceive receive = new SgBReceive();
                //记录待清空明细ids
                JSONArray ids = new JSONArray();
                List<SgBReceiveItem> receiveItems = Lists.newArrayList();
                List<SgBReceiveImpItem> impItems = Lists.newArrayList();
                Map<Long, BigDecimal> skuTeusCounts = Maps.newHashMap();
                try {
                    List<SgBReceive> bReceives = receiveMapper.selectList(new QueryWrapper<SgBReceive>().lambda()
                            .eq(SgBReceive::getSourceBillId, sourceBillId)
                            .eq(SgBReceive::getSourceBillType, sourceBillType)
                            .eq(SgBReceive::getIsactive, SgConstants.IS_ACTIVE_Y));
                    if (CollectionUtils.isEmpty(bReceives)) {
                        v14.setCode(ResultCode.FAIL);
                        v14.setMessage("不存在来源单据id" + sourceBillId + ",来源单据类型" + sourceBillType + "的逻辑收货单!");
                        return v14;
                    } else if (bReceives.size() > 1) {
                        v14.setCode(ResultCode.FAIL);
                        v14.setMessage("存在多条来源单据id" + sourceBillId + ",来源单据类型" + sourceBillType + "的逻辑收货单!");
                        return v14;
                    } else {
                        receive = bReceives.get(0);
                    }

                    Integer billStatus = receive.getBillStatus();
                    if (!(SgReceiveConstantsIF.BILL_RECEIVE_STATUS_CREATE == billStatus
                            || SgReceiveConstantsIF.BILL_RECEIVE_STATUS_UPDATE == billStatus)) {
                        AssertUtils.logAndThrow("当前单据状态不能被清空！", locale);
                    }

                    //更新逻辑收货单表
                    receive.setTotQtyPrein(BigDecimal.ZERO);
                    receive.setServiceNode(request.getServiceNode());
                    StorageESUtils.setBModelDefalutDataByUpdate(receive, user);
                    receive.setModifierename(user.getEname());
                    receiveMapper.updateById(receive);

                    receiveItems = receiveItemMapper.selectList(new QueryWrapper<SgBReceiveItem>().lambda()
                            .eq(SgBReceiveItem::getSgBReceiveId, receive.getId()));
                    receiveItems.forEach(sgBReceiveItem -> ids.add(sgBReceiveItem.getId()));

                    //明细记录删除
                    receiveItemMapper.delete(new QueryWrapper<SgBReceiveItem>().lambda()
                            .eq(SgBReceiveItem::getSgBReceiveId, receive.getId()));

                    if (storageBoxConfig.getBoxEnable()) {
                        impItems = impItemMapper.selectList(new QueryWrapper<SgBReceiveImpItem>().lambda()
                                .eq(SgBReceiveImpItem::getSgBReceiveId, receive.getId()));

                        impItemMapper.delete(new QueryWrapper<SgBReceiveImpItem>().lambda()
                                .eq(SgBReceiveImpItem::getSgBReceiveId, receive.getId()));

                        List<SgBReceiveTeusItem> teusItems = teusItemMapper.selectList(new QueryWrapper<SgBReceiveTeusItem>().lambda()
                                .eq(SgBReceiveTeusItem::getSgBReceiveId, receive.getId()));

                        teusItemMapper.delete(new QueryWrapper<SgBReceiveTeusItem>().lambda()
                                .eq(SgBReceiveTeusItem::getSgBReceiveId, receive.getId()));

                        skuTeusCounts = teusItems.stream().collect(Collectors.groupingBy(SgBReceiveTeusItem::getPsCSkuId,
                                Collectors.reducing(BigDecimal.ZERO, SgBReceiveTeusItem::getQty, (a, b) -> a.add(b))));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    AssertUtils.logAndThrowExtra("清空逻辑收货单失败", e, locale);
                }

                Map<Long, CpCStore> negativeStock = SgStoreUtils.getNegativeStock(null, null, receiveItems, null);
                ValueHolderV14<SgStorageUpdateResult> callAPIResult = storageService.updateReceiveStoragePrein(receive, receiveItems, null,
                        null, "clean", user, false, negativeStock, false, impItems, skuTeusCounts, null, null, null);
                if (callAPIResult.isOK()) {
                    //check同步库存占用结果
                    SgStoreUtils.isSuccess("清空逻辑收货单失败!", callAPIResult);
                    v14.setCode(ResultCode.SUCCESS);
                    v14.setMessage("清空逻辑收货单成功");
                } else {
                    AssertUtils.logAndThrow("清空逻辑收货单失败!" + callAPIResult.getMessage(), locale);
                }

                //推送ES
                storeESUtils.pushESByReceive(receive.getId(), false, true, ids, receiveMapper, receiveItemMapper);
            } catch (Exception e) {
                e.printStackTrace();
                AssertUtils.logAndThrowExtra(e, user.getLocale());
            } finally {
                try {
                    redisTemplate.delete(lockKsy);
                } catch (Exception e) {
                    e.printStackTrace();
                    log.error(this.getClass().getName() + lockKsy + "释放锁失败，请联系管理员！");
                    AssertUtils.logAndThrow("释放锁失败，请联系管理员！");
                }
            }
        } else {
            v14.setCode(ResultCode.FAIL);
            v14.setMessage("清空逻辑收货单中，请稍后重试！");
        }

        if (log.isDebugEnabled()) {
            log.debug("来源单据id[" + request.getSourceBillId()
                    + "],逻辑收货单清空服务出参:" + v14.toJSONObject().toJSONString() + ";");
        }
        return v14;
    }
}
