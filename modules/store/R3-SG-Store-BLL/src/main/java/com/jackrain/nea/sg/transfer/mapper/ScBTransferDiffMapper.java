package com.jackrain.nea.sg.transfer.mapper;

import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.transfer.model.table.ScBTransferDiff;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ScBTransferDiffMapper extends ExtentionMapper<ScBTransferDiff> {
}