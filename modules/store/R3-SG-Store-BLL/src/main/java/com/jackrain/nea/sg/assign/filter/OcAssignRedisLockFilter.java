package com.jackrain.nea.sg.assign.filter;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.oc.basic.common.OcBasicConstants;
import com.jackrain.nea.redis.config.CusRedisTemplate;
import com.jackrain.nea.redis.util.RedisOpsUtil;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.tableService.filter.BaseFilter;
import com.jackrain.nea.utils.AssertUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * @author leexh
 * @since 2019/12/1 16:50
 * desc: 铺货单上锁解锁
 */
@Slf4j
@Component
public class OcAssignRedisLockFilter extends BaseFilter {

    /**
     * 上锁
     *
     * @param param 参数
     */
    @Override
    public void firstAct(JSONObject param) {
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ",firstAssignParam:" + param);
        }

        Long objId = param.getLong(OcBasicConstants.OBJID);
        if (objId > 0) {
            CusRedisTemplate<String, String> redisTemplate = RedisOpsUtil.getStrRedisTemplate();
            String lockKsy = SgConstants.SG_B_ASSIGN + ":" + objId;
            Boolean blnCanInit = redisTemplate.opsForValue().setIfAbsent(lockKsy, "OK");
            if (blnCanInit != null && blnCanInit) {
                redisTemplate.expire(lockKsy, 30, TimeUnit.MINUTES);
            } else {
                AssertUtils.logAndThrow("当前单据操作中，请稍后重试...");
            }
        }
    }

    /**
     * 解锁
     *
     * @param param 参数
     */
    @Override
    public void finalAct(JSONObject param) {
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ",finalActAssignParam:" + param);
        }
        Long objId = param.getLong(OcBasicConstants.OBJID);
        if (objId > 0) {
            CusRedisTemplate<String, String> redisTemplate = RedisOpsUtil.getStrRedisTemplate();
            String lockKsy = SgConstants.SG_B_ASSIGN + ":" + objId;
            if (redisTemplate.opsForValue().get(lockKsy) != null) {
                redisTemplate.delete(lockKsy);
            }
        }
    }
}
