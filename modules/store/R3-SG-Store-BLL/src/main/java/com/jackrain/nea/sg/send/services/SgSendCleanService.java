package com.jackrain.nea.sg.send.services;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.redis.config.CusRedisTemplate;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.config.SgStorageBoxConfig;
import com.jackrain.nea.sg.basic.model.result.SgStorageUpdateResult;
import com.jackrain.nea.sg.basic.model.table.CpCStore;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.send.common.SgSendConstantsIF;
import com.jackrain.nea.sg.send.mapper.SgBSendImpItemMapper;
import com.jackrain.nea.sg.send.mapper.SgBSendItemMapper;
import com.jackrain.nea.sg.send.mapper.SgBSendMapper;
import com.jackrain.nea.sg.send.mapper.SgBSendTeusItemMapper;
import com.jackrain.nea.sg.send.model.request.SgSendBillCleanRequest;
import com.jackrain.nea.sg.send.model.result.SgSendBillCleanResult;
import com.jackrain.nea.sg.send.model.table.SgBSend;
import com.jackrain.nea.sg.send.model.table.SgBSendImpItem;
import com.jackrain.nea.sg.send.model.table.SgBSendItem;
import com.jackrain.nea.sg.send.model.table.SgBSendTeusItem;
import com.jackrain.nea.sg.send.utils.SgStoreESUtils;
import com.jackrain.nea.sg.send.utils.SgStoreUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;


/**
 * @author 舒威
 * @since 2019/4/24
 * create at : 2019/4/24 13:18
 */
@Slf4j
@Component
public class SgSendCleanService {

    @Autowired
    private SgSendStorageService storageService;

    @Autowired
    private SgStorageBoxConfig storageBoxConfig;

    @Autowired
    private SgStoreESUtils storeESUtils;

    @Transactional(rollbackFor = Exception.class)
    public ValueHolderV14<SgSendBillCleanResult> cleanSgBSend(SgSendBillCleanRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("来源单据id[" + request.getSourceBillId()
                    + "],逻辑发货单清空服务入参:" + JSONObject.toJSONString(request) + ";");
        }

        ValueHolderV14<SgSendBillCleanResult> v14 = new ValueHolderV14<>();
        SgStoreUtils.checkBModelDefalut(request, false);
        User user = request.getLoginUser();
        Locale locale = user.getLocale();
        Long sourceBillId = request.getSourceBillId();
        Integer sourceBillType = request.getSourceBillType();

        CusRedisTemplate<String, String> redisTemplate = SgStoreUtils.initRedisTemplate();
        String lockKsy = SgConstants.SG_B_SEND + ":" + sourceBillId + ":" + sourceBillType;

        SgBSendMapper sendMapper = ApplicationContextHandle.getBean(SgBSendMapper.class);
        SgBSendItemMapper sendItemMapper = ApplicationContextHandle.getBean(SgBSendItemMapper.class);
        SgBSendImpItemMapper impItemMapper = ApplicationContextHandle.getBean(SgBSendImpItemMapper.class);
        SgBSendTeusItemMapper teusItemMapper = ApplicationContextHandle.getBean(SgBSendTeusItemMapper.class);

        Boolean ifAbsent = redisTemplate.opsForValue().setIfAbsent(lockKsy, "OK");
        if (ifAbsent != null && ifAbsent) {
            redisTemplate.expire(lockKsy, 30, TimeUnit.MINUTES);
        } else {
            v14.setCode(ResultCode.FAIL);
            v14.setMessage("清空逻辑发货单中，请稍后重试！");
            return v14;
        }

        try {
            SgBSend sgBSend = new SgBSend();
            //记录待清空明细ids
            JSONArray ids = new JSONArray();
            List<SgBSendItem> sgBSendItems = Lists.newArrayList();
            List<SgBSendImpItem> impItems = Lists.newArrayList();
            Map<Long, BigDecimal> skuTeusCounts = Maps.newHashMap();
            try {
                List<SgBSend> bSends = sendMapper.selectList(new QueryWrapper<SgBSend>().lambda()
                        .eq(SgBSend::getSourceBillId, sourceBillId)
                        .eq(SgBSend::getSourceBillType, sourceBillType)
                        .eq(SgBSend::getIsactive, SgConstants.IS_ACTIVE_Y));
                if (CollectionUtils.isEmpty(bSends)) {
                    v14.setCode(ResultCode.FAIL);
                    v14.setMessage("不存在来源单据id" + sourceBillId + ",来源单据类型" + sourceBillType + "的逻辑发货单!");
                    return v14;
                } else if (bSends.size() > 1) {
                    v14.setCode(ResultCode.FAIL);
                    v14.setMessage("存在多条来源单据id" + sourceBillId + ",来源单据类型" + sourceBillType + "的逻辑发货单!");
                    return v14;
                } else {
                    sgBSend = bSends.get(0);
                }

                if (!request.getIsRepeatClean()) {
                    Integer billStatus = sgBSend.getBillStatus();
                    if (!(SgSendConstantsIF.BILL_SEND_STATUS_CREATE.equals(billStatus)
                            || SgSendConstantsIF.BILL_SEND_STATUS_UPDATE.equals(billStatus))) {
                        AssertUtils.logAndThrow("当前单据状态不能被清空！", locale);
                    }
                }

                //更新逻辑发货单表
                sgBSend.setTotQtyPreout(BigDecimal.ZERO);
                //start sunjunlei清空totqtysend
                sgBSend.setTotQtySend(BigDecimal.ZERO);
                //end sunjunlei清空totqtysend
                sgBSend.setServiceNode(request.getServiceNode());
                StorageESUtils.setBModelDefalutDataByUpdate(sgBSend, user);
                sgBSend.setModifierename(user.getEname());
                sgBSend.setRemark(request.getRemark());
                sendMapper.updateById(sgBSend);

                sgBSendItems = sendItemMapper.selectList(new QueryWrapper<SgBSendItem>().lambda()
                        .eq(SgBSendItem::getSgBSendId, sgBSend.getId()));

                sgBSendItems.forEach(bSendItem -> ids.add(bSendItem.getId()));

                //明细记录删除
                sendItemMapper.delete(new QueryWrapper<SgBSendItem>().lambda()
                        .eq(SgBSendItem::getSgBSendId, sgBSend.getId()));

                if (storageBoxConfig.getBoxEnable()) {
                    impItems = impItemMapper.selectList(new QueryWrapper<SgBSendImpItem>().lambda()
                            .eq(SgBSendImpItem::getSgBSendId, sgBSend.getId()));

                    impItemMapper.delete(new QueryWrapper<SgBSendImpItem>().lambda()
                            .eq(SgBSendImpItem::getSgBSendId, sgBSend.getId()));

                    List<SgBSendTeusItem> teusItems = teusItemMapper.selectList(new QueryWrapper<SgBSendTeusItem>().lambda()
                            .eq(SgBSendTeusItem::getSgBSendId, sgBSend.getId()));

                    teusItemMapper.delete(new QueryWrapper<SgBSendTeusItem>().lambda()
                            .eq(SgBSendTeusItem::getSgBSendId, sgBSend.getId()));

                    skuTeusCounts = teusItems.stream().collect(Collectors.groupingBy(SgBSendTeusItem::getPsCSkuId,
                            Collectors.reducing(BigDecimal.ZERO, SgBSendTeusItem::getQty, (a, b) -> a.add(b))));
                }

            } catch (Exception e) {
                e.printStackTrace();
                AssertUtils.logAndThrowExtra("清空逻辑发货单失败", e, locale);
            }

            Map<Long, CpCStore> negativeStock = SgStoreUtils.getNegativeStock(null, null, null, sgBSendItems);
            ValueHolderV14<SgStorageUpdateResult> callAPIResult = storageService.updateSendStoragePreout(
                    sgBSend, sgBSendItems, null, null, "clean", user,
                    false, negativeStock, false, false, impItems, skuTeusCounts, null, null, null, Maps.newHashMap());
            if (callAPIResult.isOK()) {
                //check同步库存占用结果
                SgStoreUtils.isSuccess("清空逻辑发货单失败!", callAPIResult);
                v14.setCode(ResultCode.SUCCESS);
                v14.setMessage("清空逻辑发货单成功");
            } else {
                AssertUtils.logAndThrow("清空逻辑发货单失败!" + callAPIResult.getMessage(), locale);
            }

            //推送ES
            storeESUtils.pushESBySend(sgBSend.getId(), false, true, ids, sendMapper, sendItemMapper);
        } catch (Exception e) {
            e.printStackTrace();
            AssertUtils.logAndThrowExtra(e, locale);
        } finally {
            try {
                redisTemplate.delete(lockKsy);
            } catch (Exception e) {
                e.printStackTrace();
                log.error(this.getClass().getName() + lockKsy + "释放锁失败，请联系管理员！");
                AssertUtils.logAndThrow("释放锁失败，请联系管理员！");
            }
        }

        if (log.isDebugEnabled()) {
            log.debug("来源单据id[" + request.getSourceBillId()
                    + "],逻辑发货单清空服务出参" + v14.toJSONObject().toJSONString() + ";");
        }
        return v14;
    }
}
