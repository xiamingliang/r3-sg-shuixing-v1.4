package com.jackrain.nea.sg.assign.services;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.cpext.api.CpCustomerQueryCmd;
import com.jackrain.nea.cpext.model.result.CpCusQueryByStoreSingleResult;
import com.jackrain.nea.cpext.model.table.CpCustomer;
import com.jackrain.nea.data.basic.model.request.ProInfoQueryRequest;
import com.jackrain.nea.data.basic.services.BasicPsQueryService;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.oc.basic.common.OcBasicConstants;
import com.jackrain.nea.oc.sale.api.OcSaleSaveAndAuditCmd;
import com.jackrain.nea.oc.sale.model.request.OcSaleBillSaveRequest;
import com.jackrain.nea.oc.sale.model.request.OcSaleImpItemSaveRequest;
import com.jackrain.nea.oc.sale.model.request.OcSaleSaveRequest;
import com.jackrain.nea.ps.api.table.PsCPro;
import com.jackrain.nea.sg.assign.model.request.SgPrepareStorageData;
import com.jackrain.nea.sg.assign.model.request.SgPrepareStoragePreRequest;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.transfer.common.SgTransferConstantsIF;
import com.jackrain.nea.sg.transfer.model.request.SgTransferBillSaveRequest;
import com.jackrain.nea.sg.transfer.model.request.SgTransferImpItemSaveRequest;
import com.jackrain.nea.sg.transfer.model.request.SgTransferSaveRequest;
import com.jackrain.nea.sg.transfer.services.SgTransferSaveAndAuditService;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.*;

/**
 * @author :csy
 * @version :1.0
 * description ：配货单 配货服务
 * @date :2019/11/7 19:43
 */
@Component
@Slf4j
public class SgPrepareStoragePreService {


    /**
     * 配货 入口
     *
     * @return result
     */
    @Transactional(rollbackFor = Exception.class)
    public ValueHolderV14 prepareStorage(@NotNull SgPrepareStoragePreRequest request) {
        ValueHolderV14 holderV14 = new ValueHolderV14();
        User user = request.getUser();
        Locale locale = user.getLocale();
        List<JSONObject> viewData = request.getData();
        AssertUtils.cannot(CollectionUtils.isEmpty(viewData), "请求参数为空!", locale);
        //商品 id集合
        List<Long> proIds = new ArrayList<>();
        List<Long> storeIds = new ArrayList<>();


        List<SgPrepareStorageData> requestData = fixViewToRequest(viewData, proIds, storeIds, locale);
        AssertUtils.cannot(CollectionUtils.isEmpty(requestData), "请求参数为空!", locale);
        HashMap<Long, CpCusQueryByStoreSingleResult> resultHashMap = queryCustomer(storeIds);
        /*
	若选择明细中存在箱号所属的商品编码对应的商品档案上的全国价为空时，则提示：“商品的全国价为空，不允许配货！”；
	若选择明细中存在发货店仓与收货店仓为同一店仓，则提示：“发货店仓与收货店仓相同，不允许配货”；
	若选择明细中存在发货店仓所属的经销商不等于收货店仓所属经销商或者收货店仓所属经销商的上级经销商，则提示：“发货店仓所属的经销商不等于收货店仓所属经销商或者收货店仓所属经销商的上级经销商”；
        * */
        BasicPsQueryService service = ApplicationContextHandle.getBean(BasicPsQueryService.class);
        ProInfoQueryRequest proRequest = new ProInfoQueryRequest();
        proRequest.setProIdList(proIds);
        try {
            List<PsCPro> proInfo = service.getProInfo(proRequest);
            proInfo.forEach(pro -> AssertUtils.notNull(pro.getPriceUnity(), "商品的全国价为空，不允许配货！", locale));
        } catch (Exception e) {
            throw new NDSException("商品信息获取失败!");
        }
        //分批.批量插入.调拨.销售
        splitBill(user, requestData, resultHashMap);
        holderV14.setCode(ResultCode.SUCCESS);
        holderV14.setMessage("配货成功!");
        return holderV14;
    }


    /**
     * 批量查询店仓-经销商
     *
     * @param storeIds 店仓id
     * @return 店仓.经销商
     */
    private HashMap<Long, CpCusQueryByStoreSingleResult> queryCustomer(List<Long> storeIds) {
        CpCustomerQueryCmd queryCmd = (CpCustomerQueryCmd) ReferenceUtil.refer(ApplicationContextHandle
                .getApplicationContext(), CpCustomerQueryCmd.class.getName(), "cp-ext", "1.0");
        return queryCmd.queryCusByStoreIds(storeIds);
    }

    /**
     * 根据发货订单、发货店仓和收货店仓为唯一原则拆分生成多张调拨单或者销售单
     *
     * @param user          用户
     * @param storageData   数据
     * @param resultHashMap 店铺经销商
     */
    private void splitBill(User user, List<SgPrepareStorageData> storageData, HashMap<Long, CpCusQueryByStoreSingleResult> resultHashMap) {
        Locale locale = user.getLocale();
        HashMap<String, OcSaleBillSaveRequest> saleBillMap = new HashMap<>(16);
        HashMap<String, SgTransferBillSaveRequest> transferBillMap = new HashMap<>(16);
        storageData.forEach(data -> {
            //发货订单
            Long ocBSendOutId = data.getOcBSendOutId();
            //发货店仓
            Long storeId = data.getCpCStoreId();
            //收货店仓
            Long destId = data.getCpCDestId();
            CpCusQueryByStoreSingleResult origResult = resultHashMap.get(storeId);
            CpCusQueryByStoreSingleResult destResult = resultHashMap.get(destId);
            AssertUtils.notNull(origResult, String.format("id 为 %s 店仓信息为空", storeId), locale);
            AssertUtils.notNull(destResult, String.format("id 为 %s 店仓信息为空", destId), locale);
            CpCustomer origCustomer = origResult.getCpCustomer();
            CpCustomer destCustomer = destResult.getCpCustomer();
            boolean isDestUpCusEqOrigCus = origCustomer.getEcode().equals(destCustomer.getCpCCustomerupEcode());
            boolean isSameCustomer = origCustomer.getEcode().equals(destCustomer.getEcode());
            boolean isCustomerOk = isSameCustomer || isDestUpCusEqOrigCus;
            if (!isCustomerOk) {
                AssertUtils.logAndThrow("发货店仓所属的经销商不等于收货店仓所属经销商或者收货店仓所属经销商的上级经销商", locale);
            }
            String uniKey = getUniKey(ocBSendOutId, storeId, destId);
            //同经销商 做调拨处理
            if (isSameCustomer) {
                SgTransferBillSaveRequest request = transferBillMap.get(uniKey);
                if (request != null) {
                    request.getImpItems().add(buildTransferImpItem(data));
                } else {
                    transferBillMap.put(uniKey, buildTransferRequest(user, data));
                }
            } else {
                OcSaleBillSaveRequest saleRequest = saleBillMap.get(uniKey);
                if (saleRequest != null) {
                    saleRequest.getImpItemSaveRequests().add(buildSaleImpItem(data));
                } else {
                    saleBillMap.put(uniKey, buildSaleRequest(user, data));
                }
            }
        });
        //批量新增并审核 调拨单
        if (CollectionUtils.isNotEmpty(transferBillMap.values())) {
            SgTransferSaveAndAuditService service = ApplicationContextHandle.getBean(SgTransferSaveAndAuditService.class);
            List<SgTransferBillSaveRequest> requestList = new ArrayList<>();
            requestList.addAll(transferBillMap.values());
            ValueHolderV14 auditBatch = service.saveAndAuditBatch(requestList);
            AssertUtils.cannot(auditBatch.getCode() == ResultCode.FAIL, String.format("批量新增 调拨单并审核失败: %s", auditBatch.getMessage()), locale);
        }
        //批量新增并审核 销售单
        if (CollectionUtils.isNotEmpty(saleBillMap.values())) {
            OcSaleSaveAndAuditCmd ocCmd = (OcSaleSaveAndAuditCmd)
                    ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                            OcSaleSaveAndAuditCmd.class.getName(), "oc", "1.0");
            List<OcSaleBillSaveRequest> requestList = new ArrayList<>();
            requestList.addAll(saleBillMap.values());
            ValueHolderV14 v14 = ocCmd.batchSaveAndAuditSale(requestList);
            AssertUtils.cannot(v14.getCode() == ResultCode.FAIL, String.format("批量新增.销售单并审核失败:.%s", v14.getMessage()), locale);
        }
    }


    /**
     * 构建调拨单.保存request
     *
     * @param user 用户
     * @param data data
     * @return request
     */
    private SgTransferBillSaveRequest buildTransferRequest(User user, SgPrepareStorageData data) {
        SgTransferBillSaveRequest request = new SgTransferBillSaveRequest();
        request.setObjId(-1L);
        request.setLoginUser(user);
        request.setTransfer(buildTransferMainInfo(data));
        List<SgTransferImpItemSaveRequest> impItemSaveRequestList = new ArrayList<>();
        impItemSaveRequestList.add(buildTransferImpItem(data));
        request.setImpItems(impItemSaveRequestList);
        return request;
    }

    /**
     * 构建调拨单 保存主表信息
     *
     * @param data data
     * @return transfer
     */
    private SgTransferSaveRequest buildTransferMainInfo(SgPrepareStorageData data) {
        SgTransferSaveRequest request = new SgTransferSaveRequest();
        BeanUtils.copyProperties(data, request);
        request.setId(-1L);
        request.setCpCOrigId(data.getCpCStoreId());
        request.setCpCOrigEcode(data.getCpCStoreEcode());
        request.setCpCOrigEname(data.getCpCStoreEname());
        //订单类型
        request.setTransferType(SgTransferConstantsIF.TRANSFER_TYPE_ORDER);
        //发货订单id
        request.setOcBSendOutId(data.getOcBSendOutId());
        //默认自提
        request.setSendType(SgTransferConstantsIF.SEND_TYPE_ZT);
        return request;
    }


    /**
     * 构建调拨单 保存录入信息
     *
     * @param data data
     * @return transfer
     */
    private SgTransferImpItemSaveRequest buildTransferImpItem(SgPrepareStorageData data) {
        SgTransferImpItemSaveRequest impItemSaveRequest = new SgTransferImpItemSaveRequest();
        BeanUtils.copyProperties(data, impItemSaveRequest);
        impItemSaveRequest.setId(-1L);
        impItemSaveRequest.setQty(data.getQtyAvailable());
        return impItemSaveRequest;
    }


    /**
     * 获取 hashMap 唯一键
     *
     * @param splitKey [a,b,c]
     * @return a_b_c
     */
    private String getUniKey(Object... splitKey) {
        StringBuilder uniKey = new StringBuilder();
        for (Object key : splitKey) {
            uniKey.append(key);
            uniKey.append("_");
        }
        return uniKey.toString();
    }


    /**
     * 构建 销售单 request
     *
     * @param user 用户
     * @param data data
     * @return request
     */
    private OcSaleBillSaveRequest buildSaleRequest(User user, SgPrepareStorageData data) {
        OcSaleBillSaveRequest request = new OcSaleBillSaveRequest();
        request.setLoginUser(user);
        request.setObjId(-1L);
        request.setSaleSaveRequest(buildSaleMainInfo(data));
        List<OcSaleImpItemSaveRequest> impItemSaveRequestList = new ArrayList<>();
        impItemSaveRequestList.add(buildSaleImpItem(data));
        request.setImpItemSaveRequests(impItemSaveRequestList);
        return request;
    }

    /**
     * 构建 销售单主表信息 request
     *
     * @param data data
     * @return request
     */
    private OcSaleSaveRequest buildSaleMainInfo(SgPrepareStorageData data) {
        OcSaleSaveRequest saleSaveRequest = new OcSaleSaveRequest();
        BeanUtils.copyProperties(data, saleSaveRequest);
        saleSaveRequest.setBillDate(new Date());
        saleSaveRequest.setId(-1L);
        saleSaveRequest.setIsAutoIn("N");
        saleSaveRequest.setIsAutoOut("N");
        saleSaveRequest.setCpCOrigId(data.getCpCStoreId());
        saleSaveRequest.setCpCOrigEcode(data.getCpCStoreEcode());
        saleSaveRequest.setCpCOrigEname(data.getCpCStoreEname());
        saleSaveRequest.setSaleType("ORD");
        return saleSaveRequest;
    }


    /**
     * 构建 销售单明细信息 request
     *
     * @param data data
     * @return request
     */
    private OcSaleImpItemSaveRequest buildSaleImpItem(SgPrepareStorageData data) {
        OcSaleImpItemSaveRequest saleImpJo = new OcSaleImpItemSaveRequest();
        BeanUtils.copyProperties(data, saleImpJo);
        saleImpJo.setId(-1L);
        saleImpJo.setQty(data.getQtyAvailable());
        saleImpJo.setIsTeus(OcBasicConstants.IS_MATCH_SIZE_Y);
        return saleImpJo;
    }

    /**
     * 前段数据格式 转换为正常数据
     *
     * @param viewData 前端展示数据{k:{"val":v}}
     * @return {k:V}
     */
    private List<SgPrepareStorageData> fixViewToRequest(List<JSONObject> viewData, List<Long> proIds, List<Long> storeIds, Locale locale) {
        List<SgPrepareStorageData> list = new ArrayList<>();
        viewData.forEach(view -> {
            JSONObject jo = new JSONObject();
            for (Map.Entry<String, Object> entry : view.entrySet()) {
                String key = entry.getKey();
                Object value = entry.getValue();
                if (value != null) {
                    jo.put(key, ((JSONObject) value).get("orig_val"));
                }
            }
            SgPrepareStorageData data = JSONObject.parseObject(jo.toJSONString(), SgPrepareStorageData.class);
            AssertUtils.notNull(data.getPsCProId(), "数据异常-商品id不存在", locale);
            Long storeId = data.getCpCStoreId();
            Long destId = data.getCpCDestId();
            if (storeId == null || destId == null) {
                throw new NDSException(Resources.getMessage("收货店仓 或者发货店仓存在空值!", locale));
            }
            AssertUtils.cannot(storeId.equals(destId), "发货店仓与收货店仓相同，不允许配货", locale);
            proIds.add(data.getPsCProId());
            storeIds.add(storeId);
            storeIds.add(destId);
            list.add(data);
        });
        return list;

    }
}
