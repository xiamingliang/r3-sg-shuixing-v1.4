package com.jackrain.nea.sg.adjust.mapper;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.adjust.model.table.SgBAdjust;
import com.jackrain.nea.sg.basic.common.SgConstants;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;

import java.util.List;

@Mapper
public interface SgBAdjustMapper extends ExtentionMapper<SgBAdjust> {

    /**
     * 分页查询逻辑发货单
     */
    @Select("SELECT * FROM " + SgConstants.SG_B_ADJUST + " ORDER BY ID LIMIT #{page} OFFSET #{offset}")
    List<SgBAdjust> selectListByPage(@Param("page") int page, @Param("offset") int offset);

    @SelectProvider(type = AdjustSql.class, method = "queryAdjust")
    List<SgBAdjust> selectAdjustByLog(JSONObject query);


    @SelectProvider(type = AdjustSql.class, method = "queryCountAdjust")
    Integer selectCountAdjustByLog(JSONObject queryCount);

    class AdjustSql {

        public String queryAdjust(JSONObject query) {

            String startDate = query.getString("startDate");
            String endDate = query.getString("endDate");
            String billNo = query.getString("billNo");
            long pageSize = query.getLong("pageSize");
            Long offSet = query.getLong("offSet");

            StringBuilder sql = new StringBuilder("SELECT * FROM sg_b_adjust WHERE isactive = 'Y' AND (reserve_bigint01 = 1 OR reserve_bigint01 = 3)");
            if (startDate != null) sql.append("\tAND bill_date>='" + startDate + "'");
            if (endDate != null) sql.append("\tAND bill_date<='" + endDate + "'");
            if (billNo != null) sql.append("\tAND bill_no IN(" + billNo + ")");
            sql.append("\tORDER BY id LIMIT " + pageSize + " OFFSET " + offSet);
            return sql.toString();
        }

        public String queryCountAdjust(JSONObject queryCount) {

            String startDate = queryCount.getString("startDate");
            String endDate = queryCount.getString("endDate");
            String billNo = queryCount.getString("billNo");

            StringBuilder sql = new StringBuilder("SELECT COUNT(1) FROM sg_b_adjust WHERE isactive = 'Y' AND (reserve_bigint01 = 1 OR reserve_bigint01 = 3)");
            if (startDate != null) sql.append("\tAND bill_date>='" + startDate + "'");
            if (endDate != null) sql.append("\tAND bill_date<='" + endDate + "'");
            if (billNo != null) sql.append("\tAND bill_no IN(" + billNo + ")");
            return sql.toString();
        }
    }
}