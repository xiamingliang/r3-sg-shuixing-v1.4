package com.jackrain.nea.sg.transfer.services;

import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.api.SgStorageBatchUpdateCmd;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.model.request.SgStorageBatchUpdateRequest;
import com.jackrain.nea.sg.basic.model.request.SgStorageUpdateBillItemRequest;
import com.jackrain.nea.sg.basic.model.request.SgStorageUpdateBillRequest;
import com.jackrain.nea.sg.basic.model.result.SgStorageUpdateResult;
import com.jackrain.nea.sg.transfer.model.table.ScBTransferItem;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.tableService.MainTableRecord;
import com.jackrain.nea.tableService.TableServiceContext;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.utils.AssertUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;


/**
 * 库存调用服务
 *
 * @author csy
 * @since 19-03-29
 */
@Component
@Deprecated
public class ScTransferStorageService {


    /**
     * 提交库存调用
     *
     * @param items   明细信息
     * @param context 上下文
     * @param row     主表信息
     */
    public void transferSubmitStorageChange(List<ScBTransferItem> items, MainTableRecord row, TableServiceContext context) {
        this.transferStorageChange(items, row, context, true);
    }

    /**
     * 取消提交库存调用
     *
     * @param items   明细信息
     * @param context 上下文
     * @param row     主表信息
     */
    public void transferUnSubmitStorageChange(List<ScBTransferItem> items, MainTableRecord row, TableServiceContext context) {
        this.transferStorageChange(items, row, context, false);
    }


    /**
     * @param items    明细信息
     * @param context  上下文
     * @param row      主表信息
     * @param isSubmit 是否是提交
     */
    private void transferStorageChange(List<ScBTransferItem> items, MainTableRecord row, TableServiceContext context, boolean isSubmit) {
        //todo:调用库存 mq调用
        SgStorageBatchUpdateCmd sgStorageBatchUpdateCmd = (SgStorageBatchUpdateCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                SgStorageBatchUpdateCmd.class.getName(), "sg", "1.0");
        SgStorageBatchUpdateRequest request = new SgStorageBatchUpdateRequest();
        List<SgStorageUpdateBillRequest> list = new ArrayList<>();
        SgStorageUpdateBillRequest stockRequest = new SgStorageUpdateBillRequest();
        stockRequest.setBillDate(row.getMainData().getOrignalData().getDate("BILL_DATE"));
        stockRequest.setBillId(row.getId());
        stockRequest.setBillNo(row.getMainData().getOrignalData().getString("BILL_NO"));
        //todo:check
        if (isSubmit) {
            stockRequest.setBillType(SgConstantsIF.BILL_TYPE_TRANSFER);
        } else {
            stockRequest.setBillType(SgConstantsIF.BILL_TYPE_TRANSFER);
        }
        stockRequest.setChangeDate(new Date());
        List<SgStorageUpdateBillItemRequest> itemRequests = new ArrayList<>();
        for (ScBTransferItem transferItem : items) {
            SgStorageUpdateBillItemRequest itemRequest = new SgStorageUpdateBillItemRequest();
            itemRequest.setBillItemId(transferItem.getId());
            itemRequest.setCpCStoreId(row.getMainData().getOrignalData().getLong("CP_C_ORIG_ID"));
            itemRequest.setPsCSkuId(transferItem.getPsCSkuId());
            if (isSubmit) {
                itemRequest.setQtyPreoutChange(transferItem.getQty());
            } else {
                itemRequest.setQtyPreoutChange(transferItem.getQty().negate());
            }
            itemRequest.setStorageType(SgConstantsIF.STORAGE_TYPE_PREOUT);
            itemRequests.add(itemRequest);

        }
        stockRequest.setItemList(itemRequests);
        list.add(stockRequest);
        request.setBillList(list);
        request.setLoginUser(context.getUser());
        request.setMessageKey(UUID.randomUUID().toString());
        ValueHolderV14<SgStorageUpdateResult> holderV14 = sgStorageBatchUpdateCmd.updateStorageBatch(request);
        if (holderV14 == null) {
            AssertUtils.logAndThrow("调拨单库存调用失败!", context.getLocale());
        }
        AssertUtils.cannot(holderV14.getCode() == ResultCode.FAIL, "调拨单库存调用失败：" + holderV14.getMessage(), context.getLocale());
    }


}
