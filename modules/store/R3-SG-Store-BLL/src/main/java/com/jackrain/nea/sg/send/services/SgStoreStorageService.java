package com.jackrain.nea.sg.send.services;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.sg.basic.api.SgStorageBatchUpdateCmd;
import com.jackrain.nea.sg.basic.api.SgStorageBillUpdateCmd;
import com.jackrain.nea.sg.basic.api.SgStorageNoTransUpdateCmd;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.model.request.*;
import com.jackrain.nea.sg.basic.model.result.SgStorageUpdateResult;
import com.jackrain.nea.sg.basic.model.table.CpCStore;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.receive.model.request.SgReceiveBillSaveRequest;
import com.jackrain.nea.sg.receive.model.table.SgBReceive;
import com.jackrain.nea.sg.receive.model.table.SgBReceiveItem;
import com.jackrain.nea.sg.send.model.request.SgSendBillSaveRequest;
import com.jackrain.nea.sg.send.model.table.SgBSend;
import com.jackrain.nea.sg.send.model.table.SgBSendItem;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.MapUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 逻辑仓库存服务
 *
 * @author: 舒威
 * @since: 2019/4/30
 * create at : 2019/4/30 10:00
 */
@Slf4j
@Component
public class SgStoreStorageService {

    public final static String CLEAN = "clean";

    public final static String VOID = "void";

    public final static String SUBMIT = "submit";

    /**
     * 初始化同步库存占用服务-整单
     */
    public SgStorageBillUpdateCmd initSgStorageBillUpdateCmd() {
        return (SgStorageBillUpdateCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(),
                SgStorageBillUpdateCmd.class.getName(),
                SgConstantsIF.GROUP, SgConstantsIF.VERSION);
    }

    /**
     * 初始化同步库存占用服务-单条明细
     */
    public SgStorageBatchUpdateCmd initSgStorageBillBatchUpdateCmd() {
        return (SgStorageBatchUpdateCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(),
                SgStorageBatchUpdateCmd.class.getName(),
                SgConstantsIF.GROUP, SgConstantsIF.VERSION);
    }

    /**
     * 初始化同步库存占用服务-整单（不带事务）
     */
    public SgStorageNoTransUpdateCmd initSgStorageNoTransUpdateCmd() {
        return (SgStorageNoTransUpdateCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(),
                SgStorageNoTransUpdateCmd.class.getName(),
                SgConstantsIF.GROUP, SgConstantsIF.VERSION);
    }

    /**
     * 库存占用更新-整单
     *
     * @param send             逻辑发货单id
     * @param bSendItems       逻辑发货单有占用变化明细
     * @param request          request-model
     * @param map              原逻发货单明细 k-v   <skuid+逻辑仓id , 明细>
     * @param action           动作 - 提交、作废、清空用
     * @param loginUser        登录用户
     * @param isLast
     * @param negativeStock
     * @param isNegativePreout
     * @param noticesId
     * @param noticesBillno    @return 库存占用更新结果
     */
    public ValueHolderV14<SgStorageUpdateResult> updateSendStoragePreout(SgBSend send,
                                                                         List<SgBSendItem> bSendItems,
                                                                         SgSendBillSaveRequest request,
                                                                         HashMap<String, SgBSendItem> map,
                                                                         String action,
                                                                         User loginUser,
                                                                         Boolean isLast,
                                                                         Map<Long, CpCStore> negativeStock, Boolean isNegativePreout, Long noticesId, String noticesBillno) {
        ValueHolderV14<SgStorageUpdateResult> storageResult = null;
        try {
            SgStorageUpdateBillRequest billRequest = new SgStorageUpdateBillRequest();
            //单据信息
            billRequest.setBillId(send.getId());
            billRequest.setBillNo(send.getBillNo());
            billRequest.setBillDate(send.getBillDate());
            billRequest.setBillType(send.getSourceBillType());
            billRequest.setChangeDate(new Timestamp(System.currentTimeMillis()));
            billRequest.setCpCshopId(send.getCpCShopId());
            billRequest.setServiceNode(send.getServiceNode());
            billRequest.setIsCancel(request != null ? request.getIsCancel() : false);
            billRequest.setSourceBillId(send.getSourceBillId());
            billRequest.setSourceBillNo(send.getSourceBillNo());
            billRequest.setPhyOutNoticesId(noticesId);
            billRequest.setPhyOutNoticesNo(noticesBillno);

            //单据明细信息
            List<SgStorageUpdateBillItemRequest> itemList = request != null ?
                    buildSendStorageItems(request, map, negativeStock) : buildSendStorageItems(bSendItems, action, isLast, negativeStock, isNegativePreout, false);
            billRequest.setItemList(itemList);

            SgStorageUpdateControlRequest controlModel = new SgStorageUpdateControlRequest();
            controlModel.setPreoutOperateType(request != null ? request.getPreoutWarningType() : SgConstantsIF.PREOUT_RESULT_ERROR);

            SgStorageSingleUpdateRequest updateRequest = new SgStorageSingleUpdateRequest();
            updateRequest.setBill(billRequest);
            String msgKey = SgConstants.MSG_TAG_SEND + ":" + send.getBillNo();
            if (!VOID.equals(action)) {
                msgKey += System.currentTimeMillis();
            }
            updateRequest.setMessageKey(msgKey);
            updateRequest.setLoginUser(loginUser);
            updateRequest.setControlModel(controlModel);

            SgStorageBillUpdateCmd sgStorageUpdateCmd = initSgStorageBillUpdateCmd();
            if (log.isDebugEnabled()) {
                log.debug("来源单据id[" + send.getSourceBillId() + "],开始占用库存占用入参" + JSONObject.toJSONString(updateRequest));
            }
            storageResult = sgStorageUpdateCmd.updateStorageBillWithTrans(updateRequest);
            if (log.isDebugEnabled()) {
                log.debug("来源单据id[" + send.getSourceBillId() + "],占用库存占用出参" + storageResult.toJSONObject().toJSONString());
            }
        } catch (Exception e) {
            e.printStackTrace();
            AssertUtils.logAndThrowExtra("同步库存占用接口服务异常", e);
        }
        return storageResult;
    }

    /**
     * 库存占用更新-单条明细
     *
     * @param send          逻辑发货单id
     * @param bSendItems    逻辑发货单有占用变化明细
     * @param request       request-model
     * @param map           原逻发货单明细 k-v   <skuid+逻辑仓id , 明细>
     * @param action        动作 - 提交、作废、清空用
     * @param loginUser     登录用户
     * @param isLast
     * @param negativeStock
     * @return 库存占用更新结果
     */
    public ValueHolderV14<SgStorageUpdateResult> batchUpdateSendStoragePreout(SgBSend send,
                                                                              List<SgBSendItem> bSendItems,
                                                                              SgSendBillSaveRequest request,
                                                                              HashMap<String, SgBSendItem> map,
                                                                              String action,
                                                                              User loginUser,
                                                                              Boolean isLast,
                                                                              Map<Long, CpCStore> negativeStock) {
        ValueHolderV14<SgStorageUpdateResult> storageResult = null;
        try {
            long stStartTime = System.currentTimeMillis();
            SgStorageUpdateBillRequest billRequest = new SgStorageUpdateBillRequest();
            //单据信息
            billRequest.setBillId(send.getId());
            billRequest.setBillNo(send.getBillNo());
            billRequest.setBillDate(send.getBillDate());
            billRequest.setBillType(send.getSourceBillType());
            billRequest.setChangeDate(new Timestamp(System.currentTimeMillis()));
            billRequest.setCpCshopId(send.getCpCShopId());
            billRequest.setServiceNode(send.getServiceNode());
            billRequest.setIsCancel(request != null ? request.getIsCancel() : false);
            billRequest.setSourceBillId(send.getSourceBillId());
            billRequest.setSourceBillNo(send.getSourceBillNo());

            //单据明细信息
            List<SgStorageUpdateBillItemRequest> itemList = request != null ?
                    buildSendStorageItems(request, map, negativeStock) : buildSendStorageItems(bSendItems, action, isLast, negativeStock, false, false);
            billRequest.setItemList(itemList);

            SgStorageUpdateControlRequest controlModel = new SgStorageUpdateControlRequest();
            controlModel.setPreoutOperateType(request != null ? request.getPreoutWarningType() : SgConstantsIF.PREOUT_RESULT_ERROR);

            SgStorageBatchUpdateRequest updateRequest = new SgStorageBatchUpdateRequest();
            List<SgStorageUpdateBillRequest> billList = new ArrayList<>();
            billList.add(billRequest);
            updateRequest.setBillList(billList);
            String msgKey = SgConstants.MSG_TAG_SEND + ":" + send.getBillNo();
            if (!VOID.equals(action)) {
                msgKey += System.currentTimeMillis();
            }
            updateRequest.setMessageKey(msgKey);
            updateRequest.setLoginUser(loginUser);
            updateRequest.setControlModel(controlModel);

            SgStorageBatchUpdateCmd sgStorageBatchUpdateCmd = initSgStorageBillBatchUpdateCmd();
            log.debug("来源单据id[" + send.getSourceBillId() + "],开始占用库存占用入参" + JSONObject.toJSONString(updateRequest));
            storageResult = sgStorageBatchUpdateCmd.updateStorageBatch(updateRequest);
            log.debug("来源单据id[" + send.getSourceBillId() + "],占用库存占用出参" + storageResult.toJSONObject().toJSONString());
            if (log.isDebugEnabled()) {
                log.debug("来源单据id[" + send.getSourceBillId() + "],完成占单耗时:{}ms;"
                        , System.currentTimeMillis() - stStartTime);
            }
        } catch (Exception e) {
            e.printStackTrace();
            AssertUtils.logAndThrowExtra("同步库存占用接口服务异常", e);
        }
        return storageResult;
    }

    /**
     * 库存占用更新-整单(不带事务)
     *
     * @param send          逻辑发货单id
     * @param bSendItems    逻辑发货单有占用变化明细
     * @param request       request-model
     * @param map           原逻发货单明细 k-v   <skuid+逻辑仓id , 明细>
     * @param action        动作 - 提交、作废、清空用
     * @param loginUser     登录用户
     * @param isLast
     * @param negativeStock
     * @param isDeficiency
     * @return 库存占用更新结果
     */
    public ValueHolderV14<SgStorageUpdateResult> updateNoTransSendStoragePreout(SgBSend send,
                                                                                List<SgBSendItem> bSendItems,
                                                                                SgSendBillSaveRequest request,
                                                                                HashMap<String, SgBSendItem> map,
                                                                                String action,
                                                                                User loginUser,
                                                                                Boolean isLast,
                                                                                Map<Long, CpCStore> negativeStock,
                                                                                Boolean isDeficiency) {
        ValueHolderV14<SgStorageUpdateResult> storageResult = null;
        try {
            SgStorageUpdateBillRequest billRequest = new SgStorageUpdateBillRequest();
            //单据信息
            billRequest.setBillId(send.getId());
            billRequest.setBillNo(send.getBillNo());
            billRequest.setBillDate(send.getBillDate());
            billRequest.setBillType(send.getSourceBillType());
            billRequest.setChangeDate(new Timestamp(System.currentTimeMillis()));
            billRequest.setCpCshopId(send.getCpCShopId());
            billRequest.setServiceNode(send.getServiceNode());
            billRequest.setIsCancel(request != null ? request.getIsCancel() : false);
            billRequest.setSourceBillId(send.getSourceBillId());
            billRequest.setSourceBillNo(send.getSourceBillNo());

            //单据明细信息
            List<SgStorageUpdateBillItemRequest> itemList = request != null ?
                    buildSendStorageItems(request, map, negativeStock) : buildSendStorageItems(bSendItems, action, isLast, negativeStock, false, isDeficiency);
            billRequest.setItemList(itemList);

            SgStorageUpdateControlRequest controlModel = new SgStorageUpdateControlRequest();
            controlModel.setPreoutOperateType(request != null ? request.getPreoutWarningType() : SgConstantsIF.PREOUT_RESULT_ERROR);

            SgStorageSingleUpdateRequest updateRequest = new SgStorageSingleUpdateRequest();
            updateRequest.setBill(billRequest);
            String msgKey = SgConstants.MSG_TAG_SEND + ":" + send.getBillNo();
            if (!VOID.equals(action)) {
                msgKey += System.currentTimeMillis();
            }
            updateRequest.setMessageKey(msgKey); //需统一规范
            updateRequest.setLoginUser(loginUser);
            updateRequest.setControlModel(controlModel);

            SgStorageNoTransUpdateCmd sgStorageNoTransUpdateCmd = initSgStorageNoTransUpdateCmd();
            if (log.isDebugEnabled()) {
                log.debug("来源单据id[" + send.getSourceBillId() + "],开始占用库存占用入参" + JSONObject.toJSONString(updateRequest));
            }
            storageResult = sgStorageNoTransUpdateCmd.updateStorageBillNoTrans(updateRequest);
            if (log.isDebugEnabled()) {
                log.debug("来源单据id[" + send.getSourceBillId() + "],占用库存占用出参" + storageResult.toJSONObject().toJSONString());
            }
        } catch (Exception e) {
            e.printStackTrace();
            AssertUtils.logAndThrowExtra("同步库存占用接口服务异常", e);
        }
        return storageResult;
    }

    private List<SgStorageUpdateBillItemRequest> buildSendStorageItems(SgSendBillSaveRequest request, HashMap<String, SgBSendItem> map,
                                                                       Map<Long, CpCStore> negativeStock) {
        List<SgStorageUpdateBillItemRequest> itemList = new ArrayList<>();
        if (SgConstantsIF.ITEM_UPDATE_TYPE_ALL == request.getUpdateMethod()) { //全量
            //逆向冲单明细数据
            request.getItemList().forEach(itemSaveRequest -> {
                if (itemSaveRequest.getIsSendStorage()) {
                    //原逻辑发货单明细
                    SgBSendItem sendItem = map.get(itemSaveRequest.getPsCSkuId() + "_" + itemSaveRequest.getCpCStoreId()
                            + "_" + itemSaveRequest.getSourceBillItemId());
                    if (sendItem != null && 0 != BigDecimal.ZERO.compareTo(sendItem.getQtyPreout().negate())) {
                        SgStorageUpdateBillItemRequest item = new SgStorageUpdateBillItemRequest();
                        BeanUtils.copyProperties(sendItem, item);
                        item.setBillItemId(sendItem.getId());
                        //19-06-05 该属性已启用
                        //item.setStorageType(SgConstantsIF.STORAGE_TYPE_PREOUT);
                        item.setQtyPreoutChange(sendItem.getQtyPreout().negate());
                        if (MapUtils.isNotEmpty(negativeStock)) {
                            SgStorageUpdateControlRequest controlRequest = new SgStorageUpdateControlRequest();
                            CpCStore cpCStore = negativeStock.get(sendItem.getCpCStoreId());
                            Long isnegative = cpCStore.getIsnegative();
                            //1 开启负库存控制（不允许负库存）0 允许负库存
                            isnegative = isnegative == null ? 1L : isnegative;
                            if (request.getIsNegativePreout()) isnegative = 0L;
                            controlRequest.setNegativePreout(isnegative == 0);
                            controlRequest.setNegativeAvailable(isnegative == 0);
                            item.setControlmodel(controlRequest);
                        }
                        itemList.add(item);
                    }
                }
            });
        }

        request.getItemList().forEach(itemSaveRequest -> {
            if (itemSaveRequest.getIsSendStorage() && 0 != BigDecimal.ZERO.compareTo(itemSaveRequest.getQtyPreout())) {
                SgStorageUpdateBillItemRequest item = new SgStorageUpdateBillItemRequest();
                BeanUtils.copyProperties(itemSaveRequest, item);
                item.setBillItemId(itemSaveRequest.getId());
                // item.setStorageType(SgConstantsIF.STORAGE_TYPE_PREOUT);
                item.setQtyPreoutChange(itemSaveRequest.getQtyPreout());
                if (MapUtils.isNotEmpty(negativeStock)) {
                    SgStorageUpdateControlRequest controlRequest = new SgStorageUpdateControlRequest();
                    CpCStore cpCStore = negativeStock.get(itemSaveRequest.getCpCStoreId());
                    Long isnegative = cpCStore.getIsnegative();
                    isnegative = isnegative == null ? 1L : isnegative;
                    if (request.getIsNegativePreout()) isnegative = 0L;
                    controlRequest.setNegativePreout(isnegative == 0);
                    controlRequest.setNegativeAvailable(isnegative == 0);
                    item.setControlmodel(controlRequest);
                }
                itemList.add(item);
            }
        });

        return itemList;
    }

    private List<SgStorageUpdateBillItemRequest> buildSendStorageItems(List<SgBSendItem> bSendItems, String action, Boolean isLast,
                                                                       Map<Long, CpCStore> negativeStock, boolean isNegativePreout, Boolean isDeficiency) {
        List<SgStorageUpdateBillItemRequest> itemList = new ArrayList<>();
        bSendItems.forEach(sgBSendItem -> {
            BigDecimal preoutQtyChange = sgBSendItem.getQtyPreout().negate();
            BigDecimal storageQtyChange = sgBSendItem.getQtySend().negate();
            if ((!SUBMIT.equals(action)) && 0 != BigDecimal.ZERO.compareTo(preoutQtyChange)) {
                SgStorageUpdateBillItemRequest item = new SgStorageUpdateBillItemRequest();
                BeanUtils.copyProperties(sgBSendItem, item);
                item.setBillItemId(sgBSendItem.getId());
                //item.setStorageType(SgConstantsIF.STORAGE_TYPE_PREOUT);
                item.setQtyPreoutChange(preoutQtyChange);
                if (MapUtils.isNotEmpty(negativeStock)) {
                    SgStorageUpdateControlRequest controlRequest = new SgStorageUpdateControlRequest();
                    CpCStore cpCStore = negativeStock.get(sgBSendItem.getCpCStoreId());
                    Long isnegative = cpCStore.getIsnegative();
                    isnegative = isnegative == null ? 1L : isnegative;
                    if (isNegativePreout) isnegative = 0L;
                    controlRequest.setNegativePreout(isnegative == 0);
                    controlRequest.setNegativeAvailable(isnegative == 0);
                    //实缺允许负占用
                    if (isDeficiency != null && isDeficiency) {
                        controlRequest.setNegativePreout(true);
                    }
                    item.setControlmodel(controlRequest);
                }
                itemList.add(item);
            }
            if ((SUBMIT.equals(action) || isLast) && (0 != BigDecimal.ZERO.compareTo(preoutQtyChange)
                    || 0 != BigDecimal.ZERO.compareTo(storageQtyChange))) {
                SgStorageUpdateBillItemRequest item = new SgStorageUpdateBillItemRequest();
                BeanUtils.copyProperties(sgBSendItem, item);
                item.setBillItemId(sgBSendItem.getId());
                //item.setStorageType(SgConstantsIF.STORAGE_TYPE_STORAGE);
                item.setQtyPreoutChange(preoutQtyChange);
                item.setQtyStorageChange(storageQtyChange);
                if (MapUtils.isNotEmpty(negativeStock)) {
                    SgStorageUpdateControlRequest controlRequest = new SgStorageUpdateControlRequest();
                    CpCStore cpCStore = negativeStock.get(sgBSendItem.getCpCStoreId());
                    Long isnegative = cpCStore.getIsnegative();
                    isnegative = isnegative == null ? 1L : isnegative;
                    //实缺允许负库存
                    if (isNegativePreout || (isDeficiency != null && isDeficiency)) isnegative = 0L;
                    controlRequest.setNegativePreout(isnegative == 0);
                    controlRequest.setNegativeStorage(isnegative == 0);
                    controlRequest.setNegativeAvailable(isnegative == 0);
                    item.setControlmodel(controlRequest);
                }
                itemList.add(item);
            }
        });

        return itemList;
    }

    /**
     * 库存在途更新-整单
     *
     * @param receive         逻辑收货单id
     * @param bReceiveItems   逻辑收货单有占用变化明细
     * @param request         request-model
     * @param map             原逻辑收货单明细 k-v   <skuid+逻辑仓id , 明细>
     * @param action          动作 - 提交、作废用
     * @param loginUser       登录用户
     * @param isLast          是否最后一次入库标志
     * @param negativeStock
     * @param isNegativePrein
     * @param noticesId
     * @param noticesBillno   @return 库存在途更新结果
     */
    public ValueHolderV14<SgStorageUpdateResult> updateReceiveStoragePrein(SgBReceive receive,
                                                                           List<SgBReceiveItem> bReceiveItems,
                                                                           SgReceiveBillSaveRequest request,
                                                                           HashMap<String, SgBReceiveItem> map,
                                                                           String action,
                                                                           User loginUser, Boolean isLast, Map<Long, CpCStore> negativeStock, Boolean isNegativePrein, Long noticesId, String noticesBillno) {
        ValueHolderV14<SgStorageUpdateResult> storageResult = null;
        try {
            SgStorageUpdateBillRequest billRequest = new SgStorageUpdateBillRequest();

            //单据信息
            billRequest.setBillId(receive.getId());
            billRequest.setBillNo(receive.getBillNo());
            billRequest.setBillDate(receive.getBillDate());
            billRequest.setBillType(receive.getSourceBillType());
            billRequest.setChangeDate(new Timestamp(System.currentTimeMillis()));
            billRequest.setCpCshopId(receive.getCpCShopId());
            billRequest.setServiceNode(receive.getServiceNode());
            billRequest.setIsCancel(request != null ? request.getIsCancel() : false);
            billRequest.setSourceBillId(receive.getSourceBillId());
            billRequest.setSourceBillNo(receive.getSourceBillNo());
            billRequest.setPhyInNoticesId(noticesId);
            billRequest.setPhyInNoticesNo(noticesBillno);

            //单据明细信息
            List<SgStorageUpdateBillItemRequest> itemList = request != null ?
                    buildReceiveStorageItems(request, map, negativeStock) : buildReceiveStorageItems(bReceiveItems, action, isLast, negativeStock, isNegativePrein);
            billRequest.setItemList(itemList);

            SgStorageUpdateControlRequest controlModel = new SgStorageUpdateControlRequest();
            controlModel.setPreoutOperateType(request != null ? request.getPreoutWarningType() : SgConstantsIF.PREOUT_RESULT_ERROR);

            SgStorageSingleUpdateRequest updateRequest = new SgStorageSingleUpdateRequest();
            updateRequest.setBill(billRequest);
            String msgKey = SgConstants.MSG_TAG_RECEIVE + ":" + receive.getBillNo();
            if (!VOID.equals(action)) {
                msgKey += System.currentTimeMillis();
            }
            updateRequest.setMessageKey(msgKey); //需统一规范
            updateRequest.setLoginUser(loginUser);
            updateRequest.setControlModel(controlModel);

            SgStorageBillUpdateCmd sgStorageUpdateCmd = initSgStorageBillUpdateCmd();
            if (log.isDebugEnabled()) {
                log.debug("来源单据id[" + receive.getSourceBillId() + "],开始占用库存在途入参:" + JSONObject.toJSONString(updateRequest));
            }
            storageResult = sgStorageUpdateCmd.updateStorageBillWithTrans(updateRequest);
            if (log.isDebugEnabled()) {
                log.debug("来源单据id[" + receive.getSourceBillId() + "],占用库存在途出参:" + storageResult.toJSONObject().toJSONString());
            }
        } catch (Exception e) {
            e.printStackTrace();
            AssertUtils.logAndThrowExtra("同步库存占用接口服务异常", e);
        }
        return storageResult;
    }

    /**
     * 库存在途更新-单条明细
     *
     * @param receive       逻辑收货单id
     * @param bReceiveItems 逻辑收货单有占用变化明细
     * @param request       request-model
     * @param map           原逻辑收货单明细 k-v   <skuid+逻辑仓id , 明细>
     * @param action        动作 - 提交、作废用
     * @param loginUser     登录用户
     * @param isLast        是否最后一次入库标志
     * @param negativeStock
     * @return 库存在途更新结果
     */
    public ValueHolderV14<SgStorageUpdateResult> batchUpdateReceiveStoragePrein(SgBReceive receive,
                                                                                List<SgBReceiveItem> bReceiveItems,
                                                                                SgReceiveBillSaveRequest request,
                                                                                HashMap<String, SgBReceiveItem> map,
                                                                                String action,
                                                                                User loginUser, Boolean isLast, Map<Long, CpCStore> negativeStock) {
        ValueHolderV14<SgStorageUpdateResult> storageResult = null;
        try {
            SgStorageUpdateBillRequest billRequest = new SgStorageUpdateBillRequest();

            //单据信息
            billRequest.setBillId(receive.getId());
            billRequest.setBillNo(receive.getBillNo());
            billRequest.setBillDate(receive.getBillDate());
            billRequest.setBillType(receive.getSourceBillType());
            billRequest.setChangeDate(new Timestamp(System.currentTimeMillis()));
            billRequest.setCpCshopId(receive.getCpCShopId());
            billRequest.setServiceNode(receive.getServiceNode());
            billRequest.setIsCancel(request != null ? request.getIsCancel() : false);
            billRequest.setSourceBillId(receive.getSourceBillId());
            billRequest.setSourceBillNo(receive.getSourceBillNo());

            //单据明细信息
            List<SgStorageUpdateBillItemRequest> itemList = request != null ?
                    buildReceiveStorageItems(request, map, negativeStock) : buildReceiveStorageItems(bReceiveItems, action, isLast, negativeStock, false);
            billRequest.setItemList(itemList);

            SgStorageUpdateControlRequest controlModel = new SgStorageUpdateControlRequest();
            controlModel.setPreoutOperateType(request != null ? request.getPreoutWarningType() : SgConstantsIF.PREOUT_RESULT_ERROR);

            //必须使用SgConstantsIF.PREOUT_RESULT_XXX
            SgStorageBatchUpdateRequest updateRequest = new SgStorageBatchUpdateRequest();
            List<SgStorageUpdateBillRequest> billList = new ArrayList<>();
            billList.add(billRequest);
            updateRequest.setBillList(billList);
            String msgKey = SgConstants.MSG_TAG_RECEIVE + ":" + receive.getBillNo();
            if (!VOID.equals(action)) {
                msgKey += System.currentTimeMillis();
            }
            updateRequest.setMessageKey(msgKey); //需统一规范
            updateRequest.setLoginUser(loginUser);
            updateRequest.setControlModel(controlModel);

            SgStorageBatchUpdateCmd sgStorageBatchUpdateCmd = initSgStorageBillBatchUpdateCmd();
            if (log.isDebugEnabled()) {
                log.debug("来源单据id[" + receive.getSourceBillId() + "],开始占用库存在途入参:" + JSONObject.toJSONString(updateRequest));
            }
            storageResult = sgStorageBatchUpdateCmd.updateStorageBatch(updateRequest);
            if (log.isDebugEnabled()) {
                log.debug("来源单据id[" + receive.getSourceBillId() + "],占用库存在途出参:" + storageResult.toJSONObject().toJSONString());
            }
        } catch (Exception e) {
            e.printStackTrace();
            AssertUtils.logAndThrowExtra("同步库存占用接口服务异常", e);
        }
        return storageResult;
    }

    private List<SgStorageUpdateBillItemRequest> buildReceiveStorageItems(SgReceiveBillSaveRequest request,
                                                                          HashMap<String, SgBReceiveItem> map, Map<Long, CpCStore> negativeStock) {
        List<SgStorageUpdateBillItemRequest> itemList = new ArrayList<>();
        if (SgConstantsIF.ITEM_UPDATE_TYPE_ALL == request.getUpdateMethod()) { //全量
            //逆向冲单明细数据
            request.getItemList().forEach(itemSaveRequest -> {
                if (itemSaveRequest.getIsReceiveStorage()) {
                    //原逻辑发货单明细
                    SgBReceiveItem receiveItem = map.get(itemSaveRequest.getPsCSkuId() + "_" + itemSaveRequest.getCpCStoreId()
                            + "_" + itemSaveRequest.getSourceBillItemId());
                    //原逻辑发货单历史占用为0 或 新明细占用为0  不需要发库存
                    if (receiveItem != null && 0 != BigDecimal.ZERO.compareTo(receiveItem.getQtyPrein())
                            && 0 != BigDecimal.ZERO.compareTo(itemSaveRequest.getQtyPrein())) {
                        SgStorageUpdateBillItemRequest item = new SgStorageUpdateBillItemRequest();
                        BeanUtils.copyProperties(receiveItem, item);
                        item.setBillItemId(receiveItem.getId());
                        //item.setStorageType(SgConstantsIF.STORAGE_TYPE_PREIN);
                        item.setQtyPreinChange(receiveItem.getQtyPrein().negate());
                        if (MapUtils.isNotEmpty(negativeStock)) {
                            SgStorageUpdateControlRequest controlRequest = new SgStorageUpdateControlRequest();
                            CpCStore cpCStore = negativeStock.get(receiveItem.getCpCStoreId());
                            Long isnegative = cpCStore.getIsnegative();
                            //1 开启负库存控制（不允许负库存）0 允许负库存
                            isnegative = isnegative == null ? 1L : isnegative;
                            if (request.getIsNegativePrein()) isnegative = 0L;
                            controlRequest.setNegativePrein(isnegative == 0);
                            item.setControlmodel(controlRequest);
                        }
                        itemList.add(item);
                    }
                }
            });
        }

        request.getItemList().forEach(itemSaveRequest -> {
            if (itemSaveRequest.getIsReceiveStorage() && 0 != BigDecimal.ZERO.compareTo(itemSaveRequest.getQtyPrein())) {
                SgStorageUpdateBillItemRequest item = new SgStorageUpdateBillItemRequest();
                BeanUtils.copyProperties(itemSaveRequest, item);
                item.setBillItemId(itemSaveRequest.getId());
                //item.setStorageType(SgConstantsIF.STORAGE_TYPE_PREIN);
                item.setQtyPreinChange(itemSaveRequest.getQtyPrein());
                if (MapUtils.isNotEmpty(negativeStock)) {
                    SgStorageUpdateControlRequest controlRequest = new SgStorageUpdateControlRequest();
                    CpCStore cpCStore = negativeStock.get(itemSaveRequest.getCpCStoreId());
                    Long isnegative = cpCStore.getIsnegative();
                    //1 开启负库存控制（不允许负库存）0 允许负库存
                    isnegative = isnegative == null ? 1L : isnegative;
                    if (request.getIsNegativePrein()) isnegative = 0L;
                    controlRequest.setNegativePrein(isnegative == 0);
                    item.setControlmodel(controlRequest);
                }
                itemList.add(item);
            }
        });

        return itemList;
    }


    private List<SgStorageUpdateBillItemRequest> buildReceiveStorageItems(List<SgBReceiveItem> bReceiveItems,
                                                                          String action, Boolean islast, Map<Long, CpCStore> negativeStock, Boolean isNegativePrein) {
        List<SgStorageUpdateBillItemRequest> itemList = new ArrayList<>();
        bReceiveItems.forEach(sgBReceiveItem -> {
            BigDecimal preinQtyChange = sgBReceiveItem.getQtyPrein().negate();
            BigDecimal storageQtyChange = sgBReceiveItem.getQtyReceive();
            if ((!SUBMIT.equals(action)) && 0 != BigDecimal.ZERO.compareTo(preinQtyChange)) {
                SgStorageUpdateBillItemRequest prein = new SgStorageUpdateBillItemRequest();
                BeanUtils.copyProperties(sgBReceiveItem, prein);
                prein.setBillItemId(sgBReceiveItem.getId());
                //prein.setStorageType(SgConstantsIF.STORAGE_TYPE_PREIN);
                prein.setQtyPreinChange(preinQtyChange);
                if (MapUtils.isNotEmpty(negativeStock)) {
                    SgStorageUpdateControlRequest controlRequest = new SgStorageUpdateControlRequest();
                    CpCStore cpCStore = negativeStock.get(sgBReceiveItem.getCpCStoreId());
                    Long isnegative = cpCStore.getIsnegative();
                    //1 开启负库存控制（不允许负库存）0 允许负库存
                    isnegative = isnegative == null ? 1L : isnegative;
                    if (isNegativePrein) isnegative = 0L;
                    controlRequest.setNegativePrein(isnegative == 0);
                    prein.setControlmodel(controlRequest);
                }
                itemList.add(prein);
            }
            if ((SUBMIT.equals(action) || islast) && (0 != BigDecimal.ZERO.compareTo(preinQtyChange)
                    || 0 != BigDecimal.ZERO.compareTo(storageQtyChange))) {
                SgStorageUpdateBillItemRequest storage = new SgStorageUpdateBillItemRequest();
                BeanUtils.copyProperties(sgBReceiveItem, storage);
                storage.setBillItemId(sgBReceiveItem.getId());
                //storage.setStorageType(SgConstantsIF.STORAGE_TYPE_STORAGE);
                storage.setQtyPreinChange(preinQtyChange);
                storage.setQtyStorageChange(storageQtyChange);
                if (MapUtils.isNotEmpty(negativeStock)) {
                    SgStorageUpdateControlRequest controlRequest = new SgStorageUpdateControlRequest();
                    CpCStore cpCStore = negativeStock.get(sgBReceiveItem.getCpCStoreId());
                    Long isnegative = cpCStore.getIsnegative();
                    //1 开启负库存控制（不允许负库存）0 允许负库存
                    isnegative = isnegative == null ? 1L : isnegative;
                    if (isNegativePrein) isnegative = 0L;
                    controlRequest.setNegativePrein(isnegative == 0);
                    controlRequest.setNegativeStorage(isnegative == 0);
                    controlRequest.setNegativeAvailable(isnegative == 0);
                    storage.setControlmodel(controlRequest);
                }
                itemList.add(storage);
            }
        });
        return itemList;
    }
}
