package com.jackrain.nea.sg.send.utils;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.FatalBeanException;
import org.springframework.util.ClassUtils;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

/**
 * @author: 舒威
 * @since: 2019/10/20
 * create at : 2019/10/20 17:29
 */
public class BeanCopyUtils {


    private static PropertyDescriptor[] cacheDecriptor = new PropertyDescriptor[]{};

    public static void copyProperties(Object source, Object target) throws BeansException {

        Class<?> actualEditable = target.getClass();

        if (cacheDecriptor.length == 0) {
            cacheDecriptor = BeanUtils.getPropertyDescriptors(actualEditable);
        }

        PropertyDescriptor[] targetPds = cacheDecriptor;

        for (PropertyDescriptor targetPd : targetPds) {
            Method writeMethod = targetPd.getWriteMethod();
            if (writeMethod != null) {
                PropertyDescriptor sourcePd = BeanUtils.getPropertyDescriptor(source.getClass(), targetPd.getName());
                if (sourcePd != null) {
                    Method readMethod = sourcePd.getReadMethod();
                    if (readMethod != null &&
                            ClassUtils.isAssignable(writeMethod.getParameterTypes()[0], readMethod.getReturnType())) {
                        try {
                            if (!Modifier.isPublic(readMethod.getDeclaringClass().getModifiers())) {
                                readMethod.setAccessible(true);
                            }
                            Object value = readMethod.invoke(source);
                            if (!Modifier.isPublic(writeMethod.getDeclaringClass().getModifiers())) {
                                writeMethod.setAccessible(true);
                            }
                            writeMethod.invoke(target, value);
                        } catch (Throwable ex) {
                            throw new FatalBeanException(
                                    "Could not copy property '" + targetPd.getName() + "' from source to target", ex);
                        }
                    }
                }
            }
        }
    }

    public static void clear() {
        cacheDecriptor = new PropertyDescriptor[]{};
    }
}
