package com.jackrain.nea.sg.transfer.mapper;

import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.transfer.model.table.ScBTransferDiffItem;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

@Mapper
public interface ScBTransferDiffItemMapper extends ExtentionMapper<ScBTransferDiffItem> {

    @Update("UPDATE sc_b_transfer_diff_item SET handle_way=#{handleWay} WHERE id IN(${ids})")
    int updateDiffItemByIds(@Param("handleWay") String handleWay, @Param("ids") String ids);
}