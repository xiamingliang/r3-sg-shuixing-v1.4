package com.jackrain.nea.sg.transfer.services;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.model.util.AdParamUtil;
import com.jackrain.nea.redis.config.CusRedisTemplate;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.config.SgStorageBoxConfig;
import com.jackrain.nea.sg.basic.utils.R3ParamUtil;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.send.utils.SgStoreESUtils;
import com.jackrain.nea.sg.send.utils.SgStoreUtils;
import com.jackrain.nea.sg.transfer.common.ScTransferConstants;
import com.jackrain.nea.sg.transfer.common.SgTransferConstantsIF;
import com.jackrain.nea.sg.transfer.enums.SgTransferBillStatusEnum;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferItemMapper;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferMapper;
import com.jackrain.nea.sg.transfer.model.table.ScBTransfer;
import com.jackrain.nea.sg.transfer.model.table.ScBTransferItem;
import com.jackrain.nea.sg.transfer.utils.SgTransferBillGeneraUtil;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.tableService.constants.Constants;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.utility.ExceptionUtil;
import com.jackrain.nea.utils.AssertUtils;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * @author csy
 * Date: 2019/5/8
 * Description: 调拨单-取消审核
 */
@Slf4j
@Component
public class SgTransferCancelAuditService {

    @Autowired
    private SgStorageBoxConfig storageBoxConfig;

    @Autowired
    private SgStoreESUtils storeESUtils;


    public ValueHolder cancelAuditR3(Long id, User user) {
        return R3ParamUtil.convertV14(cancelAudit(id, user, false));
    }

    @Transactional(rollbackFor = Exception.class)
    public ValueHolderV14 cancelAudit(Long id, User user, boolean isCancelOut) {
        AssertUtils.notNull(user, "user is null,login first!");
        Locale locale = user.getLocale();
        AssertUtils.notNull(id, "id不能为空", locale);
        CusRedisTemplate<String, String> redisTemplate = SgStoreUtils.initRedisTemplate();
        String lockKsy = ScTransferConstants.TRANSFER_INDEX + ":" + id;
        if (redisTemplate.opsForValue().get(lockKsy) != null) {
            throw new NDSException(Resources.getMessage("当前记录正在被操作,请稍后重试!", user.getLocale()));
        } else {
            try {
                redisTemplate.opsForValue().set(lockKsy, "OK", 30, TimeUnit.MINUTES);
                SgTransferBillGeneraUtil generaUtil = ApplicationContextHandle.getBean(SgTransferBillGeneraUtil.class);
                ScBTransferMapper mapper = ApplicationContextHandle.getBean(ScBTransferMapper.class);
                ScBTransfer transfer = mapper.selectById(id);
                //======== 前置校验=======
                statusCheck(transfer, locale);
                //TODO:如果本单【调拨类型】为订单调拨，调用发货订单的【发货订单更新已配已发量】  missing
                Integer transferType = transfer.getTransferType();
                if (transferType == SgTransferConstantsIF.TRANSFER_TYPE_ORDER) {
                    ScBTransferItemMapper itemMapper = ApplicationContextHandle.getBean(ScBTransferItemMapper.class);
                    List<ScBTransferItem> itemList = itemMapper.selectList(new QueryWrapper<ScBTransferItem>().lambda()
                            .eq(ScBTransferItem::getScBTransferId, transfer.getId()));
                    generaUtil.createOcSend(user, transfer, itemList, null, SgTransferBillStatusEnum.UN_AUDITED.getVal());
                }

                if (SgConstants.IS_ACTIVE_Y.equals(transfer.getIsOutReduce())) {

                    //==========检查 是否存在对应的 出库结果单==========
                    // 调拨单取消出库跳过此控制
                    if (!isCancelOut) {
                        generaUtil.checkOutResult(transfer, user);
                    }

                    // 作废出库通知单
                    generaUtil.cancelOutNotice(user, transfer, isCancelOut);

                    // 清空逻辑发货单
                    Integer isLocked = transfer.getIsLocked();
                    if (isLocked != null && isLocked == ScTransferConstants.IS_UNLOCKED) {
                        generaUtil.cleanSend(user, transfer, isCancelOut);
                    }
                }

                if (SgConstants.IS_ACTIVE_Y.equals(transfer.getIsInIncrease())) {
                    // 如果【作废出库通知单并取消WMS】返回成功，并且参数【生成逻辑收货单节点】=审核且【是否入库加库存】=是时，
                    // 调用【清空逻辑收货单】服务
                    String checkPoint = AdParamUtil.getParam(ScTransferConstants.SYSTEM_RECEIVE);
                    if (StringUtils.isNotEmpty(checkPoint) && Constants.ACTION_SUBMIT.equals(checkPoint)) {
                        generaUtil.cleanReceive(user, transfer, SgConstantsIF.SERVICE_NODE_TRANSFER_UNSUBMIT);
                    }
                }


                ScBTransfer updateTransfer = new ScBTransfer();
                /**
                 * cs 添加是否开启 箱功能逻辑
                 * 开启 箱功能  清空 本单的 箱内条码明细 与 条码明细
                 * @data 2019.10.23 10:13
                 */
                if (storageBoxConfig.getBoxEnable()) {
                    ScBTransferBoxFunctionService scBTransferBoxFunctionService = ApplicationContextHandle.getBean(ScBTransferBoxFunctionService.class);
                    scBTransferBoxFunctionService.clearTransferItemsAndTeus(transfer);
                    updateTransfer.setTotAmtList(BigDecimal.ZERO);
                    updateTransfer.setTotQty(BigDecimal.ZERO);
                }

                updateTransfer.setId(id);
                StorageESUtils.setBModelDefalutDataByUpdate(updateTransfer, user);
                updateTransfer.setModifierename(user.getEname());
                updateTransfer.setStatus(SgTransferBillStatusEnum.UN_AUDITED.getVal());
                updateTransfer.setUncheckId(user.getId().longValue());
                updateTransfer.setUncheckEname(user.getEname());
                updateTransfer.setUncheckName(user.getName());
                updateTransfer.setUncheckTime(new Date());
                //更新审核状态 并置空审核人相关信息
                mapper.update(updateTransfer, new UpdateWrapper<ScBTransfer>().lambda()
                        .eq(ScBTransfer::getId, id)
                        .set(ScBTransfer::getStatusId, null)
                        .set(ScBTransfer::getStatusEname, null)
                        .set(ScBTransfer::getStatusTime, null)
                        .set(ScBTransfer::getStatusName, null));
                //推送ES
                storeESUtils.pushESByTransfer(id, false, false, null, mapper, null);
            } catch (Exception e) {
                AssertUtils.logAndThrow(ExceptionUtil.getMessage(e), locale);
            } finally {
                redisTemplate.delete(lockKsy);
            }
        }
        return new ValueHolderV14(ResultCode.SUCCESS, Resources.getMessage("取消审核成功!", locale));
    }


    /**
     * 前置校验
     *
     * @param transfer 调拨单信息
     */
    private void statusCheck(ScBTransfer transfer, Locale locale) {
/*a)	单据未审核，则提示：“当前记录未审核，不允许取消审核！”；
b)	单据拣货中，则提示：“当前记录拣货中，不允许取消审核，请联系仓库人员将出库单取消拣货！”；
c)	单据状态=部分出库或者完全出库，则提示：“当前记录已出库，不允许取消审核！”；
d)	单据已作废，则提示：“当前记录已作废，不允许取消审核！”；
e)	单据不存在，则提示：“当前记录已不存在！”。*/
        AssertUtils.notNull(transfer, "当前记录已不存在！", locale);
        Integer billStatus = transfer.getStatus();
        Long oneClickOutLibraryStatus = transfer.getReserveBigint01();
        Integer pickStatus = transfer.getPickStatus();
        AssertUtils.notNull(billStatus, "数据异常！-STATUS为空", locale);
        AssertUtils.notNull(pickStatus, "数据异常！-PICK_STATUS为空", locale);
        AssertUtils.cannot(billStatus == SgTransferBillStatusEnum.VOIDED.getVal(), "当前记录已作废，不允许取消审核！", locale);
        AssertUtils.cannot(billStatus == SgTransferBillStatusEnum.UN_AUDITED.getVal(), "当前记录未审核，不允许取消审核！", locale);
        AssertUtils.cannot(pickStatus == ScTransferConstants.PICK_STATUS_PICKING, "当前记录拣货中，不允许取消审核，请联系仓库人员将出库单取消拣货！", locale);
        AssertUtils.cannot(billStatus >= SgTransferBillStatusEnum.AUDITED_PART_OUT_NOT_IN.getVal(), "当前记录已出库，不允许取消审核！", locale);
        if (oneClickOutLibraryStatus != null) {
            AssertUtils.cannot(oneClickOutLibraryStatus.equals(ScTransferConstants.TYPE_LIBRARY_STATUS_OUTSTRAGEING), "当前记录出库中，不允许取消审核！", locale);
            AssertUtils.cannot(oneClickOutLibraryStatus.equals(ScTransferConstants.TYPE_LIBRARY_STATUS_SUCCESS_OUTSTRAGE), "当前记录出库完成，不允许取消审核！", locale);
        }
    }
}
