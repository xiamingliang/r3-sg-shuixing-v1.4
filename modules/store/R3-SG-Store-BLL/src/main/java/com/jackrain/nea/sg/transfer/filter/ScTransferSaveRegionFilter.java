package com.jackrain.nea.sg.transfer.filter;

import com.jackrain.nea.cp.result.ReginQueryResult;
import com.jackrain.nea.data.basic.model.request.RegionInfoQueryRequest;
import com.jackrain.nea.data.basic.services.BasicCpQueryService;
import com.jackrain.nea.tableService.MainTableRecord;
import com.jackrain.nea.tableService.TableServiceContext;
import com.jackrain.nea.tableService.filter.BaseFilter;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.utils.AssertUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author csy
 * Date: 2019/5/27
 * Description:补充省市区 的 filter 可改成通用的
 */

@Component
@Slf4j
public class ScTransferSaveRegionFilter extends BaseFilter {

    @Override
    public void before(TableServiceContext context, MainTableRecord row) {

        Long cityId = row.getMainData().getCommitData().getLong("RECEIVER_CITY_ID");
        Long areaId = row.getMainData().getCommitData().getLong("RECEIVER_DISTRICT_ID");
        Long provinceId = row.getMainData().getCommitData().getLong("RECEIVER_PROVINCE_ID");
        BasicCpQueryService queryService = ApplicationContextHandle.getBean(BasicCpQueryService.class);
        if (cityId != null || areaId != null || provinceId != null) {
            RegionInfoQueryRequest request = new RegionInfoQueryRequest();
            request.setCityId(cityId);
            request.setRegionId(areaId);
            request.setProId(provinceId);
            try {
                Map resultMap = queryService.getRegionInfo(request);
                if (log.isDebugEnabled()) {
                    log.debug("transfer regionInfo query result:" + resultMap.toString());
                }
                if (resultMap != null) {
                    ReginQueryResult reginQueryResult = (ReginQueryResult) resultMap.get("data");
                    if (reginQueryResult != null) {
                        row.getMainData().getCommitData().put("RECEIVER_CITY_ID", cityId);
                        row.getMainData().getCommitData().put("RECEIVER_CITY_ENAME", reginQueryResult.getCityName());
                        row.getMainData().getCommitData().put("RECEIVER_DISTRICT_ID", areaId);
                        row.getMainData().getCommitData().put("RECEIVER_DISTRICT_ENAME", reginQueryResult.getRegionName());
                        row.getMainData().getCommitData().put("RECEIVER_PROVINCE_ID", provinceId);
                        row.getMainData().getCommitData().put("RECEIVER_PROVINCE_ENAME", reginQueryResult.getProvName());
                    }
                }
            } catch (Exception e) {
                log.error("省市区信息查询失败!", e);
                AssertUtils.logAndThrow("省市区信息查询失败!");
            }
        }
    }
}
