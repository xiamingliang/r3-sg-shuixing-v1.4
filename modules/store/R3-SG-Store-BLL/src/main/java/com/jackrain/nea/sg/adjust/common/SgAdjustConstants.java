package com.jackrain.nea.sg.adjust.common;

/**
 *@author csy
 * */
public interface SgAdjustConstants {

    /**
     * 审核状态 未审核
     **/
    int ADJ_AUDIT_STATUS_N = 1;

    /**
     * 审核状态 已审核
     */
    int ADJ_AUDIT_STATUS_Y = 2;


    String ADJ_BILL_SEQ = "SEQ_SG_B_ADJUST";


    /**
     * mq msgKey的时间规则
     */
    String MQ_MSG_KEY_TIME_PATTERN = "yyyyMMddHHmmssSSS";

    /**
     * 库存消息tag
     */
    String MQ_TAG = "storage_adjust";


    /**
     * 明细表 主表id字段
     */
    String SG_B_ADJUST_ID = "sg_b_adjust_id";

    /**
     * 是否同步结算
     */
    String R3_IS_SYNCH_AC_SC_BILL_CONVERT="sg.ac.sc.is_synch_ac_sc_bill_convert";

    /**
     * 调整单性质
     * 无头件入库 : 1
     * 冲无头件；2
     * 销退错录：5
     * 分销错录：6
     * 唯品会退货：7
     */
    Long SG_B_ADJUST_PROP_NO_SOURCE_IN = 1L;
    Long SG_B_ADJUST_PROP_FLUSH_NO_SOURCE = 2L;
    Long SG_B_ADJUST_PROP_SALES_RETURN_FAIL_RECORD = 5L;
    Long SG_B_ADJUST_PROP_DISTRIB_SALES_FAIL_RECORD = 6L;
    Long SG_B_ADJUST_PROP_VIP_RETURN = 7L;
}
