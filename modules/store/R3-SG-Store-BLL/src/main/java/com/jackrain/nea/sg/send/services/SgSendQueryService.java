package com.jackrain.nea.sg.send.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Maps;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.send.mapper.SgBSendItemMapper;
import com.jackrain.nea.sg.send.mapper.SgBSendMapper;
import com.jackrain.nea.sg.send.model.request.SgStoreBillBase;
import com.jackrain.nea.sg.send.model.result.SgSendBillQueryResult;
import com.jackrain.nea.sg.send.model.table.SgBSend;
import com.jackrain.nea.sg.send.model.table.SgBSendItem;
import com.jackrain.nea.sg.send.utils.SgStoreUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;

/**
 * @author 舒威
 * @since 2019/6/27
 * create at : 2019/6/27 14:32
 */
@Slf4j
@Component
public class SgSendQueryService {

    @Autowired
    private SgBSendMapper sendMapper;

    @Autowired
    private SgBSendItemMapper sendItemMapper;

    public ValueHolderV14<SgSendBillQueryResult> querySgBSend(List<SgStoreBillBase> searchs) {
        if (log.isDebugEnabled()) {
            log.debug("批量查询逻辑发货单服务入参:" + JSONObject.toJSONString(searchs) + ";");
        }

        for (SgStoreBillBase search : searchs) {
            SgStoreUtils.checkBModelDefalut(search, true);
        }

        ValueHolderV14<SgSendBillQueryResult> v14 = new ValueHolderV14<>();
        HashMap<String, HashMap<SgBSend, List<SgBSendItem>>> results = Maps.newHashMap();

        for (SgStoreBillBase search : searchs) {
            Long sourceBillId = search.getSourceBillId();
            String sourceBillNo = search.getSourceBillNo();
            Integer sourceBillType = search.getSourceBillType();
            String key = sourceBillId + "," + sourceBillType;
            HashMap<SgBSend, List<SgBSendItem>> map=Maps.newHashMap();

            SgBSend send = sendMapper.selectOne(new QueryWrapper<SgBSend>().lambda()
                    .eq(SgBSend::getSourceBillId, sourceBillId)
                    .eq(SgBSend::getSourceBillNo, sourceBillNo)
                    .eq(SgBSend::getSourceBillType, sourceBillType)
                    .eq(SgBSend::getIsactive, SgConstants.IS_ACTIVE_Y));
            if (send != null) {
                List<SgBSendItem> items = sendItemMapper.selectList(new QueryWrapper<SgBSendItem>().lambda()
                        .eq(SgBSendItem::getSgBSendId, send.getId()));
                if (CollectionUtils.isNotEmpty(items)) {
                    map.put(send,items);
                    results.put(key,map);
                }
            }
        }

        v14.setCode(ResultCode.SUCCESS);
        v14.setMessage("success");
        SgSendBillQueryResult data=new SgSendBillQueryResult();
        data.setResults(results);
        v14.setData(data);
        return v14;
    }
}
