package com.jackrain.nea.sg.transfer.erp;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.data.basic.model.request.CpcStoreWrapper;
import com.jackrain.nea.data.basic.services.BasicCpQueryService;
import com.jackrain.nea.oc.sale.model.table.OcBTransLog;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.basic.utils.StorageUtils;
import com.jackrain.nea.sg.transfer.common.ScTransferConstants;
import com.jackrain.nea.sg.transfer.common.SgTransferConstantsIF;
import com.jackrain.nea.sg.transfer.model.table.ScBTransfer;
import com.jackrain.nea.sg.transfer.model.table.ScBTransferItem;
import com.jackrain.nea.sg.transfer.services.SgTransferRpcService;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.utils.BillType;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

/**
 * @author leexh
 * @since 2020/4/2 13:23
 * desc: 调拨出库单-出库同步erp
 */
@Slf4j
@Component
public class SgTransferOutSynErp {

    public void synErpTransferOut(Locale locale, ScBTransfer transfer, List<ScBTransferItem> itemList) {

        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ",synErpTransferOut:" + JSON.toJSONString(transfer));
        }

        try {

            // 过滤调拨性质为【同级调拨】，【发货店仓】的经营类型为‘集团直营’
            String propName = transfer.getSgBTransferPropEname();
            if (StringUtils.equals(SgTransferConstantsIF.TRANSFER_PROP_TJ_STR, propName)) {

                Long origId = transfer.getCpCOrigId();
                AssertUtils.notNull(origId, "发货店仓为空！", locale);

                BasicCpQueryService psQueryService = ApplicationContextHandle.getBean(BasicCpQueryService.class);
                CpcStoreWrapper origStore = psQueryService.findCpcStore(origId);
                AssertUtils.cannot(origStore == null || StringUtils.isEmpty(origStore.getManageTypeCode()),
                        "发货店仓/经营类型查询为空！", locale);

                // 【发货店仓】的经营类型为‘集团直营’
                String manageTypeCode = origStore.getManageTypeCode();
                if (StringUtils.equals(ScTransferConstants.MANAGE_TYPE_CODE_DIRECTLY, manageTypeCode)) {


                    // 数据组装
                    OcBTransLog transLog = new OcBTransLog();
                    transLog.setOrigBillContent(getReqData(transfer, itemList).toJSONString());
                    transLog.setBillType(BillType.TRANSFER_OUT_SYNC.getType());
                    transLog.setSourceBillNo(transfer.getId() + "");

                    if (log.isDebugEnabled()) {
                        log.debug(this.getClass().getName() + ",调拨出库同步中台中间表入参:" + JSON.toJSONString(transLog));
                    }

                    // 调用中间表服务
                    SgTransferRpcService rpcService = ApplicationContextHandle.getBean(SgTransferRpcService.class);
                    ValueHolderV14<OcBTransLog> holderV14 = rpcService.synTransLog(transLog);
                    if (log.isDebugEnabled()) {
                        log.debug(this.getClass().getName() + ",调拨出库同步中台中间表出参:" + JSON.toJSONString(holderV14));
                    }
                }
            }

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error(this.getClass().getName() + ",调拨出库同步中台中间表异常:" + StorageUtils.getExceptionMsg(e));
            }
        }
    }

    /**
     * 调拨单数据封装（传ERP）
     *
     * @param transfer 调拨单
     * @return 出参
     */
    private JSONObject getReqData(ScBTransfer transfer, List<ScBTransferItem> itemList) {

        // 主表
        JSONObject mainObj = new JSONObject();
        // 单据编号、出库日期
        mainObj.put("bri01", transfer.getBillNo());
        mainObj.put("bri03", transfer.getOutDate());

        // 发货店仓编码、发货店仓法人编码
        mainObj.put("bri04", transfer.getCpCOrigEcode());
        mainObj.put("bri05", transfer.getOrigBelongsLegal());

        // 收货店仓编码、收货店仓法人编码
        mainObj.put("bri06", transfer.getCpCDestEcode());
        mainObj.put("bri07", transfer.getDestBelongsLegal());

        // 创建人编码（用户名）
        mainObj.put("bri09", transfer.getOwnername());

        // 审核日期、审核时间、审核人员编码（用户名）
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
        mainObj.put("bri12", dateFormat.format(transfer.getStatusTime()));
        mainObj.put("bri13", timeFormat.format(transfer.getStatusTime()));
        mainObj.put("bri14", transfer.getStatusName());

        // 备注
        mainObj.put("bri10", transfer.getRemark());

        // 默认字段：类别：1=拨出，2=拨入；审核码：Y
        mainObj.put("bri00", "1");
        mainObj.put("briconf", "Y");


        // 明细表
        JSONArray itemListObj = new JSONArray();
        itemList.forEach(item -> {
            JSONObject itemObj = new JSONObject();
            // 单据编号
            itemObj.put("brj01", transfer.getBillNo());
            // 商品编码
            itemObj.put("brj03", item.getPsCProEcode());
            // 发货店仓编码
            itemObj.put("brj04", transfer.getCpCOrigEcode());
            // 收货店仓编码
            itemObj.put("brj05", transfer.getCpCDestEcode());
            // 出库数量
            itemObj.put("brj06", item.getQtyOut());
            // 单位编码
            itemObj.put("brj07", item.getUnitEcode());
            // 备注
            itemObj.put("brj08", transfer.getRemark());
            itemListObj.add(itemObj);
        });

        // 装载明细数据
        mainObj.put("detailList", itemListObj);

        // 消息体
        JSONObject billContent = new JSONObject();
        billContent.put("data", mainObj);
        JSONObject service = new JSONObject();
        // ERP服务名
        service.put("name", "transOrder");
        billContent.put("service", service);
        return billContent;
    }


}
