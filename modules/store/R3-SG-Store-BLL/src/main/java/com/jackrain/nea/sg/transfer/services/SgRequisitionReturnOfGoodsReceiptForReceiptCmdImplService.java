package com.jackrain.nea.sg.transfer.services;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.oc.sale.common.OcSaleConstantsIF;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.transfer.enums.SgTransferBillStatusEnum;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferMapper;
import com.jackrain.nea.sg.transfer.model.request.SgRequisitionRequest;
import com.jackrain.nea.sg.transfer.model.table.ScBTransfer;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author ZhangQK
 * 销退收货签收--调拨单
 * @date 2020/4/8 19:14
 */
@Slf4j
@Component
public class SgRequisitionReturnOfGoodsReceiptForReceiptCmdImplService {
    @Autowired
    private ScBTransferMapper scBTransferMapper;
    public ValueHolderV14<Map<String,Integer>> sgSaleReceiving(List<SgRequisitionRequest> ocSaleReundQueryRequestList){
        Map<String,Integer> map=new HashMap<>();
        JSONArray errorArr = new JSONArray();
        Integer accSuccess = 0, accFailed = 0;
        // SC_B_TRANSFER 调拨单     状态status
        for (SgRequisitionRequest ocSaleReundQueryRequest:ocSaleReundQueryRequestList){
                                                                   //SC_B_TRANSFER
            ScBTransfer scBTransfer = scBTransferMapper.selectOne(new QueryWrapper<ScBTransfer>().lambda()
                    .eq(ScBTransfer::getBillNo, ocSaleReundQueryRequest.getBillNo())
                    .eq(ScBTransfer::getStatus, SgTransferBillStatusEnum.AUDITED_ALL_OUT_NOT_IN.getVal())//出库完成未入库
                    .eq(ScBTransfer::getIsactive, SgConstants.IS_ACTIVE_Y));
            if (scBTransfer==null){
                accFailed=errorRecord(ocSaleReundQueryRequest.getBillNo(),"当前记录不存在！",errorArr,accFailed);
                continue;
            }else{
                ScBTransfer scBTransfer1=new ScBTransfer();
                //入库箱数qtyInteus
                scBTransfer1.setQtyInTeus(new BigDecimal(Optional.ofNullable(ocSaleReundQueryRequest.getQtyInTeus()).orElse(0)));
                //入库包数qtyPack
                scBTransfer1.setQtyInPack(new BigDecimal(Optional.ofNullable(ocSaleReundQueryRequest.getQtyInPack()).orElse(0)));
                //入库件数
                scBTransfer1.setQtyInUnit(new BigDecimal(Optional.ofNullable(ocSaleReundQueryRequest.getQtyInunit()).orElse(0)));
                scBTransferMapper.update(scBTransfer1,new QueryWrapper<ScBTransfer>().lambda().eq(ScBTransfer::getBillNo, ocSaleReundQueryRequest.getBillNo()));
                // 统计成功条数
                accSuccess++;
            }
        }
        map.put("accSuccess",accSuccess);
        map.put("accFailed",accFailed);
        if (log.isDebugEnabled()) {
            log.debug("-------------------------->调拨单收货签收<--------------------------");
            log.debug(map.toString());
        }
        ValueHolderV14<Map<String,Integer>> v14=new ValueHolderV14<>();
        v14.setData(map);
        v14.setCode(ResultCode.SUCCESS);
        v14.setMessage("处理成功");
        return v14;
    }

    /**
     * 错误数据收集
     *
     * @param billNo
     * @param message
     * @param errorArr
     */
    public Integer errorRecord(String billNo, String message, JSONArray errorArr, Integer accFailed) {
        JSONObject errorDate = new JSONObject();
        errorDate.put("code", ResultCode.FAIL);
        errorDate.put("billno", billNo);
        errorDate.put("message", message);
        errorArr.add(errorDate);
        return ++accFailed;
    }
}
