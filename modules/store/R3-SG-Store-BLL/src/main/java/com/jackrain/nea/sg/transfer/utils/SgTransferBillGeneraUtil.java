package com.jackrain.nea.sg.transfer.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.ac.sc.basic.model.enums.SourceBillType;
import com.jackrain.nea.ac.sc.basic.model.request.AcPushAcBillRequest;
import com.jackrain.nea.ac.sc.utils.utils.AcScOrigBillUtil;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.data.basic.model.request.ProInfoQueryRequest;
import com.jackrain.nea.data.basic.services.BasicPsQueryService;
import com.jackrain.nea.oc.basic.config.OcBoxConfig;
import com.jackrain.nea.oc.sale.api.OcSendOutQtyUpdateCmd;
import com.jackrain.nea.oc.sale.model.request.OcSendOutImpItemQtyUpdateRequest;
import com.jackrain.nea.oc.sale.model.request.OcSendOutItemQtyUpdateRequest;
import com.jackrain.nea.oc.sale.model.request.OcSendOutQtyUpdateRequest;
import com.jackrain.nea.ps.api.result.PsCProResult;
import com.jackrain.nea.r3.mq.util.R3MqSendHelper;
import com.jackrain.nea.sg.basic.common.R3ParamConstants;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.mapper.CpCPhyWarehouseMapper;
import com.jackrain.nea.sg.basic.model.result.SgR3BaseResult;
import com.jackrain.nea.sg.basic.model.table.CpCPhyWarehouse;
import com.jackrain.nea.sg.in.api.SgPhyInNoticesAndResultSaveCmd;
import com.jackrain.nea.sg.in.api.SgPhyInNoticesSaveCmd;
import com.jackrain.nea.sg.in.api.SgPhyInNoticesSelectBySourceCmd;
import com.jackrain.nea.sg.in.api.SgPhyInResultSelectCmd;
import com.jackrain.nea.sg.in.common.SgInConstants;
import com.jackrain.nea.sg.in.common.SgInNoticeConstants;
import com.jackrain.nea.sg.in.model.request.*;
import com.jackrain.nea.sg.in.model.result.SgBPhyInNoticesResult;
import com.jackrain.nea.sg.in.model.table.SgBPhyInResult;
import com.jackrain.nea.sg.in.model.table.SgBPhyInResultItem;
import com.jackrain.nea.sg.out.api.SgPhyOutNoticesBatchVoidAndWMSCmd;
import com.jackrain.nea.sg.out.api.SgPhyOutNoticesSaveCmd;
import com.jackrain.nea.sg.out.api.SgPhyOutQueryCmd;
import com.jackrain.nea.sg.out.api.SgPhyOutResultSaveAndAuditCmd;
import com.jackrain.nea.sg.out.common.SgOutConstantsIF;
import com.jackrain.nea.sg.out.model.request.*;
import com.jackrain.nea.sg.out.model.result.SgOutQueryResult;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutResult;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutResultItem;
import com.jackrain.nea.sg.receive.model.request.*;
import com.jackrain.nea.sg.receive.services.SgReceiveCleanService;
import com.jackrain.nea.sg.receive.services.SgReceiveSaveService;
import com.jackrain.nea.sg.receive.services.SgReceiveSubmitService;
import com.jackrain.nea.sg.receive.services.SgReceiveVoidService;
import com.jackrain.nea.sg.send.common.SgSendConstantsIF;
import com.jackrain.nea.sg.send.model.request.*;
import com.jackrain.nea.sg.send.services.SgSendCleanService;
import com.jackrain.nea.sg.send.services.SgSendSaveService;
import com.jackrain.nea.sg.send.services.SgSendSubmitService;
import com.jackrain.nea.sg.send.services.SgSendVoidService;
import com.jackrain.nea.sg.transfer.common.ScTransferConstants;
import com.jackrain.nea.sg.transfer.common.SgTransferConstantsIF;
import com.jackrain.nea.sg.transfer.config.SgTransferMqConfig;
import com.jackrain.nea.sg.transfer.enums.SgTransferBillStatusEnum;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferInItemMapper;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferMapper;
import com.jackrain.nea.sg.transfer.model.AcTransferItemRequest;
import com.jackrain.nea.sg.transfer.model.request.SgTransferBoxItemRequest;
import com.jackrain.nea.sg.transfer.model.request.SgTransferImpItemRequest;
import com.jackrain.nea.sg.transfer.model.table.*;
import com.jackrain.nea.sg.transfer.services.ScBTransferBoxFunctionService;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.DateUtil;
import com.jackrain.nea.utility.ExceptionUtil;
import com.jackrain.nea.utils.AssertUtils;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author csy
 * Date: 2019/5/10
 * Description: 调拨单生成其他单据util
 */
@Slf4j
@Component
public class SgTransferBillGeneraUtil {

    @Autowired
    private ScBTransferBoxFunctionService boxFunctionService;

    @Autowired
    private OcBoxConfig boxConfig;

    /**
     * 新增逻辑发货单
     *
     * @param user          用户
     * @param transfer      主表信息
     * @param transferItems 明细信息
     */
    public void createSendBill(User user, ScBTransfer transfer, List<ScBTransferItem> transferItems,
                               List<ScBTransferImpItem> impItems, List<ScBTransferTeusItem> boxItems) {
        createSendBill(user, transfer, transferItems, false, false,
                SgConstantsIF.SERVICE_NODE_TRANSFER_SUBMIT, impItems, boxItems);
    }


    /**
     * 新增逻辑发货单
     * <p>
     * 废弃  由结果单来调用
     *
     * @param user          用户
     * @param transfer      主表信息
     * @param transferItems 明细信息
     * @param isLast        是否是最后一次出库
     */
    @Deprecated
    public void createSendBillByOut(User user, ScBTransfer transfer, List<ScBTransferItem> transferItems, boolean isLast,
                                    List<ScBTransferImpItem> impItems, List<ScBTransferTeusItem> boxItems) {
        createSendBill(user, transfer, transferItems, true, isLast, SgConstantsIF.SERVICE_NODE_TRANSFER_OUT,
                impItems, boxItems);
    }


    /**
     * 新增逻辑发货单
     *
     * @param user          用户
     * @param transfer      主表信息
     * @param transferItems 明细信息
     * @param isOutResult   是否是出库通知单
     * @param isSetZero     是否传0
     * @param serviceNode   业务节点
     */
    private void createSendBill(User user, ScBTransfer transfer, List<ScBTransferItem> transferItems,
                                boolean isOutResult, boolean isSetZero, Long serviceNode,
                                List<ScBTransferImpItem> impItems, List<ScBTransferTeusItem> boxItems) {
        if (log.isDebugEnabled()) {
            log.debug("调拨单:" + transfer.getBillNo() + ",开始调用新增逻辑发货单");
        }
        Locale locale = user.getLocale();
        SgSendSaveService sendSaveService = ApplicationContextHandle.getBean(SgSendSaveService.class);
        SgSendBillSaveRequest request = new SgSendBillSaveRequest();
        request.setPreoutWarningType(SgConstantsIF.PREOUT_RESULT_ERROR);
        request.setLoginUser(user);

        // 2019-08-19添加逻辑：调拨差异生成的调拨单传true
        String handleWay = transfer.getHandleWay();
        Integer transferType = transfer.getTransferType();
        if (SgTransferConstantsIF.HANDLE_WAY_FHTZ.equals(handleWay) || SgTransferConstantsIF.TRANSFER_TYPE_DEFICIENCY == transferType) {
            request.setIsNegativePreout(true);
        }
        SgSendSaveRequest sgSend = new SgSendSaveRequest();
        sgSend.setServiceNode(serviceNode);
        sgSend.setRemark(transfer.getRemark());
        sgSend.setSourceBillId(transfer.getId());
        sgSend.setSourceBillNo(transfer.getBillNo());
        sgSend.setSourceBillType(SgConstantsIF.BILL_TYPE_TRANSFER);
        request.setSgSend(sgSend);

        List<SgSendItemSaveRequest> items = new ArrayList<>();
        Long storeId = transfer.getCpCOrigId();
        String storeECode = transfer.getCpCOrigEcode();
        String storeEName = transfer.getCpCOrigEname();
        transferItems.forEach(transferItem -> {
            SgSendItemSaveRequest item = new SgSendItemSaveRequest();
            BeanUtils.copyProperties(transferItem, item);
            item.setSourceBillItemId(transferItem.getId());
            item.setPsCSpec1Id(transferItem.getPsCClrId());
            item.setPsCSpec1Ecode(transferItem.getPsCClrEcode());
            item.setPsCSpec1Ename(transferItem.getPsCClrEname());
            item.setPsCSpec2Id(transferItem.getPsCSizeId());
            item.setPsCSpec2Ecode(transferItem.getPsCSizeEcode());
            item.setPsCSpec2Ename(transferItem.getPsCSizeEname());
            item.setId(null);
            item.setCpCStoreId(storeId);
            item.setCpCStoreEcode(storeECode);
            item.setCpCStoreEname(storeEName);
            if (isOutResult) {
                if (isSetZero) {
                    item.setQty(BigDecimal.ZERO);
                    item.setQtyPreout(BigDecimal.ZERO);
                } else {
                    item.setQty(transferItem.getQty().subtract(transferItem.getQtyOut()));
                    item.setQtyPreout(transferItem.getQty().subtract(transferItem.getQtyOut()));
                }
            } else {
                item.setQty(transferItem.getQty());
                item.setQtyPreout(transferItem.getQty());
            }
            items.add(item);
        });
        request.setItemList(items);

        // 箱功能开启时，传录入明细和箱内明细
        if (boxConfig.getBoxEnable()) {
            boxFunctionService.createSgSendBox(request, transfer, impItems, boxItems);
        }
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ",createSendBillForTransfer param:" + JSON.toJSONString(request));
        }

        ValueHolderV14 holderV14 = sendSaveService.saveSgBSend(request);
        AssertUtils.cannot(holderV14.getCode() == ResultCode.FAIL, "调用逻辑发货单新增失败:" + holderV14.getMessage(), locale);
    }

    /**
     * 新增出库通知单
     *
     * @param user          用户
     * @param transfer      主表信息
     * @param transferItems 明细信息
     * @param destWarehouse 收货是实体仓信息
     * @param origWarehouse 发货实体仓信息
     */
    public ValueHolderV14<SgR3BaseResult> createOutNotice(User user, ScBTransfer transfer, List<ScBTransferItem> transferItems,
                                                          CpCPhyWarehouse origWarehouse, CpCPhyWarehouse destWarehouse,
                                                          List<ScBTransferImpItem> impItems, List<ScBTransferTeusItem> boxItems) {
        if (log.isDebugEnabled()) {
            log.debug("调拨单" + transfer.getBillNo() + "开始调用新增出库通知单");
        }
        Locale locale = user.getLocale();
        SgPhyOutNoticesSaveCmd sgPhyOutNoticesSaveCmd = (SgPhyOutNoticesSaveCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext()
                , SgPhyOutNoticesSaveCmd.class.getName(), "sg", "1.0");
        SgPhyOutNoticesBillSaveRequest request = new SgPhyOutNoticesBillSaveRequest();
        request.setObjId(-1L);
        request.setLoginUser(user);
        SgPhyOutNoticesSaveRequest outNoticesRequest = new SgPhyOutNoticesSaveRequest();
        outNoticesRequest.setSourceBillId(transfer.getId());
        outNoticesRequest.setSourceBillNo(transfer.getBillNo());
        outNoticesRequest.setSourceBillType(SgConstantsIF.BILL_TYPE_TRANSFER);
        outNoticesRequest.setOutType(SgOutConstantsIF.OUT_TYPE_BIG_GOODS);
        outNoticesRequest.setCpCCustomerWarehouseId(destWarehouse.getId());
        outNoticesRequest.setCpCCsEcode(destWarehouse.getEcode());
        outNoticesRequest.setCpCCsEname(destWarehouse.getEname());
        outNoticesRequest.setCpCPhyWarehouseId(origWarehouse.getId());
        outNoticesRequest.setCpCPhyWarehouseEcode(origWarehouse.getEcode());
        outNoticesRequest.setCpCPhyWarehouseEname(origWarehouse.getEname());
        outNoticesRequest.setIsPassWms(origWarehouse.getWmsControlWarehouse());
        outNoticesRequest.setWmsStatus(SgOutConstantsIF.WMS_STATUS_WAIT_PASS);

        // 2020-05-29添加逻辑，差异调拨生成出库通知单时，"是否差异处理" 为是
        String handleWay = transfer.getHandleWay();
        if (SgTransferConstantsIF.HANDLE_WAY_FHTZ.equals(handleWay)) {
            outNoticesRequest.setIsDiffDeal(SgOutConstantsIF.IS_DIFF_DEAL_Y);
        }

        //prd修改 以下值 取单据中值
        outNoticesRequest.setReceiverName(transfer.getReceiverName());
        outNoticesRequest.setReceiverMobile(transfer.getReceiverMobile());
        outNoticesRequest.setReceiverPhone(transfer.getReceiverPhone());
        outNoticesRequest.setReceiverAddress(transfer.getReceiverAddress());
        outNoticesRequest.setReceiverZip(transfer.getReceiverZip());
        outNoticesRequest.setCpCRegionCityId(transfer.getReceiverCityId());
        outNoticesRequest.setCpCRegionCityEcode(transfer.getReceiverCityEcode());
        outNoticesRequest.setCpCRegionCityEname(transfer.getReceiverCityEname());
        outNoticesRequest.setCpCRegionAreaId(transfer.getReceiverDistrictId());
        outNoticesRequest.setCpCRegionAreaEcode(transfer.getReceiverDistrictEcode());
        outNoticesRequest.setCpCRegionAreaEname(transfer.getReceiverDistrictEname());
        outNoticesRequest.setCpCRegionProvinceId(transfer.getReceiverProvinceId());
        outNoticesRequest.setCpCRegionProvinceEcode(transfer.getReceiverProvinceEcode());
        outNoticesRequest.setCpCRegionProvinceEname(transfer.getReceiverProvinceEname());

        outNoticesRequest.setCpCLogisticsId(transfer.getCpCLogisticsId());
        outNoticesRequest.setCpCLogisticsEcode(transfer.getCpCLogisticsEcode());
        outNoticesRequest.setCpCLogisticsEname(transfer.getCpCLogisticsEname());
        outNoticesRequest.setLogisticNumber(transfer.getTransWayNo());
        outNoticesRequest.setRemark(transfer.getRemark());

        // 调拨性质
        outNoticesRequest.setSgBOutPropId(transfer.getSgBTransferPropId());
        request.setOutNoticesRequest(outNoticesRequest);

        // 箱功能开启时，传录入明细和箱内明细
        if (boxConfig.getBoxEnable()) {
            boxFunctionService.createOutNoticesBox(request, transfer, impItems, boxItems);
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + ",createOutNoticesTransfer param:" + JSON.toJSONString(request));
            }
        } else {
            List<SgPhyOutNoticesItemSaveRequest> outNoticesItemRequests = new ArrayList<>();
            transferItems.forEach(transferItem -> {
                SgPhyOutNoticesItemSaveRequest item = new SgPhyOutNoticesItemSaveRequest();
                BeanUtils.copyProperties(transferItem, item);
                item.setSourceBillItemId(transferItem.getId());
                item.setPsCSpec1Id(transferItem.getPsCClrId());
                item.setPsCSpec1Ecode(transferItem.getPsCClrEcode());
                item.setPsCSpec1Ename(transferItem.getPsCClrEname());
                item.setPsCSpec2Id(transferItem.getPsCSizeId());
                item.setPsCSpec2Ecode(transferItem.getPsCSizeEcode());
                item.setPsCSpec2Ename(transferItem.getPsCSizeEname());
                item.setId(null);
                outNoticesItemRequests.add(item);
            });
            request.setOutNoticesItemRequests(outNoticesItemRequests);
        }

        ValueHolderV14<SgR3BaseResult> holderV14 = sgPhyOutNoticesSaveCmd.saveSgPhyOutNotices(request);
        AssertUtils.cannot(holderV14.getCode() == ResultCode.FAIL, "调用出库通知单新增失败:" + holderV14.getMessage(), locale);
        return holderV14;
    }


    /**
     * 新增逻辑收货单
     *
     * @param user          用户
     * @param transfer      主表信息
     * @param transferItems 明细信息
     * @param isQtyOut      取出库数量 或者 单据数量
     */
    public void createReceiveBill(User user, ScBTransfer transfer, List<ScBTransferItem> transferItems, Boolean isQtyOut,
                                  List<ScBTransferImpItem> impItems, List<ScBTransferTeusItem> boxItems,
                                  HashMap<String, ScBTransferImpItem> impMap) {
        if (log.isDebugEnabled()) {
            log.debug("调拨单" + transfer.getBillNo() + "开始调用新增逻辑收货单");
        }
        Locale locale = user.getLocale();
        SgReceiveSaveService saveService = ApplicationContextHandle.getBean(SgReceiveSaveService.class);
        SgReceiveBillSaveRequest request = new SgReceiveBillSaveRequest();
        request.setLoginUser(user);
        request.setPreoutWarningType(SgConstantsIF.PREOUT_RESULT_ERROR);
        SgReceiveSaveRequest saveRequest = new SgReceiveSaveRequest();
        saveRequest.setRemark(transfer.getRemark());
        saveRequest.setSourceBillId(transfer.getId());
        saveRequest.setSourceBillNo(transfer.getBillNo());
        saveRequest.setSourceBillType(SgConstantsIF.BILL_TYPE_TRANSFER);
        if (isQtyOut) {
            saveRequest.setServiceNode(SgConstantsIF.SERVICE_NODE_TRANSFER_OUT);
        } else {
            saveRequest.setServiceNode(SgConstantsIF.SERVICE_NODE_TRANSFER_SUBMIT);
        }
        request.setSgReceive(saveRequest);

        List<SgReceiveItemSaveRequest> itemList = new ArrayList<>();
        Long storeId = transfer.getCpCDestId();
        String storeECode = transfer.getCpCDestEcode();
        String storeEName = transfer.getCpCDestEname();
        transferItems.forEach(transferItem -> {
            SgReceiveItemSaveRequest item = new SgReceiveItemSaveRequest();
            BeanUtils.copyProperties(transferItem, item);
            if (isQtyOut) {
                item.setQty(transferItem.getQty());
                item.setQtyPrein(transferItem.getQtyOut().subtract(transferItem.getQtyIn()));
            } else {
                item.setQty(transferItem.getQty());
                item.setQtyPrein(transferItem.getQty());
            }
            item.setId(null);
            item.setPsCSpec1Id(transferItem.getPsCClrId());
            item.setPsCSpec1Ecode(transferItem.getPsCClrEcode());
            item.setPsCSpec1Ename(transferItem.getPsCClrEname());
            item.setPsCSpec2Id(transferItem.getPsCSizeId());
            item.setPsCSpec2Ecode(transferItem.getPsCSizeEcode());
            item.setPsCSpec2Ename(transferItem.getPsCSizeEname());
            item.setSourceBillItemId(transferItem.getId());
            item.setCpCStoreId(storeId);
            item.setCpCStoreEcode(storeECode);
            item.setCpCStoreEname(storeEName);
            itemList.add(item);
        });
        request.setItemList(itemList);

        // 箱功能开启时，传录入明细和箱内明细
        if (boxConfig.getBoxEnable()) {
            boxFunctionService.createSgReceiveBox(request, transfer, impItems, boxItems, impMap);
        }
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ",createReceiveBillForTransfer param:" + JSON.toJSONString(request));
        }

        // 2019-08-19添加逻辑：调拨差异生成的调拨单传true
        String handleWay = transfer.getHandleWay();
        if (StringUtils.isNotEmpty(handleWay) && StringUtils.equals(handleWay, SgTransferConstantsIF.HANDLE_WAY_FHTZ)) {
            request.setIsNegativePrein(true);
        }
        ValueHolderV14 holderV14 = saveService.saveSgBReceive(request);
        AssertUtils.cannot(holderV14.getCode() == ResultCode.FAIL, "调用逻辑收货单新增失败:" + holderV14.getMessage(), locale);
    }


    /**
     * 清空逻辑发货单
     *
     * @param user     用户
     * @param transfer 主表信息
     */
    public void cleanSend(User user, ScBTransfer transfer, boolean isCancelOut) {
        if (log.isDebugEnabled()) {
            log.debug("调拨单" + transfer.getBillNo() + "开始调用清空逻辑发货单");
        }
        Locale locale = user.getLocale();
        SgSendCleanService cleanService = ApplicationContextHandle.getBean(SgSendCleanService.class);
        SgSendBillCleanRequest request = new SgSendBillCleanRequest();
        request.setLoginUser(user);
        request.setSourceBillId(transfer.getId());
        request.setSourceBillNo(transfer.getBillNo());
        request.setSourceBillType(SgConstantsIF.BILL_TYPE_TRANSFER);
        request.setServiceNode(SgConstantsIF.SERVICE_NODE_TRANSFER_UNSUBMIT);

        // 2020-03-04添加逻辑：pos出库单传参
        if (isCancelOut) {
            request.setIsRepeatClean(true);
            request.setRemark("调拨单[" + transfer.getBillNo() + "]取消出库！");
        }

        ValueHolderV14 holderV14 = cleanService.cleanSgBSend(request);
        com.jackrain.nea.utils.AssertUtils.cannot(holderV14.getCode() == ResultCode.FAIL, "清空逻辑发货单失败:" + holderV14.getMessage(), locale);
    }


    /**
     * 清空逻辑收货单
     *
     * @param user     用户
     * @param transfer 主表信息
     */
    public void cleanReceive(User user, ScBTransfer transfer, long serviceNode) {
        if (log.isDebugEnabled()) {
            log.debug("调拨单 " + transfer.getBillNo() + " 开始调用清空逻辑收货单");
        }
        Locale locale = user.getLocale();
        SgReceiveCleanService cleanService = ApplicationContextHandle.getBean(SgReceiveCleanService.class);
        SgReceiveBillCleanRequest request = new SgReceiveBillCleanRequest();
        request.setLoginUser(user);
        request.setServiceNode(serviceNode);
        request.setSourceBillId(transfer.getId());
        request.setSourceBillNo(transfer.getBillNo());
        request.setSourceBillType(SgConstantsIF.BILL_TYPE_TRANSFER);
        ValueHolderV14 holderV14 = cleanService.cleanSgBReceive(request);
        AssertUtils.cannot(holderV14.getCode() == ResultCode.FAIL, "清空逻辑收货单失败:" + holderV14.getMessage(), locale);
    }

    /**
     * 作废出库通知单
     *
     * @param user     用户
     * @param transfer 主表信息
     */
    public void cancelOutNotice(User user, ScBTransfer transfer, boolean isCancelOut) {
        if (log.isDebugEnabled()) {
            log.debug("调拨单" + transfer.getBillNo() + "开始调用作废出库通知单");
        }
        Locale locale = user.getLocale();
        SgPhyOutNoticesBatchVoidAndWMSCmd voidCmd = (SgPhyOutNoticesBatchVoidAndWMSCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(), SgPhyOutNoticesBatchVoidAndWMSCmd.class.getName(),
                "sg", "1.0");
        SgPhyOutNoticesBillVoidRequest request = new SgPhyOutNoticesBillVoidRequest();
        SgPhyOutNoticesSaveRequest saveRequest = new SgPhyOutNoticesSaveRequest();
        saveRequest.setSourceBillType(SgConstantsIF.BILL_TYPE_TRANSFER);
        saveRequest.setSourceBillNo(transfer.getBillNo());
        saveRequest.setSourceBillId(transfer.getId());
        List<SgPhyOutNoticesSaveRequest> noticesSaveRequests = new ArrayList<>();
        noticesSaveRequests.add(saveRequest);
        // 2020-03-03添加参数：取消出库标记
        request.setPosCancel(isCancelOut);
        request.setLoginUser(user);
        request.setNoticesSaveRequests(noticesSaveRequests);
        ValueHolderV14<SgR3BaseResult> v14 = voidCmd.batchVoidSgPhyOutNoticesAndWMS(request);
        SgR3BaseResult data = v14.getData();
        AssertUtils.cannot(v14.getCode() == ResultCode.FAIL, "作废出库通知单失败:" + v14.getMessage(), locale);
        if (data != null && CollectionUtils.isNotEmpty(data.getDataArr())) {
            JSONObject obj = data.getDataArr().getJSONObject(0);
            if (obj.getInteger(R3ParamConstants.CODE) == ResultCode.FAIL) {
                AssertUtils.logAndThrow("作废出库通知单失败:" + obj.getString(R3ParamConstants.MESSAGE), locale);
            }
        }
    }


    /**
     * 新增或者更新入库通知单
     *
     * @param user          用户
     * @param transfer      主表信息
     * @param transferItems 明细信息
     */
    public ValueHolderV14 createInNotice(User user, ScBTransfer transfer, List<ScBTransferItem> transferItems, CpCPhyWarehouse origWarehouse,
                                         CpCPhyWarehouse destWarehouse, List<SgTransferImpItemRequest> impItems, List<SgTransferBoxItemRequest> boxItems,
                                         HashMap<String, ScBTransferImpItem> impMap) {
        Locale locale = user.getLocale();
        SgPhyInNoticesSaveCmd saveCmd = (SgPhyInNoticesSaveCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                SgPhyInNoticesSaveCmd.class.getName(), SgConstantsIF.GROUP, SgConstantsIF.VERSION);
        ValueHolderV14 holderV14 = saveCmd.saveSgBPhyInNotices(builtInNoticeRequest(user, transfer, transferItems,
                origWarehouse, destWarehouse, false, impItems, boxItems, impMap));
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ",新增入库通知单出参:" + JSON.toJSONString(holderV14));
        }
        AssertUtils.cannot(holderV14.getCode() == ResultCode.FAIL, "新增或者更新入库通知单失败:" + holderV14.getMessage(), locale);
        return holderV14;
    }

    /**
     * 取消逻辑发货单
     *
     * @param transfer 调拨单信息
     * @param user     用户
     */
    public void cancelSend(User user, ScBTransfer transfer) {
        if (log.isDebugEnabled()) {
            log.debug("调拨单" + transfer.getBillNo() + "开始调用取消逻辑发货单");
        }
        Locale locale = user.getLocale();
        SgSendVoidService voidService = ApplicationContextHandle.getBean(SgSendVoidService.class);
        SgSendBillVoidRequest request = new SgSendBillVoidRequest();
        request.setLoginUser(user);
        request.setSourceBillId(transfer.getId());
        request.setSourceBillNo(transfer.getBillNo());
        request.setSourceBillType(SgConstantsIF.BILL_TYPE_TRANSFER);
        request.setIsExist(true);
        ValueHolderV14 holderV14 = voidService.voidSgBSend(request);
        AssertUtils.cannot(holderV14.getCode() == ResultCode.FAIL, "取消逻辑发货单失败:" + holderV14.getMessage(), locale);
    }


    /**
     * 取消逻辑收货单
     *
     * @param transfer 调拨单信息
     * @param user     用户
     */
    public void cancelRecive(User user, ScBTransfer transfer) {
        if (log.isDebugEnabled()) {
            log.debug("调拨单" + transfer.getBillNo() + "开始调用取消逻辑收货单");
        }
        Locale locale = user.getLocale();
        SgReceiveVoidService voidService = ApplicationContextHandle.getBean(SgReceiveVoidService.class);
        SgReceiveBillVoidRequest request = new SgReceiveBillVoidRequest();
        request.setLoginUser(user);
        request.setSourceBillId(transfer.getId());
        request.setSourceBillNo(transfer.getBillNo());
        request.setSourceBillType(SgConstantsIF.BILL_TYPE_TRANSFER);
        request.setIsExist(true);
        ValueHolderV14 holderV14 = voidService.voidSgBReceive(request);
        AssertUtils.cannot(holderV14.getCode() == ResultCode.FAIL, "取消逻辑收货单失败:" + holderV14.getMessage(), locale);
    }

    /**
     * 获取入库单信息 并对比 调拨单是否可以入库完成
     *
     * @param transfer 调拨单信息
     * @param user     用户
     */
    public boolean compareInResult(User user, ScBTransfer transfer) {
        Locale locale = user.getLocale();
        // 调用【查询入库通知单服务】
        SgPhyInNoticesSelectBySourceCmd inNoticesCmd = (SgPhyInNoticesSelectBySourceCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(),
                SgPhyInNoticesSelectBySourceCmd.class.getName(),
                SgConstantsIF.GROUP, SgConstantsIF.VERSION);

        SgPhyInNoticesSelectBySourceRequest sourceRequest = new SgPhyInNoticesSelectBySourceRequest();
        sourceRequest.setSourceBillId(transfer.getId());
        sourceRequest.setSourceBillType(SgConstantsIF.BILL_TYPE_TRANSFER);
        sourceRequest.setLoginUser(user);
        ValueHolderV14<List<SgBPhyInNoticesResult>> v14 = inNoticesCmd.selectBySource(sourceRequest);
        if (log.isDebugEnabled()) {
            log.debug("入库通知单查询返回结果:" + v14);
        }
        if (v14.getCode() == ResultCode.FAIL) {
            AssertUtils.logAndThrow(v14.getMessage(), user.getLocale());
        }
        // 是否全部入库
        Boolean isInAll = true;
        List<SgBPhyInNoticesResult> inNoticesResults = v14.getData();
        if (!org.springframework.util.CollectionUtils.isEmpty(inNoticesResults)) {
            for (SgBPhyInNoticesResult inNoticesResult : inNoticesResults) {
                Integer billStatus = inNoticesResult.getNotices().getBillStatus();
                // 是否全部入库
                if (billStatus != SgInNoticeConstants.BILL_STATUS_IN_ALL) {
                    isInAll = false;
                    break;
                }
            }
        } else {
            AssertUtils.logAndThrow("不存在对应的入库通知单，请检查！", user.getLocale());
        }
        if (isInAll) {
            ScBTransferInItemMapper inItemMapper = ApplicationContextHandle.getBean(ScBTransferInItemMapper.class);
            int totalInNum = inItemMapper.selectCount(new QueryWrapper<ScBTransferInItem>().lambda()
                    .eq(ScBTransferInItem::getScBTransferId, transfer.getId())
                    .eq(ScBTransferInItem::getIsactive, SgConstants.IS_ACTIVE_Y));
            int totalInMq = 0;
            Object o = ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                    SgPhyInResultSelectCmd.class.getName(), "sg", "1.0");
            AssertUtils.notNull(o, "未获取到bean" + SgPhyInResultSelectCmd.class.getName(), locale);
            SgPhyInResultSelectCmd selectBySourceCmd = (SgPhyInResultSelectCmd) o;
            SgBPhyInResultSaveRequest request = new SgBPhyInResultSaveRequest();
            request.setSourceBillId(transfer.getId());
            request.setSourceBillType(SgConstantsIF.BILL_TYPE_TRANSFER);

            ValueHolderV14<List<SgBPhyInResult>> holderV14 = selectBySourceCmd.selectSgPhyInResult(request);
            if (holderV14.getCode() == ResultCode.FAIL) {
                AssertUtils.logAndThrow("查询入库结果单失败!:" + holderV14.getMessage(), locale);
            }
            List<SgBPhyInResult> inResultList = holderV14.getData();
            if (CollectionUtils.isEmpty(inResultList)) {
                return false;
            } else {
                for (SgBPhyInResult inResult : inResultList) {
                    if (inResult.getIsactive().equalsIgnoreCase(SgConstants.IS_ACTIVE_Y)) {
                        totalInMq++;
                    }
                }
            }
            //出库=最后一次入库总数 且入库记录数=mq消费记录数
            if (log.isDebugEnabled()) {
                log.debug("总入库数:" + totalInNum + ",总消费数量:" + totalInMq);
            }
            return totalInNum == totalInMq;
        } else {
            return false;
        }
    }


    /**
     * 调用发货订单 更新以配已发量
     * <p>
     * 6.18 上线不上发货单内容
     */
    public void createOcSend(User user, ScBTransfer transfer, List<ScBTransferItem> transferItems, List<ScBTransferItem> inItems, int status) {

        Locale locale = user.getLocale();
        OcSendOutQtyUpdateCmd updateCmd = (OcSendOutQtyUpdateCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                OcSendOutQtyUpdateCmd.class.getName(), "oc", "1.0");
        OcSendOutQtyUpdateRequest request = new OcSendOutQtyUpdateRequest();
        request.setObjId(transfer.getOcBSendOutId());
        request.setUser(user);
        List<OcSendOutItemQtyUpdateRequest> items = new ArrayList<>();
        request.setIsLast(false);
        if (status == SgTransferBillStatusEnum.AUDITED_NOT_OUT.getVal()) {
            // 正常审核增加已配量，已发量不变
            transferItems.forEach(transferItem -> {
                OcSendOutItemQtyUpdateRequest updateRequest = new OcSendOutItemQtyUpdateRequest();
                updateRequest.setPsCSkuEcode(transferItem.getPsCSkuEcode());
                updateRequest.setQtyOccu(transferItem.getQty());
                updateRequest.setQtyConsign(BigDecimal.ZERO);
                items.add(updateRequest);
            });
        } else if (status == SgTransferBillStatusEnum.UN_AUDITED.getVal()) {
            transferItems.forEach(transferItem -> {
                OcSendOutItemQtyUpdateRequest updateRequest = new OcSendOutItemQtyUpdateRequest();
                updateRequest.setPsCSkuEcode(transferItem.getPsCSkuEcode());
                updateRequest.setQtyOccu(transferItem.getQty().negate());
                updateRequest.setQtyConsign(BigDecimal.ZERO);
                items.add(updateRequest);
            });
        } else if (status >= SgTransferBillStatusEnum.AUDITED_ALL_OUT_NOT_IN.getVal()) {
            transferItems.forEach(transferItem -> {
                // 自动出入库
                OcSendOutItemQtyUpdateRequest updateRequest = new OcSendOutItemQtyUpdateRequest();
                updateRequest.setPsCSkuEcode(transferItem.getPsCSkuEcode());
                updateRequest.setQtyOccu(BigDecimal.ZERO);
                updateRequest.setQtyConsign(transferItem.getQty());
                items.add(updateRequest);
            });
        } else {
            inItems.forEach(transferItem -> {
                OcSendOutItemQtyUpdateRequest updateRequest = new OcSendOutItemQtyUpdateRequest();
                updateRequest.setPsCSkuEcode(transferItem.getPsCSkuEcode());
                updateRequest.setQtyOccu(BigDecimal.ZERO);
                updateRequest.setQtyConsign(transferItem.getQtyOut());
                items.add(updateRequest);
            });
        }
        request.setItems(items);

        /**
         *  cs 添加 是否开启箱库存 逻辑
         * @date 2019.10.22 20:14
         *  是否开启 箱库存开关
         */
        if (boxConfig.getBoxEnable()) {
            ScBTransferBoxFunctionService scBTransferBoxFunctionService = ApplicationContextHandle.getBean(ScBTransferBoxFunctionService.class);
            List<OcSendOutImpItemQtyUpdateRequest> ocImpItemQtyList = scBTransferBoxFunctionService.getScBTransferImpItems(transfer, status);
            request.setImpItems(ocImpItemQtyList);
        }

        ValueHolderV14 holderV14 = updateCmd.updateSendOutQty(request);
        AssertUtils.cannot(holderV14.getCode() == ResultCode.FAIL, "更新发货订单已配已发量失败:" + holderV14.getMessage(), locale);
    }


    /**
     * 新增并审核出库结果单
     */
    public void createOutResult(User user, ScBTransfer transfer, List<ScBTransferItem> transferItems
            , Long outNoticeId, String outNoticeNo, HashMap outSkuMap, List<ScBTransferImpItem> impItems,
                                List<ScBTransferTeusItem> boxItems) {
        if (log.isDebugEnabled()) {
            log.debug("调拨单" + transfer.getBillNo() + "开始调用新增并审核出库结果单");
        }
        Locale locale = user.getLocale();
        AssertUtils.notNull(transfer, "调拨单不存在!", locale);
        AssertUtils.notNull(outSkuMap, "出库通通知返回的明细不存在!", locale);
        if (log.isDebugEnabled()) {
            log.debug("出库通知明细:" + outSkuMap.toString());
        }
        AssertUtils.cannot(CollectionUtils.isEmpty(transferItems), "明细不能为空", locale);
        SgPhyOutResultSaveAndAuditCmd saveAndAuditCmd = (SgPhyOutResultSaveAndAuditCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                SgPhyOutResultSaveAndAuditCmd.class.getName(), "sg", "1.0");
        Long origStoreId = transfer.getCpCOrigId();
        Long destStoreId = transfer.getCpCDestId();
        AssertUtils.notNull(origStoreId, "数据异常-发货店仓id不存在", locale);
        AssertUtils.notNull(destStoreId, "数据异常-收货店仓id不存在", locale);
        CpCPhyWarehouseMapper warehouseMapper = ApplicationContextHandle.getBean(CpCPhyWarehouseMapper.class);
        CpCPhyWarehouse origWarehouse = warehouseMapper.selectByStoreId(origStoreId);
        CpCPhyWarehouse destWarehouse = warehouseMapper.selectByStoreId(destStoreId);
        AssertUtils.notNull(origWarehouse, "数据异常-发货实体仓不存在", locale);
        AssertUtils.notNull(destWarehouse, "数据异常-收货实体仓不存在", locale);
        SgPhyOutResultBillSaveRequest outRequest = new SgPhyOutResultBillSaveRequest();
        outRequest.setLoginUser(user);
        outRequest.setObjId(-1L);
        SgPhyOutResultSaveRequest outResultRequest = new SgPhyOutResultSaveRequest();

        outResultRequest.setId(-1L);
        outResultRequest.setSgBPhyOutNoticesId(outNoticeId);
        outResultRequest.setSgBPhyOutNoticesBillno(outNoticeNo);
        outResultRequest.setSourceBillId(transfer.getId());
        outResultRequest.setSourceBillNo(transfer.getBillNo());
        outResultRequest.setSourceBillType(SgConstantsIF.BILL_TYPE_TRANSFER);
        outResultRequest.setCpCPhyWarehouseId(origWarehouse.getId());
        outResultRequest.setCpCPhyWarehouseEcode(origWarehouse.getEcode());
        outResultRequest.setCpCPhyWarehouseEname(origWarehouse.getEname());
        outResultRequest.setCpCCustomerWarehouseId(destWarehouse.getId());
        outResultRequest.setCpCCsEname(destWarehouse.getEname());
        outResultRequest.setCpCCsEcode(destWarehouse.getEcode());
        outResultRequest.setIsLast(SgOutConstantsIF.OUT_IS_LAST_Y);
        outRequest.setOutResultRequest(outResultRequest);

        // 箱功能开启时，传录入明细和箱内明细
        if (boxConfig.getBoxEnable()) {
            boxFunctionService.createAndAuditOutResultBox(outRequest, transfer, impItems, boxItems);
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + ",createAndAuditOutResultBoxTransfer param:"
                        + JSON.toJSONString(outRequest));
            }
        } else {
            List<SgPhyOutResultItemSaveRequest> outResultItemRequests = new ArrayList<>();
            transferItems.forEach(transferItem -> {
                SgPhyOutResultItemSaveRequest resultItemSaveRequest = new SgPhyOutResultItemSaveRequest();
                BeanUtils.copyProperties(transferItem, resultItemSaveRequest);
                resultItemSaveRequest.setId(-1L);
                resultItemSaveRequest.setPsCSpec1Id(transferItem.getPsCClrId());
                resultItemSaveRequest.setPsCSpec1Ecode(transferItem.getPsCClrEcode());
                resultItemSaveRequest.setPsCSpec1Ename(transferItem.getPsCClrEname());
                resultItemSaveRequest.setPsCSpec2Id(transferItem.getPsCSizeId());
                resultItemSaveRequest.setPsCSpec2Ecode(transferItem.getPsCSizeEcode());
                resultItemSaveRequest.setPsCSpec2Ename(transferItem.getPsCSizeEname());
                resultItemSaveRequest.setSgBPhyOutNoticesItemId((Long) outSkuMap.get(transferItem.getPsCSkuEcode()));
                outResultItemRequests.add(resultItemSaveRequest);
            });
            outRequest.setOutResultItemRequests(outResultItemRequests);
        }

        ValueHolderV14 holderV14 = saveAndAuditCmd.saveOutResultAndAudit(outRequest);
        AssertUtils.cannot(holderV14.getCode() == ResultCode.FAIL, "新增并审核出库结果单失败:" + holderV14.getMessage(), locale);
    }


    /**
     * 新增入库通知单同时新增新增并审核入库结果单
     */
    public void autoIn(User user, ScBTransfer transfer, List<ScBTransferItem> transferItems, CpCPhyWarehouse origWarehouse,
                       CpCPhyWarehouse destWarehouse, List<SgTransferImpItemRequest> impItems, List<SgTransferBoxItemRequest> boxItems,
                       HashMap<String, ScBTransferImpItem> impMap) {
        SgPhyInNoticesAndResultSaveCmd saveCmd = (SgPhyInNoticesAndResultSaveCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                SgPhyInNoticesAndResultSaveCmd.class.getName(), SgConstantsIF.GROUP, SgConstantsIF.VERSION);
        ValueHolderV14 holderV14 = saveCmd.saveSgBPhyInNoticesAndResult(builtInNoticeRequest(user, transfer, transferItems,
                origWarehouse, destWarehouse, true, impItems, boxItems, impMap));
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ",autoIn,holderV14:" + JSON.toJSONString(holderV14));
        }
        AssertUtils.cannot(holderV14.getCode() == ResultCode.FAIL, "新增入库通知单失败:" + holderV14.getMessage(), user.getLocale());
    }


    /**
     * 构建入库通知单需要的 参数
     */
    private List<SgPhyInNoticesBillSaveRequest> builtInNoticeRequest(User user, ScBTransfer transfer, List<ScBTransferItem> transferItems,
                                                                     CpCPhyWarehouse origWarehouse, CpCPhyWarehouse destWarehouse, boolean isAutoIn,
                                                                     List<SgTransferImpItemRequest> impItems, List<SgTransferBoxItemRequest> boxItems,
                                                                     HashMap<String, ScBTransferImpItem> impMap) {
        if (log.isDebugEnabled()) {
            log.debug("调拨单" + transfer.getBillNo() + "开始调用新增入库通知单");
        }
        List<SgPhyInNoticesBillSaveRequest> requestList = new ArrayList<>();
        SgPhyInNoticesBillSaveRequest saveRequest = new SgPhyInNoticesBillSaveRequest();
        saveRequest.setLoginUser(user);
        SgPhyInNoticesRequest notices = new SgPhyInNoticesRequest();
        notices.setCpCPhyWarehouseId(destWarehouse.getId());
        notices.setCpCPhyWarehouseEcode(destWarehouse.getEcode());
        notices.setCpCPhyWarehouseEname(destWarehouse.getEname());
        notices.setCpCCsEcode(origWarehouse.getEcode());
        notices.setCpCCsEname(origWarehouse.getEname());
        notices.setCpCCustomerWarehouseId(origWarehouse.getId());
        notices.setInType(SgInConstants.IN_TYPE_BIG_CARGO);
        notices.setSourceBillType(SgConstantsIF.BILL_TYPE_TRANSFER);
        notices.setSourceBillNo(transfer.getBillNo());
        notices.setSourceBillId(transfer.getId());
        notices.setIsPassWms(destWarehouse.getWmsControlWarehouse());
        notices.setWmsStatus(0L);
        notices.setRemark(transfer.getRemark());
        //prd删除 调了这些传输信息
        notices.setCpCLogisticsId(transfer.getCpCLogisticsId());
        notices.setCpCLogisticsEcode(transfer.getCpCLogisticsEcode());
        notices.setCpCLogisticsEname(transfer.getCpCLogisticsEname());
        notices.setLogisticNumber(transfer.getTransWayNo());
        saveRequest.setNotices(notices);

        String handleWay = transfer.getHandleWay();
        if (StringUtils.isNotEmpty(handleWay) && StringUtils.equals(handleWay, SgTransferConstantsIF.HANDLE_WAY_FHTZ)) {
            notices.setReserveBigint01(1L);
        }

        // 箱功能开启时传录入明细和箱内明细过去
        if (boxConfig.getBoxEnable()) {

            boxFunctionService.createInNoticesBox(saveRequest, transfer, isAutoIn, impItems, boxItems, impMap);
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + ",saveInNoticesBoxForTransfer param:" +
                        JSON.toJSONString(saveRequest));
            }
        } else {
            List<SgPhyInNoticesItemRequest> itemList = new ArrayList<>();
            transferItems.forEach(transferItem -> {
                SgPhyInNoticesItemRequest item = new SgPhyInNoticesItemRequest();
                BeanUtils.copyProperties(transferItem, item);
                item.setId(-1L);

                item.setQty(transferItem.getQtyOut());
                if (isAutoIn) {
                    if (StringUtils.isNotEmpty(handleWay) &&
                            StringUtils.equals(handleWay, SgTransferConstantsIF.HANDLE_WAY_FHTZ)) {
                        item.setQtyIn(BigDecimal.ZERO);
                    } else {
                        item.setQtyIn(transferItem.getQtyOut());
                    }
                } else {
                    item.setQtyIn(null);
                }
                item.setPsCSpec1Id(transferItem.getPsCClrId());
                item.setPsCSpec1Ecode(transferItem.getPsCClrEcode());
                item.setPsCSpec1Ename(transferItem.getPsCClrEname());
                item.setPsCSpec2Id(transferItem.getPsCSizeId());
                item.setPsCSpec2Ecode(transferItem.getPsCSizeEcode());
                item.setPsCSpec2Ename(transferItem.getPsCSizeEname());
                item.setSourceBillItemId(transferItem.getId());
                itemList.add(item);
            });
            saveRequest.setItemList(itemList);
        }

        requestList.add(saveRequest);
        return requestList;
    }


    /**
     * 逻辑发货单发货
     *
     * @param transfer 调拨单
     * @param items    明细
     * @param user     用户
     */
    public void sendOut(ScBTransfer transfer, List<ScBTransferItem> items, User user, List<ScBTransferImpItem> impItems,
                        List<ScBTransferTeusItem> boxItems) {
        SgSendSubmitService service = ApplicationContextHandle.getBean(SgSendSubmitService.class);
        SgSendBillSubmitRequest request = new SgSendBillSubmitRequest();
        request.setLoginUser(user);
        request.setSourceBillId(transfer.getId());
        request.setSourceBillNo(transfer.getBillNo());
        request.setSourceBillType(SgConstantsIF.BILL_TYPE_TRANSFER);

        // 2019-08-19添加逻辑：调拨差异生成的调拨单传true
        String handleWay = transfer.getHandleWay();
        Integer transferType = transfer.getTransferType();
        if ((StringUtils.isNotEmpty(handleWay) && StringUtils.equals(handleWay, SgTransferConstantsIF.HANDLE_WAY_FHTZ))
                || (transferType != null && transferType == SgTransferConstantsIF.TRANSFER_TYPE_DEFICIENCY)) {
            request.setIsNegativePreout(true);
        }

        SgBPhyOutResult phyOutResult = new SgBPhyOutResult();
        phyOutResult.setTotQtyOut(transfer.getTotQty());
        phyOutResult.setIsLast(SgSendConstantsIF.IS_LASTSEND_Y);
        request.setPhyOutResult(phyOutResult);

        List<SgBPhyOutResultItem> phyOutResultItems = new ArrayList<>();
        items.forEach(transferItem -> {
            SgBPhyOutResultItem item = new SgBPhyOutResultItem();
            item.setPsCSpec1Id(transferItem.getPsCClrId());
            item.setPsCSpec1Ecode(transferItem.getPsCClrEcode());
            item.setPsCSpec1Ename(transferItem.getPsCClrEname());
            item.setPsCSpec2Id(transferItem.getPsCSizeId());
            item.setPsCSpec2Ecode(transferItem.getPsCSizeEcode());
            item.setPsCSpec2Ename(transferItem.getPsCSizeEname());
            item.setPriceList(transferItem.getPriceList());
            item.setPsCProId(transferItem.getPsCProId());
            item.setPsCProEcode(transferItem.getPsCProEcode());
            item.setPsCProEname(transferItem.getPsCProEname());
            item.setPsCSkuId(transferItem.getPsCSkuId());
            item.setPsCSkuEcode(transferItem.getPsCSkuEcode());
            item.setQty(transferItem.getQty());
            phyOutResultItems.add(item);
        });
        request.setPhyOutResultItems(phyOutResultItems);

        // 箱功能开启时，传录入明细和箱内明细
        if (boxConfig.getBoxEnable()) {
            boxFunctionService.createSgSendOutBox(request, transfer, impItems, boxItems);
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + ",createSgSendOutBox param:" + JSON.toJSONString(request));
            }
        }

        ValueHolderV14 holderV14 = service.submitSgBSend(request);
        AssertUtils.cannot(holderV14.getCode() == ResultCode.FAIL, "逻辑发货单-发货失败:" + holderV14.getMessage(), user.getLocale());
    }


    /**
     * 逻辑收货单 收货
     *
     * @param transfer 调拨单
     * @param items    明细
     * @param user     用户
     */
    public void receiveIn(ScBTransfer transfer, List<ScBTransferItem> items, User user,
                          List<ScBTransferImpItem> impItems, List<ScBTransferTeusItem> boxItems) {
        SgReceiveSubmitService service = ApplicationContextHandle.getBean(SgReceiveSubmitService.class);
        SgReceiveBillSubmitRequest request = new SgReceiveBillSubmitRequest();
        SgBPhyInResult phyInResult = new SgBPhyInResult();
        request.setLoginUser(user);
        request.setSourceBillId(transfer.getId());
        request.setSourceBillNo(transfer.getBillNo());
        request.setSourceBillType(SgConstantsIF.BILL_TYPE_TRANSFER);
        phyInResult.setTotQtyIn(transfer.getTotQty());
        phyInResult.setIsLast(SgSendConstantsIF.IS_LASTSEND_Y);
        request.setPhyInResult(phyInResult);

        List<SgBPhyInResultItem> phyInResultItems = new ArrayList<>();
        items.forEach(transferItem -> {
            SgBPhyInResultItem item = new SgBPhyInResultItem();
            item.setPsCSpec1Id(transferItem.getPsCClrId());
            item.setPsCSpec1Ecode(transferItem.getPsCClrEcode());
            item.setPsCSpec1Ename(transferItem.getPsCClrEname());
            item.setPsCSpec2Id(transferItem.getPsCSizeId());
            item.setPsCSpec2Ecode(transferItem.getPsCSizeEcode());
            item.setPsCSpec2Ename(transferItem.getPsCSizeEname());
            item.setPriceList(transferItem.getPriceList());
            item.setPsCProId(transferItem.getPsCProId());
            item.setPsCProEcode(transferItem.getPsCProEcode());
            item.setPsCProEname(transferItem.getPsCProEname());
            item.setPsCSkuId(transferItem.getPsCSkuId());
            item.setPsCSkuEcode(transferItem.getPsCSkuEcode());
            item.setQtyIn(transferItem.getQty());
            phyInResultItems.add(item);
        });
        request.setPhyInResultItems(phyInResultItems);

        // 箱功能开启时，传录入明细和箱内明细
        if (boxConfig.getBoxEnable()) {
            boxFunctionService.createSgReceiveInBox(request, transfer, impItems, boxItems);
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + ",receiveInForTransfer param:" + JSON.toJSONString(request));
            }
        }

        // 2019-08-19添加逻辑：调拨差异生成的调拨单传true
        String handleWay = transfer.getHandleWay();
        if (StringUtils.isNotEmpty(handleWay) && StringUtils.equals(handleWay, SgTransferConstantsIF.HANDLE_WAY_FHTZ)) {
            request.setIsNegativePrein(true);
        }
        ValueHolderV14 holderV14 = service.submitSgBReceive(request);
        AssertUtils.cannot(holderV14.getCode() == ResultCode.FAIL, "逻辑收货单-收货失败:" + holderV14.getMessage(), user.getLocale());
    }


    /**
     * 取消审核 校验是否存在出库结果单
     *
     * @param transfer 调拨单
     * @param user     用户
     */
    public void checkOutResult(ScBTransfer transfer, User user) {
        SgPhyOutQueryCmd queryCmd = (SgPhyOutQueryCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                SgPhyOutQueryCmd.class.getName(), SgConstantsIF.GROUP, SgConstantsIF.VERSION);

        SgPhyOutBillQueryRequest request = new SgPhyOutBillQueryRequest();
        List<SgPhyOutBillBaseRequest> baseRequests = new ArrayList<>();
        SgPhyOutBillBaseRequest sgPhyOutBillBaseRequest = new SgPhyOutBillBaseRequest();
        sgPhyOutBillBaseRequest.setSourceBillId(transfer.getId());
        sgPhyOutBillBaseRequest.setSourceBillNo(transfer.getBillNo());
        sgPhyOutBillBaseRequest.setSourceBillType(SgConstantsIF.BILL_TYPE_TRANSFER);
        baseRequests.add(sgPhyOutBillBaseRequest);
        request.setReturnData(SgOutConstantsIF.RETURN_OUT_RESULT);
        request.setIsReturnItem(false);
        request.setBaseRequests(baseRequests);
        ValueHolderV14<List<SgOutQueryResult>> holderV14 = queryCmd.queryOutBySource(request);
        AssertUtils.cannot(holderV14.getCode() == ResultCode.FAIL, "查询失败:" + holderV14.getMessage(), user.getLocale());
        AssertUtils.cannot(CollectionUtils.isNotEmpty(holderV14.getData()), "当前调拨单已出库，不允许取消审核", user.getLocale());
    }

    /**
     * 更新调拨单生成结算日期发送mq失败
     *
     * @param id       调拨单ID
     * @param billType 出or入
     */
    public int updateTransfer(Long id, int billType, boolean isSuccess) {
        ScBTransferMapper transferMapper = ApplicationContextHandle.getBean(ScBTransferMapper.class);
        ScBTransfer updateTransfer = new ScBTransfer();
        updateTransfer.setId(id);

        if (billType == SourceBillType.TRANSFER_OUT.getValue() && isSuccess) {
            updateTransfer.setIsOutSettleLog(ScTransferConstants.SEND_MQ_SUCCESS);
        } else if (billType == SourceBillType.TRANSFER_OUT.getValue() && !isSuccess) {
            updateTransfer.setIsOutSettleLog(ScTransferConstants.SEND_MQ_FAIL);
        } else if (billType == SourceBillType.TRANSFER_IN.getValue() && isSuccess) {
            updateTransfer.setIsInSettleLog(ScTransferConstants.SEND_MQ_SUCCESS);
        } else if (billType == SourceBillType.TRANSFER_IN.getValue() && !isSuccess) {
            updateTransfer.setIsInSettleLog(ScTransferConstants.SEND_MQ_FAIL);
        }

        return transferMapper.updateById(updateTransfer);
    }

    /**
     * @param user          用户信息
     * @param scBTransfer   调拨单
     * @param transferItems 明细
     * @param billType      结算类型
     * @param isSubmit      是审核?
     * @param isFill        是补发?
     * @return 日志发送结果
     */
    public boolean createSettleLogForTransfer(User user, ScBTransfer scBTransfer, List<ScBTransferItem> transferItems,
                                              SourceBillType billType, Boolean isSubmit, Boolean isFill) {
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ",createSettleLogForTransfer billType:" + billType);
        }
        Boolean isSendFail = false;
        SgTransferBillGeneraUtil generaUtil = ApplicationContextHandle.getBean(SgTransferBillGeneraUtil.class);
        try {
            AcPushAcBillRequest billRequest = new AcPushAcBillRequest();
            billRequest.setBillType(billType);
            billRequest.setSourceBillNo(scBTransfer.getBillNo());
            if (billType.getValue() == SourceBillType.TRANSFER_OUT.getValue()) {
                billRequest.setSourceBillDate(scBTransfer.getOutDate());
            } else if (billType.getValue() == SourceBillType.TRANSFER_IN.getValue()) {
                billRequest.setSourceBillDate(scBTransfer.getInDate());
            }
            JSONObject bill = new JSONObject();
            bill.put(SgConstants.SG_B_TRANSFER.toUpperCase(), scBTransfer);

            BasicPsQueryService queryService = ApplicationContextHandle.getBean(BasicPsQueryService.class);
            ProInfoQueryRequest queryRequest = new ProInfoQueryRequest();

            List<String> proCodeList = transferItems.stream().map(ScBTransferItem::getPsCProEcode).collect(Collectors.toList());
            List<PsCProResult> proInfoList = new ArrayList<>();

            int itemNum = proCodeList.size();
            int pageSize = SgConstants.SG_COMMON_INSERT_PAGE_SIZE;
            int page = itemNum / pageSize;
            if (itemNum % pageSize != 0) page++;
            for (int i = 0; i < page; i++) {
                int startIndex = i * pageSize;
                int endIndex = (i + 1) * pageSize;
                List<String> proCodes = proCodeList.subList(startIndex, endIndex < itemNum ? endIndex : itemNum);
                queryRequest.setProEcodeList(proCodes);
                proInfoList.addAll(queryService.getLittleProInfo(queryRequest));
            }
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + ",createSettleLogForTransfer,proInfoList:" + proInfoList);
            }

            if (CollectionUtils.isEmpty(proInfoList)) {
                log.debug(this.getClass().getName() + ",createSettleLogForTransfer,商品信息查询为空！");
            }

            HashMap<String, PsCProResult> proMap = new HashMap<>();
            proInfoList.forEach(proInfo -> {
                proMap.put(proInfo.getEcode(), proInfo);
            });

            List<AcTransferItemRequest> itemRequests = JSONObject.parseArray(
                    JSONObject.toJSONString(transferItems), AcTransferItemRequest.class);
            itemRequests.forEach(outItem -> {
                if (isSubmit) {
                    outItem.setQtyIn(outItem.getQty());
                    outItem.setQtyOut(outItem.getQty());
                }
                outItem.setProLabelJson(proMap.get(outItem.getPsCProEcode()));
            });

            bill.put(SgConstants.SG_B_TRANSFER_ITEM.toUpperCase(), itemRequests);
            billRequest.setBill(bill);
            billRequest.setUser(user);

            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + ",调拨单生成结算日志入参:" + JSONObject.toJSONString(billRequest));
            }
            ValueHolderV14 v14 = AcScOrigBillUtil.pushAcBill(billRequest);
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + ",调拨单生成结算日志出参:" + v14);
            }
            if (v14 != null && v14.getCode() == ResultCode.SUCCESS) {
                int result = generaUtil.updateTransfer(scBTransfer.getId(), billType.getValue(), true);
                if (log.isDebugEnabled()) {
                    log.debug(this.getClass().getName() + ",createSettleLogForTransfer,success,result:" + result);
                }
            } else {
                isSendFail = true;
            }

        } catch (Exception e) {
            isSendFail = true;
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + ",调拨单生成结算日志异常:" + e.getMessage());
            }
        }

        if (isSendFail && !isFill) {
            int result = generaUtil.updateTransfer(scBTransfer.getId(), billType.getValue(), false);
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + ",createSettleLogForTransfer,fail,result:" + result);
            }
        }
        return !isSendFail;
    }


    /**
     * 调拨差异 生成团购单
     *
     * @param user         用户
     * @param transferDiff 调拨差异主表
     * @param itemList     调拨差异明细
     */
    public void genRetail(@NotNull User user, ScBTransferDiff transferDiff, List<ScBTransferDiffItem> itemList) {
        if (log.isDebugEnabled()) {
            log.debug(String.format("调拨差异单 %s 开始调用 零售", transferDiff.getBillNo()));
        }
        Locale locale = user.getLocale();
        HashMap requestMap = buildRetailRequest(user, transferDiff, itemList);
        String body = JSONObject.toJSONString(requestMap.get("param"));
        SgTransferMqConfig config = ApplicationContextHandle.getBean(SgTransferMqConfig.class);
        R3MqSendHelper sendHelper = ApplicationContextHandle.getBean(R3MqSendHelper.class);
        try {
            String msgId = sendHelper.sendMessage("default", body, config.getRetailTopic(), "drp_to_retail_notify", UUID.randomUUID().toString());
            if (log.isDebugEnabled()) {
                log.debug(String.format("调拨差异单 %s 调用零售消息发送成功 ! ,消息id %s", transferDiff.getBillNo(), msgId));
            }
        } catch (Exception e) {
            AssertUtils.logAndThrow(ExceptionUtil.getMessage(e), locale);
        }
    }

    /**
     * 构造 零售入参
     * <p>
     * <p>
     * {
     * "username": "3300201",
     * "data": [
     * {
     * "retail": {
     * "BILL_DATE": "2019-11-18",
     * "RETAIL_TYPE": 0,
     * "CP_C_STORE_ECODE": "AB001",
     * "CP_C_STORE_ID": 7,
     * "CP_C_STORE_ENAME": "测试2店"
     * },
     * "retail_item": [
     * {
     * "PS_C_PRO_ECODE": "989416166",
     * "PS_C_CLR_ECODE": "0100",
     * "PS_C_SIZE_ENAME": "6.5",
     * "PS_C_PRO_ID": 399,
     * "PS_C_CLR_ID": 3785,
     * "PS_C_PRO_ENAME": "经典鞋",
     * "PS_C_SKU_ID": 1,
     * "PS_C_CLR_ENAME": "白色",
     * "CP_C_SALER_ENUMNO": "00001",
     * "PS_C_SKU_ECODE": "989416166001001",
     * "PS_C_SIZE_ID": 3491,
     * "SALE_TYPE": "0",
     * "RETAIL_PRICE": 439,
     * "QTY_BILL": 1
     * }
     * ],
     * "reatilpay_item": [
     * {
     * "PAY_WAY_ID": 4,
     * "PAY_WAY_NAME": "现金",
     * "PAY_AMT": 439,
     * "PAY_WAY_CODE": "1",
     * "PAY_TYPE": "CASH"
     * }
     * ]
     * }
     * ]
     * }
     *
     * @param user         用户
     * @param transferDiff 差异单主表
     * @param itemList     差异单明细
     * @return request
     */
    private HashMap buildRetailRequest(User user, ScBTransferDiff transferDiff, List<ScBTransferDiffItem> itemList) {
        HashMap<String, Object> map = new HashMap<>(16);
        JSONObject param = new JSONObject();
        map.put("param", param);
        param.put("username", user.getName());
        param.put("command", "/api/retail/pos/v1/uploadRetail");
        JSONArray dataList = new JSONArray();
        JSONObject retail = buildRetail(transferDiff);
        JSONArray retailItem = new JSONArray();
        BigDecimal payAmt = BigDecimal.ZERO;
        for (ScBTransferDiffItem diffItem : itemList) {
            BigDecimal qtyDiff = diffItem.getQtyDiff();
            BigDecimal priceList = diffItem.getPriceList();
            AssertUtils.notNull(qtyDiff, "明细数量为空");
            AssertUtils.notNull(priceList, "明细吊牌价为空");
            payAmt = payAmt.add(qtyDiff.multiply(priceList));
            retailItem.add(buildRetailItem(diffItem));
        }
        JSONArray retailPayItem = new JSONArray();
        retailPayItem.add(buildRetailPayItem(payAmt));
        JSONObject data = new JSONObject();
        retail.put("SUM_AMT_ACTUAL", payAmt);
        data.put("retail", retail);
        data.put("retail_item", retailItem);
        data.put("reatilpay_item", retailPayItem);
        dataList.add(data);
        param.put("data", dataList);
        return map;
    }


    private JSONObject buildRetail(ScBTransferDiff diff) {
        JSONObject retailJo = new JSONObject();
        retailJo.put("BILL_DATE", DateUtil.format(new Date(), "yyyy-MM-dd"));
        //正常零售 0
        retailJo.put("RETAIL_TYPE", 0);
        //写死.2
        retailJo.put("BILL_ADD_TYPE", 2);
        retailJo.put("CP_C_STORE_ECODE", diff.getCpCStoreEcodeSend());
        retailJo.put("CP_C_STORE_ENAME", diff.getCpCStoreEnameSend());
        retailJo.put("CP_C_STORE_ID", diff.getCpCStoreIdSend());
        retailJo.put("REMARK", String.format("由调拨单：%s 差异处理生成!", diff.getBillNo()));
        return retailJo;
    }

    /**
     * 构建零售 明细
     *
     * @param diffItem 调拨差异明细
     * @return 零售明细
     */
    private JSONObject buildRetailItem(ScBTransferDiffItem diffItem) {
        JSONObject retailItem = new JSONObject();
        retailItem.put("PS_C_PRO_ID", diffItem.getPsCProId());
        retailItem.put("PS_C_PRO_ECODE", diffItem.getPsCProEcode());
        retailItem.put("PS_C_PRO_ENAME", diffItem.getPsCProEname());

        retailItem.put("PS_C_SKU_ECODE", diffItem.getPsCSkuEcode());
        retailItem.put("PS_C_SKU_ID", diffItem.getPsCSkuId());

        retailItem.put("PS_C_CLR_ID", diffItem.getPsCClrId());
        retailItem.put("PS_C_CLR_ECODE", diffItem.getPsCClrEcode());
        retailItem.put("PS_C_CLR_ENAME", diffItem.getPsCClrEname());

        retailItem.put("PS_C_SIZE_ID", diffItem.getPsCSizeId());
        retailItem.put("PS_C_SIZE_ECODE", diffItem.getPsCSizeEcode());
        retailItem.put("PS_C_SIZE_ENAME", diffItem.getPsCSizeEname());
        BigDecimal qtyDiff = diffItem.getQtyDiff();
        BigDecimal priceList = diffItem.getPriceList();
        retailItem.put("QTY_BILL", qtyDiff);
        retailItem.put("SALE_TYPE", 0);
        retailItem.put("RETAIL_PRICE", priceList);
        retailItem.put("PRICE_LIST", priceList);
        return retailItem;
    }

    private JSONObject buildRetailPayItem(BigDecimal payAmt) {
        JSONObject payItem = new JSONObject();
        payItem.put("PAY_WAY_ID", 4);
        payItem.put("PAY_TYPE", "CASH");
        payItem.put("PAY_AMT", payAmt);
        return payItem;
    }
}
