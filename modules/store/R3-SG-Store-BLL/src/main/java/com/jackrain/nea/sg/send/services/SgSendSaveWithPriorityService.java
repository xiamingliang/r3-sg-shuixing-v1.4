package com.jackrain.nea.sg.send.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.model.util.ModelUtil;
import com.jackrain.nea.oc.basic.common.OcBasicConstants;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.config.SgStorageBoxConfig;
import com.jackrain.nea.sg.basic.model.request.SgStoreWithPrioritySearchItemRequest;
import com.jackrain.nea.sg.basic.model.result.SgStorageOutStockResult;
import com.jackrain.nea.sg.basic.model.result.SgStorageUpdateResult;
import com.jackrain.nea.sg.basic.model.result.SgStoreWithPrioritySearchItemResult;
import com.jackrain.nea.sg.basic.model.result.SgStoreWithPrioritySearchResult;
import com.jackrain.nea.sg.basic.model.table.CpCStore;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.oms.api.SgStorageOmsQueryCmd;
import com.jackrain.nea.sg.oms.model.request.SgStoreWithPrioritySearchRequest;
import com.jackrain.nea.sg.send.common.SgSendConstants;
import com.jackrain.nea.sg.send.common.SgSendConstantsIF;
import com.jackrain.nea.sg.send.mapper.SgBSendItemMapper;
import com.jackrain.nea.sg.send.mapper.SgBSendMapper;
import com.jackrain.nea.sg.send.model.request.SgSendBillSaveRequest;
import com.jackrain.nea.sg.send.model.request.SgSendImpItemSaveRequest;
import com.jackrain.nea.sg.send.model.request.SgSendItemSaveRequest;
import com.jackrain.nea.sg.send.model.request.SgSendSaveWithPriorityRequest;
import com.jackrain.nea.sg.send.model.result.SgSendBillSaveResult;
import com.jackrain.nea.sg.send.model.result.SgSendSaveWithPriorityResult;
import com.jackrain.nea.sg.send.model.table.SgBSend;
import com.jackrain.nea.sg.send.model.table.SgBSendItem;
import com.jackrain.nea.sg.send.utils.SgStoreESUtils;
import com.jackrain.nea.sg.send.utils.SgStoreUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author 舒威
 * @since 2019/4/25
 * create at : 2019/4/25 13:37
 */
@Slf4j
@Component
public class SgSendSaveWithPriorityService {

    @Autowired
    private SgSendSaveService sendSaveService;

    @Autowired
    private SgBSendMapper sendMapper;

    @Autowired
    private SgBSendItemMapper sendItemMapper;

    @Autowired
    private SgStoreESUtils storeESUtils;

    @Autowired
    private SgStorageBoxConfig storageBoxConfig;

    public ValueHolderV14<SgSendSaveWithPriorityResult> searchLogic(SgSendSaveWithPriorityRequest request) {
        ValueHolderV14<SgSendSaveWithPriorityResult> v14 = new ValueHolderV14<>();
        long start = System.currentTimeMillis();
        try {
            SgSendSaveWithPriorityService sgSendSaveWithPriorityService =
                    ApplicationContextHandle.getBean(SgSendSaveWithPriorityService.class);
            v14 = sgSendSaveWithPriorityService.searchLogicWithTrans(request);
        } catch (Exception e) {
            e.printStackTrace();
            v14.setCode(ResultCode.FAIL);
            v14.setMessage("erorr -> " + e.getMessage());
        }
        if (log.isDebugEnabled()) {
            log.debug("来源单据id[" + request.getSgSend().getSourceBillId()
                    + "],寻源逻辑仓占单服务出参:{},寻源占单总耗时:{}ms;", JSONObject.toJSONString(v14), System.currentTimeMillis() - start);
        }
        return v14;
    }

    public ValueHolderV14<SgSendSaveWithPriorityResult> searchLogicWithTrans(SgSendSaveWithPriorityRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("来源单据id[" + request.getSgSend().getSourceBillId()
                    + "],寻源逻辑仓占单服务入参:" + JSONObject.toJSONString(request) + ";");
        }

        ValueHolderV14<SgSendSaveWithPriorityResult> v14 = new ValueHolderV14<>();
        SgSendSaveWithPriorityResult logicSearchResult = new SgSendSaveWithPriorityResult();
        checkParams(request);

        User user = request.getLoginUser();
        Boolean isSpecChange = request.getIsSpecChange();

        long start = System.currentTimeMillis();
        SgStorageOmsQueryCmd sgStorageOmsQueryCmd = (SgStorageOmsQueryCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(),
                SgStorageOmsQueryCmd.class.getName(),
                "sg", "1.0");

        SgStoreWithPrioritySearchRequest omsRequest = new SgStoreWithPrioritySearchRequest();
        List<SgStoreWithPrioritySearchItemRequest> itemList = new ArrayList<>();
        request.getItemList().forEach(item -> {
            SgStoreWithPrioritySearchItemRequest itemRequest = new SgStoreWithPrioritySearchItemRequest();
            itemRequest.setQtyChange(item.getQty());
            itemRequest.setPsCSkuId(item.getPsCSkuId());
            itemRequest.setSourceItemId(item.getSourceBillItemId());
            itemRequest.setPriorityList(request.getPriorityList());
            itemList.add(itemRequest);
        });
        omsRequest.setTable(request.getTable());
        omsRequest.setLoginUser(user);
        omsRequest.setBillNo(request.getSgSend().getSourceBillNo());
        omsRequest.setItemList(itemList);
        omsRequest.setPriorityList(request.getPriorityList());
        try {
             /*TODO 开始逻辑仓寻仓生成占用库存信息*/
            if (log.isDebugEnabled()) {
                log.debug("来源单据id[" + request.getSgSend().getSourceBillId() + "],寻源分仓入参:" + JSONObject.toJSONString(omsRequest));
            }
            ValueHolderV14<SgStoreWithPrioritySearchResult> callAPIResult =
                    sgStorageOmsQueryCmd.searchSgStoreWithPriority(omsRequest);
            if (log.isDebugEnabled()) {
                log.debug("来源单据id[" + request.getSgSend().getSourceBillId()
                        + "],寻源分仓出参:{},寻源分仓耗时:{}ms;", JSONObject.toJSONString(callAPIResult), System.currentTimeMillis() - start);
            }
            if (callAPIResult.isOK()) {
                SgStoreWithPrioritySearchResult searchData = callAPIResult.getData();
                int preoutUpdateResult = searchData.getPreoutUpdateResult();
                if (isSpecChange && preoutUpdateResult == SgConstantsIF.PREOUT_RESULT_OUT_STOCK) {
                    AssertUtils.logAndThrow("寻源分仓占单失败，商品缺货！", user.getLocale());
                }

                List<SgStoreWithPrioritySearchItemResult> resultList = searchData.getItemResultList();
                List<SgStoreWithPrioritySearchItemResult> outStockItemList = searchData.getOutStockItemList();
                if (CollectionUtils.isNotEmpty(outStockItemList)) {
                    logicSearchResult.setOutStockItemList(outStockItemList);
                }

                //寻源占用库存结果  skuid-list
                HashMap<String, List<SgStoreWithPrioritySearchItemResult>> map = Maps.newHashMap();
                //原单据明细skuid-list
                HashMap<String, List<SgBSendItem>> sendMap = Maps.newHashMap();
                List<SgSendItemSaveRequest> var = Lists.newArrayList();

                if (CollectionUtils.isNotEmpty(resultList)) {
                    resultList.forEach(result -> {
                        String key = result.getSourceItemId() + "-" + result.getPsCSkuId();
                        if (map.containsKey(key)) {
                            List<SgStoreWithPrioritySearchItemResult> searchItemResults = map.get(key);
                            //明细被分配逻辑仓  存在拆分情况
                            searchItemResults.add(result);
                        } else {
                            List<SgStoreWithPrioritySearchItemResult> searchItemResults = Lists.newArrayList();
                            searchItemResults.add(result);
                            map.put(key, searchItemResults);
                        }
                    });
                }

                List<SgBSend> bSends = sendMapper.selectList(new QueryWrapper<SgBSend>()
                        .eq(SgSendConstants.SOURCE_BILL_ID, request.getSgSend().getSourceBillId())
                        .eq(SgSendConstants.SOURCE_BILL_NO, request.getSgSend().getSourceBillNo())
                        .eq(SgSendConstants.SOURCE_BILL_TYPE, request.getSgSend().getSourceBillType())
                        .eq(SgSendConstants.IS_ACTIVE, SgConstants.IS_ACTIVE_Y));
                //新增逻辑发货单标识
                if (CollectionUtils.isEmpty(bSends)) {
                    request.getItemList().forEach(var1 -> {
                        String key = var1.getSourceBillItemId() + "-" + var1.getPsCSkuId();
                        List<SgStoreWithPrioritySearchItemResult> var2 = map.get(key);
                        //寻仓结果 不为空 -> 当前sku记录 有占用变化
                        if (CollectionUtils.isNotEmpty(var2)) {
                            for (SgStoreWithPrioritySearchItemResult result : var2) {
                                SgSendItemSaveRequest itemSaveRequest = new SgSendItemSaveRequest();
                                BeanUtils.copyProperties(var1, itemSaveRequest);
                                itemSaveRequest.setCpCStoreId(result.getCpCStoreId());
                                itemSaveRequest.setCpCStoreEcode(result.getCpCStoreEcode());
                                itemSaveRequest.setCpCStoreEname(result.getCpCStoreEname());
                                itemSaveRequest.setQtyPreout(result.getQtyPreout());
                                itemSaveRequest.setRank(result.getRank());
                                itemSaveRequest.setId(ModelUtil.getSequence(SgSendConstants.BILL_SEND_ITEM));
                                //记录待更新的数据 - 待发库存数据
                                var.add(itemSaveRequest);
                            }
                        }
                    });
                    //仅更新有占用变化的明细
                    request.setItemList(var);

                    //条码明细(散码)转录入明细
                    if (storageBoxConfig.getBoxEnable()) {
                        impItemsConvertMethod(request);
                    }

                    SgBSend send = new SgBSend();
                    BeanUtils.copyProperties(request.getSgSend(), send);
                    Long id = ModelUtil.getSequence(SgSendConstants.BILL_SEND);
//                    String billNo = SgStoreUtils.getBillNoByRedis(SgSendConstants.SEQ_SEND.toUpperCase(), SgSendConstants.BILL_SEND.toUpperCase(), send, user);
                    String billNo=SgStoreUtils.getBillNo(SgSendConstants.SEQ_SEND.toUpperCase(), SgSendConstants.BILL_SEND.toUpperCase(), send, user.getLocale());
                    send.setId(id);
                    send.setBillNo(billNo);
                    //查询负库存控制
                    Map<Long, CpCStore> negativeStock = SgStoreUtils.getNegativeStock(request, null, null, null);
                    SgStoreStorageService storageService = ApplicationContextHandle.getBean(SgStoreStorageService.class);
                    ValueHolderV14<SgStorageUpdateResult> storageResult = storageService.batchUpdateSendStoragePreout(send, null, request, Maps.newHashMap(), null, request.getLoginUser(), false, negativeStock);
                    if (storageResult.isOK()) {
                        SgStorageUpdateResult preoutData = storageResult.getData();
                        //check同步库存占用结果
                        SgStoreUtils.isSuccess("占单失败!", storageResult);
                        int preoutStatus = preoutData.getPreoutUpdateResult();
                        //当占单结果不等于成功,同步至寻源分仓结果
                        if (preoutStatus != SgConstantsIF.PREOUT_RESULT_SUCCESS) {
                            logicSearchResult.setPreoutResult(preoutStatus);
                        } else {
                            logicSearchResult.setPreoutResult(preoutUpdateResult);
                        }
                        logicSearchResult.setPreoutUpdateResult(preoutStatus);
                        logicSearchResult.setUpdateResult(preoutData);
                        List<SgStorageOutStockResult> preoutStockList = preoutData.getOutStockItemList();
                        //将占用结果 缺货明细返回给订单
                        if (CollectionUtils.isNotEmpty(preoutStockList)) {
                            for (SgStorageOutStockResult outStockResult : preoutStockList) {
                                SgStoreWithPrioritySearchItemResult itemResult = new SgStoreWithPrioritySearchItemResult();
                                BeanUtils.copyProperties(outStockResult, itemResult);
                                List<SgStoreWithPrioritySearchItemResult> outStockItems = logicSearchResult.getOutStockItemList();
                                if (CollectionUtils.isEmpty(outStockItems)) {
                                    outStockItems = Lists.newArrayList();
                                }
                                outStockItems.add(itemResult);
                                logicSearchResult.setOutStockItemList(outStockItems);
                            }
                            //当寻源时有库存,占单时无库存时,要剔除掉 缺货的明细
                            List<Long> outStockItemIds = preoutStockList.stream().filter(o -> o.getBillItemId() != null)
                                    .map(SgStorageOutStockResult::getBillItemId).collect(Collectors.toList());
                            //实际占上有库存的明细(即将插入逻辑单的明细)
                            if (CollectionUtils.isNotEmpty(outStockItemIds)) {
                                List<SgSendItemSaveRequest> var2 = Lists.newArrayList();
                                for (SgSendItemSaveRequest itemSaveRequest : request.getItemList()) {
                                    if (!outStockItemIds.contains(itemSaveRequest.getId())) {
                                        var2.add(itemSaveRequest);
                                    }
                                }
                                request.setItemList(var2);

                                //条码明细(散码)转录入明细
                                if (storageBoxConfig.getBoxEnable()) {
                                    impItemsConvertMethod(request);
                                }

                                if (CollectionUtils.isEmpty(var2)) {
                                    log.info("来源单据id[" + request.getSgSend().getSourceBillId() + "],寻源时有库存,占单时缺货!");
                                }
                            }
                        }
                        v14.setData(logicSearchResult);
                        v14.setMessage(storageResult.getMessage());
                        saveSgBSend(request, send);
                        return v14;
                    } else {
                        AssertUtils.logAndThrow("占用失败:" + storageResult.getMessage());
                    }
                } else {
                    SgBSend sgBSend = bSends.get(0);
                    if (sgBSend != null) {
                        List<SgBSendItem> sgBSendItems = sendItemMapper.selectList(new QueryWrapper<SgBSendItem>()
                                .eq(SgSendConstants.BILL_SEND_ID, sgBSend.getId()));
                        if (CollectionUtils.isNotEmpty(sgBSendItems)) {
                            sgBSendItems.forEach(sendItem -> {
                                String key = sendItem.getSourceBillItemId() + "-" + sendItem.getPsCSkuId();
                                if (sendMap.containsKey(key)) {
                                    List<SgBSendItem> items = sendMap.get(key);
                                    items.add(sendItem);
                                } else {
                                    List<SgBSendItem> items = Lists.newArrayList();
                                    items.add(sendItem);
                                    sendMap.put(key, items);
                                }
                            });
                            request.setOrgItemList(sgBSendItems);
                        }
                        request.setOrgSgSend(sgBSend);
                    }

                      /*
                     * 将寻仓预占结果 与请求明细(无逻辑仓占用信息) 一一匹对上
                     *   1)明细有占用变化，记录待更新明细信息，更新占用
                     *   2)明细无占用变化，仅更新逻辑发货单明细的原单数量，不更新占用
                     * */
                    request.getItemList().forEach(var1 -> {
                        String key = var1.getSourceBillItemId() + "-" + var1.getPsCSkuId();
                        //寻仓结果
                        List<SgStoreWithPrioritySearchItemResult> var2 = map.get(key);
                        //原明细
                        List<SgBSendItem> var3 = sendMap.get(key);
                        //寻仓结果 不为空 -> 当前sku记录 有占用变化
                        if (CollectionUtils.isNotEmpty(var2)) {
                            for (SgStoreWithPrioritySearchItemResult result : var2) {
                                SgSendItemSaveRequest itemSaveRequest = new SgSendItemSaveRequest();
                                BeanUtils.copyProperties(var1, itemSaveRequest);
                                itemSaveRequest.setCpCStoreId(result.getCpCStoreId());
                                itemSaveRequest.setCpCStoreEcode(result.getCpCStoreEcode());
                                itemSaveRequest.setCpCStoreEname(result.getCpCStoreEname());
                                itemSaveRequest.setQtyPreout(result.getQtyPreout());
                                itemSaveRequest.setRank(result.getRank());
                                //记录待更新的数据 - 待发库存数据
                                var.add(itemSaveRequest);
                            }
                            //无占用变化，从原逻辑发货单明细中获取逻辑店仓，且不发库存
                        } else if (CollectionUtils.isNotEmpty(var3)) {
                            var3.forEach(sgBSendItem -> {
                                sgBSendItem.setQty(var1.getQty());
                                StorageESUtils.setBModelDefalutDataByUpdate(sgBSendItem, user);
                                sgBSendItem.setModifierename(user.getEname());
                                sendItemMapper.updateById(sgBSendItem);
                            });
                        }
                    });

                    request.setItemList(var); //仅更新有占用变化的明细

                    //条码明细(散码)转录入明细
                    if (storageBoxConfig.getBoxEnable()) {
                        impItemsConvertMethod(request);
                    }

                    if (CollectionUtils.isEmpty(var)) {  //若无待更新占用变化明细，仅更新主表后返回,推送ES
                        Long objId = sendSaveService.saveRecordRPC(request, sgBSend, null, null, null);
                        v14.setCode(ResultCode.SUCCESS);
                        v14.setMessage("success");
                        logicSearchResult.setPreoutResult(preoutUpdateResult);
                        v14.setData(logicSearchResult);
                        //推送es
                        storeESUtils.pushESBySend(objId, false, false, null, sendMapper, null);
                        return v14;
                    }

                /*开始保存逻辑发货单*/
                    request.setUpdateMethod(SgConstantsIF.ITEM_UPDATE_TYPE_INC);
                    request.setIsOrder(Boolean.TRUE);
                    long startSave = System.currentTimeMillis();
                    ValueHolderV14<SgSendBillSaveResult> saveAPIResult = sendSaveService.saveSgBSend(request);
                    if (log.isDebugEnabled()) {
                        log.debug("来源单据id[" + request.getSgSend().getSourceBillId() + "],新增发货单并占单耗时:{}ms;"
                                , System.currentTimeMillis() - startSave);
                    }
                    if (saveAPIResult.isOK()) {
                        v14.setCode(ResultCode.SUCCESS);
                        v14.setMessage(saveAPIResult.getMessage());
                        //寻源分仓结果
                        logicSearchResult.setPreoutResult(preoutUpdateResult);
                        SgSendBillSaveResult data = saveAPIResult.getData();
                        SgStorageUpdateResult updateResult = data.getUpdateResult();
                        List<SgStorageOutStockResult> preoutStockList = updateResult.getOutStockItemList();
                        //占单结果
                        int preoutStatus = data.getPreoutUpdateResult();
                        logicSearchResult.setPreoutUpdateResult(preoutStatus);
                        logicSearchResult.setUpdateResult(updateResult);
                        //当占单结果不等于成功,同步至寻源分仓结果
                        if (preoutStatus != SgConstantsIF.PREOUT_RESULT_SUCCESS) {
                            logicSearchResult.setPreoutResult(preoutStatus);
                        }
                        //将占用结果 缺货明细返回给订单
                        if (CollectionUtils.isNotEmpty(preoutStockList)) {
                            for (SgStorageOutStockResult outStockResult : preoutStockList) {
                                SgStoreWithPrioritySearchItemResult itemResult = new SgStoreWithPrioritySearchItemResult();
                                BeanUtils.copyProperties(outStockResult, itemResult);
                                List<SgStoreWithPrioritySearchItemResult> outStockItems = logicSearchResult.getOutStockItemList();
                                if (CollectionUtils.isEmpty(outStockItems)) {
                                    outStockItems = Lists.newArrayList();
                                }
                                outStockItems.add(itemResult);
                                logicSearchResult.setOutStockItemList(outStockItems);
                            }
                        }
                        v14.setData(logicSearchResult);
                        return v14;
                    } else {
                        v14.setCode(ResultCode.FAIL);
                        v14.setMessage(saveAPIResult.getMessage());
                        return v14;
                    }
                }
            } else {
                v14.setCode(ResultCode.FAIL);
                v14.setMessage(callAPIResult.getMessage());
                return v14;
            }
        } catch (Exception e) {
            e.printStackTrace();
            AssertUtils.logAndThrowExtra(e, user.getLocale());
        }
        return null;
    }

    /**
     * 条码明细(散码)转录入明细
     */
    private void impItemsConvertMethod(SgSendSaveWithPriorityRequest request) {
        if (CollectionUtils.isNotEmpty(request.getItemList())) {
            List<SgSendImpItemSaveRequest> impItemList = Lists.newArrayList();
            for (SgSendItemSaveRequest itemSaveRequest : request.getItemList()) {
                SgSendImpItemSaveRequest impItemSaveRequest = new SgSendImpItemSaveRequest();
                BeanUtils.copyProperties(itemSaveRequest, impItemSaveRequest);
                impItemSaveRequest.setId(-1L);
                impItemSaveRequest.setIsTeus(OcBasicConstants.IS_MATCH_SIZE_N);
                impItemList.add(impItemSaveRequest);
            }
            request.setImpItemList(impItemList);
        }
    }

    private void checkParams(SgSendSaveWithPriorityRequest request) {
        User loginUser = request.getLoginUser();
        AssertUtils.notNull(loginUser, "用户未登录！");
        AssertUtils.notBlank(request.getSgSend().getSourceBillNo(), "来源单据编号不能为空！");
    }

    @Async("esAsync")
    public void saveSgBSend(SgSendBillSaveRequest request, SgBSend send) {
        User user = request.getLoginUser();
        try {
            send.setBillStatus(SgSendConstantsIF.BILL_SEND_STATUS_CREATE);
            send.setOwnerename(user.getEname());
            send.setModifierename(user.getEname());

            List<SgSendItemSaveRequest> requestItemList = request.getItemList();
            if (CollectionUtils.isNotEmpty(requestItemList)) {
                BigDecimal totalQtyPreout = requestItemList.stream()
                        .map(SgSendItemSaveRequest::getQtyPreout).reduce(BigDecimal.ZERO, (a, b) -> a.add(b));
                BigDecimal totalQtySend = requestItemList.stream()
                        .map(SgSendItemSaveRequest::getQtySend).reduce(BigDecimal.ZERO, (a, b) -> a.add(b));

                send.setTotQtyPreout(totalQtyPreout);
                send.setTotQtySend(totalQtySend);
            }
            StorageESUtils.setBModelDefalutData(send, user);
            sendMapper.insert(send);

            //新增-逻辑发货单明细
            sendSaveService.batchSaveItems(request, Maps.newHashMap(), send.getId(), false, user);

            //新增-逻辑发货单录入明细
            sendSaveService.batchSaveImpItems(request, Maps.newHashMap(), send.getId(), false, user);

            //推送ES
            storeESUtils.pushESBySend(send.getId(), true, false, null, sendMapper, sendItemMapper);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("保存逻辑发货单异常:" + e.getMessage(), user.getLocale());
        }
    }
}
