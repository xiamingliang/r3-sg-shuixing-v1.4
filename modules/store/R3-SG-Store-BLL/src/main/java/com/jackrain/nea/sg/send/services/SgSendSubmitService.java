package com.jackrain.nea.sg.send.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.redis.config.CusRedisTemplate;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.config.SgStorageBoxConfig;
import com.jackrain.nea.sg.basic.model.result.SgStorageUpdateResult;
import com.jackrain.nea.sg.basic.model.table.CpCStore;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutResult;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutResultImpItem;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutResultItem;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutResultTeusItem;
import com.jackrain.nea.sg.send.common.SgSendConstants;
import com.jackrain.nea.sg.send.common.SgSendConstantsIF;
import com.jackrain.nea.sg.send.mapper.SgBSendImpItemMapper;
import com.jackrain.nea.sg.send.mapper.SgBSendItemMapper;
import com.jackrain.nea.sg.send.mapper.SgBSendMapper;
import com.jackrain.nea.sg.send.mapper.SgBSendTeusItemMapper;
import com.jackrain.nea.sg.send.model.request.SgSendBillSubmitRequest;
import com.jackrain.nea.sg.send.model.result.SgSendBillSubmitResult;
import com.jackrain.nea.sg.send.model.result.SgSendCheckResult;
import com.jackrain.nea.sg.send.model.table.SgBSend;
import com.jackrain.nea.sg.send.model.table.SgBSendImpItem;
import com.jackrain.nea.sg.send.model.table.SgBSendItem;
import com.jackrain.nea.sg.send.model.table.SgBSendTeusItem;
import com.jackrain.nea.sg.send.utils.SgStoreESUtils;
import com.jackrain.nea.sg.send.utils.SgStoreUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;


/**
 * @author 舒威
 * @since 2019/4/24
 * create at : 2019/4/24 13:18
 */
@Slf4j
@Component
public class SgSendSubmitService {

    @Autowired
    private SgBSendMapper sendMapper;

    @Autowired
    private SgBSendItemMapper sendItemMapper;

    @Autowired
    private SgSendStorageService storageService;

    @Autowired
    private SgStorageBoxConfig storageBoxConfig;

    @Autowired
    private SgStoreESUtils storeESUtils;

    public ValueHolderV14<SgSendBillSubmitResult> submitSgBSend(SgSendBillSubmitRequest request) {
        ValueHolderV14<SgSendBillSubmitResult> v14 = new ValueHolderV14<>();
        try {
            SgSendSubmitService sgSendSubmitService = ApplicationContextHandle.getBean(SgSendSubmitService.class);
            v14 = sgSendSubmitService.submitSgBSendWithTrans(request);
        } catch (Exception e) {
            e.printStackTrace();
            v14.setCode(ResultCode.FAIL);
            v14.setMessage("error -> " + e.getMessage());
        }
        if (log.isDebugEnabled()) {
            log.debug("来源单据id[" + request.getSourceBillId()
                    + "],逻辑发货单出库服务出参" + v14.toJSONObject().toJSONString() + ";");
        }
        return v14;
    }

    @Transactional(rollbackFor = Exception.class)
    public ValueHolderV14<SgSendBillSubmitResult> submitSgBSendWithTrans(SgSendBillSubmitRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("来源单据id[" + request.getSourceBillId()
                    + "],逻辑发货单出库服务入参:" + JSONObject.toJSONString(request) + ";");
        }

        ValueHolderV14<SgSendBillSubmitResult> v14 = new ValueHolderV14<>();

        //参数校验
        SgSendCheckResult checkResult = checkParams(request);

        SgBSend send = checkResult.getSend();
        List<SgBSendItem> sendItems = checkResult.getSendItems();
        User user = request.getLoginUser();

        SgBPhyOutResult phyOutResult = request.getPhyOutResult();
        Long noticesId = phyOutResult.getSgBPhyOutNoticesId();
        String noticesBillno = phyOutResult.getSgBPhyOutNoticesBillno();

        //加锁
        CusRedisTemplate<String, String> redisTemplate = SgStoreUtils.initRedisTemplate();
        String lockKsy = SgConstants.SG_B_SEND + ":" + request.getSourceBillId() + ":" + request.getSourceBillType();
        Boolean ifAbsent = redisTemplate.opsForValue().setIfAbsent(lockKsy, "OK");
        if (ifAbsent != null && ifAbsent) {
            redisTemplate.expire(lockKsy, 30, TimeUnit.MINUTES);
        } else {
            v14.setCode(ResultCode.FAIL);
            v14.setMessage("逻辑发货单发货中，请稍后重试！");
            return v14;
        }

        try {
            List<SgBSendItem> releaseStorages = Lists.newArrayList(); //记录库存释放明细
            List<SgBSendItem> lastReleaseStorages = Lists.newArrayList(); //记录最后一次为是的库存释放明细
            List<SgBSendImpItem> impReleaseStorages = Lists.newArrayList(); //记录库存释放明细
            List<SgBSendImpItem> impLastReleaseStorages = Lists.newArrayList(); //记录最后一次为是的库存释放明细

            //更新逻辑发货单
            Boolean isLast = updateSend(request, send, sendItems, releaseStorages, lastReleaseStorages, impReleaseStorages, impLastReleaseStorages);

            //获取负库存相关信息
            Map<Long, CpCStore> negativeStock = SgStoreUtils.getNegativeStock(null, null, null, sendItems);

            Map<Long, BigDecimal> orgSkuTeusCounts = Maps.newHashMap();
            Map<Long, BigDecimal> skuTeusCounts = Maps.newHashMap();

            //获取sku箱内数量信息
            getTeusCountsInfo(request, send.getId(), skuTeusCounts, orgSkuTeusCounts, isLast);

            //更新库存
            ValueHolderV14<SgStorageUpdateResult> callAPIResult =
                    storageService.updateSendStoragePreout(send, isLast ? lastReleaseStorages : releaseStorages,
                            null, null, "submit", user, isLast, negativeStock, request.getIsNegativePreout(),
                            false, isLast ? impLastReleaseStorages : impReleaseStorages, skuTeusCounts, null, noticesId, noticesBillno, orgSkuTeusCounts);
            if (callAPIResult.isOK()) {
                //check同步库存占用结果
                SgStoreUtils.isSuccess("逻辑发货单发货失败", callAPIResult);
                v14.setCode(ResultCode.SUCCESS);
                v14.setMessage("逻辑发货单发货成功");
            } else {
                AssertUtils.logAndThrow("逻辑发货单发货失败!" + callAPIResult.getMessage());
            }
        } catch (Exception e) {
            e.printStackTrace();
            AssertUtils.logAndThrowExtra(e, user.getLocale());
        } finally {
            try {
                redisTemplate.delete(lockKsy);
            } catch (Exception e) {
                e.printStackTrace();
                log.error(this.getClass().getName() + lockKsy + "释放锁失败，请联系管理员！");
                AssertUtils.logAndThrow("释放锁失败，请联系管理员！");
            }
        }
        return v14;
    }

    /**
     * BeanCopy出一份出库明细，避免下游继续使用该明细出现数据问题
     */
    public List<SgBPhyOutResultItem> beanCopyOutResultItems(SgSendBillSubmitRequest request) {
        List<SgBPhyOutResultItem> orgResultItems = request.getPhyOutResultItems();
        List<SgBPhyOutResultItem> phyOutResultItems = Lists.newArrayList();
        for (SgBPhyOutResultItem orgResultItem : orgResultItems) {
            SgBPhyOutResultItem outResultItem = new SgBPhyOutResultItem();
            BeanUtils.copyProperties(orgResultItem, outResultItem);
            phyOutResultItems.add(outResultItem);
        }
        return phyOutResultItems;
    }

    /**
     * 获取sku箱内数量信息
     */
    public void getTeusCountsInfo(SgSendBillSubmitRequest request, Long id, Map<Long, BigDecimal> skuTeusCounts, Map<Long, BigDecimal> orgSkuTeusCounts, Boolean isLast) {
        if (CollectionUtils.isNotEmpty(request.getPhyOutResultTeusItems())) {
            skuTeusCounts.putAll(request.getPhyOutResultTeusItems().stream().collect(Collectors.groupingBy(SgBPhyOutResultTeusItem::getPsCSkuId,
                    Collectors.reducing(BigDecimal.ZERO, SgBPhyOutResultTeusItem::getQty, BigDecimal::add))));
            //存在 最后一次出库 箱内出0 , 此时释放箱内占用 需要查询原箱内明细表
            if (isLast) {
                SgBSendTeusItemMapper teusItemMapper = ApplicationContextHandle.getBean(SgBSendTeusItemMapper.class);
                orgSkuTeusCounts.putAll(teusItemMapper.selectList(new QueryWrapper<SgBSendTeusItem>().lambda()
                        .eq(SgBSendTeusItem::getSgBSendId, id)).stream()
                        .collect(Collectors.groupingBy(SgBSendTeusItem::getPsCSkuId,
                                Collectors.reducing(BigDecimal.ZERO, SgBSendTeusItem::getQty, BigDecimal::add))));
            }
        }
    }

    /**
     * 更新逻辑发货单录入明细、获取库存更新明细
     */
    private void updateSendAndGetImpReleaseStorages(SgSendBillSubmitRequest request, List<SgBSendImpItem> sgBSendImpItems, List<SgBSendImpItem> impReleaseStorages,
                                                    List<SgBSendImpItem> impLastReleaseStorages, Boolean isLast, Long sendId, User user, SgBSendImpItemMapper impItemMapper, Boolean isRepeatOut) {
        //原录入明细-散码 key:skuid
        Map<Long, List<SgBSendImpItem>> scatterMap = sgBSendImpItems.stream()
                .filter(o -> o.getIsTeus().equals(SgSendConstantsIF.IS_TEUS_N)).collect(Collectors.groupingBy(SgBSendImpItem::getPsCSkuId));
        //原录入明细-箱 key: 箱号id  整箱不会拆分
        Map<Long, SgBSendImpItem> boxMap = sgBSendImpItems.stream()
                .filter(o -> o.getIsTeus().equals(SgSendConstantsIF.IS_TEUS_Y)).collect(Collectors.toMap(SgBSendImpItem::getPsCTeusId, o -> o));
        //出库录入明细-散码 key:skuid
        Map<Long, SgBPhyOutResultImpItem> outScatterMap = request.getPhyOutResultImpItems().stream()
                .filter(o -> o.getIsTeus().equals(SgSendConstantsIF.IS_TEUS_N)).collect(Collectors.toMap(SgBPhyOutResultImpItem::getPsCSkuId, o -> o));
        //出库录入明细-箱 key:箱号   整箱不会拆分
        Map<Long, SgBPhyOutResultImpItem> outBoxMap = request.getPhyOutResultImpItems().stream()
                .filter(o -> o.getIsTeus().equals(SgSendConstantsIF.IS_TEUS_Y)).collect(Collectors.toMap(SgBPhyOutResultImpItem::getPsCTeusId, o -> o));

        //散码
        if (MapUtils.isNotEmpty(scatterMap) && MapUtils.isNotEmpty(outScatterMap)) {
            for (Map.Entry<Long, List<SgBSendImpItem>> sendEntry : scatterMap.entrySet()) {
                Long sku = sendEntry.getKey();
                List<SgBSendImpItem> impItems = sendEntry.getValue();
                //过滤出录入明细中 逻辑仓优先级不为空的数据
                List<SgBSendImpItem> rankNotNulls = impItems.stream().filter(s -> s.getRank() != null).collect(Collectors.toList());

                //当sku分行 并且 逻辑仓优先级不为空的情况下  按照逻辑仓优先级排序 并释放占用
                //记录当前sku分行数
                int size = impItems.size();
                if (size > 1 && size == rankNotNulls.size()) {
                    impItems = impItems.stream().sorted((p1, p2) -> p2.getRank() - p1.getRank()).collect(Collectors.toList());
                }

                SgBPhyOutResultImpItem outImpItem = outScatterMap.get(sku);
                if (outImpItem != null) {
                    BigDecimal qty = outImpItem.getQtyOut(); //出库数量
                    for (SgBSendImpItem impItem : impItems) {
                        //impItem 用作库存更新对象，update 逻辑发货单明细更新对象
                        SgBSendImpItem update = new SgBSendImpItem();
                        BeanUtils.copyProperties(impItem, update);

                        BigDecimal orgQtyPreout = impItem.getQtyPreout();
                        BigDecimal orgQtySend = impItem.getQtySend();
                        BigDecimal subtract = qty.abs().subtract(orgQtyPreout.abs());
                        if (subtract.compareTo(BigDecimal.ZERO) > 0) {
                            //出库数量>当前发货单明细占用数量，当前发货单明细 占用全部释放, 继续遍历
                            //库存 -> 待发货数量，待释放占用量
                            BigDecimal qtySend = size > 1 ? orgQtyPreout : qty;
                            impItem.setQtySend(qtySend);
                            impItem.setQtyPreout(qtySend);
                            impReleaseStorages.add(impItem);
                            // 逻辑发货单 -> 累加发货数量,更新占用数量
                            update.setQtySend(orgQtySend.add(qtySend));
                            update.setQtyPreout(orgQtyPreout.subtract(qtySend));
                            StorageESUtils.setBModelDefalutDataByUpdate(update, user);
                            update.setModifierename(user.getEname());
                            impItemMapper.updateById(update);
                            qty = subtract;
                            //参与完计算后 分行数--
                            size--;
                        } else if (subtract.compareTo(BigDecimal.ZERO) < 0) {
                            //出库数量<当前发货单明细占用数量，当前发货单明细 占单部分释放, 终止遍历
                            //库存 -> 待发货数量，待释放占用量 例如:原占用10件,出库5件,最后一次出库,释放10件，明细更新:占用数量=0,已发货数量=5
                            impItem.setQtySend(qty);
                            impItem.setQtyPreout(isLast ? orgQtyPreout : qty);
                            impReleaseStorages.add(impItem);
                            // 逻辑发货单 -> 累加发货数量,更新占用数量
                            update.setQtySend(orgQtySend.add(qty));
                            update.setQtyPreout(isLast ? BigDecimal.ZERO : subtract.negate());
                            StorageESUtils.setBModelDefalutDataByUpdate(update, user);
                            update.setModifierename(user.getEname());
                            impItemMapper.updateById(update);
                            break;
                        } else if (0 == subtract.compareTo(BigDecimal.ZERO)) {
                            //出库数量=当前发货单明细占用数量，当前发货单明细 占用全部释放, 终止遍历
                            //库存 -> 待发货数量，待释放占用量
                            impItem.setQtySend(qty);
                            impItem.setQtyPreout(qty);
                            impReleaseStorages.add(impItem);
                            //逻辑发货单 -> 累加发货数量,更新占用数量
                            update.setQtySend(orgQtySend.add(qty));
                            update.setQtyPreout(orgQtyPreout.subtract(qty));
                            StorageESUtils.setBModelDefalutDataByUpdate(update, user);
                            update.setModifierename(user.getEname());
                            impItemMapper.updateById(update);
                            break;
                        }
                    }
                }else {
                    impItems.forEach(o -> o.setQtySend(BigDecimal.ZERO));
                }
                if (isLast) {
                    impLastReleaseStorages.addAll(impItems);
                }
            }

        }


        //箱
        if (MapUtils.isNotEmpty(boxMap) && MapUtils.isNotEmpty(outBoxMap)) {
            for (Map.Entry<Long, SgBSendImpItem> sendEntry : boxMap.entrySet()) {
                Long teusId = sendEntry.getKey();
                SgBSendImpItem impItem = sendEntry.getValue();
                SgBPhyOutResultImpItem outImpItem = outBoxMap.get(teusId);
                if (outImpItem != null) {
                    BigDecimal qty = outImpItem.getQtyOut(); //出库数量
                    //impItem 用作库存更新对象，update 逻辑发货单明细更新对象
                    SgBSendImpItem update = new SgBSendImpItem();
                    BeanUtils.copyProperties(impItem, update);

                    BigDecimal orgQtyPreout = impItem.getQtyPreout();
                    BigDecimal orgQtySend = impItem.getQtySend();
                    BigDecimal subtract = qty.abs().subtract(orgQtyPreout.abs());
                    if (subtract.compareTo(BigDecimal.ZERO) >= 0) {
                        //出库箱数>当前发货单录入明细占用箱数，当前发货单录入明细 占用箱数全部释放 出库数量=占用箱数
                        //库存 -> 待发货数量，待释放占用量
                        impItem.setQtySend(qty);
                        impItem.setQtyPreout(qty);
                        impReleaseStorages.add(impItem);
                        //逻辑发货单 -> 累加发货数量,更新占用数量
                        update.setQtySend(orgQtySend.add(qty));
                        update.setQtyPreout(orgQtyPreout.subtract(qty));
                        StorageESUtils.setBModelDefalutDataByUpdate(update, user);
                        update.setModifierename(user.getEname());
                        impItemMapper.updateById(update);
                    } else if (subtract.compareTo(BigDecimal.ZERO) < 0) {
                        //出库箱数<当前发货单录入明细占用箱数，当前发货单录入明细 占单部分释放(释放箱数=出库箱数)
                        //库存 -> 待发货数量，待释放占用量，待释放占用量,例如：原占用10件,出库5件,最后一次出库,释放10件，明细更新:占用数量=0,已发货数量=5
                        impItem.setQtySend(qty);
                        impItem.setQtyPreout(isLast ? orgQtyPreout : qty);
                        impReleaseStorages.add(impItem);

                        //逻辑发货单 -> 累加发货数量,更新占用数量
                        update.setQtySend(orgQtySend.add(qty));
                        update.setQtyPreout(isLast ? BigDecimal.ZERO : subtract.negate());
                        StorageESUtils.setBModelDefalutDataByUpdate(update, user);
                        update.setModifierename(user.getEname());
                        impItemMapper.updateById(update);
                    }
                }
                if (isLast) {
                    impLastReleaseStorages.add(impItem);
                }
            }

        }

        //最后一次出库，将逻辑发货单明细中 所有占用数量非0的记录更新为0
        if (!isRepeatOut && isLast) {
            SgBSendImpItem update = new SgBSendImpItem();
            update.setQtyPreout(BigDecimal.ZERO);
            StorageESUtils.setBModelDefalutDataByUpdate(update, user);
            int count = impItemMapper.update(update, new UpdateWrapper<SgBSendImpItem>().lambda().eq(SgBSendImpItem::getSgBSendId, sendId));
            if (count != sgBSendImpItems.size()) {
                AssertUtils.logAndThrow("最后一次出库时,逻辑发货单明细更新失败！");
            }
        }
    }

    /**
     * 更新逻辑发货单明细、获取库存更新明细
     */
    private void updateSendAndGetReleaseStorages(List<SgBPhyOutResultItem> phyOutResultItems, List<SgBSendItem> sgBSendItems, List<SgBSendItem> releaseStorages,
                                                 List<SgBSendItem> lastReleaseStorages, Boolean isLast, Long sendId, User user, Boolean isRepeatOut) {
        //原录入明细-散码 key:skuid
        Map<Long, List<SgBSendItem>> map = sgBSendItems.stream().collect(Collectors.groupingBy(SgBSendItem::getPsCSkuId));

        Map<Long, SgBPhyOutResultItem> requestMap = Maps.newHashMap();
        //如果出库结果明细有拆分  合并出库结果明细
        for (SgBPhyOutResultItem phyOutResultItem : phyOutResultItems) {
            Long psCSkuId = phyOutResultItem.getPsCSkuId();
            if (requestMap.containsKey(psCSkuId)) {
                SgBPhyOutResultItem orgResultItem = requestMap.get(psCSkuId);
                BigDecimal orgResultItemQty = Optional.ofNullable(orgResultItem.getQty()).orElse(BigDecimal.ZERO);
                BigDecimal qty = Optional.ofNullable(phyOutResultItem.getQty()).orElse(BigDecimal.ZERO);
                orgResultItem.setQty(qty.add(orgResultItemQty));
            } else {
                requestMap.put(psCSkuId, phyOutResultItem);
            }
        }

        if (MapUtils.isNotEmpty(map)) {
            for (Map.Entry<Long, List<SgBSendItem>> sendEntry : map.entrySet()) {
                Long sku = sendEntry.getKey();
                List<SgBSendItem> sendItems = sendEntry.getValue();
                //过滤出明细中 逻辑仓优先级不为空的数据
                List<SgBSendItem> rankNotNulls = sendItems.stream().filter(s -> s.getRank() != null).collect(Collectors.toList());

                //当sku分行 并且 逻辑仓优先级不为空的情况下  按照逻辑仓优先级排序 并释放占用
                //记录当前sku分行数
                int size = sendItems.size();
                if (size > 1 && size == rankNotNulls.size()) {
                    sendItems = sendItems.stream().
                            sorted((p1, p2) -> p2.getRank() - p1.getRank()).collect(Collectors.toList());
                }

                SgBPhyOutResultItem phyOutResultItem = requestMap.get(sku);
                if (phyOutResultItem != null) {
                    BigDecimal qty = phyOutResultItem.getQty(); //出库数量
                    for (SgBSendItem sendItem : sendItems) {
                        //sendItem 用作库存更新对象，updateSgBSendItem 逻辑发货单明细更新对象
                        SgBSendItem updateSgBSendItem = new SgBSendItem();
                        BeanUtils.copyProperties(sendItem, updateSgBSendItem);

                        BigDecimal orgQtyPreout = sendItem.getQtyPreout();
                        BigDecimal orgQtySend = sendItem.getQtySend();
                        BigDecimal subtract = qty.abs().subtract(orgQtyPreout.abs());
                        if (subtract.compareTo(BigDecimal.ZERO) > 0) {
                            //出库数量>当前发货单明细占用数量，当前发货单明细 占用全部释放, 继续遍历
                            BigDecimal qtySend = size > 1 ? orgQtyPreout : qty;
                            //库存 -> 待发货数量,待释放占用量
                            sendItem.setQtySend(qtySend);
                            sendItem.setQtyPreout(qtySend);
                            releaseStorages.add(sendItem);
                            //逻辑发货单 -> 累加发货数量,更新占用数量
                            updateSgBSendItem.setQtySend(orgQtySend.add(qtySend));
                            updateSgBSendItem.setQtyPreout(orgQtyPreout.subtract(qtySend));
                            StorageESUtils.setBModelDefalutDataByUpdate(updateSgBSendItem, user);
                            updateSgBSendItem.setModifierename(user.getEname());
                            sendItemMapper.updateById(updateSgBSendItem);
                            qty = subtract;
                            //参与完计算后 分行数--
                            size--;
                        } else if (subtract.compareTo(BigDecimal.ZERO) < 0) {
                            //出库数量<当前发货单明细占用数量，当前发货单明细 占单部分释放, 终止遍历
                            //库存 -> 待发货数量,待释放占用量 例如：原占用10件,出库5件,最后一次出库,释放10件，明细更新:占用数量=0,已发货数量=5
                            sendItem.setQtySend(qty);
                            sendItem.setQtyPreout(isLast ? orgQtyPreout : qty);
                            releaseStorages.add(sendItem);
                            //逻辑发货单 -> 累加发货数量,更新占用数量
                            updateSgBSendItem.setQtySend(orgQtySend.add(qty));
                            updateSgBSendItem.setQtyPreout(isLast ? BigDecimal.ZERO : subtract.negate());
                            StorageESUtils.setBModelDefalutDataByUpdate(updateSgBSendItem, user);
                            updateSgBSendItem.setModifierename(user.getEname());
                            sendItemMapper.updateById(updateSgBSendItem);
                            break;
                        } else if (0 == subtract.compareTo(BigDecimal.ZERO)) {
                            //出库数量=当前发货单明细占用数量，当前发货单明细 占用全部释放, 终止遍历
                            //库存 -> 待发货数量,待释放占用量
                            sendItem.setQtySend(qty);
                            sendItem.setQtyPreout(qty);
                            releaseStorages.add(sendItem);
                            //逻辑发货单 -> 累加发货数量,更新占用数量
                            updateSgBSendItem.setQtySend(orgQtySend.add(qty));
                            updateSgBSendItem.setQtyPreout(orgQtyPreout.subtract(qty));
                            StorageESUtils.setBModelDefalutDataByUpdate(updateSgBSendItem, user);
                            updateSgBSendItem.setModifierename(user.getEname());
                            sendItemMapper.updateById(updateSgBSendItem);
                            break;
                        }
                    }
                }else {
                    sendItems.forEach(o -> o.setQtySend(BigDecimal.ZERO));
                }
                if (isLast) {
                    lastReleaseStorages.addAll(sendItems);
                }
            }

            //最后一次出库，将逻辑发货单明细中 所有占用数量非0的记录更新为0
            if (!isRepeatOut && isLast) {
                int count = sendItemMapper.updatePreoutBySendId(sendId);
                if (count != sgBSendItems.size()) {
                    AssertUtils.logAndThrow("最后一次出库时,逻辑发货单明细更新失败！");
                }
            }
        }
    }

    /**
     * 更新业务节点
     */
    private Long updateServiceNode(Integer billType) {
        switch (billType) {
            case SgConstantsIF.BILL_TYPE_RETAIL:
                return null;
            case SgConstantsIF.BILL_TYPE_SALE:
                return SgConstantsIF.SERVICE_NODE_SALE_OUT_SUBMIT;
            case SgConstantsIF.BILL_TYPE_SALE_REF:
                return SgConstantsIF.SERVICE_NODE_REF_SALE_OUT_SUBMIT;
            case SgConstantsIF.BILL_TYPE_PUR_REF:
                return SgConstantsIF.SERVICE_NODE_REF_PUR_OUT_SUBMIT;
            case SgConstantsIF.BILL_TYPE_TRANSFER:
                return SgConstantsIF.SERVICE_NODE_TRANSFER_OUT_SUBMIT;
            case SgConstantsIF.BILL_TYPE_RETAIL_POS:
                return SgConstantsIF.SERVICE_NODE_RETAIL_POS_SUBMIT;
            default:
                return null;
        }
    }

    /**
     * 更新逻辑发货单
     */
    public Boolean updateSend(SgSendBillSubmitRequest request, SgBSend send, List<SgBSendItem> sendItems,
                              List<SgBSendItem> releaseStorages, List<SgBSendItem> lastReleaseStorages,
                              List<SgBSendImpItem> impReleaseStorages, List<SgBSendImpItem> impLastReleaseStorages) {
        User user = request.getLoginUser();
        Long id = send.getId();

        BigDecimal totQtyPreout = Optional.ofNullable(send.getTotQtyPreout()).orElse(BigDecimal.ZERO);//总占用数
        BigDecimal totQtyOut = Optional.ofNullable(request.getPhyOutResult().getTotQtyOut()).orElse(BigDecimal.ZERO); //总出库数
        BigDecimal release = totQtyPreout.subtract(totQtyOut);//释放占用数
        /*TODO 设计缺陷，暂定每次都是最后一次出入库*/
        //是否最后一次标识为是 或 差异数量(总占用-总出库)为0
        Boolean isLast = (SgSendConstantsIF.IS_LASTSEND_Y.equals(request.getPhyOutResult().getIsLast()) || 0 == BigDecimal.ZERO.compareTo(release));


        //更新逻辑发货单明细、获取库存更新明细
        if (CollectionUtils.isNotEmpty(sendItems)) {
            //BeanCopy出一份出库明细，避免下游继续使用该明细出现数据问题
            List<SgBPhyOutResultItem> phyOutResultItems = beanCopyOutResultItems(request);
            updateSendAndGetReleaseStorages(phyOutResultItems, sendItems, releaseStorages, lastReleaseStorages, isLast, id, user, request.getIsRepeatOut());
        }

        //更新逻辑发货单录入明细、获取库存更新明细
        if (storageBoxConfig.getBoxEnable()) {
            SgBSendImpItemMapper impItemMapper = ApplicationContextHandle.getBean(SgBSendImpItemMapper.class);
            List<SgBSendImpItem> impItems = impItemMapper.selectList(new QueryWrapper<SgBSendImpItem>().lambda().eq(SgBSendImpItem::getSgBSendId, id));
            if (CollectionUtils.isNotEmpty(impItems)) {
                updateSendAndGetImpReleaseStorages(request, impItems, impReleaseStorages, impLastReleaseStorages, isLast, id, user, impItemMapper, request.getIsRepeatOut());
            }
        }

        JSONObject object = sendItemMapper.selectTotalPreoutBySendId(id);

        //更新主表单据状态、业务节点、总占用数、总发货数、发货时间等信息
        send.setBillStatus(isLast ? SgSendConstantsIF.BILL_SEND_STATUS_ALL_SEND : SgSendConstantsIF.BILL_SEND_STATUS_PART_SEND);
        send.setServiceNode(updateServiceNode(request.getSourceBillType()));
        send.setTotQtyPreout(object.getBigDecimal("qty_preout"));//更新主表总占用数量
        send.setTotQtySend(object.getBigDecimal("qty_send"));
        send.setSendTime(new Date());
        StorageESUtils.setBModelDefalutDataByUpdate(send, user);
        send.setModifierename(user.getEname());
        sendMapper.updateById(send);
        //推送ES
        storeESUtils.pushESBySend(id, true, false, null, sendMapper, sendItemMapper);

        return isLast;
    }


    public SgSendCheckResult checkParams(SgSendBillSubmitRequest request) {
        SgStoreUtils.checkR3BModelDefalut(request);
        SgStoreUtils.checkBModelDefalut(request, false);
        AssertUtils.notNull(request.getPhyOutResult(), "出库结果单不能为空！");
        AssertUtils.notEmpty(request.getPhyOutResultItems(), "出库结果明细不能为空！");

        SgBSend send = sendMapper.selectOne(new QueryWrapper<SgBSend>()
                .eq(SgSendConstants.SOURCE_BILL_ID, request.getSourceBillId())
                .eq(SgSendConstants.SOURCE_BILL_TYPE, request.getSourceBillType())
                .eq(SgSendConstants.IS_ACTIVE, SgConstants.IS_ACTIVE_Y));
        AssertUtils.notNull(send, "该记录已不存在！", request.getLoginUser().getLocale());

        if (!request.getIsRepeatOut() && send.getBillStatus().equals(SgSendConstantsIF.BILL_SEND_STATUS_ALL_SEND)) {
            AssertUtils.logAndThrow("逻辑发货单已完全出库,不允许重复出库!");
        }

        List<SgBSendItem> sendItems = sendItemMapper.selectList(new QueryWrapper<SgBSendItem>().lambda().eq(SgBSendItem::getSgBSendId, send.getId()));
        AssertUtils.notEmpty(sendItems, "逻辑发货单明细为空,不允许出库!");

        SgSendCheckResult checkResult = new SgSendCheckResult();
        checkResult.setSend(send);
        checkResult.setSendItems(sendItems);
        return checkResult;
    }
}
