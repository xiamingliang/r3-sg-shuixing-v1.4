package com.jackrain.nea.sg.transfer.validate;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferMapper;
import com.jackrain.nea.sg.transfer.mapper.SgBTransferPropMapper;
import com.jackrain.nea.tableService.MainTableRecord;
import com.jackrain.nea.tableService.TableServiceContext;
import com.jackrain.nea.tableService.constants.Constants;
import com.jackrain.nea.tableService.validate.BaseValidator;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.utils.AssertUtils;
import org.springframework.stereotype.Component;

import java.util.Locale;

/**
 * @author huang.zaizai
 * 校验 调拨性质
 */
@Component
public class ScTransferPropValidate extends BaseValidator {

    @Override
    public void validate(TableServiceContext context, MainTableRecord currentRow) {
        Locale locale = context.getLocale();
        JSONObject commitData = currentRow.getMainData().getCommitData();
        JSONObject data = currentRow.getMainData().getOrignalData();
        //插入时判断
        if (Constants.ACTION_ADD.equals(context.getActionName())) {
            SgBTransferPropMapper transferPropMapper = ApplicationContextHandle.getBean(SgBTransferPropMapper.class);
            int count = transferPropMapper.selectTransferPropByEname(commitData.getString("ENAME"));
            AssertUtils.cannot(count > 0, "当前名称已存在，不允许保存！", locale);
        } else {
            AssertUtils.cannot(data.size() == 0, "当前记录已不存在！", locale);
        }
        if (Constants.ACTION_DELETE.equals(context.getActionName())) {
            ScBTransferMapper transferMapper = ApplicationContextHandle.getBean(ScBTransferMapper.class);
            int count = transferMapper.selectTransferByPropId(commitData.getLong("ID"));
            AssertUtils.cannot(count > 0, "当前记录已被调拨单引用，不允许删除！", locale);
        }
    }
}