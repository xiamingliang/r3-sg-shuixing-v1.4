package com.jackrain.nea.sg.transfer.mapper;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.transfer.model.table.ScBTransfer;
import org.apache.ibatis.annotations.*;

import java.util.Date;
import java.util.List;

@Mapper
public interface ScBTransferMapper extends ExtentionMapper<ScBTransfer> {

    /**
     * 分页查询逻辑发货单
     */
    @Select("SELECT * FROM " + SgConstants.SG_B_TRANSFER + " ORDER BY ID LIMIT #{page} OFFSET #{offset}")
    List<ScBTransfer> selectListByPage(@Param("page") int page, @Param("offset") int offset);


    @Select("SELECT COUNT(*) FROM SC_B_TRANSFER " +
            "WHERE sg_b_transfer_prop_id = #{transferPropId} AND STATUS <> 8")
    int selectTransferByPropId(@Param("transferPropId") Long transferPropId);

    /**
     * 根据主键更新sap状态
     */
    @Update("<script> update sc_b_transfer set sap_status = #{sapStatus} where id in " +
            "  <foreach item='item' collection='idList' " +
            " open='(' separator=',' close=')'> #{item} </foreach>  </script>" )
    int updateTransferSapStatus(@Param("sapStatus") Integer sapStatus,@Param("idList") List<Long> idList);

    /**
     * 捞取出库完成发&送失败的调拨单
     *
     * @param query 查询条件
     * @return 查询结果
     */
    @SelectProvider(type = TransferSql.class, method = "queryTransferByOut")
    List<ScBTransfer> selectTransferByLogOut(JSONObject query);

    /**
     * 捞取入库完成发&送失败的调拨单
     *
     * @param query 查询条件
     * @return 查询结果
     */
    @SelectProvider(type = TransferSql.class, method = "queryTransferByIn")
    List<ScBTransfer> selectTransferByLogIn(JSONObject query);

    class TransferSql {

        public String queryTransferByOut(JSONObject query) {

            String startDate = query.getString("startDate");
            String endDate = query.getString("endDate");
            String billNo = query.getString("billNo");
            long startId = query.getLong("startId");
            long pageSize = query.getLong("pageSize");

            StringBuilder sql = new StringBuilder("SELECT * FROM sc_b_transfer WHERE isactive = 'Y' " +
                    "AND status > 4 AND (is_out_settle_log = 1 OR is_out_settle_log = 3)");
            sql.append("\tAND id>" + startId);
            if (startDate != null) sql.append("\tAND bill_date>='" + startDate + "'");
            if (endDate != null) sql.append("\tAND bill_date<='" + endDate + "'");
            if (billNo != null) sql.append("\tAND bill_no IN(" + billNo + ")");
            sql.append("\tORDER BY id LIMIT " + pageSize);
            return sql.toString();
        }

        public String queryTransferByIn(JSONObject query) {

            String startDate = query.getString("startDate");
            String endDate = query.getString("endDate");
            String billNo = query.getString("billNo");
            long startId = query.getLong("startId");
            long pageSize = query.getLong("pageSize");

            StringBuilder sql = new StringBuilder("SELECT * FROM sc_b_transfer WHERE isactive = 'Y' " +
                    "AND status = 7 AND (is_in_settle_log = 1 OR is_in_settle_log = 3)");
            sql.append("\tAND id>" + startId);
            if (startDate != null) sql.append("\tAND bill_date>='" + startDate + "'");
            if (endDate != null) sql.append("\tAND bill_date<='" + endDate + "'");
            if (billNo != null) sql.append("\tAND bill_no IN(" + billNo + ")");
            sql.append("\tORDER BY id LIMIT " + pageSize);
            return sql.toString();
        }
    }


    /**
     * 批量更新出/入库拣货状态
     */
    @Update("UPDATE sc_b_transfer\n" +
            "SET pick_out_status = #{arg0},\n" +
            " picker_out_id = #{arg1},\n" +
            " picker_out_name = #{arg2},\n" +
            " picker_out_ename = #{arg3},\n" +
            " picker_out_time = #{arg4}\n" +
            "WHERE\n" +
            "\tid IN (${arg5})")
    int batchUpdateTransferOutPickByIds(Integer outPickStatus, Long userId, String userName, String userEName,
                                    Date pickTime, String ids);

    @Update("UPDATE sc_b_transfer\n" +
            "SET pick_status = #{arg0},\n" +
            " picker_id = #{arg1},\n" +
            " picker_name = #{arg2},\n" +
            " picker_ename = #{arg3},\n" +
            " pick_time = #{arg4}\n" +
            "WHERE\n" +
            "\tid IN (${arg5})")
    int batchUpdateTransferInPickByIds(Integer inPickStatus, Long userId, String userName, String userEName,
                                   Date pickTime, String ids);


    /**
     *  更新【总出库数量】、【总出库吊牌金额】、【总出库销售金额】 为 0
     */
    @Update("update  sc_b_transfer set  TOT_QTY_OUT = 0,TOT_AMT_OUT_LIST=0,\n" +
            "modifieddate=#{date},modifierid=#{userid},modifiername=#{username},\n" +
            "modifierename=#{userename}  where  id  = #{id}")
    int updateMoneyAndNumById(@Param("id")Long id, @Param("userid")Integer userid,
                              @Param("username")  String name,  @Param("userename") String userename,@Param("date") Date date);
}