package com.jackrain.nea.sg.transfer.config;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * @author :csy
 * @version :1.0
 * description ：调拨单相关 mq config
 * @date :2019/11/27 10:37
 */
@Slf4j
@Configuration
@Data
public class SgTransferMqConfig {

    @Value("${r3.oms.retail.mq.topic}")
    private String retailTopic;

}
