package com.jackrain.nea.sg.adjust.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.jackrain.nea.ps.api.result.PsCProResult;
import com.jackrain.nea.sg.adjust.model.table.SgBAdjustItem;
import lombok.Data;

import java.io.Serializable;

/**
 * 财务结算用
 *
 * @author: 舒威
 * @since: 2019/8/1
 * create at : 2019/8/1 9:10
 */
@Data
public class AcAdjustItem extends SgBAdjustItem implements Serializable {

    @JSONField(name = "PRO_LABEL_JSON")
    private PsCProResult proLabelJson;
}
