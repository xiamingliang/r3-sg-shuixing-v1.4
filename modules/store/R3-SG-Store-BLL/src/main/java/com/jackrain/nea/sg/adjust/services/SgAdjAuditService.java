package com.jackrain.nea.sg.adjust.services;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.jackrain.nea.ac.sc.basic.model.enums.SourceBillType;
import com.jackrain.nea.ac.sc.basic.model.request.AcPushAcBillRequest;
import com.jackrain.nea.ac.sc.utils.utils.AcScOrigBillUtil;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.config.PropertiesConf;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.model.util.ModelUtil;
import com.jackrain.nea.oc.basic.common.OcBasicConstants;
import com.jackrain.nea.oc.basic.model.request.QueryBoxRequest;
import com.jackrain.nea.oc.basic.services.QueryBoxItemsService;
import com.jackrain.nea.ps.api.result.PsCProResult;
import com.jackrain.nea.psext.model.table.PsCTeusItem;
import com.jackrain.nea.redis.config.CusRedisTemplate;
import com.jackrain.nea.sg.adjust.common.SgAdjustConstants;
import com.jackrain.nea.sg.adjust.mapper.SgBAdjustImpItemMapper;
import com.jackrain.nea.sg.adjust.mapper.SgBAdjustItemMapper;
import com.jackrain.nea.sg.adjust.mapper.SgBAdjustMapper;
import com.jackrain.nea.sg.adjust.mapper.SgBAdjustTeusItemMapper;
import com.jackrain.nea.sg.adjust.model.AcAdjustItem;
import com.jackrain.nea.sg.adjust.model.request.SgAdjustBillAuditRequest;
import com.jackrain.nea.sg.adjust.model.result.SgAdjustBillQueryResult;
import com.jackrain.nea.sg.adjust.model.table.SgBAdjust;
import com.jackrain.nea.sg.adjust.model.table.SgBAdjustImpItem;
import com.jackrain.nea.sg.adjust.model.table.SgBAdjustItem;
import com.jackrain.nea.sg.adjust.model.table.SgBAdjustTeusItem;
import com.jackrain.nea.sg.adjust.utils.SgAdjustAsyncUtil;
import com.jackrain.nea.sg.basic.api.SgStorageNoTransUpdateCmd;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.config.SgStorageBoxConfig;
import com.jackrain.nea.sg.basic.model.request.*;
import com.jackrain.nea.sg.basic.model.table.CpCStore;
import com.jackrain.nea.sg.basic.utils.R3ParamUtil;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.phyadjust.model.table.SgBPhyAdjustImpItem;
import com.jackrain.nea.sg.send.common.SgSendConstantsIF;
import com.jackrain.nea.sg.send.utils.SgStoreESUtils;
import com.jackrain.nea.sg.send.utils.SgStoreUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.utility.ExceptionUtil;
import com.jackrain.nea.utils.AssertUtils;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @author csy
 */

@Slf4j
@Component
public class SgAdjAuditService {

    @Autowired
    private SgBAdjustMapper mapper;

    @Autowired
    private SgBAdjustItemMapper itemMapper;

    @Autowired
    private SgStoreESUtils storeESUtils;

    @Autowired
    private SgStorageBoxConfig storageBoxConfig;

    /**
     * 接口入参
     */
    @Transactional(rollbackFor = Exception.class)
    public ValueHolderV14 audit(SgAdjustBillAuditRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("Start SgAdjAuditService.audit. ReceiveParams:SgAdjustBillAuditRequest="
                    + JSONObject.toJSONString(request) + ";");
        }
        AssertUtils.notNull(request, "参数为空-request");
        User user = request.getUser();
        Long sourceId = request.getSourceBillId();
        Integer sourceBillType = request.getSourceBillType();
        AssertUtils.notNull(user, "参数为空-user");
        AssertUtils.notNull(sourceId, "参数为空-sourceId");
        AssertUtils.notNull(sourceBillType, "参数为空-sourceBillType");
        SgBAdjust adjust = mapper.selectOne(
                new QueryWrapper<SgBAdjust>().lambda()
                        .eq(SgBAdjust::getSourceBillId, sourceId)
                        .eq(SgBAdjust::getSourceBillType, sourceBillType));
        return audit(adjust, mapper, user);
    }


    @Transactional(rollbackFor = Exception.class)
    public ValueHolder audit(Long objId, User user) {
        if (log.isDebugEnabled()) {
            log.debug("Start SgAdjAuditService.audit. ReceiveParams:objId="
                    + objId + ";");
        }
        SgBAdjust adjust = mapper.selectById(objId);
        return R3ParamUtil.convertV14(audit(adjust, mapper, user));
    }

    /**
     * 审核
     *
     * @param user   用户
     * @param adjust 主表信息
     * @param mapper mapper
     */
    private ValueHolderV14 audit(SgBAdjust adjust, SgBAdjustMapper mapper, User user) {
        //===单据状态校验===
        Locale locale = user.getLocale();
        SgAdjustBillQueryResult checkResult = statusCheck(adjust, user);
        CusRedisTemplate<String, String> redisTemplate = SgStoreUtils.initRedisTemplate();
        String lockKsy = SgConstants.SG_B_ADJUST + ":" + adjust.getBillNo();
        if (redisTemplate.opsForValue().get(lockKsy) != null) {
            throw new NDSException(Resources.getMessage("当前记录正在被操作,请稍后重试!", user.getLocale()));
        } else {
            try {
                redisTemplate.opsForValue().set(lockKsy, "OK", 30, TimeUnit.MINUTES);
                Long objId = adjust.getId();
                //===跟新单据审核状态 审核人等相关信息===
                SgBAdjust updateAdj = new SgBAdjust();
                updateAdj.setId(objId);
                StorageESUtils.setBModelDefalutDataByUpdate(updateAdj, user);
                updateAdj.setModifierename(user.getEname());
                updateAdj.setBillStatus(SgAdjustConstants.ADJ_AUDIT_STATUS_Y);
                updateAdj.setCheckerId(user.getId().longValue());
                updateAdj.setCheckerEname(user.getEname());
                updateAdj.setCheckerName(user.getName());
                updateAdj.setCheckTime(new Date());
                //启用箱库存审核更新总数量
                if (storageBoxConfig.getBoxEnable()) {
                    List<SgBAdjustItem> adjustItems = checkResult.getAdjustItems();
                    AssertUtils.isNotEmpty("当前记录明细为空，不允许审核!", adjustItems);
                    BigDecimal totQty = adjustItems.stream().map(SgBAdjustItem::getQty).reduce(BigDecimal.ZERO, BigDecimal::add);
                    updateAdj.setTotQty(totQty);
                }
                //=== 调用库存服务 ==
                changeStorage(checkResult, user);
                mapper.updateById(updateAdj);
                try {
                    sendAcAdjustAndPushEs(objId, user, mapper);
                } catch (Exception e) {
                    e.printStackTrace();
                    log.error("生成结算日志并推送ES失败!" + e.getMessage());
                }
            } catch (Exception e) {
                AssertUtils.logAndThrow(ExceptionUtil.getMessage(e), locale);
            } finally {
                redisTemplate.delete(lockKsy);
            }
            return new ValueHolderV14(ResultCode.SUCCESS, SgConstants.MESSAGE_STATUS_SUCCESS);
        }
    }


    /**
     * 单据状态校验
     */
    private SgAdjustBillQueryResult statusCheck(SgBAdjust adjust, User user) {
        Locale locale = user.getLocale();
        AssertUtils.notNull(adjust, "当前单据已不存在!", locale);
        AssertUtils.notNull(adjust.getBillStatus(), "当数据错误-bill_status不存在!", locale);
        AssertUtils.cannot(adjust.getBillStatus() == SgAdjustConstants.ADJ_AUDIT_STATUS_Y, "当前单据已审核，不能重复审核!", locale);
        SgAdjustBillQueryResult checkResult = new SgAdjustBillQueryResult();
        checkResult.setAdjust(adjust);

        //异步删除明细中调整数量为0的记录
        //考虑到拆箱/装箱 剔除该逻辑
//        SgAdjustAsyncUtil asynUtil = ApplicationContextHandle.getBean(SgAdjustAsyncUtil.class);
//        asynUtil.deleteAdjustItemsByZero(adjust.getId());

        if (storageBoxConfig.getBoxEnable()) {
            SgBAdjustImpItemMapper impItemMapper = ApplicationContextHandle.getBean(SgBAdjustImpItemMapper.class);
            List<SgBAdjustImpItem> impItems = impItemMapper.selectList(new QueryWrapper<SgBAdjustImpItem>().lambda()
                    .eq(SgBAdjustImpItem::getSgBAdjustId, adjust.getId())
                    .ne(SgBAdjustImpItem::getQty, BigDecimal.ZERO));
            AssertUtils.isTrue(CollectionUtils.isNotEmpty(impItems), "调整单录入明细不能为空!");
            checkResult.setAdjustImpItems(impItems);
            impItemsConvertMethod(checkResult, adjust.getId(), user);
        }
        return checkResult;
    }

    /**
     * 录入明细转箱明细和条码明细
     *
     * @param checkResult 数据源
     * @param id          主表Id
     * @param user        操作用户
     */
    private void impItemsConvertMethod(SgAdjustBillQueryResult checkResult, Long id, User user) {
        List<SgBAdjustImpItem> impitemList = checkResult.getAdjustImpItems();
        List<SgBAdjustTeusItem> teusItemList = Lists.newArrayList();
        HashMap<Long, SgBAdjustItem> itemsMap = Maps.newHashMap();
        //箱id-箱明细request
        HashMap<String, SgBAdjustImpItem> teusMap = Maps.newHashMap();
        for (SgBAdjustImpItem impitem : impitemList) {
            if ((SgSendConstantsIF.IS_TEUS_Y).equals(impitem.getIsTeus())) {
                AssertUtils.isTrue(StringUtils.isNotEmpty(impitem.getPsCTeusEcode()), "录入明细" + impitem.getPsCProEcode() + ",箱号为空!");
                AssertUtils.isTrue(!teusMap.containsKey(impitem.getPsCTeusEcode()), "存在重复箱号" + impitem.getPsCTeusEcode() + "录入明细!");
                teusMap.put(impitem.getPsCTeusEcode(), impitem);
            } else {
                SgBAdjustItem item = new SgBAdjustItem();
                BeanUtils.copyProperties(impitem, item);
                item.setId(ModelUtil.getSequence(SgConstants.SG_B_PHY_ADJUST_ITEM));
                item.setSgBAdjustId(id);
                item.setOwnerename(user.getEname());
                item.setModifierename(user.getEname());
                StorageESUtils.setBModelDefalutData(item, user);
                itemsMap.put(item.getPsCSkuId(), item);
            }
        }

        if (CollectionUtils.isNotEmpty(teusMap.keySet())) {
            //获取箱内明细，并且 箱内明细转换成条码明细
            try {
                QueryBoxItemsService queryTeusService = ApplicationContextHandle.getBean(QueryBoxItemsService.class);
                QueryBoxRequest boxRequest = new QueryBoxRequest();
                List<String> teusEcodes = new ArrayList<>(teusMap.keySet());
                boxRequest.setBoxECodes(teusEcodes);
                boxRequest.setBoxStatus(OcBasicConstants.BOX_STATUS_IN);
                List<PsCTeusItem> psCTeusItems = queryTeusService.queryBoxItemsByBoxInfo(boxRequest);
                if (CollectionUtils.isNotEmpty(psCTeusItems)) {
                    for (PsCTeusItem psCTeusItem : psCTeusItems) {
                        SgBAdjustTeusItem teusItem = new SgBAdjustTeusItem();
                        BeanUtils.copyProperties(psCTeusItem, teusItem);
                        //给箱内明细赋值商品信息
                        SgBAdjustImpItem impItem = teusMap.get(psCTeusItem.getPsCTeusEcode());
                        //R3矩阵保存没有箱id,在此补充
                        if (impItem.getPsCTeusId() == null) {
                            impItem.setPsCTeusId(psCTeusItem.getPsCTeusId());
                        }
                        StorageESUtils.setBModelDefalutData(impItem, user);
                        teusItem.setQty(impItem.getQty().multiply(teusItem.getQty()));
                        //teusItem.setSourceBillItemId(impItem.getSourceBillItemId());
                        teusItem.setPsCProId(impItem.getPsCProId());
                        teusItem.setPsCProEcode(impItem.getPsCProEcode());
                        teusItem.setPsCProEname(impItem.getPsCProEname());
                        teusItem.setId(ModelUtil.getSequence(SgConstants.SG_B_ADJUST_TEUS_ITEM));
                        teusItem.setSgBAdjustId(id);
                        teusItem.setCpCStoreId(impItem.getCpCStoreId());
                        teusItem.setCpCStoreEcode(impItem.getCpCStoreEcode());
                        teusItem.setCpCStoreEname(impItem.getCpCStoreEname());
                        teusItem.setOwnerename(user.getEname());
                        teusItem.setModifierename(user.getEname());
                        teusItemList.add(teusItem);
                    }
                    //箱内明细转条码明细
                    for (SgBAdjustTeusItem teusItem : teusItemList) {
                        Long psCSkuId = teusItem.getPsCSkuId();
                        SgBAdjustImpItem impItem = teusMap.get(teusItem.getPsCTeusEcode());
                        if (itemsMap.containsKey(psCSkuId)) {
                            SgBAdjustItem item = itemsMap.get(psCSkuId);
                            BigDecimal qty = Optional.ofNullable(teusItem.getQty()).orElse(BigDecimal.ZERO);
                            item.setQty(qty.add(item.getQty()));
                        } else {
                            SgBAdjustItem item = new SgBAdjustItem();
                            BeanUtils.copyProperties(teusItem, item);
                            item.setId(ModelUtil.getSequence(SgConstants.SG_B_PHY_ADJUST_ITEM));
                            item.setCpCStoreId(impItem.getCpCStoreId());
                            item.setCpCStoreEcode(impItem.getCpCStoreEcode());
                            item.setCpCStoreEcode(impItem.getCpCStoreEname());
                            item.setSgBAdjustId(id);
                            item.setOwnerename(user.getEname());
                            item.setModifierename(user.getEname());
                            StorageESUtils.setBModelDefalutData(item, user);
                            itemsMap.put(psCSkuId, item);
                        }
                    }
                } else {
                    AssertUtils.logAndThrow("根据箱号获取箱明细为空!");
                }
            } catch (Exception e) {
                e.printStackTrace();
                AssertUtils.logAndThrow("解析箱明细异常!" + e.getMessage());
            }
        }

        if (CollectionUtils.isNotEmpty(teusItemList)) {
            SgBAdjustTeusItemMapper teusItemMapper = ApplicationContextHandle.getBean(SgBAdjustTeusItemMapper.class);
            SgStoreUtils.batchInsertTeus(teusItemList, "库存调整单箱内明细", SgConstants.SG_COMMON_ITEM_INSERT_PAGE_SIZE, teusItemMapper);
            checkResult.setAdjustTeusItems(teusItemList);
        }

        if (MapUtils.isNotEmpty(itemsMap)) {
            List<SgBAdjustItem> itemList = new ArrayList<>(itemsMap.values());
            SgStoreUtils.batchInsertTeus(itemList, "库存调整单条码明细", SgConstants.SG_COMMON_ITEM_INSERT_PAGE_SIZE, itemMapper);
            checkResult.setAdjustItems(itemList);
        }
    }

    /**
     * 调用库存服务
     */
    private void changeStorage(SgAdjustBillQueryResult checkResult, User user) {
        Locale locale = user.getLocale();
        Long startTime = System.currentTimeMillis();
        SgBAdjust adjust = checkResult.getAdjust();
        List<SgBAdjustItem> items = checkResult.getAdjustItems();
        List<SgBAdjustImpItem> impItems = checkResult.getAdjustImpItems();
        List<SgBAdjustTeusItem> teusItems = checkResult.getAdjustTeusItems();

        SgStorageSingleUpdateRequest request = new SgStorageSingleUpdateRequest();
        if (CollectionUtils.isNotEmpty(items)) {
            SgStorageUpdateBillRequest stockRequest = new SgStorageUpdateBillRequest();
            stockRequest.setBillDate(adjust.getBillDate());
            stockRequest.setBillId(adjust.getId());
            stockRequest.setBillNo(adjust.getBillNo());
            stockRequest.setBillType(SgConstantsIF.BILL_TYPE_ADJUST);
            stockRequest.setChangeDate(new Date());
            stockRequest.setIsCancel(false);
            stockRequest.setServiceNode(SgConstantsIF.SERVICE_NODE_ADJUST_SUBMIT);

            //2019-09-06 逻辑调整单增加负库存控制
            Set<Long> stores = items.stream().map(SgBAdjustItem::getCpCStoreId).collect(Collectors.toSet());
            Map<Long, CpCStore> negativeStock = SgStoreUtils.getNegativeStock(new ArrayList<>(stores));
            //若明细只有一个逻辑仓，在最外层加负库存控制即可
            if (stores.size() == 1 && MapUtils.isNotEmpty(negativeStock)) {
                SgStorageUpdateControlRequest controlRequest = new SgStorageUpdateControlRequest();
                CpCStore cpCStore = negativeStock.get(items.get(0).getCpCStoreId());
                Long isnegative = cpCStore.getIsnegative();
                isnegative = isnegative == null ? 1L : isnegative;
                controlRequest.setNegativeStorage(isnegative == 0);
                controlRequest.setNegativeAvailable(isnegative == 0);
                request.setControlModel(controlRequest);
            }

            //调整录入明细-箱
            Map<Long, BigDecimal> skuTeusCounts = Maps.newHashMap();
            if (storageBoxConfig.getBoxEnable()) {
                if (CollectionUtils.isNotEmpty(teusItems)) {
                    skuTeusCounts = teusItems.stream().collect(Collectors.groupingBy(SgBAdjustTeusItem::getPsCSkuId,
                            Collectors.reducing(BigDecimal.ZERO, SgBAdjustTeusItem::getQty, BigDecimal::add)));
                }

                List<SgTeusStorageUpdateBillItemRequest> teusList = Lists.newArrayList();
                impItems.stream()
                        .filter(o -> o.getIsTeus() != null && o.getIsTeus().equals(OcBasicConstants.IS_MATCH_SIZE_Y))
                        .forEach(o -> {
                            SgTeusStorageUpdateBillItemRequest teusStorageUpdateBillItemRequest = new SgTeusStorageUpdateBillItemRequest();
                            BeanUtils.copyProperties(o, teusStorageUpdateBillItemRequest);
                            teusStorageUpdateBillItemRequest.setBillItemId(o.getId());
                            teusStorageUpdateBillItemRequest.setStorageType(SgConstantsIF.STORAGE_TYPE_STORAGE);
                            teusStorageUpdateBillItemRequest.setQtyStorageChange(o.getQty());
                            teusStorageUpdateBillItemRequest.setCpCStoreId(o.getCpCStoreId());
                            teusStorageUpdateBillItemRequest.setCpCStoreEcode(o.getPsCProEcode());
                            teusStorageUpdateBillItemRequest.setCpCStoreEname(o.getCpCStoreEname());
                            //若明细 存在多个逻辑仓 需要每条明细都去check下负库存控制
                            if (stores.size() > 1 && MapUtils.isNotEmpty(negativeStock) && negativeStock.containsKey(o.getCpCStoreId())) {
                                SgStorageUpdateControlRequest controlRequest = new SgStorageUpdateControlRequest();
                                CpCStore cpCStore = negativeStock.get(o.getCpCStoreId());
                                Long isnegative = cpCStore.getIsnegative();
                                isnegative = isnegative == null ? 1L : isnegative;
                                controlRequest.setNegativeStorage(isnegative == 0);
                                controlRequest.setNegativeAvailable(isnegative == 0);
                                teusStorageUpdateBillItemRequest.setControlmodel(controlRequest);
                            }
                            teusList.add(teusStorageUpdateBillItemRequest);
                        });
                stockRequest.setTeusList(teusList);
            }

            //调整明细
            List<SgStorageUpdateBillItemRequest> itemRequests = new ArrayList<>();
            for (SgBAdjustItem adjustItem : items) {
                SgStorageUpdateBillItemRequest itemRequest = new SgStorageUpdateBillItemRequest();
                BeanUtils.copyProperties(adjustItem, itemRequest);
                itemRequest.setBillItemId(adjustItem.getId());
                itemRequest.setCpCStoreId(adjustItem.getCpCStoreId());
                itemRequest.setPsCSkuId(adjustItem.getPsCSkuId());
                itemRequest.setQtyStorageChange(adjustItem.getQty());
                itemRequest.setStorageType(SgConstantsIF.STORAGE_TYPE_STORAGE);
                //若明细 存在多个逻辑仓 需要每条明细都去check下负库存控制
                if (stores.size() > 1 && MapUtils.isNotEmpty(negativeStock) && negativeStock.containsKey(adjustItem.getCpCStoreId())) {
                    SgStorageUpdateControlRequest controlRequest = new SgStorageUpdateControlRequest();
                    CpCStore cpCStore = negativeStock.get(adjustItem.getCpCStoreId());
                    Long isnegative = cpCStore.getIsnegative();
                    isnegative = isnegative == null ? 1L : isnegative;
                    controlRequest.setNegativeStorage(isnegative == 0);
                    controlRequest.setNegativeAvailable(isnegative == 0);
                    itemRequest.setControlmodel(controlRequest);
                }
                BigDecimal teusQty = skuTeusCounts.get(adjustItem.getPsCSkuId());
                if (teusQty != null) {
                    itemRequest.setQtyStorageTeusChange(teusQty);
                }
                itemRequests.add(itemRequest);
            }
            stockRequest.setItemList(itemRequests);
            request.setBill(stockRequest);
            request.setLoginUser(user);
            request.setMessageKey(SgConstants.MSG_TAG_ADJUST + ":" + adjust.getBillNo());
            SgStorageNoTransUpdateCmd service = (SgStorageNoTransUpdateCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                    SgStorageNoTransUpdateCmd.class.getName(), SgConstantsIF.GROUP, SgConstantsIF.VERSION);
            ValueHolderV14 holderV14 = service.updateStorageBillNoTrans(request);
            if (log.isDebugEnabled()) {
                log.debug("Finish SgStorageNoTransUpdateService.updateStorageBill. Return:Results:{} spend time:{};",
                        holderV14.toJSONObject(), System.currentTimeMillis() - startTime);
            }
            if (holderV14 == null) {
                throw new NDSException(Resources.getMessage("库存调用失败!-同步调用返回为空", locale));
            }
            AssertUtils.cannot(holderV14.getCode() == ResultCode.FAIL, "库存调用失败：" + holderV14.getMessage(), locale);
        }
    }

    /**
     * 获取结算日志明细
     */
    public List<AcAdjustItem> getAcAdjustitems(List<SgBAdjustItem> items) {
        Set<String> proEcodes = items.stream().map(SgBAdjustItem::getPsCProEcode).collect(Collectors.toSet());
        List<String> pros = new ArrayList<>(proEcodes);

        //分批获取商品信息  500/次
        List<PsCProResult> littleProInfo = StorageESUtils.getPros(pros, SgConstants.SG_COMMON_INSERT_PAGE_SIZE);

        //构造结算所需明细
        Map<String, PsCProResult> proResultMap = Maps.newHashMap();
        for (PsCProResult psCProResult : littleProInfo) {
            String ecode = psCProResult.getEcode();
            if (StringUtils.isEmpty(ecode)) {
                AssertUtils.logAndThrow("补充商品信息ecode为空!");
            }
            if (!proResultMap.containsKey(ecode)) {
                proResultMap.put(ecode, psCProResult);
            }
        }

        List<AcAdjustItem> adjustItems = JSON.parseArray(JSON.toJSONString(items), AcAdjustItem.class);
        for (AcAdjustItem adjustItem : adjustItems) {
            adjustItem.setProLabelJson(proResultMap.get(adjustItem.getPsCProEcode()));
        }

        return adjustItems;
    }

    public void sendAcAdjustAndPushEs(Long objid, User user, SgBAdjustMapper mapper) {
        if (log.isDebugEnabled()) {
            log.debug("Start SgAdjAuditService.pushAcAdjust. ReceiveParams:objId=" + objid + ";");
        }
        SgBAdjust acAdjust = mapper.selectById(objid);
        SgBAdjust updateAdj = new SgBAdjust();
        try {
            // 2019-07-23添加逻辑：生成结算日志
            PropertiesConf pconf = ApplicationContextHandle.getBean(PropertiesConf.class);
            String isSynch = pconf.getProperty(SgAdjustConstants.R3_IS_SYNCH_AC_SC_BILL_CONVERT);
            if (log.isDebugEnabled()) {
                log.debug("R3_IS_SYNCH_AC_SC_BILL_CONVERT:" + isSynch);
            }
            if (Boolean.valueOf(isSynch)) {
                SgBAdjustItemMapper itemMapper = ApplicationContextHandle.getBean(SgBAdjustItemMapper.class);
                List<SgBAdjustItem> items = itemMapper.selectList(new QueryWrapper<SgBAdjustItem>().lambda()
                        .eq(SgBAdjustItem::getSgBAdjustId, acAdjust.getId()));
                //获取结算日志明细
                List<AcAdjustItem> adjustItems = getAcAdjustitems(items);
                AcPushAcBillRequest billRequest = new AcPushAcBillRequest();
                billRequest.setBillType(SourceBillType.ADJUST);
                billRequest.setSourceBillNo(acAdjust.getBillNo());
                Date sourceBillDate = Optional.ofNullable(acAdjust.getCheckTime()).orElse(new Date());
                billRequest.setSourceBillDate(sourceBillDate);
                JSONObject bill = new JSONObject();
                bill.put(SgConstants.SG_B_ADJUST.toUpperCase(), acAdjust);
                bill.put(SgConstants.SG_B_ADJUST_ITEM.toUpperCase(), adjustItems);
                Long propId = acAdjust.getSgBAdjustPropId();
                //库存调整性质是无头件、冲无头件、销退错录、分销错录，写死经销商编码和名称
                if (propId != null && (propId.equals(SgAdjustConstants.SG_B_ADJUST_PROP_NO_SOURCE_IN) ||
                        propId.equals(SgAdjustConstants.SG_B_ADJUST_PROP_FLUSH_NO_SOURCE) ||
                        propId.equals(SgAdjustConstants.SG_B_ADJUST_PROP_SALES_RETURN_FAIL_RECORD) ||
                        propId.equals(SgAdjustConstants.SG_B_ADJUST_PROP_DISTRIB_SALES_FAIL_RECORD))) {
                    bill.put("CP_C_CUSTOMER_ECODE", "WTKH");
                    bill.put("CP_C_CUSTOMER_ENAME", "无头客户");
                } else if (propId != null && propId.equals(SgAdjustConstants.SG_B_ADJUST_PROP_VIP_RETURN)) {
                    bill.put("CP_C_CUSTOMER_ECODE", "10070");
                    bill.put("CP_C_CUSTOMER_ENAME", "唯品会（中国）有限公司");
                }
                billRequest.setBill(bill);
                billRequest.setUser(user);
                if (log.isDebugEnabled()) {
                    log.debug("调整单生成结算日志入参：" + JSONObject.toJSONString(billRequest));
                }
                ValueHolderV14 v14 = AcScOrigBillUtil.pushAcBill(billRequest);
                //记录发送结算日志状态 成功
                updateAdj.setReserveBigint01(2L);
                log.info("调整单生成结算日志:" + v14);
            }
        } catch (Exception e) {
            //记录发送结算日志状态 失败
            updateAdj.setReserveBigint01(3L);
            log.error("生成结算日志异常!" + e.getMessage());
        }
        //更新发送结算日志状态
        updateAdj.setId(acAdjust.getId());
        StorageESUtils.setBModelDefalutDataByUpdate(updateAdj, user);
        updateAdj.setModifierename(user.getEname());
        mapper.updateById(updateAdj);

        acAdjust.setReserveBigint01(updateAdj.getReserveBigint01());

        //推送ES
        storeESUtils.pushESByAdjust(objid, false, false, null, mapper, null);
    }
}

