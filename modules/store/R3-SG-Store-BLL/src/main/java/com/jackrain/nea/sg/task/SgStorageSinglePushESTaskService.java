package com.jackrain.nea.sg.task;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.adjust.common.SgAdjustConstants;
import com.jackrain.nea.sg.adjust.mapper.SgBAdjustItemMapper;
import com.jackrain.nea.sg.adjust.mapper.SgBAdjustMapper;
import com.jackrain.nea.sg.adjust.model.table.SgBAdjust;
import com.jackrain.nea.sg.adjust.model.table.SgBAdjustItem;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.receive.common.SgReceiveConstants;
import com.jackrain.nea.sg.receive.mapper.SgBReceiveItemMapper;
import com.jackrain.nea.sg.receive.mapper.SgBReceiveMapper;
import com.jackrain.nea.sg.receive.model.table.SgBReceive;
import com.jackrain.nea.sg.receive.model.table.SgBReceiveItem;
import com.jackrain.nea.sg.send.common.SgSendConstants;
import com.jackrain.nea.sg.send.mapper.SgBSendItemMapper;
import com.jackrain.nea.sg.send.mapper.SgBSendMapper;
import com.jackrain.nea.sg.send.model.table.SgBSend;
import com.jackrain.nea.sg.send.model.table.SgBSendItem;
import com.jackrain.nea.sg.transfer.common.ScTransferConstants;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferItemMapper;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferMapper;
import com.jackrain.nea.sg.transfer.model.table.ScBTransfer;
import com.jackrain.nea.sg.transfer.model.table.ScBTransferItem;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author 舒威
 * @since 2019/8/2
 * create at : 2019/8/2 14:43
 */
@Slf4j
@Component
public class SgStorageSinglePushESTaskService {


    /**
     * 刷新逻辑仓相关业务ES-指定id
     */
    public ValueHolderV14 pushSingleStoreES(JSONObject params) {
        if (log.isDebugEnabled()) {
            log.debug("Start Single Push ES Store . ReceiveParams:params:{};", JSONObject.toJSONString(params));
        }
        ValueHolderV14 result = new ValueHolderV14(ResultCode.SUCCESS, "message");
        long startTime = System.currentTimeMillis();
        if (params != null) {
            String table = params.getString("table");
            JSONArray ids = params.getJSONArray("ids");
            if (CollectionUtils.isNotEmpty(ids)) {
                switch (table) {
                    case SgConstants.SG_B_SEND:
                        pushSingleESSend(ids);
                        break;
                    case SgConstants.SG_B_RECEIVE:
                        pushSingleESReceive(ids);
                        break;
                    case SgConstants.SG_B_ADJUST:
                        pushSingleESAdjust(ids);
                        break;
                    case SgConstants.SG_B_TRANSFER:
                        pushSingleESTransfer(ids);
                        break;
                    default:
                        log.debug(table + "暂不支持推送es!");
                }
            }
        }

        if (log.isDebugEnabled()) {
            log.debug("Finish  Batch Push ES Store: spend time:{}ms;", System.currentTimeMillis() - startTime);
        }

        return result;
    }


    /**
     * 刷新调拨单es
     */
    private void pushSingleESTransfer(JSONArray ids) {
        if (log.isDebugEnabled()) {
            log.debug("Start Single Push ES Transfer");
        }

        long startTime = System.currentTimeMillis();
        ScBTransferMapper transferMapper = ApplicationContextHandle.getBean(ScBTransferMapper.class);
        ScBTransferItemMapper transferItemMapper = ApplicationContextHandle.getBean(ScBTransferItemMapper.class);
        String index = SgConstants.SG_B_TRANSFER;
        String childType = SgConstants.SG_B_TRANSFER_ITEM;

        //批量推送主表
        List<ScBTransfer> transfers = transferMapper.selectList(new QueryWrapper<ScBTransfer>().lambda().in(ScBTransfer::getId, ids));
        StorageESUtils.singlePushES(transfers, ids, index, null, null, ScBTransfer.class, ScBTransferItem.class);

        //批量推送明细表
        List<ScBTransferItem> transferitems = transferItemMapper.selectList(new QueryWrapper<ScBTransferItem>().lambda().in(ScBTransferItem::getScBTransferId, ids));
        StorageESUtils.singlePushES(transferitems, ids, index, childType, ScTransferConstants.TRANSFER_PARENT_FIELD.toUpperCase(), ScBTransfer.class, ScBTransferItem.class);

        if (log.isDebugEnabled()) {
            log.debug("Finish  Batch Single ES Transfer: spend time:{}ms;", System.currentTimeMillis() - startTime);
        }
    }

    /**
     * 刷新逻辑调整单es
     */
    private void pushSingleESAdjust(JSONArray ids) {
        if (log.isDebugEnabled()) {
            log.debug("Start Single Push ES Adjust");
        }

        long startTime = System.currentTimeMillis();
        SgBAdjustMapper adjustMapper = ApplicationContextHandle.getBean(SgBAdjustMapper.class);
        SgBAdjustItemMapper adjustItemMapper = ApplicationContextHandle.getBean(SgBAdjustItemMapper.class);
        String index = SgConstants.SG_B_ADJUST;
        String childType = SgConstants.SG_B_ADJUST_ITEM;

        //批量推送主表
        List<SgBAdjust> adjusts = adjustMapper.selectList(new QueryWrapper<SgBAdjust>().lambda().in(SgBAdjust::getId, ids));
        StorageESUtils.singlePushES(adjusts, ids, index, null, null, SgBAdjust.class, SgBAdjustItem.class);

        //批量推送明细表
        List<SgBAdjustItem> adjustitems = adjustItemMapper.selectList(new QueryWrapper<SgBAdjustItem>().lambda().in(SgBAdjustItem::getSgBAdjustId, ids));
        StorageESUtils.singlePushES(adjustitems, ids, index, childType, SgAdjustConstants.SG_B_ADJUST_ID.toUpperCase(), SgBAdjust.class, SgBAdjustItem.class);

        if (log.isDebugEnabled()) {
            log.debug("Finish  Single Push ES Adjust: spend time:{}ms;", System.currentTimeMillis() - startTime);
        }
    }

    /**
     * 刷新逻辑收货单es
     */
    private void pushSingleESReceive(JSONArray ids) {
        if (log.isDebugEnabled()) {
            log.debug("Start Single Push ES Receive");
        }

        long startTime = System.currentTimeMillis();
        SgBReceiveMapper receiveMapper = ApplicationContextHandle.getBean(SgBReceiveMapper.class);
        SgBReceiveItemMapper receiveItemMapper = ApplicationContextHandle.getBean(SgBReceiveItemMapper.class);
        String index = SgConstants.SG_B_RECEIVE;
        String childType = SgConstants.SG_B_RECEIVE_ITEM;

        //批量推送主表
        List<SgBReceive> receives = receiveMapper.selectList(new QueryWrapper<SgBReceive>().lambda().in(SgBReceive::getId, ids));
        StorageESUtils.singlePushES(receives, ids, index, null, null, SgBReceive.class, SgBReceiveItem.class);

        //批量推送明细表
        List<SgBReceiveItem> receiveitems = receiveItemMapper.selectList(new QueryWrapper<SgBReceiveItem>().lambda().in(SgBReceiveItem::getSgBReceiveId, ids));
        StorageESUtils.singlePushES(receiveitems, ids, index, childType, SgReceiveConstants.BILL_RECEIVE_ID.toUpperCase(), SgBReceive.class, SgBReceiveItem.class);

        if (log.isDebugEnabled()) {
            log.debug("Finish  Single Push ES Receive: spend time:{}ms;", System.currentTimeMillis() - startTime);
        }
    }

    /**
     * 刷新逻辑发货单es
     */
    private void pushSingleESSend(JSONArray ids) {
        if (log.isDebugEnabled()) {
            log.debug("Start Single Push ES Send");
        }

        long startTime = System.currentTimeMillis();
        SgBSendMapper sendMapper = ApplicationContextHandle.getBean(SgBSendMapper.class);
        SgBSendItemMapper sendItemMapper = ApplicationContextHandle.getBean(SgBSendItemMapper.class);
        String index = SgConstants.SG_B_SEND;
        String childType = SgConstants.SG_B_SEND_ITEM;

        List<SgBSend> sends = sendMapper.selectList(new QueryWrapper<SgBSend>().lambda().in(SgBSend::getId, ids));
        StorageESUtils.singlePushES(sends, ids, index, null, null, SgBSend.class, SgBSendItem.class);

        List<SgBSendItem> senditems = sendItemMapper.selectList(new QueryWrapper<SgBSendItem>().lambda().in(SgBSendItem::getSgBSendId, ids));
        StorageESUtils.singlePushES(senditems, ids, index, childType, SgSendConstants.BILL_SEND_ID.toUpperCase(), SgBSend.class, SgBSendItem.class);

        if (log.isDebugEnabled()) {
            log.debug("Finish  Single Push ES Send: spend time:{}ms;", System.currentTimeMillis() - startTime);
        }
    }

}
