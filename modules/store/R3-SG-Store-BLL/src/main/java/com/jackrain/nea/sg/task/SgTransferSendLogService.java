package com.jackrain.nea.sg.task;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.jackrain.nea.ac.sc.basic.model.enums.SourceBillType;
import com.jackrain.nea.ac.sc.basic.model.request.AcPushAcBillRequest;
import com.jackrain.nea.ac.sc.utils.utils.AcScOrigBillUtil;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.data.basic.model.request.ProInfoQueryRequest;
import com.jackrain.nea.data.basic.services.BasicPsQueryService;
import com.jackrain.nea.es.util.ElasticSearchUtil;
import com.jackrain.nea.ps.api.result.PsCProResult;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.transfer.common.ScTransferConstants;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferItemMapper;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferMapper;
import com.jackrain.nea.sg.transfer.model.AcTransferItemRequest;
import com.jackrain.nea.sg.transfer.model.table.ScBTransfer;
import com.jackrain.nea.sg.transfer.model.table.ScBTransferItem;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.face.impl.UserImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author leexh
 * @since 2019/8/12 13:19
 * desc: 捞取生成结算日志失败的调拨单，重新生成结算日志
 */
@Slf4j
@Component
public class SgTransferSendLogService {

    public ValueHolderV14 sendLogForTransfer(JSONObject params) {

        ValueHolderV14 v14 = new ValueHolderV14();
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ",调拨单生成结算日志任务---开始执行");
        }

        ScBTransferMapper transferMapper = ApplicationContextHandle.getBean(ScBTransferMapper.class);
        ScBTransferItemMapper itemMapper = ApplicationContextHandle.getBean(ScBTransferItemMapper.class);

        boolean isNext = true;
        long startId = 0L;

        //查询参数
        int pageSize = SgConstants.SG_COMMON_INSERT_PAGE_SIZE;
        JSONObject whereKey = createParam(params);
        whereKey.put("pageSize", pageSize);
        long startTime = System.currentTimeMillis();
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ",调拨单补发结算日志任务开始执行:" + startTime);
        }

        List<ScBTransfer> transferOutList = new ArrayList<>();
        // 捞取出库完成发送结算失败的数据进行补发
        while (isNext) {
            whereKey.put("startId", startId);
            List<ScBTransfer> transfers = transferMapper.selectTransferByLogOut(whereKey);
            if (CollectionUtils.isNotEmpty(transfers)) {
                startId = transfers.get(transfers.size() - 1).getId();
                transferOutList.addAll(transfers);
            } else {
                isNext = false;
            }
        }

        if (transferOutList.size() > 0) {
            List<AcPushAcBillRequest> billList = new ArrayList<>();
            for (ScBTransfer transfer : transferOutList) {
                Long id = transfer.getId();
                Integer count = itemMapper.selectCount(new QueryWrapper<ScBTransferItem>().lambda().
                        eq(ScBTransferItem::getScBTransferId, id));

                List<ScBTransferItem> transferItems = new ArrayList<>();
                int pageNum = count / pageSize;
                if (count % pageSize != 0) pageNum++;
                for (int i = 0; i < pageNum; i++) {
                    transferItems.addAll(itemMapper.selectItemListByPage(id, pageSize, i * pageSize));
                }

                // 构造结算日志入参
                AcPushAcBillRequest billParam = createAcBillParam(transfer, transferItems, SourceBillType.TRANSFER_OUT,
                        getRootUser(), transfer.getOutDate());
                if (billParam != null) {
                    billList.add(billParam);
                }
            }

            // 生成结算日志
            if (billList.size() > 0) {
                ValueHolderV14 outV14 = AcScOrigBillUtil.batchPushAcBill(billList);
                log.debug(this.getClass().getName() + ",batchPushAcBill:" + JSON.toJSONString(outV14));
                List<Long> ids = transferOutList.stream().map(ScBTransfer::getId).collect(Collectors.toList());
                if (outV14.getCode() == ResultCode.SUCCESS) {
                    updateTransfer(getRootUser(), ids, transferMapper, ScTransferConstants.ACTION_OUT, true);
                } else {
                    updateTransfer(getRootUser(), ids, transferMapper, ScTransferConstants.ACTION_OUT, false);
                }
            }
        }

        long outTime = System.currentTimeMillis();
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ",调拨单出库补发结算日志任务结束:" +
                    System.currentTimeMillis() + ",耗时:" + (outTime - startTime) / 1000 + "s");
        }

        // 捞取入库完成发送结算失败的数据进行补发
        isNext = true;
        startId = 0L;
        List<ScBTransfer> transferInList = new ArrayList<>();
        while (isNext) {
            whereKey.put("startId", startId);
            List<ScBTransfer> transfers = transferMapper.selectTransferByLogIn(whereKey);
            if (CollectionUtils.isNotEmpty(transfers)) {
                startId = transfers.get(transfers.size() - 1).getId();
                transferInList.addAll(transfers);
            } else {
                isNext = false;
            }
        }

        if (transferInList.size() > 0) {
            List<AcPushAcBillRequest> billList = new ArrayList<>();
            for (ScBTransfer transfer : transferInList) {
                Long id = transfer.getId();
                Integer count = itemMapper.selectCount(new QueryWrapper<ScBTransferItem>().lambda().
                        eq(ScBTransferItem::getScBTransferId, id));

                List<ScBTransferItem> transferItems = new ArrayList<>();
                int pageNum = count / pageSize;
                if (count % pageSize != 0) pageNum++;
                for (int i = 0; i < pageNum; i++) {
                    transferItems.addAll(itemMapper.selectItemListByPage(id, pageSize, i * pageSize));
                }

                // 构造结算日志入参
                AcPushAcBillRequest billParam = createAcBillParam(transfer, transferItems, SourceBillType.TRANSFER_IN,
                        getRootUser(), transfer.getInDate());
                if (billParam != null) {
                    billList.add(billParam);
                }
            }

            // 生成结算日志
            if (billList.size() > 0) {
                ValueHolderV14 inV14 = AcScOrigBillUtil.batchPushAcBill(billList);
                log.debug(this.getClass().getName() + ",batchPushAcBill:" + JSON.toJSONString(inV14));
                List<Long> ids = transferInList.stream().map(ScBTransfer::getId).collect(Collectors.toList());
                if (inV14.getCode() == ResultCode.SUCCESS) {
                    updateTransfer(getRootUser(), ids, transferMapper, ScTransferConstants.ACTION_IN, true);
                } else {
                    updateTransfer(getRootUser(), ids, transferMapper, ScTransferConstants.ACTION_IN, false);
                }
            }
        }

        long endTime = System.currentTimeMillis();
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ",调拨单入库补发结算日志任务结束:" +
                    System.currentTimeMillis() + ",耗时:" + (endTime - outTime) / 1000 + "s,总耗时:" +
                    (endTime - outTime) / 1000 + "s");
        }

        v14.setCode(ResultCode.SUCCESS);
        v14.setMessage("执行成功！");
        return v14;
    }

    /**
     * 更新调拨单发送结算日志状态
     *
     * @param user           用户信息
     * @param idList         调拨单id集合
     * @param transferMapper mapper
     * @param action         出or入
     */
    private void updateTransfer(User user, List<Long> idList, ScBTransferMapper transferMapper, String action, boolean isSuccess) {

        try {
            int pageSize = SgConstants.SG_COMMON_INSERT_PAGE_SIZE;
            int idsNum = idList.size();
            int pageNum = idsNum / pageSize;
            if (idsNum % pageSize != 0) {
                pageNum++;
            }

            // 批量更新调拨单
            List<List<Long>> pageList = StorageESUtils.averageAssign(idList, pageNum);
            for (List<Long> ids : pageList) {
                ScBTransfer updateTransfer = new ScBTransfer();
                updateTransfer.setModifierid(user.getId().longValue());
                updateTransfer.setModifiername(user.getName());
                updateTransfer.setModifierename(user.getEname());
                updateTransfer.setModifieddate(new Timestamp(System.currentTimeMillis()));
                if (StringUtils.equals(action, ScTransferConstants.ACTION_OUT)) {
                    updateTransfer.setIsOutSettleLog(isSuccess ? ScTransferConstants.SEND_MQ_SUCCESS :
                            ScTransferConstants.SEND_MQ_FAIL);
                } else if (StringUtils.equals(action, ScTransferConstants.ACTION_IN)) {
                    updateTransfer.setIsInSettleLog(isSuccess ? ScTransferConstants.SEND_MQ_SUCCESS :
                            ScTransferConstants.SEND_MQ_FAIL);
                }

                int update = transferMapper.update(updateTransfer, new UpdateWrapper<ScBTransfer>().lambda()
                        .in(ScBTransfer::getId, ids));
                if (update != ids.size()) {
                    if (log.isDebugEnabled()) {
                        log.debug(this.getClass().getName() + ",updateTransfer,更新失败！");
                    }
                }

                // 批量推送es
                List<ScBTransfer> transfers = transferMapper.selectList(new QueryWrapper<ScBTransfer>().lambda()
                        .in(ScBTransfer::getId, ids));
                String index = SgConstants.SG_B_TRANSFER.toLowerCase();
                ElasticSearchUtil.indexDocuments(index, index, transfers);
            }
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error(this.getClass().getName() + ",updateTransfer,更新异常:" + e.getMessage(), e);
            }
        }
    }

    /**
     * 构造发送结算日志入参
     *
     * @param scBTransfer   调拨单
     * @param transferItems 明细
     * @param billType      类型
     * @param user          用户
     * @return 封装后的入参
     */
    private AcPushAcBillRequest createAcBillParam(ScBTransfer scBTransfer, List<ScBTransferItem> transferItems,
                                                  SourceBillType billType, User user, Date sourceBillDate) {

        AcPushAcBillRequest billRequest = new AcPushAcBillRequest();
        try {
            billRequest.setBillType(billType);
            billRequest.setSourceBillNo(scBTransfer.getBillNo());
            billRequest.setSourceBillDate(sourceBillDate);
            JSONObject bill = new JSONObject();
            bill.put(SgConstants.SG_B_TRANSFER.toUpperCase(), scBTransfer);

            BasicPsQueryService queryService = ApplicationContextHandle.getBean(BasicPsQueryService.class);
            ProInfoQueryRequest queryRequest = new ProInfoQueryRequest();

            List<String> proCodeList = transferItems.stream().map(ScBTransferItem::getPsCProEcode).collect(Collectors.toList());
            List<PsCProResult> proInfoList = new ArrayList<>();

            int itemNum = proCodeList.size();
            int pageSize = SgConstants.SG_COMMON_INSERT_PAGE_SIZE;
            int page = itemNum / pageSize;
            if (itemNum % pageSize != 0) page++;
            for (int i = 0; i < page; i++) {
                int startIndex = i * pageSize;
                int endIndex = (i + 1) * pageSize;
                List<String> proCodes = proCodeList.subList(startIndex, endIndex < itemNum ? endIndex : itemNum);
                queryRequest.setProEcodeList(proCodes);
                proInfoList.addAll(queryService.getLittleProInfo(queryRequest));
            }
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + ",createAcBillParam,proInfoList:" + proInfoList);
            }

            if (CollectionUtils.isEmpty(proInfoList)) {
                log.debug(this.getClass().getName() + ",createAcBillParam,商品信息查询为空！");
            }

            HashMap<String, PsCProResult> proMap = new HashMap<>();
            proInfoList.forEach(proInfo -> {
                proMap.put(proInfo.getEcode(), proInfo);
            });

            List<AcTransferItemRequest> itemRequests = JSONObject.parseArray(
                    JSONObject.toJSONString(transferItems), AcTransferItemRequest.class);
            itemRequests.forEach(outItem -> {
                outItem.setProLabelJson(proMap.get(outItem.getPsCProEcode()));
            });

            bill.put(SgConstants.SG_B_TRANSFER_ITEM.toUpperCase(), itemRequests);
            billRequest.setBill(bill);
            billRequest.setUser(user);
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error(this.getClass().getName() + ",createAcBillParamError:" + e.getMessage());
            }
            return null;
        }
        return billRequest;
    }


    /**
     * 封装参数
     *
     * @param params 入参
     * @return 构造之后的参数
     */
    public JSONObject createParam(JSONObject params) {
        JSONObject whereKey = new JSONObject();
        try {
            if (params != null) {
                String startDate = params.getString("startDate");
                if (StringUtils.isNotEmpty(startDate)) {
                    whereKey.put("startDate", startDate);
                }
                String endDate = params.getString("endDate");
                if (StringUtils.isNotEmpty(endDate)) {
                    whereKey.put("endDate", endDate);
                }

                JSONArray billNos = params.getJSONArray("billNo");
                if (CollectionUtils.isNotEmpty(billNos)) {
                    JSONArray billNoArr = new JSONArray();
                    for (Object billNo : billNos) {
                        billNoArr.add("'" + billNo + "'");
                    }
                    whereKey.put("billNo", StringUtils.join(billNoArr, ","));
                }
            }
        } catch (Exception e) {
            AssertUtils.logAndThrow("参数格式有误，请参考文档！");
        }
        return whereKey;
    }


    /**
     * 获取root用户
     *
     * @return 返回用户
     */
    public User getRootUser() {
        UserImpl user = new UserImpl();
        user.setId(893);
        user.setName("root");
        user.setEname("系统管理员");
        user.setTruename("系统管理员");
        user.setClientId(37);
        user.setOrgId(27);
        return user;
    }
}
