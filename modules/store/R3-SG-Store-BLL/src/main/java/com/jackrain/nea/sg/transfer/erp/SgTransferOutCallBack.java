package com.jackrain.nea.sg.transfer.erp;

import com.jackrain.nea.sg.transfer.mapper.ScBTransferMapper;
import com.jackrain.nea.sg.transfer.model.table.ScBTransfer;
import com.jackrain.nea.util.ApplicationContextHandle;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author leexh
 * @since 2020/4/2 20:27
 * desc: 调拨出库同步erp回传
 */
@Slf4j
@Component
public class SgTransferOutCallBack {

    /**
     * 更新调拨出库单号（erp）
     *
     * @param objId     调拨单ID
     * @param erpBillNo erp调拨出库单号
     */
    public void updateTransferErpBillNo(Long objId, String erpBillNo, boolean type) {
        ScBTransfer updateTransfer = new ScBTransfer();
        updateTransfer.setId(objId);
        if (type) {
            updateTransfer.setTransoutBillNo(erpBillNo);
        } else {
            updateTransfer.setRefundSaleBillNo(erpBillNo);
        }
        ScBTransferMapper mapper = ApplicationContextHandle.getBean(ScBTransferMapper.class);
        int result = mapper.updateById(updateTransfer);
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ",更新调拨出库单号结果:" + result);
        }
    }
}
