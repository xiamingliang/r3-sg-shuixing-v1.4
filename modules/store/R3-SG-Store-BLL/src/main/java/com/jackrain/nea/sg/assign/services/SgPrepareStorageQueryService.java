package com.jackrain.nea.sg.assign.services;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.Feature;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.benmanes.caffeine.cache.Cache;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.core.schema.Column;
import com.jackrain.nea.core.schema.Table;
import com.jackrain.nea.core.schema.TableManager;
import com.jackrain.nea.cp.constants.R3CommonResultConstants;
import com.jackrain.nea.cp.result.CpCStore;
import com.jackrain.nea.data.basic.model.request.StoreInfoQueryRequest;
import com.jackrain.nea.data.basic.services.BasicCpQueryService;
import com.jackrain.nea.data.basic.services.BasicPsQueryService;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.psext.model.table.PsCTeus;
import com.jackrain.nea.redis.config.CusRedisTemplate;
import com.jackrain.nea.redis.util.RedisOpsUtil;
import com.jackrain.nea.sg.basic.mapper.SgBTeusStorageMapper;
import com.jackrain.nea.sg.basic.model.table.SgBTeusStorage;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.Tools;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.utility.ExceptionUtil;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.face.impl.UserImpl;
import com.jackrain.nea.web.query.DefaultWebEvent;
import com.jackrain.nea.web.query.QuerySession;
import com.jackrain.nea.web.query.QuerySessionImpl;
import com.jackrain.nea.web.services.QueryAKCmd;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.*;

/**
 * @author :csy
 * @version :1.0
 * description ：配货查询 服务
 * @date :2019/11/6 11:42
 */
@Component
@Slf4j
public class SgPrepareStorageQueryService {

    /**
     * 店仓性质 店仓
     */
    private static final String STORE_NATURE_STO = "STO";


    /**
     * 店仓性质 仓库
     */
    private static final String STORE_NATURE_WH = "WH";

    /**
     * 配货单页面查询
     * 实现框架 queryList重载
     * step.1.参数规范
     * step.2.查询符合条件的箱定义list hashMap(Long,box)
     * step.3.分页查询箱库存
     * step.4.(未查询箱定义的情况下) 补查箱定义
     * step.5.整合 箱库存/箱定义
     * step.6.ak值转换
     *
     * @param session session
     * @return 框架 queryList标准返参
     */
    public ValueHolder execute(QuerySession session) {
        ValueHolder vh = new ValueHolder();
        DefaultWebEvent event = session.getEvent();
        JSONObject searchData = JSON.parseObject(JSON.toJSONStringWithDateFormat(event.getParameterValue("PARAM"), "yyyy-MM-dd HH:mm:ss", SerializerFeature.WriteMapNullValue), Feature.OrderedField);
        if (log.isDebugEnabled()) {
            log.debug(String.format("SgPrepareStorageQueryService param : %s", searchData.toJSONString()));
        }
        JSONObject fixedColumns = searchData.getJSONObject("fixedcolumns");
        AssertUtils.notNull(fixedColumns, "查询时需传入对应数据!");
        //step.1.参数获取 赋值
        String remark = fixedColumns.getString("REMARK");
        //多选框 查询
        List<Long> teuIdList = getIdLongList(fixedColumns.getJSONArray("PS_C_TEUS_ID"));
        List<Long> proIdList = getIdLongList(fixedColumns.getJSONArray("PS_C_PRO_ID"));
        List<Long> storeIdList = getIdLongList(fixedColumns.getJSONArray("CP_C_STORE_ID"));
        List<Long> destIdList = getIdLongList(fixedColumns.getJSONArray("CP_C_DEST_ID"));
        List<Long> sendOutIdList = getIdLongList(fixedColumns.getJSONArray("OC_B_SEND_OUT_ID"));
        List<Long> purchaseOrderIdList = getIdLongList(fixedColumns.getJSONArray("OC_B_PURCHASE_ORDER_ID"));
        int start = 0;
        int pageSize = 10;
        Integer startIndex = searchData.getInteger("startindex");
        Integer range = searchData.getInteger("range");
        if (startIndex != null) {
            start = startIndex;
        }
        if (range != null) {
            pageSize = range;
        }
        JSONObject data = new JSONObject();
        data.put("start", start);
        data.put("totalRowCount", 0);
        data.put("rowCount", pageSize);
        data.put("row", null);
        vh.put(R3CommonResultConstants.KEY_CODE, ResultCode.SUCCESS);
        vh.put(R3CommonResultConstants.KEY_MESSAGE, "success");
        vh.put(R3CommonResultConstants.KEY_DATA, data);
        //是否先 进行箱定义的查询
        boolean isQueryTeuFirst = CollectionUtils.isNotEmpty(destIdList) || CollectionUtils.isNotEmpty(sendOutIdList) || CollectionUtils.isNotEmpty(purchaseOrderIdList);
        List<String> teuECodeList = new ArrayList<>();
        HashMap<String, PsCTeus> teuHashMap = new HashMap<>(16);

        //step.2..查询符合条件的箱定义list hashMap(Long,box)
        if (isQueryTeuFirst) {
            fillTeuInfoByCondition(destIdList, sendOutIdList, purchaseOrderIdList, teuECodeList, teuHashMap);
            //没有满足的 箱定义 直接返回
            if (CollectionUtils.isEmpty(teuECodeList)) {
                return vh;
            }
        }
        //step.3.分页查询箱库存
        SgBTeusStorageMapper mapper = ApplicationContextHandle.getBean(SgBTeusStorageMapper.class);
        int totalRowCount = mapper.selectCount(new QueryWrapper<SgBTeusStorage>().lambda()
                .eq(SgBTeusStorage::getIsactive, "Y")
                .in(CollectionUtils.isNotEmpty(teuECodeList), SgBTeusStorage::getPsCTeusEcode, teuECodeList)
                .like(StringUtils.isNotEmpty(remark), SgBTeusStorage::getRemark, remark)
                .in(CollectionUtils.isNotEmpty(teuIdList), SgBTeusStorage::getPsCTeusId, teuIdList)
                .in(CollectionUtils.isNotEmpty(proIdList), SgBTeusStorage::getPsCProId, proIdList)
                .gt(SgBTeusStorage::getQtyAvailable, BigDecimal.ZERO)
                .in(CollectionUtils.isNotEmpty(storeIdList), SgBTeusStorage::getCpCStoreId, storeIdList)
        );
        List<SgBTeusStorage> storageList = mapper.selectList(new QueryWrapper<SgBTeusStorage>().lambda()
                .eq(SgBTeusStorage::getIsactive, "Y")
                .in(CollectionUtils.isNotEmpty(teuECodeList), SgBTeusStorage::getPsCTeusEcode, teuECodeList)
                .like(StringUtils.isNotEmpty(remark), SgBTeusStorage::getRemark, remark)
                .in(CollectionUtils.isNotEmpty(teuIdList), SgBTeusStorage::getPsCTeusId, teuIdList)
                .in(CollectionUtils.isNotEmpty(proIdList), SgBTeusStorage::getPsCProId, proIdList)
                .gt(SgBTeusStorage::getQtyAvailable, BigDecimal.ZERO)
                .in(CollectionUtils.isNotEmpty(storeIdList), SgBTeusStorage::getCpCStoreId, storeIdList)
                .last(String.format(" limit %s offset %s ", pageSize, start))
        );
        List<JSONObject> resultList = getResultData(storageList, isQueryTeuFirst, teuHashMap, teuECodeList);
        data.put("totalRowCount", totalRowCount);
        data.put("row", resultList);
        return vh;
    }


    /**
     * 获取 查询条件ids集合 pg库数据类型 要求
     *
     * @param array 数组
     * @return list<Long>
     */
    private List<Long> getIdLongList(JSONArray array) {
        List<Long> ids = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(array)) {
            for (int i = 0; i < array.size(); i++) {
                ids.add(array.getLongValue(i));
            }
        }
        return ids;
    }


    /**
     * step.4.(未查询箱定义的情况下) 补查箱定义
     * step.5.整合 箱库存/箱定义
     * 获取 返回给前段页面的 信息
     *
     * @param storageList     箱库存信息
     * @param isQueryTeuFirst 是否已经查过箱 信息
     * @param teuHashMap      箱定义信息
     * @param teuECodeList    箱编码列表
     * @return 转义过返回给前段的数据
     */
    private List<JSONObject> getResultData(List<SgBTeusStorage> storageList, boolean isQueryTeuFirst, HashMap<String, PsCTeus> teuHashMap, List<String> teuECodeList) {
        UserImpl user = new UserImpl();
        QuerySession session = new QuerySessionImpl(user);
        TableManager tm = session.getTableManager();
        Table table = tm.getTable("SG_B_PREPARE_STORAGE");
        HashSet<Long> destStoreIds = new HashSet<>();
        CusRedisTemplate<Object, Object> template = RedisOpsUtil.getStrRedisTemplate();
        List<JSONObject> resultList = new ArrayList<>();
        List<JSONObject> unfixedList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(storageList)) {
            for (SgBTeusStorage storage : storageList) {
                String teuECode = storage.getPsCTeusEcode();
                Long id = storage.getId();
                JSONObject jo = JSON.parseObject(JSON.toJSONString(storage));
                AssertUtils.notNull(teuECode, String.format("id 为 %s 的 箱编码为空!", id));
                if (isQueryTeuFirst) {
                    fillTeuInfo(jo, teuHashMap, id, teuECode, destStoreIds);
                    JSONObject frontResult = buildFrontResult(jo, table, template, user);
                    resultList.add(frontResult);
                } else {
                    teuECodeList.add(teuECode);
                    unfixedList.add(jo);
                }
            }
        }
        //step.5.
        if (!isQueryTeuFirst && CollectionUtils.isNotEmpty(storageList)) {
            teuHashMap = fillTeuInfoByTeuECodeList(teuECodeList);
            AssertUtils.notNull(teuHashMap, "箱定义查询为空!");
            for (JSONObject jo : unfixedList) {
                Long id = jo.getLong("ID");
                String teuECode = jo.getString("PS_C_TEUS_ECODE");
                fillTeuInfo(jo, teuHashMap, id, teuECode, destStoreIds);
                JSONObject frontResult = buildFrontResult(jo, table, template, user);
                resultList.add(frontResult);
            }
        }
        List<Long> storeIds = new ArrayList<>();
        storeIds.addAll(destStoreIds);
        HashMap<Long, CpCStore> storeNature = getStoreNature(storeIds);
        afterFix(resultList, storeNature);
        return resultList;
    }

    /**
     * 查询 并填充箱信息
     *
     * @param destIdList          收货店仓List
     * @param sendOutIdList       发货订单id List
     * @param purchaseOrderIdList 采购订单id List
     * @param teuECodeList        箱定义编码集合(需补充数据)
     * @param teuHashMap          箱定义 map   (需补充数据)
     */
    private void fillTeuInfoByCondition(List<Long> destIdList, List<Long> sendOutIdList, List<Long> purchaseOrderIdList,
                                        List<String> teuECodeList, HashMap<String, PsCTeus> teuHashMap) {
        BasicPsQueryService service = ApplicationContextHandle.getBean(BasicPsQueryService.class);
        JSONObject whereKeys = new JSONObject();
        whereKeys.put("CP_C_DEST_ID", destIdList);
        whereKeys.put("OC_B_SEND_OUT_ID", sendOutIdList);
        whereKeys.put("OC_B_PURCHASE_ORDER_ID", purchaseOrderIdList);
        List<PsCTeus> teuList = service.queryTeuListByArrayCondition(whereKeys);
        if (CollectionUtils.isNotEmpty(teuECodeList)) {
            teuList.forEach(psCTeu -> {
                teuECodeList.add(psCTeu.getEcode());
                teuHashMap.put(psCTeu.getEcode(), psCTeu);
            });
        }
    }


    /**
     * 根据 箱编码集合 查询箱信息
     *
     * @param teuECodeList 编码结合
     */
    private HashMap<String, PsCTeus> fillTeuInfoByTeuECodeList(List<String> teuECodeList) {
        BasicPsQueryService service = ApplicationContextHandle.getBean(BasicPsQueryService.class);
        return service.queryTeusInfoByEcode(teuECodeList);
    }

    /**
     * 构建 框架返回的格式
     * 同时 补充ak值
     *
     * @param jo 入参 {k:v}
     * @return {k:{"val":v}}
     */
    private JSONObject buildFrontResult(JSONObject jo, Table table, CusRedisTemplate redisTmp, User user) {
        Cache cache = ApplicationContextHandle.getBean(Cache.class);
        JSONObject data = new JSONObject();
        if (jo != null) {
            for (Map.Entry<String, Object> entry : jo.entrySet()) {
                JSONObject val = new JSONObject();
                Object value = entry.getValue();
                Column column = table.getColumn(entry.getKey());
                if (column != null && column.getReferenceTable() != null && value != null) {
                    Table referenceTable = column.getReferenceTable();
                    val.put("val", getAkValWithCache(column, value.toString(), redisTmp, cache, user));
                    val.put("reftablename", referenceTable.getName());
                    val.put("reftableid", referenceTable.getId());
                    val.put("reftabdesc", referenceTable.getDescription(user.getLocale()));
                    val.put("refobjid", value);
                    val.put("colid", column.getId());
                } else {
                    val.put("val", value);
                }
                val.put("orig_val", value);
                data.put(entry.getKey(), val);
            }
        }
        return data;
    }


    /**
     * 补充明细信息
     *
     * @param jo         原jsonObj
     * @param teuHashMap 箱定义信息
     * @param id         id
     * @param teuECode   箱eCode
     */
    private void fillTeuInfo(JSONObject jo, HashMap<String, PsCTeus> teuHashMap, Long id, String teuECode, HashSet<Long> destStoreIds) {
        PsCTeus psCTeus = teuHashMap.get(teuECode);
        AssertUtils.notNull(psCTeus, String.format("数据错误,id.为%s.的.箱定义信息未找到", id));
        jo.put("TOT_QTY", psCTeus.getTotQty().multiply(jo.getBigDecimal("QTY_AVAILABLE") == null ? BigDecimal.ZERO : jo.getBigDecimal("QTY_AVAILABLE")));
        jo.put("CP_C_DEST_ID", psCTeus.getCpCDestId());
        jo.put("CP_C_DEST_ECODE", psCTeus.getCpCDestEcode());
        jo.put("CP_C_DEST_ENAME", psCTeus.getCpCDestEname());
        jo.put("OC_B_PURCHASE_ORDER_ID", psCTeus.getOcBPurchaseOrderId());
        jo.put("OC_B_SEND_OUT_ID", psCTeus.getOcBSendOutId());
        if (psCTeus.getCpCDestId() != null) {
            destStoreIds.add(psCTeus.getCpCDestId());
        }
    }


    /**
     * 获取外键 ak 的 val
     *
     * @param col        col
     * @param columnData ak val
     * @param redisTmp   redis
     * @param cache      cache
     * @param user       user
     * @return name/code
     */
    private String getAkValWithCache(Column col, String columnData, CusRedisTemplate redisTmp, Cache cache, User user) {
        String pKey = col.getReferenceTable() + "_" + columnData;
        String pVal;
        if (cache.getIfPresent(pKey) != null) {
            return String.valueOf(cache.getIfPresent(pKey));
        } else {

            if (BooleanUtils.toBoolean(redisTmp.hasKey("refremoteval:" + pKey))) {
                pVal = (String) redisTmp.opsForValue().get("refremoteval:" + pKey);
                cache.put(pKey, pVal);
                return pVal;
            } else {
                String center = col.getReferenceTable().getCategory().getSubSystem().getCenter();
                String[] gv = center.split(":");
                if (gv.length != 2) {
                    throw new NDSException("center is error");
                } else {
                    Object o = ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(), QueryAKCmd.class.getName(), gv[0], gv[1]);

                    HashMap<String, Object> parm = new HashMap<>(16);
                    parm.put("colid", col.getId());
                    parm.put("ids", columnData);
                    parm.put("user", user);
                    ValueHolder val = ((QueryAKCmd) o).execute(parm);
                    pVal = (String) val.get("data");
                    String afterParseValue = Tools.nvl(pVal, "");
                    cache.put(pKey, afterParseValue);
                    redisTmp.opsForValue().set("refremoteval:" + pKey, Tools.nvl(pVal, ""));
                    return pVal;
                }
            }
        }
    }


    /**
     * 获取店仓性质
     *
     * @param ids storeIds
     * @return storeId/natureId
     */
    private HashMap<Long, CpCStore> getStoreNature(List<Long> ids) {
        BasicCpQueryService service = ApplicationContextHandle.getBean(BasicCpQueryService.class);
        StoreInfoQueryRequest request = new StoreInfoQueryRequest();
        request.setIds(ids);
        try {
            return service.getStoreInfo(request);
        } catch (Exception e) {
            String errMsg = e.getMessage();
            if (StringUtils.isEmpty(errMsg)) {
                errMsg = ExceptionUtil.getMessage(e);
            }
            throw new NDSException("店仓性质查询失败!" + errMsg, e);
        }
    }


    /**
     * 额外信息补充
     *
     * @param list         数据
     * @param storeHashMap 店仓信息
     */
    private void afterFix(List<JSONObject> list, HashMap<Long, CpCStore> storeHashMap) {
        for (JSONObject jo : list) {
            Long val = jo.getJSONObject("CP_C_DEST_ID").getLongValue("orig_val");
            JSONObject storeNature = new JSONObject();
            storeNature.put("val", getStoreNature(val, storeHashMap));
            jo.put("DEST_STORE_N", storeNature);
        }
    }


    private String getStoreNature(Long storeId, HashMap<Long, CpCStore> storeHashMap) {
        if (storeId != null && storeHashMap != null) {
            CpCStore store = storeHashMap.get(storeId);
            if (store != null) {
                if (STORE_NATURE_STO.equals(store.getStorenature())) {
                    return "门店";
                } else if (STORE_NATURE_WH.equals((store.getStorenature()))) {
                    return "仓库";
                }
            }
        }
        return null;
    }
}
