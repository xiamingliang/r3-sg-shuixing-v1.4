package com.jackrain.nea.sg.assign.validate;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.sg.assign.common.SgAssignConstantsIF;
import com.jackrain.nea.sg.assign.mapper.SgBAssignImpItemMapper;
import com.jackrain.nea.sg.assign.model.table.SgBAssignImpItem;
import com.jackrain.nea.tableService.MainTableRecord;
import com.jackrain.nea.tableService.TableServiceContext;
import com.jackrain.nea.tableService.validate.BaseValidator;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.utils.AssertUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;
import java.util.Locale;

/**
 * @author : 孙俊磊
 * @since : 2019-10-11
 * create at : 2019-10-11 11:44 AM
 */
@Slf4j
@Component
public class SgAssignSubmitValidate extends BaseValidator {

    @Override
    public void validate(TableServiceContext context, MainTableRecord currentRow) {
        JSONObject origData = currentRow.getOrignalData();
        Integer status = origData.getInteger("STATUS");
        AssertUtils.cannot(status == SgAssignConstantsIF.ASSIGN_STATUS_AUDIT, "当前记录已审核，不允许重复审核！", context.getLocale());
        AssertUtils.cannot(status == SgAssignConstantsIF.ASSIGN_STATUS_VOID, "当前记录已作废，不允许审核！", context.getLocale());
        checkAssignStatus(context.getLocale(), currentRow.getId(), origData);
    }

    private void checkAssignStatus(Locale locale, Long objId, JSONObject origData) {

        // 主表发货店仓、发货经销商
        Long origId = origData.getLong("CP_C_ORIG_ID");
        Long origCustomerId = origData.getLong("CP_C_CUSTOMERUP_ID");

        SgBAssignImpItemMapper itemMapper = ApplicationContextHandle.getBean(SgBAssignImpItemMapper.class);
        // 删除数量为0的记录
        int delCount = itemMapper.delete(new QueryWrapper<SgBAssignImpItem>().lambda()
                .eq(SgBAssignImpItem::getSgBAssignId, objId)
                .eq(SgBAssignImpItem::getQty, BigDecimal.ZERO));
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ",铺货单删除明细数量为0的记录:" + delCount);
        }

        List<SgBAssignImpItem> impItems = itemMapper.selectList(new QueryWrapper<SgBAssignImpItem>().lambda()
                .eq(SgBAssignImpItem::getSgBAssignId, objId));
        AssertUtils.cannot(CollectionUtils.isEmpty(impItems), "当前记录无明细，不允许提交！", locale);

        // 判断主表发货店仓和明细收货店仓关系
        for (SgBAssignImpItem item : impItems) {

            // 收货店仓
            Long destId = item.getCpCDestId();
            AssertUtils.notNull(destId, "明细收货店仓不存在!", locale);

            // 收货经销商
            Long customerDestId = item.getCpCCustomerdestId();
            AssertUtils.notNull(customerDestId, "明细收货经销商不存在！", locale);

            // 1.若发货店仓与收货店仓相同，则提示：“收货店仓与发货店仓不允许相同”
            AssertUtils.cannot(destId.equals(origId), "收货店仓与发货店仓不允许相同", locale);

            // 2.如果发货店仓所属经销商不等于收货店仓的所属经销商，且不等于收货店仓所属上级经销商，
            // 则提示：“发货经销商需要等于收货经销商或者等于收货经销商上级经销商”
            AssertUtils.cannot((!origCustomerId.equals(customerDestId) && !origCustomerId.equals(item.getCpCCustomerdestupId())),
                    "发货经销商需要等于收货经销商或者等于收货经销商的上级经销商", locale);
        }
    }
}
