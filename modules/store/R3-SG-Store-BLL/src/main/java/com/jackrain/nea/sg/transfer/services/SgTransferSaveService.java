package com.jackrain.nea.sg.transfer.services;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.Feature;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Lists;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.oc.basic.common.OcBasicConstants;
import com.jackrain.nea.sg.basic.common.R3ParamConstants;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.config.SgStorageBoxConfig;
import com.jackrain.nea.sg.basic.model.result.SgR3BaseResult;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.basic.utils.R3ParamUtil;
import com.jackrain.nea.sg.basic.utils.StorageUtils;
import com.jackrain.nea.sg.transfer.common.ScTransferConstants;
import com.jackrain.nea.sg.transfer.enums.SgTransferBillStatusEnum;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferMapper;
import com.jackrain.nea.sg.transfer.model.request.OcTransferUpdatePickRequest;
import com.jackrain.nea.sg.transfer.model.request.SgTransferBillSaveRequest;
import com.jackrain.nea.sg.transfer.model.request.SgTransferUpdateSapStatusRequest;
import com.jackrain.nea.sg.transfer.model.result.OcTransferUpdatePickResult;
import com.jackrain.nea.sg.transfer.model.table.ScBTransfer;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.tableService.service.TableServiceCmdService;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.utility.ExceptionUtil;
import com.jackrain.nea.utils.OcCommonUtils;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.query.DefaultWebEvent;
import com.jackrain.nea.web.query.QuerySession;
import com.jackrain.nea.web.query.QuerySessionImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author csy
 * Date: 2019/5/5
 * Description:
 */
@Slf4j
@Component
public class SgTransferSaveService {

    @Autowired
    private SgStorageBoxConfig boxConfig;

    @Autowired
    private ScBTransferMapper scBTransferMapper;

    /**
     * r3框架入参用
     */
    ValueHolder save(SgTransferBillSaveRequest request) {
        return R3ParamUtil.convertV14WithResult(save(request, true));
    }


    /**
     * @param request    入参
     * @param isNewTrans 控制是否同一事务
     */
    @Transactional(rollbackFor = Exception.class)
    public ValueHolderV14<SgR3BaseResult> save(SgTransferBillSaveRequest request, boolean isNewTrans) {

       log.debug("SgTransferSaveService===>param"+JSONObject.toJSONString(request) );
        AssertUtils.notNull(request, "请求参数为空!");
        User user = request.getLoginUser();
        AssertUtils.notNull(user, "用户未登录");
        ValueHolderV14<SgR3BaseResult> holderV14 = new ValueHolderV14<>();
        TableServiceCmdService service = ApplicationContextHandle.getBean(TableServiceCmdService.class);
        QuerySession session = new QuerySessionImpl(user);
        DefaultWebEvent event = new DefaultWebEvent("dosave", new HashMap(16));
        JSONObject param = new JSONObject();
        Long objId = request.getObjId();
        if (objId == null) {
            objId = -1L;
        }
        SgTransferBillSaveRequest saveRequest = new SgTransferBillSaveRequest();

        // 开启箱功能是传录入明细进去
        if (boxConfig.getBoxEnable()) {
            saveRequest.setImpItems(request.getImpItems());
        } else {
            saveRequest.setItems(request.getItems());
        }
        saveRequest.setTransfer(request.getTransfer());
        param.put(R3ParamConstants.FIXCOLUMN, JSON.parseObject(JSON.toJSONStringWithDateFormat(saveRequest, "yyyy-MM-dd HH:mm:ss", SerializerFeature.IgnoreNonFieldGetter), Feature.OrderedField));
        param.put(R3ParamConstants.OBJID, objId);
        param.put(R3ParamConstants.TABLE, SgConstants.SG_B_TRANSFER.toUpperCase());
        event.put(R3ParamConstants.PARAM, param);
        session.setEvent(event);
        ValueHolder holder;
        if (isNewTrans) {
            // 延续上一个事务
            holder = service.executeWithNorTrans(session);
        } else {
            // 新事务
            holder = service.execute(session);
        }
        AssertUtils.notNull(holder, "调拨单新增返回为空!", user.getLocale());
        AssertUtils.cannot(holder.get("code") == null || (Integer) holder.get("code") == -1, "调拨单新增失败:" + holder.get("message"), user.getLocale());
        holderV14.setCode((Integer) holder.get(R3ParamConstants.CODE));
        holderV14.setMessage((String) holder.get(R3ParamConstants.MESSAGE));
        SgR3BaseResult baseResult = new SgR3BaseResult();
        AssertUtils.notNull(holder.get(R3ParamConstants.DATA), "调拨单新增返回data为空", user.getLocale());
        baseResult.setDataJo(JSONObject.parseObject(holder.get(R3ParamConstants.DATA).toString()));
        holderV14.setData(baseResult);
        return holderV14;
    }

    /**
     * sap回传，更新sap状态
     *
     * @param request
     * @return
     */
    public ValueHolderV14 updateSapStatus(SgTransferUpdateSapStatusRequest request) {
        ValueHolderV14 valueHolderV14 = new ValueHolderV14();
        valueHolderV14.setCode(ResultCode.SUCCESS);
        valueHolderV14.setMessage("sap回传更新sap状态成功！！！");
        //sap状态
        Integer sapStatus = request.getSapStatus();
        //主键
        List<Long> idList = request.getIdList();
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + " sap回传更新状态参数主键:" + JSONObject.toJSONString(idList) + " 状态:" + sapStatus);
        }
        List<List<Long>> updatePageList =
                StorageUtils.getPageList(idList, SgConstants.SG_COMMON_MAX_QUERY_PAGE_SIZE);
        try {
            //分页批量更新
            for (List<Long> pageList : updatePageList) {
                int count = scBTransferMapper.updateTransferSapStatus(sapStatus, pageList);
                if (log.isDebugEnabled()) {
                    log.debug(this.getClass().getName() + " sap回传更新成功,更新参数主键:" + JSONObject.toJSONString(pageList) + " 更新状态:" + sapStatus + " 更新条数:" + count);
                }
            }
        } catch (Exception e) {
            valueHolderV14.setCode(ResultCode.FAIL);
            valueHolderV14.setMessage("sap回传更新失败！！！");
        }
        return valueHolderV14;
    }


    /**
     * 批量更新调拨单拣货状态
     *
     * @param request 入参
     * @return 出参
     */
    public ValueHolderV14<List<OcTransferUpdatePickResult>> batchUpdateTransferPickStatus(OcTransferUpdatePickRequest request) {

        ValueHolderV14<List<OcTransferUpdatePickResult>> v14 = new ValueHolderV14<>(ResultCode.SUCCESS, "success!");
        com.jackrain.nea.utils.AssertUtils.notNull(request, "入参不能为空！");
        User user = request.getUser();
        com.jackrain.nea.utils.AssertUtils.notNull(user, "用户信息不能为空！");

        List<Long> ids = request.getIds();
        com.jackrain.nea.utils.AssertUtils.cannot(CollectionUtils.isEmpty(ids), "调拨单id不能为空！", user.getLocale());

        Integer outPickStatus = request.getOutPickStatus();
        Integer inPickStatus = request.getInPickStatus();
        com.jackrain.nea.utils.AssertUtils.cannot((outPickStatus == null && inPickStatus == null), "出/入库拣货状态不能同时为空！", user.getLocale());

        List<Long> updateByIds = Lists.newArrayList();
        List<OcTransferUpdatePickResult> resultList = Lists.newArrayList();
        try {
            // 批量查询调拨单
            ScBTransferMapper transferMapper = ApplicationContextHandle.getBean(ScBTransferMapper.class);
            List<ScBTransfer> transferList = queryTransferList(transferMapper, ids);
            if (CollectionUtils.isEmpty(transferList)) {
                com.jackrain.nea.utils.AssertUtils.logAndThrow("调拨单查询为空！" + JSON.toJSONString(ids), user.getLocale());
            }

            Map<Long, ScBTransfer> transferMap = transferList.stream().collect(Collectors.toMap(ScBTransfer::getId, Function.identity()));
            ids.forEach(id -> {

                // 1.收集不存在的调拨单
                if (!transferMap.containsKey(id)) {
                    errorTransfer(resultList, id, null, "调拨单不存在！");
                } else {
                    // 2.遍历收集不允许修改拣货状态的调拨单
                    ScBTransfer transfer = transferMap.get(id);
                    String billNo = transfer.getBillNo();
                    Integer saleStatus = transfer.getStatus();

                    if (outPickStatus != null) {
                        // 如果是出库拣货,判断将要更新的拣货状态
                        if (outPickStatus == ScTransferConstants.PICK_STATUS_UNPICKED) {
                            // 原单状态 = 已审核未出库   允许更新【出库拣货状态】为“未拣货”
                            if (saleStatus == SgTransferBillStatusEnum.AUDITED_NOT_OUT.getVal()) {
                                updateByIds.add(id);
                            } else {
                                errorTransfer(resultList, id, billNo, "调拨单状态为：" +
                                        SgTransferBillStatusEnum.getTransferStatusDes(saleStatus) + ",不允许更新出库拣货状态为未拣货！");
                            }
                        } else if (outPickStatus == ScTransferConstants.PICK_STATUS_PICKING) {
                            // 原单状态= 已审核未出库、部分出库%，   允许更新【出库拣货状态】为“拣货中”
                            if (saleStatus == SgTransferBillStatusEnum.AUDITED_NOT_OUT.getVal() ||
                                    saleStatus == SgTransferBillStatusEnum.AUDITED_PART_OUT_NOT_IN.getVal() ||
                                    saleStatus == SgTransferBillStatusEnum.AUDITED_PART_OUT_PART_IN.getVal()) {
                                updateByIds.add(id);
                            } else {
                                errorTransfer(resultList, id, billNo, "调拨单状态为：" +
                                        SgTransferBillStatusEnum.getTransferStatusDes(saleStatus) + ",不允许更新出库拣货状态为拣货中！");
                            }
                        }

                    } else if (inPickStatus != null) {
                        // 如果是入库拣货，判断将要更新的拣货状态
                        if (inPickStatus == ScTransferConstants.PICK_STATUS_UNPICKED) {
                            // 原单状态= %未入库   允许更新【入库拣货状态】为“未拣货”。
                            if (saleStatus == SgTransferBillStatusEnum.AUDITED_PART_OUT_NOT_IN.getVal() ||
                                    saleStatus == SgTransferBillStatusEnum.AUDITED_ALL_OUT_NOT_IN.getVal()) {
                                updateByIds.add(id);
                            } else {
                                errorTransfer(resultList, id, billNo, "调拨单状态为：" +
                                        SgTransferBillStatusEnum.getTransferStatusDes(saleStatus) + ",不允许更新入库拣货状态为未拣货！");
                            }

                        } else if (inPickStatus == ScTransferConstants.PICK_STATUS_PICKING) {
                            // 原单状态= %未入库、%部分入库， 允许更新【入库拣货状态】为“拣货中
                            if (saleStatus >= SgTransferBillStatusEnum.AUDITED_PART_OUT_NOT_IN.getVal() &&
                                    saleStatus <= SgTransferBillStatusEnum.AUDITED_ALL_OUT_PART_IN.getVal()) {
                                updateByIds.add(id);
                            } else {
                                errorTransfer(resultList, id, billNo, "调拨单状态为：" +
                                        SgTransferBillStatusEnum.getTransferStatusDes(saleStatus) + ",不允许更新入库拣货状态为拣货中！");
                            }
                        }
                    }
                }
            });

            // 更新调拨单拣货状态
            updateTransferPickStatus(user, transferMapper, updateByIds, outPickStatus, inPickStatus);
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error(this.getClass().getName() + ",更新调拨单拣货状态异常！", ExceptionUtil.getMessage(e));
            }
            com.jackrain.nea.utils.AssertUtils.logAndThrowExtra("更新调拨单拣货状态异常！", e);
        }

        if (CollectionUtils.isNotEmpty(resultList)) {
            v14.setCode(ResultCode.FAIL);
            v14.setMessage("部分更新成功！");
            v14.setData(resultList);
        }
        return v14;
    }


    /**
     * 查询批量调拨单
     *
     * @param transferMapper mapper
     * @param ids            调拨单ids
     */
    private List<ScBTransfer> queryTransferList(ScBTransferMapper transferMapper, List<Long> ids) {

        List<ScBTransfer> saleList = Lists.newArrayList();
        if (ids.size() > OcBasicConstants.COMMON_INSERT_PAGE_SIZE) {

            List<List<Long>> assignList = OcCommonUtils.assignList(ids, OcBasicConstants.COMMON_INSERT_PAGE_SIZE);
            if (CollectionUtils.isNotEmpty(assignList)) {
                for (List<Long> list : assignList) {
                    if (CollectionUtils.isEmpty(list)) {
                        continue;
                    }
                    saleList.addAll(transferMapper.selectList(new QueryWrapper<ScBTransfer>().lambda().in(ScBTransfer::getId, list)));
                }
            }
        } else {
            saleList = transferMapper.selectList(new QueryWrapper<ScBTransfer>().lambda().in(ScBTransfer::getId, ids));
        }
        return saleList;
    }


    /**
     * 更新拣货状态
     *
     * @param transferMapper mapper
     * @param updateByIds    调拨单ids
     * @param outPickStatus  出库拣货状态
     * @param inPickStatus   入库拣货状态
     */
    private void updateTransferPickStatus(User user, ScBTransferMapper transferMapper, List<Long> updateByIds,
                                          Integer outPickStatus, Integer inPickStatus) {

        // 更新调拨单拣货状态
        if (CollectionUtils.isNotEmpty(updateByIds)) {
            int count = 0;
            Long userId = user.getId().longValue();
            String name = user.getName();
            String eName = user.getEname();
            if (updateByIds.size() > OcBasicConstants.COMMON_INSERT_PAGE_SIZE) {

                List<List<Long>> lists = OcCommonUtils.assignList(updateByIds, OcBasicConstants.COMMON_INSERT_PAGE_SIZE);
                if (CollectionUtils.isNotEmpty(lists)) {
                    for (List<Long> list : lists) {
                        if (CollectionUtils.isEmpty(list)) {
                            continue;
                        }

                        // 循环批量更新调拨单拣货状态
                        if (outPickStatus != null) {
                            count += transferMapper.batchUpdateTransferOutPickByIds(outPickStatus, userId, name, eName,
                                    new Timestamp(System.currentTimeMillis()), StringUtils.join(list, ","));
                        } else if (inPickStatus != null) {
                            count += transferMapper.batchUpdateTransferInPickByIds(inPickStatus, userId, name, eName,
                                    new Timestamp(System.currentTimeMillis()), StringUtils.join(list, ","));
                        }
                    }
                }

            } else {
                if (outPickStatus != null) {
                    count = transferMapper.batchUpdateTransferOutPickByIds(outPickStatus, userId, name, eName,
                            new Timestamp(System.currentTimeMillis()), StringUtils.join(updateByIds, ","));
                } else if (inPickStatus != null) {
                    count = transferMapper.batchUpdateTransferInPickByIds(inPickStatus, userId, name, eName,
                            new Timestamp(System.currentTimeMillis()), StringUtils.join(updateByIds, ","));
                }
            }

            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + ",调拨单更细拣货状态成功！" + count);
            }
        }
    }

    /**
     * 收集不允许更新拣货状态的调拨单
     *
     * @param resultList 错误集合
     * @param id         调拨单id
     * @param billNo     调拨单单号
     * @param errorMsg   错误原因
     */
    public void errorTransfer(List<OcTransferUpdatePickResult> resultList, Long id, String billNo, String errorMsg) {
        OcTransferUpdatePickResult result = new OcTransferUpdatePickResult();
        result.setObjId(id);
        result.setBillNo(billNo);
        result.setErrorMsg(errorMsg);
        resultList.add(result);
    }
}