package com.jackrain.nea.sg.transfer.services;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.Feature;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.redis.config.CusRedisTemplate;
import com.jackrain.nea.redis.util.RedisOpsUtil;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.send.api.SgSendCleanSaveCmd;
import com.jackrain.nea.sg.send.model.request.SgSendBillCleanRequest;
import com.jackrain.nea.sg.send.model.result.SgSendBillCleanResult;
import com.jackrain.nea.sg.transfer.common.ScTransferConstants;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferItemMapper;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferMapper;
import com.jackrain.nea.sg.transfer.model.table.ScBTransfer;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.utils.AssertUtils;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.query.DefaultWebEvent;
import com.jackrain.nea.web.query.QuerySession;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.concurrent.TimeUnit;

/**
 * @author: 汪聿森
 * @Date: Created in 2019-08-23 17:11
 * @Description : 取消锁单
 */
@Slf4j
@Component
public class ScBTransferUnLockService {
    @Autowired
    private ScBTransferMapper scBTransferMapper;
    @Autowired
    private ScBTransferItemMapper scBTransferItemMapper;
    @Reference(version = "1.0", group = "sg")
    private SgSendCleanSaveCmd sgSendCleanSaveCmd;

    public ValueHolder execute(QuerySession session) {
        ValueHolder vh = new ValueHolder();
        DefaultWebEvent event = session.getEvent();
        JSONObject param = JSON.parseObject(JSON.toJSONStringWithDateFormat(event.getParameterValue("param"),
                "yyyy-MM-dd HH:mm:ss", SerializerFeature.WriteMapNullValue), Feature.OrderedField);
        if (log.isDebugEnabled()) {
            log.info("ScBTransferLockService 参数:" + JSONObject.toJSONString(param));
        }
        Long objId = param.getLong("objid");
        ScBTransfer scBTransfer = scBTransferMapper.selectById(objId);
        //记录不存在，则提示：“当前记录已不存在！”
        if (scBTransfer == null) {
            throw new NDSException(Resources.getMessage("当前记录已不存在", session.getLocale()));
        }
        String billNo = scBTransfer.getBillNo();
        String lockKsy = ScTransferConstants.TRANSFER_TYPE + ":" + billNo;
        CusRedisTemplate<String, String> redisTemplate = RedisOpsUtil.getObjRedisTemplate();
        Boolean blnCanInit = redisTemplate.opsForValue().setIfAbsent(lockKsy, "ok");
        if (blnCanInit != null && blnCanInit) {
            redisTemplate.expire(lockKsy, 60, TimeUnit.SECONDS);
        } else {
            AssertUtils.logAndThrow("当前调拨单正在操作中，请稍后重试！调拨单号：" + billNo);
        }
        try {
            Integer isLocked = scBTransfer.getIsLocked();
            //【是否锁单】=否，则提示：“当前记录没有进行锁单，不允许取消锁单！”
            if (isLocked != null && isLocked == ScTransferConstants.IS_UNLOCKED) {
                throw new NDSException(Resources.getMessage("当前记录没有进行锁单，不允许取消锁单！", session.getLocale()));
            }
            Integer status = scBTransfer.getStatus();
            //【单据状态】<>未审核，则提示：“当前记录的单据状态不允许取消锁单！”
            if (ScTransferConstants.SEND_OUT_STATUS_WAIT != status) {
                throw new NDSException(Resources.getMessage("当前记录的单据状态不允许取消锁单！", session.getLocale()));
            }
            //调用清空逻辑发货单
            cleanSgSend(session.getUser(), scBTransfer);
            //更新本单的【是否锁单】=否
            scBTransfer.setIsLocked(0);
            scBTransfer.setModifierid(Long.valueOf(session.getUser().getId()));
            scBTransfer.setModifiername(session.getUser().getName());
            scBTransfer.setModifierename(session.getUser().getEname());
            scBTransfer.setModifieddate(new Timestamp(System.currentTimeMillis()));
            if (scBTransferMapper.updateById(scBTransfer) > 0) {
                vh.put("code", 0);
                vh.put("message", Resources.getMessage("取消锁单成功！", session.getLocale()));
            } else {
                vh.put("code", -1);
                vh.put("message", Resources.getMessage("取消锁单失败！", session.getLocale()));
            }
        } catch (NDSException e) {
            log.error(this.getClass().getName() + "调拨单取消锁单异常" + e);
            AssertUtils.logAndThrowExtra("调拨单取消锁单异常", e);
        } finally {
            try {
                redisTemplate.delete(lockKsy);
            } catch (Exception e) {
                log.error(this.getClass().getName() + "redis锁释放失败，请联系管理员！lockKsy：" + lockKsy);
                AssertUtils.logAndThrow("redis锁释放失败，请联系管理员！");
            }
        }
        return vh;
    }

    /**
     * 清空逻辑发货单
     *
     * @param user        用户信息
     * @param scBTransfer 调拨单对象
     */
    public void cleanSgSend(User user, ScBTransfer scBTransfer) {
        SgSendBillCleanRequest cleanRequest = new SgSendBillCleanRequest();
        cleanRequest.setSourceBillType(SgConstantsIF.BILL_TYPE_TRANSFER);
        cleanRequest.setSourceBillId(scBTransfer.getId());
        cleanRequest.setSourceBillNo(scBTransfer.getBillNo());
        cleanRequest.setServiceNode(SgConstantsIF.SERVICE_NODE_TRANSFER_UNLOCK);
        cleanRequest.setLoginUser(user);
        ValueHolderV14<SgSendBillCleanResult> cleanSgBSendV14 = sgSendCleanSaveCmd.cleanSgBSend(cleanRequest);
        if (cleanSgBSendV14.getCode() == ResultCode.FAIL) {
            AssertUtils.logAndThrow("清空逻辑发货单异常：" + cleanSgBSendV14.getMessage(), user.getLocale());
        }
    }
}
