//package com.jackrain.nea.sg.transfer.services;
//
//import com.alibaba.fastjson.JSONObject;
//import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
//import com.jackrain.nea.config.Resources;
//import com.jackrain.nea.common.ResultCode;
//import com.jackrain.nea.es.util.ElasticSearchUtil;
//import com.jackrain.nea.exception.NDSException;
//import com.jackrain.nea.sc.in.model.request.ScBInWBRequest;
//import com.jackrain.nea.sc.in.model.table.ScBIn;
//import com.jackrain.nea.sc.in.model.table.ScBInItem;
//import com.jackrain.nea.sc.out.model.request.ScBOutWBRequest;
//import com.jackrain.nea.sc.out.model.table.ScBOut;
//import com.jackrain.nea.sc.out.model.table.ScBOutItem;
//import com.jackrain.nea.sc.transfer.common.ScTransferConstants;
//import com.jackrain.nea.sc.transfer.mapper.ScBTransferItemMapper;
//import com.jackrain.nea.sc.transfer.mapper.ScBTransferMapper;
//import com.jackrain.nea.sc.transfer.model.table.ScBTransfer;
//import com.jackrain.nea.sc.transfer.model.table.ScBTransferItem;
//import com.jackrain.nea.sys.domain.ValueHolderV14;
//import com.jackrain.nea.tableService.common.Constants;
//import com.jackrain.nea.util.ApplicationContextHandle;
//import com.jackrain.nea.utils.AssertUtils;
//import com.jackrain.nea.web.face.User;
//import lombok.extern.slf4j.Slf4j;
//import org.apache.commons.lang3.StringUtils;
//import org.springframework.stereotype.Component;
//
//import java.io.IOException;
//import java.math.BigDecimal;
//import java.util.Date;
//import java.util.Locale;
//
//@Component
//@Slf4j
//public class ScTransferBaseService {
//
//
//    /**
//     * 出库单 回写
//     */
//    ValueHolderV14 outBillWriteBack(ScBOutWBRequest request) {
//        log.debug("outBillWriteBack - request-" + request.toString());
//        ValueHolderV14 holderV14 = new ValueHolderV14();
//        User loginUser = request.getLoginUser();
//        ////SUBMIT：提交回写，PICK：拣货回写
//
//        AssertUtils.notNull(loginUser, "user is null,login first!");
//        Locale locale = loginUser.getLocale();
//        ScBTransferMapper mapper = ApplicationContextHandle.getBean(ScBTransferMapper.class);
//        ScBTransferItemMapper itemMapper = ApplicationContextHandle.getBean(ScBTransferItemMapper.class);
//        ScBOut out = request.getOut();
//        String billNo = out.getBillNo();
//        AssertUtils.notNull(billNo, "单据编号不存在", locale);
//        Long mainId = getIdFromEsByBillNo(billNo, locale);
//        AssertUtils.notNull(mainId, "单据编号为" + billNo + "的调拨单不存在!");
//        ScBTransfer origDto = mapper.selectById(mainId);
//        AssertUtils.notNull(origDto, "当前单据已不存在!", locale);
//        ScBTransfer updateDo = createUpdateDo(out, mainId, origDto, loginUser, request.getAction());
//
//        mapper.updateById(updateDo);
//        if (request.getItems() != null && !request.getItems().isEmpty()) {
//            for (ScBOutItem item : request.getItems()) {
//                AssertUtils.notNull(item.getPsCSkuEcode(), "条码eCode不存在", locale);
//                UpdateWrapper<ScBTransferItem> updateWrapper = new UpdateWrapper<>();
//                updateWrapper.eq("PS_C_SKU_ECODE", item.getPsCSkuEcode()).eq("SC_B_TRANSFER_ID", mainId);
//                itemMapper.update(createUpdateItemDo(item, loginUser), updateWrapper);
//            }
//        }
//
//
//        //推送es 目前 只推送了主表
//        try {
//            ElasticSearchUtil.indexDocument(ScTransferConstants.TRANSFER_INDEX, ScTransferConstants.TRANSFER_TYPE, updateDo, mainId);
//        } catch (IOException e) {
//            log.error("es推送失败", e);
//            throw new NDSException(Resources.getMessage("es推送失败", locale));
//        }
//        holderV14.setCode(ResultCode.SUCCESS);
//        holderV14.setMessage("success");
//        return holderV14;
//    }
//
//
//    /**
//     * 入库单 回写
//     */
//    ValueHolderV14 inBillWriteBack(ScBInWBRequest request) {
//        log.debug("inBillWriteBack - request-" + request.toString());
//        ValueHolderV14 holder = new ValueHolderV14();
//        User loginUser = request.getLoginUser();
//        AssertUtils.notNull(loginUser, "user is null,login first!");
//        Locale locale = loginUser.getLocale();
//        ScBTransferMapper mapper = ApplicationContextHandle.getBean(ScBTransferMapper.class);
//        ScBTransferItemMapper itemMapper = ApplicationContextHandle.getBean(ScBTransferItemMapper.class);
//        ScBIn in = request.getIn();
//        String billNo = in.getBillNo();
//        AssertUtils.notNull(billNo, "单据编号不存在", locale);
//        Long mainId = getIdFromEsByBillNo(billNo, locale);
//        ScBTransfer origDto = mapper.selectById(mainId);
//        AssertUtils.notNull(origDto, "当前单据已不存在!", locale);
//
//
//        ScBTransfer updateDo = createInUpdateDo(in, mainId, origDto, loginUser);
//        mapper.updateById(updateDo);
//        if (request.getItems() != null && !request.getItems().isEmpty()) {
//            for (ScBInItem item : request.getItems()) {
//                AssertUtils.notNull(item.getPsCSkuEcode(), "条码eCode不存在", locale);
//                UpdateWrapper<ScBTransferItem> updateWrapper = new UpdateWrapper<>();
//                updateWrapper.eq("PS_C_SKU_ECODE", item.getPsCSkuEcode()).eq("SC_B_TRANSFER_ID", mainId);
//                itemMapper.update(createUpdateInItemDo(item, loginUser), updateWrapper);
//            }
//        }
//
//
//        //推送es 目前 只推送了主表
//        try {
//            ElasticSearchUtil.indexDocument(ScTransferConstants.TRANSFER_INDEX, ScTransferConstants.TRANSFER_TYPE, updateDo, mainId);
//        } catch (IOException e) {
//            log.error("es推送失败", e);
//            throw new NDSException(Resources.getMessage("es推送失败", locale));
//        }
//
//
//        holder.setCode(ResultCode.SUCCESS);
//        holder.setMessage("success");
//        return holder;
//    }
//
//
//    /**
//     * 根据单据编号获取 id
//     *
//     * @param billNo 单据编号
//     */
//    private Long getIdFromEsByBillNo(String billNo, Locale locale) {
//
//
//        JSONObject whereKeys = new JSONObject();
//        whereKeys.put("BILL_NO", billNo);
//        String[] queryFDs = new String[]{"ID"};
//        JSONObject esResult = ElasticSearchUtil.search(ScTransferConstants.TRANSFER_INDEX, ScTransferConstants.TRANSFER_TYPE, whereKeys, null, null, 1, 0, queryFDs);
//        log.debug("es query result" + esResult.toJSONString());
//
//        //es查询id
//        if (esResult.getJSONArray("data") != null && esResult.getJSONArray("data").size() > 0) {
//            return esResult.getJSONArray("data").getJSONObject(0).getLong("ID");
//        } else if (esResult.getIntValue("code") == -1) {
//            AssertUtils.logAndThrow("es search fail:" + esResult.getString("message"), locale);
//        }
//        return null;
//    }
//
//
//    /**
//     * 转化成修改的entity
//     *
//     * @param out 出库单记录
//     */
//
//    private ScBTransfer createUpdateDo(ScBOut out, Long mainId, ScBTransfer transfer, User user, String action) {
//        //	根据本单出库情况，更新对应调拨单的出库日期、运输方式、快递公司、运单号、物流备注、拣货状态、出库状态、总出库数量、总出库吊牌金额、出库人、出库时间；
//        transfer.setId(mainId);
//        transfer.setOutDate(new Date());
//        //  字段不存在   transfer.setTransWay();
//        transfer.setTransWayNo(out.getTransWayNo());
//        transfer.setCpCLogisticsId(out.getCpCLogisticsId());
//        transfer.setLogisticsRemark(out.getLogisticsRemark());
//        transfer.setPickStatus(out.getPickStatus());
//        transfer.setTotQtyOut(out.getTotQtyOut());
//        transfer.setTotAmtOutList(out.getTotAmtOutList());
//        //测试提出：差异数量 出库减入库 此时入库为0
//        transfer.setTotQtyDiff(out.getTotQtyOut());
//        Date date = new Date();
//        transfer.setModifiername(user.getName());
//        transfer.setModifierename(user.getEname());
//        transfer.setModifierid(user.getId().longValue());
//        transfer.setModifieddate(date);
//        if (StringUtils.isNotEmpty(action)) {
//            if ("SUBMIT".equalsIgnoreCase(action)) {
//                transfer.setOuterId(user.getId().longValue());
//                transfer.setOuterEname(user.getEname());
//                transfer.setOuterName(user.getName());
//                transfer.setOutTime(date);
//                transfer.setOutDate(out.getOutDate());
//                transfer.setOutStatus(Constants.STATUS_FINISHED);
//            } else if ("PICK".equalsIgnoreCase(action)) {
//                transfer.setPickerName(user.getName());
//                transfer.setPickerEname(user.getEname());
//                transfer.setPickerId(user.getId().longValue());
//                transfer.setPickTime(out.getPickTime());
//            }
//        }
//        return transfer;
//    }
//
//
//    /**
//     * 转化成修改的entity
//     *
//     * @param outItem 出库单明细记录
//     */
//    private ScBTransferItem createUpdateItemDo(ScBOutItem outItem, User user) {
//        //更新调拨单明细表的出库数量、差异数量（出库数量-入库数量）、出库吊牌金额
//        ScBTransferItem item = new ScBTransferItem();
//        item.setQtyOut(outItem.getQty());
//        item.setAmtOutList(outItem.getAmtOutList());
//        if (item.getQtyIn() == null) {
//            item.setQtyIn(BigDecimal.ZERO);
//        }
//        BigDecimal qtyIn = item.getQtyIn();
//        AssertUtils.notNull(outItem.getQty(), "出库数量为空!");
//        item.setQtyDiff(outItem.getQty().subtract(qtyIn));
//        item.setModifiername(user.getName());
//        item.setModifierename(user.getEname());
//        item.setModifierid(user.getId().longValue());
//        item.setModifieddate(new Date());
//        return item;
//    }
//
//
//    private ScBTransfer createInUpdateDo(ScBIn in, Long mainId, ScBTransfer transfer, User user) {
//        /*
//        * 根据本单入库情况，更新对应调拨单的入库日期、入库状态、总入库数量、总入库吊牌金额、入库人、入库时间；
//        * */
//        transfer.setId(mainId);
//        Date date = new Date();
//
//        transfer.setTotQtyIn(in.getTotQtyIn());
//        transfer.setTotQtyDiff(in.getTotQtyDiff());
//        transfer.setTotAmtInList(in.getTotAmtInList());
//
//        transfer.setModifiername(user.getName());
//        transfer.setModifierename(user.getEname());
//        transfer.setModifierid(user.getId().longValue());
//        transfer.setModifieddate(date);
//
//        transfer.setInStatus(Constants.STATUS_FINISHED);
//        transfer.setInerName(user.getName());
//        transfer.setInerEname(user.getEname());
//        transfer.setInerId(user.getId().longValue());
//        transfer.setInDate(in.getInDate());
//        transfer.setInTime(date);
//        return transfer;
//    }
//
//    /**
//     * 转化成修改的entity
//     *
//     * @param inItem 入库单明细记录
//     */
//    private ScBTransferItem createUpdateInItemDo(ScBInItem inItem, User user) {
//        // 更新调拨单明细表的入库数量、差异数量（出库数量-入库数量）、入库吊牌金额
//        ScBTransferItem item = new ScBTransferItem();
//        item.setQtyIn(inItem.getQty());
//        item.setQtyDiff(inItem.getQtyDiff());
//        item.setAmtInList(inItem.getAmtInList());
//        item.setModifiername(user.getName());
//        item.setModifierename(user.getEname());
//        item.setModifierid(user.getId().longValue());
//        item.setModifieddate(new Date());
//        return item;
//    }
//
//}
