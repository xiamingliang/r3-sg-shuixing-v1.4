package com.jackrain.nea.sg.transfer.services;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Lists;
import com.jackrain.nea.ac.sc.basic.model.enums.SourceBillType;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.cpext.model.table.CpCustomer;
import com.jackrain.nea.model.util.AdParamUtil;
import com.jackrain.nea.model.util.ModelUtil;
import com.jackrain.nea.oc.basic.common.OcBasicConstants;
import com.jackrain.nea.oc.sale.api.OcSendOutQtyUpdateCmd;
import com.jackrain.nea.oc.sale.model.request.*;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.config.SgStorageBoxConfig;
import com.jackrain.nea.sg.basic.mapper.CpCPhyWarehouseMapper;
import com.jackrain.nea.sg.basic.model.table.CpCPhyWarehouse;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.basic.utils.StorageUtils;
import com.jackrain.nea.sg.in.api.SgPhyInNoticesSelectBySourceCmd;
import com.jackrain.nea.sg.in.api.SgPhyInPickOrderGenerateCmd;
import com.jackrain.nea.sg.in.model.request.SgPhyInPickUpGoodSaveRequest;
import com.jackrain.nea.sg.out.api.SgPhyOutQueryCmd;
import com.jackrain.nea.sg.out.common.SgOutConstantsIF;
import com.jackrain.nea.sg.out.model.request.SgOutResultMQRequest;
import com.jackrain.nea.sg.out.model.result.SgOutResultItemMqResult;
import com.jackrain.nea.sg.out.model.result.SgOutResultMqResult;
import com.jackrain.nea.sg.out.model.result.SgOutResultSendMsgResult;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutResultImpItem;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutResultTeusItem;
import com.jackrain.nea.sg.transfer.common.ScTransferConstants;
import com.jackrain.nea.sg.transfer.common.SgTransferConstantsIF;
import com.jackrain.nea.sg.transfer.enums.SgTransferBillStatusEnum;
import com.jackrain.nea.sg.transfer.erp.SgTransferOutSynErp;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferImpItemMapper;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferItemMapper;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferMapper;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferOutItemMapper;
import com.jackrain.nea.sg.transfer.model.request.SgTransferBoxItemRequest;
import com.jackrain.nea.sg.transfer.model.request.SgTransferImpItemRequest;
import com.jackrain.nea.sg.transfer.model.request.SgTransferOutWBItemRequest;
import com.jackrain.nea.sg.transfer.model.request.SgTransferOutWBRequest;
import com.jackrain.nea.sg.transfer.model.table.ScBTransfer;
import com.jackrain.nea.sg.transfer.model.table.ScBTransferImpItem;
import com.jackrain.nea.sg.transfer.model.table.ScBTransferItem;
import com.jackrain.nea.sg.transfer.model.table.ScBTransferOutItem;
import com.jackrain.nea.sg.transfer.utils.SgTransferBillGeneraUtil;
import com.jackrain.nea.sg.transfer.utils.SgTransferBillStatusUtil;
import com.jackrain.nea.sg.transfer.utils.SgTransferBillUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.utils.AssertUtils;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author csy
 * Date: 2019/5/8
 * Description:  调拨单-出库结果单回写
 */

@Slf4j
@Component
public class SgTransferOutResultService {

    @Autowired
    private ScBTransferBoxFunctionService boxFunctionService;

    @Autowired
    private SgStorageBoxConfig boxConfig;

    @Autowired
    private SgTransferRpcService rpcService;

    @Autowired
    private SgTransferBillUtils billUtils;

    @Autowired
    private SgTransferOutStockService sgTransferOutStockService;

    @Reference(group = "sg", version = "1.0")
    SgPhyInNoticesSelectBySourceCmd sgPhyInNoticesSelectBySourceCmd;
    @Reference(group = "sg", version = "1.0")
    SgPhyInPickOrderGenerateCmd sgPhyInPickOrderGenerateCmd;

    public ValueHolderV14 writeBackOutResult(SgOutResultMQRequest requestMQ, boolean isCancelOut) {
        if (log.isDebugEnabled()) {
            log.debug("出库结果单mq参数:" + requestMQ);
        }
        User loginUser = requestMQ.getLoginUser();
        AssertUtils.notNull(loginUser, "用户信息不能为空！");
        Locale locale = loginUser.getLocale();

        Long outResultId = requestMQ.getId();
        AssertUtils.notNull(outResultId, "出库结果单ID不能为空！", locale);
        //mq入参中是否一键出入库标志
        Boolean isOneClickOutLibrary = requestMQ.getIsOneClickOutLibrary();
        if (isOneClickOutLibrary == null) {
            isOneClickOutLibrary = false;
        }
        List<Long> ids = new ArrayList<>();
        ids.add(outResultId);

        SgPhyOutQueryCmd outQueryCmd = (SgPhyOutQueryCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(),
                SgPhyOutQueryCmd.class.getName(),
                SgConstantsIF.GROUP, SgConstantsIF.VERSION);
        ValueHolderV14<List<SgOutResultSendMsgResult>> holderV14 = outQueryCmd.querySgPhyOutResultMQBody(ids, loginUser);
        log.debug(this.getClass().getName() + ",SgTransferOutResultService:" + JSON.toJSONString(holderV14));
        if (holderV14.getCode() == ResultCode.FAIL) {
            AssertUtils.logAndThrow("出库结果单查询失败！" + holderV14.getMessage());
        }

        List<SgOutResultSendMsgResult> data = holderV14.getData();
        if (CollectionUtils.isEmpty(data)) {
            AssertUtils.logAndThrow("出库结果单查询为空！");
        }

        SgOutResultSendMsgResult sendMsgResult = data.get(0);
        Long objId = sendMsgResult.getMqResult().getSourceBillId();

        ScBTransferMapper mapper = ApplicationContextHandle.getBean(ScBTransferMapper.class);
        AssertUtils.notNull(objId, "来源单据ID不能为空！");
        ScBTransfer transfer = mapper.selectById(objId);
        // 校验调拨单
        statusCheck(transfer, locale);

        SgTransferOutResultService service = ApplicationContextHandle.getBean(SgTransferOutResultService.class);
        return service.writeBackOutResult(msgBodyToRequestModel(sendMsgResult), isOneClickOutLibrary, transfer, isCancelOut);
    }

    /**
     * 备货出库（出库不影响库存，根据调拨单回写）
     *
     * @param user  用户信息
     * @param objId 调拨单ID
     * @return v14
     */
    public ValueHolderV14 writeBackOutResultByTransfer(User user, Long objId) {

        AssertUtils.notNull(user, "用户信息为空！");
        Locale locale = user.getLocale();
        AssertUtils.notNull(objId, "调拨单ID不能为空！", locale);

        ScBTransferMapper transferMapper = ApplicationContextHandle.getBean(ScBTransferMapper.class);
        ScBTransfer transfer = transferMapper.selectById(objId);
        // 校验调拨单
        statusCheck(transfer, locale);

        // 封装调拨单明细信息
        List<SgTransferOutWBItemRequest> itemRequestList = Lists.newArrayList();
        List<SgTransferImpItemRequest> impItemRequestList = Lists.newArrayList();

        ScBTransferItemMapper itemMapper = ApplicationContextHandle.getBean(ScBTransferItemMapper.class);
        List<ScBTransferItem> itemList = itemMapper.selectList(new QueryWrapper<ScBTransferItem>().lambda()
                .eq(ScBTransferItem::getScBTransferId, objId));

        AssertUtils.cannot(CollectionUtils.isEmpty(itemList), "调拨单条码明细查询为空！", locale);
        itemList.forEach(item -> {

            // 封装调拨单条码明细
            SgTransferOutWBItemRequest itemRequest = new SgTransferOutWBItemRequest();
            BeanUtils.copyProperties(item, itemRequest);
            itemRequest.setQtyOut(item.getQty());
            itemRequestList.add(itemRequest);

            // 封装调拨单录入明细
            SgTransferImpItemRequest impItemRequest = new SgTransferImpItemRequest();
            BeanUtils.copyProperties(item, impItemRequest);
            impItemRequest.setQtyOut(item.getQty());
            impItemRequest.setPsCSpec1Id(item.getPsCClrId());
            impItemRequest.setPsCSpec1Ecode(item.getPsCClrEcode());
            impItemRequest.setPsCSpec1Ename(item.getPsCClrEname());
            impItemRequest.setPsCSpec2Id(item.getPsCSizeId());
            impItemRequest.setPsCSpec2Ecode(item.getPsCSizeEcode());
            impItemRequest.setPsCSpec2Ename(item.getPsCSizeEname());
            impItemRequest.setIsTeus(OcBasicConstants.IS_MATCH_SIZE_N);
            impItemRequestList.add(impItemRequest);
        });

        // 封装调拨出库单入参
        SgTransferOutWBRequest request = new SgTransferOutWBRequest();
        request.setObjId(objId);
        request.setLoginUser(user);
        request.setItems(itemRequestList);
        request.setImpItemRequests(impItemRequestList);
        request.setBillNo(transfer.getBillNo());
        request.setIsLastOut(0);
        request.setOutTime(new Date());

        SgTransferOutResultService service = ApplicationContextHandle.getBean(SgTransferOutResultService.class);
        return service.writeBackOutResult(request, false, transfer, false);
    }


    /**
     * 回写出结果
     */
    @Transactional(rollbackFor = Exception.class)
    public ValueHolderV14 writeBackOutResult(SgTransferOutWBRequest request, Boolean isOneClickOutLibrary, ScBTransfer transfer, boolean isCancelOut) {
        AssertUtils.notNull(request, "请求参数为空!");
        if (log.isDebugEnabled()) {
            log.debug("start SgTransferOutResultService writeBackOutResult:" + JSON.toJSONString(request));
        }
        User user = request.getLoginUser();
        AssertUtils.notNull(user, "user is null,login first!");
        Locale locale = user.getLocale();
        List<SgTransferOutWBItemRequest> wbItemRequestList = request.getItems();
        Long objId = request.getObjId();
        Long outId = request.getOutId();
        String outBillNo = request.getOutBillNo();
        AssertUtils.notNull(objId, "参数为空-objId", locale);
        AssertUtils.notNull(CollectionUtils.isEmpty(wbItemRequestList), "明细不能为空!", locale);
        // 2019-08-21添加逻辑：合并明细
        List<SgTransferOutWBItemRequest> requestItems = mergeTransferOutItem(wbItemRequestList);
        ScBTransferMapper mapper = ApplicationContextHandle.getBean(ScBTransferMapper.class);
        ScBTransfer transferNew = new ScBTransfer();
        try {


            // 2020-03-27修改逻辑：erp出，中台入时，过来的调拨单没有出库单信息，出库数量=调拨数量=入库数量
            if (outId != null && StringUtils.isNotEmpty(outBillNo)) {

                //插入出库明细
                ScBTransferOutItemMapper outItemMapper = ApplicationContextHandle.getBean(ScBTransferOutItemMapper.class);
                int count = outItemMapper.selectCount(new QueryWrapper<ScBTransferOutItem>().lambda()
                        .eq(ScBTransferOutItem::getSgBPhyOutResultId, outId)
                        .eq(ScBTransferOutItem::getSgBPhyOutResultBillNo, outBillNo));
                AssertUtils.cannot(count > 0, "已经消费过的出库结果单", locale);
                ScBTransferOutItem outItem = new ScBTransferOutItem();
                outItem.setId(ModelUtil.getSequence("SC_B_TRANSFER_OUT_ITEM"));
                outItem.setScBTransferId(objId);
                outItem.setSgBPhyOutResultId(outId);
                outItem.setSgBPhyOutResultBillNo(outBillNo);
                StorageUtils.setBModelDefalutData(outItem, user);
                outItem.setModifierename(user.getEname());
                outItemMapper.insert(outItem);
            }

            Boolean isLast = request.getIsLastOut() == 0;
            ScBTransferItemMapper itemMapper = ApplicationContextHandle.getBean(ScBTransferItemMapper.class);
            List<ScBTransferItem> origList = itemMapper.selectList(new QueryWrapper<ScBTransferItem>().lambda()
                    .eq(ScBTransferItem::getScBTransferId, objId)
                    .eq(ScBTransferItem::getIsactive, SgConstants.IS_ACTIVE_Y));
            HashMap<String, ScBTransferItem> origMap = new HashMap<>(128);
            for (ScBTransferItem transferItem : origList) {
                origMap.put(transferItem.getPsCSkuEcode(), transferItem);
            }
            //入库增量传入
            List<ScBTransferItem> inItemList = new ArrayList<>();
            requestItems.forEach(requestItem -> {
                itemParamCheck(requestItem, locale);

                ScBTransferItem origItem = origMap.get(requestItem.getPsCSkuEcode());
                AssertUtils.notNull(origItem, "不存在的出库明细" + requestItem.getPsCSkuEcode(), locale);
                ScBTransferItem updateItem = new ScBTransferItem();
                StorageESUtils.setBModelDefalutDataByUpdate(updateItem, user);
                updateItem.setModifierename(user.getEname());
                BigDecimal totalOut = requestItem.getQtyOut().add(origItem.getQtyOut());
                BigDecimal amtOutPrice = totalOut.multiply(origItem.getPriceList());
                updateItem.setQtyOut(requestItem.getQtyOut().add(origItem.getQtyOut()));
                updateItem.setAmtOutList(amtOutPrice);
                updateItem.setId(origItem.getId());
                updateItem.setQtyDiff(totalOut.subtract(origItem.getQtyIn()));
                itemMapper.updateById(updateItem);

                //            ScBTransferItem inItem=origItem
                origItem.setQtyOut(requestItem.getQtyOut());
                StorageESUtils.setBModelDefalutDataByUpdate(origItem, user);
                origItem.setModifierename(user.getEname());
                inItemList.add(origItem);
            });

            // 箱功能开启时，更新录入明细出库数量(此处需要收集录入明细，用于生成入库通知单时传来源明细id用)
            HashMap<String, ScBTransferImpItem> impMap = null;
            if (boxConfig.getBoxEnable()) {
                impMap = boxFunctionService.updateTransferImpForOut(user, objId,
                        request.getImpItemRequests(), ScTransferConstants.ACTION_OUT);
            }

            HashMap<String, BigDecimal> qtySumMap = itemMapper.selectsAllQtySum(objId);
            BigDecimal totQtyOut = qtySumMap.get("qty_out");
            BigDecimal qtySum = qtySumMap.get("qty");
            BigDecimal totQtyIn = qtySumMap.get("qty_in");
            BigDecimal amtOutList = qtySumMap.get("amt_out_list");

            //是否总数相等 【出库数量】=【调拨数量】
            Boolean isTotalEq = totQtyOut.equals(qtySum);
            SgTransferBillStatusEnum preStatus = SgTransferBillStatusEnum.getTransferBillStatus(transfer.getStatus());
            int currentBillStatus = SgTransferBillStatusUtil.getOutStatusIntVal(isTotalEq, preStatus, isLast, locale);
            List<ScBTransferItem> scBTransferItems = itemMapper.selectList(
                    new QueryWrapper<ScBTransferItem>().lambda().eq(ScBTransferItem::getScBTransferId, objId));

            //=========生成 其他单据======
            generateBill(transfer, scBTransferItems, inItemList, user, currentBillStatus, request,
                    request.getImpItemRequests(), request.getBoxItems(), impMap, isCancelOut);

            ScBTransfer updateTransfer = new ScBTransfer();
            updateTransfer = StorageESUtils.setBModelDefalutDataByUpdate(updateTransfer, user);
            updateTransfer.setId(transfer.getId());
            updateTransfer.setModifierename(user.getEname());

            // 调拨单状态（发货方调整，切入库不影响库存时，单据状态为出库完成入库完成）
            updateTransfer.setStatus((ScTransferConstants.IS_AUTO_N.equals(transfer.getIsInIncrease())
                    && SgTransferConstantsIF.HANDLE_WAY_FHTZ.equals(transfer.getHandleWay()))
                    ? SgTransferBillStatusEnum.AUDITED_ALL_OUT_ALL_IN.getVal() : currentBillStatus);

            //汇总字段计算
            updateTransfer.setTotQty(qtySum);
            updateTransfer.setTotQtyOut(totQtyOut);
            updateTransfer.setTotQtyDiff(totQtyOut.subtract(totQtyIn));
            updateTransfer.setTotAmtOutList(amtOutList);

            updateTransfer.setOutDate(new Date());
            updateTransfer.setOutTime(request.getOutTime());
            updateTransfer.setOuterId(user.getId().longValue());
            updateTransfer.setOuterEname(user.getEname());
            updateTransfer.setOuterName(user.getName());

            if (currentBillStatus >= SgTransferBillStatusEnum.AUDITED_ALL_OUT_NOT_IN.getVal()) {
                updateTransfer.setPickOutStatus(ScTransferConstants.PICK_STATUS_PICKED);
            } else {
                updateTransfer.setPickOutStatus(ScTransferConstants.PICK_STATUS_PICKING);
            }
            updateTransfer.setPickTime(request.getOutTime());
            updateTransfer.setPickerId(user.getId().longValue());
            updateTransfer.setPickerEname(user.getEname());
            updateTransfer.setPickerName(user.getName());


            mapper.updateById(updateTransfer);
            //如果一键入库标志为true，那么更新调拨单一键出库为出库完成
            if (isOneClickOutLibrary) {
                transferNew.setId(transfer.getId());
                transferNew.setReserveBigint01(ScTransferConstants.TYPE_LIBRARY_STATUS_SUCCESS_OUTSTRAGE);
                if (mapper.updateById(transferNew) > 0) {
                    if (log.isDebugEnabled()) {
                        log.debug("一键出库出库完成");
                    }
                }
            }

            //推送ES
            String index = ScTransferConstants.TRANSFER_INDEX;
            String parentKey = ScTransferConstants.TRANSFER_PARENT_FIELD;
            ScBTransfer transferEs = mapper.selectById(objId);

            List<ScBTransferItem> transferItemList = itemMapper.selectList(new QueryWrapper<ScBTransferItem>().lambda()
                    .eq(ScBTransferItem::getScBTransferId, objId));
            // 箱功能开启时，推送录入明细到es
            if (boxConfig.getBoxEnable()) {
                ScBTransferImpItemMapper impMapper = ApplicationContextHandle.getBean(ScBTransferImpItemMapper.class);
                List<ScBTransferImpItem> impItems = impMapper.selectList(
                        new QueryWrapper<ScBTransferImpItem>().lambda()
                                .eq(ScBTransferImpItem::getScBTransferId, objId));
                String type = SgConstants.SC_B_TRANSFER_IMP_ITEM;
                StorageESUtils.pushESBModelByUpdate(transferEs, impItems, transferEs.getId(), null,
                        index, index, type, parentKey, ScBTransfer.class, ScBTransferImpItem.class, false);
            } else {
                String type = ScTransferConstants.TRANSFER_TYPE;
                StorageESUtils.pushESBModelByUpdate(transferEs, transferItemList, transferEs.getId(), null,
                        index, index, type, parentKey, ScBTransfer.class, ScBTransferItem.class, false);
            }

            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + ",调拨出库服务调拨性质:" + transfer.getSgBTransferPropEname());
            }

            // 2020-03-31添加逻辑：调拨出库单出库新增逻辑（同级调拨逻辑）
            List<ScBTransferItem> itemList = transferItemList.stream().filter(item ->
                    item.getQtyOut().compareTo(BigDecimal.ZERO) > 0).collect(Collectors.toList());
            if (SgTransferConstantsIF.TRANSFER_PROP_TJ_STR.equals(transfer.getSgBTransferPropEname())) {
                // 1.生成多角单据（销售退货单）
                createRefSaleForTransferOut(user, transferEs, itemList);
                // 2.通知erp
                SgTransferOutSynErp synErp = ApplicationContextHandle.getBean(SgTransferOutSynErp.class);
                synErp.synErpTransferOut(user.getLocale(), transferEs, itemList);

            } else if (SgTransferConstantsIF.TRANSFER_PROP_TH_STR.equals(transfer.getSgBTransferPropEname())) {

                // 调拨性质=退货调拨：回写退货申请单出库数量和单据状态
                billUtils.updateRefundApply(user, transferEs, inItemList, ScTransferConstants.ACTION_OUT);

                // 通知erp
                sgTransferOutStockService.outStockTransfer(transfer, currentBillStatus);
            }

            // 2019-07-18添加逻辑：生成结算日志
            if (currentBillStatus >= SgTransferBillStatusEnum.AUDITED_ALL_OUT_NOT_IN.getVal()) {
                List<ScBTransferItem> itemsLog = itemMapper.selectList(new QueryWrapper<ScBTransferItem>().lambda()
                        .eq(ScBTransferItem::getScBTransferId, objId));
                SgTransferBillGeneraUtil generaUtil = ApplicationContextHandle.getBean(SgTransferBillGeneraUtil.class);
                generaUtil.createSettleLogForTransfer(user, transferEs, itemsLog, SourceBillType.TRANSFER_OUT, false, false);
            }
        } catch (Exception e) {
            if (isOneClickOutLibrary) {
                transferNew.setId(transfer.getId());
                transferNew.setReserveBigint01(ScTransferConstants.TYPE_LIBRARY_STATUS_FAIL_OUTSTRAGE);
                mapper.updateById(transferNew);
            }
            AssertUtils.logAndThrowExtra("调拨出库服务异常！异常信息：", e);
        }
        return new ValueHolderV14(ResultCode.SUCCESS, SgConstants.MESSAGE_STATUS_SUCCESS);
    }


    /**
     * 单据状态校验
     *
     * @param transfer 原单信息
     */

    private void statusCheck(ScBTransfer transfer, Locale locale) {
        AssertUtils.notNull(transfer, "当前记录已不存在!", locale);
        Integer billStatus = transfer.getStatus();
        AssertUtils.notNull(billStatus, "数据异常-status为空!", locale);

        AssertUtils.cannot(billStatus == SgTransferBillStatusEnum.UN_AUDITED.getVal(), "当前单据未审核!", locale);
        //出库完成标志符
        boolean isOutDone = (billStatus == SgTransferBillStatusEnum.AUDITED_ALL_OUT_PART_IN.getVal()
                || billStatus == SgTransferBillStatusEnum.AUDITED_ALL_OUT_ALL_IN.getVal()
                || billStatus == SgTransferBillStatusEnum.AUDITED_ALL_OUT_NOT_IN.getVal());
        AssertUtils.cannot(isOutDone, "当前单据出库已完成!", locale);
    }

    /**
     * 明细校验
     */
    private void itemParamCheck(SgTransferOutWBItemRequest itemRequest, Locale locale) {
        AssertUtils.notNull(itemRequest.getQtyOut(), "出库数量不能为空!", locale);
        AssertUtils.notNull(itemRequest.getPsCSkuEcode(), "出库sku不能为空!", locale);
    }


    /**
     * 创建或回写 其他单据
     *
     * @param user              用户
     * @param transfer          主表信息
     * @param currentBillStatus 当前的 单据状态
     * @param scBTransferItems  明细信息
     */
    private void generateBill(ScBTransfer transfer, List<ScBTransferItem> scBTransferItems, List<ScBTransferItem> inItems,
                              User user, int currentBillStatus, SgTransferOutWBRequest request,
                              List<SgTransferImpItemRequest> impItems, List<SgTransferBoxItemRequest> boxItems,
                              HashMap<String, ScBTransferImpItem> impMap, boolean isCancelOut) {

        Locale locale = user.getLocale();
        CpCPhyWarehouseMapper warehouseMapper = ApplicationContextHandle.getBean(CpCPhyWarehouseMapper.class);
        Long origStoreId = transfer.getCpCOrigId();
        Long destStoreId = transfer.getCpCDestId();
        AssertUtils.notNull(origStoreId, "数据异常-发货店仓id不存在", locale);
        AssertUtils.notNull(destStoreId, "数据异常-收货店仓id不存在", locale);
        CpCPhyWarehouse origWarehouse = warehouseMapper.selectByStoreId(origStoreId);
        CpCPhyWarehouse destWarehouse = warehouseMapper.selectByStoreId(destStoreId);
        AssertUtils.notNull(origWarehouse, "数据异常-发货实体仓不存在", locale);
        AssertUtils.notNull(destWarehouse, "数据异常-收货实体仓不存在", locale);
        SgTransferBillGeneraUtil generaUtil = ApplicationContextHandle.getBean(SgTransferBillGeneraUtil.class);

        // 订单逻辑暂时屏蔽掉
//        if (transfer.getOcBSendOutId() != null) {
//            updateSendOutQty(request, transfer);
//        }

        // 调用逻辑收货单保存服务
        String isInIncrease = transfer.getIsInIncrease();
        String checkPoint = AdParamUtil.getParam(ScTransferConstants.SYSTEM_RECEIVE);
        AssertUtils.cannot(StringUtils.isEmpty(checkPoint), "系统参数 收货生成节点不存在!", locale);

        // 系统参数【生成逻辑收货单】=出库且【是否入库加库存】=是时
        if (ScTransferConstants.IS_AUTO_Y.equals(isInIncrease)) {
            if (ScTransferConstants.ACTION_OUT.equalsIgnoreCase(checkPoint)) {
                generaUtil.createReceiveBill(user, transfer, scBTransferItems, true,
                        boxFunctionService.requestToImpItem(impItems), boxFunctionService.requestToBoxItem(boxItems), impMap);
            }

            // 如果当前单据是取消出库调用，则不生成入库通知单
            if (!isCancelOut) {

                // 如果本单的【自动入库】=是，则调用【新增入库通知单并新增并审核入库结果单】
                String isAutoIn = transfer.getIsAutoIn();
                if (log.isDebugEnabled()) {
                    log.debug(this.getClass().getName() + ",generateBill,isAutoIn:" + isAutoIn);
                }
                if (StringUtils.equals(isAutoIn, ScTransferConstants.IS_AUTO_Y)) {
                    // 新增入库通知单同时新增新增并审核入库结果单
                    generaUtil.autoIn(user, transfer, inItems, origWarehouse, destWarehouse, impItems, boxItems, impMap);
                } else {
                    if (log.isDebugEnabled()) {
                        log.debug(this.getClass().getName() + ",generateBill,currentBillStatus:" + currentBillStatus);
                    }
                    if (currentBillStatus >= SgTransferBillStatusEnum.AUDITED_PART_OUT_NOT_IN.getVal()) {
                        ValueHolderV14 holderV14 = generaUtil.createInNotice(user, transfer, inItems, origWarehouse, destWarehouse, impItems, boxItems, impMap);
                        if (null != holderV14.getData()) {
                            Boolean autoFlag = Boolean.valueOf(AdParamUtil.getParam(ScTransferConstants.SYSTEM_AUTO_PICKNO));
                            // 当【是否自动生成入库拣货单】=是
                            if (autoFlag) {
                                addInPick(user, transfer, (Long) holderV14.getData());
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * 更新已配已发量
     *
     * @param request  入参
     * @param transfer 调拨单对象
     */
    private void updateSendOutQty(SgTransferOutWBRequest request, ScBTransfer transfer) {

        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ",updateSendOutQtyParam:" + JSON.toJSONString(request));
        }
        int isLastOut = request.getIsLastOut();
        List<SgTransferOutWBItemRequest> items = request.getItems();


        List<OcSendOutItemQtyUpdateRequest> itemQtyUpdateRequests = new ArrayList<>();
        if (isLastOut == SgOutConstantsIF.OUT_IS_LAST_Y) {

            // 出库结果单回写数量
            HashMap<String, BigDecimal> hashMap = new HashMap<>();
            items.forEach(requestItems -> hashMap.put(requestItems.getPsCSkuEcode(), requestItems.getQtyOut()));

            ScBTransferItemMapper itemMapper = ApplicationContextHandle.getBean(ScBTransferItemMapper.class);
            List<ScBTransferItem> transferItems = itemMapper.selectList(
                    new QueryWrapper<ScBTransferItem>().lambda()
                            .eq(ScBTransferItem::getScBTransferId, transfer.getId()));

            if (CollectionUtils.isEmpty(transferItems)) {
                AssertUtils.logAndThrow("调拨单明细为空！调拨单单据编号:" + transfer.getBillNo(),
                        request.getLoginUser().getLocale());
            }

            transferItems.forEach(item -> {
                String skuCode = item.getPsCSkuEcode();
                BigDecimal qty = item.getQty();
                BigDecimal transferQtyOut = item.getQtyOut();
                OcSendOutItemQtyUpdateRequest itemQtyUpdateRequest = new OcSendOutItemQtyUpdateRequest();
                itemQtyUpdateRequest.setPsCSkuEcode(skuCode);

                // 已配量、已发量
                if (hashMap.containsKey(skuCode)) {
                    BigDecimal qtyOut = hashMap.get(skuCode);
                    itemQtyUpdateRequest.setQtyOccu(qty.add(qtyOut).subtract(transferQtyOut).negate());
                    itemQtyUpdateRequest.setQtyConsign(qtyOut);
                } else {
                    itemQtyUpdateRequest.setQtyOccu(qty.subtract(transferQtyOut).negate());
                    itemQtyUpdateRequest.setQtyConsign(BigDecimal.ZERO);
                }

                itemQtyUpdateRequests.add(itemQtyUpdateRequest);
            });

        } else {

            // 未全部出库的情况
            items.forEach(item -> {
                OcSendOutItemQtyUpdateRequest itemQtyUpdateRequest = new OcSendOutItemQtyUpdateRequest();
                itemQtyUpdateRequest.setPsCSkuEcode(item.getPsCSkuEcode());

                // 已配数量、已发数量
                itemQtyUpdateRequest.setQtyOccu(item.getQtyOut().negate());
                itemQtyUpdateRequest.setQtyConsign(item.getQtyOut());
                itemQtyUpdateRequests.add(itemQtyUpdateRequest);
            });
        }

        OcSendOutQtyUpdateRequest updateRequest = new OcSendOutQtyUpdateRequest();
        updateRequest.setObjId(transfer.getOcBSendOutId());
        updateRequest.setItems(itemQtyUpdateRequests);
        updateRequest.setUser(request.getLoginUser());

        // 箱功能开启时，需要回写录入明细的已配已发量
        if (boxConfig.getBoxEnable()) {
            updateRequest.setImpItems(boxFunctionService.createSendOutImp(request.getImpItemRequests()));
        }

        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ",transferUpdateSendOutQty 更新已配已发量入参:" + JSON.toJSONString(updateRequest));
        }
        OcSendOutQtyUpdateCmd sendOutQtyUpdateCmd = (OcSendOutQtyUpdateCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(),
                OcSendOutQtyUpdateCmd.class.getName(), "oc", "1.0");
        ValueHolderV14 v14 = sendOutQtyUpdateCmd.updateSendOutQty(updateRequest);
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ",transferUpdateSendOutQtyHolderV14:" + JSON.toJSONString(v14));
        }
    }

    /**
     * 出库结果单消息出来的明细同一sku存在多行的情况，需合并
     *
     * @param requestItems 调拨单明细
     * @return 合并后的明细
     */
    private List<SgTransferOutWBItemRequest> mergeTransferOutItem(List<SgTransferOutWBItemRequest> requestItems) {

        List<SgTransferOutWBItemRequest> mergeList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(requestItems)) {
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + ",mergeTransferOutItem,合并前:" + requestItems.size());
            }

            // 转map
            HashMap<String, SgTransferOutWBItemRequest> hashMap = new HashMap<>();
            requestItems.forEach(item -> {
                String skuCode = item.getPsCSkuEcode();
                if (hashMap.containsKey(skuCode)) {
                    SgTransferOutWBItemRequest itemRequest = hashMap.get(skuCode);
                    BigDecimal qtyOut1 = item.getQtyOut() != null ? item.getQtyOut() : BigDecimal.ZERO;
                    BigDecimal qtyOutN = itemRequest.getQtyOut() != null ? itemRequest.getQtyOut() : BigDecimal.ZERO;
                    itemRequest.setQtyOut(qtyOut1.add(qtyOutN));
                    hashMap.put(skuCode, itemRequest);
                } else {
                    hashMap.put(skuCode, item);
                }
            });
            // 转list
            mergeList.addAll(hashMap.values());

            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + ",mergeTransferOutItem,合并后:" + mergeList.size());
            }
        }
        return mergeList;
    }

    private SgTransferOutWBRequest msgBodyToRequestModel(SgOutResultSendMsgResult sendMsgRequest) {
        SgTransferOutWBRequest wbRequest = new SgTransferOutWBRequest();
        SgOutResultMqResult outResultSaveRequest = sendMsgRequest.getMqResult();
        wbRequest.setBillNo(outResultSaveRequest.getSourceBillNo());
        wbRequest.setObjId(outResultSaveRequest.getSourceBillId());
        wbRequest.setOutId(outResultSaveRequest.getId());
        wbRequest.setOutBillNo(outResultSaveRequest.getBillNo());
        wbRequest.setOutBillType(outResultSaveRequest.getSourceBillType().longValue());
        wbRequest.setIsLastOut(outResultSaveRequest.getIsLast());
        wbRequest.setOutTime(outResultSaveRequest.getOutTime());
        List<SgTransferOutWBItemRequest> inItemRequest = new ArrayList<>();

        List<SgOutResultItemMqResult> outResultItemSaveRequests = sendMsgRequest.getMqResultItem();
        for (SgOutResultItemMqResult outResultItemSaveRequest : outResultItemSaveRequests) {
            SgTransferOutWBItemRequest itemRequest = new SgTransferOutWBItemRequest();
            BeanUtils.copyProperties(outResultItemSaveRequest, itemRequest);
            itemRequest.setQtyOut(outResultItemSaveRequest.getQty());
            inItemRequest.add(itemRequest);
        }

        // 箱功能开启时，需要回写录入明细
        if (boxConfig.getBoxEnable()) {
            if (CollectionUtils.isNotEmpty(sendMsgRequest.getImpItems())) {
                List<SgTransferImpItemRequest> impItemRequests = Lists.newArrayList();
                for (SgBPhyOutResultImpItem resultImpItem : sendMsgRequest.getImpItems()) {
                    SgTransferImpItemRequest impItemRequest = new SgTransferImpItemRequest();
                    BeanUtils.copyProperties(resultImpItem, impItemRequest);
                    impItemRequests.add(impItemRequest);
                }
                wbRequest.setImpItemRequests(impItemRequests);
            }

            // 出库服务里需要生成入库通知单，需要传箱内明细
            List<SgTransferBoxItemRequest> boxItemsRequests;
            if (!CollectionUtils.isEmpty(sendMsgRequest.getTeusItems())) {
                boxItemsRequests = Lists.newArrayList();
                for (SgBPhyOutResultTeusItem mqBoxItem : sendMsgRequest.getTeusItems()) {
                    SgTransferBoxItemRequest boxItemsRequest = new SgTransferBoxItemRequest();
                    BeanUtils.copyProperties(mqBoxItem, boxItemsRequest);
                    boxItemsRequests.add(boxItemsRequest);
                }
                wbRequest.setBoxItems(boxItemsRequests);
            }
        }

        wbRequest.setItems(inItemRequest);
        wbRequest.setLoginUser(sendMsgRequest.getLoginUser());
        return wbRequest;
    }

    /**
     * 生成入库拣货单
     *
     * @param user     用户信息
     * @param transfer 调拨单
     * @param noticeId 入库通知单ID
     */
    private void addInPick(User user, ScBTransfer transfer, Long noticeId) {

        // 【新增入库拣货单服务】
        // 调拨单 ==》入库通知单==》入库拣货单
        List<Long> noticeIdsForPick = new ArrayList<>();
        noticeIdsForPick.add(noticeId);
        SgPhyInPickOrderGenerateCmd saveAndAuditCmd = (SgPhyInPickOrderGenerateCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                SgPhyInPickOrderGenerateCmd.class.getName(), "sg", "1.0");
        SgPhyInPickUpGoodSaveRequest pickUpGoodSaveRequest = new SgPhyInPickUpGoodSaveRequest();
        pickUpGoodSaveRequest.setInNoticeIdList(noticeIdsForPick);
        pickUpGoodSaveRequest.setLoginUser(user);

        // 2020-04-03添加逻辑：同级调拨/要货调拨时，传：发货店仓法人和来源性质
        pickUpGoodSaveRequest.setIsTransferOut(true);
        pickUpGoodSaveRequest.setBelongsLegal(transfer.getOrigBelongsLegal());
        if (SgTransferConstantsIF.TRANSFER_PROP_TJ_STR.equals(transfer.getSgBTransferPropEname())) {
            // 同级调拨
            pickUpGoodSaveRequest.setSourceBillProp(1L);
        } else if (SgTransferConstantsIF.TRANSFER_PROP_YH_STR.equals(transfer.getSgBTransferPropEname())) {
            // 要货调拨（同销售单的申购销售）
            pickUpGoodSaveRequest.setSourceBillProp(2L);
        } else if (SgTransferConstantsIF.TRANSFER_PROP_TH_STR.equals(transfer.getSgBTransferPropEname())) {
            // 退货调拨
            pickUpGoodSaveRequest.setSourceBillProp(3L);
        }

        ValueHolderV14<Long> holderV14 = saveAndAuditCmd.invoke(pickUpGoodSaveRequest);
        if (log.isDebugEnabled()) {
            log.debug("生成拣货单出参:" + JSON.toJSONString(holderV14));
        }
    }

    /**
     * 调拨出库单-出库服务增加逻辑
     *
     * @param user     用户信息
     * @param transfer 调拨单
     * @param itemList 调拨单明细
     */
    private boolean createRefSaleForTransferOut(User user, ScBTransfer transfer, List<ScBTransferItem> itemList) {

        try {

            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + ",createRefSaleForTransferOut:" + JSON.toJSONString(transfer));
            }

            // 检查经营类型和对应ERP账套、门店经销商品牌，返回中转仓
            Locale locale = user.getLocale();
            CpCustomer customer = billUtils.checkStoreAndCustomer(locale, transfer.getCpCOrigId(), transfer.getCpCDestId(),
                    ScTransferConstants.ACTION_OUT);

            ValueHolderV14 holderV14 = new ValueHolderV14(ResultCode.SUCCESS, "success");
            if (customer != null) {

                // 生成一张出库完成入库完成的销售退货单（不影响库存）
                OcRefundSaleBillSaveRequest request = new OcRefundSaleBillSaveRequest();
                request.setObjId(-1L);
                request.setLoginUser(user);
                // 主表信息
                OcRefundSaleSaveRequest refundSaleRequest = new OcRefundSaleSaveRequest();
                // 收发货店仓
                refundSaleRequest.setCpCOrigId(transfer.getCpCOrigId());
                refundSaleRequest.setCpCOrigEcode(transfer.getCpCOrigEcode());
                refundSaleRequest.setCpCOrigEname(transfer.getCpCOrigEname());
                refundSaleRequest.setCpCDestId(customer.getCpCStoreId());
                refundSaleRequest.setCpCDestEcode(customer.getCpCStoreEcode());
                refundSaleRequest.setCpCDestEname(customer.getCpCStoreEname());
                // 单据日期、出入库日期
                Date sysDate = new Date();
                refundSaleRequest.setBillDate(sysDate);
                refundSaleRequest.setOutDate(sysDate);
                refundSaleRequest.setInDate(sysDate);
                // 出否影响库存（否），是否自动出入库（是）
                refundSaleRequest.setIsOutReduce(ScTransferConstants.IS_AUTO_N);
                refundSaleRequest.setIsInIncrease(ScTransferConstants.IS_AUTO_N);
                refundSaleRequest.setIsAutoOut(ScTransferConstants.IS_AUTO_Y);
                refundSaleRequest.setIsAutoIn(ScTransferConstants.IS_AUTO_Y);
                // 备注
                refundSaleRequest.setRemark("由调拨单出库[" + transfer.getBillNo() + "]多角产生");
                request.setRefundSaleSaveRequest(refundSaleRequest);

                // 明细信息
                List<OcRefundSaleImpItemSaveRequest> itemListRequest = Lists.newArrayList();
                itemList.forEach(item -> {
                    OcRefundSaleImpItemSaveRequest itemImp = new OcRefundSaleImpItemSaveRequest();
                    BeanUtils.copyProperties(item, itemImp);
                    itemImp.setId(-1L);
                    itemListRequest.add(itemImp);
                });
                request.setRefundSaleImpItemSaveRequests(itemListRequest);

                if (log.isDebugEnabled()) {
                    log.debug(this.getClass().getName() + ",调拨出库单出库生成销售退货单入参:" + JSON.toJSONString(request));
                }
                holderV14 = rpcService.refundSaleSaveAndAudit(request);
                if (log.isDebugEnabled()) {
                    log.debug(this.getClass().getName() + ",调拨出库单出库生成销售退货单出参:" + JSON.toJSONString(holderV14));
                }
            }
            return holderV14.isOK();
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error(this.getClass().getName() + ",调拨出库单出库失败:" + StorageUtils.getExceptionMsg(e));
            }
            return false;
        }
    }

}
