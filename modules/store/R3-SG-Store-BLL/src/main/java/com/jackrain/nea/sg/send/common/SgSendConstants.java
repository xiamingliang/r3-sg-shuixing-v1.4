package com.jackrain.nea.sg.send.common;

/**
 * @author 舒威
 * @since 2019/4/19
 * create at : 2019/4/19 16:35
 */
public interface SgSendConstants {

    /**
     * 主键
     */
    String ID = "id";

    /**
     * 条码id
     */
    String PS_C_SKU_ID = "ps_c_sku_id";

    /**
     * 逻辑仓id
     */
    String CP_C_STORE_ID = "cp_c_store_id";

    /**
     * 是否启用
     */
    String IS_ACTIVE = "isactive";

    /**
     * 序号生成器-逻辑发货单
     */
    String SEQ_SEND = "SQE_SG_B_SEND";

    /**
     * 表名
     */
    String BILL_SEND = "sg_b_send";
    String BILL_SEND_ITEM = "sg_b_send_item";
    String BILL_SEND_ID = "sg_b_send_id";

    /**
     * 来源单据信息
     */
    String SOURCE_BILL_ID = "source_bill_id";
    String SOURCE_BILL_NO = "source_bill_no";
    String SOURCE_BILL_TYPE = "source_bill_type";
    String SOURCE_BILL_ITEM_ID = "source_bill_item_id";

    String ACTION_VOID = "VOID";
    String ACTION_SUBMIT = "SUBMIT";
    String ACTION_CLEAR = "CLEAR";
}
