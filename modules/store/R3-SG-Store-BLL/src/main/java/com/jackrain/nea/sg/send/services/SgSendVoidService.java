package com.jackrain.nea.sg.send.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.redis.config.CusRedisTemplate;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.model.result.SgStorageUpdateResult;
import com.jackrain.nea.sg.basic.model.table.CpCStore;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.send.common.SgSendConstantsIF;
import com.jackrain.nea.sg.send.mapper.SgBSendItemMapper;
import com.jackrain.nea.sg.send.mapper.SgBSendMapper;
import com.jackrain.nea.sg.send.model.request.SgSendBillVoidRequest;
import com.jackrain.nea.sg.send.model.result.SgSendBillVoidResult;
import com.jackrain.nea.sg.send.model.table.SgBSend;
import com.jackrain.nea.sg.send.model.table.SgBSendItem;
import com.jackrain.nea.sg.send.utils.SgStoreESUtils;
import com.jackrain.nea.sg.send.utils.SgStoreUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @author 舒威
 * @since 2019/4/24
 * create at : 2019/4/24 13:18
 */
@Slf4j
@Component
public class SgSendVoidService {

    @Autowired
    private SgSendStorageService storageService;

    @Autowired
    private SgStoreESUtils storeESUtils;

    @Transactional(rollbackFor = Exception.class)
    public ValueHolderV14<SgSendBillVoidResult> voidSgBSend(SgSendBillVoidRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("来源单据id[" + request.getSourceBillId()
                    + "],逻辑发货单取消入参:" + JSONObject.toJSONString(request) + ";");
        }

        ValueHolderV14<SgSendBillVoidResult> v14 = new ValueHolderV14<>();
        SgStoreUtils.checkR3BModelDefalut(request);
        SgStoreUtils.checkBModelDefalut(request, false);
        Long id = request.getObjId();
        User user = request.getLoginUser();
        Locale locale = user.getLocale();

        Long sourceBillId = request.getSourceBillId();
        Integer sourceBillType = request.getSourceBillType();

        CusRedisTemplate<String, String> redisTemplate = SgStoreUtils.initRedisTemplate();
        String lockKsy = SgConstants.SG_B_SEND + ":" + sourceBillId + ":" + sourceBillType;

        SgBSendMapper sendMapper = ApplicationContextHandle.getBean(SgBSendMapper.class);
        SgBSendItemMapper sendItemMapper = ApplicationContextHandle.getBean(SgBSendItemMapper.class);

        Boolean ifAbsent = redisTemplate.opsForValue().setIfAbsent(lockKsy, "OK");
        if (ifAbsent != null && ifAbsent) {
            redisTemplate.expire(lockKsy, 30, TimeUnit.MINUTES);
        } else {
            v14.setCode(ResultCode.FAIL);
            v14.setMessage("取消逻辑发货单中，请稍后重试！");
            return v14;
        }

        try {
            SgBSend sgBSend = new SgBSend();
            List<SgBSendItem> orgSgBSendItems = Lists.newArrayList();
            List<SgBSendItem> releaseStorages = Lists.newArrayList();
            try {
                if (null != id) {
                    sgBSend = sendMapper.selectById(id);
                } else {
                    List<SgBSend> bSends = sendMapper.selectList(new QueryWrapper<SgBSend>().lambda()
                            .eq(SgBSend::getSourceBillId, sourceBillId)
                            .eq(SgBSend::getSourceBillType, sourceBillType)
                            .eq(SgBSend::getIsactive, SgConstants.IS_ACTIVE_Y));
                    if (CollectionUtils.isEmpty(bSends)) {
                        v14.setCode(request.getIsExist() ? ResultCode.SUCCESS : ResultCode.FAIL);
                        v14.setMessage("不存在来源单据id" + sourceBillId + ",来源单据类型" + sourceBillType + "的逻辑发货单!");
                        return v14;
                    } else if (bSends.size() > 1) {
                        v14.setCode(request.getIsExist() ? ResultCode.SUCCESS : ResultCode.FAIL);
                        v14.setMessage("存在多条来源单据id" + sourceBillId + ",来源单据类型" + sourceBillType + "的逻辑发货单!");
                        return v14;
                    } else {
                        sgBSend = bSends.get(0);
                    }
                }
                if (sgBSend == null) {
                    v14.setCode(request.getIsExist() ? ResultCode.SUCCESS : ResultCode.FAIL);
                    v14.setMessage("当前记录已不存在！");
                    return v14;
                }
                id = sgBSend.getId();

                orgSgBSendItems = sendItemMapper.selectList(new QueryWrapper<SgBSendItem>().lambda()
                        .eq(SgBSendItem::getSgBSendId, id));

                for (SgBSendItem orgSgBSendItem : orgSgBSendItems) {
                    SgBSendItem clone = (SgBSendItem) orgSgBSendItem.clone();
                    releaseStorages.add(clone);
                }

                //更新逻辑发货单表
                sgBSend.setTotQtyPreout(BigDecimal.ZERO);
                sgBSend.setIsactive(SgConstants.IS_ACTIVE_N);
                sgBSend.setBillStatus(SgSendConstantsIF.BILL_SEND_STATUS_CANCELED);
                StorageESUtils.setBModelDefalutDataByUpdate(sgBSend, user);
                sgBSend.setModifierename(user.getEname());
                sendMapper.updateById(sgBSend);
                orgSgBSendItems.forEach(orgSgBSendItem -> {
                    orgSgBSendItem.setQtyPreout(BigDecimal.ZERO);
                    StorageESUtils.setBModelDefalutDataByUpdate(orgSgBSendItem, user);
                    orgSgBSendItem.setModifierename(user.getEname());
                    sendItemMapper.updateById(orgSgBSendItem);
                });

                Map<Long, CpCStore> negativeStock = SgStoreUtils.getNegativeStock(null, null, null, releaseStorages);
                ValueHolderV14<SgStorageUpdateResult> callAPIResult = storageService.updateSendStoragePreout(
                        sgBSend, releaseStorages, null, null, "void", user,
                        false, negativeStock, false, request.getIsDeficiency(),null, null, null, null, null, Maps.newHashMap());
                if (callAPIResult.isOK()) {
                    //check同步库存占用结果
                    SgStoreUtils.isSuccess("取消逻辑发货单失败!", callAPIResult);
                    v14.setCode(ResultCode.SUCCESS);
                    v14.setMessage("取消逻辑发货单成功!");
                } else {
                    AssertUtils.logAndThrow("取消逻辑发货单失败!" + callAPIResult.getMessage(), locale);
                }

                //推送ES
                storeESUtils.pushESBySend(id, true, false, null, sendMapper, sendItemMapper);
            } catch (Exception e) {
                e.printStackTrace();
                AssertUtils.logAndThrowExtra("取消逻辑发货单失败", e, locale);
            }

        } catch (Exception e) {
            e.printStackTrace();
            AssertUtils.logAndThrowExtra(e, locale);
        } finally {
            try {
                redisTemplate.delete(lockKsy);
            } catch (Exception e) {
                e.printStackTrace();
                log.error(this.getClass().getName() + lockKsy + "释放锁失败，请联系管理员！");
                AssertUtils.logAndThrow("释放锁失败，请联系管理员！");
            }
        }

        if (log.isDebugEnabled()) {
            log.debug("来源单据id[" + request.getSourceBillId()
                    + "],逻辑发货单取消出参:" + v14.toJSONObject().toJSONString() + ";");
        }

        return v14;
    }
}
