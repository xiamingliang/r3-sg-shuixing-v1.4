package com.jackrain.nea.sg.transfer.services;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.oc.sale.model.table.OcBTransLog;
import com.jackrain.nea.oc.sx.api.OcRefApplyQueryCmd;
import com.jackrain.nea.oc.sx.model.request.result.OcRefundApplyResult;
import com.jackrain.nea.sg.basic.utils.StorageUtils;
import com.jackrain.nea.sg.transfer.enums.SgTransferBillStatusEnum;
import com.jackrain.nea.sg.transfer.model.table.ScBTransfer;
import com.jackrain.nea.sx.model.table.CpCCustomer;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.utils.BillType;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Micheal
 * @version 1.0
 * @date 2020/4/8 16:26
 */

@Slf4j
@Component
@Transactional(rollbackFor = Exception.class)
public class SgTransferOutStockService {

    @Autowired
    private SgTransferRpcService sgTransferRpcService;
    @Reference(version = "1.0", group = "oc")
    private OcRefApplyQueryCmd ocRefApplyQueryCmd;


    public void outStockTransfer(ScBTransfer transfer, int currentBillStatus) {

        try {
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + ",调拨单状态:" + currentBillStatus);
            }

            if (SgTransferBillStatusEnum.AUDITED_ALL_OUT_NOT_IN.getVal() == currentBillStatus) {

                CpCCustomer customer = sgTransferRpcService.queryCpCustomer(transfer.getCpCCustomerId());
                if (customer != null && customer.getIsErpOrg() == 1) {

                    if (log.isDebugEnabled()) {
                        log.debug(this.getClass().getName() + ",调拨出库查询退货申请单入参:" + JSON.toJSONString(transfer.getApplyBillNo()));
                    }
                    ValueHolderV14<List<OcRefundApplyResult>> holderV14 = ocRefApplyQueryCmd.findRefundApplyOrder(transfer.getApplyBillNo());
                    if (log.isDebugEnabled()) {
                        log.debug(this.getClass().getName() + ",调拨出库查询退货申请单出参:" + JSON.toJSONString(holderV14));
                    }

                    List<OcRefundApplyResult> result = holderV14.getData();
                    if (CollectionUtils.isNotEmpty(result)) {
                        JSONObject jsonObject = new JSONObject();
                        OcRefundApplyResult ocRefundApplyResultMain = result.get(0);

                        //单据编号
                        jsonObject.put("tc_abf21", transfer.getBillNo());
                        //单据日期
                        jsonObject.put("tc_abf03", transfer.getBillDate());
                        //cp_c_orig_ecode发出店仓编码
                        jsonObject.put("tc_abfplant", ocRefundApplyResultMain.getCpCOrigEcode());
                        //物流公司ECODE
                        jsonObject.put("tc_abf15", ocRefundApplyResultMain.getCpCLogisticsEcode());
                        //物流单号
                        jsonObject.put("tc_abf17", ocRefundApplyResultMain.getTransWayNo());
                        //出库箱数
                        jsonObject.put("tc_abf23", ocRefundApplyResultMain.getQtyTeusOut());
                        //出库包数
                        jsonObject.put("tc_abf24", ocRefundApplyResultMain.getQtyPackageOut());
                        //出库件数
                        jsonObject.put("tc_abf25", ocRefundApplyResultMain.getQtyPieceOut());
                        //备注
                        jsonObject.put("tc_abf12", transfer.getRemark());
                        //退货客户编号
                        jsonObject.put("tc_abf13", ocRefundApplyResultMain.getCpCClientEcode());
                        //所属法人
                        jsonObject.put("tc_abflegal", ocRefundApplyResultMain.getBelongsLegalEcode());

                        JSONArray detailList = new JSONArray();
                        result.forEach(ocRefundApplyResult -> {
                            JSONObject object = new JSONObject();
                            // 商品编码
                            object.put("tc_abg04", ocRefundApplyResult.getPsCProEcode());
                            // 箱码
                            object.put("tc_abg11", ocRefundApplyResult.getTeusNo());
                            // 箱型
                            object.put("tc_abg06", ocRefundApplyResult.getTeusType());
                            // 退货数量
                            object.put("tc_abg07", ocRefundApplyResult.getQtyRefund());
                            // 退货原因
                            object.put("tc_abg10", ocRefundApplyResult.getRefundReason());
                            object.put("tc_abgplant", ocRefundApplyResultMain.getCpCOrigEcode());
                            object.put("tc_abglegal", ocRefundApplyResultMain.getBelongsLegalEcode());
                            detailList.add(object);
                        });

                        JSONObject origBillContent = new JSONObject();
                        origBillContent.put("data", jsonObject);
                        jsonObject.put("detailList", detailList);

                        JSONObject serviceObject = new JSONObject();
                        serviceObject.put("name", "returnOutOfStock");
                        origBillContent.put("service", serviceObject);

                        OcBTransLog transLog = new OcBTransLog();
                        transLog.setOrigBillContent(origBillContent.toJSONString());
                        transLog.setSourceBillNo(transfer.getId() + "");
                        transLog.setBillType(BillType.REFUND_TRANSFER_OUT_SYNC.getType());

                        if (log.isDebugEnabled()) {
                            log.debug("调拨出库同步erp传中间表入参:" + JSON.toJSONString(transLog));
                        }
                        //调用invoke方法
                        ValueHolderV14<OcBTransLog> invoke = sgTransferRpcService.synTransLog(transLog);
                        if (log.isDebugEnabled()) {
                            log.debug("调拨出库同步erp传中间表出参:" + JSON.toJSONString(invoke));
                        }
                    }
                }
            }
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error(this.getClass().getName() + ",退货调拨出库通知erp异常:", StorageUtils.getExceptionMsg(e));
            }
        }
    }
}
