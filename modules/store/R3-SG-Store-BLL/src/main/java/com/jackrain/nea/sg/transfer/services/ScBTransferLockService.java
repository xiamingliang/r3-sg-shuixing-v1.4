package com.jackrain.nea.sg.transfer.services;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.Feature;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.redis.config.CusRedisTemplate;
import com.jackrain.nea.redis.util.RedisOpsUtil;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.send.api.SgSendCmd;
import com.jackrain.nea.sg.send.model.request.SgSendBillSaveRequest;
import com.jackrain.nea.sg.send.model.request.SgSendItemSaveRequest;
import com.jackrain.nea.sg.send.model.request.SgSendSaveRequest;
import com.jackrain.nea.sg.send.model.result.SgSendBillSaveResult;
import com.jackrain.nea.sg.transfer.common.ScTransferConstants;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferItemMapper;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferMapper;
import com.jackrain.nea.sg.transfer.model.table.ScBTransfer;
import com.jackrain.nea.sg.transfer.model.table.ScBTransferItem;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.utils.AssertUtils;
import com.jackrain.nea.web.face.User;
import com.jackrain.nea.web.query.DefaultWebEvent;
import com.jackrain.nea.web.query.QuerySession;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author: 汪聿森
 * @Date: Created in 2019-08-23 17:11
 * @Description : 锁单
 */
@Slf4j
@Component
public class ScBTransferLockService {
    @Autowired
    private ScBTransferMapper scBTransferMapper;
    @Autowired
    private ScBTransferItemMapper scBTransferItemMapper;
    @Reference(version = "1.0", group = "sg")
    private SgSendCmd sgSendCmd;

    public ValueHolder execute(QuerySession session) {
        ValueHolder vh = new ValueHolder();
        DefaultWebEvent event = session.getEvent();
        JSONObject param = JSON.parseObject(JSON.toJSONStringWithDateFormat(event.getParameterValue("param"),
                "yyyy-MM-dd HH:mm:ss", SerializerFeature.WriteMapNullValue), Feature.OrderedField);
        if (log.isDebugEnabled()) {
            log.info("ScBTransferLockService 参数:" + JSONObject.toJSONString(param));
        }
        Long objId = param.getLong("objid");
        ScBTransfer scBTransfer = scBTransferMapper.selectById(objId);
        List<ScBTransferItem> scBTransferItems = scBTransferItemMapper.selectList(
                new QueryWrapper<ScBTransferItem>().eq(ScTransferConstants.TRANSFER_PARENT_FIELD, objId));
        //记录不存在，则提示：“当前记录已不存在！”
        if (scBTransfer == null) {
            throw new NDSException(Resources.getMessage("当前记录已不存在", session.getLocale()));
        }
        String billNo = scBTransfer.getBillNo();
        String lockKsy = ScTransferConstants.TRANSFER_TYPE + ":" + billNo;
        CusRedisTemplate<String, String> redisTemplate = RedisOpsUtil.getObjRedisTemplate();
        Boolean blnCanInit = redisTemplate.opsForValue().setIfAbsent(lockKsy, "ok");
        if (blnCanInit != null && blnCanInit) {
            redisTemplate.expire(lockKsy, 60, TimeUnit.SECONDS);
        } else {
            AssertUtils.logAndThrow("当前调拨单正在操作中，请稍后重试！调拨单号：" + billNo);
        }
        try {
            Integer isLocked = scBTransfer.getIsLocked();
            //【是否锁单】=是，则提示：“当前记录的已锁单，不允许重复锁单！”
            if (isLocked != null && isLocked == ScTransferConstants.IS_LOCKED) {
                throw new NDSException(Resources.getMessage("当前记录的已锁单，不允许重复锁单！", session.getLocale()));
            }
            Integer status = scBTransfer.getStatus();
            //【单据状态】<>未审核，则提示：“当前记录的单据状态不允许锁单！”
            if (ScTransferConstants.SEND_OUT_STATUS_WAIT != status) {
                throw new NDSException(Resources.getMessage("当前记录的单据状态不允许锁单！", session.getLocale()));
            }
            //调用【新增或更新逻辑发货单】的服务
            saveSgSend(session.getUser(), scBTransfer, scBTransferItems);
            //更新本单的【是否锁单】=是
            scBTransfer.setIsLocked(1);
            scBTransfer.setModifierid(Long.valueOf(session.getUser().getId()));
            scBTransfer.setModifiername(session.getUser().getName());
            scBTransfer.setModifierename(session.getUser().getEname());
            scBTransfer.setModifieddate(new Timestamp(System.currentTimeMillis()));
            if (scBTransferMapper.updateById(scBTransfer) > 0) {
                vh.put("code", 0);
                vh.put("message", Resources.getMessage("锁单成功！", session.getLocale()));
            } else {
                vh.put("code", -1);
                vh.put("message", Resources.getMessage("锁单失败！", session.getLocale()));
            }
        } catch (NDSException e) {
            log.error(this.getClass().getName() + "调拨单锁单异常" + e);
            AssertUtils.logAndThrowExtra("调拨单锁单异常", e);
        } finally {
            try {
                redisTemplate.delete(lockKsy);
            } catch (Exception e) {
                log.error(this.getClass().getName() + "redis锁释放失败，请联系管理员！lockKsy：" + lockKsy);
                AssertUtils.logAndThrow("redis锁释放失败，请联系管理员！");
            }
        }
        return vh;
    }

    /**
     * 即调用【新增或更新逻辑发货单】的服务
     *
     * @param user             用户信息
     * @param scBTransfer      调拨单
     * @param scBTransferItems 调拨单明细
     */
    public void saveSgSend(User user, ScBTransfer scBTransfer, List<ScBTransferItem> scBTransferItems) {
        SgSendBillSaveRequest sendBillSaveRequest = new SgSendBillSaveRequest();
        /** 主表信息 */
        SgSendSaveRequest sgSend = new SgSendSaveRequest();
        sgSend.setSourceBillType(SgConstantsIF.BILL_TYPE_TRANSFER);
        sgSend.setServiceNode(SgConstantsIF.SERVICE_NODE_TRANSFER_LOCK);
        sgSend.setSourceBillId(scBTransfer.getId());
        sgSend.setSourceBillNo(scBTransfer.getBillNo());
        sgSend.setRemark(scBTransfer.getRemark());
        sendBillSaveRequest.setSgSend(sgSend);

        Long storeId = scBTransfer.getCpCOrigId();
        String storeCode = scBTransfer.getCpCOrigEcode();
        String storeName = scBTransfer.getCpCOrigEname();

        /** 明细信息 */
        List<SgSendItemSaveRequest> itemList = new ArrayList<>();
        scBTransferItems.forEach(item -> {
            SgSendItemSaveRequest sendItemSaveRequest = new SgSendItemSaveRequest();
            BeanUtils.copyProperties(item, sendItemSaveRequest);
            sendItemSaveRequest.setCpCStoreId(storeId);
            sendItemSaveRequest.setCpCStoreEcode(storeCode);
            sendItemSaveRequest.setCpCStoreEname(storeName);
            sendItemSaveRequest.setSourceBillItemId(item.getId());
            sendItemSaveRequest.setQtyPreout(item.getQty());
            sendItemSaveRequest.setPsCSpec1Id(item.getPsCClrId());
            sendItemSaveRequest.setPsCSpec1Ecode(item.getPsCClrEcode());
            sendItemSaveRequest.setPsCSpec1Ename(item.getPsCClrEname());
            sendItemSaveRequest.setPsCSpec2Id(item.getPsCSizeId());
            sendItemSaveRequest.setPsCSpec2Ecode(item.getPsCSizeEcode());
            sendItemSaveRequest.setPsCSpec2Ename(item.getPsCSizeEname());
            // 添加国标码
            sendItemSaveRequest.setGbcode(item.getGbcode());
            itemList.add(sendItemSaveRequest);
        });

        sendBillSaveRequest.setItemList(itemList);
        sendBillSaveRequest.setPreoutWarningType(SgConstantsIF.PREOUT_RESULT_ERROR);
        sendBillSaveRequest.setUpdateMethod(SgConstantsIF.ITEM_UPDATE_TYPE_ALL);
        sendBillSaveRequest.setLoginUser(user);

        ValueHolderV14<SgSendBillSaveResult> saveSgBSendV14 = sgSendCmd.saveSgBSend(sendBillSaveRequest);
        if (saveSgBSendV14.getCode() == ResultCode.FAIL) {
            AssertUtils.logAndThrow("新增或更新逻辑发货单异常：" + saveSgBSendV14.getMessage(), user.getLocale());
        }
    }
}
