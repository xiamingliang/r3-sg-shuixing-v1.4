package com.jackrain.nea.sg.transfer.services;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.core.db.Tools;
import com.jackrain.nea.redis.config.CusRedisTemplate;
import com.jackrain.nea.redis.util.RedisOpsUtil;
import com.jackrain.nea.sg.basic.common.R3ParamConstants;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.config.SgStorageBoxConfig;
import com.jackrain.nea.sg.basic.mapper.CpCPhyWarehouseMapper;
import com.jackrain.nea.sg.basic.model.request.SgR3BaseRequest;
import com.jackrain.nea.sg.basic.model.result.SgR3BaseResult;
import com.jackrain.nea.sg.basic.model.table.CpCPhyWarehouse;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.phyadjust.api.SgPhyAdjSaveAndAuditCmd;
import com.jackrain.nea.sg.phyadjust.common.SgPhyAdjustConstantsIF;
import com.jackrain.nea.sg.phyadjust.model.request.SgPhyAdjustBillSaveRequest;
import com.jackrain.nea.sg.phyadjust.model.request.SgPhyAdjustImpItemRequest;
import com.jackrain.nea.sg.phyadjust.model.request.SgPhyAdjustSaveRequest;
import com.jackrain.nea.sg.send.utils.SgStoreESUtils;
import com.jackrain.nea.sg.transfer.common.ScTransferConstants;
import com.jackrain.nea.sg.transfer.common.SgTransferConstantsIF;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferDiffItemMapper;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferDiffMapper;
import com.jackrain.nea.sg.transfer.model.request.*;
import com.jackrain.nea.sg.transfer.model.table.ScBTransferDiff;
import com.jackrain.nea.sg.transfer.model.table.ScBTransferDiffItem;
import com.jackrain.nea.sg.transfer.utils.SgTransferBillGeneraUtil;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * @author leexh
 * @since 2019/7/19 20:09
 * desc: 调拨差异处理
 */
@Slf4j
@Component
public class ScTransferDiffService {

    @Autowired
    private SgStoreESUtils storeESUtils;


    @Autowired
    private SgStorageBoxConfig storageBoxConfig;

    /**
     * 调拨差异出库保存服务
     *
     * @param request 入参
     * @return 出参
     */
    @Transactional(rollbackFor = Exception.class)
    public ValueHolderV14<SgR3BaseResult> saveTransferDiff(ScBTransferDiffBillRequest request) {

        AssertUtils.notNull(request, "参数不能为空!");
        AssertUtils.notNull(request.getLoginUser(), "用户信息不能为空!");
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ",调拨差异处理保存入参:" + JSON.toJSONString(request));
        }

        User user = request.getLoginUser();
        String eName = user.getEname();
        AssertUtils.notNull(request.getObjId(), "objId不能为空！", user.getLocale());
        Long objId = request.getObjId();
        AssertUtils.notNull(objId, "请选择要保存的数据!", user.getLocale());

        ScBTransferDiffRequest transferDiffRequest = request.getTransferDiffRequest();
        List<ScBTransferDiffItemRequest> itemRequests = request.getTransferDiffItemRequests();
        ScBTransferDiffMapper transferDiffMapper = ApplicationContextHandle.getBean(ScBTransferDiffMapper.class);
        ScBTransferDiffItemMapper itemMapper = ApplicationContextHandle.getBean(ScBTransferDiffItemMapper.class);

        if (objId < 0) {

            AssertUtils.notNull(transferDiffRequest, "主表信息不能为空!", user.getLocale());
            if (CollectionUtils.isEmpty(itemRequests)) {
                AssertUtils.logAndThrow("调拨差异明细不能为空!", user.getLocale());
            }

            // 主表新增
            ScBTransferDiff newTransferDiff = new ScBTransferDiff();
            BeanUtils.copyProperties(transferDiffRequest, newTransferDiff);

            objId = Tools.getSequence(SgConstants.SC_B_TRANSFER_DIFF);
            newTransferDiff.setId(objId);
            ScBTransferDiff transferDiff = StorageESUtils.setBModelDefalutData(newTransferDiff, user);
            transferDiff.setOwnerename(eName);
            transferDiff.setModifierename(eName);
            transferDiffMapper.insert(transferDiff);

            // 明细新增
            List<ScBTransferDiffItem> itemList = new ArrayList<>();
            for (ScBTransferDiffItemRequest item : itemRequests) {
                ScBTransferDiffItem newTransferDiffItem = new ScBTransferDiffItem();
                BeanUtils.copyProperties(item, newTransferDiffItem);
                newTransferDiffItem.setId(Tools.getSequence(SgConstants.SC_B_TRANSFER_DIFF_ITEM));
                newTransferDiffItem.setScBTransferDiffId(objId);
                ScBTransferDiffItem transferDiffItem = StorageESUtils.setBModelDefalutData(newTransferDiffItem, user);
                transferDiffItem.setOwnerename(eName);
                transferDiffItem.setModifierename(eName);
                //默认处理方式为【发货方调整】
                //transferDiffItem.setHandleWay(SgTransferConstantsIF.HANDLE_WAY_FHTZ);
                itemList.add(transferDiffItem);
            }

            List<List<ScBTransferDiffItem>> baseModelPageList = StorageESUtils.getBaseModelPageList(
                    itemList, SgConstants.SG_COMMON_INSERT_PAGE_SIZE);
            int result = 0;
            for (List<ScBTransferDiffItem> pageList : baseModelPageList) {

                if (CollectionUtils.isEmpty(pageList)) {
                    continue;
                }
                int count = itemMapper.batchInsert(pageList);
                if (count != pageList.size()) {
                    log.error(this.getClass().getName() + ",saveTransferDiff,调拨差异处理明细批量新增失败！");
                    AssertUtils.logAndThrow("调拨差异处理明细批量新增失败！", user.getLocale());
                }
                result += count;
            }
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + ",saveTransferDiff,调拨差异处理明细新增成功条数:" + result);
            }

        } else {

            ScBTransferDiff transferDiff = transferDiffMapper.selectById(objId);
            AssertUtils.notNull(transferDiff, "当前调拨差异单已不存在！", user.getLocale());

            // 修改主表
            if (transferDiffRequest != null) {
                Integer isBatch = transferDiffRequest.getIsBatch();
                String handleWay = transferDiffRequest.getHandleWay();
                String remark = transferDiffRequest.getRemark();
                if (isBatch == null) {
                    isBatch = transferDiff.getIsBatch() == null ? SgTransferConstantsIF.IS_BATCH_Y : transferDiff.getIsBatch();
                }
                if (StringUtils.isEmpty(handleWay)) {
                    handleWay = transferDiff.getHandleWay();
                }

                if (isBatch == SgTransferConstantsIF.IS_BATCH_Y && StringUtils.isEmpty(handleWay)) {
                    AssertUtils.logAndThrow("请选择处理方式!", user.getLocale());
                } else if (isBatch == SgTransferConstantsIF.IS_BATCH_Y && StringUtils.isNotEmpty(handleWay)) {
                    transferDiff.setHandleWay(handleWay);

                    // 批量修改明细的处理方式
                    List<ScBTransferDiffItem> diffItems = itemMapper.selectList(
                            new QueryWrapper<ScBTransferDiffItem>().lambda()
                                    .eq(ScBTransferDiffItem::getScBTransferDiffId, transferDiff.getId())
                                    .eq(ScBTransferDiffItem::getHandleStatus, SgTransferConstantsIF.HANDLE_STATUS_N)
                                    .isNull(ScBTransferDiffItem::getHandleWay)
                    );
                    if (CollectionUtils.isNotEmpty(diffItems)) {
                        if (log.isDebugEnabled()) {
                            log.debug(this.getClass().getName() + ",saveTransferDiff,调拨差异处理明细批量修改数量:" + diffItems.size());
                        }
                        int result = 0;
                        List<List<ScBTransferDiffItem>> baseModelPageList = StorageESUtils.getBaseModelPageList(
                                diffItems, SgConstants.SG_COMMON_MAX_INSERT_PAGE_SIZE);
                        for (List<ScBTransferDiffItem> pageList : baseModelPageList) {
                            int count = itemMapper.updateDiffItemByIds(handleWay, StringUtils.join(getDiffItemIds(pageList), ","));
                            result += count;
                        }
                        if (log.isDebugEnabled()) {
                            log.debug(this.getClass().getName() + ",saveTransferDiff,调拨差异处理明细批量修改成功数量:" + result);
                        }
                    }

                } else if (isBatch == SgTransferConstantsIF.IS_BATCH_N) {
                    transferDiff.setHandleWay(null);
                }
                transferDiff.setIsBatch(isBatch);


                if (remark != null) {
                    transferDiff.setRemark(remark);
                }
                transferDiffMapper.updateById(transferDiff);
            }

            if (!CollectionUtils.isEmpty(itemRequests)) {
                for (ScBTransferDiffItemRequest item : itemRequests) {
                    Long id = item.getId();
                    if (id != null) {
                        ScBTransferDiffItem transferDiffItem = itemMapper.selectOne(
                                new QueryWrapper<ScBTransferDiffItem>().lambda()
                                        .eq(ScBTransferDiffItem::getScBTransferDiffId, transferDiff.getId())
                                        .eq(ScBTransferDiffItem::getId, id));
                        AssertUtils.notNull(transferDiffItem, "当前明细已不存在！", user.getLocale());
                        String itemHandleWay = item.getHandleWay();
                        Integer handleStatus = transferDiffItem.getHandleStatus();
                        if (StringUtils.isNotEmpty(itemHandleWay) && handleStatus != null &&
                                handleStatus == SgTransferConstantsIF.HANDLE_STATUS_N) {
                            ScBTransferDiffItem updateItem = new ScBTransferDiffItem();
                            updateItem.setId(id);
                            updateItem.setHandleWay(itemHandleWay);
                            StorageESUtils.setBModelDefalutDataByUpdate(updateItem, user);
                            updateItem.setModifierename(eName);
                            itemMapper.updateById(updateItem);
                        }
                    }
                }
            }
        }

        // 推送es
        if (transferDiffRequest != null || !CollectionUtils.isEmpty(itemRequests)) {
            storeESUtils.pushESByTransferDiff(objId, true, false, null, transferDiffMapper, itemMapper);
        }

        ValueHolderV14<SgR3BaseResult> v14 = new ValueHolderV14<>();
        SgR3BaseResult baseResult = new SgR3BaseResult();
        JSONObject dataJo = new JSONObject();
        dataJo.put(R3ParamConstants.OBJID, objId);
        baseResult.setDataJo(dataJo);
        v14.setCode(ResultCode.SUCCESS);
        v14.setMessage("保存成功！");
        v14.setData(baseResult);
        return v14;
    }


    /**
     * 差异处理服务
     *
     * @param request 出参
     * @return 入参
     */
    @Transactional(rollbackFor = Exception.class)
    public ValueHolderV14<SgR3BaseResult> dealTransferDiff(SgR3BaseRequest request) {

        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ".差异处理服务入参:" + JSON.toJSONString(request));
        }

        AssertUtils.notNull(request, "参数不能为空!");
        User loginUser = request.getLoginUser();
        AssertUtils.notNull(loginUser, "用户信息不能为空!");
        Locale locale = loginUser.getLocale();
        long userId = loginUser.getId().longValue();
        String name = loginUser.getName();
        String eName = loginUser.getEname();

        Long objId = request.getObjId();
        AssertUtils.notNull(objId, "请选择要处理的数据！", locale);

        CusRedisTemplate<String, String> redisTemplate = RedisOpsUtil.getStrRedisTemplate();
        String lockKsy = SgConstants.SC_B_TRANSFER_DIFF + ":" + objId;
        Boolean blnCanInit = redisTemplate.opsForValue().setIfAbsent(lockKsy, "OK");
        try {
            if (blnCanInit) {
                redisTemplate.expire(lockKsy, 60, TimeUnit.SECONDS);

                ScBTransferDiffMapper diffMapper = ApplicationContextHandle.getBean(ScBTransferDiffMapper.class);
                ScBTransferDiff transferDiff = diffMapper.selectById(objId);
                AssertUtils.notNull(transferDiff, "记录已不存在！", locale);
                if (log.isDebugEnabled()) {
                    log.debug(this.getClass().getName() + ".查询差异处理单:" + JSON.toJSONString(transferDiff));
                }

                Integer billStatus = transferDiff.getBillStatus();
                if (billStatus == SgTransferConstantsIF.BILL_STATUS_AUDIT) {
                    AssertUtils.logAndThrow("当前记录已审核，不允许差异处理！", locale);
                }
                ScBTransferDiffItemMapper itemMapper = ApplicationContextHandle.getBean(ScBTransferDiffItemMapper.class);
                List<ScBTransferDiffItem> diffItems = itemMapper.selectList(
                        new QueryWrapper<ScBTransferDiffItem>().lambda()
                                .eq(ScBTransferDiffItem::getScBTransferDiffId, objId));

                // 筛选明细中【处理状态】=未处理，并且【处理方式】不为空的明细
                if (!CollectionUtils.isEmpty(diffItems)) {
                    if (log.isDebugEnabled()) {
                        log.debug(this.getClass().getName() + ".dealTransferDiff,查询差异处理单明细:" + JSON.toJSONString(diffItems));
                    }
                    List<ScBTransferDiffItem> itemList = new ArrayList<>();
                    List<ScBTransferDiffItem> fhtzItemList = new ArrayList<>();
                    List<ScBTransferDiffItem> qhdsItemList = new ArrayList<>();
                    List<ScBTransferDiffItem> emptyHandleWayItemList = new ArrayList<>();

                    for (ScBTransferDiffItem diffItem : diffItems) {
                        Integer handleStatus = diffItem.getHandleStatus();
                        if (handleStatus == SgTransferConstantsIF.HANDLE_STATUS_N) {
                            String itemHandleWay = diffItem.getHandleWay();
                            if (StringUtils.isNotEmpty(itemHandleWay)) {

                                // 更新明细数据
                                ScBTransferDiffItem updateItem = new ScBTransferDiffItem();
                                updateItem.setId(diffItem.getId());
                                updateItem.setHandleStatus(SgTransferConstantsIF.HANDLE_STATUS_Y);
                                // 更新修改人、处理人
                                updateItem.setModifierid(userId);
                                updateItem.setModifiername(name);
                                updateItem.setModifierename(eName);
                                updateItem.setModifieddate(new Date());
                                updateItem.setHandlerId(userId);
                                updateItem.setHandlerName(name);
                                updateItem.setHandlerEname(eName);
                                updateItem.setHandleTime(new Date());
                                itemMapper.updateById(updateItem);
                                itemList.add(diffItem);
                                // 收集【处理方式】=“发货方调整”的明细，经产品确认目前只处理这一种处理方式
                                if (StringUtils.equals(itemHandleWay, SgTransferConstantsIF.HANDLE_WAY_FHTZ)) {
                                    fhtzItemList.add(diffItem);
                                    // 收集【处理方式】=“确认丢失”的明细，
                                } else if (StringUtils.equals(itemHandleWay, SgTransferConstantsIF.HANDLE_WAY_QRDS)) {
                                    qhdsItemList.add(diffItem);
                                }
                            } else {
                                // 未处理并且处理方式为空的明细，用于计算更新主表状态
                                emptyHandleWayItemList.add(diffItem);
                            }
                        }
                    }

                    // 若差异处理明细中【处理状态】=【未处理】对应的【处理方式】全部为空,则提示:
                    if (itemList.size() <= 0) {
                        AssertUtils.logAndThrow("明细处理方式为空，不允许差异处理！", locale);
                    }
                    if (log.isDebugEnabled()) {
                        log.debug(this.getClass().getName() + ",dealTransferDiff,未处理并且处理方式不为空的调拨差异明细:"
                                + JSON.toJSONString(itemList));
                    }

                    // 如果【处理方式】=“发货方调整”，生成一张“单据状态”=出库完成入库完成的调拨单并审核
                    // 差异处理 明细要做分类 2019-11-4
                    if (fhtzItemList.size() > 0) {
                        log.info(this.getClass().getName()+".fhtzItemList"+JSON.toJSONString(fhtzItemList));
                        insertAndAuditTransfer(loginUser, transferDiff, fhtzItemList,
                                SgTransferConstantsIF.TRANSFER_TYPE_DIFF, SgTransferConstantsIF.HANDLE_WAY_FHTZ);
                    } else if (CollectionUtils.isNotEmpty(qhdsItemList)) {
                        handleQhDiffItem(loginUser, transferDiff, qhdsItemList);
                    }

                    // 更新主表状态和字段
                    if (emptyHandleWayItemList.size() == 0) {
                        transferDiff.setBillStatus(SgTransferConstantsIF.BILL_STATUS_AUDIT);
                    }
                    ScBTransferDiff scBTransferDiff = StorageESUtils.setBModelDefalutDataByUpdate(transferDiff, loginUser);
                    scBTransferDiff.setModifierename(eName);
                    diffMapper.updateById(scBTransferDiff);

                    // 推送es
                    storeESUtils.pushESByTransferDiff(objId, true, false, null, diffMapper, itemMapper);
                } else {
                    return new ValueHolderV14<>(ResultCode.FAIL, "当前差异单明细为空!");
                }
            } else {
                String msg = "差异处理中，请稍后...";
                if (log.isDebugEnabled()) {
                    log.debug(this.getClass().getName() + ",debug," + msg);
                }
                AssertUtils.logAndThrow(msg, locale);
            }

        } catch (Exception e) {
            redisTemplate.delete(lockKsy);
            log.error(this.getClass().getName() + ",dealTransferDiff,差异处理异常:", e.getMessage() + "---" + e);
            AssertUtils.logAndThrow("差异处理异常:" + e.getMessage());
        } finally {
            redisTemplate.delete(lockKsy);
        }

        ValueHolderV14<SgR3BaseResult> v14 = new ValueHolderV14<>();
        SgR3BaseResult baseResult = new SgR3BaseResult();
        JSONObject dataJo = new JSONObject();
        dataJo.put(R3ParamConstants.OBJID, objId);
        baseResult.setDataJo(dataJo);
        v14.setCode(ResultCode.SUCCESS);
        v14.setMessage("处理成功！");
        v14.setData(baseResult);
        return v14;
    }

    /**
     * 生成调拨单并审核
     *
     * @param user         用户信息
     * @param transferDiff 调拨差异单
     * @param itemList     明细
     * @param transferType 调拨类型
     * @param handleWay    处理方式
     * @return
     */
    private void insertAndAuditTransfer(User user, ScBTransferDiff transferDiff, List<ScBTransferDiffItem> itemList,
                                        Integer transferType, String handleWay) {

        if(log.isDebugEnabled()){
            log.debug(this.getClass().getName()+".transferDiff"+JSON.toJSONString(transferDiff));
        }
        SgTransferBillSaveRequest request = new SgTransferBillSaveRequest();

        // 主表信息
        SgTransferSaveRequest transfer = new SgTransferSaveRequest();
        BeanUtils.copyProperties(transferDiff, transfer);
        // 单据日期、发货订单
        transfer.setId(-1L);
        transfer.setBillNo(null);
        transfer.setBillDate(new Date());
        // 发货店仓
        transfer.setCpCOrigId(transferDiff.getCpCStoreIdSend());
        transfer.setCpCOrigEcode(transferDiff.getCpCStoreEcodeSend());
        transfer.setCpCOrigEname(transferDiff.getCpCStoreEnameSend());
        // 收货店仓
        transfer.setCpCDestId(transferDiff.getCpCStoreId());
        transfer.setCpCDestEcode(transferDiff.getCpCStoreEcode());
        transfer.setCpCDestEname(transferDiff.getCpCStoreEname());
        // 调拨类型、处理方式
        transfer.setTransferType(transferType);
        transfer.setHandleWay(handleWay);
        // 出入库日期
        transfer.setOutDate(new Date());
        transfer.setInDate(new Date());
        transfer.setRemark("由调拨单：[" + transferDiff.getBillNo() + "]差异处理生成！");
        // 标记为接口调用
        transfer.setReserveVarchar01(SgConstants.IS_ACTIVE_Y);
        // 处理方式为发货方调整的赋值自动出库和自动入库为是
        if (StringUtils.isNotEmpty(handleWay) && StringUtils.equals(handleWay, SgTransferConstantsIF.HANDLE_WAY_FHTZ)) {
            transfer.setIsAutoIn(ScTransferConstants.IS_AUTO_Y);
            transfer.setIsAutoOut(ScTransferConstants.IS_AUTO_Y);
        }
        request.setTransfer(transfer);

        if (storageBoxConfig.getBoxEnable()) {

            List<SgTransferImpItemSaveRequest> items = new ArrayList<>();
            itemList.forEach(item -> {
                SgTransferImpItemSaveRequest itemSaveRequest = new SgTransferImpItemSaveRequest();
                BeanUtils.copyProperties(item, itemSaveRequest);
                itemSaveRequest.setId(-1L);
                // 调拨数量=差异单的差异数量取负；出入库数量和差异数量取0
                itemSaveRequest.setQty(item.getQtyDiff().negate());
                itemSaveRequest.setQtyOut(BigDecimal.ZERO);
                itemSaveRequest.setQtyIn(BigDecimal.ZERO);
                itemSaveRequest.setIsTeus(SgTransferConstantsIF.IS_TEUS_N);
                items.add(itemSaveRequest);
            });
            request.setImpItems(items);
        } else {
            // 明细信息
            List<SgTransferItemSaveRequest> items = new ArrayList<>();
            itemList.forEach(item -> {
                SgTransferItemSaveRequest itemSaveRequest = new SgTransferItemSaveRequest();
                BeanUtils.copyProperties(item, itemSaveRequest);
                itemSaveRequest.setId(-1L);
                // 调拨数量=差异单的差异数量取负；出入库数量和差异数量取0
                itemSaveRequest.setQty(item.getQtyDiff().negate());
                itemSaveRequest.setQtyOut(BigDecimal.ZERO);
                itemSaveRequest.setQtyIn(BigDecimal.ZERO);
                itemSaveRequest.setQtyDiff(BigDecimal.ZERO);
                items.add(itemSaveRequest);
            });
            request.setItems(items);
        }
        request.setObjId(-1L);
        request.setLoginUser(user);

        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ",insertAndAuditTransfer,新增并审核调拨单入参:" + JSON.toJSONString(request));
        }
        SgTransferSaveAndAuditService service = ApplicationContextHandle.getBean(SgTransferSaveAndAuditService.class);
        ValueHolderV14 v14 = service.saveAndAudit(request);
        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ",insertAndAuditTransfer,新增并审核调拨单出参:" + v14);
        }
        if (v14 != null && v14.getCode() == ResultCode.FAIL) {
            AssertUtils.logAndThrow("调拨差异处理服务生成调拨单失败:" + v14.getMessage(), user.getLocale());
        }
    }


    /**
     * 处理缺货丢失的 差异明细
     *
     * @param user         用户
     * @param transferDiff 主表
     * @param itemList     明细
     */
    private void handleQhDiffItem(User user, ScBTransferDiff transferDiff, List<ScBTransferDiffItem> itemList) {
        //生成 库存调整单
        SgPhyAdjSaveAndAuditCmd cmd = (SgPhyAdjSaveAndAuditCmd) ReferenceUtil.refer(ApplicationContextHandle.getApplicationContext(),
                SgPhyAdjSaveAndAuditCmd.class.getName(), "sg", "1.0");
        SgPhyAdjustBillSaveRequest request = new SgPhyAdjustBillSaveRequest();
        request.setLoginUser(user);
        request.setObjId(-1L);
        request.setPhyAdjust(buildPhyAdj(transferDiff));
        request.setImpItems(buildImpItemList(itemList));
        request.setRedisKey("sg:sc_b_transfer_diff:" + SgConstantsIF.SERVICE_NODE_TRANSFER_DIFF_DEAL + ":" + transferDiff.getId());
        ValueHolderV14 holderV14 = cmd.saveAndAuditAdj(request);
        AssertUtils.cannot(holderV14.getCode() == ResultCode.FAIL, "调用库存调整单新增并审核失败:" + holderV14.getMessage());
        //生成 团购单
        SgTransferBillGeneraUtil genUtil = ApplicationContextHandle.getBean(SgTransferBillGeneraUtil.class);
        genUtil.genRetail(user, transferDiff, itemList);
    }


    /**
     * 构建库存调整单
     *
     * @param transferDiff 差异处理
     * @return
     */
    private SgPhyAdjustSaveRequest buildPhyAdj(ScBTransferDiff transferDiff) {
        /*
 	【单据编号】：PA+YYYYMMDD+8位流水
	【单据日期】：当前日期，YYYYMMDD
	【逻辑店仓】：调拨差异处理单的发货店仓
	【调整店仓】：调拨差异处理单的发货店仓在【逻辑仓档案】上对应的实体仓
	【单据类型】：差异调整
	【调整性质】：损益调整
	【来源单据类型】：调拨差异处理单
	【来源单ID】：调拨差异处理单ID
	【来源单据编号】：调拨差异处理单单据编号
	【备注】：由调拨单：XXXX（单据编号）差异处理生成！
        * */
        SgPhyAdjustSaveRequest request = new SgPhyAdjustSaveRequest();
        Long storeId = transferDiff.getCpCStoreId();
        request.setCpCStoreId(storeId);
        request.setCpCStoreEcode(transferDiff.getCpCStoreEcode());
        request.setCpCStoreEname(transferDiff.getCpCStoreEname());
        CpCPhyWarehouseMapper mapper = ApplicationContextHandle.getBean(CpCPhyWarehouseMapper.class);
        CpCPhyWarehouse warehouse = mapper.selectByStoreId(storeId);
        AssertUtils.notNull(warehouse, String.format("店仓id为 %s 对应的实体仓不存在", storeId));
        request.setCpCPhyWarehouseId(warehouse.getId());
        request.setBillType(SgPhyAdjustConstantsIF.BILL_TYPE_DIFF);
        //t调整性质 损益
        request.setSgBAdjustPropId(SgPhyAdjustConstantsIF.ADJUST_PROP_PROFIT);
        //单据类型  差异调整
        request.setSourceBillType(SgPhyAdjustConstantsIF.BILL_TYPE_DIFF);
        request.setSourceBillId(transferDiff.getId());
        request.setSourceBillNo(transferDiff.getBillNo());
        request.setRemark(String.format("由调拨单：%s 差异处理生成！", transferDiff.getBillNo()));
        return request;
    }


    private List<SgPhyAdjustImpItemRequest> buildImpItemList(List<ScBTransferDiffItem> diffItemList) {
        List<SgPhyAdjustImpItemRequest> impItemList = new ArrayList<>();
        diffItemList.forEach(diffItem -> impItemList.add(buildImpItem(diffItem)));
        return impItemList;
    }

    private SgPhyAdjustImpItemRequest buildImpItem(ScBTransferDiffItem diffItem) {
        /*
	【吊牌价】：调拨差异处理单明细中的“吊牌价”
	【调整数量】：调拨差异处理单明细中的“差异数量”
	【吊牌金额】：调拨差异处理单明细中的“差异数量”*“吊牌价”
*/
        SgPhyAdjustImpItemRequest item = new SgPhyAdjustImpItemRequest();
        item.setIsTeus(SgTransferConstantsIF.IS_TEUS_N);
        item.setPsCSpec1Id(diffItem.getPsCClrId());
        item.setPsCSpec1Ecode(diffItem.getPsCClrEcode());
        item.setPsCSpec1Ename(diffItem.getPsCClrEname());
        item.setPsCSpec2Id(diffItem.getPsCSizeId());
        item.setPsCSpec2Ecode(diffItem.getPsCSizeEcode());
        item.setPsCSpec2Ename(diffItem.getPsCSizeEname());
        item.setId(-1L);
        item.setPsCSkuId(diffItem.getPsCSkuId());
        item.setPsCSkuEcode(diffItem.getPsCSkuEcode());
        item.setPsCProId(diffItem.getPsCProId());
        item.setPsCProEcode(diffItem.getPsCProEcode());
        item.setPsCProEname(diffItem.getPsCProEname());
        item.setPriceList(diffItem.getPriceList());
        item.setSourceBillItemId(diffItem.getId());
        item.setQty(diffItem.getQtyDiff());
        return item;
    }

    private List<Long> getDiffItemIds(List<ScBTransferDiffItem> items) {
        List<Long> ids = new ArrayList<>();
        for (ScBTransferDiffItem item : items) {
            ids.add(item.getId());
        }
        return ids;
    }
}
