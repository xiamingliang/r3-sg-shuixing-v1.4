package com.jackrain.nea.sg.transfer.services;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.redis.config.CusRedisTemplate;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.receive.api.SgReceiveCmd;
import com.jackrain.nea.sg.receive.model.request.SgReceiveBillVoidRequest;
import com.jackrain.nea.sg.receive.model.result.SgReceiveBillVoidResult;
import com.jackrain.nea.sg.send.mapper.SgBSendMapper;
import com.jackrain.nea.sg.send.model.table.SgBSend;
import com.jackrain.nea.sg.send.utils.SgStoreESUtils;
import com.jackrain.nea.sg.send.utils.SgStoreUtils;
import com.jackrain.nea.sg.transfer.common.ScTransferConstants;
import com.jackrain.nea.sg.transfer.enums.SgTransferBillStatusEnum;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferMapper;
import com.jackrain.nea.sg.transfer.model.table.ScBTransfer;
import com.jackrain.nea.sg.transfer.utils.SgTransferBillGeneraUtil;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.utility.ExceptionUtil;
import com.jackrain.nea.utils.AssertUtils;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * @author csy
 * Date: 2019/5/8
 * Description:调拨单-作废
 */

@Slf4j
@Component
public class SgTransferVoidService {

    @Autowired
    private SgStoreESUtils storeESUtils;

    @Transactional(rollbackFor = Exception.class)
    public ValueHolderV14 cancelTransfer(Long id, User user) {
        AssertUtils.notNull(user, "user is null,login first!");
        Locale locale = user.getLocale();
        AssertUtils.notNull(id, "参数为空-objId", locale);
        CusRedisTemplate<String, String> redisTemplate = SgStoreUtils.initRedisTemplate();
        String lockKsy = ScTransferConstants.TRANSFER_INDEX + ":" + id;
        if (redisTemplate.opsForValue().get(lockKsy) != null) {
            throw new NDSException(Resources.getMessage("当前记录正在被操作,请稍后重试!", user.getLocale()));
        } else {
            try {
                redisTemplate.opsForValue().set(lockKsy, "OK", 30, TimeUnit.MINUTES);
                ScBTransferMapper mapper = ApplicationContextHandle.getBean(ScBTransferMapper.class);
                SgBSendMapper sendMapper=ApplicationContextHandle.getBean(SgBSendMapper.class);
                ScBTransfer transfer = mapper.selectById(id);
                statusCheck(transfer, locale);

                // 查找该调拨单是否有存在有未作废逻辑发货单
//                SgBSend sgBSend= sendMapper.selectOne(new QueryWrapper<SgBSend>().lambda()
//                        .eq(SgBSend::getSourceBillNo,transfer.getBillNo())
//                        .eq(SgBSend::getIsactive,"Y")
//                        .eq(SgBSend::getSourceBillType,7));
//                log.debug("SgTransferVoidService===>"+(sgBSend!=null));




                if (SgConstants.IS_ACTIVE_Y.equals(transfer.getIsOutReduce()) &&
                        SgConstants.IS_ACTIVE_Y.equals(transfer.getIsInIncrease())) {
                    //======取消逻辑发货单==========
                    SgTransferBillGeneraUtil generaUtil = ApplicationContextHandle.getBean(SgTransferBillGeneraUtil.class);
                    generaUtil.cancelSend(user, transfer);

                    // ======取消逻辑发收货单==========
                    generaUtil.cancelRecive(user,transfer);

                }
                ScBTransfer updateTransfer = new ScBTransfer();
                updateTransfer.setId(transfer.getId());
                StorageESUtils.setBModelDefalutDataByUpdate(updateTransfer, user);
                updateTransfer.setModifierename(user.getEname());
                updateTransfer.setStatus(SgTransferBillStatusEnum.VOIDED.getVal());
                updateTransfer.setIsactive(SgConstants.IS_ACTIVE_N);
                updateTransfer.setDelerId(user.getId().longValue());
                updateTransfer.setDelerEname(user.getEname());
                updateTransfer.setDelerName(user.getName());
                updateTransfer.setDelTime(new Date());
                mapper.updateById(updateTransfer);

                //推送ES
                storeESUtils.pushESByTransfer(id, false, false, null, mapper, null);

            } catch (Exception e) {
                AssertUtils.logAndThrow(ExceptionUtil.getMessage(e), locale);
            } finally {
                redisTemplate.delete(lockKsy);
            }
        }
        return new ValueHolderV14(ResultCode.SUCCESS, SgConstants.MESSAGE_STATUS_SUCCESS);
    }


    private void statusCheck(ScBTransfer transfer, Locale locale) {
        /*f)	单据状态<>未审核，则提示：“当前记录已审核，不允许作废！”
          g)	单据状态=已作废，则提示：“当前记录已作废，不允许重复作废！”
          h)	单据不存在，则提示：“当前记录已不存在！”*/
        AssertUtils.notNull(transfer, "当前单据已不存在!", locale);
        Integer billStatus = transfer.getStatus();
        AssertUtils.notNull(billStatus, "数据异常-STATUS不存在!", locale);
        AssertUtils.cannot(billStatus == SgTransferBillStatusEnum.VOIDED.getVal(), "当前记录已作废，不允许重复作废！", locale);
        AssertUtils.cannot(billStatus == SgTransferBillStatusEnum.AUDITED_NOT_OUT.getVal(), "当前记录已审核，不允许作废！", locale);
        AssertUtils.cannot((billStatus > SgTransferBillStatusEnum.AUDITED_NOT_OUT.getVal() &&
                billStatus < SgTransferBillStatusEnum.VOIDED.getVal()), "当前记录已有出/入库记录，不允许作废！", locale);
        Integer isLocked = transfer.getIsLocked();
        AssertUtils.cannot(isLocked == ScTransferConstants.IS_LOCKED, "当前调拨单是锁单状态，不允许作废!", locale);
    }
}
