package com.jackrain.nea.sg.transfer.services;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.util.TypeUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Lists;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.model.util.ModelUtil;
import com.jackrain.nea.oc.basic.common.OcBasicConstants;
import com.jackrain.nea.oc.basic.model.request.QueryBoxRequest;
import com.jackrain.nea.oc.basic.services.QueryBoxItemsService;
import com.jackrain.nea.oc.sale.api.OcSendOutQueryCmd;
import com.jackrain.nea.oc.sale.model.request.OcSendOutBillQueryRequest;
import com.jackrain.nea.oc.sale.model.request.OcSendOutImpItemQtyUpdateRequest;
import com.jackrain.nea.oc.sale.model.result.OcSendOutResult;
import com.jackrain.nea.oc.sale.model.table.OcBSendOutImpItem;
import com.jackrain.nea.psext.model.table.PsCTeusItem;
import com.jackrain.nea.redis.config.CusRedisTemplate;
import com.jackrain.nea.redis.util.RedisOpsUtil;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.basic.utils.StorageUtils;
import com.jackrain.nea.sg.in.model.request.SgPhyInNoticesBillSaveRequest;
import com.jackrain.nea.sg.in.model.request.SgPhyInNoticesImpItemSaveRequest;
import com.jackrain.nea.sg.in.model.request.SgPhyInNoticesTeusItemSaveRequest;
import com.jackrain.nea.sg.in.model.table.SgBPhyInResultImpItem;
import com.jackrain.nea.sg.in.model.table.SgBPhyInResultTeusItem;
import com.jackrain.nea.sg.out.model.request.*;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutResultImpItem;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutResultTeusItem;
import com.jackrain.nea.sg.receive.model.request.SgReceiveBillSaveRequest;
import com.jackrain.nea.sg.receive.model.request.SgReceiveBillSubmitRequest;
import com.jackrain.nea.sg.receive.model.request.SgReceiveImpItemSaveRequest;
import com.jackrain.nea.sg.receive.model.request.SgReceiveTeusItemSaveRequest;
import com.jackrain.nea.sg.send.model.request.SgSendBillSaveRequest;
import com.jackrain.nea.sg.send.model.request.SgSendBillSubmitRequest;
import com.jackrain.nea.sg.send.model.request.SgSendImpItemSaveRequest;
import com.jackrain.nea.sg.send.model.request.SgSendTeusItemSaveRequest;
import com.jackrain.nea.sg.transfer.common.ScTransferConstants;
import com.jackrain.nea.sg.transfer.common.SgTransferConstantsIF;
import com.jackrain.nea.sg.transfer.enums.SgTransferBillStatusEnum;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferImpItemMapper;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferItemMapper;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferMapper;
import com.jackrain.nea.sg.transfer.mapper.ScBTransferTeusItemMapper;
import com.jackrain.nea.sg.transfer.model.request.SgTransferBoxItemRequest;
import com.jackrain.nea.sg.transfer.model.request.SgTransferImpItemRequest;
import com.jackrain.nea.sg.transfer.model.table.ScBTransfer;
import com.jackrain.nea.sg.transfer.model.table.ScBTransferImpItem;
import com.jackrain.nea.sg.transfer.model.table.ScBTransferItem;
import com.jackrain.nea.sg.transfer.model.table.ScBTransferTeusItem;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.tableService.MainTableRecord;
import com.jackrain.nea.tableService.SubTableRecord;
import com.jackrain.nea.tableService.constants.Constants;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.utils.AssertUtils;
import com.jackrain.nea.utils.OcCommonUtils;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @Discription: 调拨单 箱功能 相关方法
 * @Author cs
 * @Date 2019/10/18 13:56
 */
@Slf4j
@Component
public class ScBTransferBoxFunctionService {


    @Autowired
    private ScBTransferTeusItemMapper scBTransferTeusItemMapper;

    @Autowired
    private ScBTransferItemMapper scBTransferItemMapper;

    @Autowired
    private ScBTransferImpItemMapper scBTransferImpItemMapper;

    @Autowired
    private ScBTransferMapper scBTransferMapper;

    /**
     * 调拨单明细保存
     * 录入明细中只获取数量和单价，不计算金额
     *
     * @param row 入参
     */
    public void saveTransferImpItem(MainTableRecord row) {

        if (log.isDebugEnabled()) {
            log.debug(this.getClass().getName() + ",箱功能逻辑,saveTransferImpItem");
        }

        String tableName = row.getTable().getName();
        for (SubTableRecord subTableRecord : row.getSubTables().values()) {

            subTableRecord.getRows().forEach(record -> {
                JSONObject commitData = record.getCommitData();
                //获取子表信息录入明细表
                if (commitData.getLong("ID") == null || -1 == commitData.getLong("ID")) {

                    commitData.put("ID", -1);
                    // todo 调拨价取商品中心的调拨价，待PsCProSkuResult里价调拨价字段，这里暂时先给默认值
                    if (commitData.getBigDecimal("PRICE_ORDER") == null) {
                        commitData.put("PRICE_ORDER", BigDecimal.ZERO);
                    }

                    if (commitData.getBigDecimal("QTY_OUT") == null) {
                        commitData.put("QTY_OUT", BigDecimal.ZERO);
                    }
                    if (commitData.getBigDecimal("QTY_IN") == null) {
                        commitData.put("QTY_IN", BigDecimal.ZERO);
                    }
                    commitData.put("QTY_DIFF", BigDecimal.ZERO);

                    // 是否箱=否(水星没有箱的概念)
                    commitData.put("IS_TEUS", OcBasicConstants.IS_MATCH_SIZE_N);

                    if (ScTransferConstants.SC_B_TRANSFER_OUT.equals(tableName)) {
                        commitData.put("PRICE_ORDER", commitData.getBigDecimal("PRICE_FACTORY"));
                    }
                    // 出厂价用完remove调
                    commitData.remove("PRICE_FACTORY");
                }

                // 计算折扣
                BigDecimal priceOrder = commitData.getBigDecimal("PRICE_ORDER");
                if (priceOrder != null) {
                    BigDecimal priceList = record.getNewData().getBigDecimal("PRICE_LIST");
                    if (priceList != null && priceList.compareTo(BigDecimal.ZERO) != 0) {
                        commitData.put("DISCOUNT", priceOrder.divide(priceList, 2, BigDecimal.ROUND_HALF_UP));
                    } else {
                        commitData.put("DISCOUNT", BigDecimal.ZERO);
                    }
                }
            });
        }
    }

    /**
     * -- 需求 调拨单审核
     * -- 功能 如果该调拨单的 条码明细为空，
     * 1. 删除相关联的 录入明细表中 数量 = 0 的记录
     * 2. 将录入明细 写入 到条码明细
     * 3. 将录入明细 中“是否箱”=是 的记录，写入 到箱内条码明细
     */

    /**
     * 删除 关联 录入明细表 中 数量 =0 的 记录
     *
     * @param transfer
     * @return
     */
    public JSONArray deleteImpItems(ScBTransfer transfer) {

        JSONArray idsArr = new JSONArray();
        List<ScBTransferImpItem> impItems = scBTransferImpItemMapper.selectList(
                new QueryWrapper<ScBTransferImpItem>().lambda()
                        .eq(ScBTransferImpItem::getScBTransferId, transfer.getId())
                        .eq(ScBTransferImpItem::getQty, BigDecimal.ZERO));

        if (CollectionUtils.isNotEmpty(impItems)) {

            List<Long> ids = impItems.stream().map(impItem ->
                    impItem.getId()).collect(Collectors.toList());
            for (Long id : ids) {
                idsArr.add(id);
            }

            scBTransferImpItemMapper.delete(new QueryWrapper<ScBTransferImpItem>().lambda()
                    .eq(ScBTransferImpItem::getScBTransferId, transfer.getId())
                    .eq(ScBTransferImpItem::getQty, BigDecimal.ZERO));
        }

        return idsArr;
    }


    /**
     * 订单类型调拨单，需要检查录入明细在
     * 发货订单中是否存在、是否大于订单剩余量
     *
     * @param locale    国际化
     * @param objId     直发单id
     * @param sendOutId 发货订单id
     */
    public void checkTransferQty(Locale locale, Long objId, Long sendOutId, ScBTransferImpItemMapper impItemMapper) {

        // 查询录入明细中是否箱=是的记录，返回配码维度
        List<ScBTransferImpItem> boxImpItems = impItemMapper.selectImpIsBoxList(objId);

        // 查询录入明细中是否箱=否的记录，返回条码维度
        List<ScBTransferImpItem> skuImpItems = impItemMapper.selectList(new QueryWrapper<ScBTransferImpItem>().lambda()
                .eq(ScBTransferImpItem::getScBTransferId, objId)
                .eq(ScBTransferImpItem::getIsTeus, OcBasicConstants.IS_MATCH_SIZE_N));

        // 校验直发单中的录入明细在发货订单中是否存在
        if (!CollectionUtils.isEmpty(boxImpItems) || !CollectionUtils.isEmpty(skuImpItems)) {

            OcSendOutBillQueryRequest request = new OcSendOutBillQueryRequest();
            request.setId(sendOutId);
            List<OcSendOutBillQueryRequest> requests = Lists.newArrayList();
            requests.add(request);
            OcSendOutQueryCmd sendOutQueryCmd = (OcSendOutQueryCmd) ReferenceUtil.refer(
                    ApplicationContextHandle.getApplicationContext(),
                    OcSendOutQueryCmd.class.getName(), "oc", "1.0");
            ValueHolderV14<List<OcSendOutResult>> holderV14 = sendOutQueryCmd.querySendOut(requests);
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + ",调拨单查询发货订单出参:" + JSON.toJSONString(holderV14));
            }
            if (holderV14.getCode() == ResultCode.FAIL) {
                AssertUtils.logAndThrow("调拨单查询发货订单失败！" + holderV14.getMessage());
            }

            OcSendOutResult sendOutResult = holderV14.getData().get(0);
            AssertUtils.cannot(sendOutResult == null || CollectionUtils.isEmpty(sendOutResult.getImpItems()),
                    "发货订单录入明细为空！", locale);

            HashMap<String, BigDecimal> matchSizeMap = new HashMap<>();
            HashMap<String, BigDecimal> skuMap = new HashMap<>();
            for (OcBSendOutImpItem orderImpItem : sendOutResult.getImpItems()) {
                if (orderImpItem.getIsMatchsize() == OcBasicConstants.IS_MATCH_SIZE_Y) {
                    // 配码时，商品 + 配码 + 颜色作为key
                    String matchSizeKey = orderImpItem.getPsCProEcode() + orderImpItem.getPsCMatchsizeEcode() +
                            orderImpItem.getPsCSpec1Ecode();
                    matchSizeMap.put(matchSizeKey, orderImpItem.getQtyRem());
                } else {
                    skuMap.put(orderImpItem.getPsCSkuEcode(), orderImpItem.getQtyRem());
                }
            }

            if (!CollectionUtils.isEmpty(boxImpItems)) {
                if (log.isDebugEnabled()) {
                    log.debug(this.getClass().getName() + ",checkTransferQty boxImpItems:" + JSON.toJSONString(boxImpItems));
                }
                if (matchSizeMap.size() == 0) {
                    AssertUtils.logAndThrow("发货订单中不存在对应的配码，请检查！", locale);
                }

                for (ScBTransferImpItem boxImpItem : boxImpItems) {

                    // 录入明细配码维度的箱数量
                    BigDecimal qty = boxImpItem.getQty();
                    String matchSizeKey = boxImpItem.getPsCProEcode() + boxImpItem.getPsCMatchsizeEcode() +
                            boxImpItem.getPsCSpec1Ecode();
                    BigDecimal qtyRem = matchSizeMap.get(matchSizeKey);

                    // 如果发货订单录入明细中配码数量为空或者小于直发单录入明细数量，则报错
                    if (qtyRem == null || qty.compareTo(qtyRem) > 0) {
                        AssertUtils.logAndThrow("商品[" + boxImpItem.getPsCProEcode() + "]的箱数量[" + qty +
                                "]大于订单剩余数量[" + qtyRem + "]，不允许审核！", locale);
                    }
                }
            }

            if (!CollectionUtils.isEmpty(skuImpItems)) {
                if (skuMap.size() == 0) {
                    AssertUtils.logAndThrow("发货订单中不存在对应的条码，请检查！", locale);
                }

                for (ScBTransferImpItem skuImpItem : skuImpItems) {

                    // 录入明细配码维度的条码数量
                    BigDecimal qty = skuImpItem.getQty();
                    BigDecimal qtyRem = skuMap.get(skuImpItem.getPsCSkuEcode());
                    if (qtyRem == null || qty.compareTo(qtyRem) > 0) {
                        AssertUtils.logAndThrow("条码[" + skuImpItem.getPsCSkuEcode() + "]的数量[" + qty +
                                "]大于订单剩余数量[" + qtyRem + "]，不允许审核！", locale);
                    }
                }
            }
        }
    }

    public List<ScBTransferItem> getItemList(ScBTransfer transfer, Locale locale) {

        List<ScBTransferItem> itemList = scBTransferItemMapper.selectList(new QueryWrapper<ScBTransferItem>().lambda()
                .eq(ScBTransferItem::getScBTransferId, transfer.getId()));
        return itemList;
    }


    /**
     * 将 本调拨单的 录入明细 写入到 条码明细
     *
     * @param transfer
     * @param user
     */
    public void insertItemFromImp(ScBTransfer transfer, User user) {

        AssertUtils.notNull(transfer, " 该调拨单不存在请检查！");

        // 创建保存 箱内条码明细 model list
        List<ScBTransferTeusItem> scBTransferTeusItems = new ArrayList<ScBTransferTeusItem>();

        // 拆分 去重后的 map
        HashMap<String, ScBTransferItem> scBtransItemMap = new HashMap<>();
        try {

            // 查询出  所有 调拨单关联的  箱操作 录入明细 汇总
            List<ScBTransferImpItem> scBTransferImpIsTeusItems = scBTransferImpItemMapper.selectList(new QueryWrapper<ScBTransferImpItem>().lambda()
                    .eq(ScBTransferImpItem::getScBTransferId, transfer.getId())
                    .eq(ScBTransferImpItem::getIsTeus, SgTransferConstantsIF.IS_TEUS_Y));

            // 箱类型 录入明细不为空
            if (CollectionUtils.isNotEmpty(scBTransferImpIsTeusItems)) {

                // 1.收集箱号和箱信息（暂不考虑箱有重复记录的情况）
                HashMap<String, ScBTransferImpItem> teusMap = new HashMap<>();

                List<Long> boxIds = new ArrayList<>();
                // 循环 箱 操作
                scBTransferImpIsTeusItems.forEach(impItem -> {
                    teusMap.put(impItem.getPsCTeusEcode(), impItem);
                    boxIds.add(impItem.getPsCTeusId());
                });

                if (CollectionUtils.isNotEmpty(boxIds)) {
                    QueryBoxItemsService boxItemsService = ApplicationContextHandle.getBean(QueryBoxItemsService.class);
                    QueryBoxRequest boxRequest = new QueryBoxRequest();
                    boxRequest.setBoxIds(boxIds);
                    boxRequest.setBoxStatus(OcBasicConstants.BOX_STATUS_IN);
                    // 箱 明细数据  boxItems
                    List<PsCTeusItem> boxItems = boxItemsService.queryBoxItemsByBoxInfo(boxRequest);
                    if (CollectionUtils.isEmpty(boxItems)) {
                        AssertUtils.logAndThrow("箱明细为空，请检查！箱号：" + JSON.toJSONString(boxIds));
                    }

                    // 5.合并重复记录，转换成调拨单明细对象
                    boxItems.forEach(boxItemPo -> {
                        // 先计算 箱数量 ，再合并 相同条码明细
                        String boxCode = boxItemPo.getPsCTeusEcode();
                        ScBTransferImpItem scBTransferImpItem = teusMap.get(boxCode);
                        // 箱数量为0的记录前置逻辑已删除，此处为null认为箱明细没关联到箱
                        AssertUtils.notNull(scBTransferImpItem, "箱号" + boxCode + "不存在，箱和明细关联有误，请检查！");
                        if (boxItemPo.getQty() != null) {
                            // scBTransferImpItem.getQty() 箱数量  teusItem.getQty() 箱明细中的数量
                            boxItemPo.setQty(scBTransferImpItem.getQty().multiply(boxItemPo.getQty()));
                        } else {
                            boxItemPo.setQty(BigDecimal.ZERO);
                        }

                        // 5.合并重复记录，转换成调拨单明细对象
                        String skuCode = boxItemPo.getPsCSkuEcode();
                        ScBTransferItem scBTransferItem = scBtransItemMap.get(skuCode);
                        if (scBTransferItem != null) {

                            // 若存在，数量累加 重新计算吊牌金额 ，调拨价金额
                            scBTransferItem.setQty(scBTransferItem.getQty().add(boxItemPo.getQty()));
                            calculateAmt(scBTransferItem);

                        } else {
                            // 若不存在，转换成 调拨单条码明细对象 添加到集合中
                            ScBTransferItem scBTransferItemNew = new ScBTransferItem();
                            BeanUtils.copyProperties(boxItemPo, scBTransferItemNew);

                            // 获取id
                            scBTransferItemNew.setId(ModelUtil.getSequence(SgConstants.SG_B_TRANSFER_ITEM));
                            scBTransferItemNew.setScBTransferId(transfer.getId());

                            scBTransferItemNew.setPsCClrId(boxItemPo.getPsCSpec1Id());
                            scBTransferItemNew.setPsCClrEcode(boxItemPo.getPsCSpec1Ecode());
                            scBTransferItemNew.setPsCClrEname(boxItemPo.getPsCSpec1Ename());
                            scBTransferItemNew.setPsCSizeId(boxItemPo.getPsCSpec2Id());
                            scBTransferItemNew.setPsCSizeEcode(boxItemPo.getPsCSpec2Ecode());
                            scBTransferItemNew.setPsCSizeEname(boxItemPo.getPsCSpec2Ename());

                            // 箱明细中没有吊牌价，从录入明细中获取吊牌价
                            ScBTransferImpItem scBTransferImpItem2 = teusMap.get(boxItemPo.getPsCTeusEcode());
                            BigDecimal priceList = scBTransferImpItem2.getPriceList();
                            BigDecimal priceOrder = scBTransferImpItem2.getPriceOrder();
                            // 吊牌价
                            scBTransferItemNew.setPriceList(priceList);
                            scBTransferItemNew.setPriceOrder(priceOrder);
                            // 数量默认值
                            scBTransferItemNew.setQtyIn(BigDecimal.ZERO);
                            scBTransferItemNew.setQtyOut(BigDecimal.ZERO);
                            scBTransferItemNew.setQtyDiff(BigDecimal.ZERO);

                            // 吊牌金额, 调拨价金额
                            calculateAmt(scBTransferItemNew);
                            // 补充用户信息
                            StorageESUtils.setBModelDefalutData(scBTransferItemNew, user);

                            // 添加到集合中
                            scBtransItemMap.put(skuCode, scBTransferItemNew);
                        }

                        // 组装箱内 明细
                        ScBTransferTeusItem scBTransferTeusItem = new ScBTransferTeusItem();
                        BeanUtils.copyProperties(boxItemPo, scBTransferTeusItem);

                        // 获取id
                        scBTransferTeusItem.setId(ModelUtil.getSequence(SgConstants.SC_B_TRANSFER_TEUS_ITEM));
                        scBTransferTeusItem.setScBTransferId(transfer.getId());

                        StorageESUtils.setBModelDefalutData(scBTransferTeusItem, user);
                        scBTransferTeusItems.add(scBTransferTeusItem);
                    });

                }

            }

            // 查询出所有 调拨单关联的 非箱 录入明细汇总
            List<ScBTransferImpItem> scBTransferImpNotTeusItems = scBTransferImpItemMapper.selectList(new QueryWrapper<ScBTransferImpItem>().lambda()
                    .eq(ScBTransferImpItem::getScBTransferId, transfer.getId())
                    .eq(ScBTransferImpItem::getIsTeus, SgTransferConstantsIF.IS_TEUS_N));

            if (CollectionUtils.isNotEmpty(scBTransferImpNotTeusItems)) {

                // 6 非箱 数据不为空，将 数据合并
                scBTransferImpNotTeusItems.forEach(scBTransferImpItem -> {
                    String skuCode = scBTransferImpItem.getPsCSkuEcode();
                    ScBTransferItem scBTransferItem = scBtransItemMap.get(skuCode);
                    if (scBTransferItem != null) {
                        // 若存在，数量累加 重新计算吊牌金额和退货金额
                        scBTransferItem.setQty(scBTransferItem.getQty().add(scBTransferImpItem.getQty()));
                        calculateAmt(scBTransferItem);
                    } else {

                        // 若不存在，转换成 调拨条码明细 对象添加到集合中
                        ScBTransferItem scBTransferItemNew = new ScBTransferItem();
                        BeanUtils.copyProperties(scBTransferImpItem, scBTransferItemNew);
                        // 获取id
                        scBTransferItemNew.setId(ModelUtil.getSequence(SgConstants.SG_B_TRANSFER_ITEM));
                        scBTransferItemNew.setScBTransferId(transfer.getId());

                        // 颜色尺寸
                        scBTransferItemNew.setPsCClrId(scBTransferImpItem.getPsCSpec1Id());
                        scBTransferItemNew.setPsCClrEcode(scBTransferImpItem.getPsCSpec1Ecode());
                        scBTransferItemNew.setPsCClrEname(scBTransferImpItem.getPsCSpec1Ename());
                        scBTransferItemNew.setPsCSizeId(scBTransferImpItem.getPsCSpec2Id());
                        scBTransferItemNew.setPsCSizeEcode(scBTransferImpItem.getPsCSpec2Ecode());
                        scBTransferItemNew.setPsCSizeEname(scBTransferImpItem.getPsCSpec2Ename());

                        // 吊牌金额,调拨价金额
                        calculateAmt(scBTransferItemNew);
                        // 数量默认值
                        scBTransferItemNew.setQtyIn(BigDecimal.ZERO);
                        scBTransferItemNew.setQtyOut(BigDecimal.ZERO);
                        scBTransferItemNew.setQtyDiff(BigDecimal.ZERO);

                        // 补充用户信息
                        StorageESUtils.setBModelDefalutData(scBTransferItemNew, user);

                        // 添加到集合中
                        scBtransItemMap.put(skuCode, scBTransferItemNew);
                    }
                });
            }

            // 批量信息箱内明细表和条码明细表
            batchInsertItem(user.getLocale(), scBTransferTeusItems, scBtransItemMap);
            // 更新主表统计字段
            updateMainTable(user, transfer.getId());

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error(this.getClass().getName() + ",录入明细拆分失败:" + StorageUtils.getExceptionMsg(e));
            }
            AssertUtils.logAndThrow("录入明细拆分失败！失败原因：" + StorageUtils.getExceptionMsg(e));
        }

    }

    /**
     * 组装 箱内明细 model list
     *
     * @param user
     * @param objId
     * @param boxItems
     * @param boxMap
     * @return
     */
    private List<ScBTransferTeusItem> createScbTransferTeusItem(User user, Long objId, List<PsCTeusItem> boxItems,
                                                                HashMap<String, ScBTransferImpItem> boxMap) {
        List<ScBTransferTeusItem> boxItemList = new ArrayList<>();
        boxItems.forEach(boxItem -> {

            ScBTransferTeusItem scBTransferTeusItem = new ScBTransferTeusItem();
            BeanUtils.copyProperties(boxItem, scBTransferTeusItem);

            // 获取id
            scBTransferTeusItem.setId(ModelUtil.getSequence(SgConstants.SC_B_TRANSFER_TEUS_ITEM));
            scBTransferTeusItem.setScBTransferId(objId);

            StorageESUtils.setBModelDefalutData(scBTransferTeusItem, user);
            boxItemList.add(scBTransferTeusItem);
        });
        return boxItemList;
    }


    /**
     * 计算吊牌金额和退货金额
     *
     * @param scbtransferitem 对象
     */
    private void calculateAmt(ScBTransferItem scbtransferitem) {
        BigDecimal qty = scbtransferitem.getQty();

        //  吊牌价
        BigDecimal priceList = scbtransferitem.getPriceList();
        // 调拨价
        BigDecimal priceOrder = scbtransferitem.getPriceOrder();

        //  计算 添加 吊牌金额与调拨金额
        scbtransferitem.setAmtList(qty.multiply(priceList));
        scbtransferitem.setAmtOrder(qty.multiply(priceOrder));
    }


    /**
     * 批量插入调拨单  箱明细 与 条码明细
     *
     * @param locale
     * @param boxItemList
     * @param scBTransItemMap
     */
    public void batchInsertItem(Locale locale, List<ScBTransferTeusItem> boxItemList,
                                HashMap<String, ScBTransferItem> scBTransItemMap) {

        if (CollectionUtils.isNotEmpty(boxItemList)) {
            // 批量信息箱内明细
            if (boxItemList.size() > ScTransferConstants.INSERT_TEUS_NUM) {

                List<List<ScBTransferTeusItem>> dataList = StorageESUtils.getBaseModelPageList(boxItemList, TypeUtils.castToInt(ScTransferConstants.INSERT_TEUS_NUM));

                int result = 0;
                for (List<ScBTransferTeusItem> pageList : dataList) {
                    if (CollectionUtils.isEmpty(pageList)) {
                        continue;
                    }
                    int count = scBTransferTeusItemMapper.batchInsert(pageList);
                    if (count != pageList.size()) {
                        AssertUtils.logAndThrow("箱内明细批量新增失败！", locale);
                    }
                    result += count;
                }
                if (log.isDebugEnabled()) {
                    log.debug(this.getClass().getName() + ",箱内明细批量新增成功:" + result);
                }
            } else {
                int count = scBTransferTeusItemMapper.batchInsert(boxItemList);
                if (count != boxItemList.size()) {
                    AssertUtils.logAndThrow("箱内明细批量新增失败！", locale);
                }
                if (log.isDebugEnabled()) {
                    log.debug(this.getClass().getName() + ",箱内明细批量新增成功:" + count);
                }
            }


        }

        if (MapUtils.isNotEmpty(scBTransItemMap)) {
            // 不为空

            // 批量信息条码明细
            List<ScBTransferItem> scBTransferItemList = new ArrayList<>(scBTransItemMap.values());

            // map 处理， 进行插入操作
//            scBTransItemMap.values().forEach(scBTransferItem -> {
//                scBTransferItemList.add(scBTransferItem);
//            });

            if (scBTransferItemList.size() > ScTransferConstants.INSERT_ITEMS_NUM) {

                // 拆分 插入 条码明细表
                List<List<ScBTransferItem>> dataList = StorageESUtils.getBaseModelPageList(scBTransferItemList, TypeUtils.castToInt(ScTransferConstants.INSERT_ITEMS_NUM));
                int result = 0;
                for (List<ScBTransferItem> pageList : dataList) {
                    if (CollectionUtils.isEmpty(pageList)) {
                        continue;
                    }
                    int count = scBTransferItemMapper.batchInsert(pageList);
                    if (count != pageList.size()) {
                        AssertUtils.logAndThrow("条码明细批量新增失败！", locale);
                    }
                    result += count;
                }
                if (log.isDebugEnabled()) {
                    log.debug(this.getClass().getName() + ",条码明细批量新增成功:" + result);
                }
            } else {

                int count = scBTransferItemMapper.batchInsert(scBTransferItemList);
                if (count != scBTransferItemList.size()) {
                    AssertUtils.logAndThrow("条码明细批量新增失败！", locale);
                }

                if (log.isDebugEnabled()) {
                    log.debug(this.getClass().getName() + ",条码明细批量新增成功:" + count);
                }
            }
        }
    }

    /**
     * 修改 调拨单主表 记录
     *
     * @param user
     * @param objId
     */
    private void updateMainTable(User user, Long objId) {

        AssertUtils.notNull(user, "用户信息不能为空！");
        AssertUtils.notNull(objId, "调拨单 id 不能为空！", user.getLocale());

        ScBTransferItem scBTransferItem = scBTransferItemMapper.selectSumQtyAndAmtByTransferId(objId);
        if (scBTransferItem != null) {
            ScBTransfer scBTransfer = new ScBTransfer();
            scBTransfer.setId(objId);

            // 总调拨数量 ,总标准金额
            BigDecimal amtQty = scBTransferItem.getQty();
            BigDecimal amtList = scBTransferItem.getAmtList();
            scBTransfer.setTotAmtList(amtList);
            scBTransfer.setTotQty(amtQty);
            StorageESUtils.setBModelDefalutDataByUpdate(scBTransfer, user);
            scBTransferMapper.updateById(scBTransfer);
        }
    }


    /**
     * 获取 调拨单 录入明细 信息
     * -- 需求 调拨单 审核 取消审核 箱内明细 的操作
     * -- 功能 如果本单 销售类型 = 订单调拨，更新对应发货订单录入明细中的已配已发量。
     */
    public List<OcSendOutImpItemQtyUpdateRequest> getScBTransferImpItems(ScBTransfer transfer, int status) {


        // 查询出  所有 调拨单关联的  非箱 录入明细 汇总
        List<ScBTransferImpItem> scBTransferImpNotTeusItems = scBTransferImpItemMapper.selectList(new QueryWrapper<ScBTransferImpItem>().lambda()
                .eq(ScBTransferImpItem::getScBTransferId, transfer.getId())
                .eq(ScBTransferImpItem::getIsTeus, SgTransferConstantsIF.IS_TEUS_N));

        // 查询出  所有 调拨单关联的  箱操作 录入明细 汇总
        List<ScBTransferImpItem> scBTransferImpIsTeusItems = scBTransferImpItemMapper.selectImpIsBoxList(transfer.getId());

        // set 参数  OcSendOutImpItemQtyUpdateRequest 箱功能 - 录入明细
        List<OcSendOutImpItemQtyUpdateRequest> ocItemsList = new ArrayList<>();

        if (!CollectionUtils.isEmpty(scBTransferImpNotTeusItems)) {
            // 非箱操作
            for (ScBTransferImpItem scBTransferImpItem : scBTransferImpNotTeusItems) {

                // 非箱操作， 直接可以 调用 RPC
                OcSendOutImpItemQtyUpdateRequest ocSendOutImpItemQtyUpdateRequest = new OcSendOutImpItemQtyUpdateRequest();
                ocSendOutImpItemQtyUpdateRequest.setPsCSkuEcode(scBTransferImpItem.getPsCSkuEcode());
                ocSendOutImpItemQtyUpdateRequest.setIsMatchsize(OcBasicConstants.IS_MATCH_SIZE_N);
                ocSendOutImpItemQtyUpdateRequest.setPsCProId(scBTransferImpItem.getPsCProId());
                ocSendOutImpItemQtyUpdateRequest.setPsCSpec1Id(scBTransferImpItem.getPsCSpec1Id());

                if (status == SgTransferBillStatusEnum.AUDITED_NOT_OUT.getVal()) {

                    ocSendOutImpItemQtyUpdateRequest.setQtyOccu(scBTransferImpItem.getQty());
                    ocSendOutImpItemQtyUpdateRequest.setQtyConsign(BigDecimal.ZERO);
                } else if (status == SgTransferBillStatusEnum.UN_AUDITED.getVal()) {

                    ocSendOutImpItemQtyUpdateRequest.setQtyOccu(scBTransferImpItem.getQty().negate());
                    ocSendOutImpItemQtyUpdateRequest.setQtyConsign(BigDecimal.ZERO);
                } else if (status >= SgTransferBillStatusEnum.AUDITED_ALL_OUT_NOT_IN.getVal()) {

                    ocSendOutImpItemQtyUpdateRequest.setQtyOccu(BigDecimal.ZERO);
                    ocSendOutImpItemQtyUpdateRequest.setQtyConsign(scBTransferImpItem.getQty());
                }
                ocItemsList.add(ocSendOutImpItemQtyUpdateRequest);

            }
        }

        if (!CollectionUtils.isEmpty(scBTransferImpIsTeusItems)) {
            for (ScBTransferImpItem scBTransferImpItem : scBTransferImpIsTeusItems) {

                OcSendOutImpItemQtyUpdateRequest ocSendOutImpItemQtyUpdateRequest = new OcSendOutImpItemQtyUpdateRequest();

                // 箱操作 按配码 识别，不用 skuecode
                ocSendOutImpItemQtyUpdateRequest.setIsMatchsize(OcBasicConstants.IS_MATCH_SIZE_Y);
                ocSendOutImpItemQtyUpdateRequest.setPsCMatchsizeId(scBTransferImpItem.getPsCMatchsizeId());
                ocSendOutImpItemQtyUpdateRequest.setPsCProId(scBTransferImpItem.getPsCProId());
                ocSendOutImpItemQtyUpdateRequest.setPsCSpec1Id(scBTransferImpItem.getPsCSpec1Id());

                if (status == SgTransferBillStatusEnum.AUDITED_NOT_OUT.getVal()) {

                    ocSendOutImpItemQtyUpdateRequest.setQtyOccu(scBTransferImpItem.getQty());
                    ocSendOutImpItemQtyUpdateRequest.setQtyConsign(BigDecimal.ZERO);
                } else if (status == SgTransferBillStatusEnum.UN_AUDITED.getVal()) {

                    ocSendOutImpItemQtyUpdateRequest.setQtyOccu(scBTransferImpItem.getQty().negate());
                    ocSendOutImpItemQtyUpdateRequest.setQtyConsign(BigDecimal.ZERO);
                } else if (status >= SgTransferBillStatusEnum.AUDITED_ALL_OUT_NOT_IN.getVal()) {

                    ocSendOutImpItemQtyUpdateRequest.setQtyOccu(BigDecimal.ZERO);
                    ocSendOutImpItemQtyUpdateRequest.setQtyConsign(scBTransferImpItem.getQty());
                }
                ocItemsList.add(ocSendOutImpItemQtyUpdateRequest);

            }
        }

        return ocItemsList;
    }


    /**
     * -- 需求 取消调拨单审核
     * * * -- 功能 清空本单 条码明细表
     */
    public void clearTransferItemsAndTeus(ScBTransfer transfer) {

        scBTransferItemMapper.delete(new QueryWrapper<ScBTransferItem>().lambda()
                .eq(ScBTransferItem::getScBTransferId, transfer.getId()));
        scBTransferTeusItemMapper.delete(new QueryWrapper<ScBTransferTeusItem>().lambda()
                .eq(ScBTransferTeusItem::getScBTransferId, transfer.getId()));

    }


    /**
     * 封装新增逻辑发货单入参（箱功能）
     *
     * @param request  逻辑发货单对象
     * @param transfer 对象
     */
    public void createSgSendBox(SgSendBillSaveRequest request, ScBTransfer transfer,
                                List<ScBTransferImpItem> impItems, List<ScBTransferTeusItem> boxItems) {

        SgSendBillSaveRequest requestRedis = new SgSendBillSaveRequest();
        requestRedis.setSgSend(request.getSgSend());
        requestRedis.setItemList(request.getItemList());
        // 发货店仓信息
        Long origId = transfer.getCpCOrigId();
        String origECode = transfer.getCpCOrigEcode();
        String origEName = transfer.getCpCOrigEname();

        // 录入明细参数封装
        if (CollectionUtils.isNotEmpty(impItems)) {
            List<SgSendImpItemSaveRequest> impItemList = Lists.newArrayList();
            for (ScBTransferImpItem transferImpItem : impItems) {
                SgSendImpItemSaveRequest sendImpItemSaveRequest = new SgSendImpItemSaveRequest();
                BeanUtils.copyProperties(transferImpItem, sendImpItemSaveRequest);
                // 来源单据id(调拨单id)
                sendImpItemSaveRequest.setSourceBillItemId(transferImpItem.getId());
                // 店仓信息
                sendImpItemSaveRequest.setCpCStoreId(origId);
                sendImpItemSaveRequest.setCpCStoreEcode(origECode);
                sendImpItemSaveRequest.setCpCStoreEname(origEName);
                // 占单
                sendImpItemSaveRequest.setQtyPreout(transferImpItem.getQty());
                impItemList.add(sendImpItemSaveRequest);
            }
            requestRedis.setImpItemList(impItemList);
        }

        // 箱内明细参数封装
        if (CollectionUtils.isNotEmpty(boxItems)) {
            List<SgSendTeusItemSaveRequest> boxItemList = Lists.newArrayList();
            for (ScBTransferTeusItem boxItem : boxItems) {
                SgSendTeusItemSaveRequest sendBoxItemSaveRequest = new SgSendTeusItemSaveRequest();
                BeanUtils.copyProperties(boxItem, sendBoxItemSaveRequest);
//                // 来源单据id(调拨单id)
//                sendBoxItemSaveRequest.setSourceBillItemId(boxItem.getId());
                // 店仓信息
                sendBoxItemSaveRequest.setCpCStoreId(origId);
                sendBoxItemSaveRequest.setCpCStoreEcode(origECode);
                sendBoxItemSaveRequest.setCpCStoreEname(origEName);
                boxItemList.add(sendBoxItemSaveRequest);
            }
            requestRedis.setTeusItemList(boxItemList);
        }

        // 如果数据(主表 + 箱内明细 + 录入明细)大于4兆，则存redis
        String billSize = requestRedis.toString();
        if (billSize.getBytes().length > OcBasicConstants.REDIS_MEMORY_SIZE) {

            // redisKey："中心名:当前表名:下游表名:单据编号:动作"
            String redisKey = SgConstants.CENTER_NAME_SG + ":" + OcBasicConstants.REDIS_TRANSFER + ":"
                    + OcBasicConstants.REDIS_SEND + ":" + transfer.getBillNo() + ":" + Constants.ACTION_SUBMIT;

            CusRedisTemplate<String, String> redisTemplate = RedisOpsUtil.getStrRedisTemplate();
            redisTemplate.opsForValue().set(redisKey, JSON.toJSONString(requestRedis), 7, TimeUnit.DAYS);
            request.setRedisKey(redisKey);
            request.setItemList(null);
        } else {
            request.setImpItemList(requestRedis.getImpItemList());
            request.setTeusItemList(requestRedis.getTeusItemList());
        }
    }

    /**
     * 封装新增逻辑收货单入参（箱功能）
     *
     * @param request  增逻辑收货单对象
     * @param transfer 主表数据
     */
    public void createSgReceiveBox(SgReceiveBillSaveRequest request, ScBTransfer transfer,
                                   List<ScBTransferImpItem> impItems, List<ScBTransferTeusItem> boxItems,
                                   HashMap<String, ScBTransferImpItem> impMap) {

        SgReceiveBillSaveRequest requestRedis = new SgReceiveBillSaveRequest();
        requestRedis.setSgReceive(request.getSgReceive());
        requestRedis.setItemList(request.getItemList());
        // 发货店仓信息
        Long storeId = transfer.getCpCDestId();
        String storeECode = transfer.getCpCDestEcode();
        String storeEName = transfer.getCpCDestEname();

        // 录入明细参数封装
        if (CollectionUtils.isNotEmpty(impItems)) {
            List<SgReceiveImpItemSaveRequest> impItemList = Lists.newArrayList();
            for (ScBTransferImpItem impItem : impItems) {
                SgReceiveImpItemSaveRequest receiveImpItemSaveRequest = new SgReceiveImpItemSaveRequest();
                BeanUtils.copyProperties(impItem, receiveImpItemSaveRequest);

                // 来源单据明细id
                if (impMap != null) {
                    // 销售退货出库服务调用逻辑收时，收集录入明细信息（impMap）
                    String impKey = impItem.getPsCTeusEcode() + "-" + impItem.getPsCSkuEcode();
                    if (!impMap.containsKey(impKey)) {
                        AssertUtils.logAndThrow("调拨单录入明细不存在商品[" + impKey + "]");
                    }
                    receiveImpItemSaveRequest.setSourceBillItemId(impMap.get(impKey).getId());
                } else {
                    // 审核时impMap为空，取录入明细的明细id
                    receiveImpItemSaveRequest.setSourceBillItemId(impItem.getId());
                }

                // 店仓信息
                receiveImpItemSaveRequest.setCpCStoreId(storeId);
                receiveImpItemSaveRequest.setCpCStoreEcode(storeECode);
                receiveImpItemSaveRequest.setCpCStoreEname(storeEName);
                // 占单
                receiveImpItemSaveRequest.setQtyPrein(impItem.getQty());
                impItemList.add(receiveImpItemSaveRequest);
            }
            requestRedis.setImpItemList(impItemList);
        }

        // 箱内明细参数封装
        if (CollectionUtils.isNotEmpty(boxItems)) {
            List<SgReceiveTeusItemSaveRequest> boxItemList = Lists.newArrayList();
            for (ScBTransferTeusItem boxItem : boxItems) {
                SgReceiveTeusItemSaveRequest receiveBoxItemSaveRequest = new SgReceiveTeusItemSaveRequest();
                BeanUtils.copyProperties(boxItem, receiveBoxItemSaveRequest);
                // 店仓信息
                receiveBoxItemSaveRequest.setCpCStoreId(storeId);
                receiveBoxItemSaveRequest.setCpCStoreEcode(storeECode);
                receiveBoxItemSaveRequest.setCpCStoreEname(storeEName);
                boxItemList.add(receiveBoxItemSaveRequest);
            }
            requestRedis.setTeusItemList(boxItemList);
        }

        // 如果数据(主表 + 箱内明细 + 录入明细)大于4兆，则存redis
        String billSize = requestRedis.toString();
        if (billSize.getBytes().length > OcBasicConstants.REDIS_MEMORY_SIZE) {

            // redisKey："中心名:当前表名:下游表名:单据编号:动作"
            String redisKey = OcBasicConstants.CENTER_NAME_OC + ":" + OcBasicConstants.REDIS_TRANSFER + ":"
                    + OcBasicConstants.REDIS_RECEIVE + ":" + transfer.getBillNo() + ":" + Constants.ACTION_SUBMIT;

            CusRedisTemplate<String, String> redisTemplate = RedisOpsUtil.getStrRedisTemplate();
            redisTemplate.opsForValue().set(redisKey, JSON.toJSONString(requestRedis), 7, TimeUnit.DAYS);
            request.setRedisKey(redisKey);
            request.setItemList(null);
        } else {
            request.setImpItemList(requestRedis.getImpItemList());
            request.setTeusItemList(requestRedis.getTeusItemList());
        }
    }


    /**
     * 封装新增逻辑收货单【收货】入参（箱功能）
     *
     * @param request  增逻辑收货单对象
     * @param transfer 主表数据
     */
    public void createSgReceiveInBox(SgReceiveBillSubmitRequest request, ScBTransfer transfer,
                                     List<ScBTransferImpItem> impItems, List<ScBTransferTeusItem> boxItems) {

        SgReceiveBillSubmitRequest requestRedis = new SgReceiveBillSubmitRequest();
        requestRedis.setPhyInResultItems(request.getPhyInResultItems());
        // 录入明细参数封装
        if (CollectionUtils.isNotEmpty(impItems)) {
            List<SgBPhyInResultImpItem> impItemList = Lists.newArrayList();
            for (ScBTransferImpItem transferImpItem : impItems) {
                SgBPhyInResultImpItem inResultImpItem = new SgBPhyInResultImpItem();
                BeanUtils.copyProperties(transferImpItem, inResultImpItem);
                impItemList.add(inResultImpItem);
            }
            requestRedis.setPhyInResultImpItems(impItemList);
        }

        // 箱内明细参数封装
        if (CollectionUtils.isNotEmpty(boxItems)) {
            List<SgBPhyInResultTeusItem> boxItemList = Lists.newArrayList();
            for (ScBTransferTeusItem boxItem : boxItems) {
                SgBPhyInResultTeusItem inResultBoxItem = new SgBPhyInResultTeusItem();
                BeanUtils.copyProperties(boxItem, inResultBoxItem);
                boxItemList.add(inResultBoxItem);
            }
            requestRedis.setPhyInResultTeusItems(boxItemList);
        }

        // 如果数据(主表 + 箱内明细 + 录入明细)大于4兆，则存redis
        String billSize = requestRedis.toString();
        if (billSize.getBytes().length > OcBasicConstants.REDIS_MEMORY_SIZE) {

            // redisKey："中心名:当前表名:下游表名:单据编号:动作"
//            String redisKey = SgConstants.CENTER_NAME_SG + ":" + OcBasicConstants.REDIS_TRANSFER + ":"
//                    + OcBasicConstants.REDIS_RECEIVE + ":" + transfer.getBillNo() + ":" + Constants.ACTION_IN;
//
//            CusRedisTemplate<String, String> redisTemplate = RedisOpsUtil.getStrRedisTemplate();
//            redisTemplate.opsForValue().set(redisKey, JSON.toJSONString(requestRedis), 7, TimeUnit.DAYS);
//            request.setRedis(redisKey);

        } else {
            request.setPhyInResultImpItems(requestRedis.getPhyInResultImpItems());
            request.setPhyInResultTeusItems(requestRedis.getPhyInResultTeusItems());
        }
    }

    /**
     * 封装新增逻辑发货单【发货】入参（箱功能）
     *
     * @param request  增逻辑收货单对象
     * @param transfer 主表数据
     */
    public void createSgSendOutBox(SgSendBillSubmitRequest request, ScBTransfer transfer,
                                   List<ScBTransferImpItem> impItems, List<ScBTransferTeusItem> boxItems) {

        SgSendBillSubmitRequest requestRedis = new SgSendBillSubmitRequest();
        requestRedis.setPhyOutResultItems(request.getPhyOutResultItems());
        // 录入明细参数封装
        if (CollectionUtils.isNotEmpty(impItems)) {
            List<SgBPhyOutResultImpItem> impItemList = Lists.newArrayList();
            for (ScBTransferImpItem transferImpItem : impItems) {
                SgBPhyOutResultImpItem outResultImpItem = new SgBPhyOutResultImpItem();
                BeanUtils.copyProperties(transferImpItem, outResultImpItem);
                impItemList.add(outResultImpItem);
            }
            requestRedis.setPhyOutResultImpItems(impItemList);
        }

        // 箱内明细参数封装
        if (CollectionUtils.isNotEmpty(boxItems)) {
            List<SgBPhyOutResultTeusItem> boxItemList = Lists.newArrayList();
            for (ScBTransferTeusItem boxItem : boxItems) {
                SgBPhyOutResultTeusItem outResultBoxItem = new SgBPhyOutResultTeusItem();
                BeanUtils.copyProperties(boxItem, outResultBoxItem);
                boxItemList.add(outResultBoxItem);
            }
            requestRedis.setPhyOutResultTeusItems(boxItemList);
        }

        // 如果数据(主表 + 箱内明细 + 录入明细)大于4兆，则存redis
        String billSize = requestRedis.toString();
        if (billSize.getBytes().length > OcBasicConstants.REDIS_MEMORY_SIZE) {

            // redisKey："中心名:当前表名:下游表名:单据编号:动作"
//            String redisKey = SgConstants.CENTER_NAME_SG + ":" + OcBasicConstants.REDIS_TRANSFER + ":"
//                    + OcBasicConstants.REDIS_RECEIVE + ":" + transfer.getBillNo() + ":" + Constants.ACTION_IN;
//
//            CusRedisTemplate<String, String> redisTemplate = RedisOpsUtil.getStrRedisTemplate();
//            redisTemplate.opsForValue().set(redisKey, JSON.toJSONString(requestRedis), 7, TimeUnit.DAYS);
//            request.setRedis(redisKey);

        } else {
            request.setPhyOutResultImpItems(requestRedis.getPhyOutResultImpItems());
            request.setPhyOutResultTeusItems(requestRedis.getPhyOutResultTeusItems());
        }
    }

    /**
     * 封装新增出库通知单入参（箱功能）
     *
     * @param request  参数
     * @param transfer 调拨单
     */
    public void createOutNoticesBox(SgPhyOutNoticesBillSaveRequest request, ScBTransfer transfer,
                                    List<ScBTransferImpItem> impItems, List<ScBTransferTeusItem> boxItems) {

        SgPhyOutNoticesBillSaveRequest requestRedis = new SgPhyOutNoticesBillSaveRequest();
        requestRedis.setOutNoticesRequest(request.getOutNoticesRequest());

        // 录入明细参数封装
        if (CollectionUtils.isNotEmpty(impItems)) {
            List<SgPhyOutNoticesImpItemSaveRequest> impItemList = Lists.newArrayList();
            for (ScBTransferImpItem impItem : impItems) {
                SgPhyOutNoticesImpItemSaveRequest outNoticesImpItemSaveRequest = new SgPhyOutNoticesImpItemSaveRequest();
                BeanUtils.copyProperties(impItem, outNoticesImpItemSaveRequest);
                // 来源单据id(调拨单id)
                outNoticesImpItemSaveRequest.setSourceBillItemId(impItem.getId());
                impItemList.add(outNoticesImpItemSaveRequest);
            }
            requestRedis.setImpItemList(impItemList);
        }

        // 箱内明细参数封装
        if (!org.springframework.util.CollectionUtils.isEmpty(boxItems)) {
            List<SgPhyOutNoticesTeusItemSaveRequest> boxItemList = Lists.newArrayList();
            for (ScBTransferTeusItem boxItem : boxItems) {
                SgPhyOutNoticesTeusItemSaveRequest outNoticesBoxItemSaveRequest = new SgPhyOutNoticesTeusItemSaveRequest();
                BeanUtils.copyProperties(boxItem, outNoticesBoxItemSaveRequest);
                boxItemList.add(outNoticesBoxItemSaveRequest);
            }
            requestRedis.setTeusItemList(boxItemList);
        }

        // 如果数据(主表 + 箱内明细 + 录入明细)大于4兆，则存redis
        String billSize = requestRedis.toString();
        if (billSize.getBytes().length > OcBasicConstants.REDIS_MEMORY_SIZE) {

            // redisKey："中心名:当前表名:下游表名:单据编号:动作"
            String redisKey = SgConstants.CENTER_NAME_SG + ":" + OcBasicConstants.REDIS_TRANSFER + ":"
                    + OcBasicConstants.REDIS_PHY_OUT_NOTICES + ":" + transfer.getBillNo() + ":"
                    + Constants.ACTION_SUBMIT;

            CusRedisTemplate<String, String> redisTemplate = RedisOpsUtil.getStrRedisTemplate();
            redisTemplate.opsForValue().set(redisKey, JSON.toJSONString(requestRedis), 7, TimeUnit.DAYS);
            request.setRedisKey(redisKey);
        } else {
            request.setImpItemList(requestRedis.getImpItemList());
            request.setTeusItemList(requestRedis.getTeusItemList());
        }
    }


    /**
     * 封装新增并审核出库结果单入参（箱功能）
     *
     * @param request  结果单对象
     * @param transfer 调拨单对象
     */
    public void createAndAuditOutResultBox(SgPhyOutResultBillSaveRequest request, ScBTransfer transfer,
                                           List<ScBTransferImpItem> impItems, List<ScBTransferTeusItem> boxItems) {
        SgPhyOutResultBillSaveRequest requestRedis = new SgPhyOutResultBillSaveRequest();
        requestRedis.setOutResultRequest(request.getOutResultRequest());

        // 录入明细参数封装
        if (CollectionUtils.isNotEmpty(impItems)) {
            List<SgPhyOutResultImpItemSaveRequest> impItemList = Lists.newArrayList();
            for (ScBTransferImpItem saleImpItem : impItems) {
                SgPhyOutResultImpItemSaveRequest outResultImpItemSaveRequest = new SgPhyOutResultImpItemSaveRequest();
                BeanUtils.copyProperties(saleImpItem, outResultImpItemSaveRequest);
                outResultImpItemSaveRequest.setQtyOut(saleImpItem.getQty());
                // 来源单据id(销售单id)
                impItemList.add(outResultImpItemSaveRequest);
            }
            requestRedis.setImpItemList(impItemList);
        }

        // 箱内明细参数封装
        if (CollectionUtils.isNotEmpty(boxItems)) {
            List<SgPhyOutResultTeusItemSaveRequest> boxItemList = Lists.newArrayList();
            for (ScBTransferTeusItem boxItem : boxItems) {
                SgPhyOutResultTeusItemSaveRequest outNoticesBoxItemSaveRequest = new SgPhyOutResultTeusItemSaveRequest();
                BeanUtils.copyProperties(boxItem, outNoticesBoxItemSaveRequest);
                boxItemList.add(outNoticesBoxItemSaveRequest);
            }
            requestRedis.setTeusItemList(boxItemList);
        }

        // 如果数据(主表 + 箱内明细 + 录入明细)大于4兆，则存redis
        String billSize = requestRedis.toString();
        if (billSize.getBytes().length > OcBasicConstants.REDIS_MEMORY_SIZE) {

            // redisKey："中心名:当前表名:下游表名:单据编号:动作"
            String redisKey = SgConstants.CENTER_NAME_SG + ":" + OcBasicConstants.REDIS_TRANSFER + ":"
                    + OcBasicConstants.REDIS_PHY_OUT_RESULT + ":" + transfer.getBillNo() + ":"
                    + Constants.ACTION_SUBMIT;

            CusRedisTemplate<String, String> redisTemplate = RedisOpsUtil.getStrRedisTemplate();
            redisTemplate.opsForValue().set(redisKey, JSON.toJSONString(requestRedis), 7, TimeUnit.DAYS);
            request.setRedisKey(redisKey);
        } else {
            request.setImpItemList(requestRedis.getImpItemList());
            request.setTeusItemList(requestRedis.getTeusItemList());
        }
    }

    /**
     * 调拨出库服务，回写录入明细出库数量
     *
     * @param user            用户
     * @param objId           调拨单id
     * @param impItemRequests 录入明细
     */
    @Transactional(rollbackFor = Exception.class)
    public HashMap<String, ScBTransferImpItem> updateTransferImpForOut(User user, Long objId, List<SgTransferImpItemRequest> impItemRequests,
                                                                       String outOrIn) {

        HashMap<String, ScBTransferImpItem> impMap = null;
        if (!CollectionUtils.isEmpty(impItemRequests)) {
            impMap = new HashMap<>();
            if (log.isDebugEnabled()) {
                log.debug(this.getClass().getName() + ",updateTransferImpForOut param:" + impItemRequests.size());
            }
            ScBTransferImpItemMapper impItemMapper = ApplicationContextHandle.getBean(ScBTransferImpItemMapper.class);

            for (SgTransferImpItemRequest impItemRequest : impItemRequests) {

                if (log.isDebugEnabled()) {
                    log.debug(this.getClass().getName() + ",updateTransferImpForOut impItemRequest:"
                            + JSON.toJSONString(impItemRequest));
                }
                ScBTransferImpItem transferImpItem;
                Integer isBox = impItemRequest.getIsTeus();
                if (isBox == OcBasicConstants.IS_MATCH_SIZE_Y) {
                    transferImpItem = impItemMapper.selectOne(
                            new QueryWrapper<ScBTransferImpItem>().lambda()
                                    .eq(ScBTransferImpItem::getScBTransferId, objId)
                                    .eq(ScBTransferImpItem::getPsCTeusEcode, impItemRequest.getPsCTeusEcode()));
                    AssertUtils.notNull(transferImpItem, "录入明细为空！箱号：" + impItemRequest.getPsCTeusEcode());
                } else {
                    transferImpItem = impItemMapper.selectOne(
                            new QueryWrapper<ScBTransferImpItem>().lambda()
                                    .eq(ScBTransferImpItem::getScBTransferId, objId)
                                    .eq(ScBTransferImpItem::getPsCSkuEcode, impItemRequest.getPsCSkuEcode()));

                    // 2020-04-23存在串码入库的情况
//                    AssertUtils.notNull(transferImpItem, "录入明细为空！条码：" + impItemRequest.getPsCSkuEcode());
                }

                if (transferImpItem != null) {

                    // 收集录入明细，生成入库通知单时用
                    impMap.put(impItemRequest.getPsCTeusEcode() + "-" + impItemRequest.getPsCSkuEcode(), transferImpItem);

                    // 更新出/入库数量、操作人
                    ScBTransferImpItem updateImpItem = new ScBTransferImpItem();
                    updateImpItem.setId(transferImpItem.getId());

                    // 更新录入明细的出库数量
                    if (StringUtils.equals(outOrIn, ScTransferConstants.ACTION_OUT)) {
                        updateImpItem.setQtyOut(transferImpItem.getQtyOut() != null ? transferImpItem.getQtyOut()
                                .add(impItemRequest.getQtyOut()) : impItemRequest.getQtyOut());
                        // 差异数量(出库数量-入库数量)
                        updateImpItem.setQtyDiff(updateImpItem.getQtyOut().subtract(transferImpItem.getQtyIn()));

                    } else if (StringUtils.equals(outOrIn, ScTransferConstants.ACTION_IN)) {
                        // 更新录入明细的入库数量
                        updateImpItem.setQtyIn(transferImpItem.getQtyIn() != null ? transferImpItem.getQtyIn()
                                .add(impItemRequest.getQtyIn()) : impItemRequest.getQtyIn());
                        // 差异数量(出库数量-入库数量)
                        updateImpItem.setQtyDiff(transferImpItem.getQtyOut().subtract(updateImpItem.getQtyIn()));

                        // 水星特殊需求：入库时会修改价格
                        if (impItemRequest.getTransferPrice() != null) {
                            updateImpItem.setPriceOrder(impItemRequest.getTransferPrice());
                        }
                    }

                    OcCommonUtils.setBModelDefalutDataByUpdate(updateImpItem, user);
                    int count = impItemMapper.updateById(updateImpItem);
                    if (log.isDebugEnabled()) {
                        log.debug(this.getClass().getName() + ",updateTransferImpForOut update:" + count);
                    }
                }
            }
        }
        return impMap;
    }

    /**
     * 封装新增入库通知单入参（箱功能）
     *
     * @param request  参数
     * @param transfer 调拨单
     */
    public void createInNoticesBox(SgPhyInNoticesBillSaveRequest request, ScBTransfer transfer, boolean isAutoIn,
                                   List<SgTransferImpItemRequest> impItems, List<SgTransferBoxItemRequest> boxItems,
                                   HashMap<String, ScBTransferImpItem> impMap) {

        SgPhyInNoticesBillSaveRequest requestRedis = new SgPhyInNoticesBillSaveRequest();
        requestRedis.setNotices(request.getNotices());

        // 录入明细参数封装
        if (!CollectionUtils.isEmpty(impItems)) {
            List<SgPhyInNoticesImpItemSaveRequest> impItemList = Lists.newArrayList();
            for (SgTransferImpItemRequest impItem : impItems) {
                SgPhyInNoticesImpItemSaveRequest inNoticesImpItemSaveRequest = new SgPhyInNoticesImpItemSaveRequest();
                BeanUtils.copyProperties(impItem, inNoticesImpItemSaveRequest);
                inNoticesImpItemSaveRequest.setQty(impItem.getQtyOut());    // 本次出库数量=入库通知单通知数量

                // 来源单据明细id
                String impKey = impItem.getPsCTeusEcode() + "-" + impItem.getPsCSkuEcode();
                if (MapUtils.isEmpty(impMap) || !impMap.containsKey(impKey)) {
                    AssertUtils.logAndThrow("调拨单录入明细不存在商品[" + impKey + "]");
                }
                inNoticesImpItemSaveRequest.setSourceBillItemId(impMap.get(impKey).getId());
                if (isAutoIn) {
                    // 自动入库时，入库数量=通知数量（本次出库数量）
                    inNoticesImpItemSaveRequest.setQtyIn(impItem.getQtyOut());
                } else {
                    inNoticesImpItemSaveRequest.setQtyIn(BigDecimal.ZERO);
                }
                impItemList.add(inNoticesImpItemSaveRequest);
            }
            requestRedis.setImpItemList(impItemList);
        }

        // 箱内明细参数封装
        if (!CollectionUtils.isEmpty(boxItems)) {
            List<SgPhyInNoticesTeusItemSaveRequest> boxItemList = Lists.newArrayList();
            for (SgTransferBoxItemRequest boxItem : boxItems) {
                SgPhyInNoticesTeusItemSaveRequest boxItemSaveRequest = new SgPhyInNoticesTeusItemSaveRequest();
                BeanUtils.copyProperties(boxItem, boxItemSaveRequest);
                boxItemList.add(boxItemSaveRequest);
            }
            requestRedis.setTeusItemList(boxItemList);
        }

        // 如果数据(主表 + 箱内明细 + 录入明细)大于4兆，则存redis
        String billSize = requestRedis.toString();
        if (billSize.getBytes().length > OcBasicConstants.REDIS_MEMORY_SIZE) {

            // redisKey："中心名:当前表名:下游表名:单据编号:动作"
            String redisKey = SgConstants.CENTER_NAME_SG + ":" + OcBasicConstants.REDIS_TRANSFER + ":"
                    + OcBasicConstants.REDIS_PHY_IN_NOTICES + ":" + transfer.getBillNo() + ":"
                    + Constants.ACTION_OUT;

            CusRedisTemplate<String, String> redisTemplate = RedisOpsUtil.getStrRedisTemplate();
            redisTemplate.opsForValue().set(redisKey, JSON.toJSONString(requestRedis), 7, TimeUnit.DAYS);
            request.setRedisKey(redisKey);
        } else {
            request.setImpItemList(requestRedis.getImpItemList());
            request.setTeusItemList(requestRedis.getTeusItemList());
        }
    }

    /**
     * 出库回传转录入明细
     *
     * @param requests 入参
     * @return 出参
     */
    public List<ScBTransferImpItem> requestToImpItem(List<SgTransferImpItemRequest> requests) {
        List<ScBTransferImpItem> list = null;
        if (CollectionUtils.isNotEmpty(requests)) {
            list = Lists.newArrayList();
            for (SgTransferImpItemRequest request : requests) {
                ScBTransferImpItem impItem = new ScBTransferImpItem();
                BeanUtils.copyProperties(request, impItem);
                // 入库数量取本次出库结果单回写的出库数量
                impItem.setQty(request.getQtyOut());
                list.add(impItem);
            }
        }
        return list;
    }

    /**
     * 出库回传转箱内明细
     *
     * @param requests 入参
     * @return 出参
     */
    public List<ScBTransferTeusItem> requestToBoxItem(List<SgTransferBoxItemRequest> requests) {

        List<ScBTransferTeusItem> list = null;
        if (CollectionUtils.isNotEmpty(requests)) {
            list = Lists.newArrayList();
            for (SgTransferBoxItemRequest request : requests) {
                ScBTransferTeusItem boxItem = new ScBTransferTeusItem();
                BeanUtils.copyProperties(request, boxItem);
                list.add(boxItem);
            }
        }
        return list;
    }


    /**
     * 订单类型的调拨单，调拨出库服务，更新发货订单已配已发量
     *
     * @param impItemList 出库结果单录入明细
     * @return 组装后的发货订单录入明细
     */
    public List<OcSendOutImpItemQtyUpdateRequest> createSendOutImp(List<SgTransferImpItemRequest> impItemList) {

        List<OcSendOutImpItemQtyUpdateRequest> sendOutImp = null;
        if (CollectionUtils.isNotEmpty(impItemList)) {
            sendOutImp = Lists.newArrayList();

            // 转换成配码维度，用于更新已配已发量
            HashMap<Long, SgTransferImpItemRequest> matchSizeMap = new HashMap<>();
            for (SgTransferImpItemRequest impItem : impItemList) {
                Integer isBox = impItem.getIsTeus();

                // 是否箱=是时，把箱转成配码维度
                if (isBox == OcBasicConstants.IS_MATCH_SIZE_Y) {

                    Long matchSizeId = impItem.getPsCMatchsizeId();
                    SgTransferImpItemRequest itemSaveRequest = matchSizeMap.get(matchSizeId);
                    if (itemSaveRequest != null) {
                        itemSaveRequest.setQtyIn(itemSaveRequest.getQtyIn().add(impItem.getQtyIn()));
                    } else {
                        matchSizeMap.put(matchSizeId, impItem);
                    }

                } else {

                    // 散码直接收集
                    OcSendOutImpItemQtyUpdateRequest request = new OcSendOutImpItemQtyUpdateRequest();
                    request.setPsCProId(impItem.getPsCProId());
                    request.setPsCSkuEcode(impItem.getPsCSkuEcode());
                    request.setIsMatchsize(OcBasicConstants.IS_MATCH_SIZE_N);
                    // 已配量、已发量
                    request.setQtyOccu(impItem.getQtyOut().negate());
                    request.setQtyConsign(impItem.getQtyOut());
                    sendOutImp.add(request);
                }
            }

            // 配码维度
            if (MapUtils.isNotEmpty(matchSizeMap)) {
                for (SgTransferImpItemRequest impItem : matchSizeMap.values()) {
                    OcSendOutImpItemQtyUpdateRequest request = new OcSendOutImpItemQtyUpdateRequest();
                    // 商品、配码、颜色
                    request.setPsCProId(impItem.getPsCProId());
                    request.setPsCMatchsizeId(impItem.getPsCMatchsizeId());
                    request.setPsCSpec1Id(impItem.getPsCSpec1Id());
                    request.setIsMatchsize(OcBasicConstants.IS_MATCH_SIZE_Y);
                    // 已配量、已发量
                    request.setQtyOccu(impItem.getQtyOut().negate());
                    request.setQtyConsign(impItem.getQtyOut());
                    sendOutImp.add(request);
                }
            }
        }
        return sendOutImp;
    }
}
