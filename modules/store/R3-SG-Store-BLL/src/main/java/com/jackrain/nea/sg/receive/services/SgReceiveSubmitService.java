package com.jackrain.nea.sg.receive.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.redis.config.CusRedisTemplate;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.config.SgStorageBoxConfig;
import com.jackrain.nea.sg.basic.model.result.SgStorageUpdateResult;
import com.jackrain.nea.sg.basic.model.table.CpCStore;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.in.common.SgInNoticeConstants;
import com.jackrain.nea.sg.in.model.result.SgBPhyInNoticesResult;
import com.jackrain.nea.sg.in.model.table.SgBPhyInResult;
import com.jackrain.nea.sg.in.model.table.SgBPhyInResultImpItem;
import com.jackrain.nea.sg.in.model.table.SgBPhyInResultItem;
import com.jackrain.nea.sg.in.model.table.SgBPhyInResultTeusItem;
import com.jackrain.nea.sg.out.common.SgOutConstantsIF;
import com.jackrain.nea.sg.out.model.result.SgOutQueryResult;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutNotices;
import com.jackrain.nea.sg.receive.common.SgReceiveConstants;
import com.jackrain.nea.sg.receive.common.SgReceiveConstantsIF;
import com.jackrain.nea.sg.receive.mapper.SgBReceiveImpItemMapper;
import com.jackrain.nea.sg.receive.mapper.SgBReceiveItemMapper;
import com.jackrain.nea.sg.receive.mapper.SgBReceiveMapper;
import com.jackrain.nea.sg.receive.model.request.SgReceiveBillSubmitRequest;
import com.jackrain.nea.sg.receive.model.result.SgReceiveBillSubmitResult;
import com.jackrain.nea.sg.receive.model.table.SgBReceive;
import com.jackrain.nea.sg.receive.model.table.SgBReceiveImpItem;
import com.jackrain.nea.sg.receive.model.table.SgBReceiveItem;
import com.jackrain.nea.sg.send.common.SgSendConstantsIF;
import com.jackrain.nea.sg.send.utils.SgStoreESUtils;
import com.jackrain.nea.sg.send.utils.SgStoreUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;


/**
 * @author 舒威
 * @since 2019/4/24
 * create at : 2019/4/29 16:36
 */
@Slf4j
@Component
public class SgReceiveSubmitService {

    @Autowired
    private SgBReceiveMapper receiveMapper;

    @Autowired
    private SgBReceiveItemMapper receiveItemMapper;

    @Autowired
    private SgReceiveStorageService storageService;

    @Autowired
    private SgReceiveRPCService rpcService;

    @Autowired
    private SgStorageBoxConfig storageBoxConfig;

    @Autowired
    private SgStoreESUtils storeESUtils;

    @Transactional(rollbackFor = Exception.class)
    public ValueHolderV14<SgReceiveBillSubmitResult> submitSgBReceive(SgReceiveBillSubmitRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("来源单据id[" + request.getSourceBillId()
                    + "],逻辑收货单入库服务入参:" + JSONObject.toJSONString(request) + ";");
        }

        ValueHolderV14<SgReceiveBillSubmitResult> v14 = new ValueHolderV14<>();
        SgStoreUtils.checkR3BModelDefalut(request);
        SgStoreUtils.checkBModelDefalut(request, false);
        AssertUtils.notNull(request.getPhyInResult(), "入库结果单不能为空！");
        AssertUtils.notEmpty(request.getPhyInResultItems(), "入库结果单明细不能为空！");

        Long sourceBillId = request.getSourceBillId();
        Integer billType = request.getSourceBillType();
        SgBPhyInResult phyInResult = request.getPhyInResult();
        List<SgBPhyInResultItem> orgInresultItems = request.getPhyInResultItems();
        List<SgBPhyInResultItem> phyInResultItems = Lists.newArrayList();
        for (SgBPhyInResultItem orgInresultItem : orgInresultItems) {
            SgBPhyInResultItem inResultItem = new SgBPhyInResultItem();
            BeanUtils.copyProperties(orgInresultItem, inResultItem);
            phyInResultItems.add(inResultItem);
        }

        Long noticesId = phyInResult.getSgBPhyInNoticesId();
        String noticesBillno = phyInResult.getSgBPhyInNoticesBillno();

        User user = request.getLoginUser();
        SgReceiveBillSubmitResult data = new SgReceiveBillSubmitResult();

        CusRedisTemplate<String, String> redisTemplate = SgStoreUtils.initRedisTemplate();
        String lockKsy = SgConstants.SG_B_RECEIVE + ":" + sourceBillId + ":" + billType;

        if (redisTemplate.opsForValue().get(lockKsy) == null) {
            try {
                redisTemplate.opsForValue().set(lockKsy, "OK", 30, TimeUnit.MINUTES);
                SgBReceive sgBReceive = receiveMapper.selectOne(new QueryWrapper<SgBReceive>().lambda()
                        .eq(SgBReceive::getSourceBillId, sourceBillId)
                        .eq(SgBReceive::getSourceBillType, billType)
                        .eq(SgBReceive::getIsactive, SgConstants.IS_ACTIVE_Y));
                AssertUtils.notNull(sgBReceive, "该记录已不存在！", user.getLocale());

                BigDecimal totQtyIn = Optional.ofNullable(phyInResult.getTotQtyIn()).orElse(BigDecimal.ZERO); //总入库数
                BigDecimal orgTotQtyReceive = Optional.ofNullable(sgBReceive.getTotQtyReceive()).orElse(BigDecimal.ZERO);
                sgBReceive.setTotQtyReceive(totQtyIn.add(orgTotQtyReceive)); //已收货总数

                Integer billStatus = getReceiveStatus(sourceBillId, billType, user);
                sgBReceive.setBillStatus(billStatus);
                //当结果单传参为最后一次入库 并且 收货单单据状态为收货完成 时 才为最后一次入库
                Boolean isLast = billStatus.equals(SgReceiveConstantsIF.BILL_RECEIVE_STATUS_ALL_RECEIVE);

                List<SgBReceiveItem> releaseStorages = Lists.newArrayList(); //记录库存释放明细
                List<SgBReceiveItem> lastReleaseStorages = Lists.newArrayList();

                List<SgBReceiveItem> sgBReceiveItems = receiveItemMapper.selectList(new QueryWrapper<SgBReceiveItem>()
                        .eq(SgReceiveConstants.BILL_RECEIVE_ID, sgBReceive.getId()));

                if (CollectionUtils.isNotEmpty(sgBReceiveItems)) {
                    updateReceiveAndGetReleaseStorages(request, sgBReceiveItems, releaseStorages, lastReleaseStorages, isLast, sgBReceive.getId(), user);
                } else {
                    AssertUtils.logAndThrow("逻辑收货单明细为空,不允许入库!");
                }

                Map<Long, BigDecimal> skuTeusCounts = Maps.newHashMap();
                List<SgBReceiveImpItem> impReleaseStorages = Lists.newArrayList(); //记录库存释放明细
                List<SgBReceiveImpItem> impLastReleaseStorages = Lists.newArrayList();

                if (storageBoxConfig.getBoxEnable()) {
                    SgBReceiveImpItemMapper impItemMapper = ApplicationContextHandle.getBean(SgBReceiveImpItemMapper.class);
                    List<SgBReceiveImpItem> impItems = impItemMapper.selectList(new QueryWrapper<SgBReceiveImpItem>().lambda().eq(SgBReceiveImpItem::getSgBReceiveId, sgBReceive.getId()));
                    if (CollectionUtils.isNotEmpty(impItems)) {
                        updateReceiveAndGetImpReleaseStorages(request, impItems, impReleaseStorages, impLastReleaseStorages, isLast, sgBReceive.getId(), user, impItemMapper);
                    }
                    skuTeusCounts = request.getPhyInResultTeusItems().stream().collect(Collectors.groupingBy(SgBPhyInResultTeusItem::getPsCSkuId,
                            Collectors.reducing(BigDecimal.ZERO, SgBPhyInResultTeusItem::getQty, (a, b) -> a.add(b))));
                }


                JSONObject object = receiveItemMapper.selectTotalPreinByReceiveId(sgBReceive.getId());
                sgBReceive.setTotQtyPrein(object.getBigDecimal("qty_prein"));//更新主表总在途数量
                sgBReceive.setTotQtyReceive(object.getBigDecimal("qty_receive"));
                sgBReceive.setReceiveTime(new Date());
                StorageESUtils.setBModelDefalutDataByUpdate(sgBReceive, user);
                sgBReceive.setModifierename(user.getEname());
                receiveMapper.updateById(sgBReceive);

                Map<Long, CpCStore> negativeStock = SgStoreUtils.getNegativeStock(null, null, sgBReceiveItems, null);
                //更新业务节点
                sgBReceive.setServiceNode(updateServiceNode(billType));
                ValueHolderV14<SgStorageUpdateResult> callAPIResult =
                        storageService.updateReceiveStoragePrein(sgBReceive, isLast ? lastReleaseStorages : releaseStorages,
                                null, null, "submit", request.getLoginUser(), isLast, negativeStock, request.getIsNegativePrein(),
                                isLast ? impLastReleaseStorages : impReleaseStorages, skuTeusCounts, null, noticesId, noticesBillno);
                if (callAPIResult.isOK()) {
                    //check同步库存在途结果
                    SgStoreUtils.isSuccess("逻辑收货单收货失败!", callAPIResult);
                    v14.setCode(ResultCode.SUCCESS);
                    v14.setMessage("逻辑收货单收货成功");
                    v14.setData(data);
                } else {
                    AssertUtils.logAndThrow("逻辑收货单收货失败!" + callAPIResult.getMessage());
                }

                //推送ES
                storeESUtils.pushESByReceive(sgBReceive.getId(), true, false, null, receiveMapper, receiveItemMapper);

            } catch (Exception e) {
                e.printStackTrace();
                AssertUtils.logAndThrowExtra(e, user.getLocale());
            } finally {
                try {
                    redisTemplate.delete(lockKsy);
                } catch (Exception e) {
                    e.printStackTrace();
                    log.error(this.getClass().getName() + lockKsy + "释放锁失败，请联系管理员！");
                    AssertUtils.logAndThrow("释放锁失败，请联系管理员！");
                }
            }
        } else {
            v14.setCode(ResultCode.FAIL);
            v14.setMessage("逻辑收货单收货中，请稍后重试！");
        }
        if (log.isDebugEnabled()) {
            log.debug("来源单据id[" + request.getSourceBillId()
                    + "],逻辑收货单入库服务出参:" + v14.toJSONObject().toJSONString() + ";");
        }
        return v14;
    }

    private void updateReceiveAndGetImpReleaseStorages(SgReceiveBillSubmitRequest request, List<SgBReceiveImpItem> sgBReceiveImpItems, List<SgBReceiveImpItem> impReleaseStorages,
                                                       List<SgBReceiveImpItem> impLastReleaseStorages, Boolean isLast, Long receiveId, User user, SgBReceiveImpItemMapper impItemMapper) {
        //原录入明细-散码 key:skuid
        Map<Long, List<SgBReceiveImpItem>> scatterMap = sgBReceiveImpItems.stream()
                .filter(o -> o.getIsTeus().equals(SgSendConstantsIF.IS_TEUS_N)).collect(Collectors.groupingBy(SgBReceiveImpItem::getPsCSkuId));
        //原录入明细-箱 key: 箱号id  整箱不会拆分
        Map<Long, SgBReceiveImpItem> boxMap = sgBReceiveImpItems.stream()
                .filter(o -> o.getIsTeus().equals(SgSendConstantsIF.IS_TEUS_Y)).collect(Collectors.toMap(SgBReceiveImpItem::getPsCTeusId, o -> o));
        //入库录入明细-散码 key:skuid
        Map<Long, SgBPhyInResultImpItem> inScatterMap = request.getPhyInResultImpItems().stream()
                .filter(o -> o.getIsTeus().equals(SgSendConstantsIF.IS_TEUS_N)).collect(Collectors.toMap(SgBPhyInResultImpItem::getPsCSkuId, o -> o));
        //入库录入明细-箱 key:箱号   整箱不会拆分
        Map<Long, SgBPhyInResultImpItem> inBoxMap = request.getPhyInResultImpItems().stream()
                .filter(o -> o.getIsTeus().equals(SgSendConstantsIF.IS_TEUS_Y)).collect(Collectors.toMap(SgBPhyInResultImpItem::getPsCTeusId, o -> o));

        //散码
        if (MapUtils.isNotEmpty(scatterMap) && MapUtils.isNotEmpty(inScatterMap)) {
            for (Map.Entry<Long, List<SgBReceiveImpItem>> receiveEntry : scatterMap.entrySet()) {
                Long sku = receiveEntry.getKey();
                List<SgBReceiveImpItem> impItems = receiveEntry.getValue();
                //过滤出录入明细中 逻辑仓优先级不为空的数据
                List<SgBReceiveImpItem> rankNotNulls = impItems.stream().filter(s -> s.getRank() != null).collect(Collectors.toList());

                //当sku分行 并且 逻辑仓优先级不为空的情况下  按照逻辑仓优先级排序 并释放在途
                //记录当前sku分行数
                int size = impItems.size();
                if (size > 1 && size == rankNotNulls.size()) {
                    impItems = impItems.stream().sorted((p1, p2) -> p2.getRank() - p1.getRank()).collect(Collectors.toList());
                }

                SgBPhyInResultImpItem inImpItem = inScatterMap.get(sku);
                if (inImpItem != null) {
                    BigDecimal qty = inImpItem.getQtyIn(); //入库数量
                    for (SgBReceiveImpItem impItem : impItems) {
                        //impItem 用作库存更新对象，update 逻辑收货单明细更新对象
                        SgBReceiveImpItem update = new SgBReceiveImpItem();
                        BeanUtils.copyProperties(impItem, update);

                        BigDecimal qtyPrein = impItem.getQtyPrein();
                        BigDecimal orgQtyReceive = impItem.getQtyReceive();
                        BigDecimal subtract = qty.abs().subtract(qtyPrein.abs());
                        if (subtract.compareTo(BigDecimal.ZERO) > 0) {
                            //入库数量>当前收货单明细在途数量，当前收货单明细 在途全部释放, 继续遍历
                            //库存 -> 待收货数量
                            BigDecimal qtySend = size > 1 ? qtyPrein : qty;
                            impItem.setQtyReceive(qtySend);
                            //库存 -> 待释放在途量
                            impItem.setQtyPrein(qtyPrein);
                            impReleaseStorages.add(impItem);

                            if (log.isDebugEnabled()) {
                                log.debug("来源单据id[" + request.getSourceBillId() + "],当前行记录入库数>在途数,实际入库数量:", qtySend);
                            }

                            // 逻辑收货单 -> 累加收货数量
                            update.setQtyReceive(orgQtyReceive.add(qtySend));
                            //逻辑收货单 -> 释放在途
                            update.setQtyPrein(BigDecimal.ZERO);
                            StorageESUtils.setBModelDefalutDataByUpdate(update, user);
                            update.setModifierename(user.getEname());
                            impItemMapper.updateById(update);
                            qty = subtract;
                            //参与完计算后 分行数--
                            size--;
                        } else if (subtract.compareTo(BigDecimal.ZERO) < 0) {
                            //入库数量<当前收货单明细在途数量，当前收货单明细 在途部分释放, 终止遍历
                            //库存 -> 待收货数量
                            impItem.setQtyReceive(qty);
                            // 原在途10件,入库5件,最后一次入库,释放10件，明细更新:在途数量=0,已收货数量=5
                            //库存 -> 待释放在途量
                            impItem.setQtyPrein(isLast ? qtyPrein : qty);
                            impReleaseStorages.add(impItem);

                            //逻辑收货单 -> 累加收货数量
                            update.setQtyReceive(orgQtyReceive.add(qty));
                            //逻辑收货单 -> 释放在途
                            update.setQtyPrein(isLast ? BigDecimal.ZERO : subtract.negate());
                            StorageESUtils.setBModelDefalutDataByUpdate(update, user);
                            update.setModifierename(user.getEname());
                            impItemMapper.updateById(update);
                            break;
                        } else if (0 == subtract.compareTo(BigDecimal.ZERO)) {
                            //入库数量=当前收货单明细在途数量，当前收货单明细 在途全部释放, 终止遍历
                            //库存 -> 待收货数量
                            impItem.setQtyReceive(qty);
                            //库存 -> 待释放在途量
                            impItem.setQtyPrein(qtyPrein);
                            impReleaseStorages.add(impItem);

                            //逻辑收货单 -> 累加收货数量
                            update.setQtyReceive(orgQtyReceive.add(qty));
                            //逻辑收货单 -> 释放在途
                            update.setQtyPrein(BigDecimal.ZERO);
                            StorageESUtils.setBModelDefalutDataByUpdate(update, user);
                            update.setModifierename(user.getEname());
                            impItemMapper.updateById(update);
                            break;
                        }
                    }
                }else {
                    impItems.forEach(o -> o.setQtyReceive(BigDecimal.ZERO));
                }
                if (isLast) {
                    impLastReleaseStorages.addAll(impItems);
                }
            }

        }


        //箱
        if (MapUtils.isNotEmpty(boxMap) && MapUtils.isNotEmpty(inBoxMap)) {
            for (Map.Entry<Long, SgBReceiveImpItem> receiveEntry : boxMap.entrySet()) {
                Long teusId = receiveEntry.getKey();
                SgBReceiveImpItem impItem = receiveEntry.getValue();
                SgBPhyInResultImpItem inImpItem = inBoxMap.get(teusId);
                if (inImpItem != null) {
                    BigDecimal qty = inImpItem.getQtyIn(); //入库数量
                    //impItem 用作库存更新对象，update 逻辑收货单明细更新对象
                    SgBReceiveImpItem update = new SgBReceiveImpItem();
                    BeanUtils.copyProperties(impItem, update);

                    BigDecimal qtyPrein = impItem.getQtyPrein();
                    BigDecimal orgQtyReceive = impItem.getQtyReceive();
                    BigDecimal subtract = qty.abs().subtract(qtyPrein.abs());
                    if (subtract.compareTo(BigDecimal.ZERO) > 0) {
                        //入库箱数>当前收货单录入明细在途箱数，当前收货单录入明细 在途箱数全部释放 入库数量=在途箱数
                        //库存 -> 待收货数量
                        impItem.setQtyReceive(qtyPrein);
                        //库存 -> 待释放在途量
                        impItem.setQtyPrein(qtyPrein);
                        impReleaseStorages.add(impItem);

                        if (log.isDebugEnabled()) {
                            log.debug("来源单据id[" + request.getSourceBillId() + "],当前行记录入库数>在途数,实际入库数量:", qtyPrein);
                        }

                        // 逻辑收货单 -> 累加收货数量
                        update.setQtyReceive(orgQtyReceive.add(qtyPrein));
                        //逻辑收货单 -> 释放在途
                        update.setQtyPrein(BigDecimal.ZERO);
                        StorageESUtils.setBModelDefalutDataByUpdate(update, user);
                        update.setModifierename(user.getEname());
                        impItemMapper.updateById(update);
                    } else if (subtract.compareTo(BigDecimal.ZERO) < 0) {
                        //入库箱数<当前收货单录入明细在途箱数，当前收货单录入明细 在途部分释放(释放箱数=入库箱数)
                        //库存 -> 待收货数量
                        impItem.setQtyReceive(qty);
                        // 原在途10件,入库5件,最后一次入库,释放10件，明细更新:在途数量=0,已收货数量=5
                        //库存 -> 待释放在途量
                        impItem.setQtyPrein(isLast ? qtyPrein : qty);
                        impReleaseStorages.add(impItem);

                        //逻辑收货单 -> 累加收货数量
                        update.setQtyReceive(orgQtyReceive.add(qty));
                        //逻辑收货单 -> 释放在途
                        update.setQtyPrein(isLast ? BigDecimal.ZERO : subtract.negate());
                        StorageESUtils.setBModelDefalutDataByUpdate(update, user);
                        update.setModifierename(user.getEname());
                        impItemMapper.updateById(update);
                    } else if (0 == subtract.compareTo(BigDecimal.ZERO)) {
                        //入库数量=当前收货单明细在途数量，当前收货单明细 在途全部释放, 终止遍历
                        //库存 -> 待收货数量
                        impItem.setQtyReceive(qty);
                        //库存 -> 待释放在途量
                        impItem.setQtyPrein(qtyPrein);
                        impReleaseStorages.add(impItem);

                        //逻辑收货单 -> 累加收货数量
                        update.setQtyReceive(orgQtyReceive.add(qty));
                        //逻辑收货单 -> 释放在途
                        update.setQtyPrein(BigDecimal.ZERO);
                        StorageESUtils.setBModelDefalutDataByUpdate(update, user);
                        update.setModifierename(user.getEname());
                        impItemMapper.updateById(update);
                    }
                }
                if (isLast) {
                    impLastReleaseStorages.add(impItem);
                }
            }

        }

        //最后一次入库，将逻辑收货单明细中 所有在途数量非0的记录更新为0
        if (isLast) {
            SgBReceiveImpItem update = new SgBReceiveImpItem();
            update.setQtyPrein(BigDecimal.ZERO);
            StorageESUtils.setBModelDefalutDataByUpdate(update, user);
            int count = impItemMapper.update(update, new UpdateWrapper<SgBReceiveImpItem>().lambda().eq(SgBReceiveImpItem::getSgBReceiveId, receiveId));
            if (count != sgBReceiveImpItems.size()) {
                AssertUtils.logAndThrow("最后一次入库时,逻辑发收单明细更新失败！");
            }
        }
    }

    private void updateReceiveAndGetReleaseStorages(SgReceiveBillSubmitRequest request, List<SgBReceiveItem> sgBReceiveItems, List<SgBReceiveItem> releaseStorages, List<SgBReceiveItem> lastReleaseStorages, Boolean isLast, Long receiveId, User user) {
        Map<Long, List<SgBReceiveItem>> map = sgBReceiveItems.stream().collect(Collectors.groupingBy(SgBReceiveItem::getPsCSkuId));

        Map<Long, SgBPhyInResultItem> requestMap = Maps.newHashMap();
        //如果入库结果明细有拆分  合并入库结果明细
        for (SgBPhyInResultItem phyInResultItem : request.getPhyInResultItems()) {
            Long psCSkuId = phyInResultItem.getPsCSkuId();
            if (requestMap.containsKey(psCSkuId)) {
                SgBPhyInResultItem orgResultItem = requestMap.get(psCSkuId);
                BigDecimal orgResultItemQty = Optional.ofNullable(orgResultItem.getQtyIn()).orElse(BigDecimal.ZERO);
                BigDecimal qty = Optional.ofNullable(phyInResultItem.getQtyIn()).orElse(BigDecimal.ZERO);
                orgResultItem.setQtyIn(qty.add(orgResultItemQty));
            } else {
                requestMap.put(psCSkuId, phyInResultItem);
            }
        }

        if (MapUtils.isNotEmpty(map)) {
            for (Map.Entry<Long, List<SgBReceiveItem>> receiveEntry : map.entrySet()) {
                Long sku = receiveEntry.getKey();
                List<SgBReceiveItem> receiveItems = receiveEntry.getValue();
                //过滤出明细中 逻辑仓优先级不为空的数据
                List<SgBReceiveItem> rankNotNulls = receiveItems.stream().filter(s -> s.getRank() != null).collect(Collectors.toList());

                //当sku分行 并且 逻辑仓优先级不为空的情况下  按照逻辑仓优先级排序 并释放在途
                //记录当前sku分行数
                int size = receiveItems.size();
                if (size > 1 && size == rankNotNulls.size()) {
                    receiveItems = receiveItems.stream().
                            sorted((p1, p2) -> p2.getRank() - p1.getRank()).collect(Collectors.toList());
                }
                SgBPhyInResultItem phyInResultItem = requestMap.get(sku);
                if (phyInResultItem != null) {
                    BigDecimal qty = phyInResultItem.getQtyIn(); //入库数量
                    for (SgBReceiveItem receiveItem : receiveItems) {
                        //receiveItem 用作库存更新对象，updateReceiveItem为 逻辑收货单明细更新对象
                        SgBReceiveItem updateReceiveItem = new SgBReceiveItem();
                        BeanUtils.copyProperties(receiveItem, updateReceiveItem);

                        BigDecimal qtyPrein = receiveItem.getQtyPrein();
                        BigDecimal orgQtyReceive = receiveItem.getQtyReceive();
                        BigDecimal subtract = qty.abs().subtract(qtyPrein.abs());
                        if (subtract.compareTo(BigDecimal.ZERO) > 0) {
                            //入库数量>当前收货单明细在途数量，当前收货单明细 在途全部释放, 继续遍历
                            //库存 -> 待入库数量
                            BigDecimal qtyReceive = size > 1 ? qtyPrein : qty;
                            receiveItem.setQtyReceive(qtyReceive);
                            //库存 -> 待释放在途量
                            receiveItem.setQtyPrein(qtyPrein);
                            releaseStorages.add(receiveItem);

                            if (log.isDebugEnabled()) {
                                log.debug("来源单据id[" + request.getSourceBillId()
                                        + "],当前行记录入库数>在途数,实际入库数量:", qtyReceive);
                            }

                            // 逻辑收货单 -> 累加入库数量
                            updateReceiveItem.setQtyReceive(orgQtyReceive.add(qtyReceive));
                            //逻辑收货单 -> 释放在途
                            updateReceiveItem.setQtyPrein(BigDecimal.ZERO);
                            StorageESUtils.setBModelDefalutDataByUpdate(updateReceiveItem, user);
                            updateReceiveItem.setModifierename(user.getEname());
                            receiveItemMapper.updateById(updateReceiveItem);
                            qty = subtract;
                            //参与完计算后 分行数--
                            size--;
                        } else if (subtract.compareTo(BigDecimal.ZERO) < 0) {
                            //入库数量<当前收货单明细在途数量，当前收货单明细 在途部分释放, 终止遍历
                            //库存 -> 待入库数量
                            receiveItem.setQtyReceive(qty);
                            // 原在途10件,入库5件,最后一次入库,释放10件，明细更新:在途数量=0,已入库数量=5
                            //库存 -> 待释放在途量
                            receiveItem.setQtyPrein(isLast ? qtyPrein : qty);
                            releaseStorages.add(receiveItem);

                            // 逻辑收货单 -> 累加入库数量
                            updateReceiveItem.setQtyReceive(orgQtyReceive.add(qty));
                            //逻辑收货单 -> 释放在途
                            updateReceiveItem.setQtyPrein(isLast ? BigDecimal.ZERO : subtract.negate());
                            StorageESUtils.setBModelDefalutDataByUpdate(updateReceiveItem, user);
                            updateReceiveItem.setModifierename(user.getEname());
                            receiveItemMapper.updateById(updateReceiveItem);
                            break;
                        } else if (0 == subtract.compareTo(BigDecimal.ZERO)) {
                            //入库数量=当前收货单明细在途数量，当前收货单明细 在途全部释放, 终止遍历
                            //库存 -> 待入库数量
                            receiveItem.setQtyReceive(qty);
                            //库存 -> 待释放在途量
                            receiveItem.setQtyPrein(qtyPrein);
                            releaseStorages.add(receiveItem);

                            // 逻辑收货单 -> 累加入库数量
                            updateReceiveItem.setQtyReceive(orgQtyReceive.add(qty));
                            //逻辑收货单 -> 释放在途
                            updateReceiveItem.setQtyPrein(BigDecimal.ZERO);
                            StorageESUtils.setBModelDefalutDataByUpdate(updateReceiveItem, user);
                            updateReceiveItem.setModifierename(user.getEname());
                            receiveItemMapper.updateById(updateReceiveItem);
                            break;
                        }
                    }
                }else {
                    //入库明细中不包含该sku,置空原入库数量,避免重复入库
                    receiveItems.forEach(o -> o.setQtyReceive(BigDecimal.ZERO));
                }
                if (isLast) {
                    lastReleaseStorages.addAll(receiveItems);
                }
            }

            //最后一次入库，将逻辑收货单明细中 所有在途数量非0的记录更新为0
            if (isLast) {
                int count = receiveItemMapper.updatePreinByReceiveId(receiveId);
                if (count != sgBReceiveItems.size()) {
                    AssertUtils.logAndThrow("最后一次入库时,逻辑收货单明细更新失败！");
                }
            }
        }

    }

    private Long updateServiceNode(Integer billType) {
        switch (billType) {
            case SgConstantsIF.BILL_TYPE_RETAIL:
                return null;
            case SgConstantsIF.BILL_TYPE_SALE:
                return SgConstantsIF.SERVICE_NODE_SALE_IN_SUBMIT;
            case SgConstantsIF.BILL_TYPE_SALE_REF:
                return SgConstantsIF.SERVICE_NODE_REF_SALE_IN_SUBMIT;
            case SgConstantsIF.BILL_TYPE_PUR:
                return SgConstantsIF.SERVICE_NODE_PUR_IN_SUBMIT;
            case SgConstantsIF.BILL_TYPE_TRANSFER:
                return SgConstantsIF.SERVICE_NODE_TRANSFER_IN_SUBMIT;
            default:
                return null;
        }
    }

    private Integer getReceiveStatus(Long sourceBillId, Integer sourceBillType, User user) {
        Integer billStatus = SgReceiveConstantsIF.BILL_RECEIVE_STATUS_ALL_RECEIVE;
        ValueHolderV14<List<SgOutQueryResult>> queryPhyOutNotices = rpcService.queryPhyOutNotices(sourceBillId, sourceBillType);
        if (queryPhyOutNotices.isOK()) {
            List<SgOutQueryResult> phyOutNoticesData = queryPhyOutNotices.getData();
            if (CollectionUtils.isNotEmpty(phyOutNoticesData)) {
                SgBPhyOutNotices outNotices = phyOutNoticesData.get(0).getNotices().getOutNotices();
                Integer outStatus = outNotices.getBillStatus();
                if (outStatus == SgOutConstantsIF.OUT_NOTICES_STATUS_PART || outStatus == SgOutConstantsIF.OUT_NOTICES_STATUS_WAIT) {
                    billStatus = SgReceiveConstantsIF.BILL_RECEIVE_STATUS_PART_RECEIVE;
                } else if (outStatus == SgOutConstantsIF.OUT_NOTICES_STATUS_ALL) {
                    //查询入库通知单服务
                    ValueHolderV14<List<SgBPhyInNoticesResult>> queryPhyInNotices = rpcService.queryPhyInNotices(sourceBillId, sourceBillType, user);
                    if (queryPhyInNotices.isOK()) {
                        List<SgBPhyInNoticesResult> data = queryPhyInNotices.getData();
                        for (SgBPhyInNoticesResult notices : data) {
                            Integer inStatus = notices.getNotices().getBillStatus();
                            if (inStatus != SgInNoticeConstants.BILL_STATUS_IN_ALL) {
                                billStatus = SgReceiveConstantsIF.BILL_RECEIVE_STATUS_PART_RECEIVE;
                                break;
                            }
                        }
                    }
                }
            } else {
                //查询入库通知单服务
                ValueHolderV14<List<SgBPhyInNoticesResult>> queryPhyInNotices = rpcService.queryPhyInNotices(sourceBillId, sourceBillType, user);
                if (queryPhyInNotices.isOK()) {
                    List<SgBPhyInNoticesResult> data = queryPhyInNotices.getData();
                    for (SgBPhyInNoticesResult notices : data) {
                        Integer inStatus = notices.getNotices().getBillStatus();
                        if (inStatus != SgInNoticeConstants.BILL_STATUS_IN_ALL) {
                            billStatus = SgReceiveConstantsIF.BILL_RECEIVE_STATUS_PART_RECEIVE;
                            break;
                        }
                    }
                }
            }
        }
        return billStatus;
    }
}
