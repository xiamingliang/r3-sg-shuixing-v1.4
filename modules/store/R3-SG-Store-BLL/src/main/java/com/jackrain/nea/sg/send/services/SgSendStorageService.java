package com.jackrain.nea.sg.send.services;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.sg.basic.api.SgStorageBillUpdateCmd;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.model.request.*;
import com.jackrain.nea.sg.basic.model.result.SgStorageUpdateResult;
import com.jackrain.nea.sg.basic.model.table.CpCStore;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.send.common.SgSendConstantsIF;
import com.jackrain.nea.sg.send.model.request.SgSendBillSaveRequest;
import com.jackrain.nea.sg.send.model.table.SgBSend;
import com.jackrain.nea.sg.send.model.table.SgBSendImpItem;
import com.jackrain.nea.sg.send.model.table.SgBSendItem;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.*;

/**
 * 逻辑仓库存服务
 *
 * @author: 舒威
 * @since: 2019/4/30
 * create at : 2019/4/30 10:00
 */
@Slf4j
@Component
public class SgSendStorageService {

    public final static String CLEAN = "clean";

    public final static String VOID = "void";

    public final static String SUBMIT = "submit";

    /**
     * 初始化同步库存占用服务-整单
     */
    public SgStorageBillUpdateCmd initSgStorageBillUpdateCmd() {
        return (SgStorageBillUpdateCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(),
                SgStorageBillUpdateCmd.class.getName(),
                SgConstantsIF.GROUP, SgConstantsIF.VERSION);
    }


    /**
     * 库存更新-逻辑发货单
     *
     * @param send             逻辑发货单id
     * @param bSendItems       逻辑发货单有占用变化明细
     * @param request          request-model
     * @param map              原逻发货单明细 k-v   <skuid+逻辑仓id , 明细>
     * @param action           动作 - 提交、作废、清空用
     * @param loginUser        登录用户
     * @param isLast           是否最后一次出入库
     * @param negativeStock    负库存 k-v   <storeid , 逻辑仓信息>
     * @param isDeficiency     是否实缺业务
     * @param impList          录入明细
     * @param skuTeusCounts    箱内明细数量合计 k-v   <skuid , 箱内明细数量合计>
     * @param impItemMap       原录入明细 k-v   <skuid+逻辑仓id+来源单据明细id , 录入明细>    @return 库存占用更新结果
     * @param noticesId
     * @param noticesBillno
     * @param orgSkuTeusCounts
     */
    public ValueHolderV14<SgStorageUpdateResult> updateSendStoragePreout(SgBSend send,
                                                                         List<SgBSendItem> bSendItems,
                                                                         SgSendBillSaveRequest request,
                                                                         HashMap<String, SgBSendItem> map,
                                                                         String action,
                                                                         User loginUser,
                                                                         Boolean isLast,
                                                                         Map<Long, CpCStore> negativeStock,
                                                                         Boolean isNegativePreout,
                                                                         Boolean isDeficiency,
                                                                         List<SgBSendImpItem> impList,
                                                                         Map<Long, BigDecimal> skuTeusCounts,
                                                                         HashMap<String, SgBSendImpItem> impItemMap,
                                                                         Long noticesId,
                                                                         String noticesBillno,
                                                                         Map<Long, BigDecimal> orgSkuTeusCounts) {
        ValueHolderV14<SgStorageUpdateResult> storageResult = null;
        try {
            SgStorageUpdateBillRequest billRequest = new SgStorageUpdateBillRequest();
            //单据信息
            billRequest.setBillId(send.getId());
            billRequest.setBillNo(send.getBillNo());
            billRequest.setBillDate(send.getBillDate());
            billRequest.setBillType(send.getSourceBillType());
            billRequest.setChangeDate(new Timestamp(System.currentTimeMillis()));
            billRequest.setCpCshopId(send.getCpCShopId());
            billRequest.setServiceNode(send.getServiceNode());
            billRequest.setIsCancel(request != null ? request.getIsCancel() : false);
            billRequest.setSourceBillId(send.getSourceBillId());
            billRequest.setSourceBillNo(send.getSourceBillNo());
            billRequest.setPhyOutNoticesId(noticesId);
            billRequest.setPhyOutNoticesNo(noticesBillno);

            //单据明细信息
            List<SgStorageUpdateBillItemRequest> itemList = request != null ?
                    buildSendStorageItems(request, map, negativeStock, skuTeusCounts) : buildSendStorageItems(bSendItems, action, isLast, negativeStock, isNegativePreout, isDeficiency, skuTeusCounts, orgSkuTeusCounts);
            billRequest.setItemList(itemList);

            //录入明细信息
            if ((request != null && CollectionUtils.isNotEmpty(request.getImpItemList())) || CollectionUtils.isNotEmpty(impList)) {
                List<SgTeusStorageUpdateBillItemRequest> teusList = request != null ?
                        buildTeusSendStorageItems(request, impItemMap, negativeStock) :
                        buildTeusSendStorageItems(impList, action, isLast, negativeStock, isNegativePreout);
                billRequest.setTeusList(teusList);
            }

            SgStorageUpdateControlRequest controlModel = new SgStorageUpdateControlRequest();
            controlModel.setPreoutOperateType(request != null ? request.getPreoutWarningType() : SgConstantsIF.PREOUT_RESULT_ERROR);

            SgStorageSingleUpdateRequest updateRequest = new SgStorageSingleUpdateRequest();
            updateRequest.setBill(billRequest);
            String msgKey = SgConstants.MSG_TAG_SEND + ":" + send.getBillNo();
            if (!VOID.equals(action)) {
                msgKey += System.currentTimeMillis();
            }
            updateRequest.setMessageKey(msgKey);
            updateRequest.setLoginUser(loginUser);
            updateRequest.setControlModel(controlModel);

            SgStorageBillUpdateCmd sgStorageUpdateCmd = initSgStorageBillUpdateCmd();
            if (log.isDebugEnabled()) {
                log.debug("来源单据id[" + send.getSourceBillId() + "],开始占用库存占用入参" + JSONObject.toJSONString(updateRequest));
            }
            storageResult = sgStorageUpdateCmd.updateStorageBillWithTrans(updateRequest);
            if (log.isDebugEnabled()) {
                log.debug("来源单据id[" + send.getSourceBillId() + "],占用库存占用出参" + storageResult.toJSONObject().toJSONString());
            }
        } catch (Exception e) {
            e.printStackTrace();
            AssertUtils.logAndThrowExtra("同步库存占用接口服务异常", e);
        }
        return storageResult;
    }


    private List<SgStorageUpdateBillItemRequest> buildSendStorageItems(SgSendBillSaveRequest request, HashMap<String, SgBSendItem> map,
                                                                       Map<Long, CpCStore> negativeStock, Map<Long, BigDecimal> skuTeusCounts) {
        List<SgStorageUpdateBillItemRequest> itemList = new ArrayList<>();
        if (SgConstantsIF.ITEM_UPDATE_TYPE_ALL.equals(request.getUpdateMethod())) { //全量
            //逆向冲单明细数据
            request.getItemList().forEach(itemSaveRequest -> {
                if (itemSaveRequest.getIsSendStorage()) {
                    //原逻辑发货单明细
                    SgBSendItem sendItem = map.get(itemSaveRequest.getPsCSkuId() + "_" + itemSaveRequest.getCpCStoreId() + "_" + itemSaveRequest.getSourceBillItemId());
                    if (sendItem != null && 0 != BigDecimal.ZERO.compareTo(sendItem.getQtyPreout().negate())) {
                        SgStorageUpdateBillItemRequest item = new SgStorageUpdateBillItemRequest();
                        BeanUtils.copyProperties(sendItem, item);
                        item.setBillItemId(sendItem.getId());
                        //19-06-05 该属性已启用
                        //item.setStorageType(SgConstantsIF.STORAGE_TYPE_PREOUT);
                        item.setQtyPreoutChange(sendItem.getQtyPreout().negate());
                        if (MapUtils.isNotEmpty(negativeStock)) {
                            SgStorageUpdateControlRequest controlRequest = getStorageUpdateControlRequest(sendItem.getCpCStoreId(), negativeStock, request.getIsNegativePreout(), false);
                            item.setControlmodel(controlRequest);
                        }
                        //添加箱内明細
                        if (MapUtils.isNotEmpty(skuTeusCounts)) {
                            if (!ObjectUtils.isEmpty(skuTeusCounts.get(itemSaveRequest.getPsCSkuId()))) {
                                item.setQtyPreoutTeusChange(skuTeusCounts.get(itemSaveRequest.getPsCSkuId()).negate());
                            }
                        }
                        itemList.add(item);
                    }
                }
            });
        }

        request.getItemList().forEach(itemSaveRequest -> {
            if (itemSaveRequest.getIsSendStorage() && 0 != BigDecimal.ZERO.compareTo(itemSaveRequest.getQtyPreout())) {
                SgStorageUpdateBillItemRequest item = new SgStorageUpdateBillItemRequest();
                BeanUtils.copyProperties(itemSaveRequest, item);
                item.setBillItemId(itemSaveRequest.getId());
                // item.setStorageType(SgConstantsIF.STORAGE_TYPE_PREOUT);
                item.setQtyPreoutChange(itemSaveRequest.getQtyPreout());
                if (MapUtils.isNotEmpty(negativeStock)) {
                    SgStorageUpdateControlRequest controlRequest = getStorageUpdateControlRequest(itemSaveRequest.getCpCStoreId(), negativeStock, request.getIsNegativePreout(), false);
                    item.setControlmodel(controlRequest);
                }
                //添加箱内明細
                if (MapUtils.isNotEmpty(skuTeusCounts)) {
                    if (!ObjectUtils.isEmpty(skuTeusCounts.get(itemSaveRequest.getPsCSkuId()))) {
                        item.setQtyPreoutTeusChange(skuTeusCounts.get(itemSaveRequest.getPsCSkuId()));
                    }
                }
                itemList.add(item);
            }
        });

        return itemList;
    }

    private List<SgStorageUpdateBillItemRequest> buildSendStorageItems(List<SgBSendItem> bSendItems, String action, Boolean isLast, Map<Long, CpCStore> negativeStock,
                                                                       Boolean isNegativePreout, Boolean isDeficiency,
                                                                       Map<Long, BigDecimal> skuTeusCounts, Map<Long, BigDecimal> orgSkuTeusCounts) {
        List<SgStorageUpdateBillItemRequest> itemList = new ArrayList<>();
        bSendItems.forEach(sgBSendItem -> {
            BigDecimal preoutQtyChange = sgBSendItem.getQtyPreout().negate();
            BigDecimal storageQtyChange = sgBSendItem.getQtySend().negate();
            if ((!SUBMIT.equals(action)) && 0 != BigDecimal.ZERO.compareTo(preoutQtyChange)) {
                SgStorageUpdateBillItemRequest item = new SgStorageUpdateBillItemRequest();
                BeanUtils.copyProperties(sgBSendItem, item);
                item.setBillItemId(sgBSendItem.getId());
                //item.setStorageType(SgConstantsIF.STORAGE_TYPE_PREOUT);
                item.setQtyPreoutChange(preoutQtyChange);
                if (MapUtils.isNotEmpty(negativeStock)) {
                    SgStorageUpdateControlRequest controlRequest = getStorageUpdateControlRequest(sgBSendItem.getCpCStoreId(), negativeStock, isNegativePreout, isDeficiency);
                    item.setControlmodel(controlRequest);
                }
                //添加箱内明細
                if (MapUtils.isNotEmpty(skuTeusCounts)) {
                    if (!ObjectUtils.isEmpty(skuTeusCounts.get(sgBSendItem.getPsCSkuId()))) {
                        item.setQtyPreoutTeusChange(skuTeusCounts.get(sgBSendItem.getPsCSkuId()).negate());
                    }
                }
                itemList.add(item);
            }
            if ((SUBMIT.equals(action) || isLast) && (0 != BigDecimal.ZERO.compareTo(preoutQtyChange)
                    || 0 != BigDecimal.ZERO.compareTo(storageQtyChange))) {
                SgStorageUpdateBillItemRequest item = new SgStorageUpdateBillItemRequest();
                BeanUtils.copyProperties(sgBSendItem, item);
                item.setBillItemId(sgBSendItem.getId());
                //item.setStorageType(SgConstantsIF.STORAGE_TYPE_STORAGE);
                item.setQtyPreoutChange(preoutQtyChange);
                item.setQtyStorageChange(storageQtyChange);
                if (MapUtils.isNotEmpty(negativeStock)) {
                    SgStorageUpdateControlRequest controlRequest = getStorageUpdateControlRequest(sgBSendItem.getCpCStoreId(), negativeStock, isNegativePreout, false);
                    item.setControlmodel(controlRequest);
                }
                //添加箱内明細
                if (MapUtils.isNotEmpty(skuTeusCounts) || MapUtils.isNotEmpty(orgSkuTeusCounts)) {
                    if (!ObjectUtils.isEmpty(skuTeusCounts.get(sgBSendItem.getPsCSkuId()))) {
                        if (isLast) {
                            item.setQtyPreoutTeusChange(orgSkuTeusCounts.get(sgBSendItem.getPsCSkuId()).negate());
                        } else {
                            item.setQtyPreoutTeusChange(skuTeusCounts.get(sgBSendItem.getPsCSkuId()).negate());
                        }
                        item.setQtyStorageTeusChange(skuTeusCounts.get(sgBSendItem.getPsCSkuId()).negate());
                    }
                }
                itemList.add(item);
            }
        });

        return itemList;
    }


    private List<SgTeusStorageUpdateBillItemRequest> buildTeusSendStorageItems(SgSendBillSaveRequest request, HashMap<String, SgBSendImpItem> map, Map<Long, CpCStore> negativeStock) {
        List<SgTeusStorageUpdateBillItemRequest> itemList = new ArrayList<>();
        if (SgConstantsIF.ITEM_UPDATE_TYPE_ALL.equals(request.getUpdateMethod())) { //全量
            //逆向冲单明细数据
            request.getImpItemList().forEach(itemSaveRequest -> {
                if (itemSaveRequest.getIsSendStorage() && itemSaveRequest.getIsTeus().equals(SgSendConstantsIF.IS_TEUS_Y)) {
                    //原录入明细
                    SgBSendImpItem impItem = map.get(itemSaveRequest.getPsCProId() + "_" + itemSaveRequest.getCpCStoreId() + "_" + itemSaveRequest.getSourceBillItemId());
                    if (impItem != null && 0 != BigDecimal.ZERO.compareTo(impItem.getQtyPreout().negate())) {
                        SgTeusStorageUpdateBillItemRequest teusStorageitem = new SgTeusStorageUpdateBillItemRequest();
                        BeanUtils.copyProperties(impItem, teusStorageitem);
                        teusStorageitem.setBillItemId(impItem.getId());
                        teusStorageitem.setQtyPreoutChange(impItem.getQtyPreout().negate());
                        if (MapUtils.isNotEmpty(negativeStock)) {
                            SgStorageUpdateControlRequest controlRequest = getStorageUpdateControlRequest(impItem.getCpCStoreId(), negativeStock, request.getIsNegativePreout(), false);
                            teusStorageitem.setControlmodel(controlRequest);
                        }
                        itemList.add(teusStorageitem);
                    }
                }
            });
        }

        request.getImpItemList().forEach(itemSaveRequest -> {
            if (itemSaveRequest.getIsSendStorage() && itemSaveRequest.getIsTeus().equals(SgSendConstantsIF.IS_TEUS_Y) && 0 != BigDecimal.ZERO.compareTo(itemSaveRequest.getQtyPreout())) {
                SgTeusStorageUpdateBillItemRequest teusStorageitem = new SgTeusStorageUpdateBillItemRequest();
                BeanUtils.copyProperties(itemSaveRequest, teusStorageitem);
                teusStorageitem.setBillItemId(itemSaveRequest.getId());
                teusStorageitem.setQtyPreoutChange(itemSaveRequest.getQtyPreout());
                if (MapUtils.isNotEmpty(negativeStock)) {
                    SgStorageUpdateControlRequest controlRequest = getStorageUpdateControlRequest(itemSaveRequest.getCpCStoreId(), negativeStock, request.getIsNegativePreout(), false);
                    teusStorageitem.setControlmodel(controlRequest);
                }
                itemList.add(teusStorageitem);
            }
        });

        return itemList;
    }

    private List<SgTeusStorageUpdateBillItemRequest> buildTeusSendStorageItems(List<SgBSendImpItem> impItems, String action, Boolean isLast, Map<Long, CpCStore> negativeStock, Boolean isNegativePreout) {
        List<SgTeusStorageUpdateBillItemRequest> itemList = new ArrayList<>();
        impItems.forEach(impItem -> {
            if (impItem.getIsTeus().equals(SgSendConstantsIF.IS_TEUS_Y)) {
                BigDecimal preoutQtyChange = impItem.getQtyPreout().negate();
                BigDecimal storageQtyChange = impItem.getQtySend().negate();
                if ((!SUBMIT.equals(action)) && 0 != BigDecimal.ZERO.compareTo(preoutQtyChange)) {
                    SgTeusStorageUpdateBillItemRequest teusStorageitem = new SgTeusStorageUpdateBillItemRequest();
                    BeanUtils.copyProperties(impItem, teusStorageitem);
                    teusStorageitem.setBillItemId(impItem.getId());
                    teusStorageitem.setQtyPreoutChange(preoutQtyChange);
                    if (MapUtils.isNotEmpty(negativeStock)) {
                        SgStorageUpdateControlRequest controlRequest = getStorageUpdateControlRequest(impItem.getCpCStoreId(), negativeStock, isNegativePreout, false);
                        teusStorageitem.setControlmodel(controlRequest);
                    }
                    itemList.add(teusStorageitem);
                }
                if ((SUBMIT.equals(action) || isLast) && (0 != BigDecimal.ZERO.compareTo(preoutQtyChange) || 0 != BigDecimal.ZERO.compareTo(storageQtyChange))) {
                    SgTeusStorageUpdateBillItemRequest teusStorageitem = new SgTeusStorageUpdateBillItemRequest();
                    BeanUtils.copyProperties(impItem, teusStorageitem);
                    teusStorageitem.setBillItemId(impItem.getId());
                    teusStorageitem.setQtyPreoutChange(preoutQtyChange);
                    teusStorageitem.setQtyStorageChange(storageQtyChange);
                    if (MapUtils.isNotEmpty(negativeStock)) {
                        SgStorageUpdateControlRequest controlRequest = getStorageUpdateControlRequest(impItem.getCpCStoreId(), negativeStock, isNegativePreout, false);
                        teusStorageitem.setControlmodel(controlRequest);
                    }
                    itemList.add(teusStorageitem);
                }
            }
        });

        return itemList;
    }


    /**
     * @param cpCStoreId    逻辑仓id
     * @param negativeStock 逻辑仓Map
     * @param isNegative    是否允许府库粗你
     * @param isDeficiency
     * @return controlRequest  库存更新负库存控制request
     */
    public SgStorageUpdateControlRequest getStorageUpdateControlRequest(Long cpCStoreId, Map<Long, CpCStore> negativeStock, Boolean isNegative, Boolean isDeficiency) {
        SgStorageUpdateControlRequest controlRequest = new SgStorageUpdateControlRequest();
        CpCStore cpCStore = negativeStock.get(cpCStoreId);
        //1 开启负库存控制（不允许负库存）0 允许负库存
        Long isnegative = isNegative ? 0L : Optional.ofNullable(cpCStore.getIsnegative()).orElse(1L);
        //是否实缺业务
        if (isDeficiency != null && isDeficiency) {
            isnegative = 0L;
        }
        controlRequest.setNegativePrein(isnegative == 0);
        controlRequest.setNegativePreout(isnegative == 0);
        controlRequest.setNegativeStorage(isnegative == 0);
        controlRequest.setNegativeAvailable(isnegative == 0);
        return controlRequest;
    }
}
