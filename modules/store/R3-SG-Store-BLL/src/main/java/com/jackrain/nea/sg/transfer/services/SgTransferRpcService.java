package com.jackrain.nea.sg.transfer.services;

import com.jackrain.nea.cpext.model.table.CpCustomer;
import com.jackrain.nea.oc.sale.api.OcRefundSaleSaveAndAuditCmd;
import com.jackrain.nea.oc.sale.api.OcSaleSaveAndAuditCmd;
import com.jackrain.nea.oc.sale.api.OcTransLogCmd;
import com.jackrain.nea.oc.sale.model.request.OcRefundSaleBillSaveRequest;
import com.jackrain.nea.oc.sale.model.request.OcSaleBillSaveRequest;
import com.jackrain.nea.oc.sale.model.table.OcBTransLog;
import com.jackrain.nea.oc.sx.api.OcRefundApplyUpdateCmd;
import com.jackrain.nea.oc.sx.model.request.OcRefundApplyOrderRequest;
import com.jackrain.nea.sg.in.api.SgBInPickorderItemQueryCmd;
import com.jackrain.nea.sg.in.model.table.SgBInPickorder;
import com.jackrain.nea.sx.api.ClCClientOrgRelationQueryCmd;
import com.jackrain.nea.sx.api.CpCustomerQuerySxCmd;
import com.jackrain.nea.sx.model.table.ClCClientOrgRelation;
import com.jackrain.nea.sx.model.table.CpCCustomer;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author leexh
 * @since 2020/3/24 19:11
 * desc: rpc调用类
 */
@Slf4j
@Component
public class SgTransferRpcService {

    @Reference(version = "1.0", group = "cp-sx")
    private ClCClientOrgRelationQueryCmd queryCmd;

    @Reference(version = "1.0", group = "cp-sx")
    private CpCustomerQuerySxCmd customerQuerySxCmd;

    @Reference(version = "1.0", group = "oc")
    private OcSaleSaveAndAuditCmd saleSaveAndAuditCmd;

    @Reference(version = "1.0", group = "oc")
    private OcRefundSaleSaveAndAuditCmd refundSaleSaveAndAuditCmd;

    @Reference(version = "1.0", group = "oc")
    private OcTransLogCmd transLogCmd;

    @Reference(version = "1.0", group = "oc")
    private OcRefundApplyUpdateCmd applyUpdateCmd;

    @Reference(version = "1.0", group = "sg")
    private SgBInPickorderItemQueryCmd inPickOrderQueryCmd;

    /**
     * 根据送货客户编码查询
     *
     * @param code 送货客户编码
     * @return 送货客户
     */
    public List<ClCClientOrgRelation> queryOrgRelationsByStore(String code) {
        return queryCmd.queryOrgRelationsByStore(code);
    }

    /**
     * 根据送货客户ID查询
     *
     * @param id 送货客户ID
     * @return 送货客户
     */
    public ClCClientOrgRelation queryClientOrgRelationById(Long id) {
        return queryCmd.queryOrgRelationsById(id);
    }


    /**
     * 根据经销商ID查询经销商
     *
     * @param customerId 经销商编码
     */
    public CpCCustomer queryCpCustomer(Long customerId) {
        return customerQuerySxCmd.queryCpCustomer(customerId);
    }

    /**
     * 根据经销商ID查询上级经销商信息
     *
     * @param customerId 经销商ID
     */
    public List<CpCustomer> queryCustomerUpInfoById(Long customerId) {
        return customerQuerySxCmd.queryCustomerUpInfoById(customerId);
    }


    /**
     * 新增并审核销售单
     *
     * @param request 入参
     * @return v14
     */
    public ValueHolderV14 saleSaveAndAudit(OcSaleBillSaveRequest request) {
        return saleSaveAndAuditCmd.saveAndAuditSale(request);
    }

    /**
     * 新增并审核销售退货单
     *
     * @param request 入参
     * @return v14
     */
    public ValueHolderV14 refundSaleSaveAndAudit(OcRefundSaleBillSaveRequest request) {
        return refundSaleSaveAndAuditCmd.saveAndAuditRefundSale(request);
    }

    /**
     * 中台中间表服务
     *
     * @param transLog 入参
     * @return 出参
     */
    public ValueHolderV14<OcBTransLog> synTransLog(OcBTransLog transLog) {
        return transLogCmd.invoke(transLog);
    }

    /**
     * 更新退货申请单出库数量和状态
     *
     * @param request 入参
     * @return v14
     */
    public ValueHolderV14<OcRefundApplyOrderRequest> updateOutRefundApply(OcRefundApplyOrderRequest request) {
        return applyUpdateCmd.updateOutRefundApply(request);
    }

    /**
     * 更新退货申请单入库数量和状态
     *
     * @param request 入参
     * @return v14
     */
    public ValueHolderV14<OcRefundApplyOrderRequest> updateInRefundApply(OcRefundApplyOrderRequest request) {
        return applyUpdateCmd.updateInRefundApply(request);
    }

    /**
     * 根据来源单号查询拣货单号
     *
     * @param sourceBillNo 来源单号
     * @return 入库拣货单对象
     */
    public SgBInPickorder queryPickOrderBySourceBillNo(String sourceBillNo) {
        return inPickOrderQueryCmd.queryPickOrderBySourceBillNo(sourceBillNo);
    }
}
