package com.jackrain.nea.sg.task;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.google.common.collect.Lists;
import com.jackrain.nea.ac.sc.basic.model.enums.SourceBillType;
import com.jackrain.nea.ac.sc.basic.model.request.AcPushAcBillRequest;
import com.jackrain.nea.ac.sc.utils.utils.AcScOrigBillUtil;
import com.jackrain.nea.config.Resources;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.es.util.ElasticSearchUtil;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.adjust.common.SgAdjustConstants;
import com.jackrain.nea.sg.adjust.mapper.SgBAdjustItemMapper;
import com.jackrain.nea.sg.adjust.mapper.SgBAdjustMapper;
import com.jackrain.nea.sg.adjust.model.AcAdjustItem;
import com.jackrain.nea.sg.adjust.model.table.SgBAdjust;
import com.jackrain.nea.sg.adjust.model.table.SgBAdjustItem;
import com.jackrain.nea.sg.adjust.services.SgAdjAuditService;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

/**
 * @author leexh
 * @since 2019/8/14 16:19
 * desc: 捞取生成结算日志失败的逻辑调整单
 * ，重新生成结算日志
 */
@Slf4j
@Component
public class SgAdjustSendLogService {

    @Autowired
    private SgTransferSendLogService transferLogService;

    @Autowired
    private SgAdjAuditService adjAuditService;

    @Autowired
    private SgBAdjustMapper adjustMapper;

    @Autowired
    private SgBAdjustItemMapper itemMapper;

    public ValueHolderV14 sendLogForAdjust(JSONObject params) {

        log.info("调整单重新生成结算日志入参:" + JSONObject.toJSONString(params));
        ValueHolderV14 v14 = new ValueHolderV14(ResultCode.SUCCESS, "执行生成结算日志成功!");

        int pageSize = SgConstants.SG_COMMON_MAX_INSERT_PAGE_SIZE;
        int itemPageSize = SgConstants.SG_COMMON_ITEM_INSERT_PAGE_SIZE;

        User user = transferLogService.getRootUser();

        JSONArray error = new JSONArray();

        //实际待处理调整单id
        List<Long> adjustids = Lists.newArrayList();
        try {
            //待生成结算日志调整单数量
            int fail = 0;
            //实际生成结算日志数据
            List<AcPushAcBillRequest> acBillRequests = Lists.newArrayList();

            //查询参数
            JSONObject whereKey = transferLogService.createParam(params);
            whereKey.put("pageSize", pageSize);

            //获取主表分页数
            Integer cnt = adjustMapper.selectCountAdjustByLog(whereKey);
            if (cnt == null || cnt == 0) {
                v14.setCode(ResultCode.FAIL);
                v14.setMessage("没有符合条件的待传结算日志的调整但!");
                return v14;
            }

            int page = cnt / pageSize;
            if (cnt % pageSize != 0) {
                page++;
            }

            for (int i = 0; i < page; i++) {
                whereKey.put("offSet", i * pageSize);
                List<SgBAdjust> adjusts = adjustMapper.selectAdjustByLog(whereKey);
                if (CollectionUtils.isNotEmpty(adjusts)) {
                    fail += adjusts.size();
                    for (SgBAdjust adjust : adjusts) {
                        try {
                            //获取明细分页数
                            int itemCnt = itemMapper.selectCount(new QueryWrapper<SgBAdjustItem>().lambda().eq(SgBAdjustItem::getSgBAdjustId, adjust.getId()));
                            int itemPage = itemCnt / itemPageSize;
                            if (itemCnt % itemPageSize != 0) {
                                itemPage++;
                            }

                            List<SgBAdjustItem> adjustitems = Lists.newArrayList();
                            for (int k = 0; k < itemPage; k++) {
                                List<SgBAdjustItem> items = itemMapper.selectAdjustItemListByPage(adjust.getId(), itemPageSize, k * itemPageSize);
                                if (CollectionUtils.isNotEmpty(items)) {
                                    adjustitems.addAll(items);
                                }
                            }

                            AcPushAcBillRequest acBillRequest = createSettleLogForAdjust(user, adjust, adjustitems);
                            if (acBillRequest != null) acBillRequests.add(acBillRequest);
                            adjustids.add(adjust.getId());
                        } catch (Exception e) {
                            log.error("objid:" + adjust.getId() + "获取结算日志数据失败!" + e.getMessage());
                            JSONObject err = new JSONObject();
                            err.put("objid", adjust.getId());
                            err.put("message", "objid:" + adjust.getId() + "获取结算日志数据失败!" + e.getMessage());
                            error.add(err);
                        }
                    }
                }

            }

            if (CollectionUtils.isNotEmpty(error)) {
                v14.setCode(ResultCode.FAIL);
                v14.setMessage(JSONObject.toJSONString(error));
                v14.setData(error);
            }

            if (CollectionUtils.isNotEmpty(acBillRequests)) {
                try {
                    log.info("待生成结算日志调整单数量:" + fail + ",实际生成结算日志数量:" + acBillRequests.size()
                            + ",实际待处理调整单ids:" + JSONObject.toJSONString(adjustids));

                    ValueHolderV14 result = AcScOrigBillUtil.batchPushAcBill(acBillRequests);

                    log.info("调整单重新生成结算日志结果:" + JSONObject.toJSONString(result));

                    if (result.isOK()) {
                        updateAdjust(user, adjustids, Boolean.TRUE);
                    } else {
                        AssertUtils.logAndThrow(result.getMessage());
                    }
                } catch (Exception e) {
                    log.error("调整单重新生成结算日志失败!" + e.getMessage());
                    updateAdjust(user, adjustids, Boolean.FALSE);
                    v14.setCode(ResultCode.FAIL);
                    v14.setMessage("调整单重新生成结算日志失败!" + e.getMessage());
                }
            }
        } catch (Exception e) {
            v14.setCode(ResultCode.FAIL);
            v14.setMessage("生成结算日志异常:" + e.getMessage());
        }

        log.info("调整单重新生成结算日志出参:" + JSONObject.toJSONString(v14));
        return v14;
    }

    /**
     * 补发结算日志
     *
     * @param user   用户信息
     * @param adjust 调整单
     * @param items  明细
     * @return 发送结果
     */

    private AcPushAcBillRequest createSettleLogForAdjust(User user, SgBAdjust adjust, List<SgBAdjustItem> items) {

        try {
            //获取结算日志明细
            List<AcAdjustItem> adjustItems = adjAuditService.getAcAdjustitems(items);
            AcPushAcBillRequest billRequest = new AcPushAcBillRequest();
            billRequest.setBillType(SourceBillType.ADJUST);
            billRequest.setSourceBillNo(adjust.getBillNo());
            Date checkTime = adjust.getCheckTime();
            if (checkTime == null) {
                AssertUtils.logAndThrow("调整单id" + adjust.getId() + "审核时间为空,请联系管理员!");
            }
            billRequest.setSourceBillDate(checkTime);

            JSONObject bill = new JSONObject();
            bill.put(SgConstants.SG_B_ADJUST.toUpperCase(), adjust);
            bill.put(SgConstants.SG_B_ADJUST_ITEM.toUpperCase(), adjustItems);
            Long propId = adjust.getSgBAdjustPropId();
            //库存调整性质是无头件、冲无头件、销退错录、分销错录，写死经销商编码和名称
            if (propId != null && (propId.equals(SgAdjustConstants.SG_B_ADJUST_PROP_NO_SOURCE_IN) ||
                    propId.equals(SgAdjustConstants.SG_B_ADJUST_PROP_FLUSH_NO_SOURCE) ||
                    propId.equals(SgAdjustConstants.SG_B_ADJUST_PROP_SALES_RETURN_FAIL_RECORD) ||
                    propId.equals(SgAdjustConstants.SG_B_ADJUST_PROP_DISTRIB_SALES_FAIL_RECORD))) {
                bill.put("CP_C_CUSTOMER_ECODE", "WTKH");
                bill.put("CP_C_CUSTOMER_ENAME", "无头客户");
            } else if (propId != null && propId.equals(SgAdjustConstants.SG_B_ADJUST_PROP_VIP_RETURN)) {
                bill.put("CP_C_CUSTOMER_ECODE", "10070");
                bill.put("CP_C_CUSTOMER_ENAME", "唯品会（中国）有限公司");
            }
            billRequest.setBill(bill);
            billRequest.setUser(user);
            return billRequest;
        } catch (Exception e) {
            log.error("获取结算日志数据异常:" + e.getMessage());
            throw new NDSException(Resources.getMessage("获取结算日志数据异常:" + e.getMessage()));
        }
    }


    /**
     * 更新调拨单发送结算日志状态
     *
     * @param user 用户信息
     * @param ids  调整单ids
     */
    private void updateAdjust(User user, List<Long> ids, Boolean isOk) {
        if (CollectionUtils.isNotEmpty(ids)) {
            int pageSize = SgConstants.SG_COMMON_INSERT_PAGE_SIZE;
            //获取主表分页数
            int cnt = ids.size();
            int page = cnt / pageSize;
            if (cnt % pageSize != 0) {
                page++;
            }
            List<List<Long>> sourceList = StorageESUtils.averageAssign(ids, page);
            if (CollectionUtils.isNotEmpty(sourceList)) {
                for (List<Long> source : sourceList) {
                    Long reserveBigint01 = isOk ? 2L : 3L;
                    SgBAdjust update = new SgBAdjust();
                    StorageESUtils.setBModelDefalutDataByUpdate(update, user);
                    update.setModifierename(user.getEname());
                    update.setReserveBigint01(reserveBigint01);
                    int num = adjustMapper.update(update, new UpdateWrapper<SgBAdjust>().lambda().in(SgBAdjust::getId, source));
                    if (num == source.size()) {
                        log.info("调整单重新生成结算,更新调整单状态成功:" + num + "条!");
                    } else {
                        log.error("调整单重新生成结算,更新调整单状态成功:" + num + "条,失败:" + (source.size() - num) + "条,更新失败ids:" + JSONObject.toJSONString(source));
                    }

                    /*推送ES*/
                    List<SgBAdjust> adjusts = adjustMapper.selectList(new QueryWrapper<SgBAdjust>().lambda().in(SgBAdjust::getId, source));
                    try {
                        ElasticSearchUtil.indexDocuments(SgConstants.SG_B_ADJUST, SgConstants.SG_B_ADJUST, adjusts);
                    } catch (Exception e) {
                        log.error("push batch es error!" + JSONObject.toJSONString(source) + ":" + e.getMessage());
                    }
                }
            }
        }
    }
}
