package com.jackrain.nea.sg.transfer.validate;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.transfer.common.SgTransferConstantsIF;
import com.jackrain.nea.tableService.MainTableRecord;
import com.jackrain.nea.tableService.TableServiceContext;
import com.jackrain.nea.tableService.constants.Constants;
import com.jackrain.nea.tableService.validate.BaseValidator;
import com.jackrain.nea.utils.AssertUtils;
import org.springframework.stereotype.Component;

import java.util.Locale;

/**
 * @author zhu lin yu
 * @since 2019/10/16
 * create at : 2019/10/16 9:46
 */
@Component
public class ScTransferSaveValidate extends BaseValidator {
    @Override
    public void validate(TableServiceContext tableServiceContext, MainTableRecord mainTableRecord) {
        Locale locale = tableServiceContext.getLocale();
        if (Constants.ACTION_SAVE.equals(tableServiceContext.getActionName())) {
            JSONObject orignalData = mainTableRecord.getOrignalData();
            Integer status = orignalData.getInteger("STATUS");
            AssertUtils.cannot(status != null && status != SgTransferConstantsIF.BILL_STATUS_WAIT, "当前单据已审核，不允许保存！", locale);
        }
    }
}
