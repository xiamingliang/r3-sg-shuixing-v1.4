package com.jackrain.nea.sg.adjust.mapper;

import com.jackrain.nea.jdbc.mybatis.plus.ExtentionMapper;
import com.jackrain.nea.sg.adjust.common.SgAdjustConstants;
import com.jackrain.nea.sg.adjust.model.table.SgBAdjustItem;
import com.jackrain.nea.sg.basic.common.SgConstants;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.math.BigDecimal;
import java.util.List;

@Mapper
public interface SgBAdjustItemMapper extends ExtentionMapper<SgBAdjustItem> {

    /**
     * 查询明细 数量的汇总
     *
     * @param adjId 主表id
     * @return qty汇总
     */
    @Select("select sum(qty) from sg_b_adjust_item where sg_b_adjust_id = #{adjId} ")
    BigDecimal selectSumQtyByAdjId(@Param("adjId") Long adjId);

    /**
     * 分页查询逻辑调整单
     */
    @Select("SELECT * FROM " + SgConstants.SG_B_ADJUST_ITEM + " ORDER BY ID LIMIT #{page} OFFSET #{offset}")
    List<SgBAdjustItem> selectListByPage(@Param("page") int page, @Param("offset") int offset);

    /**
     * 分页查询逻辑调整单
     */
    @Select("SELECT * FROM " + SgConstants.SG_B_ADJUST_ITEM + " WHERE " + SgAdjustConstants.SG_B_ADJUST_ID + " = #{objid} ORDER BY ID LIMIT #{page} OFFSET #{offset}")
    List<SgBAdjustItem> selectAdjustItemListByPage(@Param("objid") long objid, @Param("page") int page, @Param("offset") int offset);
}