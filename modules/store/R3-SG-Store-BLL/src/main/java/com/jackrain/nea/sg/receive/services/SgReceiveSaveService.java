package com.jackrain.nea.sg.receive.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.model.util.ModelUtil;
import com.jackrain.nea.redis.config.CusRedisTemplate;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.config.SgStorageBoxConfig;
import com.jackrain.nea.sg.basic.model.result.SgStorageUpdateResult;
import com.jackrain.nea.sg.basic.model.table.CpCStore;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.receive.common.SgReceiveConstants;
import com.jackrain.nea.sg.receive.common.SgReceiveConstantsIF;
import com.jackrain.nea.sg.receive.mapper.SgBReceiveImpItemMapper;
import com.jackrain.nea.sg.receive.mapper.SgBReceiveItemMapper;
import com.jackrain.nea.sg.receive.mapper.SgBReceiveMapper;
import com.jackrain.nea.sg.receive.mapper.SgBReceiveTeusItemMapper;
import com.jackrain.nea.sg.receive.model.request.SgReceiveBillSaveRequest;
import com.jackrain.nea.sg.receive.model.request.SgReceiveImpItemSaveRequest;
import com.jackrain.nea.sg.receive.model.request.SgReceiveItemSaveRequest;
import com.jackrain.nea.sg.receive.model.request.SgReceiveTeusItemSaveRequest;
import com.jackrain.nea.sg.receive.model.result.SgReceiveBillSaveResult;
import com.jackrain.nea.sg.receive.model.table.SgBReceive;
import com.jackrain.nea.sg.receive.model.table.SgBReceiveImpItem;
import com.jackrain.nea.sg.receive.model.table.SgBReceiveItem;
import com.jackrain.nea.sg.receive.model.table.SgBReceiveTeusItem;
import com.jackrain.nea.sg.send.model.request.SgSendTeusItemSaveRequest;
import com.jackrain.nea.sg.send.utils.SgStoreESUtils;
import com.jackrain.nea.sg.send.utils.SgStoreUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @author 舒威
 * @since 2019/4/19
 * create at : 2019/4/28 22:11
 */
@Slf4j
@Component
public class SgReceiveSaveService {

    @Autowired
    private SgBReceiveMapper receiveMapper;

    @Autowired
    private SgBReceiveItemMapper receiveItemMapper;

    @Autowired
    private SgReceiveStorageService storageService;

    @Autowired
    private SgStorageBoxConfig storageBoxConfig;

    @Autowired
    private SgStoreESUtils storeESUtils;

    @Transactional(rollbackFor = Exception.class)
    public ValueHolderV14<SgReceiveBillSaveResult> saveSgBReceive(SgReceiveBillSaveRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("来源单据id[" + request.getSgReceive().getSourceBillId()
                    + "],逻辑收货单保存入参:" + JSONObject.toJSONString(request) + ";");
        }

        ValueHolderV14<SgReceiveBillSaveResult> v14 = new ValueHolderV14<>();
        checkParams(request);

        CusRedisTemplate<String, String> redisTemplate = SgStoreUtils.initRedisTemplate();
        Long sourceBillId = request.getSgReceive().getSourceBillId();
        Integer sourceBillType = request.getSgReceive().getSourceBillType();
        String lockKsy = SgConstants.SG_B_RECEIVE + ":" + sourceBillId + ":" + sourceBillType;

        if (redisTemplate.opsForValue().get(lockKsy) == null) {
            try {
                redisTemplate.opsForValue().set(lockKsy, "OK", 30, TimeUnit.MINUTES);

                List<SgBReceiveItem> orgReceiveItems = Lists.newArrayList();
                HashMap<String, SgBReceiveItem> map = Maps.newHashMap();
                HashMap<String, SgBReceiveImpItem> impItemMap = Maps.newHashMap();
                HashMap<String, SgBReceiveTeusItem> teusItemMap = Maps.newHashMap();

                SgBReceive orgReceive = receiveMapper.selectOne(new QueryWrapper<SgBReceive>().lambda()
                        .eq(SgBReceive::getSourceBillId, request.getSgReceive().getSourceBillId())
                        .eq(SgBReceive::getSourceBillType, request.getSgReceive().getSourceBillType())
                        .eq(SgBReceive::getIsactive, SgConstants.IS_ACTIVE_Y));
                if (orgReceive != null) {
                    orgReceiveItems = receiveItemMapper.selectList(new QueryWrapper<SgBReceiveItem>().lambda()
                            .eq(SgBReceiveItem::getSgBReceiveId, orgReceive.getId()));
                    if (CollectionUtils.isNotEmpty(orgReceiveItems)) {
                        orgReceiveItems.forEach(sgBReceiveItem ->
                                map.put(sgBReceiveItem.getPsCSkuId() + "_" + sgBReceiveItem.getCpCStoreId() + "_" + sgBReceiveItem.getSourceBillItemId(), sgBReceiveItem));
                    }
                    /*TODO 考虑明细可能上万,后期查询会做调整*/
                    if (storageBoxConfig.getBoxEnable()) {
                        //记录录入明细
                        SgBReceiveImpItemMapper impItemMapper = ApplicationContextHandle.getBean(SgBReceiveImpItemMapper.class);
                        List<SgBReceiveImpItem> impItems = impItemMapper.selectList(new QueryWrapper<SgBReceiveImpItem>().lambda()
                                .eq(SgBReceiveImpItem::getSgBReceiveId, orgReceive.getId()));
                        if (CollectionUtils.isNotEmpty(impItems)) {
                            impItems.forEach(o -> impItemMap.put(o.getPsCProId() + "_" + o.getCpCStoreId() + "_" + o.getSourceBillItemId(), o));
                        }
                        //记录箱内明细
                        SgBReceiveTeusItemMapper teusItemMapper = ApplicationContextHandle.getBean(SgBReceiveTeusItemMapper.class);
                        List<SgBReceiveTeusItem> teusItems = teusItemMapper.selectList(new QueryWrapper<SgBReceiveTeusItem>().lambda()
                                .eq(SgBReceiveTeusItem::getSgBReceiveId, orgReceive.getId()));
                        if (CollectionUtils.isNotEmpty(teusItems)) {
                            teusItems.forEach(o -> teusItemMap.put(o.getPsCSkuId() + "_" + o.getCpCStoreId() + "_" + o.getPsCTeusId(), o));
                        }
                    }
                }
                Long objid = saveRecordRPC(request, orgReceive, map, impItemMap, teusItemMap);
                SgBReceive receive = receiveMapper.selectById(objid);
                Map<Long, CpCStore> negativeStock = SgStoreUtils.getNegativeStock(null, request, null, null);
                Map<Long, BigDecimal> skuTeusCounts = CollectionUtils.isNotEmpty(request.getTeusItemList()) ? request.getTeusItemList().stream().collect(Collectors.groupingBy(SgReceiveTeusItemSaveRequest::getPsCSkuId, Collectors.reducing(BigDecimal.ZERO, SgReceiveTeusItemSaveRequest::getQty, BigDecimal::add))) : Maps.newHashMap();

                ValueHolderV14<SgStorageUpdateResult> storageResult = storageService.updateReceiveStoragePrein(receive, null, request, map,
                        null, request.getLoginUser(), false, negativeStock, false, null, skuTeusCounts, impItemMap, null, null);
                if (storageResult.isOK()) {
                    //分销单据 -> check同步库存占用结果
                    SgStoreUtils.isSuccess("逻辑收货单保存失败!", storageResult);
                    v14.setCode(ResultCode.SUCCESS);
                    v14.setMessage("success");
                    SgReceiveBillSaveResult data = new SgReceiveBillSaveResult();
                    data.setPreoutUpdateResult(storageResult.getData().getPreoutUpdateResult());
                    v14.setData(data);
                } else {
                    AssertUtils.logAndThrow("占用失败:" + storageResult.getMessage());
                }

                //推送ES
                storeESUtils.pushESByReceive(objid, true, false, null, receiveMapper, receiveItemMapper);
            } catch (Exception e) {
                e.printStackTrace();
                AssertUtils.logAndThrowExtra(e, request.getLoginUser().getLocale());
            } finally {
                try {
                    redisTemplate.delete(lockKsy);
                } catch (Exception e) {
                    e.printStackTrace();
                    log.error(this.getClass().getName() + lockKsy + "释放锁失败，请联系管理员！");
                    AssertUtils.logAndThrow("释放锁失败，请联系管理员！");
                }
            }
        } else {
            v14.setCode(ResultCode.FAIL);
            v14.setMessage("逻辑收货单正在被操作，请稍后重试！");
        }

        if (log.isDebugEnabled()) {
            log.debug("来源单据id[" + request.getSgReceive().getSourceBillId()
                    + "],逻辑收货单保存返回结果:" + v14.toJSONObject().toJSONString() + ";");
        }
        return v14;
    }

    /**
     * RPC-逻辑收货单保存
     *
     * @param request     request-model
     * @param orgReceive  原逻辑收货单
     * @param map         原逻辑收货单明细
     * @param impItemMap  原逻辑收货单录入明细
     * @param teusItemMap 原逻辑收货单箱内明细
     * @return 逻辑收货单id
     */
    private Long saveRecordRPC(SgReceiveBillSaveRequest request, SgBReceive orgReceive, HashMap<String, SgBReceiveItem> map, HashMap<String, SgBReceiveImpItem> impItemMap, HashMap<String, SgBReceiveTeusItem> teusItemMap) {
        User user = request.getLoginUser();
        Long issertId = null;
        try {
            //全量true、增量false
            Boolean isAll = request.getUpdateMethod().intValue() == SgConstantsIF.ITEM_UPDATE_TYPE_ALL.intValue();
            issertId = orgReceive == null ? ModelUtil.getSequence(SgReceiveConstants.BILL_RECEIVE) : orgReceive.getId();

            //保存-逻辑收货单主表
            saveMainRecord(request, orgReceive, issertId, isAll, user);

            //保存条码明细
            batchSaveItems(request, map, issertId, isAll, user);

            //保存录入/箱内明细
            if (storageBoxConfig.getBoxEnable()) {
                batchSaveImpItems(request, impItemMap, issertId, isAll, user);
                batchSaveTeusItems(request, teusItemMap, issertId, isAll, user);
            }

        } catch (Exception e) {
            e.printStackTrace();
            AssertUtils.logAndThrow("保存逻辑收货单异常:" + e.getMessage(), user.getLocale());
        }
        return issertId;
    }

    private void saveMainRecord(SgReceiveBillSaveRequest request, SgBReceive sgBReceive, Long issertId, Boolean isAll, User user) {
        boolean flag = sgBReceive == null;
        int billStatus = flag ? SgReceiveConstantsIF.BILL_RECEIVE_STATUS_CREATE : SgReceiveConstantsIF.BILL_RECEIVE_STATUS_UPDATE;

        SgBReceive receive = new SgBReceive();
        BeanUtils.copyProperties(request.getSgReceive(), receive);

        //获取单据编号
        String billNo = flag ? SgStoreUtils.getBillNo(SgReceiveConstants.SEQ_RECEIVE,
                SgReceiveConstants.BILL_RECEIVE.toUpperCase(), receive, user.getLocale()) : sgBReceive.getBillNo();
        receive.setId(issertId);
        receive.setBillNo(billNo);
        receive.setBillStatus(billStatus);
        receive.setOwnerename(user.getEname());
        receive.setModifierename(user.getEname());

        BigDecimal totalQtyPrein = request.getItemList().stream().map(SgReceiveItemSaveRequest::getQtyPrein).reduce(BigDecimal.ZERO, (a, b) -> a.add(b));
        BigDecimal totalQtyReceive = request.getItemList().stream().map(SgReceiveItemSaveRequest::getQtyReceive).reduce(BigDecimal.ZERO, (a, b) -> a.add(b));
        //更新且为增量的情况下  累加上总在途数量
        //来源单据类型若是【调拨单】【销售单】【销售退货单】 在途量取 出库数量-入库数量
        if (!flag) {
            totalQtyReceive = null;
            if (!isAll) {
                BigDecimal totQtyPrein = Optional.ofNullable(sgBReceive.getTotQtyPrein()).orElse(BigDecimal.ZERO);
                totalQtyPrein = totalQtyPrein.add(totQtyPrein);
            }
        }
        receive.setTotQtyPrein(totalQtyPrein);
        receive.setTotQtyReceive(totalQtyReceive);

        if (flag) {
            StorageESUtils.setBModelDefalutData(receive, user);
            receiveMapper.insert(receive);
        } else {
            StorageESUtils.setBModelDefalutDataByUpdate(receive, user);
            receiveMapper.updateById(receive);
        }
    }

    private void batchSaveItems(SgReceiveBillSaveRequest request, HashMap<String, SgBReceiveItem> map, Long issertId, Boolean isAll, User user) {
        //保存-逻辑收货单明细
        List<SgBReceiveItem> updateList = Lists.newArrayList();
        List<SgBReceiveItem> insertList = Lists.newArrayList();
        for (SgReceiveItemSaveRequest item : request.getItemList()) {
            String key = item.getPsCSkuId() + "_" + item.getCpCStoreId() + "_" + item.getSourceBillItemId();
            SgBReceiveItem sgBReceiveItem = map.get(key);

            SgBReceiveItem receiveItem = new SgBReceiveItem();
            Long itemId = null == sgBReceiveItem ? ModelUtil.getSequence(SgReceiveConstants.BILL_RECEIVE_ITEM) : sgBReceiveItem.getId();
            item.setId(itemId);
            BeanUtils.copyProperties(item, receiveItem);
            receiveItem.setSgBReceiveId(issertId);
            receiveItem.setOwnerename(user.getEname());
            receiveItem.setModifierename(user.getEname());
            if (null == sgBReceiveItem) {
                StorageESUtils.setBModelDefalutData(receiveItem, user);
                insertList.add(receiveItem);
            } else {
                //历史在途数量
                BigDecimal qtyPrein = Optional.ofNullable(sgBReceiveItem.getQtyPrein()).orElse(BigDecimal.ZERO);
                BigDecimal reqQtyPrein = Optional.ofNullable(item.getQtyPrein()).orElse(BigDecimal.ZERO);
                //全量更新  待更新在途=请求在途数量  增量更新  待更新在途=历史在途+请求在途
                BigDecimal newQtyPrein = isAll ? reqQtyPrein : qtyPrein.add(reqQtyPrein);
                if (newQtyPrein.compareTo(BigDecimal.ZERO) <= 0) {
                    receiveItemMapper.deleteById(sgBReceiveItem.getId());
                } else {
                    receiveItem.setQtyPrein(newQtyPrein);
                    StorageESUtils.setBModelDefalutDataByUpdate(receiveItem, user);
                    updateList.add(receiveItem);
                }
            }
        }

        if (CollectionUtils.isNotEmpty(insertList)) {
            SgStoreUtils.batchInsertTeus(insertList, "逻辑收货单明细", SgConstants.SG_COMMON_INSERT_PAGE_SIZE, receiveItemMapper);
        }
        if (CollectionUtils.isNotEmpty(updateList)) {
            updateList.forEach(sgBReceiveItem -> receiveItemMapper.updateById(sgBReceiveItem));
        }
    }

    private void batchSaveImpItems(SgReceiveBillSaveRequest request, HashMap<String, SgBReceiveImpItem> impItemMap, Long issertId, Boolean isAll, User user) {
        List<SgReceiveImpItemSaveRequest> impItemList = request.getImpItemList();
        if (CollectionUtils.isNotEmpty(impItemList)) {
            SgBReceiveImpItemMapper impItemMapper = ApplicationContextHandle.getBean(SgBReceiveImpItemMapper.class);
            List<SgBReceiveImpItem> insertItems = new ArrayList<>();
            List<SgBReceiveImpItem> updateItems = new ArrayList<>();
            for (SgReceiveImpItemSaveRequest impItemSaveRequest : impItemList) {
                String key = impItemSaveRequest.getPsCProId() + "_" + impItemSaveRequest.getCpCStoreId() + "_" + impItemSaveRequest.getSourceBillItemId();
                SgBReceiveImpItem orgImpItem = impItemMap.get(key);
                Long impId = null == orgImpItem ? ModelUtil.getSequence(SgConstants.SG_B_RECEIVE_IMP_ITEM) : orgImpItem.getId();
                SgBReceiveImpItem impItem = new SgBReceiveImpItem();
                impItemSaveRequest.setId(impId);
                BeanUtils.copyProperties(impItemSaveRequest, impItem);
                impItem.setId(impId);
                impItem.setSgBReceiveId(issertId);
                impItem.setOwnerename(user.getEname());
                impItem.setModifierename(user.getEname());
                if (ObjectUtils.isEmpty(orgImpItem)) {
                    StorageESUtils.setBModelDefalutData(impItem, user);
                    insertItems.add(impItem);
                } else {
                    BigDecimal qtyPrein = Optional.ofNullable(orgImpItem.getQtyPrein()).orElse(BigDecimal.ZERO);
                    BigDecimal reqQtyPrein = Optional.ofNullable(impItem.getQtyPrein()).orElse(BigDecimal.ZERO);
                    BigDecimal newQtyPrein = isAll ? reqQtyPrein : qtyPrein.add(reqQtyPrein);
                    if (newQtyPrein.compareTo(BigDecimal.ZERO) <= 0) {
                        impItemMapper.deleteById(orgImpItem.getId());
                    } else {
                        impItem.setQtyPrein(newQtyPrein);
                        StorageESUtils.setBModelDefalutDataByUpdate(impItem, user);
                        updateItems.add(impItem);
                    }
                }
            }

            if (CollectionUtils.isNotEmpty(insertItems)) {
                SgStoreUtils.batchInsertTeus(insertItems, "逻辑收货单录入明细", SgConstants.SG_COMMON_ITEM_INSERT_PAGE_SIZE, impItemMapper);
            }
            if (CollectionUtils.isNotEmpty(updateItems)) {
                updateItems.forEach(o -> impItemMapper.updateById(o));
            }
        }
    }

    private void batchSaveTeusItems(SgReceiveBillSaveRequest request, HashMap<String, SgBReceiveTeusItem> teusItemMap, Long issertId, Boolean isAll, User user) {
        List<SgReceiveTeusItemSaveRequest> teusItemList = request.getTeusItemList();
        if (CollectionUtils.isNotEmpty(teusItemList)) {
            SgBReceiveTeusItemMapper teusItemMapper = ApplicationContextHandle.getBean(SgBReceiveTeusItemMapper.class);
            List<SgBReceiveTeusItem> insertItems = new ArrayList<>();
            List<SgBReceiveTeusItem> updateItems = new ArrayList<>();
            for (SgReceiveTeusItemSaveRequest teusItemSaveRequest : teusItemList) {
                String key = teusItemSaveRequest.getPsCSkuId() + "_" + teusItemSaveRequest.getCpCStoreId() + "_" + teusItemSaveRequest.getPsCTeusId();
                SgBReceiveTeusItem orgTeusItem = teusItemMap.get(key);
                Long teusId = null == orgTeusItem ? ModelUtil.getSequence(SgConstants.SG_B_RECEIVE_TEUS_ITEM) : orgTeusItem.getId();
                SgBReceiveTeusItem teusItem = new SgBReceiveTeusItem();
                teusItemSaveRequest.setId(teusId);
                BeanUtils.copyProperties(teusItemSaveRequest, teusItem);
                teusItem.setSgBReceiveId(issertId);
                teusItem.setOwnerename(user.getEname());
                teusItem.setModifierename(user.getEname());
                if (ObjectUtils.isEmpty(orgTeusItem)) {
                    StorageESUtils.setBModelDefalutData(teusItem, user);
                    insertItems.add(teusItem);
                } else {
                    BigDecimal qty = Optional.ofNullable(orgTeusItem.getQty()).orElse(BigDecimal.ZERO);
                    BigDecimal reqQty = Optional.ofNullable(teusItem.getQty()).orElse(BigDecimal.ZERO);
                    BigDecimal newQty = isAll ? reqQty : qty.add(reqQty);
                    teusItem.setQty(newQty);
                    StorageESUtils.setBModelDefalutDataByUpdate(teusItem, user);
                    updateItems.add(teusItem);
                }
            }

            if (CollectionUtils.isNotEmpty(insertItems)) {
                SgStoreUtils.batchInsertTeus(insertItems, "逻辑收货单箱内明细", SgConstants.SG_COMMON_ITEM_INSERT_PAGE_SIZE, teusItemMapper);
            }
            if (CollectionUtils.isNotEmpty(updateItems)) {
                updateItems.forEach(o -> teusItemMapper.updateById(o));
            }
        }
    }

    /**
     * 参数校验
     *
     * @param request request-model
     */
    public void checkParams(SgReceiveBillSaveRequest request) {
        Long id = request.getObjId();
        SgStoreUtils.checkR3BModelDefalut(request);
        AssertUtils.notNull(request.getSgReceive(), "主表信息不能为空！");
        if (storageBoxConfig.getBoxEnable()) {
            SgStoreUtils.impSgReceiveRequestConvertMethod(request);
        }
        SgStoreUtils.checkBModelDefalut(request.getItemList());
        if (null == id) {
            SgStoreUtils.checkBModelDefalut(request.getSgReceive(), false);
        }
    }
}
