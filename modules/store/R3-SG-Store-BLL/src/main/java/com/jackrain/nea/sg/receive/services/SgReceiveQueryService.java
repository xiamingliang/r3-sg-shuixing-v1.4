package com.jackrain.nea.sg.receive.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Maps;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.receive.mapper.SgBReceiveItemMapper;
import com.jackrain.nea.sg.receive.mapper.SgBReceiveMapper;
import com.jackrain.nea.sg.receive.model.result.SgReceiveBillQueryResult;
import com.jackrain.nea.sg.receive.model.table.SgBReceive;
import com.jackrain.nea.sg.receive.model.table.SgBReceiveItem;
import com.jackrain.nea.sg.send.model.request.SgStoreBillBase;
import com.jackrain.nea.sg.send.utils.SgStoreUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;

/**
 * @author 舒威
 * @since 2019/7/17
 * create at : 2019/7/17 14:49
 */
@Slf4j
@Component
public class SgReceiveQueryService {

    @Autowired
    private SgBReceiveMapper receiveMapper;

    @Autowired
    private SgBReceiveItemMapper receiveItemMapper;

    public ValueHolderV14<SgReceiveBillQueryResult> querySgBReceive(List<SgStoreBillBase> searchs) {
        if (log.isDebugEnabled()) {
            log.debug("批量查询逻辑收货单服务入参:" + JSONObject.toJSONString(searchs) + ";");
        }

        for (SgStoreBillBase search : searchs) {
            SgStoreUtils.checkBModelDefalut(search, false);
        }

        ValueHolderV14<SgReceiveBillQueryResult> v14 = new ValueHolderV14<>();
        HashMap<String, HashMap<SgBReceive, List<SgBReceiveItem>>> results = Maps.newHashMap();

        for (SgStoreBillBase search : searchs) {
            Long sourceBillId = search.getSourceBillId();
            Integer sourceBillType = search.getSourceBillType();
            String key = sourceBillId + "," + sourceBillType;
            HashMap<SgBReceive, List<SgBReceiveItem>> map=Maps.newHashMap();

            SgBReceive receive = receiveMapper.selectOne(new QueryWrapper<SgBReceive>().lambda()
                    .eq(SgBReceive::getSourceBillId, sourceBillId)
                    .eq(SgBReceive::getSourceBillType, sourceBillType)
                    .eq(SgBReceive::getIsactive, SgConstants.IS_ACTIVE_Y));
            if (receive != null) {
                List<SgBReceiveItem> items = receiveItemMapper.selectList(new QueryWrapper<SgBReceiveItem>().lambda()
                        .eq(SgBReceiveItem::getSgBReceiveId, receive.getId()));
                if (CollectionUtils.isNotEmpty(items)) {
                    map.put(receive,items);
                    results.put(key,map);
                }
            }
        }

        v14.setCode(ResultCode.SUCCESS);
        v14.setMessage("success");
        SgReceiveBillQueryResult data=new SgReceiveBillQueryResult();
        data.setResults(results);
        v14.setData(data);
        return v14;
    }
}
