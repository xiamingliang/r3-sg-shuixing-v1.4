package com.jackrain.nea.sg.send.services;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.model.util.ModelUtil;
import com.jackrain.nea.redis.config.CusRedisTemplate;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.config.SgStorageBoxConfig;
import com.jackrain.nea.sg.basic.model.result.SgStorageOutStockResult;
import com.jackrain.nea.sg.basic.model.result.SgStorageUpdateResult;
import com.jackrain.nea.sg.basic.model.table.CpCStore;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.send.common.SgSendConstants;
import com.jackrain.nea.sg.send.common.SgSendConstantsIF;
import com.jackrain.nea.sg.send.mapper.SgBSendImpItemMapper;
import com.jackrain.nea.sg.send.mapper.SgBSendItemMapper;
import com.jackrain.nea.sg.send.mapper.SgBSendMapper;
import com.jackrain.nea.sg.send.mapper.SgBSendTeusItemMapper;
import com.jackrain.nea.sg.send.model.request.SgSendBillSaveRequest;
import com.jackrain.nea.sg.send.model.request.SgSendImpItemSaveRequest;
import com.jackrain.nea.sg.send.model.request.SgSendItemSaveRequest;
import com.jackrain.nea.sg.send.model.request.SgSendTeusItemSaveRequest;
import com.jackrain.nea.sg.send.model.result.SgSendBillSaveResult;
import com.jackrain.nea.sg.send.model.table.SgBSend;
import com.jackrain.nea.sg.send.model.table.SgBSendImpItem;
import com.jackrain.nea.sg.send.model.table.SgBSendItem;
import com.jackrain.nea.sg.send.model.table.SgBSendTeusItem;
import com.jackrain.nea.sg.send.utils.SgStoreESUtils;
import com.jackrain.nea.sg.send.utils.SgStoreUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;


/**
 * @author 舒威
 * @since 2019/4/19
 * create at : 2019/4/19 14:30
 */
@Slf4j
@Component
public class SgSendSaveService {

    @Autowired
    private SgBSendMapper sendMapper;

    @Autowired
    private SgBSendItemMapper sendItemMapper;

    @Autowired
    private SgSendStorageService storageService;

    @Autowired
    private SgStorageBoxConfig storageBoxConfig;

    @Autowired
    private SgStoreESUtils storeESUtils;

    public ValueHolderV14<SgSendBillSaveResult> saveSgBSend(SgSendBillSaveRequest request) {
        ValueHolderV14<SgSendBillSaveResult> v14 = new ValueHolderV14<>();
        long start = System.currentTimeMillis();
        try {
            SgSendSaveService sgSendSaveService = ApplicationContextHandle.getBean(SgSendSaveService.class);
            v14 = sgSendSaveService.saveSgBSendWithTrans(request);
            if (log.isDebugEnabled()) {
                log.debug("来源单据id[" + request.getSgSend().getSourceBillId()
                        + "],逻辑发货单保存出参:{},耗时:{};", v14.toJSONObject().toJSONString(), (System.currentTimeMillis() - start));
            }
        } catch (Exception e) {
            e.printStackTrace();
            AssertUtils.logAndThrowExtra("逻辑发货单新增或更新异常", e);
        }
        return v14;
    }

    @Transactional(rollbackFor = Exception.class)
    public ValueHolderV14<SgSendBillSaveResult> saveSgBSendWithTrans(SgSendBillSaveRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("来源单据id[" + request.getSgSend().getSourceBillId()
                    + "],逻辑发货单保存入参:" + JSONObject.toJSONString(request) + ";");
        }

        ValueHolderV14<SgSendBillSaveResult> v14 = new ValueHolderV14<>();
        v14.setCode(ResultCode.SUCCESS);
        v14.setMessage("success");

        checkParams(request);

        CusRedisTemplate<String, String> redisTemplate = SgStoreUtils.initRedisTemplate();
        Long sourceBillId = request.getSgSend().getSourceBillId();
        String sourceBillNo = request.getSgSend().getSourceBillNo();
        Integer sourceBillType = request.getSgSend().getSourceBillType();
        User loginUser = request.getLoginUser();
        String lockKsy = SgConstants.SG_B_SEND + ":" + sourceBillId + ":" + sourceBillType;

        //加锁校验
        Boolean ifAbsent = redisTemplate.opsForValue().setIfAbsent(lockKsy, "OK");
        if (ifAbsent != null && ifAbsent) {
            redisTemplate.expire(lockKsy, 30, TimeUnit.MINUTES);
        } else {
            v14.setCode(ResultCode.FAIL);
            v14.setMessage("逻辑发货单正在被操作，请稍后重试！");
            return v14;
        }

        try {
            //记录原逻辑发货单明细(全量更新库存逆向冲单用-分销 调拨销售多次出入库 更新用)
            HashMap<String, SgBSendItem> map = Maps.newHashMap();
            HashMap<String, SgBSendImpItem> impItemMap = Maps.newHashMap();
            HashMap<String, SgBSendTeusItem> teusItemMap = Maps.newHashMap();

            //check是否存在原单
            SgBSend orgSend = checkExists(sourceBillId, sourceBillNo, sourceBillType, map, impItemMap, teusItemMap);

            //保存逻辑发货单
            long saveStartTime = System.currentTimeMillis();
            Long objid = saveRecordRPC(request, orgSend, map, impItemMap, teusItemMap);
            if (log.isDebugEnabled()) {
                log.debug("来源单据id[" + sourceBillId + "],新增逻辑发货单耗时:{}ms;", System.currentTimeMillis() - saveStartTime);
            }

            SgBSend send = sendMapper.selectById(objid);

            //获取负库存控制信息
            Map<Long, CpCStore> negativeStock = SgStoreUtils.getNegativeStock(request, null, null, null);

            //获取箱内明细信息
            Map<Long, BigDecimal> skuTeusCounts = CollectionUtils.isNotEmpty(request.getTeusItemList()) ?
                    request.getTeusItemList().stream().collect(
                            Collectors.groupingBy(SgSendTeusItemSaveRequest::getPsCSkuId,
                                    Collectors.reducing(BigDecimal.ZERO, SgSendTeusItemSaveRequest::getQty, BigDecimal::add))) : Maps.newHashMap();

            //更新库存
            //分销单据-全量，零售发货单-增量（寻源后增量）
            ValueHolderV14<SgStorageUpdateResult> storageResult = storageService.updateSendStoragePreout(send, null, request, map,
                    null, request.getLoginUser(), false, negativeStock, false, false, null,
                    skuTeusCounts, impItemMap, null, null, Maps.newHashMap());

            updateStorageAfter(objid, storageResult, v14, loginUser);

            //推送ES
            storeESUtils.pushESBySend(objid, true, false, null, sendMapper, sendItemMapper);
        } catch (Exception e) {
            e.printStackTrace();
            AssertUtils.logAndThrowExtra(e, request.getLoginUser().getLocale());
        } finally {
            try {
                redisTemplate.delete(lockKsy);
            } catch (Exception e) {
                e.printStackTrace();
                log.error(this.getClass().getName() + lockKsy + "释放锁失败，请联系管理员！");
                AssertUtils.logAndThrow("释放锁失败，请联系管理员！");
            }
        }
        return v14;
    }

    private void updateStorageAfter(Long objid, ValueHolderV14<SgStorageUpdateResult> storageResult,
                                    ValueHolderV14<SgSendBillSaveResult> v14, User loginUser) {
        if (storageResult.isOK()) {
            SgStorageUpdateResult resultData = storageResult.getData();
            List<SgStorageOutStockResult> outStockItemList = resultData.getOutStockItemList();
            int preoutUpdateResult = resultData.getPreoutUpdateResult();
            //check同步库存占用结果
            SgStoreUtils.isSuccess("逻辑发货单保存失败!", storageResult);
            if (CollectionUtils.isNotEmpty(outStockItemList)) {
                preoutUpdateResult = SgConstantsIF.PREOUT_RESULT_OUT_STOCK;
                //零售发货单 -> 缺货，将逻辑发货单占用更新为0
                try {
                    for (SgStorageOutStockResult outStockResult : outStockItemList) {
                        Long billItemId = outStockResult.getBillItemId();
                        SgBSendItem sendItem = new SgBSendItem();
                        sendItem.setId(billItemId);
                        sendItem.setQtyPreout(BigDecimal.ZERO);
                        StorageESUtils.setBModelDefalutDataByUpdate(sendItem, loginUser);
                        sendItemMapper.updateById(sendItem);
                    }
                    List<SgBSendItem> sendItems = sendItemMapper.selectList(new QueryWrapper<SgBSendItem>()
                            .eq(SgSendConstants.BILL_SEND_ID, objid));
                    BigDecimal reduce = sendItems.stream().map(SgBSendItem::getQtyPreout).reduce(BigDecimal.ZERO, (a, b) -> a.add(b));
                    reduce = reduce == null ? BigDecimal.ZERO : reduce;
                    SgBSend updateSend = new SgBSend();
                    updateSend.setId(objid);
                    updateSend.setTotQtyPreout(reduce);
                    int count = sendMapper.updateById(updateSend);
                    if (count > 1) {
                        if (log.isDebugEnabled()) {
                            log.debug("逻辑发货单" + objid + "同步占用失败，更新总占用数量成功");
                        }
                    }
                } catch (Exception e) {
                    AssertUtils.logAndThrowExtra("同步库存缺货更新逻辑发货单占用异常", e);
                }
            }
            SgSendBillSaveResult data = new SgSendBillSaveResult();
            data.setPreoutUpdateResult(preoutUpdateResult);
            data.setUpdateResult(resultData);
            v14.setData(data);
            v14.setMessage(storageResult.getMessage());
        } else {
            AssertUtils.logAndThrow("占用失败:" + storageResult.getMessage());
        }
    }

    public SgBSend checkExists(Long sourceBillId, String sourceBillNo, Integer sourceBillType, HashMap<String, SgBSendItem> map,
                               HashMap<String, SgBSendImpItem> impItemMap, HashMap<String, SgBSendTeusItem> teusItemMap) {
        SgBSend orgSend = sendMapper.selectOne(new QueryWrapper<SgBSend>().lambda()
                .eq(SgBSend::getSourceBillId, sourceBillId)
                .eq(SgBSend::getSourceBillNo, sourceBillNo)
                .eq(SgBSend::getSourceBillType, sourceBillType)
                .eq(SgBSend::getIsactive, SgConstants.IS_ACTIVE_Y));
        if (orgSend != null) {
            //记录原条码明细
            List<SgBSendItem> orgSendItems = sendItemMapper.selectList(new QueryWrapper<SgBSendItem>().lambda()
                    .eq(SgBSendItem::getSgBSendId, orgSend.getId()));
            if (CollectionUtils.isNotEmpty(orgSendItems)) {
                orgSendItems.forEach(o -> map.put(o.getPsCSkuId() + "_" + o.getCpCStoreId() + "_" + o.getSourceBillItemId(), o));
            }
            /*TODO 考虑明细可能上万,后期查询会做调整*/
            if (storageBoxConfig.getBoxEnable()) {
                //记录录入明细
                SgBSendImpItemMapper impItemMapper = ApplicationContextHandle.getBean(SgBSendImpItemMapper.class);
                List<SgBSendImpItem> impItems = impItemMapper.selectList(new QueryWrapper<SgBSendImpItem>().lambda()
                        .eq(SgBSendImpItem::getSgBSendId, orgSend.getId()));
                if (CollectionUtils.isNotEmpty(impItems)) {
                    impItems.forEach(o -> impItemMap.put(o.getPsCProId() + "_" + o.getCpCStoreId() + "_" + o.getSourceBillItemId(), o));
                }
                //记录箱内明细
                SgBSendTeusItemMapper teusItemMapper = ApplicationContextHandle.getBean(SgBSendTeusItemMapper.class);
                List<SgBSendTeusItem> teusItems = teusItemMapper.selectList(new QueryWrapper<SgBSendTeusItem>().lambda().eq(SgBSendTeusItem::getSgBSendId, orgSend.getId()));
                if (CollectionUtils.isNotEmpty(teusItems)) {
                    teusItems.forEach(o -> teusItemMap.put(o.getPsCSkuId() + "_" + o.getCpCStoreId() + "_" + o.getPsCTeusId(), o));
                }
            }
        }
        return orgSend;
    }

    /**
     * RPC-逻辑发货单保存
     *
     * @param request     request-model
     * @param orgSend     原逻辑发货单
     * @param map         原逻辑发货单明细
     * @param impItemMap  原逻辑发货单录入明细
     * @param teusItemMap 原逻辑发货单箱内明细
     * @return 逻辑发货单id
     */
    public Long saveRecordRPC(SgSendBillSaveRequest request, SgBSend orgSend, HashMap<String, SgBSendItem> map, HashMap<String, SgBSendImpItem> impItemMap, HashMap<String, SgBSendTeusItem> teusItemMap) {
        User user = request.getLoginUser();
        Long issertId = null;
        try {
            //全量true、增量false
            Boolean isAll = request.getUpdateMethod().intValue() == SgConstantsIF.ITEM_UPDATE_TYPE_ALL.intValue();
            issertId = orgSend == null ? ModelUtil.getSequence(SgSendConstants.BILL_SEND) : orgSend.getId();

            //保存-逻辑发货单主表
            saveMainRecord(request, orgSend, issertId, isAll, user);

            //保存条码明细
            batchSaveItems(request, map, issertId, isAll, user);

            //保存录入/箱内明细
            if (storageBoxConfig.getBoxEnable()) {
                batchSaveImpItems(request, impItemMap, issertId, isAll, user);
                batchSaveTeusItems(request, teusItemMap, issertId, isAll, user);
            }

        } catch (Exception e) {
            e.printStackTrace();
            AssertUtils.logAndThrow("保存逻辑发货单异常:" + e.getMessage(), user.getLocale());
        }
        return issertId;
    }

    public void saveMainRecord(SgSendBillSaveRequest request, SgBSend sgBSend, Long issertId, Boolean isAll, User user) {
        boolean flag = sgBSend == null;
        int billStatus = flag ? SgSendConstantsIF.BILL_SEND_STATUS_CREATE : SgSendConstantsIF.BILL_SEND_STATUS_UPDATE;

        SgBSend send = new SgBSend();
        BeanUtils.copyProperties(request.getSgSend(), send);

        //获取单据编号
        String billNo = flag ? SgStoreUtils.getBillNo(SgSendConstants.SEQ_SEND.toUpperCase(),
                SgSendConstants.BILL_SEND.toUpperCase(), send, user.getLocale()) : sgBSend.getBillNo();
        send.setId(issertId);
        send.setBillNo(billNo);
        send.setBillStatus(billStatus);
        send.setOwnerename(user.getEname());
        send.setModifierename(user.getEname());

        List<SgSendItemSaveRequest> requestItemList = request.getItemList();
        BigDecimal totalQtyPreout = requestItemList.stream()
                .map(SgSendItemSaveRequest::getQtyPreout).reduce(BigDecimal.ZERO, (a, b) -> a.add(b));
        BigDecimal totalQtySend = requestItemList.stream()
                .map(SgSendItemSaveRequest::getQtySend).reduce(BigDecimal.ZERO, (a, b) -> a.add(b));
        //更新且为增量的情况下  累加上总占用数量
        if (!flag && !isAll) {
            BigDecimal totQtyPreout = Optional.ofNullable(sgBSend.getTotQtyPreout()).orElse(BigDecimal.ZERO);
            totalQtyPreout = totalQtyPreout.add(totQtyPreout);
        }
        send.setTotQtyPreout(totalQtyPreout);
        send.setTotQtySend(totalQtySend);

        if (flag) {
            StorageESUtils.setBModelDefalutData(send, user);
            sendMapper.insert(send);
        } else {
            StorageESUtils.setBModelDefalutDataByUpdate(send, user);
            sendMapper.updateById(send);
        }
    }

    public void batchSaveItems(SgSendBillSaveRequest request, HashMap<String, SgBSendItem> map, Long issertId, Boolean isAll, User user) {
        //保存-逻辑发货单明细
        List<SgBSendItem> updateList = Lists.newArrayList();
        List<SgBSendItem> insertList = Lists.newArrayList();
        for (SgSendItemSaveRequest item : request.getItemList()) {
            String key = item.getPsCSkuId() + "_" + item.getCpCStoreId() + "_" + item.getSourceBillItemId();
            SgBSendItem sgBSendItem = map.get(key);

            SgBSendItem sendItem = new SgBSendItem();
            Long itemId = null == sgBSendItem ? ModelUtil.getSequence(SgSendConstants.BILL_SEND_ITEM) : sgBSendItem.getId();
            item.setId(itemId);
            BeanUtils.copyProperties(item, sendItem);
            sendItem.setSgBSendId(issertId);
            sendItem.setOwnerename(user.getEname());
            sendItem.setModifierename(user.getEname());
            if (null == sgBSendItem) {
                StorageESUtils.setBModelDefalutData(sendItem, user);
                insertList.add(sendItem);
            } else {
                BigDecimal qtyPreout = Optional.ofNullable(sgBSendItem.getQtyPreout()).orElse(BigDecimal.ZERO);
                BigDecimal reqQtyPreout = Optional.ofNullable(item.getQtyPreout()).orElse(BigDecimal.ZERO);
                BigDecimal newQtyPreout = isAll ? reqQtyPreout : qtyPreout.add(reqQtyPreout);
                if (newQtyPreout.compareTo(BigDecimal.ZERO) <= 0) {
                    sendItemMapper.deleteById(sgBSendItem.getId());
                } else {
                    sendItem.setQtyPreout(newQtyPreout);
                    StorageESUtils.setBModelDefalutDataByUpdate(sendItem, user);
                    updateList.add(sendItem);
                }
            }
        }

        if (CollectionUtils.isNotEmpty(insertList)) {
            SgStoreUtils.batchInsertTeus(insertList, "逻辑发货单明细", SgConstants.SG_COMMON_INSERT_PAGE_SIZE, sendItemMapper);
        }
        if (CollectionUtils.isNotEmpty(updateList)) {
            updateList.forEach(sgBSendItem -> sendItemMapper.updateById(sgBSendItem));
        }

    }

    public void batchSaveImpItems(SgSendBillSaveRequest request, HashMap<String, SgBSendImpItem> impItemMap, Long issertId, Boolean isAll, User user) {
        List<SgSendImpItemSaveRequest> impItemList = request.getImpItemList();
        if (CollectionUtils.isNotEmpty(impItemList)) {
            SgBSendImpItemMapper impItemMapper = ApplicationContextHandle.getBean(SgBSendImpItemMapper.class);
            List<SgBSendImpItem> insertItems = new ArrayList<>();
            List<SgBSendImpItem> updateItems = new ArrayList<>();
            for (SgSendImpItemSaveRequest impItemSaveRequest : impItemList) {
                String key = impItemSaveRequest.getPsCProId() + "_" + impItemSaveRequest.getCpCStoreId() + "_" + impItemSaveRequest.getSourceBillItemId();
                SgBSendImpItem orgImpItem = impItemMap.get(key);
                Long impId = null == orgImpItem ? ModelUtil.getSequence(SgConstants.SG_B_SEND_IMP_ITEM) : orgImpItem.getId();
                SgBSendImpItem impItem = new SgBSendImpItem();
                impItemSaveRequest.setId(impId);
                BeanUtils.copyProperties(impItemSaveRequest, impItem);
                impItem.setId(impId);
                impItem.setSgBSendId(issertId);
                impItem.setOwnerename(user.getEname());
                impItem.setModifierename(user.getEname());
                if (ObjectUtils.isEmpty(orgImpItem)) {
                    StorageESUtils.setBModelDefalutData(impItem, user);
                    insertItems.add(impItem);
                } else {
                    BigDecimal qtyPreout = Optional.ofNullable(orgImpItem.getQtyPreout()).orElse(BigDecimal.ZERO);
                    BigDecimal reqQtyPreout = Optional.ofNullable(impItem.getQtyPreout()).orElse(BigDecimal.ZERO);
                    BigDecimal newQtyPreout = isAll ? reqQtyPreout : qtyPreout.add(reqQtyPreout);
                    if (newQtyPreout.compareTo(BigDecimal.ZERO) <= 0) {
                        impItemMapper.deleteById(orgImpItem.getId());
                    } else {
                        impItem.setQtyPreout(newQtyPreout);
                        StorageESUtils.setBModelDefalutDataByUpdate(impItem, user);
                        updateItems.add(impItem);
                    }
                }
            }

            if (CollectionUtils.isNotEmpty(insertItems)) {
                SgStoreUtils.batchInsertTeus(insertItems, "逻辑发货单录入明细", SgConstants.SG_COMMON_INSERT_PAGE_SIZE, impItemMapper);
            }
            if (CollectionUtils.isNotEmpty(updateItems)) {
                updateItems.forEach(o -> impItemMapper.updateById(o));
            }
        }
    }

    public void batchSaveTeusItems(SgSendBillSaveRequest request, HashMap<String, SgBSendTeusItem> teusItemMap, Long issertId, Boolean isAll, User user) {
        List<SgSendTeusItemSaveRequest> teusItemList = request.getTeusItemList();
        if (CollectionUtils.isNotEmpty(teusItemList)) {
            SgBSendTeusItemMapper teusItemMapper = ApplicationContextHandle.getBean(SgBSendTeusItemMapper.class);
            List<SgBSendTeusItem> insertItems = new ArrayList<>();
            List<SgBSendTeusItem> updateItems = new ArrayList<>();
            for (SgSendTeusItemSaveRequest teusItemSaveRequest : teusItemList) {
                String key = teusItemSaveRequest.getPsCSkuId() + "_" + teusItemSaveRequest.getCpCStoreId() + "_" + teusItemSaveRequest.getPsCTeusId();
                SgBSendTeusItem orgTeusItem = teusItemMap.get(key);
                Long teusId = null == orgTeusItem ? ModelUtil.getSequence(SgConstants.SG_B_SEND_TEUS_ITEM) : orgTeusItem.getId();
                SgBSendTeusItem teusItem = new SgBSendTeusItem();
                teusItemSaveRequest.setId(teusId);
                BeanUtils.copyProperties(teusItemSaveRequest, teusItem);
                teusItem.setSgBSendId(issertId);
                teusItem.setOwnerename(user.getEname());
                teusItem.setModifierename(user.getEname());
                if (ObjectUtils.isEmpty(orgTeusItem)) {
                    StorageESUtils.setBModelDefalutData(teusItem, user);
                    insertItems.add(teusItem);
                } else {
                    BigDecimal qty = Optional.ofNullable(orgTeusItem.getQty()).orElse(BigDecimal.ZERO);
                    BigDecimal reqQty = Optional.ofNullable(teusItem.getQty()).orElse(BigDecimal.ZERO);
                    BigDecimal newQty = isAll ? reqQty : qty.add(reqQty);
                    teusItem.setQty(newQty);
                    StorageESUtils.setBModelDefalutDataByUpdate(teusItem, user);
                    updateItems.add(teusItem);
                }
            }

            if (CollectionUtils.isNotEmpty(insertItems)) {
                SgStoreUtils.batchInsertTeus(insertItems, "逻辑发货单箱内明细", SgConstants.SG_COMMON_INSERT_PAGE_SIZE, teusItemMapper);
            }
            if (CollectionUtils.isNotEmpty(updateItems)) {
                updateItems.forEach(o -> teusItemMapper.updateById(o));
            }
        }
    }

    /**
     * 参数校验
     *
     * @param request request-model
     * @return teusProMap 商品编码-箱ID/ECODE
     */
    public void checkParams(SgSendBillSaveRequest request) {
        Long id = request.getObjId();
        SgStoreUtils.checkR3BModelDefalut(request);
        AssertUtils.notNull(request.getSgSend(), "主表信息不能为空！");
        if (storageBoxConfig.getBoxEnable()) {
            SgStoreUtils.impRequestConvertMethod(request);
        }
        SgStoreUtils.checkBModelDefalut(request.getItemList());
        if (null == id) {
            SgStoreUtils.checkBModelDefalut(request.getSgSend(), true);
        }
    }
}
