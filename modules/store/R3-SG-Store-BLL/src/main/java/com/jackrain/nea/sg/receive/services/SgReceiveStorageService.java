package com.jackrain.nea.sg.receive.services;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.common.ReferenceUtil;
import com.jackrain.nea.sg.basic.api.SgStorageBillUpdateCmd;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.model.request.*;
import com.jackrain.nea.sg.basic.model.result.SgStorageUpdateResult;
import com.jackrain.nea.sg.basic.model.table.CpCStore;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.receive.model.request.SgReceiveBillSaveRequest;
import com.jackrain.nea.sg.receive.model.table.SgBReceive;
import com.jackrain.nea.sg.receive.model.table.SgBReceiveImpItem;
import com.jackrain.nea.sg.receive.model.table.SgBReceiveItem;
import com.jackrain.nea.sg.send.common.SgSendConstantsIF;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.*;

@Slf4j
@Component
public class SgReceiveStorageService {

    public final static String CLEAN = "clean";

    public final static String VOID = "void";

    public final static String SUBMIT = "submit";

    /**
     * 初始化同步库存占用服务-整单
     */
    public SgStorageBillUpdateCmd initSgStorageBillUpdateCmd() {
        return (SgStorageBillUpdateCmd) ReferenceUtil.refer(
                ApplicationContextHandle.getApplicationContext(),
                SgStorageBillUpdateCmd.class.getName(),
                SgConstantsIF.GROUP, SgConstantsIF.VERSION);
    }


    /**
     * 库存更新-逻辑收货单
     *  @param receive         逻辑收货单id
     * @param bReceiveItems   逻辑收货单有占用变化明细
     * @param request         request-model
     * @param map             原逻辑收货单明细 k-v   <skuid+逻辑仓id , 明细>
     * @param action          动作 - 提交、作废用
     * @param loginUser       登录用户
     * @param isLast          是否最后一次入库标志
     * @param negativeStock
     * @param isNegativePrein
     * @param skuTeusCounts
     * @param impItemMap      @return 库存在途更新结果
     * @param noticesId
     * @param noticesBillno
     */
    public ValueHolderV14<SgStorageUpdateResult> updateReceiveStoragePrein(SgBReceive receive,
                                                                           List<SgBReceiveItem> bReceiveItems,
                                                                           SgReceiveBillSaveRequest request,
                                                                           HashMap<String, SgBReceiveItem> map,
                                                                           String action,
                                                                           User loginUser,
                                                                           Boolean isLast,
                                                                           Map<Long, CpCStore> negativeStock,
                                                                           Boolean isNegativePrein,
                                                                           List<SgBReceiveImpItem> impList,
                                                                           Map<Long, BigDecimal> skuTeusCounts,
                                                                           HashMap<String, SgBReceiveImpItem> impItemMap, Long noticesId, String noticesBillno) {
        ValueHolderV14<SgStorageUpdateResult> storageResult = null;
        try {
            SgStorageUpdateBillRequest billRequest = new SgStorageUpdateBillRequest();

            //单据信息
            billRequest.setBillId(receive.getId());
            billRequest.setBillNo(receive.getBillNo());
            billRequest.setBillDate(receive.getBillDate());
            billRequest.setBillType(receive.getSourceBillType());
            billRequest.setChangeDate(new Timestamp(System.currentTimeMillis()));
            billRequest.setCpCshopId(receive.getCpCShopId());
            billRequest.setServiceNode(receive.getServiceNode());
            billRequest.setIsCancel(request != null ? request.getIsCancel() : false);
            billRequest.setSourceBillId(receive.getSourceBillId());
            billRequest.setSourceBillNo(receive.getSourceBillNo());
            billRequest.setPhyInNoticesId(noticesId);
            billRequest.setPhyInNoticesNo(noticesBillno);

            //单据明细信息
            List<SgStorageUpdateBillItemRequest> itemList = request != null ?
                    buildReceiveStorageItems(request, map, negativeStock, skuTeusCounts) : buildReceiveStorageItems(bReceiveItems, action, isLast, negativeStock, isNegativePrein, skuTeusCounts);
            billRequest.setItemList(itemList);

            //录入明细信息
            if ((request != null && CollectionUtils.isNotEmpty(request.getImpItemList())) || CollectionUtils.isNotEmpty(impList)) {
                List<SgTeusStorageUpdateBillItemRequest> teusList = request != null ?
                        buildTeusReceiveStorageItems(request, impItemMap, negativeStock) :
                        buildTeusReceiveStorageItems(impList, action, isLast, negativeStock, isNegativePrein);
                billRequest.setTeusList(teusList);
            }

            SgStorageUpdateControlRequest controlModel = new SgStorageUpdateControlRequest();
            controlModel.setPreoutOperateType(request != null ? request.getPreoutWarningType() : SgConstantsIF.PREOUT_RESULT_ERROR);

            SgStorageSingleUpdateRequest updateRequest = new SgStorageSingleUpdateRequest();
            updateRequest.setBill(billRequest);
            String msgKey = SgConstants.MSG_TAG_RECEIVE + ":" + receive.getBillNo();
            if (!VOID.equals(action)) {
                msgKey += System.currentTimeMillis();
            }
            updateRequest.setMessageKey(msgKey); //需统一规范
            updateRequest.setLoginUser(loginUser);
            updateRequest.setControlModel(controlModel);

            SgStorageBillUpdateCmd sgStorageUpdateCmd = initSgStorageBillUpdateCmd();
            if (log.isDebugEnabled()) {
                log.debug("来源单据id[" + receive.getSourceBillId() + "],开始占用库存在途入参:" + JSONObject.toJSONString(updateRequest));
            }
            storageResult = sgStorageUpdateCmd.updateStorageBillWithTrans(updateRequest);
            if (log.isDebugEnabled()) {
                log.debug("来源单据id[" + receive.getSourceBillId() + "],占用库存在途出参:" + storageResult.toJSONObject().toJSONString());
            }
        } catch (Exception e) {
            e.printStackTrace();
            AssertUtils.logAndThrowExtra("同步库存占用接口服务异常", e);
        }
        return storageResult;
    }


    private List<SgStorageUpdateBillItemRequest> buildReceiveStorageItems(SgReceiveBillSaveRequest request,
                                                                          HashMap<String, SgBReceiveItem> map, Map<Long, CpCStore> negativeStock, Map<Long, BigDecimal> skuTeusCounts) {
        List<SgStorageUpdateBillItemRequest> itemList = new ArrayList<>();
        if (SgConstantsIF.ITEM_UPDATE_TYPE_ALL.equals(request.getUpdateMethod())) { //全量
            //逆向冲单明细数据
            request.getItemList().forEach(itemSaveRequest -> {
                if (itemSaveRequest.getIsReceiveStorage()) {
                    //原逻辑发货单明细
                    SgBReceiveItem receiveItem = map.get(itemSaveRequest.getPsCSkuId() + "_" + itemSaveRequest.getCpCStoreId() + "_" + itemSaveRequest.getSourceBillItemId());
                    //原逻辑发货单历史占用为0 或 新明细占用为0  不需要发库存
                    if (receiveItem != null && 0 != BigDecimal.ZERO.compareTo(receiveItem.getQtyPrein())
                            && 0 != BigDecimal.ZERO.compareTo(itemSaveRequest.getQtyPrein())) {
                        SgStorageUpdateBillItemRequest item = new SgStorageUpdateBillItemRequest();
                        BeanUtils.copyProperties(receiveItem, item);
                        item.setBillItemId(receiveItem.getId());
                        //item.setStorageType(SgConstantsIF.STORAGE_TYPE_PREIN);
                        item.setQtyPreinChange(receiveItem.getQtyPrein().negate());
                        if (MapUtils.isNotEmpty(negativeStock)) {
                            SgStorageUpdateControlRequest controlRequest = getStorageUpdateControlRequest(itemSaveRequest.getCpCStoreId(), negativeStock, request.getIsNegativePrein());
                            item.setControlmodel(controlRequest);
                        }
                        //添加箱内明細
                        if (MapUtils.isNotEmpty(skuTeusCounts)) {
                            if (!ObjectUtils.isEmpty(skuTeusCounts.get(itemSaveRequest.getPsCSkuId()))) {
                                item.setQtyPreinTeusChange(skuTeusCounts.get(itemSaveRequest.getPsCSkuId()).negate());
                            }
                        }
                        itemList.add(item);
                    }
                }
            });
        }

        request.getItemList().forEach(itemSaveRequest -> {
            if (itemSaveRequest.getIsReceiveStorage() && 0 != BigDecimal.ZERO.compareTo(itemSaveRequest.getQtyPrein())) {
                SgStorageUpdateBillItemRequest item = new SgStorageUpdateBillItemRequest();
                BeanUtils.copyProperties(itemSaveRequest, item);
                item.setBillItemId(itemSaveRequest.getId());
                //item.setStorageType(SgConstantsIF.STORAGE_TYPE_PREIN);
                item.setQtyPreinChange(itemSaveRequest.getQtyPrein());
                if (MapUtils.isNotEmpty(negativeStock)) {
                    SgStorageUpdateControlRequest controlRequest = getStorageUpdateControlRequest(itemSaveRequest.getCpCStoreId(), negativeStock, request.getIsNegativePrein());
                    item.setControlmodel(controlRequest);
                }
                //添加箱内明細
                if (MapUtils.isNotEmpty(skuTeusCounts)) {
                    if (!ObjectUtils.isEmpty(skuTeusCounts.get(itemSaveRequest.getPsCSkuId()))) {
                        item.setQtyPreinTeusChange(skuTeusCounts.get(itemSaveRequest.getPsCSkuId()));
                    }
                }
                itemList.add(item);
            }
        });

        return itemList;
    }


    private List<SgStorageUpdateBillItemRequest> buildReceiveStorageItems(List<SgBReceiveItem> bReceiveItems,
                                                                          String action, Boolean islast, Map<Long, CpCStore> negativeStock, Boolean isNegativePrein, Map<Long, BigDecimal> skuTeusCounts) {
        List<SgStorageUpdateBillItemRequest> itemList = new ArrayList<>();
        bReceiveItems.forEach(sgBReceiveItem -> {
            BigDecimal preinQtyChange = sgBReceiveItem.getQtyPrein().negate();
            BigDecimal storageQtyChange = sgBReceiveItem.getQtyReceive();
            if ((!SUBMIT.equals(action)) && 0 != BigDecimal.ZERO.compareTo(preinQtyChange)) {
                SgStorageUpdateBillItemRequest prein = new SgStorageUpdateBillItemRequest();
                BeanUtils.copyProperties(sgBReceiveItem, prein);
                prein.setBillItemId(sgBReceiveItem.getId());
                //prein.setStorageType(SgConstantsIF.STORAGE_TYPE_PREIN);
                prein.setQtyPreinChange(preinQtyChange);
                if (MapUtils.isNotEmpty(negativeStock)) {
                    SgStorageUpdateControlRequest controlRequest = getStorageUpdateControlRequest(sgBReceiveItem.getCpCStoreId(), negativeStock, isNegativePrein);
                    prein.setControlmodel(controlRequest);
                }
                //添加箱内明細
                if (MapUtils.isNotEmpty(skuTeusCounts)) {
                    if (!ObjectUtils.isEmpty(skuTeusCounts.get(sgBReceiveItem.getPsCSkuId()))) {
                        prein.setQtyPreinTeusChange(skuTeusCounts.get(sgBReceiveItem.getPsCSkuId()).negate());
                    }
                }
                itemList.add(prein);
            }
            if ((SUBMIT.equals(action) || islast) && (0 != BigDecimal.ZERO.compareTo(preinQtyChange)
                    || 0 != BigDecimal.ZERO.compareTo(storageQtyChange))) {
                SgStorageUpdateBillItemRequest storage = new SgStorageUpdateBillItemRequest();
                BeanUtils.copyProperties(sgBReceiveItem, storage);
                storage.setBillItemId(sgBReceiveItem.getId());
                //storage.setStorageType(SgConstantsIF.STORAGE_TYPE_STORAGE);
                storage.setQtyPreinChange(preinQtyChange);
                storage.setQtyStorageChange(storageQtyChange);
                if (MapUtils.isNotEmpty(negativeStock)) {
                    SgStorageUpdateControlRequest controlRequest = getStorageUpdateControlRequest(sgBReceiveItem.getCpCStoreId(), negativeStock, isNegativePrein);
                    storage.setControlmodel(controlRequest);
                }
                //添加箱内明細
                if (MapUtils.isNotEmpty(skuTeusCounts)) {
                    if (!ObjectUtils.isEmpty(skuTeusCounts.get(sgBReceiveItem.getPsCSkuId()))) {
                        storage.setQtyPreinTeusChange(skuTeusCounts.get(sgBReceiveItem.getPsCSkuId()).negate());
                        storage.setQtyStorageTeusChange(skuTeusCounts.get(sgBReceiveItem.getPsCSkuId()));
                    }
                }
                itemList.add(storage);
            }
        });
        return itemList;
    }

    private List<SgTeusStorageUpdateBillItemRequest> buildTeusReceiveStorageItems(SgReceiveBillSaveRequest request, HashMap<String, SgBReceiveImpItem> map, Map<Long, CpCStore> negativeStock) {
        List<SgTeusStorageUpdateBillItemRequest> itemList = new ArrayList<>();
        if (SgConstantsIF.ITEM_UPDATE_TYPE_ALL.equals(request.getUpdateMethod())) { //全量
            //逆向冲单明细数据
            request.getImpItemList().forEach(itemSaveRequest -> {
                if (itemSaveRequest.getIsReceiveStorage() && itemSaveRequest.getIsTeus().equals(SgSendConstantsIF.IS_TEUS_Y)) {
                    //原录入明细
                    SgBReceiveImpItem impItem = map.get(itemSaveRequest.getPsCProId() + "_" + itemSaveRequest.getCpCStoreId() + "_" + itemSaveRequest.getSourceBillItemId());
                    if (impItem != null && 0 != BigDecimal.ZERO.compareTo(impItem.getQtyPrein().negate())) {
                        SgTeusStorageUpdateBillItemRequest teusStorageitem = new SgTeusStorageUpdateBillItemRequest();
                        BeanUtils.copyProperties(impItem, teusStorageitem);
                        teusStorageitem.setBillItemId(impItem.getId());
                        teusStorageitem.setQtyPreinChange(impItem.getQtyPrein().negate());
                        if (MapUtils.isNotEmpty(negativeStock)) {
                            SgStorageUpdateControlRequest controlRequest = getStorageUpdateControlRequest(impItem.getCpCStoreId(), negativeStock, request.getIsNegativePrein());
                            teusStorageitem.setControlmodel(controlRequest);
                        }
                        itemList.add(teusStorageitem);
                    }
                }
            });
        }

        request.getImpItemList().forEach(itemSaveRequest -> {
            if (itemSaveRequest.getIsReceiveStorage() && itemSaveRequest.getIsTeus().equals(SgSendConstantsIF.IS_TEUS_Y) && 0 != BigDecimal.ZERO.compareTo(itemSaveRequest.getQtyPrein())) {
                SgTeusStorageUpdateBillItemRequest teusStorageitem = new SgTeusStorageUpdateBillItemRequest();
                BeanUtils.copyProperties(itemSaveRequest, teusStorageitem);
                teusStorageitem.setBillItemId(itemSaveRequest.getId());
                teusStorageitem.setQtyPreinChange(itemSaveRequest.getQtyPrein());
                if (MapUtils.isNotEmpty(negativeStock)) {
                    SgStorageUpdateControlRequest controlRequest = getStorageUpdateControlRequest(itemSaveRequest.getCpCStoreId(), negativeStock, request.getIsNegativePrein());
                    teusStorageitem.setControlmodel(controlRequest);
                }
                itemList.add(teusStorageitem);
            }
        });

        return itemList;
    }

    private List<SgTeusStorageUpdateBillItemRequest> buildTeusReceiveStorageItems(List<SgBReceiveImpItem> impItems, String action, Boolean isLast, Map<Long, CpCStore> negativeStock, Boolean isNegativePrein) {
        List<SgTeusStorageUpdateBillItemRequest> itemList = new ArrayList<>();
        impItems.forEach(impItem -> {
            if (impItem.getIsTeus().equals(SgSendConstantsIF.IS_TEUS_Y)) {
                BigDecimal preinQtyChange = impItem.getQtyPrein().negate();
                BigDecimal storageQtyChange = impItem.getQtyReceive();
                if ((!SUBMIT.equals(action)) && 0 != BigDecimal.ZERO.compareTo(preinQtyChange)) {
                    SgTeusStorageUpdateBillItemRequest teusStorageitem = new SgTeusStorageUpdateBillItemRequest();
                    BeanUtils.copyProperties(impItem, teusStorageitem);
                    teusStorageitem.setBillItemId(impItem.getId());
                    teusStorageitem.setQtyPreinChange(preinQtyChange);
                    if (MapUtils.isNotEmpty(negativeStock)) {
                        SgStorageUpdateControlRequest controlRequest = getStorageUpdateControlRequest(impItem.getCpCStoreId(), negativeStock, isNegativePrein);
                        teusStorageitem.setControlmodel(controlRequest);
                    }
                    itemList.add(teusStorageitem);
                }
                if ((SUBMIT.equals(action) || isLast) && (0 != BigDecimal.ZERO.compareTo(preinQtyChange) || 0 != BigDecimal.ZERO.compareTo(storageQtyChange))) {
                    SgTeusStorageUpdateBillItemRequest teusStorageitem = new SgTeusStorageUpdateBillItemRequest();
                    BeanUtils.copyProperties(impItem, teusStorageitem);
                    teusStorageitem.setBillItemId(impItem.getId());
                    teusStorageitem.setQtyPreinChange(preinQtyChange);
                    teusStorageitem.setQtyStorageChange(storageQtyChange);
                    if (MapUtils.isNotEmpty(negativeStock)) {
                        SgStorageUpdateControlRequest controlRequest = getStorageUpdateControlRequest(impItem.getCpCStoreId(), negativeStock, isNegativePrein);
                        teusStorageitem.setControlmodel(controlRequest);
                    }
                    itemList.add(teusStorageitem);
                }
            }
        });

        return itemList;
    }


    /**
     * @param cpCStoreId    逻辑仓id
     * @param negativeStock 逻辑仓Map
     * @param isNegative    是否允许负库存
     * @return controlRequest  库存更新负库存控制request
     */
    public SgStorageUpdateControlRequest getStorageUpdateControlRequest(Long cpCStoreId, Map<Long, CpCStore> negativeStock, Boolean isNegative) {
        SgStorageUpdateControlRequest controlRequest = new SgStorageUpdateControlRequest();
        CpCStore cpCStore = negativeStock.get(cpCStoreId);
        //1 开启负库存控制（不允许负库存）0 允许负库存
        Long isnegative = isNegative ? 0L : Optional.ofNullable(cpCStore.getIsnegative()).orElse(1L);
        controlRequest.setNegativePrein(isnegative == 0);
        controlRequest.setNegativePreout(isnegative == 0);
        controlRequest.setNegativeStorage(isnegative == 0);
        controlRequest.setNegativeAvailable(isnegative == 0);
        return controlRequest;
    }
}
