package com.jackrain.nea.sg.adjust.services;


import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Lists;
import com.jackrain.nea.constants.ResultCode;
import com.jackrain.nea.model.util.ModelUtil;
import com.jackrain.nea.sg.adjust.common.SgAdjustConstants;
import com.jackrain.nea.sg.adjust.mapper.SgBAdjustImpItemMapper;
import com.jackrain.nea.sg.adjust.mapper.SgBAdjustItemMapper;
import com.jackrain.nea.sg.adjust.mapper.SgBAdjustMapper;
import com.jackrain.nea.sg.adjust.model.request.*;
import com.jackrain.nea.sg.adjust.model.table.SgBAdjust;
import com.jackrain.nea.sg.adjust.model.table.SgBAdjustImpItem;
import com.jackrain.nea.sg.adjust.model.table.SgBAdjustItem;
import com.jackrain.nea.sg.basic.common.R3ParamConstants;
import com.jackrain.nea.sg.basic.common.SgConstants;
import com.jackrain.nea.sg.basic.config.SgStorageBoxConfig;
import com.jackrain.nea.sg.basic.model.result.SgR3BaseResult;
import com.jackrain.nea.sg.basic.utils.AssertUtils;
import com.jackrain.nea.sg.basic.utils.R3ParamUtil;
import com.jackrain.nea.sg.basic.utils.StorageESUtils;
import com.jackrain.nea.sg.send.utils.SgStoreESUtils;
import com.jackrain.nea.sg.send.utils.SgStoreUtils;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ApplicationContextHandle;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.face.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author csy
 */

@Slf4j
@Component
public class SgAdjSaveService {

    @Autowired
    private SgBAdjustMapper mapper;

    @Autowired
    private SgBAdjustItemMapper itemMapper;

    @Autowired
    private SgStorageBoxConfig storageBoxConfig;

    @Autowired
    private SgStoreESUtils storeESUtils;

    /**
     * r3框架入参用
     */
    ValueHolder saveSgBAdjustR3(SgAdjustBillSaveRequest request) {
        return R3ParamUtil.convertV14WithResult(saveSgBAdjust(request, true));
    }

    /**
     * @param request request
     * @param isR3    是不是框架（页面） 入参
     */
    @Transactional(rollbackFor = Exception.class)
    public ValueHolderV14<SgR3BaseResult> saveSgBAdjust(SgAdjustBillSaveRequest request, boolean isR3) {
        if (log.isDebugEnabled()) {
            log.debug("Start SgAdjSaveService.saveSgBAdjust. ReceiveParams:SgAdjustBillSaveRequest="
                    + JSONObject.toJSONString(request) + ";");
        }
        //参数校验-主表参数
        checkParams(request, isR3);
        User user = request.getLoginUser();
        Long objId = request.getObjId();
        if (null != objId && objId < 0) {
            return insert(request, user, isR3);
        } else {
            return update(objId, request, user, isR3);
        }
    }

    /**
     * 参数校验
     *
     * @param isR3    是否框架入参
     * @param request 入参
     */
    private void checkParams(SgAdjustBillSaveRequest request, boolean isR3) {
        AssertUtils.notNull(request, "请求参数为空!");
        User user = request.getLoginUser();
        AssertUtils.notNull(user, "用户未登陆");
        Locale locale = user.getLocale();
        if (request.getSgBAdjust() != null && request.getSgBAdjust().getSourceBillId() == null) {
            isR3 = true;
        }
        Long objId = request.getObjId();
        if (null != objId && objId < 0) {
            if (isR3) {
                SgAdjustSaveRequest adjustRequest = request.getSgBAdjust();
                AssertUtils.notNull(adjustRequest, "参数为空!", locale);
                AssertUtils.notNull(objId, "参数为空!-objId", locale);
                AssertUtils.notNull(adjustRequest.getBillType(), "参数为空!-单据类型", locale);
            }
        }
        //接口入参 这些参数 必传*
        if (!isR3) {
            SgAdjustSaveRequest adjustRequest = request.getSgBAdjust();
            AssertUtils.notNull(adjustRequest, "参数为空!", locale);
            AssertUtils.notNull(adjustRequest.getSourceBillId(), "参数为空!-来源单据id", locale);
            AssertUtils.notNull(adjustRequest.getSourceBillType(), "参数为空!-来源单据类型", locale);
        }
    }

    /**
     * 新增-主表
     */
    private ValueHolderV14<SgR3BaseResult> insert(SgAdjustBillSaveRequest request, User user, boolean isR3) {
        ValueHolderV14<SgR3BaseResult> holderV14 = new ValueHolderV14<>();
        //主表对象
        SgAdjustSaveRequest adjSaveRequest = request.getSgBAdjust();

        if (!isR3) {
            int total = mapper.selectCount(new QueryWrapper<SgBAdjust>().lambda()
                    .eq(SgBAdjust::getSourceBillId, adjSaveRequest.getSourceBillId())
                    .eq(SgBAdjust::getSourceBillType, adjSaveRequest.getSourceBillType())
                    .eq(adjSaveRequest.getSgBAdjustPropId() != null, SgBAdjust::getSgBAdjustPropId, adjSaveRequest.getSgBAdjustPropId())
                    .eq(SgBAdjust::getIsactive, SgConstants.IS_ACTIVE_Y));
            AssertUtils.cannot(total > 0, "已存在的单据，不允许重复新增!", user.getLocale());
        }

        //新增主表
        SgBAdjust adjust = new SgBAdjust();
        BeanUtils.copyProperties(adjSaveRequest, adjust);
        Long id = ModelUtil.getSequence(SgConstants.SG_B_ADJUST);
        adjust.setId(id);
        adjust.setBillDate(new Date());
        adjust.setBillStatus(SgAdjustConstants.ADJ_AUDIT_STATUS_N);
        adjust.setOwnerename(user.getEname());
        adjust.setModifierename(user.getEname());
        StorageESUtils.setBModelDefalutData(adjust, user);
        //单据编号
        if (StringUtils.isEmpty(adjust.getBillNo())) {
            adjust.setBillNo(SgStoreUtils.getBillNo(SgAdjustConstants.ADJ_BILL_SEQ, SgConstants.SG_B_ADJUST.toUpperCase(), adjust, user.getLocale()));
        }

        if (storageBoxConfig.getBoxEnable()) {
            //录入明细
            List<SgAdjustImpItemSaveRequest> impItemSaveRequests = request.getImpItems();
            if (CollectionUtils.isNotEmpty(impItemSaveRequests)) {
                List<SgBAdjustImpItem> impItems = Lists.newArrayList();
                SgBAdjustImpItemMapper impItemMapper = ApplicationContextHandle.getBean(SgBAdjustImpItemMapper.class);
                impItemSaveRequests.forEach(item -> impItems.add(createInsertSgAdjustItemImp(item, user, id)));
                SgStoreUtils.batchInsertTeus(impItems, "逻辑调整单录入明细", SgConstants.SG_COMMON_INSERT_PAGE_SIZE, impItemMapper);
            }
        } else {
            //条码明细
            List<SgAdjustItemSaveRequest> items = request.getItems();
            BigDecimal totQty = items.stream().map(SgAdjustItemSaveRequest::getQty).reduce(BigDecimal.ZERO, BigDecimal::add);
            adjust.setTotQty(totQty);

            List<SgBAdjustItem> insertItems = Lists.newArrayList();
            items.forEach(item -> {
                SgBAdjustItem adjustItem = createInsertItem(item, user, id);
                insertItems.add(adjustItem);
            });
            //批量保存逻辑调整单明细
            if (CollectionUtils.isNotEmpty(insertItems)) {
                SgStoreUtils.batchInsertTeus(insertItems, "逻辑调整单明细", SgConstants.SG_COMMON_INSERT_PAGE_SIZE, itemMapper);
            }
        }

        //新增主表
        mapper.insert(adjust);

        //推送ES
        storeESUtils.pushESByAdjust(adjust.getId(), true, false, null, mapper, itemMapper);

        holderV14.setCode(ResultCode.SUCCESS);
        holderV14.setMessage(SgConstants.MESSAGE_STATUS_SUCCESS);
        SgR3BaseResult result = new SgR3BaseResult();
        JSONObject resultData = new JSONObject();
        resultData.put(R3ParamConstants.TABLENAME, SgConstants.SG_B_ADJUST);
        resultData.put(R3ParamConstants.OBJID, id);
        result.setDataJo(resultData);
        holderV14.setData(result);
        return holderV14;
    }

    /**
     * 修改-主表
     *
     * @param user    用户
     * @param request 请求对象
     * @param objId   主表id
     * @param isR3    是否由框架传入
     */
    private ValueHolderV14<SgR3BaseResult> update(Long objId, SgAdjustBillSaveRequest request, User user, boolean isR3) {
        Locale locale = user.getLocale();
        ValueHolderV14<SgR3BaseResult> holderV14 = new ValueHolderV14<>();

        //主表对象
        SgAdjustSaveRequest adjSaveRequest = request.getSgBAdjust();
        //调整单明细对象
        List<SgAdjustItemSaveRequest> items = request.getItems();
        if (CollectionUtils.isEmpty(items)) {
            items = Lists.newArrayList();
        }

        SgBAdjust existAdj = isR3 ? mapper.selectById(objId) : mapper.selectOne(new QueryWrapper<SgBAdjust>().lambda()
                .eq(SgBAdjust::getSourceBillId, adjSaveRequest.getSourceBillId())
                .eq(SgBAdjust::getSourceBillType, adjSaveRequest.getSourceBillType())
                .eq(SgBAdjust::getIsactive, SgConstants.IS_ACTIVE_Y));

        statusCheck(existAdj, locale);

        Long id = existAdj.getId();

        SgBAdjust updateAdj = new SgBAdjust();
        if (null != adjSaveRequest) {
            BeanUtils.copyProperties(adjSaveRequest, updateAdj);
        }
        updateAdj.setId(id);
        updateAdj.setModifierename(user.getEname());
        StorageESUtils.setBModelDefalutDataByUpdate(updateAdj, user);

        if (storageBoxConfig.getBoxEnable()) {
            //更新调整明細
            updateSgBAdjustItem(id, items, user, isR3);
            BigDecimal totQty = itemMapper.selectSumQtyByAdjId(id);
            updateAdj.setTotQty(totQty == null ? BigDecimal.ZERO : totQty);
        } else {
            //更新录入
            if (storageBoxConfig.getBoxEnable()) {
                updateSgBAdjustImpItem(id, request, user, isR3);
            }
        }
        //更新主表
        mapper.updateById(updateAdj);

        //推送ES
        storeESUtils.pushESByAdjust(id, true, false, null, mapper, itemMapper);

        holderV14.setCode(ResultCode.SUCCESS);
        holderV14.setMessage(SgConstants.MESSAGE_STATUS_SUCCESS);
        SgR3BaseResult result = new SgR3BaseResult();
        JSONObject dataJo = new JSONObject();
        dataJo.put(R3ParamConstants.OBJID, id);
        dataJo.put(R3ParamConstants.TABLENAME, SgConstants.SG_B_ADJUST.toUpperCase());
        result.setDataJo(dataJo);
        holderV14.setData(result);
        return holderV14;
    }

    /**
     * 逻辑库存录入/箱内明细-更新
     */
    private void updateSgBAdjustImpItem(Long id, SgAdjustBillSaveRequest request, User user, boolean isR3) {
        //TODO 此处待校验明细id/来源单据id是否为空
        List<SgAdjustImpItemSaveRequest> itemSaveRequests = request.getImpItems();
        List<SgAdjustImpItemSaveRequest> insertList = itemSaveRequests.stream().filter(o -> o.getId() == null || o.getId() == -1L)
                .collect(Collectors.toList());
        List<Long> queryCondition = isR3 ?
                itemSaveRequests.stream().filter(o -> o.getId() != null && o.getId() > 0)
                        .map(SgAdjustImpItemSaveRequest::getId).collect(Collectors.toList()) :
                itemSaveRequests.stream().filter(o -> o.getSourceBillItemId() != null)
                        .map(SgAdjustImpItemSaveRequest::getSourceBillItemId).collect(Collectors.toList());

        if (CollectionUtils.isNotEmpty(queryCondition)) {
            SgBAdjustImpItemMapper impItemMapper = ApplicationContextHandle.getBean(SgBAdjustImpItemMapper.class);
            Map<Long, SgBAdjustImpItem> exsistMap = isR3 ?
                    impItemMapper.selectList(new QueryWrapper<SgBAdjustImpItem>().lambda()
                            .in(SgBAdjustImpItem::getId, queryCondition))
                            .stream().collect(Collectors.toMap(SgBAdjustImpItem::getId, o -> o)) :
                    impItemMapper.selectList(new QueryWrapper<SgBAdjustImpItem>().lambda()
                            .eq(SgBAdjustImpItem::getSgBAdjustId, id)
                            .in(SgBAdjustImpItem::getSourceBillItemId, queryCondition))
                            .stream().collect(Collectors.toMap(SgBAdjustImpItem::getSourceBillItemId, o -> o));
            for (SgAdjustImpItemSaveRequest itemSaveRequest : itemSaveRequests) {
                SgBAdjustImpItem existItem = exsistMap.get(isR3 ? itemSaveRequest.getId() : itemSaveRequest.getSourceBillItemId());
                if (existItem != null) {
                    //TODO 目前覆盖 是否需要增量修改？
                    SgBAdjustImpItem update = new SgBAdjustImpItem();
                    BeanUtils.copyProperties(itemSaveRequest, update);
                    StorageESUtils.setBModelDefalutDataByUpdate(update, user);
                    update.setModifierename(user.getEname());
//                    if (update.getQty() != null) {
//                        update.setAmtList(update.getQty().multiply(existItem.getPriceList() == null ? BigDecimal.ZERO : existItem.getPriceList()));
//                    }
                    impItemMapper.updateById(update);
                } else {
                    insertList.add(itemSaveRequest);
                }
            }
        }

        //构建待新增明细
        List<SgBAdjustImpItem> impItems = Lists.newArrayList();
        insertList.forEach(item -> impItems.add(createInsertSgAdjustItemImp(item, user, id)));

        if (CollectionUtils.isNotEmpty(insertList)) {
            SgBAdjustImpItemMapper impItemMapper = ApplicationContextHandle.getBean(SgBAdjustImpItemMapper.class);
            SgStoreUtils.batchInsertTeus(insertList, "逻辑调整单录入明细", SgConstants.SG_COMMON_INSERT_PAGE_SIZE, impItemMapper);
        }
    }

    /**
     * 逻辑库存明细-更新
     */
    private void updateSgBAdjustItem(Long id, List<SgAdjustItemSaveRequest> items, User user, boolean isR3) {
        List<SgBAdjustItem> insertItems = new ArrayList<>();
        items.forEach(item -> {
            Long itemId = item.getId();
            if (isR3) {
                AssertUtils.notNull(itemId, "明细id为空", user.getLocale());
                if (itemId > 0) {
                    SgBAdjustItem adjustItem = new SgBAdjustItem();
                    BeanUtils.copyProperties(item, adjustItem);
                    itemMapper.updateById(adjustItem);
                } else {
                    SgBAdjustItem adjustItem = createInsertItem(item, user, id);
                    insertItems.add(adjustItem);
                }
            } else {
                if (itemId == null) {
                    AssertUtils.notNull(item.getSourceBillItemId(), "明细修改-参数为空-SOURCE_BILL_ITEM_ID!", user.getLocale());
                    //todo:接口 明细修改  后续是否开放增量更新====目前全量跟新（因为目前接口不会调用明细的更新 只会有新增）
                    SgBAdjustItem adjustItem = itemMapper.selectOne(new QueryWrapper<SgBAdjustItem>().lambda()
                            .eq(SgBAdjustItem::getSgBAdjustId, id)
                            .eq(SgBAdjustItem::getSourceBillItemId, item.getSourceBillItemId()));
                    AssertUtils.notNull(adjustItem, "明细SOURCE_BILL_ITEM_ID为" + item.getSourceBillItemId() + "的记录已经不存在", user.getLocale());
                    SgBAdjustItem updateItem = new SgBAdjustItem();
                    BeanUtils.copyProperties(item, updateItem);
                    updateItem.setAmtList(updateItem.getQty().multiply(updateItem.getPriceList() == null ? BigDecimal.ZERO : updateItem.getPriceList()));
                    itemMapper.updateById(updateItem);
                } else {
                    SgBAdjustItem adjustItem = createInsertItem(item, user, id);
                    insertItems.add(adjustItem);
                }
            }
        });
        if (CollectionUtils.isNotEmpty(insertItems)) {
            SgStoreUtils.batchInsertTeus(insertItems, "逻辑调整单明细", SgConstants.SG_COMMON_INSERT_PAGE_SIZE, itemMapper);
        }
    }

    /**
     * 逻辑库存明细
     *
     * @param requestItem 明细对象集合
     * @param user        用户对象
     * @param mainId      主表Id
     * @return SgBAdjustItem
     */
    private SgBAdjustItem createInsertItem(SgAdjustItemSaveRequest requestItem, User user, Long mainId) {
        SgBAdjustItem item = new SgBAdjustItem();
        BeanUtils.copyProperties(requestItem, item);
        item.setSgBAdjustId(mainId);
        item.setId(ModelUtil.getSequence(SgConstants.SG_B_ADJUST_ITEM));
        StorageESUtils.setBModelDefalutData(item, user);
        item.setModifierename(user.getEname());
        item.setOwnerename(user.getEname());
        item.setAmtList(item.getQty().multiply(item.getPriceList() == null ? BigDecimal.ZERO : item.getPriceList()));
        return item;
    }

    /**
     * 逻辑库存录入明细
     *
     * @param requestItem 录入明细对象集合
     * @param user        用户对象
     * @param mainId      主表Id
     * @return SgBAdjustImpItem
     */
    private SgBAdjustImpItem createInsertSgAdjustItemImp(SgAdjustImpItemSaveRequest requestItem, User user, Long mainId) {
        SgBAdjustImpItem sgBAdjustImpItem = new SgBAdjustImpItem();
        BeanUtils.copyProperties(requestItem, sgBAdjustImpItem);
        sgBAdjustImpItem.setSgBAdjustId(mainId);
        sgBAdjustImpItem.setId(ModelUtil.getSequence(SgConstants.SG_B_ADJUST_IMP_ITEM));
        StorageESUtils.setBModelDefalutData(sgBAdjustImpItem, user);
        sgBAdjustImpItem.setModifierename(user.getEname());
        sgBAdjustImpItem.setOwnerename(user.getEname());
        return sgBAdjustImpItem;
    }

    /**
     * 该单据 是否可编辑的 check
     */
    private void statusCheck(SgBAdjust origAdj, Locale locale) {
        AssertUtils.notNull(origAdj, "当前记录已不存在!", locale);
        AssertUtils.notNull(origAdj.getIsactive(), "数据错误-单据作废状态为空!", locale);
        AssertUtils.notNull(origAdj.getBillStatus(), "数据错误-单据状态为空!", locale);
        AssertUtils.cannot((origAdj.getBillStatus() != SgAdjustConstants.ADJ_AUDIT_STATUS_N), "单据状态不符合，服务结束!", locale);
        AssertUtils.cannot(SgConstants.IS_ACTIVE_N.equalsIgnoreCase(origAdj.getIsactive()), "当前单据已作废,不可编辑", locale);
    }
}
