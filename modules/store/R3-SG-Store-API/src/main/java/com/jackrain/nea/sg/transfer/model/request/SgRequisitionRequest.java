package com.jackrain.nea.sg.transfer.model.request;

import lombok.Data;

import java.io.Serializable;

/**
 * 销退入库签收
 */
@Data
public class SgRequisitionRequest implements Serializable {
    /**
     * 调拨单据
     */
    private String billNo;
    private Integer qtyInTeus;//入库箱数
    private Integer qtyInPack;//入库包数

    private Integer qtyInunit;//入库件数


}
