package com.jackrain.nea.sg.transfer.model.request;

import com.alibaba.fastjson.annotation.JSONField;
import com.jackrain.nea.sg.basic.model.request.SgR3BaseRequest;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author leexh
 * @since 2019/7/19 20:40
 * desc:
 */
@Data
public class ScBTransferDiffBillRequest  extends SgR3BaseRequest implements Serializable {

    /**
     * 调拨差异处理单主表
     */
    @JSONField(name = "SC_B_TRANSFER_DIFF")
    private ScBTransferDiffRequest transferDiffRequest;

    /**
     * 调拨差异处理单明细
     */
    @JSONField(name = "SC_B_TRANSFER_DIFF_ITEM")
    private List<ScBTransferDiffItemRequest> transferDiffItemRequests;
}
