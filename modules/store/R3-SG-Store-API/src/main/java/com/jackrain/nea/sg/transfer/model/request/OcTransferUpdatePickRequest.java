package com.jackrain.nea.sg.transfer.model.request;

import com.jackrain.nea.web.face.User;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author leexh
 * @since 2019/11/27 10:41
 * desc: 批量更新调拨单拣货状态
 */
@Data
public class OcTransferUpdatePickRequest implements Serializable {

    /**
     * 调拨单id集合
     */
    private List<Long> ids;

    /**
     * 出/入库拣货状态
     */
    private Integer outPickStatus;
    private Integer inPickStatus;

    /**
     * 用户信息
     */
    private User user;

}
