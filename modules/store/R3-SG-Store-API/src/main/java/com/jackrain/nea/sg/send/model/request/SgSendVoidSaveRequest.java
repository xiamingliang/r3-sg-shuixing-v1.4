package com.jackrain.nea.sg.send.model.request;

import com.jackrain.nea.web.face.User;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author 舒威
 * @since 2019/4/26
 * create at : 2019/4/26 13:23
 */
@Data
public class SgSendVoidSaveRequest implements Serializable {

    private List<SgSendBillVoidRequest> voidRequests;

    private List<SgSendSaveWithPriorityRequest> saveWithPriorityRequests;

    /**
     * 是否仅执行批量作废
     */
    private Boolean isBatchVoid;

    private User loginUser;

    public SgSendVoidSaveRequest() {
        this.isBatchVoid = false;
    }
}
