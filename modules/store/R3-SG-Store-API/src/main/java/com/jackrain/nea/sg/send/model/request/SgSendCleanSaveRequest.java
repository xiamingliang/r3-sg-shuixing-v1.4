package com.jackrain.nea.sg.send.model.request;

import lombok.Data;

import java.io.Serializable;

/**
 * @author 舒威
 * @since 2019/4/26
 * create at : 2019/4/26 13:23
 */
@Data
public class SgSendCleanSaveRequest implements Serializable {

    private SgSendBillCleanRequest sendBillCleanRequest;

    private SgSendSaveWithPriorityRequest logicSearchRequest;
}
