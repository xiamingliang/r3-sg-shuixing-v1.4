package com.jackrain.nea.sg.adjust.api;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.adjust.model.request.SgAdjustBillAuditRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;

public interface SgAdjAuditCmd {

    ValueHolderV14 audit(SgAdjustBillAuditRequest request) throws NDSException;
}
