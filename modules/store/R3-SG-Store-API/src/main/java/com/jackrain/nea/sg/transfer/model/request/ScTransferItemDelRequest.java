package com.jackrain.nea.sg.transfer.model.request;

import com.jackrain.nea.sg.basic.model.request.SgR3BaseRequest;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author leexh
 * @since 2019/10/21 18:05
 * desc:
 */
@Data
public class ScTransferItemDelRequest extends SgR3BaseRequest implements Serializable {

    /**
     * 明细id
     */
    private List<Long> itemIds;
}
