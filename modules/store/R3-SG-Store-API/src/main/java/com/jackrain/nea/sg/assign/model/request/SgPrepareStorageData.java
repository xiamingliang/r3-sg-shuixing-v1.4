package com.jackrain.nea.sg.assign.model.request;

import com.alibaba.fastjson.annotation.JSONField;
import com.jackrain.nea.sg.basic.model.table.SgBTeusStorage;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author :csy
 * @version :1.0
 * description ：
 * @date :2019/11/7 20:15
 */
@Data
public class SgPrepareStorageData extends SgBTeusStorage implements Serializable {

//     `oc_b_purchase_order_id` bigint(20) DEFAULT NULL COMMENT '采购订单',
//            `oc_b_send_out_id` bigint(20) DEFAULT NULL COMMENT '发货订单',
//            `cp_c_dest_id` bigint(20) DEFAULT NULL COMMENT '收货店仓id',
//            `cp_c_dest_ecode` varchar(20) DEFAULT NULL COMMENT '收货店仓编码',
//            `cp_c_dest_ename` varchar(255) DEFAULT NULL COMMENT '收货店仓名称',
//            `cp_c_customer_id` bigint(20) DEFAULT NULL COMMENT '收货经销商id',
//            `cp_c_customer_ecode` varchar(20) DEFAULT NULL COMMENT '收货经销商编码',
//            `cp_c_customer_ename` varchar(255) DEFAULT NULL COMMENT '收货经销商名称',
    
    @JSONField(name = "TOT_QTY")
    private BigDecimal totQty;

    @JSONField(name = "OC_B_PURCHASE_ORDER_ID")
    private Long ocBPurchaseOrderId;

    @JSONField(name = "OC_B_SEND_OUT_ID")
    private Long ocBSendOutId;

    @JSONField(name = "CP_C_DEST_ID")
    private Long cpCDestId;

    @JSONField(name = "CP_C_DEST_ECODE")
    private String cpCDestEcode;

    @JSONField(name = "CP_C_DEST_ENAME")
    private String cpCDestEname;

    @JSONField(name = "CP_C_CUSTOMER_ID")
    private Long cpCCustomerId;

    @JSONField(name = "CP_C_CUSTOMER_ECODE")
    private String cpCCustomerEcode;

    @JSONField(name = "CP_C_CUSTOMER_ENAME")
    private String cpCCustomerEname;

}
