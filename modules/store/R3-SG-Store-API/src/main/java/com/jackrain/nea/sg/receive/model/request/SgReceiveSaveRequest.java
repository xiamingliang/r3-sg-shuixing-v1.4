package com.jackrain.nea.sg.receive.model.request;


import com.jackrain.nea.sg.send.model.request.SgStoreBillBase;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author 舒威
 * @since 2019/4/28
 * create at : 2019/4/28 18:43
 */
@Data
public class SgReceiveSaveRequest extends SgStoreBillBase implements Serializable {

    private Long id;

    private String billNo;

    private Integer billStatus;

    private Date billDate;

    private Long cpCShopId;

    private String cpCShopTitle;

    private String sourcecode;

    private Integer dealStatus;

    private BigDecimal totQtyPrein;

    private BigDecimal totQtyReceive;

    private Date receiveTime;

    private String remark;

    private Long serviceNode;

    public SgReceiveSaveRequest() {
        this.billDate = new Date();
        this.dealStatus = 0;
    }
}
