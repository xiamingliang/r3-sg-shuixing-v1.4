package com.jackrain.nea.sg.send.api;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.send.model.request.SgSendBillCleanRequest;
import com.jackrain.nea.sg.send.model.request.SgSendCleanSaveRequest;
import com.jackrain.nea.sg.send.model.result.SgSendBillCleanResult;
import com.jackrain.nea.sg.send.model.result.SgSendSaveWithPriorityResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 * 逻辑发货单清空后新增(更新)接口
 * 零售发货单改单（改逻辑仓）用
 *
 * @author 舒威
 * @since 2019/4/26
 * create at : 2019/4/26 13:14
 */
public interface SgSendCleanSaveCmd {

    ValueHolderV14<SgSendBillCleanResult> cleanSgBSend(SgSendBillCleanRequest request) throws NDSException;

    ValueHolderV14<SgSendSaveWithPriorityResult> cleanSaveSgBSend(SgSendCleanSaveRequest request) throws NDSException;
}
