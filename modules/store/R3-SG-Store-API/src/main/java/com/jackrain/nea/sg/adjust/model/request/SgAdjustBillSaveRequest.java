package com.jackrain.nea.sg.adjust.model.request;

import com.alibaba.fastjson.annotation.JSONField;
import com.jackrain.nea.sg.basic.model.request.SgR3BaseRequest;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author csy
 */

@Data
public class SgAdjustBillSaveRequest extends SgR3BaseRequest implements Serializable {


    /**
     * 主表信息
     */
    @JSONField(name = "SG_B_ADJUST")
    private SgAdjustSaveRequest sgBAdjust;

    /**
     * 明细信息
     */
    @JSONField(name = "SG_B_ADJUST_ITEM")
    private List<SgAdjustItemSaveRequest> items;

    /**
     * 录入明细
     */
    @JSONField(name = "SG_B_ADJUST_IMP_ITEM")
    private List<SgAdjustImpItemSaveRequest> impItems;

    /**
     * 箱内明细信息
     */
    @JSONField(name = "SG_B_ADJUST_TEUS_ITEM")
    private List<SgAdjustTeusItemSaveRequest> teusItems;

    /**
     * 录入明细是否从reids获取(默认fasle)
     */
    @JSONField(name = "REDIS_KEY")
    private String redisKey;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SgAdjustBillSaveRequest)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        SgAdjustBillSaveRequest that = (SgAdjustBillSaveRequest) o;

        if (sgBAdjust != null ? !sgBAdjust.equals(that.sgBAdjust) : that.sgBAdjust != null) {
            return false;
        }
        return items != null ? items.equals(that.items) : that.items == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (sgBAdjust != null ? sgBAdjust.hashCode() : 0);
        result = 31 * result + (items != null ? items.hashCode() : 0);
        return result;
    }
}
