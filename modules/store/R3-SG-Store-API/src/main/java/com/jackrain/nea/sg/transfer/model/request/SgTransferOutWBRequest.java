package com.jackrain.nea.sg.transfer.model.request;

import com.jackrain.nea.sg.basic.model.request.SgR3BaseRequest;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author csy
 * Date: 2019/5/8
 * Description: 出库单回写model
 */
@Data
public class SgTransferOutWBRequest extends SgR3BaseRequest implements Serializable {

    //    出库结果单ID	取传入的出库结果单ID	是
//    出库结果单单据编号	取传入的出库结果单单据编号	是
//    出库结果单单据类型	取传入的来源单据类型，即调拨出库	是
//    调拨单ID	取传入的来源单据ID	是
//    调拨单单据编号	取传入的来源单据编号	是
//    是否最后一次出库	取传入的参数，默认N	是
//    出库人	取传入的出库人	是
//    出库时间	取传入的出库时间	是


    /**
     * 出库单id
     */
    private Long outId;

    /**
     * 出库单单据编号
     */
    private String outBillNo;

    /**
     * 出库单单据类型
     */
    private Long outBillType;

    /**
     * 调拨单主表id
     */
    private Long objId;

    /**
     * 调拨单单据编号
     */
    private String billNo;

    /**
     * 是否最后一次出库
     * 0-是
     * 1-不是
     */
    private int isLastOut;

    /**
     * 出库时间
     */
    private Date outTime;


    /**
     * 入库单回写明细
     */
    private List<SgTransferOutWBItemRequest> items;

    /**
     * 录入明细
     */
    private List<SgTransferImpItemRequest> impItemRequests;

    /**
     * 是否erp过来的调拨单
     */
    private Boolean isFromErp;

    /**
     * 箱内明细
     * 出库结果单回写用于生成入库通知单使用
     */
    private List<SgTransferBoxItemRequest> boxItems;

    public SgTransferOutWBRequest() {
        this.isLastOut = 1;
        this.isFromErp = false;
    }
}
