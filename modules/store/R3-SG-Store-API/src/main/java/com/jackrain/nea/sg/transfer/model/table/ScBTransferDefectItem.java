package com.jackrain.nea.sg.transfer.model.table;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.*;
import com.jackrain.nea.sys.domain.BaseModel;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@TableName(value = "sc_b_transfer_defect_item")
@Data
public class ScBTransferDefectItem extends BaseModel {
    @JSONField(name = "ID")
    @TableField(fill = FieldFill.INSERT)
    @TableId(value = "id", type = IdType.INPUT)
    private Long id;

    @JSONField(name = "SC_B_TRANSFER_ID")
    private Long scBTransferId;

    @JSONField(name = "PS_C_SKU_ID")
    private Long psCSkuId;

    @JSONField(name = "PS_C_SKU_ECODE")
    private String psCSkuEcode;

    @JSONField(name = "PS_C_PRO_ID")
    private Long psCProId;

    @JSONField(name = "PS_C_PRO_ECODE")
    private String psCProEcode;

    @JSONField(name = "PS_C_PRO_ENAME")
    private String psCProEname;

    @JSONField(name = "PS_C_CLR_ID")
    private Long psCClrId;

    @JSONField(name = "PS_C_CLR_ECODE")
    private String psCClrEcode;

    @JSONField(name = "PS_C_CLR_ENAME")
    private String psCClrEname;

    @JSONField(name = "PS_C_SIZE_ID")
    private Long psCSizeId;

    @JSONField(name = "PS_C_SIZE_ECODE")
    private String psCSizeEcode;

    @JSONField(name = "PS_C_SIZE_ENAME")
    private String psCSizeEname;

    @JSONField(name = "PRICE_LIST")
    private BigDecimal priceList;

    @JSONField(name = "QTY")
    private BigDecimal qty;

    @JSONField(name = "QTY_OUT")
    private BigDecimal qtyOut;

    @JSONField(name = "QTY_IN")
    private BigDecimal qtyIn;

    @JSONField(name = "QTY_DIFF")
    private BigDecimal qtyDiff;

    @JSONField(name = "AMT_LIST")
    private BigDecimal amtList;

    @JSONField(name = "AMT_OUT_LIST")
    private BigDecimal amtOutList;

    @JSONField(name = "AMT_IN_LIST")
    private BigDecimal amtInList;

    @JSONField(name = "VERSION")
    private Long version;

    @JSONField(name = "OWNERID")
    private Long ownerid;

    @JSONField(name = "OWNERENAME")
    private String ownerename;

    @JSONField(name = "OWNERNAME")
    private String ownername;

    @JSONField(name = "CREATIONDATE")
    private Date creationdate;

    @JSONField(name = "MODIFIERID")
    private Long modifierid;

    @JSONField(name = "MODIFIERENAME")
    private String modifierename;

    @JSONField(name = "MODIFIERNAME")
    private String modifiername;

    @JSONField(name = "MODIFIEDDATE")
    private Date modifieddate;

    @JSONField(name = "ISACTIVE")
    private String isactive;

    @JSONField(name = "RESERVE_BIGINT01")
    private Long reserveBigint01;

    @JSONField(name = "RESERVE_BIGINT02")
    private Long reserveBigint02;

    @JSONField(name = "RESERVE_BIGINT03")
    private Long reserveBigint03;

    @JSONField(name = "RESERVE_BIGINT04")
    private Long reserveBigint04;

    @JSONField(name = "RESERVE_BIGINT05")
    private Long reserveBigint05;

    @JSONField(name = "RESERVE_BIGINT06")
    private Long reserveBigint06;

    @JSONField(name = "RESERVE_BIGINT07")
    private Long reserveBigint07;

    @JSONField(name = "RESERVE_BIGINT08")
    private Long reserveBigint08;

    @JSONField(name = "RESERVE_BIGINT09")
    private Long reserveBigint09;

    @JSONField(name = "RESERVE_BIGINT10")
    private Long reserveBigint10;

    @JSONField(name = "RESERVE_VARCHAR01")
    private String reserveVarchar01;

    @JSONField(name = "RESERVE_VARCHAR02")
    private String reserveVarchar02;

    @JSONField(name = "RESERVE_VARCHAR03")
    private String reserveVarchar03;

    @JSONField(name = "RESERVE_VARCHAR04")
    private String reserveVarchar04;

    @JSONField(name = "RESERVE_VARCHAR05")
    private String reserveVarchar05;

    @JSONField(name = "RESERVE_VARCHAR06")
    private String reserveVarchar06;

    @JSONField(name = "RESERVE_VARCHAR07")
    private String reserveVarchar07;

    @JSONField(name = "RESERVE_VARCHAR08")
    private String reserveVarchar08;

    @JSONField(name = "RESERVE_VARCHAR09")
    private String reserveVarchar09;

    @JSONField(name = "RESERVE_VARCHAR10")
    private String reserveVarchar10;

    @JSONField(name = "RESERVE_DECIMAL01")
    private BigDecimal reserveDecimal01;

    @JSONField(name = "RESERVE_DECIMAL02")
    private BigDecimal reserveDecimal02;

    @JSONField(name = "RESERVE_DECIMAL03")
    private BigDecimal reserveDecimal03;

    @JSONField(name = "RESERVE_DECIMAL04")
    private BigDecimal reserveDecimal04;

    @JSONField(name = "RESERVE_DECIMAL05")
    private BigDecimal reserveDecimal05;

    @JSONField(name = "RESERVE_DECIMAL06")
    private BigDecimal reserveDecimal06;

    @JSONField(name = "RESERVE_DECIMAL07")
    private BigDecimal reserveDecimal07;

    @JSONField(name = "RESERVE_DECIMAL08")
    private BigDecimal reserveDecimal08;

    @JSONField(name = "RESERVE_DECIMAL09")
    private BigDecimal reserveDecimal09;

    @JSONField(name = "RESERVE_DECIMAL10")
    private BigDecimal reserveDecimal10;

    @JSONField(name = "GBCODE")
    private String gbcode;

    @JSONField(name = "PS_C_BRAND_ID")
    private Long psCBrandId;

    @JSONField(name = "SG_B_PHY_IN_RESULT_ID")
    private Long sgBPhyInResultId;

    @JSONField(name = "SG_B_PHY_IN_RESULT_BILL_NO")
    private String sgBPhyInResultBillNo;
}