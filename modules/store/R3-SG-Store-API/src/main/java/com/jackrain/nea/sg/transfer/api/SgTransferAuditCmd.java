package com.jackrain.nea.sg.transfer.api;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.basic.model.result.SgR3BaseResult;
import com.jackrain.nea.sg.transfer.model.request.SgTransferBillAuditRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.util.ValueHolder;
import com.jackrain.nea.web.face.User;

/**
 * @author csy
 * Date: 2019/5/5
 * Description:
 */
public interface SgTransferAuditCmd {

    /**
     * 调拨单审核
     *
     * @param request 参数
     * @return result
     * @throws NDSException 框架封装异常
     */
    ValueHolderV14<SgR3BaseResult> audit(SgTransferBillAuditRequest request) throws NDSException;

    ValueHolder audit(Long objId, User user) throws NDSException;
}
