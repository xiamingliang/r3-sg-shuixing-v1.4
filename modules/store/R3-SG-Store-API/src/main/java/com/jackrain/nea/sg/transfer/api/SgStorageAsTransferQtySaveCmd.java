package com.jackrain.nea.sg.transfer.api;

import com.jackrain.nea.sys.Command;

/**
 * @author zhu lin yu
 * @since 2019/10/10
 * create at : 2019/10/10 19:36
 */
public interface SgStorageAsTransferQtySaveCmd extends Command {
}
