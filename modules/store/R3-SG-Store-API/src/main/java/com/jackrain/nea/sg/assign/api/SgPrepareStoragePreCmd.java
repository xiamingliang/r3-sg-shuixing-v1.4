package com.jackrain.nea.sg.assign.api;

import com.jackrain.nea.sg.assign.model.request.SgPrepareStoragePreRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 * @author :csy
 * @version :1.0
 * description ：
 * @date :2019/11/7 19:30
 */
public interface SgPrepareStoragePreCmd {

    /**
     * 配货
     *
     * @param request request
     * @return v14
     */
    ValueHolderV14 prepareStorage(SgPrepareStoragePreRequest request);
}
