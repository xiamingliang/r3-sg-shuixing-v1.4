package com.jackrain.nea.sg.receive.model.result;

import com.jackrain.nea.sg.basic.model.result.SgR3BaseResult;
import lombok.Data;

import java.io.Serializable;

/**
 * @author: 舒威
 * @since: 2019/4/28
 * create at : 2019/4/28 22:48
 */
@Data
public class SgReceiveBillSaveResult extends SgR3BaseResult implements Serializable {

    /**
     * 占用更新结果
     */
    private int preoutUpdateResult;
}
