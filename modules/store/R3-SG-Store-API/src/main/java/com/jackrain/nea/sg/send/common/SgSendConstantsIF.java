package com.jackrain.nea.sg.send.common;

/**
 * @author 舒威
 * @since 2019/4/19
 * create at : 2019/4/19 16:35
 */
public interface SgSendConstantsIF {


    /**
     * 单据状态
     * CREATE创建 1，UPDATE更新 2 ，PART_SEND部分发货 3，ALL_SEND全部发货 4, CANCELED已作废 5
     */
    Integer BILL_SEND_STATUS_CREATE = 1;
    Integer BILL_SEND_STATUS_UPDATE = 2;
    Integer BILL_SEND_STATUS_PART_SEND = 3;
    Integer BILL_SEND_STATUS_ALL_SEND = 4;
    Integer BILL_SEND_STATUS_CANCELED = 5;

    /**
     * 处理状态（开发使用）
     * 0未处理，1已处理
     */
    Integer BILL_SEND_STATUS_DEAL = 1;
    Integer BILL_SEND_STATUS_UNDEAL = 0;


    /**
     * 是否最后一次发货
     */
    Integer IS_LASTSEND_N = 1;
    Integer IS_LASTSEND_Y = 0;

    /**
     * 是否箱
     */
    Integer IS_TEUS_N = 0;
    Integer IS_TEUS_Y = 1;
}
