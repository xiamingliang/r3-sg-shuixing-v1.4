package com.jackrain.nea.sg.receive.common;

/**
 * @author 舒威
 * @since 2019/4/19
 * create at : 2019/4/19 16:35
 */
public interface SgReceiveConstantsIF {

    /**
     * 单据状态
     * CREATE创建 1，UPDATE更新 2 ，PART_RECEIVE部分收货 3，ALL_RECEIVE全部收货 4, CANCELED已作废 5
     */
    Integer BILL_RECEIVE_STATUS_CREATE = 1;
    Integer BILL_RECEIVE_STATUS_UPDATE = 2;
    Integer BILL_RECEIVE_STATUS_PART_RECEIVE = 3;
    Integer BILL_RECEIVE_STATUS_ALL_RECEIVE = 4;
    Integer BILL_RECEIVE_STATUS_CANCELED = 5;

    /**
     * 处理状态（开发使用）
     * 0未处理，1已处理
     */
    Integer BILL_RECEIVE_STATUS_DEAL = 1;
    Integer BILL_RECEIVE_STATUS_UNDEAL = 0;


    /**
     * 是否最后一次收货
     */
    Integer IS_LASTRECEIVE_N = 1;
    Integer IS_LASTRECEIVE_Y = 0;
}
