package com.jackrain.nea.sg.transfer.api;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.transfer.model.request.SgTransferBillSaveRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 * @author csy
 * Date: 2019/5/20
 * Description:
 */
public interface SgTransferAutoOutCmd {

    /**
     * 调拨单审核
     *
     * @param request 参数
     * @return result
     * @throws NDSException 框架封装异常
     */
    ValueHolderV14 autoOut(SgTransferBillSaveRequest request) throws NDSException;
}
