package com.jackrain.nea.sg.transfer.model.request;

import com.alibaba.fastjson.annotation.JSONField;
import com.jackrain.nea.sg.basic.model.request.SgR3BaseRequest;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author csy
 * Date: 2019/5/5
 * Description:
 */
@Data
public class SgTransferBillSaveRequest extends SgR3BaseRequest implements Serializable {

    @JSONField(name = "SC_B_TRANSFER")
    private SgTransferSaveRequest transfer;

    @JSONField(name = "SC_B_TRANSFER_ITEM")
    private List<SgTransferItemSaveRequest> items;

    @JSONField(name = "SC_B_TRANSFER_IMP_ITEM")
    private List<SgTransferImpItemSaveRequest> impItems;


}
