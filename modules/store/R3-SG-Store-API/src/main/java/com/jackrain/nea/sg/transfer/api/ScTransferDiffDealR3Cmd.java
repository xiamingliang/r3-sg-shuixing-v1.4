package com.jackrain.nea.sg.transfer.api;

import com.jackrain.nea.sys.Command;

/**
 * @author leexh
 * @since 2019/7/22 14:26
 * desc: 差异处理服务
 */
public interface ScTransferDiffDealR3Cmd extends Command {
}
