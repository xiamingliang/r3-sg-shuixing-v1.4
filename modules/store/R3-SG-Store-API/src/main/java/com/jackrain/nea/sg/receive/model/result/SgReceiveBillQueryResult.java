package com.jackrain.nea.sg.receive.model.result;

import com.jackrain.nea.sg.receive.model.table.SgBReceive;
import com.jackrain.nea.sg.receive.model.table.SgBReceiveItem;
import lombok.Data;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

/**
 * @author 舒威
 * @since 2019/7/17
 * create at : 2019/7/17 14:46
 */
@Data
public class SgReceiveBillQueryResult implements Serializable {

    // k：订单id,订单类型      v:逻辑收货单主子表
    private HashMap<String, HashMap<SgBReceive, List<SgBReceiveItem>>> results;
}
