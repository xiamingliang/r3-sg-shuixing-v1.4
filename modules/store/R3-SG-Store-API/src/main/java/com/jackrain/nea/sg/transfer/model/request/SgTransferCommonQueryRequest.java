package com.jackrain.nea.sg.transfer.model.request;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author csy
 * Date: 2019/5/15
 * Description: 根据单据状态 查询调拨单
 */
@Data
public class SgTransferCommonQueryRequest implements Serializable {

    @JSONField(name = "TRANSFER_TYPE")
    private Integer transferType;

    @JSONField(name = "BILL_NO")
    private List<String> billNos;

    @JSONField(name = "OC_B_SEND_OUT_ID")
    private Long ocBSendOutId;

    @JSONField(name = "OC_B_SEND_OUT_NO")
    private String ocBSendOutNo;

    @JSONField(name = "ID")
    private List<Long> ids;
}
