package com.jackrain.nea.sg.transfer.api;

import com.jackrain.nea.sys.Command;

/**
 * @author: 汪聿森
 * @Date: Created in 2019-09-12 11:36
 * @Description : 调拨单一键出库
 */
public interface ScTransferOneClickOutLibraryCmd extends Command {
}
