package com.jackrain.nea.sg.receive.api;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.receive.model.request.SgReceiveBillCleanRequest;
import com.jackrain.nea.sg.receive.model.request.SgReceiveBillSaveRequest;
import com.jackrain.nea.sg.receive.model.request.SgReceiveBillSubmitRequest;
import com.jackrain.nea.sg.receive.model.request.SgReceiveBillVoidRequest;
import com.jackrain.nea.sg.receive.model.result.*;
import com.jackrain.nea.sg.send.model.request.SgStoreBillBase;
import com.jackrain.nea.sys.domain.ValueHolderV14;

import java.util.List;

/**
 * 逻辑收货单
 *
 * @author 舒威
 * @since 2019/4/19
 * create at : 2019/4/19 13:34
 */
public interface SgReceiveCmd {

    ValueHolderV14<SgReceiveBillQueryResult> querySgBReceive(List<SgStoreBillBase> searchs);

    ValueHolderV14<SgReceiveBillSaveResult> saveSgBReceive(SgReceiveBillSaveRequest request) throws NDSException;

    ValueHolderV14<SgReceiveBillVoidResult> voidSgBReceive(SgReceiveBillVoidRequest request) throws NDSException;

    ValueHolderV14<SgReceiveBillSubmitResult> submitSgBReceive(SgReceiveBillSubmitRequest request) throws NDSException;

    ValueHolderV14<SgReceiveBillCleanResult> cleanSgBReceive(SgReceiveBillCleanRequest request) throws NDSException;

}
