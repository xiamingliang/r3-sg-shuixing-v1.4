package com.jackrain.nea.sg.transfer.model.request;

import com.jackrain.nea.sg.basic.model.request.SgR3BaseRequest;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author csy
 * Date: 2019/5/8
 * Description: 入库单回写model
 */
@Data
public class SgTransferInWBRequest extends SgR3BaseRequest implements Serializable {

    /**
     * 入库单id
     */
    private Long inId;

    /**
     * 入库单单据编号
     */
    private String inBillNo;

    /**
     * 入库单单据类型
     */
    private Long inBillType;

    /**
     * 调拨单主表id
     */
    private Long objId;

    /**
     * 调拨单单据编号
     */
    private String billNo;

    /**
     * 是否最后一次入库 默认N
     */
    private Integer isLastIn;

    /**
     * 入库时间
     */
    private Date inTime;


    /**
     * 入库单回写明细
     */
    private List<SgTransferInWBItemRequest> items;

    /**
     * 录入明细
     */
    private List<SgTransferImpItemRequest> impItemRequests;

    /**
     *残次品明细
     */
    private List<ScBTransferDefectItemRequest> defectItems;
}
