package com.jackrain.nea.sg.send.model.request;

import lombok.Data;

import java.io.Serializable;

/**
 * @author 舒威
 * @since 2019/4/24
 * create at : 2019/4/24 13:15
 */
@Data
public class SgSendBillVoidRequest extends SgStoreBillBase implements Serializable {

    /*
    * 查询逻辑发货单是否存在 标志
    * */
    private Boolean isExist;

    /*
   * 是否实缺业务
   * */
    private Boolean isDeficiency;

    public SgSendBillVoidRequest() {
        this.isExist = false;
        this.isDeficiency = false;
    }
}
