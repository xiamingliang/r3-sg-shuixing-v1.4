package com.jackrain.nea.sg.transfer.api;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.basic.model.result.SgR3BaseResult;
import com.jackrain.nea.sg.transfer.model.request.SgTransferBillSaveRequest;
import com.jackrain.nea.sg.transfer.model.request.SgTransferUpdateSapStatusRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 * @author csy
 * Date: 2019/5/5
 * Description:
 */
public interface SgTransferSaveCmd {

    /**
     * 调拨单保存/编辑
     *
     * @param request 参数
     * @return result
     * @throws NDSException 框架封装异常
     */
    ValueHolderV14<SgR3BaseResult> save(SgTransferBillSaveRequest request) throws NDSException;

    ValueHolderV14 updateSapStatus(SgTransferUpdateSapStatusRequest request);
}
