package com.jackrain.nea.sg.receive.model.request;

import com.jackrain.nea.sg.send.model.request.SgStoreBillTeusItemBase;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author: heliu
 * @since: 2019/10/21
 * create at : 2019/10/21 14:51
 */
@Data
public class SgReceiveTeusItemSaveRequest extends SgStoreBillTeusItemBase implements Serializable {

    private Long id;

    private Long sourceBillItemId;

    private Long sgBReceiveId;

    private Long cpCStoreId;

    private String cpCStoreEcode;

    private String cpCStoreEname;

    private BigDecimal qty;

    private BigDecimal priceList;

    private String remark;

    public SgReceiveTeusItemSaveRequest() {
        this.qty = BigDecimal.ZERO;
    }
}