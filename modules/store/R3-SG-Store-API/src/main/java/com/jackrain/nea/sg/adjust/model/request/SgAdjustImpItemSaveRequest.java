package com.jackrain.nea.sg.adjust.model.request;

import com.jackrain.nea.sg.send.model.request.SgStoreBillTeusItemBase;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author: heliu
 * @since: 2019/10/29
 * create at : 2019/10/29 15:17
 */
@Data
public class SgAdjustImpItemSaveRequest extends SgStoreBillTeusItemBase implements Serializable {

    private Long id;

    private Long sgBAdjustId;

    private Long sourceBillItemId;

    private Long cpCStoreId;

    private String cpCStoreEcode;

    private String cpCStoreEname;

    private Integer isTeus;

    private BigDecimal qty;

    private BigDecimal priceList;

    private String remark;

    public SgAdjustImpItemSaveRequest() {
        this.qty = BigDecimal.ZERO;
    }
}