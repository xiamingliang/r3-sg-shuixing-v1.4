package com.jackrain.nea.sg.transfer.api;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.transfer.model.request.SgTransferBillSaveRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;

import java.util.List;

/**
 * @author csy
 * Date: 2019/5/7
 * Description:
 */
public interface SgTransferSaveAndAuditCmd {
    /**
     * 调拨单新增并审核
     *
     * @param request 参数
     * @return result
     * @throws NDSException 框架封装异常
     */
    ValueHolderV14 saveAndAudit(SgTransferBillSaveRequest request) throws NDSException;

    /**
     * 调拨单批量新增并审核
     *
     * @param requests 入参
     * @return 出参
     * @throws NDSException 框架封装异常
     */
    ValueHolderV14 saveAndAuditBatch(List<SgTransferBillSaveRequest> requests) throws NDSException;
}
