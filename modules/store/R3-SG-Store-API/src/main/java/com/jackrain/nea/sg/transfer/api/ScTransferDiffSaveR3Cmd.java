package com.jackrain.nea.sg.transfer.api;

import com.jackrain.nea.sys.Command;

/**
 * @author leexh
 * @since 2019/7/19 20:24
 * desc: 保存调拨差异处理保存服务
 */
public interface ScTransferDiffSaveR3Cmd extends Command {
}
