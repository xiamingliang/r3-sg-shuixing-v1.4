package com.jackrain.nea.sg.transfer.model.request;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.io.Serializable;

/**
 * @author csy
 * Date: 2019/5/9
 * Description:
 */
@Data
public class SgTransferBaseWBItemRequest implements Serializable {

    @JSONField(name = "ID")
    private Long id;

    @JSONField(name = "PS_C_SKU_ID")
    private Long psCSkuId;

    //ECODE
    @JSONField(name = "PS_C_SKU_ECODE")
    private String psCSkuEcode;

    @JSONField(name = "PS_C_PRO_ID")
    private Long psCProId;
    //ECODE
    @JSONField(name = "PS_C_PRO_ECODE")
    private String psCProEcode;
    //ECODE
    @JSONField(name = "PS_C_PRO_ENAME")
    private String psCProEname;

    @JSONField(name = "PS_C_CLR_ID")
    private Long psCClrId;

    @JSONField(name = "PS_C_CLR_ECODE")
    private String psCClrEcode;

    @JSONField(name = "PS_C_CLR_ENAME")
    private String psCClrEname;

    @JSONField(name = "PS_C_SIZE_ID")
    private Long psCSizeId;

    @JSONField(name = "PS_C_SIZE_ECODE")
    private String psCSizeEcode;

    @JSONField(name = "PS_C_SIZE_ENAME")
    private String psCSizeEname;
}
