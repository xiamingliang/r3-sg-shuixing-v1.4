package com.jackrain.nea.sg.transfer.model.result;

import lombok.Data;

import java.io.Serializable;

/**
 * @author leexh
 * @since 2019/11/27 11:11
 * desc: 批量更新调拨单拣货状态出参
 */
@Data
public class OcTransferUpdatePickResult implements Serializable {

    /**
     * 调拨单信息
     */
    private Long objId;
    private String billNo;

    /**
     * 错误信息
     */
    private String errorMsg;

}
