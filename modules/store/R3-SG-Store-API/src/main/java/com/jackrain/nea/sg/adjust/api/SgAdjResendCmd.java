package com.jackrain.nea.sg.adjust.api;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 * @author: 舒威
 * @since: 2019/8/20
 * create at : 2019/8/20 9:55
 */
public interface SgAdjResendCmd {

    ValueHolderV14 resendAdjLog(JSONObject param);
}
