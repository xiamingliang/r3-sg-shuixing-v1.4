package com.jackrain.nea.sg.transfer.model.request;

import com.jackrain.nea.web.face.User;
import lombok.Data;

import java.io.Serializable;

/**
 * @author leexh
 * @since 2019/7/29 17:40
 * desc:
 */
@Data
public class SgTransferDiffQueryRequest implements Serializable {

    private Long id;

    private String billNo;

    private Long ocBSendOutId;

    private User user;
}
