package com.jackrain.nea.sg.transfer.model.request;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: zyj
 * Date: 2019/11/1
 * Time: 17:04
 * Description: Sap回传 更新调拨单sap状态
 */
@Data
public class SgTransferUpdateSapStatusRequest implements Serializable{
    /**
     * sap回传状态
     */
    private Integer sapStatus;

    /**
     * 调拨单主键
     */
    private List<Long> idList;

}
