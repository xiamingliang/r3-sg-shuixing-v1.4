package com.jackrain.nea.sg.transfer.model.request;

import com.alibaba.fastjson.annotation.JSONField;
import com.jackrain.nea.es.annotations.Field;
import com.jackrain.nea.es.constans.FieldType;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author leexh
 * @since 2019/10/21 18:46
 * desc: 录入明细保存入参
 */
@Data
public class SgTransferImpItemSaveRequest implements Serializable {

    @JSONField(name = "ID")
    private Long id;

    @JSONField(name = "SC_B_TRANSFER_ID")
    private Long scBTransferId;

    @JSONField(name = "IS_TEUS")
    private Integer isTeus;

    @JSONField(name = "PS_C_TEUS_ID")
    private Long psCTeusId;

    @JSONField(name = "PS_C_TEUS_ECODE")
    private String psCTeusEcode;

    @JSONField(name = "PS_C_MATCHSIZE_ID")
    private Long psCMatchsizeId;

    @JSONField(name = "PS_C_MATCHSIZE_ECODE")
    private String psCMatchsizeEcode;

    @JSONField(name = "PS_C_MATCHSIZE_ENAME")
    private String psCMatchsizeEname;

    @JSONField(name = "PS_C_SKU_ID")
    private Long psCSkuId;

    @JSONField(name = "PS_C_SKU_ECODE")
    private String psCSkuEcode;

    @JSONField(name = "PS_C_PRO_ID")
    private Long psCProId;

    @JSONField(name = "PS_C_PRO_ECODE")
    private String psCProEcode;

    @JSONField(name = "PS_C_PRO_ENAME")
    private String psCProEname;

    @JSONField(name = "PS_C_SPEC1_ID")
    private Long psCSpec1Id;

    @JSONField(name = "PS_C_SPEC1_ECODE")
    private String psCSpec1Ecode;

    @JSONField(name = "PS_C_SPEC1_ENAME")
    private String psCSpec1Ename;

    @JSONField(name = "PS_C_SPEC2_ID")
    private Long psCSpec2Id;

    @JSONField(name = "PS_C_SPEC2_ECODE")
    private String psCSpec2Ecode;

    @JSONField(name = "PS_C_SPEC2_ENAME")
    private String psCSpec2Ename;

    @JSONField(name = "PRICE_LIST")
    private BigDecimal priceList;

    @JSONField(name = "DISCOUNT")
    private BigDecimal discount;

    @JSONField(name = "QTY")
    private BigDecimal qty;

    @JSONField(name = "QTY_OUT")
    private BigDecimal qtyOut;

    @JSONField(name = "QTY_IN")
    private BigDecimal qtyIn;

    @JSONField(name = "REMARK")
    private String remark;

    // ZH 2019-12-26 SH  STRAT

//    @JSONField(name = "ps_c_brand_id")
//    private  Integer psCBrandId;

    @JSONField(name = "is_no_pack")
    private  String isNoPack;

    @JSONField(name = "price_order")
    private  BigDecimal priceOrder;

    @JSONField(name = "price_country")
    private  BigDecimal priceCountry;


    // ZH 2019-12-26 SH END
}
