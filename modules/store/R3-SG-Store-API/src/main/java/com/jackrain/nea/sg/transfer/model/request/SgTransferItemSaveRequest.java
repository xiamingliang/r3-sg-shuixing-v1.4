package com.jackrain.nea.sg.transfer.model.request;

import com.alibaba.fastjson.annotation.JSONField;
import com.jackrain.nea.es.annotations.Field;
import com.jackrain.nea.es.constans.FieldType;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author csy
 */
@Data
public class SgTransferItemSaveRequest implements Serializable {
    @JSONField(name = "ID")
    private Long id;

    @JSONField(name = "PS_C_SKU_ID")
    private Long psCSkuId;

    @JSONField(name = "PS_C_SKU_ECODE")
    private String psCSkuEcode;

    @JSONField(name = "PS_C_PRO_ID")
    private Long psCProId;

    @JSONField(name = "PS_C_PRO_ECODE")
    private String psCProEcode;

    @JSONField(name = "PS_C_PRO_ENAME")
    private String psCProEname;

    @JSONField(name = "PS_C_CLR_ID")
    private Long psCClrId;

    @JSONField(name = "PS_C_CLR_ECODE")
    private String psCClrEcode;

    @JSONField(name = "PS_C_CLR_ENAME")
    private String psCClrEname;

    @JSONField(name = "PS_C_SIZE_ID")
    private Long psCSizeId;

    @JSONField(name = "PS_C_SIZE_ECODE")
    private String psCSizeEcode;

    @JSONField(name = "PS_C_SIZE_ENAME")
    private String psCSizeEname;

    @JSONField(name = "PRICE_LIST")
    private BigDecimal priceList;

    @JSONField(name = "QTY")
    private BigDecimal qty;

    @JSONField(name = "QTY_OUT")
    private BigDecimal qtyOut;

    @JSONField(name = "QTY_IN")
    private BigDecimal qtyIn;

    @JSONField(name = "QTY_DIFF")
    private BigDecimal qtyDiff;

    @JSONField(name = "GBCODE")
    private String gbcode;

    // ZH 2019-12-26 SH  STRAT

    @JSONField(name = "ps_c_brand_id")
    private  Integer psCBrandId;

    @JSONField(name = "is_no_pack")
    private  String isNoPack;

    @JSONField(name = "price_country")
    private  BigDecimal priceCountry;


    // ZH 2019-12-26 SH END

}