package com.jackrain.nea.sg.send.api;

import com.jackrain.nea.sys.Command;

/**
 * 逻辑发货单
 *
 * @author 舒威
 * @since 2019/4/19
 * create at : 2019/4/19 13:34
 */
public interface SgSendVoidR3Cmd extends Command {

}
