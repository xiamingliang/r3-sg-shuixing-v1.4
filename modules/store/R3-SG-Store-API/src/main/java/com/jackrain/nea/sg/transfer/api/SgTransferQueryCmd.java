package com.jackrain.nea.sg.transfer.api;

import com.github.pagehelper.PageInfo;
import com.jackrain.nea.sg.transfer.model.request.SgTransferCommonQueryRequest;
import com.jackrain.nea.sg.transfer.model.request.SgTransferForPdaRequest;
import com.jackrain.nea.sg.transfer.model.request.SgTransferQueryByStatusRequest;
import com.jackrain.nea.sg.transfer.model.result.ScTransferImpItemAndTeusItemResult;
import com.jackrain.nea.sg.transfer.model.table.ScBTransfer;
import com.jackrain.nea.sg.transfer.model.table.ScBTransferImpItem;
import com.jackrain.nea.sg.transfer.model.table.ScBTransferItem;
import com.jackrain.nea.sys.domain.ValueHolderV14;

import java.util.List;

/**
 * @author csy
 * Date: 2019/5/15
 * Description:
 */
public interface SgTransferQueryCmd {
    /**
     * 根据单据状态查询 调拨单列表（分页）
     *
     * @param request 分页入参
     * @return 调拨单信息 分页
     */
    ValueHolderV14<PageInfo<ScBTransfer>> queryTransferList(SgTransferQueryByStatusRequest request);


    /**
     * 根据主表查询调拨单明细
     *
     * @param objId 主表id
     * @return 明细信息
     */
    ValueHolderV14<List<ScBTransferItem>> queryItemsById(Long objId);

    /**
     * 根据主表查询调拨单明细
     *
     * @param request 主表id
     * @return 明细信息
     */
    ValueHolderV14<List<ScBTransfer>> queryTransferByCommon(SgTransferCommonQueryRequest request);

    /**
     * 查询调拨主表信息(支持分页)
     *
     * @param request pda入参
     * @return 出参
     */
    ValueHolderV14<PageInfo<ScBTransfer>> queryTransferForPda(SgTransferForPdaRequest request);


    /**
     * 查询调拨录入明细信息，包含箱明细信息(支持分页)
     *
     * @param request pda入参
     * @return 出参
     */
    ValueHolderV14<PageInfo<ScTransferImpItemAndTeusItemResult>> queryTransferImpItemForPda(SgTransferForPdaRequest request);


    /**
     * add chenxinxing  2020.01.04
     * 根据主表ID查询调拨单
     * @param objIds 主表id
     */
    List<ScBTransfer> queryScBTransferByIds(List<Long> objIds);

    /**
     * 根据退货申请单号查询指定状态的调拨单
     * @param statusList 状态List
     * @param applyBillNo 退货申请单号
     * @return
     */
    List<ScBTransfer> queryScBTransferByStatus(List<Integer> statusList,String applyBillNo);

    /**
     * 根据主表查询调拨单明细
     *
     * @param objId 主表id
     * @return 明细信息
     */
    List<ScBTransferItem> queryItems(Long objId);

    /**
     * 根据主表查询调拨单明细
     *
     * @param objId 主表id
     * @return 明细信息
     */
    List<ScBTransferImpItem> queryImpItems(Long objId);


    /**
     * 根据备货单号查询调拨单
     * @param stockBillNo
     * @return
     */
    ValueHolderV14<ScBTransfer> queryScBTransferByStockBillNo(String stockBillNo);
}
