package com.jackrain.nea.sg.receive.api;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.receive.model.request.SgReceiveBillSaveRequest;
import com.jackrain.nea.sg.receive.model.result.SgReceiveBillSaveResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 * 逻辑收货单-补偿服务
 *
 * @author 舒威
 * @since 2019/6/10
 * create at : 2019/6/10 14:47
 */
public interface SgReceiveCancelCmd {

    ValueHolderV14<SgReceiveBillSaveResult> cancelSaveSgBReceive(SgReceiveBillSaveRequest request) throws NDSException;
}
