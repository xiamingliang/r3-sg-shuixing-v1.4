package com.jackrain.nea.sg.receive.model.request;

import com.jackrain.nea.sg.send.model.request.SgStoreBillItemBase;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author 舒威
 * @since 2019/4/19
 * create at : 2019/4/19 14:20
 */
@Data
public class SgReceiveItemSaveRequest extends SgStoreBillItemBase implements Serializable {

    private Long id;

    private Long sgBReceiveId;

    private BigDecimal qty;

    private BigDecimal qtyPrein;

    private BigDecimal qtyReceive;

    private Integer rank;
    //是否发送在途库存
    private Boolean isReceiveStorage;
    //负库存控制
    private Long isnegative;

    public SgReceiveItemSaveRequest() {
        this.isReceiveStorage = true;
        this.qtyReceive = BigDecimal.ZERO;
        this.qty = BigDecimal.ZERO;
        this.qtyPrein = BigDecimal.ZERO;
    }
}
