package com.jackrain.nea.sg.transfer.model.table;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.*;
import com.jackrain.nea.es.annotations.Document;
import com.jackrain.nea.es.annotations.Field;
import com.jackrain.nea.es.constans.FieldType;
import com.jackrain.nea.sys.domain.BaseModelES;
import lombok.Data;

import java.math.BigDecimal;

@TableName(value = "sc_b_transfer_imp_item")
@Data
@Document(index = "sc_b_transfer", type = "sc_b_transfer_imp_item")
public class ScBTransferImpItem extends BaseModelES {
    @JSONField(name = "ID")
    @TableField(fill = FieldFill.INSERT)
    @TableId(value = "id", type = IdType.INPUT)
    @Field(type = FieldType.Long)
    private Long id;

    @JSONField(name = "SC_B_TRANSFER_ID")
    @Field(type = FieldType.Long)
    private Long scBTransferId;

    @JSONField(name = "IS_TEUS")
    @Field(type = FieldType.Integer)
    private Integer isTeus;

    @JSONField(name = "PS_C_TEUS_ID")
    @Field(type = FieldType.Long)
    private Long psCTeusId;

    @JSONField(name = "PS_C_MATCHSIZE_ID")
    @Field(type = FieldType.Long)
    private Long psCMatchsizeId;

    @JSONField(name = "PS_C_MATCHSIZE_ECODE")
    @Field(type = FieldType.Keyword)
    private String psCMatchsizeEcode;

    @JSONField(name = "PS_C_MATCHSIZE_ENAME")
    @Field(type = FieldType.Keyword)
    private String psCMatchsizeEname;

    @JSONField(name = "PS_C_SKU_ID")
    @Field(type = FieldType.Long)
    private Long psCSkuId;

    @JSONField(name = "PS_C_SKU_ECODE")
    @Field(type = FieldType.Keyword)
    private String psCSkuEcode;

    @JSONField(name = "PS_C_PRO_ID")
    @Field(type = FieldType.Long)
    private Long psCProId;

    @JSONField(name = "PS_C_PRO_ECODE")
    @Field(type = FieldType.Keyword)
    private String psCProEcode;

    @JSONField(name = "PS_C_PRO_ENAME")
    @Field(type = FieldType.Keyword)
    private String psCProEname;

    @JSONField(name = "PS_C_SPEC1_ID")
    @Field(type = FieldType.Long)
    private Long psCSpec1Id;

    @JSONField(name = "PS_C_SPEC1_ECODE")
    @Field(type = FieldType.Keyword)
    private String psCSpec1Ecode;

    @JSONField(name = "PS_C_SPEC1_ENAME")
    @Field(type = FieldType.Keyword)
    private String psCSpec1Ename;

    @JSONField(name = "PS_C_SPEC2_ID")
    @Field(type = FieldType.Long)
    private Long psCSpec2Id;

    @JSONField(name = "PS_C_SPEC2_ECODE")
    @Field(type = FieldType.Keyword)
    private String psCSpec2Ecode;

    @JSONField(name = "PS_C_SPEC2_ENAME")
    @Field(type = FieldType.Keyword)
    private String psCSpec2Ename;

    @JSONField(name = "PRICE_LIST")
    @Field(type = FieldType.Double)
    private BigDecimal priceList;

    @JSONField(name = "DISCOUNT")
    @Field(type = FieldType.Double)
    private BigDecimal discount;

    @JSONField(name = "QTY")
    @Field(type = FieldType.Double)
    private BigDecimal qty;

    @JSONField(name = "QTY_OUT")
    @Field(type = FieldType.Double)
    private BigDecimal qtyOut;

    @JSONField(name = "QTY_IN")
    @Field(type = FieldType.Double)
    private BigDecimal qtyIn;

    @JSONField(name = "REMARK")
    @Field(type = FieldType.Keyword)
    private String remark;

    @JSONField(name = "VERSION")
    @Field(type = FieldType.Long)
    private Long version;

    @JSONField(name = "OWNERENAME")
    @Field(type = FieldType.Keyword)
    private String ownerename;

    @JSONField(name = "MODIFIERENAME")
    @Field(type = FieldType.Keyword)
    private String modifierename;

    @JSONField(name = "PS_C_TEUS_ECODE")
    @Field(type = FieldType.Keyword)
    private String psCTeusEcode;

    @JSONField(name = "QTY_DIFF")
    @Field(type = FieldType.Double)
    private BigDecimal qtyDiff;

    @JSONField(name = "IS_NO_PACK")
    @Field(type = FieldType.Keyword)
    private String isNoPack;

    @JSONField(name = "PRICE_ORDER")
    @Field(type = FieldType.Double)
    private BigDecimal priceOrder;

    @JSONField(name = "PRICE_COUNTRY")
    @Field(type = FieldType.Double)
    private BigDecimal priceCountry;

    @JSONField(name = "UNIT_ID")
    @Field(type = FieldType.Long)
    private Long unitId;

    @JSONField(name = "UNIT_ECODE")
    @Field(type = FieldType.Keyword)
    private String unitEcode;

    @JSONField(name = "UNIT_ENAME")
    @Field(type = FieldType.Keyword)
    private String unitEname;

    @JSONField(name = "RESULT")
    @Field(type = FieldType.Keyword)
    private String result;

    @JSONField(name = "PACK_STATUS")
    @Field(type = FieldType.Keyword)
    private String packStatus;
}