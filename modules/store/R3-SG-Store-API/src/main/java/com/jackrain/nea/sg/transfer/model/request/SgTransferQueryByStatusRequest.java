package com.jackrain.nea.sg.transfer.model.request;

import lombok.Data;

import java.io.Serializable;

/**
 * @author csy
 * Date: 2019/5/15
 * Description: 根据单据状态 查询调拨单
 */
@Data
public class SgTransferQueryByStatusRequest implements Serializable {


    /**
     * 单据状态
     */
    private int billStatus;

    /**
     * 页大小
     */
    private int pageSize;

    /**
     * 当前页
     */
    private int pageNum;


    public SgTransferQueryByStatusRequest() {
        this.pageNum = 1;
        this.pageSize = 1000;
    }
}
