package com.jackrain.nea.sg.send.api;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.send.model.request.SgSendBillVoidRequest;
import com.jackrain.nea.sg.send.model.request.SgSendVoidSaveRequest;
import com.jackrain.nea.sg.send.model.result.SgSendBatchVoidResult;
import com.jackrain.nea.sg.send.model.result.SgSendSaveWithPriorityResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;

import java.util.List;

/**
 * @author 舒威
 * @since 2019/6/27
 * create at : 2019/6/27 14:58
 */
public interface SgSendVoidSaveCmd {

    ValueHolderV14<List<SgSendSaveWithPriorityResult>> voidSaveSgBSend(SgSendVoidSaveRequest request) throws NDSException;

    ValueHolderV14<SgSendBatchVoidResult> batchVoidSgBSend(List<SgSendBillVoidRequest> requests, User user) throws NDSException;
}
