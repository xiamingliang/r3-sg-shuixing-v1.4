package com.jackrain.nea.sg.transfer.model.request;

import com.jackrain.nea.web.face.User;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author leexh
 * @since 2019/10/15 16:58
 * desc: 查询调拨单信息（PDA使用）
 */
@Data
public class SgTransferForPdaRequest implements Serializable {

    /**
     * 门店ID(查询主表信息时必传)
     */
    private Long storeId;

    /**
     * 单据id
     */
    private Long id;


    /**
     * 单据编号
     */
    private String billNo;

    /**
     * 调拨类型
     */
    private Integer transferType;

    /**
     * 开始时间(修改时间)
     */
    private Date startTime;

    /**
     * 结束时间(修改时间)
     */
    private Date endTime;

    /**
     * 分页单位（每页显示条数）
     */
    private Integer pageSize;

    /**
     * 当前页
     */
    private Integer currentPage;

    /**
     * 用户信息
     */
    private User user;
}
