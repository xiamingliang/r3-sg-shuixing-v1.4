package com.jackrain.nea.sg.adjust.model.request;

import com.alibaba.fastjson.annotation.JSONField;
import com.jackrain.nea.web.face.User;
import lombok.Data;

import java.io.Serializable;

/**
 * @author csy
 */

@Data
public class SgAdjustBillAuditRequest implements Serializable {

    /**
     * 原单据类型
     */
    @JSONField(name = "SOURCE_BILL_TYPE")
    private Integer sourceBillType;

    /**
     * 原单据id
     */
    @JSONField(name = "SOURCE_BILL_ID")
    private Long sourceBillId;

    /**
     * 用户
     */
    private User user;
}
