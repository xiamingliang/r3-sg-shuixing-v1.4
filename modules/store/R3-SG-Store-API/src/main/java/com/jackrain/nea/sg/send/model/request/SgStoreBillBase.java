package com.jackrain.nea.sg.send.model.request;

import com.jackrain.nea.sg.basic.model.request.SgR3BaseRequest;
import lombok.Data;

import java.io.Serializable;

/**
 * @author 舒威
 * @since 2019/4/25
 * create at : 2019/4/25 15:58
 */
@Data
public class SgStoreBillBase extends SgR3BaseRequest implements Serializable {

    /**
     * 来源单据ID
     */
    private Long sourceBillId;

    /**
     * 来源单据编号
     */
    private String sourceBillNo;

    /**
     * 来源单据类型
     */
    private Integer sourceBillType;

    /**
     * 是否开启箱功能
     */
    private Boolean isOpenTeus;

    public SgStoreBillBase() {
        this.isOpenTeus = false;
    }
}
