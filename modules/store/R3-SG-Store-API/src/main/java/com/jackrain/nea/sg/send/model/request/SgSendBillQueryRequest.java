package com.jackrain.nea.sg.send.model.request;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author 舒威
 * @since 2019/7/18
 * create at : 2019/7/18 16:03
 */
@Data
public class SgSendBillQueryRequest implements Serializable {

    /**
     *  逻辑仓ids
     */
    private List<Long> storeIds;

    /**
     *  条码ids
     */
    private List<Long> skuIds;

    /**
     *  状态
     */
    private List<Integer> billStatus;

    /**
     *  来源单据类型
     */
    private List<Integer> sourceBillTypes;
}
