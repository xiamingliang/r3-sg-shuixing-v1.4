package com.jackrain.nea.sg.send.model.result;

import com.jackrain.nea.sg.send.model.table.SgBSend;
import com.jackrain.nea.sg.send.model.table.SgBSendItem;
import lombok.Data;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

/**
 * @author 舒威
 * @since 2019/6/27
 * create at : 2019/6/27 13:59
 */
@Data
public class SgSendBillQueryResult implements Serializable {

    // k：订单id,订单类型      v:逻辑发货单主子表
    private HashMap<String, HashMap<SgBSend, List<SgBSendItem>>> results;
}
