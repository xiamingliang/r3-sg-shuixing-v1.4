package com.jackrain.nea.sg.transfer.model.request;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class ScBTransferDiffRequest implements Serializable {
    private Long id;

    private String billNo;

    private Long ocBSendOutId;

    private Date billDate;

    private Long cpCStoreIdSend;

    private String cpCStoreEcodeSend;

    private String cpCStoreEnameSend;

    private Long cpCCustomerId;

    private String cpCCustomerEcode;

    private String cpCCustomerEname;

    private Long cpCStoreId;

    private String cpCStoreEcode;

    private String cpCStoreEname;

    private Date outTime;

    private Date inTime;

    private String diffReason;

    private String remark;

    private Integer isBatch;

    private String handleWay;

    private BigDecimal totQty;

    private Integer billStatus;

    private Long adOrgId;

    private Long adClientId;

    private Long ownerid;

    private String ownerename;

    private String ownername;

    private Date creationdate;

    private Long modifierid;

    private String modifierename;

    private String modifiername;

    private Date modifieddate;

    private String isactive;

    private String sendType;

    private Long sgBTransferPropId;

    private String sgBTransferPropEname;
}