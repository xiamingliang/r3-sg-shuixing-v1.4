package com.jackrain.nea.sg.transfer.model.request;

import com.alibaba.fastjson.annotation.JSONField;
import com.jackrain.nea.web.face.User;
import lombok.Data;

import java.io.Serializable;

/**
 * @author csy
 * Date: 2019/5/5
 * Description:
 */
@Data
public class SgTransferBillAuditRequest implements Serializable {
    /**
     * 原单据类型
     */
    @JSONField(name = "SOURCE_BILL_TYPE")
    private Integer sourceBillType;

    /**
     * 原单据id
     */
    @JSONField(name = "SOURCE_BILL_ID")
    private Long sourceBillId;

    /**
     * 用户
     */
    private User user;
}
