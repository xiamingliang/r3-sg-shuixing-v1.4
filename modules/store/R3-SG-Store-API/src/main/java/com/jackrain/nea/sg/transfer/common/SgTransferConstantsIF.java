package com.jackrain.nea.sg.transfer.common;

/**
 * @author csy
 * Date: 2019/5/7
 * Description:
 */
public interface SgTransferConstantsIF {

    /**
     * 调拨类型
     * 1=正常调拨
     * 2=订单调拨
     * 3=实缺调拨
     * 4=差异调拨
     */
    int TRANSFER_TYPE_NORMAL = 1;
    int TRANSFER_TYPE_ORDER = 2;
    int TRANSFER_TYPE_DEFICIENCY = 3;
    int TRANSFER_TYPE_DIFF = 4;

    //==============发货类型================
    /***
     * 发货类型 自提
     */
    String SEND_TYPE_ZT = "ZT";

    /**
     * 发货类型 调样
     */
    String SEND_TYPE_DY = "DY";


    /**
     * 是否批量处理:
     * 0=否
     * 1=是
     */
    int IS_BATCH_N = 0;
    int IS_BATCH_Y = 1;

    /**
     * 处理方式:
     * 1=发货方补单
     * 2=发货方调整
     * 3=收货方调整
     * 4=确认丢失
     */
    String HANDLE_WAY_FHBD = "1";
    String HANDLE_WAY_FHTZ = "2";
    String HANDLE_WAY_SHTZ = "3";
    String HANDLE_WAY_QRDS = "4";

    /**
     * 处理状态
     * 0=未处理
     * 1=已处理
     */
    int HANDLE_STATUS_N = 0;
    int HANDLE_STATUS_Y = 1;

    /**
     * 调拨差异单-单据状态
     * 1=未审核
     * 2=已审核
     * 3=已作废
     */
    int BILL_STATUS_WAIT = 1;
    int BILL_STATUS_AUDIT = 2;
    int BILL_STATUS_VOID = 3;


    /**
     * 调拨性质
     * 1 正品调拨
     * 2 次品调拨
     * 3 要货调拨
     * 4 退货调拨
     * 5 同级调拨
     */
    Long TRANSFER_PROP_ZP = 1L;
    Long TRANSFER_PROP_CP = 2L;
    Long TRANSFER_PROP_YH = 3L;
    Long TRANSFER_PROP_TH = 4L;
    Long TRANSFER_PROP_TJ = 5L;
    Long TRANSFER_PROP_ZJ = 6L;

    String TRANSFER_PROP_ZP_STR = "正品调拨";
    String TRANSFER_PROP_CP_STR = "次品调拨";
    String TRANSFER_PROP_YH_STR = "要货调拨";
    String TRANSFER_PROP_TH_STR = "退货调拨";
    String TRANSFER_PROP_TJ_STR = "同级调拨";
    String TRANSFER_PROP_ZJ_STR = "直接调拨";

    /**
     * 箱功能，是否箱
     * 0=否
     * 1=是
     */
    int IS_TEUS_N = 0;
    int IS_TEUS_Y = 1;
}
