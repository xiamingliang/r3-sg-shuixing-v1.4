package com.jackrain.nea.sg.adjust.api;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.adjust.model.request.SgAdjustBillSaveRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;

public interface SgAdjSaveAndAuditCmd {
    ValueHolderV14 saveAndAuditAdj(SgAdjustBillSaveRequest request) throws NDSException;
}
