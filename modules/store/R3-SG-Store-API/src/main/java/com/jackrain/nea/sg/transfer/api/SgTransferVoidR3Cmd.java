package com.jackrain.nea.sg.transfer.api;

import com.jackrain.nea.sys.Command;
import com.jackrain.nea.sys.domain.ValueHolderV14;
import com.jackrain.nea.web.face.User;

/**
 * @author csy
 * Date: 2019/5/10
 * Description:
 */
public interface SgTransferVoidR3Cmd extends Command {
    ValueHolderV14 cancelTransfer(Long id, User user);
}
