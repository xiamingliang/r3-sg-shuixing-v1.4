package com.jackrain.nea.sg.adjust.model.result;

import com.jackrain.nea.sg.adjust.model.table.SgBAdjust;
import com.jackrain.nea.sg.adjust.model.table.SgBAdjustImpItem;
import com.jackrain.nea.sg.adjust.model.table.SgBAdjustItem;
import com.jackrain.nea.sg.adjust.model.table.SgBAdjustTeusItem;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author: 舒威
 * @since: 2019/11/24
 * create at : 2019/11/24 10:41
 */
@Data
public class SgAdjustBillQueryResult implements Serializable {

    private SgBAdjust adjust;

    private List<SgBAdjustItem> adjustItems;

    private List<SgBAdjustImpItem> adjustImpItems;

    private List<SgBAdjustTeusItem> adjustTeusItems;

    private String redisKey;
}
