package com.jackrain.nea.sg.transfer.api;

import com.jackrain.nea.sg.transfer.model.request.OcTransferUpdatePickRequest;
import com.jackrain.nea.sg.transfer.model.result.OcTransferUpdatePickResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;

import java.util.List;

/**
 * @author leexh
 * @since 2019/11/29 14:07
 * desc: 批量更新拣货状态接口
 */
public interface SgTransferUpdatePickCmd {

    /**
     * 批量更新调拨单拣货状态接口
     *
     * @param request 入参
     * @return 出参
     */
    ValueHolderV14<List<OcTransferUpdatePickResult>> batchUpdateTransferPickStatus(OcTransferUpdatePickRequest request);
}
