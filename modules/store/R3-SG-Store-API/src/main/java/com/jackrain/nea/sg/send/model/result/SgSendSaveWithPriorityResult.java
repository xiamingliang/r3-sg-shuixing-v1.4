package com.jackrain.nea.sg.send.model.result;

import com.jackrain.nea.sg.basic.model.result.SgStoreWithPrioritySearchItemResult;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author 舒威
 * @since 2019/4/25
 * create at : 2019/4/25 15:47
 */
@Data
public class SgSendSaveWithPriorityResult extends SgSendBillSaveResult implements Serializable {

    /**
     * 寻源分仓结果
     */
    private int preoutResult;


    /**
     * 来源单据ID
     */
    private Long sourceBillId;

    /**
     * 来源单据编号
     */
    private String sourceBillNo;

    /**
     * 来源单据类型
     */
    private Integer sourceBillType;

    /**
     *缺货明细列表
     */
    private List<SgStoreWithPrioritySearchItemResult> outStockItemList;

}
