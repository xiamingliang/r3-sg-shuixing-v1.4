package com.jackrain.nea.sg.transfer.api;

import com.jackrain.nea.sys.Command;

/**
 * @author: 汪聿森
 * @Date: Created in 2019-08-23 17:09
 * @Description : 取消锁单
 */
public interface ScBTransferUnLockCmd  extends Command {
}
