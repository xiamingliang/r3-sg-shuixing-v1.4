package com.jackrain.nea.sg.receive.model.request;

import com.jackrain.nea.sg.in.model.table.SgBPhyInResult;
import com.jackrain.nea.sg.in.model.table.SgBPhyInResultImpItem;
import com.jackrain.nea.sg.in.model.table.SgBPhyInResultItem;
import com.jackrain.nea.sg.in.model.table.SgBPhyInResultTeusItem;
import com.jackrain.nea.sg.send.model.request.SgStoreBillBase;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author: 舒威
 * @since: 2019/4/28
 * create at : 2019/4/28 23:17
 */
@Data
public class SgReceiveBillSubmitRequest extends SgStoreBillBase implements Serializable {

    /**
     * 入库结果单
     */
    private SgBPhyInResult phyInResult;

    /**
     * 入库结果单明细
     */
    private List<SgBPhyInResultItem> phyInResultItems;

    /**
     * 入库结果单录入明细
     */
    private List<SgBPhyInResultImpItem> phyInResultImpItems;

    /**
     * 入库结果单箱内明细
     */
    private List<SgBPhyInResultTeusItem> phyInResultTeusItems;

    /**
     * 在途是否允许负库存(调拨差异用)
     */
    private Boolean isNegativePrein;

    public SgReceiveBillSubmitRequest() {
        this.isNegativePrein = false;
    }
}
