package com.jackrain.nea.sg.transfer.model.request;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author leexh
 * @since 2019/7/18 19:47
 * desc: 残次品明细
 */
@Data
public class ScBTransferDefectItemRequest implements Serializable {

    private Long id;

    private Long scBTransferId;

    private Long psCSkuId;

    private String psCSkuEcode;

    private Long psCProId;

    private String psCProEcode;

    private String psCProEname;

    private Long psCClrId;

    private String psCClrEcode;

    private String psCClrEname;

    private Long psCSizeId;

    private String psCSizeEcode;

    private String psCSizeEname;

    private BigDecimal priceList;

    private BigDecimal qty;

    private BigDecimal qtyOut;

    private BigDecimal qtyIn;

    private BigDecimal qtyDiff;

    private BigDecimal amtList;

    private BigDecimal amtOutList;

    private BigDecimal amtInList;

    private Long version;

    private Long ownerid;

    private String ownerename;

    private String ownername;

    private Date creationdate;

    private Long modifierid;

    private String modifierename;

    private String modifiername;

    private Date modifieddate;

    private String isactive;

    private String gbcode;

    private Long psCBrandId;

    private Long sgBPhyInResultId;

    private String sgBPhyInResultBillNo;
}
