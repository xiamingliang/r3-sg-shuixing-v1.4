package com.jackrain.nea.sg.assign.model.request;

import com.alibaba.fastjson.JSONObject;
import com.jackrain.nea.web.face.User;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author :csy
 * @version :1.0
 * description ：
 * @date :2019/11/7 19:48
 */
@Data
public class SgPrepareStoragePreRequest implements Serializable {

    private User user;

    private List<Long> ids;

    private List<JSONObject> data;
}
