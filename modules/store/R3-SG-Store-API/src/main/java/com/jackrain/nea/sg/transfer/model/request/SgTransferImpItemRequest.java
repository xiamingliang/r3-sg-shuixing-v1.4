package com.jackrain.nea.sg.transfer.model.request;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author leexh
 * @since 2019/11/11 17:59
 * desc:
 */
@Data
public class SgTransferImpItemRequest implements Serializable {

    /**
     * 明细ID
     */
    private Long id;

    /**
     * 调拨单ID
     */
    private Long scBTransferId;

    /**
     * 箱信息
     */
    private Integer isTeus;
    private Long psCTeusId;
    private String psCTeusEcode;

    /**
     * 配码信息
     */
    private Long psCMatchsizeId;
    private String psCMatchsizeEcode;
    private String psCMatchsizeEname;

    /**
     * 商品信息
     */
    private Long psCProId;
    private String psCProEcode;
    private String psCProEname;

    /**
     * 颜色信息
     */
    private Long psCSpec1Id;
    private String psCSpec1Ecode;
    private String psCSpec1Ename;

    /**
     * 尺寸信息
     */
    private Long psCSpec2Id;
    private String psCSpec2Ecode;
    private String psCSpec2Ename;


    /**
     * 条码信息
     */
    private Long psCSkuId;
    private String psCSkuEcode;

    /**
     * 价格
     */
    private BigDecimal priceList;
    private BigDecimal transferPrice;

    /**
     * 出入库数量
     */
    private BigDecimal qtyOut;
    private BigDecimal qtyIn;

    /**
     * 判定结果
     */
    private String result;
    /**
     * 原包材情况
     */
    private String packStatus;
}
