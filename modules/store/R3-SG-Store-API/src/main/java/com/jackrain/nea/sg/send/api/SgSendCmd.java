package com.jackrain.nea.sg.send.api;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.send.model.request.*;
import com.jackrain.nea.sg.send.model.result.*;
import com.jackrain.nea.sys.domain.ValueHolderV14;

import java.util.List;

/**
 * 逻辑发货单
 *
 * @author 舒威
 * @since 2019/4/19
 * create at : 2019/4/19 13:34
 */
public interface SgSendCmd {

    ValueHolderV14<SgSendBillQueryResult> querySgBSend(List<SgStoreBillBase> searchs) throws NDSException;

    ValueHolderV14<SgSendActualLackQueryResult> querySgBSend(SgSendBillQueryRequest searchs) throws NDSException;

    ValueHolderV14<SgSendBillSaveResult> saveSgBSend(SgSendBillSaveRequest request) throws NDSException;

    ValueHolderV14<SgSendBillVoidResult> voidSgBSend(SgSendBillVoidRequest request) throws NDSException;

    ValueHolderV14<SgSendBillSubmitResult> submitSgBSend(SgSendBillSubmitRequest request) throws NDSException;
}
