package com.jackrain.nea.sg.transfer.model.request;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author csy
 * Date: 2019/5/8
 * Description: 出库单回写明细信息
 */
@Data
public class SgTransferOutWBItemRequest extends SgTransferBaseWBItemRequest implements Serializable {

    @JSONField(name = "QTY_OUT")
    private BigDecimal qtyOut;

}
