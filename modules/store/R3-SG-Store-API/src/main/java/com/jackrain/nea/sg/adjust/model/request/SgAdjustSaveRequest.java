package com.jackrain.nea.sg.adjust.model.request;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.io.Serializable;

@Data
public class SgAdjustSaveRequest implements Serializable {

    @JSONField(name = "BILL_NO")
    private String billNo;

    @JSONField(name = "BILL_TYPE")
    private Integer billType;
    
    @JSONField(name = "SG_B_PHY_ADJUST_ID")
    private Long sgBPhyAdjustId;

    @JSONField(name = "SG_B_PHY_ADJUST_BILL_NO")
    private String sgBPhyAdjustBillNo;

    @JSONField(name = "SG_B_ADJUST_PROP_ID")
    private Long sgBAdjustPropId;

    @JSONField(name = "SOURCE_BILL_TYPE")
    private Integer sourceBillType;

    @JSONField(name = "SOURCE_BILL_ID")
    private Long sourceBillId;

    @JSONField(name = "SOURCE_BILL_NO")
    private String sourceBillNo;

    /**
     * 备注
     * 非必传
     */
    @JSONField(name = "REMARK")
    private String remark;

}