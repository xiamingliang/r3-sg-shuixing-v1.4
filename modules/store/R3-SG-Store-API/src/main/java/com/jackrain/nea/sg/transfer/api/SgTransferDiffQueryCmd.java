package com.jackrain.nea.sg.transfer.api;

import com.jackrain.nea.sg.transfer.model.request.SgTransferDiffQueryRequest;
import com.jackrain.nea.sg.transfer.model.table.ScBTransferDiff;
import com.jackrain.nea.sys.domain.ValueHolderV14;

import java.util.List;

/**
 * @author leexh
 * @since 2019/7/29 17:31
 * desc:
 */
public interface SgTransferDiffQueryCmd {

    /**
     * 根据订单ID查询调拨差异单
     */
    ValueHolderV14<List<ScBTransferDiff>> queryTransferDiff(SgTransferDiffQueryRequest request);
}
