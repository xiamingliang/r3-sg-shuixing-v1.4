package com.jackrain.nea.sg.send.model.request;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author: 舒威
 * @since: 2019/10/20
 * create at : 2019/10/20 11:33
 */
@Data
public class SgSendImpItemSaveRequest extends SgStoreBillTeusItemBase implements Serializable {

    private Long id;

    private Long sgBSendId;

    private Long sourceBillItemId;

    private Long cpCStoreId;

    private String cpCStoreEcode;

    private String cpCStoreEname;

    private Integer isTeus;

    private BigDecimal qty;

    private BigDecimal qtyPreout;

    private BigDecimal qtySend;

    private String remark;

    //是否发库存
    private Boolean isSendStorage;

    //负库存控制
    private Long isnegative;

    public SgSendImpItemSaveRequest() {
        this.isSendStorage = true;
        this.qty = BigDecimal.ZERO;
        this.qtyPreout = BigDecimal.ZERO;
        this.qtySend = BigDecimal.ZERO;
    }
}
