package com.jackrain.nea.sg.receive.model.request;

import com.jackrain.nea.sg.send.model.request.SgStoreBillBase;
import lombok.Data;

import java.io.Serializable;

/**
 * @author 舒威
 * @since 2019/5/7
 * create at : 2019/5/7 16:12
 */
@Data
public class SgReceiveBillCleanRequest extends SgStoreBillBase implements Serializable {

    private Long serviceNode;
}
