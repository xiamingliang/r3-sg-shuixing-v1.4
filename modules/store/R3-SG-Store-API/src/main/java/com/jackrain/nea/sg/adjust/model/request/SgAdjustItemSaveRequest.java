package com.jackrain.nea.sg.adjust.model.request;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class SgAdjustItemSaveRequest implements Serializable {
    @JSONField(name = "ID")
    private Long id;

    @JSONField(name = "SG_B_ADJUST_ID")
    private Long sgBAdjustId;

    @JSONField(name = "SOURCE_BILL_ITEM_ID")
    private Long sourceBillItemId;

    @JSONField(name = "CP_C_STORE_ID")
    private Long cpCStoreId;

    @JSONField(name = "CP_C_STORE_ECODE")
    private String cpCStoreEcode;

    @JSONField(name = "CP_C_STORE_ENAME")
    private String cpCStoreEname;

    @JSONField(name = "QTY")
    private BigDecimal qty;

    @JSONField(name = "PS_C_SKU_ID")
    private Long psCSkuId;

    @JSONField(name = "PS_C_SKU_ECODE")
    private String psCSkuEcode;

    @JSONField(name = "PS_C_PRO_ID")
    private Long psCProId;

    @JSONField(name = "PS_C_PRO_ECODE")
    private String psCProEcode;

    @JSONField(name = "PS_C_PRO_ENAME")
    private String psCProEname;

    @JSONField(name = "PS_C_SPEC1_ID")
    private Long psCSpec1Id;

    @JSONField(name = "PS_C_SPEC1_ECODE")
    private String psCSpec1Ecode;

    @JSONField(name = "PS_C_SPEC1_ENAME")
    private String psCSpec1Ename;

    @JSONField(name = "PS_C_SPEC2_ID")
    private Long psCSpec2Id;

    @JSONField(name = "PS_C_SPEC2_ECODE")
    private String psCSpec2Ecode;

    @JSONField(name = "PS_C_SPEC2_ENAME")
    private String psCSpec2Ename;

    @JSONField(name = "PRICE_LIST")
    private BigDecimal priceList;
    
    @JSONField(name = "SG_B_PHY_ADJUST_ITEM_ID")
    private Long sgBPhyAdjustItemId;

    @JSONField(name = "GBCODE")
    private String gbcode;
}