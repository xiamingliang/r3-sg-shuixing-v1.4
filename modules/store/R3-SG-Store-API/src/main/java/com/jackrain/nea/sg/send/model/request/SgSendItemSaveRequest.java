package com.jackrain.nea.sg.send.model.request;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author 舒威
 * @since 2019/4/19
 * create at : 2019/4/19 14:20
 */
@Data
public class SgSendItemSaveRequest extends SgStoreBillItemBase implements Serializable {

    private Long id;

    private Long sgBSendId;

    private Integer rank;

    private BigDecimal qty;

    private BigDecimal qtyPreout;

    private BigDecimal qtySend;

    private BigDecimal priceList;
    //是否发库存
    private Boolean isSendStorage;
    //负库存控制
    private Long isnegative;

    public SgSendItemSaveRequest() {
        this.isSendStorage = true;
        this.qtySend = BigDecimal.ZERO;
        this.qty = BigDecimal.ZERO;
        this.qtyPreout = BigDecimal.ZERO;
        this.priceList = BigDecimal.ZERO;
    }
}
