package com.jackrain.nea.sg.receive.model.request;

import com.alibaba.fastjson.annotation.JSONField;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.model.request.SgR3BaseRequest;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author 舒威
 * @since 2019/4/19
 * create at : 2019/4/28 22:10
 */
@Data
public class SgReceiveBillSaveRequest extends SgR3BaseRequest implements Serializable {

    /**
     * 主表信息
     */
    @JSONField(name = "SG_B_RECEIVE")
    private SgReceiveSaveRequest sgReceive;

    /**
     * 明细信息
     */
    @JSONField(name = "SG_B_RECEIVE_ITEM")
    private List<SgReceiveItemSaveRequest> itemList;

    /**
     * 录入明细信息
     */
    @JSONField(name = "SG_B_RECEIVE_IMP_ITEM")
    private List<SgReceiveImpItemSaveRequest> impItemList;

    /**
     * 箱内明细信息
     */
    @JSONField(name = "SG_B_RECEIVE_TEUS_ITEM")
    private List<SgReceiveTeusItemSaveRequest> teusItemList;

    /**
     * 录入明细是否从reids获取(默认fasle)
     */
    @JSONField(name = "REDIS_KEY")
    private String redisKey;

    /**
     * 占用报警类型
     */
    @JSONField(name = "PREOUT_WARNING_TYPE")
    private Integer preoutWarningType;

    /**
     * 修改方式
     */
    @JSONField(name = "METHOD")
    private Integer updateMethod;

    /**
     * 是否库存取消操作
     */
    private Boolean isCancel;

    /**
     * 在途是否允许负库存(调拨差异用)
     */
    private Boolean isNegativePrein;

    public SgReceiveBillSaveRequest() {
        this.isCancel = false;
        this.isNegativePrein = false;
        this.preoutWarningType = SgConstantsIF.PREOUT_RESULT_ERROR;
        this.updateMethod = SgConstantsIF.ITEM_UPDATE_TYPE_ALL;
    }
}
