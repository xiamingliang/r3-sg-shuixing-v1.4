package com.jackrain.nea.sg.transfer.api;

import com.jackrain.nea.sys.Command;

/**
 * @author leexh
 * @since 2019/7/2 15:20
 * desc: 导入采购订单未完成量服务
 */
public interface OcImportOrderToTransferCmd extends Command {
}
