package com.jackrain.nea.sg.receive.api;

import com.jackrain.nea.sys.Command;

/**
 * @author: 舒威
 * @since: 2019/4/28
 * create at : 2019/4/28 23:11
 */
public interface SgReceiveVoidR3Cmd extends Command {
}
