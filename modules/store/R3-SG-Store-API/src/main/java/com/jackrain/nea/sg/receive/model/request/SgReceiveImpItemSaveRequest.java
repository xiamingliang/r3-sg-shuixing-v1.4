package com.jackrain.nea.sg.receive.model.request;

import com.jackrain.nea.sg.send.model.request.SgStoreBillTeusItemBase;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author: heliu
 * @since: 2019/10/21
 * create at : 2019/10/21 14:49
 */
@Data
public class SgReceiveImpItemSaveRequest extends SgStoreBillTeusItemBase implements Serializable {

    private Long id;

    private Long sgBReceiveId;

    private Long sourceBillItemId;

    private Long cpCStoreId;

    private String cpCStoreEcode;

    private String cpCStoreEname;

    private Integer isTeus;

    private BigDecimal qty;

    private BigDecimal priceList;

    private BigDecimal qtyPrein;

    private BigDecimal qtyReceive;

    private String remark;

    //是否发库存
    private Boolean isReceiveStorage;

    //负库存控制
    private Long isnegative;

    public SgReceiveImpItemSaveRequest() {
        this.isReceiveStorage = true;
        this.qty = BigDecimal.ZERO;
        this.qtyPrein = BigDecimal.ZERO;
        this.qtyReceive = BigDecimal.ZERO;
    }
}