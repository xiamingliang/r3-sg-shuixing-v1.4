package com.jackrain.nea.sg.adjust.api;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.adjust.model.request.SgAdjustBillSaveRequest;
import com.jackrain.nea.sg.basic.model.result.SgR3BaseResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 * @author csy
 */
public interface SgAdjSaveCmd {
    /**
     * 保存/编辑 逻辑调整单
     *
     * @param request 参数
     * @return result
     * @throws NDSException 框架封装的异常
     */
    ValueHolderV14<SgR3BaseResult> saveAdj(SgAdjustBillSaveRequest request) throws NDSException;
    
}
