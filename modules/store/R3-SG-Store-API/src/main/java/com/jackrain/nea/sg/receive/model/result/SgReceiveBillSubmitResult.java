package com.jackrain.nea.sg.receive.model.result;

import lombok.Data;

import java.io.Serializable;

/**
 * @author: 舒威
 * @since: 2019/4/28
 * create at : 2019/4/28 23:24
 */
@Data
public class SgReceiveBillSubmitResult implements Serializable {

    /**
     * 逻辑仓id(退货单用)
     */
    private Long cpCStoreId;

    /**
     * 逻辑仓ecode(退货单用)
     */
    private String cpCStoreEcode;

    /**
     * 逻辑仓ename(退货单用)
     */
    private String cpCStoreEname;
}
