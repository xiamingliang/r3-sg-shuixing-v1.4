package com.jackrain.nea.sg.send.model.result;

import lombok.Data;

import java.io.Serializable;

/**
 * @author: 舒威
 * @since: 2019/11/14
 * create at : 2019/11/14 16:43
 */
@Data
public class SgSendVoidResult implements Serializable {

    /**
     * 来源单据id
     */
    private Long sourceBillId;

    /**
     * 来源单据类型
     */
    private Integer sourceBillType;

    /**
     * code 成功0 失败-1
     */
    private Integer code;

    /**
     * 返回信息
     */
    private String message;
}
