package com.jackrain.nea.sg.send.model.table;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.*;
import com.jackrain.nea.es.annotations.Document;
import com.jackrain.nea.es.annotations.Field;
import com.jackrain.nea.es.constans.FieldType;
import com.jackrain.nea.sys.domain.BaseModelES;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@TableName(value = "sg_b_send")
@Data
@Document(index = "sg_b_send",type = "sg_b_send")
@ApiModel
public class SgBSend extends BaseModelES {
    @JSONField(name = "ID")
    @TableField(fill = FieldFill.INSERT)
    @TableId(value = "id", type = IdType.INPUT)
    @Field(type = FieldType.Long)
    @ApiModelProperty(name = "ID")
    private Long id;

    @JSONField(name = "BILL_NO")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "BILL_NO")
    private String billNo;

    @JSONField(name = "BILL_STATUS")
    @Field(type = FieldType.Integer)
    @ApiModelProperty(name = "BILL_STATUS")
    private Integer billStatus;

    @JSONField(name = "SOURCE_BILL_TYPE")
    @Field(type = FieldType.Integer)
    @ApiModelProperty(name = "SOURCE_BILL_TYPE")
    private Integer sourceBillType;

    @JSONField(name = "SOURCE_BILL_ID")
    @Field(type = FieldType.Long)
    @ApiModelProperty(name = "SOURCE_BILL_ID")
    private Long sourceBillId;

    @JSONField(name = "SOURCE_BILL_NO")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "SOURCE_BILL_NO")
    private String sourceBillNo;

    @JSONField(name = "CP_C_SHOP_ID")
    @Field(type = FieldType.Long)
    @ApiModelProperty(name = "CP_C_SHOP_ID")
    private Long cpCShopId;

    @JSONField(name = "CP_C_SHOP_TITLE")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "CP_C_SHOP_TITLE")
    private String cpCShopTitle;

    @JSONField(name = "SOURCECODE")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "SOURCECODE")
    private String sourcecode;

    @JSONField(name = "DEAL_STATUS")
    @Field(type = FieldType.Integer)
    @ApiModelProperty(name = "DEAL_STATUS")
    private Integer dealStatus;

    @JSONField(name = "TOT_QTY_PREOUT")
    @Field(type = FieldType.Double)
    @ApiModelProperty(name = "TOT_QTY_PREOUT")
    private BigDecimal totQtyPreout;

    @JSONField(name = "TOT_QTY_SEND")
    @Field(type = FieldType.Double)
    @ApiModelProperty(name = "TOT_QTY_SEND")
    private BigDecimal totQtySend;

    @JSONField(name = "SEND_TIME")
    @Field(type = FieldType.Long)
    @ApiModelProperty(name = "SEND_TIME")
    private Date sendTime;

    @JSONField(name = "REMARK")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "REMARK")
    private String remark;

    @JSONField(name = "AD_ORG_ID")
    @Field(type = FieldType.Long)
    @ApiModelProperty(name = "AD_ORG_ID")
    private Long adOrgId;

    @JSONField(name = "ISACTIVE")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "ISACTIVE")
    private String isactive;

    @JSONField(name = "AD_CLIENT_ID")
    @Field(type = FieldType.Long)
    @ApiModelProperty(name = "AD_CLIENT_ID")
    private Long adClientId;

    @JSONField(name = "OWNERID")
    @Field(type = FieldType.Long)
    @ApiModelProperty(name = "OWNERID")
    private Long ownerid;

    @JSONField(name = "OWNERENAME")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "OWNERENAME")
    private String ownerename;

    @JSONField(name = "OWNERNAME")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "OWNERNAME")
    private String ownername;

    @JSONField(name = "CREATIONDATE")
    @Field(type = FieldType.Long)
    @ApiModelProperty(name = "CREATIONDATE")
    private Date creationdate;

    @JSONField(name = "MODIFIERID")
    @Field(type = FieldType.Long)
    @ApiModelProperty(name = "MODIFIERID")
    private Long modifierid;

    @JSONField(name = "MODIFIERENAME")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "MODIFIERENAME")
    private String modifierename;

    @JSONField(name = "MODIFIERNAME")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "MODIFIERNAME")
    private String modifiername;

    @JSONField(name = "MODIFIEDDATE")
    @Field(type = FieldType.Long)
    @ApiModelProperty(name = "MODIFIEDDATE")
    private Date modifieddate;

    @JSONField(name = "SERVICE_NODE")
    @Field(type = FieldType.Long)
    @ApiModelProperty(name = "SERVICE_NODE")
    private Long serviceNode;

    @JSONField(name = "BILL_DATE")
    @Field(type = FieldType.Long)
    @ApiModelProperty(name = "BILL_DATE")
    private Date billDate;

    @JSONField(name = "RESERVE_BIGINT01")
    @Field(type = FieldType.Long)
    @ApiModelProperty(name = "RESERVE_BIGINT01")
    private Long reserveBigint01;

    @JSONField(name = "RESERVE_BIGINT02")
    @Field(type = FieldType.Long)
    @ApiModelProperty(name = "RESERVE_BIGINT02")
    private Long reserveBigint02;

    @JSONField(name = "RESERVE_BIGINT03")
    @Field(type = FieldType.Long)
    @ApiModelProperty(name = "RESERVE_BIGINT03")
    private Long reserveBigint03;

    @JSONField(name = "RESERVE_BIGINT04")
    @Field(type = FieldType.Long)
    @ApiModelProperty(name = "RESERVE_BIGINT04")
    private Long reserveBigint04;

    @JSONField(name = "RESERVE_BIGINT05")
    @Field(type = FieldType.Long)
    @ApiModelProperty(name = "RESERVE_BIGINT05")
    private Long reserveBigint05;

    @JSONField(name = "RESERVE_BIGINT06")
    @Field(type = FieldType.Long)
    @ApiModelProperty(name = "RESERVE_BIGINT06")
    private Long reserveBigint06;

    @JSONField(name = "RESERVE_BIGINT07")
    @Field(type = FieldType.Long)
    @ApiModelProperty(name = "RESERVE_BIGINT07")
    private Long reserveBigint07;

    @JSONField(name = "RESERVE_BIGINT08")
    @Field(type = FieldType.Long)
    @ApiModelProperty(name = "RESERVE_BIGINT08")
    private Long reserveBigint08;

    @JSONField(name = "RESERVE_BIGINT09")
    @Field(type = FieldType.Long)
    @ApiModelProperty(name = "RESERVE_BIGINT09")
    private Long reserveBigint09;

    @JSONField(name = "RESERVE_BIGINT10")
    @Field(type = FieldType.Long)
    @ApiModelProperty(name = "RESERVE_BIGINT10")
    private Long reserveBigint10;

    @JSONField(name = "RESERVE_VARCHAR01")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "RESERVE_VARCHAR01")
    private String reserveVarchar01;

    @JSONField(name = "RESERVE_VARCHAR02")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "RESERVE_VARCHAR02")
    private String reserveVarchar02;

    @JSONField(name = "RESERVE_VARCHAR03")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "RESERVE_VARCHAR03")
    private String reserveVarchar03;

    @JSONField(name = "RESERVE_VARCHAR04")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "RESERVE_VARCHAR04")
    private String reserveVarchar04;

    @JSONField(name = "RESERVE_VARCHAR05")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "RESERVE_VARCHAR05")
    private String reserveVarchar05;

    @JSONField(name = "RESERVE_VARCHAR06")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "RESERVE_VARCHAR06")
    private String reserveVarchar06;

    @JSONField(name = "RESERVE_VARCHAR07")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "RESERVE_VARCHAR07")
    private String reserveVarchar07;

    @JSONField(name = "RESERVE_VARCHAR08")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "RESERVE_VARCHAR08")
    private String reserveVarchar08;

    @JSONField(name = "RESERVE_VARCHAR09")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "RESERVE_VARCHAR09")
    private String reserveVarchar09;

    @JSONField(name = "RESERVE_VARCHAR10")
    @Field(type = FieldType.Keyword)
    @ApiModelProperty(name = "RESERVE_VARCHAR10")
    private String reserveVarchar10;

    @JSONField(name = "RESERVE_DECIMAL01")
    @Field(type = FieldType.Double)
    @ApiModelProperty(name = "RESERVE_DECIMAL01")
    private BigDecimal reserveDecimal01;

    @JSONField(name = "RESERVE_DECIMAL02")
    @Field(type = FieldType.Double)
    @ApiModelProperty(name = "RESERVE_DECIMAL02")
    private BigDecimal reserveDecimal02;

    @JSONField(name = "RESERVE_DECIMAL03")
    @Field(type = FieldType.Double)
    @ApiModelProperty(name = "RESERVE_DECIMAL03")
    private BigDecimal reserveDecimal03;

    @JSONField(name = "RESERVE_DECIMAL04")
    @Field(type = FieldType.Double)
    @ApiModelProperty(name = "RESERVE_DECIMAL04")
    private BigDecimal reserveDecimal04;

    @JSONField(name = "RESERVE_DECIMAL05")
    @Field(type = FieldType.Double)
    @ApiModelProperty(name = "RESERVE_DECIMAL05")
    private BigDecimal reserveDecimal05;

    @JSONField(name = "RESERVE_DECIMAL06")
    @Field(type = FieldType.Double)
    @ApiModelProperty(name = "RESERVE_DECIMAL06")
    private BigDecimal reserveDecimal06;

    @JSONField(name = "RESERVE_DECIMAL07")
    @Field(type = FieldType.Double)
    @ApiModelProperty(name = "RESERVE_DECIMAL07")
    private BigDecimal reserveDecimal07;

    @JSONField(name = "RESERVE_DECIMAL08")
    @Field(type = FieldType.Double)
    @ApiModelProperty(name = "RESERVE_DECIMAL08")
    private BigDecimal reserveDecimal08;

    @JSONField(name = "RESERVE_DECIMAL09")
    @Field(type = FieldType.Double)
    @ApiModelProperty(name = "RESERVE_DECIMAL09")
    private BigDecimal reserveDecimal09;

    @JSONField(name = "RESERVE_DECIMAL10")
    @Field(type = FieldType.Double)
    @ApiModelProperty(name = "RESERVE_DECIMAL10")
    private BigDecimal reserveDecimal10;
}