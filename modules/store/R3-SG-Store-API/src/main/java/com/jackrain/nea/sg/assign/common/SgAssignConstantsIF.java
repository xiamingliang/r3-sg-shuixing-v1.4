package com.jackrain.nea.sg.assign.common;

/**
 * @author leexh
 * @since 2019/11/27 17:28
 * desc:
 */
public interface SgAssignConstantsIF {

    /**
     * 铺货单-单据状态
     * 1.未审核
     * 2.已审核
     * 3.已作废
     */
    int ASSIGN_STATUS_WAIT = 1;
    int ASSIGN_STATUS_AUDIT = 2;
    int ASSIGN_STATUS_VOID = 3;
}
