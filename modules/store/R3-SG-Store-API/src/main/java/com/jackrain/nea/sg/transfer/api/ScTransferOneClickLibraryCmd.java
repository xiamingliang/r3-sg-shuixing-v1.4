package com.jackrain.nea.sg.transfer.api;

import com.jackrain.nea.sys.Command;

/**
 * @author: 汪聿森
 * @Date: Created in 2019-09-12 13:42
 * @Description : 调拨单一键入库
 */
public interface ScTransferOneClickLibraryCmd extends Command {
}
