package com.jackrain.nea.sg.transfer.model.table;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.*;
import com.jackrain.nea.sys.domain.BaseModel;
import lombok.Data;

@TableName(value = "sc_b_transfer_out_item")
@Data
public class ScBTransferOutItem extends BaseModel {
    @JSONField(name = "ID")
    @TableField(fill = FieldFill.INSERT)
    @TableId(value = "id", type = IdType.INPUT)
    private Long id;

    @JSONField(name = "SC_B_TRANSFER_ID")
    private Long scBTransferId;

    @JSONField(name = "SG_B_PHY_OUT_RESULT_ID")
    private Long sgBPhyOutResultId;

    @JSONField(name = "SG_B_PHY_OUT_RESULT_BILL_NO")
    private String sgBPhyOutResultBillNo;

    @JSONField(name = "REMARK")
    private String remark;

    @JSONField(name = "CUSTOMIZE1")
    private String customize1;

    @JSONField(name = "CUSTOMIZE2")
    private String customize2;

    @JSONField(name = "CUSTOMIZE3")
    private String customize3;

    @JSONField(name = "CUSTOMIZE4")
    private String customize4;

    @JSONField(name = "CUSTOMIZE5")
    private String customize5;

    @JSONField(name = "OWNERENAME")
    private String ownerename;

    @JSONField(name = "MODIFIERENAME")
    private String modifierename;
}