package com.jackrain.nea.sg.transfer.model.result;

import com.jackrain.nea.sg.transfer.model.table.ScBTransferImpItem;
import com.jackrain.nea.sg.transfer.model.table.ScBTransferTeusItem;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author leexh
 * @since 2019/10/17 15:25
 * desc:
 */
@Data
public class ScTransferImpItemAndTeusItemResult extends ScBTransferImpItem implements Serializable {

    /**
     * 箱明细
     */
    private List<ScBTransferTeusItem> teusItems;
}
