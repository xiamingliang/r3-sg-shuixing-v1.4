package com.jackrain.nea.sg.send.model.request;

import com.jackrain.nea.sg.out.model.table.SgBPhyOutResult;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutResultImpItem;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutResultItem;
import com.jackrain.nea.sg.out.model.table.SgBPhyOutResultTeusItem;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author 舒威
 * @since 2019/4/25
 * create at : 2019/4/25 15:54
 */
@Data
public class SgSendBillSubmitRequest extends SgStoreBillBase implements Serializable {

    /**
     * 出库结果单
     */
    private SgBPhyOutResult phyOutResult;

    /**
     * 出库结果单明细
     */
    private List<SgBPhyOutResultItem> phyOutResultItems;

    /**
     * 出库结果单录入明细
     */
    private List<SgBPhyOutResultImpItem> phyOutResultImpItems;

    /**
     * 出库结果单箱内明细
     */
    private List<SgBPhyOutResultTeusItem> phyOutResultTeusItems;

    /**
     * 占用是否允许负库存（调拨差异用）
     */
    private Boolean isNegativePreout;

    private Boolean isRepeatOut;

    public SgSendBillSubmitRequest() {
        this.isNegativePreout = false;
        this.isRepeatOut = false;
    }
}
