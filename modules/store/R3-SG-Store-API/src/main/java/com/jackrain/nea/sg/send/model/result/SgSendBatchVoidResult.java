package com.jackrain.nea.sg.send.model.result;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author: 舒威
 * @since: 2019/11/14
 * create at : 2019/11/14 16:43
 */
@Data
public class SgSendBatchVoidResult implements Serializable {

    private List<SgSendVoidResult> voidResults;

}
