package com.jackrain.nea.sg.transfer.enums;

import lombok.Getter;


/**
 * @author csy
 * Date: 2019/5/7
 * Description: 调拨单单据状态
 */
@Getter
public enum SgTransferBillStatusEnum {
    /**
     * 未审核
     */
    UN_AUDITED(1, "未审核"),

    /**
     * 已审核未出库
     */
    AUDITED_NOT_OUT(2, "已审核未出库"),

    /**
     * 已部分出库未入库
     */
    AUDITED_PART_OUT_NOT_IN(3, "已部分出库未入库"),


    /**
     * 已部分出库已部分入库
     */
    AUDITED_PART_OUT_PART_IN(4, "已部分出库已部分入库"),

    /**
     * 已完全出库未入库
     */
    AUDITED_ALL_OUT_NOT_IN(5, "已完全出库未入库"),

    /**
     * 已完全出库已部分入库
     */
    AUDITED_ALL_OUT_PART_IN(6, "已完全出库已部分入库"),

    /**
     * 已完全出库已完全入库
     */
    AUDITED_ALL_OUT_ALL_IN(7, "已完全出库已完全入库"),

    /**
     * 已作废
     */
    VOIDED(8, "已作废");

    /**
     * 值
     */
    private int val;

    /**
     * 描述
     */
    private String des;

    SgTransferBillStatusEnum(int val, String des) {
        this.des = des;
        this.val = val;
    }


    /**
     * 获取枚举
     */
    public static SgTransferBillStatusEnum getTransferBillStatus(int val) {
        for (SgTransferBillStatusEnum statusEnum : SgTransferBillStatusEnum.values()) {
            if (statusEnum.getVal() == val) {
                return statusEnum;
            }
        }
        return null;
    }

    /**
     * 获取描述
     */
    public static String getTransferStatusDes(int val) {
        for (SgTransferBillStatusEnum statusEnum : SgTransferBillStatusEnum.values()) {
            if (statusEnum.getVal() == val) {
                return statusEnum.getDes();
            }
        }
        return null;
    }

}
