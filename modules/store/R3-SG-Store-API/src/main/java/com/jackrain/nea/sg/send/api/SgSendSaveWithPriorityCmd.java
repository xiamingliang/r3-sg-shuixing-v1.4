package com.jackrain.nea.sg.send.api;

import com.jackrain.nea.sg.send.model.request.SgSendSaveWithPriorityRequest;
import com.jackrain.nea.sg.send.model.result.SgSendSaveWithPriorityResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 * 寻源逻辑仓并新增(更新)逻辑发货单
 *
 * @author 舒威
 * @since 2019/4/25
 * create at : 2019/4/25 13:27
 */
public interface SgSendSaveWithPriorityCmd {

    ValueHolderV14<SgSendSaveWithPriorityResult> saveSendWithPriority(SgSendSaveWithPriorityRequest request);
}
