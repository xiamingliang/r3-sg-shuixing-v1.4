package com.jackrain.nea.sg.send.model.request;

import com.jackrain.nea.sg.basic.model.request.StStockPriorityRequest;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author 舒威
 * @since 2019/4/25
 * create at : 2019/4/25 13:30
 */
@Data
public class SgSendSaveWithPriorityRequest extends SgSendBillSaveRequest implements Serializable {

    /**
     * 来源单据表
     */
    private String table;

    /**
     * 虚拟仓优先级列表
     */
    private List<StStockPriorityRequest> priorityList;
}
