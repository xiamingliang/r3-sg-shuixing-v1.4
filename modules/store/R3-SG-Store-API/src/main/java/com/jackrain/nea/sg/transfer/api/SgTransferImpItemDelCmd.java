package com.jackrain.nea.sg.transfer.api;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.basic.model.result.SgR3BaseResult;
import com.jackrain.nea.sg.transfer.model.request.ScTransferItemDelRequest;
import com.jackrain.nea.sg.transfer.model.request.SgTransferBillSaveRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 * @author leexh
 * @since 2019/10/21 16:40
 * desc: 调拨单明细删除
 */
public interface SgTransferImpItemDelCmd {

    /**
     * 调拨单明细删除
     *
     * @param request 参数
     * @return result
     * @throws NDSException 框架封装异常
     */
    ValueHolderV14<SgR3BaseResult> delTransferItem(ScTransferItemDelRequest request) throws NDSException;
}
