package com.jackrain.nea.sg.send.model.request;

import lombok.Data;

import java.io.Serializable;

/**
 * @author 舒威
 * @since 2019/4/29
 * create at : 2019/4/29 20:59
 */
@Data
public class SgStoreBillItemBase implements Serializable {

    private Long sourceBillItemId;

    private Long cpCStoreId;

    private String cpCStoreEcode;

    private String cpCStoreEname;

    private Long psCSkuId;

    private String psCSkuEcode;

    private Long psCProId;

    private String psCProEcode;

    private String psCProEname;

    private Long psCSpec1Id;

    private String psCSpec1Ecode;

    private String psCSpec1Ename;

    private Long psCSpec2Id;

    private String psCSpec2Ecode;

    private String psCSpec2Ename;

    private String gbcode;
}
