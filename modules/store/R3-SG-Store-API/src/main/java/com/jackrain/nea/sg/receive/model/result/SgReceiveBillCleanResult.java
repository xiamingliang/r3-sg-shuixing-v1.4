package com.jackrain.nea.sg.receive.model.result;

import lombok.Data;

import java.io.Serializable;

/**
 * @author 舒威
 * @since 2019/5/7
 * create at : 2019/5/7 16:14
 */
@Data
public class SgReceiveBillCleanResult implements Serializable {

}
