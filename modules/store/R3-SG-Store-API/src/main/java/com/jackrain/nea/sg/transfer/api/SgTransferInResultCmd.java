package com.jackrain.nea.sg.transfer.api;

import com.jackrain.nea.sg.transfer.model.request.SgTransferInWBRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;


/**
 * @author leexh
 * @since 2020/4/20 18:58
 * desc: 退货调拨入库（erp使用）
 */
public interface SgTransferInResultCmd {

    /**
     * 退货调拨入库
     *
     * @param request 入参
     * @return v14
     */
    ValueHolderV14 writeBackInResultForErp(SgTransferInWBRequest request);

}
