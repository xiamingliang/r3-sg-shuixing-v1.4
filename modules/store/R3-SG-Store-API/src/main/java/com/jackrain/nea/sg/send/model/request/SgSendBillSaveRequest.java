package com.jackrain.nea.sg.send.model.request;

import com.alibaba.fastjson.annotation.JSONField;
import com.jackrain.nea.sg.basic.common.SgConstantsIF;
import com.jackrain.nea.sg.basic.model.request.SgR3BaseRequest;
import com.jackrain.nea.sg.send.model.table.SgBSend;
import com.jackrain.nea.sg.send.model.table.SgBSendItem;
import lombok.Data;
import lombok.NonNull;

import java.io.Serializable;
import java.util.List;

/**
 * @author 舒威
 * @since 2019/4/19
 * create at : 2019/4/19 13:52
 */
@Data
public class SgSendBillSaveRequest extends SgR3BaseRequest implements Serializable {

    /**
     * 主表信息
     */
    @NonNull
    @JSONField(name = "SG_B_SEND")
    private SgSendSaveRequest sgSend;

    /**
     * 明细信息
     */
    @NonNull
    @JSONField(name = "SG_B_SEND_ITEM")
    private List<SgSendItemSaveRequest> itemList;

    /**
     * 录入明细信息
     */
    @JSONField(name = "SG_B_SEND_IMP_ITEM")
    private List<SgSendImpItemSaveRequest> impItemList;

    /**
     * 箱内明细信息
     */
    @JSONField(name = "SG_B_SEND_TEUS_ITEM")
    private List<SgSendTeusItemSaveRequest> teusItemList;

    /**
     * 占用报警类型
     */
    @JSONField(name = "PREOUT_WARNING_TYPE")
    private Integer preoutWarningType;

    /**
     * 修改方式
     */
    @JSONField(name = "METHOD")
    private Integer updateMethod;

    /**
     * 是否为修改规则标志（全渠道用）
     */
    @JSONField(name = "IS_SPEC_CHANGE")
    private Boolean isSpecChange;


    /**
     * 占用是否允许负库存（调拨差异用）
     */
    @JSONField(name = "IS_NEGATIVE_PREOUT")
    private Boolean isNegativePreout;


    /**
     * 录入明细是否从reids获取(默认fasle)
     */
    @JSONField(name = "REDIS_KEY")
    private String redisKey;

    /**
     * 是否库存取消操作
     */
    private Boolean isCancel;

    private SgBSend orgSgSend;

    private List<SgBSendItem> orgItemList;

    /**
     * 是否订单占单
     */
    private Boolean isOrder;

    public SgSendBillSaveRequest() {
        this.isCancel = false;
        this.isSpecChange = false;
        this.isNegativePreout = false;
        this.preoutWarningType = SgConstantsIF.PREOUT_RESULT_ERROR;
        this.updateMethod = SgConstantsIF.ITEM_UPDATE_TYPE_ALL;
        this.isOrder = false;
    }
}
