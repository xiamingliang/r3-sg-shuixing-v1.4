package com.jackrain.nea.sg.transfer.model.table;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.*;
import com.jackrain.nea.es.annotations.Document;
import com.jackrain.nea.es.annotations.Field;
import com.jackrain.nea.es.constans.FieldType;
import com.jackrain.nea.sys.domain.BaseModelES;
import lombok.Data;

import java.math.BigDecimal;

@TableName(value = "sc_b_transfer_item")
@Data
@Document(index = "sc_b_transfer", type = "sc_b_transfer_item")
public class ScBTransferItem extends BaseModelES {
    @JSONField(name = "ID")
    @TableField(fill = FieldFill.INSERT)
    @TableId(value = "id", type = IdType.INPUT)
    @Field(type = FieldType.Long)
    private Long id;

    @JSONField(name = "SC_B_TRANSFER_ID")
    @Field(type = FieldType.Long)
    private Long scBTransferId;

    @JSONField(name = "PS_C_SKU_ID")
    @Field(type = FieldType.Long)
    private Long psCSkuId;

    @JSONField(name = "PS_C_SKU_ECODE")
    @Field(type = FieldType.Keyword)
    private String psCSkuEcode;

    @JSONField(name = "PS_C_PRO_ID")
    @Field(type = FieldType.Long)
    private Long psCProId;

    @JSONField(name = "PS_C_PRO_ECODE")
    @Field(type = FieldType.Keyword)
    private String psCProEcode;

    @JSONField(name = "PS_C_PRO_ENAME")
    @Field(type = FieldType.Keyword)
    private String psCProEname;

    @JSONField(name = "PS_C_CLR_ID")
    @Field(type = FieldType.Long)
    private Long psCClrId;

    @JSONField(name = "PS_C_CLR_ECODE")
    @Field(type = FieldType.Keyword)
    private String psCClrEcode;

    @JSONField(name = "PS_C_CLR_ENAME")
    @Field(type = FieldType.Keyword)
    private String psCClrEname;

    @JSONField(name = "PS_C_SIZE_ID")
    @Field(type = FieldType.Long)
    private Long psCSizeId;

    @JSONField(name = "PS_C_SIZE_ECODE")
    @Field(type = FieldType.Keyword)
    private String psCSizeEcode;

    @JSONField(name = "PS_C_SIZE_ENAME")
    @Field(type = FieldType.Keyword)
    private String psCSizeEname;

    @JSONField(name = "PRICE_LIST")
    @Field(type = FieldType.Double)
    private BigDecimal priceList;

    @JSONField(name = "QTY")
    @Field(type = FieldType.Double)
    private BigDecimal qty;

    @JSONField(name = "QTY_OUT")
    @Field(type = FieldType.Double)
    private BigDecimal qtyOut;

    @JSONField(name = "QTY_IN")
    @Field(type = FieldType.Double)
    private BigDecimal qtyIn;

    @JSONField(name = "QTY_DIFF")
    @Field(type = FieldType.Double)
    private BigDecimal qtyDiff;

    @JSONField(name = "AMT_LIST")
    @Field(type = FieldType.Double)
    private BigDecimal amtList;

    @JSONField(name = "AMT_OUT_LIST")
    @Field(type = FieldType.Double)
    private BigDecimal amtOutList;

    @JSONField(name = "AMT_IN_LIST")
    @Field(type = FieldType.Double)
    private BigDecimal amtInList;

    @JSONField(name = "VERSION")
    @Field(type = FieldType.Long)
    private Long version;

    @JSONField(name = "OWNERENAME")
    @Field(type = FieldType.Keyword)
    private String ownerename;

    @JSONField(name = "MODIFIERENAME")
    @Field(type = FieldType.Keyword)
    private String modifierename;

    @JSONField(name = "RESERVE_BIGINT01")
    @Field(type = FieldType.Long)
    private Long reserveBigint01;

    @JSONField(name = "RESERVE_BIGINT02")
    @Field(type = FieldType.Long)
    private Long reserveBigint02;

    @JSONField(name = "RESERVE_BIGINT03")
    @Field(type = FieldType.Long)
    private Long reserveBigint03;

    @JSONField(name = "RESERVE_BIGINT04")
    @Field(type = FieldType.Long)
    private Long reserveBigint04;

    @JSONField(name = "RESERVE_BIGINT05")
    @Field(type = FieldType.Long)
    private Long reserveBigint05;

    @JSONField(name = "RESERVE_BIGINT06")
    @Field(type = FieldType.Long)
    private Long reserveBigint06;

    @JSONField(name = "RESERVE_BIGINT07")
    @Field(type = FieldType.Long)
    private Long reserveBigint07;

    @JSONField(name = "RESERVE_BIGINT08")
    @Field(type = FieldType.Long)
    private Long reserveBigint08;

    @JSONField(name = "RESERVE_BIGINT09")
    @Field(type = FieldType.Long)
    private Long reserveBigint09;

    @JSONField(name = "RESERVE_BIGINT10")
    @Field(type = FieldType.Long)
    private Long reserveBigint10;

    @JSONField(name = "RESERVE_VARCHAR01")
    @Field(type = FieldType.Keyword)
    private String reserveVarchar01;

    @JSONField(name = "RESERVE_VARCHAR02")
    @Field(type = FieldType.Keyword)
    private String reserveVarchar02;

    @JSONField(name = "RESERVE_VARCHAR03")
    @Field(type = FieldType.Keyword)
    private String reserveVarchar03;

    @JSONField(name = "RESERVE_VARCHAR04")
    @Field(type = FieldType.Keyword)
    private String reserveVarchar04;

    @JSONField(name = "RESERVE_VARCHAR05")
    @Field(type = FieldType.Keyword)
    private String reserveVarchar05;

    @JSONField(name = "RESERVE_VARCHAR06")
    @Field(type = FieldType.Keyword)
    private String reserveVarchar06;

    @JSONField(name = "RESERVE_VARCHAR07")
    @Field(type = FieldType.Keyword)
    private String reserveVarchar07;

    @JSONField(name = "RESERVE_VARCHAR08")
    @Field(type = FieldType.Keyword)
    private String reserveVarchar08;

    @JSONField(name = "RESERVE_VARCHAR09")
    @Field(type = FieldType.Keyword)
    private String reserveVarchar09;

    @JSONField(name = "RESERVE_VARCHAR10")
    @Field(type = FieldType.Keyword)
    private String reserveVarchar10;

    @JSONField(name = "RESERVE_DECIMAL01")
    @Field(type = FieldType.Double)
    private BigDecimal reserveDecimal01;

    @JSONField(name = "RESERVE_DECIMAL02")
    @Field(type = FieldType.Double)
    private BigDecimal reserveDecimal02;

    @JSONField(name = "RESERVE_DECIMAL03")
    @Field(type = FieldType.Double)
    private BigDecimal reserveDecimal03;

    @JSONField(name = "RESERVE_DECIMAL04")
    @Field(type = FieldType.Double)
    private BigDecimal reserveDecimal04;

    @JSONField(name = "RESERVE_DECIMAL05")
    @Field(type = FieldType.Double)
    private BigDecimal reserveDecimal05;

    @JSONField(name = "RESERVE_DECIMAL06")
    @Field(type = FieldType.Double)
    private BigDecimal reserveDecimal06;

    @JSONField(name = "RESERVE_DECIMAL07")
    @Field(type = FieldType.Double)
    private BigDecimal reserveDecimal07;

    @JSONField(name = "RESERVE_DECIMAL08")
    @Field(type = FieldType.Double)
    private BigDecimal reserveDecimal08;

    @JSONField(name = "RESERVE_DECIMAL09")
    @Field(type = FieldType.Double)
    private BigDecimal reserveDecimal09;

    @JSONField(name = "RESERVE_DECIMAL10")
    @Field(type = FieldType.Double)
    private BigDecimal reserveDecimal10;

    @JSONField(name = "GBCODE")
    @Field(type = FieldType.Keyword)
    private String gbcode;

    @JSONField(name = "PS_C_BRAND_ID")
    @Field(type = FieldType.Long)
    private Long psCBrandId;

    @JSONField(name = "IS_NO_PACK")
    @Field(type = FieldType.Keyword)
    private String isNoPack;

    @JSONField(name = "PRICE_ORDER")
    @Field(type = FieldType.Double)
    private BigDecimal priceOrder;

    @JSONField(name = "AMT_ORDER")
    @Field(type = FieldType.Double)
    private BigDecimal amtOrder;

    @JSONField(name = "PRICE_COUNTRY")
    @Field(type = FieldType.Double)
    private BigDecimal priceCountry;

    @JSONField(name = "UNIT_ID")
    @Field(type = FieldType.Long)
    private Long unitId;

    @JSONField(name = "UNIT_ECODE")
    @Field(type = FieldType.Keyword)
    private String unitEcode;

    @JSONField(name = "UNIT_ENAME")
    @Field(type = FieldType.Keyword)
    private String unitEname;

    @JSONField(name = "RESULT")
    @Field(type = FieldType.Keyword)
    private String result;

    @JSONField(name = "PACK_STATUS")
    @Field(type = FieldType.Keyword)
    private String packStatus;
}