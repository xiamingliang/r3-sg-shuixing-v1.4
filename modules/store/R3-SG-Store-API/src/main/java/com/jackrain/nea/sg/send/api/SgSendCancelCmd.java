package com.jackrain.nea.sg.send.api;

import com.jackrain.nea.exception.NDSException;
import com.jackrain.nea.sg.send.model.request.SgSendBillSaveRequest;
import com.jackrain.nea.sg.send.model.result.SgSendBillSaveResult;
import com.jackrain.nea.sys.domain.ValueHolderV14;

/**
 * @author 舒威
 * @since 2019/6/10
 * create at : 2019/6/10 20:38
 */
public interface SgSendCancelCmd {

    ValueHolderV14<SgSendBillSaveResult> cancelSaveSgBSend(SgSendBillSaveRequest request) throws NDSException;
}
