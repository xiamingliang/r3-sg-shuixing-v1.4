package com.jackrain.nea.sg.send.model.result;

import com.jackrain.nea.sg.basic.model.result.SgR3BaseResult;
import com.jackrain.nea.sg.basic.model.result.SgStorageUpdateResult;
import lombok.Data;

import java.io.Serializable;

/**
 * @author 舒威
 * @since 2019/4/23
 * create at : 2019/4/23 17:59
 */
@Data
public class SgSendBillSaveResult extends SgR3BaseResult implements Serializable {

    /*
    * 占用标志
    */
    private int preoutUpdateResult;

    /**
     * 占用结果
     */
    private SgStorageUpdateResult updateResult;

}
