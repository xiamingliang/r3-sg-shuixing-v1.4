package com.jackrain.nea.sg.send.model.request;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author: 舒威
 * @since: 2019/10/20
 * create at : 2019/10/20 11:33
 */
@Data
public class SgSendTeusItemSaveRequest extends SgStoreBillTeusItemBase implements Serializable {

    private Long id;

    private Long sourceBillItemId;

    private Long sgBSendId;

    private Long cpCStoreId;

    private String cpCStoreEcode;

    private String cpCStoreEname;

    private BigDecimal qty;

    private String remark;

    public SgSendTeusItemSaveRequest() {
        this.qty = BigDecimal.ZERO;
    }
}
