package com.jackrain.nea.sg.send.model.result;

import com.jackrain.nea.sg.send.model.table.SgBSend;
import com.jackrain.nea.sg.send.model.table.SgBSendItem;
import com.jackrain.nea.web.face.User;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author: 舒威
 * @since: 2020/2/12
 * create at : 2020/2/12 15:46
 */
@Data
public class SgSendCheckResult implements Serializable {

    private SgBSend send;

    private List<SgBSendItem> sendItems;

    private User loginUser;
}
