package com.jackrain.nea.sg.transfer.model.result;

import com.github.pagehelper.PageInfo;
import com.jackrain.nea.sg.transfer.model.table.ScBTransfer;
import lombok.Data;

/**
 * @author csy
 * Date: 2019/5/15
 * Description:  根据单据状态 查询调拨单-分页返回
 */
@Data
public class SgTransferQueryByStatusResult {


    private PageInfo<ScBTransfer> transferPageInfo;
}
