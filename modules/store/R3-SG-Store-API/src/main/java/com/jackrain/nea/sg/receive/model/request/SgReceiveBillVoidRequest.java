package com.jackrain.nea.sg.receive.model.request;

import com.jackrain.nea.sg.send.model.request.SgStoreBillBase;
import lombok.Data;

import java.io.Serializable;

/**
 * @author: 舒威
 * @since: 2019/4/28
 * create at : 2019/4/28 22:56
 */
@Data
public class SgReceiveBillVoidRequest extends SgStoreBillBase implements Serializable {

    /*
    * 查询逻辑收货单是否存在 标志
    * */
    private Boolean isExist;

    public SgReceiveBillVoidRequest() {
        isExist = false;
    }
}
