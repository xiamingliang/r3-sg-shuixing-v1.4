package com.jackrain.nea.sg.transfer.model.request;

import com.alibaba.fastjson.annotation.JSONField;
import com.jackrain.nea.es.annotations.Field;
import com.jackrain.nea.es.constans.FieldType;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author csy
 */
@Data
public class SgTransferSaveRequest implements Serializable {
    @JSONField(name = "ID")
    private Long id;

    @JSONField(name = "ORDER_NO")
    private Long orderNo;

    @JSONField(name = "TRANSFER_TYPE")
    private Integer transferType;

    @JSONField(name = "BILL_NO")
    private String billNo;

    @JSONField(name = "BILL_DATE")
    private Date billDate;

    @JSONField(name = "CP_C_ORIG_ID")
    private Long cpCOrigId;

    @JSONField(name = "CP_C_ORIG_ECODE")
    private String cpCOrigEcode;

    @JSONField(name = "CP_C_ORIG_ENAME")
    private String cpCOrigEname;

    @JSONField(name = "CP_C_DEST_ID")
    private Long cpCDestId;

    @JSONField(name = "CP_C_DEST_ECODE")
    private String cpCDestEcode;

    @JSONField(name = "CP_C_DEST_ENAME")
    private String cpCDestEname;

    @JSONField(name = "CP_C_CUSTOMER_ID")
    private Long cpCCustomerId;

    @JSONField(name = "CP_C_CUSTOMER_ECODE")
    private String cpCCustomerEcode;

    @JSONField(name = "CP_C_CUSTOMER_ENAME")
    private String cpCCustomerEname;

    @JSONField(name = "OC_B_SEND_OUT_ID")
    private Long ocBSendOutId;

    @JSONField(name = "OC_B_SEND_OUT_NO")
    private String ocBSendOutNo;

    @JSONField(name = "OUT_DATE")
    private Date outDate;

    @JSONField(name = "IN_DATE")
    private Date inDate;


    @JSONField(name = "REMARK")
    private String remark;

    @JSONField(name = "TRANS_WAY")
    private Long transWay;

    @JSONField(name = "CP_C_LOGISTICS_ID")
    private Long cpCLogisticsId;

    @JSONField(name = "CP_C_LOGISTICS_ECODE")
    private String cpCLogisticsEcode;

    @JSONField(name = "CP_C_LOGISTICS_ENAME")
    private String cpCLogisticsEname;

    @JSONField(name = "TRANS_WAY_NO")
    private String transWayNo;

    @JSONField(name = "LOGISTICS_REMARK")
    private String logisticsRemark;

    @JSONField(name = "SAP_STATUS")
    private Integer sapStatus;

    @JSONField(name = "STATUS")
    private Integer status;

    @JSONField(name = "RECEIVER_NAME")
    private String receiverName;

    @JSONField(name = "RECEIVER_MOBILE")
    private String receiverMobile;

    @JSONField(name = "RECEIVER_PHONE")
    private String receiverPhone;

    @JSONField(name = "RECEIVER_PROVINCE_ID")
    private Long receiverProvinceId;

    @JSONField(name = "RECEIVER_PROVINCE_ECODE")
    private String receiverProvinceEcode;

    @JSONField(name = "RECEIVER_PROVINCE_ENAME")
    private String receiverProvinceEname;

    @JSONField(name = "RECEIVER_CITY_ID")
    private Long receiverCityId;

    @JSONField(name = "RECEIVER_CITY_ECODE")
    private String receiverCityEcode;

    @JSONField(name = "RECEIVER_CITY_ENAME")
    private String receiverCityEname;

    @JSONField(name = "RECEIVER_DISTRICT_ID")
    private Long receiverDistrictId;

    @JSONField(name = "RECEIVER_DISTRICT_ECODE")
    private String receiverDistrictEcode;

    @JSONField(name = "RECEIVER_DISTRICT_ENAME")
    private String receiverDistrictEname;

    @JSONField(name = "RECEIVER_ADDRESS")
    private String receiverAddress;

    @JSONField(name = "RECEIVER_ZIP")
    private String receiverZip;

    @JSONField(name = "BATCH_NO")
    private String batchNo;

    @JSONField(name = "SEND_TYPE")
    private String sendType;

    @JSONField(name = "FULL_LINK")
    private String fullLink;

    @JSONField(name = "IS_AUTO_IN")
    private String isAutoIn;

    @JSONField(name = "IS_AUTO_OUT")
    private String isAutoOut;

    @JSONField(name = "SG_B_TRANSFER_PROP_ID")
    private Long sgBTransferPropId;

    @JSONField(name = "SG_B_TRANSFER_PROP_ENAME")
    private String sgBTransferPropEname;

    /**
     * 新增调拨单征用(标记字段)
     */
    @JSONField(name = "RESERVE_VARCHAR01")
    private String reserveVarchar01;

    @JSONField(name = "HANDLE_WAY")
    private String handleWay;

    @JSONField(name = "IS_OUT_REDUCE")
    private String isOutReduce;

    @JSONField(name = "IS_IN_INCREASE")
    private String isInIncrease;

    /**
     * wms单号
     */
    @JSONField(name = "WMS_BILL_NO")
    private String wmsBillNo;

    @JSONField(name = "QTY_TEUS_LOAD")
    private BigDecimal qtyTeusLoad;

    @JSONField(name = "QTY_TEUS_STANDARD")
    private BigDecimal qtyTeusStandard;

    @JSONField(name = "QTY_PACK")
    private BigDecimal qtyPack;

    @JSONField(name = "QTY_ENLARGE")
    private BigDecimal qtyEnlarge;

    @JSONField(name = "QTY_ANNEX")
    private BigDecimal qtyAnnex;

    @JSONField(name = "QTY_MEDIUM")
    private BigDecimal qtyMedium;

    @JSONField(name = "QTY_TEUS_MATTRESS")
    private BigDecimal qtyTeusMattress;

    @JSONField(name = "QTY_TEUS_MATTRESS_ENLARGE")
    private BigDecimal qtyTeusMattressEnlarge;

    @JSONField(name = "STOCK_BILL_NO")
    @Field(type = FieldType.Keyword)
    private String stockBillNo;

    @JSONField(name = "TRANSOUT_BILL_NO")
    private String transoutBillNo;

    @JSONField(name = "TRANSIN_BILL_NO")
    private String transinBillNo;

    @JSONField(name = "UNOUTER_ID")
    private Integer unouterId;

    @JSONField(name = "UNOUTER_ENAME")
    private String unouterEname;

    @JSONField(name = "UNOUTER_NAME")
    private String unouterName;

    @JSONField(name = "UNOUT_TIME")
    private Date unoutTime;

    @JSONField(name = "REQUEST_BILL_NO")
    private String requestBillNo;

    @JSONField(name = "SOURCE_BILL_ID")
    private Integer sourceBillId;

    @JSONField(name = "SOURCE_BILL_NO")
    private String sourceBillNo;

    @JSONField(name = "CP_C_CLIENT_ID")
    private Long cpCClientId;

    @JSONField(name = "CP_C_CLIENT_ECODE")
    private String cpCClientEcode;

    @JSONField(name = "CP_C_CLIENT_ENAME")
    private String cpCClientEname;

    @JSONField(name = "CP_C_CUSTOMERUP_ID")
    private Integer cpCCustomerupId;

    @JSONField(name = "CP_C_CUSTOMERUP_ECODE")
    private String cpCCustomerupEcode;

    @JSONField(name = "CP_C_CUSTOMERUP_ENAME")
    private String cpCCustomerupEname;

    @JSONField(name = "PRICE_CONDITION")
    private String priceCondition;

    @JSONField(name = "ORIG_BELONGS_LEGAL")
    private String origBelongsLegal;

    @JSONField(name = "DEST_BELONGS_LEGAL")
    private String destBelongsLegal;

    @JSONField(name = "APPLY_BILL_NO")
    private String applyBillNo;
}