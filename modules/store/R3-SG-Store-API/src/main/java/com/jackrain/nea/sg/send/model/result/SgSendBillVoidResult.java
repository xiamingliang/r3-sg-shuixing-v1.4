package com.jackrain.nea.sg.send.model.result;

import com.jackrain.nea.sg.basic.model.result.SgR3BaseResult;
import lombok.Data;

import java.io.Serializable;

/**
 * @author 舒威
 * @since 2019/4/24
 * create at : 2019/4/24 13:15
 */
@Data
public class SgSendBillVoidResult extends SgR3BaseResult implements Serializable {
}
