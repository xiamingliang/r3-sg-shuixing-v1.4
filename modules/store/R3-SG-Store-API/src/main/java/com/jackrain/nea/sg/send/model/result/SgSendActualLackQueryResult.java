package com.jackrain.nea.sg.send.model.result;

import com.jackrain.nea.sg.send.model.table.SgBSend;
import com.jackrain.nea.sg.send.model.table.SgBSendItem;
import lombok.Data;

import java.io.Serializable;
import java.util.HashMap;

/**
 * @author 舒威
 * @since 2019/7/18
 * create at : 2019/7/18 16:11
 */
@Data
public class SgSendActualLackQueryResult implements Serializable {

    // k：逻辑发货单     v: 逻辑仓,条码  -  逻辑发货单明细
    private HashMap<SgBSend, HashMap<String,SgBSendItem>> results;
}
