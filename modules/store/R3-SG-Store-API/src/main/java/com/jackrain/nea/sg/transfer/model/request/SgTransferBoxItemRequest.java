package com.jackrain.nea.sg.transfer.model.request;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author leexh
 * @since 2019/11/22 18:34
 * desc:
 */
@Data
public class SgTransferBoxItemRequest implements Serializable {

    @JSONField(name = "PS_C_TEUS_ID")
    private Long psCTeusId;

    @JSONField(name = "PS_C_TEUS_ECODE")
    private String psCTeusEcode;

    @JSONField(name = "PS_C_MATCHSIZE_ID")
    private Long psCMatchsizeId;

    @JSONField(name = "PS_C_MATCHSIZE_ECODE")
    private String psCMatchsizeEcode;

    @JSONField(name = "PS_C_MATCHSIZE_ENAME")
    private String psCMatchsizeEname;

    @JSONField(name = "PS_C_SKU_ID")
    private Long psCSkuId;

    @JSONField(name = "PS_C_SKU_ECODE")
    private String psCSkuEcode;

    @JSONField(name = "PS_C_PRO_ID")
    private Long psCProId;

    @JSONField(name = "PS_C_PRO_ECODE")
    private String psCProEcode;

    @JSONField(name = "PS_C_PRO_ENAME")
    private String psCProEname;

    @JSONField(name = "PS_C_SPEC1_ID")
    private Long psCSpec1Id;

    @JSONField(name = "PS_C_SPEC1_ECODE")
    private String psCSpec1Ecode;

    @JSONField(name = "PS_C_SPEC1_ENAME")
    private String psCSpec1Ename;

    @JSONField(name = "PS_C_SPEC2_ID")
    private Long psCSpec2Id;

    @JSONField(name = "PS_C_SPEC2_ECODE")
    private String psCSpec2Ecode;

    @JSONField(name = "PS_C_SPEC2_ENAME")
    private String psCSpec2Ename;

    @JSONField(name = "QTY")
    private BigDecimal qty;
}
