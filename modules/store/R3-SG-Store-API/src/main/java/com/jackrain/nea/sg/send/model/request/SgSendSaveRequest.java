package com.jackrain.nea.sg.send.model.request;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author 舒威
 * @since 2019/4/19
 * create at : 2019/4/19 13:52
 */
@Data
public class SgSendSaveRequest extends SgStoreBillBase implements Serializable {

    private Long id; //订单id  -1表示新增 其他表示更新

    private String billNo;

    private Integer billStatus;

    private Date billDate;

    private Long cpCShopId;

    private String cpCShopTitle;

    private String sourcecode;

    private Integer dealStatus;

    private Date sendTime;

    private String remark;

    private Long serviceNode;

    public SgSendSaveRequest() {
        this.billDate = new Date();
        this.dealStatus = 0;
    }
}
