package com.jackrain.nea.sg.send.model.request;

import lombok.Data;

import java.io.Serializable;

/**
 * @author 舒威
 * @since 2019/4/24
 * create at : 2019/4/24 13:15
 */
@Data
public class SgSendBillCleanRequest extends SgStoreBillBase implements Serializable {

    private Long serviceNode;

    private String remark;

    private Boolean isRepeatClean;

    public SgSendBillCleanRequest() {
        this.isRepeatClean = false;
    }
}
