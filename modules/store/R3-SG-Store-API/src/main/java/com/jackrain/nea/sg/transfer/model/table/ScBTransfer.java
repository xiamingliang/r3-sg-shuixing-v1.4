package com.jackrain.nea.sg.transfer.model.table;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.*;
import com.jackrain.nea.es.annotations.Document;
import com.jackrain.nea.es.annotations.Field;
import com.jackrain.nea.es.constans.FieldType;
import com.jackrain.nea.sys.domain.BaseModelES;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@TableName(value = "sc_b_transfer")
@Data
@Document(index = "sc_b_transfer", type = "sc_b_transfer")
public class ScBTransfer extends BaseModelES {
    @JSONField(name = "ID")
    @TableField(fill = FieldFill.INSERT)
    @TableId(value = "id", type = IdType.INPUT)
    @Field(type = FieldType.Long)
    private Long id;

    @JSONField(name = "ORDER_NO")
    @Field(type = FieldType.Long)
    private Long orderNo;

    @JSONField(name = "TRANSFER_TYPE")
    @Field(type = FieldType.Integer)
    private Integer transferType;

    @JSONField(name = "BILL_NO")
    @Field(type = FieldType.Keyword)
    private String billNo;

    @JSONField(name = "BILL_DATE")
    @Field(type = FieldType.Long)
    private Date billDate;

    @JSONField(name = "CP_C_ORIG_ID")
    @Field(type = FieldType.Long)
    private Long cpCOrigId;

    @JSONField(name = "CP_C_ORIG_ECODE")
    @Field(type = FieldType.Keyword)
    private String cpCOrigEcode;

    @JSONField(name = "CP_C_ORIG_ENAME")
    @Field(type = FieldType.Keyword)
    private String cpCOrigEname;

    @JSONField(name = "CP_C_DEST_ID")
    @Field(type = FieldType.Long)
    private Long cpCDestId;

    @JSONField(name = "CP_C_DEST_ECODE")
    @Field(type = FieldType.Keyword)
    private String cpCDestEcode;

    @JSONField(name = "CP_C_DEST_ENAME")
    @Field(type = FieldType.Keyword)
    private String cpCDestEname;

    @JSONField(name = "CP_C_CUSTOMER_ID")
    @Field(type = FieldType.Long)
    private Long cpCCustomerId;

    @JSONField(name = "CP_C_CUSTOMER_ECODE")
    @Field(type = FieldType.Keyword)
    private String cpCCustomerEcode;

    @JSONField(name = "CP_C_CUSTOMER_ENAME")
    @Field(type = FieldType.Keyword)
    private String cpCCustomerEname;

    @JSONField(name = "OC_B_SEND_OUT_ID")
    @Field(type = FieldType.Long)
    private Long ocBSendOutId;

    @JSONField(name = "OC_B_SEND_OUT_NO")
    @Field(type = FieldType.Keyword)
    private String ocBSendOutNo;

    @JSONField(name = "OUT_DATE")
    @Field(type = FieldType.Long)
    private Date outDate;

    @JSONField(name = "IN_DATE")
    @Field(type = FieldType.Long)
    private Date inDate;

    @JSONField(name = "REMARK")
    @Field(type = FieldType.Keyword)
    private String remark;

    @JSONField(name = "TRANS_WAY")
    @Field(type = FieldType.Long)
    private Long transWay;

    @JSONField(name = "CP_C_LOGISTICS_ID")
    @Field(type = FieldType.Long)
    private Long cpCLogisticsId;

    @JSONField(name = "CP_C_LOGISTICS_ECODE")
    @Field(type = FieldType.Keyword)
    private String cpCLogisticsEcode;

    @JSONField(name = "CP_C_LOGISTICS_ENAME")
    @Field(type = FieldType.Keyword)
    private String cpCLogisticsEname;

    @JSONField(name = "TRANS_WAY_NO")
    @Field(type = FieldType.Keyword)
    private String transWayNo;

    @JSONField(name = "LOGISTICS_REMARK")
    @Field(type = FieldType.Keyword)
    private String logisticsRemark;

    @JSONField(name = "TOT_QTY")
    @Field(type = FieldType.Double)
    private BigDecimal totQty;

    @JSONField(name = "TOT_QTY_OUT")
    @Field(type = FieldType.Double)
    private BigDecimal totQtyOut;

    @JSONField(name = "TOT_QTY_IN")
    @Field(type = FieldType.Double)
    private BigDecimal totQtyIn;

    @JSONField(name = "TOT_QTY_DIFF")
    @Field(type = FieldType.Double)
    private BigDecimal totQtyDiff;

    @JSONField(name = "TOT_AMT_LIST")
    @Field(type = FieldType.Double)
    private BigDecimal totAmtList;

    @JSONField(name = "TOT_AMT_OUT_LIST")
    @Field(type = FieldType.Double)
    private BigDecimal totAmtOutList;

    @JSONField(name = "TOT_AMT_IN_LIST")
    @Field(type = FieldType.Double)
    private BigDecimal totAmtInList;

    @JSONField(name = "SAP_STATUS")
    @Field(type = FieldType.Integer)
    private Integer sapStatus;

    @JSONField(name = "STATUS")
    @Field(type = FieldType.Integer)
    private Integer status;

    @JSONField(name = "OUT_STATUS")
    @Field(type = FieldType.Integer)
    private Integer outStatus;

    @JSONField(name = "IN_STATUS")
    @Field(type = FieldType.Integer)
    private Integer inStatus;

    @JSONField(name = "PICK_STATUS")
    @Field(type = FieldType.Integer)
    private Integer pickStatus;

    @JSONField(name = "STATUS_ID")
    @Field(type = FieldType.Long)
    private Long statusId;

    @JSONField(name = "STATUS_ENAME")
    @Field(type = FieldType.Keyword)
    private String statusEname;

    @JSONField(name = "STATUS_NAME")
    @Field(type = FieldType.Keyword)
    private String statusName;

    @JSONField(name = "STATUS_TIME")
    @Field(type = FieldType.Long)
    private Date statusTime;

    @JSONField(name = "UNCHECK_ID")
    @Field(type = FieldType.Long)
    private Long uncheckId;

    @JSONField(name = "UNCHECK_ENAME")
    @Field(type = FieldType.Keyword)
    private String uncheckEname;

    @JSONField(name = "UNCHECK_NAME")
    @Field(type = FieldType.Keyword)
    private String uncheckName;

    @JSONField(name = "UNCHECK_TIME")
    @Field(type = FieldType.Long)
    private Date uncheckTime;

    @JSONField(name = "OUTER_ID")
    @Field(type = FieldType.Long)
    private Long outerId;

    @JSONField(name = "OUTER_ENAME")
    @Field(type = FieldType.Keyword)
    private String outerEname;

    @JSONField(name = "OUTER_NAME")
    @Field(type = FieldType.Keyword)
    private String outerName;

    @JSONField(name = "OUT_TIME")
    @Field(type = FieldType.Long)
    private Date outTime;

    @JSONField(name = "INER_ID")
    @Field(type = FieldType.Long)
    private Long inerId;

    @JSONField(name = "INER_ENAME")
    @Field(type = FieldType.Keyword)
    private String inerEname;

    @JSONField(name = "INER_NAME")
    @Field(type = FieldType.Keyword)
    private String inerName;

    @JSONField(name = "IN_TIME")
    @Field(type = FieldType.Long)
    private Date inTime;

    @JSONField(name = "PICKER_ID")
    @Field(type = FieldType.Long)
    private Long pickerId;

    @JSONField(name = "PICKER_ENAME")
    @Field(type = FieldType.Keyword)
    private String pickerEname;

    @JSONField(name = "PICKER_NAME")
    @Field(type = FieldType.Keyword)
    private String pickerName;

    @JSONField(name = "PICK_TIME")
    @Field(type = FieldType.Long)
    private Date pickTime;

    @JSONField(name = "DELER_ID")
    @Field(type = FieldType.Long)
    private Long delerId;

    @JSONField(name = "DELER_ENAME")
    @Field(type = FieldType.Keyword)
    private String delerEname;

    @JSONField(name = "DELER_NAME")
    @Field(type = FieldType.Keyword)
    private String delerName;

    @JSONField(name = "DEL_TIME")
    @Field(type = FieldType.Long)
    private Date delTime;

    @JSONField(name = "VERSION")
    @Field(type = FieldType.Long)
    private Long version;

    @JSONField(name = "OWNERENAME")
    @Field(type = FieldType.Keyword)
    private String ownerename;

    @JSONField(name = "MODIFIERENAME")
    @Field(type = FieldType.Keyword)
    private String modifierename;

    @JSONField(name = "RECEIVER_NAME")
    @Field(type = FieldType.Keyword)
    private String receiverName;

    @JSONField(name = "RECEIVER_MOBILE")
    @Field(type = FieldType.Keyword)
    private String receiverMobile;

    @JSONField(name = "RECEIVER_PHONE")
    @Field(type = FieldType.Keyword)
    private String receiverPhone;

    @JSONField(name = "RECEIVER_PROVINCE_ID")
    @Field(type = FieldType.Long)
    private Long receiverProvinceId;

    @JSONField(name = "RECEIVER_PROVINCE_ECODE")
    @Field(type = FieldType.Keyword)
    private String receiverProvinceEcode;

    @JSONField(name = "RECEIVER_PROVINCE_ENAME")
    @Field(type = FieldType.Keyword)
    private String receiverProvinceEname;

    @JSONField(name = "RECEIVER_CITY_ID")
    @Field(type = FieldType.Long)
    private Long receiverCityId;

    @JSONField(name = "RECEIVER_CITY_ECODE")
    @Field(type = FieldType.Keyword)
    private String receiverCityEcode;

    @JSONField(name = "RECEIVER_CITY_ENAME")
    @Field(type = FieldType.Keyword)
    private String receiverCityEname;

    @JSONField(name = "RECEIVER_DISTRICT_ID")
    @Field(type = FieldType.Long)
    private Long receiverDistrictId;

    @JSONField(name = "RECEIVER_DISTRICT_ECODE")
    @Field(type = FieldType.Keyword)
    private String receiverDistrictEcode;

    @JSONField(name = "RECEIVER_DISTRICT_ENAME")
    @Field(type = FieldType.Keyword)
    private String receiverDistrictEname;

    @JSONField(name = "RECEIVER_ADDRESS")
    @Field(type = FieldType.Keyword)
    private String receiverAddress;

    @JSONField(name = "RECEIVER_ZIP")
    @Field(type = FieldType.Keyword)
    private String receiverZip;

    @JSONField(name = "BATCH_NO")
    @Field(type = FieldType.Keyword)
    private String batchNo;

    @JSONField(name = "RESERVE_BIGINT01")
    @Field(type = FieldType.Long)
    private Long reserveBigint01;

    @JSONField(name = "RESERVE_BIGINT02")
    @Field(type = FieldType.Long)
    private Long reserveBigint02;

    @JSONField(name = "RESERVE_BIGINT03")
    @Field(type = FieldType.Long)
    private Long reserveBigint03;

    @JSONField(name = "RESERVE_BIGINT04")
    @Field(type = FieldType.Long)
    private Long reserveBigint04;

    @JSONField(name = "RESERVE_BIGINT05")
    @Field(type = FieldType.Long)
    private Long reserveBigint05;

    @JSONField(name = "RESERVE_BIGINT06")
    @Field(type = FieldType.Long)
    private Long reserveBigint06;

    @JSONField(name = "RESERVE_BIGINT07")
    @Field(type = FieldType.Long)
    private Long reserveBigint07;

    @JSONField(name = "RESERVE_BIGINT08")
    @Field(type = FieldType.Long)
    private Long reserveBigint08;

    @JSONField(name = "RESERVE_BIGINT09")
    @Field(type = FieldType.Long)
    private Long reserveBigint09;

    @JSONField(name = "RESERVE_BIGINT10")
    @Field(type = FieldType.Long)
    private Long reserveBigint10;

    @JSONField(name = "RESERVE_VARCHAR01")
    @Field(type = FieldType.Keyword)
    private String reserveVarchar01;

    @JSONField(name = "RESERVE_VARCHAR02")
    @Field(type = FieldType.Keyword)
    private String reserveVarchar02;

    @JSONField(name = "RESERVE_VARCHAR03")
    @Field(type = FieldType.Keyword)
    private String reserveVarchar03;

    @JSONField(name = "RESERVE_VARCHAR04")
    @Field(type = FieldType.Keyword)
    private String reserveVarchar04;

    @JSONField(name = "RESERVE_VARCHAR05")
    @Field(type = FieldType.Keyword)
    private String reserveVarchar05;

    @JSONField(name = "RESERVE_VARCHAR06")
    @Field(type = FieldType.Keyword)
    private String reserveVarchar06;

    @JSONField(name = "RESERVE_VARCHAR07")
    @Field(type = FieldType.Keyword)
    private String reserveVarchar07;

    @JSONField(name = "RESERVE_VARCHAR08")
    @Field(type = FieldType.Keyword)
    private String reserveVarchar08;

    @JSONField(name = "RESERVE_VARCHAR09")
    @Field(type = FieldType.Keyword)
    private String reserveVarchar09;

    @JSONField(name = "RESERVE_VARCHAR10")
    @Field(type = FieldType.Keyword)
    private String reserveVarchar10;

    @JSONField(name = "RESERVE_DECIMAL01")
    @Field(type = FieldType.Double)
    private BigDecimal reserveDecimal01;

    @JSONField(name = "RESERVE_DECIMAL02")
    @Field(type = FieldType.Double)
    private BigDecimal reserveDecimal02;

    @JSONField(name = "RESERVE_DECIMAL03")
    @Field(type = FieldType.Double)
    private BigDecimal reserveDecimal03;

    @JSONField(name = "RESERVE_DECIMAL04")
    @Field(type = FieldType.Double)
    private BigDecimal reserveDecimal04;

    @JSONField(name = "RESERVE_DECIMAL05")
    @Field(type = FieldType.Double)
    private BigDecimal reserveDecimal05;

    @JSONField(name = "RESERVE_DECIMAL06")
    @Field(type = FieldType.Double)
    private BigDecimal reserveDecimal06;

    @JSONField(name = "RESERVE_DECIMAL07")
    @Field(type = FieldType.Double)
    private BigDecimal reserveDecimal07;

    @JSONField(name = "RESERVE_DECIMAL08")
    @Field(type = FieldType.Double)
    private BigDecimal reserveDecimal08;

    @JSONField(name = "RESERVE_DECIMAL09")
    @Field(type = FieldType.Double)
    private BigDecimal reserveDecimal09;

    @JSONField(name = "RESERVE_DECIMAL10")
    @Field(type = FieldType.Double)
    private BigDecimal reserveDecimal10;

    @JSONField(name = "SEND_TYPE")
    @Field(type = FieldType.Keyword)
    private String sendType;

    @JSONField(name = "FULL_LINK")
    @Field(type = FieldType.Keyword)
    private String fullLink;

    @JSONField(name = "IS_AUTO_IN")
    @Field(type = FieldType.Keyword)
    private String isAutoIn;

    @JSONField(name = "IS_AUTO_OUT")
    @Field(type = FieldType.Keyword)
    private String isAutoOut;

    @JSONField(name = "SG_B_TRANSFER_PROP_ID")
    @Field(type = FieldType.Long)
    private Long sgBTransferPropId;

    @JSONField(name = "SG_B_TRANSFER_PROP_ENAME")
    @Field(type = FieldType.Keyword)
    private String sgBTransferPropEname;

    @JSONField(name = "HANDLE_WAY")
    @Field(type = FieldType.Keyword)
    private String handleWay;

    @JSONField(name = "IS_OUT_SETTLE_LOG")
    @Field(type = FieldType.Integer)
    private Integer isOutSettleLog;

    @JSONField(name = "IS_IN_SETTLE_LOG")
    @Field(type = FieldType.Integer)
    private Integer isInSettleLog;

    @JSONField(name = "IS_LOCKED")
    @Field(type = FieldType.Integer)
    private Integer isLocked;

    @JSONField(name = "CP_C_CLIENT_ID")
    @Field(type = FieldType.Long)
    private Long cpCClientId;

    @JSONField(name = "QTY_TEUS_LOAD")
    @Field(type = FieldType.Double)
    private BigDecimal qtyTeusLoad;

    @JSONField(name = "QTY_TEUS_STANDARD")
    @Field(type = FieldType.Double)
    private BigDecimal qtyTeusStandard;

    @JSONField(name = "QTY_PACK")
    @Field(type = FieldType.Double)
    private BigDecimal qtyPack;

    @JSONField(name = "QTY_ENLARGE")
    @Field(type = FieldType.Double)
    private BigDecimal qtyEnlarge;

    @JSONField(name = "QTY_ANNEX")
    @Field(type = FieldType.Double)
    private BigDecimal qtyAnnex;

    @JSONField(name = "QTY_MEDIUM")
    @Field(type = FieldType.Double)
    private BigDecimal qtyMedium;

    @JSONField(name = "QTY_TEUS_MATTRESS")
    @Field(type = FieldType.Double)
    private BigDecimal qtyTeusMattress;

    @JSONField(name = "QTY_TEUS_MATTRESS_ENLARGE")
    @Field(type = FieldType.Double)
    private BigDecimal qtyTeusMattressEnlarge;

    @JSONField(name = "STOCK_BILL_NO")
    @Field(type = FieldType.Keyword)
    private String stockBillNo;

    @JSONField(name = "TRANSOUT_BILL_NO")
    @Field(type = FieldType.Keyword)
    private String transoutBillNo;

    @JSONField(name = "TRANSIN_BILL_NO")
    @Field(type = FieldType.Keyword)
    private String transinBillNo;

    @JSONField(name = "UNOUTER_ID")
    @Field(type = FieldType.Long)
    private Long unouterId;

    @JSONField(name = "UNOUTER_ENAME")
    @Field(type = FieldType.Keyword)
    private String unouterEname;

    @JSONField(name = "UNOUTER_NAME")
    @Field(type = FieldType.Keyword)
    private String unouterName;

    @JSONField(name = "UNOUT_TIME")
    @Field(type = FieldType.Long)
    private Date unoutTime;

    @JSONField(name = "REQUEST_BILL_NO")
    @Field(type = FieldType.Keyword)
    private String requestBillNo;

    @JSONField(name = "IS_OUT_REDUCE")
    @Field(type = FieldType.Keyword)
    private String isOutReduce;

    @JSONField(name = "IS_STORAGE_CHG")
    @Field(type = FieldType.Keyword)
    private String isStorageChg;

    @JSONField(name = "IS_IN_INCREASE")
    @Field(type = FieldType.Keyword)
    private String isInIncrease;

    @JSONField(name = "PICK_OUT_STATUS")
    @Field(type = FieldType.Integer)
    private Integer pickOutStatus;

    @JSONField(name = "PICKER_OUT_ID")
    @Field(type = FieldType.Long)
    private Long pickerOutId;

    @JSONField(name = "PICKER_OUT_ENAME")
    @Field(type = FieldType.Keyword)
    private String pickerOutEname;

    @JSONField(name = "PICKER_OUT_NAME")
    @Field(type = FieldType.Keyword)
    private String pickerOutName;

    @JSONField(name = "PICKER_OUT_TIME")
    @Field(type = FieldType.Long)
    private Date pickerOutTime;

    @JSONField(name = "WMS_BILL_NO")
    @Field(type = FieldType.Keyword)
    private String wmsBillNo;

    @JSONField(name = "SOURCE_BILL_ID")
    @Field(type = FieldType.Long)
    private Long sourceBillId;

    @JSONField(name = "SOURCE_BILL_NO")
    @Field(type = FieldType.Keyword)
    private String sourceBillNo;

    @JSONField(name = "CP_C_CLIENT_ECODE")
    @Field(type = FieldType.Keyword)
    private String cpCClientEcode;

    @JSONField(name = "CP_C_CLIENT_ENAME")
    @Field(type = FieldType.Keyword)
    private String cpCClientEname;

    @JSONField(name = "CP_C_CUSTOMERUP_ID")
    @Field(type = FieldType.Long)
    private Long cpCCustomerupId;

    @JSONField(name = "CP_C_CUSTOMERUP_ECODE")
    @Field(type = FieldType.Keyword)
    private String cpCCustomerupEcode;

    @JSONField(name = "CP_C_CUSTOMERUP_ENAME")
    @Field(type = FieldType.Keyword)
    private String cpCCustomerupEname;

    @JSONField(name = "PRICE_CONDITION")
    @Field(type = FieldType.Keyword)
    private String priceCondition;

    @JSONField(name = "ORIG_BELONGS_LEGAL")
    @Field(type = FieldType.Keyword)
    private String origBelongsLegal;

    @JSONField(name = "APPLY_BILL_NO")
    @Field(type = FieldType.Keyword)
    private String applyBillNo;

    @JSONField(name = "DEST_BELONGS_LEGAL")
    @Field(type = FieldType.Keyword)
    private String destBelongsLegal;

    @JSONField(name = "QTY_IN_TEUS")
    @Field(type = FieldType.Double)
    private BigDecimal qtyInTeus;

    @JSONField(name = "QTY_IN_PACK")
    @Field(type = FieldType.Double)
    private BigDecimal qtyInPack;

    @JSONField(name = "QTY_IN_UNIT")
    @Field(type = FieldType.Double)
    private BigDecimal qtyInUnit;

    @JSONField(name = "REFUND_SALE_BILL_NO")
    @Field(type = FieldType.Keyword)
    private String refundSaleBillNo;
}