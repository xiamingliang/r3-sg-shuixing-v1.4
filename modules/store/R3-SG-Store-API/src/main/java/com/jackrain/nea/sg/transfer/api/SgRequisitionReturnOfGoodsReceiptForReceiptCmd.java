package com.jackrain.nea.sg.transfer.api;

import com.jackrain.nea.sg.transfer.model.request.SgRequisitionRequest;
import com.jackrain.nea.sys.domain.ValueHolderV14;

import java.util.List;
import java.util.Map;

/**
 * @author ZhangQK
 * 销退收货签收--调拨单
 * @date 2020/4/8 17:34
 */
public interface SgRequisitionReturnOfGoodsReceiptForReceiptCmd {
    ValueHolderV14<Map<String,Integer>> sgSaleReceiving(List<SgRequisitionRequest> ocSaleReundQueryRequestList);
}
